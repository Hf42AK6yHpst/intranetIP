<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_stuPerformance.php");

intranet_auth();
intranet_opendb();

$outputMode = standardizeFormPostValue($_POST['outputMode']);
$viewType = standardizeFormPostValue($_POST['viewType']);
$startDate = standardizeFormPostValue($_POST['startDate']);
$endDate = standardizeFormPostValue($_POST['endDate']);
$subjectGroupIdAry = $_POST['subjectGroupIdAry'];

$stuPerformance = new libeClassApp_stuPerformance();

$headerAry = array();
$dataAry = array();


$headerAry = $stuPerformance->getSubjectViewReportHeaderAry($viewType);
$numOfHeader = count($headerAry);
$dataAry = $stuPerformance->getSubjectViewReportDataAry($viewType, $startDate, $endDate, $subjectGroupIdAry);
$numOfRow = count($dataAry);

if ($outputMode == 'print') {
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once $PATH_WRT_ROOT.'/includes/table/table_commonTable.php';
	$linterface = new interface_html();
	
	
	$tableObj = new table_commonTable();
	$tableObj->setTableType('view');
	$tableObj->setApplyConvertSpecialChars(false);
	$tableObj->addHeaderTr(new tableTr());
	$tableObj->addHeaderCell(new tableTh('#'));
	for ($i=0; $i<$numOfHeader; $i++) {
		$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
	}
	
	for ($i=0; $i<$numOfRow; $i++) {
		$tableObj->addBodyTr(new tableTr());
		$tableObj->addBodyCell(new tableTd($i+1));
		$tableObj->addBodyCell(new tableTd($dataAry[$i]['subjectName']));
		$tableObj->addBodyCell(new tableTd($dataAry[$i]['subjectGroupName']));
		$tableObj->addBodyCell(new tableTd($dataAry[$i]['className']));
		$tableObj->addBodyCell(new tableTd($dataAry[$i]['classNumber']));
		$tableObj->addBodyCell(new tableTd($dataAry[$i]['studentName']));
		
		if ($viewType == 'raw') {
			$tableObj->addBodyCell(new tableTd($dataAry[$i]['dateInput']));
			$tableObj->addBodyCell(new tableTd($dataAry[$i]['score']));
			$tableObj->addBodyCell(new tableTd($dataAry[$i]['comment']));
		}
		else if ($viewType == 'statistics') {
			$tableObj->addBodyCell(new tableTd($dataAry[$i]['totalScore']));
			$tableObj->addBodyCell(new tableTd($dataAry[$i]['numOfComment']));
		}
	}
	$htmlAry['resultTable'] = $tableObj->returnHtml();
	
	
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	$x = '';
	$x .= '<table width="100%" align="center" class="print_hide" border="0">'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</table>'."\r\n";
	$x .= $htmlAry['resultTable'];
	
	echo $x;
}
else if ($outputMode == 'export') {
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	
	$lexport = new libexporttext();
	$lclass = new libclass();
	
	$className = $lclass->getClassName($classId);
	
	$exportDataAry = array();
	for ($i=0; $i<$numOfRow; $i++) {
		$exportDataAry[$i][] = $dataAry[$i]['subjectName'];
		$exportDataAry[$i][] = $dataAry[$i]['subjectGroupName'];
		$exportDataAry[$i][] = $dataAry[$i]['className'];
		$exportDataAry[$i][] = $dataAry[$i]['classNumber'];
		$exportDataAry[$i][] = strip_tags($dataAry[$i]['studentName']);
		
		if ($viewType == 'raw') {
			$exportDataAry[$i][] = $dataAry[$i]['dateInput'];
			$exportDataAry[$i][] = $dataAry[$i]['score'];
			$exportDataAry[$i][] = $dataAry[$i]['comment'];
		}
		else {
			$exportDataAry[$i][] = $dataAry[$i]['totalScore'];
			$exportDataAry[$i][] = $dataAry[$i]['numOfComment'];
		}
	}
	
	$exportContent = $lexport->GET_EXPORT_TXT($exportDataAry, $headerAry);
	$lexport->EXPORT_FILE("student_performance_subject_".date('YmdHis').".csv", $exportContent);
}

intranet_closedb();
?>