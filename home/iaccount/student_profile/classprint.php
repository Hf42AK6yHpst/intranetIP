<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src=/file/$imgfile align=absmiddle>" : "&nbsp;";

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

$printTable = "
	<table width='100%' align='center' class='print_hide' border='0'>
	<tr>
		<td align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
	</tr>
</table>";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
	$school_info = "<table border=0 width='100%'>\n";
	$school_info .= "<tr><td align='center' class='eSportprinttitle'>$schoolbadge <b>$school_name<br><br>$i_Profile_Attendance</b></font></td></tr>\n";
	$school_info .= "</table>\n<br>\n";
}

$lclass = new libclass();
$lattend = new libattendance();
$lword = new libwordtemplates();
$reasons = $lword->getWordListAttendance();


$hasRight = true;

$lu_viewer = new libuser($UserID);
$lattend = new libattendance();

if ($lu_viewer->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    if(empty($class))
    {
    	$hasRight = false;
	}
	else
	{
		# class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid'",$classid, 0, 1);

	    # re-new classname
	    $ClassName = $lclass->getClassName($classid);
	}
}
else
{
	$hasRight = false;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



$class_name = $lattend->getClassName($classid);
$now = time();

$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

$sql = "SELECT ClassID FROM INTRNAET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";

$result = $lattend->getAttendanceListByClass($class_name,$start,$end,$reason);

$x = "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='eSporttableborder'>
<tr>
<td width='10%' class='eSporttdborder eSportprinttabletitle'>$i_UserClassNumber</td>
<td width='30%' class='eSporttdborder eSportprinttabletitle'>$i_UserEnglishName</td>
<td width='20%' class='eSporttdborder eSportprinttabletitle'>$i_Profile_Absent</td>
<td width='20%' class='eSporttdborder eSportprinttabletitle'>$i_Profile_Late</td>
<td width='20%' class='eSporttdborder eSportprinttabletitle'>$i_Profile_EarlyLeave</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$absence,$late,$earlyleave) = $result[$i];

     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
     $display = $name;
     $link = "$display";
     $x .= "<tr>
             <td class='eSporttdborder eSportprinttext'>$classnumber</td>
             <td class='eSporttdborder eSportprinttext'>$link</td>
             <td class='eSporttdborder eSportprinttext'>$absence</td>
             <td class='eSporttdborder eSportprinttext'>$late</td>
             <td class='eSporttdborder eSportprinttext'>$earlyleave</td>
            </tr>\n";
}
$x .= "</table>\n";

$datetype += 0;

$functionbar = "&nbsp; $i_Attendance_Date: $i_Profile_From $start $i_Profile_To $end";
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$functionbar .= "<br>&nbsp; $i_Attendance_Reason: $reasonSelect";

?>

<?=$printTable?>
<?=$school_info?>
<table width='100%' border='0' cellpadding='4' cellspacing='0'>
<tr><td class='eSportprinttitle'>&nbsp; <?= $i_UserClassName.": ".$class_name?></td></tr>
<tr><td class='eSportprinttitle'><?= $functionbar ?></td></tr>

<tr>
	<td><?=$x?></td>
        </tr>
</table>

<?
intranet_closedb();
?>
