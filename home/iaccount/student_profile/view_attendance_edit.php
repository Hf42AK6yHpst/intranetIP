<?php
# 20140306
//die();

# Editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $i_Attendance_DetailedAttendanceRecord;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lword = new libwordtemplates();
$lattend = new libattendance();
?>
<? /* ?>
<script language="JavaScript" src=<?=$intranet_httppath?>/templates/ajax_yahoo.js></script>
<script language="JavaScript" src=<?=$intranet_httppath?>/templates/ajax_connection.js></script>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     #ToolTip2{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
isMenu = true;
</script>

<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
<div id="ToolTip2"></div>
<div id="ToolTip3" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>

<? */ ?>

<script language="Javascript" src="/templates/tooltip.js"></script>
<style type="text/css">
	#ToolTip2{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>
<div id="ToolTip2"></div>
<div id="ToolTip3" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>

<script language="JavaScript">
isToolTip = false;
function setToolTip(value)
{
	isToolTip=value;
}
</script>

<style type="text/css">
	#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
    isMenu =false;
</script>
<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>

<SCRIPT Language="JavaScript">
<!--
/*
function putBack(target, value)
{
	var temp = eval("document.form1.reason_"+target);
    temp.value = value;
	hideMenu('ToolMenu');
}
function moveToolTip2(lay, FromTop, FromLeft)
{
	if (tooltip_ns6)
	{
		var myElement = document.getElementById(lay);
		myElement.style.left = (FromLeft + 10) + "px";
		myElement.style.top = (FromTop + document.body.scrollTop) + "px";
	}
	
	else if(tooltip_ie4)
	{
		eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
	}
	else if(tooltip_ns4)
	{
		eval(doc + lay + sty + ".top = "  +  FromTop)
	}
	
	if (!tooltip_ns6) 
		eval(doc + lay + sty + ".left = " + (FromLeft + 10));
}
// AJAX follow-up
var callback = {
    success: function ( o )
    {
            jChangeContent( "ToolMenu", o.responseText );
    }
}
// start AJAX
function retrieveReason(type,record_id)
{
    //FormObject.testing.value = 1;
    obj = document.form1;
    var myElement = document.getElementById("ToolMenu");
    
    showMenu("ToolMenu","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
    YAHOO.util.Connect.setForm(obj);

    // page for processing and feedback
    var path = "getReason.php?RecordType=" + type + "&RecordID=" + record_id;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
*/

-->
</SCRIPT>

<?
$lu = new libuser($StudentID);
$toolbar = "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
$toolbar .= "<tr valign=\"top\">";
$toolbar .= "<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">". $i_UserStudentName ."</span></td>";
$toolbar .= "<td class=\"tabletext\">". $lu->UserNameClassNumber() ."</td>";
$toolbar .= "</tr>";
$toolbar .= "</table>";

$reason = $lword->getWordListAttendance($RecordType);
echo $linterface->CONVERT_TO_JS_ARRAY($reason, "jArrayWords", 1);

$sql = "SELECT 
				DATE_FORMAT(AttendanceDate,'%Y-%m-%d'), RecordType, DayType, Reason 
		FROM 
		
				PROFILE_STUDENT_ATTENDANCE 
		WHERE 
				StudentAttendanceID = '$RecordID'";
				
$result = $lattend->returnArray($sql,4);

$table_content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>
			        <tr>
			          <td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>
			          <td class='tablegreentop tabletopnolink' >$i_Attendance_Type</td>
			          <td class='tablegreentop tabletopnolink' >$i_Attendance_DayType</td>
			          <td class='tablegreentop tabletopnolink' >$i_Attendance_Reason</td>
			        </tr>\n";

for($i=0; $i<sizeof($result); $i++)
{
	$select_default_reason = $linterface->GET_PRESET_LIST("jArrayWords", $i, "reason_".$RecordID);
	
	list ($attendDate,$type,$dayType,$reason) = $result[$i]; 
	switch ($type)
	{
	   case 1: $typeStr = $i_Profile_Absent; break;
	   case 2: $typeStr = $i_Profile_Late; break;
	   case 3: $typeStr = $i_Profile_EarlyLeave; break;
	   default: $typeStr = "Error"; break;
	}
	$dayStr = $i_DayTypeArray[$dayType];
	$table_content .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
	<td class='tablegreenrow tabletext'>$attendDate</td>
	<td class='tablegreenrow tabletext'>$typeStr</td>
	<td class='tablegreenrow tabletext'>$dayStr</td>
	<td class='tablegreenrow tabletext'><input id='reason_$RecordID' name='reason_$RecordID' type='text' value='$reason' class='textboxnum'> $select_default_reason</td>
	<input name='record_id[$i]' type='hidden' value='$RecordID'>
	</tr>";
}

$table_content .= "</table>";
?>

<form name="form1" action="view_attendance_update.php" method="POST">
<br />
<?=$toolbar?>

<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?=$table_content?></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
</table>

<? /* ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td height=10px></td></tr>
<tr><td height=20px><hr size=1></td></tr>
<tr><td align=right><?=btnSubmit();?><?=btnReset();?></td></tr>
</table>
<? */ ?>

<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
		<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
		<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1);","cancelbtn") ?>
		</td>
</tr>
</table>    

<input type="hidden" name="type" value="<?=$type?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="Year" value="<?=$Year?>">
<input type="hidden" name="start_date" value="<?=$start_date?>">
<input type="hidden" name="end_date" value="<?=$end_date?>">
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>