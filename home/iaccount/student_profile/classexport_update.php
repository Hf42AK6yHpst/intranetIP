<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lstudentprofile = new libstudentprofile();
$CurrentPage	= "PageAttendanceRecord";

$lexport = new libexporttext();

$li = new libfilesystem();

$lclass = new libclass();
$lattend = new libattendance();
$lword = new libwordtemplates();
$reasons = $lword->getWordListAttendance();

$hasRight = true;

$lu_viewer = new libuser($UserID);
$lattend = new libattendance();

if ($lu_viewer->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    if(empty($class))
    {
    	$hasRight = false;
	}
	else
	{
		# class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid'",$classid, 0, 1);

	    # re-new classname
	    $ClassName = $lclass->getClassName($classid);
	}
}
else
{
	$hasRight = false;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$class_name = $lattend->getClassName($classid);
$now = time();

$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));
if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}
$result = $lattend->getAttendanceListByClass($class_name,$start,$end,$reason);
intranet_closedb();

$pad_columns = array();
for ($i=0; $i<sizeof($Fields); $i++)
{
	$pad_columns[] = " ";
}

$rows = array();
$exportColumn = array();

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "";
$exportColumn[0] = $school_name;

$r = 0;
$rows[$r++] = array_merge(array(" "," "),$pad_columns);	# empty line

$rows[$r++] = array_merge(array($i_UserClassName.":", $class_name),$pad_columns);
$rows[$r++] = array_merge(array($i_Attendance_Date.":", "$i_Profile_From $start $i_Profile_To $end"),$pad_columns);
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$rows[$r++] = array_merge(array($i_Attendance_Reason.":", $reasonSelect),$pad_columns);

$rows[$r++] = array_merge(array(" "," "),$pad_columns);	# empty line

# prepare sub header
$rows[$r][] = $i_UserClassNumber;
$rows[$r][] = $i_UserEnglishName;
for ($i=0; $i<sizeof($Fields); $i++)
{
	$exportColumn[$i+1] = "";
	
	switch ($Fields[$i]) {
		case "absent": 
			$rows[$r][] = $i_Profile_Absent;
			$f_absent=true; 
			break;
		case "late": 
			$rows[$r][] = $i_Profile_Late;
			 $f_late=true; 
			 break;
		case "earlyleave": 
			$rows[$r][] = $i_Profile_EarlyLeave;
			 $f_earlyleave=true; 
			 break;
	}
}
$exportColumn[$i+1] = "";
$r++;

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$absence,$late,$earlyleave) = $result[$i];
     
     $rows[$r][] = $classnumber=="" ? "":$classnumber;
     $rows[$r][] = $name;
     $rows[$r][] = ($f_absent) ? $absence : "";
     $rows[$r][] = ($f_late) ? $late : "";
	 $rows[$r][] = ($f_earlyleave) ? $earlyleave : "";
	 $r++;
}

$filename = "eclass-user-".session_id()."-".time().".csv";
$export_content .= $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>
