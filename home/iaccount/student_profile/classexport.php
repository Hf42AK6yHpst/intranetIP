<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");

intranet_auth();
intranet_opendb();
$lattend = new libattendance();

$linterface 	= new interface_html();
$lstudentprofile = new libstudentprofile();
$LibUser = new libuser($UserID);
$CurrentPage	= "PageAttendanceRecord";
$lprofile = new libprofile();
$lclass = new libclass();

$hasRight = true;

$lu_viewer = new libuser($UserID);
$lattend = new libattendance();

if ($lu_viewer->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    if(empty($class))
    {
    	$hasRight = false;
	}
	else
	{
		# class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid'",$classid, 0, 1);

	    # re-new classname
	    $ClassName = $lclass->getClassName($classid);
	}
}
else
{
	$hasRight = false;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$class_name = $lattend->getClassName($classid);

### Title ###
$TAGS_OBJ[] = array($i_Profile_Attendance,"attendance_record.php", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Report_Class,"classview.php", 1);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_export.($intranet_session_language=="en"?" ":"").$i_Discipline_System_Report_AwardPunish_ClassRecord);

?>

<script language="javascript">
function checkform(obj){
     if(countOption(obj.elements["Fields[]"])==0){ alert(globalAlertMsg18); return false; }
}
</script>

<br />
<form name="form1" action="classexport_update.php" method="post" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_ClassName?></span></td>
					<td class="tabletext"><?=$class_name?></td>
				</tr>
                                
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Attendance_Date?></span></td>
					<td class="tabletext"><?=$i_Profile_From?> <?=$start?> <?=$i_Profile_To?> <?=$end?></td>
				</tr>

				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Attendance_Reason?></span></td>
					<td class="tabletext"><?= ($reason!="") ? $reason : $i_status_all; ?></td>
				</tr>
				
				
				
				
				
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_export_msg2?></span></td>
					<td class="tabletext">
                                        <select name=Fields[] size=5 multiple>
                			<option value=absent SELECTED><?= $i_Profile_Absent ?></option>
                			<option value=late SELECTED><?= $i_Profile_Late ?></option>
                			<option value=earlyleave SELECTED><?= $i_Profile_EarlyLeave ?></option>
                			<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option>
                			</select>
                                        </td>
				</tr>
                                 <tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                                	<td colspan="2" align="center">
                                        <?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2");?>
                                        <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancel_btn");?>
                                        </td>
                		</tr>
                                
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
</table>

<input type="hidden" name="start" value="<?= $start ?>">
<input type="hidden" name="end" value="<?= $end ?>">
<input type="hidden" name="reason" value="<?= $reason ?>">
<input type="hidden" name="classid" value="<?= $classid ?>">

</form>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
