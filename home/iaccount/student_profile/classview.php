<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lstudentprofile = new libstudentprofile();
$CurrentPage	= "PageAttendanceRecord";
$lprofile = new libprofile();
$lclass = new libclass();

$hasRight = true;

$lu_viewer = new libuser($UserID);
$lattend = new libattendance();

if ($lu_viewer->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    if(empty($class))
    {
    	$hasRight = false;
	}
	else
	{
		# class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid'",$classid, 0, 1);

	    # re-new classname
	    $ClassName = $lclass->getClassName($classid);
	}
}
else
{
	$hasRight = false;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lattend = new libattendance();
$lword = new libwordtemplates();
$reasons = $lword->getWordListAttendance();
$classid = $lattend->getClassID($ClassName);

$now = time();

$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

$start = ($start == "") ? $today : $start;
$end = ($end == "") ? $today : $end;

$result = $lattend->getAttendanceListByClass($ClassName,$start,$end,$reason);

$x = "<TABLE cellSpacing=\"0\" cellPadding=\"4\" width=\"100%\" border=\"0\">
<tr class='tablebluetop tabletopnolink'>
<td width='10%' class='tablebluetop tabletopnolink'>$i_UserClassNumber</td>
<td width='30%' class='tablebluetop tabletopnolink'>$i_UserStudentName</td>
<td width='20%' class='tablebluetop tabletopnolink'>$i_Profile_Absent</td>
<td width='20%' class='tablebluetop tabletopnolink'>$i_Profile_Late</td>
<td width='20%' class='tablebluetop tabletopnolink'>$i_Profile_EarlyLeave</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$absence,$late,$earlyleave) = $result[$i];
     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
     $display = $name;
     $link = "<a class='tablebluelink' href=\"JavaScript:viewAttendByDate($id,'$start','$end')\">$display</a>";
     $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>
             <td class='tabletext'>$classnumber</td>
             <td class='tabletext'>$link</td>
             <td class='tabletext'>$absence</td>
             <td class='tabletext'>$late</td>
             <td class='tabletext'>$earlyleave</td>
            </tr>\n";
}
if (sizeof($result) == 0)
{
        $x .= "<tr><td colspan='5' class='tabletext' align='center'>".$no_msg."</td></tr>\n";
}
$x .= "</table>\n";

$datetype += 0;
$selected[$datetype] = "SELECTED";

$classid = $lattend->getClassID($ClassName);

$daySelect = "<SELECT name='datetype' onChange=\"changeDateType(this.form)\">\n";
$daySelect .= "<OPTION value='0' ".$selected[0].">$i_Profile_Today</OPTION>\n";
$daySelect .= "<OPTION value='1' ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$daySelect .= "<OPTION value='2' ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$daySelect .= "<OPTION value='3' ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$daySelect .= "</SELECT>\n";

$reasonSelect	= getSelectByValue($reasons,"name='reason'",$reason,1);
$export_btn 	= "<a href=javascript:checkGet(document.form1,'classexport.php') class='contenttool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_export.gif' width='18' height='18' border='0' align='absmiddle'> $button_export </a>";
$print_btn	= "<a href=javascript:openPrintPage() class='contenttool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_print.gif' width='18' height='18' border='0' align='absmiddle'> $button_print </a>";
                       
### Title ###
$TAGS_OBJ[] = array($i_Profile_Attendance,"attendance_record.php", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Report_Class,"classview.php", 1);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
}
function openPrintPage()
{
        newWindow("classprint.php?classid=<?=$classid?>&start=<?=$start?>&end=<?=$end?>&reason=<?=$reason?>",4);
}
function viewAttendByDate(id,start,end)
{
         //newWindow("/home/profile/school/view_attendance.php?StudentID=" + id + "&start_date=" + start + "&end_date=" + end, 1);
         newWindow("view_attendance.php?StudentID=" + id + "&start_date=" + start + "&end_date=" + end, 1);
}
</SCRIPT>


<br />   
<form name="form1" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_ClassName?></span></td>
					<td class="tabletext"><?=$class_selection?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Profile_SelectSemester?></span></td>
					<td class="tabletext">
                                        <?=$daySelect?><br />
                                        <?=$i_Profile_From?> <input type='text' name='start' value='<?=$start?>' class='textboxnum'> <?=$linterface->GET_CALENDAR("form1","start");?> 
                                        <?=$i_Profile_To?> <input type='text' name='end' value='<?=$end?>' class='textboxnum'>  <?=$linterface->GET_CALENDAR("form1","end");?>
                                        <span class='tabletextremark'>(yyyy-mm-dd) </span>
                                        </td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Profile_SelectReason?></span></td>
					<td class="tabletext"><?=$reasonSelect?></td>
				</tr>
                                <tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                                	<td colspan="2" align="center">
                                        <?=$linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");?>
                                        </td>
                		</tr>
                                
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
</table>

<input type="hidden" name="type" value="<?=$type?>">
<? /*
<input type="hidden" name="ClassName" value="<?=$ClassName?>">
<input type="hidden" name="classid" value="<?=$classid?>">
*/ ?>
<input type="hidden" name="year" value="<?=$year?>">
     
</form>

<!--- list //--->
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        	<td align="right">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
        			<td align="right">
                                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        				<tr>
        					<td align="left"><?= $linterface->GET_NAVIGATION2($i_Discipline_System_Report_AwardPunish_ClassRecord) ?></td>
					</tr>
        				</table>
				</td>
        		</tr>
                        <tr>
                        	<td align="left" class="tabletextremark">
                                	<table border="0" cellspacing="0" cellpadding="2">
                                        <tr>
                                                <td><?=$export_btn?></td>
                                                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"></td>
                                                <td><?=$print_btn?></td>
                                                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"></td>
                                        </tr>
                                        </table>
				</td>
        		</tr>
        		</table>
		</td>
        </tr>
        <tr>
        	<td><?=$x?></td>
        </tr>                                      
        <tr> 
		<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
	</tr>
        </table>

<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
