<?php
#using: 

/***************************************
 * 
 * Date:	2019-07-24 [Henry]
 * Details: fixed php 5.4 issue
 *
 ***************************************/
 
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
### preserve table view
if ($ck_login_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_login_record_page_number", $pageNo, 0, "", "", 0);
	$ck_login_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_login_record_page_number!="")
{
	$pageNo = $ck_login_record_page_number;
}

if ($ck_circular_login_record_order!=$order && $order!="")
{
	setcookie("ck_circular_admin_page_order", $order, 0, "", "", 0);
	$ck_login_record_page_order = $order;
} else if (!isset($order) && $ck_login_record_page_order!="")
{
	$order = $ck_login_record_page_order;
}

if ($ck_login_record_page_field!=$field && $field!="")
{
	setcookie("ck_login_record_page_field", $field, 0, "", "", 0);
	$ck_login_record_page_field = $field;
} else if (!isset($field) && $ck_login_record_page_field!="")
{
	$field = $ck_login_record_page_field;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);

# Date conds
$date_field = "StartTime";
switch ($range)
{
    case 0: // TODAY
         $date_conds = "AND TO_DAYS(now()) = TO_DAYS($date_field)";
         break;
    case 1: // LAST WEEK
         $date_conds = "AND UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=604800";
         break;
    case 2: // LAST 2 WEEKS
         $date_conds = "AND UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=1209600";
         break;
    case 3: // LAST MONTH
         $date_conds = "AND UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=2678400";
         break;
    case 4: // ALL
         $date_conds = "";
         break;
    default:
         $date_conds = "AND TO_DAYS(now()) = TO_DAYS($field)";
         break;
}

$sql  = "SELECT
               StartTime, DateModified, UNIX_TIMESTAMP(DateModified) - UNIX_TIMESTAMP(StartTime) as duration
         FROM INTRANET_LOGIN_SESSION
         WHERE UserID = '$UserID' $date_conds
         ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array = array("StartTime","DateModified", "duration");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,9);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tablebluetop tabletopnolink'>#</td>\n"; 
$li->column_list .= "<td width='30%' class='tablebluetop'>".$li->column($pos++, $i_UsageStartTime)."</td>\n";
$li->column_list .= "<td width='40%' class='tablebluetop'>".$li->column($pos++, $i_UsageEndTime)."</td>\n";
$li->column_list .= "<td width='30%' class='tablebluetop'>".$li->column($pos++, $i_UsageDuration)."</td>\n";

// TABLE FUNCTION BAR
## select time period for filtering
$array_search_name = array($i_UsageToday, $i_UsageLastWeek, $i_UsageLast2Week, $i_UsageLastMonth, $AllRecords);
$array_search_data = array("0", "1", "2", "3", "4");
$searchbar = getSelectByValueDiffName($array_search_data,$array_search_name,"name='range' onChange='this.form.pageNo.value=1;this.form.submit();'",$range,0,1);

$CurrentPage = "PageLoginRecord";
$linterface = new interface_html();
$lprofile = new libprofile();

### Title ###
$TAGS_OBJ[] = array($i_MyAccount_Usage);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $searchbar; ?></td>
	</tr>
</table>
<?php echo $li->display("96%", "blue"); ?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>