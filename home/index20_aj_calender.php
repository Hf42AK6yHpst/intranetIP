<?php
# using: 

################ Change Log [Start] #####################
#
#	Date	:	2012-12-06 [YatWoon]
#				Add flag checking $special_feature['HideShowAllEventIcon'] [Case#2012-1206-1051-42071]
#
#	Date:	2010-10-14	[Ronald]
#			modified HTML code part. Before modification, the hyperlink for AllEvent is broken. 
#
#	Date:	2010-05-05	[YatWoon]
#			Add "School holiday" in small calendar
#
################ Change Log [End] #####################

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycle.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lc = new libcycle();
$lcycleperiods = new libcycleperiods();
$li = new libcalevent2007($ts,$v);

$CurDate = date("Y.m.d (D)");
$CurCycleDate = $lcycleperiods->getCycleDayStringByDate(date("Y-m-d"));			

$AllEventText = $i_status_all.(($intranet_session_language=="en") ? " ": "" ).$i_Events;

$x = "
		<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		<tr>
			<td>						
			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
			<tr>
				<td class=\"indexcalendartoday\" align=\"left\">{$CurDate}</td>
				<td align=\"right\" class=\"indexcalendarstoday\">{$CurCycleDate}</td>
			</tr>
			</table>
			<span id=\"CalContentDiv\"  >
			".$li->displayCalendar()."
			</span>							
			</td>
		</tr>
		<tr>
			<td align=\"center\" valign=\"bottom\" height=\"100%\" >
			<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
			<tr>
				<td width=\"12\"><p>".$li->displayPrevMonth()."</p></td>
				<td align=\"center\" class=\"indexcalendarmonth\">".$li->displayMonthText()."</td>
				<td width=\"12\"><p>".$li->displayNextMonth()."</p></td>
			</tr>
			</table>
			</td>
		</tr>		
		<tr>
			<td align=\"center\" valign=\"bottom\">
			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
			<tr>
				<td align=\"center\" valign=\"middle\">
					<a href=\"javascript:fe_view_event_by_type(3)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_holiday_off.gif\" alt=\"$i_EventTypeString[2]\" title=\"$i_EventTypeString[2]\" name=\"ca3\" width=\"25\" height=\"15\" border=\"0\" id=\"ca3\" onMouseOver=\"MM_swapImage('ca3','','{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_holiday_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>
				</td>
				<td align=\"center\" valign=\"middle\">
					<a href=\"javascript:fe_view_event_by_type(4)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_school_holiday_off.gif\" alt=\"$i_EventTypeString[4]\" title=\"$i_EventTypeString[4]\" name=\"ca4\" width=\"25\" height=\"15\" border=\"0\" id=\"ca4\" onMouseOver=\"MM_swapImage('ca4','','{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_school_holiday_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>
				</td>
				<td align=\"center\" valign=\"middle\">
					<a href=\"javascript:fe_view_event_by_type(0)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_schoolevent_off.gif\" alt=\"$i_EventTypeString[0]\" title=\"$i_EventTypeString[0]\" name=\"ca2\" width=\"25\" height=\"15\" border=\"0\" id=\"ca2\" onMouseOver=\"MM_swapImage('ca2','','{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_schoolevent_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>
				</td>
				<td align=\"center\" valign=\"middle\">
					<a href=\"javascript:fe_view_event_by_type(1)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_academicevent_off.gif\" alt=\"$i_EventTypeString[1]\" title=\"$i_EventTypeString[1]\" name=\"ca1\" width=\"25\" height=\"15\" border=\"0\" id=\"ca1\" onMouseOver=\"MM_swapImage('ca1','','{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_academicevent_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>
				</td>
				<td align=\"center\" valign=\"middle\">
					<a href=\"javascript:fe_view_event_by_type(2)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_groupevent_off.gif\" alt=\"$i_EventTypeString[3]\" title=\"$i_EventTypeString[3]\" name=\"ca5\" width=\"25\" height=\"15\" border=\"0\" id=\"ca5\" onMouseOver=\"MM_swapImage('ca5','','{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_groupevent_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>
				</td>
				";
				
			if(!$special_feature['HideShowAllEventIcon']) 
				{
				$x .= "<td align=\"center\" valign=\"middle\">";
$AllEventText = $i_status_all.(($intranet_session_language=="en") ? " ": "" ).$i_Events;
$x .= " 
					<a href=\"javascript:showAllEventList()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_allevent_off.gif\" alt=\"$AllEventText\" title=\"$AllEventText\" name=\"ca6\" width=\"25\" height=\"15\" border=\"0\" id=\"ca6\" onMouseOver=\"MM_swapImage('ca6','','{$image_path}/{$LAYOUT_SKIN}/index/calendar_2/btn_allevent_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>
				</td>";
			}
			$x .= "</tr>
			</table>											
			</td>
		</tr>
		</table>
		";		
$benchmark['after calendar'] = time();		
		
echo $x;	

intranet_closedb();
	
?>

