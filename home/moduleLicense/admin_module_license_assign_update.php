<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

# IES Module License access right checking
//libIntranetModule::checkModuleLicenceAccess();

intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("module_script.php");
###########################################################
//$objInstall	= new elibrary_install();

$libIntranetModule = new libIntranetModule();

##Get Post Variables
$aryStudentName 	= isset($_REQUEST['aryStudentName'])? $_REQUEST['aryStudentName'] : "";
$aryStudentID 	= isset($_REQUEST['aryStudentID'])? $_REQUEST['aryStudentID'] : "";
$ModuleLicenseID 	= isset($_REQUEST['ModuleLicenseID'])? trim($_REQUEST['ModuleLicenseID']) : "";
$ModuleID 		= isset($_REQUEST['ModuleID'])? trim($_REQUEST['ModuleID']) : "";

# IES Module License access right checking
libIntranetModule::checkModuleLicenceAccessByModuleId($ModuleID);


$arySucc = array();
$aryFail = array();


## Loop Through list of students
foreach($aryStudentID as $i=>$StudentID){
	if(!empty($StudentID)){
		$ModuleInfo = $libIntranetModule->add_module_student($ModuleID,$StudentID);
		$ModuleCode = $ModuleInfo["CODE"];
		$ModuleDescription = $ModuleInfo["DESCRIPTION"];
		if($ModuleInfo){
			//$arySucc[$i] = $aryStudentName[$i];
			$arySucc[$i] = array($Lang['ModuleLicense']['ModuleCode']=>$ModuleCode,$Lang['ModuleLicense']['Description']=>$ModuleDescription,$Lang['ModuleLicense']['LicensedStudents']=>$aryStudentName[$i]);
		}else{										
			//$aryFail[$i] = $aryStudentName[$i];
			$aryFail[$i] = array($Lang['ModuleLicense']['ModuleCode']=>$ModuleCode,$Lang['ModuleLicense']['Description']=>$ModuleDescription,$Lang['ModuleLicense']['LicensedStudents']=>$aryStudentName[$i]);
		}		
	}
}
$succ_fail_header	= $eLib["html"]["student_name"];
		 
## Echo succ & fail results				 
//echo $objInstall->gen_result_ui($succ_fail_header, $arySucc, $aryFail);
echo $libIntranetModule->gen_result_ui($arySucc, $aryFail, true, "828");




###########################################################
intranet_closedb();
?>