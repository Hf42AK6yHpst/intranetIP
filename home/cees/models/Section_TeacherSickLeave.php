<?php
include_once 'Section.php';

class Section_TeacherSickLeave extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_STAFF_SICK_LEAVE';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('StaffName', 'DateNo', 'WithCertificate');
    var $jsonToDbMapping = array(
        'teacherName' => 'StaffName',
        'thisMonthDay' => 'DateNo',
        'WithCer' => 'WithCertificate'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $SickLeaveResult = $this->db->returnResultSet($sql);
        $Result = array();
        $i = 0;
        foreach ($SickLeaveResult as $SickLeaveArr) {

            $Result[$i]['teacherName'] = $SickLeaveArr['teacherName'];
            $Result[$i]['thisMonthDay'] = floatval($SickLeaveArr['thisMonthDay']);
            $Result[$i]['WithCer'] = floatval($SickLeaveArr['WithCer']);
            $Result[$i]['lastMonthDay'] = $this->getBeforeAbsentDay($Result[$i]['teacherName']);
            $i++;
        }
        $this->contents = $Result;
    }

    function getBeforeAbsentDay($StaffName)
    {
        if(trim($StaffName)=='') return 0;
        $sql = "SELECT AcademicYearID,Month,Year FROM CEES_SCHOOL_MONTHLY_REPORT WHERE ReportID = '$this->reportId'";
        $thisMonthAry = $this->db->returnResultSet($sql);
        $AcademicYearID = $thisMonthAry[0]['AcademicYearID'];
        $thisMonth = $thisMonthAry[0]['Month'];
        $thisYear = $thisMonthAry[0]['Year'];

        // 2020-03-02 (Philips)
        $hasBracket = strpos($StaffName, '(');
        if($hasBracket){
            $StaffName = substr($StaffName, 0, $hasBracket);
            while(substr($StaffName,-1)==' '){
                $StaffName = substr($StaffName,0, -1);
            }
        }

        // 2020-04-20 (Philips) - Checking of correct name [DM#1369]
        $sql = "SELECT DISTINCT StaffName FROM CEES_SCHOOL_MONTHLY_REPORT_STAFF_SICK_LEAVE WHERE StaffName LIKE '%$StaffName%'";
        $nameMatching = $this->db->returnVector($sql);
        $nameCondAry = array();
        foreach($nameMatching as $recordName){
            $tempName = $recordName;
            $hasBracket = strpos($tempName, '(');
            if($hasBracket){
                $tempName = substr($tempName, 0, $hasBracket);
                while(substr($tempName,-1)==' '){
                    $tempName = substr($tempName,0, -1);
                }
            }
            $cmpResult = strcmp(trim($StaffName), trim($tempName));
            if($cmpResult == 0) $nameCondAry[] = $recordName;
        }
        $nameCond = (!empty($nameCondAry)) ? " AND mrssl.StaffName IN ('" . implode("','", $nameCondAry) . "') " : ' AND 0 ';

        $StartYear = getStartDateOfAcademicYear($AcademicYearID);
        $EndYear = getEndDateOfAcademicYear($AcademicYearID);


        //$start = strtotime(date('Y-m-01', strtotime($StartYear)));
        $start = strtotime(date('Y-09-01', strtotime($StartYear)));
        $end = $loopMonth = strtotime(date('Y-m-t', strtotime($EndYear)));

        $date = array();
        while ($loopMonth >= $start):
            $_year = date("Y", $loopMonth);
            $_month = date("m", $loopMonth);
//            $loopMonth = strtotime("-1 month", $loopMonth);
            $loopMonth = strtotime($_year.'-'.$_month.'-01 -1 month');
            if (($thisYear >= $_year && $thisMonth > $_month) || $thisYear > $_year) {
                //     continue;

                $sql = "SELECT
                      mrssl.DateNo
                    FROM
                      CEES_SCHOOL_MONTHLY_REPORT_STAFF_SICK_LEAVE AS mrssl
                    LEFT JOIN CEES_SCHOOL_MONTHLY_REPORT AS mr ON (mr.ReportID = mrssl.ReportID)
                    WHERE mr.Month = '$_month' AND mr.Year = '$_year' $nameCond
                   ";
                // WHERE mr.Month = '$_month' AND mr.Year = '$_year' AND mrssl.StaffName LIKE '%$StaffName%'
                $DateNo = $this->db->returnVector($sql);
                $date[] = $DateNo[0];
            }
        endwhile;

        $sum = 0;
        for ($i = 0; $i < sizeof($date); $i++) {
            $sum += $date[$i];
        }
        $sum = $sum == '0' ? '--' : $sum;
        return $sum;
    }


}