<?php
include_once 'Section.php';

class Section_TeacherCourse extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_STAFF_COURSE_ATTEND';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('CourseDateFrom','StaffIDs', 'CourseName', 'Organization');
    var $jsonToDbMapping = array(
        'date' => 'CourseDateFrom',
        'teacherIds' => 'StaffIDs',
        'course' => 'CourseName',
        'organization' => 'Organization'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function save()
    {
        if (isset($this->contents)) {
            $contents = $this->contents;

            for($i=0;$i<count($contents);$i++){
                $dateList = $contents[$i]['date'];
                $dateListAry = explode(',',$dateList);
                usort($dateListAry, array($this,"dateList_sort"));
                $contents[$i]['date'] = $dateListAry;
            }

            usort($contents, array( $this, 'dateSub_sort'));
            foreach($contents as $key=>$InfoAry){
                $contents[$key]['date'] = implode(',',$InfoAry['date']);
            }

            if (!empty($contents)) {
                $dataArr = array();
                foreach ($contents as $_key => $_row) {
                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        if ($__jsonName == 'teacherIds') {
                            $dataArr[$_key][] = implode(',', $_row[$__jsonName]);
                        } else {
                            $dataArr[$_key][] = $_row[$__jsonName];
                        }
                    }
                    $dataArr[$_key][] = $this->reportId;
                }
                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);
            }
        }
    }

    public function read()
    {
        $this->fetch();
        $contents = $this->contents;
        foreach ($contents as &$_row) {
            if ($_row['date'] == '0000-00-00') {
                $_row['date'] = '';
            }
            $_row['teacherIds'] = explode(',', $_row['teacherIds']);
        }

        return $contents;
    }
}