<?php
include_once 'Section.php';

class Section_SchoolAwards extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_AWARDS_INFO';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('Award');
    var $jsonToDbMapping = array(
        'text' => 'Award'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }
}