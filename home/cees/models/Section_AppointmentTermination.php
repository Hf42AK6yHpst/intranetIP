<?php
include_once 'Section.php';

class Section_AppointmentTermination extends Section
{
    var $db;
    var $table = 'CEES_SCHOOL_MONTHLY_REPORT_APPOINTMENT_INFO';
    var $reportId;
    var $contents = array();
//'Particulars',
    var $dbFieldArr = array('Particulars','StaffName', 'Position', 'ContractType', 'EffectiveDate','Reason');
    var $jsonToDbMapping = array(
        'particularId' => 'Particulars',
        'teachername' => 'StaffName',
        'positionId' => 'Position',
        'typeId' => 'ContractType',
        'date' => 'EffectiveDate',
        'reasonId' => 'Reason'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();

        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sqlI = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}' AND Particulars = '1'";
        $this->contents['appointment'] = $this->db->returnResultSet($sqlI);

        $sqlE = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}' AND Particulars = '2'";
        $this->contents['termination'] = $this->db->returnResultSet($sqlE);
    }

    # 2020-10-12 (Philips) Keep order of records if record date is the same
    public function date_sort($a, $b){
        $timeDiff = strtotime($a['date']) - strtotime($b['date']);
        if($timeDiff == 0){
            return $a['index'] - $b['index'];
        } else {
            return $timeDiff;
        }
    }

    public function save()
    {
        if (isset($this->contents)) {
            $appointmentContents = $this->contents['appointment'];
            foreach($appointmentContents as $index => &$ac){
                $ac['index'] = $index;
            }
            usort($appointmentContents, array( $this, 'date_sort'));

            if (!empty($appointmentContents)) {
                $dataArr = array();
                foreach ($appointmentContents as $_key => $_row) {
                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        $dataArr[$_key][] = $_row[$__jsonName];
                    }
                    $dataArr[$_key][] = $this->reportId;
                }
                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);
            }

            $terminationContents = $this->contents['termination'];
            foreach($terminationContents as $index => &$tc){
                $tc['index'] = $index;
            }
            usort($terminationContents, array( $this, 'date_sort'));
            if (!empty($terminationContents)) {
                $dataArr = array();
                foreach ($terminationContents as $_key => $_row) {
                    foreach ($this->jsonToDbMapping as $__jsonName => $__dbName) {
                        $dataArr[$_key][] = $_row[$__jsonName];
                    }
                    $dataArr[$_key][] = $this->reportId;
                }
                $this->insertData($this->table, $this->getSaveFieldArr(), $dataArr);

            }

        }
    }

}