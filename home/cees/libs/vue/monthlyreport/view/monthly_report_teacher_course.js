Vue.component('teacher-course', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 28%">\
    <col style="width: 24%">\
    <col style="width: 24%">\
    <col style="width: 17%">\
    </colgroup>\
    <thead>\
    <tr>\
    <th style="text-align: left;">#</th>\
    <th style="text-align: left;">'+jsLang.Date+'</th>\
    <th style="text-align: left;">'+jsLang.StaffName+'</th>\
    <th style="text-align: left;">'+jsLang.CourseName+'</th>\
    <th style="text-align: left;">'+jsLang.OrganizedBy+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
                {{content.date}}\
            </td>\
            <td style="text-align: left;">\
                    {{content.teacherIds | getTeacherName(teachers) }}  \
            </td>\
            <td style="text-align: left;">\
                {{content.course}}\
            </td>\
            <td style="text-align: left;">\
                {{content.organization}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.TeacherCourse.slice(),
            teachers: this.$root.teachers
        }
    },
    filters: {
        getTeacherName: function (teacherids, teachers) {
            var teachername = [];
            _.forEach(teacherids, function (teacherid, key) {
                teachername.push(teachers[teacherid]);
            });

            return teachername.join(",");
        }
    }
});