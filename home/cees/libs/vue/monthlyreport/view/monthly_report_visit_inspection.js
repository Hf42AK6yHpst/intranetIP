Vue.component('school-visit', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 19%">\
    <col style="width: 74%">\
    </colgroup>\
    <thead>\
    <tr>\
    <th style="text-align: left;">#</th>\
    <th style="text-align: left;">'+jsLang.Date+'</th>\
    <th style="text-align: left;">'+jsLang.Purpose+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
                 {{content.date}}\
            </td>\
            <td style="text-align: left;">\
                {{content.purpose}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.SchoolVisit.slice()
        }
    }
});