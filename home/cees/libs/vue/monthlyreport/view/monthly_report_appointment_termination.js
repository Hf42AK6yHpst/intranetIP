Vue.component('appointment-termination', {
    template: '\
    <div>\
    <table class="table report-style tbl_extraActivities" id="tbl_other" width="100%">\
     <colgroup>\
        <col style="width: 5%">\
        <col style="width: 30%">\
        <col style="width: 25%">\
        <col style="width: 20%">\
        <col style="width: 20%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th class="subTitle" colspan="5"  style="text-align: left;">(A) '+jsLang.Appointment+'</th>\
    </tr>\
    <tr>\
        <th>#</th>\
        <th style="text-align: left;">' + jsLang.Name + '</th>\
        <th style="text-align: left;">' + jsLang.Position + '</th>\
        <th style="text-align: left;">' + jsLang.ContractType + '</th>\
        <th style="text-align: left;">' + jsLang.EffectiveDate + '</th>\
    </tr>\
    </trstyle>\
    </thead>\
    <tbody>\
            <tr v-for="(content, key) in contents.appointment" >\
                <td style="text-align: left;">{{ key + 1 }}</td>\
                <td style="text-align: left;">\
                     {{content.teachername}}\
                </td>\
                <td style="text-align: left;">\
                     {{ content.positionId | getPositionName(positions)}}\
                </td>\
                <td style="text-align: left;">\
                    {{ content.typeId | getContractTypes(contracttypes)}}\
                </td>\
                <td style="text-align: left;">\
                    {{content.date}}\
                </td>\
            </tr>\
        </tbody>\
    </table>\
     <br/>\
     <table class="table report-style tbl_extraActivities" id="tbl_other" width="100%">\
         <colgroup>\
           <col style="width: 5%">\
            <col style="width: 27%">\
            <col style="width: 23%">\
            <col style="width: 15%">\
            <col style="width: 15%">\
            <col style="width: 17%">\
        </colgroup>\
        <thead>\
        <tr>\
            <th class="subTitle" colspan="6" style="text-align: left;">(B) '+jsLang.ServiceTermination+'</th>\
        </tr>\
        <tr>\
            <th >#</th>\
            <th style="text-align: left;">' + jsLang.Name + '</th>\
            <th style="text-align: left;">' + jsLang.Position + '</th>\
            <th style="text-align: left;">' + jsLang.ContractType + '</th>\
            <th style="text-align: left;">' + jsLang.Reason + '</th>\
            <th style="text-align: left;">' + jsLang.EffectiveDate + '</th>\
        </th>\
        </tr>\
        </thead>\
        <tbody>\
            <tr v-for="(content, key) in contents.termination" >\
                <td style="text-align: left;">{{ key + 1 }}</td>\
                <td style="text-align: left;">\
                     {{content.teachername}}\
                </td>\
                <td style="text-align: left;">\
                     {{ content.positionId | getPositionName(positions)}}\
                </td>\
                <td style="text-align: left;">\
                    {{ content.typeId | getContractTypes(contracttypes)}}\
                </td>\
                <td style="text-align: left;">\
                    {{ content.reasonId | getReasons(terminationreasons)}}\
                </td>\
                <td style="text-align: left;">\
                    {{content.date}}\
                </td>\
            </tr>\
        </tbody>\
    </table>\
    </div>\
    ',
    data: function () {
        return {
            // contents: formData.AppointmentTermination.slice(),
            contents: {
                appointment: formData.AppointmentTermination.appointment.slice(),
                termination: formData.AppointmentTermination.termination.slice()
            },
            // particulars: this.$root.particulars,
            positions: this.$root.positions,
            contracttypes: this.$root.contracttypes,
            terminationreasons: this.$root.terminationreasons,
            teachers: this.$root.teachers
        }
    },
    filters: {
        getParticularName: function (value, particulars) {
            return particulars[value];
        },
        // getTeacherName: function (teacherid, teachers) {
        //
        //     return teachers[teacherid];
        // },
        getPositionName: function (positionid, positions) {
            return positions[positionid];
        },
        getContractTypes: function (typeid, contracttypes) {
            return contracttypes[typeid];
        },
        getReasons: function (reasonId, terminationreasons) {
            return terminationreasons[reasonId];
        }
    }
});