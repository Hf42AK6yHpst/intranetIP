Vue.component('student-eca', {
    template: '\
    <div>\
    	<table class="table report-style tbl_extraActivities" id="tbl_extraActivitiesA" width="100%">\
            <colgroup>\
                <col style="width: 3%">\
                <col style="width: 19%">\
                <col style="width: 20%">\
                <col style="width: 40%">\
                <col style="width: 18%">\
                </colgroup>\
        <thead>\
            <tr>\
                <th class="subTitle" colspan="5" style="text-align: left;"> (A) '+jsLang.Internal+'</th>\
            </tr>\
            <tr>\
                <th style="text-align: left;">#</th>\
                <th style="text-align: left;">'+jsLang.Date+'</th>\
                <th style="text-align: left;">'+jsLang.ActivityCategory+'</th>\
                <th style="text-align: left;">'+jsLang.ActivityName+'</th>\
                <th style="text-align: left;">'+jsLang.participantsNo+'</th>\
             </tr>\
         </thead>\
           <tbody>\
            <tr v-for="(content, key) in contents.internal" >\
                <td style="text-align: left;">{{ key + 1 }}</td>\
                <td style="text-align: left;">\
                    {{content.date}}\
                </td>\
                <td style="text-align: left;">\
                    {{content.categoryID | getCategoryName(ecacategories)}}\
                </td>\
                <td style="text-align: left;">\
                    {{content.activityName}}\
                </td>\
                <td style="text-align: left;">\
                     {{content.participantsNo}}\
                </td>\
              </tr>\
             </tbody>\
        </table>\
        <br>\
        <table class="table report-style tbl_extraActivities coutinue" id="tbl_extraActivitiesB" width="100%">\
            <colgroup>\
            <col style="width: 3%">\
            <col style="width: 19%">\
            <col style="width: 20%">\
            <col style="width: 40%">\
            <col style="width: 18%">\
            </colgroup>\
            <thead>\
                <tr>\
                    <th class="subTitle" colspan="5" style="text-align: left;"> (B) '+jsLang.External+'</th>\
                </tr> \
                   <tr>\
                    <th style="text-align: left;">#</th>\
                    <th style="text-align: left;">'+jsLang.Date+'</th>\
                    <th style="text-align: left;">'+jsLang.ActivityCategory+'</th>\
                    <th style="text-align: left;">'+jsLang.ActivityName+'</th>\
                    <th style="text-align: left;">'+jsLang.participantsNo +'</th>\
                   </tr>\
            </thead>\
            <tbody>\
                <tr v-for="(content, key) in contents.external" >\
                    <td style="text-align: left;">{{ key + 1 }}</td>\
                    <td style="text-align: left;">\
                        {{content.date}}\
                    </td>\
                    <td style="text-align: left;">\
                        {{content.categoryID | getCategoryName(ecacategories)}}\
                    </td>\
                    <td style="text-align: left;">\
                        {{content.activityName}}\
                    </td>\
                    <td style="text-align: left;">\
                        {{content.participantsNo}}\
                    </td>\
                </tr>\
            </tbody>\
            </table>\
    	</div>\
    ',
    data: function () {
        return {
            contents: {
                internal: formData.StudentEca.internal.slice(),
                external: formData.StudentEca.external.slice()
            },
            ecacategories: this.$root.categories
        }
    },
    filters: {
        getCategoryName: function (categoryID, ecacategories) {
            var categoryname = [];
            _.forEach(categoryID, function (categoryIDs, key) {
                categoryname.push(ecacategories[categoryIDs]);
            });
            return categoryname.join(",");
        }
    }
});