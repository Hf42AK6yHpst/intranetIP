Vue.component('student_enrolment', {
    template: '\
    <div>\
        <div class="import-menu">\
            <ul>\
                <li  v-if="'+StudentEnrol+' == 1" ><a href="#" data-toggle="modal" data-target="#stdEnrolmentModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromStudentAttendance+'</a></li>\n\
                <li><a href="#" data-toggle="modal" data-target="#studentEnrollModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\n\
            </ul>\
        </div>\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
	    <col style="width: 10%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
    	<col style="width: 15%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th>' + jsLang.Level + '</th>\
        <th>' + jsLang.Approved + '</th>\
        <th>' + jsLang.Operation + '</th>\
        <th>' + jsLang.Places + '</th>\
        <th>' + jsLang.SeptemberNo + '</th>\
        <th>' + jsLang.thisMonthNo + '</th>\
        <th>' + jsLang.AverageRate + '</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>\
    			{{content.level}}\
            </td>\
            <td>\
    			 <input type="text" class="form-control " id="\'inputApprovedNo\' + key" v-model="content.approvedNo">\
            </td>\
            <td>\
    			 <input type="text" class="form-control " id="\'inputOperationNo\' + key" v-model="content.operationNo">\
            </td>\
	        <td>\
    			<input type="text" class="form-control " id="\'inputPlaceApprovedNo\' + key" v-model="content.placeApprovedNo">\
	    	</td>\
            <td>\
    			{{content.lastMonthEnrolled}}\
    		</td>\
	    	<td>\
    			<input type="text" class="form-control" id="\'inputNoOfEnrolled\' +key" v-model="content.thisMonthEnrolled">\
            </td>\
		    <td>\
    	 		<input type="text" class="form-control" id="\'inputAverageAttendanceRate\'+key" v-model="content.averageAttendance">\
		    </td>\
        </tr>\
    </tbody>\
    </table>\
    </div>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.StudentEnrolment.slice(),
            row: {
                level: '',
                approvedNo: '',
                operationNo: '',
                placeApprovedNo: '',
                lastMonthEnrolled: '',
                thisMonthEnrolled: '',
                averageAttendance: ''
            }
        }
    },
    computed: {
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },

    methods: {
        assign: function () {
            submitForm.StudentEnrolment = this.contents;
        },
        confirmData: function (){
            var dbData;
            var self = this;
            var importdata = [];
            var importdata1 = 0;
            var importdata2 = 0;
            var importdata3 = 0;
            var importdata4 = 0;
            var importdata5 = 0;
            var importdata6 = 0;
            $.ajax({
                url: "/home/cees/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStudentAttendanceMonthlyData',
                    TargetDate: this.year+'-'+this.month+'-01',
                },
                success: function (data) {
                    dbData = data;

                    _.forEach(dbData, function (AttendanceData, key) {
                        if((AttendanceData.Level == 'S1' || AttendanceData.Level == 'P1') && importdata1==0){
                            self.contents[0].level =  AttendanceData.Level;
                            self.contents[0].operationNo =  ''+AttendanceData.ClassesInOperation;
                            // self.contents[0].lastMonthEnrolled =  ''+AttendanceData.EnrolledInSeptember;
                            self.contents[0].thisMonthEnrolled =  ''+AttendanceData.EnrolledThisMonth;
                            self.contents[0].averageAttendance =  ''+AttendanceData.AttendanceRate;
                            importdata1 = 1;
                        }

                        if((AttendanceData.Level == 'S2' || AttendanceData.Level == 'P2') && importdata2 ==0){
                            self.contents[1].level =  AttendanceData.Level;
                            self.contents[1].operationNo =  ''+AttendanceData.ClassesInOperation;
                            // self.contents[1].lastMonthEnrolled =  ''+AttendanceData.EnrolledInSeptember;
                            self.contents[1].thisMonthEnrolled =  ''+AttendanceData.EnrolledThisMonth;
                            self.contents[1].averageAttendance =  ''+AttendanceData.AttendanceRate;
                            importdata2 = 1;
                        }

                        if((AttendanceData.Level == 'S3' || AttendanceData.Level == 'P3') && importdata3 ==0){
                            self.contents[2].level =  AttendanceData.Level;
                            self.contents[2].operationNo =  ''+AttendanceData.ClassesInOperation;
                            // self.contents[2].lastMonthEnrolled =  ''+AttendanceData.EnrolledInSeptember;
                            self.contents[2].thisMonthEnrolled =  ''+AttendanceData.EnrolledThisMonth;
                            self.contents[2].averageAttendance =  ''+AttendanceData.AttendanceRate;
                            importdata3 = 1;
                        }

                        if((AttendanceData.Level == 'S4' || AttendanceData.Level == 'P4') && importdata4==0){
                            self.contents[3].level =  AttendanceData.Level;
                            self.contents[3].operationNo =  ''+AttendanceData.ClassesInOperation;
                            // self.contents[3].lastMonthEnrolled =  ''+AttendanceData.EnrolledInSeptember;
                            self.contents[3].thisMonthEnrolled =  ''+AttendanceData.EnrolledThisMonth;
                            self.contents[3].averageAttendance =  ''+AttendanceData.AttendanceRate;
                            importdata4 = 1;
                        }

                        if((AttendanceData.Level == 'S5' || AttendanceData.Level == 'P5') && importdata5==0){
                            self.contents[4].level =  AttendanceData.Level;
                            self.contents[4].operationNo =  ''+AttendanceData.ClassesInOperation;
                            // self.contents[4].lastMonthEnrolled =  ''+AttendanceData.EnrolledInSeptember;
                            self.contents[4].thisMonthEnrolled =  ''+AttendanceData.EnrolledThisMonth;
                            self.contents[4].averageAttendance =  ''+AttendanceData.AttendanceRate;
                            importdata5 = 1;
                        }

                        if((AttendanceData.Level == 'S6' || AttendanceData.Level == 'P6') && importdata6==0){
                            self.contents[5].level =  AttendanceData.Level;
                            self.contents[5].operationNo =  ''+AttendanceData.ClassesInOperation;
                            // self.contents[5].lastMonthEnrolled =  ''+AttendanceData.EnrolledInSeptember;
                            self.contents[5].thisMonthEnrolled =  ''+AttendanceData.EnrolledThisMonth;
                            self.contents[5].averageAttendance =  ''+AttendanceData.AttendanceRate;
                            importdata6 = 1;
                        }
                    });
                }
            });
        },
        confirmData2: function(){
            var self = this;
            this.confirmDataNow(this, 'studentEnroll', {
                special: 'enroll',
                before: function(data){
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    data[1] = (data[1] != "") ? data[1] : "Error";
                    data[2] = (data[2] != "") ? data[2] : "Error";
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    data[4] = (data[4] != "") ? data[4] : "Error";
                    data[5] = (data[5] != "") ? data[5] : "Error";
                    return data;
                },
                addRow: function(data, index){
                    let x = index;
                    if (self.contents[x].level == data[0]) {
                        self.contents[x].approvedNo = data[1];
                        self.contents[x].operationNo = data[2];
                        self.contents[x].placeApprovedNo = data[3];
                        //self.contents[x].lastMonthEnrolled = "";
                        self.contents[x].thisMonthEnrolled = data[4];
                        self.contents[x].averageAttendance = data[5];
                    }
                }
            });
        },
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-stdEnrolmentModal', this.confirmData);

        eventHub.$on('confirm-info-studentEnrollModal', this.confirmData2);
    }
});


Vue.component('stdEnrolmentModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul><li>'+jsLang.Month+'/'+jsLang.Year+': {{month}}/{{year}}</li></ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 22%;">\
                    <col style="width: 26%;">\
                    <col style="width: 26%;">\
                    <col style="width: 26%;">\
                </colgroup>\
                <thead>\
                    <th>'+jsLang.Level+'</th>\
                    <th>'+jsLang.Operation+'</th>\
                    <th>'+jsLang.thisMonthNo+'</th>\
                    <th>'+jsLang.AverageRate+'</th>\
                 </thead>\
                    <tbody>\
                       <tr v-for="(content, key) in stdEnrolmentModalContents" >\
                            <td>{{content.level}}</td>\
                            <td>{{content.operationNo}}</td>\
                            <td>{{content.thisMonthEnrolled}}</td>\
                            <td>{{content.averageAttendance}}</td>\
                       </tr>\
                   </tbody>\
                </table>\
                <p class="question text-center">'+jsLang.EnsureImport+'</p>\
             </div>\
          </div>\
            ',
    data: function () {

        return {
            stdEnrolmentModalContents:
                [{
                    level: '',
                    operationNo: '',
                    // lastMonthEnrolled: '',
                    thisMonthEnrolled: '',
                    averageAttendance:''
                }]
        }
    },

    created: function () {


    },
    method: {
        confirm: function (){
            console.log(this.stdEnrolmentModalContents);
        }
    },
    computed: {
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;
        var importdata = [];
        $.ajax({
            url: "/home/cees/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStudentAttendanceMonthlyData',
                TargetDate: this.year+'-'+this.month+'-01',
              //  Month:
            },
            success: function (data) {
                dbData = data;
                var importdata1 = 0;
                var importdata2 = 0;
                var importdata3 = 0;
                var importdata4 = 0;
                var importdata5 = 0;
                var importdata6 = 0;

                if(dbData[0].Level.substring(0,1) == 'S'){
                    var levelName = 'S';
                }else{
                    var levelName = 'P';
                }
                _.forEach(dbData, function (AttendanceData, key) {

                    if((AttendanceData.Level == 'S1' || AttendanceData.Level == 'P1') && importdata1==0){
                        importdata1 = {
                            level: AttendanceData.Level,
                            operationNo:AttendanceData.ClassesInOperation ,
                            // lastMonthEnrolled: AttendanceData.EnrolledInSeptember,
                            thisMonthEnrolled: AttendanceData.EnrolledThisMonth,
                            averageAttendance:AttendanceData.AttendanceRate
                        };
                    }

                    if((AttendanceData.Level == 'S2' || AttendanceData.Level == 'P2') && importdata2 ==0){
                        importdata2 = {
                            level: AttendanceData.Level,
                            operationNo:AttendanceData.ClassesInOperation ,
                            // lastMonthEnrolled: AttendanceData.EnrolledInSeptember,
                            thisMonthEnrolled: AttendanceData.EnrolledThisMonth,
                            averageAttendance:AttendanceData.AttendanceRate
                        };
                    }

                    if((AttendanceData.Level == 'S3' || AttendanceData.Level == 'P3') && importdata3 ==0){
                        importdata3 = {
                            level: AttendanceData.Level,
                            operationNo:AttendanceData.ClassesInOperation ,
                            // lastMonthEnrolled: AttendanceData.EnrolledInSeptember,
                            thisMonthEnrolled: AttendanceData.EnrolledThisMonth,
                            averageAttendance:AttendanceData.AttendanceRate
                        };
                     }

                    if((AttendanceData.Level == 'S4' || AttendanceData.Level == 'P4') && importdata4==0){
                        importdata4 = {
                            level: AttendanceData.Level,
                            operationNo:AttendanceData.ClassesInOperation ,
                            // lastMonthEnrolled: AttendanceData.EnrolledInSeptember,
                            thisMonthEnrolled: AttendanceData.EnrolledThisMonth,
                            averageAttendance:AttendanceData.AttendanceRate
                        };
                    }

                    if((AttendanceData.Level == 'S5' || AttendanceData.Level == 'P5') && importdata5==0){
                        importdata5 = {
                            level: AttendanceData.Level,
                            operationNo:AttendanceData.ClassesInOperation ,
                            // lastMonthEnrolled: AttendanceData.EnrolledInSeptember,
                            thisMonthEnrolled: AttendanceData.EnrolledThisMonth,
                            averageAttendance:AttendanceData.AttendanceRate
                        };

                    }

                    if((AttendanceData.Level == 'S6' || AttendanceData.Level == 'P6') && importdata6==0){
                        importdata6 = {
                            level: AttendanceData.Level,
                            operationNo:AttendanceData.ClassesInOperation ,
                            // lastMonthEnrolled: AttendanceData.EnrolledInSeptember,
                            thisMonthEnrolled: AttendanceData.EnrolledThisMonth,
                            averageAttendance:AttendanceData.AttendanceRate
                        };
                    }


                });

                if(importdata1 == '0'){
                    importdata1 = {
                        level: levelName + '1',
                        operationNo:'-' ,
                        thisMonthEnrolled:'-' ,
                        averageAttendance:'-'
                    }
                }

                if(importdata2 == '0'){
                    importdata2 = {
                        level: levelName + '2',
                        operationNo:'-' ,
                        thisMonthEnrolled:'-' ,
                        averageAttendance:'-'
                    }
                }

                if(importdata3 == '0'){
                    importdata3 = {
                        level: levelName + '3',
                        operationNo:'-' ,
                        thisMonthEnrolled:'-' ,
                        averageAttendance:'-'
                    }
                }

                if(importdata4 == 0){
                    importdata4 = {
                        level: levelName + '4',
                        operationNo:'-' ,
                        thisMonthEnrolled:'-' ,
                        averageAttendance:'-'
                    }
                }

                if(importdata5 == 0){
                    importdata5 = {
                        level: levelName + '5',
                        operationNo:'-' ,
                        thisMonthEnrolled:'-' ,
                        averageAttendance:'-'
                    };
                }
                if(importdata6 == 0){
                    importdata6 = {
                        level: levelName + '6',
                        operationNo:'-' ,
                        thisMonthEnrolled:'-' ,
                        averageAttendance:'-'
                    }
                }
                importdata.push(importdata1,importdata2,importdata3,importdata4,importdata5,importdata6);
            }
        })
            .done(function() {
               self.stdEnrolmentModalContents = importdata;
            });
    }

});

Vue.component('studentEnrollModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}}  : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Level,
                jsLang.Approved,
                jsLang.Operation,
                jsLang.Places,
                jsLang.thisMonthNo,
                jsLang.AverageRate
            ],
            popup:[
                null,
                null,
                null,
                null,
                null,
                null
            ],
            reminder:[
                null,
                null,
                null,
                null,
                null,
                null
            ],
            filetemp: "/home/cees/monthlyreport/template/studentEnroll",
            fileFieldId: "studentEnrollFile"
        }
    },
    mounted: function(){
    }
});