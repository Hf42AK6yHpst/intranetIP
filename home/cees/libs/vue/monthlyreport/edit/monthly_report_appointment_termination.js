Vue.component('appointment-termination', {
    template: '\
    <div>\
    <table class="table report-style tbl_extraActivities" id="tbl_other">\
    <colgroup>\
	    <col style="width: 5%">\
	    <col style="width: 25%">\
	    <col style="width: 25%">\
	    <col style="width: 20%">\
	    <col style="width: 20%">\
	    <col style="width: 5%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th class="subTitle" colspan="6">(A) '+jsLang.Appointment+'</th>\
    </tr>\
    <tr>\
        <th colspan="6">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#appointmentModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
        <th>#</th>\
        <th>' + jsLang.Name + '</th>\
        <th>' + jsLang.Position + '</th>\
        <th>' + jsLang.ContractType + '</th>\
        <th>' + jsLang.EffectiveDate + '</th>\
        <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents.appointment" >\
            <td>{{ key + 1 }}</td>\
            <td>\
                <input type="text" class="form-control" id="\'inputTeachername\'+ key" v-model="content.teachername" v-bind:type="\'appointment_termination\'">\
            </td>\
            <td>\
    	  		<select v-model="content.positionId" :id="\'_positionselect_\' + key" class="selectpicker" data-live-search="true">\
    	  		<option disabled>'+jsLang.Select+'</option>\
                     <option v-for="(position,id) in positions" :value="id">{{ position }}</option>\
                </select>\
            </td>\
            <td>\
    	  		<select v-model="content.typeId" :id="\'_typeselect_\' + key" class="selectpicker" >\
    	  		<option disabled>'+jsLang.Select+'</option>\
                    <option v-for="(type,id) in contracttypes" :value="id">{{ type }}</option>\
                </select>\
            </td>\
	    	<td>\
    			<div is="multi-date-input" v-bind:id="key" v-bind:type="\'appointment_termination_a\'" v-bind:TypeKey="\'appointment\'">\
	        	</div>\
            <td>\
            	<div is="del-btn" v-bind:remove="removea" v-bind:target="key" v-bind:location="contents.appointment">\
            	</div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="adda"  v-bind:model="\'appointment\'"></tr>\
    </tbody>\
    </table>\
    <table class="table report-style tbl_extraActivities" id="tbl_other">\
    <colgroup>\
    <col style="width: 5%">\
    <col style="width: 23%">\
    <col style="width: 23%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 17%">\
    <col style="width: 4%">\
    </colgroup>\
     <thead>\
      <tr>\
        <th class="subTitle" colspan="7">(B) '+jsLang.ServiceTermination+'</th>\
      </tr>\
      <tr>\
        <th colspan="7">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#terminationModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
       </tr>\
        <tr >\
            <th>#</th>\
            <th>' + jsLang.Name + '</th>\
            <th>' + jsLang.Position + '</th>\
            <th>' + jsLang.ContractType + '</th>\
            <th>' + jsLang.Reason + '</th>\
            <th>' + jsLang.EffectiveDate + '</th>\
            <th>&nbsp;</th>\
        </tr>\
      </thead>\
      <tbody>\
      <tr v-for="(content, key) in contents.termination" >\
          <td>{{ key + 1 }}</td>\
          <td>\
              <input type="text" class="form-control" id="\'inputTeachername\'+ key" v-model="content.teachername" v-bind:type="\'appointment_termination\'">\
          </td>\
          <td>\
  	  		<select v-model="content.positionId" :id="\'_positionselect_\' + key" class="selectpicker" data-live-search="true">\
  	  		<option disabled>'+jsLang.Select+'</option>\
                   <option v-for="(position,id) in positions" :value="id">{{ position }}</option>\
              </select>\
          </td>\
          <td>\
  	  		<select v-model="content.typeId" :id="\'_typeselect_\' + key" class="selectpicker" >\
  	  		    <option disabled>'+jsLang.Select+'</option>\
                <option v-for="(type,id) in contracttypes" :value="id">{{ type }}</option>\
              </select>\
          </td>\
          <td>\
              <select v-model="content.reasonId" :id="\'reasonselect_\' + key" class="selectpicker" >\
                  <option disabled>'+jsLang.Select+'</option>\
                  <option v-for="(reason,id) in terminationreasons" :value="id" >{{ reason }}</option>\
              </select>\
    	  </td>\
	      <td>\
  			<div is="multi-date-input" v-bind:id="key" v-bind:type="\'appointment_termination_t\'" v-bind:TypeKey="\'termination\'">\
	         </div>\
	      </td>\
          <td>\
          	<div is="del-btn" v-bind:remove="removet" v-bind:target="key" v-bind:location="contents.termination">\
          	</div>\
          </td>\
      </tr>\
        <tr is="add-row" v-bind:add="addt" v-bind:model="\'termination\'"></tr>\
        </tbody>\
    </table>\
    </div>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
       //     contents: formData.AppointmentTermination.slice(),
            contents: {
                appointment: formData.AppointmentTermination.appointment.slice(),
                termination: formData.AppointmentTermination.termination.slice()
            },
   //         particulars: this.$root.particulars,
            positions: this.$root.positions,
            contracttypes: this.$root.contracttypes,
            terminationreasons: this.$root.terminationreasons,
            // row: {
  //              particularId: '',
  //               teachername: '',
  //               positionId: '',
  //               typeId: '',
  //               date: ''
  //           },
            row: {
                appointment: {
                    teachername: '',
                    positionId: '',
                    particularId: '1',
                    typeId: '',
                    date: ''
                },
                termination: {
                    teachername: '',
                    positionId: '',
                    particularId:'2',
                    typeId: '',
                    reasonId:'',
                    date: ''
                }
            }
        }
    },
    methods: {
        // add: function (e) {
        //     this.addNewRows(e.target.value, this.row, this.contents);
        //     this.$nextTick(function () {
        //         $('.selectpicker').selectpicker();
        //     });
        //
        // },
        adda: function (e) {
            // this.addNewRows(e.target.value, this.row.appointment, this.contents.appointment);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        addt: function (e) {
            // this.addNewRows(e.target.value, this.row.termination, this.contents.termination);
            this.$nextTick(function () {
                $('.selectpicker').selectpicker();
            });
        },
        removea: function (key) {
            this.removeRow(key, this.contents.appointment)
        },
        removet: function (key) {
            this.removeRow(key, this.contents.termination)
        },
        assign: function () {
            submitForm.AppointmentTermination = this.contents;
        },
        confirmData: function(){
            var self = this;
            this.confirmDataNow(this, 'appointment', {
                contents: self.contents.appointment,
                before: function(data){
                    data[1] = jsLang.PositionName[data[1]];
                    if(data[1] == null)
                        data[1] = "Error";
                    data[2] = self.$root.contracttypes[data[2]];
                    if(data[2] == null)
                        data[2] = "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        teachername: data[0],
                        positionId: data[1],
                        particularId: '1',
                        typeId: data[2],
                        date: data[3]
                    };
                }
            });
            /*var self = this;
            var uploadfile = $('#appointmentFile')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\n");
                for( var x in allResults) {
                    if(allResults[x]!="")
                        resultAry.push(allResults[x].split(','));
                }
                //File reading
                $.ajax({
                    url: "/home/cees/monthlyreport/ajaxImport/appointment",
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData['appointmentTempData'] = result;
                        Vue.component('appointmentConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData['appointmentTempData'].result));
                                for(var x in data.data){
                                    data.data[x][1] = jsLang.PositionName[data.data[x][1]];
                                    if(data.data[x][1] == null)
                                        data.data[x][1] = "Error";
                                    data.data[x][2] = self.$root.contracttypes[data.data[x][2]];
                                    if(data.data[x][2] == null)
                                        data.data[x][2] = "Error";
                                }
                                var here = this;
                                here.error = window.prop.tempData['appointmentTempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                eventHub.$once('confirm-data-appointmentModal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData['appointmentTempData'].result;
                                        for(var x in data.data){
                                            newArr.push({
                                                teachername: data.data[x][0],
                                                positionId: data.data[x][1],
                                                particularId: '1',
                                                typeId: data.data[x][2],
                                                date: data.data[x][3]
                                            });
                                        }
                                        self.addRows(newArr, self.contents.appointment);
                                        this.$nextTick(function () {
                                            $('.selectpicker').selectpicker();
                                        });
                                        eventHub.$emit('close-appointmentModal');
                                        window.prop.tempData['appointmentTempData'] = null;
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-appointmentModal');
                    }
                });
            }
            fr.readAsText(uploadfile);*/

        },
        confirmData2: function(){
            var self = this;
            this.confirmDataNow(this, 'termination', {
                contents: self.contents.termination,
                before: function(data){
                    data[1] = jsLang.PositionName[data[1]];
                    if(data[1] == null)
                        data[1] = "Error";
                    data[2] = self.$root.contracttypes[data[2]];
                    if(data[2] == null)
                        data[2] = "Error";
                    data[3] = self.$root.terminationreasons[data[3]];
                    if(data[3] == null)
                        data[3] == "Error";
                    return data;
                },
                addRow: function(data){
                    return {
                        teachername: data[0],
                        positionId: data[1],
                        particularId: '2',
                        typeId: data[2],
                        reasonId: data[3],
                        date: data[4]
                    };
                }
            });
            /*var self = this;
            var uploadfile = $('#terminationFile')[0].files[0];
            //File reading
            var fr = new FileReader();
            var resultAry = [];
            fr.onload = () => {
                var allResults = fr.result.split("\n");
                for( var x in allResults) {
                    if(allResults[x]!="")
                        resultAry.push(allResults[x].split(','));
                }
                //File reading
                $.ajax({
                    url: "/home/cees/monthlyreport/ajaxImport/termination",
                    method: "POST",
                    type: "POST",
                    data: {"content" : resultAry},
                    success: function(data){
                        var result = JSON.parse(data);
                        console.log(result);
                        if(!window.prop){
                            window.prop = {};
                            window.prop.tempData = [];
                        }
                        window.prop.tempData['terminationTempData'] = result;
                        Vue.component('terminationConfirmTable', {
                            template: self.$root.confirmTableTemplate,
                            data: function(){
                                return{
                                    TotalError : jsLang.TotalError,
                                    Remark : jsLang.Remark,
                                    result : {},
                                    error : 0,
                                    errorLang: self.$root.errorLang
                                };
                            },
                            mounted: function(){
                                var data = JSON.parse(JSON.stringify(window.prop.tempData['terminationTempData'].result));
                                for(var x in data.data){
                                    data.data[x][1] = jsLang.PositionName[data.data[x][1]];
                                    if(data.data[x][1] == null)
                                        data.data[x][1] = "Error";
                                    data.data[x][2] = self.$root.contracttypes[data.data[x][2]];
                                    if(data.data[x][2] == null)
                                        data.data[x][2] = "Error";
                                    data.data[x][3] = self.$root.terminationreasons[data.data[x][3]];
                                    if(data.data[x][3] == null)
                                        data.data[x][3] == "Error";
                                }
                                var here = this;
                                here.error = window.prop.tempData['terminationTempData'].error;
                                here.result = data;
                                if(here.error != 0){
                                    $('button.btn-prev').show();
                                    $('button.btn-confirm').hide();
                                }
                                eventHub.$once('confirm-data-terminationModal', function(){
                                    if(here.error == 0){
                                        //eventHub.$emit('add-data-appointmentModal');
                                        var newArr = [];
                                        var data = window.prop.tempData['terminationTempData'].result;
                                        for(var x in data.data){
                                            newArr.push({
                                                teachername: data.data[x][0],
                                                positionId: data.data[x][1],
                                                particularId: '1',
                                                typeId: data.data[x][2],
                                                reasonId: data.data[x][3],
                                                date: data.data[x][4]
                                            });
                                        }
                                        self.addRows(newArr, self.contents.termination);
                                        this.$nextTick(function () {
                                            $('.selectpicker').selectpicker();
                                        });
                                        eventHub.$emit('close-terminationModal');
                                        window.prop.tempData['terminationTempData'] = null;
                                    }
                                });
                            },
                        });
                        eventHub.$emit('confirm-table-terminationModal');
                    }
                });
            }
            fr.readAsText(uploadfile);*/

        }
    },

    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-appointmentModal', this.confirmData);
        eventHub.$on('confirm-info-terminationModal', this.confirmData2);
    }
});
Vue.component('appointmentModal', {
   template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder" \
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
       return {
           UploadFile : jsLang.UploadFile,
           TemplateFile : jsLang.TemplateFile,
           columns:[
               jsLang.Name,
               jsLang.Position,
               jsLang.ContractType ,
               jsLang.EffectiveDate
           ],
           popup:[
               null,
               { title : jsLang.Position, content: this.$root.positions},
               { title : jsLang.ContractType, content: this.$root.contracttypes},
               null
           ],
           reminder:[
               null,
               null,
               null,
               "YYYY-MM-DD or DD/MM/YYYY"
           ],
           filetemp: "/home/cees/monthlyreport/template/appointment",
           fileFieldId: "appointmentFile"
       }
    }
});

Vue.component('terminationModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Name,
                jsLang.Position,
                jsLang.ContractType,
                jsLang.Reason,
                jsLang.EffectiveDate
            ],
            popup:[
                null,
                { title : jsLang.Position, content: this.$root.positions},
                { title : jsLang.ContractType, content: this.$root.contracttypes},
                { title : jsLang.Reason, content: this.$root.terminationreasons},
                null
            ],
            reminder:[
                null,
                null,
                null,
                null,
                "YYYY-MM-DD or DD/MM/YYYY"
            ],
            filetemp: "/home/cees/monthlyreport/template/termination",
            fileFieldId: "terminationFile"
        }
    }
});