// JavaScript Document

/* ----- Table I Appointment and Termination of Service Start -----*/
var i = 3;
function appointmentAddRow() {
    var table = document.getElementById("tbl_appointment");
    var row = table.insertRow(i);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
	var cell7 = row.insertCell(6);
    cell1.innerHTML = i;
	cell2.innerHTML = "<select class=\"selectpicker\" title=\"Please select\"><option>Appointment</option><option>Termination of Service</option></select>";
	cell3.innerHTML = "<select class=\"selectpicker\" title=\"Please select\"><option>Chan Wai Ming Wendy</option><option>Hung Ka Hon Stephen</option></select>";
	cell4.innerHTML = "<select class=\"selectpicker\" title=\"Please select\"><option>GT</option><option>NGT</option></select>";
	cell5.innerHTML = "<select class=\"selectpicker\" title=\"Please select\"><option>Permanent (P)</option><option>Contract (C)</option></select>";
	cell6.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\" /><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
	cell7.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"appointmentDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	i++;
	$('.selectpicker').addClass('dropdown');
	$('.selectpicker').selectpicker('refresh');
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_appointment tr:not(:last-child) td:last-child').addClass('btn-delete');
}

function appointmentAddMultRow() {
   var row = Array.from($(".appointmentRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      appointmentAddRow();
   }
}

function appointmentDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_appointment").rows.length;
    document.getElementById("tbl_appointment").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_appointment");
		table.rows[k].cells[0].innerHTML = k;
		k++;
	}
	i--;
}

/* ----- Table I Appointment and Termination of Service End -----*/

/* ----- Table II Substitute Teacher Information (SLRS) Function Start -----*/
var ii = 3;
function subTeacherInfoAddRow() {
    var table = document.getElementById("tbl_subTeacherInfo");
    var row = table.insertRow(ii);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
	var cell7 = row.insertCell(6);
	var cell8 = row.insertCell(7);
    cell1.innerHTML = ii;
    cell2.innerHTML = "<input type=\"text\" class=\"form-control\" id=\"inputNameSubstituteTeacher"+ii+"\" placeholder=\"\">";
	cell3.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\" /><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
	cell4.innerHTML = "to";
	cell5.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\" /><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
	cell6.innerHTML = "&nbsp;";
	cell7.innerHTML = "<select class=\"selectpicker\" title=\"Please select\"><option>Lee Lai Lai Linda</option><option>Hui Ka Hung Henry</option></select>";
	cell8.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"subTeacherInfoDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	ii++;
	$('.selectpicker').addClass('dropdown');
	$('.selectpicker').selectpicker('refresh');
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_subTeacherInfo td:nth-child(4)').addClass('text-center');
	$('#tbl_subTeacherInfo tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function subTeacherInfoAddMultRow() {
   var row = Array.from($(".subTeacherInfoRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      subTeacherInfoAddRow();
   }
}

function subTeacherInfoDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_subTeacherInfo").rows.length;
	var z = k+1;
    document.getElementById("tbl_subTeacherInfo").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_subTeacherInfo");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputNameSubstituteTeacher"+z).id = "inputNameSubstituteTeacher"+k;
		k++;
		z++;
	}
	ii--;
}

/* ----- Table II Substitute Teacher Information (SLRS) Function End -----*/

/* ----- Table III Sick Leave of Staff Start -----*/
var iii = 4;
function sickLeaveAddRow() {
    var table = document.getElementById("tbl_sickLeave");
    var row = table.insertRow(iii);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
    cell1.innerHTML = iii;
    cell2.innerHTML = "<select class=\"selectpicker selectName\" title=\"Please select\"><option>Ma Yan Yee Amy</option><option>Wong Chi Wun Justin</option><option>Wong Wing Sze Annie</option></select>";
	cell3.innerHTML = "<select class=\"selectpicker selectDays\" title=\"----\"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option><option>16</option><option>17</option><option>18</option><option>19</option><option>20</option><option>21</option><option>22</option><option>23</option><option>24</option><option>25</option><option>26</option><option>27</option><option>28</option><option>29</option><option>30</option><option>31</option></select>";
	cell4.innerHTML = "<div class=\"form-check form-check-inline text-center\"><label class=\"form-check-label\"><input class=\"form-check-input\" type=\"checkbox\" id=\"inlineCheckbox"+iii+"\" value=\"option2\"><span>&nbsp;</span></label></div>";
	cell5.innerHTML = Math.round(Math.random()*10);
	cell6.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"sickLeaveDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	iii++;
	$('.selectpicker').addClass('dropdown');
	$('.selectpicker').selectpicker('refresh');
	$('#tbl_sickLeave tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function sickLeaveAddMultRow() {
   var row = Array.from($(".sickLeaveRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      sickLeaveAddRow();
   }
}

function sickLeaveDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_sickLeave").rows.length;
	var z = k+1;
    document.getElementById("tbl_sickLeave").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_sickLeave");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inlineCheckbox"+z).id = "inlineCheckbox"+k;
		k++;
		z++;
	}
	iii--;
}

/* ----- Table III Sick Leave of Staff End -----*/


/* ----- Table V School Visit and School Inspection Start -----*/
var v = 3;
function schoolVisitAddRow() {
    var table = document.getElementById("tbl_schoolVisit");
    var row = table.insertRow(v);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
    cell1.innerHTML = v;
    cell2.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\"><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
	cell3.innerHTML = "<input type=\"text\" class=\"form-control inputPurpose\" id=\"inputPurpose"+v+"\">";
	cell4.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"schoolVisitDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	v++;
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_schoolVisit tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function schoolVisitAddMultRow() {
   var row = Array.from($(".schoolVisitRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      schoolVisitAddRow();
   }
}

function schoolVisitDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_schoolVisit").rows.length;
	var z = k+1;
    document.getElementById("tbl_schoolVisit").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_schoolVisit");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputPurpose"+z).id = "inputPurpose"+k;
		k++;
		z++;
	}
	v--;
}

/* ----- Table V School Visit and School Inspection End -----*/


/* ----- Table VI School Administration Start -----*/
var vi = 3;
function schoolAdminAddRow() {
    var table = document.getElementById("tbl_schoolAdmin");
    var row = table.insertRow(vi);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
    cell1.innerHTML = vi;
    cell2.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\"><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
	cell3.innerHTML = "<input type=\"text\" class=\"form-control inputParticulars\" id=\"inputParticulars"+vi+"\" placeholder=\"\">";
	cell4.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"schoolAdminDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	vi++;
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_schoolAdmin tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function schoolAdminAddMultRow() {
   var row = Array.from($(".schoolAdminRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      schoolAdminAddRow();
   }
}

function schoolAdminDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_schoolAdmin").rows.length;
	var z = k+1;
    document.getElementById("tbl_schoolAdmin").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_schoolAdmin");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputParticulars"+z).id = "inputParticulars"+k;
		k++;
		z++;
	}
	vi--;
}

/* ----- Table VI School Administration End -----*/


/* ----- Table VII Course / Seminar attended by Staff Start -----*/
var vii = 3;
function courseAddRow() {
    var table = document.getElementById("tbl_course");
    var row = table.insertRow(vii);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
    cell1.innerHTML = vii;
    cell2.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\"><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
	cell3.innerHTML = " <select class=\"selectpicker\" multiple data-live-search=\"true\" data-live-search-placeholder=\"Please select\" data-actions-box=\"true\" title=\"Please select\"><option>Chan Tai Man Tom</option><option>Chan Wai Ming Wendy</option><option>Cheng Pui Pui Pinky</option><option>Cheung Wing Yan Helen</option><option>Hui Ka Hung Henry</option><option>Hung Ka Hon Stephen</option></select>";
	cell4.innerHTML = "<input type=\"text\" class=\"form-control inputCourseName\" id=\"inputCourseName"+vii+"\">";
	cell5.innerHTML = "<input type=\"text\" class=\"form-control inputOrganizer\" id=\"inputOrganizer"+vii+"\">";
	cell6.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"courseDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	vii++;
	$('.selectpicker').addClass('dropdown');
	$('.selectpicker').selectpicker('refresh');
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_course tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function courseAddMultRow() {
   var row = Array.from($(".courseRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      courseAddRow();
   }
}

function courseDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_course").rows.length;
	var z = k+1;
    document.getElementById("tbl_course").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_course");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputCourseName"+z).id = "inputCourseName"+k;
		document.getElementById("inputOrganizer"+z).id = "inputOrganizer"+k;
		k++;
		z++;
	}
	vii--;
}

/* ----- Table VII Course / Seminar attended by Staff End -----*/


/* ----- Table VIII Extra-curricular Activities of Students -----*/
var viiia = 4;
function extraActivitiesAAddRow() {
	var k = viiia -1;
    var table = document.getElementById("tbl_extraActivitiesA");
    var row = table.insertRow(viiia);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
    cell1.innerHTML = k;
    cell2.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\"><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
    cell3.innerHTML = "<input type=\"text\" class=\"form-control inputActivityName\" id=\"inputActivityName_a"+k+"\">";
	cell4.innerHTML = "<input type=\"text\" class=\"form-control inputNoParticipants\" id=\"inputNoParticipants_a"+k+"\">";
	cell5.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"extraActivitiesADeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	viiia++;
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_extraActivitiesA tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function extraActivitiesAAddMultRow() {
   var row = Array.from($(".extraActivitiesARow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      extraActivitiesAAddRow();
   }
}

function extraActivitiesADeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_extraActivitiesA").rows.length;
	var z = k-1;
    document.getElementById("tbl_extraActivitiesA").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_extraActivitiesA");
		table.rows[k].cells[0].innerHTML = z;
		document.getElementById("inputActivityName_a"+k).id = "inputActivityName_a"+z;
		document.getElementById("inputNoParticipants_a"+k).id = "inputNoParticipants_a"+z;
		k++;
		z++;
	}
	viiia--;
}

var viiib = 4;
function extraActivitiesBAddRow() {
	var k = viiib -1;
    var table = document.getElementById("tbl_extraActivitiesB");
    var row = table.insertRow(viiib);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
    cell1.innerHTML = k;
    cell2.innerHTML = "<div class=\"input-group date datetimepicker\"><input type=\"text\" class=\"form-control\"><span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span></div>";
    cell3.innerHTML = "<input type=\"text\" class=\"form-control inputActivityName\" id=\"inputActivityName_b"+k+"\">";
	cell4.innerHTML = "<input type=\"text\" class=\"form-control inputNoParticipants\" id=\"inputNoParticipants_b"+k+"\">";
	cell5.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"extraActivitiesBDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	viiib++;
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_extraActivitiesB tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function extraActivitiesBAddMultRow() {
   var row = Array.from($(".extraActivitiesBRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      extraActivitiesBAddRow();
   }
}

function extraActivitiesBDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_extraActivitiesB").rows.length;
	var z = k-1;
    document.getElementById("tbl_extraActivitiesB").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_extraActivitiesB");
		table.rows[k].cells[0].innerHTML = z;
		document.getElementById("inputActivityName_b"+k).id = "inputActivityName_b"+z;
		document.getElementById("inputNoParticipants_b"+k).id = "inputNoParticipants_b"+z;
		k++;
		z++;
	}
	viiib--;
}
/* ----- Table VIII Course / Seminar attended by Staff End -----*/


/* ----- Table IX School / Student Award(S) -----*/
var ix = 3;
function awardAddRow() {
    var table = document.getElementById("tbl_award");
    var row = table.insertRow(ix);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
    cell1.innerHTML = ix;
    cell2.innerHTML = "<input type=\"text\" class=\"form-control inputAwardDes\" id=\"inputAwardDes"+ix+"\">";
	cell3.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"awardDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	ix++;
	$('#tbl_award tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function awardAddMultRow() {
   var row = Array.from($(".awardRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      awardAddRow();
   }
}

function awardDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_award").rows.length;
	var z = k+1;
    document.getElementById("tbl_award").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_award");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputAwardDes"+z).id = "inputAwardDes"+k;
		k++;
		z++;
	}
	ix--;
}

/* ----- Table IX Course / Seminar attended by Staff End -----*/


/* ----- Table X Advancedd Notice of Ceremony / Important Activity -----*/
var x = 2;
function advancedAddRow() {
    var table = document.getElementById("tbl_advanced");
    var row = table.insertRow(x);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
    cell1.innerHTML = x;
    cell2.innerHTML = "<input type=\"text\" class=\"form-control inputDescription\" id=\"inputDescriptions"+x+"\">";
	cell3.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"advancedDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	x++;
	$('#tbl_advanced tr:not(:last-child) td:last-child').addClass('btn-delete');
}


function advancedAddMultRow() {
   var row = Array.from($(".advancedRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      advancedAddRow();
   }
}

function advancedDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_advanced").rows.length;
	var z = k+1;
    document.getElementById("tbl_advanced").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_advanced");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputDescriptions"+z).id = "inputDescriptions" + k;
		k++;
		z++;
	}
	x--;
}
/* ----- Table X CAdvancedd Notice of Ceremony / Important Activity End -----*/


/* ----- Table XI Others -----*/
var xi = 2;
function otherAddRow() {
    var table = document.getElementById("tbl_other");
    var row = table.insertRow(xi);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
    cell1.innerHTML = xi;
	cell2.innerHTML = "<input type=\"text\" class=\"form-control inputOthersDes\" id=\"inputOthersDes"+xi+"\">";
	cell3.innerHTML = "<a href=\"#\" class=\"btn-delete\" onclick=\"otherDeleteRow(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
	xi++;
	$('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD'});
	$('#tbl_other tr:not(:last-child) td:last-child').addClass('btn-delete');
}

function otherChangeAddMultRow(r) {
	var table = document.getElementById("tbl_other");
	var k = r.parentNode.parentNode.rowIndex;
    table.rows[k].cells[0].innerHTML = "<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>&nbsp;<select class=\"selectpicker noOfRows otherRow\" id=\"otherRow\" title=\"5\" onChange=\"otherAddMultRow()\"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option></select> row(s)";
	$('.selectpicker').addClass('dropdown');
	$('.selectpicker').selectpicker('refresh');
}

function otherAddMultRow() {
   var row = Array.from($(".otherRow").find(':selected')).map(function(item){
      return $(item).text();
   });
   for (var k=0; k<row; k++) {
      otherAddRow();
   }
}

function otherDeleteRow(r) {
	var k = r.parentNode.parentNode.rowIndex;
	var y = document.getElementById("tbl_other").rows.length;
	var z = k+1;
    document.getElementById("tbl_other").deleteRow(k);
	
	while (y-2>k){
		var table = document.getElementById("tbl_other");
		table.rows[k].cells[0].innerHTML = k;
		document.getElementById("inputOthersDes"+z).id = "inputOthersDes "+ k;
		k++;
		z++;
	}
	xi--;
}
/* ----- Table XI Others -----*/