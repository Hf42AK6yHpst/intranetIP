<?php
$reportInfoAssoc = $_PAGE['View']['Data']['ReportsInfo'];
$start = strtotime(date('Y-m-01', strtotime($_PAGE['View']['Parameter']['YearStart'])));
$end = $loopMonth = strtotime(date('Y-m-t', strtotime($_PAGE['View']['Parameter']['YearEnd'])));
?>

<table class="table report-style">
    <colgroup>
        <col style="width: 20%">
        <col style="width: 40%">
        <col style="width: 40%">
    </colgroup>
    <thead>
    <tr>
        <th><?php echo $Lang['CEES']['MonthlyReport']['List']['Report'] ?></th>
        <th><?php echo $Lang['CEES']['MonthlyReport']['List']['LastInput'] ?></th>
        <th><?php echo $Lang['CEES']['MonthlyReport']['List']['LastSubmitted'] ?></th>
    </tr>
    </thead>
    <tbody>
    <?php while ($loopMonth >= $start):
        $_year = date("Y", $loopMonth);
        $_month = date("m", $loopMonth);
        $_monthsql = date("n", $loopMonth);
//        $loopMonth = strtotime("-1 month", $loopMonth);`
//        $loopMonth =  strtotime(date('Y-m-01', strtotime("-1 month", $loopMonth)));
        $loopMonth = strtotime($_year.'-'.$_month.'-01 -1 month');
//        if ($_year > date("Y", time()) || $_month > date("m", time())) {
        if (($_year >= date("Y", time()) && $_month >  date("m", time())) || $_year > date("Y", time()) ) {
            continue;
        }
        $urlStatus = $reportInfoAssoc[$_year][$_monthsql]['DraftStatus'] == '0' ? 'view' : 'edit';

        ?>
        <tr>
            <td><a href="<? echo '/home/cees/monthlyreport/' . $urlStatus . '/' . $_PAGE['View']['Data']['schoolType'] . '/' . $_year . $_month ?>"><? echo $_month . '/' . $_year ?></a></td>
            <td><?php echo(isset($reportInfoAssoc[$_year][$_monthsql]['ModifiedDate']) ? $reportInfoAssoc[$_year][$_monthsql]['ModifiedDate'] : '--') ?></td>
            <td><?php echo(isset($reportInfoAssoc[$_year][$_monthsql]['SubmittedToCeesDate']) ? $reportInfoAssoc[$_year][$_monthsql]['SubmittedToCeesDate'] : '--') ?></td>
        </tr>
    <? endwhile; ?>
    </tbody>
</table>
