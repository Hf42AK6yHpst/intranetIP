<?php
$reportArr = array(
    array('11', '2017', '--', '--'),
    array('10', '2017', '2017-10-03 11:52', '--'),
    array('09', '2017', '2017-09-01 11:00', '2017-09-02 10:30'),
);
$selectedSchoolType = $schoolType? $schoolType : 'N';
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TWGHS Monthly Report</title>
    <script type="text/javascript" src="/home/cees/libs/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/home/cees/libs/js/moment.js"></script>
    <script type="text/javascript" src="/home/cees/libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="/home/cees/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="/home/cees/libs/bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/home/cees/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/css/form-TWGHS.css"/>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <meta name="theme-color" content="#ffffff">
</head>
<script>
    $(document).ready(function () {
        var academicYearSelect = $('select#academicYearId');
        var schoolTypeSelect = $('select#schoolType');

        // init
        ajaxGetTable(academicYearSelect.val(), schoolTypeSelect.val());

        // listeners
        academicYearSelect.change(function () {
            ajaxGetTable($(this).val(), $('select#schoolType').val());
        });
        schoolTypeSelect.change(function () {
            ajaxGetTable($('select#academicYearId').val(), $(this).val());
        });
    });

    var ajaxGetTable = function (academicYearId, schoolType) {
        console.log('herere:' + academicYearId);
        $('div#listTable').html('<img src="/images/2009a/indicator.gif">');
        $.ajax({
            type: "GET",
            url: "/home/cees/monthlyreport/ajaxReportList",
            data: {academicYearId: academicYearId, schoolType: schoolType},
            success: function (data) {
                $('div#listTable').html(data);
            }
        })
    }
</script>
<body class="monthlyReport">
<div id="wrapper">
    <!-- Main-content -->
    <div id="main-body" style="height:calc(100% + 20px);">
        <!-- Module content-->
        <!-- Content with or without tabs -->
        <div class="content-wrapper">
            <div class="content-header">
                <span class="title"><?php echo $Lang['CEES']['MonthlyReport']['ModuleName'] ?></span>
            </div>
            <hr class="titleDivider"/>

            <div id="subMenu">
                <?php echo getSelectByAssoArray($_PAGE['View']['Data']['academicYear'], 'class="selectpicker" id="academicYearId"', Get_Current_Academic_Year_ID(), 0, 1) ?>
                <?php echo getSelectByAssoArray($_PAGE['View']['Data']['schoolType'], 'class="selectpicker" id="schoolType"', $selectedSchoolType, 0, 1) ?>
            </div>
            <div class="col-sm-12" id="listTable">
                <img src="/images/2009a/indicator.gif">
            </div>
        </div>
    </div>
    <!-- /Content with or without tabs -->
</div>
<!-- /Wrapper -->
</body>
</html>