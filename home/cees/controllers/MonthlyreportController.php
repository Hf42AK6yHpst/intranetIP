<?php
include_once("BaseController.php");

class MonthlyreportController extends BaseController
{
    public function __construct()
    {
        global $_PAGE;
        parent::__construct();

        $this->RequestMapping['/monthlyreport'] = array('function' => 'index');

        // regex for 201709
        //$this->RequestMapping['/monthlyreport/edit/(20[0-9][0-9](0[1-9]|1[0-2]))'] = array('function' => 'edit');
        //$this->RequestMapping['/monthlyreport/view/(20[0-9][0-9](0[1-9]|1[0-2]))'] = array('function' => 'view');
        $this->RequestMapping['/monthlyreport/edit/([A-Z])/(20[0-9][0-9](0[1-9]|1[0-2]))'] = array('function' => 'edit');
        $this->RequestMapping['/monthlyreport/view/([A-Z])/(20[0-9][0-9](0[1-9]|1[0-2]))'] = array('function' => 'view');

        $this->RequestMapping['/monthlyreport/update'] = array('function' => 'update');
        $this->RequestMapping['/monthlyreport/exportasDoc'] = array('function' => 'exportasDoc');
        $this->RequestMapping['/monthlyreport/sync'] = array('function' => 'syncToCees');

        $this->RequestMapping['/monthlyreport/ajax'] = array('function' => 'ajax');
        $this->RequestMapping['/monthlyreport/ajaxReportList'] = array('function' => 'ajaxReportList');
        $this->RequestMapping['/monthlyreport/ajaxImport/([\w\d]+)'] = array('function' => 'ajaxImport');
        $this->RequestMapping['/monthlyreport/ajaxGetDisplaySection'] = array('function' => 'ajaxGetDisplaySection');

        $this->RequestMapping['/monthlyreport/template/([\w\d]+)'] = array('function' => 'template');
    }

    public function index()
    {
        global $_PAGE, $sys_custom, $Lang;

        $_PAGE['View']['Data']['schoolType'] = array();
        $_PAGE['View']['Data']['schoolType']['N'] = $Lang['CEES']['MonthlyReport']['JSLang']['NormalSchool'];
        if($sys_custom['SDAS']['CEES']['MonthlyReport']['BoardingSch']) {
            $_PAGE['View']['Data']['schoolType']['B'] = $Lang['CEES']['MonthlyReport']['JSLang']['BoardingSchool'];
        }
        $_PAGE['View']['Data']['academicYear'] = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), 'AcademicYearID', array('YearNameEN'), 1);

        echo $main = $this->getViewContent('/monthlyreport/list.php');
    }

    public function ajaxReportList()
    {
        global $sys_custom;

        // Check if School Type valid
        $schTypeBoardingCheck = !$sys_custom['SDAS']['CEES']['MonthlyReport']['BoardingSch'] || ($sys_custom['SDAS']['CEES']['MonthlyReport']['BoardingSch'] && $_GET['schoolType'] != 'B');
        if($_GET['schoolType'] != 'N' && $schTypeBoardingCheck) {
            echo '';
            exit;
        }

        global $_PAGE;

        $sql = "SELECT Year, Month, SubmitStatus, DraftStatus, ModifiedDate, SubmittedToCeesDate FROM CEES_SCHOOL_MONTHLY_REPORT WHERE SchoolType = '".($_GET['schoolType']=='N'? '' : $_GET['schoolType'])."'";
        $_PAGE['View']['Data']['ReportsInfo'] = BuildMultiKeyAssoc($_PAGE['db']->returnResultSet($sql), array('Year', 'Month'));

        $_PAGE['View']['Parameter']['YearStart'] = getStartDateOfAcademicYear($_GET['academicYearId']);
        $_PAGE['View']['Parameter']['YearEnd'] = getEndDateOfAcademicYear($_GET['academicYearId']);
        $_PAGE['View']['Data']['schoolType'] = $_GET['schoolType'];

        echo $main = $this->getViewContent('/monthlyreport/ajax_list_table.php');
    }

    public function ajaxGetDisplaySection(){
        global $_PAGE;

        $reportID = $_POST['reportId'];
        $year = $_POST['year'];
        $month = $_POST['month'];

        include_once("models/MonthlyReportModel.php");
        $reportModel = new MonthlyReportModel($_PAGE['db'], $year, $month, $reportID);

        $SectionInfoList = $reportModel->getUserAccessSections();


        $SectionAccessRightAry = array();
        $i = 0;
        foreach ($SectionInfoList as $SectionInfoAry) {
            $title = $SectionInfoAry['SectionName'];
            $type = $SectionInfoAry['TemplateName'];
            $SectionAccessRightAry[$i]['title'] = $title;
            $SectionAccessRightAry[$i]['type'] = $type;
            $i++;
        }
//        $SectionAccessRightAry = $SectionInfoList;
        if (function_exists('json_encode')) {

            $SectionAccessRightJson = json_encode($SectionAccessRightAry);
        } else {

            $SectionAccessRightJson = __json_encode($SectionAccessRightAry);

        }

        echo $SectionAccessRightJson;
    }

    public function view($schoolType='', $report = '')
    {
        $this->editandview($schoolType, $report, '1');
    }
    public function edit($schoolType='', $report = '')
    {
        $this->editandview($schoolType, $report, '0');
    }

    public function editandview($schoolType='', $report = '', $view = '0')
    {
        global $sys_custom;

        if ($report == '') {
            die();
        }
        $schTypeBoardingCheck = !$sys_custom['SDAS']['CEES']['MonthlyReport']['BoardingSch'] || ($sys_custom['SDAS']['CEES']['MonthlyReport']['BoardingSch'] && $schoolType != 'B');
        if($schoolType != 'N' && $schTypeBoardingCheck) {
            die();
        }

        global $_PAGE, $intranet_root, $Lang;

        $year = substr($report, 0, 4);
        $month = substr($report, 4);

        include_once("models/MonthlyReportModel.php");
        $reportModel = new MonthlyReportModel($_PAGE['db'], $year, $month, '', $schoolType);

        $reportInfo = $reportModel->getReportInfo();
        $reportData = $reportModel->getReportData();
        $TeacherInfo = $reportModel->getTeacherName();
        $OLECategoryInfo = $reportModel->getOLECategory();

        if($view == '1'){
            $TeacherList = array();
            foreach ($TeacherInfo as $TeacherInfoAry) {
                $id = intval($TeacherInfoAry['id']);
                $name = $TeacherInfoAry['name'];
                if($TeacherInfoAry['Teaching'] == '2'){
                    $name = isEJ()?convert2unicode($name,1):$name;
                }
                $TeacherList[$id] = $name;
            }

            $OLECategory = array();
            foreach ($OLECategoryInfo as $OLECategoryInfoAry) {
                $id = $OLECategoryInfoAry['id'];
                $name = $OLECategoryInfoAry['name'];
                $OLECategory[$id] = $name;
            }
        }else{
//            $TeacherList = $TeacherInfo;
            $OLECategory = $OLECategoryInfo;
            $i = 0;
            $j = 0;
            $k = 0;
            foreach ($TeacherInfo  as $TeacherInfoAry) {
                $id = intval($TeacherInfoAry['id']);
                $name = $TeacherInfoAry['name'];
                $teaching = $TeacherInfoAry['Teaching'];

                if($teaching == '1'){ // teaching staff
                    $TeachingList[$i]['id'] = $id;
                    $TeachingList[$i]['name'] = $name;
                    $i++;
                }else if ($teaching == '0'){
                    $NonTeacherList[$j]['id'] = $id;
                    $NonTeacherList[$j]['name'] = $name;
                    $j++;
                } else if ($teaching == '2'){
//                    debug_pr(convert2unicode($name));
                    $ArchivedTeacherList[$k]['id'] = $id;
                    $ArchivedTeacherList[$k]['name'] = isEJ()?convert2unicode($name,1):$name;
                    $k++;
                }
                else if ($teaching == 'S'){
//                    debug_pr(convert2unicode($name));
                    $SubstituteTeacherList[$k]['id'] = $id;
                    $SubstituteTeacherList[$k]['name'] = isEJ()?convert2unicode($name,1):$name;
                    $k++;
                }
            }

            $TeacherList[1] = $TeachingList;
            $TeacherList[0] = $NonTeacherList;
            $TeacherList[2] = $ArchivedTeacherList;
            $TeacherList[3] = $SubstituteTeacherList;
        }

//        debug_pr($Lang['CEES']['MonthlyReport']['JSLang']['OLECategoryAry']);
//        $OLECategory = isEJ()?$Lang['CEES']['MonthlyReport']['JSLang']['OLECategoryAry']:$OLECategory;

        $LangAry = $Lang['CEES']['MonthlyReport']['JSLang'];

        if (function_exists('json_encode')) {
            $reportDataJson = json_encode($reportData);
            $TeacherListJson = json_encode($TeacherList);
            $OLECategoryJson = json_encode($OLECategory);
            $LangJson = json_encode($LangAry);
        } else {
            $reportDataJson = __json_encode($reportData);
            $TeacherListJson = __json_encode($TeacherList);
            $OLECategoryJson = __json_encode($OLECategory);
            $LangJson = __json_encode($LangAry);
        }

        $_PAGE['View']['Data']['year'] = $year;
        $_PAGE['View']['Data']['month'] = $month;
        $_PAGE['View']['Data']['schoolType'] = $schoolType;
        $_PAGE['View']['Data']['reportInfo'] = $reportInfo;
        $_PAGE['View']['Data']['reportDataJson'] = $reportDataJson;
        $_PAGE['View']['Data']['TeacherListJson'] = $TeacherListJson;
        $_PAGE['View']['Data']['OLECategoryJson'] = $OLECategoryJson;
        $_PAGE['View']['Data']['LangJson'] = $LangJson;

        if ($view == '1') {
            echo $main = $this->getViewContent('/monthlyreport/view.php');
        } else {
            echo $main = $this->getViewContent('/monthlyreport/edit.php');
        }
    }

    public function update()
    {
        global $intranet_root, $_PAGE;

        $postDataArr = $_POST['reportData'];
        $reportID = $_POST['reportId'];
        $reportSchoolType = $_POST['reportSchoolType'];
        $year = $_POST['year'];
        $month = $_POST['month'];

        include_once("models/MonthlyReportModel.php");
        // $reportModel = new MonthlyReportModel($_PAGE['db'], $year, $month, '', $reportSchoolType);
        $reportModel = new MonthlyReportModel($_PAGE['db'], $year, $month, $reportID);
        $DraftStatus = $_POST['DraftStatus'];

        $SupervisorName = $_POST['supervisorName'];
        if ($DraftStatus != '') {
            $reportModel->changeDraftStatus($DraftStatus);
        }

        if ($SupervisorName != '') {
            $reportModel->updateSupervisorName($SupervisorName);
        }

        if (!empty($postDataArr)) {
            $reportModel->setReportData($postDataArr);
            $reportModel->saveReport();
        }
    }

    function exportasDoc(){
        $report_html = $this->view('201901');
        echo $report_html;
    }

    public function syncToCees()
    {

        global $intranet_root, $ceesConfig, $config_school_code, $sys_custom, $_PAGE;
        include_once($intranet_root . "/includes/cees/libcees.php");
        $libcees = new libcees();

        include_once("models/MonthlyReportModel.php");
        $Year = $_POST['year'];
        $Month = str_pad($_POST['month'], 2, '0', STR_PAD_LEFT);
        $reportId = $_POST['reportId'];
        $SubmitStatus = $_POST['SubmitStatus'];

        $reportModel = new MonthlyReportModel($_PAGE['db'], $Year, $Month, $reportId);
        $reportInfo = $reportModel->getReportInfo();
        $sectionsData = $reportModel->getReportData();


        if ($SubmitStatus != '') {
            $reportModel->changeSubmitStatus($SubmitStatus);
        }

        unset($reportInfo['ReportID']);
        unset($reportInfo['ReportKey']);
        unset($reportInfo['AcademicYearID']);
        unset($reportInfo['SubmitStatus']);
        unset($reportInfo['DraftStatus']);
        unset($reportInfo['InuptBy']);
        unset($reportInfo['InputDate']);
        unset($reportInfo['ModifiedBy']);
        unset($reportInfo['ModifiedDate']);
        $data = array();
        $data['reportInfo'] = $reportInfo;
        $data['sectionsData'] = $sectionsData;

        $result = $libcees->syncMonthlyReportToCentral($Year . $Month, $data);

    }

    public function ajax(){
        global $_PAGE;
        include_once("models/Section_TeacherSickLeave.php");
        include_once("models/MonthlyReportModel.php");

        $reportId = $_POST['reportId'];

//        $reportModel = new MonthlyReportModel($_PAGE['db'], '2017', '11', $reportId);
   //     $SectionSickLeaveModel = $reportModel->Section_TeacherSickLeave();
        $SickLeaveSection = new Section_TeacherSickLeave($_PAGE['db'],$reportId);
        switch ($_POST['Method'] ){
            case 'getSickLeaveData':
                echo $SickLeaveSection->getBeforeAbsentDay($_POST['TeacherName']);
                break;
            case 'getLastMonthSickLeaveDate':
                echo $SickLeaveSection->getBeforeAbsentDay($_POST['TeacherName']);
                break;
        }
    }

    public function ajaxImport($report = ''){
        global $_PAGE, $intranet_root;
        include_once($intranet_root."/includes/libfilesystem.php");
        include_once($intranet_root."/includes/libimporttext.php");
        $data = $_POST['content'];
        $dataResult = "";
        $error = 0;
        switch($report){
            Case 'appointment':
                $dataResult = $this->validcheckAppointment($data);
                break;
            Case 'termination':
                $dataResult = $this->validcheckTermination($data);
                break;
            Case 'substituteTeacher':
                $dataResult = $this->validcheckSubstituteTeacher($data);
                break;
            Case 'sickLeaveCSV':
                $dataResult = $this->validcheckSickLeave($data);
                break;
            Case 'studentEnroll':
                $dataResult = $this->validcheckStudentEnroll($data);
                break;
            Case 'visitInspection':
                $dataResult = $this->validcheckVisitInspection($data);
                break;
            Case 'schoolAdministration':
                $dataResult = $this->validcheckSchoolAdministration($data);
                break;
            Case 'teacherCourse':
                $dataResult = $this->validcheckTeacherCourse($data);
                break;
            Case 'studentECA':
                $dataResult = $this->validcheckStudentECA($data);
                break;
            Case 'schoolAward':
                $dataResult = $this->validcheckSchoolAward($data);
                break;
            Case 'advancedNotice':
                $dataResult = $this->validcheckAdvancedNotice($data);
                break;
            Case 'othersReport':
                $dataResult = $this->validcheckOthersReport($data);
                break;
            default:
                $error = 1;
                break;
        }
        foreach($dataResult['checkResult'] as $row){
            foreach($row as $field){
                if(is_array($field)){
                    foreach($field as $rs){
                        if($rs != 1)
                            $error++;
                    }
                }
                else if($field != 1)
                    $error++;
            }
        }
        $result = array(
            'error' => $error,
            'result' => $dataResult
        );

        echo $this->json_encode($result);
    }

    public function validcheckAppointment($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Name", "Position", "Contract Type", "Effective Date");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['StaffName'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Position'],
                $Lang['CEES']['MonthlyReport']['JSLang']['ContractType'],
                $Lang['CEES']['MonthlyReport']['JSLang']['EffectiveDate']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] != "" ? 1 : 0;
                $checkResult[$i][1] = $this->checkPosition($data[$i][1]);
                $checkResult[$i][2] = $this->checkContractType(($data[$i][2]));
                $checkResult[$i][3] = $this->checkDate($data[$i][3]);
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckTermination($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Name", "Position", "Contract Type", "Termination Reason", "Effective Date");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['StaffName'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Position'],
                $Lang['CEES']['MonthlyReport']['JSLang']['ContractType'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Reason'],
                $Lang['CEES']['MonthlyReport']['JSLang']['EffectiveDate']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] != "" ? 1 : 0;
                $checkResult[$i][1] = $this->checkPosition($data[$i][1]);
                $checkResult[$i][2] = $this->checkContractType(($data[$i][2]));
                $checkResult[$i][3] = $this->checkTerminationReason(($data[$i][3]));
                $checkResult[$i][4] = $this->checkDate($data[$i][4]);
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckSubstituteTeacher($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Name of Substitute", "Date", "Facilitate Vacancy", "Name of Substituted");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['SubstituteTeacher'],
                $Lang['CEES']['MonthlyReport']['JSLang']['SubstitutionDate'],
                $Lang['CEES']['MonthlyReport']['JSLang']['FacilitateVacancy'],
                $Lang['CEES']['MonthlyReport']['JSLang']['SubstitutedName']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] != "" ? 1 : 0;
                $checkResult[$i][1] = $this->checkDate($data[$i][1]);
                $checkResult[$i][2] = $this->checkFacilitateVacancy(($data[$i][2]));
                $checkResult[$i][3] = $this->checkTeacher($data[$i][3]);
                $data[$i][3] = $this->teacherUserLoginToUserID($data[$i][3]);
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckSickLeave($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Name", "Days", "Medical Cetification");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['StaffName'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Absent'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Certificate'],
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] != "" ? 1 : 0;
                $checkResult[$i][1] = $this->checkDays($data[$i][1], 0.5);
                $checkResult[$i][2] = $this->checkCertification(($data[$i][2]));
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckStudentEnroll($data){
        global $Lang;
        if(!empty($data) && sizeof($data) == 6 + 1) {
            $data_format = array("Level", "Aprroved No.", "No. in Operation", "No. of School Places", "No. Enrolled", "Attendance Rate");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Level'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Approved'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Operation'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Places'],
                $Lang['CEES']['MonthlyReport']['JSLang']['thisMonthNo'],
                $Lang['CEES']['MonthlyReport']['JSLang']['AverageRate']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<6; $i++){
                $condition = isEJ()? $data[$i][0] == "P".($i+1):$data[$i][0] == "S".($i+1);
                $checkResult[$i][0] = $condition ? 1 : 0 ;
                $checkResult[$i][1] = $this->checkPositiveInt($data[$i][1]);
                $checkResult[$i][2] = $this->checkPositiveInt($data[$i][2]);
                $checkResult[$i][3] = $this->checkPositiveInt($data[$i][3]);
                $checkResult[$i][4] = $this->checkPositiveInt($data[$i][4]);
                $checkResult[$i][5] = $this->checkPercentage($data[$i][5]);
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckVisitInspection($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Date", "Purpose");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Date'],
                $Lang['CEES']['MonthlyReport']['JSLang']['Purpose']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $this->checkDate($data[$i][0]) ;
                $checkResult[$i][1] = $data[$i][1] != "" ? 1 : 0;
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckSchoolAdministration($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Date", "Particulars");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Date'],
                $Lang['CEES']['MonthlyReport']['JSLang']['AdministrationParticulars']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $this->checkDate($data[$i][0]) ;
                $checkResult[$i][1] = $data[$i][1] != "" ? 1 : 0;
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckTeacherCourse($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Date", "Name", "Course", "Organization");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Date'],
                $Lang['CEES']['MonthlyReport']['JSLang']['StaffName'],
                $Lang['CEES']['MonthlyReport']['JSLang']['CourseName'],
                $Lang['CEES']['MonthlyReport']['JSLang']['OrganizedBy']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $this->checkDate($data[$i][0]) ;
                $checkResult[$i][1] = $this->checkTeacher($data[$i][1]);
                $data[$i][1] = $this->teacherUserLoginToUserID($data[$i][1]);
                $checkResult[$i][2] = $data[$i][2] != "" ? 1 : 0;
                $checkResult[$i][3] = $data[$i][3] != "" ? 1 : 0;
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckStudentECA($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Date", "Category", "Name", "Total");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Date'],
                $Lang['CEES']['MonthlyReport']['JSLang']['ActivityCategory'],
                $Lang['CEES']['MonthlyReport']['JSLang']['ActivityName'],
                $Lang['CEES']['MonthlyReport']['JSLang']['participantsNo']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $this->checkDate($data[$i][0]) ;
                $checkResult[$i][1] = $this->checkOLECategoryArray($data[$i][1]);
                $checkResult[$i][2] = $data[$i][2] != "" ? 1 : 0;
                $checkResult[$i][3] = $this->checkPositiveInt($data[$i][3]);
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckSchoolAward($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Award");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Award']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] ? 1 : 0;
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckAdvancedNotice($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Description");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Description']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] ? 1 : 0;
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function validcheckOthersReport($data){
        global $Lang;
        if(!empty($data)) {
            $data_format = array("Description");
            $col_name = array_shift($data);
            $data_Lang = array(
                $Lang['CEES']['MonthlyReport']['JSLang']['Description']
            );

            // check format
            $check = sizeof($data_format) == sizeof($col_name);
            for($i=0; $i<sizeof($data_format) && $check; $i++){
                if($data_format[$i] != $col_name[$i]) {
                    $check = 0;
                }
            }
            $checkResult = array();
            $rowResult = array();
            for($i=0;$i<sizeof($data); $i++){
                $checkResult[$i][0] = $data[$i][0] ? 1 : 0;
                $rowResult[$i] = $this->checkResult($checkResult[$i]);
            }

            return array(
                "columns" => $data_Lang,
                "data" => $data,
                "checkResult" => $checkResult,
                "rowResult" => $rowResult
            );
        }
        return null;
    }

    public function template($section = '')
    {
        global $_PAGE;
        $reportType = $section;

        echo $main = $this->getCSVFile('/monthlyreport/template/Monthlyreport/'. $reportType .'.csv',$reportType);
    }

    public function getCSVFile($filepath, $reportType){
        global $intranet_root;
        if(file_exists($intranet_root.$this->getViewBaseDir().$filepath)){
            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:".filesize($intranet_root.$this->getViewBaseDir().$filepath));
            header("Content-Disposition: attachment; filename=".$reportType."_template.csv");
            readfile($intranet_root.$this->getViewBaseDir().$filepath);
        } else {
            return "File not existed!";
        }
    }

    // Start of data input validation checking
    function checkPosition($field){
        global $Lang;
        if(!$field) return 'empty';
        if($field >= 1 && $field <= count($Lang['CEES']['MonthlyReport']['JSLang']['PositionName'])){
            return 1;
        } else
            return 'invalid';
    }

    function checkContractType($field){
        $selections = array('Permanent','Establishment','Funding','Contract');

        if(!$field) return 'empty';
        if($field >=0 && $field <= count($selections)){
            return 1;
        } else
            return 'invalid';
    }

    function checkTerminationReason($field){
        global $Lang;

        $selections = array(
            $Lang['CEES']['MonthlyReport']['JSLang']['Expired'],
            $Lang['CEES']['MonthlyReport']['JSLang']['Resign'],
            $Lang['CEES']['MonthlyReport']['JSLang']['Retire'],
            $Lang['CEES']['MonthlyReport']['JSLang']['Termination']
        );

        if(!$field) return 'empty';
        if($field >=0 && $field <= count($selections)){
            return 1;
        } else
            return 'invalid';
    }

    function checkDate(&$field){
        //Date format YYYY-MM-DD
        $check_format1 = preg_match('/^[\d]{4}[-]([\d]{2}||[\d]{1})[-]([\d]{2}||[\d]{1})$/i', $field);
        $check_format2 = preg_match('/^([\d]{2}||[\d]{1})[\/]([\d]{2}||[\d]{1})[\/][\d]{4}$/i', $field);
        if($check_format1 || $check_format2) {
            if ($check_format1) {
                $date = explode('-', $field);
                return checkdate($date[1], $date[2], $date[0]) ? 1 : 0;
            } else {
                $date = explode('/', $field);
                $field = $date[2] . '-' . $date[1] . '-' . $date[0];
                return checkdate($date[1], $date[0], $date[2]) ? 1 : 0;
            }
        }
        return 'invalid';
    }

    function checkFacilitateVacancy($field){
        // Only (Y || N)
        if($field == "")
            return 'empty';
        return ($field == "Y" || $field == "N") ? 1 : 'invalid';
    }

    function checkDays($field, $step = 1){
        $checkInt = $this->checkPositiveInt($field, $step);
        if($checkInt != 1)
            return $checkInt;
        return ($field >= 0 && $field <=30) ? 1 : 'invalid';
    }

    function checkCertification($field){
        if($field == "")
            return 'empty';
        return ($field >=0 && fmod($field, 0.5) == 0 && is_numeric($field)) ? 1 : 'invalid';
    }

    function checkResult($row){
        foreach($row as $field){
            if(is_array($field)){
                if($this->checkResult($field) == 0)
                    return 0;
            } else {
                if($field != 1) return 0;
            }
        }
        return 1;
    }

    function checkPositiveInt($field, $step = 1){
        return ($field >= 0 && is_numeric($field) && fmod($field, $step) == 0) ? 1 : 'invalid';
    }

    function checkPercentage($field){
        return ($field >= 0 && $field <= 100) ? 1 : 'invalid';
    }

    function checkOLECategoryArray($field){
        global $Lang;
        if(empty($field) || $field[0] == "")
            return 'empty';
        $category = $Lang['CEES']['MonthlyReport']['JSLang']['OLECategoryAry'];
        $array = is_array($field);
        if($array){
            $resultArr = array();
            foreach($field as $id){
                $resultArr[$id] = (array_key_exists($id, $category))? 1 : 'ne';
            }
            return $resultArr;
        }else {
            return array_key_exists($field, $category) ? 1 : 'ne';
        }
    }

    function checkTeacher($field){
        $array = is_array($field);
        $teachers = $this->getTeacherInfo();
        $userloginArr = array();
        foreach($teachers as $teacher){
            $userloginArr[$teacher['UserLogin']] = $teacher;
        }
        if($array){
            $resultArr = array();
            foreach($field as $ul){
                if(array_key_exists($ul, $userloginArr)){
                    $resultArr["u".$userloginArr[$ul]['UserID']] = 1;
                } else {
                    $resultArr[$ul] = 'ne';
                }
            }
            return $resultArr;
        } else {
            return array_key_exists($field, $userloginArr) ? 1 : 'ne';
        }
    }

    // End of data input validation checking

    function teacherUserLoginToUserID($field)
    {
        $array = is_array($field);
        $newArr = array();
        $teachers = $this->getTeacherInfo();
        $userloginArr = array();
        foreach ($teachers as $teacher) {
            $userloginArr[$teacher['UserLogin']] = $teacher;
        }
        if ($array) {
            $resultArr = array();
            foreach ($field as $ul) {
                if (array_key_exists($ul, $userloginArr)) {
                    $newArr[] = $userloginArr[$ul]['UserID'];
                } else {
                    $newArr[] = $ul;
                }
            }
            return $newArr;
        } else {
            if (array_key_exists($field, $userloginArr)) {
                return $userloginArr[$field]['UserID'];
            } else {
                return $field;
            }
        }
    }
    public function getTeacherInfo()
    {
        global $intranet_session_language, $intranet_root;
        include_once($intranet_root.'/includes/libdb.php');

        $db = new libdb();
        $tablename = isEJ()?'INTRANET_USER_UTF8':'INTRANET_USER';

        if($intranet_session_language == 'b5'){
            $name  = "IF(ChineseName is null or ChineseName ='', EnglishName, " . Get_Lang_Selection('ChineseName', 'EnglishName') . " )";
        }else{
            $name  = "IF(EnglishName is null or EnglishName ='', ChineseName, " . Get_Lang_Selection('ChineseName', 'EnglishName') . " )";
        }

        $col = "UserID, UserLogin, $name as Name";

        $sql = "SELECT 
                    $col
                FROM
                    ".$tablename."
                WHERE 
                    RecordType='1' AND
                    RecordStatus='1' 
                        ";
        $TeacherInfo = $db->returnResultSet($sql);

        return $TeacherInfo;
    }

    //json encode
    function json_encode($a=false)
    {
        if (is_null($a)) return 'null';
        if ($a === false) return 'false';
        if ($a === true) return 'true';
        if (is_scalar($a))
        {
            if (is_float($a))
            {
                // Always use "." for floats.
                return floatval(str_replace(",", ".", strval($a)));
            }

            if (is_string($a))
            {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
            }
            else
                return $a;
        }
        $isList = true;
        for ($i = 0, reset($a); $i < count($a); $i++, next($a))
        {
            if (key($a) !== $i)
            {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList)
        {
            foreach ($a as $v) $result[] = $this->json_encode($v);
            return '[' . join(',', $result) . ']';
        }
        else
        {
            foreach ($a as $k => $v) $result[] = $this->json_encode($k).':'.$this->json_encode($v);
            return '{' . join(',', $result) . '}';
        }
    }
    //json encode
}