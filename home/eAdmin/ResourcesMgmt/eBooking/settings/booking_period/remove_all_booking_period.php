<?php
//using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

$lebooking_ui = new libebooking_ui();

intranet_auth();
intranet_opendb();

$StartDate = getStartDateOfAcademicYear($AcademicYearID);
$EndDate = getEndDateOfAcademicYear($AcademicYearID);
$date_cond = " (RecordDate Between '$StartDate' AND '$EndDate') ";

if($FacilityType == 1) {
	$facility_cond = " AND (RelatedSubLocationID = $targetRoom AND RelatedItemID = 0) ";
} else if($FacilityType == 2) {
	$facility_cond = " AND (RelatedSubLocationID = 0 AND RelatedItemID = $targetItem) ";
} else {
	$facility_cond = " AND (RelatedSubLocationID = 0 AND RelatedItemID = 0) ";
}
$sql = "DELETE FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $date_cond $facility_cond";
$result = $lebooking_ui->db_db_query($sql);

if($result){
	if($FacilityType == 1) {
		header("location: specific_available_period.php?AcademicYearID=AcademicYearID&FacilityType=$FacilityType&targetRoom=$targetRoom&msg=".urlencode($Lang['General']['ReturnMessage']['DeleteSuccess']));
	}else if($FacilityType == 2) { 
		header("location: specific_available_period.php?AcademicYearID=AcademicYearID&FacilityType=$FacilityType&targetItem=$targetItem&msg=".urlencode($Lang['General']['ReturnMessage']['DeleteSuccess']));
	}else{
		header("location: general_available_period.php?AcademicYearID=AcademicYearID&msg=".urlencode($Lang['General']['ReturnMessage']['DeleteSuccess']));
	}	
}else{
	if($FacilityType == 1) {
		header("location: specific_available_period.php?AcademicYearID=AcademicYearID&FacilityType=$FacilityType&targetRoom=$targetRoom&msg=".urlencode($Lang['General']['ReturnMessage']['DeleteUnsuccess']));
	}else if($FacilityType == 2) { 
		header("location: specific_available_period.php?AcademicYearID=AcademicYearID&FacilityType=$FacilityType&targetItem=$targetItem&msg=".urlencode($Lang['General']['ReturnMessage']['DeleteUnsuccess']));
	}else{
		header("location: general_available_period.php?AcademicYearID=AcademicYearID&msg=".urlencode($Lang['General']['ReturnMessage']['DeleteUnsuccess']));
	}
}
exit();
intranet_closedb();
?>