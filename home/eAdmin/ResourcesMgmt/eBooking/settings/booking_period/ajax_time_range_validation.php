<?php
// Using :
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

if(!$IsSuspension)
{
	### Check Time range is valid first 
	if(sizeof($ArrayTimeSlot) == 0){
		for($i=0; $i<$NoOfTimePeriod; $i++){
			if($ArrayStartTime[$i] < $ArrayEndTime[$i]){
				if($i!=0){
					if($ArrayEndTime[$i-1] < $ArrayStartTime[$i]){
						$pass_counter++;
					}
				}else{
					$pass_counter++;
				}
			}
		}
	}else{
		
		if($NoOfTimePeriod == sizeof(array_unique($ArrayTimeSlot)))
		{
			for($i=0; $i<$NoOfTimePeriod; $i++){
				if($ArrayTimeSlot[$i] != ""){
					$pass_counter++;
				}
			}
		}
	}
}

if(!$IsSuspension && $pass_counter != $NoOfTimePeriod)
{
	$return_value = -1;
}
else
{
	### Check Any Existing Booking Period
//	if($PeriodType == "BASIC")
//	{
//		$cond = " AND RelatedItemID = 0 AND RelatedSubLocationID = 0 ";
//	}
//	else
//	{
//		if($FacilityType == 1)
//			$cond = " AND RelatedItemID = 0 AND RelatedSubLocationID = $FacilityID ";
//		else
//			$cond = " AND RelatedItemID = $FacilityID AND RelatedSubLocationID = 0 ";
//	}
	list($RelatedSubLocationID, $RelatedItemID) = $lebooking->Process_FacilityID($FacilityType, $FacilityID);
	$cond = " AND RelatedItemID = '$RelatedItemID' AND RelatedSubLocationID = '$RelatedSubLocationID' ";
	$Table = $IsSuspension==1?"INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION":"INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD";
	
	$sql = "SELECT IF(COUNT(*)>0,1,0) FROM $Table WHERE (RecordDate BETWEEN '$StartDate' AND '$EndDate') $cond";
	$arrResult = $lebooking->returnVector($sql);
	
	if($arrResult[0] == 0)
		$return_value = 1;
	else
		$return_value = 0;
}

echo $return_value;

/*
### Get the exactly StartTime and EndTime
if($TimeSlotID == 0){
	$StartTime = date("H:i:s",mktime($StartHour, $StartMin, 0, 0,0,0));
	$EndTime = date("H:i:s",mktime($EndHour, $EndMin, 0, 0,0,0));
}else{
	$sql = "SELECT StartTime, EndTime FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID = $TimeSlotID";
	$arrResult = $lebooking->returnArray($sql,2);
	if(sizeof($lebooking) > 0){
		list($StartTime, $EndTime) = $arrResult[0]; 
	}
}

// build the SQL condition
if($PeriodType == "BASIC"){
	
	$cond = " AND RelatedItemID = 0 AND RelatedSubLocationID = 0 ";
	if($PeriodID != "")
		$cond .= " AND RelatedPeriodID != $PeriodID ";
}
else
{
	if($FacilityType == 1)
		$cond = " AND RelatedItemID = 0 AND RelatedSubLocationID = $FacilityID ";
	else
		$cond = " AND RelatedItemID = $FacilityID AND RelatedSubLocationID = 0 ";
	if($PeriodID != "")
		$cond .= " AND RelatedPeriodID != $PeriodID ";
}

// time range validation - start
if(($RepeatOption == 0)||($RepeatOption == 1))	// Does not repeat || Daily
{
	$sql = "SELECT IF(COUNT(*)>0,1,0) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RecordDate BETWEEN '$StartDate' AND '$EndDate') AND StartTime = '$StartTime' AND EndTime = '$EndTime'";
	$sql .= $cond;
	$result = $lebooking->returnVector($sql);
	if($result[0] == 0)
		$return_value = true;
}
else if($RepeatOption == 2)						// Repeat on Weekday
{
	if(sizeof($ArrayWeekday)>0) 
	{
		unset($arrRepeatValue);
		foreach($ArrayWeekday as $key=>$val)
		{
			$arrTargetWeekday[] = "'".$val."'";
			$arrRepeatValue[] = $val;
		}
		$targetWeekday = implode(",",$arrTargetWeekday);
		$final_repeat_value = implode(",",$arrRepeatValue);
		
		$start_year = substr($StartDate,0,4);
		$start_month = substr($StartDate,5,2);
		$start_day = substr($StartDate,-2);
		$strDateDiff = strtotime($EndDate) - strtotime($StartDate);
		$numOfDayDiff = round(($strDateDiff + 86400)/ 86400 );
		
		unset($parent_period_id);
		unset($result);
		unset($pass_cnt);
		unset($total_num_of_date);
		for($i=0; $i<$numOfDayDiff; $i++)
		{
			$ts_targetDate = mktime(0,0,0,$start_month,$start_day+$i,$start_year);
			$tmp_weekday = date("w",$ts_targetDate);
			
			if(in_array($tmp_weekday,$ArrayWeekday))
			{
				$total_num_of_date++;
				$targetDate = date("Y-m-d",$ts_targetDate);
				
				$sql = "SELECT IF(COUNT(*)>0,1,0) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RecordDate = '$targetDate') AND StartTime = '$StartTime' AND EndTime = '$EndTime'";
				$sql .= $cond;
				$result = $lebooking->returnVector($sql);
				if($result[0] == 0)
					$pass_cnt++;
			}
		}
	}
	if($pass_cnt == $total_num_of_date)
		$return_value = true;
}
else if($RepeatOption == 3)						// Repeat on Cycle Day
{
	if(sizeof($ArrayCycleday)>0)
	{
		unset($arrRepeatValue);
		unset($result);
		unset($pass_cnt);
		unset($total_num_of_date);
		foreach($ArrayCycleday as $key=>$val)
		{
			$arrCycleDay[] = "'".$val."'";
			$arrRepeatValue[] = $val;
		}
		$targetCycleDay = implode(",",$arrCycleDay);
		$final_repeat_value = implode(",",$arrRepeatValue);
		
		$sql = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate BETWEEN '$StartDate' AND '$EndDate' AND TextShort IN ($targetCycleDay)";
		$arrTargetRecordDate = $lebooking->returnArray($sql,1);
		
		if(sizeof($arrTargetRecordDate))
		{
			for($i=0; $i<sizeof($arrTargetRecordDate); $i++)
			{
				list($targetDate) = $arrTargetRecordDate[$i];
				$total_num_of_date++;
				
				$sql = "SELECT IF(COUNT(*)>0,1,0) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RecordDate = '$targetDate') AND StartTime = '$StartTime' AND EndTime = '$EndTime'";
				$sql .= $cond;
				$result = $lebooking->returnVector($sql);
				if($result[0] == 0)
					$pass_cnt++;
			}
		}
	}
	if($pass_cnt == $total_num_of_date)
		$return_value = true;
}


*/

intranet_closedb();
?>