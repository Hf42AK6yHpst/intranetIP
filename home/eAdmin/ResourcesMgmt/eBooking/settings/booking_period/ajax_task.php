<?php
// using: 

################################################## Change Log ##################################################
##
##  Date    :   2019-04-29 Cameron
##              fix cross site scripting by applying cleanCrossSiteScriptingCode() to variables
##
##	Date	:	2010-11-25 (Ronald)
##	Details :	Add a new case "ChangeCopyFromCategory", "ShowSelectTargetBookingPeriod", "CopyStart"
##

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();
$li = new libebooking_ui();

switch ($task)
{
	case "ChangeCopyFromCategory":
	
		if($TargetCategoryID == 1)
		{

			$filter = $li->Get_Booking_Location_Selection("Layer_FacilityID","","  onChange='js_Change_CopyFrom_Target(this.value);' ",0,0,0,$SchoolYearID,$excludeFacilityID);
			$filter = "<tr><td valign='top' width='30%' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations']."</td><td>$filter</td></tr>";
		}
		else
		{
			$filter = $li->Get_Booking_Item_Selection("Layer_FacilityID","","  onChange='js_Change_CopyFrom_Target(this.value);' ",0,0,$SchoolYearID,$excludeFacilityID);
			$filter = "<tr><td valign='top' width='30%' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ItemName']."</td><td>".$filter."</td></tr>";
			
		}

		$output = "<table border='0' cellpadding='3' cellspacing='0' width='100%'>";
		$output .= $filter; 
		$output .= "</table>";
	break;
	
	case "ShowSelectTargetBookingPeriod":		
		$output = "<table border='0' cellpadding='3' cellspacing='0' width='100%'>";
		$output .= "<tr>";
		$output .= "<td valign='top' width='30%' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod']."</td>";
		$output .= "<td>".$li->Get_Facility_Detailed_Booking_Period_Table($SchoolYearID, $TargetCategoryID, $TargetFacilityID, 0)."</td>";
		$output .= "</tr>";
		$output .= "</table>";
	break;
	
	case "CopyStart":
		
		$li->Start_Trans();
		
		$CopyToArr[] = array($CopyToType, $CopyToFacilityID);
		
		$Result = $li->Copy_Booking_Period_Setting($SchoolYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr);
		
		if($Result){
			$li->Commit_Trans();
			$output = 1;
		}else{
			$li->RollBack_Trans();
			$output = 0;
		}
	break;
	
	case "Get_BookingPeriod_TimeZone_Edit_Layer":
	    $TimeZoneID = IntegerSafe($TimeZoneID);
	    $SpecialTimetableSettingID = IntegerSafe($SpecialTimetableSettingID);
	    $FacilityType = IntegerSafe(cleanCrossSiteScriptingCode($FacilityType));
	    $FacilityID = IntegerSafe($FacilityID);
		echo $li->Get_BookingPeriod_TimeZone_Edit_Layer($TimeZoneID, $SpecialTimetableSettingID, $FacilityType, $FacilityID);
	break;
	
	case "Get_Booking_Table_Setting_View":
	    $AcademicYearID = IntegerSafe($AcademicYearID);
	    $FacilityType = IntegerSafe(cleanCrossSiteScriptingCode($FacilityType));
	    $FacilityID = IntegerSafe($FacilityID);
		switch ($SettingView)
		{
			case 0:
				echo $li->Booking_Period_Time_Zone_Setting($AcademicYearID, $FacilityType, $FacilityID);
			break;
			case 1:
				echo $li->Booking_Period_Special_Setting_Table($AcademicYearID, $FacilityType, $FacilityID);
			break;
			case 2:
				echo $li->Booking_Period_Day_Suspension_Table($AcademicYearID, $FacilityType, $FacilityID);
			break;
		}
	break;
}
echo $output;

intranet_closedb();
?>