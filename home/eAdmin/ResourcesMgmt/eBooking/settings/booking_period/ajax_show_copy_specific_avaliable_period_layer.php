<?php
// using : Ronald

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$lebooking_ui	= new libebooking_ui();

echo $lebooking_ui->SpecificAvaliablePeriodCopyForm($SchoolYearID,$FacilityType,$FacilityID);

intranet_closedb();
?>