<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$lebooking->Check_Page_Permission();

# Get data
$GroupID = $_REQUEST['GroupID'];
$DeleteUserIDArr = $_REQUEST['DeleteUserIDArr'];

$Keyword_Index = stripslashes($_REQUEST['Keyword_Index']);
$pageNo_Index = $_REQUEST['pageNo_Index'];
$order_Index = $_REQUEST['order_Index'];
$field_Index = $_REQUEST['field_Index'];
$numPerPage_Index = $_REQUEST['numPerPage_Index'];

$Keyword = stripslashes($_REQUEST['Keyword']);
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
$success = $objMgmtGroup->Delete_Member($DeleteUserIDArr);

$ReturnMsgKey = ($success)? "DeleteMgmtGroupMemberSuccess" : "DeleteMgmtGroupMemberFailed";

intranet_closedb();

$para = '';
$para .= "&Keyword=$Keyword&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage";
$para .= "&Keyword_Index=$Keyword_Index&pageNo_Index=$pageNo_Index&order_Index=$order_Index&field_Index=$field_Index&numPerPage_Index=$numPerPage_Index";

header("Location: member_list.php?GroupID=$GroupID&ReturnMsgKey=$ReturnMsgKey".$para);
?>