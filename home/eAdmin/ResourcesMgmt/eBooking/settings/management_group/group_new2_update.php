<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$SelectedUserIDArr = $_REQUEST['SelectedUserIDArr'];
$GroupID = $_REQUEST['GroupID'];
$Keyword = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword']));
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
$Success = $objMgmtGroup->Add_Member($SelectedUserIDArr);


intranet_closedb();

$FromMemberList = $_REQUEST['FromMemberList'];
if ($FromMemberList)
{
	$Keyword_Index = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword_Index']));
	$pageNo_Index = $_REQUEST['pageNo_Index'];
	$order_Index = $_REQUEST['order_Index'];
	$field_Index = $_REQUEST['field_Index'];
	$numPerPage_Index = $_REQUEST['numPerPage_Index'];
	
	$nextPage = "member_list.php?";
	$extraPara = "&GroupID=$GroupID&Keyword_Index=$Keyword_Index&num_per_page_Index=$num_per_page_Index&pageNo_Index=$pageNo_Index&order_Index=$order_Index&field_Index=$field_Index&numPerPage_Index=$numPerPage_Index";
	$ReturnMsgKey = ($Success)? 'AddMgmtGroupMemberSuccess' : 'AddMgmtGroupMemberFailed';
}
else
{
	$nextPage = "management_group.php?";
	$ReturnMsgKey = ($Success)? 'AddMgmtGroupSuccess' : 'AddMgmtGroupFailed';
}

$para = "Keyword=$Keyword&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage&ReturnMsgKey=$ReturnMsgKey";
header("Location: ".$nextPage.$para.$extraPara);

?>