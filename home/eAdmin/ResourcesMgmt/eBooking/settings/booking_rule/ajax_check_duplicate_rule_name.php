<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$lebooking->Start_Trans();

$rule_name = $lebooking->Get_Safe_Sql_Query($rule_name);

$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_USER_BOOKING_RULE WHERE RuleName = '$rule_name' AND RuleID != $targetRuleID";
$result = $lebooking->returnVector($sql);

if($result[0] > 0){
	// Rule name is duplicate
	echo "0";
	$lebooking->Commit_Trans();
} else {
	// Rule name is passed
	echo "1";
	$lebooking->Commit_Trans();
}

intranet_closedb();
?>