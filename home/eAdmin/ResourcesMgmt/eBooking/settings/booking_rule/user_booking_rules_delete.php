<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_opendb();

$lebooking = new libebooking();
$lebooking->Start_Trans();

if(is_array($RuleID)){
//	$target_rule_id = implode(",",$RuleID);
	
	$result = $lebooking->Delete_User_Booking_Rule($RuleID);	
	
	if($result)
	{
		$lebooking->Commit_Trans();	
		header("location: user_booking_rules.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['DeleteBookingRuleSuccessfully']));
	}
	else
	{
		$lebooking->RollBack_Trans();
		header("location: user_booking_rules.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['DeleteBookingRuleFailed']));
	}
}

intranet_closedb();
?>