<?php
// using : 

#################################################
#	Date:	2017-07-17	Villa #P100181 
#			Add DisableBooking 
#
#	Date:	2011-06-21	YatWoon
#			Change "Allow Booking" from selection box to textbox
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$lebooking->Start_Trans();

if($user_type==5) // Custom Group
	$TargetUserArr = Get_User_Array_From_Common_Choose($TargetUser);

$BookingWithin_Day = $bookWithin ? $BookingWithin_Day : 0;

if($targetRuleID == "")
{
	$Result["InsertUserBookingRule"] = $lebooking->Insert_User_Booking_Rule( $rule_title, $needApproval, $BookingWithin_Day, $canBookForOthers, $user_type, $disableBooking);
	$targetRuleID = mysql_insert_id();
	$Result["UpdateTargetUser"] = $lebooking->Update_User_Booking_Rule_Target_User($targetRuleID, $user_type, $targetForm);
	$Result["UpdateCustomGroupUser"] = $lebooking->Update_User_Booking_Rule_Custom_Group_User($targetRuleID, $TargetUserArr);
	
	if(!in_array(false,(array)$Result)) {
		$lebooking->Commit_Trans();
		header("location: user_booking_rules.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['InsertBookingRuleSuccessfully']));
	} else {
		$lebooking->RollBack_Trans();
		header("location: user_booking_rules.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['InsertBookingRuleFailed']));
	}
}
else
{
	$Result["UpdateUserBookingRule"] = $lebooking->Update_User_Booking_Rule($targetRuleID,  $rule_title, $needApproval, $BookingWithin_Day, $canBookForOthers, $user_type, $disableBooking);
	$Result["UpdateTargetUser"] = $lebooking->Update_User_Booking_Rule_Target_User($targetRuleID, $user_type, $targetForm);
	$Result["UpdateCustomGroupUser"] = $lebooking->Update_User_Booking_Rule_Custom_Group_User($targetRuleID, $TargetUserArr);
	
	if(!in_array(false,(array)$Result))
	{
		$lebooking->Commit_Trans();
		header("location: user_booking_rules.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditBookingRuleSuccessfully']));
	} else {
		$lebooking->RollBack_Trans();
		header("location: user_booking_rules.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditBookingRuleFailed']));
	}
}

intranet_closedb();
?>