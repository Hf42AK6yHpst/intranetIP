<?php
// using : 

#################################################
#
#   Date:   2019-04-29 Cameron
#           fix js error: don't apply checkOptionAll() and checkOption() to object that doesn't exist 
#
#   Date:   2018-03-26 Isaac
#           pluged in Target user selection plugin to Custom Group User Selection session.
#
#	Date:	2017-07-17	Villa #P100181 
#			Add Disable Booking
#
#	Date:	2011-06-21	YatWoon
#			Change "Allow Booking" from selection box to textbox
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_UserBookingRule";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$edit_mode = 0;
if(is_array($RuleID)){
    	
	$edit_mode = 1;
	$target_rule_id = implode(",",$RuleID);
	
//	$sql = "SELECT RuleName, NeedApproval, DaysBeforeUse, CanBookForOthers FROM INTRANET_EBOOKING_USER_BOOKING_RULE WHERE RuleID = $target_rule_id";
//	$arrExistRuleInfo = $lebooking->returnArray($sql,4);
	$arrExistRuleInfo = $lebooking->Get_User_Booking_Rule($target_rule_id);
	
	if(sizeof($arrExistRuleInfo)>0) {
		list($rule_id,$rule_name,$need_approval,$days_before_use,$can_book_for_others, $RecordType, $DisableBooking) = $arrExistRuleInfo[0];
		
		$arrUserTypeForm = array();
		if($RecordType==5) // custom User Group
		{
			$arrRuleTargetUser = $lebooking->Get_User_Booking_Rule_Custom_Group_User_Mapping($rule_id);
//			$arrCustomGroupUser = Get_Array_By_Key($arrUserRule, "UserID");
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$luser = new libuser();
			$ExistUserOption = array();
			$i = 0;
			foreach($arrRuleTargetUser as $thisUser)
			{
// 			    $ExistUserOption[$thisUser['UserID']] = $luser->getNameWithClassNumber($thisUser['UserID']);
			    $ExistUserOption[$i][0] = 'U'.$thisUser['UserID']; #userID
			    $ExistUserOption[$i][1] = $luser->getNameWithClassNumber($thisUser['UserID']); #user's Name
				$i++;
			}
		}
		else
		{
			$arrRuleTargetUser = $lebooking->Get_User_Booking_Rule_Target_Form_Mapping($rule_id);
			$arrUserTypeForm = Get_Array_By_Key($arrRuleTargetUser, "YearID");
		}
	
	}
}

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$arrNavigation[] = array($Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule'], "user_booking_rules.php");
if($edit_mode) {
	$arrNavigation[] = array($rule_name, "");
	$arrNavigation[] = array($Lang['eBooking']['Button']['FieldTitle']['EditRule'], "");
} else {
	$arrNavigation[] = array($Lang['eBooking']['Button']['FieldTitle']['NewRule'], "");
}
$NavBar = $linterface->GET_NAVIGATION($arrNavigation);
	
## gen radio button for "User Type"
$target_display = " style='display:none;' ";
$target_css = " ";
$staff_check = " CHECKED ";
$student_check = "";
$parent_check = "";

$user_type_selection .= $linterface->Get_Radio_Button("user_type_staff", "user_type", 1, $RecordType==1||!$edit_mode, $Class="", $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TeacherAndStaff'], "showTargetLayer();",$isDisabled=0)."&nbsp;&nbsp;&nbsp;";
$user_type_selection .= $linterface->Get_Radio_Button("user_type_student", "user_type", 2, $RecordType==2, $Class="", $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Students'], "showTargetLayer();",$isDisabled=0)."&nbsp;&nbsp;&nbsp;";
$user_type_selection .= $linterface->Get_Radio_Button("user_type_parent", "user_type", 3, $RecordType==3, $Class="", $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Parents'], "showTargetLayer();",$isDisabled=0)."&nbsp;&nbsp;&nbsp;";
$user_type_selection .= $linterface->Get_Radio_Button("user_type_group", "user_type", 5, $RecordType==5, $Class="", $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CustomGroup'], "showTargetLayer();",$isDisabled=0)."&nbsp;&nbsp;&nbsp;";

## gen checkbox for "Target"
$sql = "SELECT YearID, YearName FROM YEAR ORDER BY Sequence ASC";
$arrYearForm = $lebooking->returnArray($sql,2);
if(sizeof($arrYearForm)>0)
{
	$form_check_all = "";
	if(sizeof($arrUserTypeForm) == sizeof($arrYearForm))
	{
		$form_check_all = " CHECKED ";
	}
	$user_target_selection .= "<input type='checkbox' name='checkAll' id='checkAll' value='1' onClick='checkAllTarget(1);' $form_check_all>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['All']."<BR>";
	for($i=0; $i<sizeof($arrYearForm); $i++)
	{
		list($form_id, $form_name) = $arrYearForm[$i];
		if(($i%5 == 0) && ($i!=0))
			$user_target_selection .= "<br>";
			
		$form_checked = "";
		if($edit_mode) {
			if(in_array($form_id, $arrUserTypeForm))
			{
				$form_checked = " CHECKED ";
			}
		}
		$user_target_selection .= "<input type='checkbox' name='targetForm[]' value='$form_id' onClick='checkAllTarget(0);' $form_checked>$form_name"."&nbsp;&nbsp;&nbsp;";
	}
}


# Custom Group User Selection
// $AddBtn = $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=TargetUser[]&permitted_type=1,2', 9)");
// $DelBtn = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['TargetUser[]'])");
// $UserSelection = getSelectByAssoArray($ExistUserOption, " id='TargetUser[]' name='TargetUser[]' multiple size='10' ", '', $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
// $UserSelect = '<table cellspacing="1" cellpadding="1" border="0">'."\n";
// 	$UserSelect .= '<tbody>'."\n";
// 		$UserSelect .= '<tr>'."\n";
// 			$UserSelect .= '<td>'."\n";
// 				$UserSelect .= $UserSelection."\n";
// 			$UserSelect .= '</td>'."\n";
// 			$UserSelect .= '<td>'."\n";
// 				$UserSelect .= $AddBtn;
// 				$UserSelect .= '<br>'."\n";
// 				$UserSelect .= $DelBtn;
// 			$UserSelect .= '</td>'."\n";
// 		$UserSelect .= '</tr>'."\n";
// 	$UserSelect .= '</tbody>'."\n";
// $UserSelect .= '</table>'."\n";
// $UserSelect .= $linterface->MultiSelectionRemark()."\n";
$ExistUserOption = !empty($ExistUserOption)?$ExistUserOption:array("" => str_repeat(' ',40));
$UserSelect = "<div id = 'divSelectTarget'></div>";


$need_approval_check = " CHECKED ";
$no_need_approval_check = "";
if($edit_mode){
	if($need_approval == 1) {
		$need_approval_check = " CHECKED ";
		$no_need_approval_check = "";
	} else {
		$need_approval_check = "";
		$no_need_approval_check = " CHECKED ";
	}
}
## gen radio button for "Need Approval"
$need_approval_selection .= "<input type='radio' name='needApproval' id='need_approval_yes' value='1' $need_approval_check>".'<label for="need_approval_yes">'.$Lang['General']['Yes']."&nbsp;".'</label>';
$need_approval_selection .= "<input type='radio' name='needApproval' id='need_approval_no' value='0' $no_need_approval_check>".'<label for="need_approval_no">'.$Lang['General']['No']."&nbsp;".'</label>';

$bookWithin_noLimit_check = " CHECKED ";
$bookWithin_Limit_check = "";
if($edit_mode){
	if($days_before_use != 0) {
		$bookWithin_noLimit_check = "";
		$bookWithin_Limit_check = " CHECKED ";
	} else {
		$bookWithin_noLimit_check = " CHECKED ";
		$bookWithin_Limit_check = "";
	}
}
## gen radio button for "Booking Within"
$BookingWithinSelection .= "<input type='radio' name='bookWithin' id='bookWithin_noLimit' value='0' onClick='setUseSpecificPeriod(0)' $bookWithin_noLimit_check>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NoLimit']."&nbsp;";
$BookingWithinSelection .= "<input type='radio' name='bookWithin' id='bookWithin_Limit' value='1' onClick='setUseSpecificPeriod(1)' $bookWithin_Limit_check>";

$BookingWithin_Day_selection = " DISABLED ";
if($edit_mode){
	$BookingWithin_Day_selection = " ";
	if(($days_before_use == "") || ($days_before_use == 0))
	{
		$BookingWithin_Day_selection = " DISABLED ";
	}
}
/*
$BookingWithinSelection .= "<select name='BookingWithin_Day' id='BookingWithin_Day' $BookingWithin_Day_selection>";
for($i=1; $i<=sizeof($Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod']); $i++)
{
	$booking_day_checked = "";
	if($edit_mode){
		if(($days_before_use == 7) && ($i==1)){
			$booking_day_checked = " SELECTED ";
		}
		if(($days_before_use == 30) && ($i==2)){
			$booking_day_checked = " SELECTED ";
		}
		if(($days_before_use == 365) && ($i==3)){
			$booking_day_checked = " SELECTED ";
		}
	}
	
	if($_SESSION['intranet_session_language'] == "en")
		$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Within']." ".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][$i];
	else
		$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][$i]." ".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Within'];

	$BookingWithinSelection .= "<option value='$i' $booking_day_checked>".$txt_day_before_use."</option>";
}
$BookingWithinSelection .= "</select>";
*/
$BookingWithinSelection_textbox = "<input type='text' class='textboxnum2' name='BookingWithin_Day' id='BookingWithin_Day' value='". $days_before_use."' ". $BookingWithin_Day_selection ." maxlength=3>";
if($_SESSION['intranet_session_language'] == "en")
		$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Within']." ". $BookingWithinSelection_textbox ." ". $Lang['eBooking']['WithinDays'];
	else
		$txt_day_before_use = $BookingWithinSelection_textbox ." ". $Lang['eBooking']['WithinDays'];
$BookingWithinSelection .= $txt_day_before_use;

## gen radio button for "Can book for other"
$can_book_for_other_checked = "CHECKED";
$cannot_book_for_other_checked = "";
if($edit_mode) {
	if($can_book_for_others == 1) {
		$can_book_for_other_checked = "CHECKED";
		$cannot_book_for_other_checked = "";
	} else {
		$can_book_for_other_checked = "";
		$cannot_book_for_other_checked = "CHECKED";
	}	
}
$CanBookForOthers .= "<input type='radio' name='canBookForOthers' id='canBookForOthers_yes' value='1' $can_book_for_other_checked>".'<label for="canBookForOthers_yes">'.$Lang['General']['Yes']."&nbsp;".'</label>';
$CanBookForOthers .= "<input type='radio' name='canBookForOthers' id='canBookForOthers_no' value='0' $cannot_book_for_other_checked>".'<label for="canBookForOthers_no">'.$Lang['General']['No']."&nbsp;".'</label>';

## DisableBooking ##
if($DisableBooking){
	$disableBooking_yes = "CHECKED";
	$disableBooking_no = "";
}else{
	$disableBooking_yes = "";
	$disableBooking_no = "CHECKED";
}
$DisableBooking = '<input type="radio" name="disableBooking" id="disableBooking_yes" value="1" '.$disableBooking_yes.'>'.'<label for="disableBooking_yes">'.$Lang['General']['Yes'].'</label>';
$DisableBooking .= '<input type="radio" name="disableBooking" id="disableBooking_No" value="0" '.$disableBooking_no.'>'.'<label for="disableBooking_No">'.$Lang['General']['No'].'</label>';;



## Gen Button ##
//$btn .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "javascript: checkForm();")."&nbsp;";
$btn .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript: checkForm();")."&nbsp;";
$btn .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location='user_booking_rules.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

$linterface->LAYOUT_START();
?>

<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script language="javascript">
	$().ready(function(){
		showTargetLayer();
		if(	$('input[name="disableBooking"]:checked').val()=='1'){
			$('.disableBookingRow').hide();
		}else{
			$('.disableBookingRow').show();
		}
		$('input[name="disableBooking"]').click(function(){
			if($(this).val()=='1'){
				$('.disableBookingRow').hide();
			}else{
				$('.disableBookingRow').show();
			}
		});
		
// 		Setup user select plugin
	 	var divId ='divSelectTarget'
	 	var targetUserTypeAry = [1,2]; 
	 	var userSelectedFieldName = 'TargetUser[]'; 
	 	var userSelectedFieldId = 'TargetUser[]'; 
	 	var selectedUsersData = <?php echo $jsonObj->encode($ExistUserOption)?>;
	 	Init_User_Selection_Menu(divId,targetUserTypeAry,userSelectedFieldName,userSelectedFieldId,selectedUsersData);
	});

	function showTargetLayer()
	{
		switch (true)
		{
			case $("#user_type_staff").attr("checked"): 
				$("tr.CustomGroupRow").hide();
				$("tr.TargetFormRow").hide();
			break;
			case  $("#user_type_student").attr("checked"): 
			case  $("#user_type_parent").attr("checked"):
				$("tr.CustomGroupRow").hide();
				$("tr.TargetFormRow").show();
			break;
			case  $("#user_type_group").attr("checked"):
				$("tr.CustomGroupRow").show();
				$("tr.TargetFormRow").hide();
			break;
		} 
	}
	
	function checkAllTarget(val)
	{
		if(val == 1)
		{
			var tmp_form = document.getElementsByName('targetForm[]');
			for(var i=0; i < tmp_form.length; i++)
			{
				if(document.form1.checkAll.checked)
					tmp_form[i].checked = true;
				else
					tmp_form[i].checked = false;
			}
		}
		else
		{
			document.form1.checkAll.checked = false;
		}
	}
	
	function setUseSpecificPeriod(val)
	{
		if(val == 1) {
			$('#BookingWithin_Day').attr('disabled','');
		} else {
			$('#BookingWithin_Day').attr('disabled','disabled');
		}
	}

	function checkForm()
	{
		var user_type_check = 0;
		var FormValid = true;
		var ErrId = new Array();
		
		$("div.WarnMsg").hide();
		
		/* check rule title is empty */
		if($('#rule_title').val() == "")
		{
			$("div#WarnEmptyrule_title").show();
			ErrId.push("rule_title");
			FormValid = false;
		}
		
		// check target Form		
		var user_type = $("input[name='user_type']:checked").val();
		if((user_type==2 || user_type==3) && $("input[name='targetForm[]']:checked").length==0)
		{
			$("div#WarnEmptyTargetForm").show();
			ErrId.push("TargetFormRow");
			FormValid = false;
		}
		
		//check target user
				
		if(user_type==5)
		{
			if (document.getElementById("TargetUser[]")) {
				checkOption(document.getElementById("TargetUser[]"));
			}
			if(!$("select#TargetUser\\[\\]").children().length)
			{
				$("div#WarnEmptyTargetUser").show();
				ErrId.push("CustomGroupRow");
				FormValid = false;
			}
		}
		

		/* check booking within day */
		//if($('#bookWithin_Limit').attr('checked') && $('#BookingWithin_Day').val() == "")  		
		if($('#bookWithin_Limit').attr('checked') )	
		{
			if($('#BookingWithin_Day').val() == "" ){
				$("div#WarnEmptyBookingWithin_Day").show();
				ErrId.push("bookWithin_Limit");
				FormValid = false;
			}
			else if($('#BookingWithin_Day').val() < 0){
				$("div#WarnEmptyBookingWithin_Day_Negative").show();
				ErrId.push("bookWithin_Limit");
				FormValid = false;
			}
		}

		
		if(!FormValid)
		{
			$("#"+ErrId[0]).focus();
		}
		else
		{
			/* ajax - check any duplicate rule title */
			if($('#targetRuleID').val() != "") {
				var targetRuleID = $('#targetRuleID').val();
			} else {
				var targetRuleID = 0;
			}
			$.post(
				"ajax_check_duplicate_rule_name.php",
				{
					"rule_name" : $('#rule_title').val(),
					"targetRuleID" : targetRuleID
				},
				function(responseText)
				{
					if(responseText == 0) 
					{
						$("div#WarnExistrule_title").show();
						$('#rule_title').focus()
					}
					else
					{
						if (document.getElementById("TargetUser[]")) {
							checkOptionAll(document.getElementById("TargetUser[]"));
						}
						document.form1.action = "user_booking_rules_update.php";
						document.form1.submit();
					}
				}
			);
			
			
		}
	}
</script>

<form name="form1" action="" method="POST">
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="left" class="navigation"><?=$NavBar;?></td>
	</tr>
</table>
<br>
<div class="table_board">
<table border="0" cellpadding="5" cellspacing="0" class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['RuleTitle'];?></td>
		<td>
			<input type="text" name="rule_title" id="rule_title" value="<?=$rule_name;?>">
			<?=$linterface->Get_Form_Warning_Msg("WarnEmptyrule_title", $Lang['eBooking']['Settings']['BookingRule']['JSWarning']['BookingRuleNameEmpty'], "WarnMsg")?>
			<?=$linterface->Get_Form_Warning_Msg("WarnExistrule_title", $Lang['eBooking']['Settings']['BookingRule']['JSWarning']['BookingRuleNameIsUsed'], "WarnMsg")?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserType'];?></td>
		<td><?=$user_type_selection;?></td>
	</tr>
	<tr class="TargetFormRow">
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Target'];?></td>
		<td>
			<?=$user_target_selection;?>
			<?=$linterface->Get_Form_Warning_Msg("WarnEmptyTargetForm", $Lang['eBooking']['Settings']['BookingRule']['JSWarning']['PleaseSelectUserTarget'], "WarnMsg")?>
		</td>
	</tr>
	<tr class="CustomGroupRow">
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TargetUser'];?></td>
		<td>
			<?=$UserSelect;?>
			<?=$linterface->Get_Form_Warning_Msg("WarnEmptyTargetUser", $Lang['eBooking']['Settings']['BookingRule']['JSWarning']['PleaseSelectTargetUser'], "WarnMsg")?>
		</td>
	</tr>
	
</table>
<?=$linterface->Get_Form_Separate_Title($Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingRulesOptions'])?>
<table border="0" cellpadding="5" cellspacing="0" class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['DisableBooking']?></td>
		<td><?=$DisableBooking?></td>
	</tr>
	<tr class="disableBookingRow">
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NeedApproval'];?></td>
		<td><?=$need_approval_selection;?></td>
	</tr>
	<tr class="disableBookingRow">
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingWithin'];?></td>
		<td>
			<?=$BookingWithinSelection;?>
			<?=$linterface->Get_Form_Warning_Msg("WarnEmptyBookingWithin_Day", $Lang['eBooking']['Settings']['BookingRule']['JSWarning']['AllowBookingDayEmpty'], "WarnMsg")?>
			<?=$linterface->Get_Form_Warning_Msg("WarnEmptyBookingWithin_Day_Negative", $Lang['eBooking']['Settings']['BookingRule']['JSWarning']['AllowBookingDayNegative'], "WarnMsg")?>
		</td>
	</tr>
	<tr class="disableBookingRow">
		<td class="field_title"><?=$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CanBookForOthers'];?></td>
		<td><?=$CanBookForOthers;?></td>
	</tr>
</table>
</div>
<div class="edit_bottom_v30">
	<?=$btn;?>
</div>


<input type='hidden' name='targetRuleID' id='targetRuleID' value='<?=$target_rule_id?>'>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>