<?php
// using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_opendb();

$lebooking_ui = new libebooking_ui();

switch ($Action) {
	case "Check_Unique_GroupName":
		$GroupID = stripslashes($_REQUEST['GroupID']);
		$Value = stripslashes($_REQUEST['Value']);
		
		$returnString = $lebooking_ui->Is_Follow_Up_Group_Name_Valid($Value, $GroupID);
		echo $returnString; 
	break;
	
	case "Validate_Selected_Member":
		$GroupID = stripslashes($_REQUEST['GroupID']);
		$SelectedUserList = stripslashes($_REQUEST['SelectedUserList']);
		$returnString = $lebooking_ui->Get_Invalidate_FollowUp_Member_Selection($GroupID, $SelectedUserList);
	break;
}

intranet_closedb();
?>