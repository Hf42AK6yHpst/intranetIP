<?php
//using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_opendb();

$lebooking = new libebooking();

$lebooking->Start_Trans();

if(sizeof($GroupIDArr) > 0)
{
	$targetGroupID = implode(",",$GroupIDArr);
}
	
$sql = "DELETE FROM INTRANET_EBOOKING_FOLLOWUP_GROUP WHERE GroupID IN ($targetGroupID)";
$result['DeleteGroup'] = $lebooking->db_db_query($sql);

$sql = "DELETE FROM INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER WHERE GroupID IN ($targetGroupID)";
$result['DeleteGroupMember'] = $lebooking->db_db_query($sql);

if(!in_array(false,$result)){
	$lebooking->Commit_Trans();
	$ReturnMsgKey = "DeleteFollowUpGroupSuccess";
}else{
	$lebooking->RollBack_Trans();
	$ReturnMsgKey = "DeleteFollowUpGroupFailed";
}

$para = "Keyword=$Keyword&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage";
header("Location: follow_up_group.php?".$para."&ReturnMsgKey=".$ReturnMsgKey);

intranet_closedb();
?>