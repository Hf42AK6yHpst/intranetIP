<?php
// using : ronald
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);
$libebooking_ui = new libebooking_ui();

$returnString = '';
if ($Action == "FollowUpGroup_Table")
{
	$Keyword = stripslashes($_REQUEST['Keyword']);
	
	$PageNumber = stripslashes($_REQUEST['PageNumber']);
	$Order = stripslashes($_REQUEST['Order']);
	$SortField = stripslashes($_REQUEST['SortField']);
	$PageSize = stripslashes($_REQUEST['PageSize']);
	$pageSizeChangeEnabled = true;
	
	$returnString = $libebooking_ui->Get_Settings_FollowUpGroup_Table($Keyword, $PageNumber, $PageSize, $Order, $SortField);
}
else if ($Action == "MemberList_Table")
{
	$GroupID = stripslashes($_REQUEST['GroupID']);
	$Keyword = stripslashes($_REQUEST['Keyword']);
	
	$PageNumber = stripslashes($_REQUEST['PageNumber']);
	$Order = stripslashes($_REQUEST['Order']);
	$SortField = stripslashes($_REQUEST['SortField']);
	$PageSize = stripslashes($_REQUEST['PageSize']);
	$pageSizeChangeEnabled = true;
	
	$returnString = $libebooking_ui->Get_Settings_FollowUpGroup_MemberList_Table($GroupID, $Keyword, $PageNumber, $PageSize, $Order, $SortField);
}

intranet_closedb();

echo $returnString;
?>