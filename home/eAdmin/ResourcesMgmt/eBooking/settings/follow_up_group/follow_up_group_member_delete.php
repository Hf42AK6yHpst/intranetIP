<?php
// using : Ronald
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$lebooking->Start_Trans();

$GroupID = $_REQUEST['GroupID'];
$DeleteUserIDArr = $_REQUEST['DeleteUserIDArr'];

$Keyword_Index = stripslashes($_REQUEST['Keyword_Index']);
$pageNo_Index = $_REQUEST['pageNo_Index'];
$order_Index = $_REQUEST['order_Index'];
$field_Index = $_REQUEST['field_Index'];
$numPerPage_Index = $_REQUEST['numPerPage_Index'];

$Keyword = stripslashes($_REQUEST['Keyword']);
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

if(sizeof($DeleteUserIDArr) > 0)
{
	$targetUserID = implode(",",$DeleteUserIDArr);
}

$sql = "DELETE FROM INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER WHERE UserID IN ($targetUserID)";
$result['DeleteGroupMember'] = $lebooking->db_db_query($sql);

if(!in_array(false,$result)){
	$lebooking->Commit_Trans();
	$ReturnMsgKey = "DeleteFollowUpGroupMemberSuccess";
}else{
	$lebooking->RollBack_Trans();
	$ReturnMsgKey = "DeleteFollowUpGroupMemberFailed";
}

$para = '';
$para .= "&Keyword=$Keyword&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage";
$para .= "&Keyword_Index=$Keyword_Index&pageNo_Index=$pageNo_Index&order_Index=$order_Index&field_Index=$field_Index&numPerPage_Index=$numPerPage_Index";

header("Location: follow_up_group_member_list.php?GroupID=$GroupID&ReturnMsgKey=$ReturnMsgKey".$para);

intranet_closedb();
?>