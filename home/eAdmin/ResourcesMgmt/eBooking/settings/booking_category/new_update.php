<?php
//editing by:
################Change Log [Start]##################
#
#	Date: 2015-01-19 (Omas)
#		Created this page
#
################Change Log [End]##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$lebooking->Check_Page_Permission();

$CategoryName = standardizeFormPostValue($_POST['CategoryName']);
$UserID = $_SESSION['UserID'];
$CategoryID = $_REQUEST['CategoryID'];

if(is_array($CategoryID) && $isDelete == 1){
	$CategoryIDstr = implode('\',\'',$CategoryID);
	//Delete
	$result = $lebooking->insertUpdatDelete_BookingCategory('DELETE', $UserID, '',$CategoryIDstr);

	if($result>0){
		$msg = "DeleteSuccess";
	}else{
		$msg = "DeleteFail";
	}
}
else{
	if($CategoryID > 0){
		//Edit
		$result = $lebooking->insertUpdatDelete_BookingCategory('UPDATE', $UserID, $CategoryName, $CategoryID);
	
		if($result>0){
			$msg = "UpdateSuccess";
		}else{
			$msg = "UpdateFail";
		}
	}
	else{
		//New
		$result = $lebooking->insertUpdatDelete_BookingCategory('INSERT', $UserID, $CategoryName);
	
		if($result>0){
			$msg = "AddSuccess";
		}else{
			$msg = "AddFail";
		}	

	}
}


intranet_closedb();
header("Location: index.php?xmsg={$msg}");

?>