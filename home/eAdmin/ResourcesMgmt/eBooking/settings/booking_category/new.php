<?php
//editing by:
################Change Log [Start]##################
#
#	Date: 2015-01-19 (Omas)
#		Created this page
#
################Change Log [End]##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$lebooking = new libebooking();
$lebooking_ui 	= new libebooking_ui();

$lebooking->Check_Page_Permission();

# Value Ready for edit
if(!empty($CategoryID)){
	if(is_array($CategoryID)){
		$CategoryIDStr = $CategoryID[0];
	}
	else{
		$CategoryIDStr = $CategoryID;
	}
	$CategoryAssoAry= $lebooking->getBookingCategoryByCategoryID($CategoryIDStr,1);
	$CategoryNameValue = $CategoryAssoAry[$CategoryIDStr]['CategoryName'];
}
else{
	$CategoryNameValue='';
}

# Page Setting & Left Menu
$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingCategory";
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['BookingCategory']['BookingCategory']);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();


# Start layout
$linterface->LAYOUT_START($xmsg);

# Navigation bar
$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['BookingCategory']['BookingCategory'], "javascript:history.back();");
$PAGE_NAVIGATION[] = (isset($CategoryID) && $CategoryID>0) ? array($Lang['Btn']['Edit']) : array($Lang['Btn']['New']);
$navigationBar = $linterface->GET_NAVIGATION4($PAGE_NAVIGATION);

# PageEndButton
$PageEndButton .= $linterface->GET_ACTION_BTN($button_submit, "submit");
$PageEndButton .= '&nbsp;';
$PageEndButton .=  $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'");



?>

<script>
$(document).ready( function() {

});

function checkform(obj){
    	if(!check_text(obj.Title, "Please fill in the Title.")) return false;
}
</script>

<form name="form1" action="new_update.php" method="post" onsubmit="return checkform(this)">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				
				<?=$navigationBar?>

			</td>
		</tr>
	</table>

	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" class="form_table_v30">
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$eEnrollmentMenu['cat_title']?>&nbsp;<span class="tabletextrequire">*</span>
			</td>
			<td class="tabletext" width="70%">
				<input type="text" id="CategoryName" name="CategoryName" value="<?=intranet_htmlspecialchars($CategoryNameValue)?>" size="50" />
			</td>
		</tr>
		<tr>
			<td align="left" class="tabletextremark">
				<?=$Lang['General']['RequiredField']?>
			</td>
		</tr>
		
		<input type="hidden" id="CategoryID" name="CategoryID" value="<?=$CategoryIDStr?>" />
	</table>

	<div class="edit_bottom_v30">
		<?=$PageEndButton?>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>