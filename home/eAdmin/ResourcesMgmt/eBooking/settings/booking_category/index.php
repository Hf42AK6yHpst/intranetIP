<?php
//editing by:
################Change Log [Start]##################
#
#	Date: 2015-01-19 (Omas)
#		Created this page
#
################Change Log [End]##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$lebooking = new libebooking();
$lebooking_ui 	= new libebooking_ui();

$lebooking->Check_Page_Permission();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingCategory";
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['BookingCategory']['BookingCategory']);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$sql = $lebooking->getBookingCategorySQL();
$categoryInfoAry = $lebooking->returnResultSet($sql);
$numOfCategory = count($categoryInfoAry);

if ($xmsg == 'DeleteSuccess'){
	$xmsg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}
else if ($xmsg == 'DeleteFail'){
	$xmsg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}
else if ($xmsg == 'AddSuccess'){
	$xmsg = $Lang['General']['ReturnMessage']['AddSuccess'];
}
else if ($xmsg == 'AddFail'){
	$xmsg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
}
else if ($xmsg == 'UpdateSuccess'){
	$xmsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if ($xmsg == 'UpdateFail'){
	$xmsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
$linterface->LAYOUT_START($xmsg);


echo $linterface->Include_JS_CSS();	

## Edit/ Delete Buttons	
$ActionBtnArr = array();
$ActionBtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'CategoryID[]\',\'new.php\')');
$ActionBtnArr[] = array('delete', 'javascript:goDeleteCategory()');

## Display Result
/////>>>>>
$widthCatTitle = 89;
$widthOleCategory = 40;
$widthCategoryType = 20;

/////<<<<<
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="get" action="category2.php">'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div class="content_top_tool">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .= $linterface->Get_Content_Tool_v30("new","new.php")."\n";
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);			
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th style="width:3%;">#</th>'."\n";
									$x .= '<th style="width:89%;">'.$eEnrollmentMenu['cat_title'].'</th>'."\n";				
									$x .= '<th style="width:5%;">&nbsp;</th>'."\n";
									$x .= '<th style="width:3%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'CategoryID[]\'):setChecked(0,this.form,\'CategoryID[]\')" name="checkmaster"></th>'."\n";
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";
					
							$x .= '<tbody>'."\n";
								if ($numOfCategory == 0) {
									$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else {
									for ($i=0; $i<$numOfCategory; $i++) {
										$thisCategoryID  = $categoryInfoAry[$i]['CategoryID'];					
										$thisCategoryNameLink = $categoryInfoAry[$i]['CNameLink'];
										$thisOleCategoryID = $categoryInfoAry[$i]['OleCategoryID'];

										$x .= '<tr id="tr_'.$thisCategoryID .'">'."\n";
											$x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
											$x .= '<td>'.$thisCategoryNameLink.'</td>'."\n";
											$x .= '<td class="Dragable">'."\n";
												$x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move'])."\n";
											$x .= '</td>'."\n";
											$x .= '<td>'."\n";
												$x .= '<input type="checkbox" id="CategoryChk" class="CategoryChk" onclick="unset_checkall(this,document.getElementById(\'form1\'));" name="CategoryID[]" value="'.$thisCategoryID.'">'."\n";
											$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									}					
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .= '<input type="hidden" id="isDelete" name="isDelete" value=0 >'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";

//$x .= '<div id="debugArea"></div>';

?>

<script language="javascript">
$(document).ready( function() {	
	js_Init_DND_Table();
});

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "ContentTable") {
				Block_Element(table.id);
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					{
						var thisID = rows[i].id;
						var thisIDArr = thisID.split('_');
						var thisObjectID = thisIDArr[1];
						RecordOrder += thisObjectID + ",";
					}
				}

				// Update DB
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_Category",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						js_Reset_Display_Range();
						
						// Get system message
						if (ReturnData=='1') {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess']?>';			
						}
						else {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
						$('#debugArea').html(ReturnData);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
	
//Delete Category	
function goDeleteCategory() {
	var jsSelectedCategoryIDArr = new Array();
	$('input.CategoryChk:checked').each( function() {
		jsSelectedCategoryIDArr[jsSelectedCategoryIDArr.length] = $(this).val();
	});
	var jsSelectedCategoryIDList = jsSelectedCategoryIDArr.join(',');

	$.post(
		"ajax_update.php", 
		{ 
			Action: "Is_Category_Record",
			CategoryIDList: jsSelectedCategoryIDList	
		},
		
		function(ReturnData) {
			var jsCanSubmit = false;
			if (ReturnData == '1') {
				if (confirm('<?=$Lang['eEnrolment']['Settings']['WarningArr']['CategoryLinkedToDataAlready']?>')) {
					jsCanSubmit = true;
				}
				else {
					jsCanSubmit = false;
				}
			}
			else {
				jsCanSubmit = true;
			}
			if (jsCanSubmit) {
				$('input#isDelete').val(1);
				$('form#form1').attr('action', 'new_update.php').submit();
			}
		}
	);
}

//Reset the ranking display
function js_Reset_Display_Range(){
	var jsRowCounter = 0;
	$('span.rowNumSpan').each( function () {
		$(this).html(++jsRowCounter);
	});
}
</script>

<?php
echo $x;
?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>