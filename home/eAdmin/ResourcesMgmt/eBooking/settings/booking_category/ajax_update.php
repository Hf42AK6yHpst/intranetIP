<?php
//editing by:
################Change Log [Start]##################
#
#	Date: 2015-01-19 (Omas)
#		Created this page
#
################Change Log [End]##################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$lebooking->Check_Page_Permission();

$Action = $_REQUEST['Action'];
if ($Action == 'Reorder_Category') {
	$DisplayOrderString = $_POST['DisplayOrderString'];
	$displayOrderArr = $lebooking->Get_Update_Display_Order_Arr($DisplayOrderString);
	$success = $lebooking->Update_Category_Record_DisplayOrder($displayOrderArr);
	echo $success? '1' : '0';
}
else if ($Action == 'Is_Category_Record') {
	$CategoryIDList = $_POST['CategoryIDList'];
	$CategoryIDArr = explode(',', $CategoryIDList);
	echo ($lebooking->Is_Category_Linked_Data($CategoryIDArr))? '1' : '0';	
	
}
intranet_closedb();
?>