<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
intranet_auth();
intranet_opendb();

$lebooking_ui	= new libebooking_ui();
$libdb = new libdb();

## Get parameters from form
if (isset($_GET['ReaderID'])) {
	$ReaderID = $_GET['ReaderID'];
}
else{
    $ReaderID = $_POST['ReaderID'];
}


if (is_array($ReaderID))
{
	$ReaderID = $ReaderID;
}
else
{
	$ReaderID = array($ReaderID);
}

$action = $_POST['actionType'];


$successAry = array();
$libdb->Start_Trans();

$numOfReaderID = count($ReaderID);

//echo $numOfReaderID;
for($i=0; $i<$numOfReaderID; $i++){
$extraCardReaderObj = new liblocation_cardReader($ReaderID[$i]);	

	if($action =='enable'){
		$status = 1;
		$extraCardReaderObj ->setRecordStatus($status);
		$extraCardReaderObj ->save();
	}
	
	elseif($action =='disable'){
		$status = 0;
		$extraCardReaderObj ->setRecordStatus($status);
		$extraCardReaderObj ->save();
	}
	if (in_array(false, $successAry)) {
		$libdb->Rollback_Trans();
		$returnMsgKey = 'UpdateUnSuccess';
	}
	else {
		$libdb->Commit_Trans();
		$returnMsgKey = 'UpdateSuccess';
	}

}

intranet_closedb();
header("Location: index.php?returnMsgKey=$returnMsgKey");
?>