<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_item.php");

intranet_auth();
intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Delete_Item')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$objItem = new eBooking_Item($ItemID);
	$success = $objItem->Delete_Object();
	
	$returnString = ($success)? '1' : '0';
}else if($Action == 'Delete_Item_Attachment')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$objItem = new eBooking_Item($ItemID);
	$success = $objItem->Delete_Item_Attachment($ItemID);
	
	$returnString = ($success)? '1' : '0';
}else if($Action == 'Upload_Item_Attachment'){
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$objItem = new eBooking_Item($ItemID);
	$objItem->Delete_Item_Attachment($ItemID);
	$returnString = '<script type="text/javascript">'."\n";
	if($Attachment!=""){
		if($_FILES["Attachment"]["error"]>0){
			$returnString.= 'window.parent.Upload_Attachemnt_Feedback("'.$ItemID.'",0);';
		}else{
			$attachment_location = $_FILES["Attachment"]["tmp_name"];
			$attachment_name = stripslashes(trim($_FILES["Attachment"]["name"]));
			$success= $objItem->Upload_Item_Attachment($ItemID,$attachment_location,$attachment_name);
			if($success)
				$returnString.= 'window.parent.Upload_Attachemnt_Feedback("'.$ItemID.'",1);';
			else 
				$returnString.= 'window.parent.Upload_Attachemnt_Feedback("'.$ItemID.'",0);';
		}
	}else{
		$returnString.= 'window.parent.Upload_Attachemnt_Feedback("'.$ItemID.'",0);';
	}
	$returnString.= '</script>'."\n";
}

echo $returnString;

intranet_closedb();
?>