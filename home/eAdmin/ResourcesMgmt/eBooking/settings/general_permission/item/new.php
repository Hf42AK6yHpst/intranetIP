<?php
// using : 
#############################################################################
##	Date	:	2011-07-12 (Marcus)
##				do not force client to select follow/approve group 
##
###########################################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking_ui 		= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$CategoryID = $_REQUEST['CategoryID'];
$Category2ID = $_REQUEST['Category2ID'];
$ItemID = $_REQUEST['ItemID'];
echo $lebooking_ui->Get_Settings_GeneralPermission_Item_New_UI($CategoryID, $Category2ID, $ItemID);

?>

<script language="javascript">
$(document).ready( function() {	
	$('select#SelectedCategoryID').focus();
});

function js_Go_Back_To_Item_List()
{
	window.location = 'view.php';
}

function js_Go_Back_To_Item_Details()
{
	window.location = 'view_item_details.php?CategoryID=<?=$CategoryID?>&Category2ID=<?=$Category2ID?>&ItemID=<?=$ItemID?>';
}

function js_Reload_SubCategory_Selection(jsCategoryID)
{
	if (jsCategoryID != '')
	{
		$('#SubCategorySelectionDiv').html('');
		
		$('#SubCategorySelectionDiv').load(
			"ajax_reload.php", 
			{ 
				Action: 'Reload_SubCategory_Selection',
				CategoryID: jsCategoryID,
				ID_Name: 'SelectedCategory2ID',
				isAll: 0
			},
			function(ReturnData)
			{
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	else
	{
		$('div#SubCategorySelectionDiv').html('');
	}
}

function js_Update_Item_Info(jsItemID)
{
	var objForm = document.getElementById('form1');
	
	// Check if selected a category or not
	var jsSelectedCategoryID = $('select#SelectedCategoryID').val();
	if (jsSelectedCategoryID == '')
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectCategory']?>");
		$('select#SelectedCategoryID').focus();
		return false;
	}
	
	// Check if selected a sub-category or not
	var jsSelectedCategory2ID = $('select#SelectedCategory2ID').val();
	if (jsSelectedCategory2ID == '')
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectSubCategory']?>");
		$('select#SelectedCategory2ID').focus();
		return false;
	}
	
	// Check if entered a Code
	var jsItemCode = $('input#ItemCode').val();
	if (jsItemCode == '')
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputItemCode']?>");
		$('input#ItemCode').focus();
		return false;
	}
	
	// Check if entered a English Name
	var jsNameEng = $('input#NameEng').val();
	if (jsNameEng == '')
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputNameEng']?>");
		$('input#NameEng').focus();
		return false;
	}
	
	// Check if entered a Chinese Name
	var jsNameChi = $('input#NameChi').val();
	if (jsNameChi == '')
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputNameChi']?>");
		$('input#NameChi').focus();
		return false;
	}
	
	// Check if selected a location or not
	var jsBuilidingID = $('select#BuildingID').val();
	var jsFloorID = $('select#FloorID').val();
	var jsRoomID = $('select#RoomID').val();
	
	if (jsBuilidingID == '' || jsBuilidingID == null || jsFloorID == '' || jsFloorID == null || jsRoomID == '' || jsRoomID == null)
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectLocation']?>");
		$('select#RoomID').focus();
		return false;
	}
	
	// Check if selected any Mgmt Groups
//	if (countChecked(objForm, 'MgmtGroupIDArr[]') == 0)
//	{
//		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//		$('input#MgmtGroup_All').focus();
//		return false;
//	}
	
	// Check if selected any Follow-up Groups
//	if (countChecked(objForm, 'FollowUpGroupIDArr[]') == 0)
//	{
//		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneFollowUpGroup']?>");
//		$('input#FollowUpGroup_All').focus();
//		return false;
//	}
	
	// Check if selected any User Booking Rules
	if (countChecked(objForm, 'RuleIDArr[]') == 0)
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule']?>");
		$('input#RuleID_All').focus();
		return false;
	}
	
	// Check if Item Code Valid
	$.post(
		"ajax_validate.php", 
		{ 
			Action: 'Check_Unique_ItemCode',
			ItemID: jsItemID,
			ItemCode: $('input#ItemCode').val()
		},
		function(isValid)
		{
			if (isValid == '1')
			{
				objForm.action = 'new_update.php';
				objForm.submit();
			}
			else
			{
				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['CodeIsInUse']?>");
				$('input#ItemCode').focus();
				return false;
			}		
		}
	);
}

function js_Delete_Item_Attachment(jsItemID)
{
	if(confirm('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteAttachment']?>')){
		$.post(
			"ajax_update.php",
			{
				"Action":"Delete_Item_Attachment",
				"ItemID":jsItemID
			},
			function(responseText){
				if(responseText == '1'){
					jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentSuccess']?>';
					$('div#AttachmentUploadDiv').show();
					$('div#AttachmentManageDiv').hide();
				}else{
					jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentFailed']?>';
				}
				Get_Return_Message(jsReturnMsg);
			}
		);
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>