<?php
// using : 
/*
 * Change Log:
 * Date	2017-03-23 Villa #W114642 
 * Add AllowBookingIndependence Field
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_item.php");

intranet_auth();
intranet_opendb();

$ItemIDList = stripslashes($_REQUEST['ItemIDList']);
$ItemIDArr = explode(',', $ItemIDList);
$numOfItem = count($ItemIDArr);
for ($i=0; $i<$numOfItem; $i++)
{
	$thisItemID = $ItemIDArr[$i];
	$thisObjItem = new eBooking_Item($thisItemID);
	
	### Update Item Booking Info
	$DataArr = array();
	$DataArr['AllowBooking'] = $_REQUEST['AllowBooking_'.$thisItemID];
	$DataArr['AllowBookingIndependence'] = $_REQUEST['AllowBookingIndependence_'.$thisItemID];
	$DataArr['BookingDayBeforehand'] = $_REQUEST['BookingDayBeforehand_'.$thisItemID];
	//debug_pr($DataArr);
	$SuccessArr[$thisItemID]['Update_ItemInfo'] = $thisObjItem->Update_Object($DataArr);
	
	### Update Item Management Group
	$thisMgmtGroupList = $_REQUEST['MgmtGroup_'.$thisItemID];
	$thisMgmtGroupArr = explode(',', $thisMgmtGroupList);
	$SuccessArr[$thisItemID]['ManagementGroup'] = $thisObjItem->Update_Item_Management_Group($thisMgmtGroupArr);
	
	### Update Item Follow Up Group
	$thisFollowUpGroupList = $_REQUEST['FollowUpGroup_'.$thisItemID];
	$thisFollowUpGroupArr = explode(',', $thisFollowUpGroupList);
	$SuccessArr[$thisItemID]['FollowUpGroup'] = $thisObjItem->Update_Item_FollowUp_Group($thisFollowUpGroupArr);
	
	### Update Item User Booking Rule
	$thisUserBookingRuleList = $_REQUEST['UserBookingRule_'.$thisItemID];
	$thisUserBookingRuleArr = explode(',', $thisUserBookingRuleList);
	$SuccessArr[$thisItemID]['UserBookingRule'] = $thisObjItem->Update_Item_User_Booking_Rule($thisUserBookingRuleArr);
}

intranet_closedb();

$ReturnMsgKey = (in_array(false, $SuccessArr) == false)? 'EditItemSuccess' : 'EditItemFailed';
header("Location: view.php?ReturnMsgKey=$ReturnMsgKey");
?>