<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$linventory		= new libinventory();
$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "view.php", 1);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "../item/view.php", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

if(($targetBuilding != "")&&($targetFloor != "")&&($SubLocationID != ""))
{
	$sql = "SELECT 
				CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
			FROM
				INVENTORY_LOCATION_BUILDING AS building
				INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
			WHERE 
				building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
				AND building.BuildingID = $targetBuilding
				AND floor.LocationLevelID = $targetFloor
				AND room.LocationID = $SubLocationID";
	$arrLocationNameDisplay = $lebooking->returnVector($sql);
	if(sizeof($arrLocationNameDisplay) > 0){
		$LocationNameDisplay = $arrLocationNameDisplay[0];
	}
}

$table_content .= $lebooking_ui->getGeneralPermissionRoomDetailEdit_UI($targetBuilding,$targetFloor,$SubLocationID);

$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "javascript:js_Go_Back_To_View_Detail($targetBuilding,$targetFloor,$SubLocationID)");
$PAGE_NAVIGATION[] = array($LocationNameDisplay, "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$linterface->LAYOUT_START();
?>

<script language='javascript'>
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "targetBuilding";
	arrCookies[arrCookies.length] = "targetFloor";
	arrCookies[arrCookies.length] = "targetRoom";
	arrCookies[arrCookies.length] = "pageNo";
	arrCookies[arrCookies.length] = "numPerPage";
	
	function checkForm()
	{
		var check_booking_rule_cnt = 0;
		var check_mgmt_group_cnt = 0;
		var check_follow_up_group_cnt = 0;
	
		for(var i=0; i<document.getElementsByName("UserBookingRule[]").length; i++)
		{
			if(document.getElementsByName("UserBookingRule[]")[i].checked)
			{
				check_booking_rule_cnt++;
			}
		}
		if(check_booking_rule_cnt == 0)
		{
			alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule']?>");
			return false;
		}
		
		for(var i=0; i<document.getElementsByName("MgmtGroup[]").length; i++)
		{
			if(document.getElementsByName("MgmtGroup[]")[i].checked)
			{
				check_mgmt_group_cnt++;
			}
		}
//		if(check_mgmt_group_cnt == 0)
//		{
//			alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//			return false;
//		}
		
		
		for(var i=0; i<document.getElementsByName("FollowUpGroup[]").length; i++)
		{
			if(document.getElementsByName("FollowUpGroup[]")[i].checked)
			{
				check_follow_up_group_cnt++;
			}
		}
//		if(check_follow_up_group_cnt == 0)
//		{
//			alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneFollowUpGroup']?>");
//			return false;
//		}
		
		if(check_positive_int(document.form1.DayBeforeBooking,"<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['BookingDayBeforehandIsNotValid'];?>","",0))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
	function js_Go_Back_To_View_Detail(building_id,location_id,sub_location_id)
	{
		sub_location_id = sub_location_id || '';
		//window.location = 'view_details.php?building_id='+building_id+'&location_id='+location_id+'&sub_location_id='+sub_location_id;
		
		AssignCookies();
		SetCookies();
		
		window.location = 'view_details.php';
	}
	
	function js_Delete_Room_Attachment(jsRoomID)
	{
		$.post(
			"ajax_update_room_attachment.php",
			{
				"Action":"DeleteRoomAttachment",
				"RoomID":jsRoomID
			},
			function(responseText){
				var jsReturnMsg = '';
				if(responseText == '1'){
					jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentSuccess']?>';
					$('div#AttachmentUploadDiv').show();
					$('div#AttachmentManageDiv').hide();
				}else{
					jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentFailed']?>';
				}
				Get_Return_Message(jsReturnMsg);
			}
		);
	}
	
</script>

<br>
<form id="form1" name="form1" method="post" action="view_details_edit_update.php" enctype="multipart/form-data" onsubmit="return checkForm();">
	<table width="98%" border="0" cellpadding"0" cellspacing="0">
		<tr>
			<td><?=$PageNavigation;?></td>
		</tr>
	</table>
	<br>
	<?=$table_content;?>
</form>

<?
$linterface->LAYOUT_STOP();
?>