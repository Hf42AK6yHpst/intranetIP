<?php
// using : yat

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$linventory		= new libinventory();
$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "view.php", 1);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "../item/view.php", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

if(($targetBuilding != "")&&($targetFloor != "")&&($SubLocationID != ""))
{
//	$sql = "SELECT 
//				CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
//			FROM
//				INVENTORY_LOCATION_BUILDING AS building
//				INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
//				INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
//			WHERE 
//				building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
//				AND building.BuildingID = $targetBuilding
//				AND floor.LocationLevelID = $targetFloor
//				AND room.LocationID = $SubLocationID";
//	$arrLocationNameDisplay = $lebooking->returnVector($sql);
//	if(sizeof($arrLocationNameDisplay) > 0){
//		$LocationNameDisplay = $arrLocationNameDisplay[0];
//	}
	$LocationInfo = $lebooking->getAllBookableLocationName($SubLocationID);
	$Suffix = Get_Lang_Selection("CH","EN");
	$LocationNameDisplay = $LocationInfo[0]['BldgName'.$Suffix]." > ".$LocationInfo[0]['FloorName'.$Suffix]." > ".$LocationInfo[0]['RoomName'.$Suffix];

}

$table_content .= $lebooking_ui->getGeneralPermissionRoomDetailView_UI($targetBuilding,$targetFloor,$SubLocationID);

$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "javascript:js_Go_Back_To_Room_List($targetBuilding,$targetFloor,'')");
$PAGE_NAVIGATION[] = array($LocationNameDisplay, "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$linterface->LAYOUT_START($msg);
?>

<script language='javascript'>
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "targetBuilding";
	arrCookies[arrCookies.length] = "targetFloor";
	arrCookies[arrCookies.length] = "targetRoom";
	arrCookies[arrCookies.length] = "pageNo";
	arrCookies[arrCookies.length] = "numPerPage";
	
	$(document).ready( function() {	
	//	if(action != "") {
	//		Reload_Location_Selection("Floor",targetBuilding,targetFloor);
	//		Reload_Location_Selection("Room",targetFloor,targetRoom);
	//	} else {
	//		Reload_Location_Selection("Floor",targetBuilding,targetFloor);
	//		Reload_Location_Selection("Room",targetFloor,targetRoom);
	//	}
	//	AssignCookies();
	});
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				//alert($('#'+arrCookies[i]).val());
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				//alert($.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
	function js_Go_Back_To_Room_List(building_id,location_id,sub_location_id)
	{
		sub_location_id = sub_location_id || '';
		//window.location = 'view.php?action=ChangeView&targetBuilding='+building_id+'&targetFloor='+location_id+'&targetRoom='+sub_location_id;
		
		AssignCookies();
		SetCookies();
		
		window.location = 'view.php';
	}
	
	function js_Edit_Details(building_id,location_id,sub_location_id)
	{
		//window.location = 'view_details_edit.php?action=EditDetails&building_id='+building_id+'&location_id='+location_id+'&sub_location_id='+sub_location_id;
		
		AssignCookies();
		SetCookies();
		
		window.location = 'view_details_edit.php';
	}
	
</script>

<br>
<form id="form1" name="form1" method="post" onsubmit="return false">
	<table width="98%" border="0" cellpadding"0" cellspacing="0">
		<tr>
			<td><?=$PageNavigation;?></td>
		</tr>
	</table>
	<br>
	<?=$table_content;?>
</form>

<?
$linterface->LAYOUT_STOP();
?>