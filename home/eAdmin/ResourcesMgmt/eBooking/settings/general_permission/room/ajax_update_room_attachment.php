<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$Action = $_REQUEST['Action'];

if($Action=='DeleteRoomAttachment'){
	$RoomID = $_REQUEST['RoomID'];
	$result = $lebooking->Delete_Room_Attachment($RoomID);
	if($result)
		echo "1";
	else 
		echo "0";
}else if($Action=='UploadRoomAttachment'){
	$RoomID = stripslashes($_REQUEST['RoomID']);
	$lebooking->Delete_Room_Attachment($RoomID);
	$returnString = '<script type="text/javascript">'."\n";
	if($Attachment!=""){
		if($_FILES["Attachment"]["error"]>0){
			$returnString.= 'window.parent.Upload_Attachment_Feedback("'.$RoomID.'",0);';
		}else{
			$attachment_location = $_FILES["Attachment"]["tmp_name"];
			$attachment_name = stripslashes(trim($_FILES["Attachment"]["name"]));
			$success= $lebooking->Upload_Room_Attachment($RoomID,$attachment_location,$attachment_name);
			if($success)
				$returnString.= 'window.parent.Upload_Attachment_Feedback("'.$RoomID.'",1);';
			else 
				$returnString.= 'window.parent.Upload_Attachment_Feedback("'.$RoomID.'",0);';
		}
	}else{
		$returnString.= 'window.parent.Upload_Attachment_Feedback("'.$RoomID.'",0);';
	}
	$returnString.= '</script>'."\n";
	echo $returnString;
}

intranet_closedb();
?>