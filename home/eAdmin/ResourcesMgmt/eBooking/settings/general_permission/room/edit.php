<?php
// using : Cameron  
/********************************** Change Log *******************************************************
 * 
 * 2018-09-04 (Cameron): add function ApplyToSelected()
 * 
 * 2017-09-27 (Paul) : Fix the broken UI for PowerClass
 * 2011-03-16 (Carlos): Fix wrongly getting cookie/post values problem when switching View / Edit mode
 * 
 * Date	:	2011-07-12 (Marcus)
 * 				do not force client to select follow/approve group  
 *****************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("targetBuilding", "targetBuilding");
$arrCookies[] = array("targetFloor", "targetFloor");
$arrCookies[] = array("targetRoom", "targetRoom");
$arrCookies[] = array("pageNo_eBooking_Settings_GeneralPermission_Room_View", "pageNo_eBooking_Settings_GeneralPermission_Room_View");
$arrCookies[] = array("numPerPage_eBooking_Settings_GeneralPermission_Room_View", "numPerPage_eBooking_Settings_GeneralPermission_Room_View");

if(isset($clearCoo) && $clearCoo == 1)
{
	if(isset($_COOKIE['targetBuilding'])) {
		$_COOKIE['targetBuilding'] = "";
		$targetBuilding = "";
	}
	if(isset($_COOKIE['targetFloor'])) {
		$_COOKIE['targetFloor'] = "";
		$targetFloor = "";
	}
	if(isset($_COOKIE['targetRoom'])) {
		$_COOKIE['targetRoom'] = "";
		$targetRoom = "";
	}
	if(isset($_COOKIE['numPerPage_eBooking_Settings_GeneralPermission_Room_View'])) {
		$_COOKIE['numPerPage_eBooking_Settings_GeneralPermission_Room_View'] = "";
		$numPerPage = "";
	}
	if(isset($_COOKIE['pageNo_eBooking_Settings_GeneralPermission_Room_View'])) {
		$_COOKIE['pageNo_eBooking_Settings_GeneralPermission_Room_View'] = "";
		$pageNo = "";
	}
	if(isset($_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'])) {
		$_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'] = "";
		$pageNo = "";
	}
}
else
{
	// for click 'Next page' & 'Prev page' use
	if(isset($_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'])) {
		$pageNo_eBooking_Settings_GeneralPermission_Room_View = $_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'];
		$pageNo = $_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'];
	} else {
		$pageNo_eBooking_Settings_GeneralPermission_Room_View = $_COOKIE['pageNo_eBooking_Settings_GeneralPermission_Room_View'];
		$pageNo = $_COOKIE['pageNo_eBooking_Settings_GeneralPermission_Room_View'];
	}

	if(isset($_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View'])) {
		$numPerPage_eBooking_Settings_GeneralPermission_Room_View = $_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View'];
		$numPerPage = $_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View'];
	} else {
		$numPerPage_eBooking_Settings_GeneralPermission_Room_View = $_COOKIE['numPerPage_eBooking_Settings_GeneralPermission_Room_View'];
		$numPerPage = $_COOKIE['numPerPage_eBooking_Settings_GeneralPermission_Room_View'];
	}
		
	if(isset($_COOKIE['targetRoom']))
	{
		if($_COOKIE['targetRoom'] === "undefined")
		{
			$targetRoom = "";
		}
	}
	
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$linventory		= new libinventory();
$lebooking->Check_Page_Permission();

$targetBuilding = (isset($_POST['targetBuilding'])/* && $_POST['targetBuilding'] != ""*/) ? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor'])/* && $_POST['targetFloor'] != ""*/) ? $_POST['targetFloor'] : $targetFloor;
$targetRoom		= (isset($_POST['targetRoom'])/* && $_POST['targetRoom'] != ""*/) ? $_POST['targetRoom'] : $targetRoom;
$action			= (isset($action) && $action != "") ? $action : "";

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "view.php?clearCoo=1", 1);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "../item/view.php?clearCoo=1", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arrBuilding = $lebooking->Get_All_Allow_Booking_Building_Array();
$building_selection = getSelectByArray($arrBuilding, " name='targetBuilding' id='targetBuilding' onChange='javascript: Reload_Location_Selection(\"Floor\",this.value)' DISABLED",$targetBuilding,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Building']." -- ");
$building_filter .= "<div id='BuildingDiv' style='display: inline;'>".$building_selection."</div>";

$floor_filter .= "<div id='FloorDiv' style='display: inline;'>".$floor_selection."</div>";

$room_filter .= "<div id='RoomDiv' style='display: inline;'>".$room_selection."</div>";

$ModeArr = '';
$ModeArr[] = array($Lang['Btn']['View'], "icon_view.gif", "javascript:Go_View_Mode();", 0);
$ModeArr[] = array($Lang['Btn']['Edit'], "icon_edit_b.gif", "", 1);
$mode_toolbar = $lebooking_ui->Get_Mode_Toolbar($ModeArr);

$toolbar .= "<table width='96%' border='0' cellpadding='3' cellspacing='0'>";
$toolbar .= "<tr>";
$toolbar .= "<td align='left'>".$building_filter.$floor_filter.$room_filter."</td>";
$toolbar .= "</tr>";
$toolbar .= "</table>";

$table_content .= $lebooking_ui->initJavaScript();
$table_content .= $lebooking_ui->getRoomPermissionEditTable($targetBuilding,$targetFloor,$targetRoom,$pageNo,$numPerPage);
?>

<script language="javascript">
	
	var targetBuilding = "<?=$targetBuilding;?>";
	var targetFloor = "<?=$targetFloor;?>";
	var targetRoom = "<?=$targetRoom;?>";
	var action = "<?=$action;?>";
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "targetBuilding";
	arrCookies[arrCookies.length] = "targetFloor";
	arrCookies[arrCookies.length] = "targetRoom";
	arrCookies[arrCookies.length] = "pageNo_eBooking_Settings_GeneralPermission_Room_View";
	arrCookies[arrCookies.length] = "numPerPage_eBooking_Settings_GeneralPermission_Room_View";
	
	$(document).ready( function() {	
		SetCookies();
		if(action != "") {
			Reload_Location_Selection("Floor",targetBuilding,targetFloor);
			Reload_Location_Selection("Room",targetFloor,targetRoom);
		}else{
			Reload_Location_Selection("Floor",targetBuilding,targetFloor);
			Reload_Location_Selection("Room",targetFloor,targetRoom);
		}
	});
	
	function ShowHideLayers(div_name,act)
	{
		if(act == "show"){
			$("#"+div_name).show();
		}else{
			$("#"+div_name).hide();
		}
	}
	
	function js_Show_Detail_Layer(jsDetailType, jsSubLocationID, jsClickedObjID)
	{
		js_Hide_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'ShowAllAvaliableBooking'){
			jsAction = 'ShowEdit_AllRoomAvaliableBooking';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowAllRoomBookingRule'){
			jsAction = 'ShowEdit_AllRoomBookingRule';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowAllManagementGroup'){
			jsAction = 'ShowEdit_AllManagementGroup';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowAllFollowUpGroup'){
			jsAction = 'ShowEdit_AllFollowUpGroup';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowAllUserBookingRule'){
			jsAction = 'ShowEdit_AllUserBookingRule';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowAllUserBookingRule'){
			jsAction = 'ShowEdit_AllUserBookingRule';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowAvaliableBooking'){
			jsAction = 'ShowEdit_AvaliableBooking';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowDayBeforeBooking'){
			jsAction = 'ShowEdit_DayBeforeBooking';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}	
		else if(jsDetailType == 'ShowFacilities'){
			jsAction = 'ShowEdit_Facilities';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowManagmentGroup'){
			jsAction = 'ShowEdit_ManagmentGroup';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}		
		else if(jsDetailType == 'ShowFollowUpGroup'){
			jsAction = 'ShowEdit_FollowUpGroup';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}	
		else if(jsDetailType == 'ShowUserBookingRule'){
			jsAction = 'ShowEdit_UserBookingRule';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}	
		else if(jsDetailType == 'ShowLocationDescription'){
			jsAction = 'ShowLocationDescription';
			jsOffsetLeft = 0;
			jsOffsetTop = -10;
		}else if(jsDetailType == 'ShowRoomAttachment'){
			jsAction = 'ShowRoomAttachment';
			jsOffsetLeft = 0;
			jsOffsetTop = -25;
		}
		
		$('div#DetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>');
		
		js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop);
		MM_showHideLayers('DetailsLayer','','show');
				
		$('div#DetailsLayerContentDiv').load(
			"ajax_reload.php", 
			{ 
				Action: jsAction,
				SubLocationID: jsSubLocationID
			},
			function(returnString)
			{
				$('div#DetailsLayerContentDiv').css('z-index', '999');
				$('div#DetailsLayerContentDiv').css('overflow', 'auto');
				
				if($('div#DetailsLayerContentDiv').height() > 400)
				{
					$('div#DetailsLayerContentDiv').height(400);
				}
				else
				{
					$('div#DetailsLayerContentDiv').css('height', 'auto');
				}
			}
		);
	}
	
	function js_Hide_Detail_Layer()
	{
		MM_showHideLayers('DetailsLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop){
		var ClickedLeft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft');
		<?php
		if($sys_custom['PowerClass']){
		?>
			var posleft = ClickedLeft-400;
		<?php
		}else{
		?>
			var posleft = (ClickedLeft+400)>$(window).width()? ($(window).width()-400) : ClickedLeft;
		<?php	
		}	
		?>
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
		
		document.getElementById('DetailsLayer').style.left = posleft + "px";
		document.getElementById('DetailsLayer').style.top = postop + "px";
		document.getElementById('DetailsLayer').style.visibility = 'visible';
	}
	
	function Go_View_Mode()
	{
		$('#targetBuilding').attr('disabled',false);
		$('#targetFloor').attr('disabled',false);
		$('#targetRoom').attr('disabled',false);
		
		var targetBuilding = $('#targetBuilding').val();
		var targetFloor = $('#targetFloor').val();
		
		if($('#targetFloor').length != 0)
			var targetFloor = $('#targetFloor').val();
		else
			var targetFloor = "#";
		
		if($('#targetRoom').length != 0)
			var targetRoom = $('#targetRoom').val();
		else
			var targetRoom = "#";
		
		// Set the Cookies
		AssignCookies();
		
		document.form1.action = "view.php";
		document.form1.submit();
		//window.location = 'view.php';
	}
	
	function Reload_Location_Selection(ModuleName,val,selected_val)
	{
		selected_val = selected_val || '';
		
		// Set Cookies
		SetCookies();
		AssignCookies();
		
		Block_Document();
		
		if(ModuleName == "Floor")
		{
			$.post(
				"ajax_load_location_selection.php",
				{
					"ModuleName" : ModuleName,
					"targetBuilding" : val,
					"targetFloor" : selected_val,
					"disabled" : 1
				},
				function(responseText){
					$("#FloorDiv").html(responseText);
					UnBlock_Document();
					initThickBox();
				}
			);
		}
		else if(ModuleName == "Room")
		{
			if($('#targetRoom').length != 0)
				$('#targetRoom').val('');
			var targetBuilding = $('#targetBuilding').val();
			$.post(
				"ajax_load_location_selection.php",
				{
					"ModuleName" : ModuleName,
					"targetBuilding" : targetBuilding,
					"targetFloor" : val,
					"targetRoom" : selected_val,
					"disabled" : 1
				},
				function(responseText){
					$("#RoomDiv").html(responseText);
					UnBlock_Document();
					initThickBox();
				}
			);
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				//alert($('#'+arrCookies[i]).val());
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				//alert($.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}

	// AJAX function - reload the Room Permission Edit View Table
	function reloadMainContent(targetBuilding, targetFloor, targetRoom, pageNo, numPerPage, Order, SortField)
	{
		targetBuilding = targetBuilding || $('#targetBuilding').val();
		targetFloor = targetFloor || $('#targetFloor').val();
		
		if($('#pageNo_eBooking_Settings_GeneralPermission_Room_View').length)
			pageNo = pageNo || $('#pageNo_eBooking_Settings_GeneralPermission_Room_View').val();
		else
			pageNo = 1; 
			
		if($('#numPerPage_eBooking_Settings_GeneralPermission_Room_View').length)	
			numPerPage = numPerPage || $('#numPerPage_eBooking_Settings_GeneralPermission_Room_View').val();
		else
			numPerPage = 20;
		
		if($('#targetRoom').length == 0) {
			targetRoom = targetRoom || '';
		} else {
			targetRoom = targetRoom || $('#targetRoom').val();
		}
		
		AssignCookies();
		SetCookies();
		
		if(targetBuilding == ""){
			$.cookies.del("targetFloor");
			$.cookies.del("targetRoom");
		}
		
		Block_Document();
		$.post(
			"ajax_load_room_permission_edit_view.php",
			{
				"targetBuilding":targetBuilding,
				"targetFloor":targetFloor,
				"targetRoom":targetRoom,
				"pageNo":pageNo, 
				"numPerPage":numPerPage
			},
			function(responseText){
				$('#room_permission_edit_view').html(responseText);
				UnBlock_Document();
				initThickBox();
			}
		);
	}
	
	function ApplyToAll(div_name)
	{
		
		if(div_name == "All_AvaliableBooking")
		{
			if(! $('input[name=All_AvaliableBooking]').is(':checked') ) {
				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAllowBooking'];?>");
				return false;
			}
			
			$.post(
				"ajax_update_all_avaliable_booking.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : $('#targetRoom').val(),
					"AvaliableForBooking" : $('input[name=All_AvaliableBooking]:checked').val(),
					"displayedSubLocationIdList" : $('input#displayedSubLocationIdList').val()
				},
				function(responseText){
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		} 
		else if (div_name == "AllRoomBookingRule")
		{
			if(!check_positive_int_30(document.getElementById("AllBookingDay"),"<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputPositiveInterger']?>","","","WarnAllBookingDay"))
			return false;
			
			$.post(
				"ajax_update_all_room_booking_rule.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoom" : $('#targetRoom').val(),
					"DayBeforeBooking" : $('#AllBookingDay').val(),
					"displayedSubLocationIdList" : $('input#displayedSubLocationIdList').val()
				},
				function(responseText){ 
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		}
		else if (div_name == "AllManagementGroup")
		{
			var check_cnt = 0;
			var targetManagementGroup = "";
			
			for(var i=0; i<document.getElementsByName("AllManagementGroup[]").length; i++)
			{
				if(document.getElementsByName("AllManagementGroup[]")[i].checked)
				{
					check_cnt++;
					
					if((targetManagementGroup == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetManagementGroup = targetManagementGroup+delimiter+document.getElementsByName("AllManagementGroup[]")[i].value;
				}
			}
			
//			if(check_cnt>0)
//			{
				$.post(
					"ajax_update_all_management_group.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : $('#targetRoom').val(),
						"targetManagementGroup" : targetManagementGroup,
						"displayedSubLocationIdList" : $('input#displayedSubLocationIdList').val()
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}	
					}
				);
//			}
		}
		else if (div_name == "AllFollowUpGroup")
		{
			var check_cnt = 0;
			var targetFollowUpGroup = "";
			
			for(var i=0; i<document.getElementsByName("AllFollowUpGroup[]").length; i++)
			{
				if(document.getElementsByName("AllFollowUpGroup[]")[i].checked)
				{
					check_cnt++;
					
					if((targetFollowUpGroup == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetFollowUpGroup = targetFollowUpGroup+delimiter+document.getElementsByName("AllFollowUpGroup[]")[i].value;
				}
			}
			
//			if(check_cnt>0)
//			{
				$.post(
					"ajax_update_all_follow_up_group.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : $('#targetRoom').val(),
						"targetFollowUpGroup" : targetFollowUpGroup,
						"displayedSubLocationIdList" : $('input#displayedSubLocationIdList').val()
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}	
					}
				);
//			}
		}
		else if (div_name == "AllUserBookingRule")
		{
			var check_cnt = 0;
			var targetUserBookingRule = "";
			
			for(var i=0; i<document.getElementsByName("AllUserBookingRule[]").length; i++)
			{
				if(document.getElementsByName("AllUserBookingRule[]")[i].checked)
				{
					check_cnt++;
					
					if((targetUserBookingRule == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetUserBookingRule = targetUserBookingRule+delimiter+document.getElementsByName("AllUserBookingRule[]")[i].value;
				}
			}
			
			if(check_cnt>0)
			{
				$.post(
					"ajax_update_all_user_booking_rule.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoom" : $('#targetRoom').val(),
						"targetUserBookingRule" : targetUserBookingRule,
						"displayedSubLocationIdList" : $('input#displayedSubLocationIdList').val()
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
					}
				);
			}
		}
		
//		if(!Invalid)
			js_Hide_Detail_Layer();
	}

	function ApplyToSelected(div_name)
	{
		var _locationID;
		var selectedLocationID = '';
		
		$('input.locationChk:checked').each(function(){
			_locationID = $(this).attr('id').replace('locationChk_', '');
			if (selectedLocationID != '') {
				selectedLocationID += ',' + _locationID;
			}
			else {
				selectedLocationID = _locationID;
			}
		});

		if (selectedLocationID == '') {
			alert("<?php echo $Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneLocation'];?>");
			return false;
		}
		
		if(div_name == "SelectedAvaliableBooking")
		{
			if(! $('input[name=All_AvaliableBooking]').is(':checked') ) {
				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAllowBooking'];?>");
				return false;
			}
			
			$.post(
				"ajax_update_all_avaliable_booking.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : $('#targetRoom').val(),
					"AvaliableForBooking" : $('input[name=All_AvaliableBooking]:checked').val(),
					"displayedSubLocationIdList" : selectedLocationID
				},
				function(responseText){
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		} 
		else if (div_name == "SelectedRoomBookingRule")
		{
			if(!check_positive_int_30(document.getElementById("AllBookingDay"),"<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputPositiveInterger']?>","","","WarnAllBookingDay"))
			return false;
			
			$.post(
				"ajax_update_all_room_booking_rule.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoom" : $('#targetRoom').val(),
					"DayBeforeBooking" : $('#AllBookingDay').val(),
					"displayedSubLocationIdList" : selectedLocationID
				},
				function(responseText){ 
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		}
		else if (div_name == "SelectedManagementGroup")
		{
			var check_cnt = 0;
			var targetManagementGroup = "";
			
			for(var i=0; i<document.getElementsByName("AllManagementGroup[]").length; i++)
			{
				if(document.getElementsByName("AllManagementGroup[]")[i].checked)
				{
					check_cnt++;
					
					if((targetManagementGroup == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetManagementGroup = targetManagementGroup+delimiter+document.getElementsByName("AllManagementGroup[]")[i].value;
				}
			}
			
//			if(check_cnt>0)
//			{
				$.post(
					"ajax_update_all_management_group.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : $('#targetRoom').val(),
						"targetManagementGroup" : targetManagementGroup,
						"displayedSubLocationIdList" : selectedLocationID
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}	
					}
				);
//			}
		}
		else if (div_name == "SelectedFollowUpGroup")
		{
			var check_cnt = 0;
			var targetFollowUpGroup = "";
			
			for(var i=0; i<document.getElementsByName("AllFollowUpGroup[]").length; i++)
			{
				if(document.getElementsByName("AllFollowUpGroup[]")[i].checked)
				{
					check_cnt++;
					
					if((targetFollowUpGroup == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetFollowUpGroup = targetFollowUpGroup+delimiter+document.getElementsByName("AllFollowUpGroup[]")[i].value;
				}
			}
			
//			if(check_cnt>0)
//			{
				$.post(
					"ajax_update_all_follow_up_group.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : $('#targetRoom').val(),
						"targetFollowUpGroup" : targetFollowUpGroup,
						"displayedSubLocationIdList" : selectedLocationID
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}	
					}
				);
//			}
		}
		else if (div_name == "SelectedUserBookingRule")
		{
			var check_cnt = 0;
			var targetUserBookingRule = "";
			
			for(var i=0; i<document.getElementsByName("AllUserBookingRule[]").length; i++)
			{
				if(document.getElementsByName("AllUserBookingRule[]")[i].checked)
				{
					check_cnt++;
					
					if((targetUserBookingRule == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetUserBookingRule = targetUserBookingRule+delimiter+document.getElementsByName("AllUserBookingRule[]")[i].value;
				}
			}
			
			if(check_cnt>0)
			{
				$.post(
					"ajax_update_all_user_booking_rule.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoom" : $('#targetRoom').val(),
						"targetUserBookingRule" : targetUserBookingRule,
						"displayedSubLocationIdList" : selectedLocationID
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
					}
				);
			}
		}
		
//		if(!Invalid)
			js_Hide_Detail_Layer();
	}
	
	function ApplyToSelectedLocation(ModuleName,targetLocation)
	{
		if(ModuleName == 'AvaliableBooking')
		{		
			if(! $('input[name=AvaliableBooking_'+targetLocation+']').is(':checked') ) {
				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAllowBooking'];?>");
				return false;
			}
			
			$.post(
				"ajax_update_all_avaliable_booking.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : targetLocation,
					"AvaliableForBooking" : $('input[name=AvaliableBooking_'+targetLocation+']:checked').val()
				},
				function(responseText){
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		}
		else if(ModuleName == 'DayBeforeBooking')
		{
			if(!check_positive_int_30(document.getElementById("BookingDay_"+targetLocation),"<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputPositiveInterger']?>","","","WarnBookingDay_"+targetLocation))
				return false;
			
			$.post(
				"ajax_update_all_room_booking_rule.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : targetLocation,
					"DayBeforeBooking" : $("#BookingDay_"+targetLocation).val()
				},
				function(responseText) { 
					if(responseText == 0) {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					} else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		}
		else if(ModuleName == 'Facilities')
		{
			var targetBulkIncludeWhenBooking = "";
			var targetBulkShowForReference = "";
			var targetSingleIncludeWhenBooking = "";
			var targetSingleShowForReference = "";
			var SingleItemID = "";
			var BulkItemID = "";
			
			for(var i=0; i<document.getElementsByName("SingleItemID[]").length; i++)
			{
				if((SingleItemID == "")){
					var delimiter = "";
				}else{
					var delimiter = ",";
				}
				SingleItemID = SingleItemID+delimiter+document.getElementsByName("SingleItemID[]")[i].value;
			}
			
			for(var i=0; i<document.getElementsByName("BulkItemID[]").length; i++)
			{
				if((BulkItemID == "")){
					var delimiter = "";
				}else{
					var delimiter = ",";
				}
				BulkItemID = BulkItemID+delimiter+document.getElementsByName("BulkItemID[]")[i].value;
			}
			
			for(var i=0; i<document.getElementsByName("Bulk_IncludeWhenBooking[]").length; i++) {
				if(document.getElementsByName("Bulk_IncludeWhenBooking[]")[i].checked == true) {
					if((targetBulkIncludeWhenBooking == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetBulkIncludeWhenBooking = targetBulkIncludeWhenBooking+delimiter+document.getElementsByName("Bulk_IncludeWhenBooking[]")[i].value;
				}
			}
			
			for(var i=0; i<document.getElementsByName("Bulk_ShowForReference[]").length; i++) {
				if(document.getElementsByName("Bulk_ShowForReference[]")[i].checked == true) {
					if((targetBulkShowForReference == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetBulkShowForReference = targetBulkShowForReference+delimiter+document.getElementsByName("Bulk_ShowForReference[]")[i].value;
				}
			}
			
			for(var i=0; i<document.getElementsByName("Single_IncludeWhenBooking[]").length; i++) {
				if(document.getElementsByName("Single_IncludeWhenBooking[]")[i].checked == true) {
					if((targetSingleIncludeWhenBooking == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetSingleIncludeWhenBooking = targetSingleIncludeWhenBooking+delimiter+document.getElementsByName("Single_IncludeWhenBooking[]")[i].value;
				}
			}
			
			for(var i=0; i<document.getElementsByName("Single_ShowForReference[]").length; i++) {
				if(document.getElementsByName("Single_ShowForReference[]")[i].checked == true) {
					if((targetSingleShowForReference == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetSingleShowForReference = targetSingleShowForReference+delimiter+document.getElementsByName("Single_ShowForReference[]")[i].value;
				}
			}
						
			$.post(
				"ajax_update_all_facilities.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : targetLocation,
					"SingleItemID" : SingleItemID,
					"BulkItemID" : BulkItemID,
					"targetBulkIncludeWhenBooking" : targetBulkIncludeWhenBooking,
					"targetBulkShowForReference" : targetBulkShowForReference,
					"targetSingleIncludeWhenBooking" : targetSingleIncludeWhenBooking,
					"targetSingleShowForReference" : targetSingleShowForReference
				},
				function(responseText){
					if(responseText == 0)
					{
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
					else
					{
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}	
				}
			);
		}
		else if(ModuleName == 'ManagmentGroup')
		{
			var check_cnt = 0;
			var targetManagementGroup = "";
			
			for(var i=0; i<document.getElementsByName("TargetManagementGroup"+targetLocation+"[]").length; i++)
			{
				if(document.getElementsByName("TargetManagementGroup"+targetLocation+"[]")[i].checked)
				{
					check_cnt++;
					
					if((targetManagementGroup == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetManagementGroup = targetManagementGroup+delimiter+document.getElementsByName("TargetManagementGroup"+targetLocation+"[]")[i].value;
				}
			}
			
//			if(check_cnt>0)
//			{
				$.post(
					"ajax_update_all_management_group.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : targetLocation,
						"targetManagementGroup" : targetManagementGroup 
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}	
					}
				);
//			}
//			else
//			{
//				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//				return false;
//			}
		}
		else if(ModuleName == 'FollowUpGroup')
		{
			var check_cnt = 0;
			var targetFollowUpGroup = "";
			
			for(var i=0; i<document.getElementsByName("TargetFollowUpGroup"+targetLocation+"[]").length; i++)
			{
				if(document.getElementsByName("TargetFollowUpGroup"+targetLocation+"[]")[i].checked)
				{
					check_cnt++;
					
					if((targetFollowUpGroup == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}
					targetFollowUpGroup = targetFollowUpGroup+delimiter+document.getElementsByName("TargetFollowUpGroup"+targetLocation+"[]")[i].value;
				}
			}
			
//			if(check_cnt>0)
//			{
				$.post(
					"ajax_update_all_follow_up_group.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : targetLocation,
						"targetFollowUpGroup" : targetFollowUpGroup 
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}	
					}
				);
//			}
//			else
//			{
//				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//				return false;
//			}
		}
		else if(ModuleName == 'UserBookingRule')
		{
			var check_cnt = 0;
			var targetUserBookingRule = "";
						
			for(var i=0; i<document.getElementsByName("TargetUserBookingRule_"+targetLocation+"[]").length; i++)
			{
				if(document.getElementsByName("TargetUserBookingRule_"+targetLocation+"[]")[i].checked)
				{
					check_cnt++;
					
					if((targetUserBookingRule == "")){
						var delimiter = "";
					}else{
						var delimiter = ",";
					}

					targetUserBookingRule = targetUserBookingRule+delimiter+document.getElementsByName("TargetUserBookingRule_"+targetLocation+"[]")[i].value;
				}
			}
			if(check_cnt>0)
			{
				$.post(
					"ajax_update_all_user_booking_rule.php",
					{
						"targetBuilding" : $('#targetBuilding').val(),
						"targetFloor" : $('#targetFloor').val(),
						"targetRoomID" : targetLocation,
						"targetUserBookingRule" : targetUserBookingRule 
					},
					function(responseText){
						if(responseText == 0)
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
						else
						{
							msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
							Get_Return_Message(msg);
							reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
						}
					}
				);
			}
			else
			{
				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule']?>");
				return false;
			}
		}
		js_Hide_Detail_Layer();
	}
	
	function js_Reload_Room_Attachment_Table(jsRoomID)
	{
		$('div#DetailsLayerContentDiv').html('');
		Block_Element('DetailsLayerContentDiv');
		
		$('div#DetailsLayerContentDiv').load(
			"ajax_reload.php", 
			{ 
				"Action": 'ShowRoomAttachment',
				"SubLocationID": jsRoomID
			},
			function(ReturnData)
			{
				UnBlock_Element('DetailsLayerContentDiv');
			}
		);
	}
	
	function js_Delete_Room_Attachment(jsRoomID)
	{
		if(confirm('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteAttachment']?>')){
			$.post(
				"ajax_update_room_attachment.php",
				{
					"Action":"DeleteRoomAttachment",
					"RoomID":jsRoomID
				},
				function(responseText){
					if(responseText == '1'){
						jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentSuccess']?>';
					}else{
						jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentFailed']?>';
					}
					Get_Return_Message(jsReturnMsg);
					js_Reload_Room_Attachment_Table(jsRoomID);
				}
			);
		}
	}
	
	function js_Upload_Room_Attachment(jsRoomID)
	{
		var attachment = $.trim($('input#Attachment').val());
		if(attachment != ''){
			Block_Element('DetailsLayerContentDiv');
			var Obj = document.getElementById("FormAttachment");
			Obj.target = 'FileUploadIframe'; 
			Obj.encoding = "multipart/form-data";
			Obj.method = 'post';
			Obj.action = "ajax_update_room_attachment.php?Action=UploadRoomAttachment&RoomID="+jsRoomID;
			Obj.submit();
		}else{
			alert('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAttachment']?>');
		}
	}
	
	function Upload_Attachment_Feedback(jsRoomID,response)
	{
		var jsReturnMsg = '';
		if(response==1)
			jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['UploadItemAttachmentSuccess']?>';
		else
			jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['UploadItemAttachmentFailed']?>';
		
		Get_Return_Message(jsReturnMsg);
		js_Reload_Room_Attachment_Table(jsRoomID);
	}
	
	function js_Apply_To_All(jsType)
	{
		if (jsType == 'BookingDayBeforehand')
		{
			jsTargetValue = $('input#BookingDayBeforehand_All').val();
			
			$('input.BookingDayBeforehandTb').each( function() {
				$(this).val(jsTargetValue);
			});
		}
	}
</script>

<form name="form1" action="edit.php" method="POST" >
	<br>
	<?=$toolbar;?>
	<div id="room_permission_edit_view">
		<?=$table_content;?>
	</div>
	
	<div id="DetailsLayer" class="selectbox_layer" style="visibility:hidden; width:400px; overflow:auto;">
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				<tr>
					<td align="left" style="border-bottom: medium none;">
						<div id="DetailsLayerContentDiv"></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
?>