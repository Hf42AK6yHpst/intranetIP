<?php
/*
 *  Using:
 *
 *  2019-10-21 Cameron
 *      - fix sql syntax error for bulk items
 *
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

$lebooking = new libebooking();

intranet_auth();
intranet_opendb();

$lebooking->Start_Trans();

$arrBulkIncludeWhenBooking = explode(",",$targetBulkIncludeWhenBooking);
$arrBulkShowForReference = explode(",",$targetBulkShowForReference);
$arrSingleIncludeWhenBooking = explode(",",$targetSingleIncludeWhenBooking);
$arrSingleShowForReference = explode(",",$targetSingleShowForReference);

if($SingleItemID != "")
{
	$arrSingleItem = explode(",",$SingleItemID);
	
	for($i=0; $i<sizeof($arrSingleItem); $i++)
	{
		$sql = "SELECT count(*) FROM INTRANET_EBOOKING_ITEM WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";

		$arrSingleItemExist = $lebooking->returnVector($sql);
		if($arrSingleItemExist[0] > 0) {
			if(sizeof($arrSingleIncludeWhenBooking) > 0) {
				if(in_array($arrSingleItem[$i],$arrSingleIncludeWhenBooking)) {
					$sql = "UPDATE INTRANET_EBOOKING_ITEM SET IncludedWhenBooking = 1 WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";
					$result[] = $lebooking->db_db_query($sql);
				} else {
					$sql = "UPDATE INTRANET_EBOOKING_ITEM SET IncludedWhenBooking = 0 WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";
					$result[] = $lebooking->db_db_query($sql);
				}
			} else {
				$sql = "UPDATE INTRANET_EBOOKING_ITEM SET IncludedWhenBooking = 0 WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";
				$result[] = $lebooking->db_db_query($sql);
			}

			if(sizeof($arrSingleShowForReference) > 0) {
				if(in_array($arrSingleItem[$i],$arrSingleShowForReference)) {
					$sql = "UPDATE INTRANET_EBOOKING_ITEM SET ShowForReference = 1 WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";
					$result[] = $lebooking->db_db_query($sql);
				} else {
					$sql = "UPDATE INTRANET_EBOOKING_ITEM SET ShowForReference = 0 WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";
					$result[] = $lebooking->db_db_query($sql);
				}
			} else {
				$sql = "UPDATE INTRANET_EBOOKING_ITEM SET ShowForReference = 0 WHERE LocationID = '$targetRoomID' AND ItemID = '".$arrSingleItem[$i]."'";
				$result[] = $lebooking->db_db_query($sql);
			}

		}
	}
}

if($BulkItemID != "")
{
	$BulkItemID = explode(",",$BulkItemID);
	
	for($i=0; $i<sizeof($BulkItemID); $i++)
	{
		$sql = "SELECT count(*) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$BulkItemID[$i]."' AND LocationID = '$targetRoomID'";
		$arrBulkItemExist = $lebooking->returnVector($sql);

		if($arrBulkItemExist[0] > 0) {
			if(sizeof($arrBulkIncludeWhenBooking) > 0) {
				if(in_array($BulkItemID[$i],$arrBulkIncludeWhenBooking)) {
					$sql = "INSERT INTO INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (LocationID, BulkItemID, IncludedWhenBooking) VALUES ('$targetRoomID', '".$BulkItemID[$i]."', 1) ON DUPLICATE KEY UPDATE  IncludedWhenBooking = 1";
					$result[] = $lebooking->db_db_query($sql);
				} else {
					$sql = "INSERT INTO INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (LocationID, BulkItemID, IncludedWhenBooking) VALUES ('$targetRoomID', '".$BulkItemID[$i]."', 0) ON DUPLICATE KEY UPDATE  IncludedWhenBooking = 0";
					$result[] = $lebooking->db_db_query($sql);
				}
			} else {
				$sql = "INSERT INTO INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (LocationID, BulkItemID, IncludedWhenBooking) VALUES ('$targetRoomID', '".$BulkItemID[$i]."', 0) ON DUPLICATE KEY UPDATE  IncludedWhenBooking = 0";
				$result[] = $lebooking->db_db_query($sql);
			}
			
			if(sizeof($arrBulkShowForReference) > 0) {
				if(in_array($BulkItemID[$i],$arrBulkShowForReference)) {
					$sql = "INSERT INTO INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (LocationID, BulkItemID, ShowForReference) VALUES ('$targetRoomID', '".$BulkItemID[$i]."', 1) ON DUPLICATE KEY UPDATE  ShowForReference = 1";
					$result[] = $lebooking->db_db_query($sql);
				} else {
					$sql = "INSERT INTO INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (LocationID, BulkItemID, ShowForReference) VALUES ('$targetRoomID', '".$BulkItemID[$i]."', 0) ON DUPLICATE KEY UPDATE  ShowForReference = 0";
					$result[] = $lebooking->db_db_query($sql);
				}
			} else {
				$sql = "INSERT INTO INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (LocationID, BulkItemID, ShowForReference) VALUES ('$targetRoomID', '".$BulkItemID[$i]."', 0) ON DUPLICATE KEY UPDATE  ShowForReference = 0";
				$result[] = $lebooking->db_db_query($sql);
			}

		}
	}
}

if(sizeof($result)>0)
{
	if(!in_array(false,$result)) {
		$lebooking->Commit_Trans();
		echo 1;
	} else {
		$lebooking->RollBack_Trans();
		echo 0;
	}
}
else
{
	$lebooking->Commit_Trans();
	echo 1;
}

intranet_closedb();
?>