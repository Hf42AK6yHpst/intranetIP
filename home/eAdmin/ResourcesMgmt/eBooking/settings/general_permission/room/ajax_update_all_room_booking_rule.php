<?php
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *      - apply IntegerSafe() to related variables
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

$lebooking = new libebooking();

intranet_auth();
intranet_opendb();

$lebooking->Start_Trans();

$targetBuilding = (isset($_POST['targetBuilding'])/* && $_POST['targetBuilding'] != ""*/) ? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor'])/* && $_POST['targetFloor'] != ""*/) ? $_POST['targetFloor'] : $targetFloor;
$targetRoom		= (isset($_POST['targetRoom'])/* && $_POST['targetRoom'] != ""*/) ? $_POST['targetRoom'] : $targetRoom;
$displayedSubLocationIdList		= (isset($_POST['displayedSubLocationIdList'])/* && $_POST['displayedSubLocationIdList'] != ""*/) ? $_POST['displayedSubLocationIdList'] : $displayedSubLocationIdList;
$targetBuilding = IntegerSafe($targetBuilding);
$targetFloor = IntegerSafe($targetFloor);
$targetRoom = IntegerSafe($targetRoom);
$displayedSubLocationIdList = IntegerSafe($displayedSubLocationIdList);

if($targetRoomID == "")
{
	$displayedSubLocationIdAry = explode(',', $displayedSubLocationIdList);
	$sql = "SELECT 
				Room.LocationID 
			FROM 
				INVENTORY_LOCATION_BUILDING AS Building 
				INNER JOIN INVENTORY_LOCATION_LEVEL AS Floor ON (Building.BuildingID = Floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION AS Room ON (Floor.LocationLevelID = Room.LocationLevelID)
			WHERE
				Building.RecordStatus = 1 AND Floor.RecordStatus = 1 AND Room.RecordStatus = 1 
				AND	Building.BuildingID = '$targetBuilding' AND Floor.LocationLevelID = '$targetFloor' AND Room.LocationID IN ('".implode("','", (array)$displayedSubLocationIdAry)."')";
	$arrLocation = $lebooking->returnVector($sql);
}
else
{
	$arrLocation = explode(",",$targetRoomID);
}

//$result["UpdateRoomBookingRule"] = array();
//$result["InsertRoomBookingRule"] = array();
//$result["InsertRoomBookingRuleRelation"] = array();
$result = array();

for($i=0; $i<sizeof($arrLocation); $i++)
{
	$result[] = $lebooking->Insert_Update_Location_Booking_Rule($arrLocation[$i], $DayBeforeBooking);
	
//	$sql = "SELECT BookingRuleID FROM INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION WHERE LocationID = $arrLocation[$i]";
//	$arrExistRoomBookingRuleID = $lebooking->returnVector($sql);
//	
//	if(sizeof($arrExistRoomBookingRuleID)>0) {
//		$sql = "UPDATE 
//					INTRANET_EBOOKING_LOCATION_BOOKING_RULE 
//				SET 
//					DaysBeforeUse = $DayBeforeBooking, 
//					ModifiedBy = $UserID, 
//					DateModified = NOW() 
//				WHERE 
//					RuleID = $arrExistRoomBookingRuleID[0]";
//		$result["UpdateRoomBookingRule"][] = $lebooking->db_db_query($sql);
//	} else {
//		$sql = "INSERT INTO 
//					INTRANET_EBOOKING_LOCATION_BOOKING_RULE 
//					(DaysBeforeUse, InputBy, ModifiedBy, DateInput, DateModified)
//				VALUES
//					($DayBeforeBooking, $UserID, $UserID, NOW(), NOW())";
//		$result["InsertRoomBookingRule"][] = $lebooking->db_db_query($sql);
//		
//		$RoomBookingRuleID = $lebooking->db_insert_id();
//		
//		$sql = "INSERT INTO 
//					INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION
//					(LocationID, BookingRuleID, InputBy, ModifiedBy, DateInput, DateModified)
//				VALUES
//					($arrLocation[$i], $RoomBookingRuleID, $UserID, $UserID, NOW(), NOW())";
//		$result["InsertRoomBookingRuleRelation"][] = $lebooking->db_db_query($sql);
//	}
}

//if(sizeof($result["UpdateRoomBookingRule"])>0){
//	if(!in_array(false,$result["UpdateRoomBookingRule"]))
//		$cnt1 = 1;
//}else{
//	$cnt1 = 1;
//}
//
//if(sizeof($result["InsertRoomBookingRule"])>0){
//	if(!in_array(false,$result["InsertRoomBookingRule"]))
//		$cnt2 = 1;
//}else{
//	$cnt2 = 1;
//}
//
//if(sizeof($result["InsertRoomBookingRuleRelation"])>0){
//	if(!in_array(false,$result["InsertRoomBookingRuleRelation"]))
//		$cnt3 = 1;
//}else{
//	$cnt3 = 1;
//}

//if(($cnt1) && ($cnt2) && ($cnt3)) {
if(!in_array(false, $result)) {
	$lebooking->Commit_Trans();
	echo 1;
} else {
	$lebooking->RollBack_Trans();
	echo 0;
}

intranet_closedb();
?>