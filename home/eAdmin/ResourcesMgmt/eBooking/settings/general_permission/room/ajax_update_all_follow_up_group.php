<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

$lebooking = new libebooking();

intranet_auth();
intranet_opendb();

$lebooking->Start_Trans();

$targetBuilding = (isset($_POST['targetBuilding'])/* && $_POST['targetBuilding'] != ""*/) ? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor'])/* && $_POST['targetFloor'] != ""*/) ? $_POST['targetFloor'] : $targetFloor;
$targetRoom		= (isset($_POST['targetRoom'])/* && $_POST['targetRoom'] != ""*/) ? $_POST['targetRoom'] : $targetRoom;
$displayedSubLocationIdList		= (isset($_POST['displayedSubLocationIdList'])/* && $_POST['displayedSubLocationIdList'] != ""*/) ? $_POST['displayedSubLocationIdList'] : $displayedSubLocationIdList;

$arrFollowUpGroup = explode(",",$targetFollowUpGroup);

if($targetRoomID == "")
{
	$displayedSubLocationIdAry = explode(',', $displayedSubLocationIdList);
	$sql = "SELECT 
				Room.LocationID 
			FROM 
				INVENTORY_LOCATION_BUILDING AS Building 
				INNER JOIN INVENTORY_LOCATION_LEVEL AS Floor ON (Building.BuildingID = Floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION AS Room ON (Floor.LocationLevelID = Room.LocationLevelID)
			WHERE
				Building.RecordStatus = 1 AND Floor.RecordStatus = 1 AND Room.RecordStatus = 1 
				AND	Building.BuildingID = $targetBuilding AND Floor.LocationLevelID = $targetFloor AND Room.LocationID IN ('".implode("','", (array)$displayedSubLocationIdAry)."')";
	
	$arrLocation = $lebooking->returnVector($sql);
	if(sizeof($arrLocation)>0)
	{
		$targetRoomID = implode(",",$arrLocation);
	}
}
else
{
	$arrLocation = explode(",",$targetRoomID);
}
	
$sql = "DELETE FROM INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION WHERE LocationID IN ($targetRoomID)";
$lebooking->db_db_query($sql);

for($i=0; $i<sizeof($arrLocation); $i++)
{
	$location_id = $arrLocation[$i];
	for($j=0; $j<sizeof($arrFollowUpGroup); $j++)
	{
		$follow_up_group_id = $arrFollowUpGroup[$j];
		$sql = "INSERT INTO 
					INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION 
					(LocationID, GroupID, InputBy, ModifiedBy, DateInput, DateModified)
				VALUES
					('$location_id','$follow_up_group_id',$UserID, $UserID, NOW(),NOW())";
		$result["UpdateFollowUpGrgoup"][] = $lebooking->db_db_query($sql);
	}
}

if(in_array(false,$result["UpdateFollowUpGrgoup"])) {
	$lebooking->RollBack_Trans();
	echo 0;
} else {
	$lebooking->Commit_Trans();
	echo 1;
}

intranet_closedb();
?>