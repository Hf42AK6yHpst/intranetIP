<?php
// using : 
################### Change Log ###################
##  Date    : 2014-01-15 (Tiffany)
##            add the function js_On_Periodic_Booking_Method_Update()
##            change the function js_Check_Form() that add the PeriodicBookingMethod part
################### Change Log ###################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_SystemProperty";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['SystemProperty']['MenuTitle']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

echo $lebooking_ui->Get_System_Property_Index_UI();

?>
<script language='javascript'>
$().ready(function(){
	js_Display_View();
	js_Disable_Date_Field();
});

function js_Display_View()
{
	$(".DisplayView").show();
	$(".EditView").hide();
}

function js_Edit_View()
{
	$(".DisplayView").hide();
	$(".EditView").show();
}

function js_Disable_Date_Field()
{
	var AllowBookingStatus = $(".AllowBookingStatus:checked").val();
	if(AllowBookingStatus==3)
	{
		$("#SuspendBookingUntil").attr("disabled","") 
	}
	else
	{
		$("#SuspendBookingUntil").attr("disabled","disabled")
	}
}

function js_On_Booking_Method_Update()
{
	SpecificPeriodEnabled = $("input#SpecificPeriodEnabled").attr("checked");
	SpecificTimeEnabled = $("input#SpecificTimeEnabled").attr("checked");
	$("input#DefaultBookingMethodPeriod").attr("disabled",!SpecificPeriodEnabled);
	$("input#DefaultBookingMethodTime").attr("disabled",!SpecificTimeEnabled);
	
	if($("input.BookingMethod:checked").length==1)
	{
		$("input.DefaultBookingMethod:enabled").attr("checked","checked");
	}	
}


function js_On_Booking_Type_Update()
{
	var SingleEnabled_chk = $("input#SingleEnabled_chk").attr("checked");
	var PeriodicEnabled_chk = $("input#PeriodicEnabled_chk").attr("checked");
	$("input#DefaultBookingTypeSingle_radio").attr("disabled",!SingleEnabled_chk);
	$("input#DefaultBookingTypePeriodic_radio").attr("disabled",!PeriodicEnabled_chk);
	
	if($("input.BookingType:checked").length==1)
	{
		$("input.DefaultBookingType:enabled").attr("checked","checked");
	}	
}


function js_Check_Form()
{
	var FormValid = true;
	$("div.WarnMsg").hide();
	
	if($("input.BookingMethod:checked").length==0)
	{
		$("div#WarnSelectBookingMethod").show();
		FormValid = false;
	}
	
	if($("input.BookingType:checked").length==0)
	{
		$("div#WarnSelectBookingType").show();
		FormValid = false;
	}
	
    if($("input.PeriodicBookingMethod:checked").length==0)
	{
		$("div#WarnSelectPeriodicBookingMethod").show();
		FormValid = false;
	}
	
	if(!FormValid)
		return false;
}

function js_On_Periodic_Booking_Method_Update()
{
	WeekDayEnabled = $("input#WeekDayEnabled").attr("checked");
	CycleEnabled = $("input#CycleEnabled").attr("checked");
	$("input#DefaultPeriodicBookingMethodWeekDay").attr("disabled",!WeekDayEnabled);
	$("input#DefaultPeriodicBookingMethodCycle").attr("disabled",!CycleEnabled);
	
	if($("input.PeriodicBookingMethod:checked").length==1)
	{
		$("input.DefaultPeriodicBookingMethod:enabled").attr("checked","checked");
	}	
}

</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>