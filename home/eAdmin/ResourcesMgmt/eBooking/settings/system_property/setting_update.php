<?php
// using : 

################### Change Log ###################
##  Date    : 2019-09-18 (Tommy)
##            add setting option BookingApproveNotification
##
##	Date	: 2014-06-23 (Bill)
##			  add setting option ApprovalNotification
##
##  Date    : 2014-01-15 (Tiffany)
##            add "WeekDayEnabled", "CycleEnabled", "DefaultPeriodicBookingMethod"
##
##	Date	: 2012-10-25 (YatWoon)
##			  add setting option "ReceiveEmailNotificationFromeInventory"
##
################### Change Log ###################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$Setting['AllowBookingStatus'] = $AllowBookingStatus;
if($AllowBookingStatus==3)
	$Setting['SuspendBookingUntil'] = $SuspendBookingUntil;

$bookingTypeSingleEnabled = $_POST['SingleEnabled_chk'];
$bookingTypePeriodicEnabled = $_POST['PeriodicEnabled_chk'];
$DefaultBookingType = $_POST['DefaultBookingType'];
$Setting['bookingTypeSingleEnabled'] = $bookingTypeSingleEnabled;
$Setting['bookingTypePeriodicEnabled'] = $bookingTypePeriodicEnabled;
$Setting['DefaultBookingType'] = $DefaultBookingType;


$Setting['SpecificPeriodEnabled'] = $SpecificPeriodEnabled;
$Setting['SpecificTimeEnabled'] = $SpecificTimeEnabled;
$Setting['DefaultBookingMethod'] = $DefaultBookingMethod;
#Tiffany
$Setting['WeekDayEnabled'] = $WeekDayEnabled;
$Setting['CycleEnabled'] = $CycleEnabled;
$Setting['DefaultPeriodicBookingMethod'] = $DefaultPeriodicBookingMethod;


$Setting['DisplayBuildingName'] = $DisplayBuildingName;
$Setting['DisplayFloorName'] = $DisplayFloorName;

$Setting['BookingNotification'] = $BookingNotification;
//$Setting['DefaultApplyDoorAccess'] = $_POST['DefaultApplyDoorAccess'];

// For email notification to appproval group
$Setting['ApprovalNotification'] = $ApprovalNotification;

// For booking approve email notification to follow-up group
$Setting['BookingApproveNotification'] = $BookingApproveNotification;

if ($sys_custom['eBooking_Cust_BookingFormUrl']) {
	$Setting['BookingFormUrl'] = trim(stripslashes($BookingFormUrl));
}

//if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
	$Setting['EventInICalendar'] = $EventInICalendar;	
//}

if ($plugin['Inventory']) {
	$Setting['AllowToBookDamagedItem'] = $AllowToBookDamagedItem;
	$Setting['AllowToBookRepairingItem'] = $AllowToBookRepairingItem;
	$Setting['ReceiveEmailNotificationFromeInventory'] = $ReceiveEmailNotificationFromeInventory;
}

$Setting['ReceiveCancelBookingEmailNotification'] = $ReceiveCancelBookingEmailNotification;

$Success = $lebooking->Save_General_Setting($Setting);

if($Success)
	$msg = "SettingSaveSuccess";
else
	$msg = "SettingSaveFail";
	
header("location: ./setting.php?msg=$msg");	

intranet_closedb();
?>