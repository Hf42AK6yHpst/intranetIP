<?php
// Using:
#################
#	Date	2017-05-26 Villa #E117178
#		Fix js error due to using special char in chi for category eng field
#	Date 	2016-09-26 Omas #J103301
#		Hide Photo usage in eBooking
###################
###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();
$lebooking->Check_Page_Permission();

### Category ###
$temp[] = array($i_InventorySystem['Settings']);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";
$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['Category']." > ".$button_add)."</td></tr>";

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['Category'], "", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// get all category's Code, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng FROM INVENTORY_CATEGORY";
$CatNameList = $linventory->returnArray($sql,3);

$sql = "SELECT COUNT(*) FROM INVENTORY_CATEGORY";
$total_rec = $linventory->returnVector($sql);
$display_order .= "<select name=\"cat_display_order\" id=\"cat_display_order\">";
for($i=0; $i<=$total_rec[0]; $i++)
{
	$j=$i+1;
	if($j==$total_rec[0])
		$selected = "SELECTED=\"selected\"";
		$display_order .= "<option value=\"$j\" $selected>$j</option>";
}
$display_order .= "</select>";


## Show The Default Photos ##
$image_folder = "images/inventory";
$targetDir = $PATH_WRT_ROOT.$image_folder;
$handle = @opendir($targetDir);
while (false !== ($file = @readdir($handle)))
{
	if($file!="." && $file!='..')
	{
		if($cnt%3 == 0)
			$default_photo_link .= "<tr>";
			
			if($cnt == 0)
			{
				$checked = " CHECKED ";
			}
			else
			{
				$checked = " ";
			}
			
			$default_photo_link .= "<td class=\"tabletext\" valign=\"top\">
			<img src=\"$targetDir/$file\" width=\"100px\" height=\"100px\">
			<center><input type=\"radio\" name=\"default_photo\" value=\"$file\" $checked onClick=\"SetPhotoUpload(0);\"></center>
			</td>";
			if($cnt%3 == 2)
				$default_photo_link .= "</tr>";
				
				$cnt++;
	}
}
## end ##

$arr_PhotoUsage = array(array("0",$i_InventorySystem_Setting_Photo_DoNotUse),array("1",$i_InventorySystem_Setting_Photo_Use));
$table_content .= "<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>";
$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_Code</td><td class=\"tabletext\" valign=\"top\"><input id=\"cat_code\" name=\"cat_code\" type=\"text\" class=\"textboxnum\" maxlength=\"5\" value=\"$cat_code\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_ChineseName</td><td class=\"tabletext\" valign=\"top\"><input name=\"cat_chi_name\" id=\"cat_chi_name\" type=\"text\" class=\"textboxtext\" value=\"$cat_chi_name\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_EnglishName</td><td class=\"tabletext\" valign=\"top\"><input name=\"cat_eng_name\" id=\"cat_eng_name\" type=\"text\" class=\"textboxtext\" value=\"$cat_eng_name\"></td></tr>";
if($sys_custom['eInventory_PriceCeiling']){
	if($price_ceiling == ""){
		$price_ceiling = 0;
	}
	$price_ceiling_checked = "";
	if($apply_to_bulk){
		$price_ceiling_checked = " CHECKED ";
	}
	$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_CategorySetting_PriceCeiling</td>
	<td class=\"tabletext\" valign=\"top\">
	<input type=\"text\" name=\"price_ceiling\" id=\"price_ceiling\" class=\"textboxnum\" value=\"$price_ceiling\"> <input type='checkbox' name='apply_to_bulk' id='apply_to_bulk' value='1' $price_ceiling_checked> $i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk
	</td>
	</tr>";
}
$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_DisplayOrder</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>";
// #J103301
// $table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_Photo</td>
// 						<td>".getSelectByArray($arr_PhotoUsage,"name='photo_usage' id='photo_usage' onChange='enablePhotoSelection(this.value);'",$photo_usage,0,1)."</td></tr>";
$table_content .= "</table>";

$table_content .= "<div id='div_photo_selection' style='display:none;'>";
$table_content .= "<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>";
$table_content .= "<tr>
<td width=\"20%\"></td>
<td class=\"tabletext\" valign=\"top\">
<table border=\"0\" width=\"100%\">
$default_photo_link
</table><br>
<input type=\"radio\" name=\"default_photo\" value=\"none\" onClick=\"SetPhotoUpload(1);\">$i_InventorySystem_Category_CustomPhoto&nbsp;
<input type=\"file\" name=\"cat_photo\" class=\"file\" DISABLED><input type=\"hidden\" name=\"hidden_cat_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>";
$table_content .= "<tr><td colspan=2 align=center>".
		$linterface->GET_ACTION_BTN($button_submit, "submit", "grapAttach(this.form)", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")." ".
		$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:resetForm()", "reset_form", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")." ".
		$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")
		."</td></tr>";
		$table_content .= "</table>";
		$table_content .= "</div>";
		
		$table_content .= "<div id='div_no_photo_selection' style=''>";
		$table_content .= "<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>";
		$table_content .= "<tr>
						<td colspan=2 align=center>".
						$linterface->GET_ACTION_BTN($button_submit, "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")." ".
						$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:resetForm()", "reset_form", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")." ".
						$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")
						."</td></tr>";
						$table_content .= "</table>";
						$table_content .= "</div>";
						
						//echo generateFileUploadNameHandler("form1","cat_photo","hidden_cat_photo");
						?>

<script language="javascript">
$(document).ready( function() {	
	$('input#cat_code').focus();
});

function resetForm()
{
	$("#cat_code").val('');
	$("#cat_chi_name").val('');
	$("#cat_eng_name").val('');
	$("#price_ceiling").val(0);
	$('input:checkbox').removeAttr('checked');
	$('#cat_display_order').find('option:last').attr('selected', 'selected').parent('select');
	$('#photo_usage').find('option:first').attr('selected', 'selected').parent('select');
	$("#div_photo_selection").hide();
	$("#div_no_photo_selection").show();
}

function enablePhotoSelection(val)
{
	if(val == 1) {
		$("#div_photo_selection").show();
		$("#div_no_photo_selection").hide();
	} else {
		$("#div_photo_selection").hide();
		$("#div_no_photo_selection").show();
	}
}

function SetPhotoUpload(enabled)
{
	if(enabled == 1)
	{
		document.form1.cat_photo.disabled = false;
	}
	else
	{
		document.form1.cat_photo.disabled = true;
	}
}

<? if($photo_usage){ ?>
function grapAttach(cform)
{
	var obj = document.form1.cat_photo;
	var key;
	var s="";
	key = obj.value;
	if (key!="")
		s += key;
	cform.attachStr.value = s;
}
<? } ?>

function checkCategoryCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.cat_code.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_Category_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat_code.value == "<?=$CatNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_Category_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkCategoryChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat_chi_name.value == "<?=addslashes($CatNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_Category_Item_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkCategoryEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat_eng_name.value == "<?=addslashes($CatNameList[$i]['NameEng'])?>") {
					alert("<?=$i_InventorySystem_Input_Category_Item_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;
	<? if($photo_usage){ ?>
	var upload = Big5FileUploadHandler();
	<? } ?>
	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
	
	<? if($photo_usage){ ?>
	if(upload == true)
	{
	<? } ?>
		if(check_text(obj.cat_code,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
			if(checkCategoryCode()) {
				if(check_text(obj.cat_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
					if(check_text(obj.cat_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						<? if($sys_custom['eInventory_PriceCeiling']){ ?>
							if(obj.price_ceiling.value >= 0){
								passed = 1;
							}else{
								alert("<?=$i_InventorySystem_CategorySetting_PriceCeiling_JSWarning1;?>");
								passed = 0;
								return false;
							}
						<? } ?>
								passed = 1;	
					}else {
							passed = 0;
					}
				}else {
						passed = 0;
				}
			}else {
				error_cnt = 1;
				passed = 0;
			}
		}else {
			passed = 0;
		}
	<? if($photo_usage){ ?>
	}
	<? } ?>
				
	if(passed == 1)
	{
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.cat_code.focus();
		if(error_cnt == 2)
			obj.cat_chi_name.focus();
		if(error_cnt == 3)
			obj.cat_eng_name.focus();
			
		return false;
	}
}
</script>

<br>
<form name="form1" enctype="multipart/form-data" method="post" action="category_insert_update.php" onsubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>

	<?=$table_content?>

<input type="hidden" name="attachStr" value="">
<input type="hidden" name="flag" value="1">
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>