<?php
// Using: Ronald

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

global $pageSizeChangeEnabled;

### Cookies handling
# set cookies

if(isset($clearCoo) && $clearCoo == 1)
{
	if(isset($_COOKIE['cat_setting_pageNo'])) {
		$_COOKIE['cat_setting_pageNo'] = "";
		$cat_setting_pageNo = "";
	}
	if(isset($_COOKIE['cat_setting_numPerPage'])) {
		$_COOKIE['cat_setting_numPerPage'] = "";
		$cat_setting_numPerPage = "";
	}
	if(isset($_POST['cat_setting_pageNo']))
	{
		$_POST['cat_setting_pageNo'] = "";
		$cat_setting_pageNo = "";
	}
}
else
{
	// for click 'Next page' & 'Prev page' use
	if(isset($_POST['cat_setting_pageNo']))
		$cat_setting_pageNo = $_POST['cat_setting_pageNo'];
}

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$li				= new libdbtable2007($SortField, $Order, $PageNumber);
$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['Category'], "", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### Get The Category ###
$namefield = $linventory->getInventoryItemNameByLang();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_Catgeory_DeleteFail");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$toolbar = '<table border="0" cellpadding="0" cellspacing="0">';
$toolbar .= '<tr>';
$toolbar .= '<td>';
$toolbar .= $linterface->GET_LNK_NEW("javascript:checkNew('category_insert.php')","","","","",0)."&nbsp;";
$toolbar .= '</td>';
$toolbar .= '<td>';
$toolbar .= $linterface->GET_LNK_IMPORT("category_import.php","","","","",0);
$toolbar .= '<td>';
$toolbar .= '</tr>';
$toolbar .= '</table>';	

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:AssignCookies(); SetCookies(); checkRemove(document.form1,'cat_id[]','category_remove.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_delete
					</a>
				</td>";
					
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "Cat.NameChi";
	$altChoice = "Cat.NameEng";
}
else
{
	$firstChoice = "Cat.NameEng";
	$altChoice = "Cat.NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$table_content = $lebooking_ui->initJavaScript();
if($sys_custom['eInventory_PriceCeiling']){
	$table_content = "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\" width=\"1%\">#</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_InventorySystem_Category_Code</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"35%\">$i_InventorySystem_Category_Name</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"15%\">".$Lang['eInventory']['FieldTitle']['NoOfSubCategory']."</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"10%\">$i_InventorySystem_CategorySetting_PriceCeiling</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"15%\">$i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'cat_id[]'):setChecked(0,this.form,'cat_id[]')\"></td>";
	$table_content .= "</tr>";
}else{
	$table_content = "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\" width=\"1%\">#</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_InventorySystem_Category_Code</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"35%\">$i_InventorySystem_Category_Name</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"35%\">".$Lang['eInventory']['FieldTitle']['NoOfSubCategory']."</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'cat_id[]'):setChecked(0,this.form,'cat_id[]')\"></td>";
	$table_content .= "</tr>";
}

if($sys_custom['eInventory_PriceCeiling']){
	//$sql = "SELECT CategoryID, Code, $namefield, PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY ORDER BY DisplayOrder";
	$sql = "SELECT 
					Cat.CategoryID, 
					Cat.Code, 
					$namefield,
					Cat.PriceCeiling, 
					Cat.ApplyPriceCeilingToBulk,
					Count(SubCat.Category2ID)
			FROM 
					INVENTORY_CATEGORY as Cat
					Left Outer Join
					INVENTORY_CATEGORY_LEVEL2 as SubCat
					On (Cat.CategoryID = SubCat.CategoryID)
			Group By
					Cat.CategoryID
			ORDER BY 
					Cat.DisplayOrder
			";
	$result = $linventory->returnArray($sql,5);
	
	# Default Table Settings
	$cat_setting_pageNo = ($cat_setting_pageNo == '')? $li->pageNo=1 : $li->pageNo=$cat_setting_pageNo;
	$cat_setting_numPerPage = ($cat_setting_numPerPage == '')? $li->page_size=20 : $li->page_size=$cat_setting_numPerPage;
	$Order = ($Order == '')? 1 : $Order;
	$SortField = ($SortField == '')? 0 : $SortField;
	
	if($cat_setting_pageNo == 1)
	{
		$start = $cat_setting_pageNo;
		$end = $cat_setting_numPerPage;
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($result),($li->pageNo*$li->page_size));
	}
	else 
	{
		$start = ($cat_setting_pageNo*$cat_setting_numPerPage)-$cat_setting_numPerPage+1;
		$end = ($cat_setting_pageNo*$cat_setting_numPerPage);
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($result),($li->pageNo*$li->page_size));
	}
	
	if(sizeof($result) > 0)
	{
		//for($i=0; $i<sizeof($result); $i++)
		for($i=$start-1; $i<$end; $i++)
		{
			$j++; 
			list($cat_id, $cat_code, $cat_name, $price_ceiling, $apply_to_bulk, $num_subcat) = $result[$i];
			$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
			if($apply_to_bulk == 1){
				$str_apply_to_bulk = "Y";
			}else{
				$str_apply_to_bulk = "N";
			}
			if($cat_id != "")
			{
				$thisCanDelete = $lebooking->Check_Can_Delete_Category($cat_id, '');
				$thisDisabled = ($thisCanDelete)? '' : 'disabled';
				
				$table_content .= "<tr class=\"$css\">";
				$table_content .= "<td class=\"tabletext\">$j</td>\n";
				$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($cat_code)."</td>\n";
				$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href='#' onClick='GoEditCategoryPage($cat_id)'>".intranet_htmlspecialchars($cat_name)."</a></td>\n";
				$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href='#' onClick='GoSubCategoryPage($cat_id)'>".$num_subcat."</a></td>\n";
				$table_content .= "<td class=\"tabletext\">$price_ceiling</td>";
				$table_content .= "<td class=\"tabletext\">$str_apply_to_bulk</td>";
				$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"cat_id[]\" value=\"$cat_id\" $thisDisabled></td>";
				$table_content .= "</tr>";
			}
		}
	}
	else
	{
		$table_content .= "<tr class=\"tablerow2 tabletext\">";
		$table_content .= "<td class=\"tabletext\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td>";
		$table_content .= "</tr>\n";
	}
	
	### Table Navigation Bar ###
	$li->page_size = $cat_setting_numPerPage;
	$li->total_row = sizeof($result);
	$li->form_name = "form1";
	$li->pageNo_name = "cat_setting_pageNo";
	$li->numPerPage_name = "cat_setting_numPerPage";
	
	$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"7\">";
	$table_content .= $li->navigation();
	$table_content .= "</td></tr>";
}else{
	$sql = "SELECT 
					Cat.CategoryID, 
					Cat.Code, 
					$namefield,
					Count(SubCat.Category2ID)
			FROM 
					INVENTORY_CATEGORY as Cat
					Left Outer Join
					INVENTORY_CATEGORY_LEVEL2 as SubCat
					On (Cat.CategoryID = SubCat.CategoryID)
			Group By
					Cat.CategoryID
			ORDER BY 
					Cat.DisplayOrder
			";
	$result = $linventory->returnArray($sql,3);
	
	
	# Default Table Settings
	$cat_setting_pageNo = ($cat_setting_pageNo == '')? $li->pageNo=1 : $li->pageNo=$cat_setting_pageNo;
	$cat_setting_numPerPage = ($cat_setting_numPerPage == '')? $li->page_size=20 : $li->page_size=$cat_setting_numPerPage;
	$Order = ($Order == '')? 1 : $Order;
	$SortField = ($SortField == '')? 0 : $SortField;
	
	if($cat_setting_pageNo == 1)
	{
		$start = $cat_setting_pageNo;
		$end = $cat_setting_numPerPage;
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($result),($li->pageNo*$li->page_size));
	}
	else 
	{
		$start = ($cat_setting_pageNo*$cat_setting_numPerPage)-$cat_setting_numPerPage+1;
		$end = ($cat_setting_pageNo*$cat_setting_numPerPage);
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($result),($li->pageNo*$li->page_size));
	}
	
	if(sizeof($result) > 0)
	{
		for($i=$start-1; $i<$end; $i++)
		{
			$j++; 
			list($cat_id, $cat_code, $cat_name, $num_subcat) = $result[$i];
			$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
			if($cat_id != "")
			{
				$thisCanDelete = $lebooking->Check_Can_Delete_Category($cat_id, '');
				$thisDisabled = ($thisCanDelete)? '' : 'disabled';
				
				$table_content .= "<tr class=\"$css\">";
				$table_content .= "<td class=\"tabletext\">$j</td>\n";
				$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($cat_code)."</td>\n";
				$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href='#' onClick='GoEditCategoryPage($cat_id)'>".intranet_htmlspecialchars($cat_name)."</a></td>\n";
				$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href='#' onClick='GoSubCategoryPage($cat_id)'>".$num_subcat."</a></td>\n";
				$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"cat_id[]\" value=\"$cat_id\" $thisDisabled></td>";
				$table_content .= "</tr>";
			}
		}
	}
	else
	{
		$table_content .= "<tr class=\"tablerow2 tabletext\">";
		$table_content .= "<td class=\"tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td>";
		$table_content .= "</tr>\n";
	}
	
	### Table Navigation Bar ###
	$li->page_size = $cat_setting_numPerPage;
	$li->total_row = sizeof($result);
	$li->form_name = "form1";
	$li->pageNo_name = "cat_setting_pageNo";
	$li->numPerPage_name = "cat_setting_numPerPage";
	
	$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"5\">";
	if(sizeof($result)>0)
		$table_content .= $li->navigation_ebooking();
	$table_content .= "</td></tr>";
}
$table_content .= "<tr><td>";
$table_content .= "<input type='hidden' name='CheckboxChecked' value=0>";
$table_content .= "<input type='hidden' id='cat_setting_pageNo' name='cat_setting_pageNo' value='".$li->pageNo."'>";
$table_content .= "<input type='hidden' id='order' name='order' value='".$li->order."'>";
$table_content .= "<input type='hidden' id='field' name='field' value='".$li->field."'>";
$table_content .= "<input type='hidden' id='page_size_change' name='page_size_change' value=''>";
$table_content .= "</td></tr>";


### eBooking & eInventory Data Warning
$WarningMsgArr = array();
if ($plugin['eBooking'] && $plugin['Inventory'])
	$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['DataChangeAffect_eInventory'];
$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'];
$WarningMsg = implode('<br />', $WarningMsgArr);
$WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Caution'].'</font>', $WarningMsg, $others="");
?>

<?=$lebooking_ui->initJavaScript();?>

<script language='javascript'>
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "cat_setting_pageNo";
	arrCookies[arrCookies.length] = "cat_setting_numPerPage";
	arrCookies[arrCookies.length] = "cat_setting_category_id";
	
	$(document).ready( function() {
		<? if($clearCoo) { ?>
			for(i=0; i<arrCookies.length; i++)
			{
				var obj = arrCookies[i];
				$.cookies.del(obj);
			}
		<? } ?>
	});
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				//alert("A: "+$('#'+arrCookies[i]).val());
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				//alert("B: "+$.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
	function GoSubCategoryPage(cat_id)
	{
		var str_cat_id = cat_id.toString();
		$.cookies.set('cat_setting_category_id',str_cat_id);
		window.location = 'category_level2_setting.php?clearCoo=1';
	}
	
	function GoEditCategoryPage(cat_id)
	{
		window.location = 'category_edit.php?cat_id=' + cat_id;
	}
	
</script>
<br>
<form name="form1" action="category_setting.php" method="post">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td colspan="2" align="right"><?= $infobar ?></td>
</tr>
</table>

<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr><td colspan="2" align="center"><?=$WarningBox?></td></tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

