<?php
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Settings_Category";
//$linterface	 = new interface_html();
$linventory	 = new libinventory();
$lf			 = new libfilesystem();

/*
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);
*/

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

if($photo_usage == 0){
	$default_photo = "no_photo.jpg";
}

if($default_photo == "none")
{
	# Upload Photo #
	$re_path = "/file/photo/inventory";
	$path = "$intranet_root$re_path";
	$photo = stripslashes(${"hidden_cat2_photo"});
	$target = "";
	
	if($cat2_photo=="none" || $cat2_photo== ""){
	}
	else
	{
		$lf = new libfilesystem();
		if (!is_dir($path))
		{
			$lf->folder_new($path);
		}
		
		$ext = strtoupper($lf->file_ext($photo));
		if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG")
		{
			$target = "$path/$photo";
			$filename .= "/$photo";
			$lf->lfs_copy($cat2_photo, $target);
		}
	}
}
else
{
	$re_path = "/images/inventory/";
	$filename = "/$default_photo";
}

if($default_photo != "")
{
	$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id' AND ItemID = 0";
	$arr_result = $linventory->returnArray($sql,1);
	
	if(sizeof($arr_result)>0) 
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($part_id) = $arr_result[$i];
		
			$sql = "UPDATE INVENTORY_PHOTO_PART SET PhotoPath = '$re_path', PhotoName = '$filename' WHERE PartID = '$part_id'";
			$linventory->db_db_query($sql);
		}
	}
	if(sizeof($arr_result) == 0)
	{
		$sql = "INSERT INTO INVENTORY_PHOTO_PART 
					(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
		VALUES
					('$cat_id', '$cat2_id', '0', '$re_path', '$filename', NOW(), NOW())";
		$linventory->db_db_query($sql);
	}
}

$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id' AND ItemID = 0";
$arr_PartID = $linventory->returnArray($sql,1);
	
if(sizeof($arr_PartID)>0)
{
	for($j=0; $j<sizeof($arr_PartID); $j++)
	{
		list($part_id) = $arr_PartID[$j];
		$sql = "UPDATE 
						INVENTORY_CATEGORY_LEVEL2
				SET
						Code = '$cat2_code',
						NameChi = '$cat2_chi_name',
						NameEng = '$cat2_eng_name',
						DisplayOrder = '$cat2_display_order',
						PhotoLink = '$part_id',
						HasSoftwareLicenseModel = '$cat2_license',
						HasWarrantyExpiryDate = '$cat2_warranty',
						HasSerialNumber = '$cat2_serial',
						DateModified = NOW()
				WHERE
						Category2ID = '$cat2_id' AND
						CategoryID = '$cat_id'";

		$result['UpdateCatL2'.$i] = $linventory->db_db_query($sql);
	}
}

intranet_closedb();
if (!in_array(false,$result)) {		
	header("location: category_level2_setting.php?category_id=".$cat_id."&msg=2");
}
else {
	header("location: category_level2_setting.php?category_id=".$cat_id."&msg=14");
}
?>