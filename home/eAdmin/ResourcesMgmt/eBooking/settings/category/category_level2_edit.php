<?php
// Using:
#################
#
#   Date    2019-05-13 Cameron
#       - fix potential sql injection problem by enclosing var with apostrophe
#
#	Date	2017-05-26 Villa #E117178
#		Fix js error due to using special char in chi for category eng field
#		Hide showing phoho due to J103301
#	Date 	2016-09-26 Omas #J103301
#		Hide Photo usage in eBooking
###################
###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

// moved down as php global register $_GET in lib.php
if(!isset($cat2_id))
{
	header("location: category_setting.php");
	exit();
}

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();
$lebooking->Check_Page_Permission();

### Category ###
$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY WHERE CategoryID = '$cat_id'";
$arr_cat_name = $linventory->returnVector($sql);
$temp[] = array("<a href='category_setting.php'>".$i_InventorySystem['Category']."</a> > <a href='category_level2_setting.php?category_id=$cat_id'>".intranet_htmlspecialchars($arr_cat_name[0])."</a> > ".$i_InventorySystem['SubCategory']." > ".$button_edit);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['Category'], "", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if(is_array($cat2_id)){
	$cat2_id = $cat2_id[0];
}

// get the existing code, chi and eng name
$sql = "SELECT Code, NameChi, NameEng FROM INVENTORY_CATEGORY_LEVEL2 WHERE Category2ID != '$cat2_id'";
$CatNameList = $linventory->returnArray($sql,3);

$sql = "SELECT
Code,
NameChi,
NameEng,
DisplayOrder,
PhotoLink,
HasSoftwareLicenseModel,
HasWarrantyExpiryDate,
HasSerialNumber
FROM
INVENTORY_CATEGORY_LEVEL2
WHERE
Category2ID = '$cat2_id' AND
CategoryID = '$cat_id'";
$result = $linventory->returnArray($sql,7);

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($cat2_code, $cat2_chi_name, $cat2_eng_name, $cat2_display_order, $cat2_photo_link, $cat2_license, $cat2_warranty, $cat2_serial) = $result[$i];
		
		$sql = "SELECT COUNT(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '$cat_id'";
		$total_rec = $linventory->returnVector($sql);
		$display_order .= "<select name=\"cat2_display_order\" id=\"cat2_display_order\">";
		for($j=0; $j<$total_rec[0]; $j++)
		{
			$order=$j+1;
			if($cat2_display_order==$total_rec[0])
				$selected = "SELECTED=\"selected\"";
				$display_order .= "<option value=\"$order\" $selected>$order</option>";
		}
		$display_order .= "</select>";
		
		if($cat2_id == "")
		{
			$cat2_id = 0;
		}
		
		$sql = "SELECT
		PhotoPath,
		PhotoName
		FROM
		INVENTORY_PHOTO_PART
		WHERE
		PartID = '$cat2_photo_link'";
		
		$result = $linventory->returnArray($sql,2);
		
		if(sizeof($result)>0)
		{
			for($i=0; $i<sizeof($result); $i++)
			{
				list($path, $file_name) = $result[$i];
				$photo = "<img src='$path/$file_name' border=0 width=\"100px\" height=\"100px\">";
			}
		}
		
		## Show The Default Photos ##
		$image_folder = "images/inventory";
		$targetDir = $PATH_WRT_ROOT.$image_folder;
		$handle = @opendir($targetDir);
		while (false !== ($file = @readdir($handle)))
		{
			if($file!="." && $file!='..')
			{
				if($cnt%3 == 0)
					$default_photo_link .= "<tr>";
					
					$default_photo_link .= "<td class=\"tabletext\" valign=\"top\">
					<img src=\"$targetDir/$file\" width=\"100px\" height=\"100px\">
					<center><input type=\"radio\" name=\"default_photo\" value=\"$file\" onClick=\"SetPhotoUpload(0);\"></center>
					</td>";
					if($cnt%3 == 2)
						$default_photo_link .= "</tr>";
						
						$cnt++;
			}
		}
		## end ##
		
		$table_content .= "<table width='90%' border='0' cellpadding='5'  cellspacing='0' align='center'>";
		$table_content .= "<tr><td><table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_Code</td><td class=\"tabletext\" valign=\"top\"><input id=\"cat2_code\" name=\"cat2_code\" type=\"text\" class=\"textboxnum\" maxlength=\"5\" value='".intranet_htmlspecialchars($cat2_code)."'></td></tr>\n";
		$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_ChineseName</td><td class=\"tabletext\" valign=\"top\"><input name=\"cat2_chi_name\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($cat2_chi_name)."'></td></tr>\n";
		$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_EnglishName</td><td class=\"tabletext\" valign=\"top\"><input name=\"cat2_eng_name\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($cat2_eng_name)."'></td></tr>\n";
		$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_DisplayOrder</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>\n";
		if($cat2_license == 1)
			$license_checked = "CHECKED";
			else
				$license_checked = "";
				
				if($cat2_warranty == 1)
					$warranty_checked = "CHECKED";
					else
						$warranty_checked = "";
						
						if($cat2_serial == 1)
							$serial_checked = "CHECKED";
							else
								$serial_checked = "";
								
								$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category2_License</td><td class=\"tabletext\" valign=\"top\"><input type=\"checkbox\" name=\"cat2_license\" id=\"cat2_license\" value='1' $license_checked></td></tr>\n";
								$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category2_Warranty</td><td class=\"tabletext\" valign=\"top\"><input type=\"checkbox\" name=\"cat2_warranty\" id=\"cat2_warranty\" value='1' $warranty_checked></td></tr>\n";
								$table_content .= "<tr><td valign=\"top\" width=\"20%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category2_Serial</td><td class=\"tabletext\" valign=\"top\"><input type=\"checkbox\" name=\"cat2_serial\" id=\"cat2_serial\" value='1' $serial_checked></td></tr>\n";
								
								$arr_PhotoUsage = array(array("0",$i_InventorySystem_Setting_Photo_DoNotUse),array("1",$i_InventorySystem_Setting_Photo_Use));
								if($photo_usage == ""){
									if((substr($file_name,1,strlen($file_name)) == 'no_photo.jpg')||(substr($file_name,1,strlen($file_name)) == '')){
										$photo_usage = 0;
									}else{
										$photo_usage = 1;
									}
								}
								// 		#J103301
								// 		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Category_Photo</td>
								// 								<td>".getSelectByArray($arr_PhotoUsage,"name='photo_usage' id='photo_usage' onChange='enablePhotoSelection(this.value);'", $photo_usage, 0, 1)."</td></tr>";
								//if($photo_usage){
								$photo_link_content .= "<tr>
								<td width='20%'></td>
								<td class=\"tabletext\" valign=\"top\">
								<table border=\"0\" width=\"100%\">
								$default_photo_link
								</table><br>
								<input type=\"radio\" name=\"default_photo\" value=\"none\" onClick=\"SetPhotoUpload(1);\"><input type=\"file\" name=\"cat2_photo\" class=\"file\" DISABLED><input type=\"hidden\" name=\"hidden_cat2_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>\n";
								//}
								$table_content .= "<input name=\"part_id\" type=\"hidden\" value=\"$cat2_photo_link\">\n";
								
								$table_content .= "<input name=\"cat_id\" type=\"hidden\" value=\"$cat_id\">\n";
								$table_content .= "<input name=\"cat2_id\" type=\"hidden\" value=\"$cat2_id\">\n";
								$table_content .= "<input name=\"part_id\" type=\"hidden\" value=\"$cat2_photo_link\">\n";
								$table_content .= "</table></td>";
								
// 								$table_content .= "<td width=\"20%\" valign=\"top\" align=\"center\"><table border=\"0\" width=\"100%\" align=\"center\">";
// 								$table_content .= "<tr><td class=\"tabletext\" align=\"center\">".$Lang['eBooking']['Settings']['Category']['CurrentPhoto']."</td></tr>";
// 								$table_content .= "<tr><td align=\"right\">$photo</td></tr>";
// 								$table_content .= "</table></td></tr>";
								$table_content .= "</table>";
								}
								
								//if($photo_usage){
								$table_content .= "<div id='div_photo_selection' style='display:none;'>";
								$table_content .= "<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>";
								$table_content .= $photo_link_content;
								$table_content .= "<tr><td colspan=2 align=center>".
										$linterface->GET_ACTION_BTN($button_submit, "submit", "grapAttach(this.form)")." ".
										$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:resetForm()", "reset_form", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")." ".
										$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
										."</td></tr>";
										$table_content .= "</table>";
										$table_content .= "</div>";
										//}else{
										$table_content .= "<div id='div_no_photo_selection' style=''>";
										$table_content .= "<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>";
										$table_content .= "<tr><td colspan=2 align=center>".
												$linterface->GET_ACTION_BTN($button_submit, "submit")." ".
												$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:resetForm()", "reset_form", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" ")." ".
												$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
												."</td></tr>";
												$table_content .= "</table>";
												$table_content .= "</div>";
												//}
												
												echo generateFileUploadNameHandler("form1","cat2_photo","hidden_cat2_photo");
	}
	?>
<script language="javascript">
var display_selected_index = '';
var photo_usage_selected_index = '';
$(document).ready( function() {	
	$('input#cat2_code').focus();
	<?// if($photo_usage){ ?>
//		$("#div_photo_selection").show();
//		$("#div_no_photo_selection").hide();
	<?// } ?>
	display_selected_index = document.getElementById('cat2_display_order').selectedIndex;
// 	photo_usage_selected_index = document.getElementById('photo_usage').selectedIndex;
});

function resetForm()
{
	$("#cat2_code").val('<?=$cat2_code?>');
	$("#cat2_chi_name").val('<?=$cat2_chi_name?>');
	$("#cat2_eng_name").val('<?=$cat2_eng_name?>');
	<? if($cat2_license == 1) { ?>
		$('#cat2_license').attr('checked','true');
	<? } else { ?>
		$('#cat2_license').removeAttr('checked');
	<? } ?>
	
	<? if($cat2_warranty == 1) { ?>
		$('#cat2_warranty').attr('checked','true');
	<? } else { ?>
		$('#cat2_warranty').removeAttr('checked');
	<? } ?>
	
	<? if($cat2_serial == 1) { ?>
		$('#cat2_serial').attr('checked','true');
	<? } else { ?>
		$('#cat2_serial').removeAttr('checked');
	<? } ?>
		
	
	$('#cat2_display_order').find('option:display_selected_index').attr('selected', 'selected').parent('select');
	
	$('#photo_usage').find('option:photo_usage_selected_index').attr('selected', 'selected').parent('select');
	
	<? if($photo_usage){ ?>
		$("#div_photo_selection").show();
		$("#div_no_photo_selection").hide();
	<? } else { ?>
		$("#div_photo_selection").hide();
		$("#div_no_photo_selection").show();
	<? } ?>
}

function enablePhotoSelection(val)
{
	if(val == 1) {
		$("#div_photo_selection").show();
		$("#div_no_photo_selection").hide();
	} else {
		$("#div_photo_selection").hide();
		$("#div_no_photo_selection").show();
	}
}

function SetPhotoUpload(enabled)
{
	if(enabled == 1)
	{
		document.form1.cat2_photo.disabled = false;
	}
	else
	{
		document.form1.cat2_photo.disabled = true;
	}
}

<? if($photo_usage){ ?>
function grapAttach(cform)
{
var obj = document.form1.cat2_photo;
var key;
var s="";
key = obj.value;
if (key!="")
	s += key;
cform.attachStr.value = s;
}
<? } ?>

function checkCategoryCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.cat2_code.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_SubCategory_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat2_code.value == "<?=$CatNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_SubCategory_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkCategoryChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat2_chi_name.value == "<?=addslashes($CatNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_SubCategory_Item_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkCategoryEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat2_eng_name.value == "<?=addslashes($CatNameList[$i]['NameEng'])?>") {
					alert("<?=$i_InventorySystem_Input_SubCategory_Item_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;
	<? if($photo_usage){ ?>
	var upload = Big5FileUploadHandler();
	<? } ?>
	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
	
	<? if($photo_usage){ ?>
	if(upload == true)
	{
	<? } ?>
		if(check_text(obj.cat2_code,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
			if(checkCategoryCode()) {
				if(check_text(obj.cat2_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
					if(check_text(obj.cat2_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						passed = 1;	
					} else {
						passed = 0;
					}
				} else { 
					passed = 0;
				}
			} else {
					error_cnt = 1;
					passed = 0;
			}
		} else {
			passed = 0;
		}
	<? if($photo_usage){ ?>
	}
	<? } ?>
	
	if(passed == 1)
	{
		obj.action = "category_level2_edit_update.php";
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.cat2_code.focus();
		if(error_cnt == 2)
			obj.cat2_chi_name.focus();
		if(error_cnt == 3)
			obj.cat2_eng_name.focus();
			
		obj.action = "";
		return false;
	}
}

</script>

<br>
<form name="form1" enctype="multipart/form-data" action="" method="POST" onsubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>

<?=$table_content?>

<input type="hidden" name="attachStr" value="" />
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>