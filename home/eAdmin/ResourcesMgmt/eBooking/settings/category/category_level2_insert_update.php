<?php
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################
/*
 *  2019-04-29 Cameron
 *      - fix potential sql injection problem by enclosed var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libfiletable.php");
//include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$lfile		= new libfilesystem();
$lclass 	= new libeclass();
$linventory	= new libinventory();


$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '$cat_id' AND DisplayOrder = '$cat2_display_order'";
$result = $linventory->returnVector($sql);
$existingSubCatID = $result[0];

if($existingSubCatID != "")
{
	$sql = "SELECT MAX(DisplayOrder)+1 FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '$cat_id'";
	$result = $linventory->returnVector($sql);
	$new_order = $result[0];
	$sql = "UPDATE INVENTORY_CATEGORY_LEVEL2 SET DisplayOrder = '$new_order' WHERE CategoryID = '$cat_id' AND Category2ID = '$existingSubCatID'";
	$linventory->db_db_query($sql);
}

$sql = "INSERT INTO INVENTORY_CATEGORY_LEVEL2 
				(CategoryID, Code, NameChi, NameEng, DisplayOrder, PhotoLink, HasSoftwareLicenseModel, HasWarrantyExpiryDate, HasSerialNumber, RecordStatus, DateInput, DateModified)
		VALUES 
				('$cat_id', '$cat2_code', '$cat2_chi_name', '$cat2_eng_name', '$cat2_display_order', '', '$cat2_license', '$cat2_warranty', '$cat2_serial', '1',  NOW(), NOW())";

$linventory->db_db_query($sql);

if ($linventory->db_affected_rows($sql) == 1)
{
	$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '$cat_id' AND Code = '$cat2_code'";

	$result = $linventory->returnArray($sql,1);
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($cat2_id) = $result[$i];
		}
		
		if($photo_usage == 0){
			$default_photo = "no_photo.jpg";
		}
				
		if($default_photo == "none")
		{
			$lf = new libfilesystem();
			# Upload Photo #
			$re_path = "/file/photo/inventory";
			$path = "$intranet_root$re_path";
			if (!is_dir($path))
			{
				$lf->folder_new($path);
			}
			
			$filename = $_FILES['cat2_photo']['name'];
			$ext = strtoupper($lf->file_ext($filename));
			
			if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG")
			{
				$target = $path."/".$filename;
				$result = $lf->lfs_copy($cat_photo, $target);
			}
	    }
	    else
	    {
		    $re_path = "/images/inventory/";
		    $filename = "/$default_photo";
	    }
	    
	    $values .= "($cat_id, $cat2_id, 0, '$re_path', '$filename', NOW(), NOW())";
	    
	    $sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
		
		$linventory->db_db_query($sql);
		
		if($linventory->db_affected_rows($sql) > 0)
		{
			$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id' AND ItemID= 0"; 

			$result = $linventory->returnArray($sql,1);
			
			if(sizeof($result)>0)
			{
				list($part_id) = $result[0];
			}
		}
				
		$sql = "UPDATE INVENTORY_CATEGORY_LEVEL2 SET PhotoLink = '$part_id' WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id'";

		$linventory->db_db_query($sql);
		
	    ###################
	    
		/*
		# Upload Photo #
		$re_path = "/file/photo/inventory";
		$path = "$file_path/$re_path";
		$lfile->folder_new($path);
		
		$lremove = new libfiletable("", $path, 0, 0, "");
		$files = $lremove->files;
		
		$attachStr=stripslashes($attachStr);

		$loc = ${"cat2_photo"};
		$file = stripslashes(${"hidden_cat2_photo"});
		$des = "$path/".$file;
		
		if (!is_file($loc))
		{
		}
		if($loc=="")
		{
		} 
		else 
		{
			$ext = strtoupper($lfile->file_ext($hidden_cat2_photo));
			if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG"){
				$lfile->lfs_copy($loc, $des);
			}
				
			$lo = new libfiletable("", $path, 0, 0, "");
			$files = $lo->files;
			
			while (list($key, $value) = each($files)) {
				if(in_array($file, $files[$key]))
				{
					list($filename, $filesize) = $files[$key];
					$filename = addslashes($filename);
					$filesize = ceil($filesize/1000);
				    $values .= "$delim('$cat_id','$cat2_id','$re_path', '$filename', NOW(),NOW())";
				    $delim = ",";
				    
				    $photo_existed = 1;
			    }
			    if($photo_existed != 1)
			    {
					if($key == sizeof($files)-1)
					{
						list($filename, $filesize) = $files[$key];
						$filename = addslashes($filename);
						$filesize = ceil($filesize/1000);
					    $values .= "$delim('$cat_id','$cat2_id','$re_path', '$filename', NOW(), NOW())";
					    $delim = ",";
			    	}
				}
			}

			$sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
							(CategoryID, Category2ID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES 
							$values";

			$linventory->db_db_query($sql);
			
			if($linventory->db_affected_rows($sql) > 0)
			{
				$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id'"; 

				$result = $linventory->returnArray($sql,1);
				
				if(sizeof($result)>0)
				{
					list($part_id) = $result[0];
				}
			}
					
			$sql = "UPDATE INVENTORY_CATEGORY_LEVEL2 SET PhotoLink = '$part_id' WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id'";

			$linventory->db_db_query($sql);
		}
		*/
	}
	header("Location: category_level2_setting.php?category_id=".$cat_id."&msg=1");
}
else
{
	header("Location: category_level2_setting.php?category_id=".$cat_id."&msg=12");
}

intranet_closedb();

?>