<?php
// using : Ronald

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.cust.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui_wkf();
$linventory = new libinventory();
$li = new libfilesystem();
$lexport = new libexporttext();

// csv main content
$csv_content = $lebooking_ui->Show_WKF_Custom_Report_Export($LocationID,$ManagementGroupID,$BookingStatus,$BookingDateType);

$export_content = $lexport->GET_EXPORT_TXT($csv_content, array());
$filename = "room_booking_list".date('Y-m-d').".csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>