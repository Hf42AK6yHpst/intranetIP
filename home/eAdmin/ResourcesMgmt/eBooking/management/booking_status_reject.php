<?php
// using : 
/*********************************************** Change Log *****************************************************
 * 2019-06-13 (Cameron): fix error: should get the first row of record but not the whole query result when use list() method 
 * 2019-05-01 (Cameron): apply IntegerSafe to ID fields to fix sql injection problem
 * 2018-12-11 (Cameron): also update ProcessDateTime so that it can show the reject time
 * 2011-03-07 (Carlos): modified rollback transaction logic for each record, not rollback whole batch of records
 ****************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libebooking_door_access.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$linventory = new libinventory();
$lwebmail = new libwebmail();
$lebooking_door_access = new libebooking_door_access();

$BookingRecord = unserialize(urldecode($_POST['BookingRecord']));

$sent_email_result = true;
$emailBookingIdAry = array();

//$lebooking->Start_Trans();

foreach($BookingRecord as $booking_id => $BookingDetail)
{
    $booking_id = IntegerSafe($booking_id);
	$result = array();
	$lebooking->Start_Trans();

	for($i=0; $i<sizeof($BookingDetail['Room']); $i++)
	{
//		$room_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
		$room_id = IntegerSafe($BookingDetail['Room'][$i]);
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = '".$_SESSION['UserID']."', RejectReason = '".$lebooking->Get_Safe_Sql_Query(standardizeFormPostValue($reason[$booking_id]['Room'][$room_id]))."', ProcessDate = NOW(), ProcessDateTime = NOW(), BookingStatus = -1 WHERE FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = '$room_id' AND BookingID = '$booking_id'";

		$result['UpdateRecordDetails_'.$booking_id.'_room_'.$room_id] = $lebooking->db_db_query($sql);				
	}
	
	for($i=0; $i<sizeof($BookingDetail['Item']); $i++)
	{
//		$item_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
		$item_id = IntegerSafe($BookingDetail['Item'][$i]);
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = '".$_SESSION['UserID']."', RejectReason = '".$lebooking->Get_Safe_Sql_Query(standardizeFormPostValue($reason[$booking_id]['Item'][$item_id]))."', ProcessDate = NOW(), ProcessDateTime = NOW(), BookingStatus = -1 WHERE FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = '$item_id' AND BookingID = '$booking_id'";

		$result['UpdateRecordDetails_'.$booking_id.'_item_'.$item_id] = $lebooking->db_db_query($sql);				
	}

	### reject others if the DeleteOtherRelatedIfReject is ticked before. 
	$sql = "SELECT DeleteOtherRelatedIfReject FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$booking_id'";
	$DeleteOtherRelated = $lebooking->returnVector($sql);
	if($DeleteOtherRelated[0] > 0){
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = '".$_SESSION['UserID']."', ProcessDate = NOW(), ProcessDateTime = NOW(), BookingStatus = -1 WHERE BookingID = '$booking_id'";
		$result['DeleteOtherRelatedIfReject_'.$booking_id] = $lebooking->db_db_query($sql);
		
//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET PIC = $UserID, ProcessDate = NOW(), BookingStatus = -1 WHERE BookingID = $booking_id";
//				$lebooking->db_db_query($sql);
	}
	
	## if all the related resource is processed, send mail notice
//	if($lebooking->Check_All_Related_Booking_Request_Is_Processed($booking_id))
//	{
//		$sent_email_result = $lebooking->Email_Booking_Result($booking_id);
//	}
	
	if($lebooking->Check_All_Related_Booking_Request_Is_Processed($booking_id))
	{
		//2013-0910-1707-32073
		//$sent_email_result = $lebooking->Email_Booking_Result($booking_id);
		$emailBookingIdAry[] = $booking_id;
		
		$sql = "
			SELECT
				COUNT(*), COUNT(IF(BookingStatus = -1,1,NULL))
			FROM		
				INTRANET_EBOOKING_BOOKING_DETAILS
			WHERE 
				BookingID = '$booking_id'
		";
		list($TotalNumOfBookingResource, $TotalNumOfRejectedBooking) =  current($lebooking->returnArray($sql));
		
		if($TotalNumOfBookingResource == $TotalNumOfRejectedBooking)
		{
			$sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$booking_id'";
			$targetEventID = $lebooking->returnVector($sql);
			
			$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$targetEventID[0]."'";
			$result['DeleteCalendarEventEntry_'.$booking_id] = $lebooking->db_db_query($sql);
			
			$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = 0 WHERE BookingID = '$booking_id'";
			$result['UpdateEventRelation_'.$booking_id] = $lebooking->db_db_query($sql);
		}
	}
	
//	if(!in_array(false,$result) && $sent_email_result){
	if(!in_array(false,$result)){
		$lebooking->Commit_Trans();
		$OverallResult[] = true;
	}else{
		$lebooking->RollBack_Trans();
		$OverallResult[] = false;
	}
}

$emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
if (count($emailBookingIdAry) > 0) {
	// return true even failed to send email
	$SuccessArr['Send_eMail'] = $lebooking->Email_Booking_Result_Merged($emailBookingIdAry);
}


if ($plugin['door_access']) {
	$bookingIdAry = array_keys($BookingRecord);
	$OverallResult['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($bookingIdAry);
}

//$arrTempBookingID = explode(",",$str_BookingID);
//$arrTempBookingID = array_unique($arrTempBookingID);
//
//foreach($arrTempBookingID as $key=>$val)
//{
//	$arrBookingID[] = $val;
//}
//
//$OverallResult = array();
//
//if(sizeof($arrBookingID) > 0)
//{
//	for($i=0; $i<sizeof($arrBookingID); $i++)
//	{
//		$result = array();
//		$lebooking->Start_Trans();
//		
//		$booking_id = $arrBookingID[$i];
//		
//		$arrBookingDetails = ${$booking_id."BookingRecord"};
//				
//		if(is_array($arrBookingDetails))
//		{
////			$TotalNumOfBookingDetail = $NumOfRoomBooking[0] + $NumOfItemBooking[0];			
//			for($j=0; $j<sizeof($arrBookingDetails); $j++)
//			{
//				if(strpos($arrBookingDetails[$j],"ROOM_") === 0)
//				{
//					$room_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
//					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = $UserID, ProcessDate = NOW(), BookingStatus = -1 WHERE FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id AND BookingID = $booking_id";				
//				}
//				else if(strpos($arrBookingDetails[$j],"ITEM_") === 0)
//				{
//					$item_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
//					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = $UserID, ProcessDate = NOW(), BookingStatus = -1 WHERE FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id AND BookingID = $booking_id";
//				}
//				else
//				{
//				}
//				$result['UpdateRecordDetails_'.$i.'_'.$j] = $lebooking->db_db_query($sql);
//			}
//			
//			### reject others if the DeleteOtherRelatedIfReject is ticked before. 
//			$sql = "SELECT DeleteOtherRelatedIfReject FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $booking_id";
//			$DeleteOtherRelated = $lebooking->returnVector($sql);
//			if($DeleteOtherRelated[0] > 0){
//				$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = $UserID, ProcessDate = NOW(), BookingStatus = -1 WHERE BookingID = $booking_id";
//				$lebooking->db_db_query($sql);
//				
////				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET PIC = $UserID, ProcessDate = NOW(), BookingStatus = -1 WHERE BookingID = $booking_id";
////				$lebooking->db_db_query($sql);
//			}
//			
//			## if all the related resource is processed, send mail notice
//			if($lebooking->Check_All_Related_Booking_Request_Is_Processed($booking_id))
//			{
//				$sent_email_result = $lebooking->Email_Booking_Result($booking_id);
//			}
//			
//			if($lebooking->Check_All_Related_Booking_Request_Is_Processed($booking_id))
//			{
////				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = $booking_id";
////				$arrTotalNumOfRoomDetail = $lebooking->returnVector($sql);
////				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = $booking_id";
////				$arrTotalNumOfItemDetail = $lebooking->returnVector($sql);
////				$TotalNumOfBookingResource = $arrTotalNumOfRoomDetail[0] + $arrTotalNumOfItemDetail[0];
////				
////				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = $booking_id AND BookingStatus = -1";
////				$arrNumOfRejectedRoomBooking = $lebooking->returnVector($sql);
////				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = $booking_id AND BookingStatus = -1";
////				$arrNumOfRejectedItemBooking = $lebooking->returnVector($sql);
////				$TotalNumOfRejectedBooking = $arrNumOfRejectedRoomBooking[0] + $arrNumOfRejectedItemBooking[0];
//				$sql = "
//					SELECT
//						COUNT(*), COUNT(IF(BookingStatus = -1,1,NULL))
//					FROM		
//						INTRANET_EBOOKING_BOOKING_DETAILS
//					WHERE 
//						BookingID = $booking_id
//				";
//				list($TotalNumOfBookingResource, $TotalNumOfRejectedBooking) =  $lebooking->returnArray($sql);
//				
//				if($TotalNumOfBookingResource == $TotalNumOfRejectedBooking)
//				{
//					$sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = $booking_id";
//					$targetEventID = $lebooking->returnVector($sql);
//					
//					$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = ".$targetEventID[0];
//					$lebooking->db_db_query($sql);
//					
//					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = 0 WHERE BookingID = $booking_id";
//					$lebooking->db_db_query($sql);
//				}
//			}
//		}
//		
//		if(!in_array(false,$result) && $sent_email_result){
//			$lebooking->Commit_Trans();
//			$OverallResult[] = true;
//		}else{
//			$lebooking->RollBack_Trans();
//			$OverallResult[] = false;
//		}
//	}
//}

intranet_closedb();
if(!in_array(false,$OverallResult))
{
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedSuccessfully']));
	exit();
}else
{
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedFailed']));
	exit();
}
/*
if(!in_array(false,$result))
{
	$lebooking->Commit_Trans();
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedSuccessfully']));
	exit();
}
else
{
	$lebooking->RollBack_Trans();
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedFailed']));
	exit();
}

intranet_closedb();
*/
?>