<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-12	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$limport = new libimporttext();
$lfs = new libfilesystem();
//$luser = new libuser($_SESSION['UserID']);

#################################### CSV Checking(Start) ####################################
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lfs->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: reserve_import_step1.php?xmsg=import_failed");
	exit();
}

### move to temp folder first for others validation
$folderPrefix = $intranet_root."/file/import_temp/eBooking/";
if (!file_exists($folderPrefix)) {
	$lfs->folder_new($folderPrefix);
}
$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$targetFilePath = stripslashes($folderPrefix."/".$targetFileName);
$successAry['MoveCsvFileToTempFolder'] = $lfs->lfs_move($csvfile, $targetFilePath);

$columnTitleAry = $lebooking->getImportHeader();
$columnPropertyAry = $lebooking->getImportColumnProperty();
$csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $columnTitleAry, $columnPropertyAry);
$csvColName = array_shift($csvData);
$numOfData = count($csvData);

#################################### CSV Checking(End) ####################################

### iFrame for validation
//$thisSrc = "ajax_task.php?task=ValidateImport&targetFilePath=".$targetFilePath;
$thisSrc = "ajax_task.php?task=ValidateImport&targetFilePath=".$targetFilePath;
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";


### PageInfo
$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_BookingRequest";
$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] =array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);

### Block UI Msg
$processingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfData, $Lang['General']['ImportArr']['RecordsValidated']);

### Top Info Table
$x = '';
$x .= '<table class="form_table_v30">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td><div id="SuccessCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td><div id="FailCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;

### Buttons
$htmlAry['importBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "goImport();", 'ImportBtn', '', $Disabled=1);
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");

$linterface->LAYOUT_START();

?>
<script type="text/javascript">
$(document).ready( function() {
	
	Block_Document('<?=$processingMsg?>');
	
});

var needOverWriteRecord = 0;	// used in ajax_task.php task:ValidateImport
function goCancel() {
	window.location = 'booking_request.php';
}

function goBack() {
	window.location = 'reserve_import_step1.php';
}

function goImport() {
	if(needOverWriteRecord == 1){
		if( confirm('<?= $Lang['eBooking']['ReserveBookingAdminOverwriteConfirm'] ?>') ){
			$('form#form1').attr('action', 'reserve_import_step3.php').submit();	
		}
	}
	else if(needOverWriteRecord == 0){
		$('form#form1').attr('action', 'reserve_import_step3.php').submit();	
	}
}

</script>
<form id="form1" name="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<?=$htmlAry['iframe']?>
		<div id="ErrorTableDiv"></div>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['importBtn']?>
		<?=$htmlAry['backBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>