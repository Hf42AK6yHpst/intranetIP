<?php
// using : 
/*
 *  2019-05-03 Cameron
 *      - export data enclosed by double quotation mark
 *      - fix: passs Keyword search filter
 *      
 *  2019-04-30 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$li = new libfilesystem();
$lexport = new libexporttext();

$CurrentPageArr['eBooking'] = 1;

// Empty row
$Rows[] = array('');

$CategoryID = IntegerSafe($CategoryID);
$SubCategoryID = IntegerSafe($SubCategoryID);

$sql = "SELECT CONCAT(".$linventory->getInventoryNameByLang("cat.").",' > ',".$linventory->getInventoryNameByLang("subcat.").") FROM INVENTORY_CATEGORY as cat INNER JOIN INVENTORY_CATEGORY_LEVEL2 as subcat ON (cat.CategoryID = subcat.CategoryID) WHERE cat.CategoryID = '$CategoryID' AND subcat.Category2ID = '$SubCategoryID'";
$arrCategory = $lebooking_ui->returnVector($sql);
if(sizeof($arrCategory) > 0){
	$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['Category'],
						$arrCategory[0]);
}
$Rows[] = $tempRows;

if($ManagementGroupID == "")
{
	$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
						$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup']);
} else {
	if($ManagementGroupID != "-999"){
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		$sql = "SELECT GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE GroupID = '$ManagementGroupID'";
		$arrMgmtGroupInfo = $lebooking_ui->returnVector($sql);
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup']);
	}else{
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$arrMgmtGroupInfo[0]);
	}
}
$Rows[] = $tempRows;

$BookingStatus = IntegerSafe($BookingStatus);
$BookingStatusArr = explode(",",$BookingStatus); 
$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";

if($show_wait){
	$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'];
}
if($show_approve){
	$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'];
}
$str_booking_status = implode(" , ",$booking_status_content);
$tempRows = array($Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'],
						$str_booking_status);
$Rows[] = $tempRows;

$BookingDateType = cleanHtmlJavascript($BookingDateType);
switch ($BookingDateType)
{
	case "Future":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllComingRequest'];
	break;
	
	case "ThisWeek":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisWeek'];
	break;
	
	case "NextWeek":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextWeek'];
	break;
	
	case "ThisMonth":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisMonth'];
	break;
	
	case "NextMonth":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextMonth'];
	break;
	
	case "Past":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['PastRequest'];
	break;
}
$tempRows = array($Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['RequestDate'],
					$DateType_info);
$Rows[] = $tempRows;

// Empty row
$Rows[] = array('');

if($ItemID == ""){
	$ItemID = "ALL";
}
else {
    $ItemID = IntegerSafe($ItemID);
}
$Keyword = cleanHtmlJavascript(rawurldecode($_GET['Keyword']));

$exportColumn = array($Lang['eBooking']['Management']['FieldTitle']['ItemBooking'],'','','','','','','','','','','','','','');

// csv main content
$csv_content = $lebooking_ui->Show_Item_Booking_List_Export($CategoryID,$SubCategoryID,$ItemID,$ManagementGroupID,$BookingStatus,$BookingDateType, $pageNo = "", $order = "", $numPerPage = "", $Keyword);

// combine additional information and main content
$final_csv_content = array_merge($Rows,$csv_content);

$export_content = $lexport->GET_EXPORT_TXT($final_csv_content, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=0);

$filename = "item_booking_list".date('Y-m-d').".csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>