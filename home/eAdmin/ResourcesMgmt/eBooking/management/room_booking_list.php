<?php
// using : 
################################################################################################
##
##  Date    :   2019-05-03 Cameron
##              - fix: Keyword search in this page, print and export
##              - fix: get FacilityID value based on GET or POST method
##
##  Date    :   2019-05-01 Cameron
##              fix potential sql injection problem or cross site scripting by cast related variables to integer
##
##  Date    :   2019-02-28 Isaac
##              modified js_Show_Booking_Detail_Layer() to return both Booking Remark and Attachment
##
##	Date	:	2017-11-10 Simon #
##				# modified $li to $lebooking
##
################################################################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_RoomBooking";
$linterface 	= new interface_html();
$lebooking		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['RoomBooking']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

if( isset($BookStatus) && (sizeof($BookStatus)>0) )
{
	$BookingStatusList = implode(",", $BookStatus);
}
else
{
	if(isset($_POST['BookingStatus'])){
		$BookingStatusList = $_POST['BookingStatus'];
	}else if(isset($_GET['BookingStatus'])){
		$BookingStatusList = $_GET['BookingStatus']; 
	}else{
		$BookingStatusList = isset($BookingStatusList)? $BookingStatusList : '1,0';
	}
}

if(isset($_GET['LocationID']) && ($_SERVER['REQUEST_METHOD'] === 'GET')){     // from clicking List tab
    $FacilityID = ($_GET['LocationID'] == 'ALL') ? 'ALL' : IntegerSafe($_GET['LocationID']);
}
else if (isset($_POST['FacilityID'])){
    $FacilityID = ($_POST['FacilityID'] == 'ALL') ? 'ALL' : IntegerSafe($_POST['FacilityID']);
}
else {
    $FacilityID = 'ALL';    // default
}

if(isset($_GET['ManagementGroupID'])){
	$ManagementGroup = $_GET['ManagementGroupID'];
}

# Set Keyword
$Keyword = cleanHtmlJavascript(trim($Keyword));
$Keyword = $Keyword!=''? stripslashes($Keyword): '';

$BookingStatusList = IntegerSafe($BookingStatusList);
$ManagementGroup = IntegerSafe($ManagementGroup);

# Default Table Settings

// $pageNo = ($pageNo == '')? $li->pageNo=1 : $li->pageNo=$pageNo;
// $numPerPage = ($numPerPage == '')? $li->page_size=20 : $li->page_size=$numPerPage;
$pageNo = ($pageNo == '')? 1 : $pageNo;
$numPerPage = ($num_per_page == '')? 20 : $num_per_page;
$Order = ($Order == '')? 1 : $Order;
$SortField = ($SortField == '')? 0 : $SortField;

echo $lebooking_ui->Get_Room_Booking_List_UI($FacilityID, $ManagementGroup, $BookingStatusList, $BookingDate, $WeekStartTimeStamp, $From_eService=0, $Keyword);
echo $lebooking_ui->initJavaScript();
?>

<script language='JavaScript'>
	var CurrentView = "";
		
	$(document).ready( function() {
		CurrentView = "ListView";
		jsChangeLocation();
	});
	
	function jsChangeLocation()
	{
		var task = "ShowRoomBookingRecordInListView";
		Block_Document();
		
		<? if($BookingDate != "") { ?>
			var BookingDateType = "<?=$BookingDate;?>";
		<? } else { ?>
			var BookingDateType = $("#BookingDate").val();
		<? } ?>
		
		$.post(
			"ajax_task.php",
			{
				task : task,
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val(),
				BookingDateType : BookingDateType,
				pageNo : <?=$pageNo;?>,
				order : <?=$Order;?>,
				numPerPage : <?=$numPerPage;?>,
				Keyword: $("#Keyword").val()
			},
			function (responseData)
			{
				$("#DIV_RoomBooking").html(responseData);
				$("#DIV_RoomBooking").show();
				UnBlock_Document();
				initThickBox();
			}
		);
	}
		
	function UpdateStatusList()
	{
		var StatusListAry = new Array();
		var check_box_checked = 0;
	
		$("input.StatusCheckBox:checked").each(function(){
			StatusListAry.push($(this).val());
			check_box_checked++;
		});
	
		$("#BookingStatusList").val(StatusListAry.join(","));
			
		if(check_box_checked == 0){
			alert("<?=$Lang['eBooking']['Management']['General']['JSWarning']['PleaseSelectStatus'];?>");
		}else{
			refreshBookRecord();
		}
	}
	
	function refreshBookRecord()
	{
		MM_showHideLayers('status_option','','hide');
		jsChangeLocation();
	}
	
	function ChangeView(val)
	{
		if(val == "WeekView")
		{
			var LocationID = $("#FacilityID").val();
			var ManagementGroupID = $("#ManagementGroup").val();
			var BookingStatus = $("#BookingStatusList").val();
			var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
			
			//window.location = "room_booking_week.php?clearCoo=1";
			var param = '?LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus + '&WeekStartTimeStamp=' + WeekStartTimeStamp;
			window.location = "room_booking_week.php" + param;
		}
		else if(val == "ListView")
		{
			CurrentView = "ListView";
			$("div#caltabs_right").load(
				"ajax_task.php",
				{
					task : "ChangeSelectedTab",
					ViewMode : "ListView"
				},
				function(returnString)
				{
					jsChangeLocation();
				}
			);
		}
	}

	function js_Show_Booking_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID)
	{
		js_Hide_Booking_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'RoomBookingRemark')
			jsAction = 'Reload_Remark';
// 		else
// 			jsAction = 'Reload_RejectReason';
		
		$('div#BookingDetailContentDiv').html('<?=$Lang['General']['Loading']?>');	
		js_Change_Layer_Position(jsClickedObjID);
		MM_showHideLayers('BookingDetailLayer','','show');
				
		$('div#BookingDetailContentDiv').load(
			"ajax_reload.php", 
			{ 
				Action: jsAction,
				BookingID: jsBookingID
			},
			function(returnString)
			{
				$('div#BookingDetailContentDiv').css('z-index', '999');
			}
		);
	}
	
// 	function js_Show_Booking_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID)
// 	{
// 		js_Hide_Booking_Detail_Layer();
		
// 		var jsAction = '';
// 		if (jsDetailType == 'RoomBookingRemark')
// 			jsAction = 'ShowRoomBookingRemark';
		
		$('div#BookingDetailContentDiv').html('<?=$Lang['General']['Loading']?>');	
// 		js_Change_Layer_Position(jsClickedObjID);
// 		MM_showHideLayers('BookingDetailLayer','','show');
				
// 		$('div#BookingDetailContentDiv').load(
// 			"ajax_task.php", 
// 			{
// 				task: jsAction,
// 				BookingID: jsBookingID
// 			},
// 			function(returnString)
// 			{
// 				$('div#BookingDetailContentDiv').css('z-index', '999');
// 			}
// 		);
// 	}
	
	function js_Hide_Booking_Detail_Layer()
	{
		MM_showHideLayers('BookingDetailLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID) 
	{
		var jsOffsetLeft, jsOffsetTop;
		
		jsOffsetLeft = 285;
		jsOffsetTop = -20;
			
		var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
				
		document.getElementById('BookingDetailLayer').style.left = posleft + "px";
		document.getElementById('BookingDetailLayer').style.top = postop + "px";
		document.getElementById('BookingDetailLayer').style.visibility = 'visible';
	}
	
	function js_Print_RoomBooking_List(SubmitPage)
	{
		var LocationID = $("#FacilityID").val();
		var ManagementGroupID = $("#ManagementGroup").val();
		var BookingStatus = $("#BookingStatusList").val();
		<? if($BookingDate != "") { ?>
			var BookingDateType = "<?=$BookingDate;?>";
		<? } else { ?>
			var BookingDateType = $("#BookingDate").val();
		<? } ?>
		var Keyword = $("#Keyword").val();
				
		var param = '?LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus + '&BookingDateType=' + BookingDateType + '&Keyword='+encodeURIComponent(Keyword);
		var SubmitPage = SubmitPage || "room_booking_list_print.php"
		var url = SubmitPage + param;
		newWindow(url, 10);
	}
	
	function js_Export_RoomBooking_List(SubmitPage)
	{
		var LocationID = $("#FacilityID").val();
		var ManagementGroupID = $("#ManagementGroup").val();
		var BookingStatus = $("#BookingStatusList").val();
		<? if($BookingDate != "") { ?>
			var BookingDateType = "<?=$BookingDate;?>";
		<? } else { ?>
			var BookingDateType = $("#BookingDate").val();
		<? } ?>
		var Keyword = $("#Keyword").val();
				
		var param = '?LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus + '&BookingDateType=' + BookingDateType + '&Keyword='+encodeURIComponent(Keyword);
		var SubmitPage = SubmitPage || "room_booking_list_export.php"
		var url = SubmitPage + param;
		var originalAction = document.form1.action;
		document.form1.action = url;
		document.form1.submit();
		document.form1.action = originalAction;
	}	
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>