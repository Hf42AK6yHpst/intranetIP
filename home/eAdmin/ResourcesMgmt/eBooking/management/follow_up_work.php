<?php
//using : 
/*
 *  2019-05-01 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_FollowUpWork";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['FollowUpWork']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

$FollowUpGroupList = isset($FollowUpGroupList)? IntegerSafe($FollowUpGroupList) : '';

echo $lebooking_ui->Get_Follow_Up_Work_Week_View_UI($FollowUpGroupList, $ShowRemark, $WeekStartTimeStamp);
echo $lebooking_ui->initJavaScript();
?>

<script language='JavaScript'>
		
	$(document).ready( function() {
		
		$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar', mandatory: true});
		$('#WeekStartDate').datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0
		});
		
		jsReloadFollowUpWork();
	});
	
	function jsReloadFollowUpWork()
	{
		var task = "ShowFollowUpWorkInWeekView";
		
		if($('#RemarkSelection').is(':checked')) {
			var RemarkSelection = 1;
			
		} else {
			var RemarkSelection = 0;
		}	
		Block_Document();
		
		$.post(
			"ajax_task.php",
			{
				task : task,
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				FollowUpGroup : $("#FollowUpGroupList").val(),
				RemarkSelection : RemarkSelection 
			},
			function (responseData)
			{
				if(RemarkSelection == 1)
					$('#RemarkSelection').attr("checked",true);
				else
					$('#RemarkSelection').attr("checked",false);
					
				$("#DIV_RoomBooking").html(responseData);
				$("#DIV_RoomBooking").show();
				UnBlock_Document();
				initThickBox();
			}
		);
	}
		
	function UpdateFollowUpGroup()
	{
		document.form1.action = '';
		var FollowUpGroupAry = new Array();
	
		$("input.FollowUpGroupCheckBox:checked").each(function(){
			FollowUpGroupAry.push($(this).val())
		});
	
		$("#FollowUpGroupList").val(FollowUpGroupAry.join(","));
	}
	
	function refreshFollowUpWork()
	{
		MM_showHideLayers('follow_up_group_option','','hide');
		document.form1.submit();
	}
	
	function jsChangeWeek()
	{
		$.post(
			"ajax_task.php",
			{
				task : "ChangeDisplayWeekByCal",
				WeekStartDate : $("#WeekStartDate").val(),
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsReloadFollowUpWork();
			}
		);
	}
	
	function NextWeek()
	{
		$.post(
			"ajax_task.php",
			{
				task : "ChangeDisplayWeek",
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				ChangeValue : 7,
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsReloadFollowUpWork();
			}
		);
	}
	
	function PrevWeek()
	{
		$.post(
			"ajax_task.php",
			{
				task : "ChangeDisplayWeek",
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				ChangeValue : -7,
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsReloadFollowUpWork();
			}
		);
	}
	
	function js_Show_FollowUp_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID)
	{
		js_Hide_FollowUp_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'FollowUpDetail')
			jsAction = 'ShowFollowUpDetail';
		
		$('div#FollowUpDetailContentDiv').html('<?=$Lang['General']['Loading']?>');	
		js_Change_Layer_Position(jsClickedObjID);
		MM_showHideLayers('FollowUpDetailLayer','','show');
				
		$('div#FollowUpDetailContentDiv').load(
			"ajax_task.php", 
			{ 
				task: jsAction,
				BookingID: jsBookingID
			},
			function(returnString)
			{
				$('div#FollowUpDetailContentDiv').css('z-index', '999');
			}
		);
	}
	
	function js_Hide_FollowUp_Detail_Layer()
	{
		MM_showHideLayers('FollowUpDetailLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID) 
	{
		var jsOffsetLeft, jsOffsetTop;
		
		jsOffsetLeft = 165;
		jsOffsetTop = -50;
			
		var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
				
		document.getElementById('FollowUpDetailLayer').style.left = posleft + "px";
		document.getElementById('FollowUpDetailLayer').style.top = postop + "px";
		document.getElementById('FollowUpDetailLayer').style.visibility = 'visible';
	}
	
	function js_Print_FollowUp_Work()
	{
		var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
		var FollowUpGroup = $("#FollowUpGroupList").val();
		if($('#RemarkSelection').is(':checked')) {
			var RemarkSelection = 1;
			
		} else {
			var RemarkSelection = 0;
		}	
		
		if(FollowUpGroup != "") {
			var param = '?WeekStartTimeStamp=' + WeekStartTimeStamp + '&FollowUpGroup=' + FollowUpGroup + '&RemarkSelection=' + RemarkSelection;
			var url = "follow_up_work_print.php" + param;
			newWindow(url, 10);
		} else {
			alert("<?=$Lang['eBooking']['Management']['FollowUpWork']['WarningArr']['SelectFollowUpGroup'];?>");
		}
		
	}
	
	function js_Export_FollowUp_Work()
	{
		var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
		var FollowUpGroup = $("#FollowUpGroupList").val();
		if($('#RemarkSelection').is(':checked')) {
			var RemarkSelection = 1;
			
		} else {
			var RemarkSelection = 0;
		}	
		
		if(FollowUpGroup != "") {
			//var param = '?WeekStartTimeStamp=' + WeekStartTimeStamp + '&FollowUpGroup=' + FollowUpGroup + '&RemarkSelection=' + RemarkSelection;
			//var url = "follow_up_work_export.php" + param;
			var url = "follow_up_work_export.php";
			document.form1.action = url;
			document.form1.submit();
		} else {
			alert("<?=$Lang['eBooking']['Management']['FollowUpWork']['WarningArr']['SelectFollowUpGroup'];?>");
		}
		
	}
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>