<?php
// Using : 
/*
 *  2019-05-01 Cameron
 *      - apply IntegerSafe to ID fields to fix sql injection problem
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_BookingRequest";
$linterface 	= new interface_html();
//$linventory 	= new libinventory();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RejectBooking']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($returnMsg);

$str_BookingID = IntegerSafe($str_BookingID);
$arrTempBookingID = explode(",",$str_BookingID);
$arrTempBookingID = array_unique($arrTempBookingID);

$BookingFacilityAssoc = array();
$RoomIDArr = array();
$ItemIDArr = array();
foreach($arrTempBookingID as $key=>$booking_id)
{
	$arrBookingID[] = $booking_id;
	$arrBookingDetails = ${$booking_id."BookingRecord"};
	if(is_array($arrBookingDetails))
	{
		for($j=0; $j<sizeof($arrBookingDetails); $j++)
		{
			if(strpos($arrBookingDetails[$j],"ROOM_") === 0)
			{
				$room_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
				$BookingFacilityAssoc[$booking_id]['Room'][] = $room_id;
				$RoomIDArr[] = $room_id;
			}
			else if(strpos($arrBookingDetails[$j],"ITEM_") === 0)
			{
				$item_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
				$BookingFacilityAssoc[$booking_id]['Item'][] = $item_id;
				$ItemIDArr[] = $item_id;
			}
		}
	}
}

echo $lebooking_ui->Get_Booking_Status_Reject_Confirm_UI($BookingFacilityAssoc, $RoomIDArr, $ItemIDArr);
	
?>
<script>
function js_Assign_All_Reason()
{
	$("input.reason").val($("input#AssignReason").val());
}

var submitted = false;
function Submit(){
	if(!submitted){
		submitted = true;
		$('form#form1').attr('action', 'booking_status_reject.php').submit();
		$('#submit_button').attr('disabled',true);
	}
	else{
		return false;
	}
}
</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>