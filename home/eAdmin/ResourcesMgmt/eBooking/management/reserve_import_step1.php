<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-12	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_auth();
intranet_opendb();

### PageInfo
$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_BookingRequest";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$liblocation = new Building();

$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] =array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);


### Import Format
$ColumnTitleArr[] = $Lang['SysMgr']['Location']['Building'];
$ColumnTitleArr[] = $Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Location'];
$ColumnTitleArr[] = $Lang['SysMgr']['Location']['Room'];
$ColumnTitleArr[] = $Lang['eBooking']['General']['Export']['FieldTitle']['Date'];
$ColumnTitleArr[] = $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['StartTime'];
$ColumnTitleArr[] = $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['EndTime'];
$ColumnTitleArr[] = $Lang['General']['Remark'];
$ColumnTitleArr[] = $Lang['eBooking']['OccupiedBy'];
$ColumnPropertyArr = array(1,1,1,1,1,1,3,3);
$RemarksArr[3] = "<span class='tabletextremark'>".$Lang['eBooking']['Import']['Remarks']['Date']."</span>";
$RemarksArr[4] = "<span class='tabletextremark'>".$Lang['eBooking']['Import']['Remarks']['Time']."</span>";
$RemarksArr[5] = "<span class='tabletextremark'>".$Lang['eBooking']['Import']['Remarks']['Time']."</span>";
$RemarksArr[7] = "<span class='tabletextremark'>".$Lang['eBooking']['Import']['Remarks']['User']."</span>";

$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
$htmlAry['columnRemarks'] = $ImportPageColumn;

### Get CSV Button
$csvFile = "<a class='tablelink' href='". 'reserve_import_template.php' ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

### Building Selector
$AllBuildingArr = $liblocation->Get_All_Building();
$BuildingFilterInfoAssoArr = array();
foreach($AllBuildingArr as $_BuildingInfo){
	$_BuildingCode = $_BuildingInfo['Code'];
	$_BuildingName = Get_Lang_Selection($_BuildingInfo['NameChi'],$_BuildingInfo['NameEng']);
	$BuildingFilterInfoAssoArr[$_BuildingCode] = $_BuildingName;
}
$building_selection = getSelectByAssoArray($BuildingFilterInfoAssoArr,'name="BuildingCode" id="BuildingCode" onchange=""',$LocationID,0,0,$Lang['SysMgr']['Location']['LocationList']);//Omas

### form table
$x = '';
$x .= '<table id="html_body_frame" width="100%">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
					$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
					$x .= '<td>'.$building_selection.'<a class="tablelink" href="javascript:goDownloadSample();">'. $Lang['General']['ClickHereToDownloadSample'] .'</a></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'."\n";
						$x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']."\n";
					$x .= '</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $htmlAry['columnRemarks']."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="tabletextremark" colspan="2">'."\n";
						$x .= $linterface->MandatoryField();
						//$x .= $linterface->ReferenceField();
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['continueBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Continue'], "button", "goSubmit()", 'sumbitBtn');
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = 'booking_request.php';
}

function goSubmit() {
	if(checkForm()==true){
		$('form#form1').attr('action', 'reserve_import_step2.php').submit();	
	}
}

function goDownloadSample() {
	$('form#form1').attr('action', 'reserve_import_template.php').submit();
}

function checkForm(){
	if($("#csvfile").val() == ''){
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}
	else{
		return true;
	}
	
}
</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['continueBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>