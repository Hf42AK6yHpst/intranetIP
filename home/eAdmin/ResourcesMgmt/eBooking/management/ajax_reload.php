<?php
// using: 

################################################## Change Log ##################################################
##
##  Date    :   2019-05-01 (Cameron)
##              - fix potential sql injection problem by enclosed var with apostrophe
##
##  Date    :   2019-03-04 (Isaac)
##              Added a new case "reloadMgmtGroupLayer"
##
################################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

$lebooking = new libebooking_ui();

intranet_auth();
intranet_opendb();

$BookingID= IntegerSafe($BookingID);
$FacilityType = IntegerSafe($FacilityType);
$FacilityID = IntegerSafe($FacilityID);

if($Action == 'Reload_Remark')
{
//	$sql = "SELECT Remark FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
//	$arrRemark = $lebooking->returnArray($sql,1);
//	
//	$layer_content .= "<table border='0' cellpadding='3' cellspacing='0' width='100%'>";
//	$layer_content .= "<tr class='tabletop'>";
//	$layer_content .= "<td>".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['Remark']."</td>";
//	$layer_content .= "</tr>";
//	if(sizeof($arrRemark) > 0)
//	{
//		for($i=0; $i<sizeof($arrRemark); $i++)
//		{
//			list($remark) = $arrRemark[$i];
//			$layer_content .= "<tr class='tablerow1'><td>".nl2br(htmlspecialchars($remark))."</td></tr>";
//		}
//	}
//	$layer_content .= "<table>";
	$layer_content  = $lebooking->Get_My_Booking_Record_Detail_Layer($BookingID);
}
else if($Action == 'Reload_RejectReason')
{
//	$sql = "SELECT Remark FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
//	$arrRemark = $lebooking->returnArray($sql,1);
//	
//	$layer_content .= "<table border='0' cellpadding='3' cellspacing='0' width='100%'>";
//	$layer_content .= "<tr class='tabletop'>";
//	$layer_content .= "<td>".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['Remark']."</td>";
//	$layer_content .= "</tr>";
//	if(sizeof($arrRemark) > 0)
//	{
//		for($i=0; $i<sizeof($arrRemark); $i++)
//		{
//			list($remark) = $arrRemark[$i];
//			$layer_content .= "<tr class='tablerow1'><td>".nl2br(htmlspecialchars($remark))."</td></tr>";
//		}
//	}
//	$layer_content .= "<table>";
	$layer_content  = $lebooking->Get_My_Booking_Record_Reject_Reason_Layer($BookingID, $FacilityType, $FacilityID);
} else if($Action == "reloadMgmtGroupLayer"){
        $FacilityType = $_POST['FacilityType'];
        
        if($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM){
            $itemID = IntegerSafe(stripslashes($_POST['FacilityID']));
            $itemName = stripslashes($_POST['FacilityName']);
            $layer_content  = $Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'].' - '.$itemName;
            $layer_content .= $lebooking->Get_Item_MgmtGroup_Layer_Table($itemID);
            
        }else if($FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM){
            include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
            $linterface = new interface_html();
            
            $roomName = stripslashes($_POST['FacilityName']);
            $roomID = IntegerSafe(stripslashes($_POST['FacilityID']));
            $layer_content= $Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'].' - '.$roomName;
            $layer_content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
            $layer_content .= "<tbody>";
//             $layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
            $layer_content .= "<tr class='tabletop'>";
            $layer_content .= "<td>#</td>";
            $layer_content .= "<td>".$Lang['eBooking']['Settings']['ManagementGroup']['GroupName']."</td>";
            $layer_content .= "<td>".$Lang['eBooking']['Settings']['ManagementGroup']['Description']."</td>";
            $layer_content .= "</tr>";
            
            $sql = "SELECT management_group.GroupID, management_group.GroupName, management_group.Description FROM INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS relation INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS management_group ON (relation.GroupID = management_group.GroupID) WHERE relation.LocationID = '$roomID'";
            $arr_group_result = $lebooking->returnArray($sql,3);
//             echo $sql;
            if(sizeof($arr_group_result)>0){
                for($a=0; $a<sizeof($arr_group_result); $a++){
                    list($group_id, $group_name, $group_desc) = $arr_group_result[$a];
                    $layer_content .= "<tr class='row_approved'>";
                    $layer_content .= "<td>".($a+1)."</td>";
                    $layer_content .= "<td>".$group_name."</td>";
                    $layer_content .= "<td>".$group_desc."</td>";
                    $layer_content .= "</tr>";
                }
            } else {
                $layer_content .= "<tr><td colspan='3' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
            }
            $layer_content .= "<tr height='10px'>";
            $layer_content .= "<td></td>";
            $layer_content .= "</tr>";
            $layer_content .= "<tr>";
            $layer_content .= "<td align='center' colspan='3' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
            $layer_content .= "</tr>";
            $layer_content .= "</tbody>";
            $layer_content .= "</table>";
        }
}
echo $layer_content;

intranet_closedb();
?>