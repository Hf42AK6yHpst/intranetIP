<?php
// using :
/****************************************************** Change Log ******************************************************
 * 2019-06-27 (Cameron): hide "Specific Time" Booking Method if Booking Type is "Periodic" for $sys_custom['eBooking']['EverydayTimetable']  
 * 2019-05-01 (Cameron): fix potential sql injection problem by cast related variables to integer
 *                       assign date value to today if they are not in valid format to prevent cross site scripting attach 
 * 2018-12-31 (Cameron): fix bug by applying parseInt() to year and month in ChangeMonth() to avoid wrongly type cast
 * 2018-08-15 (Henry): 	fixed sql injection
 * 2016-9-14 (Villa):	keep $FacilityID in the form of (xxx, xxx, xxx) in stead of array form(cause an error using Get_New_Booking_Step1_UI)
 * 2015-10-30 (Omas):	added js for select all button
 * 2015-01-16 (Omas):	new Setting - Booking Category enable by $sys_custom['eBooking_BookingCategory']
 * 2015-01-15 (Omas):	new $sys_custom['eBooking_MultipleRooms'] - allow reserve multiple rooms for specific time method in eAdmin & eService
 * 2015-01-14 (Omas):	Add js to SelectYearMonth() by choosing the year/month directly
 * 2014-08-04 (Bill):	Refresh when change the start date - Get correct cycle day
 * 2013-08-20 (Carlos): $sys_custom['eBooking_ReserveMultipleRooms'] - allow reserve multiple rooms for specific time method
 * 2013-03-26 (Carlos): Modified js addDate(), CheckTimeCrash(), added getElementRowNumber(), removeDateByRef()
 * 						to allow booking multiple time periods for Single > Specific Time
 ************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$From_eAdmin = $_SESSION['EBOOKING_BOOKING_FROM_EADMIN']?1:0;

if($From_eAdmin)
{
	$CurrentPageArr['eBooking'] = 1;
	$CurrentPage	= "PageManagement_BookingRequest";
	$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest']);
}
else
{
	$CurrentPage	= "eService_MyBookingRecord";
	$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
	$From_eService=1;
}
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService);
$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

// if(isset($FacilityID) && $FacilityID != "" && !is_array($FacilityID) && strstr($FacilityID,',')!=FALSE) {
// 	$FacilityID = explode(",",$FacilityID);
// }else{
	$FacilityID = $FacilityID? IntegerSafe($FacilityID) : 0;
	$FacilityType = IntegerSafe($FacilityType);
// }

$lebooking = new libebooking();

$lebooking->Load_General_Setting();
$BookingType = $BookingType? $BookingType : $lebooking->SettingArr['DefaultBookingType'];
$BookingType = IntegerSafe($BookingType);
//$BookingType = $BookingType?$BookingType:1;

$StoredValueForBack = unserialize(urldecode($StoredValueForBack));

$targetBookingID = IntegerSafe($targetBookingID);
$BookingMethod = IntegerSafe($BookingMethod);
$SelectedWeekDay = IntegerSafe($SelectedWeekDay);
$SelectedCycleDay = IntegerSafe($SelectedCycleDay);
$RepeatType = IntegerSafe($RepeatType);

if (!intranet_validateDate($StartDate)) {
    $StartDate = date('Y-m-d');
}
if (!intranet_validateDate($EndDate)) {
    $EndDate = date('Y-m-d');
}

echo $lebooking_ui->initJavaScript();

// Pass parameter to get previous setting after refresh
echo $lebooking_ui->Get_New_Booking_Step1_UI($FacilityType, $FacilityID, $DateList, $targetBookingID, $From_eAdmin, $BookingMethod, $BookingType, $StoredValueForBack, $StartDate, $EndDate, (array)$SelectedWeekDay, (array)$SelectedCycleDay, $RepeatType,$BookingCategoryID);


# rebuild SelectedSlot
if($StoredValueForBack)
{
	if($BookingMethod==1) // Timeslot 
	{
		if($BookingType==1) // single
		{
			if($StoredValueForBack['SelectedSlot']) // from step3
			{
				$SelectedSlot =$StoredValueForBack['SelectedSlot'];
				$TimeslotIDArr = array_unique((array)array_values_recursive($SelectedSlot));
				$TimeSlotArr = $lebooking_ui->Get_Timeslot_By_TimetableID('',$TimeslotIDArr);
				$TimeSlotArr = BuildMultiKeyAssoc($TimeSlotArr, "TimeSlotID");
				
				foreach($SelectedSlot as $DateTimestamp => $TimeslotArr)
				{
					foreach($TimeslotArr as $thisTimeslotID)
					{
						$thisDate = date("Y-m-d",$DateTimestamp);
						
						$script.= 'js_Add_Timeslot("'.$thisDate.'", "'.$DateTimestamp.'", "'.$TimeSlotArr[$thisTimeslotID]['TimeslotDisplay'].'", "'.$thisTimeslotID.'")'."\n";
					}
					
				}	
			}
		}
	}
	else // Specific Time
	{
		if($BookingType==1) // Periodic
		{
			$SelectedDateArr = $StoredValueForBack['SelectedDateArr'];
			$FinalDateStr =  implode(",",$StoredValueForBack['FinalDateArr']);
			foreach($StoredValueForBack['SelectedDateArr'] as $thisSelectedDate)
			{
				$StartHour = $StoredValueForBack['StartHour'][$thisSelectedDate];
				$StartMin = $StoredValueForBack['StartMin'][$thisSelectedDate];
				$EndHour = $StoredValueForBack['EndHour'][$thisSelectedDate];
				$EndMin = $StoredValueForBack['EndMin'][$thisSelectedDate];
				$script.= ' addDate("'.$thisSelectedDate.'", "", "", "'.$StartHour.'", "'.$StartMin.'", "'.$EndHour.'", "'.$EndMin.'");'."\n";
			}
			$script.= 'FinalDateStr = "'.$FinalDateStr.'";'."\n";
			$script.= 'CheckTimeCrash(1);'."\n";
			
		}
	}
}

# rebuild Selected Time
$minInterval = isset($sys_custom['eBooking_booking_time_interval'])? $sys_custom['eBooking_booking_time_interval'] : 5;


?>

<script language="javascript">
var date = new Date();
var CurrMonth = date.getMonth()+1;
var CurrYear = date.getFullYear();
var SelectedAry = new Array();
var FinalDateStr = "";
var minInterval = parseInt('<?=$minInterval?>');

function checkWholeDaySlot(timestamp){
	var id = 'slot_'+timestamp+'_master';
	if(document.getElementById(id).checked){
		//$('[class^="slot_'+timestamp+'"]').attr('checked','checked');
		$('[class^="slot_'+timestamp+'"]:not(:checked)').click();
	}
	else{
		//$('[class^="slot_'+timestamp+'"]').removeAttr('checked');
		$('[class^="slot_'+timestamp+'"]:checked').click();
	}
}

function adjust(){
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf('MSIE');
    if (msie > 0) {
        // IE 10 or older => return version number
        return 6.5;
    }
    else{
		var a = $('#SelectDateLayer').width()/2;
		var b = $('#SpecificTimeSingleTable').width()/2;
		return b-a;
    }
}
function SelectYearMonth()
{
	var targetYear = $('#yearSelect').val();
	var targetMonth = $('#monthSelect').val();
	
//	var yearDiff = targetYear - CurrYear;
//	var monthDiff = targetMonth - CurrMonth;
//	var totalDiff = yearDiff * 12 + monthDiff;
	
	ChangeMonth('',targetYear,targetMonth);
	MM_showHideLayers('SelectDateLayer','','hide');
}

function ChangeMonth(prevnext, targetYear, targetMonth)
{
	targetYear = targetYear || '';
	targetMonth = targetMonth || '';
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
	if($("select#FacilityType").val() == '1' && $("input#BookingMethod2").attr("checked")){
		if($("#FacilityID\\[\\]").val()) {
			$("#CalendarView").show();
		}else
		{
			$("#CalendarView").hide();
			return false; 
		}
	}else 
<?php } ?>	
	if($("#FacilityID").val()!='')
		$("#CalendarView").show();
	else
	{
		$("#CalendarView").hide();
		return false;
	}
	
	if (targetMonth == '') {
		var newMonth = parseInt(CurrMonth)+parseInt(prevnext);
	}
	else {
		var newMonth = parseInt(targetMonth);
	}
	
	if (targetYear == '') {
		// do nth
	}
	else {
		CurrYear = parseInt(targetYear);
	}
	
	
	$.post(
		"ajax_task.php",
		{
			task:"Get_Booking_Calender",
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
			FacilityID: ($("select#FacilityType").val()=='1' && $("input#BookingMethod2").attr("checked")?($("#FacilityID\\[\\]").val()?$("#FacilityID\\[\\]").val().join(','):''): $("#FacilityID").val()),
<?php }else{ ?>
			FacilityID: $("#FacilityID").val(),
<?php } ?>
			FacilityType: $("select#FacilityType").val(),
			year:CurrYear,
			month:newMonth,
			From_eAdmin:<?=$From_eAdmin?>,
			selected: SelectedAry.toString()
		},
		function (data)
		{
			if(data)
			{
				if (targetYear == '' && targetMonth == '') {
					CurrYear = parseInt(CurrYear);
					CurrMonth = parseInt(CurrMonth);
					
					if(newMonth==13)
					{
						CurrMonth = 1;
						CurrYear += 1;
					}
					else if(newMonth==0)
					{
						CurrMonth = 12;
						CurrYear -= 1;
					}
					else
					{
						CurrMonth = newMonth;
					}
				}
				else {
					CurrYear = parseInt(targetYear);
					CurrMonth = parseInt(targetMonth);
				}

				$("div#CalendarWrapper").html(data);
				
				$('#CalendarDateTitle')
				  .css('cursor', 'pointer')
				  .click(
				    function(){
				     MM_showHideLayers('SelectDateLayer', '', 'show');
				     changeLayerPosition('SpecificTimeSingleTable', 'SelectDateLayer', adjust(), 0);
				    }
				  )
				  .hover(
				    function(){
				      //$(this).css('background', '#ff00ff');
				    },
				    function(){
				      $(this).css('background', '');
				    }
				  );
				
			}
		}
	);
}
/*
function addDate(datestr, Cycle, day, ParStartHour, ParStartMin, ParEndHour, ParEndMin)
{
	if(Cycle=='') Cycle='--';
	if($("tr#"+datestr).length==0)
	{
		var x = '';
		x +='<tr class="selectedTableTr row_approved" id="'+datestr+'">';
			x +='<td valign="top" nowrap>'+datestr+'</td>';
			x +='<td valign="top" nowrap>'+getTimeSel("Start",datestr, ParStartHour, ParStartMin)+'</td>';
			x +='<td valign="top" nowrap>'+getTimeSel("End",datestr, ParEndHour, ParEndMin)+'</td>';
			x +='<td valign="top" nowrap>&nbsp;</td>';
			x +='<td valign="top" nowrap><a href="javascript:void(0); removeDate(\''+datestr+'\')"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove"></a></td>';
			x +='<input type="hidden" name="SelectedDateArr[]" value="'+datestr+'">';
		x +='</tr>';
		
		$("table#selectedTable>tbody").append(x);
	}
	showHideSelectedTable();
}
*/
function addDate(datestr, Cycle, day, ParStartHour, ParStartMin, ParEndHour, ParEndMin)
{
	if(Cycle=='') Cycle='--';
	if($("tr#"+datestr).length==0)
	{
		var x = '';
		x +='<tr class="selectedTableTr row_approved">';
			x +='<td valign="top" nowrap>'+datestr+'</td>';
			x +='<td valign="top" nowrap>'+getTimeSel("Start","", ParStartHour, ParStartMin)+'</td>';
			x +='<td valign="top" nowrap>'+getTimeSel("End","", ParEndHour, ParEndMin)+'</td>';
			x +='<td valign="top" nowrap>&nbsp;</td>';
			x +='<td valign="top" nowrap><a href="javascript:void(0);" onclick="removeDateByRef(this);"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove"></a></td>';
			x +='<input type="hidden" name="SelectedDateArr[]" value="'+datestr+'">';
		x +='</tr>';
		
		$("table#selectedTable>tbody").append(x);
	}
	showHideSelectedTable();
}

function getTimeSel(ParName, ParKey, ParSelHour, ParSelMin, OnChange) {
	
	var ParSelHour = ParSelHour || 0;
	var ParSelMin = ParSelMin || 0;
	var OnChange = OnChange ||'';
	
	var x = '';
	x = "<select id=\""+ParName+"Hour"+ParKey+"\" name=\""+ParName+"Hour["+ParKey+"]\" class=\""+ParName+"Hour\" onchange=\""+OnChange+"\">";
	for (var i = 0; i < 24; i++) {
		(i < 10) ? temp = "0"+i : temp = i;
		x += "<option value=\""+temp+"\"";
		if (temp == ParSelHour) x += "selected";
		x += ">"+temp+"</option>";
	}
	x += "</select> : ";
	x += "<select id=\""+ParName+"Min"+ParKey+"\" name=\""+ParName+"Min["+ParKey+"]\" class=\""+ParName+"Min\" onchange=\""+OnChange+"\">";
	for (var i = 0; i < 60; i += minInterval) {
		(i < 10) ? temp = "0"+i : temp = i;
		x += "<option value=\""+temp+"\"";
		if (temp == ParSelMin) x += "selected";
		x += ">"+temp+"</option>";
	}
	x += "</select>";

	return x;
}

function js_Set_All_Time()
{
	$("select.StartHour").val($("select#SetStartHour").val());
	$("select.StartMin").val($("select#SetStartMin").val());
	$("select.EndHour").val($("select#SetEndHour").val());
	$("select.EndMin").val($("select#SetEndMin").val());
}
/*
function CheckTimeCrash(DefaultSelectedOnPageLoad)
{
	var AllowReserve = <?=$From_eAdmin?1:0?>;

	// 	check time selection 
	var DateRowArr = $("tr.selectedTableTr");
	var DateValid = true;
	var StartTimeArr = new Array(); 
	var EndTimeArr = new Array();

	if(DefaultSelectedOnPageLoad)
		var SelectedDateList =  FinalDateStr;
	else
		var SelectedDateList =  "";
	
	for(var i=0; i<DateRowArr.length; i++)
	{
		var datestr = $(DateRowArr[i]).attr("id");
		var SH =  $("#StartHour"+datestr).val()
		var SM =  $("#StartMin"+datestr).val()
		var EH =  $("#EndHour"+datestr).val()
		var EM =  $("#EndMin"+datestr).val()
		
		if(SH>EH || (SH==EH&&SM>=EM))
		{
			// highlight the date
			$(DateRowArr[i]).addClass("row_suspend").removeClass("row_approved");
			DateValid = false
		}
		else
		{
			// de-highlight the date 
			$(DateRowArr[i]).addClass("row_approved").removeClass("row_suspend");
			StartTimeArr.push(SH+":"+SM+":"+"00");
			EndTimeArr.push(EH+":"+EM+":"+"00");
		}		
	}
	if(!DateValid)
	{
		$("div#WarnInvalidTime").show();
		return false;		
	}
	else
	{
		$("div#WarnInvalidTime").hide();
	}

	// build DateStr
	var SelectedDateArr = new Array();
	$("input[name='SelectedDateArr[]']").each(function(){
		SelectedDateArr.push($(this).val());
	});
	
	Block_Element("TimeCheckList");
	$.post(
		"ajax_task.php",
		{
			task:"CheckTimeCrash",
			FacilityType:$("select#FacilityType").val(),
			FacilityID:$("#FacilityID").val(),
			DateList: SelectedDateArr.join(","),
			StartTimeStr: StartTimeArr.join(","),
			EndTimeStr: EndTimeArr.join(","),
			AllowReserve: AllowReserve,
			SelectedDateList: SelectedDateList
		},
		function (data)
		{
//			$("#SelectedSlotTableDiv").html(data)
			$("div#TimeCheckList").html(data);
			UnBlock_Element("TimeCheckList");
//			$("input#SpecificTimeRangeArr").val(SH+":"+SM+":00_"+EH+":"+EM+":00");
		}
	
	);
}
*/

function CheckTimeCrash(DefaultSelectedOnPageLoad)
{
	var AllowReserve = <?=$From_eAdmin?1:0?>;

	// 	check time selection 
	var DateRowArr = $("tr.selectedTableTr");
	var DateValid = true;
	var StartTimeArr = new Array(); 
	var EndTimeArr = new Array();

	if(DefaultSelectedOnPageLoad)
		var SelectedDateList =  FinalDateStr;
	else
		var SelectedDateList =  "";
	
	for(var i=0; i<DateRowArr.length; i++)
	{
		var datestr = $('[name=SelectedDateArr[]]').eq(i).val();
		var SH =  $("[name=StartHour[]]").eq(i).val();
		var SM =  $("[name=StartMin[]]").eq(i).val();
		var EH =  $("[name=EndHour[]]").eq(i).val();
		var EM =  $("[name=EndMin[]]").eq(i).val();
		var SHM = SH + SM;
		var EHM = EH + EM;
		
		$(DateRowArr[i]).addClass("row_approved").removeClass("row_suspend");
		StartTimeArr.push(SH+":"+SM+":"+"00");
		EndTimeArr.push(EH+":"+EM+":"+"00");
		
		for(var j=0;j<DateRowArr.length; j++) {
			
			if(i == j){ // skip itself
				continue;
			}
			
			var datestr2 = $('[name=SelectedDateArr[]]').eq(j).val();
			var SH2 =  $("[name=StartHour[]]").eq(j).val();
			var SM2 =  $("[name=StartMin[]]").eq(j).val();
			var EH2 =  $("[name=EndHour[]]").eq(j).val();
			var EM2 =  $("[name=EndMin[]]").eq(j).val();
			
			var SHM2 = SH2 + SM2;
			var EHM2 = EH2 + EM2;
			var isSameDayTimeOverlap = datestr==datestr2 && ((SHM >= SHM2 && SHM <= EHM2) || (EHM >= SHM2 && EHM <= EHM2) || (SHM <= SHM2 && EHM >= EHM2));
			
			if(SH>EH || (SH==EH&&SM>=EM) || isSameDayTimeOverlap)
			{
				// highlight the date
				$(DateRowArr[i]).addClass("row_suspend").removeClass("row_approved");
				DateValid = false;
			}
			else
			{
				// de-highlight the date 
				//$(DateRowArr[i]).addClass("row_approved").removeClass("row_suspend");
				//StartTimeArr.push(SH+":"+SM+":"+"00");
				//EndTimeArr.push(EH+":"+EM+":"+"00");
			}
		}
	}
	if(!DateValid)
	{
		$("div#WarnInvalidTime").show();
		return false;		
	}
	else
	{
		$("div#WarnInvalidTime").hide();
	}

	// build DateStr
	var SelectedDateArr = new Array();
	$("input[name='SelectedDateArr[]']").each(function(){
		SelectedDateArr.push($(this).val());
	});
	
	Block_Element("TimeCheckList");
	$.post(
		"ajax_task.php",
		{
			task:"CheckTimeCrash",
			FacilityType:$("select#FacilityType").val(),
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
			FacilityID: ($("select#FacilityType").val()=='1' && $("input#BookingMethod2").attr("checked")?($("#FacilityID\\[\\]").val()?$("#FacilityID\\[\\]").val().join(','):''):$("#FacilityID").val()),
<?php }else{ ?>
			FacilityID:$("#FacilityID").val(),
<?php } ?>
			DateList: SelectedDateArr.join(","),
			StartTimeStr: StartTimeArr.join(","),
			EndTimeStr: EndTimeArr.join(","),
			AllowReserve: AllowReserve,
			SelectedDateList: SelectedDateList
		},
		function (data)
		{
			$("div#TimeCheckList").html(data);
			UnBlock_Element("TimeCheckList");
		}
	);
}

function removeDate(datestr)
{
	$("tr#"+datestr).remove();
	$("tr#CheckTimeCrashTr"+datestr).remove();
	showHideSelectedTable();
}

function removeAllDate()
{
	$("table#selectedTable>tbody>tr:not(:first)").remove();
	showHideSelectedTable();
}

function showHideSelectedTable()
{
	if($("table#selectedTable>tbody>tr").length==1)
	{
		$("div#SelectedDateTableDiv").hide();
		$("div#TimeCheckList").html("");
	}
	else
		$("div#SelectedDateTableDiv").show();
	
	if($("table#CheckTimeCrashTable>tbody>tr").length==1)	
		$("div#TimeCheckList").html("");
		
}

function refreshItemCalendar(ItemID,ItemName)
{	
	$("input#FacilityID").val(ItemID);
	$("span#ItemName").html(ItemName);
	$("span#SelectEdit").html("<?=$Lang['eBooking']['eService']['Edit']?>");
	
	
	js_Load_Calendar();
}



function ConfirmChange()
{
	
	if($("input.BookingMethod:checked").length == 0 || $("input.BookingType:checked").length == 0 || $("select#FacilityType").val() == '' )
	{
		js_Update_Current_Selected_Option();
		return false;
	}
	
	if(SavedOptionVal['BookingMethod']==1)
	{
		if($("table#selectedTable>tbody>tr").length>=1)
		{
			if(!js_Remove_All_Timeslot())
			{
				js_RollBack_Selected_Option();
				return false;
			}
		}
	}
	else
	{
		if($("table#selectedTable>tbody>tr").length>=2)
		{	
			if(!confirm("<?=$Lang['eBooking']['eService']['jsMsg']['ConfirmRemoveDates']?>"))
			{
				js_RollBack_Selected_Option();
				return false;
			}
			else
			{
				removeAllDate();
			}
		}
	}

	js_Update_Current_Selected_Option();
	return true;
}

var SavedOptionVal = new Object();
function js_Update_Current_Selected_Option()
{
	SavedOptionVal['BookingMethod'] = $("input.BookingMethod:checked").val();
	SavedOptionVal['FacilityType'] = $("select#FacilityType").val();
	if($("select#FacilityID")){
		SavedOptionVal['FacilityID'] = $("select#FacilityID").val();
	}

	SavedOptionVal['BookingType'] = $("input.BookingType:checked").val();


}

function js_RollBack_Selected_Option()
{
	 $("input.BookingMethod[value='"+SavedOptionVal['BookingMethod']+"']").attr("checked","checked");
	 $("select#FacilityType").val(SavedOptionVal['FacilityType']);
	 if($("select#FacilityID")){
		 //
	 	$("select#FacilityID").val(SavedOptionVal['FacilityID']);
	 }
 	$("input.BookingType[value='"+SavedOptionVal['BookingType']+"']").attr("checked","checked");
}

function CheckForm()
{
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
	if($("select#FacilityType").val() == '1' && $("input#BookingMethod2").attr("checked")) {
		if(!$("#FacilityID\\[\\]").val() || $("#FacilityID\\[\\]").val().length == 0) {
			warnMsg = "<?=$FacilityType==1?$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectRoom']:$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectItem']?>";
			
			alert(warnMsg);
			return false;
		}
	}else
<?php } ?>	
	if(!$("#FacilityID").val())
	{
		warnMsg = "<?=$FacilityType==1?$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectRoom']:$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectItem']?>";
		
		alert(warnMsg);
		return false;
	}

	if($("input.BookingType:checked").val()== 1) // Single
	{
		if($("input.BookingMethod:checked").val()==1) // Specific Period
		{
			if($("table#selectedTable").length==0)
			{
				alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectDate']?>");
				return false;
			}
	
		}
		else // Specific Time
		{

			if($("input.DateCheckBox:checked").length==0)
			{
				alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectDate']?>");
				return false;
			}
		}
	}
	else // Periodic
	{
		var StartDate = $("#StartDate").val();
		var EndDate = $("#EndDate").val();
		
		if(compareDate(StartDate,EndDate)==1)
		{
			alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
			return false;
		}
		else if($("input#RepeatTypeWeekDay").attr("checked") && $("input.WeekDay:checked").length==0)
		{
			alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectWeekDate']?>");
			return false;
		}			
		else if($("input#RepeatTypeCycleDay").attr("checked") && $("input.CycleDay:checked").length==0)
		{
			alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectCycleDate']?>");
			return false;
		}			
	}
	
	return true;
}

function js_Go_Cancel_Booking()
{
	var objForm = document.getElementById('frm1');
	if(<?=$From_eAdmin?>)
		objForm.action = 'booking_request.php';
	else
		objForm.action = '<?=$PATH_WRT_ROOT?>home/eService/eBooking/index_list.php';
	objForm.submit();
}

function js_Toggle_Repeat_Type()
{
	var RepeatType = $("input.RepeatType:checked").val();
	switch(RepeatType)
	{
		case "1":
			$("input.WeekDay").attr("disabled","");
			$("input.CycleDay").attr("disabled","disabled");	
		break;
		case "2":
			$("input.WeekDay").attr("disabled","disabled");
			$("input.CycleDay").attr("disabled","");	
		break;
	}
}

$(document).ready( function() {

	if($("#FacilityID").length <?=($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin))?' || $("#FacilityID\\\\[\\\\]").length':''?>)
	{	
		js_Update_Current_Selected_Option();
	//	js_Toggle_Booking_Type(1);
		js_Toggle_Repeat_Type();
		js_Load_Calendar();
		ShowAttachment();
		<?=$script?>
	}

<?php if ($sys_custom['eBooking']['EverydayTimetable']):?>	
	$("input:radio[name='BookingType']").change(function(){
		if ($(this).val() == '2') {
			$("input#BookingMethod2").css("display","none");
			$("input#BookingMethod2+label").css("display","none");
			$("input#BookingMethod1").attr('checked',true);
		}
		else {
			$("input#BookingMethod2").css("display","");
			$("input#BookingMethod2+label").css("display","");
		}
	});
<?php endif;?>

	//Thick_Box_Init()
});

//function Thick_Box_Init(){
//	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
//}

function ShowInformation(targetLayer)
{
	var p = $("#ShowInfo").position();
	var div_left = p.left;
	var div_top = p.top;
	
	$("div#"+targetLayer).css( {"left":(div_left+20)+"px", "top":(div_top)+"px"} );
	$("div#"+targetLayer).show();
	$("div#"+targetLayer).html("<?=$Lang['General']['Loading'];?>");
	
	$.post(
		"ajax_task.php",
		{
			task : "ShowDescription",
			FacilityType: $("select#FacilityType").val(),
			FacilityID: $("#FacilityID").val()
		},
		function (data)
		{
			//$("div#"+targetLayer).css( {"left":(div_left+20)+"px", "top":(div_top)+"px"} );
			//$("div#"+targetLayer).show();
			$("div#"+targetLayer).html(data);
		}
	)
}

function ShowTimezoneInfo(targetLayer) {
	var p = $("#ShowTimezoneInfo").position();
	var div_left = p.left;
	var div_top = p.top;
	
	$("div#"+targetLayer).css( {"left":(div_left+20)+"px", "top":(div_top)+"px"} );
	$("div#"+targetLayer).show();
	$("div#"+targetLayer).html("<?=$Lang['General']['Loading'];?>");
	
	$.post(
		"ajax_task.php",
		{
			task : "ShowTimezone"
		},
		function (data)
		{
			$("div#"+targetLayer).html(data);
		}
	)
}

function ShowAttachment()
{
	$('#DIV_Attachment').load(
		"ajax_task.php",
		{
			task : "GetRoomAttachment",
			FacilityType: $("select#FacilityType").val(),
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
			FacilityID: ($("select#FacilityType").val()=='1' && $("input#BookingMethod2").attr("checked")? ($("#FacilityID\\[\\]").val()?$("#FacilityID\\[\\]").val().join(','):''):$("#FacilityID").val())
<?php }else{ ?>
			FacilityID: $("#FacilityID").val()
<?php } ?>
		},
		function(data){
			
		}
	);
}

function ShowBookedDetail(BookingID, thisTimeslotID)
{
	var p = $("#BookedDetail_"+thisTimeslotID).position();
	var div_left = p.left+360+20>screen.availWidth?p.left-300:p.left;
	var div_top = p.top;
	
	$("div#BookingDetailLayer").css( {"left":(div_left)+"px", "top":(div_top+20)+"px"} );
	$("div#BookingDetailLayer").show();
	$("div#BookingDetailLayer").html("<?=$Lang['General']['Loading'];?>");
	
	$.post(
		"ajax_task.php",
		{
			task : "ShowBookingDetail",
			BookingID : BookingID,
			thisTimeslotID : thisTimeslotID
		},
		function (data)
		{
			$("div#BookingDetailLayer").html(data);
		}
	)
}

function js_Hide_Information_Layer(targetLayer)
{
	$("div#"+targetLayer).hide();
	$("div#"+targetLayer).html('');
}

function js_Change_Booking_Method()
{
	if(SavedOptionVal['BookingMethod'] == $("input.BookingMethod:checked").val()) // avoid resubmit form when click the radio button twice
		return false;
	
	if(!ConfirmChange()) 
	{
		return false;
	} 
	document.frm1.action=''; 
	document.frm1.submit()
}

function js_Change_Facility()
{
	if(!ConfirmChange()) 
		return false; 
		
	js_Load_Calendar(); 
	js_Reset_Bookable_Day_Range();
	
	js_Hide_Information_Layer('DIV_DisplayInformation'); ShowAttachment();
//	if(CurrentMethod == $("input.BookingMethod:checked").val())
//		return false;
//	
//	if(!ConfirmChange()) 
//	{
//		return false;
//	} 
//	
//	js_Load_Calendar();
//	js_Reset_Bookable_Day_Range(); 
//	
//	js_Hide_Information_Layer('DIV_DisplayInformation'); ShowAttachment();
}

function js_Toggle_Booking_Type(SkipChecking)
{
	if(SavedOptionVal['BookingType'] == $("input.BookingType:checked").val()) // avoid resubmit form when click the radio button twice
		return false;
	
	if(!ConfirmChange()) 
	{
		return false;
	} 
	document.frm1.action=''; 
	document.frm1.submit()

}

function js_Change_Facility_Type()
{
	if(SavedOptionVal['FacilityType'] == $("select#FacilityType").val()) // avoid resubmit form when click the radio button twice
		return false;
	
	if(!ConfirmChange()) 
		return false; 
		
	document.frm1.action=''; 
	document.frm1.submit()
}

function js_Load_Calendar()
{
	if($("input.BookingMethod:checked").val()==1) // Timeslot View
	{
		ChangeWeek(0);
	}
	else // Calendar View
	{
		ChangeMonth(0);
	}
}

var WeekDiff = 0;
function ChangeWeek(prevnext)
{
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
	if($("select#FacilityType").val() == '1' && $("input#BookingMethod2").attr("checked")){
		if($("#FacilityID\\[\\]").val()) {
			$("#CalendarView").show();
		}else
		{
			$("#CalendarView").hide();
			return false; 
		}
	}else 
<?php } ?>	
	if($("#FacilityID").val()!='')
		$("#CalendarView").show();
	else
	{
		$("#CalendarView").hide();
		return false; 
	}	
	
	Block_Element("CalendarWrapper");
	
	NewWeek = WeekDiff+parseInt(prevnext)	

	$.post(
		"ajax_task.php",
		{
			task:"Get_Booking_Timetable",
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
			FacilityID: ($("select#FacilityType").val()=='1' && $("input#BookingMethod2").attr("checked")?($("#FacilityID\\[\\]").val()?$("#FacilityID\\[\\]").val().join(','):''):$("#FacilityID").val()),
<?php }else{ ?>
			FacilityID: $("#FacilityID").val(),
<?php } ?>
			FacilityType: $("select#FacilityType").val(),
			WeekDiff:NewWeek,
			From_eAdmin:<?=$From_eAdmin?>
		},
		function (data)
		{
			if(data)
			{
				$("#CalendarWrapper").html(data)
				WeekDiff += parseInt(prevnext)	
				js_Sync_Selected_Timeslot();
				UnBlock_Element("CalendarWrapper");
			}
		}
	);
}

function js_Check_Timeslot(SelectedDate, Timestamp, TimeslotDisplay, TimeslotID, thisCheck)
{
	if(thisCheck)
		js_Add_Timeslot(SelectedDate, Timestamp, TimeslotDisplay, TimeslotID);
	else
		js_Remove_Timeslot(Timestamp, TimeslotID);
}

var SelectSlot = new Object();

<?
//foreach($SelectedSlot as $thisTimestamp => $thisSlotArr)
//{
//	$script = 'if(!SelectSlot['.$thisTimestamp.'])'."\n";
//	$script .= 'SelectSlot['.$thisTimestamp.'] = new Object();'."\n";
//	foreach($thisSlotArr as $thisTimeSlot)
//	{
//		$script = 'if(!SelectSlot['.$thisTimestamp.']['.$thisTimestamp.'])'."\n";
//		$script .= 'SelectSlot['.$thisTimestamp.'] = new Object();'."\n";
//		
//	}
//}
?>
function js_Add_Timeslot(SelectedDate, Timestamp, TimeslotDisplay, TimeslotID)
{
	if(!SelectSlot[Timestamp])
		SelectSlot[Timestamp] = new Object();
	if(!SelectSlot[Timestamp][TimeslotID])
		SelectSlot[Timestamp][TimeslotID] = new Object();
	
	SelectSlot[Timestamp][TimeslotID].DateDisplay = SelectedDate;
	SelectSlot[Timestamp][TimeslotID].TimeslotDisplay = TimeslotDisplay;
	SelectSlot[Timestamp][TimeslotID].selected = true;
	
	js_Refresh_Selected_Slot_Table();
}

function js_Remove_Timeslot(Timestamp, TimeslotID)
{

	if(!SelectSlot[Timestamp][TimeslotID])
		return false;
	
	SelectSlot[Timestamp][TimeslotID] = null;
	$(".slot_"+Timestamp+"_"+TimeslotID).attr("checked","");
	
	js_Refresh_Selected_Slot_Table();
}

function js_Remove_All_Timeslot()
{
	if(confirm("<?=$Lang['eBooking']['eService']['JSWarning']['ConfirmDeleteSelectedSlot']?>"))
	{
		SelectSlot = new Object();
		$(".slot").attr("checked","");
		
		js_Refresh_Selected_Slot_Table();
		return true;
	}
	else
	{
		return false;
	}
}

function js_Refresh_Selected_Slot_Table()
{
	var Empty = true;
	
	var html = '';
	var hidden = '';
	 html += '<table width="100%" border="0" cellspacing="0" cellpadding="0" id="selectedTable" class="common_table_list_v30 edit_table_list_v30">';
		 html += '<thead>';
			html += '<tr>';
				html += '<th><?=$Lang['eBooking']['eService']['FieldTitle']['SelectedDate']?></th>';
				html +=  '<th><?=$Lang['SysMgr']['Timetable']['TimeSlot']?></th>';
				html +=  '<th><a href="javascript:void(0);" onclick="js_Remove_All_Timeslot()"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove All"></a></th>';
			html +=  '</tr>';
		html += '</thead>';	
	for(Timestamp in SelectSlot)
	{
		for(TimeslotID in SelectSlot[Timestamp]) 
		{
			if(SelectSlot[Timestamp][TimeslotID])
			{
				html += '<tr class="row_approved">';
					html += '<td>'+SelectSlot[Timestamp][TimeslotID].DateDisplay+'</td>';
					html +=  '<td>'+SelectSlot[Timestamp][TimeslotID].TimeslotDisplay+'</td>';
					html +=  '<td ><a href="javascript:void(0);" onclick="js_Remove_Timeslot('+Timestamp+','+TimeslotID+')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove"></a></td>';
				html +=  '</tr>';
				Empty = false;
				
				hidden += '<input type="hidden" name="SelectedSlot['+Timestamp+'][]" value="'+TimeslotID+'">';
			}
		}
	}
	html += '</table>';
	html += hidden;
	
	if(Empty)
		html='';
	
	$("#SelectedSlotTableDiv").html(html)
	
}

function js_Sync_Selected_Timeslot()
{
	for(Timestamp in SelectSlot)
	{
		for(TimeslotID in SelectSlot[Timestamp]) 
		{
			if(SelectSlot[Timestamp][TimeslotID].selected)
				$(".slot_"+Timestamp+"_"+TimeslotID).attr("checked","checked");
		}
	}
	
}

function js_Check_Date(DateField ,Obj, dateText, inst)
{
	var StartDateLimit = $("#StartDateLimit").val();
	var EndDateLimit = $("#EndDateLimit").val();
	var WarnMsg = "<?=$Lang['eBooking']['jsWarningMsg']['OutOfDateRange']?>"+StartDateLimit+" ~ "+(EndDateLimit?EndDateLimit:"<?=$Lang['eBooking']['NoLimit']?>");


	if(DateField == "StartDate")
	{
		if(compareDate(dateText, StartDateLimit)==-1) 
		{
			alert(WarnMsg);
			$(Obj).val(StartDateLimit)
		}
		// Refresh 
		<?php if($lebooking->SettingArr['CycleEnabled']){ ?>
			document.frm1.action="reserve_step1.php"; 
			document.frm1.submit()
		<?php } ?>
	}
	else
	{
		if(EndDateLimit && compareDate(dateText, EndDateLimit)==1) 
		{
			alert(WarnMsg);
			$(Obj).val(EndDateLimit)
		}
		<?php if($lebooking->SettingArr['CycleEnabled']){ ?>
			document.frm1.action="reserve_step1.php"; 
			document.frm1.submit()
		<?php } ?>
	}

}

function js_Reset_Bookable_Day_Range()
{
	$.post(
		"ajax_task.php",
		{
			task:"Get_User_Bookable_Day",
<?php if($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)){ ?>
			FacilityID: ($("select#FacilityType").val()=='1' && $("input#BookingMethod2").attr("checked")? ($("#FacilityID\\[\\]").val()?$("#FacilityID\\[\\]").val().join(','):''):$("#FacilityID").val()),
<?php }else{ ?>
			FacilityID: $("#FacilityID").val(),
<?php } ?>
			FacilityType: $("select#FacilityType").val(),
			From_eAdmin:<?=$From_eAdmin?>
		},
		function (data)
		{
			dataArr = data.split("|=|");
			var StartDateLimit = dataArr[0];
			var EndDateLimit = dataArr[1];
			
			$("input#StartDateLimit").val(StartDateLimit);
			$("input#EndDateLimit").val(EndDateLimit);
			if($("input#StartDate").val() < StartDateLimit)
				$("input#StartDate").val(StartDateLimit);
			if(EndDateLimit && $("input#EndDate").val() > EndDateLimit)
				$("input#EndDate").val(EndDateLimit);
			else if($("input#EndDate").val() < StartDateLimit)
				 $("input#EndDate").val(StartDateLimit);
		}
	);
}

function getElementRowNumber(jqObj, parentSelector, childSelector)
{
	var parentObj = jqObj.closest(parentSelector);
	var numberFound = -1;
	parentObj.find(childSelector).each(function(i){
		var currentObj = $(this);
		if(currentObj.get(0) == jqObj.get(0)) { // compare DOM element is equal
			numberFound = i;
		}
	});
	return numberFound;
}

function removeDateByRef(obj)
{
	var jqObj = $(obj).closest('tr');
	var row_num = getElementRowNumber(jqObj, 'table', 'tr.selectedTableTr');
	if(row_num > -1) {
		jqObj.remove();
		$('table#CheckTimeCrashTable tr').eq(row_num+1).remove();
		showHideSelectedTable();
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
