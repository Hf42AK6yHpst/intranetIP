<?php
// Using : 

// ################################################# Change Log ############################################################
// #
// # Date:  2020-01-24 (Tommy)
// #        - added access right checking
// #
// # Date:  2020-01-13 (Tommy)
// #        - added showing cancelled booking by "^" for ebooking admin
// #
// # Date:  2019-04-29 (Cameron)
// #        - fix cross site scripting by applying cleanCrossSiteScriptingCode() to variables
// #        - fix potential sql injection problem by enclosed var with apostrophe
// #
// # Date : 2019-03-11 (Isaac)
// #        added showing approver groups if the booking is in pending status, modifided function Get_Booking_Record_Display_Detail_Table_Row() function js_Show_Detail_Layer()
// #
// # Date : 2018-09-24 (Henry)
// #        enlarge the page time limit and memory limit
// #
// # Date : 2018-08-20 (Cameron)
// #        remove debug message in ChangeBookingStatus()
// #
// # Date : 2018-08-15 (Henry)
// # fixed XSS
// #
// # Date : 2018-08-06 Isaac #E142612
// # Changes approval ajax to '同時 批核兩個 待批 RECORDS, 以先到先得形式 APPROVE' and have a confirm dialog with messages for different crashed situattion.
// # Fixed setAllBookingIDChecked() function to match with the ajax.
// #
// # Date : 2018-03-26 (Omas)
// # added $sys_custom['eBooking']['displayRemarksDirectly'] #H133004
// #
// # Date : 2015-09-30 (Omas)
// # Add js to support IE8 or before for function lastIndexOf
// #
// # Date : 2015-05-21 (Omas)
// # Fixed js setAllBookingIDChecked() will not add that recordID into array again if it is already checked
// #
// # Date : 2015-05-12 (Omas)
// # modified to add new import btn for $sys_custom['eBooking_BookingApplication_Import_Booking']
// # modified alert message for edit
// # fix cannot edit item
// #
// # Date : 2015-04-21 (Omas)
// # modified to add new edit btn, add cookies for booking category
// #
// # Date : 2015-03-26 (Omas)
// # modified js ChangeBookingStatus() to disalbed button after submit
// #
// # Date : 2015-01-16 (Omas)
// # new Setting - Booking Category enable by $sys_custom['eBooking_BookingCategory']
// #
// # Date : 2014-01-16 (Tiffany)
// # add function CheckDate() to check the invalid choosed date.
// #
// # Date : 2011-11-24 (YatWoon)
// # if remark with attachment, then display another icon gif
// #
// # Date : 2010-06-09 (Ronald)
// # Details : add "Delete" btn into table tool bar
// #
// #########################################################################################################################

SET_TIME_LIMIT(216000);
ini_set('memory_limit','1024M');

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libebooking.php");
include_once ($PATH_WRT_ROOT . "includes/libebooking_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

// ## Cookies handling
// set cookies
$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_page_no",
    "pageNo"
);
$arrCookies[] = array(
    "ck_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eBooking_BookingStatus",
    "BookingStatus"
);
$arrCookies[] = array(
    "ck_eBooking_BookingDate",
    "BookingDate"
);
$arrCookies[] = array(
    "ck_eBooking_ManagementGroup",
    "ManagementGroup"
);
$arrCookies[] = array(
    "ck_eBooking_My_Booking_Record_Keyword",
    "keyword"
);
$arrCookies[] = array(
    "ck_eBooking_My_Booking_Record_Category",
    "BookingCategoryID"
);

if (isset($clearCoo) && $clearCoo == 1)
    clearCookies($arrCookies);
else
    updateGetCookies($arrCookies);

$BookingStatus = (isset($BookingStatus) && $BookingStatus != "") ? IntegerSafe($BookingStatus) : - 999;
$BookingDate = (isset($BookingDate) && $BookingDate != "") ? $BookingDate : 1;
$keyword = (isset($keyword) && $keyword != "") ? $keyword : "";
$keyword = cleanHtmlJavascript($keyword);

intranet_auth();
intranet_opendb();

// control reserve right in booking process
$_SESSION['EBOOKING_BOOKING_FROM_EADMIN'] = 1;

$field = trim($field) != '' ? $field : 1;
$field = IntegerSafe($field);
$order = trim($order) != '' ? $order : 1;

$CurrentPageArr['eBooking'] = 1;
$CurrentPage = "PageManagement_BookingRequest";
$linterface = new interface_html();
$linventory = new libinventory();
$lebooking = new libebooking();
$lebooking_ui = new libebooking_ui();
$li = new libdbtable2007($field, $order, $PageNumber);

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $lebooking->IS_ADMIN_USER($_SESSION["UserID"]) || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER" )){
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

function Get_Booking_Record_Display_Detail_Table_Row($itemType, $bookingId, $id, $rowNum, $infoAry, $itemInRoom = false, $lastItem = false, $roomOnly = false, $itemOnly = false, $itemInOtherItem = false, $numOfRelatedBooking = 0)
{
    global $Lang, $sys_custom, $image_path, $LAYOUT_SKIN, $lebooking, $PATH_WRT_ROOT;
    global $BookingCatRelationAssoAry; //
                                       
    // # get status image
    if ($itemType == LIBEBOOKING_FACILITY_TYPE_ROOM) {
        $nameField = 'RoomName';
        $bookingStatusFieldName = 'RoomBookingStatus';
        $bookingPicFieldName = 'RoomBookingPIC';
        $processDayBeforeFieldName = 'RoomProcessDayBefore';
        
        $approveRight = $lebooking->Check_Approve_Room_Booking_Request_Right($id);
        $checkboxTypeCode = 'ROOM';
//         include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
//         $objItem = new Room($id);
        $sql = "SELECT management_group.GroupID, management_group.GroupName, management_group.Description FROM INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS relation INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS management_group ON (relation.GroupID = management_group.GroupID) WHERE relation.LocationID = '$id'";
        $MgmtGroupArr = $lebooking->returnArray($sql,3);
        $numOfMgmtGroup = count($MgmtGroupArr);
    } else 
        if ($itemType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
            $nameField = 'ItemName';
            $bookingStatusFieldName = 'ItemBookingStatus';
            $bookingPicFieldName = 'ItemBookingPIC';
            $processDayBeforeFieldName = 'ItemProcessDayBefore';
            
            $approveRight = $lebooking->Check_Approve_Item_Booking_Request_Right($id);
            $checkboxTypeCode = 'ITEM';
            
            include_once ($PATH_WRT_ROOT . "includes/libebooking_item.php");
            $objItem = new eBooking_Item($id);
            $MgmtGroupArr = $objItem->Get_Item_Management_Group();
            $numOfMgmtGroup = count($MgmtGroupArr);
        }
    
    if ($numOfMgmtGroup == 1) {
        list ($management_group_id, $management_group_name) = $MgmtGroupArr[0];
        $thisDisplayContent = $management_group_name;
    } else
        if ($numOfMgmtGroup > 0) {
            $thisDisplayContent = $numOfMgmtGroup . " " . $Lang['eBooking']['Settings']['GeneralPermission']['Group(s)'];
        } else {
            $thisDisplayContent = " - ";
        }
        $thisDisplayContent = "<span style='vertical-align: super; white-space: normal;'>".$thisDisplayContent."</spam>";
    
    switch ($infoAry[$id][$bookingStatusFieldName]) {
        case 0:
            $thisName = $infoAry[$id][$nameField];
            $thisID = 'MgmtGroupLinkDiv_' . $bookingId;
            $img_status = '<div id="' . $thisID . '" style="white-space: nowrap;">' . "\n";
            if($numOfMgmtGroup  > 0){
            $img_status.= '<a href="javascript:js_Show_Detail_Layer(\'MgmtGroup\', ' . $bookingId . ', \'' . $thisID . '\', \'' .$itemType. '\','.$id.',\''.$thisName.'\')" class="tablelink">' . "\n";
            }
            $img_status.= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'] . "'>";           
            $img_status.= $thisDisplayContent;
            if($numOfMgmtGroup  > 0){
            $img_status.= '</a>' . "\n";
            }
            $img_status.='</div>' . "\n";
            if ($itemInRoom) {
                $td_num_css = ($lastItem) ? "class='booking_solid_line'" : "class='booking_dot_line '";
                $td_css = "class='booking_pending'";
            } else 
                if ($roomOnly) {
                    $td_num_css = "";
                    $td_css = "class='booking_pending'";
                } else 
                    if ($itemOnly) {
                        $td_num_css = ($itemInOtherItem) ? "class='booking_solid_line'" : "class='booking_dot_line'";
                        $td_css = "class='booking_pending'";
                    } else {
                        $td_num_css = "class='booking_dot_line'";
                        $td_css = "class='booking_pending booking_dot_line'";
                    }
            break;
        case 1:
            $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] . "'>" . $infoAry[$id][$bookingPicFieldName] . "<br><span class='tabletextremark'>" . $infoAry[$id][$processDayBeforeFieldName] . "</span>";
            if ($itemInRoom) {
                $td_num_css = ($lastItem) ? "class='booking_solid_line'" : "class='booking_dot_line '";
                $td_css = "class='booking_approved'";
            } else 
                if ($roomOnly) {
                    $td_num_css = "";
                    $td_css = "class='booking_approved'";
                } else 
                    if ($itemOnly) {
                        $td_num_css = ($itemInOtherItem) ? "class='booking_solid_line'" : "class='booking_dot_line'";
                        $td_css = "class='booking_approved'";
                    } else {
                        $td_num_css = "class='booking_dot_line'";
                        $td_css = "class='booking_approved booking_dot_line'";
                    }
            break;
        case - 1:
            $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] . "'>" . $infoAry[$id][$bookingPicFieldName] . "<br><span class='tabletextremark'>" . $infoAry[$id][$processDayBeforeFieldName] . "</span>";
            if ($itemInRoom) {
                $td_num_css = ($lastItem) ? "class='booking_solid_line'" : "class='booking_dot_line '";
                $td_css = "class='booking_rejected'";
            } else 
                if ($roomOnly) {
                    $td_num_css = "";
                    $td_css = "class='booking_rejected'";
                } else 
                    if ($itemOnly) {
                        $td_num_css = ($itemInOtherItem) ? "class='booking_solid_line'" : "class='booking_dot_line'";
                        $td_css = "class='booking_rejected'";
                    } else {
                        $td_num_css = "class='booking_dot_line'";
                        $td_css = "class='booking_rejected booking_dot_line'";
                    }
            break;
        case 999:
            $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory'] . "'>";
            if ($itemInRoom) {
                $td_num_css = ($lastItem) ? "class='booking_solid_line'" : "class='booking_dot_line '";
            } else 
                if ($roomOnly) {
                    $td_num_css = "";
                } else 
                    if ($itemOnly) {
                        $td_num_css = ($itemInOtherItem) ? "class='booking_solid_line'" : "class='booking_dot_line'";
                    } else {
                        $td_num_css = "";
                    }
            $td_css = "class='booking_tempory'";
            break;
    }
    if ($infoAry[$id]['RejectReason'] != "") {
        $thisLayerID = 'RejectReasonDiv_' . $bookingId . '_' . $itemType . '_' . $id;
        $img_status .= "<div id='" . $thisLayerID . "'><a href='javascript: js_Show_Detail_Layer(\"RejectReason\"," . $bookingId . ",\"$thisLayerID\", \"" . $itemType . "\", \"$id\");'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' class='tablelink'></a></div>";
    }
    
    $isCancelled = $lebooking->getCancelledBooking($bookingId);
    
    if ($itemInRoom) {
        $namePrefix = "<img align='absmiddle' src='{$image_path}/{$LAYOUT_SKIN}/icon_and.gif'>";
    } else if($isCancelled){
        if(($infoAry['IsReserve'])){
            $namePrefix = "<span class='tabletextrequire'>*</span>";
        }
        $namePrefix .= "<span class='tabletextrequire'>^</span>";
    } else {
        $namePrefix = ($infoAry['IsReserve']) ? "<span class='tabletextrequire'>*</span>" : '';
    }
    
    $rowNumDisplay = ($itemInRoom || $itemInOtherItem) ? "&nbsp;" : $rowNum;
    $nameDisplay = $namePrefix . $infoAry[$id][$nameField];
    $dateDisplay = ($itemInRoom || $itemInOtherItem) ? "&nbsp;" : $infoAry['Date'];
    $timeDisplay = ($itemInRoom || $itemInOtherItem) ? "&nbsp;" : date("H:i", strtotime($infoAry['StartTime'])) . " - " . date("H:i", strtotime($infoAry['EndTime']));
    $responsiblePersonDisplay = ($itemInRoom || $itemInOtherItem) ? "&nbsp;" : $infoAry['ResponsiblePerson'];
    $requestedByDisplay = ($itemInRoom || $itemInOtherItem) ? "&nbsp;" : $infoAry['RequestedBy'] . "<br><span class='tabletextremark'>" . $infoAry['RequestedDayBefore'] . "</span>";
    $eventDisplay = ($itemInRoom || $itemInOtherItem) ? "&nbsp;" : $infoAry['Event'];
    
    if ($sys_custom['eBooking_Cust_Remark']['WongKamFai']) {
        $relatedToId = $infoAry['RelatedTo'];
        $custRemark = $lebooking->DisplayCustRemark($relatedToId);
    }
    if ($itemInRoom || $itemInOtherItem) {
        $remarkDisplay = "&nbsp;";
    } else 
        if (trim($custRemark) || $infoAry['Remark'] != "" || $infoAry['Attachment'] != "") {
            
            $thisLayerID = 'RemarkLinkDiv_' . $bookingId;
            $icon_display = $infoAry['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
            $remarkDisplay = "<div id='" . $thisLayerID . "'><a href='javascript: js_Show_Detail_Layer(\"Remark\"," . $bookingId . ",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/" . $icon_display . "' border='0' class='tablelink'></a></div>";
            if ($sys_custom['eBooking']['displayRemarksDirectly']) {
                $remarkDisplay = nl2br(htmlspecialchars($infoAry['Remark']));
            }
        } else {
            $remarkDisplay = "&nbsp;";
        }
    
    $checkboxDisplay = '';
    $divOpenTag = '';
    $divCloseTag = '';
    if ($approveRight > 0) {
        $checkboxDisplay .= "<td valign='top' nowrap $td_css><input type='checkbox' name='" . $bookingId . "BookingRecord[]' value='" . $checkboxTypeCode . "_" . $id . "' onClick='(this.checked)?setSingleBookingIDChecked(\"" . $bookingId . "_" . $id . "\",\"" . $checkboxTypeCode . "\",1, $bookingId):setSingleBookingIDChecked(\"" . $bookingId . "_" . $id . "\",\"" . $checkboxTypeCode . "\",0,$bookingId);'></td>";
        if ($itemInRoom || $itemInOtherItem) {
            $checkboxDisplay .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
        } else {
            if (($itemOnly && $numOfRelatedBooking == 1) || $roomOnly) {
                $divOpenTag .= "<div id='hideCheckBox' style='display:none'>";
                $divCloseTag .= "</div>";
                $checkboxDisplay = "<td valign='top' nowrap $td_css>" . $divOpenTag . "<input type='checkbox' name='CheckAllBooking[]' value='$bookingId' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"" . $checkboxTypeCode . "\"):setTargetBookingIDChecked(0,this.value,\"" . $checkboxTypeCode . "\");'>" . $divCloseTag . "</td>" . $checkboxDisplay;
            }
            else {
                $checkboxDisplay .= "<td valign='top' nowrap $td_css>" . $divOpenTag . "<input type='checkbox' name='CheckAllBooking[]' value='$bookingId' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"" . $checkboxTypeCode . "\"):setTargetBookingIDChecked(0,this.value,\"" . $checkboxTypeCode . "\");'>" . $divCloseTag . "</td>";
            }
        }
    } else {
        $checkboxDisplay .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
        $checkboxDisplay .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
    }
    
    $x = '';
    $x .= "<tr>";
    $x .= "<td width='1%'>" . $rowNumDisplay . "</td>";
    $x .= "<td $td_css>" . $nameDisplay . "</td>";
    if ($sys_custom['eBooking_BookingCategory']) {
        $x .= "<td width='8%' $td_css>" . Get_String_Display($BookingCatRelationAssoAry[$bookingId]) . "</td>";
    }
    $x .= "<td $td_css>" . $dateDisplay . "</td>";
    $x .= "<td $td_css>" . $timeDisplay . "</td>";
    $x .= "<td $td_css>" . $responsiblePersonDisplay . "</td>";
    $x .= "<td $td_css>" . $requestedByDisplay . "</td>";
    if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
        $x .= "<td $td_css>" . $eventDisplay . "</td>";
    }
    $x .= "<td $td_css>" . $img_status . "</td>";
    $x .= "<td width='1%' $td_css>" . $remarkDisplay . "</td>";
    $x .= $checkboxDisplay;
    $x .= "</tr>";
    
    return $x;
}

if ($msg != "") {
    $returnMsg = $msg;
} else 
    if ($ReturnMsgKey != '') {
        $returnMsg = $Lang['eBooking']['eService']['ReturnMsg'][$ReturnMsgKey];
    }

$TAGS_OBJ[] = array(
    $Lang['eBooking']['Management']['FieldTitle']['BookingRequest']
);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($returnMsg);

// $print_btn = $linterface->GET_LNK_PRINT("booking_request_print.php",$Lang['Btn']['Print'],'','','',0);
// $import_btn = $linterface->GET_LNK_IMPORT("booking_request_import.php",$Lang['Btn']['Import'],'','','',0);
// $export_btn = $linterface->GET_LNK_EXPORT("booking_request_export.php",$Lang['Btn']['Export'],'','','',0);
// $print_btn = $linterface->Get_Content_Tool_v30("print","booking_request_print.php",$Lang['Btn']['Print']);
// $import_btn = $linterface->Get_Content_Tool_v30("import","booking_request_import.php",$Lang['Btn']['Import']);
// $export_btn = $linterface->Get_Content_Tool_v30("export","booking_request_export.php",$Lang['Btn']['Export']);
// Get_Content_Tool_v30($type, $href="#", $text="", $options=array(), $other="", $divID='')

// ## Reserve link (eBooking Admin) Only ###
$h_toolbar = '';
if ($lebooking->IS_ADMIN_USER()) {
    $h_toolbar .= $linterface->Get_Content_Tool_v30("new", "reserve_step1.php", $Lang['eBooking']['Management']['FieldTitle']['Reserve'], "", "", "", 0);
}
if ($sys_custom['eBooking_BookingApplication_Import_Booking']) {
    $h_toolbar .= $linterface->Get_Content_Tool_v30('import', 'reserve_import_step1.php');
}
if ($sys_custom['eBooking_Export_Mgmt_BookingApplication']) {
    $h_toolbar .= $linterface->Get_Content_Tool_v30('export', 'javascript:js_Go_Export();');
}

$arrBookingStatus[] = array(
    - 999,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllStatus']
);
$arrBookingStatus[] = array(
    0,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']
);
$arrBookingStatus[] = array(
    1,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']
);
$arrBookingStatus[] = array(
    - 1,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']
);
$arrBookingStatus[] = array(
    999,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_iCalTemporyRecord']
);
$booking_status_filter = getSelectByArray($arrBookingStatus, " name='BookingStatus' onChange='this.form.submit();' ", $BookingStatus, 0, 1);

$arrBookingDate[] = array(
    1,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllComingRequest']
);
$arrBookingDate[] = array(
    2,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['PastRequest']
);
$arrBookingDate[] = array(
    3,
    $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['SpecificDateRange']
);
$booking_date_filter = getSelectByArray($arrBookingDate, " name='BookingDate' onChange='this.form.submit();' ", $BookingDate, 0, 1);

if ($sys_custom['eBooking_BookingCategory']) {
    $bookingcategory_filter = $lebooking->getBookingCategorySelectionBox($BookingCategoryID, 1, 'this.form.submit();');
}

if ($BookingDate == 3) {
    if (! $StartDate || ! $EndDate) {
        $AcademicYearID = Get_Current_Academic_Year_ID();
        list ($YearStartDate, $YearEndDate) = getPeriodOfAcademicYear($AcademicYearID);
        if (! $StartDate)
            $StartDate = date("Y-m-d", strtotime($YearStartDate));
        if (! $EndDate)
            $EndDate = date("Y-m-d", strtotime($YearEndDate));
    }
    $StartDate = cleanCrossSiteScriptingCode($StartDate);
    $EndDate = cleanCrossSiteScriptingCode($EndDate);
    $StartDatePicker = $linterface->GET_DATE_PICKER("StartDate", $StartDate, "", "", "", "", "", "");
    $EndDatePicker = $linterface->GET_DATE_PICKER("EndDate", $EndDate, "", "", "", "", "", "");
    // $DateRangePicker = $StartDatePicker." ~ ".$EndDatePicker."<input type='button',name='submit_button',id='submit_button', value='".$Lang['eBooking']['Button']['FieldTitle']['Search']."', onclick='CheckDate(document.form1)'>";
    $DateRangePicker = $StartDatePicker . " ~ " . $EndDatePicker . $linterface->GET_SMALL_BTN($Lang['eBooking']['Button']['FieldTitle']['Search'], "button", "CheckDate(document.form1)");
}

$arrManagemntGroup = $lebooking->Get_User_Related_Mgmt_Group_Array($UserID);
if ($lebooking->IS_ADMIN_USER()) {
    $arrManagemntGroup[] = array(
        "0" => "-999",
        "GroupID" => "-999",
        "1" => $Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup'],
        "GroupName" => $Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup']
    );
    $management_group_filter = getSelectByArray($arrManagemntGroup, " name='ManagementGroup' onChange='this.form.submit();' ", $ManagementGroup, 1, 0, $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup']);
} else {
    if ($ManagementGroup == "")
        $ManagementGroup = $arrManagemntGroup[0][0];
    $management_group_filter = getSelectByArray($arrManagemntGroup, " name='ManagementGroup' onChange='this.form.submit();' ", $ManagementGroup, 1, 1);
}

$filter_bar = "<div class='table_filter'>" . $booking_status_filter . $management_group_filter . $booking_date_filter . $bookingcategory_filter . "</div>";
$filter_bar2 = "<div class='table_filter'>" . $DateRangePicker . "</div>";

$table_tool .= "<div class='common_table_tool'>";
// # Edit Btn ##
$table_tool .= '<a href="javascript:EditBookingRequest(\'Edit\',\'reserve_step3.php\')" class="tool_edit">' . $Lang['Btn']['Edit'] . '</a>';
// # Approve Btn ##
$table_tool .= "<a href=\"javascript:ChangeBookingStatus('Approve','booking_status_approve.php')\" class=\"tool_approve\">" . $button_approve . "</a>";
// # Reject Btn ##
$table_tool .= "<a href=\"javascript:ChangeBookingStatus('Reject','booking_status_reject_confirm.php')\" class=\"tool_reject\">" . $button_reject . "</a>";
// # Delete Btn ##
$table_tool .= "<a href=\"javascript:ChangeBookingStatus('Delete','booking_request_delete.php')\" class=\"tool_delete\">" . $button_delete . "</a>";
$table_tool .= "</div>";

if ($BookingDate != "") {
    $curr_date = date("Y-m-d");
    if ($BookingDate == 3) {} else 
        if ($BookingDate == 1) {
            $StartDate = $curr_date;
            $EndDate = '';
        } else {
            $StartDate = '';
            $EndDate = $curr_date;
        }
}

if ($keyword != "") {
    $keyword = addslashes(trim($keyword));
}

$StartDate = cleanCrossSiteScriptingCode($StartDate);
$EndDate = cleanCrossSiteScriptingCode($EndDate);

list ($arrBookingID, $arrBookingDetails) = $lebooking->Get_Booking_Record_Display_Detail($keyword, $FacilityType, $BookingStatus, '', $StartDate, $EndDate, $ManagementGroup, $TargetUserID = - 1);

// ## filter Booking Category
$BookingCategory_filter = $BookingCategoryID;
if ($BookingCategory_filter != '') {
    $BookingCatRelationAssoAry = $lebooking->getBookingMapWithCategory($arrBookingID, 'CategoryID');
    foreach ($arrBookingID as $_BookingID) {
        if ($BookingCatRelationAssoAry[$_BookingID] == $BookingCategory_filter) {
            $_tempBookingIDAry[] = $_BookingID;
        }
    }
    $arrBookingID = $_tempBookingIDAry;
}

// Default Table Settings
$pageNo = cleanCrossSiteScriptingCode($pageNo);
$pageNo = ($pageNo == '') ? $li->pageNo = 1 : $li->pageNo = $pageNo;
$pageNo = IntegerSafe($pageNo);
$numPerPage = cleanCrossSiteScriptingCode($numPerPage);
$numPerPage = ($numPerPage == '') ? $li->page_size = 20 : $li->page_size = $numPerPage;
$numPerPage = IntegerSafe($numPerPage);
$Order = ($Order == '') ? 1 : $Order;
$SortField = ($SortField == '') ? 0 : $SortField;

if ($pageNo == 1) {
    $start = $pageNo;
    $end = $numPerPage;
    $li->n_start = $start - 1;
    $li->n_end = min(sizeof($arrBookingID), ($li->pageNo * $li->page_size));
} else {
    $start = ($pageNo * $numPerPage) - $numPerPage + 1;
    
    if ($start > sizeof($arrBookingID)) {
        $start = 1;
    }
    
    $end = ($pageNo * $numPerPage);
    $li->n_start = $start - 1;
    $li->n_end = min(sizeof($arrBookingID), ($li->pageNo * $li->page_size));
}

$numOfCol = 10;
if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
    $numOfCol ++;
}
if ($sys_custom['eBooking_BookingCategory']) { // BookingCategory
    $numOfCol ++;
}

// ## Get Ary to Map Category
// $sql = "SELECT
// /*iercr.CategoryID,*/
// iercr.BookingID,
// iec.CategoryName
// FROM
// INTRANET_EBOOKING_RECORD_CATEGORY_RELATION iercr
// INNER JOIN INTRANET_EBOOKING_CATEGORY iec ON iec.CategoryID = iercr.CategoryID
// WHERE
// BOOKINGID IN ('\'".implode('\',\'',$arrBookingID)."\'')";
// $BookingCatRelationAry = $lebooking->ReturnResultSet($sql);
// $BookingCatRelationAssoAry = BuildMultiKeyAssoc($BookingCatRelationAry,'BookingID',array('CategoryName'),1);

if (sizeof($arrBookingID) > 0) {
    $BookingCatRelationAssoAry = $lebooking->getBookingMapWithCategory($arrBookingID, 'CategoryName');
    
    foreach ($arrBookingID as $key => $booking_id) {
        if ($booking_id != "")
            $arrSortedBookingID[] = $booking_id;
    }
    
    for ($i = $start - 1; $i < $end; $i ++) {
        $arrDisplayBookingID[] = $arrSortedBookingID[$i];
    }
    
    foreach ($arrDisplayBookingID as $key => $booking_id) {
        $arrRoomBookingDetails = array();
        $arrItemBookingDetails = array();
        
        if (is_array($arrBookingDetails[$booking_id]['RelatedRoom']))
            $arrTempRoomBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedRoom']);
        if (is_array($arrBookingDetails[$booking_id]['RelatedItem']))
            $arrTempItemBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedItem']);
        
        if (sizeof($arrTempRoomBookingDetails) > 0) {
            foreach ($arrTempRoomBookingDetails as $key => $val) {
                if (($val != "") && ($val != "0")) {
                    $arrRoomBookingDetails[] = $val;
                }
            }
        }
        
        if (sizeof($arrTempItemBookingDetails) > 0) {
            foreach ($arrTempItemBookingDetails as $key => $val) {
                if (($val != "") && ($val != "0")) {
                    $arrItemBookingDetails[] = $val;
                }
            }
        }
        
        $row_num = $start ++;
        
        if (($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1)) {
            // # Book for room & item at the same time
            $room_id = $arrRoomBookingDetails[0];
            $table_content .= Get_Booking_Record_Display_Detail_Table_Row(LIBEBOOKING_FACILITY_TYPE_ROOM, $booking_id, $room_id, $row_num, $arrBookingDetails[$booking_id]);
            
            // ## get status image
            // switch($arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'])
            // {
            // case 0:
            // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
            // $td_num_css = "class='booking_dot_line '";
            // $td_css = "class='booking_pending booking_dot_line '";
            // break;
            // case 1:
            // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
            // $td_num_css = "class='booking_dot_line'";
            // $td_css = "class='booking_approved booking_dot_line'";
            // break;
            // case -1:
            // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
            // $td_num_css = "class='booking_dot_line'";
            // $td_css = "class='booking_rejected booking_dot_line'";
            // break;
            // case 999:
            // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
            // $td_num_css = "";
            // $td_css = "class='booking_tempory '";
            // break;
            // }
            // if($arrBookingDetails[$booking_id][$room_id]['RejectReason'] != "")
            // {
            // $thisLayerID = 'RejectReasonDiv_'.$booking_id.'_1_'.$room_id;
            // $img_status .= "<div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"RejectReason\",".$booking_id.",\"$thisLayerID\", 1, \"$room_id\");'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' class='tablelink'></a></div>";
            // }
            //
            // $table_content .= "<tr>";
            // $table_content .= "<td width='1%'>".$row_num."</td>";
            // if($arrBookingDetails[$booking_id]['IsReserve']) {
            // $table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
            // } else {
            // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
            // }
            // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Date']."</td>";
            // $table_content .= "<td $td_css>".date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']))."</td>";
            // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['ResponsiblePerson']."</td>";
            // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['RequestedBy']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id]['RequestedDayBefore']."</span></td>";
            // if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
            // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Event']."</td>";
            // }
            // $table_content .= "<td $td_css>".$img_status."</td>";
            //
            // # check cust remark
            // if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
            // {
            // $RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
            // $cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
            // }
            //
            // if(trim($cust_remark) || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] != "")
            // {
            // $thisLayerID = 'RemarkLinkDiv_'.$booking_id;
            // $icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
            // $table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"Remark\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink'></a></div></td>";
            // }
            // else
            // {
            // $table_content .= "<td width='1%' $td_css>&nbsp;</td>";
            // }
            //
            // $ApproveRight = $lebooking->Check_Approve_Room_Booking_Request_Right($room_id);
            // if($ApproveRight > 0)
            // {
            // $table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='".$booking_id."BookingRecord[]' value='ROOM_".$room_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",0,$booking_id);'></td>";
            // $table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ROOM\"):setTargetBookingIDChecked(0,this.value,\"ROOM\");'></td>";
            // }
            // else
            // {
            // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
            // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
            // }
            // $table_content .= "</tr>";
            
            for ($i = 0; $i < sizeof($arrItemBookingDetails); $i ++) {
                $item_id = $arrItemBookingDetails[$i];
                $lastItemInRoom = ((sizeof($arrItemBookingDetails) > 0) && ($i == (sizeof($arrItemBookingDetails) - 1))) ? true : false;
                $table_content .= Get_Booking_Record_Display_Detail_Table_Row(LIBEBOOKING_FACILITY_TYPE_ITEM, $booking_id, $item_id, $row_num, $arrBookingDetails[$booking_id], $itemInRoom = true, $lastItemInRoom);
                
                // switch($arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'])
                // {
                // case 0:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
                // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                // $td_num_css = "class='booking_solid_line'";
                // $td_css = "class='booking_pending'";
                // } else {
                // $td_num_css = "class='booking_dot_line '";
                // $td_css = "class='booking_pending'";
                // }
                // break;
                // case 1:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
                // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                // $td_num_css = "class='booking_solid_line '";
                // $td_css = "class='booking_approved'";
                // } else {
                // $td_num_css = "class='booking_dot_line '";
                // $td_css = "class='booking_approved'";
                // }
                // break;
                // case -1:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
                // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                // $td_num_css = "class='booking_solid_line'";
                // $td_css = "class='booking_rejected'";
                // } else {
                // $td_num_css = "class='booking_dot_line '";
                // $td_css = "class='booking_rejected'";
                // }
                // break;
                // case 999:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
                // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                // $td_num_css = "class='booking_solid_line'";
                // $td_css = "class='booking_tempory'";
                // } else {
                // $td_num_css = "class='booking_dot_line '";
                // $td_css = "class='booking_tempory'";
                // }
                // break;
                // }
                // if($arrBookingDetails[$booking_id][$item_id]['RejectReason'] != "")
                // {
                // $thisLayerID = 'RejectReasonDiv_'.$booking_id.'_2_'.$item_id;
                // $img_status .= "<div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"RejectReason\",".$booking_id.",\"$thisLayerID\", 2, \"$item_id\");'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' class='tablelink'></a></div>";
                // }
                //
                // $table_content .= "<tr>";
                // if($i != (sizeof($arrItemBookingDetails)-1) ){
                // $table_content .= "<td width='1%' >&nbsp;</td>";
                // } else {
                // $table_content .= "<td width='1%' >&nbsp;</td>";
                // }
                // $table_content .= "<td $td_css ><img align='absmiddle' src='{$image_path}/{$LAYOUT_SKIN}/icon_and.gif'>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
                // $table_content .= "<td $td_css>&nbsp;</td>";
                // $table_content .= "<td $td_css>&nbsp;</td>";
                // $table_content .= "<td $td_css>&nbsp;</td>";
                // $table_content .= "<td $td_css>&nbsp;</td>";
                // if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
                // $table_content .= "<td $td_css>&nbsp;</td>";
                // }
                // $table_content .= "<td $td_css>".$img_status."</td>";
                // $table_content .= "<td width='1%' $td_css>&nbsp;</td>";
                //
                //
                // $ApproveRight = $lebooking->Check_Approve_Item_Booking_Request_Right($item_id);
                // if($ApproveRight > 0)
                // {
                // $table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='".$booking_id."BookingRecord[]' value='ITEM_".$item_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",0,$booking_id);'></td>";
                // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                // }
                // else
                // {
                // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                // }
                // $table_content .= "</tr>";
            }
            $table_content .= "<tr><td colspan='" . $numOfCol . "' class='booking_approved booking_solid_line'><img height='5px' src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'></td></tr>";
        } else 
            if (($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 0)) {
                // # Book for Room Only
                $room_id = $arrRoomBookingDetails[0];
                $table_content .= Get_Booking_Record_Display_Detail_Table_Row(LIBEBOOKING_FACILITY_TYPE_ROOM, $booking_id, $room_id, $row_num, $arrBookingDetails[$booking_id], $itemInRoom = false, $lastItemInRoom = false, $roomOnly = true);
                
                // ## get status image
                // switch($arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'])
                // {
                // case 0:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
                // $td_num_css = "";
                // $td_css = "class='booking_pending '";
                // break;
                // case 1:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
                // $td_num_css = "";
                // $td_css = "class='booking_approved '";
                // break;
                // case -1:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
                // $td_num_css = "";
                // $td_css = "class='booking_rejected '";
                // break;
                // case 999:
                // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
                // $td_num_css = "";
                // $td_css = "class='booking_tempory '";
                // break;
                // }
                //
                // if($arrBookingDetails[$booking_id][$room_id]['RejectReason'] != "")
                // {
                // $thisLayerID = 'RejectReasonDiv_'.$booking_id.'_1_'.$room_id;
                // $img_status .= "<div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"RejectReason\",".$booking_id.",\"$thisLayerID\", 1, \"$room_id\");'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' class='tablelink'></a></div>";
                // }
                //
                // $table_content .= "<tr>";
                // $table_content .= "<td width='1%' $td_num_css>".$row_num."</td>";
                // if($arrBookingDetails[$booking_id]['IsReserve']) {
                // $table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
                // } else {
                // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
                // }
                // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Date']."</td>";
                // $table_content .= "<td $td_css>".date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']))."</td>";
                // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['ResponsiblePerson']."</td>";
                // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['RequestedBy']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id]['RequestedDayBefore']."</span></td>";
                // if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
                // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Event']."</td>";
                // }
                // $table_content .= "<td $td_css>".$img_status."</td>";
                //
                // # check cust remark
                // if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
                // {
                // $RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
                // $cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
                // }
                //
                // if(trim($cust_remark) || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] != "")
                // {
                // $thisLayerID = 'RemarkLinkDiv_'.$booking_id;
                // $icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
                // $table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"Remark\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink'></a></div></td>";
                // }
                // else
                // {
                // $table_content .= "<td width='1%' $td_css>&nbsp;</td>";
                // }
                //
                // $ApproveRight = $lebooking->Check_Approve_Room_Booking_Request_Right($room_id);
                // if($ApproveRight>0)
                // {
                // $table_content .= "<td width='1%' valign='top' nowrap $td_css><input type='checkbox' name='".$booking_id."BookingRecord[]' value='ROOM_".$room_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",0,$booking_id);'></td>";
                // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;<div id='hideCheckBox' style='display:none'><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ROOM\"):setTargetBookingIDChecked(0,this.value,\"ROOM\");'></div></td>";
                // }
                // else
                // {
                // $table_content .= "<td width='1%' valign='top' nowrap $td_css>&nbsp;</td>";
                // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                // }
                // $table_content .= "</tr>";
                $table_content .= "<tr><td colspan='" . $numOfCol . "' class='booking_approved booking_solid_line'><img height='5px' src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'></td></tr>";
            } else 
                if (($arrBookingDetails[$booking_id]['RoomBooking'] == 0) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1)) {
                    // # Book for Item Only
                    for ($i = 0; $i < sizeof($arrItemBookingDetails); $i ++) {
                        $item_id = $arrItemBookingDetails[$i];
                        $itemInOtherItem = ($i == 0) ? false : true;
                        $lastItem = ((sizeof($arrItemBookingDetails) > 0) && ($i == (sizeof($arrItemBookingDetails) - 1))) ? true : false;
                        $numOfRelatedBooking = sizeof($arrItemBookingDetails);
                        $table_content .= Get_Booking_Record_Display_Detail_Table_Row(LIBEBOOKING_FACILITY_TYPE_ITEM, $booking_id, $item_id, $row_num, $arrBookingDetails[$booking_id], $itemInRoom = false, $lastItem, $roomOnly = false, $itemOnly = true, $itemInOtherItem, $numOfRelatedBooking);
                        
                        // switch($arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'])
                        // {
                        // case 0:
                        // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
                        // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                        // $td_num_css = "class='booking_solid_line'";
                        // $td_css = "class='booking_pending'";
                        // } else {
                        // $td_num_css = "class='booking_dot_line '";
                        // $td_css = "class='booking_pending'";
                        // }
                        // break;
                        // case 1:
                        // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
                        // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                        // $td_num_css = "class='booking_solid_line '";
                        // $td_css = "class='booking_approved'";
                        // } else {
                        // $td_num_css = "class='booking_dot_line '";
                        // $td_css = "class='booking_approved'";
                        // }
                        // break;
                        // case -1:
                        // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
                        // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                        // $td_num_css = "class='booking_solid_line'";
                        // $td_css = "class='booking_rejected'";
                        // } else {
                        // $td_num_css = "class='booking_dot_line '";
                        // $td_css = "class='booking_rejected'";
                        // }
                        // break;
                        // case 999:
                        // $img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
                        // if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
                        // $td_num_css = "class='booking_solid_line'";
                        // $td_css = "class='booking_tempory'";
                        // } else {
                        // $td_num_css = "class='booking_dot_line '";
                        // $td_css = "class='booking_tempory'";
                        // }
                        // break;
                        // }
                        // if($arrBookingDetails[$booking_id][$item_id]['RejectReason'] != "")
                        // {
                        // $thisLayerID = 'RejectReasonDiv_'.$booking_id.'_2_'.$item_id;
                        // $img_status .= "<div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"RejectReason\",".$booking_id.",\"$thisLayerID\", 2, \"$item_id\");'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' class='tablelink'></a></div>";
                        // }
                        //
                        // $table_content .= "<tr>";
                        // if($i == 0){
                        // $table_content .= "<td width='1%'>".$row_num."</td>";
                        // if($arrBookingDetails[$booking_id]['IsReserve']) {
                        // $table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
                        // } else {
                        // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
                        // }
                        // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Date']."</td>";
                        // $table_content .= "<td $td_css>".date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']))."</td>";
                        // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['ResponsiblePerson']."</td>";
                        // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['RequestedBy']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id]['RequestedDayBefore']."</span></td>";
                        // if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
                        // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Event']."</td>";
                        // }
                        // $table_content .= "<td $td_css>".$img_status."</td>";
                        //
                        // # check cust remark
                        // if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
                        // {
                        // $RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
                        // $cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
                        // }
                        //
                        // if(trim($cust_remark) || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] != "")
                        // {
                        // $thisLayerID = 'RemarkLinkDiv_'.$booking_id;
                        // $icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
                        // $table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"Remark\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink'></a></div></td>";
                        // }
                        // else
                        // {
                        // $table_content .= "<td width='1%' $td_css>&nbsp;</td>";
                        // }
                        // $ApproveRight = $lebooking->Check_Approve_Item_Booking_Request_Right($item_id);
                        // if($ApproveRight > 0)
                        // {
                        // $table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='".$booking_id."BookingRecord[]' value='ITEM_".$item_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",0,$booking_id);'></td>";
                        // if(sizeof($arrItemBookingDetails) > 1){
                        // $table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ITEM\"):setTargetBookingIDChecked(0,this.value,\"ITEM\");'></td>";
                        // }else{
                        // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;<div id='hideCheckBox' style='display:none'><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ITEM\"):setTargetBookingIDChecked(0,this.value,\"ITEM\");'></div></td>";
                        // }
                        //
                        // }
                        // else
                        // {
                        // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                        // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                        // }
                        // $table_content .= "</tr>";
                        // }else{
                        // $table_content .= "<td width='1%'>&nbsp;</td>";
                        // if($arrBookingDetails[$booking_id]['IsReserve']) {
                        // $table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
                        // } else {
                        // $table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
                        // }
                        // $table_content .= "<td $td_css>&nbsp;</td>";
                        // $table_content .= "<td $td_css>&nbsp;</td>";
                        // $table_content .= "<td $td_css>&nbsp;</td>";
                        // $table_content .= "<td $td_css>&nbsp;</td>";
                        // if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
                        // $table_content .= "<td $td_css>&nbsp;</td>";
                        // }
                        // $table_content .= "<td $td_css>".$img_status."</td>";
                        // $table_content .= "<td width='1%' $td_css>&nbsp;</td>";
                        //
                        // $ApproveRight = $lebooking->Check_Approve_Item_Booking_Request_Right($item_id);
                        // if($ApproveRight > 0)
                        // {
                        // $table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='".$booking_id."BookingRecord[]' value='ITEM_".$item_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",0,$booking_id);'></td>";
                        // //$table_content .= "<td valign='top' nowrap $td_css>&nbsp;<div id='hideCheckBox' style='display:none'><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ITEM\"):setTargetBookingIDChecked(0,this.value,\"ITEM\");'></div></td>";
                        // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                        // }
                        // else
                        // {
                        // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                        // $table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
                        // }
                        // $table_content .= "</tr>";
                        // }
                    }
                    $table_content .= "<tr><td colspan='" . $numOfCol . "' class='booking_approved booking_solid_line'><img height='5px' src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'></td></tr>";
                }
    }
} else {
    $table_content .= "<tr>";
    $table_content .= "<td valign='top' colspan='" . $numOfCol . "' align='center'>" . $Lang['General']['NoRecordAtThisMoment'] . "</td>";
    $table_content .= "</tr>";
}

// ## Table Navigation Bar ###
$li->page_size = $numPerPage;
$li->total_row = sizeof($arrBookingID);
$li->form_name = "form1";
// $li->pageNo_name = "booking_mgmt_pageNo";
// $li->numPerPage_name = "booking_mgmt_numPerPage";

$table_content .= "<tr class='tablebottom' height='20px'>";
$table_content .= "<td colspan='" . $numOfCol . "'>";
if (sizeof($arrBookingID) > 0)
    $table_content .= $li->navigation();
$table_content .= "</td>";
$table_content .= "</tr>";

$table_content .= "<tr>";
$table_content .= "<td valign='top' colspan='" . $numOfCol . "' align='left'><span class='tabletextremark'>" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ReservedByAdmin'] . "</span></td>";
$table_content .= "</tr>";

$table_content .= "<tr>";
$table_content .= "<td valign='top' colspan='" . $numOfCol . "' align='left'><span class='tabletextremark'>" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['CancelledByBookedUser'] . "</span></td>";
$table_content .= "</tr>";

$table_content .= "<tr><td>";
$table_content .= "<input type='hidden' name='CheckboxChecked' id='CheckboxChecked' value=0>";
$table_content .= "<input type='hidden' name='str_BookingID' id='str_BookingID' value=''>";
// $table_content .= "<input type='hidden' id='booking_mgmt_pageNo' name='booking_mgmt_pageNo' value='".$li->pageNo."'>";
$table_content .= "<input type='hidden' id='pageNo' name='pageNo' value='" . $li->pageNo . "'>";
$table_content .= "<input type='hidden' id='order' name='order' value='" . $li->order . "'>";
$table_content .= "<input type='hidden' id='field' name='field' value='" . $li->field . "'>";
$table_content .= "<input type='hidden' id='page_size_change' name='page_size_change' value=''>";
$table_content .= "<input type='hidden' id='numPerPage' name='numPerPage' value='" . $li->page_size . "'>";
$table_content .= "</td></tr>";

// ## Hide Layer Button
$remark_layer_content .= '<div id="RemarkLayer" class="selectbox_layer" style="visibility:hidden; width:500px;">' . "\n";
$remark_layer_content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">' . "\n";
$remark_layer_content .= '<tbody>' . "\n";
$remark_layer_content .= '<tr>' . "\n";
$remark_layer_content .= '<td align="right" style="border-bottom: medium none;">' . "\n";
$remark_layer_content .= '<a href="javascript:js_Hide_Detail_Layer()"><img border="0" src="' . $PATH_WRT_ROOT . $image_path . '/' . $LAYOUT_SKIN . '/ecomm/btn_mini_off.gif"></a>' . "\n";
$remark_layer_content .= '</td>' . "\n";
$remark_layer_content .= '</tr>' . "\n";
$remark_layer_content .= '<tr>' . "\n";
$remark_layer_content .= '<td align="left" style="border-bottom: medium none;">' . "\n";
$remark_layer_content .= '<div id="RemarkLayerContentDiv"></div>' . "\n";
$remark_layer_content .= '</td>' . "\n";
$remark_layer_content .= '</tr>' . "\n";
$remark_layer_content .= '</tbody>' . "\n";
$remark_layer_content .= '</table>' . "\n";
$remark_layer_content .= '</div>' . "\n";

// for dbtable
$pos = 0;
?>
<?=$lebooking_ui->initJavaScript();?>
<script language="javascript">
	js_ArrayRoomCheck = new Array();
	js_ArrayItemCheck = new Array();
	js_ArrayBookingID = new Array();
	
	$(document).ready( function() {
		$('input#keyword').keydown( function(evt) {
			if (Check_Pressed_Enter(evt)) {
				$('form#form1').submit();
			}
		});
	});
	
	function js_Show_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID, jsFacilityType, jsFacilityID, jsFacilityName)
	{
		var jsFacilityType = jsFacilityType || '';
		var jsFacilityID = jsFacilityID || '';
		var jsFacilityName = jsFacilityName || '';
		js_Hide_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'Remark')
			jsAction = 'Reload_Remark';
		else if (jsDetailType == 'MgmtGroup')
			jsAction = 'reloadMgmtGroupLayer';
		else
			jsAction = 'Reload_RejectReason';
		
		$('div#RemarkLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
		js_Change_Layer_Position(jsClickedObjID);
		MM_showHideLayers('RemarkLayer','','show');
				
		$('div#RemarkLayerContentDiv').load(
			"ajax_reload.php", 
			{ 
				Action: jsAction,
				BookingID: jsBookingID,
				FacilityType: jsFacilityType,
				FacilityID: jsFacilityID,
				FacilityName: jsFacilityName
			},
			function(returnString)
			{
				$('div#RemarkLayerContentDiv').css('z-index', '999');
			}
		);
	}
	
	function js_Hide_Detail_Layer()
	{
		MM_showHideLayers('RemarkLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID) {
		
		var jsOffsetLeft, jsOffsetTop;
		
		jsOffsetLeft = 450;
		jsOffsetTop = -15;
			
		var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
		
		document.getElementById('RemarkLayer').style.left = posleft + "px";
		document.getElementById('RemarkLayer').style.top = postop + "px";
		document.getElementById('RemarkLayer').style.visibility = 'visible';
	}
	
	function setAllBookingIDChecked(val, obj, element_name)
	{
        len=obj.elements.length;
        var i=0;
        var cnt = parseInt($('#CheckboxChecked').val());
        
        for( i=0 ; i<len ; i++) 
        {
            if (obj.elements[i].name==element_name)
            {
            	obj.elements[i].checked=val;
            	var booking_id = obj.elements[i].value;
            	
            	for(j=0; j<document.getElementsByName(eval('booking_id+"BookingRecord[]"')).length; j++)
            	{
            		if(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].checked != val){
	            		document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].checked = val;
	            		if(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("ROOM_") != -1)
	            		{
	            			// room
	            			room_id = (document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value).slice(5);
	            			if(val) {
	            				cnt++;
	            				setSingleBookingIDChecked(booking_id+"_"+room_id,"ROOM",1,booking_id);
	            			} else {
	            				cnt--;
	            				setSingleBookingIDChecked(booking_id+"_"+room_id,"ROOM",0,booking_id);
	            			}
	            		}
	            		else
	            		{
	            			// item
	            			item_id = (document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value).slice(5);
	            			if(val) {
	            				cnt++;
	            				setSingleBookingIDChecked(booking_id+"_"+item_id,"ITEM",1,booking_id);
	            			} else {
	            				cnt--;
	            				setSingleBookingIDChecked(booking_id+"_"+item_id,"ITEM",0,booking_id);
	            			}
	            		}
            		}
            	}
            }
        }
        $('#CheckboxChecked').val(cnt);
	}
	
	function setTargetBookingIDChecked(val, booking_id, booking_type)
	{
		for(j=0; j<document.getElementsByName(eval('booking_id+"BookingRecord[]"')).length; j++)
    	{
    		if(val != document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].checked)
    		{
    			document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].checked = val;
	    		if(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("ROOM_") != -1)
	    		{
					var room_id = document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.substr(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("_")+1,document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.length);
					var new_booking_id = booking_id+'_'+room_id;
					var new_booking_type = 'ROOM';
	    		}
	    		else
	    		{
	    			var item_id = document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.substr(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("_")+1,document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.length);
	    			var new_booking_id = booking_id+'_'+item_id;
	    			var new_booking_type = 'ITEM';
	    		}		
	    		setSingleBookingIDChecked(new_booking_id, new_booking_type, val, booking_id);
	    	}
	    	else
	    	{
	    		setSingleBookingIDChecked(new_booking_id, new_booking_type, val, booking_id);
	    	}
    	}
	}
	
	function setSingleBookingIDChecked(booking_id, booking_type, val, old_booking_id)
	{
		var cnt = parseInt($('#CheckboxChecked').val());
		if(val)
		{
			cnt++;
			if(booking_type == 'ROOM'){
				js_ArrayRoomCheck[js_ArrayRoomCheck.length] = booking_id;
			}
			if(booking_type == 'ITEM'){
				js_ArrayItemCheck[js_ArrayItemCheck.length] = booking_id;
			}
			js_ArrayBookingID[js_ArrayBookingID.length] = old_booking_id;
			$('#CheckboxChecked').val(cnt);
		}
		else
		{
			cnt--;
			if(booking_type == 'ROOM')
			{
				for(i=0; i<js_ArrayRoomCheck.length; i++)
				{
					if(js_ArrayRoomCheck[i] == booking_id)
					{
						js_ArrayRoomCheck.splice(i,1);
					}
				}
			}
			if(booking_type == 'ITEM')
			{
				for(i=0; i<js_ArrayItemCheck.length; i++)
				{
					if(js_ArrayItemCheck[i] == booking_id)
					{
						js_ArrayItemCheck.splice(i,1);
					}
				}
			}
			for(i=0; i<js_ArrayBookingID.length; i++)
			{
				if(js_ArrayBookingID[i] == old_booking_id) {
					js_ArrayBookingID.splice(i,1);
				}
			}
			$('#CheckboxChecked').val(cnt);
		}
	}
	var submit = 0;
	function ChangeBookingStatus(module, page)
	{
		var pass_checking = 0;
		
		if(module == "Approve")
		{
			var str_RoomCheck = js_ArrayRoomCheck.toString();
			var str_ItemCheck = js_ArrayItemCheck.toString();
			var str_BookingID = js_ArrayBookingID.toString();
			$('#str_BookingID').val(str_BookingID);
			if(submit == 0){
				submit = 1;
				$.post(
					"ajax_check_booking_record.php",
					{
						"action"		:	"CheckApprove",
						"strRoomCheck" 	:	str_RoomCheck,
						"strItemCheck" 	:	str_ItemCheck
					},
					function(responseText)
					{
						if(responseText == -1)
						{
							var confirmSubmit = confirm("<?=$Lang['eBooking']['Settings']['BookingRequest']['JSConfirm']['TimeOverLapped'];?>");
							if(confirmSubmit == true){
								if($('#CheckboxChecked').val() > 0) {
						        	document.form1.action=page;
						            document.form1.submit();
						        } else {
						            alert(globalAlertMsg2);
						            submit = 0;
						        }
							}else{
								location = 'booking_request.php';
							}
						}
						else if(responseText == -2)
						{
							var confirmSubmit = confirm("<?=$Lang['eBooking']['Settings']['BookingRequest']['JSConfirm']['AlreadyBooked'];?>");
							if(confirmSubmit == true){
								if($('#CheckboxChecked').val() > 0) {
						        	document.form1.action=page;
						            document.form1.submit();
						        } else {
						            alert(globalAlertMsg2);
						            submit = 0;
						        }
							}else{
								location = 'booking_request.php';
							}
						}
						else if (responseText == -3) {
				            alert("<?php echo $Lang['General']['InvalidData'];?>");
				            submit = 0;
						}
						else
						{	
// 							alert(responseText);
							//alert($('#CheckboxChecked').val());
							//return false; // temp for checking
							if($('#CheckboxChecked').val() > 0) {
					        	document.form1.action=page;
					            document.form1.submit();
					        } else {
					            alert(globalAlertMsg2);
					            submit = 0;
					        }
						}
					}
				);
			}
		}
		else if(module == "Delete")
		{
			var str_BookingID = js_ArrayBookingID.toString();
			$('#str_BookingID').val(str_BookingID);
			if(submit == 0){
				submit = 1;
				$.post(
					"ajax_check_booking_record.php",
					{
						"action"		:	"CheckDelete",
						"strBookingID" 	: 	str_BookingID
					},
					function(responseText)
					{
						if(responseText == 1) {
							if($('#CheckboxChecked').val() > 0) {
								if(confirm("<?=$Lang['eBooking']['Management']['BookingRequest']['JSWarning']['ConfirmDelete'];?>")) {
						        	document.form1.action=page;
						            document.form1.submit();
								} else {
									//continuous
									submit = 0;
								}
						    } else {
						    	alert(globalAlertMsg2);
						    	submit = 0;
						    }
						} 
						else if (responseText == -3) {
				            alert("<?php echo $Lang['General']['InvalidData'];?>");
				            submit = 0;
						} else {
							alert("<?=$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['DeleteRejected_AlreadyApproved']?>");
						}
					}
				);
			}
		}
		else
		{
			var str_BookingID = js_ArrayBookingID.toString();
			$('#str_BookingID').val(str_BookingID);
			if(submit == 0){
				submit = 1;
				if($('#CheckboxChecked').val() > 0) {
	//				if(confirm("<?=$Lang['eBooking']['Management']['BookingRequest']['JSWarning']['ConfirmReject'];?>")) {
		        		document.form1.action=page;
		            	document.form1.submit();
	//				} else {
	//					//continuous
	//				}
		        } else {
		            alert(globalAlertMsg2);
		            submit = 0;
		        }
			}
		}
	}
	
	function js_Go_Export() {
		$('form#form1').attr('action', 'export.php').submit().attr('action', 'booking_request.php');
	}
	
	function CheckDate(obj){
		if (obj.StartDate.value > obj.EndDate.value){
			alert("<?=$Lang['eBooking']['jsWarningArr']['InvalidSearchPeriod']?>");
			return false;			
		}
		else{
			
		form1.submit();
		}
		
	}
	
	function EditBookingRequest(action, page){
			
		var uniqueBookingIDArray = sortUnique(js_ArrayBookingID);
		if(js_ArrayBookingID.length == 1)
		{
			if(action == "Edit")
			{
				var Record_RoomID = uniqueBookingIDArray[0]
				// Edit
				if(js_ArrayItemCheck.length > 0){
					document.form1.action = 'reserve_step3.php?AdminEdit=1&RecordID='+uniqueBookingIDArray[0];
			    	document.form1.submit();
				}
				else{
					document.form1.action = 'reserve_step3.php?AdminEdit=1&RecordID='+uniqueBookingIDArray[0];
			    	document.form1.submit();	
				}
				
			}
		}
		else{
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectOneRequestOnly']?>");
		}
		
	}
	
	// for support IE8 or before
	if (!Array.prototype.lastIndexOf) {
		Array.prototype.lastIndexOf= function(obj, start) {
		var index=-1;
		     for (var i = (start || 0), j = this.length; i < j; i++) {
		         if (this[i] === obj) { index = i; }
		     }
		     return index;
		}
	}
	
	function sortUnique(arr) {
    arr.sort();
    var last_i;
    for (var i=0;i<arr.length;i++)
        if ((last_i = arr.lastIndexOf(arr[i])) !== i)
            arr.splice(i+1, last_i-i);
    return arr;
	}
	
</script>

<br>
<form id="form1" name="form1" action="booking_request.php" method="POST">

	<div class="content_top_tool">
		<div class="Conntent_tool"><?=$h_toolbar?></div>
		<div class="Conntent_search"><?=$linterface->Get_Search_Box_Div('keyword', stripslashes(stripslashes($keyword)));?></div>
		<br style="clear: both;">
	</div>
	<div class="table_board">
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tbody>
				<tr class="table-action-bar">
					<td valign="bottom">
						<div class="table_filter"><?=$filter_bar;?></div>
						<p class="spacer"></p>
						<div class="table_filter"><?=$filter_bar2;?></div>
						<p class="spacer"></p>
					</td>
					<td valign="bottom">
		   			<?=$table_tool?>
		   		</td>
				</tr>
			</tbody>
		</table>
		<table border='0' width='100%' cellpadding='3' cellspacing='0'>
		<?php if($sys_custom['eBooking']['displayRemarksDirectly']):?>
    		<tr class='tabletop'>
				<td width="1">#</td>
				<td width="25%"><?=$li->column(0, $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'])?></td>
    			<? if($sys_custom['eBooking_BookingCategory']){ ?>
    				<td><?=$Lang['eBooking']['Settings']['BookingCategory']['BookingCategory']?></td>
    			<? } ?>
    			<td width="114"><?=$li->column(1, $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'])?></td>
				<td width="114"><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'];?></td>
				<td width="114"><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson']?></td>
				<td width="134"><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy']?></td>
    			<? if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) { ?>
    				<td><?=$Lang['eBooking']['Event']?></td>
    			<? } ?>
    			<td width="114"><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status']?></td>
				<td width="25%"><?=$Lang['General']['Remark']?></td>
				<td width="1">&nbsp;</td>
				<td width="1"><input type='checkbox'
					onClick="(this.checked)?setAllBookingIDChecked(1,document.form1,'CheckAllBooking[]'):setAllBookingIDChecked(0,document.form1,'CheckAllBooking[]');"></td>
			</tr>
		<?php else:?>
			<tr class='tabletop'>
				<td>#</td>
				<td><?=$li->column(0, $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'])?></td>
    			<? if($sys_custom['eBooking_BookingCategory']){ ?>
    				<td><?=$Lang['eBooking']['Settings']['BookingCategory']['BookingCategory']?></td>
    			<? } ?>
    			<td><?=$li->column(1, $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'])?></td>
				<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'];?></td>
				<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson']?></td>
				<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy']?></td>
    			<? if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) { ?>
    				<td><?=$Lang['eBooking']['Event']?></td>
    			<? } ?>
    			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status']?></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><input type='checkbox'
					onClick="(this.checked)?setAllBookingIDChecked(1,document.form1,'CheckAllBooking[]'):setAllBookingIDChecked(0,document.form1,'CheckAllBooking[]');"></td>
			</tr>
		<?php endif;?>
		<?=$table_content;?>
	</table>
	<?=$remark_layer_content;?>	
</div>

</form>

<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>