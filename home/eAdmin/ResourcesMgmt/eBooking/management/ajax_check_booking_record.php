<?php
//using : 
/*
 * Change Log:
 * 
 * Date:    2019-04-30 Cameron
 *          - check if pass in parameters $strRoomCheck and $strItemCheck match expected pattern or not to avoid secuirty attack
 *
 * Date:    2018-08-06 Isaac #E142612
 *          - Added if statement to check if have room/item booking selected before running array_unique() and foreach() for room/item. 
 *  
 * Date:	2017-12-04 Omas #M132098 
 * 			- added checking avoid using Get_Facility_Booking_Record without record id causing over memory limit problem 
 * Date:	2017-03-20 Villa #L114077 
 * 			- Fix Error: change $this-> to $lebooking->
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
if(!empty($strRoomCheck) && !preg_match('/^(([\d]+_[\d]+)(,){0,1})+$/', $strRoomCheck)){
    echo -3;
}
else if(!empty($strItemCheck) && !preg_match('/^(([\d]+_[\d]+)(,){0,1})+$/', $strItemCheck)){
    echo -3;
}
else if($action == "CheckApprove")
{
	$arrRoomCheck = explode(",",$strRoomCheck);
	$arrRoomCheck = array_unique($arrRoomCheck);
	$targetRoomRecord = implode(",",$arrRoomCheck);
	
	$arrItemCheck = explode(",",$strItemCheck);
	$arrItemCheck = array_unique($arrItemCheck);
	$targetItemRecord = implode(",",$arrItemCheck);
	
	### Room ###
	for($i=0; $i<sizeof($arrRoomCheck); $i++)
	{
		$booking_details = $arrRoomCheck[$i];
		$booking_id = substr($booking_details,0,strpos($booking_details,'_'));
		$room_id = substr($booking_details,strpos($booking_details,'_')+1,strlen($booking_details));
		
//		$sql = "SELECT 
//					record.Date
//				FROM
//					INTRANET_EBOOKING_RECORD AS record INNER JOIN 
//					INTRANET_EBOOKING_ROOM_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID)
//				WHERE 
//					detail.BookingID = $booking_id";
//		$arrRecordDate = $lebooking->returnVector($sql);
		if($booking_id != ''){
			$arrRecordDate = $lebooking->Get_Facility_Booking_Record(LIBEBOOKING_FACILITY_TYPE_ROOM, '', '', '', '', $booking_id);
			$BookingDate = $arrRecordDate[0]['Date'];
			
			$arrRoom[] = $room_id;
			$arrRoomByDate[$room_id][] = $BookingDate;
			$arrRoomBooking[$room_id][$BookingDate][] = $booking_id;
		}
	}
	if(isset($arrRoom)||$arrRoom!=''){
	   $arrRoom = array_unique($arrRoom);
	}
	### Item ###
	for($i=0; $i<sizeof($arrItemCheck); $i++)
	{
		$booking_details = $arrItemCheck[$i];
		$booking_id = substr($booking_details,0,strpos($booking_details,'_'));
		$item_id = substr($booking_details,strpos($booking_details,'_')+1,strlen($booking_details));
		
//		$sql = "SELECT 
//					record.Date
//				FROM
//					INTRANET_EBOOKING_RECORD AS record INNER JOIN 
//					INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID)
//				WHERE 
//					detail.BookingID = $booking_id";
//		$arrRecordDate = $lebooking->returnVector($sql);
		if($booking_id != ''){
			$arrRecordDate = $lebooking->Get_Facility_Booking_Record(LIBEBOOKING_FACILITY_TYPE_ITEM, '', '', '', '', $booking_id);
			$BookingDate = $arrRecordDate[0]['Date'];
			
			$arrItem[] = $item_id;
			$arrItemByDate[$item_id][] = $BookingDate;
			$arrItemBooking[$item_id][$BookingDate][] = $booking_id;
		}
	}
	if(isset($arrItem)||$arrItem!=''){
	   $arrItem = array_unique($arrItem);
	}
	## Check room bookings 
	if(isset($arrRoom)||$arrRoom!=''){
    	foreach($arrRoom as $key=>$room_id)
    	{
    		if(is_array($arrRoomByDate[$room_id]))
    		{
    			$arrRoomByDate = array_unique($arrRoomByDate[$room_id]);
    			
    			for($i=0; $i<sizeof($arrRoomByDate); $i++)
    			{
    				$BookingDate = $arrRoomByDate[$i];
    				
    				if(sizeof($arrRoomBooking[$room_id][$BookingDate])>0)
    				{
    					$targetBookingID = implode(",",$arrRoomBooking[$room_id][$BookingDate]);
    					
    					for($j=0; $j<sizeof($arrRoomBooking[$room_id][$BookingDate]); $j++)
    					{
    						$booking_id = $arrRoomBooking[$room_id][$BookingDate][$j];
    						## Get the target Booking's StartTime & EndTime
    //						$sql = "SELECT 
    //									record.StartTime, record.EndTime
    //								FROM
    //									INTRANET_EBOOKING_RECORD AS record INNER JOIN 
    //									INTRANET_EBOOKING_ROOM_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID)
    //								WHERE 
    //									detail.BookingID = $booking_id AND record.Date = '$BookingDate'";
    //						$arrTargetDetails = $lebooking->returnArray($sql,5);
    						$arrTargetDetails = $lebooking->Get_Facility_Booking_Record(LIBEBOOKING_FACILITY_TYPE_ROOM, '', $BookingDate, $BookingDate, '', $booking_id);
    						
    						if(sizeof($arrTargetDetails) > 0)
    						{
    							$start_time = $arrTargetDetails[0]['StartTime'];
    							$end_time = $arrTargetDetails[0]['EndTime'];
    							
    							$cond = "( ('$start_time' = record.StartTime AND record.EndTime = '$end_time') ) OR ";
    							$cond .= "( (record.StartTime <= '$start_time' AND ('$start_time' < record.EndTime AND record.EndTime <= '$end_time') ) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime < '$end_time') AND ('$end_time' < record.EndTime) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime <= '$end_time') AND ('$start_time' <= record.EndTime AND record.EndTime <= '$end_time') ) OR ";
    							$cond .= "( (record.StartTime < '$start_time' AND '$end_time' < record.EndTime) )";
    							
    							$sql = "SELECT 
    										COUNT(record.BookingID)
    									FROM 
    										INTRANET_EBOOKING_RECORD as record INNER JOIN
    										INTRANET_EBOOKING_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID AND detail.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
    									WHERE
    										detail.FacilityID = '$room_id'
    										AND record.Date = '$BookingDate'
    										AND ($cond)
    										AND (record.BookingID IN ($targetBookingID) AND detail.BookingID != '$booking_id')";
    							$arrOtherRoomRecordExist = $lebooking->returnVector($sql);
    							
    							## Check any submitted room bookings are duplicate (booking time overlapped)
    							if($arrOtherRoomRecordExist[0] > 0)
    							{
    								$OtherRoomRecordExist++;
    							}
    							
    							$cond = "( ('$start_time' = record.StartTime AND record.EndTime = '$end_time') ) OR ";
    							$cond .= "( (record.StartTime <= '$start_time' AND ('$start_time' < record.EndTime AND record.EndTime <= '$end_time') ) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime < '$end_time') AND ('$end_time' < record.EndTime) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime <= '$end_time') AND ('$start_time' <= record.EndTime AND record.EndTime <= '$end_time') ) OR ";
    							$cond .= "( (record.StartTime < '$start_time' AND '$end_time' < record.EndTime) )";
    							
    							$sql = "SELECT 
    										COUNT(record.BookingID)
    									FROM 
    										INTRANET_EBOOKING_RECORD as record INNER JOIN
    										INTRANET_EBOOKING_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID AND detail.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
    									WHERE
    										detail.FacilityID = '$room_id'
    										AND record.Date = '$BookingDate'
    										AND ($cond)
    										AND detail.BookingStatus = 1
    										AND detail.BookingID != '$booking_id'";
    							$arrOtherRoomRecordApproved = $lebooking->returnVector($sql);
    							
    							## Check any submitted room bookings are already approved (booking status = 1)
    							if($arrOtherRoomRecordApproved[0] > 0)
    							{
    								$OtherRoomRecordApproved++;
    							}
    		
    						}
    					}
    				}
    			}
    		}
    	}
	}
	
	## Check any submitted item bookings are duplicate (booking time overlapped)
	if(isset($arrItem)||$arrItem!=''){
    	foreach($arrItem as $key=>$item_id)
    	{
    		if(is_array($arrItemByDate[$item_id]))
    		{
    			$arrItemByDate = array_unique($arrItemByDate[$item_id]);
    			
    			for($i=0; $i<sizeof($arrItemByDate); $i++)
    			{
    				$BookingDate = $arrItemByDate[$i];
    				
    				if(sizeof($arrItemBooking[$item_id][$BookingDate])>0)
    				{
    					$targetBookingID = implode(",",$arrItemBooking[$item_id][$BookingDate]);
    					
    					for($j=0; $j<sizeof($arrItemBooking[$item_id][$BookingDate]); $j++)
    					{
    						$booking_id = $arrItemBooking[$item_id][$BookingDate][$j];
    						## Get the target Booking's StartTime & EndTime
    //						$sql = "SELECT 
    //									record.StartTime, record.EndTime
    //								FROM
    //									INTRANET_EBOOKING_RECORD AS record INNER JOIN 
    //									INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID)
    //								WHERE 
    //									detail.BookingID = $booking_id AND record.Date = '$BookingDate'";
    //						$arrTargetDetails = $lebooking->returnArray($sql,5);
    						$arrTargetDetails = $lebooking->Get_Facility_Booking_Record(LIBEBOOKING_FACILITY_TYPE_ITEM, '', $BookingDate, $BookingDate, '', $booking_id);
    						
    						if(sizeof($arrTargetDetails) > 0)
    						{
    //							list($start_time, $end_time) = $arrTargetDetails[0];
    							$start_time = $arrTargetDetails[0]['StartTime'];
    							$end_time = $arrTargetDetails[0]['EndTime'];
    							
    							$cond = "( ('$start_time' = record.StartTime AND record.EndTime = '$end_time') ) OR ";
    							$cond .= "( (record.StartTime <= '$start_time' AND ('$start_time' < record.EndTime AND record.EndTime <= '$end_time') ) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime < '$end_time') AND ('$end_time' < record.EndTime) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime <= '$end_time') AND ('$start_time' <= record.EndTime AND record.EndTime <= '$end_time') ) OR ";
    							$cond .= "( (record.StartTime < '$start_time' AND '$end_time' < record.EndTime) )";
    							
    							$sql = "SELECT 
    										COUNT(record.BookingID)
    									FROM 
    										INTRANET_EBOOKING_RECORD as record INNER JOIN
    										INTRANET_EBOOKING_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID AND detail.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM.")
    									WHERE
    										detail.FacilityID = '$item_id'
    										AND record.Date = '$BookingDate'
    										AND ($cond)
    										AND (record.BookingID IN ($targetBookingID) AND detail.BookingID != '$booking_id')";
    							//echo "OtherItemRecordExist :".$sql."\r\n";
    							$arrOtherItemRecordExist = $lebooking->returnVector($sql);
    							
    							if($arrOtherItemRecordExist[0] > 0)
    							{
    								// Selected Booking Request have duplicate booking time
    								$OtherItemRecordExist++;
    							}
    							
    							$cond = "( ('$start_time' = record.StartTime AND record.EndTime = '$end_time') ) OR ";
    							$cond .= "( (record.StartTime <= '$start_time' AND ('$start_time' < record.EndTime AND record.EndTime <= '$end_time') ) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime < '$end_time') AND ('$end_time' < record.EndTime) ) OR ";
    							$cond .= "( ('$start_time' <= record.StartTime AND record.StartTime <= '$end_time') AND ('$start_time' <= record.EndTime AND record.EndTime <= '$end_time') ) OR ";
    							$cond .= "( (record.StartTime < '$start_time' AND '$end_time' < record.EndTime) )";
    							
    							$sql = "SELECT 
    										COUNT(record.BookingID)
    									FROM 
    										INTRANET_EBOOKING_RECORD as record INNER JOIN
    										INTRANET_EBOOKING_BOOKING_DETAILS AS detail ON (record.BookingID = detail.BookingID AND detail.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM.")
    									WHERE
    										detail.FacilityID = '$item_id'
    										AND record.Date = '$BookingDate'
    										AND ($cond)
    										AND detail.BookingStatus = 1
    										AND detail.BookingID != '$booking_id'";
    							//echo "OtherItemRecordApproved :".$sql."\r\n";
    							$arrOtherItemRecordApproved = $lebooking->returnVector($sql);
    							
    							## Check any submitted item bookings are already approved (booking status = 1)
    							if($arrOtherItemRecordApproved[0] > 0)
    							{
    								$OtherItemRecordApproved++;
    							}
    							
    						}
    					}
    				}
    			}
    		}
    	}
	}
	
	
	if(($OtherRoomRecordExist > 0) || ($OtherItemRecordExist > 0))
	{
		echo -1;
	}
	else if(($OtherRoomRecordApproved > 0) || ($OtherItemRecordApproved > 0))
	{
		echo -2;
	}
	else
	{
		echo 1;
	}
}
else if($action == "CheckDelete")
{
    $strBookingID = IntegerSafe($strBookingID);
	$arrTempBookingID = explode(",",$strBookingID);
	$arrBookingID = array_unique($arrTempBookingID);
	
	for($i=0; $i<sizeof($arrBookingID); $i++)
	{
		$booking_id = $arrBookingID[$i];
		
		$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = '$booking_id'";
		$arrRoomResult = $lebooking->returnVector($sql);
		
		$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = '$booking_id'";
		$arrFacilitiesResult = $lebooking->returnVector($sql);
		
		if(sizeof($arrRoomResult) > 0) {
			if(($arrRoomResult[0] == 0) || ($arrRoomResult[0] == -1) || ($arrRoomResult[0] == 999)) {
				$pass[$booking_id]['RoomCheck'] = 1;
			} else {
				$pass[$booking_id]['RoomCheck'] = 0;
			}
		} else {
			$pass[$booking_id]['RoomCheck'] = 1;
		}
			
		if(sizeof($arrFacilitiesResult) > 0) {
			if(($arrFacilitiesResult[0] == 0) || ($arrFacilitiesResult[0] == -1) || ($arrFacilitiesResult[0] == 999)) {
				$pass[$booking_id]['FacilityCheck'] = 1;
			} else {
				$pass[$booking_id]['FacilityCheck'] = 0;
			}
		} else {
			$pass[$booking_id]['FacilityCheck'] = 1;
		}
		
		if(in_array(0,$pass[$booking_id])) {
			### Cannot Delete, some request is already approved
			$result[] = -1;
		} else {
			### Can be deleted
			//$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID = $booking_id";
			//$result[] = $lebooking->db_db_query($sql);
			$result[] = 1;
		}
	}
	
	if(!in_array(-1, $result)) {
		echo 1;
	}else{
		echo -1;
	}
}


intranet_closedb();
?>