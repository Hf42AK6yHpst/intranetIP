<?php
//using: 
/*
 *  2019-05-01  Cameron
 *      - apply IntegerSafe to ID fields to fix sql injection problem
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT.'includes/liblog.php');
include_once($PATH_WRT_ROOT."includes/libebooking_door_access.php");
intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$linventory = new libinventory();
$lwebmail = new libwebmail();
$liblog = new liblog();
$lebooking_door_access = new libebooking_door_access();

$sent_email_result = true;

$lebooking->Start_Trans();

$str_BookingID = IntegerSafe($str_BookingID);
$arrTempBookingID = explode(",",$str_BookingID);
$arrTempBookingID = array_unique($arrTempBookingID);
$targetBookingID = implode(",",$arrTempBookingID);

# Select information for delete log
//INTRANET_EBOOKING_RECORD Table
$sql = "SELECT BookingID, Date, StartTime, EndTime, RequestedBy, ResponsibleUID, Event, Remark, RelatedTo, InputBy, ModifiedBy, DateInput, DateModified FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($targetBookingID)";
$ierInfoAry =  $lebooking->returnArray($sql);
$numOfInfo = count($ierInfoAry);
		
for($i=0; $i<$numOfInfo; $i++) {
	$BookingID = $ierInfoAry[$i]['BookingID'];
	$Date = $ierInfoAry[$i]['Date'];
	$StartTime = $ierInfoAry[$i]['StartTime'];
	$EndTime = $ierInfoAry[$i]['EndTime'];
	$RequestedBy = $ierInfoAry[$i]['RequestedBy'];
	$ResponsibleUID = $ierInfoAry[$i]['ResponsibleUID'];
	$Event = $ierInfoAry[$i]['Event'];
	$Remark = $ierInfoAry[$i]['Remark'];
	$RelatedTo = $ierInfoAry[$i]['RelatedTo'];
	$InputBy = $ierInfoAry[$i]['InputBy'];
	$ModifiedBy = $ierInfoAry[$i]['ModifiedBy'];
	$DateInput = $ierInfoAry[$i]['DateInput'];
	$DateModified = $ierInfoAry[$i]['DateModified'];
	$section ='Delete_eAdmin_Booking_Record';
	$tableName = 'INTRANET_EBOOKING_RECORD';
	
	# Start inserting delete log
	$ierTmpAry = array();
	$ierTmpAry['BookingID'] = $BookingID;
	$ierTmpAry['Date'] = $Date;
	$ierTmpAry['StartTime'] = $StartTime;
	$ierTmpAry['EndTime'] = $EndTime;
	$ierTmpAry['RequestedBy'] = $RequestedBy;
	$ierTmpAry['ResponsibleUID'] = $ResponsibleUID;
	$ierTmpAry['Event'] = $Event;
	$ierTmpAry['Remark'] = $Remark;
	$ierTmpAry['RelatedTo'] = $RelatedTo;
	$ierTmpAry['InputBy'] = $InputBy;
	$ierTmpAry['ModifiedBy'] = $ModifiedBy;
	$ierTmpAry['DateInput'] = $DateInput;
	$ierTmpAry['DateModified'] = $DateModified;
	
	$ierSuccessArr['Log_Delete'] = $liblog->INSERT_LOG('eBooking', $section, $liblog->BUILD_DETAIL($ierTmpAry), $tableName, $BookingID);
}

//INTRANET_EBOOKING_BOOKING_DETAILS Table
$sql = "SELECT RecordID, BookingID, FacilityType, FacilityID, PIC, ProcessDate, BookingStatus, InputBy, ModifiedBy, DateInput, DateModified FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetBookingID)";
$iebdInfoAry =  $lebooking->returnArray($sql);
$numOfInfo = count($iebdInfoAry);
		
for($j=0; $j<$numOfInfo; $j++) {
	$RecordID = $iebdInfoAry[$j]['RecordID'];
	$BookingID = $iebdInfoAry[$j]['BookingID'];
	$FacilityType = $iebdInfoAry[$j]['FacilityType'];
	$FacilityID = $iebdInfoAry[$j]['FacilityID'];
	$PIC = $iebdInfoAry[$j]['PIC'];
	$ProcessDate = $iebdInfoAry[$j]['ProcessDate'];
	$BookingStatus = $iebdInfoAry[$j]['BookingStatus'];
	$InputBy = $iebdInfoAry[$j]['InputBy'];
	$ModifiedBy = $iebdInfoAry[$j]['ModifiedBy'];
	$DateInput= $iebdInfoAry[$j]['DateInput'];
	$DateModified = $iebdInfoAry[$j]['DateModified'];
	
	$section ='Delete_eAdmin_Booking_Details';
	$tableName = 'INTRANET_EBOOKING_BOOKING_DETAILS';
	
	# Start inserting delete log
	$iebdTmpAry = array();
	$iebdTmpAry['RecordID'] = $RecordID;
	$iebdTmpAry['BookingID'] = $BookingID;
	$iebdTmpAry['FacilityType'] = $FacilityType;
	$iebdTmpAry['FacilityID'] = $FacilityID;
	$iebdTmpAry['PIC'] = $PIC;
	$iebdTmpAry['ProcessDate'] = $ProcessDate;
	$iebdTmpAry['BookingStatus'] = $BookingStatus;
	$iebdTmpAry['InputBy'] = $InputBy;
	$iebdTmpAry['ModifiedBy'] = $ModifiedBy;
	$iebdTmpAry['DateInput'] = $DateInput;
	$iebdTmpAry['DateModified'] = $DateModified;
	$iebdSuccessArr['Log_Delete'] = $liblog->INSERT_LOG('eBooking', $section, $liblog->BUILD_DETAIL($iebdTmpAry), $tableName, $RecordID);
}

# Update Record
$sql ="";
$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
$result[] = $lebooking->db_db_query($sql);

$sql ="";	
$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
$result[] = $lebooking->db_db_query($sql);

//$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
//$result[] = $lebooking->db_db_query($sql);
$sql ="";
$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
$result[] = $lebooking->db_db_query($sql);

if ($plugin['door_access']) {
	$result['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($arrTempBookingID);
}

if(!in_array(false,$result)) {
	$lebooking->Commit_Trans();	
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordDeleted']));
	exit();
} else {
	$lebooking->RollBack_Trans();
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordDeletedFailed']));
	exit();
}

intranet_closedb();
?>