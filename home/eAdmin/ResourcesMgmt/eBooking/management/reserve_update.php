<?php
// using : 

############################################ Change Log ############################################
##
##  Date     :  2019-05-01 (Cameron)
##              - fix potential sql injection problem by cast variables to integer
##
##  Date     :   2018-05-02 (Henry)
##              Added $iCalEventUpdateArr['EventCalID'] = $EventCalID[$RoomID] for choosing specific calendar to add event for
##
##  Date     :   2015-07-04 (Omas)
##              fix some php 5.4 problem 
##
##  Date     :   2015-04-21 (Omas)
##              modified to update Responsible User
##
##	Date	:	2015-02-04 (Omas)
##				Fix - when multiple room is enabled - fail to upload attachment
##
##	Date	:	2015-01-16 (Omas)
##				New field Booking Category
##
##	Date	:	2014-01-15 (Omas)
##				new $sys_custom['eBooking_MultipleRooms'] - allow reserve multiple rooms for specific time method in eAdmin & eService
##
##	Date	:	2014-08-01 (Carlos)
##				Added $iCalEventArr[0]['EventCalID'] = $EventCalID[$RoomID] for choosing specific calendar to add event for
##
##  Date	:	2013-08-22 (Carlos) 
##				$sys_custom['eBooking_ReserveMultipleRooms'] - allow reserve multiple rooms for specific time method
##
##	Date	:	2012-06-28 (Ivan) [2012-0417-1542-49054]
##				- added customized field "Event" logic
##
##	Date	:	2011-11-14 (YatWoon)
##				Improved: customization for remark ($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
##
##	Date	:	2010-06-09 (Ronald)
##	Details	:	Add a process to delete the current booking for Edit Booking
##

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_door_access.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

StartTimer('bookingRequest');

//Omas
//$FolderPath = GetCommonAttachmentFolderPath($AttachmentArr, $FolderPath);

$From_eAdmin = $_SESSION['EBOOKING_BOOKING_FROM_EADMIN']?1:0;
$BookingCategoryID = IntegerSafe($_REQUEST['BookingCategoryID']);
$CurrentBookingID = IntegerSafe($_REQUEST['CurrentBookingID']);

$FacilityType = IntegerSafe($_REQUEST['FacilityType']);
if ($FacilityType == 1)
    $RoomID = IntegerSafe($_REQUEST['FacilityID']);
else
    $ItemID = IntegerSafe($_REQUEST['FacilityID']);

    $ResponsibleUID = IntegerSafe($_REQUEST['ResponsibleUID']);
$SingleItemIDArr = $_REQUEST['SingleItemID'];
$SettingsArr['CancelDayBookingIfOneItemIsNA'] = $_REQUEST['CancelDayBookingIfOneItemIsNA'];
// $BookingRemarks = stripslashes($_REQUEST['BookingRemarks']);
$BookingRemarks = $_REQUEST['BookingRemarks'];
if(is_array($BookingRemarks)){
	foreach((array)$BookingRemarks as $key => $val){
		$BookingRemarks[$key] = stripslashes($val);
	}
}
else{
	$BookingRemarks = stripslashes($BookingRemarks);
}
$iCalenderEvent = $_REQUEST['iCalEvent'];
$StartTime_TimeSlotID_Mapping_Arr = unserialize(rawurldecode($_REQUEST['StartTime_TimeSlotID_Mapping_Arr']));
$BookingEvent = trim(stripslashes($_REQUEST['BookingEvent']));
$ApplyDoorAccess = $_POST['ApplyDoorAccess'];

if(($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)) && $BookingMethod == LIBEBOOKING_DEFAULT_BOOKING_METHOD_TIME && $FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM){
	$isReserveMultipleRooms = true;
	
	//debug_r($_REQUEST);
	//intranet_closedb();
	//exit;
}

$lebooking = new libebooking();
$lebooking_door_access = new libebooking_door_access();
//if ($FacilityType == 2)	// item
//{
//	if($SelectMethod == "Specific")
//		$TimeRangeArr = $_REQUEST['SpecificTimeRangeArr'];
//	else
//		$TimeRangeArr = $_REQUEST['TimeRangeArr'];
//	$DateList = $_REQUEST['DateList'];
//	$DateArr = explode(',', $DateList);
//	
//	list($StartTimeArr, $EndTimeArr, $StartTime_TimeSlotID_Mapping_Arr) = $lebooking->Get_StartTime_EndTime_TimeSlotID_Array_By_TimeRange_Array($TimeRangeArr);
//
//	$TmpTimeArr = $lebooking->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr);
//	$DateTimeArr = $TmpTimeArr[0];
//}
//else if ($FacilityType == 1)	// location
//{
//	$DateTimeArr = unserialize(rawurldecode($_REQUEST['DateTimeArr']));
//}
$DateTimeArr = unserialize(rawurldecode($_REQUEST['DateTimeArr']));

if($isReserveMultipleRooms) {
	include_once($PATH_WRT_ROOT."includes/libinventory.php");
	$linventory = new libinventory();
	
	$FacilityIDArr = explode(",",$_REQUEST['FacilityID']);
	
	$ResponsibleUID = $_REQUEST['ResponsibleUID'];
	$SingleItemIDArr = $_REQUEST['SingleItemID'];
	$SettingsArr['CancelDayBookingIfOneItemIsNA'] = $_REQUEST['CancelDayBookingIfOneItemIsNA'];
	$BookingRemarks = $_REQUEST['BookingRemarks'];
	$iCalenderEvent = $_REQUEST['iCalEvent'];
	$BookingEvent = $_REQUEST['BookingEvent'];
	$ApplyDoorAccess = $_POST['ApplyDoorAccess'];
	$MultipleRoomFolderPath = $_REQUEST['MultipleRoomFolderPath'];
	
	
	$SuccessArr = array();
	for($i=0;$i<count($FacilityIDArr);$i++) {
		$RoomID = $FacilityIDArr[$i];
		$BookingRemarks[$RoomID] = trim(stripslashes($BookingRemarks[$RoomID]));
		$BookingEvent[$RoomID] = trim(stripslashes($BookingEvent[$RoomID]));
		$FolderPath = GetCommonAttachmentFolderPath($AttachmentArr[$RoomID], $MultipleRoomFolderPath[$RoomID]);
				
		$iCalEventArr = array();
		$CustRemarkAry = array();
		
		### Prepare iCalendar Event array
		if($iCalenderEvent[$RoomID] == 1) {
			if($ResponsibleUID[$RoomID] == "") {
				$OwnerID = $_SESSION['UserID'];
			} else {
				$OwnerID = $ResponsibleUID[$RoomID];
			}
			
			if($RoomID != "") {
				$sql = "SELECT 
							CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
						FROM
							INVENTORY_LOCATION_BUILDING AS building
							INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
							INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
						WHERE 
							room.LocationID = '$RoomID'";
				$arrLocation = $linventory->returnVector($sql);
				$location = $arrLocation[0];
			} else {
				$location = "";
			}
			
			$iCalEventArr[0]['Owner'] = $OwnerID;
			$iCalEventArr[0]['EventTitle'] = $iCalEventTitle[$RoomID];
			$iCalEventArr[0]['EventDesc'] = $BookingRemarks[$RoomID];
			$iCalEventArr[0]['Location'] = $location;
			$iCalEventArr[0]['EventCalID'] = $EventCalID[$RoomID];
		}
		
		if(trim($iCalEventTitle[$RoomID]) != "")
		{
			$iCalEventUpdateArr['EventTitle'] = $iCalEventTitle[$RoomID];
			$iCalEventUpdateArr['EventDesc'] = $BookingRemarks[$RoomID];
			$iCalEventUpdateArr['EventCalID'] = $EventCalID[$RoomID];
		}
		
		if($From_eAdmin){
			$SuccessArr['New_Room_Reserve_RoomID_'.$RoomID] = $lebooking->New_Room_Reserve($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr[$RoomID], $BookingRemarks[$RoomID], $ResponsibleUID[$RoomID], $SettingsArr, $iCalEventArr, $FolderPath, $CustRemarkAry, $BookingEvent[$RoomID], $ApplyDoorAccess[$RoomID],$BookingCategoryID);
		}
		else{
			$SuccessArr['New_Room_Reserve_RoomID_'.$RoomID] = $lebooking->New_Room_Booking($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr[$RoomID], $BookingRemarks[$RoomID], $ResponsibleUID[$RoomID], $SettingsArr, $iCalEventArr, $FolderPath, $CustRemarkAry, $BookingEvent[$RoomID], $ApplyDoorAccess[$RoomID],$BookingCategoryID);
		}
	}
	
	if (in_array(false, $SuccessArr))
		$ReturnMsgKey = 'NewBookingFailed';
	else
		$ReturnMsgKey = 'NewBookingSuccess';
	
	$url = $From_eAdmin?"booking_request.php":$PATH_WRT_ROOT."home/eService/eBooking/index_list.php"; 
	intranet_closedb();

	header("Location: $url?ReturnMsgKey=$ReturnMsgKey");
	exit;
}

### Prepare iCalendar Event array
if($iCalenderEvent == 1) {
	include_once($PATH_WRT_ROOT."includes/libinventory.php");
	$linventory = new libinventory();
	
	if($ResponsibleUID == "") {
		$OwnerID = $_SESSION['UserID'];
	} else {
		$OwnerID = $ResponsibleUID;
	}
	
	if($RoomID != "") {
		$sql = "SELECT 
					CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
				FROM
					INVENTORY_LOCATION_BUILDING AS building
					INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
					INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
				WHERE 
					room.LocationID = '$RoomID'";
		$arrLocation = $linventory->returnVector($sql);
		$location = $arrLocation[0];
	} else {
		$location = "";
	}
	
	$iCalEventArr[0]['Owner'] = $OwnerID;
	$iCalEventArr[0]['EventTitle'] = $iCalEventTitle;
	$iCalEventArr[0]['EventDesc'] = $BookingRemarks;
	$iCalEventArr[0]['Location'] = $location;
	$iCalEventArr[0]['EventCalID'] = $EventCalID[$RoomID];
	//$lebooking->CreatePersonalCalenderEvent($OwnerID,$DateTimeArr,$StartTime_TimeSlotID_Mapping_Arr,$iCalEventTitle,$BookingRemarks,$RoomID);
}

if(trim($iCalEventTitle) != "")
{
	$iCalEventUpdateArr['EventTitle'] = $iCalEventTitle;
	$iCalEventUpdateArr['EventDesc'] = $BookingRemarks;
	$iCalEventUpdateArr['EventCalID'] = $EventCalID[$RoomID];
}

$CustRemarkAry = array();
if($sys_custom['eBooking_Cust_Remark']['WongKamFai']) 
{
	$CustRemarkAry['ActivityType'] = $ActivityType;
	$CustRemarkAry['ActivityName'] = $ActivityName;
	$CustRemarkAry['Attendance'] = $Attendance;
	$CustRemarkAry['PIC'] = $PIC;
	$CustRemarkAry['IT_Teachnician'] = $IT_Teachnician;
	$CustRemarkAry['Photographer'] = $Photographer;
	$CustRemarkAry['Clerical'] = $Clerical;
	$CustRemarkAry['Janitor'] = $Janitor;
	$CustRemarkAry['PhotoTaking'] = $PhotoTaking;
	$CustRemarkAry['VideoShooting'] = $VideoShooting;
	$CustRemarkAry['Projector'] = $Projector;
	$CustRemarkAry['Visualizer'] = $Visualizer;
	$CustRemarkAry['Microphone'] = $Microphone;
	$CustRemarkAry['MobilePASystem'] = $MobilePASystem;
	$CustRemarkAry['Laptop'] = $Laptop;
	$CustRemarkAry['Signage'] = $Signage;
	$CustRemarkAry['Souvenir'] = $Souvenir;
	$CustRemarkAry['Stationery'] = $Stationery;
	$CustRemarkAry['Flower'] = $Flower;
	$CustRemarkAry['CateringServicer'] = $CateringServicer;
	$CustRemarkAry['Others'] = $Others;
}

$FolderPath = GetCommonAttachmentFolderPath($AttachmentArr, $FolderPath);
$successAry = array();
if($CurrentBookingID != "")
{
	$successAry['update'] = $lebooking->Update_Related_Booking_Detail($CurrentBookingID, $BookingRemarks, $FolderPath, $iCalEventUpdateArr, $CustRemarkAry, $BookingEvent, $ApplyDoorAccess,$ResponsibleUID);
	if ($plugin['door_access']) {
		$successAry['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($CurrentBookingID);
	}
	
	
	if (in_array(false, (array)$successAry)) {
		$ReturnMsgKey = 'EditBookingFailed';
	}
	else {
		$ReturnMsgKey = 'EditBookingSuccess';
	}
		
		
	### Remove the existing Booking Request
//	$sql = "
//		DELETE 	
//			br, 
//			bd 
//		FROM 
//			INTRANET_EBOOKING_RECORD br 
//			INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS bd ON br.BookingID = bd.BookingID 
//		WHERE 
//			br.RelatedTo = $CurrentBookingID";
//	$lebooking->db_db_query($sql);

//	$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $CurrentBookingID";
//	$lebooking->db_db_query($sql);
	
//	$sql = "SELECT MIN(BookingID) FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $CurrentBookingID";
//	$NewRelatedTo = $lebooking->returnVector($sql);
//	
//	$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RelatedTo = ".$NewRelatedTo[0]." WHERE RelatedTo = $CurrentBookingID";
//	$lebooking->db_db_query($sql);
	
//	$sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = $CurrentBookingID";
//	$arrExistingEvent = $lebooking->returnVector($sql);
//	
//	if(sizeof($arrExistingEvent) > 0) {
//		$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = ".$arrExistingEvent[0];
//		$lebooking->db_db_query($sql);
//		
//		$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS WHERE EventID = ".$arrExistingEvent[0];
//		$lebooking->db_db_query($sql);
//		
//		$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = ".$CurrentBookingID;
//		$lebooking->db_db_query($sql);
//	}
}
else
{	
	### Create Booking Request
	if($From_eAdmin)
	{
		if ($FacilityType == 1)	// location
			$SuccessArr['New_Room_Reserve'] = $lebooking->New_Room_Reserve($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr, $iCalEventArr, $FolderPath, $CustRemarkAry, $BookingEvent, $ApplyDoorAccess,$BookingCategoryID);
		else if ($FacilityType == 2)	// item
			$SuccessArr['New_Item_Reserve'] = $lebooking->New_Item_Reserve($ItemID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $BookingRemarks, $ResponsibleUID, $iCalEventArr, $FolderPath, $CustRemarkAry, $BookingEvent,$BookingCategoryID);
	}
	else
	{
		if ($FacilityType == 1)	// location
			$SuccessArr['New_Room_Booking'] = $lebooking->New_Room_Booking($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr, $iCalEventArr, $FolderPath, $CustRemarkAry, $BookingEvent, $ApplyDoorAccess,$BookingCategoryID);
		else if ($FacilityType == 2)	// item
			$SuccessArr['New_Item_Booking'] = $lebooking->New_Item_Booking($ItemID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $BookingRemarks, $ResponsibleUID, $iCalEventArr, $FolderPath, $CustRemarkAry, $BookingEvent,$BookingCategoryID);
	}
	
	if (in_array(false, $SuccessArr))
		$ReturnMsgKey = 'NewBookingFailed';
	else
		$ReturnMsgKey = 'NewBookingSuccess';
}

$url = $From_eAdmin?"booking_request.php":$PATH_WRT_ROOT."home/eService/eBooking/index_list.php"; 

$runTime = StopTimer($precision=5, $NoNumFormat=false, 'bookingRequest');

//// For performance tunning info
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('$runTime = '.$runTime.'s');
//debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
////$lebooking->db_show_debug_log();
//$lebooking->db_show_debug_log_by_query_number(2);
//die();

intranet_closedb();

header("Location: $url?ReturnMsgKey=$ReturnMsgKey");
exit;
?>