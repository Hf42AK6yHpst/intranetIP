<?php
// using :  
/*********************************************** Change Log *****************************************************
 * 2019-04-30 (Cameron): apply IntegerSafe to ID fields to fix sql injection problem 
 * 2018-12-11 (Cameron): also update ProcessDateTime so that it can show the approve time
 * 2015-01-14 (Omas)  :	modified to merge followup email by send SendEmailToFollowUpGroup_Merged()
 * 2014-10-06 (Bill)  : modified to improve the login to first-come-first-serve 
 * 2011-10-31 (Marcus): modified rollback transaction logic for each record, commit the record even if the email cannot send out.
 * 2011-03-07 (Carlos): modified rollback transaction logic for each record, not rollback whole batch of records
 ****************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libebooking_door_access.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$linventory = new libinventory();
$lwebmail = new libwebmail();
$lebooking_door_access = new libebooking_door_access();

$sent_email_result = true;
$emailBookingIdAry = array();

//$lebooking->Start_Trans();
$str_BookingID = IntegerSafe($str_BookingID);
$arrTempBookingID = explode(",",$str_BookingID);
$arrTempBookingID = array_unique($arrTempBookingID);

foreach($arrTempBookingID as $key=>$val)
{
	$arrBookingID[] = $val;
}

$OverallResult = array();
$i = 0;
if(sizeof($arrBookingID) > 0)
{
	// To ensure first-come-first-serve
	$BookingRecordArr = $lebooking->Get_Booking_Record_By_BookingID($arrBookingID, $OrderByTime=true);
	$BookingRecordArr = BuildMultiKeyAssoc($BookingRecordArr, "BookingID");
//	for($i=0; $i<sizeof($arrBookingID); $i++)
	foreach($BookingRecordArr as $currentBooking)
	{
		$result=array();
		$lebooking->Start_Trans();
		
//		$booking_id = $arrBookingID[$i];
		$booking_id = IntegerSafe($currentBooking['BookingID']);
		
		$arrBookingDetails = ${$booking_id."BookingRecord"};

		if(is_array($arrBookingDetails))
		{
			$TotalNumOfBookingDetail = $NumOfRoomBooking[0] + $NumOfItemBooking[0];
			for($j=0; $j<sizeof($arrBookingDetails); $j++)
			{
				if(strpos($arrBookingDetails[$j],"ROOM_") === 0)
				{
					$FacilityType = LIBEBOOKING_FACILITY_TYPE_ROOM;
					$FacilityID = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
				}
				else if(strpos($arrBookingDetails[$j],"ITEM_") === 0)
				{
					$FacilityType = LIBEBOOKING_FACILITY_TYPE_ITEM;
					$FacilityID = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
				}
				else
				{
				}
				
				# Approve selected facility
				$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET PIC = '$UserID', ProcessDate = NOW(), ProcessDateTime = NOW(),  BookingStatus = 1 WHERE FacilityID = '$FacilityID' AND BookingID = '$booking_id'  AND FacilityType = '".$FacilityType."'";
				$result['UpdateRecordDetails_'.$i.'_'.$j] = $lebooking->db_db_query($sql);

				# Reject other overlapped booking record
				$BookingDate = $BookingRecordArr[$booking_id]["Date"];
				$StartTime = $BookingRecordArr[$booking_id]["StartTime"];
				$EndTime = $BookingRecordArr[$booking_id]["EndTime"];
				
				$DateTimeArr = $lebooking->Build_DateTimeArr($BookingDate, $StartTime, $EndTime);

				//2013-0910-1707-32073
				//$result['RejectDuplicateRecord_'.$i.'_'.$j] = $lebooking->Reject_Approved_Or_Pending_Booking_By_DateTimeArr($FacilityType, $FacilityID, $DateTimeArr, $booking_id);
				$thisEmailBookingIdAry = $lebooking->Reject_Approved_Or_Pending_Booking_By_DateTimeArr($FacilityType, $FacilityID, $DateTimeArr, $booking_id, $sendEmail=false, $returnEmailBookingIdAry=true);
				$emailBookingIdAry = array_merge((array)$emailBookingIdAry, (array)$thisEmailBookingIdAry);
			}
			
			## if all the related resource is processed, send mail notice
//			if($lebooking->Check_All_Related_Booking_Request_Is_Processed($booking_id))
//			{
//				$sent_email_result = $lebooking->Email_Booking_Result($booking_id);
//				$sent_follow_up_mail = $lebooking->SendMailToFollowUpGroup($booking_id);
//			}
			
			if($lebooking->Check_All_Related_Booking_Request_Is_Processed($booking_id))
			{
				//2013-0910-1707-32073
				//$sent_email_result = $lebooking->Email_Booking_Result($booking_id);
				$emailBookingIdAry[] = $booking_id;

				//$sent_follow_up_mail = $lebooking->SendMailToFollowUpGroup($booking_id);
				
//				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = $booking_id";
//				$arrTotalNumOfRoomDetail = $lebooking->returnVector($sql);
//				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = $booking_id";
//				$arrTotalNumOfItemDetail = $lebooking->returnVector($sql);
//				$TotalNumOfBookingResource = $arrTotalNumOfRoomDetail[0] + $arrTotalNumOfItemDetail[0];
//				
//				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = $booking_id AND BookingStatus = 1";
//				$arrNumOfApprovedRoomBooking = $lebooking->returnVector($sql);
//				$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = $booking_id AND BookingStatus = 1";
//				$arrNumOfApprovedItemBooking = $lebooking->returnVector($sql);
//				$TotalNumOfApprovedBooking = $arrNumOfApprovedRoomBooking[0] + $arrNumOfApprovedItemBooking[0];

				$sql = "
					SELECT
						COUNT(*), COUNT(IF(BookingStatus = 1,1,NULL))
					FROM		
						INTRANET_EBOOKING_BOOKING_DETAILS
					WHERE 
						BookingID = '$booking_id'
				";
				list($TotalNumOfBookingResource, $TotalNumOfApprovedBooking) =  $lebooking->returnArray($sql);
				
				if($TotalNumOfBookingResource == $TotalNumOfApprovedBooking)
				{
					$sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$booking_id' AND EventID != 0";
					$EventID = $lebooking->returnVector($sql);
					
					if(sizeof($EventID) > 0) {
						
						## UPDATE Calendar Event
						$sql = "SELECT 
									UserID, CalID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, IsAllDay, 
									Access, Title, Description, Location, Url, UID, ExtraIcalInfo 
								FROM 
									INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS WHERE BookingID = '$booking_id'";
						$arrResult = $lebooking->returnArray($sql);
						
						if(sizeof($arrResult)>0) {
							list($OwnerID, $calID, $event_date, $input_date, $modified_date, $duration, $is_important, $is_all_day, $access, $event_title, $event_desc, $location, $url, $uid, $extra_cal_info) = $arrResult[0];
							
							$sql = "UPDATE 
										CALENDAR_EVENT_ENTRY 
									SET 
										UserID = '$OwnerID',
										CalID = '$calID',
										EventDate = '$event_date',
										InputDate = '$input_date',
										ModifiedDate = '$modified_date',
										Duration = '$duration',
										IsImportant = '$is_important',
										IsAllDay = '$is_all_day',
										Access = '$access',
										Title = '$event_title',
										Description = '$event_desc',
										Location = '$location',
										Url = '$url',
										UID = '$uid',
										ExtraIcalInfo = '$extra_cal_info'
									WHERE
										EventID = '".$EventID[0]."'";
							$lebooking->db_db_query($sql);
						}
						
					} else {
						
						## Create iCalendar Event
						$sql = "INSERT INTO 
									CALENDAR_EVENT_ENTRY 
									(UserID, CalID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, IsAllDay, 
									Access, Title, Description, Location, Url, UID, ExtraIcalInfo)
								SELECT 
									UserID, CalID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, IsAllDay, 
									Access, Title, Description, Location, Url, UID, ExtraIcalInfo 
								FROM 
									INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS WHERE BookingID = '$booking_id'";
						$lebooking->db_db_query($sql);
	
						$iCalEventID = $lebooking->db_insert_id();
						
						$sql = "SELECT UserID, CalID FROM INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS WHERE BookingID = '$booking_id'";
						$arrInfo = $lebooking->returnArray($sql,2);
						
						if(sizeof($arrInfo)>0)
						{
							list($OwnerID, $calID) = $arrInfo[0];
						}
						
						global $schoolNameAbbrev;
						$uid = $OwnerID."-{$calID}-{$iCalEventID}@{$schoolNameAbbrev}.tg";
						$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$iCalEventID'";
						$lebooking->db_db_query($sql);
						
						$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$booking_id'";
						$RecordExist = $lebooking->returnVector($sql);
						
						if($RecordExist[0] > 0) {
							$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$iCalEventID' WHERE BookingID = '$booking_id'";
							$lebooking->db_db_query($sql);
						} else {
							$sql = "INSERT INTO INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (BookingID, EventID) VALUES ('$booking_id', '$iCalEventID')";
							$lebooking->db_db_query($sql);
						}
					}
				}
			}
		}
		
		if ($plugin['door_access']) {
			$result['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($booking_id);
		}
		
		
//		if(!in_array(false,$result) && $sent_email_result){ 
//		Approve the record even if the email failed to be sent.
		if(!in_array(false,$result) ){
			$lebooking->Commit_Trans();
			$OverallResult[] = true;
		}else{
			$lebooking->RollBack_Trans();
			$OverallResult[] = false;
		}
		$i++;
	}

	$emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
	if (count($emailBookingIdAry) > 0) {
		// return true even failed to send email
		$SuccessArr['Send_eMail'] = $lebooking->Email_Booking_Result_Merged($emailBookingIdAry);
		$sent_follow_up_mail = $lebooking->SendEmailToFollowUpGroup_Merged($emailBookingIdAry);
	}
}
//die();
intranet_closedb();
if(!in_array(false,$OverallResult))
{
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedSuccessfully']));
	exit();
}else
{
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedFailed']));
	exit();
}
/*
if((!in_array(false,$result)) && ($sent_email_result))
{
	$lebooking->Commit_Trans();	
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedSuccessfully']));
	exit();
}
else
{
	$lebooking->RollBack_Trans();
	header("Location: booking_request.php?msg=".urlencode($Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedFailed']));
	exit();
}

intranet_closedb();
*/
?>