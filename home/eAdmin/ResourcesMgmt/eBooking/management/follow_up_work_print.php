<?php
//using : 
/*
 *  2019-05-01 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$lebooking_ui = new libebooking_ui();
$CurrentPageArr['eBooking'] = 1;

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$arrFollowUpCalStyle['#b00408'] = 'cal_event_01';
$arrFollowUpCalStyle['#e97e0b'] = 'cal_event_02';
$arrFollowUpCalStyle['#d2a800'] = 'cal_event_03';
$arrFollowUpCalStyle['#467a13'] = 'cal_event_04';
$arrFollowUpCalStyle['#809121'] = 'cal_event_05';
$arrFollowUpCalStyle['#4db308'] = 'cal_event_06';
$arrFollowUpCalStyle['#8ad00f'] = 'cal_event_07';
$arrFollowUpCalStyle['#09759d'] = 'cal_event_08';
$arrFollowUpCalStyle['#21a6ae'] = 'cal_event_09';
$arrFollowUpCalStyle['#65979b'] = 'cal_event_10';
$arrFollowUpCalStyle['#2f75e9'] = 'cal_event_11';
$arrFollowUpCalStyle['#3ea6ff'] = 'cal_event_12';
$arrFollowUpCalStyle['#1e2cd8'] = 'cal_event_13';
$arrFollowUpCalStyle['#8c66d9'] = 'cal_event_14';
$arrFollowUpCalStyle['#994499'] = 'cal_event_15';
$arrFollowUpCalStyle['#5c1fa0'] = 'cal_event_16';
$arrFollowUpCalStyle['#dd4477'] = 'cal_event_17';
$arrFollowUpCalStyle['#dd4477'] = 'cal_event_18';
$arrFollowUpCalStyle['#ac5c14'] = 'cal_event_19';
$arrFollowUpCalStyle['#7f4b21'] = 'cal_event_20';
$arrFollowUpCalStyle['#000000'] = 'cal_event_00';

$isPrint = 1;
$include_JS_CSS = $lebooking_ui->initJavaScript();

$FollowUpGroup = IntegerSafe($FollowUpGroup);
$table_content .= $lebooking_ui->ShowFollowUpWorkPrintPage($WeekStartTimeStamp,$FollowUpGroup,$RemarkSelection);

$arrTargetFollowUpGroup = explode(",",$FollowUpGroup);

$sql = "SELECT GroupName, GroupColor FROM INTRANET_EBOOKING_FOLLOWUP_GROUP WHERE GroupID IN ($FollowUpGroup) Order By GroupName";
$arrGroupInfo = $lebooking_ui->returnArray($sql,3);

if(sizeof($arrGroupInfo) > 0){
	$group_info_content = '<table border="0">';
	$group_info_content .= '<tr><td>'.$Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'].' : </td>';
	for($i=0; $i<sizeof($arrGroupInfo); $i++){
		list($group_name, $group_color) = $arrGroupInfo[$i];
		
		$span_css = $arrFollowUpCalStyle[$group_color];
		$x = '<td><span class="'.$span_css.'" style="display:block; padding-top:2px; padding-left:2px; padding-bottom:2px; padding-right:2px;" >'.$group_name.'</span></td>';
		
		$group_info_content .= $x;
	}
	if(in_array("-999",$arrTargetFollowUpGroup)){
		$span_css = $arrFollowUpCalStyle['#000000'];
		$group_info_content .= '<td><span class="'.$span_css.'" style="display:block; padding-top:2px; padding-left:2px; padding-bottom:2px; padding-right:2px;" >'.$Lang['eBooking']['Settings']['FieldTitle']['NoFollowUpGroup'].'</span></td>';
	}
	$group_info_content .= '</tr>';
	$group_info_content .= '</table>';
}

?>
<?=$include_JS_CSS;?>

<br>

<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print">
			<?=$Lang['eBooking']['Management']['FieldTitle']['FollowUpWorkSchedule'];?>
		</td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$group_info_content;?>
		</td>
	</tr>
</table>

<br>

<table width='90%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td>
			<?=$table_content;?>
		</td>
	</tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>