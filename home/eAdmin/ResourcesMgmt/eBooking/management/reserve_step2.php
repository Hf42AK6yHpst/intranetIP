<?php
// using :  
###################################################### Change Log ###############################################################
## 2019-07-02 (Cameron) - fix: get $DateArr by intersect of dates of "Skip School Day" and selected dates (weekly or cycle) for Booking Method = Specific Time for EverydayTimetable
## 2019-06-26 (Cameron) - build date array for $sys_custom['eBooking']['EverydayTimetable']
## 2019-05-01 (Cameron) - fix potential sql injection problem by cast related variables to integer
## 2015-01-16 (Omas) - new Setting - Booking Category enable by $sys_custom['eBooking_BookingCategory']
## 2015-01-15 (Omas) - new $sys_custom['eBooking_MultipleRooms'] - allow reserve multiple rooms for specific time method in eAdmin & eService
## 2013-08-22 (Carlos) - $sys_custom['eBooking_ReserveMultipleRooms'] - allow reserve multiple rooms for specific time method
#################################################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


$linterface 	= new interface_html();
//$linventory	= new libinventory();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);

$From_eAdmin = $_SESSION['EBOOKING_BOOKING_FROM_EADMIN']?1:0;

if($From_eAdmin)
{
	$CurrentPageArr['eBooking'] = 1;
	$CurrentPage	= "PageManagement_BookingRequest";
	$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest']);
}
else
{
	$CurrentPage	= "eService_MyBookingRecord";
	$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
	$From_eService=1;
}
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService);

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$FacilityType = IntegerSafe($_REQUEST['FacilityType']);
$FacilityID = $_REQUEST['FacilityID'];
$RoomID = $_REQUEST['RoomID'];
$ItemID = IntegerSafe($_REQUEST['ItemID']);
$BookingMethod = IntegerSafe($BookingMethod);
$BookingType = IntegerSafe($BookingType);
$StoredValueForBack = unserialize(urldecode($StoredValueForBack));


if($FromStep == 3) // restore selected value
{
		if($StoredValueForBack['RepeatType']==1) // Weekday
		{
			$RepetitiveDateArr = Get_Dates_By_Repetitive_WeekDay_Within_Range($StoredValueForBack['StartDate'], $StoredValueForBack['EndDate'], $StoredValueForBack['SelectedWeekDay']);
			$DateArr = array_keys($RepetitiveDateArr);
		}
		else
		{
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			$lcycleperiods = new libcycleperiods();
			$CycleDayArr = $lcycleperiods->Get_Cycle_Day_Info_By_Date_Range($StoredValueForBack['StartDate'], $StoredValueForBack['EndDate'], $StoredValueForBack['SelectedCycleDay']);
			$DateArr = Get_Array_By_Key($CycleDayArr, "RecordDate");
		}
}
else if($FromStep == 1)
{
		$StoredValueForBack['StartDate'] = $StartDate;
		$StoredValueForBack['EndDate'] = $EndDate;
		$StoredValueForBack['RepeatType'] = $RepeatType;
		
		if($RepeatType==1) // Weekday
		{
			$RepetitiveDateArr = Get_Dates_By_Repetitive_WeekDay_Within_Range($StartDate, $EndDate, $SelectedWeekDay);
			$DateArr = array_keys((array)$RepetitiveDateArr);
	
			$StoredValueForBack['SelectedWeekDay'] = $SelectedWeekDay;
		}
		else // cycle day
		{
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			$lcycleperiods = new libcycleperiods();
			$CycleDayArr = $lcycleperiods->Get_Cycle_Day_Info_By_Date_Range($StartDate, $EndDate, $SelectedCycleDay);
			$DateArr = Get_Array_By_Key($CycleDayArr, "RecordDate");
			
			$StoredValueForBack['SelectedCycleDay'] = $SelectedCycleDay;
		}
}

if ($sys_custom['eBooking']['EverydayTimetable'] && $BookingMethod == LIBEBOOKING_DEFAULT_BOOKING_METHOD_TIME) {
    $academicYearID = Get_Current_Academic_Year_ID();
    $skipDateArr = $lebooking_ui->getSkipSchoolDates($academicYearID, $StoredValueForBack['StartDate'], $StoredValueForBack['EndDate'], $FacilityType, $FacilityID);
    $dateIntersectArr = array_values(array_intersect($DateArr, $skipDateArr));
    unset($DateArr);
    $DateArr = $dateIntersectArr;
}

if(!empty($DateArr))
	sort($DateArr);

if(($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)) && $BookingMethod == LIBEBOOKING_DEFAULT_BOOKING_METHOD_TIME && $FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM){
	$isReserveMultipleRooms = true;
	// Expected $FacilityID to be array type
	if(!is_array($FacilityID)) {
		$RoomID = explode(",",$FacilityID);
	}else{
		$RoomID = $FacilityID;
	}
}else{
	// Normal flow
	if ($FacilityType == 2)	// item
		$ItemID = ($FacilityID=='')? $ItemID : $FacilityID;
	else if ($FacilityType == 1)	// location
		$RoomID = ($FacilityID=='')? $RoomID : $FacilityID;
}
	
$SelectedTimeRangeArr = unserialize(rawurldecode($_REQUEST['SelectedTimeRangeArr']));

echo $lebooking_ui->Get_New_Booking_Step2_UI($DateArr, $FacilityType, $RoomID, $ItemID, $SelectedTimeRangeArr[0], $CurrentBookingID, $From_eAdmin ,$BookingMethod, $BookingType, $StoredValueForBack,$BookingCategoryID);
?>

<script language="javascript">
var ajaxLoadingImg = '<?=$linterface->Get_Ajax_Loading_Image($noLang=0, $custLang='')?>';
var SelectMethod = $("input#SelectMethod").val(); // TimeSlot/Specific  
var FinalDateStr = "<?=implode(",",(array)$StoredValueForBack['FinalDateArr'])?>";

$(document).ready( function() {
	CheckTimeCrash(<?=$From_eAdmin?1:0?>, 1);
});

function js_Show_Event_Title()
{
	if($("#iCalEvent").attr('checked')) {
		$("#div_iCalInfo").show();
	} else {
		$("#div_iCalInfo").hide();
	}
}

function js_Go_Back_Step1()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'reserve_step1.php';
	objForm.submit();
}

function js_Go_Cancel_Booking()
{
	var objForm = document.getElementById('form1');
	if(<?=$From_eAdmin?>)
		objForm.action = 'booking_request.php';
	else
		objForm.action = '<?=$PATH_WRT_ROOT?>home/eService/eBooking/index_list.php';
	objForm.submit();
}

function js_Go_Step3()
{
	if(SelectMethod=="TimeSlot")
	{
		if (js_Check_Selected_TimeSlot() == true)
		{
			var objForm = document.getElementById('form1');
			objForm.action = 'reserve_step3.php';
			objForm.submit();
		}
	}
	else if(SelectMethod=="Specific")
	{
		if (js_Check_Selected_Specific() == true)
		{
			var objForm = document.getElementById('form1');
			objForm.action = 'reserve_step3.php';
			objForm.submit();
		}
	}
}

//function js_Go_New_Item_Booking()
//{
//	$("input#SelectMethod").val(SelectMethod);
//	if(SelectMethod=="TimeSlot")
//	{
//		if (js_Check_Selected_TimeSlot() == true)
//		{
//			var objForm = document.getElementById('form1');
//			objForm.action = 'reserve_update.php';
//			objForm.submit();
//		}
//	}
//	else if(SelectMethod=="Specific")
//	{
//		if (js_Check_Selected_Specific() == true)
//		{
//			var objForm = document.getElementById('form1');
//			objForm.action = 'reserve_update.php';
//			objForm.submit();
//		}
//	}
//}

//function UpdateResponsiblePerson(jsUserID, jsUserName)
//{
//	$('input#ResponsibleUID').val(jsUserID);
//	$('span#ResponsibleUserNameSpan').html(jsUserName);
//}

function js_Check_Selected_TimeSlot()
{
	var jsCheckedTimeSlot = 0;
	$('input.TimeSlotChk').each( function () {
		if ($(this).attr('checked') == true)
			jsCheckedTimeSlot++;
	});
	
	if (jsCheckedTimeSlot == 0)
	{
		alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectTimeSlot']?>");
		$('input.TimeSlotChk').each( function () {
			$(this).focus();
			return false;
		});
		return false;
	}
	
	return true;
}

function js_Check_Selected_Specific()
{
	var jsCheckedSpecific = 0;
	jsCheckedSpecific = $('input.DateCheckBox:checked').length;
	
	if (jsCheckedSpecific == 0)
	{
		alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectTimeSlot']?>");
		return false;
	}
	else
	{
		var isFirst = 1;
		var DateListStr = '';
		$('input.DateCheckBox:checked').each(function(){
			var delimiter = isFirst==1?"":",";
			DateListStr += delimiter+$(this).val();
			isFirst = 0;
		});
		$("input#SelectedDateList").val(DateListStr);
	}
	
	return true;
}

//function js_Change_To_Period_View(View)
//{
//
//	if(View=="Specific")
//	{
//		SelectMethod= "Specific";
//		$("div#SpecificTableDiv").show();
//		$("div#PeriodTableDiv").hide();
//	}
//	else
//	{
//		SelectMethod= "TimeSlot";
//		$("div#SpecificTableDiv").hide();
//		$("div#PeriodTableDiv").show();
//	} 
//}

			
function CheckTimeCrash(AllowReserve, DefaultSelectedOnPageLoad)
{
	var AllowReserve = AllowReserve|| "";
	var SH =  $("#StartHour").val()
	var SM =  $("#StartMin").val()
	var EH =  $("#EndHour").val()
	var EM =  $("#EndMin").val()
	
	if(SH>EH || (SH==EH&&SM>=EM))
	{
		var data = "<?=addslashes($linterface->Get_Warning_Message_Box($Lang['General']['Instruction'],$Lang['eBooking']['eService']['JSWarningArr']['InvalidDurationTime']))?>";
		$("div#TimeCheckList").html(data)
		
//		alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['InvalidDurationTime']?>");
		return false
	}
	
	if(DefaultSelectedOnPageLoad)
		var SelectedDateList =  FinalDateStr;
	else
		var SelectedDateList =  "";
	
	var DateRowArr = $("#DateList").val().split(",");
	
	var DateList = new Array();
	var StartTimeArr = new Array();
	var EndTimeArr = new Array();
	for(var i=0; i<DateRowArr.length; i++)
	{
		DateList.push(DateRowArr[i]);
		StartTimeArr.push(SH+":"+SM+":"+"00");
		EndTimeArr.push(EH+":"+EM+":"+"00");
	}
	
	$("div#TimeCheckList").html(ajaxLoadingImg);
	$.post(
		"ajax_task.php",
		{
			task:"CheckTimeCrash",
			FacilityType:$("#FacilityType").val(),
			FacilityID:$("#FacilityID").val(),
			DateList: DateList.join(","),
			StartTimeStr: StartTimeArr.join(","),
			EndTimeStr: EndTimeArr.join(","),
			AllowReserve: AllowReserve,
			SelectedDateList:SelectedDateList
		},
		function (data)
		{
			$("div#TimeCheckList").html(data);
			UnBlock_Element("TimeCheckList");
		}
	
	);		

}

			
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>