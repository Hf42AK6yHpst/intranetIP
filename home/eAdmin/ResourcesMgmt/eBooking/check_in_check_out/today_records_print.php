<?php 


//using : Isaac

############################ Change Log ###########################
## Date		:	2018-01-15 (Isaac)
##              seperated Booked By	into Booked By and Booking Date columns 
##	Date    :	2017-11-17 (Isaac)
##              Post original values of the Checkin-checkout filter to prevent consistent use of the new value before user click the apply button in Today_records.php
##  Date    :	2017-11-17 (Isaac)
##              POST data from the search field to trigger print and export consistance with the search filter 
##	Date	:	2017-09-19	Simon #Y124731
##				Add current StartDate and EndDate to get data
##
###################################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$li 			= new libdbtable2007($field, $order, $PageNumber);

$SearchKeyword = standardizeFormPostValue($_POST['savedkeyword']);
$savedCheckInOutStatus = $_POST['savedCheckInOutStatus'];


//bulid header
$header = "<div class='table_board'>";
$header .="<table class='common_table_list_v30 view_table_list_v30'>";
$header .= "<tr>";
$header .= "<th>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem']."</th>";
$header .= "<th>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)']."</th>";
$header .= "<th>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time']."</th>";
$header .= "<th>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson']."</th>";
$header .= "<th>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy']."</th>";
$header .= "<th>".$Lang['eBooking']['Mail']['FieldTitle']['BookingDate']."</th>";
$header .= "<th>".$Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutReturnTime']."</th>";
$header .= "<th>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status']."</th>";

$header .= "<th>".$i_BookingRemark."</th>";
$header .= "</tr>";

// Add current StartDate and EndDate to get data
$curr_date = date("Y-m-d");
$curr_year = substr($curr_date,0,4);
$curr_month = substr($curr_date,5,2);
$curr_day = substr($curr_date,8,2);
$StartDate = date("Y-m-d",mktime(0, 0, 0, $curr_month, $curr_day, $curr_year));
$EndDate = date("Y-m-d",mktime(23, 59, 59, $curr_month, $curr_day, $curr_year));


	
$arrTempResult = $lebooking->Get_All_Facility_Booking_Record('',LIBEBOOKING_BOOKING_STATUS_APPROVED, $StartDate, $EndDate, '', $SearchKeyword, $savedCheckInOutStatus);
// debug_pr($i_BookingRemark);
if(sizeof($arrTempResult)>0)
{
	for($i=0; $i<sizeof($arrTempResult); $i++)
	{
		list($booking_id,$period_id,$booking_remark,$booking_date_string,$booking_start_time,$booking_end_time,
		$requested_by,$request_date,$responsible_ppl,
		$room_id,$room_name,$room_PIC,$room_booking_process_date,$room_booking_status,
		$item_id,$item_name,$item_PIC,$item_booking_process_date,$item_booking_status,
		$is_reserve, $room_check_in_out_remarks, $item_check_in_out_remarks, $room_PICID, $item_PICID,
		$room_reject_reason, $item_reject_reason, $Attachment, $room_current_status, $item_current_status,
		$room_check_out_time, $item_check_out_time) = $arrTempResult[$i];

		$arrBookingID[] = $booking_id;
		$arrBookingDetails[$booking_id]['Date'] = $booking_date_string;
		$arrBookingDetails[$booking_id]['RelatedPeriod'] = $period_id;
		$arrBookingDetails[$booking_id]['Remark'] = nl2br(htmlspecialchars($booking_remark));
		$arrBookingDetails[$booking_id]['StartTime'] = $booking_start_time;
		$arrBookingDetails[$booking_id]['EndTime'] = $booking_end_time;
		$arrBookingDetails[$booking_id]['RequestedBy'] = $requested_by;
		$arrBookingDetails[$booking_id]['RequestedDate'] = $request_date;
		$arrBookingDetails[$booking_id]['IsReserve'] = $is_reserve;
	
		$curr_date = date("Y-m-d");

		$booking_day_before = $request_date;

		$arrBookingDetails[$booking_id]['RequestedDayBefore'] = $booking_day_before;
		
		$arrBookingDetails[$booking_id]['ResponsiblePerson'] = $responsible_ppl;
		if($room_id != ""){
			$arrBookingDetails[$booking_id]['RoomBooking'] = 1;
			$arrBookingDetails[$booking_id]['RelatedRoom'][] = $room_id;
			$arrBookingDetails[$booking_id][$room_id]['RoomName'] = $room_name;
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC'] = $room_PIC;
			$arrBookingDetails[$booking_id][$room_id]['RoomProcessDate'] = $room_booking_process_date;
			$arrBookingDetails[$booking_id][$room_id]['CheckOutRemark'] = nl2br(htmlspecialchars($room_check_in_out_remarks,ENT_QUOTES));
			$arrBookingDetails[$booking_id][$room_id]['CheckOutTime'] = $room_check_out_time;
			$arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus'] = $room_current_status; // add room_CurrentStatus
			switch($arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus']){
				case "0":
					$arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus'] = $Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['Waiting'];
					break;
				case "1":
					$arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus'] = $Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['CheckedInOrBorrowed'];
					break;
				case "2":
					$arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus'] = $Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['CheckedOutOrReturned'];
					break;
			}
					
			$curr_date = date("Y-m-d");
				$room_booking_process_day_before = $room_booking_process_date;
			$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore'] = $room_booking_process_day_before;
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
		}else{
			$arrBookingDetails[$booking_id]['RoomBooking'] = 0;
			$arrBookingDetails[$booking_id]['RelatedRoom'][] = array();
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
			$arrBookingDetails[$booking_id][$room_id]['CheckOutRemark'] = "";
			$arrBookingDetails[$booking_id][$room_id]['CheckOutTime'] = "";
			$arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus'] = "";
		}
		
		if($item_id != ""){
			$arrBookingDetails[$booking_id]['ItemBooking'] = 1;
			$arrBookingDetails[$booking_id]['RelatedItem'][] = $item_id;
			$arrBookingDetails[$booking_id][$item_id]['ItemName'] = $item_name;
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC'] = $item_PIC;
			$arrBookingDetails[$booking_id][$item_id]['ItemProcessDate'] = $item_booking_process_date;
			$arrBookingDetails[$booking_id][$item_id]['CheckOutRemark'] = nl2br(htmlspecialchars($item_check_in_out_remarks,ENT_QUOTES));
			$arrBookingDetails[$booking_id][$item_id]['CheckOutTime'] = $item_check_out_time;
			$arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] = $item_current_status;
			switch($arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus']){
				case "0":
					$arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] = $Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['Waiting'];
					break;
				case "1":
					$arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] = $Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['CheckedInOrBorrowed'];
					break;
				case "2":
					$arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] = $Lang['eBooking']['CheckInCheckOut']['TodayBookingExport']['Status']['CheckedOutOrReturned'];
					break;
			}
	
			$curr_date = date("Y-m-d");
				$item_booking_process_day_before = $item_booking_process_date;
			$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore'] = $item_booking_process_day_before;
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
		}else{
			$arrBookingDetails[$booking_id]['ItemBooking'] = 0;
			$arrBookingDetails[$booking_id]['RelatedItem'][] = array();
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
			$arrBookingDetails[$booking_id][$item_id]['CheckOutRemark'] = "";
			$arrBookingDetails[$booking_id][$item_id]['CheckOutTime'] = "";
			$arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] = "";
		}
	}
	
	$arrBookingID = array_unique($arrBookingID);
	if(!empty($arrBookingID)){
		ksort($arrBookingID);
	}

// 	# Default Table Settings
// 	$pageNo = ($pageNo == '')? $li->pageNo=1 : $li->pageNo=$pageNo;
// 	$numPerPage = ($numPerPage == '')? $li->page_size=20 : $li->page_size=$numPerPage;
// 	$Order = ($Order == '')? 1 : $Order;
// 	$SortField = ($SortField == '')? 0 : $SortField;
	
// 	if($pageNo == 1)
// 	{
// 		$start = $pageNo;
// 		$end = $numPerPage;
// 		$li->n_start = $start-1;
// 		$li->n_end = min(sizeof($arrBookingID),($li->pageNo*$li->page_size));
// 	}
// 	else 
// 	{
// 		$start = ($pageNo*$numPerPage)-$numPerPage+1;
		
// 		if($start>sizeof($arrBookingID)){
// 			$start = 1;
// 		}
		
// 		$end = ($pageNo*$numPerPage);
// 		$li->n_start = $start-1;
// 		$li->n_end = min(sizeof($arrBookingID),($li->pageNo*$li->page_size));
// 	}
	
// 	foreach($arrBookingID as $key=>$booking_id)
// 	{
// 		if($booking_id != "")
// 			$arrSortedBookingID[] = $booking_id;
// 	}

// 	for($i = $start-1; $i < $end; $i++)
// 	{
// 		$arrDisplayBookingID[] = $arrSortedBookingID[$i];
// 	}
	
	foreach($arrBookingID as $key => $booking_id)
	{
		$arrRoomBookingDetails = array();
		$arrItemBookingDetails = array();
		
		if(is_array($arrBookingDetails[$booking_id]['RelatedRoom']))
			$arrTempRoomBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedRoom']);
		if(is_array($arrBookingDetails[$booking_id]['RelatedItem']))
			$arrTempItemBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedItem']);
		
		if(sizeof($arrTempRoomBookingDetails) > 0)
		{
			foreach($arrTempRoomBookingDetails as $key=>$val)
			{
				if(($val != "") && ($val != "0"))
				{
					$arrRoomBookingDetails[] = $val;
				}
			}
		}
		
		if(sizeof($arrTempItemBookingDetails) > 0)
		{
			foreach($arrTempItemBookingDetails as $key=>$val)
			{
				if(($val != "") && ($val != "0"))
				{
					$arrItemBookingDetails[] = $val;
				}
			}
		}
		

		## Book for room & item at the same time
		if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1) )
		{
			$room_id = $arrRoomBookingDetails[0];
			$Time = date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']));
		
			$table_content .= '<tr>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id][$room_id]['RoomName'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Date'].'</td>';
			$table_content .= '<td>'.$Time.'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['ResponsiblePerson'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['RequestedBy'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['RequestedDayBefore'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id][$room_id]['CheckOutTime'].'</td>';
			$table_content .= '<td>' . $arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus']. '</td>'; //add room_CurrentStatus
// 			$table_content .= '<td>'.$arrBookingDetails[$booking_id][$room_id]['CheckOutRemark'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Remark'].'</td>';
			$table_content .= '</tr>';
				
			for($i=0; $i<sizeof($arrItemBookingDetails); $i++)
			{
				$item_id = $arrItemBookingDetails[$i];
		
		
				$table_content .= '<tr>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id][$item_id]['ItemName'].'</td>';
				$table_content .= '<td></td>';
				$table_content .= '<td></td>';
				$table_content .= '<td></td>';
				$table_content .= '<td></td>';
				$table_content .= '<td></td>';
				if ($arrBookingDetails[$booking_id][$item_id]['CheckOutTime'] == $arrBookingDetails[$booking_id][$room_id]['CheckOutTime'] ){
					$table_content .= '<td></td>';
				}else{
					$table_content .= '<td>'.$arrBookingDetails[$booking_id][$item_id]['CheckOutTime'].'</td>';
				}
				$table_content .= '<td>' . $arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] . '</td>'; //add item_CurrentStatus
// 				$table_content .= '<td>'.$arrBookingDetails[$booking_id][$item_id]['CheckOutRemark'].'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Remark'].'</td>';
				$table_content .= '</tr>';
			}
		}
		## Book for Room Only
		else if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 0) )
		{
		
			$room_id = $arrRoomBookingDetails[0];

			$Time = date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']));
		
			$table_content .= '<tr>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id][$room_id]['RoomName'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Date'].'</td>';
			$table_content .= '<td>'.$Time.'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['ResponsiblePerson'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['RequestedBy'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['RequestedDayBefore'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id][$room_id]['CheckOutTime'].'</td>';
			$table_content .= '<td>' . $arrBookingDetails[$booking_id][$room_id]['room_CurrentStatus'] . '</td>'; // add room_CurrentStatus
// 			$table_content .= '<td>'.$arrBookingDetails[$booking_id][$room_id]['CheckOutRemark'].'</td>';
			$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Remark'].'</td>';
			$table_content .= '</tr>';
				
		}
		## Book for Item Only
		else if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 0) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1) )
		{
		
			for($i=0; $i<sizeof($arrItemBookingDetails); $i++)
			{
				$item_id = $arrItemBookingDetails[$i];
				$Time = date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']));
		
				$table_content .= '<tr>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id][$item_id]['ItemName'].'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Date'].'</td>';
				$table_content .= '<td>'.$Time.'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id]['ResponsiblePerson'].'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id]['RequestedBy'].'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id]['RequestedDayBefore'].'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id][$item_id]['CheckOutTime'].'</td>';
				$table_content .= '<td>' . $arrBookingDetails[$booking_id][$item_id]['item_CurrentStatus'] . '</td>'; // add item_CurrentStatus
// 				$table_content .= '<td>'.$arrBookingDetails[$booking_id][$item_id]['CheckOutRemark'].'</td>';
				$table_content .= '<td>'.$arrBookingDetails[$booking_id]['Remark'].'</td>';
				$table_content .= '</tr>';
					
			}
		}
	}
}
		$display = $header.$table_content;

?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

    
<?=$display?>

<?
intranet_closedb();
?>
