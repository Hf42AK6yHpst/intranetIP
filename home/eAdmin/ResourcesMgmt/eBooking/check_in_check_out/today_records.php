<?php
# using: Isaac

################################################## Change Log ############################################################
## Date		:	2017-11-24 (Isaac)
##              modified value for hidden input 'savekeyword'to intranet_htmlspecialchars($keyword)
##
## Date		:	2017-11-24 (Isaac)
##              Modified # check cust remark and js_Show_Detail_Layer() for displaying check out remark as well when the booking status is check out
##
## Date		:	2017-11-20 (Isaac)
##              add hidden field to store the original selection of the Checkin-checkout filter
##              add generate report function to combine duplicates in print_report() and export_report() function
##
## Date		:	2017-11-17 (Isaac)
##              Add hidden field to store search field's value ($keyword)
##
## Date		:	2017-09-19 (Simon)
##				Add Print and Export button ui and functions
##	
## Date		:	2011-11-24 (YatWoon)
##				if remark with attachment, then display another icon gif
##
##########################################################################################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_page_size", "numPerPage");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_page_no", "pageNo");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_page_order", "order");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_page_field", "field");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_BookingStatus", "BookingStatus");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_BookingDate", "BookingDate");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_ManagementGroup", "ManagementGroup");
$arrCookies[] = array("ck_eBooking_CheckInCheckOut_My_Booking_Record_Keyword", "keyword");	

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageCheckInCheckOut_TodayBooking";
$linterface 	= new interface_html();
$linventory 	= new libinventory();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$li 			= new libdbtable2007($SortField, $Order, $PageNumber);

$keyword		= (isset($keyword) && $keyword != "") ? $keyword : "";

$TAGS_OBJ[] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['TodayRecords'],$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/today_records.php?clearCoo=1",1);
$TAGS_OBJ[] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutBorrow'],$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/check_in_records.php?clearCoo=1");
$TAGS_OBJ[] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['History'],$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/check_out_records.php?clearCoo=1");
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

//## Set Current Date
$current_date = date("Y-m-d");
//### Keyowrd Search Field
if($keyword != "") {
	$keyword = stripslashes(trim($keyword));
}

if($CheckInOutStatus=='')
	$CheckInOutStatus = array(0);
$arrTempResult = $lebooking->Get_All_Facility_Booking_Record('',LIBEBOOKING_BOOKING_STATUS_APPROVED, $current_date, $current_date, $ManagementGroup, $keyword, $CheckInOutStatus);
//$li->sql = $sql;

if(sizeof($arrTempResult)>0)
{
	for($i=0; $i<sizeof($arrTempResult); $i++)
	{
		list($booking_id,$period_id,$booking_remark,$booking_date_string,$booking_start_time,$booking_end_time,
		$requested_by,$request_date,$responsible_ppl,
		$room_id,$room_name,$room_PIC,$room_booking_process_date,$room_booking_status,
		$item_id,$item_name,$item_PIC,$item_booking_process_date,$item_booking_status,
		$is_reserve, $room_check_in_out_remarks, $item_check_in_out_remarks, $room_PICID, $item_PICID,
		$room_reject_reason, $item_reject_reason, $Attachment, $room_current_status, $item_current_status,
		$room_check_out_time, $item_check_out_time, $relatedtoid) = $arrTempResult[$i];
		
		$arrBookingID[] = $booking_id;
		$arrBookingDetails[$booking_id]['Date'] = $booking_date_string;
		$arrBookingDetails[$booking_id]['RelatedPeriod'] = $period_id;
		$arrBookingDetails[$booking_id]['Remark'] = nl2br(htmlspecialchars($booking_remark));
		$arrBookingDetails[$booking_id]['StartTime'] = $booking_start_time;
		$arrBookingDetails[$booking_id]['EndTime'] = $booking_end_time;
		$arrBookingDetails[$booking_id]['RequestedBy'] = $requested_by;
		$arrBookingDetails[$booking_id]['RequestedDate'] = $request_date;
		$arrBookingDetails[$booking_id]['IsReserve'] = $is_reserve;
		$arrBookingDetails[$booking_id]['RelatedTo'] = $relatedtoid;
		$arrBookingDetails[$booking_id]['Attachment'] = $Attachment;
	
		$curr_date = date("Y-m-d");
		$booking_day_before = floor((strtotime($curr_date)-strtotime($request_date))/86400);
		
//		if($booking_day_before > 7) {
			$booking_day_before = $request_date;
//		} else {
//			$booking_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
//		}
		$arrBookingDetails[$booking_id]['RequestedDayBefore'] = $booking_day_before;
		
		$arrBookingDetails[$booking_id]['ResponsiblePerson'] = $responsible_ppl;
		if($room_id != ""){
			$arrBookingDetails[$booking_id]['RoomBooking'] = 1;
			$arrBookingDetails[$booking_id]['RelatedRoom'][] = $room_id;
			$arrBookingDetails[$booking_id][$room_id]['RoomName'] = $room_name;
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC'] = $room_PIC;
			$arrBookingDetails[$booking_id][$room_id]['RoomProcessDate'] = $room_booking_process_date;
			
			$curr_date = date("Y-m-d");
			$room_booking_process_day_before = floor((strtotime($curr_date)-strtotime($room_booking_process_date))/86400);
//			if($room_booking_process_day_before > 7) {
				$room_booking_process_day_before = $room_booking_process_date;
//			} else {
//				$room_booking_process_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
//			}
			
			$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore'] = $room_booking_process_day_before;
			
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
			$arrBookingDetails[$booking_id][$room_id]['CurrentStatus'] = $room_current_status;
			
		}else{
			$arrBookingDetails[$booking_id]['RoomBooking'] = 0;
			$arrBookingDetails[$booking_id]['RelatedRoom'][] = array();
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
		}
		
		if($item_id != ""){
			$arrBookingDetails[$booking_id]['ItemBooking'] = 1;
			$arrBookingDetails[$booking_id]['RelatedItem'][] = $item_id;
			$arrBookingDetails[$booking_id][$item_id]['ItemName'] = $item_name;
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC'] = $item_PIC;
			$arrBookingDetails[$booking_id][$item_id]['ItemProcessDate'] = $item_booking_process_date;
	
			$curr_date = date("Y-m-d");
			$item_booking_process_ = floor((strtotime($curr_date)-strtotime($item_booking_process_date))/86400);
//			if($room_booking_process_day_before > 7) {
				$item_booking_process_day_before = $item_booking_process_date;
//			} else {
//				$item_booking_process_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
//			}
			$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore'] = $item_booking_process_day_before;
			
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
			$arrBookingDetails[$booking_id][$item_id]['CurrentStatus'] = $item_current_status;
		}else{
			$arrBookingDetails[$booking_id]['ItemBooking'] = 0;
			$arrBookingDetails[$booking_id]['RelatedItem'][] = array();
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
		}
	}
	$arrBookingID = array_unique($arrBookingID);
	ksort($arrBookingID);

	# Default Table Settings
	$pageNo = ($pageNo == '')? $li->pageNo=1 : $li->pageNo=$pageNo;
	$numPerPage = ($numPerPage == '')? $li->page_size=20 : $li->page_size=$numPerPage;
	$Order = ($Order == '')? 1 : $Order;
	$SortField = ($SortField == '')? 0 : $SortField;
	
	if($pageNo == 1)
	{
		$start = $pageNo;
		$end = $numPerPage;
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($arrBookingID),($li->pageNo*$li->page_size));
	}
	else 
	{
		$start = ($pageNo*$numPerPage)-$numPerPage+1;
		
		if($start>sizeof($arrBookingID)){
			$start = 1;
		}
		
		$end = ($pageNo*$numPerPage);
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($arrBookingID),($li->pageNo*$li->page_size));
	}
		
	foreach($arrBookingID as $key=>$booking_id)
	{
		if($booking_id != "")
			$arrSortedBookingID[] = $booking_id;
	}

	for($i = $start-1; $i < $end; $i++)
	{
		$arrDisplayBookingID[] = $arrSortedBookingID[$i];
	}
	
	foreach($arrDisplayBookingID as $key=>$booking_id)
	{
		$arrRoomBookingDetails = array();
		$arrItemBookingDetails = array();
		
		if(is_array($arrBookingDetails[$booking_id]['RelatedRoom']))
			$arrTempRoomBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedRoom']);
		if(is_array($arrBookingDetails[$booking_id]['RelatedItem']))
			$arrTempItemBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedItem']);
		
		if(sizeof($arrTempRoomBookingDetails) > 0)
		{
			foreach($arrTempRoomBookingDetails as $key=>$val)
			{
				if(($val != "") && ($val != "0"))
				{
					$arrRoomBookingDetails[] = $val;
				}
			}
		}
		
		if(sizeof($arrTempItemBookingDetails) > 0)
		{
			foreach($arrTempItemBookingDetails as $key=>$val)
			{
				if(($val != "") && ($val != "0"))
				{
					$arrItemBookingDetails[] = $val;
				}
			}
		}
		
		$row_num = $start++;
		
		if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1) )
		{
			## Book for room & item at the same time
			$room_id = $arrRoomBookingDetails[0];
			
			## get status image
			switch($arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'])
			{
				case 0:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
					$td_num_css = "class='booking_dot_line '";
					$td_css = "class='booking_pending booking_dot_line '";
					break;
				case 1:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
					$td_num_css = "class='booking_dot_line'";
					$td_css = "class='booking_approved booking_dot_line'";
					break;
				case -1:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
					$td_num_css = "class='booking_dot_line'";
					$td_css = "class='booking_rejected booking_dot_line'";
					break;
				case 999:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
					$td_num_css = "";
					$td_css = "class='booking_tempory '";
					break;
			}
//			$thisClass = $lebooking->Get_Current_Status_Row_Class($arrBookingDetails[$booking_id][$room_id]['CurrentStatus']);
			$thisCurrentStatus = $Lang['eBooking']['CheckInOutStatusArr'][$arrBookingDetails[$booking_id][$room_id]['CurrentStatus']];
// 			debug_pr($arrBookingDetails[$booking_id][$room_id]['CurrentStatus']);
			$Disabled = $arrBookingDetails[$booking_id][$room_id]['CurrentStatus']!=LIBEBOOKING_BOOKING_Waiting?"disabled":"";
			
			$table_content .= "<tr class='$thisClass'>";
			$table_content .= "<td width='1%'>".$row_num."</td>";
			if($arrBookingDetails[$booking_id]['IsReserve']) {
				$table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
			} else {
				$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
			}
			$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Date']."</td>";
			$table_content .= "<td $td_css>".date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']))."</td>";
			$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['ResponsiblePerson']."</td>";
			$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['RequestedBy']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id]['RequestedDayBefore']."</span></td>";
			$table_content .= "<td $td_css>".$thisCurrentStatus."</td>";
			
			# check cust remark
			if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
			{
				$RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
				$cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
			}
				
			if($cust_remark || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] != "")
			{
                 $thisLayerID = 'RemarkLinkDiv_'.$booking_id;
                 $icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";                       
                 $thisCurrentStatusID = $arrBookingDetails[$booking_id][$room_id]['CurrentStatus'];
                 if ($thisCurrentStatusID == 2){
                 	$jsDetailType = "CheckOutRemark";
                 } else {
                    $jsDetailType = "Remark";
                 }
//                  debug_pr($jsDetailType);
                 $table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"$jsDetailType\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink' alt='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']."'></a></div></td>";
                                    
             }
             else
             {
             	$table_content .= "<td width='1%' $td_css>&nbsp;</td>";
             }                               	
			
			$ApproveRight = $lebooking->Check_Approve_Room_Booking_Request_Right($room_id);
			if($ApproveRight > 0)
			{
				$table_content .= "<td valign='top' nowrap $td_css><input $Disabled type='checkbox' name='".$booking_id."BookingRecord[]' value='ROOM_".$room_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",0,$booking_id);'></td>";
				$table_content .= "<td valign='top' nowrap $td_css><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ROOM\"):setTargetBookingIDChecked(0,this.value,\"ROOM\");'></td>";
			}
			else
			{
				$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
				$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
			}
			$table_content .= "</tr>";
			
			for($i=0; $i<sizeof($arrItemBookingDetails); $i++)
			{
				$item_id = $arrItemBookingDetails[$i];
				
				switch($arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'])
				{
					case 0:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
						if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
							$td_num_css = "class='booking_solid_line'";
							$td_css = "class='booking_pending'";
						} else {
							$td_num_css = "class='booking_dot_line '";
							$td_css = "class='booking_pending'";
						}
						break;
					case 1:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
						if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
							$td_num_css = "class='booking_solid_line '";
							$td_css = "class='booking_approved'";
						} else {
							$td_num_css = "class='booking_dot_line '";
							$td_css = "class='booking_approved'";
						}
						break;
					case -1:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
						if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
							$td_num_css = "class='booking_solid_line'";
							$td_css = "class='booking_rejected'";
						} else {
							$td_num_css = "class='booking_dot_line '";
							$td_css = "class='booking_rejected'";
						}
						break;
					case 999:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
						if( (sizeof($arrItemBookingDetails)>0) && ($i==(sizeof($arrItemBookingDetails)-1)) ) {
							$td_num_css = "class='booking_solid_line'";
							$td_css = "class='booking_tempory'";
						} else {
							$td_num_css = "class='booking_dot_line '";
							$td_css = "class='booking_tempory'";
						}
						break;
				}
//				$thisClass = $lebooking_ui->Get_Current_Status_Row_Class($arrBookingDetails[$booking_id][$item_id]['CurrentStatus']);
				$thisCurrentStatus = $Lang['eBooking']['CheckInOutStatusArr'][$arrBookingDetails[$booking_id][$item_id]['CurrentStatus']];
				$Disabled = $arrBookingDetails[$booking_id][$item_id]['CurrentStatus']!=LIBEBOOKING_BOOKING_Waiting?"disabled":"";
				
				$table_content .= "<tr class='$thisClass'>";
				if($i != (sizeof($arrItemBookingDetails)-1)	){
					$table_content .= "<td width='1%' >&nbsp;</td>";
				} else {
					$table_content .= "<td width='1%' >&nbsp;</td>";
				}
				$table_content .= "<td $td_css ><img align='absmiddle' src='{$image_path}/{$LAYOUT_SKIN}/icon_and.gif'>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
				$table_content .= "<td $td_css>&nbsp;</td>";
				$table_content .= "<td $td_css>&nbsp;</td>";
				$table_content .= "<td $td_css>&nbsp;</td>";
				$table_content .= "<td $td_css>&nbsp;</td>";
				$table_content .= "<td $td_css>".$thisCurrentStatus."</td>";
				# check cust remark
				if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
				{
				    $RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
				    $cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
				}
				if($cust_remark || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] !=""){
    				$thisLayerID = 'RemarkLinkDiv_'.$booking_id;
    				$icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
    				$thisCurrentStatusID = $arrBookingDetails[$booking_id][$item_id]['CurrentStatus'];
    				if ($thisCurrentStatusID == 2){
    				    $jsDetailType = "CheckOutRemark";
    				} else {
    				    $jsDetailType = "Remark";
    				}
				
				$table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"$jsDetailType\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink' alt='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']."'></a></div></td>";
				} else {
				    $table_content .= "<td width='1%' $td_css>&nbsp;</td>";
				}
				
				$ApproveRight = $lebooking->Check_Approve_Item_Booking_Request_Right($item_id);
				if($ApproveRight > 0)
				{
					$table_content .= "<td valign='top' nowrap $td_css><input $Disabled type='checkbox' name='".$booking_id."BookingRecord[]' value='ITEM_".$item_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",0,$booking_id);'></td>";
					$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
				}
				else
				{
					$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
					$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
				}
				$table_content .= "</tr>";
			}
			$table_content .= "<tr><td colspan='10' class='booking_approved booking_solid_line'><img height='5px' src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'></td></tr>";
		}
		else if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 0) )
		{
			## Book for Room Only
			$room_id = $arrRoomBookingDetails[0];
			
			## get status image
			switch($arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'])
			{
				case 0:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
					$td_num_css = "";
					$td_css = "class='booking_pending '";
					break;
				case 1:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
					$td_num_css = "";
					$td_css = "class='booking_approved '";
					break;
				case -1:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore']."</span>";
					$td_num_css = "";
					$td_css = "class='booking_rejected '";
					break;
				case 999:
					$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
					$td_num_css = "";
					$td_css = "class='booking_tempory '";
					break;
			}
//			$thisClass = $lebooking->Get_Current_Status_Row_Class($arrBookingDetails[$booking_id][$room_id]['CurrentStatus']);
			$thisCurrentStatus = $Lang['eBooking']['CheckInOutStatusArr'][$arrBookingDetails[$booking_id][$room_id]['CurrentStatus']];
			
			$Disabled = $arrBookingDetails[$booking_id][$room_id]['CurrentStatus']!=LIBEBOOKING_BOOKING_Waiting?"disabled":"";
			
			$table_content .= "<tr class='$thisClass'>";
			$table_content .= "<td width='1%' $td_num_css>".$row_num."</td>";
			if($arrBookingDetails[$booking_id]['IsReserve']) {
				$table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
			} else {
				$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$room_id]['RoomName']."</td>";
			}
			$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Date']."</td>";
			$table_content .= "<td $td_css>".date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']))."</td>";
			$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['ResponsiblePerson']."</td>";
			$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['RequestedBy']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id]['RequestedDayBefore']."</span></td>";
			$table_content .= "<td $td_css>".$thisCurrentStatus."</td>";
			# check cust remark
			if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
			{
				$RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
				$cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
			}
			if($cust_remark || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] != "")
			{
				$thisLayerID = 'RemarkLinkDiv_'.$booking_id;
				$icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
				$thisCurrentStatusID = $arrBookingDetails[$booking_id][$item_id]['CurrentStatus'];
				if ($thisCurrentStatusID == 2){
				    $jsDetailType = "CheckOutRemark";
				} else {
				    $jsDetailType = "Remark";
				}
				$table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"$jsDetailType\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink' alt='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']."'></a></div></td>";
			}
			else
			{
				$table_content .= "<td width='1%' $td_css>&nbsp;</td>";
			}
			
			$ApproveRight = $lebooking->Check_Approve_Room_Booking_Request_Right($room_id);
			if($ApproveRight>0)
			{
                $table_content .= "<td valign='top' nowrap $td_css>&nbsp;<div id='hideCheckBox' style='display:none'><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ROOM\"):setTargetBookingIDChecked(0,this.value,\"ROOM\");'></div></td>";
                $table_content .= "<td width='1%' valign='top' nowrap $td_css><input $Disabled type='checkbox' name='".$booking_id."BookingRecord[]' value='ROOM_".$room_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$room_id."\",\"ROOM\",0,$booking_id);'></td>";
			}
			else
			{
				$table_content .= "<td width='1%' valign='top' nowrap $td_css>&nbsp;</td>";
				$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
			}
			$table_content .= "</tr>";
			$table_content .= "<tr><td colspan='10' class='booking_approved booking_solid_line'><img height='5px' src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'></td></tr>";
		}
		else if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 0) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1) )
		{
			## Book for Item Only
			for($i=0; $i<sizeof($arrItemBookingDetails); $i++)
			{
				$item_id = $arrItemBookingDetails[$i];
				
				switch($arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'])
				{
					case 0:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
						$td_num_css = "";
						$td_css = "class='booking_pending '";
						break;
					case 1:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
						$td_num_css = "";
						$td_css = "class='booking_approved '";
						break;
					case -1:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
						$td_num_css = "";
						$td_css = "class='booking_rejected '";
						break;
					case 999:
						$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>".$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore']."</span>";
						$td_num_css = "";
						$td_css = "class='booking_tempory '";
						break;
				}
//				$thisClass = $lebooking_ui->Get_Current_Status_Row_Class($arrBookingDetails[$booking_id][$item_id]['CurrentStatus']);
				$thisCurrentStatus = $Lang['eBooking']['CheckInOutStatusArr'][$arrBookingDetails[$booking_id][$item_id]['CurrentStatus']];
				$Disabled = $arrBookingDetails[$booking_id][$item_id]['CurrentStatus']!=LIBEBOOKING_BOOKING_Waiting?"disabled":"";
				
				$table_content .= "<tr class='$thisClass'>";
				$table_content .= "<td width='1%' $td_num_css>".$row_num."</td>";
				if($arrBookingDetails[$booking_id]['IsReserve']) {
					$table_content .= "<td $td_css><span class='tabletextrequire'>*</span>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
				} else {
					$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id][$item_id]['ItemName']."</td>";
				}
				$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['Date']."</td>";
				$table_content .= "<td $td_css>".date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']))."</td>";
				$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['ResponsiblePerson']."</td>";
				$table_content .= "<td $td_css>".$arrBookingDetails[$booking_id]['RequestedBy']."<br><span class='tabletextremark'>".$arrBookingDetails[$booking_id]['RequestedDayBefore']."</span></td>";
				$table_content .= "<td $td_css>".$thisCurrentStatus."</td>";
				
				# check cust remark
				if($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
				{
					$RelatedToID = $arrBookingDetails[$booking_id]['RelatedTo'];
					$cust_remark = $lebooking->DisplayCustRemark($RelatedToID);
				}
				if($cust_remark || $arrBookingDetails[$booking_id]['Remark'] != "" || $arrBookingDetails[$booking_id]['Attachment'] !="")
				{
					$thisLayerID = 'RemarkLinkDiv_'.$booking_id;
					$icon_display = $arrBookingDetails[$booking_id]['Attachment'] ? "icon_remark_attachment.gif" : "icon_remark.gif";
					$thisCurrentStatusID = $arrBookingDetails[$booking_id][$item_id]['CurrentStatus'];
					if ($thisCurrentStatusID == 2){
					    $jsDetailType = "CheckOutRemark";
					} else {
					    $jsDetailType = "Remark";
					}
// 					debug_pr("$jsDetailType");
					$table_content .= "<td width='1%' $td_css><div id='".$thisLayerID."'><a href='javascript: js_Show_Detail_Layer(\"$jsDetailType\",".$booking_id.",\"$thisLayerID\");'><img src='$image_path/$LAYOUT_SKIN/". $icon_display ."' border='0' class='tablelink' alt='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']."'></a></div></td>";
				}
				else
				{
					$table_content .= "<td width='1%' $td_css>&nbsp;</td>";
				}
				$ApproveRight = $lebooking->Check_Approve_Item_Booking_Request_Right($item_id);
				if($ApproveRight > 0)
				{
                    $table_content .= "<td valign='top' nowrap $td_css>&nbsp;<div id='hideCheckBox' style='display:none'><input type='checkbox' name='CheckAllBooking[]' value='$booking_id' onClick='(this.checked)?setTargetBookingIDChecked(1,this.value,\"ITEM\"):setTargetBookingIDChecked(0,this.value,\"ITEM\");'></div></td>";
                    $table_content .= "<td valign='top' nowrap $td_css><input $Disabled type='checkbox' name='".$booking_id."BookingRecord[]' value='ITEM_".$item_id."' onClick='(this.checked)?setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",1,$booking_id):setSingleBookingIDChecked(\"".$booking_id."_".$item_id."\",\"ITEM\",0,$booking_id);'></td>";
				}
				else
				{
					$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
					$table_content .= "<td valign='top' nowrap $td_css>&nbsp;</td>";
				}
				$table_content .= "</tr>";
				$table_content .= "<tr><td colspan='10' class='booking_approved booking_solid_line'><img height='5px' src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'></td></tr>";
			}
		}
	}
}
else
{
	$table_content .= "<tr>";
	$table_content .= "<td valign='top' colspan='10' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td>";
	$table_content .= "</tr>";
}


### Table Navigation Bar ###
$li->page_size = $numPerPage;
$li->total_row = sizeof($arrBookingID);
$li->form_name = "form1";
//$li->pageNo_name = "booking_mgmt_pageNo";
//$li->numPerPage_name = "booking_mgmt_numPerPage";

$table_content .= "<tr class='tablebottom' height='20px'>";
$table_content .= "<td colspan='10'>";
if(sizeof($arrBookingID)>0)
	$table_content .= $li->navigation();
$table_content .= "</td>";
$table_content .= "</tr>";

$table_content .= "<tr>";
$table_content .= "<td valign='top' colspan='10' align='left'><span class='tabletextremark'>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ReservedByAdmin']."</span></td>";
$table_content .= "</tr>";

$table_content .= "<tr><td>";
$table_content .= "<input type='hidden' name='CheckboxChecked' id='CheckboxChecked' value=0>";
$table_content .= "<input type='hidden' name='str_BookingID' id='str_BookingID' value=''>";
//$table_content .= "<input type='hidden' id='booking_mgmt_pageNo' name='booking_mgmt_pageNo' value='".$li->pageNo."'>";
$table_content .= "<input type='hidden' id='page_action' name='page_action' value=''>";
$table_content .= "<input type='hidden' id='pageNo' name='pageNo' value='".$li->pageNo."'>";
$table_content .= "<input type='hidden' id='order' name='order' value='".$li->order."'>";
$table_content .= "<input type='hidden' id='field' name='field' value='".$li->field."'>";
$table_content .= "<input type='hidden' id='page_size_change' name='page_size_change' value=''>";
$table_content .= "<input type='hidden' id='numPerPage' name='numPerPage' value='".$li->page_size."'>";
# start add room and item hidden field pass to status_update.php page
$table_content .= "<input type='hidden' id='roomIdArr' name='roomIdArr' value=''>";
$table_content .= "<input type='hidden' id='itemIdArr' name='itemIdArr[]' value=''>";
# end
$table_content .= "</td></tr>";


### Hide Layer Button
$remark_layer_content .= '<div id="RemarkLayer" class="selectbox_layer" style="visibility:hidden; width:500px;">'."\n";
$remark_layer_content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
$remark_layer_content .= '<tbody>'."\n";
$remark_layer_content .= '<tr>'."\n";
$remark_layer_content .= '<td align="right" style="border-bottom: medium none;">'."\n";
$remark_layer_content .= '<a href="javascript:js_Hide_Detail_Layer()"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
$remark_layer_content .= '</td>'."\n";
$remark_layer_content .= '</tr>'."\n";
$remark_layer_content .= '<tr>'."\n";
$remark_layer_content .= '<td align="left" style="border-bottom: medium none;">'."\n";
$remark_layer_content .= '<div id="RemarkLayerContentDiv"></div>'."\n";
$remark_layer_content .= '</td>'."\n";
$remark_layer_content .= '</tr>'."\n";
$remark_layer_content .= '</tbody>'."\n";
$remark_layer_content .= '</table>'."\n";
$remark_layer_content .= '</div>'."\n";

## Checkin-checkout filter
$filter.= '<!--# Filter layer start #-->'."\n";
$filter.= '<div class="table_filter">'."\n";
	$filter.= '<div class="selectbox_group selectbox_group_filter"> <a onclick="MM_showHideLayers(\'status_option\',\'\',\'show\')" href="javascript:void(0);">'.$Lang['eBooking']['SelectStatus'].'</a> </div>'."\n";
	$filter.= '<p class="spacer"></p>'."\n";
	$filter.= '<div class="selectbox_layer selectbox_group_layer" id="status_option">'."\n";
		foreach(array(LIBEBOOKING_BOOKING_Waiting,LIBEBOOKING_BOOKING_CheckIn,LIBEBOOKING_BOOKING_CheckOut) as $thisCheckInOutStatus)
		{
			$Checked = (in_array($thisCheckInOutStatus, (array)$CheckInOutStatus))?"checked":"";
			if ($Checked){
			    $filter.= '<input type="hidden" value="'.$thisCheckInOutStatus.'" name="savedCheckInOutStatus[]" id="savedCheckInOutStatus'.$thisCheckInOutStatus.'">'."\n"; 
			}
			$filter.= '<input id="Status'.$thisCheckInOutStatus.'" type="checkbox" '.$Checked.' name="CheckInOutStatus[]" value='.$thisCheckInOutStatus.'>'."\n";
			$filter.= '<label for="Status'.$thisCheckInOutStatus.'">'.$Lang['eBooking']['CheckInOutStatusArr'][$thisCheckInOutStatus].'</label><br>'."\n";
		}
		$filter.= '<p class="spacer"></p>'."\n";
		$filter.= '<div class="edit_bottom">'."\n"; # js_Check_Reading_Status_Filter placed in reading_garden/reading_scheme.js
			$filter.= $linterface->Get_Small_Btn($Lang['Btn']['Apply'],"button","document.form1.submit(); ")."&nbsp;";
			$filter.= $linterface->Get_Small_Btn($Lang['Btn']['Cancel'],"button","MM_showHideLayers('status_option','','hide')");
		$filter.= '</div>'."\n";
	$filter.= '</div>'."\n";
$filter.= '</div>'."\n";
$filter.= '<!--# Filter layer end #-->'."\n";

//$IndicationArr[]= array("normal",$Lang['eBooking']['CheckInOutStatusArr'][LIBEBOOKING_BOOKING_Waiting]);
//$IndicationArr[]= array("avaliable",$Lang['eBooking']['CheckInOutStatusArr'][LIBEBOOKING_BOOKING_CheckIn]);
//$IndicationArr[]= array("drafted",$Lang['eBooking']['CheckInOutStatusArr'][LIBEBOOKING_BOOKING_CheckOut]);
//$Indication = $linterface->GET_RECORD_INDICATION_IP25($IndicationArr);

//$table_tool .= "<div class='common_table_tool'>";
//### Check-in btn
//$table_tool .= "<a href=\"javascript:js_ItemCheckIn('CheckIn','status_update.php')\" class=\"tool_set\">".$Lang['eBooking']['Button']['FieldTitle']['CheckIn']." / ".$Lang['eBooking']['Button']['FieldTitle']['Borrow']."</a>";
//$table_tool .= "</div>";
$table_tool_arr[] = array("set","javascript:js_ItemCheckIn('CheckIn','status_update.php')",$Lang['eBooking']['Button']['FieldTitle']['CheckIn']." / ".$Lang['eBooking']['Button']['FieldTitle']['Borrow']);
$table_tool = $linterface->Get_DBTable_Action_Button_IP25($table_tool_arr);

?>
<?=$lebooking_ui->initJavaScript();?>
<script language="javascript">
js_ArrayRoomCheck = new Array();
js_ArrayItemCheck = new Array();
js_ArrayBookingID = new Array();

$(document).ready(function () {
	$('input#keyword').val('').focus();
});

function setAllBookingIDChecked(val, obj, element_name)
{
    len=obj.elements.length;
    var i=0;
    var cnt = parseInt($('#CheckboxChecked').val());
    
    for( i=0 ; i<len ; i++) 
    {
        if (obj.elements[i].name==element_name)
        {
        	obj.elements[i].checked=val;
        	var booking_id = obj.elements[i].value;
        	
        	for(j=0; j<document.getElementsByName(booking_id+"BookingRecord[]").length; j++)
        	{
        		var isDisabled = document.getElementsByName(booking_id+"BookingRecord[]")[j].disabled;
        		if(isDisabled){
        			//no need to check
        		}
        		else{
	        		document.getElementsByName(booking_id+"BookingRecord[]")[j].checked = val;
	        		if(document.getElementsByName(booking_id+"BookingRecord[]")[j].value.indexOf("ROOM_") != -1)
	        		{
	        			// room
	        			if(val) {
	        				cnt++;
	        				setTargetBookingIDChecked(1,booking_id,"ROOM");
	        			} else {
	        				cnt--;
	        				setTargetBookingIDChecked(0,booking_id,"ROOM");
	        			}
	        		}
	        		else
	        		{
	        			// item
	        			if(val) {
	        				cnt++;
	        				setTargetBookingIDChecked(1,booking_id,"ITEM");
	        			} else {
	        				cnt--;
	        				setTargetBookingIDChecked(0,booking_id,"ITEM");
	        			}
	        		}
	        	}
        	}
        }
    }
    $('#CheckboxChecked').val(cnt);
}

function setTargetBookingIDChecked(val, booking_id, booking_type)
{		
	for(j=0; j<document.getElementsByName(eval('booking_id+"BookingRecord[]"')).length; j++)
	{
		if(val != parseInt(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].checked))
		{
			document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].checked = val;    			
    		if(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("ROOM_") != -1)
    		{
				var room_id = document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.substr(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("_")+1,document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.length);
				var new_booking_id = booking_id+'_'+room_id;
				var new_booking_type = 'ROOM';
    		}
    		else
    		{
    			var item_id = document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.substr(document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.indexOf("_")+1,document.getElementsByName(eval('booking_id+"BookingRecord[]"'))[j].value.length);
    			var new_booking_id = booking_id+'_'+item_id;
    			var new_booking_type = 'ITEM';
    		}
    		setSingleBookingIDChecked(new_booking_id, new_booking_type, val, booking_id);
    	}
	}
}

function setSingleBookingIDChecked(booking_id, booking_type, val, old_booking_id)
{
	var cnt = parseInt($('#CheckboxChecked').val());
	if(val)
	{
		cnt++;
		if(booking_type == 'ROOM'){
			js_ArrayRoomCheck[js_ArrayRoomCheck.length] = booking_id;
		}
		if(booking_type == 'ITEM'){
			js_ArrayItemCheck[js_ArrayItemCheck.length] = booking_id;
		}	
		js_ArrayBookingID[js_ArrayBookingID.length] = old_booking_id;
		$('#CheckboxChecked').val(cnt);
	}
	else
	{
		cnt--;
		if(booking_type == 'ROOM')
		{
			for(i=0; i<js_ArrayRoomCheck.length; i++)
			{
				if(js_ArrayRoomCheck[i] == booking_id)
				{
					js_ArrayRoomCheck.splice(i,1);
				}
			}
		}
		if(booking_type == 'ITEM')
		{
			for(i=0; i<js_ArrayItemCheck.length; i++)
			{
				if(js_ArrayItemCheck[i] == booking_id)
				{
					js_ArrayItemCheck.splice(i,1);
				}
			}
		}
		
		for(i=0; i<js_ArrayBookingID.length; i++)
		{
			if(js_ArrayBookingID[i] == old_booking_id) {
				js_ArrayBookingID.splice(i,1);
			}
		}
		$('#CheckboxChecked').val(cnt);
	}
}
	
function js_Show_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID)
{
	js_Hide_Detail_Layer();
	
	var jsActionArr = [];
	if (jsDetailType == 'Remark'){
		jsActionArr.push("Reload_Remark");
	} else if (jsDetailType == 'CheckOutRemark'){
		jsActionArr.push("Reload_Remark");
		jsActionArr.push("Reload_CheckOutRemark");
	}
	var jsActionArrString = jsActionArr.join();
// 	document.write(jsActionArrString);

	$('div#RemarkLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
	js_Change_Layer_Position(jsClickedObjID);
	MM_showHideLayers('RemarkLayer','','show');
			
	$('div#RemarkLayerContentDiv').load(
		"ajax_task.php",  
		{ 
			Action: jsActionArrString,
			BookingID: jsBookingID,
		},
		function(returnString)
		{
			$('div#RemarkLayerContentDiv').css('z-index', '999');
		}
	);
}

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('RemarkLayer','','hide');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsClickedObjID)
{	
	var jsOffsetLeft, jsOffsetTop;
	
	jsOffsetLeft = 487;
	jsOffsetTop = -15;
		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById('RemarkLayer').style.left = posleft + "px";
	document.getElementById('RemarkLayer').style.top = postop + "px";
	document.getElementById('RemarkLayer').style.visibility = 'visible';
}

function js_ItemCheckIn(action, page)
{
	if(action == 'CheckIn')
	{
		var str_RoomCheck = js_ArrayRoomCheck.toString();
		var str_ItemCheck = js_ArrayItemCheck.toString();
		var str_BookingID = js_ArrayBookingID.toString();
		
		$('#str_BookingID').val(str_BookingID);
		
		if($('#CheckboxChecked').val() > 0) {
			$('#page_action').val(action);
			//return;
        	document.form1.action=page;
            document.form1.submit();
        } else {
            alert(globalAlertMsg2);
        }
	}
}

function export_report()
{
	generate_report("today_records_export.php");

}

function print_report()
{
	generate_report("today_records_print.php");
}

function generate_report(webpage){
	document.form1.action= webpage;
	document.form1.target="_blank";
	
	document.form1.submit();
	document.form1.action="";
	document.form1.target="";
}


</script>
<form name="form1" action="" method="POST" >
	<div class="content_top_tool">
		<span class="tabletextremark" style="float:right; line-height: 25px;"> (<?=$Lang['eBooking']['SupportEnterOrScanBarCode']?>)</span>
		<span style="float:right;"><?=$linterface->Get_Search_Box_Div('keyword', $keyword);?></span>
		<input type="hidden" value="<?=intranet_htmlspecialchars($keyword)?>" name="savedkeyword" id="savedkeyword">
	</div>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tbody>
			<tr>
				<td colspan="2">
					<div class="Conntent_tool">
						<a href="javascript:export_report()" class="export"><?=$Lang['Btn']['Export']?></a>
						<a href="javascript:print_report()" class="print"><?=$Lang['Btn']['Print']?></a>
					</div>
				</td>
			</tr>
			<tr class="table-action-bar">
				<td valign="bottom">
					<div class="table_filter"><?=$filter?></div>
					<p class="spacer"></p>
					<?=$Indication?>
				</td>
				<td valign="bottom">
					<?=$table_tool?>
				</td>
			</tr>
		</tbody>
	</table>			

	<table border='0' width='100%' cellpadding='3' cellspacing='0'>
		<tr class='tabletop'>
			<td>#</td>
			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'];?></td>
			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'];?></td>
			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'];?></td>
			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson'];?></td>
			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy']?></td>
			<td><?=$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'];?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><input type='checkbox' onClick="(this.checked)?setAllBookingIDChecked(1,document.form1,'CheckAllBooking[]'):setAllBookingIDChecked(0,document.form1,'CheckAllBooking[]');"></td>
		</tr>
		<?=$table_content;?>
	</table>
	<?=$remark_layer_content;?>
</form>

 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>