<?

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "StockList_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2("ASDA")."</td></tr>"; 

$group_list = implode(",",$targetGroup);

$namefield = $linventory->getInventoryItemNameByLang();
$sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID IN ($group_list)";
$arr_admin_group = $linventory->returnArray($sql,2);

if(sizeof($arr_admin_group)>0)
{
	for($i=0; $i<sizeof($arr_admin_group); $i++)
	{
		list ($group_id, $group_name) = $arr_admin_group[$i];
		$table_content .= "<tr><td class=\"tabletext\" align=\"right\" width=\"50%\">$group_name</td>";
		$table_content .= "<td width=\"50%\"><input type=\"text\" name=\"group_assigned_qty_$group_id\" size=\"4\"></td></tr>\n";
	}
}

//echo "Target Action: ".$targetAction."<BR>";
//echo "Purchase Date: ".$item_purchase_date."<BR>";
//echo "Qty: ".$item_qty."<BR>";
//echo "Price: ".$item_purchase_price."<BR>";

$table_content .= "<input type=\"hidden\" name=\"targetAction\" value=\"$targetAction\">\n";
$table_content .= "<input type=\"hidden\" name=\"purchase_date\" value=\"$item_purchase_date\">\n";
$table_content .= "<input type=\"hidden\" name=\"total_qty\" value=\"$item_qty\">\n";
$table_content .= "<input type=\"hidden\" name=\"purchase_price\" value=\"$item_purchase_price\">\n";
$table_content .= "<input type=\"hidden\" name=\"total_supplier\" value=\"$total_no_supplier\">\n";

$total_no_supplier = $total_no_supplier==""?1:$total_no_supplier;
for($i=0; $i<$total_no_supplier; $i++)
{
	$table_content .= "<input type=\"hidden\" name=\"supplier_name$i\" value=\"".${"item_supplier_name$i"}."\">\n";
	$table_content .= "<input type=\"hidden\" name=\"supplier_contact$i\" value=\"".${"item_supplier_contact$i"}."\">\n";
	$table_content .= "<input type=\"hidden\" name=\"supplier_description$i\" value=\"".${"item_supplier_description$i"}."\">\n";
	$table_content .= "<input type=\"hidden\" name=\"supplier_invoice$i\" value=\"".${"item_invoice$i"}."\">\n";
	$table_content .= "<input type=\"hidden\" name=\"supplier_quotation$i\" value=\"".${"item_quotation$i"}."\">\n";
	$table_content .= "<input type=\"hidden\" name=\"supplier_tender$i\" value=\"".${"item_tender$i"}."\">\n";
	
	//$table_content .= ${"item_supplier_contact$i"};
	//$table_content .= ${"item_supplier_description$i"};
	//$table_content .= ${"item_invoice$i"};
	//$table_content .= ${"item_quotation$i"};
	//$table_content .= ${"item_tender$i"};
}

$table_content .= "<input type=\"hidden\" name=\"remark\" value=\"$item_remark\">\n";
?>

<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	
	var total_qty = <?echo $item_qty; ?>;
	
	<?
	for($i=0; $i<sizeof($arr_admin_group); $i++)
	{
		list ($group_id, $group_name) = $arr_admin_group[$i];
		//$table_content .= "<tr><td class=\"tabletext\" align=\"right\" width=\"50%\">$group_name</td>";
		//$table_content .= "<td width=\"50%\"><input type=\"text\" name=\"group_assigned_qty_$group_id\" size=\"4\"></td></tr>\n";
	?>
	sum = sum + group_assigned_qty_<?echo $group_id;?>;
	<?
	}
	?>
	if(sum > total_qty)
	{
		check = 0;
		alert("False");
	}
	if(check == 1)
		obj.action = category_items_history_insert_update.php;
}
</script>

<br>

<form name="form1" action="" method="POST" onSubmit="checkForm()">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<?=$infobar?>
</table>
<br>
<table width="70%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content?>
</table>

<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?=$image_path/$LAYOUT_SKIN ?>/10x10.gif" width="10" height="1" /></td></tr>
<tr><td height="10px"></td></tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	</td>
</tr>
</table>

</form>

<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb;
?>