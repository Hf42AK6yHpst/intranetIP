<?php

########################################
#
#	Date:	2011-05-26	YatWoon
#			Improved: Temporay hidden alert message
#
########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['Inventory'])
{
        include_once($PATH_WRT_ROOT."includes/libinventory.php");
        $linventory = new libinventory();
        
        if($sys_custom['eInventory_ItemCodeFormat_New']){
			if($linventory->checkItemCodeFormatSetting() == false){
				header("location: settings/others/others_setting.php");
				exit;
			}
		}
		
		/*
		if($linventory->checkBarcodeSettingChanged()){
			$js = "var barcodeChanged = 1;";
		}else{
			$js = "var barcodeChanged = 0;";
		}
		*/
//         $MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
        if ($linventory->hasAccessRight($_SESSION['UserID']))
        {
//             include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//             $linterface = new interface_html();

			# tag information
			#  $TAGS_OBJ[] = array($i_Discipline_System_Waiting_Approval_Event, "", 0);
		
			//$linterface->LAYOUT_START();
        	# Show alert / reminder
?>

			<script language="Javascript">
			<?//=$js;?>
			<?/* if($linventory->IS_ADMIN_USER($UserID)){ ?>
				if(barcodeChanged == 1){
					var answer = confirm("<?=$i_InventorySystem_Setting_Barcode_ChangedWarning;?>");
					if(answer){
						<?		
							$sql = "SELECT LogID FROM INVENTORY_BARCODE_SETTING_LOG ORDER BY DateInput DESC LIMIT 0,1";
							$tmp_LogID = $linventory->returnVector($sql);
						?>
						window.location="settings/others/others_setting.php?LogID=<?=$tmp_LogID[0];?>";
					}else{
						
					}
				}else{
					top.window.moveTo(0,0);
					if (document.all)
					{
					        top.window.resizeTo(screen.availWidth,screen.availHeight);
					} 
					else if (document.layers||document.getElementById)
					{
					        if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
						    {
						                top.window.outerHeight = screen.availHeight;
						                top.window.outerWidth = screen.availWidth;
						    }
					}
					window.location="management/inventory/items_full_list.php?clearCoo=1";
				}
			<? }else{ ?>
				top.window.moveTo(0,0);
				if (document.all)
				{
				        top.window.resizeTo(screen.availWidth,screen.availHeight);
				} 
				else if (document.layers||document.getElementById)
				{
				        if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
					    {
					                top.window.outerHeight = screen.availHeight;
					                top.window.outerWidth = screen.availWidth;
					    }
				}
				window.location="management/inventory/items_full_list.php?clearCoo=1";
			<?} */?>
			
			top.window.moveTo(0,0);
			if (document.all)
			{
			        top.window.resizeTo(screen.availWidth,screen.availHeight);
			} 
			else if (document.layers||document.getElementById)
			{
			        if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
				    {
				                top.window.outerHeight = screen.availHeight;
				                top.window.outerWidth = screen.availWidth;
				    }
			}
			window.location="management/inventory/items_full_list.php?clearCoo=1";
				
			</script>
    	<?
        		//$linterface->LAYOUT_STOP();
    	}
    	else
    	{
    	?>
			You have no priviledge to access this page.
    	<?
    	}
}
else
{
?>
	You have no priviledge to access this page.
<?
}
?>
