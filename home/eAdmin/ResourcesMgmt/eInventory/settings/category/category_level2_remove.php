<?php

// #####################################
//
// Date : 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date : 2018-02-22 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2012-07-03 YatWoon
// update system message
//
// Date: 2011-03-02 YatWoon
// re-update Display order
//
// #####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$cat2_id_list = implode(",", $cat2_id);

if ($cat_id != "" && $cat2_id_list != "") {
    $sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE CategoryID = '".$cat_id."' AND Category2ID IN ('".implode("','", $cat2_id)."')";
    $arr_result = $linventory->returnArray($sql, 1);
    
    if (sizeof($arr_result) > 0) {
        header("location: category_level2_setting.php?category_id=" . $cat_id . "&xmsg=DeleteUnsuccess");
    } else {
        $sql = "DELETE FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$cat_id."' AND Category2ID IN ('".implode("','", $cat2_id)."') AND ItemID = 0";
        $result_1 = $linventory->db_db_query($sql);
        
        $sql = "DELETE FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$cat_id."' AND Category2ID IN ('".implode("','", $cat2_id)."')";
        $result_2 = $linventory->db_db_query($sql);
        
        // #### Display order issue [Start]
        // re-order
        $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$cat_id."' order by DisplayOrder";
        $result = $linventory->returnVector($sql);
        for ($i = 0; $i < sizeof($result); $i ++) {
            $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=$i+1 where Category2ID='" . $result[$i]."'";
            $linventory->db_db_query($sql);
        }
        // #### Display order issue [End]
        
        if ($result_1 && $result_2) {
            header("location: category_level2_setting.php?category_id=" . $cat_id . "&xmsg=DeleteSuccess");
        } else {
            header("location: category_level2_setting.php?category_id=" . $cat_id . "&xmsg=DeleteUnsuccess");
        }
    }
} else {
    header("location: category_level2_setting.php?category_id=" . $cat_id . "&xmsg=DeleteUnsuccess");
}
intranet_closedb();

?>