<?php

// #####################################
//
// Date:  2019-05-13 Henry
// Security fix: SQL without quote
// 
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2011-03-01 YatWoon
// revised Display order coding
//
// #####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libfiletable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$photo_existed = 0;

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lf = new libfilesystem();

// $MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

if ($photo_usage == 0) {
    $default_photo = "no_photo.jpg";
}

if ($default_photo == "none") {
    // Upload Photo #
    $re_path = "/file/photo/inventory";
    $path = "$intranet_root$re_path";
    $photo = stripslashes(${"hidden_cat_photo"});
    $target = "";
    
    if ($cat_photo == "none" || $cat_photo == "") {} else {
        $lf = new libfilesystem();
        if (! is_dir($path)) {
            $lf->folder_new($path);
        }
        
        $ext = strtoupper($lf->file_ext($photo));
        if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG") {
            $target = "$path/$photo";
            $filename .= "/$photo";
            $lf->lfs_copy($cat_photo, $target);
        }
    }
} else {
    $re_path = "/images/inventory/";
    $filename = "/$default_photo";
}

if ($default_photo != "") {
    $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$cat_id."' AND Category2ID = 0 AND ItemID = 0";
    $arr_result = $linventory->returnArray($sql, 1);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($part_id) = $arr_result[$i];
            
            $sql = "UPDATE INVENTORY_PHOTO_PART SET PhotoPath = '$re_path', PhotoName = '$filename' WHERE PartID = '".$part_id."'";
            $linventory->db_db_query($sql);
        }
    }
    if (sizeof($arr_result) == 0) {
        $sql = "INSERT INTO INVENTORY_PHOTO_PART 
							(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES
							($cat_id, '0', '0', '$re_path', '$filename', NOW(), NOW())";
        $linventory->db_db_query($sql);
    }
}

$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$cat_id."' AND Category2ID = 0 AND ItemID = 0";
$arr_PartID = $linventory->returnArray($sql, 1);

if (sizeof($arr_PartID) > 0) {
    for ($j = 0; $j < sizeof($arr_PartID); $j ++) {
        list ($part_id) = $arr_PartID[$j];
        
        // added by Kelvin Ho 2008-12-19 (get the other category order if exists)
        $sql = "select DisplayOrder from INVENTORY_CATEGORY where CategoryID = '".$cat_id."'";
        $tmpOrder = $linventory->returnVector($sql);
        $orgOrder = $tmpOrder[0];
        
        // #### Display order issue [start]
        if ($orgOrder < $cat_display_order) {
            $sql = "update INVENTORY_CATEGORY set DisplayOrder=DisplayOrder-1 where DisplayOrder>$orgOrder and DisplayOrder<=$cat_display_order";
            $linventory->db_db_query($sql);
        } else 
            if ($orgOrder > $cat_display_order) {
                $sql = "update INVENTORY_CATEGORY set DisplayOrder=DisplayOrder+1 where DisplayOrder<$orgOrder and DisplayOrder>=$cat_display_order";
                $linventory->db_db_query($sql);
            }
        // #### Display order issue [end]
        
        if ($sys_custom['eInventory_PriceCeiling']) {
            if ($apply_to_bulk == '') {
                $apply_to_bulk = 0;
            }
            $sql = "UPDATE 
							INVENTORY_CATEGORY 
					SET 
							Code = '$cat_code', 
							NameChi = '$cat_chi_name',
							NameEng = '$cat_eng_name',
							DisplayOrder = '$cat_display_order',
							PhotoLink = '$part_id',
							PriceCeiling = '$price_ceiling',
							ApplyPriceCeilingToBulk = $apply_to_bulk,
							RecordStatus=$recordstatus
					WHERE 
							CategoryID = '".$cat_id."'";
            $result['UpdateCat' . $i] = $linventory->db_db_query($sql);
        } else {
            $sql = "UPDATE 
							INVENTORY_CATEGORY 
					SET 
							Code = '$cat_code', 
							NameChi = '$cat_chi_name',
							NameEng = '$cat_eng_name',
							DisplayOrder = '$cat_display_order',
							PhotoLink = '$part_id',
							RecordStatus=$recordstatus
					WHERE 
							CategoryID = '".$cat_id."'";
            $result['UpdateCat' . $i] = $linventory->db_db_query($sql);
        }
        
        /*
         * #added by Kelvin Ho 2008-12-19 (modify the other category order if exists)
         * $sql = "select CategoryID from INVENTORY_CATEGORY where DisplayOrder = '$cat_display_order' and CategoryID!=$cat_id";
         * $tmpOrder = $linventory->returnVector($sql);
         * $order = $tmpOrder[0];
         * if($order)
         * {
         * $sql = "update INVENTORY_CATEGORY set DisplayOrder = '$orgOrder' where CategoryID = ".$order;
         * $linventory->db_db_query($sql);
         * }
         */
        // #### Display order issue [Start]
        // re-order
        $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY order by DisplayOrder";
        $result = $linventory->returnVector($sql);
        for ($i = 0; $i < sizeof($result); $i ++) {
            $sql = "update INVENTORY_CATEGORY set DisplayOrder=$i+1 where CategoryID='" . $result[$i]."'";
            $linventory->db_db_query($sql);
        }
        // #### Display order issue [End]
    }
}
intranet_closedb();

if (! in_array(false, $result)) {
    header("Location: category_setting.php?msg=2");
} else {
    header("Location: category_setting.php?msg=14");
}
?>