<?php
# using: yat

######################################
#
#	Date:	2012-07-03	YatWoon
#			update system message
#			add "status"
#
#	Date:	2011-03-02	YatWoon
#			Fixed: incorrect query for insert INVENTORY_PHOTO_PART
#
#	Date:	2011-03-01	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$li 			= new libdb();
$lf				= new libfilesystem();
$lc 			= new libeclass();
$linventory		= new libinventory();

##### Display order issue [Start]
$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE DisplayOrder = $cat_display_order";
$result = $linventory->returnVector($sql);
$existingCatID = $result[0];
if($existingCatID != "")
{
	$sql = "update INVENTORY_CATEGORY set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$cat_display_order";
	$linventory->db_db_query($sql);
}
##### Display order issue [End]

if($sys_custom['eInventory_PriceCeiling']){
	$fields = "Code, NameChi, NameEng, DisplayOrder, PhotoLink, PriceCeiling, ApplyPriceCeilingToBulk, RecordStatus, DateInput, DateModified";
	$values = "'$cat_code', '$cat_chi_name', '$cat_eng_name', $cat_display_order, '', $price_ceiling, '$apply_to_bulk', $recordstatus, NOW(), NOW()";
}else{
	$fields = "Code, NameChi, NameEng, DisplayOrder, PhotoLink, RecordStatus, DateInput, DateModified";
	$values = "'$cat_code', '$cat_chi_name', '$cat_eng_name', $cat_display_order, '', $recordstatus, NOW(), NOW()";
}
$sql = "INSERT INTO INVENTORY_CATEGORY ($fields) VALUES ($values)";
$result = $linventory->db_db_query($sql);

##### Display order issue [Start]
# re-order
$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY order by DisplayOrder";
$result = $linventory->returnVector($sql);
for($i=0;$i<sizeof($result);$i++)
{
	$sql="update INVENTORY_CATEGORY set DisplayOrder=$i+1 where CategoryID=". $result[$i];
	$linventory->db_db_query($sql) or die(mysql_error());
}
##### Display order issue [End]

if($photo_usage == 0){
	$default_photo = "no_photo.jpg";
}	


if ($result)
{
	$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '$cat_code'";
	$result = $linventory->returnArray($sql,1);
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($cat_id) = $result[$i];
		}
		
		if($default_photo == "none")
		{
			# Upload Photo #
			$re_path = "/file/photo/inventory";
			$path = "$intranet_root$re_path";
			$photo = stripslashes(${"hidden_cat_photo"});
			$target = "";
			
			if($cat_photo=="none" || $cat_photo == ""){
			}
			else
			{
				$lf = new libfilesystem();
				if (!is_dir($path))
				{
					$lf->folder_new($path);
				}
				
				$ext = strtoupper($lf->file_ext($photo));
				if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG")
				{
					$target = "$path/$photo";
					$filename .= "/$photo";
					$lf->lfs_copy($cat_photo, $target);
				}
		    }
	    }
	    else
	    {
		    $re_path = "/images/inventory/";
		    $filename = "/$default_photo";
	    }
			
	    $values = "($cat_id, '0', '0', '$re_path', '$filename', NOW(), NOW())";
		$sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
		$li->db_db_query($sql) or die(mysql_error());
		
		if($li->db_affected_rows($sql) > 0)
		{
			$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = 0 AND ItemID = 0 ";
			
			$result = $li->returnArray($sql,1);
			
			if(sizeof($result)>0)
			{
				list($part_id) = $result[0];
			}
		}
		
		$sql = "UPDATE INVENTORY_CATEGORY SET PhotoLink = '$part_id' WHERE CategoryID = '$cat_id'";
		$linventory->db_db_query($sql);
	}
	header("Location: category_setting.php?xmsg=AddSuccess");
}
else
{
	header("Location: category_setting.php?xmsg=AddUnsuccess");
}

intranet_closedb();

?>