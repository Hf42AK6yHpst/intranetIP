<?
# using: yat

######################################
#
#	Date:	2012-07-03	YatWoon
#			update system message
#
#	Date:	2011-03-01	YatWoon
#			re-update Display order
#
######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$cat_id_list = implode(",",$cat_id);

if($cat_id_list != "")
{
	# Check Any Items Exist #
	$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE CategoryID IN ($cat_id_list)";
	$arr_result = $linventory->returnArray($sql,1);

	if(sizeof($arr_result)>0)
	{
		header("location: category_setting.php?xmsg=DeleteUnsuccess");
	}
	else
	{
		$sql = "DELETE FROM INVENTORY_PHOTO_PART WHERE CategoryID IN ($cat_id_list)";
		$result_1 = $linventory->db_db_query($sql);
		
		$sql = "DELETE FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID IN ($cat_id_list)";
		$result_2 = $linventory->db_db_query($sql);
		
		$sql = "DELETE FROM INVENTORY_CATEGORY WHERE CategoryID IN ($cat_id_list)";
		$result_3 = $linventory->db_db_query($sql);
		
		##### Display order issue [Start]
		# re-order
		$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY order by DisplayOrder";
		$result = $linventory->returnVector($sql);
		for($i=0;$i<sizeof($result);$i++)
		{
			$sql="update INVENTORY_CATEGORY set DisplayOrder=$i+1 where CategoryID=". $result[$i];
			$linventory->db_db_query($sql);
		}
		##### Display order issue [End]

		if($result_1 && $result_2 && $result_3)
		{
			header("location: category_setting.php?xmsg=DeleteSuccess");
		}
		else
		{
			header("location: category_setting.php?xmsg=DeleteUnsuccess");
		}
	}
}
else
{
	header("location: category_setting.php?xmsg=DeleteUnsuccess");
}
intranet_closedb();
?>