<?php
# using: yat

####################################
#
#	Date:	2012-07-03	YatWoon
#			IP25 layout, add "Status"
#
#	Date:	2011-03-10	YatWoon
#			update display order default value and after change "photo" default value
#
####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Category ###
// $temp[] = array($i_InventorySystem['Settings']);
// $infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";
// $infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['Category']." > ".$button_add)."</td></tr>"; 

$PAGE_NAVIGATION[] = array($i_InventorySystem['Category'],"category_setting.php");
$PAGE_NAVIGATION[] = array($button_add);

$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// get all category's Code, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng FROM INVENTORY_CATEGORY";
$CatNameList = $linventory->returnArray($sql,3);

$sql = "SELECT COUNT(*) FROM INVENTORY_CATEGORY";
$total_rec = $linventory->returnVector($sql);
$display_order .= "<select name=\"cat_display_order\">";
for($i=0; $i<=$total_rec[0]; $i++)
{
	$j=$i+1;
	$selected = "";
	if(($j==($total_rec[0]+1) && !$cat_display_order) || ($j==$cat_display_order))
		$selected = "SELECTED=\"selected\"";
	$display_order .= "<option value=\"$j\" $selected>$j</option>";
}
$display_order .= "</select>";


## Show The Default Photos ##
$image_folder = "images/inventory";
$targetDir = $PATH_WRT_ROOT.$image_folder;
$handle = @opendir($targetDir);
while (false !== ($file = @readdir($handle))) 
{
	if($file!="." && $file!='..')
	{
		if($cnt%3 == 0)
			$default_photo_link .= "<tr>";
		
		if($cnt == 0)
		{
			$checked = " CHECKED ";
		}
		else
		{
			$checked = " ";
		}
			
		$default_photo_link .= "<td class=\"tabletext\" valign=\"top\">
									<img src=\"$targetDir/$file\" width=\"100px\" height=\"100px\">
									<center><input type=\"radio\" name=\"default_photo\" value=\"$file\" $checked onClick=\"SetPhotoUpload(0);\"></center>	
								</td>";
		if($cnt%3 == 2)
			$default_photo_link .= "</tr>";
			
		$cnt++;
	}
}
## end ##

$arr_PhotoUsage = array(array("0",$i_InventorySystem_Setting_Photo_DoNotUse),array("1",$i_InventorySystem_Setting_Photo_Use));
$table_content .= "<tr><td class='field_title'>$i_InventorySystem_Category_Code</td><td><input name=\"cat_code\" type=\"text\" class=\"textboxnum\" maxlength=\"5\" value=\"$cat_code\"></td></tr>";
$table_content .= "<tr><td class='field_title'>$i_InventorySystem_Category_ChineseName</td><td><input name=\"cat_chi_name\" type=\"text\" class=\"textboxtext\" value=\"$cat_chi_name\"></td></tr>";
$table_content .= "<tr><td class='field_title'>$i_InventorySystem_Category_EnglishName</td><td><input name=\"cat_eng_name\" type=\"text\" class=\"textboxtext\" value=\"$cat_eng_name\"></td></tr>";
if($sys_custom['eInventory_PriceCeiling']){
	if($price_ceiling == ""){
		$price_ceiling = 0;
	}
	$table_content .= "<tr><td class='field_title'>$i_InventorySystem_CategorySetting_PriceCeiling</td>
							<td>
								<input type=\"text\" name=\"price_ceiling\" class=\"textboxnum\" value=\"$price_ceiling\"> <input type='checkbox' name='apply_to_bulk' value=1> $i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk
							</td>
						</tr>";
}
$table_content .= "<tr><td class='field_title'>$i_InventorySystem_Category_DisplayOrder</td><td>$display_order</td></tr>";
$table_content .= "<tr><td class='field_title'>$i_InventorySystem_Category_Photo</td>
						<td>".getSelectByArray($arr_PhotoUsage,"name='photo_usage' onChange='enablePhotoSelection();'",$photo_usage,0,1)."</td></tr>";
if($photo_usage){
$table_content .= "<tr>
						<td></td>
						<td class=\"tabletext\" valign=\"top\">
						<table border=\"0\" width=\"100%\">
						$default_photo_link
						</table><br>
						<input type=\"radio\" name=\"default_photo\" value=\"none\" onClick=\"SetPhotoUpload(1);\">$i_InventorySystem_Category_CustomPhoto&nbsp;
						<input type=\"file\" name=\"cat_photo\" class=\"file\" DISABLED><input type=\"hidden\" name=\"hidden_cat_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>";

	$submit_btn = $linterface->GET_ACTION_BTN($button_submit, "submit", "grapAttach(this.form)") ." ";			
}else{
	$submit_btn = $linterface->GET_ACTION_BTN($button_submit, "submit") ." ";
}

$arr_Status = array(array("1",$Lang['eInventory']['InUse'] ),array("-1",$Lang['eInventory']['NotInUse'] ));
$table_content .= "<tr><td class='field_title'>". $Lang['eInventory']['Status']."</td>
						<td>".getSelectByArray($arr_Status,"name='recordstatus'","",0,1)."</td></tr>";

$buttons = "
			<div class=\"edit_bottom_v30\">
			<p class=\"spacer\"></p>". $submit_btn . $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ."
			<p class=\"spacer\"></p>
			</div> 
";
				 
echo generateFileUploadNameHandler("form1","cat_photo","hidden_cat_photo");

?>

<script language="javascript">

function enablePhotoSelection()
{
	var obj = document.form1;
	obj.action = "";
	obj.submit();
}

function SetPhotoUpload(enabled)
{
	if(enabled == 1)
	{
		document.form1.cat_photo.disabled = false;
	}
	else
	{
		document.form1.cat_photo.disabled = true;
	}
}

<? if($photo_usage){ ?>
function grapAttach(cform)
{
	var obj = document.form1.cat_photo;
	var key;
	var s="";
	key = obj.value;
	if (key!="")
		s += key;
	cform.attachStr.value = s;
}
<? } ?>

function checkCategoryCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.cat_code.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_Category_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat_code.value == "<?=$CatNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_Category_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkCategoryChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat_chi_name.value == "<?=addslashes($CatNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_Category_Item_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkCategoryEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($CatNameList); $i++) {
	?>
			if (obj.cat_eng_name.value == "<?=$CatNameList[$i]['NameEng']?>") {
					alert("<?=$i_InventorySystem_Input_Category_Item_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;
	<? if($photo_usage){ ?>
	var upload = Big5FileUploadHandler();
	<? } ?>
	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
	
	<? if($photo_usage){ ?>
	if(upload == true)
	{
	<? } ?>
		if(check_text(obj.cat_code,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
			if(checkCategoryCode()) {
				if(check_text(obj.cat_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
					if(check_text(obj.cat_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						<? if($sys_custom['eInventory_PriceCeiling']){ ?>
							if(obj.price_ceiling.value >= 0){
								passed = 1;
							}else{
								alert("<?=$i_InventorySystem_CategorySetting_PriceCeiling_JSWarning1;?>");
								passed = 0;
								return false;
							}
						<? } ?>
								passed = 1;	
					}else {
							passed = 0;
					}
				}else {
						passed = 0;
				}
			}else {
				error_cnt = 1;
				passed = 0;
			}
		}else {
			passed = 0;
		}
	<? if($photo_usage){ ?>
	}
	<? } ?>
				
	if(passed == 1)
	{
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.cat_code.focus();
		if(error_cnt == 2)
			obj.cat_chi_name.focus();
		if(error_cnt == 3)
			obj.cat_eng_name.focus();
			
		return false;
	}
}
</script>

<div class="navigation">
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<p class="spacer"></p>
</div>

<form name="form1" enctype="multipart/form-data" method="post" action="category_insert_update.php" onsubmit="return checkForm();">
<table class="form_table_v30">
<?=$table_content?>
</table>

<?=$buttons?>

<input type="hidden" name="attachStr" value="">
<input type="hidden" name="flag" value="1">
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>