<?php
# using: yat

######################################
#
#	Date:	2014-05-26	YatWoon
#			fixed: failed to import special character [Case#H62358]
#
#	Date:	2012-07-03	YatWoon
#			set default "status" to "in use"
#
#	Date:	2011-03-02	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$sql = "SELECT 
				Code, 
				NameChi, 
				NameEng, 
				DisplayOrder 
		FROM 
				TEMP_INVENTORY_CATEGORY";

$arr_result = $linventory->returnArray($sql,5);

if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($category_code,$category_namechi, $category_nameeng, $category_displayorder) = $arr_result[$i];
		
		$category_namechi = addslashes(intranet_undo_htmlspecialchars($category_namechi));
		$category_nameeng = addslashes(intranet_undo_htmlspecialchars($category_nameeng));
		
		##### Display order issue [Start] 
		$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE DisplayOrder = $category_displayorder";
		$result = $linventory->returnVector($sql);
		$existingCatID = $result[0];
		if($existingCatID != "")
		{
			$sql = "update INVENTORY_CATEGORY set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$category_displayorder";
			$linventory->db_db_query($sql);
		}
		##### Display order issue [End]

		$values = "'$category_code', '$category_namechi', '".$category_nameeng."', '$category_displayorder', 1, NOW(), NOW()";
		
		$sql = "INSERT INTO INVENTORY_CATEGORY
						(Code, 
						NameChi, 
						NameEng, 
						DisplayOrder, 
						RecordStatus,
						DateInput, 
						DateModified)
				VALUES
						($values)";
		$result['NewInvItem'.$i] = $linventory->db_db_query($sql);
		
		$sql = "SELECT 
						CategoryID
				FROM
						INVENTORY_CATEGORY
				WHERE
						Code = '$category_code'
				";
		$arr_cat_id = $linventory->returnVector($sql);
		
		if(sizeof($arr_cat_id)>0)
		{
			$category_id = $arr_cat_id[0];
		}
		
		$re_path = "/images/inventory/";
		$filename = "/no_photo.jpg";
		
		$values = "($category_id, '0', '0', '$re_path', '$filename', NOW(), NOW())";
		$sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
		
		$linventory->db_db_query($sql);
		
		if($linventory->db_affected_rows($sql) > 0)
		{
			$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$category_id' AND Category2ID = 0 AND ItemID = 0 ";
			
			$result = $linventory->returnArray($sql,1);
			
			if(sizeof($result)>0)
			{
				list($part_id) = $result[0];
			}
		}
		
		$sql = "UPDATE INVENTORY_CATEGORY SET PhotoLink = '$part_id' WHERE CategoryID = '$category_id'";
		
		$linventory->db_db_query($sql);
	}

	##### Display order issue [Start]
	# re-order
	$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY order by DisplayOrder";
	$result = $linventory->returnVector($sql);
	for($i=0;$i<sizeof($result);$i++)
	{
		$sql="update INVENTORY_CATEGORY set DisplayOrder=$i+1 where CategoryID=". $result[$i];
		$linventory->db_db_query($sql) or die(mysql_error());
	}
	##### Display order issue [End]

}

if (in_array(false,$result)) {
	header("location: category_import.php?xmsg=ImportUnsuccess");
}
else {
	header("location: category_import.php?xmsg=ImportSuccess");
}
intranet_closedb();
?>