<?
# using: yat

######################################
#
#
######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();
$linventory		= new libinventory();

$cat_id_list = implode(",",$cat_id);

$sql = "update INVENTORY_CATEGORY set RecordStatus=$status where CategoryID in ($cat_id_list)";
$linventory->db_db_query($sql);

intranet_closedb();
header("location: category_setting.php?xmsg=UpdateSuccess");
?>