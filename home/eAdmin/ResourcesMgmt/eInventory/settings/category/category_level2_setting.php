<?php
// using: 

// ########################################
// Date:  2019-05-13 Henry
// Security fix: SQL without quote
// 
// Date : 2018-02-22 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2012-07-03 YatWoon
// Improved: Add "status"
// Improved: IP25 layout standard
// ########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Settings_Category";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Category ###
$sql = "SELECT " . $linventory->getInventoryNameByLang() . " as CatName FROM INVENTORY_CATEGORY WHERE CategoryID = '".$category_id."'";
$CategoryInfoArr = $linventory->returnArray($sql);
$arr_cat_name = $CategoryInfoArr[0]['CatName'];

// $temp[] = array("<a href='category_setting.php'>".$i_InventorySystem['Category']."</a> > ".intranet_htmlspecialchars($arr_cat_name)." > ".$i_InventorySystem['SubCategory']);
// $infobar1 .= "<tr><td colspan=\"2\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";

$PAGE_NAVIGATION[] = array(
    $i_InventorySystem['Category'],
    "category_setting.php"
);
$PAGE_NAVIGATION[] = array(
    intranet_htmlspecialchars($arr_cat_name)
);

$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

/*
 * if ($xmsg != "") {
 * $SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
 * } else {
 * if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
 * if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
 * if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
 * if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
 * if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_SubCatgeory_DeleteFail");
 * if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
 * }
 */

$namefield = $linventory->getInventoryNameByLang();

$sql = "SELECT 
				Category2ID, 
				CategoryID, 
				Code,
				$namefield,
				if(RecordStatus=1, '" . $Lang['eInventory']['InUse'] . "', '" . $Lang['eInventory']['NotInUse'] . "')
		FROM 
				INVENTORY_CATEGORY_LEVEL2
		WHERE
				CategoryID = '".$category_id."'
		ORDER BY
				DisplayOrder";

$result = $linventory->returnArray($sql);

$table_content = "<thead><tr>";
$table_content .= "<th width=\"1%\">#</th>";
$table_content .= "<th width=\"20%\">$i_InventorySystem_SubCategory_Code</th>";
$table_content .= "<th width=\"60%\">$i_InventorySystem_SubCategory_Name</th>";
$table_content .= "<th width=\"10%\">" . $Lang['eInventory']['Status'] . "</th>";
$table_content .= "<th width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setSubCatChecked(1,this.form,'cat2_id[]'):setSubCatChecked(0,this.form,'cat2_id[]')\"></th>";
$table_content .= "</tr></thead>";

if (sizeof($result) > 0) {
    for ($i = 0; $i < sizeof($result); $i ++) {
        list ($cat2_id, $cat_id, $cat2_code, $cat2_name, $recordstatus) = $result[$i];
        $j ++;
        // $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
        
        // $thisCanDelete = $linventory->Check_Can_Delete_Category('', $cat2_id);
        // $thisDisabled = ($thisCanDelete)? '' : 'disabled';
        
        $table_content .= "<tr>";
        $table_content .= "<td>$j</td>\n";
        $table_content .= "<td>" . intranet_htmlspecialchars($cat2_code) . "</td>\n";
        // $table_content .= "<td>".intranet_htmlspecialchars($cat2_name)."</td>\n";
        $table_content .= "<td><a class=\"tablelink\" href='#' onClick='GoEditSubCategoryPage($cat_id, $cat2_id)'>" . intranet_htmlspecialchars($cat2_name) . "</a></td>\n";
        $table_content .= "<td>" . $recordstatus . "</td>\n";
        $table_content .= "<td><input type=\"checkbox\" name=\"cat2_id[]\" value=\"$cat2_id\" $thisDisabled>";
        $table_content .= "<input name=\"cat_id\" type=\"hidden\" value=\"$cat_id\"></td>\n";
        $table_content .= "</tr>\n";
    }
} else {
    $table_content .= "<tr class=\"tablerow2 tabletext\">";
    $table_content .= "<td colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td>";
    $table_content .= "</tr>\n";
}

$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"4\"></td></tr>";

// ## eBooking & eInventory Data Warning
$WarningMsgArr = array();
if ($plugin['eBooking'] && $plugin['Inventory']) {
    $WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['DataChangeAffect_eInventory'];
    $WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'];
    $WarningMsg = implode('<br />', $WarningMsgArr);
    // $WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Warning'].'</font>', $WarningMsg, $others="");
    $WarningBox = "<fieldset class='instruction_warning_box_v30'><legend>" . $Lang['General']['Warning'] . "</legend> " . $WarningMsg . "</fieldset>";
}
?>

<script language='javascript'>

	function GoEditSubCategoryPage(cat_id, cat2_id)
	{
		window.location = 'category_level2_edit.php?cat_id=' + cat_id + '&cat2_id=' + cat2_id;
	}
	
	function setSubCatChecked(val,obj,element)
	{
		if(val == 1)
		{
			for(i=0; i<document.getElementsByName(element).length; i++){
				if(document.getElementsByName(element)[i].disabled == false)
				{
					document.getElementsByName(element)[i].checked = true; 
				}
			}	
		}
		else
		{
			for(i=0; i<document.getElementsByName(element).length; i++){
				if(document.getElementsByName(element)[i].disabled == false)
				{
					document.getElementsByName(element)[i].checked = false; 
				}
			}
		}
	}
	
</script>

<div class="navigation">
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<p class="spacer"></p>
</div>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<?=$linterface->GET_LNK_NEW("javascript:checkNew('category_level2_insert.php?cat_id=$category_id')","","","","",0);?>
		<?=$linterface->GET_LNK_IMPORT("category_level2_import.php?category_id=".$category_id."","","","","",0);?>
	</div>
	<br style="clear: both" />
</div>

<?=$WarningBox?>

<form name="form1" action="" method="post">

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="common_table_tool">
						<a
							href="javascript:checkRemove(document.form1,'cat2_id[]','category_level2_update_status.php?status=1','<?=$Lang['eInventory']['UpdateStatusConfirm']?>')"
							class="tool_other"><?=$Lang['eInventory']['SetInUse']?></a> <a
							href="javascript:checkRemove(document.form1,'cat2_id[]','category_level2_update_status.php?status=-1','<?=$Lang['eInventory']['UpdateStatusConfirm']?>')"
							class="tool_other"><?=$Lang['eInventory']['SetNotInUse']?></a> <a
							href="javascript:checkRemove(document.form1,'cat2_id[]','category_level2_remove.php')"
							class="tool_delete"><?=$button_delete?></a>
					</div>
				</td>
			</tr>
		</table>

		<table class="common_table_list">
<?=$table_content?>
</table>


	</div>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>