<?php

// using:

// #####################################
//
// Date:  2019-05-13 Henry
// Security fix: SQL without quote
// 
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2014-05-26 YatWoon
// fixed: failed to import special character [Case#H62358]
//
// Date: 2012-07-03 YatWoon
// set default "status" to "in use"
//
// Date: 2011-07-18 YatWoon
// - Fixed: Failed to import records completely due to $i vaiable re-declared
//
// Date: 2011-07-11 YatWoon
// - improve: "Display order" field is not a mandatory field
//
// Date: 2011-03-10 YatWoon
// re-update Display order
//
// #####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Settings_BasicSettings";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$sql = "SELECT 
				CategoryID, 
				Code,
				NameChi, 
				NameEng, 
				HasSoftwareLicenseModel,
				HasWarrantyExpiryDate,
				HasSerialNumber,
				DisplayOrder 
		FROM 
				TEMP_INVENTORY_CATEGORY_LEVEL2";

$arr_result = $linventory->returnArray($sql, 7);

if (sizeof($arr_result) > 0) {
    for ($k = 0; $k < sizeof($arr_result); $k ++) {
        list ($category_id, $category_code, $category_namechi, $category_nameeng, $category_license, $category_expiry, $category_serial, $category_displayorder) = $arr_result[$k];
        
        $category_namechi = addslashes(intranet_undo_htmlspecialchars($category_namechi));
        $category_nameeng = addslashes(intranet_undo_htmlspecialchars($category_nameeng));
        
        $assign_order = 0;
        if ($category_displayorder) {
            $assign_order = 1;
            // #### Display order issue [Start]
            $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$category_id."' AND DisplayOrder = '".$category_displayorder."'";
            $result = $linventory->returnVector($sql);
            $existingSubCatID = $result[0];
            if ($existingSubCatID != "") {
                $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=DisplayOrder+1 where CategoryID = '".$category_id."' and DisplayOrder>=$category_displayorder";
                $linventory->db_db_query($sql) or die(mysql_error());
            }
            // #### Display order issue [End]
        } else {
            $sql = "select max(DisplayOrder) from INVENTORY_CATEGORY_LEVEL2 where CategoryID = '".$category_id."'";
            $result = $linventory->returnVector($sql);
            $category_displayorder = $result[0] + 1;
        }
        
        $values = "'$category_id', '$category_code', '$category_namechi', '$category_nameeng', '$category_displayorder', '$category_license', '$category_expiry', '$category_serial', 1, NOW(), NOW()";
        
        $sql = "INSERT INTO INVENTORY_CATEGORY_LEVEL2
						(CategoryID,
						Code, 
						NameChi, 
						NameEng, 
						DisplayOrder, 
						HasSoftwareLicenseModel,
						HasWarrantyExpiryDate,
						HasSerialNumber,
						RecordStatus,
						DateInput, 
						DateModified)
				VALUES
						($values)";
        $result['NewInvItem' . $k] = $linventory->db_db_query($sql) or die(mysql_error());
        
        if ($assign_order) {
            // #### Display order issue [Start]
            // re-order
            $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 where CategoryID = '".$category_id."' order by DisplayOrder";
            $result = $linventory->returnVector($sql);
            for ($i = 0; $i < sizeof($result); $i ++) {
                $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=$i+1 where Category2ID='" . $result[$i]."'";
                $linventory->db_db_query($sql) or die(mysql_error());
            }
            // #### Display order issue [End]
        }
        
        $re_path = "/images/inventory/";
        $filename = "/no_photo.jpg";
        
        $sql = "SELECT
						Category2ID
				FROM
						INVENTORY_CATEGORY_LEVEL2
				WHERE
						Code = '$category_code'
				";
        
        $arr_cat2_id = $linventory->returnVector($sql);
        
        if (sizeof($arr_cat2_id) > 0) {
            $category2_id = $arr_cat2_id[0];
        }
        
        $values = "($category_id, $category2_id, 0, '$re_path', '$filename', NOW(), NOW())";
        
        $sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
        
        $linventory->db_db_query($sql);
        
        if ($linventory->db_affected_rows($sql) > 0) {
            $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$category_id' AND Category2ID = '$category2_id' AND ItemID= 0";
            
            $result = $linventory->returnArray($sql, 1);
            
            if (sizeof($result) > 0) {
                list ($part_id) = $result[0];
            }
        }
        
        $sql = "UPDATE INVENTORY_CATEGORY_LEVEL2 SET PhotoLink = '$part_id' WHERE CategoryID = '$category_id' AND Category2ID = '$category2_id'";
        
        $linventory->db_db_query($sql);
    }
}

if (in_array(false, $result)) {
    header("location: category_level2_import.php?xmsg=ImportUnsuccess&category_id=$cat_id");
} else {
    header("location: category_level2_import.php?xmsg=ImportSuccess&category_id=$cat_id");
}
intranet_closedb();
?>