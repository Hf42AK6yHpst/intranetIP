<?php

// #####################################
//
// Date:  2019-05-13 Henry
// Security fix: SQL without quote
// 
// Date : 2018-02-22 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2012-07-03 YatWoon
// update system message
//
// Date: 2011-03-02 YatWoon
// re-update Display order
//
// #####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libfiletable.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// #### Display order issue [Start]
$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$cat_id."' AND DisplayOrder = '".$cat2_display_order."'";
$result = $linventory->returnVector($sql);
$existingSubCatID = $result[0];
if ($existingSubCatID != "") {
    $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=DisplayOrder+1 where CategoryID = '".$cat_id."' and DisplayOrder>=$cat2_display_order";
    $linventory->db_db_query($sql);
}
// #### Display order issue [End]

$sql = "INSERT INTO INVENTORY_CATEGORY_LEVEL2 
				(CategoryID, Code, NameChi, NameEng, DisplayOrder, PhotoLink, HasSoftwareLicenseModel, HasWarrantyExpiryDate, HasSerialNumber, RecordStatus, DateInput, DateModified)
		VALUES 
				('$cat_id', '$cat2_code', '$cat2_chi_name', '$cat2_eng_name', $cat2_display_order, '', '$cat2_license', '$cat2_warranty', '$cat2_serial', $recordstatus, NOW(), NOW())";

$linventory->db_db_query($sql);

if ($linventory->db_affected_rows($sql) == 1) {
    // #### Display order issue [Start]
    // re-order
    $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 where CategoryID = '".$cat_id."' order by DisplayOrder";
    $result = $linventory->returnVector($sql);
    for ($i = 0; $i < sizeof($result); $i ++) {
        $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=$i+1 where Category2ID='" . $result[$i]."'";
        $linventory->db_db_query($sql) or die(mysql_error());
    }
    // #### Display order issue [End]
    
    $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '$cat_id' AND Code = '$cat2_code'";
    
    $result = $linventory->returnArray($sql, 1);
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($cat2_id) = $result[$i];
        }
        
        if ($photo_usage == 0) {
            $default_photo = "no_photo.jpg";
        }
        
        if ($default_photo == "none") {
            // Upload Photo #
            $re_path = "/file/photo/inventory";
            $path = "$intranet_root$re_path";
            $photo = stripslashes(${"hidden_cat2_photo"});
            $target = "";
            
            if ($cat2_photo == "none" || $cat2_photo == "") {} else {
                $lf = new libfilesystem();
                if (! is_dir($path)) {
                    $lf->folder_new($path);
                }
                
                $ext = strtoupper($lf->file_ext($photo));
                if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG") {
                    $target = "$path/$photo";
                    $filename .= "/$photo";
                    $lf->lfs_copy($cat2_photo, $target);
                }
            }
        } else {
            $re_path = "/images/inventory/";
            $filename = "/$default_photo";
        }
        
        $values .= "($cat_id, $cat2_id, 0, '$re_path', '$filename', NOW(), NOW())";
        
        $sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
        
        $linventory->db_db_query($sql);
        
        if ($linventory->db_affected_rows($sql) > 0) {
            $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id' AND ItemID= 0";
            
            $result = $linventory->returnArray($sql, 1);
            
            if (sizeof($result) > 0) {
                list ($part_id) = $result[0];
            }
        }
        
        $sql = "UPDATE INVENTORY_CATEGORY_LEVEL2 SET PhotoLink = '$part_id' WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id'";
        
        $linventory->db_db_query($sql);
        
        // ##################
        
        /*
         * # Upload Photo #
         * $re_path = "/file/photo/inventory";
         * $path = "$file_path/$re_path";
         * $lfile->folder_new($path);
         *
         * $lremove = new libfiletable("", $path, 0, 0, "");
         * $files = $lremove->files;
         *
         * $attachStr=stripslashes($attachStr);
         *
         * $loc = ${"cat2_photo"};
         * $file = stripslashes(${"hidden_cat2_photo"});
         * $des = "$path/".$file;
         *
         * if (!is_file($loc))
         * {
         * }
         * if($loc=="")
         * {
         * }
         * else
         * {
         * $ext = strtoupper($lfile->file_ext($hidden_cat2_photo));
         * if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG"){
         * $lfile->lfs_copy($loc, $des);
         * }
         *
         * $lo = new libfiletable("", $path, 0, 0, "");
         * $files = $lo->files;
         *
         * while (list($key, $value) = each($files)) {
         * if(in_array($file, $files[$key]))
         * {
         * list($filename, $filesize) = $files[$key];
         * $filename = addslashes($filename);
         * $filesize = ceil($filesize/1000);
         * $values .= "$delim('$cat_id','$cat2_id','$re_path', '$filename', NOW(),NOW())";
         * $delim = ",";
         *
         * $photo_existed = 1;
         * }
         * if($photo_existed != 1)
         * {
         * if($key == sizeof($files)-1)
         * {
         * list($filename, $filesize) = $files[$key];
         * $filename = addslashes($filename);
         * $filesize = ceil($filesize/1000);
         * $values .= "$delim('$cat_id','$cat2_id','$re_path', '$filename', NOW(), NOW())";
         * $delim = ",";
         * }
         * }
         * }
         *
         * $sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART
         * (CategoryID, Category2ID, PhotoPath, PhotoName, DateInput, DateModified)
         * VALUES
         * $values";
         *
         * $linventory->db_db_query($sql);
         *
         * if($linventory->db_affected_rows($sql) > 0)
         * {
         * $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id'";
         *
         * $result = $linventory->returnArray($sql,1);
         *
         * if(sizeof($result)>0)
         * {
         * list($part_id) = $result[0];
         * }
         * }
         *
         * $sql = "UPDATE INVENTORY_CATEGORY_LEVEL2 SET PhotoLink = '$part_id' WHERE CategoryID = '$cat_id' AND Category2ID = '$cat2_id'";
         *
         * $linventory->db_db_query($sql);
         * }
         */
    }
    header("Location: category_level2_setting.php?category_id=" . $cat_id . "&xmsg=AddSuccess");
} else {
    header("Location: category_level2_setting.php?category_id=" . $cat_id . "&xmsg=AddUnsuccess");
}

intranet_closedb();

?>