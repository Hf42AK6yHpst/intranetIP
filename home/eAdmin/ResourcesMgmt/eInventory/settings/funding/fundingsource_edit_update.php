<?php
// Editing by 
######################################
#
#	2013-02-07 (Carlos): added data field Barcode
#	Date:	2012-07-05	YatWoon
#			update RecordStatus
#
#	Date:	2011-03-01	YatWoon
#			revised Display order coding
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

#added by Kelvin Ho 2008-12-19 (get the other category order if exists)
$sql = "select DisplayOrder from INVENTORY_FUNDING_SOURCE where FundingSourceID = $funding_id";
$tmpOrder = $linventory->returnVector($sql);
$orgOrder = $tmpOrder[0];

##### Display order issue [start]
if($orgOrder < $cat_display_order)
{
	$sql = "update INVENTORY_FUNDING_SOURCE set DisplayOrder=DisplayOrder-1 where DisplayOrder>$orgOrder and DisplayOrder<=$funding_display_order";
	$linventory->db_db_query($sql);
}
else if($orgOrder > $cat_display_order)
{
	$sql = "update INVENTORY_FUNDING_SOURCE set DisplayOrder=DisplayOrder+1 where DisplayOrder<$orgOrder and DisplayOrder>=$funding_display_order";
	$linventory->db_db_query($sql);
}
##### Display order issue [end]
        
$sql = "UPDATE
				INVENTORY_FUNDING_SOURCE
		SET
				Code = '$FundCode', 
				FundingType = '$target_funding_type',
				NameChi = '$funding_chi_name', 
				NameEng = '$funding_eng_name',
				Barcode = '$Barcode',
				DisplayOrder = $funding_display_order,
				RecordStatus = $recordstatus,
				DateModified = NOW()
		WHERE
				FundingSourceID = $funding_id";

$result = $linventory->db_db_query($sql);

/*
#added by Kelvin Ho 2008-12-19 (modify the other category order if exists)
$sql = "select FundingSourceID from INVENTORY_FUNDING_SOURCE where DisplayOrder = '$funding_display_order' and FundingSourceID!=$funding_id";
$tmpOrder = $linventory->returnVector($sql);
$order = $tmpOrder[0];
if($order)
{
	$sql = "update INVENTORY_FUNDING_SOURCE set DisplayOrder = '$orgOrder' where FundingSourceID = ".$order;
        $linventory->db_db_query($sql);
}
*/

##### Display order issue [Start]
# re-order
$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE order by DisplayOrder";
$result = $linventory->returnVector($sql);
for($i=0;$i<sizeof($result);$i++)
{
	$sql="update INVENTORY_FUNDING_SOURCE set DisplayOrder=$i+1 where FundingSourceID=". $result[$i];
	$linventory->db_db_query($sql);
}
##### Display order issue [End]
		
header("location: fundingsource_setting.php?xmsg=UpdateSuccess");
intranet_closedb();
?>