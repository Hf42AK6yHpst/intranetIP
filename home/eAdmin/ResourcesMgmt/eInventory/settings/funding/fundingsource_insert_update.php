<?php
// Editing by 
######################################
#	2013-02-07 (Carlos): added data field Barcode
#
#	Date:	2012-07-05	YatWoon
#			add RecordStatus
#
#	Date:	2011-03-01	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

##### Display order issue [Start]
$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE DisplayOrder = $funding_display_order";
$result = $linventory->returnVector($sql);
$existingFundingSourceID = $result[0];
if($existingFundingSourceID != "")
{
	$sql = "update INVENTORY_FUNDING_SOURCE set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$funding_display_order";
	$linventory->db_db_query($sql);
}
##### Display order issue [End]

$sql = "INSERT INTO 
				INVENTORY_FUNDING_SOURCE (FundingType, Code, NameChi, NameEng, Barcode, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
		VALUES
				('$target_funding_type','$FundCode','$funding_chi_name','$funding_eng_name','$Barcode',$funding_display_order,'',$recordstatus,NOW(),NOW())";


if($linventory->db_db_query($sql)) {
	
	
	##### Display order issue [Start]
	# re-order
	$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE order by DisplayOrder";
	$result = $linventory->returnVector($sql);
	for($i=0;$i<sizeof($result);$i++)
	{
		$sql="update INVENTORY_FUNDING_SOURCE set DisplayOrder=$i+1 where FundingSourceID=". $result[$i];
		$linventory->db_db_query($sql) or die(mysql_error());
	}
	##### Display order issue [End]

	header("location: fundingsource_setting.php?xmsg=AddSuccess");
}
else
{
	header("location: fundingsource_setting.php?xmsg=AddUnsuccess");
}

intranet_closedb();
?>