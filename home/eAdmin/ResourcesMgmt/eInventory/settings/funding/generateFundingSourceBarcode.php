<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();
$barcodeAry = $linventory->getUniqueBarcode(1,"funding");
echo implode(",",$barcodeAry);

intranet_closedb();
?>