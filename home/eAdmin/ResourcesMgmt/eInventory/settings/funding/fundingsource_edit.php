<?php
# using: 

#############################################
#	2016-02-04 (Henry): php 5.4 issue move "if(!isset($funding_id)){}" after includes file 
#	2013-02-07 (Carlos): added data field Barcode
#
#	Date:	2017-04-10 	Cameron
#			Fix: apply addslashes to checkFundEngName() to escape '"\ to avoid javascript error
#				- change cancel action from history.back() to window.location = 'fundingsource_setting.php' to suppress wrong message
# 
#	Date:	2012-07-05	YatWoon
#			Improved: IP25 standard layout
#			Improved: add "Status"
#
#	Date:	2011-07-21	YatWoon
#			add "Sponsoring body" for funding type
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if(!isset($funding_id))
{
	header("location: fundingsource_setting.php");
	exit();
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_FundingSource";
$linterface 	= new interface_html();
$linventory	= new libinventory();

$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeFormat = $linventory->getBarcodeFormat();

### Category ###
// $temp[] = array($i_InventorySystem_Setting_EditFunding);
// $infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 
$PAGE_NAVIGATION[] = array($i_InventorySystem['FundingSource'],"fundingsource_setting.php");
$PAGE_NAVIGATION[] = array($button_edit);

$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// get all category's Codem, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng, Barcode FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID != $funding_id[0]";
$FundNameList = $linventory->returnArray($sql,4);

$sql = "SELECT 
				Code,
				FundingType,
				NameChi, 
				NameEng, 
				Barcode,
				DisplayOrder,
				RecordStatus
		FROM 
				INVENTORY_FUNDING_SOURCE 
		WHERE 
				FundingSourceID = $funding_id[0]";

$result = $linventory->returnArray($sql,5);

$js_existing_barcode_arr = "var jsBarcodeArr = [];\n";
for($i=0;$i<sizeof($FundNameList);$i++) {
	$fund_barcode = trim($FundNameList[$i]['Barcode']);
	if($fund_barcode != "") {
		$js_existing_barcode_arr .= "jsBarcodeArr.push('".$fund_barcode."');\n";
	}
}

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($funding_code, $funding_type, $funding_chi_name, $funding_eng_name, $barcode, $funding_display_order, $recordstatus) = $result[$i];
		
		$sql = "SELECT COUNT(*) FROM INVENTORY_FUNDING_SOURCE";
		$total_rec = $linventory->returnVector($sql);
		$display_order .= "<select name=\"funding_display_order\">";
		for($j=0; $j<$total_rec[0]; $j++)
		{
			$order=$j+1;
			if($funding_display_order==$order)
			{
				$selected = "SELECTED=\"selected\"";
			}
			else
			{
				$selected = "";	
			}
			$display_order .= "<option value=\"$order\" $selected>$order</option>";
		}
		$display_order .= "</select>";
		
		$funding_type_selection = getSelectByArray($Lang['eInventory']['FundingTypeAry'],"name=\"target_funding_type\"",$funding_type,0,1);
		
		$table_content .= "<tr><td class='field_title'>$i_InventorySystem_Funding_Code</td><td class=\"tabletext\" valign=\"top\"><input name=\"FundCode\" type=\"text\" class=\"textboxnum\" maxlength=\"10\" value='".intranet_htmlspecialchars($funding_code)."'></td></tr>\n";
		$table_content .= "<tr><td class='field_title'>".$i_InventorySystem_Setting_Funding_ChineseName."</td><td class=\"tabletext\" valign=\"top\"><input name=\"funding_chi_name\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($funding_chi_name)."'></td></tr>\n";
		$table_content .= "<tr><td class='field_title'>".$i_InventorySystem_Setting_Funding_EnglishName."</td><td class=\"tabletext\" valign=\"top\"><input name=\"funding_eng_name\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($funding_eng_name)."'></td></tr>\n";
		$table_content .= "<tr><td class='field_title'>".$i_InventorySystem_Funding_Type."</td><td class=\"tabletext\" valign=\"top\">$funding_type_selection</td></tr>\n";
		$table_content .= "<tr><td class='field_title'>".$i_InventorySystem_Item_Barcode."</td><td class=\"tabletext\" valign=\"top\"><input name=\"Barcode\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($barcode)."' size=\"200\" maxlength=\"100\">";
		$table_content .= $linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenBarcode();", "", "")."</td></tr>\n";
		
		$arr_Status = array(array("1",$Lang['eInventory']['InUse'] ),array("-1",$Lang['eInventory']['NotInUse'] ));
		$table_content .= "<tr><td class='field_title'>". $Lang['eInventory']['Status']."</td>
						<td>".getSelectByArray($arr_Status,"name='recordstatus'",$recordstatus,0,1)."</td></tr>";
						
		$table_content .= "<tr><td class='field_title'>".$i_InventorySystem_Category_DisplayOrder."</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>\n";
		$table_content .= "<input name=\"funding_id\" type=\"hidden\" value=\"$funding_id[0]\">\n";
	}
// 	$table_content .= "<tr><td colspan=2 align=center>".
// 						$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
// 						$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
// 						$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
// 					 ."</td></tr>";
	
	$buttons = "
			<div class=\"edit_bottom_v30\">
			<p class=\"spacer\"></p>".
				$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
				$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='fundingsource_setting.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
			."<p class=\"spacer\"></p>
			</div> 
	";				 
					 
}
?>
<script language="javascript">
<?=$js_existing_barcode_arr?>

function checkFundCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.FundCode.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_Funding_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($FundNameList); $i++) {
	?>
			if (obj.FundCode.value == "<?=$FundNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_Funding_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkFundChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($FundNameList); $i++) {
	?>
			if (obj.funding_chi_name.value == "<?=addslashes($FundNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_Funding_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkFundEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($FundNameList); $i++) {
	?>
			if (obj.funding_eng_name.value == "<?=addslashes($FundNameList[$i]['NameEng'])?>") {
					alert("<?=$i_InventorySystem_Input_Funding_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkBarcodeFormat(barcode)
{
	var barcode_max_length = <?=$BarcodeMaxLength;?>;
	var barcode_format = <?=$BarcodeFormat;?>;
	var barcode_checking = 0;
	
	if(barcode_format == 1)
	{
		var ValidChars = new RegExp("[0-9\ \.\/\|\$\-]");
		var tmp_length = barcode.length;
		var IsNumber=true;
		var Char;
		if(barcode != "")
		{
			if(tmp_length > barcode_max_length)
			{
				IsNumber=false;
			}
			for(i=0; i<tmp_length; i++)
			{
				if(ValidChars.test(Char = barcode.charAt(i)) == false)
				{
					IsNumber=false;
				}
			}
		}
		else
		{
			IsNumber = false;
		}
		if(IsNumber == true)
		{
			barcode_checking++;
		}
		else
		{
			barcode_checking--;
		}
		
	}
   
	if(barcode_format == 2)
	{
		var ValidChars = new RegExp("[0-9A-Z\ \.\/\|\$\-]");
		var tmp_length = barcode.length;
		var IsNumber = true;
		var Char;
		if(barcode != "")
		{
			if(tmp_length > barcode_max_length)
			{
				IsNumber=false;
			}
			for(i=0; i<tmp_length; i++)
			{
				if(ValidChars.test(Char = barcode.charAt(i)) == false)
				{
					IsNumber=false;
				}
			}
		}
		else
		{
			IsNumber = false;
		}
		if(IsNumber == true)
		{
			barcode_checking++;
		}
		else
		{
			barcode_checking--;
		}
	}
	return barcode_checking > 0;
}

function checkBarcode()
{
	var obj = document.form1;
	var barcode = obj.Barcode.value.Trim();
	if(barcode == "") {
		alert('<?=$i_InventorySystem_Input_Item_Barcode_Warning?>');
		return false;
	}
	
	if(!checkBarcodeFormat(barcode)){
		alert('<?=$Lang['SysMgr']['Location']['Warning']['BarcodeNotValid']?>');
		return false;
	}
	
	if(jIN_ARRAY(jsBarcodeArr,barcode)) {
		alert('<?=$i_InventorySystem_Input_Item_Barcode_Exist_Warning?>');
		return false;
	}
	
	return true;
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;
	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
		
	if(check_text(obj.FundCode,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
		if(checkFundCode()) {
			if(check_text(obj.funding_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
				if(checkFundChiName()) {
					if(check_text(obj.funding_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						if(checkFundEngName()) {
								if(checkBarcode()) {
									passed = 1;	
								}else{
									passed = 0;
									error_cnt = 4;
								}
						}
						else {
								error_cnt = 3;
								passed = 0;
						}
					}
					else {
							passed = 0;
					}
				}
				else {
						error_cnt = 2;
						passed = 0;
				}
			}
			else {
					passed = 0;
			}
		}
		else {
				error_cnt = 1;
				passed = 0;
		}
	}
	else {
			passed = 0;
	}
				
	if(passed == 1)
	{
		obj.action = "fundingsource_edit_update.php";
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.FundCode.focus();
		if(error_cnt == 2)
			obj.funding_chi_name.focus();
		if(error_cnt == 3)
			obj.funding_eng_name.focus();
		if(error_cnt == 4)
			obj.Barcode.focus();
			
		obj.action = "";
		return false;
	}
}

function GenBarcode()
{
	$.get(
		'generateFundingSourceBarcode.php',
		{},
		function(data){
			$('input[name="Barcode"]').val(data);
		}
	);
}
</script>

<div class="navigation">
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<p class="spacer"></p>
</div>

<form name="form1" action="" method="POST" onSubmit="return checkForm();">
<table class="form_table_v30">
<?=$table_content?>
</table>

<?=$buttons?>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>