<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$successAry 	= array();

$sql = "SELECT 
				AdminGroupCode, 
				UserLogin, 
				RecordType
		FROM 
				TEMP_INVENTORY_ADMIN_GROUP_MEMBER
		";
$arr_result = $linventory->returnArray($sql,3);

if(sizeof($arr_result)>0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($admin_group_code, $user_login, $user_type) = $arr_result[$i];
		
		$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Code = '$admin_group_code'";
		$arr_admin_group_id = $linventory->returnVector($sql);
		if(sizeof($arr_admin_group_id)>0)
			$admin_group_id = $arr_admin_group_id[0];
			
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$user_login'";
		$arr_user_id = $linventory->returnVector($sql);
		if(sizeof($arr_user_id)>0)
			$user_id = $arr_user_id[0];
			
			
		# check insert or update
		$sql = "select count(*) from INVENTORY_ADMIN_GROUP_MEMBER where UserID=$user_id and AdminGroupID=$admin_group_id";
		$result = $linventory->returnVector($sql);
		if($result[0])	# update
		{
			$sql = "UPDATE INVENTORY_ADMIN_GROUP_MEMBER set
					RecordType = $user_type,
					DateModified = now()
					where
					UserID=$user_id and AdminGroupID=$admin_group_id";
		}
		else			# insert
		{
			$values = "'$user_id', '$admin_group_id', '$user_type', '', NOW(), NOW()";
		
			$sql = "INSERT INTO INVENTORY_ADMIN_GROUP_MEMBER
						(UserID, 
						AdminGroupID, 
						RecordType, 
						RecordStatus, 
						DateInput, 
						DateModified)
				VALUES
						($values)";
		}
		$successAry['NewInvItem'.$i] = $linventory->db_db_query($sql);
	}
}

if (in_array(false,$successAry)) {
	header("location: group_member_import.php?group_id=$group_id&msg=12");
}
else {
	header("location: group_member_import.php?group_id=$group_id&msg=1");
}
intranet_closedb();
?>