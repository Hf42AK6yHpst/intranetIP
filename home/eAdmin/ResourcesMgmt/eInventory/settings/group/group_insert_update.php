<?php
# using: 

######################################
#
#	2013-02-06 (Carlos): added data field Barcode
#	Date:	2011-03-01	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();

//$TAGS_OBJ[] = array($i_InventorySystem['Group'], "", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

##### Display order issue [Start]
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE DisplayOrder = $group_display_order";
	$result = $linventory->returnVector($sql);
	$existingAdminGroupID = $result[0];
	if($existingAdminGroupID != "")
	{
		$sql = "update INVENTORY_ADMIN_GROUP set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$group_display_order";
		$linventory->db_db_query($sql);
	
	}
	##### Display order issue [End]
	
if($link_intranet_group != "")
{	
	$sql = "INSERT INTO INVENTORY_ADMIN_GROUP 
					(Code, NameChi, NameEng, IntranetGroupID, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
			VALUES
					('$GroupCode', '$group_chi_name', '$group_eng_name', $link_intranet_group, $group_display_order, '', '', NOW(), NOW())";
	
	$InsertResult['AdminGroupInsert'] = $linventory->db_db_query($sql);
	
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE NameChi = '$group_chi_name' AND NameEng = '$group_eng_name'";
	$result = $linventory->returnArray($sql,1);
	
	if(sizeof($result)>0)
	{
		list($admin_group_id) = $result[0];
	}
	
	$namefield = getNameFieldWithClassNumberByLang("c.");

	$sql = "SELECT 
					c.UserID,
					$namefield 
			FROM 
					INTRANET_GROUP AS a INNER JOIN 
					INTRANET_USERGROUP AS b ON (a.GroupID = $link_intranet_group AND a.GroupID = b.GroupID) INNER JOIN
					INTRANET_USER AS c ON (b.UserID = c.UserID)
			WHERE
					a.AcademicYearID = ".$_SESSION['CurrentSchoolYearID']." AND 
					c.RecordType = 1 AND 
					c.RecordStatus IN (1)";

	$user_list = $linventory->returnArray($sql,2);
	
	if(sizeof($user_list)>0)
	{
		for($i=0; $i<sizeof($user_list); $i++)
		{
			list($u_id, $u_name) = $user_list[$i];
			
			$sql = "INSERT INTO INVENTORY_ADMIN_GROUP_MEMBER 
								(UserID, AdminGroupID, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
								($u_id, $admin_group_id, '2', '', NOW(), NOW())";

			//$InsertResult['AdminMemberInsert'&i] = $linventory->db_db_query($sql);
			$InsertResult['AdminMemberInsert'.$i] = $linventory->db_db_query($sql);
		}
	}
}
else
{
	$sql = "INSERT INTO INVENTORY_ADMIN_GROUP 
					(Code, NameChi, NameEng, Barcode, IntranetGroupID, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
			VALUES
					('$GroupCode', '$group_chi_name', '$group_eng_name', '$Barcode' , '', $group_display_order, '', '', NOW(), NOW())";
	
	$InsertResult['AdminGroupInsert'] = $linventory->db_db_query($sql);
}
	

# re-order
$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP order by DisplayOrder";
$result = $linventory->returnVector($sql);
for($i=0;$i<sizeof($result);$i++)
{
	$sql="update INVENTORY_ADMIN_GROUP set DisplayOrder=$i+1 where AdminGroupID=". $result[$i];
	$linventory->db_db_query($sql) or die(mysql_error());
}

header("location: group_setting.php?msg=1");

/*
if(!in_array(false,$InsertResult))
{
	header("location: group_setting.php?msg=1");
}
else
{
	header("location: group_setting.php?msg=12");
}
*/

intranet_closedb();
?>