<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "StockList_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$member_id_list = implode(",",$group_member_id);

if($member_id_list != "")
{
	$sql = "DELETE FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE AdminGroupID = $admin_group_id AND UserID IN ($member_id_list)";
	$result = $linventory->db_db_query($sql);
	
	if($result)
	{
		header("location: group_member_setting.php?group_id=$admin_group_id&msg=3");
	}
	else
	{
		header("location: group_member_setting.php?group_id=$admin_group_id&msg=13");
	}
}
else
{
	header("location: group_member_setting.php?group_id=$admin_group_id&msg=3");
}
intranet_closedb();
?>