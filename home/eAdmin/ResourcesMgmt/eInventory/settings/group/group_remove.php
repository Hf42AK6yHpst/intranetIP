<?php
# using: yat

######################################
#
#	Date:	2011-03-01	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_BasicSettings";
$linventory		= new libinventory();

$group_list = implode(",",$group_id);

if($group_list != "")
{
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge IN ($group_list)";
	$arr_result_single = $linventory->returnArray($sql,1);
	
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_BULK_LOG WHERE GroupInCharge IN ($group_list)";
	$arr_result_bulk = $linventory->returnArray($sql,1);
	
	if((sizeof($arr_result_single)>0) || (sizeof($arr_result_bulk)>0))
	{
		header("location: group_setting.php?msg=13");
	}
	else
	{
		$sql = "DELETE FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE AdminGroupID IN ($group_list)";
		$result_1 = $linventory->db_db_query($sql);
		
		$sql = "DELETE FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID IN ($group_list)";
		$result_2 = $linventory->db_db_query($sql);
		##### Display order issue [Start]
		# re-order
		$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP order by DisplayOrder";
		$result = $linventory->returnVector($sql);
		for($i=0;$i<sizeof($result);$i++)
		{
			$sql="update INVENTORY_ADMIN_GROUP set DisplayOrder=$i+1 where AdminGroupID=". $result[$i];
			$linventory->db_db_query($sql);
		}
		##### Display order issue [End]
		
		if($result_1 && $result_2)
		{
			header("location: group_setting.php?msg=3");
		}
		else
		{
			header("location: group_setting.php?msg=13");
		}
	}
}
else
{
	header("location: group_setting.php?msg=13");
}
intranet_closedb();
?>