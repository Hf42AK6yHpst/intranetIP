<?php
// Editing by 
/*
 * 	2015-05-05 [Cameron]
 * 				- change to use standard listview table so that user can sort by clicking column title
 * 				ref case: 2015-0430-1649-27214
 * 
 * 2013-02-06 (Carlos): Add display column Barcode
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

	
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}


if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;


$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Group";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Title ###
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_ResourceManagementGroup_DeleteFail");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

//$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('group_insert.php')","","","","",0)." ";
//$toolbar .= $linterface->GET_LNK_IMPORT("group_import.php","","","","",0);
$toolbar = '';
$toolbar .= '<div class="content_top_tool">'."\n";
	$toolbar .= '<div class="Conntent_tool">'."\n";
		$toolbar .= $linterface->Get_Content_Tool_v30('new', "javascript:checkNew('group_insert.php');");
		$importOptionArr = array();
		$importOptionArr[] = array('group_import.php', $i_InventorySystem['Caretaker']);
		$importOptionArr[] = array('group_member_import.php', $i_InventorySystem_Group_Member);
		$toolbar .= $linterface->Get_Content_Tool_v30('import', "javascript:void(0);", $text="", $importOptionArr);		
	$toolbar .= '</div>'."\n";
$toolbar .= '</div>'."\n";
	
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'group_id[]','group_remove.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'group_id[]','group_edit.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
					
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";


//$sql = "SELECT AdminGroupID, Code, $namefield as Name, IntranetGroupID, Barcode FROM INVENTORY_ADMIN_GROUP";
$sql = "SELECT 
			Code, 
			CONCAT('<a class=\"tablelink\" href=\"group_member_setting.php?group_id=',AdminGroupID,'\">',$namefield,'</a>'), 
			BarCode,
			CONCAT('<input type=\"checkbox\" name=\"group_id[]\" value=\"',AdminGroupID,'\">'),
			$namefield as GroupName 
		FROM 
			INVENTORY_ADMIN_GROUP";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo, true);
$li->field_array = array("Code","GroupName","BarCode");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";

$li->wrap_array = array();
$li->IsColOff = "2";


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Caretaker_Code)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Group_Name)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Barcode)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("group_id[]")."</td>\n";

?>

<br>
<form name="form1" action="" method="post">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar ?></td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<?= $li->display("96%")?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="current_pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="clearCoo" id="clearCoo" value="" />

</form>
</br>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>