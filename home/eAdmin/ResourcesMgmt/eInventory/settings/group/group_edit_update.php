<?php
// Editing by 
######################################
#
#	2013-02-06 (Carlos): add data field Barcode
#	Date:	2011-03-01	YatWoon
#			revised Display order coding
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

#added by Kelvin Ho 2008-12-19 (get the other category order if exists)
$sql = "select DisplayOrder from INVENTORY_ADMIN_GROUP where AdminGroupID = $group_id";
$tmpOrder = $linventory->returnVector($sql);
$orgOrder = $tmpOrder[0];

##### Display order issue [start]
if($orgOrder < $group_display_order)
{
	$sql = "update INVENTORY_ADMIN_GROUP set DisplayOrder=DisplayOrder-1 where DisplayOrder>$orgOrder and DisplayOrder<=$group_display_order";
	$linventory->db_db_query($sql);
}
else if($orgOrder > $cat_display_order)
{
	$sql = "update INVENTORY_ADMIN_GROUP set DisplayOrder=DisplayOrder+1 where DisplayOrder<$orgOrder and DisplayOrder>=$group_display_order";
	$linventory->db_db_query($sql);
}
##### Display order issue [end]
        
$sql = "UPDATE 
				INVENTORY_ADMIN_GROUP 
		SET
				Code = '$GroupCode', 
				NameChi = '$group_chi_name',
				NameEng = '$group_eng_name',
				Barcode = '$Barcode',
				DisplayOrder = '$group_display_order'				
		WHERE
				AdminGroupID = $group_id";
$result = $linventory->db_db_query($sql);
/*
#added by Kelvin Ho 2008-12-19 (modify the other category order if exists)
$sql = "select AdminGroupID from INVENTORY_ADMIN_GROUP where DisplayOrder = '$group_display_order' and AdminGroupID!=$group_id";
$tmpOrder = $linventory->returnVector($sql);
$order = $tmpOrder[0];
if($order)
{
	$sql = "update INVENTORY_ADMIN_GROUP set DisplayOrder = '$orgOrder' where AdminGroupID = ".$order;
        $linventory->db_db_query($sql);
}
*/

##### Display order issue [Start]
# re-order
$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP order by DisplayOrder";
$result = $linventory->returnVector($sql);
for($i=0;$i<sizeof($result);$i++)
{
	$sql="update INVENTORY_ADMIN_GROUP set DisplayOrder=$i+1 where AdminGroupID=". $result[$i];
	$linventory->db_db_query($sql);
}
##### Display order issue [End]
if($result)
{
	header("location: group_setting.php?msg=2");
}
else
{
	header("location: group_setting.php?msg=14");
}

intranet_closedb();
				
?>