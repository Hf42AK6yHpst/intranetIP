<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linventory		= new libinventory();

### Title ###
//$TAGS_OBJ[] = array($i_InventorySystem['Group'], "", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$sql = "INSERT INTO INVENTORY_ADMIN_GROUP_MEMBER 
					(UserID, AdminGroupID, RecordType, RecordStatus, DateInput, DateModified)
		VALUES
					($TargetUser, $AdminGroupID, $targetRole, '', NOW(), NOW())";

$linventory->db_db_query($sql);

if($linventory->db_affected_rows($sql)==1)
{
	header("location: group_member_setting.php?group_id=$AdminGroupID&msg=1");
}
else
{
	header("location: group_member_setting.php?group_id=$AdminGroupID&msg=12");
}

intranet_closedb();

?>
