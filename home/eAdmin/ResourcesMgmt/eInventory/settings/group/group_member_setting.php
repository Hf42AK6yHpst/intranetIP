<?php
//

/**
 * 2019-05-13 Henry - Security fix: SQL without quote
 * 2018-02-22 Henry add access right checking [Case#E135442]
 * 2012-10-11 Rita - add updation customization
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Settings_Group";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Category ###
$sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID = '".$group_id."'";
$arr_group_name = $linventory->returnVector($sql);
$temp[] = array(
    "<a href='group_setting.php'>" . $i_InventorySystem['Caretaker'] . "</a> > " . intranet_htmlspecialchars($arr_group_name[0]) . " > " . $i_InventorySystem_Group_Member
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

// ## Title ###
$TAGS_OBJ[] = array(
    $i_InventorySystem['Caretaker'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($xmsg != "") {
    $SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
    if ($msg == 1)
        $SysMsg = $linterface->GET_SYS_MSG("add");
    if ($msg == 2)
        $SysMsg = $linterface->GET_SYS_MSG("update");
    if ($msg == 3)
        $SysMsg = $linterface->GET_SYS_MSG("delete");
    if ($msg == 12)
        $SysMsg = $linterface->GET_SYS_MSG("add_failed");
    if ($msg == 13)
        $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
    if ($msg == 14)
        $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('group_member_insert.php?admin_group_id=$group_id')", "", "", "", "", 0) . " ";
$toolbar .= $linterface->GET_LNK_IMPORT("group_member_import.php?group_id=$group_id", "", "", "", "", 0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'group_member_id[]','group_member_remove.php?admin_group_id=$group_id')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:updateGroupLeader(document.form1,'group_member_id[]','group_leader_update.php?type=1')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_star_on.gif\" width=\"15\" height=\"13\" border=\"0\" align=\"absmiddle\">
						$i_InventorySystem_SetAsGroupLeader
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

if ($linventory->enableMemberUpdateLocationRight()) {
    
    $table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:updateGroupLeader(document.form1,'group_member_id[]','group_leader_update.php?type=3')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_star_setting_off.gif\" width=\"15\" height=\"13\" border=\"0\" align=\"absmiddle\"> " . $Lang['Inventoty']['Settigs']['SetAsHelper'] . "</a>
				</td>";
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
}

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:updateGroupLeader(document.form1,'group_member_id[]','group_leader_update.php?type=2')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_star_off.gif\" width=\"15\" height=\"13\" border=\"0\" align=\"absmiddle\">
						$i_InventorySystem_SetAsGroupMember
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

$table_content = "<tr class=\"tabletop\">";
$table_content .= "<td class=\"tabletoplink\" width=\"1\">#</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"65%\">$i_InventorySystem_Group_MemberName</td>";
// $table_content .= "<td class=\"tabletoplink\" width=\"15%\" align=\"right\">$i_InventorySystem_Resource_Management_GroupLeader<input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'group_leader[]'):setChecked(0,this.form,'group_leader[]')\"></td>";
$table_content .= "<td class=\"tabletoplink\" width=\"30%\" align=\"right\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'group_member_id[]'):setChecked(0,this.form,'group_member_id[]')\"></td>";
$table_content .= "</tr>";

$namefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT 
				a.UserID, 
				a.AdminGroupID, 
				$namefield,
				a.RecordType, 
				CASE a.RecordType
					WHEN " . MANAGEMENT_GROUP_ADMIN . " THEN 1
					WHEN " . MANAGEMENT_GROUP_HELPER . " THEN 2
					WHEN " . MANAGEMENT_GROUP_MEMBER . " THEN 3
				END AS DisplayOrder
		FROM 
				INVENTORY_ADMIN_GROUP_MEMBER AS a LEFT OUTER JOIN
				INTRANET_USER AS b ON (a.UserID = b.UserID)
		WHERE 
				a.AdminGroupID = $group_id AND
				a.RecordType IN (" . MANAGEMENT_GROUP_ADMIN . ", " . MANAGEMENT_GROUP_MEMBER . ", " . MANAGEMENT_GROUP_HELPER . ") AND
				b.RecordStatus IN (0,1,2)
		ORDER BY
				DisplayOrder, b.ClassName, b.ClassNumber, b.EnglishName";

$result = $linventory->returnArray($sql, 3);

if (sizeof($result) > 0) {
    for ($i = 0; $i < sizeof($result); $i ++) {
        $j ++;
        list ($user_id, $admin_group_id, $user_name, $user_group_role) = $result[$i];
        $css = $i % 2 == 0 ? "tablerow1 tabletext" : "tablerow2 tabletext";
        
        if ($user_group_role == MANAGEMENT_GROUP_ADMIN) {
            $checked = " CHECKED ";
            $user_name = "<font color=\"red\">$user_name</font>&nbsp;<img src=\"$image_path/$LAYOUT_SKIN/icon_star_on.gif\" width=\"15\" height=\"13\">";
        } elseif ($user_group_role == MANAGEMENT_GROUP_HELPER) {
            if ($linventory->enableMemberUpdateLocationRight()) {
                $user_name = "$user_name &nbsp;<img src=\"$image_path/$LAYOUT_SKIN/icon_star_setting_off.gif \" width=\"15\" height=\"13\">";
            }
        } else {
            $checked = " ";
        }
        
        $table_content .= "<tr class=\"$css\">\n";
        $table_content .= "<td class=\"tabletext\">$j</td>\n";
        $table_content .= "<td class=\"tabletext\">$user_name</td>\n";
        $table_content .= "<td class=\"tabletext\" align=\"right\"><input type=\"checkbox\" name=\"group_member_id[]\" value=\"$user_id\"></td>\n";
        $table_content .= "</tr>\n";
        $table_content .= "<input type=\"hidden\" name=\"group_id\" value=$admin_group_id>";
    }
} else {
    $table_content .= "<tr class=\"tablerow2 tabletext\">\n";
    $table_content .= "<td class=\"tabletext\" colspan=\"4\" align=\"center\">$i_no_record_exists_msg</td>\n";
    $table_content .= "</tr>\n";
}
$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"4\"></td></tr>";
$table_content .= "<tr><td class=\"tabletext\" colspan=\"4\" align=\"left\"><font color=\"red\">$i_InventorySystem_Group_Member_LeaderRemark</font>&nbsp;<img src=\"$image_path/$LAYOUT_SKIN/icon_star_on.gif\" width=\"15\" height=\"13\"></td></tr>";

?>

<script language="javascript">
function updateGroupLeader(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
</script>

<br>
<form name="form1" action="" method="post">
	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar1 ?></td>
		</tr>
	</table>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar2 ?></td>
		</tr>
	</table>
	<br>
	<table width="90%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $SysMsg ?></td>
		</tr>
		<tr>
			<td colspan="2" align="left"><?= $toolbar ?></td>
		</tr>
		<tr class="table-action-bar">
			<td colspan="2" align="right">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
								<?=$table_tool?>
							</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
	<?=$table_content?>
</table>
</form>
</br>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

