<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_WriteOffReason";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Category ###
//$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['Settings']." > ".$i_InventorySystem['FundingSource']." > ".$button_add)."</td></tr>"; 
//$temp[] = array($i_InventorySystem['Settings']);
$temp[] = array($i_InventorySystem_Setting_NewWriteOffReason);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 
//$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['WriteOffReason']." > ".$button_add)."</td></tr>"; 

$TAGS_OBJ[] = array($i_InventorySystem['WriteOffReason'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$sql = "SELECT COUNT(*) FROM INVENTORY_WRITEOFF_REASON_TYPE";
$total_rec = $linventory->returnVector($sql);
$display_order .= "<select name=\"ReasonType_display_order\">";
for($i=0; $i<=$total_rec[0]; $i++)
{
	$j=$i+1;
	if($j==$total_rec[0])
		$selected = "SELECTED=\"selected\"";
	$display_order .= "<option value=\"$j\" $selected>$j</option>";
}
$display_order .= "</select>";

$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_InventorySystem_Setting_WriteOffReason_ChineseName."</td><td class=\"tabletext\" valign=\"top\"><input name=\"ReasonTypeChiName\" type=\"text\" class=\"textboxtext\" size=\"200\" maxlength=\"200\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_InventorySystem_Setting_WriteOffReason_EnglishName."</td><td class=\"tabletext\" valign=\"top\"><input name=\"ReasonTypeEngName\" type=\"text\" class=\"textboxtext\" size=\"200\" maxlength=\"200\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_InventorySystem_Category_DisplayOrder."</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>";

$table_content .= "<tr><td colspan=2 align=center>".
					$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
					$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
					$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
				 ."</td></tr>";

?>
<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	var passed = 0;
	
	if(check_text(obj.ReasonTypeChiName,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning?>"))
		if(check_text(obj.ReasonTypeEngName,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning?>"))
				passed = 1;
			
	if(passed == 1)
	{
		obj.action = "write_off_reason_insert_update.php";
		return true;
	}
	else
	{
		obj.action = "";
		//obj.submit();
		return false;
	}
	
}
</script>

<br>
<form name="form1" method="post" action="" onSubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>