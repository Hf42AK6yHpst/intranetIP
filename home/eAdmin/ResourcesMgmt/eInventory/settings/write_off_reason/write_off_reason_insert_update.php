<?php

######################################
#
#	Date:	2011-03-10	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

##### Display order issue [Start]
$sql = "SELECT ReasonTypeID FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE DisplayOrder = $ReasonType_display_order";
$result = $linventory->returnVector($sql);
$existingID = $result[0];
if($existingID != "")
{
	$sql = "update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$ReasonType_display_order";
	$linventory->db_db_query($sql);
}
##### Display order issue [End]

$sql = "INSERT INTO 
				INVENTORY_WRITEOFF_REASON_TYPE (ReasonTypeNameEng, ReasonTypeNameChi, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
		VALUES
				('$ReasonTypeEngName','$ReasonTypeChiName',$ReasonType_display_order,'','',NOW(),NOW())";


if($linventory->db_db_query($sql)) {
	
	##### Display order issue [Start]
	# re-order
	$sql = "SELECT ReasonTypeID FROM INVENTORY_WRITEOFF_REASON_TYPE order by DisplayOrder";
	$result = $linventory->returnVector($sql);
	for($i=0;$i<sizeof($result);$i++)
	{
		$sql="update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder=$i+1 where ReasonTypeID=". $result[$i];
		$linventory->db_db_query($sql) or die(mysql_error());
	}
	##### Display order issue [End]

	header("location: write_off_reason_setting.php?msg=1");
}
else
{
	header("location: write_off_reason_setting.php?msg=12");
}

intranet_closedb();
?>