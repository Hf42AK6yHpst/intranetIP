<?php
//kenneth
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();

//$TAGS_OBJ[] = array($i_InventorySystem['Location'], "", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();


$sql = "INSERT INTO INVENTORY_LOCATION 
				(LocationLevelID, Code, NameChi, NameEng, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
		VALUES
				($level_id, '$LocationCode','$location_chi_name', '$location_eng_name', $location_display_order, '', '', NOW(), NOW())";

if($linventory->db_db_query($sql))
	header("location: location_setting.php?level_id=".$level_id."&msg=1");
else
	header("location: location_setting.php?level_id=".$level_id."&msg=12");
	
intranet_closedb();
?>