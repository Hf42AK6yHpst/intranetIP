<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Settings_Location";
$linterface = new interface_html();
$linventory	= new libinventory();

$level_id_list = implode(",",$level_id);

if($level_id_list != "")
{
	$sql = "SELECT LocationID FROM INVENTORY_LOCATION WHERE LocationLevelID IN ($level_id_list)";
	$arr_location = $linventory->returnVector($sql);
	$location_id_list = implode(",",$arr_location);
	
	if($location_id_list == "")
	{
		$sql = "DELETE FROM INVENTORY_LOCATION WHERE LocationLevelID IN ($level_id_list)";
		$linventory->db_db_query($sql);
			
		$sql = "DELETE FROM INVENTORY_LOCATION_LEVEL WHERE LocationLevelID IN ($level_id_list)";
		$linventory->db_db_query($sql);
		
		header("location: location_level_setting.php?msg=3");
	}
	else
	{	
		$sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE LocationID IN ($location_id_list)";
		$arr_result_single = $linventory->returnArray($sql,1);
		
		$sql = "SELECT ItemID FROM INVENTORY_ITEM_BULK_LOG WHERE LocationID IN ($location_id_list)";
		$arr_result_bulk = $linventory->returnArray($sql,1);
		
		if((sizeof($arr_result_single)>0) || (sizeof($arr_result_bulk)>0))
		{
			header("location: location_level_setting.php?msg=13");
		}
		else
		{
			$sql = "DELETE FROM INVENTORY_LOCATION WHERE LocationLevelID IN ($level_id_list)";
			$result_1 = $linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_LOCATION_LEVEL WHERE LocationLevelID IN ($level_id_list)";
			$result_2 = $linventory->db_db_query($sql);
			
			if($result_1 && $result_2)
			{
				header("location: location_level_setting.php?msg=3");
			}
			else
			{
				header("location: location_level_setting.php?msg=13");
			}
		}
	}
}
else
{
	header("location: location_level_setting.php?msg=13");
}
intranet_closedb();
?>