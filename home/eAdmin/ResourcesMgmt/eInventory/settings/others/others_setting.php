<?php
// using: 

// ###########################################
//
// Date: 2019-07-25 Tommy
// add new item code format setting
//
// Date: 2018-02-26 Henry
// developing... !!!PLEASE DON'T COPY TO 149!!!
//
// Date: 2018-02-26 Henry
// add access right checking [Case#E135442]
//
// Date: 2013-12-13 Henry
// Improved: can disable the warranty expiry warning [Case#2012-0105-1032-11071]
//
// Date: 2013-01-28 YatWoon
// Improved: skip to check the item code format is already set or not [Case#2013-0125-1413-47156]
//
// Date: 2011-05-25 YatWoon
// update to IP25 standard layout
//
// ###########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Settings_Others";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$itemCodeFormat_setting = "";
$year_checked = "";
$group_checked = "";
$funding_checked = "";
$location_checked = "";

$TAGS_OBJ[] = array(
    $Lang['eInventory']['SystemProperties'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

// # Item code format setting
if ($sys_custom['eInventory_ItemCodeFormat_New']) {
    $ItemCodeFormat_Display = "";
    if ($linventory->checkItemCodeFormatSetting() == true) {
        // $itemCodeFormat_setting = " DISABLED ";
        
        $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
        
        if (sizeof($arr_itemCodeFormat) > 0) {
            for ($i = 0; $i < sizeof($arr_itemCodeFormat); $i ++) {
                if ($arr_itemCodeFormat[$i] == 1) {
                    $ItemCodeFormat_Display .= $i_InventorySystem_Setting_ItemCodeFormat_Year . "<br>";
                    $year_checked = " CHECKED ";
                }
                if ($arr_itemCodeFormat[$i] == 2) {
                    $ItemCodeFormat_Display .= $i_InventorySystem_Item_GroupCode . "<br>";
                    $group_checked = " CHECKED ";
                }
                if ($arr_itemCodeFormat[$i] == 3) {
                    $ItemCodeFormat_Display .= $i_InventorySystem_Item_FundingSourceCode . "<br>";
                    $funding_checked = " CHECKED ";
                }
                if ($arr_itemCodeFormat[$i] == 4) {
                    $ItemCodeFormat_Display .= $i_InventorySystem_Item_Location2Code . "<br>";
                    $location_checked = " CHECKED ";
                }
            }
        }
    } else {
        $ItemCodeFormat_Display = $linterface->GET_SYS_MSG("", $i_InventorySystem_Setting_ItemCodeFormatSetting_Warning);
    }
    
    $ItemCodeFormat_Selectioin = "
		<input type=\"checkbox\" name=\"ItemCodeFormat[]\" value=1 $year_checked $itemCodeFormat_setting	id='ItemCodeFormat1' > <label for='ItemCodeFormat1'>$i_InventorySystem_Setting_ItemCodeFormat_Year</label><br>
		<input type=\"checkbox\" name=\"ItemCodeFormat[]\" value=2 $group_checked $itemCodeFormat_setting	id='ItemCodeFormat2' > <label for='ItemCodeFormat2'>$i_InventorySystem_Item_GroupCode</label><br>
		<input type=\"checkbox\" name=\"ItemCodeFormat[]\" value=3 $funding_checked $itemCodeFormat_setting	id='ItemCodeFormat3' > <label for='ItemCodeFormat3'>$i_InventorySystem_Item_FundingSourceCode</label><br>
		<input type=\"checkbox\" name=\"ItemCodeFormat[]\" value=4 $location_checked $itemCodeFormat_setting id='ItemCodeFormat4' > <label for='ItemCodeFormat4'>$i_InventorySystem_Item_Location2Code</label>
	";
}

// # Item code format setting (new)
if($sys_custom['eInventory_SysPpt']){
    $ItemCodeFormat_Display_new = "";
//    if ($linventory->checkItemCodeFormatSetting() == true) {
        
        $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
        if(is_array($arr_itemCodeFormat)){
            $itemCodeGenVal = implode(',', $arr_itemCodeFormat);
        }
        //debug_pr($itemCodeFormat);
    
        for($i=0;$i<7;$i++){
            $x .= '<select name="ItemCodeFormat[]" class="ItemCodeFormat">';
            $x .= '    <option value="NotUsing">'.$Lang['eInventory']['System']['ItemCode']['format']['N/A'].'</option>';
            $x .= '    <option value="PurchaseYear">'. $i_InventorySystem_Setting_ItemCodeFormat_Year .'</option>';
            $x .= '    <option value="CategoryCode">'. $i_InventorySystem_Item_CategoryCode .'</option>';
            $x .= '    <option value="SubCategoryCode">'. $i_InventorySystem_SubCategory_Code .'</option>';
            $x .= '	   <option value="ResourceManagementGroupCode">'. $i_InventorySystem_Item_GroupCode .'</option>';
            $x .= '    <option value="FundingSource">'. $i_InventorySystem_Item_FundingSourceCode .'</option>';
            $x .= '    <option value="FundingSource2">'. $i_InventorySystem_Item_FundingSource2Code .'</option>';
            $x .= '    <option value="LocationCode">'. $i_InventorySystem_Item_Location2Code .'</option>';
            $x .= '    </select>';
        }
        $x .= '    <input type="hidden" name="itemCodeSettings[itemcode_setting]" id="itemCodeSettings[itemcode_setting]" class="txtItemCodeFormat" value="'.$itemCodeGenVal.'" ><br>';
        // $x .= $lPCM_ui->Get_Form_Warning_Msg('CaseCodeMissingWarnDiv', $Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code must include Sequence Number'], $Class='warnMsgDiv');
         $x .= '<span id="ItemCodeDuplicateWarnDiv"></span>';
        
        $GeneralSelected=$AcademicYearSelected=$ResourceGroupSelected=$FundingSourceSelected=$LocationSelected='';
        if(sizeof($arr_itemCodeFormat) > 0){
            for($i = 0; $i < sizeof($arr_itemCodeFormat); $i++){
                if($arr_itemCodeFormat[$i] == 'PurchaseYear'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_Setting_ItemCodeFormat_Year .")";
                }else if($arr_itemCodeFormat[$i] == 'CategoryCode'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_Item_CategoryCode. ")";
                }else if($arr_itemCodeFormat[$i] == 'SubCategoryCode'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_SubCategory_Code .")";
                }else if($arr_itemCodeFormat[$i] == 'ResourceManagementGroupCode'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_Item_GroupCode .")";
                }else if($arr_itemCodeFormat[$i] == 'FundingSource'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_Item_FundingSourceCode .")";
                }else if($arr_itemCodeFormat[$i] == 'FundingSource2'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_Item_Funding2Code .")";
                }else if($arr_itemCodeFormat[$i] == 'LocationCode'){
                    $ItemCodeFormat_Display_new .= "(". $i_InventorySystem_Item_Location2Code .")";
                }
            }
            
    //        $x .= '<select class="ItemCodeSequenceFormat" onchange="ItemCodeSequenceInit(this.value)">';
    //        $x .= '</select';
            //$x .= '<input type="hidden" name="itemCodeSettings[]" id="itemCodeSettings[]" class="txtItemCodeFormat" value="'.$itemCodeFormat.'" >';
        }else{
            $ItemCodeFormat_Display_new = $linterface->GET_SYS_MSG("", $i_InventorySystem_Setting_ItemCodeFormatSetting_Warning);
        }
}

// # Group Leader Add Item Right Setting
$groupLeaderAddItemRight = $linventory->retriveGroupLeaderAddItemRight();

// # Disallow Group Leader to write-off items
$groupLeaderNowAllowWriteOffItem = $linventory->retriveGroupLeaderNotAllowWriteOffItem();

// # Stock take period setting
$StocktakePeriodStart = $linventory->getStocktakePeriodStart();
$StocktakePeriodEnd = $linventory->getStocktakePeriodEnd();
$StocktakePeriodStart = ($StocktakePeriodStart == "") ? date('Y-m-d') : $StocktakePeriodStart;
$StocktakePeriodEnd = ($StocktakePeriodEnd == "") ? date('Y-m-d') : $StocktakePeriodEnd;

// # Warranty Expiry Reminder setting
$WarrantyExpiryWarning = $linventory->getWarrantyExpiryWarningDayPeriod();
for ($i = 0; $i <= 30; $i ++)
    $data[] = array(
        $i,
        $i
    );
$WarrantyExpiryWarning_selection = getSelectByArray($data, "name='warranty_expiry_warning'", trim($WarrantyExpiryWarning), 0, 1);

// # Barcode Max Length
$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeMaxLength = $BarcodeMaxLength ? $BarcodeMaxLength : 10;
$data = array();
for ($i = 1; $i <= 20; $i ++)
    $data[] = array(
        $i,
        $i
    );
$BarcodeMaxLength_selection = getSelectByArray($data, "name='barcode_max_length'", trim($BarcodeMaxLength), 0, 1);

// # Barcode Format Setting
$BarcodeFormat = $linventory->getBarcodeFormat();
$BarcodeFormat_Display = $BarcodeFormat == 2 ? $i_InventorySystem_Setting_BarcodeFormat_BothNumAndChar : $i_InventorySystem_Setting_BarcodeFormat_NumOnly;
$BarcodeFormat_Selection = "
	<input type=\"radio\" name=\"barcode_format\" value=1 " . ($BarcodeFormat != 2 ? "checked" : "") . " id='barcode_format1'> <label for='barcode_format1'>$i_InventorySystem_Setting_BarcodeFormat_NumOnly</label><br>
	<input type=\"radio\" name=\"barcode_format\" value=2 " . ($BarcodeFormat == 2 ? "checked" : "") . " id='barcode_format2'> <label for='barcode_format2'>$i_InventorySystem_Setting_BarcodeFormat_BothNumAndChar</label>
";

// # Item code digit Length
// $ItemCodeDigitLength = $linventory->getItemCodeDigitLength();
if(isset($sys_custom['eInventory_SysPpt'])){
    $length = $linventory->getItemCodeDigitLength();
    $ItemCodeDigitLength = $length ? $length : 5;
    $data = array();
    for ($i = 1; $i <= 10; $i ++)
        $data[] = array(
            $i,
            $i
        );
    $ItemCodeDigitLength_selection = getSelectByArray($data, "name='item_code_digit_length'", trim($ItemCodeDigitLength), 0, 1);
    
    // # Item code separator
    // $ItemCodeSeparator = $linventory->getItemCodeSeparator();
    $ItemCodeSeparator = $linventory->getItemCodeSeparator();
    $ItemCodeSeparator = $ItemCodeSeparator ? $ItemCodeSeparator : '';
    
    $data = array(
        array(
            0,
            $Lang['eInventory']['System']['Separtor']['NotUsing']
        ),
        array(
            1,
            '_'
        ),
        array(
            2,
            '/'
        ),
        array(
            3,
            '-'
        )
    );
    
    $separator = "";
    if($ItemCodeSeparator == 0){
        $separator = $Lang['eInventory']['System']['Separtor']['NotUsing'];
    }else if($ItemCodeSeparator == 1){
        $separator = '_';
    }else if($ItemCodeSeparator == 2){
        $separator = '/';
    }else if($ItemCodeSeparator == 3){
        $separator = '-';
    }
    
    $ItemCodeSeparator_selection = getSelectByArray($data, "name='item_code_separator'", trim($ItemCodeSeparator), 0, 1);
}

$StockTakePriceSingle = $linventory->getStockTakePriceSingle();
$StockTakePriceBulk = $linventory->getStockTakePriceBulk();
$PriceSettingSingle = "<input type='text' id='StockTakePriceSingle' name='StockTakePriceSingle' size=\"4\" value=\"" . $StockTakePriceSingle . "\" /> ";
$PriceSettingBulk = "<input type='text' id='StockTakePriceBulk' name='StockTakePriceBulk' size=\"4\" value=\"" . $StockTakePriceBulk . "\" /> ";
?>


<script language="javascript">
<!--
var canSubmit = true;

function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    alert(err);
	}

	formAction = $('#form1').attr('action');
	if(checkResult == true && formAction != '' ){
		$('#form1').submit();
	}
	else{
		canSubmit = true;
		return false;
	}
}

function checkForm(){
	// Item Code Settings
	
	$('#form1').attr('action', 'others_setting_update.php');
	
	var item_code_have_sequence_number=false;
	var item_code_is_not_duplicated=true;
	var item_code_array=new Array();
	$('.ItemCodeFormat').each(function(i){
		var value=$(this).val();
		if(value!='NotUsing'){
			if(!item_code_array[value]){
				item_code_array[value]=true;
			}else{
				item_code_is_not_duplicated=false;
			}
		}
	});
	
	if(item_code_is_not_duplicated){
		canSubmit = true;
	}else{
		if(!item_code_is_not_duplicated){
			document.getElementById('ItemCodeDuplicateWarnDiv').innerHTML = "<font color=red style=font-weight:bold;>* Item Code partials must not be duplicated</font>";
		}
		canSubmit=false;
	}
	
	return canSubmit;
}

$(function(){
	var display_value = '';
	if($('.txtItemCodeFormat').val()==''){
	}else{
		var array_format=$('.txtItemCodeFormat').val().split(',');
		$('.ItemCodeFormat').each(function(i){
			if(array_format[i]!=''){
				var value='NotUsing';
				var format=array_format[i];
				if(format=='PurchaseYear'){
					value='PurchaseYear';
				}else if(format=='CategoryCode'){
					value='CategoryCode';
				}else if(format=='SubCategoryCode'){
					value='SubCategoryCode';
				}else if(format=='ResourceManagementGroupCode'){
					value='ResourceManagementGroupCode';
				}else if(format=='FundingSource'){
					value='FundingSource';
				}else if(format=='FundingSource2'){
					value='FundingSource2';
				}else if(format=='LocationCode'){
					value='LocationCode';
				}
				$(this).val(value);
			}
		});
	}

});

//-->
</script>


<form name="form1" id="form1" method="get" action="others_setting_update.php">

	<div class="table_board">
		<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
		<p class="spacer"></p>
		<div class="form_sub_title_v30">
			<em>- <span class="field_title"> <?=$eDiscipline['SettingName']?> </span>-
			</em>
			<p class="spacer"></p>
		</div>

		<table class="form_table_v30">
	
	<? if($sys_custom['eInventory_ItemCodeFormat_New']) {?>
	<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem_Setting_ItemCodeFormat_Title?></td>
				<td><span class="Edit_Hide"><?=$ItemCodeFormat_Display?></span> <span
					class="Edit_Show" style="display: none">
		<?=$ItemCodeFormat_Selectioin?>
		</span></td>
			</tr>
	<? } ?>
	
			<? if($sys_custom['eInventory_SysPpt']){ ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem_Setting_ItemCodeFormat_Title?></td>
				<td><span class="Edit_Hide"><?=$ItemCodeFormat_Display_new?></span> <span
					class="Edit_Show" style="display: none">

<?echo $x; ?>
		</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?echo $Lang['eInventory']['System']['NumberDigits']?></td>
				<td><span class="Edit_Hide"><?=$ItemCodeDigitLength?></span> <span
					class="Edit_Show" style="display: none">
		<?=$ItemCodeDigitLength_selection?>
		</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?echo $Lang['eInventory']['System']['Separator']?></td>
				<td><span class="Edit_Hide"><?=$separator?></span> <span
					class="Edit_Show" style="display: none">
		<?=$ItemCodeSeparator_selection?>
		</span></td>
			</tr>
<? } ?>
	
	<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem_Setting_GroupLeader_AllowAddItem?></td>
				<td><span class="Edit_Hide"><?=$groupLeaderAddItemRight ? $i_general_yes:$i_general_no?></span>
					<span class="Edit_Show" style="display: none"> <input type="radio"
						name="Leader_AddItemRight" value="1"
						<?=$groupLeaderAddItemRight ? "checked":""?>
						id="groupLeaderAddItemRight1"> <label
						for="groupLeaderAddItemRight1"><?=$i_general_yes?></label> <input
						type="radio" name="Leader_AddItemRight" value="0"
						<?=$groupLeaderAddItemRight ? "":"checked"?>
						id="groupLeaderAddItemRight0"> <label
						for="groupLeaderAddItemRight0"><?=$i_general_no?></label>
				</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eInventory']['DoNotAllowGroupLeaderWriteOffItem']?></td>
				<td><span class="Edit_Hide"><?=$groupLeaderNowAllowWriteOffItem ? $i_general_yes:$i_general_no?></span>
					<span class="Edit_Show" style="display: none"> <input type="radio"
						name="groupLeaderNowAllowWriteOffItem" value="1"
						<?=$groupLeaderNowAllowWriteOffItem ? "checked":""?>
						id="groupLeaderNowAllowWriteOffItem1"> <label
						for="groupLeaderNowAllowWriteOffItem1"><?=$i_general_yes?></label>
						<input type="radio" name="groupLeaderNowAllowWriteOffItem"
						value="0" <?=$groupLeaderNowAllowWriteOffItem ? "":"checked"?>
						id="groupLeaderNowAllowWriteOffItem0"> <label
						for="groupLeaderNowAllowWriteOffItem0"><?=$i_general_no?></label>
				</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem_Setting_StockCheckPeriod?></td>
				<td><span class="Edit_Hide"><?=$StocktakePeriodStart?> <?=$Lang['General']['To']?> <?=$StocktakePeriodEnd?></span>
					<span class="Edit_Show" style="display: none">
		<?=$linterface->GET_DATE_PICKER("stocktake_period_start", $StocktakePeriodStart)?> <?=$Lang['General']['To']?> <?=$linterface->GET_DATE_PICKER("stocktake_period_end", $StocktakePeriodEnd)?>
		</span></td>
			</tr>


			<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem['StockTakeScope']?></td>
				<td><span class="Edit_Hide"><p>[<?=$i_InventorySystem_ItemType_Single?>]: <?= str_replace("[=PriceSingle=]", $StockTakePriceSingle, $i_InventorySystem['StockTakePriceSingle']) ?></p>
						<p>[<?=$i_InventorySystem_ItemType_Bulk?>]: <?= str_replace("[=PriceBulk=]", $StockTakePriceBulk, $i_InventorySystem['StockTakePriceBulk']) ?></p>
				</span> <span class="Edit_Show" style="display: none">
						<p>[<?=$i_InventorySystem_ItemType_Single?>]: <?= str_replace("[=PriceSingle=]", $PriceSettingSingle, $i_InventorySystem['StockTakePriceSingle']) ?></p>
						<p>[<?=$i_InventorySystem_ItemType_Bulk?>]: <?= str_replace("[=PriceBulk=]", $PriceSettingBulk, $i_InventorySystem['StockTakePriceBulk']) ?></p>
				</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem_Setting_WarrantyExpiryWarning?>: <?=$i_InventorySystem_Setting_WarrantyGiveWarning1?>...<?=$i_InventorySystem_Setting_WarrantyGiveWarning2?></td>
				<td><span class="Edit_Hide"><?=$WarrantyExpiryWarning?></span> <span
					class="Edit_Show" style="display: none">
		<?=$WarrantyExpiryWarning_selection?>
		<div class="tabletextremark"><?=$Lang['eInventory']['WarrantyExpiryWarningRemark']?></div>
				</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eInventory']['BarcodeMaxLength1']?></td>
				<td><span class="Edit_Hide"><?=$BarcodeMaxLength?> <?=$Lang['eInventory']['BarcodeMaxLength2']?></span>
					<span class="Edit_Show" style="display: none">
		<?=$BarcodeMaxLength_selection?> <?=$i_InventorySystem_Setting_BarcodeMaxLength2?>
		</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?=$i_InventorySystem_Setting_BarcodeFormat?></td>
				<td><span class="Edit_Hide"><?=$BarcodeFormat_Display?></span> <span
					class="Edit_Show" style="display: none">
		<?=$BarcodeFormat_Selection?>
		</span></td>
			</tr>
			
		</table>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "button","goSubmit();","UpdateBtn", "style='display: none'")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
		</div>
	</div>

</form>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>