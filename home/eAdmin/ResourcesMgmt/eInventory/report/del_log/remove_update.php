<?php

//Modifying by: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();
$lg = new liblog();

$x = $lg->DELETE_LOG($linventory->ModuleName, $StartDate, $EndDate);

intranet_closedb();

header("location: index.php?StartDate=$StartDate&EndDate=$EndDate&msg=DeleteSuccess");
?>