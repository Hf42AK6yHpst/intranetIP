<?php
// using: 

// ############################
// Date: 2019-09-02 (Tommy)
// add maximum width and height to school badge
//
// Date: 2019-06-24 (Tommy)
// add funding source print out 
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-02 Henry
// PHP 5.4 fix: change split to explode
//
// Date: 2013-02-19 YatWoon
// Fixed: Failed to display bulk item funding name [Case#2013-0131-1431-57073]
//
// Date: 2013-01-16 YatWoon
// Fixed: Display item's funding instead of write-off record's funding data [Case#:2013-0107-1503-41073]
//
// Date: 2012-07-10 YatWoon
// Enhanced: support 2 funding source for single item
//
// Date: 2012-05-29 [YatWoon]
// Improved: add funding column
//
// Date: 2011-11-30 [YatWoon]
// Improved: display deleted user name [Case#2011-1111-1515-17132]
//
// Date: 2011-11-07 YatWoon
// Improved: Add "Date Type" [Case#2011-1013-1705-11066]
//
// Date: 2011-06-15 YatWoon
// Improved: add sortby option [Case#2010-1214-1004-44096]
//
// Date: 2011-05-26 YatWoon
// Fixed: N attachment will display N records
//
// Date: 2011-04-26 YatWoon
// improved: display format
//
// ############################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Report_WrittenOffItems";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();

$school_data = explode("\n", get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root . "/file/schoolbadge.txt");
$badge_size = getimagesize($PATH_WRT_ROOT."/file/".$school_badge);
$badge_width = $badge_size[0];
$badge_height = $badge_siz[1];
$maxWidth = 120;
$maxHeight = 60;
if($badge_width > $maxWidth){
    $badge_width = $maxWidth;
}
if($badge_height > $maxHeight){
    $badge_height = $maxHeight;
}
if ($school_badge != "") {
    $badge_image = "<img src='/file/$school_badge' width='$badge_width'px height='$badge_height'px>";
}

$start_date = str_replace("'", "", stripslashes($start_date));
$end_date = str_replace("'", "", stripslashes($end_date));
$targetLocation = str_replace("'", "", stripslashes($targetLocation));
$targetGroup = str_replace("'", "", stripslashes($targetGroup));
$targetFundSource = str_replace("'", "", stripslashes($targetFundSource));

if ($start_date == "") {
    $start_date = $linventory->getStocktakePeriodStart();
}
if ($end_date == "") {
    $end_date = $linventory->getStocktakePeriodEnd();
}

if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND a.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND a.GroupInCharge = $targetGroup ";
}
if ($targetFundSource == "") {
    $cond3 = " ";
} else {
    $cond3 = " AND a.FundingSourceID = $targetFundSource ";
}

switch ($sortby) {
    case "ItemCode":
        $orderby = "b.ItemCode";
        break;
    case "ItemName":
        $orderby = "this_item_name";
        break;
    case "Location":
        $orderby = "building.DisplayOrder, floor.DisplayOrder, c.DisplayOrder";
        break;
    case "ResourceMgmtGroup":
        $orderby = "d.DisplayOrder";
        break;
    case "Qty":
        $orderby = "a.WriteOffQty";
        break;
    case "RequestDate":
        $orderby = "a.RequestDate";
        break;
    case "ApproveDate":
        $orderby = "a.ApproveDate";
        break;
}

if ($groupBy == 1) {
    $arr_tmp_location = explode(",", $selected_cb);
    
    if (sizeof($arr_tmp_location) > 0) {
        for ($a = 0; $a < sizeof($arr_tmp_location); $a ++) {
            $location_id = $arr_tmp_location[$a];
            
            $sql = "SELECT 
							CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("a.") . ")
					FROM
							INVENTORY_LOCATION AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID)
					WHERE
							a.LocationID = $location_id
					";
            $arr_tmp = $linventory->returnArray($sql, 1);
            
            if (sizeof($arr_tmp) > 0) {
                list ($location_name) = $arr_tmp[0];
            }
            
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }
            
            if ($target_admin_group != "") {
                $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
            }
            
            $RequestPerson = getNameFieldByLang2("f.");
            $ApprovePerson = getNameFieldByLang2("g.");
            
            $RequestArchivedPerson = getNameFieldByLang2("h.");
            $ApproveArchivedPerson = getNameFieldByLang2("i.");
            /*
             * $sql = "SELECT
             * b.ItemCode,
             * b.ItemType,
             * ".$linventory->getInventoryItemNameByLang("b.")." as this_item_name,
             * concat(".$linventory->getInventoryNameByLang("building.").",'>',".$linventory->getInventoryNameByLang("floor.").",'>',".$linventory->getInventoryNameByLang("c.")."),
             * ".$linventory->getInventoryNameByLang("d.").",
             * ".$linventory->getInventoryNameByLang("funding.").",
             * a.WriteOffQty,
             * a.RequestDate,
             * IFNULL($RequestPerson,
             * IF( $RequestArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('<font color=red>*</font> ', $RequestArchivedPerson))
             * ),
             * a.ApproveDate,
             * IFNULL($ApprovePerson,
             * IF( $ApproveArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('<font color=red>*</font> ', $ApproveArchivedPerson))
             * ),
             * a.WriteOffReason,
             * ".$linventory->getInventoryNameByLang("funding2.")."
             * FROM
             * INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
             * INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
             * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
             * INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
             * INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
             * INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
             * INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN
             * INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
             * left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
             * left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
             * left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
             * left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID or funding.FundingSourceID=s.FundingSource)
             * left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
             * WHERE
             * a.RecordStatus = 1
             * AND c.LocationID IN (".$location_id.")
             * AND (a.". $DateType ." BETWEEN '$start_date' AND '$end_date')
             * order by $orderby
             * ";
             */
            $sql = "SELECT 
							b.ItemCode, 
							b.ItemType, 
							" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
							concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
							" . $linventory->getInventoryNameByLang("d.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							a.WriteOffQty,
							a.RequestDate,
							IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
									),
							a.ApproveDate, 
							IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
									),
							a.WriteOffReason,
							" . $linventory->getInventoryNameByLang("funding2.") . ",
							a.ItemID, a.LocationID
					FROM
							INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
							INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
							INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
							INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
							left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
							left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
							left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
							left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
					WHERE
							a.RecordStatus = 1 
							AND c.LocationID IN (" . $location_id . ")
							AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
					order by $orderby
				";
            $arr_result = $linventory->returnArray($sql);
            
            // build data array
            $ary = array();
            foreach ($arr_result as $k => $d) {
                $ary[$d['ItemType']][] = $d;
            }
            
            $table_content .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">";
            $table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"12\">" . intranet_htmlspecialchars($location_name) . "</td></tr>";
            
            for ($k = 1; $k <= 2; $k ++) // 1 = single, 2 = bulk
{
                if ($k == 2) {
                    $table_content .= "<br>";
                    $table_content .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">";
                }
                $table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"11\">" . ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk) . "</td></tr>";
                
                $table_content .= "<tr>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Location</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['FundingSource'] . "</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffQty</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffReason_Name</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_RequestPerson</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Write_Off_RequestTime</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApprovePerson</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApproveDate</td>";
                $table_content .= "</tr>";
                
                $arr_result = $ary[$k];
                if (sizeof($arr_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_result); $i ++) {
                        list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                        
                        if ($k == 2) {
                            // retrieve bulk item latest funding source name
                            $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                            $result_bl = $linventory->returnVector($sql_bl);
                            $funding_name = $result_bl[0];
                        }
                        
                        $table_content .= "<tr>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($location_name) . "</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($group_name) . "</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_qty</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_reason</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_person</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_date</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_person</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_date</td>";
                        $table_content .= "</tr>";
                    }
                }
                if (sizeof($arr_result) == 0) {
                    $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"11\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                }
                $table_content .= "</table>";
            }
            
            $table_content .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . "<br><br>";
            
            // ## Principal Signature [CRM Ref No.: 2010-0211-1408]
            $table_content .= "<br>";
            $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
            $table_content .= "	<tr>
									<td align='right' width='30%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
									<td align='right' width='10%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
								</tr>";
            $table_content .= "</table>";
            $table_content .= "<br><br>";
        }
    }
} else 
    if ($groupBy == 2) {
        $arr_tmp_item_type = array(
            array(
                "1",
                $i_InventorySystem_ItemType_Single
            ),
            array(
                "2",
                $i_InventorySystem_ItemType_Bulk
            )
        );
        
        if (sizeof($arr_tmp_item_type) > 0) {
            for ($a = 0; $a < sizeof($arr_tmp_item_type); $a ++) {
                list ($item_type, $item_type_name) = $arr_tmp_item_type[$a];
                
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                }
                
                if ($target_admin_group != "") {
                    $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                }
                
                $RequestPerson = getNameFieldByLang2("f.");
                $ApprovePerson = getNameFieldByLang2("g.");
                
                $RequestArchivedPerson = getNameFieldByLang2("h.");
                $ApproveArchivedPerson = getNameFieldByLang2("i.");
                
                $sql = "SELECT 
							b.ItemCode, 
							b.ItemType, 
							" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
							concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
							" . $linventory->getInventoryNameByLang("d.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							a.WriteOffQty,
							a.RequestDate,
							IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
									),
							a.ApproveDate, 
							IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
									),
							a.WriteOffReason,
							" . $linventory->getInventoryNameByLang("funding2.") . ",
							a.ItemID, a.LocationID
					FROM
							INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
							INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
							INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
							INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
							left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
							left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
							left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
							left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
					WHERE
							a.RecordStatus = 1 AND
							b.ItemType = $item_type
							AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
					order by $orderby
					";
                
                $arr_result = $linventory->returnArray($sql, 13);
                
                $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">";
                $table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"12\">$item_type_name</td></tr>";
                $table_content .= "<tr>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Location</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['FundingSource'] . "</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffQty</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffReason_Name</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_RequestPerson</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Write_Off_RequestTime</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApprovePerson</td>";
                $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApproveDate</td>";
                $table_content .= "</tr>";
                
                if (sizeof($arr_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_result); $i ++) {
                        list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                        
                        if ($item_type == 2) {
                            // retrieve bulk item latest funding source name
                            $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                            $result_bl = $linventory->returnVector($sql_bl);
                            $funding_name = $result_bl[0];
                        }
                        $table_content .= "<tr>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($location_name) . "</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($group_name) . "</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_qty</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_reason</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_person</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_date</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_person</td>";
                        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_date</td>";
                        $table_content .= "</tr>";
                    }
                }
                if (sizeof($arr_result) == 0) {
                    $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"11\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                }
                $table_content .= "</table>";
                
                $table_content .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . "<br><br>";
                
                // ## Principal Signature [CRM Ref No.: 2010-0211-1408]
                $table_content .= "<br>";
                $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
                $table_content .= "	<tr>
									<td align='right' width='30%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
									<td align='right' width='10%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
								</tr>";
                $table_content .= "</table>";
                $table_content .= "<br><br>";
            }
        }
    } else 
        if ($groupBy == 3) {
            $arr_tmp_group = explode(",", $selected_cb);
            
            if (sizeof($arr_tmp_group) > 0) {
                for ($a = 0; $a < sizeof($arr_tmp_group); $a ++) {
                    $admin_group_id = $arr_tmp_group[$a];
                    
                    $sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID = '".$admin_group_id."'";
                    $arr_tmp_admin = $linventory->returnArray($sql, 1);
                    
                    if (sizeof($arr_tmp_admin) > 0) {
                        list ($admin_group_name) = $arr_tmp_admin[0];
                    }
                    
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                    }
                    
                    if ($target_admin_group != "") {
                        $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                    }
                    
                    $RequestPerson = getNameFieldByLang2("f.");
                    $ApprovePerson = getNameFieldByLang2("g.");
                    $RequestArchivedPerson = getNameFieldByLang2("h.");
                    $ApproveArchivedPerson = getNameFieldByLang2("i.");
                    $sql = "SELECT 
							b.ItemCode, 
							b.ItemType, 
							" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
							concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
							" . $linventory->getInventoryNameByLang("d.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							a.WriteOffQty,
							a.RequestDate,
							IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
									),
							a.ApproveDate, 
							IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
									),
							a.WriteOffReason,
							" . $linventory->getInventoryNameByLang("funding2.") . ",
							a.ItemID, a.LocationID
					FROM
							INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
							INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
							INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
							INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
							left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
							left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
							left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
							left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
					WHERE
							a.RecordStatus = 1
							AND d.AdminGroupID IN (" . $admin_group_id . ")
							AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
					order by $orderby
					";
                    $arr_result = $linventory->returnArray($sql);
                    
                    $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">";
                    $table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"12\">" . intranet_htmlspecialchars($admin_group_name) . "</td></tr>";
                    $table_content .= "<tr>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Location</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['FundingSource'] . "</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffQty</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffReason_Name</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_RequestPerson</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Write_Off_RequestTime</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApprovePerson</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApproveDate</td>";
                    $table_content .= "</tr>";
                    
                    if (sizeof($arr_result) > 0) {
                        for ($i = 0; $i < sizeof($arr_result); $i ++) {
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                            
                            if ($item_type == 2) {
                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                            }
                            $table_content .= "<tr>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($location_name) . "</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($group_name) . "</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_qty</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_reason</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_person</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_date</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_person</td>";
                            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_date</td>";
                            $table_content .= "</tr>";
                        }
                    }
                    if (sizeof($arr_result) == 0) {
                        $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"11\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                    }
                    $table_content .= "</table>";
                    
                    $table_content .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . "<br><br>";
                    
                    // ## Principal Signature [CRM Ref No.: 2010-0211-1408]
                    $table_content .= "<br>";
                    $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
                    $table_content .= "	<tr>
									<td align='right' width='30%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
									<td align='right' width='10%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
								</tr>";
                    $table_content .= "</table>";
                    $table_content .= "<br><br>";
                }
            }
        } 
    // funding source start
else if ($groupBy == 4) {
    $arr_tmp_funding_source = explode(",", $selected_cb);

    if (sizeof($arr_tmp_funding_source) > 0) {
        for ($a = 0; $a < sizeof($arr_tmp_funding_source); $a ++) {
            $funding_source_id = $arr_tmp_funding_source[$a];

            $sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID = '" . $funding_source_id . "'";
            $arr_tmp_fs = $linventory->returnArray($sql, 1);

            if (sizeof($arr_tmp_fs) > 0) {
                list ($funding_source_name) = $arr_tmp_fs[0];
            }

            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '" . $UserID . "'";

            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }

            if ($target_admin_group != "") {
                $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
            }

            $RequestPerson = getNameFieldByLang2("f.");
            $ApprovePerson = getNameFieldByLang2("g.");
            $RequestArchivedPerson = getNameFieldByLang2("h.");
            $ApproveArchivedPerson = getNameFieldByLang2("i.");
            $sql = "SELECT
							b.ItemCode,
							b.ItemType,
							" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
							concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
							" . $linventory->getInventoryNameByLang("d.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							a.WriteOffQty,
							a.RequestDate,
							IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson))
									),
							a.ApproveDate,
							IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson))
									),
							a.WriteOffReason,
							" . $linventory->getInventoryNameByLang("funding2.") . ",
							a.ItemID, a.LocationID
					FROM
							INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
							INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
							INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN
							INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
							left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
							left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
							left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID OR funding.FundingSourceID=s.FundingSource)
							left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
					WHERE
							a.RecordStatus = 1
							AND funding.FundingSourceID IN (" . $funding_source_id . ")
							AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
					order by $orderby
					";
            $arr_result = $linventory->returnArray($sql);

            $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">";
            $table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"12\">" . intranet_htmlspecialchars($funding_source_name) . "</td></tr>";
            $table_content .= "<tr>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Location</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['FundingSource'] . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffQty</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_WriteOffReason_Name</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_RequestPerson</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Write_Off_RequestTime</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApprovePerson</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_ApproveDate</td>";
            $table_content .= "</tr>";

            if (sizeof($arr_result) > 0) {
                for ($i = 0; $i < sizeof($arr_result); $i ++) {
                    list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];

                    if ($item_type == 2) {
                        // retrieve bulk item latest funding source name
                        $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource 
                                   where a.ItemID=$this_item_id 
                                    and a.FundingSource!='' and a.FundingSource != 0 and a.LocationID=$this_location_id 
                                    order by a.RecordID desc limit 1";
                        $result_bl = $linventory->returnVector($sql_bl);
                        $funding_name = $result_bl[0];
                    }
                    $table_content .= "<tr>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($location_name) . "</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($group_name) . "</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_qty</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$write_off_reason</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_person</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$request_date</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_person</td>";
                    $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$approve_date</td>";
                    $table_content .= "</tr>";
                }
            }
            if (sizeof($arr_result) == 0) {
                $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"11\" align=\"center\">$i_no_record_exists_msg</td></tr>";
            }
            $table_content .= "</table>";

            $table_content .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . "<br><br>";

            // ## Principal Signature [CRM Ref No.: 2010-0211-1408]
            $table_content .= "<br>";
            $table_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
            $table_content .= "	<tr>
									<td align='right' width='30%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
									<td align='right' width='10%'>" . $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate'] . ":</td><td align='left' width='20%' style='border-bottom:1px solid black;'>&nbsp;</td>
								</tr>";
            $table_content .= "</table>";
            $table_content .= "<br><br>";
        }
    }
}

include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");
?>
<br>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<br>
<table width="90%" align="center" border="0">
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$i_InventorySystem_Report_ItemWrittenOff?></font></td>
	</tr>
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$school_name?></font><?=$badge_image?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td class="eSportprinttext"><?echo $i_general_startdate.": ".$start_date."&nbsp;&nbsp;&nbsp;".$i_general_enddate.": ".$end_date;?></td>
	</tr>
</table>

<?=$table_content;?>

<?
intranet_closedb();
?>