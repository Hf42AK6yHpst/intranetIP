<?php

// #################################################
// Date: 2020-08-14 (Henry)
// display stocktake location in remarks field if not in correct location [Case#P186248]
//
// Date: 2019-07-15 (Henry)
// bug fix for wrong remark display after stocktake an item [Case#G189505]
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2017-08-16 Henry
// stocktake for bulk item with different location bug fix [Case#P121300]
//
// Date: 2014-07-31 Bill
// Add detailed print mode
//
// Date: 2012-08-24 YatWoon
// Missing to add stocktake mode checking
//
// #################################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();
$lexport = new libexporttext();

$start_date = str_replace("'", "", stripslashes($start_date));
$end_date = str_replace("'", "", stripslashes($end_date));
$targetLocation = str_replace("'", "", stripslashes($targetLocation));
$targetGroup = str_replace("'", "", stripslashes($targetGroup));
$targetProgress = str_replace("'", "", stripslashes($targetProgress));

if ($start_date == "") {
    $start_date = $linventory->getStocktakePeriodStart();
}
if ($end_date == "") {
    $end_date = $linventory->getStocktakePeriodEnd();
}

// get stock checked bulk item#
if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND c.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND c.GroupInCharge = $targetGroup ";
}
if (($targetProgress == "") || ($targetProgress == 1)) {
    $cond3 = " AND c.Action = " . ITEM_ACTION_STOCKCHECK;
}
if ($targetProgress == 2) {
    $cond3 = " AND c.Action != " . ITEM_ACTION_STOCKCHECK;
}

$sql = "SELECT
				DISTINCT b.RecordID
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN 
				INVENTORY_ITEM_BULK_LOG AS c ON (b.ItemID = c.ItemID)
		WHERE
				(c.RecordDate BETWEEN '$start_date' AND '$end_date')
				$cond1
				$cond2
				$cond3
		";

$arr_result = $linventory->returnArray($sql, 1);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($checked_bulk_item) = $arr_result[$i];
        $arr_checked_bulk_list[] = $checked_bulk_item;
    }
}
if (sizeof($arr_checked_bulk_list) > 0) {
    $checked_bulk_list = implode(",", $arr_checked_bulk_list);
} else {
    $checked_bulk_list = "";
}
// get stock checked single item #
if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND b.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND b.GroupInCharge = $targetGroup ";
}
if (($targetProgress == "") || ($targetProgress == 1)) {
    $cond3 = " AND a.Action = " . ITEM_ACTION_STOCKCHECK;
}
if ($targetProgress == 2) {
    $cond3 = " AND a.Action != " . ITEM_ACTION_STOCKCHECK;
}

$sql = "SELECT 
					DISTINCT a.ItemID 
			FROM 
					INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
			WHERE 
					(a.RecordDate BETWEEN '$start_date' AND '$end_date')
					$cond1
					$cond2
					$cond3
			";

$arr_result = $linventory->returnArray($sql, 1);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($checked_single_item) = $arr_result[$i];
        $arr_checked_single_list[$i] = $checked_single_item;
    }
}
if (sizeof($arr_checked_single_list) > 0) {
    $checked_single_list = implode(",", $arr_checked_single_list);
} else {
    $checked_single_list = "";
}

$file_content .= "\"$i_InventorySystem_Report_ItemFinishStockCheck\"\n";
$file_content .= "\"$i_general_startdate\",";
$file_content .= "\"$start_date\"\n";
$file_content .= "\"$i_general_enddate\",";
$file_content .= "\"$end_date\"\n";
$file_content .= "\n";

$exportColumn = array(
    $i_InventorySystem_Report_ItemFinishStockCheck,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);
$rows[] = array(
    $i_general_startdate,
    $start_date
);
$rows[] = array(
    $i_general_enddate,
    $end_date
);

// Query more fields: detailed print mode
if ($export_type == 2) {
    // ## Single Item ###
    $extra_fields1 = ", a.NameChi, a.NameEng, a.DescriptionChi, a.DescriptionEng";
    $extra_fields1 .= ", " . $linventory->getInventoryNameByLang("fs.") . " as funding ";
    $extra_fields1 .= ", case a.Ownership
						when 1 then '$i_InventorySystem_Ownership_School'
						when 2 then '$i_InventorySystem_Ownership_Government'
						when 3 then '$i_InventorySystem_Ownership_Donor'
						end as ownership
						";
    $extra_fields1 .= ", if(b.WarrantyExpiryDate ='0000-00-00', '', b.WarrantyExpiryDate)";
    $extra_fields1 .= ", b.SoftwareLicenseModel, b.SerialNumber, b.Brand, b.SupplierName, b.SupplierContact, b.SupplierDescription, b.QuotationNo, b.TenderNo, b.InvoiceNo, if(b.PurchaseDate ='0000-00-00', '', b.PurchaseDate), b.MaintainInfo";
    
    // $extra_from1 = "left join INVENTORY_ITEM_SINGLE_EXT as i on (i.ItemID=a.ItemID) ";
    // $extra_from1 .= "left join INVENTORY_CATEGORY AS j ON (b.CategoryID = j.CategoryID) ";
    // $extra_from1 .= "left join INVENTORY_CATEGORY_LEVEL2 AS k ON (b.Category2ID = k.Category2ID) ";
    
    // Data: Funding Source
    $extra_from1 = " left join INVENTORY_FUNDING_SOURCE as fs on (b.FundingSource = fs.FundingSourceID) ";
    
    // For: Group By: Resource Management Group
    // Data: Location
    if ($groupBy == 3) {
        $extra_fields1 .= ", CONCAT(" . $linventory->getInventoryNameByLang("building2.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . ")";
        $extra_from1 .= " left JOIN INVENTORY_LOCATION_BUILDING AS building2 ON (e.BuildingID = building2.BuildingID) ";
    }
    
    // ## Bulk Item ###
    $extra_fields2 = ", a.NameChi, a.NameEng, a.DescriptionChi, a.DescriptionEng";
    // $extra_fields2 .= ", ".$linventory->getInventoryNameByLang("l.") ." as funding ";
    $extra_fields2 .= ", case a.Ownership
						when 1 then '$i_InventorySystem_Ownership_School'
						when 2 then '$i_InventorySystem_Ownership_Government'
						when 3 then '$i_InventorySystem_Ownership_Donor'
						end as ownership
						";
    $extra_fields2 .= ", " . $linventory->getInventoryNameByLang("o.") . " as var_group ";
    
    // $extra_from2 .= "left join INVENTORY_CATEGORY AS j ON (b.CategoryID = j.CategoryID) ";
    // $extra_from2 .= "left join INVENTORY_CATEGORY_LEVEL2 AS k ON (b.Category2ID = k.Category2ID) ";
    // $extra_from2 .= "left join INVENTORY_ITEM_BULK_LOCATION AS m ON (m.ItemID = a.ItemID and m.LocationID=c.LocationID) ";
    // $extra_from2 .= "left join INVENTORY_FUNDING_SOURCE as l on (l.FundingSourceID = m.FundingSourceID) ";
    
    // Data: Variance Manager
    $extra_from2 = "left join INVENTORY_ITEM_BULK_EXT AS n ON (n.ItemID = a.ItemID) ";
    $extra_from2 .= "left join INVENTORY_ADMIN_GROUP AS o ON (o.AdminGroupID = n.BulkItemAdmin) ";
    
    // For: Group By: Resource Management Group
    // Data: Location
    if ($groupBy == 3) {
        $extra_fields2 .= ", CONCAT(" . $linventory->getInventoryNameByLang("building2.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . ")";
        $extra_from2 .= " left JOIN INVENTORY_LOCATION_BUILDING AS building2 ON (e.BuildingID = building2.BuildingID) ";
    }
}

// ## Start to gen report ###
// Group By: Location
if ($groupBy == 1) {
    $cond = "WHERE LocationID in (" . $selected_cb . ")";
    
    $sql = "Select DISTINCT(CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",' > '," . $linventory->getInventoryItemNameByLang("b.") . ",' > '," . $linventory->getInventoryItemNameByLang("a.") . ")) as LocationName, LocationID 
				FROM INVENTORY_LOCATION AS a INNER JOIN 
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
				$cond 
				ORDER BY building.DisplayOrder, b.DisplayOrder, a.DisplayOrder";
    $arr_location = $linventory->returnArray($sql, 1);
    
    for ($a = 0; $a < sizeof($arr_location); $a ++) {
        list ($location_name, $locationID) = $arr_location[$a];
        
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
        }
        if ($target_admin_group != "") {
            $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
        }
        
        // ## Single Item ###
        $price_ceiling_cond = "";
        if ($sys_custom['eInventory_PriceCeiling']) {
            $price_ceiling_cond = "  AND (b.UnitPrice>=f.PriceCeiling) ";
        }
        
        $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						b.TagCode,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						b.UnitPrice,
						b.PurchasedPrice,
						a.StockTakeOption
						$extra_fields1
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						$extra_from1
				WHERE
						a.ItemType = 1 AND 
						d.LocationID = " . $locationID . " AND
						a.RecordStatus = 1
				ORDER BY
						a.ItemCode";
        $arr_single_result = $linventory->returnArray($sql, 7);
        $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
        
        $location_table_content .= "<table border=\"0\" width=\"90%\" class=\"eSporttableborder\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
        
        // $file_content .= "\"$location_name\"\n";
        // $file_content .= "\"$i_InventorySystem_ItemType_Single\"\n";
        // $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"$i_InventorySystem_Item_Barcode\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
        //
        // $rows[] = array('');
        // $rows[] = array($location_name);
        // $rows[] = array($i_InventorySystem_ItemType_Single);
        // $rows[] = array('',$i_InventorySystem_Item_Code,$i_InventorySystem_Item_Name,$i_InventorySystem_Item_Barcode,$i_InventorySystem['Category'],$i_InventorySystem['Caretaker'],$i_InventorySystem_Stocktake_LastStocktakeBy,$i_InventorySystem_Last_Stock_Take_Time,$i_InventorySystem_Item_Remark);
        
        // Header
        $file_content .= "\n";
        $file_content .= "\"$location_name\"\n";
        $file_content .= "\"$i_InventorySystem_ItemType_Single\"\n";
        $rows[] = array(
            ''
        );
        $rows[] = array(
            $location_name
        );
        $rows[] = array(
            $i_InventorySystem_ItemType_Single
        );
        
        // Basic Information
        if ($export_type == 1) {
            $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"$i_InventorySystem_Item_Barcode\",\"" . $i_InventorySystem['Category'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                '',
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_Name,
                $i_InventorySystem_Item_Barcode,
                $i_InventorySystem['Category'],
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }  // Details
else {
            $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Barcode\",\"$i_InventorySystem_Item_ChineseName\",\".$i_InventorySystem_Item_EnglishName.\",\"$i_InventorySystem_Item_ChineseDescription\",\".$i_InventorySystem_Item_EnglishDescription.\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",
								\"$i_InventorySystem_Item_Ownership\",\"$i_InventorySystem_Item_Warrany_Expiry\",\"$i_InventorySystem_Item_License_Type\",\".$i_InventorySystem_Item_Serial_Num.\",\"$i_InventorySystem_Item_Brand_Name\",\".$i_InventorySystem_Item_Supplier_Name.\",\"$i_InventorySystem_Item_Supplier_Contact\",\"$i_InventorySystem_Item_Supplier_Description\",\"$i_InventorySystem_Item_Quot_Num\",\"$i_InventorySystem_Item_Tender_Num\",
								\"$i_InventorySystem_Item_Invoice_Num\",\"$i_InventorySystem_Item_Purchase_Date\",\"$i_InventorySystem_Unit_Price\",\"$i_InventorySystemItemMaintainInfo\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                '',
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_Barcode,
                $i_InventorySystem_Item_ChineseName,
                $i_InventorySystem_Item_EnglishName,
                $i_InventorySystem_Item_ChineseDescription,
                $i_InventorySystem_Item_EnglishDescription,
                $i_InventorySystem['Category'],
                $i_InventorySystem_Item_Location,
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem['FundingSource'],
                $i_InventorySystem_Item_Ownership,
                $i_InventorySystem_Item_Warrany_Expiry,
                $i_InventorySystem_Item_License_Type,
                $i_InventorySystem_Item_Serial_Num,
                $i_InventorySystem_Item_Brand_Name,
                $i_InventorySystem_Item_Supplier_Name,
                $i_InventorySystem_Item_Supplier_Contact,
                $i_InventorySystem_Item_Supplier_Description,
                $i_InventorySystem_Item_Quot_Num,
                $i_InventorySystem_Item_Tender_Num,
                $i_InventorySystem_Item_Invoice_Num,
                $i_InventorySystem_Item_Purchase_Date,
                $i_InventorySystem_Unit_Price,
                $i_InventorySystemItemMaintainInfo,
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }
        
        if (sizeof($arr_single_result) > 0) {
            for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                
                // list($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                
                // ## Records
                // Basic Information
                if ($export_type == 1) {
                    list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                }  // Details
else {
                    list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_category, $single_item_unitprice, $single_item_purchasedprice, $single_item_stocktakeoption, $single_item_chinesename, $single_item_englishname, $single_item_chinesedescription, $single_item_englishdescription, $single_item_fundingsource, $single_item_ownership, $single_item_warrantyexpiry, $single_item_licensemodel, $single_item_serialnumber, $single_item_brand, $single_item_suppliername, $single_item_suppliercontact, $single_item_supplierdescription, $single_item_quotation, $single_item_tender, $single_item_invoice, $single_item_purchasedate, $single_item_maintaininfo) = $arr_single_result[$i];
                }
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT 
								a.Action,
								a.DateInput,
								IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "'),
								IF(a.Remark != '', a.Remark, ' - ')
						FROM 
								INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
								INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
								a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
						ORDER BY
								a.DateInput DESC LIMIT 0,1
						";
                // echo $sql."<BR>";
                $arr_result = $linventory->returnArray($sql, 4);
                
                // Stocktake Records
                if (sizeof($arr_result) > 0) {
                    for ($j = 0; $j < sizeof($arr_result); $j ++) {
                        list ($item_action, $item_record_date, $user_name, $remark) = $arr_result[$j];
                        
                        $sql = "SELECT 
									CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
						 		FROM 
									INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
									INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
									INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
								WHERE 
									b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
								ORDER BY
									b.DateInput DESC LIMIT 0,1";
									
                        $location_result = $linventory->returnArray($sql, 1);
                        
                        if($location_result){
                            $location_found = $location_result[0][0];
                            
                            if(trim($remark) !='-'){
                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                            else{
                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                        }
                        
                        if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                            // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                            $action_img = "Y";
                        } else {
                            // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                            $action_img = "N";
                        }
                        
                        // $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                        // $rows[] = array($action_img,$single_item_code,$single_item_name,$single_item_barcode,$single_item_category,$single_item_group,$user_name,$item_record_date,$remark);
                        
                        // ## Export content
                        // Basic Infomation
                        if ($export_type == 1) {
                            $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                            $rows[] = array(
                                $action_img,
                                $single_item_code,
                                $single_item_name,
                                $single_item_barcode,
                                $single_item_category,
                                $single_item_group,
                                $user_name,
                                $item_record_date,
                                $remark
                            );
                        }  // Deatils
else {
                            $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_barcode\",\"$single_item_chinesename\",\"$single_item_englishname\",\"$single_item_chinesedescription\",\"$single_item_englishdescription\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$single_item_fundingsource\",
												\"$single_item_ownership\",\"$single_item_warrantyexpiry\",\"$single_item_licensemodel\",\"$single_item_serialnumber\",\"$single_item_brand\",\"$single_item_suppliername\",\"$single_item_suppliercontact\",\"$single_item_supplierdescription\",\"$single_item_quotation\",\"$single_item_tender\",
												\"$single_item_invoice\",\"$single_item_purchasedate\",\"$single_item_unitprice\",\"$single_item_maintaininfo\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                            $rows[] = array(
                                $action_img,
                                $single_item_code,
                                $single_item_barcode,
                                $single_item_chinesename,
                                $single_item_englishname,
                                $single_item_chinesedescription,
                                $single_item_englishdescription,
                                $single_category,
                                $location_name,
                                $single_item_group,
                                $single_item_fundingsource,
                                $single_item_ownership,
                                $single_item_warrantyexpiry,
                                $single_item_licensemodel,
                                $single_item_serialnumber,
                                $single_item_brand,
                                $single_item_suppliername,
                                $single_item_suppliercontact,
                                $single_item_supplierdescription,
                                $single_item_quotation,
                                $single_item_tender,
                                $single_item_invoice,
                                $single_item_purchasedate,
                                $single_item_unitprice,
                                $single_item_maintaininfo,
                                $user_name,
                                $item_record_date,
                                $remark
                            );
                        }
                    }
                } else {
                    // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                    $action_img = "N";
                    
                    // if($user_name == "")
                    // $user_name = " - ";
                    // if($date_input == "")
                    // $date_input = " - ";
                    // if($item_remark == "")
                    // $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    $user_name = " - ";
                    $date_input = " - ";
                    $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    // $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                    // $rows[] = array($action_img,$single_item_code,$single_item_name,$single_item_barcode,$single_item_category,$single_item_group,$user_name,$date_input,$item_remark);
                    
                    // ## Export content
                    // Basic Infomation
                    if ($export_type == 1) {
                        $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                        $rows[] = array(
                            $action_img,
                            $single_item_code,
                            $single_item_name,
                            $single_item_barcode,
                            $single_item_category,
                            $single_item_group,
                            $user_name,
                            $date_input,
                            $item_remark
                        );
                    }  // Details
else {
                        $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_barcode\",\"$single_item_chinesename\",\"$single_item_englishname\",\"$single_item_chinesedescription\",\"$single_item_englishdescription\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$single_item_fundingsource\",
											\"$single_item_ownership\",\"$single_item_warrantyexpiry\",\"$single_item_licensemodel\",\"$single_item_serialnumber\",\"$single_item_brand\",\"$single_item_suppliername\",\"$single_item_suppliercontact\",\"$single_item_supplierdescription\",\"$single_item_quotation\",\"$single_item_tender\",
											\"$single_item_invoice\",\"$single_item_purchasedate\",\"$single_item_unitprice\",\"$single_item_maintaininfo\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                        $rows[] = array(
                            $action_img,
                            $single_item_code,
                            $single_item_barcode,
                            $single_item_chinesename,
                            $single_item_englishname,
                            $single_item_chinesedescription,
                            $single_item_englishdescription,
                            $single_category,
                            $location_name,
                            $single_item_group,
                            $single_item_fundingsource,
                            $single_item_ownership,
                            $single_item_warrantyexpiry,
                            $single_item_licensemodel,
                            $single_item_serialnumber,
                            $single_item_brand,
                            $single_item_suppliername,
                            $single_item_suppliercontact,
                            $single_item_supplierdescription,
                            $single_item_quotation,
                            $single_item_tender,
                            $single_item_invoice,
                            $single_item_purchasedate,
                            $single_item_unitprice,
                            $single_item_maintaininfo,
                            $user_name,
                            $date_input,
                            $item_remark
                        );
                    }
                }
            }
        }
        
        if (sizeof($arr_single_result) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        
        // ## Bulk Item ###
        if ($sys_custom['eInventory_PriceCeiling']) {
            $arr_targetSuccessBulkItem = array();
            $arr_targetFailBulkItem = array();
            $success_bulk_item_cond = "";
            $fail_bulk_item_cond = "";
            $sql = "SELECT 
							a.ItemID, a.CategoryID 
					FROM 
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 2 AND 
							d.LocationID = " . $locationID . "
					ORDER BY
							a.NameEng
					";
            $tmp_result = $linventory->returnArray($sql, 2);
            if (sizeof($tmp_result) > 0) {
                for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                    list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $tmp_item_id AND Action = " . ITEM_ACTION_PURCHASE . "";
                    $total_cost = $linventory->returnVector($sql);
                    
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $tmp_item_id";
                    $total_qty = $linventory->returnVector($sql);
                    
                    $unit_price = $total_cost[0] / $total_qty[0];
                    
                    $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = $tmp_cat_id";
                    $tmp_result2 = $linventory->returnArray($sql, 2);
                    if (sizeof($tmp_result2) > 0) {
                        for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                            list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                            if ($apply_to_bulk == 1) {
                                if ($unit_price >= $price_ceiling) {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                } else {
                                    $arr_targetFailBulkItem[] = $tmp_item_id;
                                }
                            } else {
                                $arr_targetSuccessBulkItem[] = $tmp_item_id;
                            }
                        }
                    }
                }
            }
            if (sizeof($arr_targetSuccessBulkItem) > 0) {
                $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
            }
            if (sizeof($arr_targetFailBulkItem) > 0) {
                $targetFailList = implode(",", $arr_targetFailBulkItem);
                $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
            }
        }
        
        $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.locationid,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						" . $linventory->getInventoryNameByLang("funding.") . ",
						b.GroupInCharge,
						b.FundingSourceID,
						b.Quantity,
						a.StockTakeOption
						$extra_fields2
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
						$extra_from2
				WHERE
						a.ItemType = 2 AND 
						d.LocationID = " . $locationID . "
						$success_bulk_item_cond
						$fail_bulk_item_cond
				ORDER BY
						a.NameEng
						";
        $arr_bulk_result = $linventory->returnArray($sql);
        $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
        
        // Header
        $file_content .= "\n";
        $file_content .= "\"$i_InventorySystem_ItemType_Bulk\"\n";
        $rows[] = array(
            ''
        );
        $rows[] = array(
            $i_InventorySystem_ItemType_Bulk
        );
        
        // Basic Information
        if ($export_type == 1) {
            $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"" . $i_InventorySystem['Category'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",\"$i_InventorySystem_Stocktake_ExpectedQty\",\"$i_InventorySystem_TotalMissingQty\",\"$i_InventorySystem_TotalSurplusQty\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_Name,
                $i_InventorySystem['Category'],
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem['FundingSource'],
                $i_InventorySystem_Stocktake_ExpectedQty,
                $i_InventorySystem_TotalMissingQty,
                $i_InventorySystem_TotalSurplusQty,
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }  // Details
else {
            $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_ChineseName\",\".$i_InventorySystem_Item_EnglishName.\",\"$i_InventorySystem_Item_ChineseDescription\",\".$i_InventorySystem_Item_EnglishDescription.\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",
								\"$i_InventorySystem_Item_Ownership\",\"$i_InventorySystem_Item_BulkItemAdmin\",\".$i_InventorySystem_Stocktake_ExpectedQty.\",\"$i_InventorySystem_TotalMissingQty\",\".$i_InventorySystem_TotalSurplusQty.\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_ChineseName,
                $i_InventorySystem_Item_EnglishName,
                $i_InventorySystem_Item_ChineseDescription,
                $i_InventorySystem_Item_EnglishDescription,
                $i_InventorySystem['Category'],
                $i_InventorySystem_Item_Location,
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem['FundingSource'],
                $i_InventorySystem_Item_Ownership,
                $i_InventorySystem_Item_BulkItemAdmin,
                $i_InventorySystem_Stocktake_ExpectedQty,
                $i_InventorySystem_TotalMissingQty,
                $i_InventorySystem_TotalSurplusQty,
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }
        
        if (sizeof($arr_bulk_result) > 0) {
            for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                
                // ## Records
                // Basic Information
                if ($export_type == 1) {
                    list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                }  // Details
else {
                    list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty, $bulk_stocktakeoption, $bulk_item_chinesename, $bulk_item_englishname, $bulk_item_chinesedescription, $bulk_item_englishdescription, $bulk_item_ownership, $bulk_item_vargroup) = $arr_bulk_result[$i];
                }
                
                $bulk_lastmodified = "";
                $person_in_charge = "";
                $stocktake_remark = "";
                
                $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                $MaxDateInput = $linventory->returnVector($sql);
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT
							IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
							IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
						FROM 
							INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
							a.ACTION = 2 AND 
							a.ItemID = $bulk_item_id AND
							a.LocationID = " . $bulk_item_locationid . " 
							and a.GroupInCharge=" . $bulk_group_id . " 
							and a.FundingSource =" . $bulk_funding_id . " and
							(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
							a.DateInput IN ('" . $MaxDateInput[0] . "')
						GROUP BY
							a.ItemID
						";
                $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                if (sizeof($arr_stock_take_time_person) > 0)
                    list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                    
                    /*
                 * $sql = "SELECT
                 * b.Quantity
                 * FROM
                 * INVENTORY_ITEM AS a INNER JOIN
                 * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                 * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                 * WHERE
                 * a.ItemID = ".$bulk_item_id." AND
                 * b.LocationID = ".$bulk_item_locationid;
                 *
                 * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                 * if(sizeof($arr_stock_take_quantity) > 0)
                 * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                 * if($bulk_item_quantity=="")
                 * $bulk_item_quantity=0;
                 */
                
                $sql = "SELECT 
								StockCheckQty,
								IF(Remark='' OR Remark is null,'-',Remark),
								QtyChange
						FROM 
								INVENTORY_ITEM_BULK_LOG 
						WHERE 
								ItemID = $bulk_item_id AND 
								LocationID = $bulk_item_locationid AND
								GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
								Action IN (2,3,4) AND
								(RecordDate BETWEEN '$start_date' and '$end_date') AND
								DateInput IN ('" . $MaxDateInput[0] . "')
						ORDER BY
								DateInput DESC
						";
                $arr_stocktake_qty = $linventory->returnArray($sql);
                if (sizeof($arr_stocktake_qty) > 0) {
                    list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                } else {
                    $stocktake_remark = "";
                    $stocktake_qty_change = 0;
                    $stocktake_qty = 0;
                }
                
                // $item_different = $stocktake_qty - $bulk_item_quantity;
                $item_different = $stocktake_qty_change;
                // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                
                if ($person_in_charge == "")
                    $person_in_charge = " - ";
                if ($bulk_lastmodified == "")
                    $bulk_lastmodified = " - ";
                if ($stocktake_remark == "")
                    $stocktake_remark = $i_InventorySystem_Report_Stocktake_NotDone;
                
                if ($item_different > 0) {
                    $diff_qty_1 = 0;
                    $diff_qty_2 = $item_different;
                    // $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    // $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                }
                if ($item_different < 0) {
                    $diff_qty_1 = $item_different;
                    $diff_qty_2 = 0;
                    // $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                    // $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                if ($item_different == 0) {
                    $diff_qty_1 = 0;
                    $diff_qty_2 = 0;
                    // $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    // $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                
                if ($person_in_charge == "")
                    $person_in_charge = " - ";
                if ($bulk_lastmodified == "")
                    $bulk_lastmodified = " - ";
                    
                    // $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_item_quantity\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    // $rows[] = array($bulk_item_code,$bulk_item_name,$bulk_item_category,$bulk_item_group,$bulk_item_quantity,$diff_qty_1,$diff_qty_2,$person_in_charge,$bulk_lastmodified,$stocktake_remark);
                    
                // $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_funding\",\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    // $rows[] = array($bulk_item_code,$bulk_item_name,$bulk_item_category,$bulk_item_group,$bulk_funding,$expect_qty,$diff_qty_1,$diff_qty_2,$person_in_charge,$bulk_lastmodified,$stocktake_remark);
                    
                // ## Export Content
                    // Basic Information
                if ($export_type == 1) {
                    $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_funding\",\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    $rows[] = array(
                        $bulk_item_code,
                        $bulk_item_name,
                        $bulk_item_category,
                        $bulk_item_group,
                        $bulk_funding,
                        $expect_qty,
                        $diff_qty_1,
                        $diff_qty_2,
                        $person_in_charge,
                        $bulk_lastmodified,
                        $stocktake_remark
                    );
                }  // Details
else {
                    $file_content .= "\"$bulk_item_code\",\"$bulk_item_chinesename\",\"$bulk_item_englishname\",\"$bulk_item_chinesedescription\",\"$bulk_item_englishdescription\",\"$bulk_item_category\",\"$location_name\",\"$bulk_item_group\",\"$bulk_funding\",\"$bulk_item_ownership\",\"$bulk_item_vargroup\",
										\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    $rows[] = array(
                        $bulk_item_code,
                        $bulk_item_chinesename,
                        $bulk_item_englishname,
                        $bulk_item_chinesedescription,
                        $bulk_item_englishdescription,
                        $bulk_item_category,
                        $location_name,
                        $bulk_item_group,
                        $bulk_funding,
                        $bulk_item_ownership,
                        $bulk_item_vargroup,
                        $expect_qty,
                        $diff_qty_1,
                        $diff_qty_2,
                        $person_in_charge,
                        $bulk_lastmodified,
                        $stocktake_remark
                    );
                }
            }
        }
        
        if (sizeof($arr_bulk_result) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        $file_content .= "\n";
    }
} 

// Group by: Item Type
else 
    if ($groupBy == 2) {
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
            $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
        }
        
        // # Single Item ##
        $price_ceiling_cond = "";
        if ($sys_custom['eInventory_PriceCeiling']) {
            $price_ceiling_cond = "  AND (b.UnitPrice>=f.PriceCeiling) ";
        }
        $sql = "SELECT 
					a.ItemCode, 
					a.ItemID,
					b.TagCode,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					d.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
					b.UnitPrice,
					b.PurchasedPrice,
					a.StockTakeOption
					$extra_fields1
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
					INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (e.BuildingID = building.BuildingID) INNER JOIN
					INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					$extra_from1
			WHERE
					a.ItemType = 1 AND a.RecordStatus = 1
			ORDER BY
					a.ItemCode";
        $arr_single_result = $linventory->returnArray($sql);
        $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
        
        // Header
        $file_content .= "\n";
        $file_content .= "\"$i_InventorySystem_ItemType_Single\"\n";
        $rows[] = array(
            ''
        );
        $rows[] = array(
            $i_InventorySystem_ItemType_Single
        );
        
        // Basic Information
        if ($export_type == 1) {
            $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"$i_InventorySystem_Item_Barcode\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                '',
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_Name,
                $i_InventorySystem_Item_Barcode,
                $i_InventorySystem['Category'],
                $i_InventorySystem_Item_Location,
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }  // Details
else {
            $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Barcode\",\"$i_InventorySystem_Item_ChineseName\",\".$i_InventorySystem_Item_EnglishName.\",\"$i_InventorySystem_Item_ChineseDescription\",\".$i_InventorySystem_Item_EnglishDescription.\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",
							\"$i_InventorySystem_Item_Ownership\",\"$i_InventorySystem_Item_Warrany_Expiry\",\"$i_InventorySystem_Item_License_Type\",\".$i_InventorySystem_Item_Serial_Num.\",\"$i_InventorySystem_Item_Brand_Name\",\".$i_InventorySystem_Item_Supplier_Name.\",\"$i_InventorySystem_Item_Supplier_Contact\",\"$i_InventorySystem_Item_Supplier_Description\",\"$i_InventorySystem_Item_Quot_Num\",\"$i_InventorySystem_Item_Tender_Num\",
							\"$i_InventorySystem_Item_Invoice_Num\",\"$i_InventorySystem_Item_Purchase_Date\",\"$i_InventorySystem_Unit_Price\",\"$i_InventorySystemItemMaintainInfo\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                '',
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_Barcode,
                $i_InventorySystem_Item_ChineseName,
                $i_InventorySystem_Item_EnglishName,
                $i_InventorySystem_Item_ChineseDescription,
                $i_InventorySystem_Item_EnglishDescription,
                $i_InventorySystem['Category'],
                $i_InventorySystem_Item_Location,
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem['FundingSource'],
                $i_InventorySystem_Item_Ownership,
                $i_InventorySystem_Item_Warrany_Expiry,
                $i_InventorySystem_Item_License_Type,
                $i_InventorySystem_Item_Serial_Num,
                $i_InventorySystem_Item_Brand_Name,
                $i_InventorySystem_Item_Supplier_Name,
                $i_InventorySystem_Item_Supplier_Contact,
                $i_InventorySystem_Item_Supplier_Description,
                $i_InventorySystem_Item_Quot_Num,
                $i_InventorySystem_Item_Tender_Num,
                $i_InventorySystem_Item_Invoice_Num,
                $i_InventorySystem_Item_Purchase_Date,
                $i_InventorySystem_Unit_Price,
                $i_InventorySystemItemMaintainInfo,
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }
        
        // debug_pr($arr_single_result);
        
        if (sizeof($arr_single_result) > 0) {
            for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                
                // list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_item_locationid, $single_item_location, $single_category ) = $arr_single_result[$i];
                // ## Records
                // Basic Information
                if ($export_type == 1) {
                    list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_item_locationid, $single_item_location, $single_category) = $arr_single_result[$i];
                }  // Details
else {
                    list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_item_locationid, $single_item_location, $single_category, $single_item_unitprice, $single_item_purchasedprice, $single_item_stocktakeoption, $single_item_chinesename, $single_item_englishname, $single_item_chinesedescription, $single_item_englishdescription, $single_item_fundingsource, $single_item_ownership, $single_item_warrantyexpiry, $single_item_licensemodel, $single_item_serialnumber, $single_item_brand, $single_item_suppliername, $single_item_suppliercontact, $single_item_supplierdescription, $single_item_quotation, $single_item_tender, $single_item_invoice, $single_item_purchasedate, $single_item_maintaininfo) = $arr_single_result[$i];
                }
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT 
							a.Action,
							a.DateInput,
							IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "'),
							a.Remark
					FROM 
							INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
					WHERE 
							a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
					ORDER BY
							a.DateInput DESC LIMIT 0,1
					";
                $arr_result = $linventory->returnArray($sql, 4);
                
                // Stocktake Records
                if (sizeof($arr_result) > 0) {
                    for ($j = 0; $j < sizeof($arr_result); $j ++) {
                        list ($item_action, $date_input, $user_name, $item_remark) = $arr_result[$j];
                        
                        $sql = "SELECT 
									CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
						 		FROM 
									INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
									INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
									INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
								WHERE 
									b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
								ORDER BY
									b.DateInput DESC LIMIT 0,1";
									
                        $location_result = $linventory->returnArray($sql, 1);
                        
                        if($location_result){
                            $location_found = $location_result[0][0];
                            
                            if(trim($remark) !='-'){
                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                            else{
                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                        }
                        
                        if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                            // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                            $action_img = "Y";
                        } else {
                            // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                            $action_img = "N";
                        }
                        
                        // ## Export Content
                        // Basic Information
                        if ($export_type == 1) {
                            $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                            $rows[] = array(
                                $action_img,
                                $single_item_code,
                                $single_item_name,
                                $single_item_barcode,
                                $single_category,
                                $single_item_location,
                                $single_item_group,
                                $user_name,
                                $date_input,
                                $item_remark
                            );
                        }  // details export
else {
                            $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_barcode\",\"$single_item_chinesename\",\"$single_item_englishname\",\"$single_item_chinesedescription\",\"$single_item_englishdescription\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$single_item_fundingsource\",
											\"$single_item_ownership\",\"$single_item_warrantyexpiry\",\"$single_item_licensemodel\",\"$single_item_serialnumber\",\"$single_item_brand\",\"$single_item_suppliername\",\"$single_item_suppliercontact\",\"$single_item_supplierdescription\",\"$single_item_quotation\",\"$single_item_tender\",
											\"$single_item_invoice\",\"$single_item_purchasedate\",\"$single_item_unitprice\",\"$single_item_maintaininfo\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                            $rows[] = array(
                                $action_img,
                                $single_item_code,
                                $single_item_barcode,
                                $single_item_chinesename,
                                $single_item_englishname,
                                $single_item_chinesedescription,
                                $single_item_englishdescription,
                                $single_category,
                                $single_item_location,
                                $single_item_group,
                                $single_item_fundingsource,
                                $single_item_ownership,
                                $single_item_warrantyexpiry,
                                $single_item_licensemodel,
                                $single_item_serialnumber,
                                $single_item_brand,
                                $single_item_suppliername,
                                $single_item_suppliercontact,
                                $single_item_supplierdescription,
                                $single_item_quotation,
                                $single_item_tender,
                                $single_item_invoice,
                                $single_item_purchasedate,
                                $single_item_unitprice,
                                $single_item_maintaininfo,
                                $user_name,
                                $date_input,
                                $item_remark
                            );
                        }
                    }
                } else {
                    // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                    $action_img = "N";
                    
                    // if($user_name == "")
                    // $user_name = " - ";
                    // if($date_input == "")
                    // $date_input = " - ";
                    // if($item_remark == "")
                    // $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    $user_name = " - ";
                    $date_input = " - ";
                    $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    // ## Export Content
                    // Basic Information
                    if ($export_type == 1) {
                        $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                        $rows[] = array(
                            $action_img,
                            $single_item_code,
                            $single_item_name,
                            $single_item_barcode,
                            $single_category,
                            $single_item_location,
                            $single_item_group,
                            $user_name,
                            $date_input,
                            $item_remark
                        );
                    }  // Deatils
else {
                        $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_barcode\",\"$single_item_chinesename\",\"$single_item_englishname\",\"$single_item_chinesedescription\",\"$single_item_englishdescription\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$single_item_fundingsource\",
										\"$single_item_ownership\",\"$single_item_warrantyexpiry\",\"$single_item_licensemodel\",\"$single_item_serialnumber\",\"$single_item_brand\",\"$single_item_suppliername\",\"$single_item_suppliercontact\",\"$single_item_supplierdescription\",\"$single_item_quotation\",\"$single_item_tender\",
										\"$single_item_invoice\",\"$single_item_purchasedate\",\"$single_item_unitprice\",\"$single_item_maintaininfo\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                        $rows[] = array(
                            $action_img,
                            $single_item_code,
                            $single_item_barcode,
                            $single_item_chinesename,
                            $single_item_englishname,
                            $single_item_chinesedescription,
                            $single_item_englishdescription,
                            $single_category,
                            $single_item_location,
                            $single_item_group,
                            $single_item_fundingsource,
                            $single_item_ownership,
                            $single_item_warrantyexpiry,
                            $single_item_licensemodel,
                            $single_item_serialnumber,
                            $single_item_brand,
                            $single_item_suppliername,
                            $single_item_suppliercontact,
                            $single_item_supplierdescription,
                            $single_item_quotation,
                            $single_item_tender,
                            $single_item_invoice,
                            $single_item_purchasedate,
                            $single_item_unitprice,
                            $single_item_maintaininfo,
                            $user_name,
                            $date_input,
                            $item_remark
                        );
                    }
                }
            }
        }
        if (sizeof($arr_single_result) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        
        // # Bulk Item ##
        if ($sys_custom['eInventory_PriceCeiling']) {
            $arr_targetSuccessBulkItem = array();
            $arr_targetFailBulkItem = array();
            $success_bulk_item_cond = "";
            $fail_bulk_item_cond = "";
            $sql = "SELECT 
						a.ItemID, a.CategoryID
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
				WHERE
						a.ItemType = 2
				ORDER BY
						a.ItemCode";
            $tmp_result = $linventory->returnArray($sql, 2);
            if (sizeof($tmp_result) > 0) {
                for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                    list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $tmp_item_id AND Action = " . ITEM_ACTION_PURCHASE . "";
                    $total_cost = $linventory->returnVector($sql);
                    
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $tmp_item_id";
                    $total_qty = $linventory->returnVector($sql);
                    
                    $unit_price = $total_cost[0] / $total_qty[0];
                    
                    $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = $tmp_cat_id";
                    $tmp_result2 = $linventory->returnArray($sql, 2);
                    if (sizeof($tmp_result2) > 0) {
                        for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                            list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                            if ($apply_to_bulk == 1) {
                                if ($unit_price >= $price_ceiling) {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                } else {
                                    $arr_targetFailBulkItem[] = $tmp_item_id;
                                }
                            } else {
                                $arr_targetSuccessBulkItem[] = $tmp_item_id;
                            }
                        }
                    }
                }
            }
            if (sizeof($arr_targetSuccessBulkItem) > 0) {
                $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
            }
            if (sizeof($arr_targetFailBulkItem) > 0) {
                $targetFailList = implode(",", $arr_targetFailBulkItem);
                $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
            }
        }
        
        $sql = "SELECT 
					a.ItemCode, 
					a.ItemID,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					d.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
					" . $linventory->getInventoryNameByLang("funding.") . ",
					b.GroupInCharge,
					b.FundingSourceID,
					b.Quantity,
					a.StockTakeOption
					$extra_fields2
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (e.BuildingID = building.BuildingID) INNER JOIN
					INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
					$extra_from2
			WHERE
					a.ItemType = 2
					$success_bulk_item_cond
					$fail_bulk_item_cond
			ORDER BY
					a.ItemCode";
        $arr_bulk_result = $linventory->returnArray($sql);
        $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
        
        // Header
        $file_content .= "\n";
        $file_content .= "\"$i_InventorySystem_ItemType_Bulk\"\n";
        $rows[] = array(
            ''
        );
        $rows[] = array(
            $i_InventorySystem_ItemType_Bulk
        );
        
        // Basic Information
        if ($export_type == 1) {
            $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",\"$i_InventorySystem_Stocktake_ExpectedQty\",\"$i_InventorySystem_TotalMissingQty\",\"$i_InventorySystem_TotalSurplusQty\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_Name,
                $i_InventorySystem['Category'],
                $i_InventorySystem_Item_Location,
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem['FundingSource'],
                $i_InventorySystem_Stocktake_ExpectedQty,
                $i_InventorySystem_TotalMissingQty,
                $i_InventorySystem_TotalSurplusQty,
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }  // Details
else {
            $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_ChineseName\",\".$i_InventorySystem_Item_EnglishName.\",\"$i_InventorySystem_Item_ChineseDescription\",\".$i_InventorySystem_Item_EnglishDescription.\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",
							\"$i_InventorySystem_Item_Ownership\",\"$i_InventorySystem_Item_BulkItemAdmin\",\".$i_InventorySystem_Stocktake_ExpectedQty.\",\"$i_InventorySystem_TotalMissingQty\",\".$i_InventorySystem_TotalSurplusQty.\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
            $rows[] = array(
                $i_InventorySystem_Item_Code,
                $i_InventorySystem_Item_ChineseName,
                $i_InventorySystem_Item_EnglishName,
                $i_InventorySystem_Item_ChineseDescription,
                $i_InventorySystem_Item_EnglishDescription,
                $i_InventorySystem['Category'],
                $i_InventorySystem_Item_Location,
                $i_InventorySystem['Caretaker'],
                $i_InventorySystem['FundingSource'],
                $i_InventorySystem_Item_Ownership,
                $i_InventorySystem_Item_BulkItemAdmin,
                $i_InventorySystem_Stocktake_ExpectedQty,
                $i_InventorySystem_TotalMissingQty,
                $i_InventorySystem_TotalSurplusQty,
                $i_InventorySystem_Stocktake_LastStocktakeBy,
                $i_InventorySystem_Last_Stock_Take_Time,
                $i_InventorySystem_Item_Remark
            );
        }
        
        if (sizeof($arr_bulk_result) > 0) {
            for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                
                // list($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_location, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                
                // ## Records
                // Basic Information
                if ($export_type == 1) {
                    list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_location, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                }  // Details
else {
                    list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_location, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty, $bulk_stocktakeoption, $bulk_item_chinesename, $bulk_item_englishname, $bulk_item_chinesedescription, $bulk_item_englishdescription, $bulk_item_ownership, $bulk_item_vargroup) = $arr_bulk_result[$i];
                }
                
                $bulk_lastmodified = "";
                $person_in_charge = "";
                $stocktake_remark = "";
                
                $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                $MaxDateInput = $linventory->returnVector($sql);
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT
						IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
						IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
					FROM 
						INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
						INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
					WHERE 
						a.ACTION = 2 AND 
						a.ItemID = $bulk_item_id AND
						a.LocationID = " . $bulk_item_locationid . " 
						and a.GroupInCharge=" . $bulk_group_id . " 
						and a.FundingSource =" . $bulk_funding_id . " and
						(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
						a.DateInput IN ('" . $MaxDateInput[0] . "')
					GROUP BY
						a.ItemID
					";
                $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                if (sizeof($arr_stock_take_time_person) > 0)
                    list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                    
                    /*
                 * $sql = "SELECT
                 * b.Quantity
                 * FROM
                 * INVENTORY_ITEM AS a INNER JOIN
                 * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                 * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                 * WHERE
                 * a.ItemID = ".$bulk_item_id." AND
                 * b.LocationID = ".$bulk_item_locationid;
                 *
                 * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                 * if(sizeof($arr_stock_take_quantity) > 0)
                 * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                 * if($bulk_item_quantity=="")
                 * $bulk_item_quantity=0;
                 */
                
                $sql = "SELECT 
							StockCheckQty,
							IF(Remark='' OR Remark is null,'-',Remark),
							QtyChange
					FROM 
							INVENTORY_ITEM_BULK_LOG 
					WHERE 
							ItemID = $bulk_item_id AND 
							LocationID = $bulk_item_locationid AND
							GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
							Action IN (2,3,4) AND
							(RecordDate BETWEEN '$start_date' and '$end_date') AND
							DateInput IN ('" . $MaxDateInput[0] . "')
					ORDER BY
							DateInput DESC
					";
                $arr_stocktake_qty = $linventory->returnArray($sql);
                if (sizeof($arr_stocktake_qty) > 0) {
                    list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                } else {
                    $stocktake_remark = "";
                    $stocktake_qty_change = 0;
                    $stocktake_qty = 0;
                }
                
                // $item_different = $stocktake_qty - $bulk_item_quantity;
                $item_different = $stocktake_qty_change;
                // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                
                if ($item_different > 0) {
                    $diff_qty_1 = 0;
                    $diff_qty_2 = $item_different;
                    // $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    // $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                }
                if ($item_different < 0) {
                    $diff_qty_1 = $item_different;
                    $diff_qty_2 = 0;
                    // $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                    // $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                if ($item_different == 0) {
                    $diff_qty_1 = 0;
                    $diff_qty_2 = 0;
                    // $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    // $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                
                if ($person_in_charge == "")
                    $person_in_charge = " - ";
                if ($bulk_lastmodified == "")
                    $bulk_lastmodified = " - ";
                if ($stocktake_remark == "")
                    $stocktake_remark = $i_InventorySystem_Report_Stocktake_NotDone;
                    
                    // $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_item_location\",\"$bulk_item_quantity\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    // $rows[] = array($bulk_item_code,$bulk_item_name,$bulk_item_category,$bulk_item_location,$bulk_item_group,$bulk_item_quantity,$diff_qty_1,$diff_qty_2,$person_in_charge,$bulk_lastmodified,$stocktake_remark);
                    // $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_funding\",\"$bulk_item_location\",\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    // $rows[] = array($bulk_item_code,$bulk_item_name,$bulk_item_category,$bulk_item_location,$bulk_item_group,$bulk_funding,$expect_qty,$diff_qty_1,$diff_qty_2,$person_in_charge,$bulk_lastmodified,$stocktake_remark);
                    
                // ## Export Content
                    // Basic Information
                if ($export_type == 1) {
                    $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_funding\",\"$bulk_item_location\",\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    $rows[] = array(
                        $bulk_item_code,
                        $bulk_item_name,
                        $bulk_item_category,
                        $bulk_item_location,
                        $bulk_item_group,
                        $bulk_funding,
                        $expect_qty,
                        $diff_qty_1,
                        $diff_qty_2,
                        $person_in_charge,
                        $bulk_lastmodified,
                        $stocktake_remark
                    );
                }  // Details
else {
                    $file_content .= "\"$bulk_item_code\",\"$bulk_item_chinesename\",\"$bulk_item_englishname\",\"$bulk_item_chinesedescription\",\"$bulk_item_englishdescription\",\"$bulk_item_category\",\"$bulk_item_location\",\"$bulk_item_group\",\"$bulk_funding\",\"$bulk_item_ownership\",\"$bulk_item_vargroup\",
									\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                    $rows[] = array(
                        $bulk_item_code,
                        $bulk_item_chinesename,
                        $bulk_item_englishname,
                        $bulk_item_chinesedescription,
                        $bulk_item_englishdescription,
                        $bulk_item_category,
                        $bulk_item_location,
                        $bulk_item_group,
                        $bulk_funding,
                        $bulk_item_ownership,
                        $bulk_item_vargroup,
                        $expect_qty,
                        $diff_qty_1,
                        $diff_qty_2,
                        $person_in_charge,
                        $bulk_lastmodified,
                        $stocktake_remark
                    );
                }
            }
        }
        
        if (sizeof($arr_bulk_result) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
    }     

    // Group by: Resource Management Group
    else 
        if ($groupBy == 3) {
            
            $cond = "WHERE AdminGroupID in (" . $selected_cb . ")";
            
            if ($linventory->IS_ADMIN_USER($UserID)) {
                $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
            } else {
                $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
            }
            $arr_group = $linventory->returnArray($sql, 2);
            
            for ($a = 0; $a < sizeof($arr_group); $a ++) {
                list ($group_name, $admin_group_id) = $arr_group[$a];
                
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                    $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                }
                
                // ## Single Item ###
                $price_ceiling_cond = "";
                if ($sys_custom['eInventory_PriceCeiling']) {
                    $price_ceiling_cond = " AND (b.UnitPrice>=f.PriceCeiling) ";
                }
                $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						b.TagCode,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						b.UnitPrice,
						b.PurchasedPrice,
						a.StockTakeOption
						$extra_fields1
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						$extra_from1
				WHERE
						a.ItemType = 1 AND 
						c.AdminGroupID = " . $admin_group_id . " AND
						a.RecordStatus = 1
				ORDER BY
						a.ItemCode";
                $arr_single_result = $linventory->returnArray($sql, 7);
                $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
                
                // Header
                $file_content .= "\n";
                $file_content .= "\"$group_name\"\n";
                $file_content .= "\"$i_InventorySystem_ItemType_Single\"\n";
                $rows[] = array(
                    ''
                );
                $rows[] = array(
                    $group_name
                );
                $rows[] = array(
                    $i_InventorySystem_ItemType_Single
                );
                
                // basic export
                if ($export_type == 1) {
                    $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"$i_InventorySystem_Item_Barcode\",\"" . $i_InventorySystem['Category'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
                    $rows[] = array(
                        '',
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Name,
                        $i_InventorySystem_Item_Barcode,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem_Stocktake_LastStocktakeBy,
                        $i_InventorySystem_Last_Stock_Take_Time,
                        $i_InventorySystem_Item_Remark
                    );
                }  // details export
else {
                    $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Barcode\",\"$i_InventorySystem_Item_ChineseName\",\".$i_InventorySystem_Item_EnglishName.\",\"$i_InventorySystem_Item_ChineseDescription\",\".$i_InventorySystem_Item_EnglishDescription.\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",
								\"$i_InventorySystem_Item_Ownership\",\"$i_InventorySystem_Item_Warrany_Expiry\",\"$i_InventorySystem_Item_License_Type\",\".$i_InventorySystem_Item_Serial_Num.\",\"$i_InventorySystem_Item_Brand_Name\",\".$i_InventorySystem_Item_Supplier_Name.\",\"$i_InventorySystem_Item_Supplier_Contact\",\"$i_InventorySystem_Item_Supplier_Description\",\"$i_InventorySystem_Item_Quot_Num\",\"$i_InventorySystem_Item_Tender_Num\",
								\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
                    $rows[] = array(
                        '',
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Barcode,
                        $i_InventorySystem_Item_ChineseName,
                        $i_InventorySystem_Item_EnglishName,
                        $i_InventorySystem_Item_ChineseDescription,
                        $i_InventorySystem_Item_EnglishDescription,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem_Item_Location,
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_Item_Ownership,
                        $i_InventorySystem_Item_Warrany_Expiry,
                        $i_InventorySystem_Item_License_Type,
                        $i_InventorySystem_Item_Serial_Num,
                        $i_InventorySystem_Item_Brand_Name,
                        $i_InventorySystem_Item_Supplier_Name,
                        $i_InventorySystem_Item_Supplier_Contact,
                        $i_InventorySystem_Item_Supplier_Description,
                        $i_InventorySystem_Item_Quot_Num,
                        $i_InventorySystem_Item_Tender_Num,
                        $i_InventorySystem_Item_Invoice_Num,
                        $i_InventorySystem_Item_Purchase_Date,
                        $i_InventorySystem_Unit_Price,
                        $i_InventorySystemItemMaintainInfo,
                        $i_InventorySystem_Stocktake_LastStocktakeBy,
                        $i_InventorySystem_Last_Stock_Take_Time,
                        $i_InventorySystem_Item_Remark
                    );
                }
                
                if (sizeof($arr_single_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                        
                        // ## Records
                        // Basic Information
                        if ($export_type == 1) {
                            list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                        }  // Details
else {
                            list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category, $single_item_unitprice, $single_item_purchasedprice, $single_item_stocktakeoption, $single_item_chinesename, $single_item_englishname, $single_item_chinesedescription, $single_item_englishdescription, $single_item_fundingsource, $single_item_ownership, $single_item_warrantyexpiry, $single_item_licensemodel, $single_item_serialnumber, $single_item_brand, $single_item_suppliername, $single_item_suppliercontact, $single_item_supplierdescription, $single_item_quotation, $single_item_tender, $single_item_invoice, $single_item_purchasedate, $single_item_maintaininfo, $single_item_location) = $arr_single_result[$i];
                        }
                        
                        $namefield = getNameFieldByLang2("b.");
                        $sql = "SELECT 
								a.Action,
								a.DateInput,
								IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "'),
								IF(a.Remark != '', a.Remark, ' - ')
						FROM 
								INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
								INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
								a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
						ORDER BY
								a.DateInput DESC LIMIT 0,1
						";
                        $arr_result = $linventory->returnArray($sql, 4);
                        
                        // Stocktake Records
                        if (sizeof($arr_result) > 0) {
                            for ($j = 0; $j < sizeof($arr_result); $j ++) {
                                list ($item_action, $item_record_date, $user_name, $remark) = $arr_result[$j];
                                
	                            $sql = "SELECT 
											CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
								 		FROM 
											INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
											INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
											INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
											INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
										WHERE 
											b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
										ORDER BY
											b.DateInput DESC LIMIT 0,1";
											
	                            $location_result = $linventory->returnArray($sql, 1);
	                            
	                			if($location_result){ 
	                            	$location_found = $location_result[0][0];
	                               
		                            if(trim($remark) !='-'){
		                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
		                            }
		                            else{
		                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
		                            }
		                        }
                        
                                if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                                    // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                                    $action_img = "Y";
                                } else {
                                    // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                                    $action_img = "N";
                                }
                                
                                // $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                                // $rows[] = array($action_img,$single_item_code,$single_item_name,$single_item_barcode,$single_item_category,$single_item_group,$user_name,$item_record_date,$remark);
                                
                                // ## Export content
                                // Basic Information
                                if ($export_type == 1) {
                                    $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                                    $rows[] = array(
                                        $action_img,
                                        $single_item_code,
                                        $single_item_name,
                                        $single_item_barcode,
                                        $single_item_category,
                                        $single_item_group,
                                        $user_name,
                                        $item_record_date,
                                        $remark
                                    );
                                }  // Details
else {
                                    $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_barcode\",\"$single_item_chinesename\",\"$single_item_englishname\",\"$single_item_chinesedescription\",\"$single_item_englishdescription\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$single_item_fundingsource\",
												\"$single_item_ownership\",\"$single_item_warrantyexpiry\",\"$single_item_licensemodel\",\"$single_item_serialnumber\",\"$single_item_brand\",\"$single_item_suppliername\",\"$single_item_suppliercontact\",\"$single_item_supplierdescription\",\"$single_item_quotation\",\"$single_item_tender\",
												\"$single_item_invoice\",\"$single_item_purchasedate\",\"$single_item_unitprice\",\"$single_item_maintaininfo\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                                    $rows[] = array(
                                        $action_img,
                                        $single_item_code,
                                        $single_item_barcode,
                                        $single_item_chinesename,
                                        $single_item_englishname,
                                        $single_item_chinesedescription,
                                        $single_item_englishdescription,
                                        $single_item_category,
                                        $single_item_location,
                                        $single_item_group,
                                        $single_item_fundingsource,
                                        $single_item_ownership,
                                        $single_item_warrantyexpiry,
                                        $single_item_licensemodel,
                                        $single_item_serialnumber,
                                        $single_item_brand,
                                        $single_item_suppliername,
                                        $single_item_suppliercontact,
                                        $single_item_supplierdescription,
                                        $single_item_quotation,
                                        $single_item_tender,
                                        $single_item_invoice,
                                        $single_item_purchasedate,
                                        $single_item_unitprice,
                                        $single_item_maintaininfo,
                                        $user_name,
                                        $item_record_date,
                                        $remark
                                    );
                                }
                            }
                        } else {
                            // $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                            $action_img = "N";
                            
                            // if($user_name == "")
                            // $user_name = " - ";
                            // if($item_record_date == "")
                            // $item_record_date = " - ";
                            // if($remark == "")
                            // $remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                            
                            $user_name = " - ";
                            $item_record_date = " - ";
                            $remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                            
                            // $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                            // $rows[] = array($action_img,$single_item_code,$single_item_name,$single_item_barcode,$single_item_category,$single_item_group,$user_name,$item_record_date,$remark);
                            
                            if ($export_type == 1) {
                                $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_name\",\"$single_item_barcode\",\"$single_item_category\",\"$single_item_group\",\"$user_name\",\"$item_record_date\",\"$remark\"\n";
                                $rows[] = array(
                                    $action_img,
                                    $single_item_code,
                                    $single_item_name,
                                    $single_item_barcode,
                                    $single_item_category,
                                    $single_item_group,
                                    $user_name,
                                    $item_record_date,
                                    $remark
                                );
                            }  // details export
else {
                                $file_content .= "\"$action_img\",\"$single_item_code\",\"$single_item_barcode\",\"$single_item_chinesename\",\"$single_item_englishname\",\"$single_item_chinesedescription\",\"$single_item_englishdescription\",\"$single_category\",\"$single_item_location\",\"$single_item_group\",\"$single_item_fundingsource\",
												\"$single_item_ownership\",\"$single_item_warrantyexpiry\",\"$single_item_licensemodel\",\"$single_item_serialnumber\",\"$single_item_brand\",\"$single_item_suppliername\",\"$single_item_suppliercontact\",\"$single_item_supplierdescription\",\"$single_item_quotation\",\"$single_item_tender\",
												\"$single_item_invoice\",\"$single_item_purchasedate\",\"$single_item_unitprice\",\"$single_item_maintaininfo\",\"$user_name\",\"$date_input\",\"$item_remark\"\n";
                                $rows[] = array(
                                    $action_img,
                                    $single_item_code,
                                    $single_item_barcode,
                                    $single_item_chinesename,
                                    $single_item_englishname,
                                    $single_item_chinesedescription,
                                    $single_item_englishdescription,
                                    $single_item_category,
                                    $single_item_location,
                                    $single_item_group,
                                    $single_item_fundingsource,
                                    $single_item_ownership,
                                    $single_item_warrantyexpiry,
                                    $single_item_licensemodel,
                                    $single_item_serialnumber,
                                    $single_item_brand,
                                    $single_item_suppliername,
                                    $single_item_suppliercontact,
                                    $single_item_supplierdescription,
                                    $single_item_quotation,
                                    $single_item_tender,
                                    $single_item_invoice,
                                    $single_item_purchasedate,
                                    $single_item_unitprice,
                                    $single_item_maintaininfo,
                                    $user_name,
                                    $item_record_date,
                                    $remark
                                );
                            }
                        }
                    }
                }
                if (sizeof($arr_single_result) == 0) {
                    $file_content .= "\"$i_no_record_exists_msg\"\n";
                    $rows[] = array(
                        $i_no_record_exists_msg
                    );
                }
                
                // ## Bulk Item ###
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                    $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                }
                
                if ($sys_custom['eInventory_PriceCeiling']) {
                    $arr_targetSuccessBulkItem = array();
                    $arr_targetFailBulkItem = array();
                    $success_bulk_item_cond = "";
                    $fail_bulk_item_cond = "";
                    $sql = "SELECT 
							a.ItemID, a.CategoryID
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 2 AND 
							c.AdminGroupID = " . $admin_group_id . "
					ORDER BY
							a.NameEng
					";
                    $tmp_result = $linventory->returnArray($sql, 2);
                    if (sizeof($tmp_result) > 0) {
                        for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                            list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                            $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $tmp_item_id AND Action = " . ITEM_ACTION_PURCHASE . "";
                            $total_cost = $linventory->returnVector($sql);
                            
                            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $tmp_item_id";
                            $total_qty = $linventory->returnVector($sql);
                            
                            $unit_price = $total_cost[0] / $total_qty[0];
                            
                            $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = $tmp_cat_id";
                            $tmp_result2 = $linventory->returnArray($sql, 2);
                            if (sizeof($tmp_result2) > 0) {
                                for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                                    list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                                    if ($apply_to_bulk == 1) {
                                        if ($unit_price >= $price_ceiling) {
                                            $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                        } else {
                                            $arr_targetFailBulkItem[] = $tmp_item_id;
                                        }
                                    } else {
                                        $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                    }
                                }
                            }
                        }
                    }
                    if (sizeof($arr_targetSuccessBulkItem) > 0) {
                        $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                        $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
                    }
                    if (sizeof($arr_targetFailBulkItem) > 0) {
                        $targetFailList = implode(",", $arr_targetFailBulkItem);
                        $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
                    }
                }
                $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.locationid,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						" . $linventory->getInventoryNameByLang("funding.") . ",
						b.GroupInCharge,
						b.FundingSourceID,
						b.Quantity,
						a.StockTakeOption
						$extra_fields2
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
						$extra_from2
				WHERE
						a.ItemType = 2 AND 
						c.AdminGroupID = " . $admin_group_id . "
						$success_bulk_item_cond
						$fail_bulk_item_cond
				ORDER BY
						a.NameEng
						";
                $arr_bulk_result = $linventory->returnArray($sql);
                $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
                
                // Header
                $file_content .= "\"$i_InventorySystem_ItemType_Bulk\"\n";
                $rows[] = array(
                    ''
                );
                $rows[] = array(
                    $i_InventorySystem_ItemType_Bulk
                );
                
                // Basic Information
                if ($export_type == 1) {
                    $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"" . $i_InventorySystem['Category'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",\"$i_InventorySystem_Stocktake_ExpectedQty\",\"$i_InventorySystem_TotalMissingQty\",\"$i_InventorySystem_TotalSurplusQty\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Name,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_Stocktake_ExpectedQty,
                        $i_InventorySystem_TotalMissingQty,
                        $i_InventorySystem_TotalSurplusQty,
                        $i_InventorySystem_Stocktake_LastStocktakeBy,
                        $i_InventorySystem_Last_Stock_Take_Time,
                        $i_InventorySystem_Item_Remark
                    );
                }  // Details
else {
                    $file_content .= "\"\",\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_ChineseName\",\".$i_InventorySystem_Item_EnglishName.\",\"$i_InventorySystem_Item_ChineseDescription\",\".$i_InventorySystem_Item_EnglishDescription.\",\"" . $i_InventorySystem['Category'] . "\",\"$i_InventorySystem_Item_Location\",\"" . $i_InventorySystem['Caretaker'] . "\",\"" . $i_InventorySystem['FundingSource'] . "\",
								\"$i_InventorySystem_Item_Ownership\",\"$i_InventorySystem_Item_BulkItemAdmin\",\".$i_InventorySystem_Stocktake_ExpectedQty.\",\"$i_InventorySystem_TotalMissingQty\",\".$i_InventorySystem_TotalSurplusQty.\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\",\"$i_InventorySystem_Item_Remark\"\n";
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_ChineseName,
                        $i_InventorySystem_Item_EnglishName,
                        $i_InventorySystem_Item_ChineseDescription,
                        $i_InventorySystem_Item_EnglishDescription,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem_Item_Location,
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_Item_Ownership,
                        $i_InventorySystem_Item_BulkItemAdmin,
                        $i_InventorySystem_Stocktake_ExpectedQty,
                        $i_InventorySystem_TotalMissingQty,
                        $i_InventorySystem_TotalSurplusQty,
                        $i_InventorySystem_Stocktake_LastStocktakeBy,
                        $i_InventorySystem_Last_Stock_Take_Time,
                        $i_InventorySystem_Item_Remark
                    );
                }
                
                if (sizeof($arr_bulk_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                        
                        // ## Records
                        // Basic Information
                        if ($export_type == 1) {
                            list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                        }  // Details
else {
                            list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty, $bulk_stocktakeoption, $bulk_item_chinesename, $bulk_item_englishname, $bulk_item_chinesedescription, $bulk_item_englishdescription, $bulk_item_ownership, $bulk_item_vargroup, $bulk_item_location) = $arr_bulk_result[$i];
                        }
                        
                        $bulk_lastmodified = "";
                        $person_in_charge = "";
                        $stocktake_remark = "";
                        
                        $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                        $MaxDateInput = $linventory->returnVector($sql);
                        
                        $namefield = getNameFieldByLang2("b.");
                        $sql = "SELECT
							IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
							IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
						FROM 
							INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
							a.ACTION = 2 AND 
							a.ItemID = $bulk_item_id AND
							a.LocationID = " . $bulk_item_locationid . " 
							and a.GroupInCharge=" . $bulk_group_id . " 
							and a.FundingSource =" . $bulk_funding_id . " and
							(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
							a.DateInput IN ('" . $MaxDateInput[0] . "')
						GROUP BY
							a.ItemID
						";
                        $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                        if (sizeof($arr_stock_take_time_person) > 0)
                            list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                            
                            /*
                         * $sql = "SELECT
                         * b.Quantity
                         * FROM
                         * INVENTORY_ITEM AS a INNER JOIN
                         * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                         * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                         * WHERE
                         * a.ItemID = ".$bulk_item_id." AND
                         * b.LocationID = ".$bulk_item_locationid;
                         *
                         * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                         * if(sizeof($arr_stock_take_quantity) > 0)
                         * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                         * if($bulk_item_quantity=="")
                         * $bulk_item_quantity=0;
                         */
                        
                        $sql = "SELECT 
								StockCheckQty,
								IF(Remark='' OR Remark is null,'-',Remark),
								QtyChange 
						FROM 
								INVENTORY_ITEM_BULK_LOG 
						WHERE 
								ItemID = $bulk_item_id AND 
								LocationID = $bulk_item_locationid AND
								GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
								Action IN (2,3,4) AND
								(RecordDate BETWEEN '$start_date' and '$end_date') AND
								DateInput IN ('" . $MaxDateInput[0] . "')
						ORDER BY
								DateInput DESC
						";
                        $arr_stocktake_qty = $linventory->returnArray($sql);
                        if (sizeof($arr_stocktake_qty) > 0) {
                            list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                        } else {
                            $stocktake_remark = "";
                            $stocktake_qty_change = 0;
                            $stocktake_qty = 0;
                        }
                        
                        // $item_different = $stocktake_qty - $bulk_item_quantity;
                        $item_different = $stocktake_qty_change;
                        // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                        
                        if ($item_different > 0) {
                            $diff_qty_1 = 0;
                            $diff_qty_2 = $item_different;
                            // $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                            // $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                        }
                        if ($item_different < 0) {
                            $diff_qty_1 = $item_different;
                            $diff_qty_2 = 0;
                            // $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                            // $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                        }
                        if ($item_different == 0) {
                            $diff_qty_1 = 0;
                            $diff_qty_2 = 0;
                            // $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                            // $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                        }
                        
                        if ($person_in_charge == "")
                            $person_in_charge = " - ";
                        if ($bulk_lastmodified == "")
                            $bulk_lastmodified = " - ";
                        if ($stocktake_remark == "")
                            $stocktake_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                            
                            // $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_item_quantity\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                            // $rows[] = array($bulk_item_code,$bulk_item_name,$bulk_item_category,$bulk_item_group,$bulk_item_quantity,$diff_qty_1,$diff_qty_2,$person_in_charge,$bulk_lastmodified,$stocktake_remark);
                            // $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_funding\",\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                            // $rows[] = array($bulk_item_code,$bulk_item_name,$bulk_item_category,$bulk_item_group,$bulk_funding,$expect_qty,$diff_qty_1,$diff_qty_2,$person_in_charge,$bulk_lastmodified,$stocktake_remark);
                            
                        // ## Export Content
                            // Basic Information
                        if ($export_type == 1) {
                            $file_content .= "\"$bulk_item_code\",\"$bulk_item_name\",\"$bulk_item_category\",\"$bulk_item_group\",\"$bulk_funding\",\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                            $rows[] = array(
                                $bulk_item_code,
                                $bulk_item_name,
                                $bulk_item_category,
                                $bulk_item_group,
                                $bulk_funding,
                                $expect_qty,
                                $diff_qty_1,
                                $diff_qty_2,
                                $person_in_charge,
                                $bulk_lastmodified,
                                $stocktake_remark
                            );
                        }  // details export
else {
                            $file_content .= "\"$bulk_item_code\",\"$bulk_item_chinesename\",\"$bulk_item_englishname\",\"$bulk_item_chinesedescription\",\"$bulk_item_englishdescription\",\"$bulk_item_category\",\"$bulk_item_location\",\"$bulk_item_group\",\"$bulk_funding\",\"$bulk_item_ownership\",\"$bulk_item_vargroup\",
										\"$expect_qty\",\"$diff_qty_1\",\"$diff_qty_2\",\"$person_in_charge\",\"$bulk_lastmodified\",\"$stocktake_remark\"\n";
                            $rows[] = array(
                                $bulk_item_code,
                                $bulk_item_chinesename,
                                $bulk_item_englishname,
                                $bulk_item_chinesedescription,
                                $bulk_item_englishdescription,
                                $bulk_item_category,
                                $bulk_item_location,
                                $bulk_item_group,
                                $bulk_funding,
                                $bulk_item_ownership,
                                $bulk_item_vargroup,
                                $expect_qty,
                                $diff_qty_1,
                                $diff_qty_2,
                                $person_in_charge,
                                $bulk_lastmodified,
                                $stocktake_remark
                            );
                        }
                    }
                }
                if (sizeof($arr_bulk_result) == 0) {
                    $file_content .= "\"$i_no_record_exists_msg\"\n";
                    $rows[] = array(
                        $i_no_record_exists_msg
                    );
                }
                $file_content .= "\n";
            }
        }
$display = $file_content;

intranet_closedb();

$filename = "stocktake_unicode.csv";

// debug_pr($rows);
$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>