<?php
// Using: 
// ############################################
// Date: 2019-09-02 (Tommy)
// add maximum width and height to school badge
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-02 Henry
// PHP 5.4 fix: change split to explode
//
// Date: 2015-11-02 Cameron
// Add filter by specified number of days for warranty expiry
//
// Date: 2012-03-07 YatWoon
// Fixed: Should be displayed all the expired items, not coming x days expired items
//
// ############################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Report_WarrantyExpiry";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();

$school_data = explode("\n", get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root . "/file/schoolbadge.txt");
$badge_size = getimagesize($PATH_WRT_ROOT."/file/".$school_badge);
$badge_width = $badge_size[0];
$badge_height = $badge_siz[1];
$maxWidth = 120;
$maxHeight = 60;
if($badge_width > $maxWidth){
    $badge_width = $maxWidth;
}
if($badge_height > $maxHeight){
    $badge_height = $maxHeight;
}
if ($school_badge != "") {
    $badge_image = "<img src='".$PATH_WRT_ROOT."/file/$school_badge' width='$badge_width'px height='$badge_height'px>";
}

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_WarrantyExpiry,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");
$DayPeriod = $linventory->getWarrantyExpiryWarningDayPeriod();
$date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $DayPeriod, date("Y")));

if ($linventory->IS_ADMIN_USER($UserID))
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
else
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";

$tmp_arr_group = $linventory->returnVector($sql);
if (sizeof($tmp_arr_group) > 0) {
    $target_admin_group = implode(",", $tmp_arr_group);
}

$cond = '';
if ($target_admin_group != "") {
    $cond .= " AND c.AdminGroupID IN ($target_admin_group) ";
}

$warranty_expire = ($warranty_expire == '') ? 0 : $warranty_expire;
if ($warranty_expire == 0) {
    $cond .= " AND b.WarrantyExpiryDate <= '$curr_date' ";
} else {
    $cond .= " AND b.WarrantyExpiryDate <= DATE_ADD(CURDATE(),INTERVAL $warranty_expire DAY) ";
    $cond .= " AND b.WarrantyExpiryDate > '$curr_date' ";
}

$sql = "SELECT 
					a.ItemCode, 
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("d.") . "),
					b.WarrantyExpiryDate
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '0000-00-00') INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS floor ON (d.LocationLevelID = floor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
			WHERE 
					1
					$cond
			ORDER BY
					b.WarrantyExpiryDate";

$arr_expiry = $linventory->returnArray($sql, 4);

$table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" width=\"25%\">$i_InventorySystem_Item_Code</td>";
$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"20%\">$i_InventorySystem_Report_ItemName</td>";
$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"20%\">$i_InventorySystem_Group_Name</td>";
$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"20%\">$i_InventorySystem_Item_Location</td>";
$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"15%\">$i_InventorySystem_Report_WarrantyExpiryDate</td></tr>";

if (sizeof($arr_expiry) > 0) {
    for ($i = 0; $i < sizeof($arr_expiry); $i ++) {
        list ($item_code, $item_name, $item_group, $item_location, $item_warranty_expiry) = $arr_expiry[$i];
        
        $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($item_group) . "</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($item_location) . "</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_warranty_expiry</td></tr>";
    }
}
if (sizeof($arr_expiry) == 0) {
    $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");
?>

<br>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>

</table>
<br>
<table width="90%" align="center" border="0">
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$i_InventorySystem_Report_WarrantyExpiry?></font></td>
	</tr>
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$school_name?></font><?=$badge_image?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="5"
	align="center" class="eSporttableborder">
	<?=$table_content;?>
</table>

<?
intranet_closedb();
?>