<?php
// using:

// #########################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2017-12-19 Cameron
// redirect to reports.php in PowerClass with error message if basic setting is not setup
//
// Date: 2012-07-10 (YatWoon)
// Enhanced: support 2 funding source for single item
//
// Date: 2012-07-04 YatWoon
// Improved: display category/sub-category with distinct from INVENTORY_ITEM
//
// Date: 2011-03-18 YatWoon
// - update query, hidden bulk item if quantity is 0
// - Fixed: display incorrect item latest status data
//
// #########################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Report_InventoryStatus";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui = new liblocation_ui();

// ## Check user have set all the basic setting ###
$sql = "SELECT LocationID FROM INVENTORY_LOCATION";
$arr_location_check = $linventory->returnVector($sql);

$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
$arr_group_check = $linventory->returnVector($sql);

// $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY";
$sql = "SELECT 
			distinct(a.CategoryID)
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY as b on a.CategoryID = b.CategoryID
		";
$arr_category_check = $linventory->returnVector($sql);

$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE";
$arr_funding_check = $linventory->returnVector($sql);

if ((sizeof($arr_location_check) == 0) || (sizeof($arr_group_check) == 0) || (sizeof($arr_category_check) == 0) || (sizeof($arr_funding_check) == 0)) {
    if ($sys_custom['PowerClass']) {
        header("Location: " . $PATH_WRT_ROOT . "home/PowerClass/reports.php?error=MissingBasicSetting");
    } else {
        header("Location: " . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php");
    }
}
// ## END ###

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_ItemStatus,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arr_groupBy = array(
    array(
        1,
        $i_InventorySystem['Location']
    ),
    array(
        2,
        $i_InventorySystem['item_type']
    ),
    array(
        3,
        $i_InventorySystem['Caretaker']
    ),
    array(
        4,
        $i_InventorySystem['Category']
    )
);
$groupby_selection = getSelectByArray($arr_groupBy, "name=\"groupBy\" id=\"groupBy\" onChange=\"this.form.submit();\" ", $groupBy);
// # report - group by
$report_filter .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Report_Stocktake_groupby</td>";

$report_filter .= "<td>$groupby_selection</td></tr>\n";

// # group by - location
if ($groupBy == 1) {
    $report_filter .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Location'] . "</td>";
    $report_filter .= "<td>" . $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '') . "</td>";
    
    if ($TargetFloor != "") {
        $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$TargetFloor."' AND RecordStatus = 1";
        $result = $linventory->returnArray($sql, 2);
        
        $room_selection = "<select id=\"TargetRoom[]\" name=\"TargetRoom[]\" multiple size=\"10\">";
        if (sizeof($TargetRoom) == 0) {
            $room_selection .= "<option value=\"\" selected> - Select Room - </option>";
        } else {
            $room_selection .= "<option value=\"\"> - Select Room - </option>";
        }
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($location_id, $location_name) = $result[$i];
                
                $selected = "";
                if (is_array($TargetRoom)) {
                    if (in_array($location_id, $TargetRoom)) {
                        $selected = " SELECTED ";
                    }
                }
                $room_selection .= "<option value=\"$location_id\" $selected>$location_name</option>";
            }
        }
        $room_selection .= "</select>";
        
        $report_filter .= "<tr><td></td>";
        $report_filter .= "<td>" . $room_selection . "</td>";
    }
}

// # group by - resource mgmt group
if ($groupBy == 3) {
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
    $result = $linventory->returnArray($sql, 2);
    
    // $group_selection = getSelectByArray($arr_group, " name=\"targetGroup\" ", $targetGroup, 1, 0);
    
    $group_selection = "<select id=\"targetGroup[]\" name=\"targetGroup[]\" multiple size=\"10\">";
    if (sizeof($targetGroup) == 0) {
        $group_selection .= "<option value=\"\" selected> - Select Group - </option>";
    } else {
        $group_selection .= "<option value=\"\"> - Select Group - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($group_id, $group_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetGroup)) {
                if (in_array($group_id, $targetGroup)) {
                    $selected = " SELECTED ";
                }
            }
            $group_selection .= "<option value=\"$group_id\" $selected>$group_name</option>";
        }
    }
    $group_selection .= "</select>";
    
    $report_filter .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $report_filter .= "<td>" . $group_selection . "</td>";
}
// # group by - Item Category
if ($groupBy == 4) {
    
    // $sql = "SELECT CategoryID, ".$linventory->getInventoryNameByLang()." as CatName FROM INVENTORY_CATEGORY ORDER BY DisplayOrder";
    $sql = "SELECT 
			distinct(a.CategoryID), 
			" . $linventory->getInventoryNameByLang("b.") . " as CatName
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY as b on a.CategoryID = b.CategoryID
		ORDER BY b.DisplayOrder";
    $arr_category = $linventory->returnArray($sql, 2);
    
    $cat_selection = getSelectByArray($arr_category, " name=\"targetCategory\" onChange=\"this.form.submit();\"  ", $targetCategory, 0, 0);
    if ($targetCategory != "") {
        // $sql = "SELECT a.Category2ID, ".$linventory->getInventoryNameByLang("a.")."
        // FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID)
        // WHERE b.CategoryID = $targetCategory ORDER BY b.DisplayOrder, a.DisplayOrder";
        $sql = "SELECT 
			distinct(a.Category2ID), 
			" . $linventory->getInventoryNameByLang("b.") . " 
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY_LEVEL2 as b on b.Category2ID = a.Category2ID
			left join INVENTORY_CATEGORY as c on c.CategoryID=a.CategoryID
		 WHERE b.CategoryID = $targetCategory 
		ORDER BY b.DisplayOrder, c.DisplayOrder";
        $arr_category2 = $linventory->returnArray($sql, 2);
        
        $sub_cat_selection = "<select id=\"targetSubCategory[]\" name=\"targetSubCategory[]\" multiple size=\"10\">";
        if (sizeof($targetSubCategory) == 0) {
            $sub_cat_selection .= "<option value=\"\" selected> - Select Sub-category - </option>";
        } else {
            $sub_cat_selection .= "<option value=\"\"> - select sub-category - </option>";
        }
        
        if (sizeof($arr_category2) != 0) {
            for ($i = 0; $i < sizeof($arr_category2); $i ++) {
                list ($sub_cat_id, $sub_cat_name) = $arr_category2[$i];
                
                $selected = "";
                
                if (sizeof($targetSubCategory) != 0) {
                    if (in_array($sub_cat_id, $targetSubCategory)) {
                        $selected = " SELECTED ";
                    }
                }
                $sub_cat_selection .= "<option value='$sub_cat_id' $selected>$sub_cat_name</option>";
            }
        }
        $sub_cat_selection .= "</selection>";
    }
    
    $report_filter .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Category'] . "</td>";
    $report_filter .= "<td>" . $cat_selection . "</td>";
    $report_filter .= "<tr><td>&nbsp;</td><td>" . $sub_cat_selection . "</td></tr>";
}

$report_filter .= "<tr><td colspan=\"2\" align=\"center\">" . $linterface->GET_ACTION_BTN($button_submit, "submit") . " " . "</td></tr>\n";
$report_filter .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";

// #######################################################################################
// #######################################################################################
// #######################################################################################
// ## Result
// #######################################################################################
// #######################################################################################
// #######################################################################################

// # Start to gen report
if ($step == "final") {
    if (sizeof($TargetRoom)) {
        $groupby_val = implode(",", $TargetRoom);
    }
    if (sizeof($targetGroup)) {
        $groupby_val = implode(",", $targetGroup);
    }
    if (sizeof($targetSubCategory) > 0) {
        $groupby_val = implode(",", $targetSubCategory);
    }
    
    if ($groupBy == 1) // #### Location
{
        $single_empty = 0;
        $bulk_empty = 0;
        
        if (is_array($TargetRoom))
            $cond = "AND LocationID in (" . implode($TargetRoom, ",") . ")";
        
        $sql = "Select DISTINCT(CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",' > '," . $linventory->getInventoryItemNameByLang("b.") . ",' > '," . $linventory->getInventoryItemNameByLang("a.") . ")) as LocationName, LocationID 
				FROM INVENTORY_LOCATION AS a INNER JOIN 
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
				$cond 
				ORDER BY b.LocationLevelID";
        $arr_location = $linventory->returnArray($sql, 1);
        
        // # category filter
        $sql = "SELECT CategoryID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_CATEGORY";
        $arrayCat = $linventory->returnArray($sql, 2);
        $location_table_content .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>";
        $location_table_content .= "<tr><td>";
        if (sizeof($result) > 0) {
            $location_table_content .= getSelectByArray($arrayCat, " name='targetCategoryFilter' onChange='this.form.step.value=\"final\"; this.form.submit();' ", $targetCategoryFilter, 0, 0);
        }
        // # sub-category filter
        if ($targetCategoryFilter != "") {
            $sql = "SELECT Category2ID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$targetCategoryFilter."'";
            $arraySubCat = $linventory->returnArray($sql, 2);
            
            $location_table_content .= getSelectByArray($arraySubCat, " name='targetSubCategoryFilter' onChange='this.form.step.value=\"final\"; this.form.submit();' ", $targetSubCategoryFilter, 0, 0);
        }
        $location_table_content .= "</td></tr></table><br>";
        
        // # Toolbar - Print & Export
        // $toolbar .= "<tr><td class=\"tabletext\">".$linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetLocation','$targetGroup','$groupBy','$groupby_val','$targetSubCategoryFilter','')","","","","",0)."&nbsp;&nbsp;&nbsp;";
        // $toolbar .= $linterface->GET_LNK_EXPORT("item_status_export.php?targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val&SubCatFilter=$targetSubCategoryFilter","","","","",0)."&nbsp;&nbsp;&nbsp;</td></tr>\n";
        
        for ($a = 0; $a < sizeof($arr_location); $a ++) {
            $location_table_content .= "<table border=\"0\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
            list ($location_name, $locationID) = $arr_location[$a];
            
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }
            
            if ($target_admin_group != "") {
                $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
            }
            
            if ($targetSubCategoryFilter != "") {
                $SubCatCond = " AND SUBCAT.Category2ID = $targetSubCategoryFilter ";
            } else 
                if ($targetCategoryFilter != "") {
                    $sql = "SELECT a.Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) WHERE b.CategoryID = '".$targetCategoryFilter."' ORDER BY b.DisplayOrder, a.DisplayOrder";
                    $arrTargetSubCat = $linventory->returnVector($sql);
                    if (sizeof($arrTargetSubCat > 0)) {
                        $targetSubCategory = implode(",", $arrTargetSubCat);
                        $SubCatCond = " AND SUBCAT.Category2ID IN ($targetSubCategory) ";
                    }
                }
            
            // single items #
            $sql = "SELECT 
							a.ItemID 
					FROM 
							INVENTORY_ITEM AS a INNER JOIN 
							INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID $cond) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
					WHERE
							a.RecordStatus = 1 AND
							c.LocationID = $locationID
					ORDER BY
							a.ItemCode
					";
            $arr_tmp_single_item = $linventory->returnArray($sql, 1);
            
            $location_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">" . intranet_htmlspecialchars($location_name) . "</td></tr>";
            $location_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>";
            $location_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\" colspan=\"3\">" . $i_InventorySystemItemStatus . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
            $location_table_content .= "</tr>";
            
            if (sizeof($arr_tmp_single_item) > 0) {
                for ($i = 0; $i < sizeof($arr_tmp_single_item); $i ++) {
                    list ($tmp_single_item_id) = $arr_tmp_single_item[$i];
                    
                    $sql = "SELECT 
									DISTINCT ITEM.ItemID,
									ITEM.ItemCode,
									CONCAT(" . $linventory->getInventoryNameByLang("CAT.") . ",'>'," . $linventory->getInventoryNameByLang("SUBCAT.") . "),
									" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
									CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
									" . $linventory->getInventoryNameByLang("AG.") . ",
									" . $linventory->getInventoryNameByLang("funding.") . ",
									" . $linventory->getInventoryNameByLang("funding2.") . "
							FROM 
									INVENTORY_ITEM AS ITEM INNER JOIN 
									INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
									INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
									INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) LEFT OUTER JOIN
									INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
									left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
									left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
							WHERE 
									ITEM.ItemID = $tmp_single_item_id AND
									ITEM.RecordStatus = 1 AND
									ITEM.ItemType=1 AND 
									L.LocationID='" . $locationID . "' 
									$SubCatCond
							LIMIT 0,1
							";
                    $arr_single_result = $linventory->returnArray($sql);
                    
                    if (is_array($arr_single_result) && count($arr_single_result) > 0) {
                        foreach ($arr_single_result as $Key => $Value) {
                            // if($i%2 == 0)
                            // $table_row_css = " class=\"tablebluerow1\" ";
                            // else
                            // $table_row_css = " class=\"tablebluerow2\" ";
                            $location_table_content .= "<tr bgcolor=\"#FFFFCC\">";
                            
                            list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $item_funding, $item_funding2) = $Value;
                            
                            $sql = "SELECT 
											NewStatus, DateModified 
									FROM 
											INVENTORY_ITEM_SINGLE_STATUS_LOG
									WHERE
											ItemID = $item_id AND
											Action IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
									ORDER BY
											DateModified DESC 
									LIMIT 0,1
									";
                            $tmp_single_item_status = $linventory->returnArray($sql, 2);
                            
                            if (sizeof($tmp_single_item_status) > 0) {
                                list ($item_status, $date_modified) = $tmp_single_item_status[0];
                            }
                            
                            if ($item_status == "")
                                $status = " - ";
                            else 
                                if ($item_status == ITEM_STATUS_NORMAL)
                                    $status = $i_InventorySystem_ItemStatus_Normal;
                                else 
                                    if ($item_status == ITEM_STATUS_DAMAGED)
                                        $status = $i_InventorySystem_ItemStatus_Damaged;
                                    else 
                                        if ($item_status == ITEM_STATUS_REPAIR)
                                            $status = $i_InventorySystem_ItemStatus_Repair;
                                        else
                                            $status = " - ";
                            
                            $location_table_content .= "<td class=\"single tabletext\">" . $item_code . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . $item_name . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . $item_cat . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($item_funding) . (($item_funding2) ? ", " . intranet_htmlspecialchars($item_funding2) : "") . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\" colspan=\"3\">" . $status . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . $date_modified . "</td></tr>";
                        }
                    } else {
                        $single_empty ++;
                    }
                }
            }
            if ((sizeof($arr_tmp_single_item) == 0) || (sizeof($arr_tmp_single_item) == $single_empty)) {
                $location_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\" single tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
            }
            // single items end #
            
            // bulk items #
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
                $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
            }
            
            if ($targetSubCategoryFilter != "") {
                $SubCatCond = " AND SUBCAT.Category2ID IN ($targetSubCategoryFilter) ";
            } else 
                if ($targetCategoryFilter != "") {
                    $sql = "SELECT a.Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) WHERE b.CategoryID = '".$targetCategoryFilter."' ORDER BY b.DisplayOrder, a.DisplayOrder";
                    $arrTargetSubCat = $linventory->returnVector($sql);
                    if (sizeof($arrTargetSubCat > 0)) {
                        $targetSubCategoryFilter = implode(",", $arrTargetSubCat);
                        $SubCatCond = " AND SUBCAT.Category2ID IN ($targetSubCategoryFilter) ";
                    }
                }
            
            $sql = "SELECT 
							a.ItemID, b.GroupInCharge, b.FundingSourceID
					FROM 
							INVENTORY_ITEM AS a 
							INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0 $cond) 
							INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
					WHERE
							a.RecordStatus = 1 AND
							c.LocationID = $locationID
					ORDER BY 
							a.ItemCode, b.RecordID
					";
            $arr_tmp_bulk_item = $linventory->returnArray($sql);
            $location_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">$i_InventorySystem_ItemType_Bulk</td></tr>";
            $location_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_ItemStatus_Normal . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_ItemStatus_Repair . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_ItemStatus_Damaged . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
            $location_table_content .= "</tr>";
            
            if (sizeof($arr_tmp_bulk_item) > 0) {
                for ($i = 0; $i < sizeof($arr_tmp_bulk_item); $i ++) {
                    list ($tmp_bulk_item_id, $tmp_bulk_group_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$i];
                    
                    $sql = "SELECT 
									DISTINCT ITEM.ItemID,
									ITEM.ItemCode,
									CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
									" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
									CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
									" . $linventory->getInventoryNameByLang("AG.") . ",
									" . $linventory->getInventoryNameByLang("funding.") . ",
									LOCATION.GroupInCharge,
									LOCATION.FundingSourceID
							FROM 
									INVENTORY_ITEM AS ITEM INNER JOIN 
									INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
									INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
									INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0) LEFT OUTER JOIN
									INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
									left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
							WHERE 
									ITEM.ItemID = $tmp_bulk_item_id AND
									ITEM.RecordStatus = 1 AND
									ITEM.ItemType = 2 AND 
									L.LocationID='" . $locationID . "' AND LOCATION.GroupInCharge=$tmp_bulk_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
									$SubCatCond
							LIMIT 0,1
							";
                    $arr_bulk_result = $linventory->returnArray($sql);
                    
                    if (is_array($arr_bulk_result) && count($arr_bulk_result) > 0) {
                        foreach ($arr_bulk_result as $Key => $Value) {
                            $qty_normal = "";
                            $qty_repair = "";
                            $qty_damage = "";
                            $date_modified = "";
                            
                            // if($i%2 == 0)
                            // $table_row_css = " class=\"tablebluerow1\" ";
                            // else
                            // $table_row_css = " class=\"tablebluerow2\" ";
                            $location_table_content .= "<tr bgcolor=\"#FFFFCC\">";
                            
                            list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $bulk_funding, $bulk_group_id, $bulk_funding_id) = $Value;
                            $sql = "SELECT 
											if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
											if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
											if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
											BL.DateModified
									FROM
											INVENTORY_ITEM_BULK_LOG AS BL 
									WHERE
											BL.ItemID = $item_id AND
											BL.LocationID = $locationID AND
											BL.GroupInCharge=$bulk_group_id and BL.FundingSource=$bulk_funding_id and
											BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
									ORDER BY
											DateModified DESC
									LIMIT 0,1										
									";
                            $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                            
                            if (sizeof($tmp_bulk_item_status) > 0) {
                                list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                            }
                            
                            if ($qty_normal == "")
                                $qty_normal = " - ";
                            if ($qty_repair == "")
                                $qty_repair = " - ";
                            if ($qty_damage == "")
                                $qty_damage = " - ";
                            if ($date_modified == "")
                                $date_modified = " - ";
                            
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $item_code . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $item_name . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $item_cat . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $qty_normal . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $qty_repair . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $qty_damage . "</td>";
                            $location_table_content .= "<td class=\"bulk tabletext\">" . $date_modified . "</td></tr>";
                        }
                    } else {
                        $bulk_empty ++;
                    }
                }
            }
            if ((sizeof($arr_tmp_bulk_item) == 0) || (sizeof($arr_tmp_bulk_item) == $bulk_empty)) {
                $location_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\" bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
            }
            $location_table_content .= "<tr><td colspan=\"10\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
            $location_table_content .= "</table><br>";
            
            // # Toolbar - Print & Export
            $toolbar = "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetLocation','$targetGroup','$groupBy','$groupby_val','$targetSubCategoryFilter','')", "", "", "", "", 0);
            $toolbar .= $linterface->GET_LNK_EXPORT("item_status_export.php?targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val&SubCatFilter=$targetSubCategoryFilter", "", "", "", "", 0) . "</td></tr>\n";
        }
    } else 
        if ($groupBy == 2) // #### Item Type
{
            // # Toolbar - Print & Export
            $toolbar = "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetLocation','$targetGroup','$groupBy','$groupby_val','','')", "", "", "", "", 0);
            $toolbar .= $linterface->GET_LNK_EXPORT("item_status_export.php?targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val", "", "", "", "", 0) . "</td></tr>\n";
            
            // single item #
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
                $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
            }
            
            $sql = "SELECT 
						a.ItemID 
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID $cond) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
				WHERE
						a.RecordStatus = 1
				ORDER BY 
						a.ItemCode
				";
            $arr_tmp_single_item = $linventory->returnArray($sql, 1);
            
            $single_report_filter .= "<tr><td class=\"tablename\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>";
            $single_report_filter .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystemItemStatus . "</td>";
            $single_report_filter .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
            $single_report_filter .= "</tr>";
            
            if (sizeof($arr_tmp_single_item) > 0) {
                for ($i = 0; $i < sizeof($arr_tmp_single_item); $i ++) {
                    list ($tmp_single_item_id) = $arr_tmp_single_item[$i];
                    
                    $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",						
								CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								" . $linventory->getInventoryNameByLang("funding2.") . "
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
								INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) INNER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
						WHERE 
								ITEM.ItemID = $tmp_single_item_id AND
								ITEM.ItemType=1 AND
								ITEM.RecordStatus = 1
						";
                    
                    $arr_single_result = $linventory->returnArray($sql);
                    
                    if (is_array($arr_single_result) && count($arr_single_result) > 0) {
                        foreach ($arr_single_result as $Key => $Value) {
                            $item_status = "";
                            $date_modified = "";
                            
                            // if($i%2 == 0)
                            // $table_row_css = " class=\"tablebluerow1\" ";
                            // else
                            // $table_row_css = " class=\"tablebluerow2\" ";
                            
                            $single_report_filter .= "<tr bgcolor=\"#FFFFCC\">";
                            
                            list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $item_funding, $item_funding2) = $Value;
                            
                            $sql = "SELECT 
										NewStatus, DateModified 
								FROM 
										INVENTORY_ITEM_SINGLE_STATUS_LOG
								WHERE
										ItemID = $item_id AND
										ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
								ORDER BY
										DateModified DESC 
								LIMIT 0,1
								";
                            
                            $tmp_single_item_status = $linventory->returnArray($sql, 2);
                            
                            if (sizeof($tmp_single_item_status) > 0) {
                                list ($item_status, $date_modified) = $tmp_single_item_status[0];
                            }
                            
                            if ($item_status == "")
                                $status = " - ";
                            else 
                                if ($item_status == ITEM_STATUS_NORMAL)
                                    $status = $i_InventorySystem_ItemStatus_Normal;
                                else 
                                    if ($item_status == ITEM_STATUS_DAMAGED)
                                        $status = $i_InventorySystem_ItemStatus_Damaged;
                                    else 
                                        if ($item_status == ITEM_STATUS_REPAIR)
                                            $status = $i_InventorySystem_ItemStatus_Repair;
                                        else
                                            $status = " - ";
                            
                            if ($date_modified == "")
                                $date_modified = " - ";
                            
                            $single_report_filter .= "<td class=\"single tabletext\">" . $item_code . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . $item_name . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . $item_cat . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($item_funding) . (($item_funding2) ? ", " . intranet_htmlspecialchars($item_funding2) : "") . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . $status . "</td>";
                            $single_report_filter .= "<td class=\"single tabletext\">" . $date_modified . "</td></tr>";
                        }
                    }
                }
            }
            if (sizeof($arr_tmp_single_item) == 0) {
                $single_report_filter .= "<tr class=\"single\"><td colspan=\"10\" class=\"tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
            }
            // end of single item #
            
            // bulk items #
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
                $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
            }
            
            $sql = "SELECT 
						a.ItemID, b.LocationID, b.GroupInCharge, b.FundingSourceID
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0 $cond) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
				WHERE
						a.RecordStatus = 1
				ORDER BY
						a.ItemCode, b.RecordID
				";
            
            $arr_tmp_bulk_item = $linventory->returnArray($sql);
            
            $bulk_report_filter .= "<tr class=\"tabletop\"><td class=\"tablename\" bgcolor=\"#FFFFFF\" colspan=\"10\">$i_InventorySystem_ItemType_Bulk</td></tr>";
            $bulk_report_filter .= "<tr class=\"tabletop\">";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_Item_Code . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_Item_Name . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['Category'] . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['Location'] . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['Caretaker'] . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['FundingSource'] . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_ItemStatus_Normal . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_ItemStatus_Repair . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_ItemStatus_Damaged . "</td>";
            $bulk_report_filter .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
            $bulk_report_filter .= "</tr>";
            
            if (sizeof($arr_tmp_bulk_item) > 0) {
                for ($i = 0; $i < sizeof($arr_tmp_bulk_item); $i ++) {
                    list ($tmp_bulk_item_id, $tmp_bulk_item_location, $tmp_bulk_group_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$i];
                    
                    $sql = "SELECT 
								ITEM.ItemID,
								ITEM.ItemCode,
								CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								L.LocationID,
								CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								LOCATION.GroupInCharge,
								LOCATION.FundingSourceID
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
								INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID AND LOCATION.Locationid = $tmp_bulk_item_location and LOCATION.Quantity>0) LEFT OUTER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
						WHERE 
								ITEM.ItemID = $tmp_bulk_item_id AND
								ITEM.RecordStatus = 1 AND
								ITEM.ItemType = 2
								AND LOCATION.LocationID=$tmp_bulk_item_location AND LOCATION.GroupInCharge=$tmp_bulk_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
						LIMIT 0,1
						";
                    $arr_bulk_result = $linventory->returnArray($sql);
                    
                    if (is_array($arr_bulk_result) && count($arr_bulk_result) > 0) {
                        foreach ($arr_bulk_result as $Key => $Value) {
                            $qty_normal = "";
                            $qty_repair = "";
                            $qty_damage = "";
                            $date_modified = "";
                            
                            if ($i % 2 == 0)
                                $table_row_css = " class=\"tablebluerow1\" ";
                            else
                                $table_row_css = " class=\"tablebluerow2\" ";
                            
                            $bulk_report_filter .= "<tr class=\"bulk\">";
                            
                            list ($item_id, $item_code, $item_cat, $item_name, $location_id, $location, $resource_mgt_group, $bulk_funding, $bulk_group_id, $bulk_funding_id) = $Value;
                            $sql = "SELECT 
										if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
										if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
										if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
										BL.DateModified
								FROM
										INVENTORY_ITEM_BULK_LOG AS BL 
								WHERE
										BL.ItemID = $item_id AND
										BL.LocationID = $location_id AND
										BL.GroupInCharge=$bulk_group_id and BL.FundingSource=$bulk_funding_id and
										BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
								ORDER BY
										DateModified DESC, RecordID DESC
								LIMIT 0,1										
								";
                            $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                            
                            if (sizeof($tmp_bulk_item_status) > 0) {
                                list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                            }
                            
                            if ($qty_normal == "")
                                $qty_normal = " - ";
                            if ($qty_repair == "")
                                $qty_repair = " - ";
                            if ($qty_damage == "")
                                $qty_damage = " - ";
                            if ($date_modified == "")
                                $date_modified = " - ";
                            
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $item_code . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $item_name . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $item_cat . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $qty_normal . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $qty_repair . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $qty_damage . "</td>";
                            $bulk_report_filter .= "<td class=\"bulk tabletext\">" . $date_modified . "</td></tr>";
                        }
                    }
                }
            }
            if (sizeof($arr_tmp_bulk_item) == 0) {
                $bulk_report_filter .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\" bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
            }
            $bulk_report_filter .= "<tr><td colspan=\"9\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
            $bulk_report_filter .= "<br>";
        } else 
            if ($groupBy == 3) // #### Resources Management Group
{
                // # Toolbar - Print & Export
                $toolbar = "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetLocation','$targetGroup','$groupBy','$groupby_val','','')", "", "", "", "", 0);
                $toolbar .= $linterface->GET_LNK_EXPORT("item_status_export.php?targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val", "", "", "", "", 0) . "</td></tr>\n";
                
                if (is_array($targetGroup))
                    $cond = "WHERE AdminGroupID in (" . implode($targetGroup, ",") . ")";
                
                $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY AdminGroupID";
                $arr_group = $linventory->returnArray($sql, 2);
                
                for ($a = 0; $a < sizeof($arr_group); $a ++) {
                    list ($group_name, $admin_group_id) = $arr_group[$a];
                    
                    // single items #
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                        $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
                    }
                    
                    $sql = "SELECT 
							a.ItemID 
					FROM 
							INVENTORY_ITEM AS a INNER JOIN 
							INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID $cond) INNER JOIN 
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID)
					WHERE
							a.RecordStatus = 1 AND
							c.AdminGroupID = $admin_group_id
					ORDER BY
							a.ItemCode
					";
                    $arr_tmp_single_item = $linventory->returnArray($sql, 1);
                    
                    $group_table_content .= "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
                    $group_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">" . intranet_htmlspecialchars($group_name) . "</td></tr>";
                    $group_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" colspan=\"3\">" . $i_InventorySystemItemStatus . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
                    $group_table_content .= "</tr>";
                    
                    if (sizeof($arr_tmp_single_item) > 0) {
                        for ($i = 0; $i < sizeof($arr_tmp_single_item); $i ++) {
                            list ($tmp_single_item_id) = $arr_tmp_single_item[$i];
                            
                            $sql = "SELECT 
									DISTINCT ITEM.ItemID,
									ITEM.ItemCode,
									CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
									" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
									CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
									" . $linventory->getInventoryNameByLang("AG.") . ",
									" . $linventory->getInventoryNameByLang("funding.") . ",
									" . $linventory->getInventoryNameByLang("funding2.") . "
							FROM 
									INVENTORY_ITEM AS ITEM INNER JOIN 
									INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
									INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
									INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) INNER JOIN
									INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
									left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
									left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
							WHERE 
									ITEM.ItemType = 1 AND 
									ITEM.RecordStatus = 1 AND
									ITEM.ItemID = $tmp_single_item_id AND
									AG.AdminGroupID='" . $admin_group_id . "' 
							";
                            $arr_single_result = $linventory->returnArray($sql);
                            
                            if (sizeof($arr_single_result) > 0) {
                                for ($j = 0; $j < sizeof($arr_single_result); $j ++) {
                                    list ($single_item_id, $single_item_code, $item_cat, $single_item_name, $single_item_location, $single_item_group, $item_funding, $item_funding2) = $arr_single_result[$j];
                                    
                                    // if($i%2 == 0)
                                    // $table_row_css = " class=\"tablerow1\" ";
                                    // else
                                    // $table_row_css = " class=\"tablerow2\" ";
                                    
                                    $sql = "SELECT 
											NewStatus, DateModified 
									FROM 
											INVENTORY_ITEM_SINGLE_STATUS_LOG
									WHERE
											ItemID = $single_item_id AND
											ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
									ORDER BY
											DateModified DESC 
									LIMIT 0,1
									";
                                    $tmp_single_item_status = $linventory->returnArray($sql, 2);
                                    
                                    if (sizeof($tmp_single_item_status) > 0) {
                                        list ($single_item_status, $single_item_date_modified) = $tmp_single_item_status[0];
                                    }
                                    
                                    if ($single_item_status == "")
                                        $single_item_status = " - ";
                                    else 
                                        if ($single_item_status == ITEM_STATUS_NORMAL)
                                            $single_item_status = $i_InventorySystem_ItemStatus_Normal;
                                        else 
                                            if ($single_item_status == ITEM_STATUS_DAMAGED)
                                                $single_item_status = $i_InventorySystem_ItemStatus_Damaged;
                                            else 
                                                if ($single_item_status == ITEM_STATUS_REPAIR)
                                                    $single_item_status = $i_InventorySystem_ItemStatus_Repair;
                                                else
                                                    $single_item_status = " - ";
                                    
                                    $group_table_content .= "<tr $table_row_css><td class=\"single tabletext\">$single_item_code</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$single_item_name</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">" . $item_cat . "</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_location) . "</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($item_funding) . (($item_funding2) ? ", " . intranet_htmlspecialchars($item_funding2) : "") . "</td>";
                                    $group_table_content .= "<td class=\"single tabletext\" colspan=\"3\">$single_item_status</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$single_item_date_modified</td></tr>";
                                }
                            }
                        }
                    }
                    if (sizeof($arr_tmp_single_item) == 0) {
                        $group_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\" single tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                    }
                    
                    // bulk item #
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                        $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
                    }
                    
                    $sql = "SELECT 
							a.ItemID, b.LocationID, b.FundingSourceID
					FROM 
							INVENTORY_ITEM AS a INNER JOIN 
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0  $cond) INNER JOIN 
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID)
					WHERE
							a.RecordStatus = 1 AND
							c.AdminGroupID = $admin_group_id
					ORDER BY
							a.ItemCode, b.RecordID
					";
                    $arr_tmp_bulk_item = $linventory->returnArray($sql);
                    
                    $group_table_content .= "<tr class=\"tabletop\"><td class=\"tablename\" bgcolor=\"#FFFFFF\" colspan=\"9\">$i_InventorySystem_ItemType_Bulk</td></tr>";
                    $group_table_content .= "<tr class=\"tabletop\">";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_Item_Code . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_Item_Name . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['Category'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['Location'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['Caretaker'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem['FundingSource'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_ItemStatus_Normal . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_ItemStatus_Repair . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_InventorySystem_ItemStatus_Damaged . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
                    $group_table_content .= "</tr>";
                    
                    if (sizeof($arr_tmp_bulk_item) > 0) {
                        for ($i = 0; $i < sizeof($arr_tmp_bulk_item); $i ++) {
                            list ($tmp_bulk_item_id, $tmp_location_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$i];
                            $sql = "SELECT 
									DISTINCT ITEM.ItemID,
									ITEM.ItemCode,
									CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
									" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
									CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
									" . $linventory->getInventoryNameByLang("AG.") . ",
									" . $linventory->getInventoryNameByLang("funding.") . ",
									LOCATION.FundingSourceID,
									L.LocationID
							FROM 
									INVENTORY_ITEM AS ITEM INNER JOIN 
									INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
									INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
									INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0) LEFT OUTER JOIN
									INVENTORY_LOCATION AS L ON  (L.LocationID = LOCATION.LocationID $cond1 and LOCATION.LocationID=$tmp_location_id) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
									left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
							WHERE 
									ITEM.ItemType=2 AND 
									ITEM.ItemID = $tmp_bulk_item_id AND
									AG.AdminGroupID='" . $admin_group_id . "'
									AND LOCATION.GroupInCharge=$admin_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
							LIMIT 0,1
							";
                            
                            $arr_bulk_result = $linventory->returnArray($sql);
                            
                            if (sizeof($arr_bulk_result) > 0) {
                                for ($j = 0; $j < sizeof($arr_bulk_result); $j ++) {
                                    list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $bulk_funding, $bulk_funding_id, $location_id) = $arr_bulk_result[$j];
                                    
                                    $qty_normal = "";
                                    $qty_repair = "";
                                    $qty_damage = "";
                                    $date_modified = "";
                                    
                                    // if($i%2 == 0)
                                    // $table_row_css = " class=\"tablerow1\" ";
                                    // else
                                    // $table_row_css = " class=\"tablerow2\" ";
                                    
                                    $sql = "SELECT 
											if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
											if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
											if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
											BL.DateModified
									FROM
											INVENTORY_ITEM_BULK_LOG AS BL 
									WHERE
											BL.ItemID = $item_id AND
											BL.GroupInCharge = $admin_group_id AND
											BL.LocationID = $location_id AND BL.FundingSource=$bulk_funding_id and
											BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
									ORDER BY
											DateModified DESC, RecordID DESC
									LIMIT 0,1										
									";
                                    
                                    $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                                    
                                    if (sizeof($tmp_bulk_item_status) > 0) {
                                        list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                                    }
                                    
                                    if ($qty_normal == "")
                                        $qty_normal = " - ";
                                    if ($qty_repair == "")
                                        $qty_repair = " - ";
                                    if ($qty_damage == "")
                                        $qty_damage = " - ";
                                    if ($date_modified == "")
                                        $date_modified = " - ";
                                    
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $item_code . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $item_name . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $item_cat . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $qty_normal . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $qty_repair . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $qty_damage . "</td>";
                                    $group_table_content .= "<td class=\"bulk tabletext\">" . $date_modified . "</td></tr>";
                                }
                            }
                        }
                    }
                    if (sizeof($arr_tmp_bulk_item) == 0) {
                        $group_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\"bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                    }
                    $group_table_content .= "<tr><td colspan=\"9\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
                    $group_table_content .= "</table><br><br>";
                }
            } else 
                if ($groupBy == 4) // #### Category
{
                    $single_empty = 0;
                    $bulk_empty = 0;
                    
                    if (is_array($targetSubCategory)) {
                        $sub_cat_id_list = implode(",", $targetSubCategory);
                        $sub_cat_cond = "Category2ID IN ($sub_cat_id_list)";
                    }
                    
                    $location_filter .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>";
                    $location_filter .= "<tr>";
                    $location_filter .= "<td>";
                    
                    $building_selection = $llocation_ui->Get_Building_Selection($TargetBuilding, 'TargetBuilding', 'resetLocationSelection(\'Building\'); this.form.step.value=\'final\'; this.form.submit();', 0, 0, '');
                    $location_filter .= $building_selection;
                    
                    if ($TargetBuilding != "") {
                        $floor_selection = $llocation_ui->Get_Floor_Selection($TargetBuilding, $TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.step.value=\'final\'; this.form.submit();', 0, 0, '');
                        
                        $location_filter .= $floor_selection;
                    }
                    if (($TargetBuilding != "") && ($TargetFloor != "")) {
                        
                        $room_selection .= $llocation_ui->Get_Room_Selection($TargetFloor, $TargetSubLocation, 'TargetSubLocation', 'resetLocationSelection(\'Room\'); this.form.step.value=\'final\'; this.form.submit();', 0, 0, '');
                        $location_filter .= $room_selection;
                    }
                    $location_filter .= "</td></tr></table><BR>";
                    
                    $cat_table_content .= $location_filter;
                    
                    // # Toolbar - Print & Export
                    // $toolbar .= "<tr><td class=\"tabletext\">".$linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetLocation','$targetGroup','$groupBy','$groupby_val','','$TargetSubLocation')","","","","",0)."&nbsp;&nbsp;&nbsp;";
                    // $toolbar .= $linterface->GET_LNK_EXPORT("item_status_export.php?targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val&SubLocationFilter=$TargetSubLocation","","","","",0)."&nbsp;&nbsp;&nbsp;</td></tr>\n";
                    
                    for ($i = 0; $i < sizeof($targetSubCategory); $i ++) {
                        $targetSubCatID = $targetSubCategory[$i];
                        $sql = "SELECT CONCAT(" . $linventory->getInventoryNameByLang("a.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ") FROM INVENTORY_CATEGORY AS a INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) WHERE Category2ID = '".$targetSubCatID."' ORDER BY a.DisplayOrder, b.DisplayOrder";
                        $CategoryFullName = $linventory->returnVector($sql);
                        
                        $cat_table_content .= "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
                        $cat_table_content .= "<tr><td colspan=\"10\" class=\"tablename\" >" . $CategoryFullName[0] . "</td></tr>";
                        $cat_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\" colspan=\"3\">" . $i_InventorySystemItemStatus . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
                        $cat_table_content .= "</tr>";
                        
                        // single items #
                        $sql = "SELECT 
							a.ItemID 
					FROM 
							INVENTORY_ITEM AS a
					WHERE
							a.RecordStatus = 1 AND
							a.ItemType = 1 AND 
							a.Category2ID IN ($targetSubCatID)
					ORDER BY
							a.ItemCode
					";
                        $arr_tmp_single_item = $linventory->returnArray($sql, 1);
                        if (sizeof($arr_tmp_single_item) > 0) {
                            for ($j = 0; $j < sizeof($arr_tmp_single_item); $j ++) {
                                list ($tmp_single_item_id) = $arr_tmp_single_item[$j];
                                
                                if ($TargetSubLocation != "") {
                                    $Cond_SubLocation = " AND L.LocationID IN ($TargetSubLocation) ";
                                } else 
                                    if (($TargetBuilding != "") || ($TargetFloor != "")) {
                                        if ($TargetBuilding != "")
                                            $cond_floor = " AND building.BuildingID = '".$TargetBuilding."' ";
                                        if ($TargetFloor != "")
                                            $cond_room = " AND floor.LocationLevelID = '".$TargetFloor."' ";
                                        $sql = "SELECT 
									room.LocationID 
								FROM 
									INVENTORY_LOCATION_BUILDING AS building 
									INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
									INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
								WHERE
									building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1 
									$cond_floor $cond_room";
                                        $arrTargetRoom = $linventory->returnVector($sql);
                                        if (sizeof($arrTargetRoom) > 0) {
                                            $TargetSubLocation = implode(",", $arrTargetRoom);
                                            $Cond_SubLocation = " AND L.LocationID IN ($TargetSubLocation) ";
                                        }
                                    }
                                
                                $sql = "SELECT 
									DISTINCT ITEM.ItemID,
									ITEM.ItemCode,
									CONCAT(" . $linventory->getInventoryNameByLang("CAT.") . ",'>'," . $linventory->getInventoryNameByLang("SUBCAT.") . "),
									" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
									CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
									" . $linventory->getInventoryNameByLang("AG.") . ",
									" . $linventory->getInventoryNameByLang("funding.") . ",
									" . $linventory->getInventoryNameByLang("funding2.") . "
							FROM 
									INVENTORY_ITEM AS ITEM INNER JOIN 
									INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
									INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
									INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) LEFT OUTER JOIN
									INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
									left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
									left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
							WHERE 
									ITEM.ItemID = $tmp_single_item_id AND
									ITEM.RecordStatus = 1 AND
									ITEM.ItemType=1 AND 
									ITEM.Category2ID='" . $targetSubCatID . "' 
									$Cond_SubLocation 
							LIMIT 0,1";
                                $arr_single_result = $linventory->returnArray($sql);
                                
                                if (is_array($arr_single_result) && count($arr_single_result) > 0) {
                                    foreach ($arr_single_result as $Key => $Value) {
                                        // if($i%2 == 0)
                                        // $table_row_css = " class=\"tablebluerow1\" ";
                                        // else
                                        // $table_row_css = " class=\"tablebluerow2\" ";
                                        $location_table_content .= "<tr bgcolor=\"#FFFFCC\">";
                                        
                                        list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $item_funding, $item_funding2) = $Value;
                                        
                                        $sql = "SELECT 
											NewStatus, DateModified 
									FROM 
											INVENTORY_ITEM_SINGLE_STATUS_LOG
									WHERE
											ItemID = $item_id AND
											Action IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
									ORDER BY
											DateModified DESC 
									LIMIT 0,1
									";
                                        $tmp_single_item_status = $linventory->returnArray($sql, 2);
                                        
                                        if (sizeof($tmp_single_item_status) > 0) {
                                            list ($item_status, $date_modified) = $tmp_single_item_status[0];
                                        }
                                        
                                        if ($item_status == "")
                                            $status = " - ";
                                        else 
                                            if ($item_status == ITEM_STATUS_NORMAL)
                                                $status = $i_InventorySystem_ItemStatus_Normal;
                                            else 
                                                if ($item_status == ITEM_STATUS_DAMAGED)
                                                    $status = $i_InventorySystem_ItemStatus_Damaged;
                                                else 
                                                    if ($item_status == ITEM_STATUS_REPAIR)
                                                        $status = $i_InventorySystem_ItemStatus_Repair;
                                                    else
                                                        $status = " - ";
                                        
                                        $cat_table_content .= "<td class=\"single tabletext\">" . $item_code . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\">" . $item_name . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\">" . $item_cat . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($item_funding) . (($item_funding2) ? ", " . intranet_htmlspecialchars($item_funding2) : "") . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\" colspan=\"3\">" . $status . "</td>";
                                        $cat_table_content .= "<td class=\"single tabletext\">" . $date_modified . "</td></tr>";
                                    }
                                } else {
                                    $single_empty ++;
                                }
                            }
                        }
                        if ((sizeof($arr_tmp_single_item) == 0) || (sizeof($arr_tmp_single_item) == $single_empty)) {
                            $cat_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\" single tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                        }
                        // single items end #
                        
                        // Bulk item start #
                        $sql = "SELECT 
							a.ItemID, b.LocationID, b.GroupInCharge, b.FundingSourceID  
					FROM 
							INVENTORY_ITEM AS a INNER JOIN 
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID $cond)
					WHERE
							a.RecordStatus = 1 AND
							a.ItemType = 2 AND 
							a.Category2ID IN ($targetSubCatID)
					ORDER BY
							a.ItemCode, b.RecordID
					";
                        $arr_tmp_bulk_item = $linventory->returnArray($sql);
                        
                        $cat_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_ItemStatus_Normal . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_ItemStatus_Damaged . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_ItemStatus_Repair . "</td>";
                        $cat_table_content .= "<td class=\"tabletopnolink\" >" . $i_LastModified . "</td>";
                        $cat_table_content .= "</tr>";
                        if (sizeof($arr_tmp_bulk_item) > 0) {
                            for ($j = 0; $j < sizeof($arr_tmp_bulk_item); $j ++) {
                                list ($tmp_bulk_item_id, $tmp_bulk_item_location, $tmp_bulk_group_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$j];
                                
                                if ($TargetSubLocation != "") {
                                    $Cond_SubLocation = " AND L.LocationID IN ($TargetSubLocation) ";
                                } else 
                                    if (($TargetBuilding != "") || ($TargetFloor != "")) {
                                        if ($TargetBuilding != "")
                                            $cond_floor = " AND building.BuildingID = '".$TargetBuilding."' ";
                                        if ($TargetFloor != "")
                                            $cond_room = " AND floor.LocationLevelID = '".$TargetFloor."' ";
                                        $sql = "SELECT 
									room.LocationID 
								FROM 
									INVENTORY_LOCATION_BUILDING AS building 
									INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
									INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
								WHERE
									building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1 
									$cond_floor $cond_room";
                                        $arrTargetRoom = $linventory->returnVector($sql);
                                        
                                        if (sizeof($arrTargetRoom) > 0) {
                                            $TargetSubLocation = implode(",", $arrTargetRoom);
                                            $Cond_SubLocation = " AND L.LocationID IN ($TargetSubLocation) ";
                                        }
                                    }
                                
                                /*
                                 * $sql = "SELECT
                                 * DISTINCT ITEM.ItemID,
                                 * ITEM.ItemCode,
                                 * CONCAT(".$linventory->getInventoryNameByLang("CAT.").",'>',".$linventory->getInventoryNameByLang("SUBCAT.")."),
                                 * ".$linventory->getInventoryItemNameByLang("ITEM.").",
                                 * L.LocationID,
                                 * CONCAT(".$linventory->getInventoryItemNameByLang("building.").",'>',".$linventory->getInventoryItemNameByLang("floor.").",'>',".$linventory->getInventoryItemNameByLang("L.")."),
                                 * ".$linventory->getInventoryNameByLang("AG.")."
                                 * FROM
                                 * INVENTORY_ITEM AS ITEM INNER JOIN
                                 * INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
                                 * INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
                                 * INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0) LEFT OUTER JOIN
                                 * INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
                                 * INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
                                 * INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
                                 * INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2)
                                 * WHERE
                                 * ITEM.ItemID = $tmp_bulk_item_id AND
                                 * ITEM.RecordStatus = 1 AND
                                 * ITEM.ItemType = 2 AND
                                 * ITEM.Category2ID='".$targetSubCatID."'
                                 * $Cond_SubLocation
                                 * LIMIT 0,1
                                 * ";
                                 */
                                
                                $sql = "SELECT 
									DISTINCT ITEM.ItemID,
									ITEM.ItemCode,
									CONCAT(" . $linventory->getInventoryNameByLang("CAT.") . ",'>'," . $linventory->getInventoryNameByLang("SUBCAT.") . "),
									" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
									L.LocationID,
									CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
									" . $linventory->getInventoryNameByLang("AG.") . ",
									" . $linventory->getInventoryNameByLang("funding.") . ",
									LOCATION.GroupInCharge,
									LOCATION.FundingSourceID,
									L.LocationID
							FROM 
									INVENTORY_ITEM AS ITEM INNER JOIN 
									INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
									INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
									INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0) LEFT OUTER JOIN
									INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
									left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
							WHERE 
									ITEM.ItemID = $tmp_bulk_item_id AND
									ITEM.RecordStatus = 1 AND
									ITEM.ItemType = 2 AND 
									ITEM.Category2ID='" . $targetSubCatID . "' 
									AND LOCATION.LocationID=$tmp_bulk_item_location AND LOCATION.GroupInCharge=$tmp_bulk_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
									$Cond_SubLocation 
							";
                                $arr_bulk_result = $linventory->returnArray($sql);
                                
                                if (is_array($arr_bulk_result) && count($arr_bulk_result) > 0) {
                                    foreach ($arr_bulk_result as $Key => $Value) {
                                        $qty_normal = "";
                                        $qty_repair = "";
                                        $qty_damage = "";
                                        $date_modified = "";
                                        
                                        // if($i%2 == 0)
                                        // $table_row_css = " class=\"tablebluerow1\" ";
                                        // else
                                        // $table_row_css = " class=\"tablebluerow2\" ";
                                        $location_table_content .= "<tr bgcolor=\"#FFFFCC\">";
                                        
                                        list ($item_id, $item_code, $item_cat, $item_name, $locationID, $location, $resource_mgt_group, $bulk_funding, $bulk_group_id, $bulk_funding_id, $location_id) = $Value;
                                        
                                        $sql = "SELECT 
											if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
											if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
											if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
											BL.DateModified
									FROM
											INVENTORY_ITEM_BULK_LOG AS BL 
									WHERE
											BL.ItemID = $item_id AND
											BL.LocationID = $locationID AND
											BL.GroupInCharge=$bulk_group_id and BL.FundingSource=$bulk_funding_id and
											BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
									ORDER BY
											DateModified DESC, RecordID DESC
									LIMIT 0,1										
									";
                                        
                                        $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                                        
                                        if (sizeof($tmp_bulk_item_status) > 0) {
                                            list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                                        }
                                        
                                        if ($qty_normal == "")
                                            $qty_normal = " - ";
                                        if ($qty_repair == "")
                                            $qty_repair = " - ";
                                        if ($qty_damage == "")
                                            $qty_damage = " - ";
                                        if ($date_modified == "")
                                            $date_modified = " - ";
                                        
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $item_code . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $item_name . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $item_cat . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($resource_mgt_group) . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $qty_normal . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $qty_damage . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $qty_repair . "</td>";
                                        $cat_table_content .= "<td class=\"bulk tabletext\">" . $date_modified . "</td></tr>";
                                    }
                                } else {
                                    $bulk_empty ++;
                                }
                            }
                        }
                        if ((sizeof($arr_tmp_bulk_item) == 0) || (sizeof($arr_tmp_bulk_item) == $bulk_empty)) {
                            $cat_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"10\" class=\" bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                        }
                        $cat_table_content .= "<tr><td colspan=\"10\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
                        $cat_table_content .= "</table><br><br>";
                    }
                    
                    // # Toolbar - Print & Export
                    $toolbar = "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetLocation','$targetGroup','$groupBy','$groupby_val','','$TargetSubLocation')", "", "", "", "", 0);
                    $toolbar .= $linterface->GET_LNK_EXPORT("item_status_export.php?targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val&SubLocationFilter=$TargetSubLocation", "", "", "", "", 0) . "</td></tr>\n";
                }
}

?>

<script language="javascript">

function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("TargetFloor")){
			document.getElementById("TargetFloor").value = "";
		}
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
}

function openPrintPage(TargetLocation, TargetGroup, GroupBy, GroupByValue, SubCatFilter, SubLocationFilter)
{
	newWindow("item_status_print.php?targetLocation="+TargetLocation+"&targetGroup="+TargetGroup+"&groupBy="+GroupBy+"&selected_cb="+GroupByValue+"&SubCatFilter="+SubCatFilter+"&SubLocationFilter="+SubLocationFilter,10);
}

function checkForm()
{
	var groupby = document.getElementById('groupBy').value;
	var location_cb = 0;
	var group_cb = 0;
	var sub_cat_cb = 0;
	
	if(groupby == 1) {
		if(document.getElementById('TargetRoom[]')) {
			for(i=0; i<document.getElementById('TargetRoom[]').length; i++) {
				if(document.getElementById('TargetRoom[]')[i].selected == true) {
					if(document.getElementById('TargetRoom[]')[i].value != "") {
						location_cb++;
					}
				}
			}
		}
	}
	if(groupby == 3){
		if(document.getElementById('targetGroup[]')) {
			for(i=0; i<document.getElementById('targetGroup[]').length; i++) {
				if(document.getElementById('targetGroup[]')[i].selected == true) {
					if(document.getElementById('targetGroup[]')[i].value != "") {
						group_cb++;
					}
				}
			}
		}
	}
	if(groupby == 4){
		if(document.getElementById('targetSubCategory[]')) {
			for(i=0; i<document.getElementById('targetSubCategory[]').length; i++) {
				if(document.getElementById('targetSubCategory[]')[i].selected == true) {
					if(document.getElementById('targetSubCategory[]')[i].value != "") {
						sub_cat_cb++;
					}
				}
			}
		}
	}
		
	if(location_cb>0||group_cb>0||groupby==2||sub_cat_cb>0){
		document.form1.step.value = "final";
		return true;
	}
	else if(groupby==1)
	{
		alert('<?=$i_InventorySystem['jsLocationCheckBox']?>');
		return false;
	}
	else if(groupby==3)
	{
		alert('<?=$i_InventorySystem['jsGroupCheckBox']?>');
		return false;	
	}
	else if(groupby=="")
	{
		alert('<?=$i_InventorySystem['jsLocationGroupCheckBox']?>');
		return false;	
	}
	else if(groupby==4)
	{
		alert('Please select a category');
		return false;	
	}
}
</script>

<br>
<form name="form1" action="" method="post" onSubmit="return checkForm()">
	<table border="0" width="96%" cellspacing="0" cellpadding="5"
		align="center">
<?=$report_filter;?>
</table>
	<br>

<? if(($groupBy != "") && ($step == "final")){ ?>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$toolbar;?>
</table>
	<br>
<? } ?>

<?
if (($groupBy == 1) && ($step == "final"))
    echo $location_table_content;
else 
    if (($groupBy == 3) && ($step == "final"))
        echo $group_table_content;
    else 
        if (($groupBy == 4) && ($step == "final"))
            echo $cat_table_content;
?>

<? if(($groupBy==2)&&($step == "final")){ ?>
<table border="0" width="100%" align="center" cellspacing="0"
		cellpadding="5">
<?=$single_report_filter;?>
</table>
	<table border="0" width="100%" align="center" cellspacing="0"
		cellpadding="5">
<?=$bulk_report_filter;?>
</table>
<? } ?>

<input type="hidden" name="step" value="">
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>