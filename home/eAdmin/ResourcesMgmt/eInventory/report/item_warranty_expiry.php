<?php

// using:

// ############################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2017-12-19 Cameron
// redirect to reports.php in PowerClass with error message if basic setting is not setup
//
// Date: 2015-11-02 Cameron
// Add filter by expiry days option list
//
// Date: 2012-03-07 YatWoon
// Fixed: Should be displayed all the expired items, not coming x days expired items
//
// ############################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Report_WarrantyExpiry";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Check user have set all the basic setting ###
$sql = "SELECT LocationID FROM INVENTORY_LOCATION";
$arr_location_check = $linventory->returnVector($sql);

$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
$arr_group_check = $linventory->returnVector($sql);

$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY";
$arr_category_check = $linventory->returnVector($sql);

$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE";
$arr_funding_check = $linventory->returnVector($sql);

if ((sizeof($arr_location_check) == 0) || (sizeof($arr_group_check) == 0) || (sizeof($arr_category_check) == 0) || (sizeof($arr_funding_check) == 0)) {
    if ($sys_custom['PowerClass']) {
        header("Location: " . $PATH_WRT_ROOT . "home/PowerClass/reports.php?error=MissingBasicSetting");
    } else {
        header("Location: " . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php");
    }
}
// ## END ###

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_WarrantyExpiry,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$warranty_expire = ($warranty_expire == '') ? 0 : $warranty_expire;

$toolbar .= "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage()", "", "", "", "", 0);
$toolbar .= $linterface->GET_LNK_EXPORT("item_warranty_expiry_export.php?warranty_expire=$warranty_expire", "", "", "", "", 0) . "&nbsp;&nbsp;&nbsp;</td></tr>";

$curr_date = date("Y-m-d");
$DayPeriod = $linventory->getWarrantyExpiryWarningDayPeriod();
$date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $DayPeriod, date("Y")));

$arr_warranty_expire = array();
$arr_warranty_expire[] = array(
    0,
    $i_InventorySystem_Report_Filter_Expired
);
$arr_warranty_expire[] = array(
    15,
    $i_InventorySystem_Report_Filter_Expire_in_15
);
$arr_warranty_expire[] = array(
    30,
    $i_InventorySystem_Report_Filter_Expire_in_30
);
$arr_warranty_expire[] = array(
    50,
    $i_InventorySystem_Report_Filter_Expire_in_50
);

$opt_warranty_expire = getSelectByArray($arr_warranty_expire, "name=\"warranty_expire\" onChange=\"this.form.submit();\"", $warranty_expire, 0, 1);

$filterbar = "
	<tr>
		<td align=\"left\">
			$opt_warranty_expire
		</td>
	</tr>";

if ($linventory->IS_ADMIN_USER($UserID))
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
else
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";

$tmp_arr_group = $linventory->returnVector($sql);
if (sizeof($tmp_arr_group) > 0) {
    $target_admin_group = implode(",", $tmp_arr_group);
}

$cond = '';
if ($target_admin_group != "") {
    $cond .= " AND c.AdminGroupID IN ($target_admin_group) ";
}

if ($warranty_expire == 0) {
    $cond .= " AND b.WarrantyExpiryDate <= '$curr_date' ";
} else {
    $cond .= " AND b.WarrantyExpiryDate <= DATE_ADD(CURDATE(),INTERVAL $warranty_expire DAY) ";
    $cond .= " AND b.WarrantyExpiryDate > '$curr_date' ";
}

// b.WarrantyExpiryDate BETWEEN '$curr_date' AND '$date_range'
$sql = "SELECT 
					a.ItemCode, 
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("d.") . "),
					b.WarrantyExpiryDate
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '0000-00-00') INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS floor ON (d.LocationLevelID = floor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
			WHERE 
					1
					$cond
			ORDER BY
					b.WarrantyExpiryDate";
$arr_expiry = $linventory->returnArray($sql);

$table_content .= "<tr class=\"tabletop\">";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Report_ItemName</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Group_Name</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Report_WarrantyExpiryDate</td>";
$table_content .= "</tr>";

if (sizeof($arr_expiry) > 0) {
    for ($i = 0; $i < sizeof($arr_expiry); $i ++) {
        list ($item_code, $item_name, $item_group, $item_location, $item_warranty_expiry) = $arr_expiry[$i];
        
        $j = $i + 1;
        if ($j % 2 == 0)
            $table_row_css = " class=\"tablebluerow1\" ";
        else
            $table_row_css = " class=\"tablebluerow2\" ";
        
        $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\">$item_code</td>";
        $table_content .= "<td class=\"tabletext\">$item_name</td>";
        $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($item_group) . "</td>";
        $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($item_location) . "</td>";
        $table_content .= "<td class=\"tabletext\">$item_warranty_expiry</td></tr>";
        $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=5></td></tr>";
    }
}
if (sizeof($arr_expiry) == 0) {
    $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
    $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=5></td></tr>";
}
// $table_content .= "<tr class=\"tablebluebottom\" height=\"20px\"><td colspan=\"5\"></td></tr>";

?>
<script language="javascript">
function openPrintPage()
{
     newWindow("item_warranty_expiry_print.php?warranty_expire=<?=$warranty_expire?>",10);
}
</script>
<br>
<form name="form1" action="item_warranty_expiry.php" method="POST">
	<table border="0" width="100%" cellspacing="0" cellpadding="5">
<?=$toolbar;?>
</table>

	<table border="0" width="90%" cellspacing="0" cellpadding="5">
<?=$filterbar;?>
</table>

	<table border="0" width="100%" cellspacing="0" cellpadding="5">
<?=$table_content;?>
</table>
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td class="dotline"><img src="<?=$image_path?>/2007a/10x10.gif"
				width="10" height="5"></td>
		</tr>
	</table>
	<br>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>