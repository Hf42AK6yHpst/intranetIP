<?php
// using: 

// #####################################
//
// Date: 2019-06-24 (Tommy)
// add "funding source" selection
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
// 
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2017-12-22 Henry
// added column to show the item attachment [Case#Y133022]
//
// Date: 2017-12-19 Cameron
// redirect to reports.php in PowerClass with error message if basic setting is not setup
//
// Date: 2013-02-19 YatWoon
// Fixed: Failed to display bulk item funding name [Case#2013-0131-1431-57073]
//
// Date: 2013-01-16 YatWoon
// Fixed: Display item's funding instead of write-off record's funding data [Case#:2013-0107-1503-41073]
//
// Date: 2012-07-10 YatWoon
// Enhanced: support 2 funding source for single item
//
// Date: 2012-05-29 [YatWoon]
// Improved: add funding column
//
// Date: 2012-03-07 [YatWoon]
// Improved: display default date range as "last year" to "today"
//
// Date: 2011-11-30 [YatWoon]
// Improved: display deleted user name [Case#2011-1111-1515-17132]
//
// Date: 2011-11-07 YatWoon
// Improved: Add "Date Type" [Case#2011-1013-1705-11066]
//
// Date: 2011-06-15 YatWoon
// Improved: add sortby option [Case#2010-1214-1004-44096]
//
// Date: 2011-05-26 YatWoon
// Fixed: N attachment will display N records
//
// Date: 2011-05-17 YatWoon
// enhanced: export details
//
// Date: 2011-04-26 YatWoon
// improved: display format
//
// Date: 2011-03-16 YatWoon
// change wordings from "Deleted Staff" to "User account not exists."
//
// #####################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Report_WrittenOffItems";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui = new liblocation_ui();

// Init JS for date picker #
$js_init .= '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.js"></script>';
$js_init .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.css" type="text/css" />';

// ## Check user have set all the basic setting ###
$sql = "SELECT LocationID FROM INVENTORY_LOCATION";
$arr_location_check = $linventory->returnVector($sql);

$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
$arr_group_check = $linventory->returnVector($sql);

$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY";
$arr_category_check = $linventory->returnVector($sql);

$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE";
$arr_funding_check = $linventory->returnVector($sql);

if ((sizeof($arr_location_check) == 0) || (sizeof($arr_group_check) == 0) || (sizeof($arr_category_check) == 0) || (sizeof($arr_funding_check) == 0)) {
    if ($sys_custom['PowerClass']) {
        header("Location: " . $PATH_WRT_ROOT . "home/PowerClass/reports.php?error=MissingBasicSetting");
    } else {
        header("Location: " . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php");
    }
}
// ## END ###

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_ItemWrittenOff,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// $stocktake_start_date = $linventory->getStocktakePeriodStart();
// $stocktake_end_date = $linventory->getStocktakePeriodEnd();

$tmp_end_date = date("Y-m-d");
$tmp_start_date = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y") - 1));

if ($start_date == "") {
    $start_date = $tmp_start_date;
}
if ($end_date == "") {
    $end_date = $tmp_end_date;
}

// # Date Range ##
$report_filter .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">$i_InventorySystem_Setting_StockCheckDateRange</td>";
$report_filter .= "<td class=\"tabletext\">
						$i_InventorySystem_From &nbsp; <input type=\"text\" name=\"start_date\" id=\"start_date\" class=\"textboxnum\" maxlength=\"10\" value=\"$start_date\"> 
						$i_InventorySystem_To &nbsp; <input type=\"text\" name=\"end_date\" id=\"end_date\" class=\"textboxnum\" maxlength=\"10\" value=\"$end_date\">
					</td></tr>\n";

$report_filter .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['DateType'] . "</td>";
$report_filter .= "<td><input type='radio' name='DateType' value='RequestDate' id='DateType1' " . ($DateType != "ApproveDate" ? "checked" : "") . "><label for='DateType1'>" . $i_InventorySystem_Write_Off_RequestTime . "</label> <input type='radio' name='DateType' value='ApproveDate' id='DateType2' " . ($DateType == "ApproveDate" ? "checked" : "") . "><label for='DateType2'>" . $i_InventorySystem_ApproveDate . "</label> </td>";
$report_filter .= "</tr>";

$arr_groupBy = array(
    array(
        1,
        $i_InventorySystem['Location']
    ),
    array(
        2,
        $i_InventorySystem['item_type']
    ),
    array(
        3,
        $i_InventorySystem['Caretaker']
    ),
    array(
        4,
        $i_InventorySystem['FundingSource']
    )
);
$groupby_selection = getSelectByArray($arr_groupBy, "name=\"groupBy\" id=\"groupBy\" onChange=\"this.form.submit();\" ", $groupBy);

// # report - group by
$report_filter .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">$i_InventorySystem_Report_Stocktake_groupby</td>";
$report_filter .= "<td>$groupby_selection</td></tr>\n";

// # group by - location
if ($groupBy == 1) {
    // $building_selection = $llocation_ui->Get_Building_Selection($TargetBuilding, 'TargetBuilding', 'resetLocationSelection(\'Building\'); this.form.submit();', 0, 0, '');
    
    $report_filter .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $i_InventorySystem['Location'] . "</td>";
    $report_filter .= "<td>" . $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '');
    
    if ($TargetFloor != "") {
        $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$TargetFloor."' AND RecordStatus = 1";
        $result = $linventory->returnArray($sql, 2);
        
        $room_selection = "<select id=\"TargetRoom[]\" name=\"TargetRoom[]\" multiple size=\"10\">";
        if (sizeof($TargetRoom) == 0) {
            $room_selection .= "<option value=\"\" selected> - select room - </option>";
        } else {
            $room_selection .= "<option value=\"\"> - select room - </option>";
        }
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($location_id, $location_name) = $result[$i];
                
                $selected = "";
                if (is_array($TargetRoom)) {
                    if (in_array($location_id, $TargetRoom)) {
                        $selected = " SELECTED ";
                    }
                }
                $room_selection .= "<option value=\"$location_id\" $selected>$location_name</option>";
            }
        }
        $room_selection .= "</select>";
        
        // $report_filter .= "<tr><td></td>";
        // $report_filter .= "<td>".$room_selection."</td>";
        $report_filter .= "<br>" . $room_selection;
    }
    
    $report_filter .= "</td></tr>";
}
// # group by - resource mgmt group
if ($groupBy == 3) {
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
    $result = $linventory->returnArray($sql, 2);
    
    $group_selection = "<select id=\"targetGroup[]\" name=\"targetGroup[]\" multiple size=\"10\">";
    if (sizeof($targetGroup) == 0) {
        $group_selection .= "<option value=\"\" selected> - select group - </option>";
    } else {
        $group_selection .= "<option value=\"\"> - select group - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($group_id, $group_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetGroup)) {
                if (in_array($group_id, $targetGroup)) {
                    $selected = " SELECTED ";
                }
            }
            $group_selection .= "<option value=\"$group_id\" $selected>$group_name</option>";
        }
    }
    $group_selection .= "</select>";
    
    $report_filter .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $i_InventorySystem['Location'] . "</td>";
    $report_filter .= "<td>" . $group_selection . "</td>";
}

// groupBy4(funding source) selection
if ($groupBy == 4) {
    $sql = "SELECT FundingSourceID, " . $linventory->getInventoryNameByLang() . " as FundingSourceName FROM INVENTORY_FUNDING_SOURCE WHERE RecordStatus !=0 ORDER BY DisplayOrder";
    $result = $linventory->returnArray($sql, 2);
    
    $funding_source_selection = "<select id=\"targetFundSource[]\" name=\"targetFundSource[]\" multiple size=\"10\">";
    if (sizeof($targetFundSource) == 0) {
        $funding_source_selection .= "<option value=\"\" selected> - select funding source - </option>";
    } else {
        $funding_source_selection .= "<option value=\"\"> - select funding source - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($funding_source_id, $funding_source_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetFundSource)) {
                if (in_array($funding_source_id, $targetFundSource)) {
                    $selected = " SELECTED ";
                }
            }
            $funding_source_selection .= "<option value=\"$funding_source_id\" $selected>$funding_source_name</option>";
        }
    }
    $funding_source_selection .= "</select>";
    
    $report_filter .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $i_InventorySystem['FundingSource'] . "</td>";
    $report_filter .= "<td>" . $funding_source_selection . "</td>";
}

// ## Sort By
$sort_by = "<select name='sortby'>";
$sort_by .= "<option value='ItemCode' " . ($sortby == "ItemCode" ? "selected" : "") . ">" . $i_InventorySystem_Item_Code . "</option>";
$sort_by .= "<option value='ItemName' " . ($sortby == "ItemName" ? "selected" : "") . ">" . $i_InventorySystem_Item_Name . "</option>";
$sort_by .= "<option value='Location' " . ($sortby == "Location" ? "selected" : "") . ">" . $i_InventorySystem_Item_Location . "</option>";
$sort_by .= "<option value='ResourceMgmtGroup' " . ($sortby == "ResourceMgmtGroup" ? "selected" : "") . ">" . $i_InventorySystem['Caretaker'] . "</option>";
$sort_by .= "<option value='Qty' " . ($sortby == "Qty" ? "selected" : "") . ">" . $i_InventorySystem_WriteOffQty . "</option>";
$sort_by .= "<option value='RequestDate' " . ($sortby == "RequestDate" ? "selected" : "") . ">" . $i_InventorySystem_Write_Off_RequestTime . "</option>";
$sort_by .= "<option value='ApproveDate' " . ($sortby == "ApproveDate" ? "selected" : "") . ">" . $i_InventorySystem_ApproveDate . "</option>";
$sort_by .= "</select>";
$report_filter .= "<tr><td class='field_title'>" . $Lang['eInventory']['SortBy'] . "</td>";

$report_filter .= "<td>" . $sort_by . "</td></tr>";

if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND a.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND a.GroupInCharge = $targetGroup ";
}
if ($targetLocationLevel == "") {
    $cond3 = " ";
} else {
    $cond3 = " AND c.LocationLevelID = $targetLocationLevel ";
}
if ($targetFundSource == ""){
    $cond4 = " ";    
}else{
    $cond4 = " AND ifs.FundingSourceID = $targetFundSource";
}

// # Start to gen report
if ($step == "final") {
    
    switch ($sortby) {
        case "ItemCode":
            $orderby = "b.ItemCode";
            break;
        case "ItemName":
            $orderby = "this_item_name";
            break;
        case "Location":
            $orderby = "building.DisplayOrder, floor.DisplayOrder, c.DisplayOrder";
            break;
        case "ResourceMgmtGroup":
            $orderby = "d.DisplayOrder";
            break;
        case "Qty":
            $orderby = "a.WriteOffQty";
            break;
        case "RequestDate":
            $orderby = "a.RequestDate";
            break;
        case "ApproveDate":
            $orderby = "a.ApproveDate";
            break;
    }
    
    if (sizeof($TargetRoom)) {
        $groupby_val = implode(",", $TargetRoom);
    }
    if (sizeof($targetGroup)) {
        $groupby_val = implode(",", $targetGroup);
    }
    if(sizeof($targetFundSource)){
        $groupby_val = implode(",", $targetFundSource);
    }
    // $toolbar .= "<tr><td class=\"tabletext\">".$linterface->GET_LNK_PRINT("javascript:openPrintPage('$start_date','$end_date','$targetLocation','$targetGroup','$groupBy','$groupby_val')","","","","",0);
    // $toolbar .= $linterface->GET_LNK_EXPORT("item_written_off_export.php?start_date='$start_date'&end_date='$end_date'&targetLocation='$targetLocation'&targetGroup='$targetGroup'&groupBy=$groupBy&selected_cb=$groupby_val","","","","",0)."</td></tr>";
    
    $toolbar .= "<tr><td class=\"tabletext\">";
    $toolbar .= "<div class=\"Conntent_tool\">";
    // export
    $toolbar .= "<div class=\"btn_option\">
				<a href=\"javascript:\" class=\"export\" id=\"btn_export\" onclick=\"MM_showHideLayers('export_option','','show');document.getElementById('btn_export').className = 'export parent_btn';\"> " . $Lang['Btn']['Export'] . "</a>
				<br style=\"clear:both\" />
					<div class=\"btn_option_layer\" id=\"export_option\" onclick=\"MM_showHideLayers('export_option','','hide');document.getElementById('btn_export').className = 'export';\">
					  <a href=\"javascript:click_export(1);\" class=\"sub_btn\"> " . $Lang['eInventory']['BasicInfo'] . "</a>
					  <a href=\"javascript:click_export(2);\" class=\"sub_btn\"> " . $Lang['eInventory']['Details'] . "</a>
					</div>
				</div>";
    
    // print
    $toolbar .= $linterface->GET_LNK_PRINT_IP25("javascript:openPrintPage('$start_date','$end_date','$targetLocation','$targetGroup','$targetFundSource','$groupBy','$groupby_val', '$sortby', '$DateType')");
    $toolbar .= "</div>";
    $toolbar .= "</td></tr>";
    
    if ($groupBy == 1) {
        $arr_tmp_location = $TargetRoom;
        
        if (sizeof($arr_tmp_location) > 0) {
            for ($a = 0; $a < sizeof($arr_tmp_location); $a ++) {
                $location_id = $arr_tmp_location[$a];
                
                $sql = "SELECT 
								CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("a.") . ")
						FROM
								INVENTORY_LOCATION AS a INNER JOIN
								INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID)
						WHERE
								a.LocationID = $location_id
						";
                $arr_tmp = $linventory->returnArray($sql, 1);
                if (sizeof($arr_tmp) > 0) {
                    list ($location_name) = $arr_tmp[0];
                }
                
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                }
                
                if ($target_admin_group != "") {
                    $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                }
                
                $RequestPerson = getNameFieldByLang2("f.");
                $ApprovePerson = getNameFieldByLang2("g.");
                
                $RequestArchivedPerson = getNameFieldByLang2("h.");
                $ApproveArchivedPerson = getNameFieldByLang2("i.");
                /*
                 * $sql = "SELECT
                 * b.ItemCode,
                 * b.ItemType,
                 * ".$linventory->getInventoryItemNameByLang("b.")." as this_item_name,
                 * concat(".$linventory->getInventoryNameByLang("building.").",'>',".$linventory->getInventoryNameByLang("floor.").",'>',".$linventory->getInventoryNameByLang("c.")."),
                 * ".$linventory->getInventoryNameByLang("d.").",
                 * ".$linventory->getInventoryNameByLang("funding.").",
                 * a.WriteOffQty,
                 * a.RequestDate,
                 * IFNULL($RequestPerson,
                 * IF( $RequestArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('<font color=red>*</font> ', $RequestArchivedPerson))
                 * ),
                 * a.ApproveDate,
                 * IFNULL($ApprovePerson,
                 * IF( $ApproveArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('<font color=red>*</font> ', $ApproveArchivedPerson))
                 * ),
                 * a.WriteOffReason,
                 * a.RecordID,
                 * ".$linventory->getInventoryNameByLang("funding2.")."
                 * FROM
                 * INVENTORY_ITEM_WRITE_OFF_RECORD AS a
                 * INNER JOIN INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID)
                 * INNER JOIN INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID)
                 * INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID)
                 * INNER JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
                 * INNER JOIN INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond)
                 * LEFT OUTER JOIN INTRANET_USER AS f ON (a.RequestPerson = f.UserID)
                 * LEFT OUTER JOIN INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
                 * left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
                 * left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
                 * left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
                 * left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID or funding.FundingSourceID=s.FundingSource)
                 * left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
                 * WHERE
                 * a.RecordStatus = 1
                 * AND c.LocationID IN (".$location_id.")
                 * AND (a.". $DateType ." BETWEEN '$start_date' AND '$end_date')
                 *
                 * order by $orderby
                 * ";
                 */
                $sql = "SELECT 
								b.ItemCode, 
								b.ItemType, 
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
									),
								a.ApproveDate, 
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
									),
								a.WriteOffReason,
								a.RecordID,
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
												INVENTORY_ITEM_WRITE_OFF_RECORD AS a 
								INNER JOIN 		INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) 
								INNER JOIN 		INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) 
								INNER JOIN 		INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) 
								INNER JOIN 		INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) 
								INNER JOIN 		INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond)
								LEFT OUTER JOIN INTRANET_USER AS f ON (a.RequestPerson = f.UserID)
								LEFT OUTER JOIN INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
								left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
						WHERE
								a.RecordStatus = 1 
								AND c.LocationID IN (" . $location_id . ")
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
								
						order by $orderby
						";
                $arr_result = $linventory->returnArray($sql);
                // build data array
                $ary = array();
                foreach ($arr_result as $k => $d) {
                    $ary[$d['ItemType']][] = $d;
                }
                
                // display location
                $table_content .= "<tr><td colspan=\"12\">" . $linterface->GET_NAVIGATION2(intranet_htmlspecialchars("$location_name")) . "</td></tr>\n";
                // $table_content .= "<tr><td class=\"tablename\" colspan=\"11\">".intranet_htmlspecialchars($location_name)."</td></tr>";
                
                for ($k = 1; $k <= 2; $k ++) // 1 = single, 2 = bulk
{
                    $table_content .= "<tr><td class=\"tablename\" colspan=\"13\">" . ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk) . "</td></tr>\n";
                    $table_content .= "<tr class=\"tabletop\">";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffReason_Name</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_RequestPerson</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApprovePerson</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['ItemAttachment'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Attachment</td>";
                    $table_content .= "</tr>";
                    
                    $arr_result = $ary[$k];
                    if (sizeof($arr_result) == 0) {
                        $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"13\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                        $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=13></td></tr>";
                    } else {
                        for ($i = 0; $i < sizeof($arr_result); $i ++) {
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $request_id, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                            // $j=$i+1;
                            // if($j%2 == 0)
                            // $row_css = " class=\"tablerow2\" ";
                            // else
                            // $row_css = " class=\"tablerow1\" ";
                            
                            if ($k == 1) {
                                $Style = 'single tabletext';
                            } else {
                                $Style = 'bulk tabletext';
                                
                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                            }
                            
                            $table_content .= "<tr $row_css>";
                            $table_content .= "<td class=\"$Style\">$item_code</td>";
                            $table_content .= "<td class=\"$Style\">$item_name</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($location_name) . "</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($group_name) . "</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                            $table_content .= "<td class=\"$Style\">$write_off_qty</td>";
                            $table_content .= "<td class=\"$Style\">$write_off_reason</td>";
                            $table_content .= "<td class=\"$Style\">$request_person</td>";
                            $table_content .= "<td class=\"$Style\">$request_date</td>";
                            $table_content .= "<td class=\"$Style\">$approve_person</td>";
                            $table_content .= "<td class=\"$Style\">$approve_date</td>";
                            
                            // ## retrieve item attachment
                            $sql = "select AttachmentPath, Filename from INVENTORY_ITEM_ATTACHMENT_PART where ItemID=$this_item_id";
                            $arr_attachment = $linventory->returnArray($sql);
                            if (sizeof($arr_attachment) > 0) {
                                $table_content .= "<td class=\"$Style\">";
                                
                                for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                                    list ($attachment_path, $attachment_name) = $arr_attachment[$j];
                                    $attachment_link = "<a href=\"" . $attachment_path . rawurlencode($attachment_name) . "\" target=\"_blank\">$attachment_name</a><br>";
                                    
                                    $table_content .= $attachment_link;
                                }
                                $table_content .= "</td>";
                            } else {
                                $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            }
                            
                            // ## retrieve write-off attachment
                            $sql = "select PhotoPath, PhotoName from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$request_id";
                            $result = $linventory->returnArray($sql);
                            if (! empty($result)) {
                                $table_content .= "<td class=\"$Style\">";
                                
                                foreach ($result as $k1 => $d1) {
                                    list ($photo_path, $photo_name) = $d1;
                                    
                                    $full_path = $photo_path . "/" . $photo_name;
                                    $table_content .= "<a href=\"$full_path\">$photo_name</a><br>";
                                }
                                $table_content .= "</td>";
                            } else {
                                $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            }
                            
                            $table_content .= "</tr>";
                            $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=13></td></tr>";
                        }
                    }
                    
                    $table_content .= "<tr height=\"20px\"><td colspan=\"12\"></td></tr>";
                }
            }
        }
    } else 
        if ($groupBy == 2) {
            $arr_tmp_item_type = array(
                array(
                    "1",
                    $i_InventorySystem_ItemType_Single
                ),
                array(
                    "2",
                    $i_InventorySystem_ItemType_Bulk
                )
            );
            
            if (sizeof($arr_tmp_item_type) > 0) {
                for ($a = 0; $a < sizeof($arr_tmp_item_type); $a ++) {
                    list ($item_type, $item_type_name) = $arr_tmp_item_type[$a];
                    
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                    }
                    
                    if ($target_admin_group != "") {
                        $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                    }
                    
                    $RequestPerson = getNameFieldByLang2("f.");
                    $RequestArchivedPerson = getNameFieldByLang2("h.");
                    $ApprovePerson = getNameFieldByLang2("g.");
                    $ApproveArchivedPerson = getNameFieldByLang2("i.");
                    /*
                     * $sql = "SELECT
                     * b.ItemCode,
                     * b.ItemType,
                     * ".$linventory->getInventoryItemNameByLang("b.")." as this_item_name,
                     * concat(".$linventory->getInventoryNameByLang("building.").",'>',".$linventory->getInventoryNameByLang("floor.").",'>',".$linventory->getInventoryNameByLang("c.")."),
                     * ".$linventory->getInventoryNameByLang("d.").",
                     * ".$linventory->getInventoryNameByLang("funding.").",
                     * a.WriteOffQty,
                     * a.RequestDate,
                     * IFNULL($RequestPerson,
                     * IF( $RequestArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('<font color=red>*</font> ', $RequestArchivedPerson))
                     * ),
                     * a.ApproveDate,
                     * IFNULL($ApprovePerson,
                     * IF( $ApproveArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('<font color=red>*</font> ', $ApproveArchivedPerson))
                     * ),
                     * a.WriteOffReason,
                     * a.RecordID,
                     * ".$linventory->getInventoryNameByLang("funding2.")."
                     * FROM
                     * INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
                     * INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
                     * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
                     * INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
                     * INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
                     * INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
                     * INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN
                     * INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
                     * left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
                     * left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
                     * left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
                     * left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID or funding.FundingSourceID=s.FundingSource)
                     * left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
                     * WHERE
                     * a.RecordStatus = 1 AND
                     * b.ItemType = $item_type
                     * AND (a.". $DateType ." BETWEEN '$start_date' AND '$end_date')
                     * order by $orderby
                     * ";
                     */
                    $sql = "SELECT 
								b.ItemCode, 
								b.ItemType, 
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
									),
								a.ApproveDate, 
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
									),
								a.WriteOffReason,
								a.RecordID,
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
								INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
								INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
								INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
								INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
								left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
						WHERE
								a.RecordStatus = 1 AND
								b.ItemType = $item_type
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
						order by $orderby
						";
                    $arr_result = $linventory->returnArray($sql);
                    
                    $table_content .= "<tr><td class=\"tablename\" colspan=\"13\">$item_type_name</td></tr>";
                    $table_content .= "<tr class=\"tabletop\">";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffReason_Name</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_RequestPerson</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApprovePerson</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['ItemAttachment'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Attachment</td>";
                    $table_content .= "</tr>";
                    
                    if (sizeof($arr_result) > 0) {
                        for ($i = 0; $i < sizeof($arr_result); $i ++) {
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $request_id, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                            // $j=$i+1;
                            // if($j%2 == 0)
                            // $row_css = " class=\"tablerow2\" ";
                            // else
                            // $row_css = " class=\"tablerow1\" ";
                            
                            if ($item_type == 1) {
                                $Style = 'single tabletext';
                            } else {
                                $Style = 'bulk tabletext';
                                
                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                            }
                            
                            $table_content .= "<tr $row_css>";
                            $table_content .= "<td class=\"$Style\">$item_code</td>";
                            $table_content .= "<td class=\"$Style\">$item_name</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($location_name) . "</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($group_name) . "</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                            $table_content .= "<td class=\"$Style\">$write_off_qty</td>";
                            $table_content .= "<td class=\"$Style\">$write_off_reason</td>";
                            $table_content .= "<td class=\"$Style\">$request_person</td>";
                            $table_content .= "<td class=\"$Style\">$request_date</td>";
                            $table_content .= "<td class=\"$Style\">$approve_person</td>";
                            $table_content .= "<td class=\"$Style\">$approve_date</td>";
                            
                            // ## retrieve item attachment
                            $sql = "select AttachmentPath, Filename from INVENTORY_ITEM_ATTACHMENT_PART where ItemID=$this_item_id";
                            $arr_attachment = $linventory->returnArray($sql);
                            if (sizeof($arr_attachment) > 0) {
                                $table_content .= "<td class=\"$Style\">";
                                
                                for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                                    list ($attachment_path, $attachment_name) = $arr_attachment[$j];
                                    $attachment_link = "<a href=\"" . $attachment_path . rawurlencode($attachment_name) . "\" target=\"_blank\">$attachment_name</a><br>";
                                    
                                    $table_content .= $attachment_link;
                                }
                                $table_content .= "</td>";
                            } else {
                                $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            }
                            
                            // ## retrieve write-off attachment
                            $sql = "select PhotoPath, PhotoName from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$request_id";
                            $result = $linventory->returnArray($sql);
                            if (! empty($result)) {
                                $table_content .= "<td class=\"$Style\">";
                                
                                foreach ($result as $k1 => $d1) {
                                    list ($photo_path, $photo_name) = $d1;
                                    
                                    $full_path = $photo_path . "/" . $photo_name;
                                    $table_content .= "<a href=\"$full_path\">$photo_name</a><br>";
                                }
                                $table_content .= "</td>";
                            } else {
                                $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            }
                            //
                            // if($photo_name != "")
                            // {
                            // $full_path = $photo_path."/".$photo_name;
                            // $table_content .= "<td class=\"$Style\"><a href=\"$full_path\">$photo_name</a></td>";
                            // }
                            // else
                            // $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            $table_content .= "</tr>";
                            $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=13></td></tr>";
                        }
                    }
                    if (sizeof($arr_result) == 0) {
                        $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"13\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                    }
                    $table_content .= "<tr height=\"20px\"><td colspan=\"13\"></td></tr>";
                }
            }
        } else 
            if ($groupBy == 3) {
                $arr_tmp_group = $targetGroup;
                
                if (sizeof($arr_tmp_group) > 0) {
                    for ($a = 0; $a < sizeof($arr_tmp_group); $a ++) {
                        $admin_group_id = $arr_tmp_group[$a];
                        
                        $sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID = '".$admin_group_id."'";
                        $arr_tmp_admin = $linventory->returnArray($sql, 1);
                        
                        if (sizeof($arr_tmp_admin) > 0) {
                            list ($admin_group_name) = $arr_tmp_admin[0];
                        }
                        
                        if ($linventory->IS_ADMIN_USER($UserID))
                            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                        else
                            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                        
                        $tmp_arr_group = $linventory->returnVector($sql);
                        if (sizeof($tmp_arr_group) > 0) {
                            $target_admin_group = implode(",", $tmp_arr_group);
                        }
                        
                        if ($target_admin_group != "") {
                            $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                        }
                        
                        $RequestPerson = getNameFieldByLang2("f.");
                        $ApprovePerson = getNameFieldByLang2("g.");
                        $RequestArchivedPerson = getNameFieldByLang2("h.");
                        $ApproveArchivedPerson = getNameFieldByLang2("i.");
                        
                        $sql = "SELECT 
								b.ItemCode, 
								b.ItemType, 
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
									),
								a.ApproveDate, 
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
									),
								a.WriteOffReason,
								a.RecordID,
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
								INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
								INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
								INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
								INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
								left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
						WHERE
								a.RecordStatus = 1
								AND d.AdminGroupID IN (" . $admin_group_id . ")
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
						order by $orderby
						";
                        $arr_result = $linventory->returnArray($sql);
                        // build data array
                        $ary = array();
                        foreach ($arr_result as $k => $d) {
                            $ary[$d['ItemType']][] = $d;
                        }
                        
                        // display group name
                        $table_content .= "<tr><td colspan=\"12\">" . $linterface->GET_NAVIGATION2(intranet_htmlspecialchars("$admin_group_name")) . "</td></tr>\n";
                        // $table_content .= "<tr><td class=\"tablename\" colspan=\"11\">".intranet_htmlspecialchars($admin_group_name)."</td></tr>";
                        
                        for ($k = 1; $k <= 2; $k ++) // 1 = single, 2 = bulk
{
                            $table_content .= "<tr><td class=\"tablename\" colspan=\"13\">" . ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk) . "</td></tr>\n";
                            
                            $table_content .= "<tr class=\"tabletop\">";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
                            $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                            $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffReason_Name</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_RequestPerson</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApprovePerson</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
                            $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['ItemAttachment'] . "</td>";
                            $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Attachment</td>";
                            $table_content .= "</tr>";
                            
                            $arr_result = $ary[$k];
                            if (sizeof($arr_result) > 0) {
                                for ($i = 0; $i < sizeof($arr_result); $i ++) {
                                    list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $request_id, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                                    // $j=$i+1;
                                    // if($j%2 == 0)
                                    // $row_css = " class=\"tablerow2\" ";
                                    // else
                                    // $row_css = " class=\"tablerow1\" ";
                                    
                                    if ($k == 1) {
                                        $Style = 'single tabletext';
                                    } else {
                                        $Style = 'bulk tabletext';
                                        
                                        // retrieve bulk item latest funding source name
                                        $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                        $result_bl = $linventory->returnVector($sql_bl);
                                        $funding_name = $result_bl[0];
                                    }
                                    
                                    $table_content .= "<tr $row_css>";
                                    $table_content .= "<td class=\"$Style\">$item_code</td>";
                                    $table_content .= "<td class=\"$Style\">$item_name</td>";
                                    $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($location_name) . "</td>";
                                    $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($group_name) . "</td>";
                                    $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                                    $table_content .= "<td class=\"$Style\">$write_off_qty</td>";
                                    $table_content .= "<td class=\"$Style\">$write_off_reason</td>";
                                    $table_content .= "<td class=\"$Style\">$request_person</td>";
                                    $table_content .= "<td class=\"$Style\">$request_date</td>";
                                    $table_content .= "<td class=\"$Style\">$approve_person</td>";
                                    $table_content .= "<td class=\"$Style\">$approve_date</td>";
                                    
                                    // ## retrieve item attachment
                                    $sql = "select AttachmentPath, Filename from INVENTORY_ITEM_ATTACHMENT_PART where ItemID=$this_item_id";
                                    $arr_attachment = $linventory->returnArray($sql);
                                    if (sizeof($arr_attachment) > 0) {
                                        $table_content .= "<td class=\"$Style\">";
                                        
                                        for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                                            list ($attachment_path, $attachment_name) = $arr_attachment[$j];
                                            $attachment_link = "<a href=\"" . $attachment_path . rawurlencode($attachment_name) . "\" target=\"_blank\">$attachment_name</a><br>";
                                            
                                            $table_content .= $attachment_link;
                                        }
                                        $table_content .= "</td>";
                                    } else {
                                        $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                                    }
                                    
                                    // ## retrieve write-off attachment
                                    $sql = "select PhotoPath, PhotoName from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$request_id";
                                    $result = $linventory->returnArray($sql);
                                    if (! empty($result)) {
                                        $table_content .= "<td class=\"$Style\">";
                                        
                                        foreach ($result as $k1 => $d1) {
                                            list ($photo_path, $photo_name) = $d1;
                                            
                                            $full_path = $photo_path . "/" . $photo_name;
                                            $table_content .= "<a href=\"$full_path\">$photo_name</a><br>";
                                        }
                                        $table_content .= "</td>";
                                    } else {
                                        $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                                    }
                                    $table_content .= "</tr>";
                                    $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=13></td></tr>";
                                }
                            } else {
                                $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"13\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                            }
                            $table_content .= "<tr height=\"20px\"><td colspan=\"13\"></td></tr>";
                        }
                    }
                }
            } 
        // funding source start
    else if ($groupBy == 4) {
        $arr_tmp_funding_source = $targetFundSource;
        //debug_pr($arr_tmp_funding_source);

        if (sizeof($arr_tmp_funding_source) > 0) {
            for ($a = 0; $a < sizeof($arr_tmp_funding_source); $a ++) {
                $funding_source_id = $arr_tmp_funding_source[$a];
                
                $sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID = '".$funding_source_id."'";
                $arr_tmp_fs = $linventory->returnArray($sql, 1);
                
                
                if (sizeof($arr_tmp_fs) > 0) {
                    list ($funding_source_name) = $arr_tmp_fs[0];
                }
                
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '" . $UserID . "'";

                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                }

                if ($target_admin_group != "") {
                    $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                }

                $RequestPerson = getNameFieldByLang2("f.");
                $ApprovePerson = getNameFieldByLang2("g.");
                $RequestArchivedPerson = getNameFieldByLang2("h.");
                $ApproveArchivedPerson = getNameFieldByLang2("i.");

                $sql = "SELECT
								b.ItemCode,
								b.ItemType,
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . " ,
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson))
									),
								a.ApproveDate,
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson))
									),
								a.WriteOffReason,
								a.RecordID,
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
								INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
								INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
								INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN
								INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
								left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID OR funding.FundingSourceID = s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
						WHERE
								a.RecordStatus = 1
								AND funding.FundingSourceID IN (" . $funding_source_id . ")
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
						order by $orderby
						";
                $arr_result = $linventory->returnArray($sql);
                
                // build data array
                $ary = array();
                foreach ($arr_result as $k => $d) {
                    $ary[$d['ItemType']][] = $d;
                }

                // display group name
                $table_content .= "<tr><td colspan=\"12\">" . $linterface->GET_NAVIGATION2(intranet_htmlspecialchars("$funding_source_name")) . "</td></tr>\n";
                // $table_content .= "<tr><td class=\"tablename\" colspan=\"11\">".intranet_htmlspecialchars($admin_group_name)."</td></tr>";

                for ($k = 1; $k <= 2; $k ++) // 1 = single, 2 = bulk
                {
                    $table_content .= "<tr><td class=\"tablename\" colspan=\"13\">" . ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk) . "</td></tr>\n";

                    $table_content .= "<tr class=\"tabletop\">";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffReason_Name</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_RequestPerson</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApprovePerson</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
                    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['ItemAttachment'] . "</td>";
                    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Attachment</td>";
                    $table_content .= "</tr>";

                    $arr_result = $ary[$k];
                    
                    if (sizeof($arr_result) > 0) {
                        for ($i = 0; $i < sizeof($arr_result); $i ++) {
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $request_id, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];

                            if ($k == 1) {
                                $Style = 'single tabletext';
                            } else {
                                $Style = 'bulk tabletext';

                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource 
                                        WHERE a.ItemID=$this_item_id
                                        AND a.FundingSource!='' AND a.FundingSource != 0 AND a.LocationID = $this_location_id
                                        order by a.RecordID desc limit 1";

                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                            }

                            $table_content .= "<tr $row_css>";
                            $table_content .= "<td class=\"$Style\">$item_code</td>";
                            $table_content .= "<td class=\"$Style\">$item_name</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($location_name) . "</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($group_name) . "</td>";
                            $table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
                            $table_content .= "<td class=\"$Style\">$write_off_qty</td>";
                            $table_content .= "<td class=\"$Style\">$write_off_reason</td>";
                            $table_content .= "<td class=\"$Style\">$request_person</td>";
                            $table_content .= "<td class=\"$Style\">$request_date</td>";
                            $table_content .= "<td class=\"$Style\">$approve_person</td>";
                            $table_content .= "<td class=\"$Style\">$approve_date</td>";

                            // ## retrieve item attachment
                            $sql = "select AttachmentPath, Filename from INVENTORY_ITEM_ATTACHMENT_PART where ItemID=$this_item_id";
                            $arr_attachment = $linventory->returnArray($sql);
                            if (sizeof($arr_attachment) > 0) {
                                $table_content .= "<td class=\"$Style\">";

                                for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                                    list ($attachment_path, $attachment_name) = $arr_attachment[$j];
                                    $attachment_link = "<a href=\"" . $attachment_path . rawurlencode($attachment_name) . "\" target=\"_blank\">$attachment_name</a><br>";

                                    $table_content .= $attachment_link;
                                }
                                $table_content .= "</td>";
                            } else {
                                $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            }

                            // ## retrieve write-off attachment
                            $sql = "select PhotoPath, PhotoName from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$request_id";
                            $result = $linventory->returnArray($sql);
                            if (! empty($result)) {
                                $table_content .= "<td class=\"$Style\">";

                                foreach ($result as $k1 => $d1) {
                                    list ($photo_path, $photo_name) = $d1;

                                    $full_path = $photo_path . "/" . $photo_name;
                                    $table_content .= "<a href=\"$full_path\">$photo_name</a><br>";
                                }
                                $table_content .= "</td>";
                            } else {
                                $table_content .= "<td class=\"$Style\">&nbsp;</td>";
                            }
                            $table_content .= "</tr>";
                            $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=13></td></tr>";
                        }
                    } else {
                        $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"13\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                    }
                    $table_content .= "<tr height=\"20px\"><td colspan=\"13\"></td></tr>";
                }
            }
        }
    } 
    //end group4
}
?>

<?=$js_init;?>

<script language="javascript">

function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("TargetFloor")){
			document.getElementById("TargetFloor").value = "";
		}
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
}

function openPrintPage(StartDate, EndDate, TargetLocation, TargetGroup, TargetFundSource, GroupBy, GroupByValue, sortby, DateType)
{
	newWindow(" item_written_off_print.php?start_date='"+StartDate+"'&end_date='"+EndDate+"'&targetLocation="+TargetLocation+"&targetGroup="+TargetGroup+"&targetFundSource="+TargetFundSource+"&groupBy="+GroupBy+"&sortby="+sortby+"&selected_cb="+GroupByValue+"&DateType="+DateType,10);
}

function checkForm()
{
	var groupby = document.getElementById('groupBy').value;
	var location_cb = 0;
	var group_cb = 0;
	var funding_source_cb = 0;
	
	if(groupby == 1) {
		if(document.getElementById('TargetRoom[]')) {
			for(i=0; i<document.getElementById('TargetRoom[]').length; i++) {
				if(document.getElementById('TargetRoom[]')[i].selected == true) {
					if(document.getElementById('TargetRoom[]')[i].value != "") {
						location_cb++;
					}
				}
			}
		}
	}
	if(groupby == 3){
		if(document.getElementById('targetGroup[]')) {
			for(i=0; i<document.getElementById('targetGroup[]').length; i++) {
				if(document.getElementById('targetGroup[]')[i].selected == true) {
					if(document.getElementById('targetGroup[]')[i].value != "") {
						group_cb++;
					}
				}
			}
		}
	}
	if(groupby == 4){
		if(document.getElementById('targetFundSource[]')) {
			for(i=0; i<document.getElementById('targetFundSource[]').length; i++) {
				if(document.getElementById('targetFundSource[]')[i].selected == true) {
					if(document.getElementById('targetFundSource[]')[i].value != "") {
						funding_source_cb++;
					}
				}
			}
		}
	}
		
	if(location_cb>0||group_cb>0||funding_source_cb>0||groupby==2){
		document.form1.step.value = "final";
		return true;
	}
	else if(groupby==1)
	{
		alert('<?=$i_InventorySystem['jsLocationCheckBox']?>');
		return false;
	}
	else if(groupby==3)
	{
		alert('<?=$i_InventorySystem['jsGroupCheckBox']?>');
		return false;	
	}
	else if(groupby==4)
	{
		alert('<?=$Lang['eInventory']['FundingSource']['jsFundingSourceCheckBox']?>');
		return false;	
	}
	else if(groupby=="")
	{
		alert('<?=$i_InventorySystem['jsLocationGroupCheckBox']?>');
		return false;	
	}
}

$(document).ready(function(){ 
	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
	$('#start_date').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
	$('#end_date').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
});

function click_export(x)
{
	window.location="item_written_off_export.php?start_date='<?=$start_date?>'&end_date='<?=$end_date?>'&targetLocation='<?=$targetLocation?>'&targetGroup='<?=$targetGroup?>'&targetFundSource='<?=$targetFundSource?>'&groupBy=<?=$groupBy?>&sortby=<?=$sortby?>&selected_cb=<?=$groupby_val?>&DateType=<?=$DateType?>&export_type="+x;
}
</script>

<br>

<form name="form1" action="" method="post" onSubmit="return checkForm()">

	<table class="form_table_v30">
<?=$report_filter;?>
</table>

	<div class="edit_bottom_v30">
		<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_submit, "submit")?>
</div>

<? if(($groupBy != "") && ($step=="final")){ ?>
<table border="0" width="92%" cellspacing="0" cellpadding="5">
<?=$toolbar;?>
</table>
	<br>
<? } ?>

<table border="0" width="100%" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content?>
</table>
<?if($table_content) {?>
<?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?>
<? } ?>

<? 
/*
    * if(($groupBy!="") && ($step=="final")){ ?>
    * <table border="0" width="100%" cellpadding="5" cellspacing="0">
    * <tr>
    * <td><table border="0" cellpadding="0">
    * <tr>
    * <td><table border="0">
    * <tr>
    * <td width="10" height="10"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
    * <tr>
    * <td class="single"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
    * </tr>
    * </table></td>
    * <td><span class="tabletext"><?=$i_InventorySystem_ItemType_Single?></span></td>
    * </tr>
    * </table></td>
    * <td><table width="100%" border="0">
    * <tr>
    * <td width="10" height="10"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
    * <tr>
    * <td class="bulk"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
    * </tr>
    * </table></td>
    * <td><span class="tabletext"><?=$i_InventorySystem_ItemType_Bulk?></span></td>
    * </tr>
    * </table></td>
    * </tr>
    * </table></td>
    * </tr>
    * </table>
    * <? }
    */
?>
<input type="hidden" name="flag" value="1"> <input type="hidden"
		name="step" value="0">
</form>

<br>

<?
$linterface->LAYOUT_STOP();
intranet_closed;
?>

