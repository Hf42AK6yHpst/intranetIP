<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

intranet_auth();
intranet_opendb();

$ldamu = new libdigitalarchive_moduleupload();
if(!$ldamu->User_Can_Archive_File()){
	exit;
}

$task = $_REQUEST['task'];
if($task=='GetUploadFileToDA_TBForm') {
	$lfile = new libfilesystem();
	$report_url = $_REQUEST['report_url'];
	
	$archive_path = $intranet_root."/file/inventory/tmp_archive";
	$dir_path = $archive_path."/u".$_SESSION['UserID'];
	
	if(file_exists($dir_path)){
		$lfile->folder_remove_recursive($dir_path);
	}
	
	if(!is_dir($archive_path)){
		$lfile->folder_new($archive_path);
		$lfile->chmod_R($archive_path,0777);
	}
	
	$lfile->folder_new($dir_path);
	$lfile->chmod_R($dir_path, 0777);
	
	$server_link = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/";
	
	$html_content = getHtmlByUrl($report_url);
	//$html_content = str_replace($PATH_WRT_ROOT,$server_link,$html_content);
	$html_content = str_replace($PATH_WRT_ROOT,'',$html_content);
	$html_content = $ldamu->Embed_Image_CSS_To_HTML($html_content);
	//$datetime = date("YmdHis");
	$target_filepath = $dir_path."/".$Lang['Header']['Menu']['eInventory']." - ".$i_InventorySystem_Report_FixedAssetsRegister.".html";
	
	$lfile->file_write($html_content,$target_filepath);
	
	echo $ldamu->Get_Module_Files_Group_Folder_List_TB_Form('eInventory',$dir_path);
}

intranet_closedb();
?>