<?php
// using: 
// ####################################
// 2020-02-07 Tommy
//  Customization for plkchc #A170032
//
// ####################################
ini_set('memory_limit', '1G');
ini_set('max_execution_time', '600');

$PATH_WRT_ROOT = "../../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN . "/layout/print_header_no25css.php");
// include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

if (! sizeof($_POST)) {
    header("Location: index.php");
}
intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// Retreive item
$with_data = 0;

$StockTakePriceSingle = $linventory->getStockTakePriceSingle();
$StockTakePriceBulk = $linventory->getStockTakePriceBulk();

// #####################
// Report Title
// #####################
$table .= "<table border='0' width='100%'>";
$table .= "<tr>";
$table .= "<td align='center' colspan='3'><font size=3><b>" . GET_SCHOOL_NAME() . "</b></font></td>";
$table .= "</tr>";
$table .= "<tr>";
$table .= "<td align='center' colspan='3'><font size=3>" . $Lang['eInventory']['Report']['StocktakeList'] . "</font></td>";
$table .= "</tr>";
$table .= "<tr>";
$table .= "<td width='33%'>&nbsp;</td>";
$table .= "</tr>";
// if ($this_GroupID != - 1) {
// $table .= "<tr>";
// $table .= "<td colspan='3' align='right'>" . . "</td>";
// $table .= "</tr>";
// }

$table .= "</table>";
$table .= "<table border='0' width='100%' cellpadding='02' cellspacing='0' class='eSporttableborder'>";
// #####################
// records list table
// #####################

$table .= "<tr>";
$table .= "<td width='17.3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['ItemCode_PLKCHC'] . "</b></td>";
$table .= "<td width='14%' align='center' class='eSporttdborder' ><b>" . $i_InventorySystem_Item_Name . "</b></td>";
// $table .= "<td width='20%' align='center' class='eSporttdborder' rowspan='2'><b>" . $Lang['eInventory']['ItemDetails'] . "</b></td>";
$table .= "<td width='9.3%' align='center' class='eSporttdborder' ><b>" . $i_InventorySystem['Caretaker'] . "</b></td>";
$table .= "<td width='9.3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['ExpectedLocation_PLKCHC'] . "</b></td>";
$table .= "<td width='9.3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['CurrentLocation_PLKCHC'] . "</b></td>";
$table .= "<td width='3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['Original_Quantity_PLKCHC'] . "</b></td>";
$table .= "<td width='3%' align='center' class='eSporttdborder' ><b>" . $i_InventorySystem_Stocktake_StocktakeQty . "</b></td>";
$table .= "<td width='3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['VarianceQty_PLKCHC'] . "</b></td>";
$table .= "<td width='9.3%' align='center' class='eSporttdborder' ><b>" . $i_InventorySystem_Report_Col_Sch_Unit_Price . "</b></td>";
$table .= "<td width='9.3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['TotalPrice_PLKCHC'] . "</b></td>";
$table .= "<td width='9.3%' align='center' class='eSporttdborder' ><b>" . $Lang['eInventory']['StockCheckPeriod_PLKCHC'] . "</b></td>";

$table .= "</tr>";
$table .= "<tr>";
// $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['NoSuchItem'] . "</b></td>";
// $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['Variation'] . "</b></td>";
$table .= "</tr>";

foreach ($targetLocation as $k => $this_LocationID) {
    // Location Name
    $thisRoom = new Room($this_LocationID);
    $thisLocationName = $thisRoom->RoomName;
    
    for ($this_item_type = 1; $this_item_type <= 2; $this_item_type ++) {
        $thisLocationResult = array();
        $this_location_group = array();
        if (empty($targetGroup)) {
            $this_location_group[0] = - 1;
        } else {
            $this_location_group = $targetGroup;
        }
        
        // Loop with Resource ManagementGroup
        foreach ($this_location_group as $k2 => $this_GroupID) {
            if ($this_GroupID != - 1) {
                $thisGroupName = $linventory->returnGroupNameByGroupID($this_GroupID);
                $sql_cond = "AND b.GroupInCharge = '$this_GroupID' ";
            }
            if ($this_item_type == 1) {
                // #### Single item
                $single_funding = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', f.code, concat(f.code,', ',f2.code))";
                $sql = "SELECT
                                a.ItemID,
								b.TagCode as Barcode, 
								$single_funding as FundingCode, 
								" . $linventory->getInventoryNameByLang("d2.") . " as SubCatName,
								" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName, 
								" . $linventory->getInventoryDescriptionNameByLang("a.") . " as ItemDescription, 
								1 as Qty,
                                b.UnitPrice as unit_price,
                                b.PurchasedPrice
						FROM 
								INVENTORY_ITEM AS a 
								INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON a.ItemID = b.ItemID 
								INNER JOIN INVENTORY_FUNDING_SOURCE AS f on f.FundingSourceID = b.FundingSource 
								LEFT JOIN INVENTORY_FUNDING_SOURCE AS f2 ON b.FundingSource2 = f2.FundingSourceID 
								INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID 
						WHERE
								a.ItemType = 1 AND
								a.RecordStatus = 1 AND
								b.LocationID = '$this_LocationID' 
								$sql_cond and 
								(a.StockTakeOption='YES' OR (a.StockTakeOption='FOLLOW_SETTING' and b.UnitPrice>=" . $StockTakePriceSingle . ")) 
						ORDER BY
					            a.ItemCode";
                
            } else {
                // #### Bulk item
                $sql = "SELECT
                            a.ItemID,
							e.Barcode, 
							c.code as FundingCode, 
							" . $linventory->getInventoryNameByLang("d2.") . " as SubCatName,
							" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName, 
							" . $linventory->getInventoryDescriptionNameByLang("a.") . " as ItemDescription, 
							b.Quantity as Qty, 
							avg(d.UnitPrice) as unit_price, 
                            d.PurchasedPrice,
							a.StockTakeOption as StockTakeOption
					FROM
							INVENTORY_ITEM AS a 
							INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) 
							INNER JOIN INVENTORY_FUNDING_SOURCE as c on c.FundingSourceID = b.FundingSourceID 
							INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID  
                            INNER JOIN INVENTORY_ITEM_BULK_EXT AS e ON (a.ItemID = e.ItemID) 
							LEFT JOIN INVENTORY_ITEM_BULK_LOG as d on (d.ItemID=a.ItemID and d.Action=1)
					WHERE
							a.ItemType = 2 AND
							a.RecordStatus = 1 AND
							b.LocationID ='$this_LocationID'  
							$sql_cond
					group by 
						b.ItemID, b.FundingSourceID  
					having
						StockTakeOption ='YES' or (StockTakeOption='FOLLOW_SETTING' and Qty*unit_price >=" . $StockTakePriceBulk . ")
					ORDER BY
					    a.ItemCode";
            }
            // $sql = $sql1 . " union " . $sql2 ." ORDER BY ItemCode";
            $arr_result = $linventory->returnArray($sql);

            if (empty($arr_result))
                continue;
            $with_data = 1;
            $totalQty = 0;

            if (sizeof($arr_result) > 0) {
                foreach ($arr_result as $k1 => $data) {

                    $sql1 = "SELECT
                				b.LocationID,
                				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
                				b.AdminGroupID,
                				" . $linventory->getInventoryNameByLang("e.") . ",
                				b.PersonInCharge,
                				" . getNameFieldByLang2("f.") . ",
                				b.DateModified,
                				CONCAT('+',b.SurplusQty)
                			FROM
                				INVENTORY_ITEM AS a INNER JOIN
                				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
                				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
                				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
                				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
                				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
                				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
                			WHERE
                				a.ItemID IN ('" . $data['ItemID'] . "')
                			";
                    $sql2 = "SELECT
                				b.LocationID,
                				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
                				b.AdminGroupID,
                				" . $linventory->getInventoryNameByLang("e.") . ",
                				b.PersonInCharge,
                				" . getNameFieldByLang2("f.") . ",
                				b.DateModified,
                				CONCAT('-',b.MissingQty)
                		FROM
                				INVENTORY_ITEM AS a INNER JOIN
                				INVENTORY_ITEM_MISSING_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
                				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
                				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
                				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
                				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
                				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
                		WHERE
                				a.ItemID IN ('" . $data['ItemID'] . "')
                		";

                    $sql = $sql1 . " union " . $sql2;
                    $arr_stocktake_result = $linventory->returnArray($sql, 8);

                    if ($this_item_type == 1) {
                        $sql = "SELECT
            				b.LocationID,
            				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
            			FROM
            				INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
            				INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
            				INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
            				inner join INVENTORY_LOCATION_BUILDING as d on (d.BuildingID = c.BuildingID)
            			WHERE
            				a.ItemID IN ('" . $data['ItemID'] . "')
                            AND a.LocationID = '$this_LocationID' 
                            AND a.GroupInCharge = ".$this_GroupID."
            			";
                    } else {
                        $sql = "SELECT
            				b.LocationID,
            				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
            			FROM
            				INVENTORY_ITEM_BULK_EXT AS a INNER JOIN
                            INVENTORY_ITEM_BULK_LOCATION AS bl ON (bl.ItemID = a.ItemID) INNER JOIN
            				INVENTORY_LOCATION AS b ON (bl.LocationID = b.LocationID) INNER JOIN
            				INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
            				inner join INVENTORY_LOCATION_BUILDING as d on (d.BuildingID = c.BuildingID)
            			WHERE
            				a.ItemID IN ('" . $data['ItemID'] . "')
                            AND bl.LocationID = '$this_LocationID' 
                            AND bl.GroupInCharge = ".$this_GroupID."
            			";
                    }
                    $arr_original_location = $linventory->returnArray($sql, 2);
                    list ($original_location_id, $original_location_name) = $arr_original_location[0];

                    // #####################
                    // table content
                    // #####################
                    
                    if (sizeof($arr_stocktake_result) > 0) {
                        list ($location_id, $found_location, $admin_group_id, $groupInCharge, $personInCharge_id, $personInCharge, $record_date, $qty) = $arr_stocktake_result[0];
                        
                        if($qty < 0){
                            $totalQty = $qty + $data['Qty'];
                        }else{
                            $totalQty = $data['Qty'] + $qty;
                        }
//                         $price = $totalQty * $data['unit_price'];
//                         $price = $price > 0 ? $price : 0;
                        $table .= "<tr>";
                        $table .= "<td class='eSporttdborder'>" . $data['Barcode'] . "</td>";
                        $table .= "<td class='eSporttdborder'>" . $data['ItemName'] . "</td>";
                        // $table .= "<td class='eSporttdborder'>" . $data['ItemDescription'] . "</td>";
                        $table .= "<td class='eSporttdborder'>" . $thisGroupName;
                        $table .= "<td class='eSporttdborder'>" . $original_location_name;
                        $table .= "<td class='eSporttdborder'>" . $found_location;
                        // if ($this_item_type == 2) {
                        $table .= "<td class='eSporttdborder' align='center'>" . $data['Qty'] . "</td>";
                        $table .= "<td class='eSporttdborder' align='center'>" . $totalQty . "</td>";
                        $table .= "<td class='eSporttdborder' align='center'>" . $qty . "</td>";
                        // }
                        $table .= "<td class='eSporttdborder'>" . number_format($data['unit_price'], 2) . "</td>";
                        $table .= "<td class='eSporttdborder'>" . number_format($data['PurchasedPrice'], 2) . "</td>";
                        $table .= "<td class='eSporttdborder'>$record_date</td>";
                        $table .= "</tr>";
                    } else {
                        if ($this_item_type == 1) {
                            $sql = "SELECT
                                    MAX(ss.DateInput)
    						FROM
    								INVENTORY_ITEM AS a INNER JOIN
            					    INVENTORY_ITEM_SINGLE_STATUS_LOG AS ss ON (a.ItemID = ss.ItemID)
    								INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON a.ItemID = b.ItemID
    								INNER JOIN INVENTORY_FUNDING_SOURCE AS f on f.FundingSourceID = b.FundingSource
    								LEFT JOIN INVENTORY_FUNDING_SOURCE AS f2 ON b.FundingSource2 = f2.FundingSourceID
    								INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID
    						WHERE
    								a.ItemType = 1 AND
    								a.RecordStatus = 1 AND
    								b.LocationID = '$this_LocationID'
    								$sql_cond and
    								(a.StockTakeOption='YES' OR (a.StockTakeOption='FOLLOW_SETTING' and b.UnitPrice>=" . $StockTakePriceSingle . "))
    						ORDER BY
    					            ss.RecordDate DESC";
                        } else {
                            $sql = "SELECT
                                    MAX(bl.DateInput),
        							b.Quantity as Qty,
        							avg(d.UnitPrice) as unit_price,
        							a.StockTakeOption as StockTakeOption
        					FROM
        							INVENTORY_ITEM AS a INNER JOIN
                                    INVENTORY_ITEM_BULK_LOG AS bl ON (a.ItemID = bl.ItemID)
        							INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0)
        							INNER JOIN INVENTORY_FUNDING_SOURCE as c on c.FundingSourceID = b.FundingSourceID
        							INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID
        							LEFT JOIN INVENTORY_ITEM_BULK_LOG as d on (d.ItemID=a.ItemID and d.Action=1)
        					WHERE
        							a.ItemType = 2 AND
        							a.RecordStatus = 1 AND
        							b.LocationID ='$this_LocationID'
        							$sql_cond
        					group by
        						b.ItemID, b.FundingSourceID
        					having
        						StockTakeOption ='YES' or (StockTakeOption='FOLLOW_SETTING' and Qty*unit_price >=" . $StockTakePriceBulk . ")
        					ORDER BY
        					    bl.RecordDate DESC";

                        }
                        $date_result = $linventory->returnVector($sql);

                        $price = ($data['PurchasedPrice'] == 0) ? ($data['unit_price'] * $data['Qty']) : $data['PurchasedPrice'];
//                         $price = $data['Qty'] * $data['unit_price'];
//                         $price = ($price > 0) ? $price : 0;
                        $table .= "<tr>";
                        $table .= "<td class='eSporttdborder'>" . $data['Barcode'] . "</td>";
                        $table .= "<td class='eSporttdborder'>" . $data['ItemName'] . "</td>";
                        // $table .= "<td class='eSporttdborder'>" . $data['ItemDescription'] . "</td>";
                        $table .= "<td class='eSporttdborder'>" . $thisGroupName;
                        $table .= "<td class='eSporttdborder'>" . $original_location_name;
                        $table .= "<td class='eSporttdborder'>" . $original_location_name;
                        // if ($this_item_type == 2) {
                        $table .= "<td class='eSporttdborder' align='center'>" . $data['Qty'] . "</td>";
                        $table .= "<td class='eSporttdborder' align='center'>" . $data['Qty'] . "</td>";
                        $table .= "<td class='eSporttdborder' align='center'>0</td>";
                        // }
                        $table .= "<td class='eSporttdborder'>" . number_format($data['unit_price'],2) . "</td>";
                        $table .= "<td class='eSporttdborder'>" . number_format($price, 2) . "</td>";
                        $table .= "<td class='eSporttdborder'>" . $date_result[0] . "</td>";
                        $table .= "</tr>";
                    }
                }
            }
        }
    }
}
$table .= "</table>";

$table .= "<div style='page-break-after:always'>&nbsp;</div>";

if (! $with_data) {
    $table .= "<table border='0' width='100%' >";
    $table .= "<tr>";
    $table .= "<td align='center'><br><br><br><br><br>" . $Lang['General']['NoRecordFound'] . "</td>";
    $table .= "</tr>";
    $table .= "</table>";
}

?>
<?if($with_data){?>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	align="center" class='print_hide'>
	<tr>
		<td align="right">
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?>
		</td>
	</tr>
</table>
<? } ?>
<style type='text/css'>
html, body, table {
	font-family: Times New Roman;
}
</style>
<?=$table?>
<?intranet_closedb();?>