<?php
// using: Henry
// ####################################
//
// with $sys_custom['eInventoryCustForSKH']
//
// ####################################
// Date:   2019-02-12  Isaac
// Fixed sql incorrect usage of UNION and ORDER BY
//
// Date:   2018-10-10  Isaac
// Added ORDER BY a.ItemCode to sql for export to consistent with stock take
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2014-12-19 Henry
// Fixed: items show incorrect quantity at Fixed Asset Stocktake list [Case#F73099]
// Deploy: ip.2.5.6.1.1
//
// Date: 2014-10-07 YatWoon
// Fixed: hide non-stocktake item in the report [Case#J68923]
// Deploy: ip.2.5.5.10.1
// ####################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN . "/layout/print_header_no25css.php");
if (! sizeof($_POST)) {
    header("Location: index.php");
}
intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// Retreive item
$with_data = 0;
foreach ($targetLocation as $k => $this_LocationID) {
    // Location Name
    $thisRoom = new Room($this_LocationID);
    $thisLocationName = $thisRoom->RoomName;
    $thisLocationResult = array();
    
    // Loop with Resource ManagementGroup
    foreach ($targetGroup as $k2 => $this_GroupID) {
        $thisGroupName = $linventory->returnGroupNameByGroupID($this_GroupID);
        
        // #### Single item
        // $single_funding = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', ".$linventory->getInventoryNameByLang("f.").", concat(".$linventory->getInventoryNameByLang("f.").",', ',".$linventory->getInventoryNameByLang("f2.")."))";
        $single_funding = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', f.code, concat(f.code,', ',f2.code))";
        $sql1 = "SELECT
						a.ItemCode, 
						$single_funding as FundingCode, 
						" . $linventory->getInventoryNameByLang("d2.") . " as SubCatName,
						" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName, 
						" . $linventory->getInventoryDescriptionNameByLang("a.") . " as ItemDescription, 
						1 as Qty, 
						b.UnitPrice as unit_price, 
						a.StockTakeOption as StockTakeOption
				FROM 
						INVENTORY_ITEM AS a 
						INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON a.ItemID = b.ItemID 
						INNER JOIN INVENTORY_FUNDING_SOURCE AS f on f.FundingSourceID = b.FundingSource 
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS f2 ON b.FundingSource2 = f2.FundingSourceID 
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID  
				WHERE
						a.ItemType = 1 AND
						a.RecordStatus = 1 AND
						b.LocationID = '$this_LocationID' AND 
						b.GroupInCharge = '$this_GroupID' and 
						(a.StockTakeOption='YES' OR (a.StockTakeOption='FOLLOW_SETTING' and b.UnitPrice>=" . $linventory->StockTakePriceSingle . "))
                        ";
        
        // #### Bulk item
        $sql2 = "SELECT
						a.ItemCode, 
						c.code as FundingCode, 
						" . $linventory->getInventoryNameByLang("d2.") . " as SubCatName,
						" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName, 
						" . $linventory->getInventoryDescriptionNameByLang("a.") . " as ItemDescription, 
						b.Quantity as Qty, 
						avg(d.UnitPrice) as unit_price, 
						a.StockTakeOption as StockTakeOption
				FROM
						INVENTORY_ITEM AS a 
						INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) 
						INNER JOIN INVENTORY_FUNDING_SOURCE as c on c.FundingSourceID = b.FundingSourceID 
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID  
						LEFT JOIN INVENTORY_ITEM_BULK_LOG as d on (d.ItemID=a.ItemID and d.Action=1)
				WHERE
						a.ItemType = 2 AND
						a.RecordStatus = 1 AND
						b.LocationID ='$this_LocationID' AND
						b.GroupInCharge = '$this_GroupID'
				group by 
					b.ItemID, b.FundingSourceID 
				having
				StockTakeOption ='YES' or (StockTakeOption='FOLLOW_SETTING' and Qty*unit_price >=" . $linventory->StockTakePriceBulk . ")
                ";
        $sql = $sql1 . " union " . $sql2 . " ORDER BY ItemCode";
        $arr_result = $linventory->returnArray($sql);
        if (empty($arr_result))
            continue;
        $with_data = 1;
        
        // #####################
        // Report Title
        // #####################
        $table .= "<table border='0' width='100%'>";
        $table .= "<tr>";
        $table .= "<td align='center' colspan='2'><font size=3><b>" . GET_SCHOOL_NAME() . "</b></font></td>";
        $table .= "</tr>";
        $table .= "<tr>";
        $table .= "<td align='center' colspan='2'><font size=3><b>" . $Lang['eInventory']['Report']['StocktakeList'] . "</b></font></td>";
        $table .= "</tr>";
        $table .= "<tr>";
        $table .= "<td width='67%'><b>" . $Lang['eEnrolment']['Location'] . "</b> " . $thisLocationName . "</td>";
        $table .= "<td width='33%'><b>" . $Lang['eInventory']['ResourceMgtGpName'] . "</b> " . $thisGroupName . "</td>";
        $table .= "</tr>";
        $table .= "</table>";
        
        // #####################
        // records list table
        // #####################
        $table .= "<table border='0' width='100%' cellpadding='02' cellspacing='0' class='eSporttableborder'>";
        $table .= "<tr>";
        $table .= "<td width='17.8%' align='center' class='eSporttdborder'><b>" . $i_InventorySystem_Item_Code . "</b></td>";
        $table .= "<td width='18.9%' align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['SourceFundingCatCode'] . "</b></td>";
        $table .= "<td width='10%' align='center' class='eSporttdborder'><b>" . $i_InventorySystem_SubCategory_Name . "</b></td>";
        $table .= "<td width='10%' align='center' class='eSporttdborder'><b>" . $i_InventorySystem_Item_Name . "</b></td>";
        $table .= "<td width='13%' align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['ItemDetails'] . "</b></td>";
        $table .= "<td width='8.9%' align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['CurrentQty'] . "</b></td>";
        $table .= "<td width='8.9%' align='center' class='eSporttdborder'><b>" . $i_InventorySystem_Stocktake_StocktakeQty . "</b></td>";
        $table .= "<td width='12.5%' align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['FollowupRemarks'] . "</b></td>";
        $table .= "</tr>";
        
        if (sizeof($arr_result) > 0) {
            foreach ($arr_result as $k1 => $data) {
                // #####################
                // table content
                // #####################
                $this_desc = $data['ItemDescription'] ? $data['ItemDescription'] : "&nbsp;";
                $table .= "<tr>";
                $table .= "<td class='eSporttdborder'>" . $data['ItemCode'] . "</td>";
                $table .= "<td class='eSporttdborder'>" . $data['FundingCode'] . "</td>";
                $table .= "<td class='eSporttdborder'>" . $data['SubCatName'] . "</td>";
                $table .= "<td class='eSporttdborder'>" . $data['ItemName'] . "</td>";
                $table .= "<td class='eSporttdborder'>" . $this_desc . "</td>";
                $table .= "<td class='eSporttdborder' align='center'>" . $data['Qty'] . "</td>";
                $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                $table .= "</tr>";
            }
        }
        
        // add empty rows at the end
        for ($row_i = 0; $row_i < 5; $row_i ++) {
            $table .= "<tr>";
            $cell_no = 8;
            for ($cell_i = 0; $cell_i < $cell_no; $cell_i ++)
                $table .= "<td class='eSporttdborder'>&nbsp;</td>";
            $table .= "</tr>";
        }
        
        $table .= "</table>";
        
        // #####################
        // Footer
        // #####################
        $table .= "<br><br><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
        $table .= "<tr><td colspan='2'><br><br>" . $Lang['eInventory']['SotcktakeDate'] . ": ________________________________________</td></tr>";
        $table .= "<tr><td colspan='2'><br><br>" . $Lang['eInventory']['SotcktakePICSignature'] . ": ________________________________________ (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td></tr>";
        $table .= "<tr><td colspan='2'><br><br>" . $Lang['eInventory']['SotcktakeApproverSignature'] . ": ________________________________________ (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td></tr>";
        $table .= "<tr><td colspan='2'><br><br>" . $Lang['eInventory']['PrincipalSignature'] . ": ________________________________________ (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td></tr>";
        $table .= "<tr><td align='center'><br><br><b>" . $Lang['eInventory']['SKH_StocktakeListRemark'] . "</b></td></tr>";
        $table .= "</table>";
        
        $table .= "<div style='page-break-after:always'>&nbsp;</div>";
    }
}

if (! $with_data) {
    $table .= "<table border='0' width='100%' >";
    $table .= "<tr>";
    $table .= "<td align='center'><br><br><br><br><br>" . $Lang['General']['NoRecordFound'] . "</td>";
    $table .= "</tr>";
    $table .= "</table>";
}

?>
<?if($with_data){?>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	align="center" class='print_hide'>
	<tr>
		<td align="right">
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?>
		</td>
	</tr>
</table>
<? } ?>
<style type='text/css'>
html, body, table {
	font-family: Times New Roman;
}
</style>
<?=$table?>
<?intranet_closedb();?>