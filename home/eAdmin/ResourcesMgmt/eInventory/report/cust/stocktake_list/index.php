<?php
# using: Henry

######################################
#	2015-05-12	Henry
#		add general print for no flag client (general deploy)
#	
#	2014-04-14	YatWoon
#		Add "select all" button for resources group selection
#
#	2014-04-09	YatWoon
#		Add SKH IP eInventory customizaiton ($sys_custom['eInventoryCustForSKH'])
#
#	2013	Yatwoon
#		Original is a customization report for $sys_custom['CatholicEducation_eInventory']
#
######################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Reports_StocktakeListReport";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$llocation_ui	= new liblocation_ui();

$TAGS_OBJ[] = array($Lang['eInventory']['Report']['StocktakeList']);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetLocation, "targetLocation[]", "", 1, "", "", "", "", "", 0, 10);

#################################################################
# $sys_custom['CatholicEducation_eInventory']  > print.php
# $sys_custom['eInventoryCustForSKH'] > skh_print.php
#################################################################
if($sys_custom['eInventoryCustForSKH'])
{
	$group_selection = $linventory->returnAdminGroupSelection();
	$group_selection .= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['targetGroup[]']); return false;");
	$action_page = "print_skh.php";
}
else if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['StocktakeListReport'])
{
	$action_page = "print.php";
}
else{
	$group_selection = $linventory->returnAdminGroupSelection();
	$group_selection .= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['targetGroup[]']); return false;");
	$action_page = "print_general.php";
}
?>

<script language="javascript">
<!--
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function checkForm()
{
 	if(!countOption(document.getElementById('targetLocation[]')))
 	{
 		alert('<?=$i_InventorySystem['jsLocationCheckBox']?>');
		return false;	
 	}
 	
 	<? if($sys_custom['eInventoryCustForSKH'] || (!$sys_custom['CatholicEducation_eInventory'] && !$sys_custom['StocktakeListReport'])) {?>
 	if(!countOption(document.getElementById('targetGroup[]')))
 	{
 		alert('<?=$i_InventorySystem['jsGroupCheckBox']?>');
		return false;	
 	}
 	<? } ?>
 	
	return true;
}
//-->
</script>

<form name="form1" action="<?=$action_page?>" method="POST" target="_blank" onSubmit="return checkForm()">
<div class="this_table">
	<table class="form_table_v30">
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem['Location']?></td>
		<td colspan="3"><?=$location_selection?></td>
	</tr>
	
	<? if($sys_custom['eInventoryCustForSKH'] || (!$sys_custom['CatholicEducation_eInventory'] && !$sys_custom['StocktakeListReport'])) {?>
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem['Caretaker']?></td>
		<td colspan="3"><?=$group_selection?></td>
	</tr>
	<? } ?>
	
	</table>

	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit");?> 
		<p class="spacer"></p>
	</div>
</div>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>