<?php
// using :

/**
 * *************************************************************
 *
 * 2019-05-13 (Henry)
 *      Security fix: SQL without quote
 * 
 * Date: 2018-02-22 Henry
 * add access right checking [Case#E135442]
 *
 * Date: 2017-12-19 Cameron
 * redirect to reports.php in PowerClass with error message if basic setting is not setup
 *
 * Date: 2013-06-24 YatWoon
 * fixed: set the checking array to array type to prevent error that cause by empty value with function in_array [Case#2013-0624-0959-35071]
 *
 * Date: 2013-05-10 YatWoon
 * fixed: a vairable is overwrite the foreach loop variable that cause the result is messed. [Case#2013-0508-1106-39156]
 *
 * Date: 2012-05-24 YatWoon
 * fixed: incorrect sql query
 *
 * Date: 2012-02-14 YatWoon
 * fixed: missing to check only select the related group if the bulk item qty>0
 *
 * Date: 2011-06-13 YatWoon
 * revised retrieve location query (fixed: retrieve written-off items)
 *
 * Date: 2011-03-01 YatWoon
 * revised retrieve single / bulk item stocktake down workflow
 *
 * Date: 2011-02-28 YatWoon
 * update # get all location with related Management group #
 * add Item record status checking in query
 *
 * 2011-02-22 (Yuen) :
 * i. added Get_Building_Floor_Selection() to display building and floor list in one pull-down menu
 *
 * *************************************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Report_StockTakeProgress";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui = new liblocation_ui();

// Init JS for date picker #
$js_init .= '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.js"></script>';
$js_init .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.css" type="text/css" />';

// ## Check user have set all the basic setting ###
$sql = "SELECT LocationID FROM INVENTORY_LOCATION where RecordStatus=1";
$arr_location_check = $linventory->returnVector($sql);

$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
$arr_group_check = $linventory->returnVector($sql);

$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY";
$arr_category_check = $linventory->returnVector($sql);

$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE";
$arr_funding_check = $linventory->returnVector($sql);

if ((sizeof($arr_location_check) == 0) || (sizeof($arr_group_check) == 0) || (sizeof($arr_category_check) == 0) || (sizeof($arr_funding_check) == 0)) {
    if ($sys_custom['PowerClass']) {
        header("Location: " . $PATH_WRT_ROOT . "home/PowerClass/reports.php?error=MissingBasicSetting");
    } else {
        header("Location: " . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php");
    }
}
// ## END ###

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_StocktakeProgress . ' ' . $i_InventorySystem['Report'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arr_done_location = array();
$arr_done_location_group = array();

$stocktake_start_date = $linventory->getStocktakePeriodStart();
$stocktake_end_date = $linventory->getStocktakePeriodEnd();

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'StocktakeReminder[]','category_level2_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reminder4.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_InventorySystem_Send_Reminder
					</a>
				</td>";

if ($start_date == "") {
    $start_date = $stocktake_start_date;
}
if ($end_date == "") {
    $end_date = $stocktake_end_date;
}

// # Date Range ##
$table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Setting_StockCheckDateRange</td>";
$table_content .= "<td class=\"tabletext\">
						$i_InventorySystem_From &nbsp; <input type=\"text\" name=\"start_date\" id=\"start_date\" class=\"textboxnum\" maxlength=\"10\" value=\"$start_date\"> 
						$i_InventorySystem_To &nbsp; <input type=\"text\" name=\"end_date\" id=\"end_date\" class=\"textboxnum\" maxlength=\"10\" value=\"$end_date\">
					</td></tr>\n";

$arr_groupBy = array(
    array(
        1,
        $i_InventorySystem['Location']
    ),
    array(
        3,
        $i_InventorySystem['Caretaker']
    )
);
$groupby_selection = getSelectByArray($arr_groupBy, "name=\"groupBy\" id=\"groupBy\" onChange=\"this.form.submit();\" ", $groupBy);

// # report - group by
$table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Report_Stocktake_groupby</td>";
$table_content .= "<td>$groupby_selection</td></tr>\n";

// # group by - location
if ($groupBy == 1) {
    $building_selection = $llocation_ui->Get_Building_Selection($TargetBuilding, 'TargetBuilding', 'resetLocationSelection(\'Building\'); this.form.submit();', 0, 0, '');
    
    $table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td>" . $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '') . "</td>";
    
    if ($TargetFloor != "") {
        $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$TargetFloor."' AND RecordStatus = 1";
        $result = $linventory->returnArray($sql, 2);
        
        $room_selection = "<select id=\"TargetRoom[]\" name=\"TargetRoom[]\" multiple size=\"10\">";
        if (sizeof($TargetRoom) == 0) {
            $room_selection .= "<option value=\"\" selected> - select room - </option>";
        } else {
            $room_selection .= "<option value=\"\" > - select room - </option>";
        }
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($location_id, $location_name) = $result[$i];
                
                $selected = "";
                if (is_array($TargetRoom)) {
                    if (in_array($location_id, $TargetRoom)) {
                        $selected = " SELECTED ";
                    }
                }
                $room_selection .= "<option value=\"$location_id\" $selected>$location_name</option>";
            }
        }
        $room_selection .= "</select>";
        
        $table_content .= "<tr><td></td>";
        $table_content .= "<td>" . $room_selection . "</td>";
    }
}
// # group by - resource mgmt group
if ($groupBy == 3) {
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
    $result = $linventory->returnArray($sql, 2);
    
    // $group_selection = getSelectByArray($arr_group, " name=\"targetGroup\" ", $targetGroup, 1, 0);
    
    $group_selection = "<select id=\"targetGroup[]\" name=\"targetGroup[]\" multiple size=\"10\">";
    if (sizeof($targetGroup) == 0) {
        $group_selection .= "<option value=\"\" selected> - select group - </option>";
    } else {
        $group_selection .= "<option value=\"\"> - select group - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($group_id, $group_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetGroup)) {
                if (in_array($group_id, $targetGroup)) {
                    $selected = " SELECTED ";
                }
            }
            $group_selection .= "<option value=\"$group_id\" $selected>$group_name</option>";
        }
    }
    $group_selection .= "</select>";
    
    $table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $table_content .= "<td>" . $group_selection . "</td>";
}

$table_content .= "<tr><td colspan=\"2\" align=\"center\">" . $linterface->GET_ACTION_BTN($button_submit, "submit") . " " . "</td></tr>\n";
$table_content .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";

// ## Start to gen report ###
if ($step == "final") {
    if (sizeof($TargetRoom)) {
        $groupby_val = implode(",", $TargetRoom);
    }
    if (sizeof($targetGroup)) {
        $groupby_val = implode(",", $targetGroup);
    }
    $toolbar .= "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$start_date','$end_date','$targetLocation','$targetGroup', '$targetProgress','$groupBy','$groupby_val')", "", "", "", "", 0);
    $toolbar .= $linterface->GET_LNK_EXPORT("stocktake_progress_export.php?start_date='$start_date'&end_date='$end_date'&targetLocation='$targetLocation'&targetGroup='$targetGroup'&targetProgress='$targetProgress'&groupBy=$groupBy&selected_cb=$groupby_val", "", "", "", "", 0) . "</td></tr>\n";
    
    if ($groupBy == 1) {
        $selected_location = implode(",", $TargetRoom);
        $arr_selected_location = $TargetRoom;
        
        // # use to control what data will be shown ##
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
        }
        // # end ##
        
        // get all location with related admin group #
        $sql = "SELECT 
					DISTINCT a.LocationID, b.GroupInCharge, c.GroupInCharge,
					b1.RecordStatus,c1.RecordStatus 
				FROM 
					INVENTORY_LOCATION AS a 

					LEFT OUTER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.LocationID = b.LocationID) 
					LEFT join INVENTORY_ITEM as b1 on (b1.ItemID=b.ItemID)
					
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOCATION AS c ON (a.LocationID = c.LocationID and c.Quantity>0) 
					LEFT join INVENTORY_ITEM as c1 on (c1.ItemID=c.ItemID)
				WHERE
					a.LocationID IN ($selected_location)
					and (b1.RecordStatus=1 or c1.RecordStatus=1)
					";
        $tmp_arr = $linventory->returnArray($sql);
        
        if (sizeof($tmp_arr) > 0) {
            for ($i = 0; $i < sizeof($tmp_arr); $i ++) {
                list ($location_id, $single_item_group, $bulk_item_group, $single_status, $bulk_status) = $tmp_arr[$i];
                
                $arr_tmp_location[] = $location_id;
                
                if (($single_item_group != "") && ($bulk_item_group != "") && $single_status && $bulk_status) {
                    if ($single_item_group != $bulk_item_group) {
                        $arr_tmp_group[$location_id][] = $single_item_group;
                        $arr_tmp_group[$location_id][] = $bulk_item_group;
                        $arr_tmp_location_record_type[$location_id]['type'] = 3; // #### Single + Bulk
                    } else {
                        $arr_tmp_group[$location_id][] = $single_item_group;
                        // $arr_tmp_group[$location_id][] = $bulk_item_group;
                        $arr_tmp_location_record_type[$location_id]['type'] = 3; // #### Single + Bulk
                    }
                } else 
                    if (($single_item_group != "") && ($bulk_item_group == "") && $single_status) {
                        $arr_tmp_group[$location_id][] = $single_item_group;
                        $arr_tmp_location_record_type[$location_id]['type'] = 1; // #### Single only
                    } else 
                        if (($single_item_group == "") && ($bulk_item_group != "") && $bulk_status) {
                            $arr_tmp_group[$location_id][] = $bulk_item_group;
                            $arr_tmp_location_record_type[$location_id]['type'] = 2; // #### Bulk only
                        }
            }
        }
        
        if (! empty($arr_tmp_location) && ! empty($arr_tmp_group)) {
            $arr_tmp_location = array_unique($arr_tmp_location);
            
            // debug_pr("======================= arr_tmp_group ");
            // debug_pr($arr_tmp_group);
            //
            // debug_pr("======================= arr_tmp_location ");
            // debug_pr($arr_tmp_location);
            //
            // ## Start to Gen stocktake done ###
            foreach ($arr_tmp_location as $location_id) {
                $admin_list = implode(",", $arr_tmp_group[$location_id]);
                $arrayGroupInCharge = array_unique($arr_tmp_group[$location_id]);
                
                foreach ($arrayGroupInCharge as $key => $group_in_charge) {
                    // ## single
                    $sql = "SELECT 
									a.LocationID, a.GroupInCharge, b.PersonInCharge, MAX(b.DateInput)
							FROM 
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) 
							WHERE 
									a.LocationID = $location_id AND 
									a.GroupInCharge IN ($group_in_charge) AND 
									b.Action IN (2,3,4) AND
									a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $group_in_charge AND LocationID = $location_id) AND
									(b.RecordDate BETWEEN '$start_date' AND '$end_date') AND
									b.DateInput IN (SELECT MAX(b.DateInput) FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) WHERE a.LocationID = $location_id AND  a.GroupInCharge IN ($group_in_charge) AND b.Action IN (2,3,4) AND a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $group_in_charge AND LocationID = $location_id) AND (b.RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY 
									a.LocationID, a.GroupInCharge";
                    $temp_array1 = $linventory->returnArray($sql, 4);
                    if (sizeof($temp_array1) > 0) {
                        list ($location_id, $group_id, $uid, $date_input) = $temp_array1[0];
                        $single_stocktake_done_rec[] = array(
                            $location_id,
                            $group_id,
                            $uid,
                            $date_input
                        );
                    }
                    
                    // ## bulk
                    $sql = "SELECT
									LocationID, GroupInCharge, PersonInCharge, MAX(DateInput)
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									LocationID = $location_id AND
									GroupInCharge IN ($admin_list) AND 
									Action IN (2,3,4) AND
									ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $group_in_charge AND LocationID = $location_id) AND 
									(RecordDate BETWEEN '$start_date' AND '$end_date') AND
									DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE LocationID = '".$location_id."' AND GroupInCharge IN ($admin_list) AND  Action IN (2,3,4) AND ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = '".$group_in_charge."' AND LocationID = '".$location_id."') AND  (RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY
									LocationID, GroupInCharge";
                    $temp_array2 = $linventory->returnArray($sql, 4);
                    if (sizeof($temp_array2) > 0) {
                        list ($location_id, $group_id, $uid, $date_input) = $temp_array2[0];
                        $bulk_stocktake_done_rec[] = array(
                            $location_id,
                            $group_id,
                            $uid,
                            $date_input
                        );
                    }
                }
                
                // debug_pr("======================= single_stocktake_done_rec ");
                // debug_pr($single_stocktake_done_rec);
                //
                // debug_pr("======================= bulk_stocktake_done_rec ");
                // debug_pr($bulk_stocktake_done_rec);
                
                if (sizeof($single_stocktake_done_rec) > 0) {
                    for ($i = 0; $i < sizeof($single_stocktake_done_rec); $i ++) {
                        list ($location_id, $admin_group_id, $user_id, $record_date) = $single_stocktake_done_rec[$i];
                        
                        if (in_array($admin_group_id, $tmp_arr_group)) {
                            // only add in array if record not exists
                            $exists = 0;
                            if (sizeof($stocktake_done_record) > 0) {
                                foreach ($stocktake_done_record as $k => $d) {
                                    list ($this_location_id, $this_admin_group_id, $this_user_id, $this_record_date) = $d;
                                    if ($location_id == $this_location_id && $admin_group_id == $this_admin_group_id) {
                                        $exists = 1;
                                        break;
                                    }
                                }
                            }
                            
                            if (! $exists) {
                                $stocktake_done[] = $location_id;
                                $stocktake_done_record[] = array(
                                    $location_id,
                                    $admin_group_id,
                                    $user_id,
                                    $record_date
                                );
                            }
                        }
                    }
                }
                
                if (sizeof($bulk_stocktake_done_rec) > 0) {
                    for ($i = 0; $i < sizeof($bulk_stocktake_done_rec); $i ++) {
                        list ($location_id, $admin_group_id, $user_id, $record_date) = $bulk_stocktake_done_rec[$i];
                        
                        if (in_array($admin_group_id, $tmp_arr_group)) {
                            // only add in array if record not exists
                            $exists = 0;
                            if (sizeof($stocktake_done_record) > 0) {
                                foreach ($stocktake_done_record as $k => $d) {
                                    list ($this_location_id, $this_admin_group_id, $this_user_id, $this_record_date) = $d;
                                    if ($location_id == $this_location_id && $admin_group_id == $this_admin_group_id) {
                                        $exists = 1;
                                        break;
                                    }
                                }
                            }
                            
                            if (! $exists) {
                                $stocktake_done[] = $location_id;
                                $stocktake_done_record[] = array(
                                    $location_id,
                                    $admin_group_id,
                                    $user_id,
                                    $record_date
                                );
                            }
                        }
                    }
                }
                
                if ($arr_tmp_location_record_type[$location_id]['type'] == 0) {
                    $arr_no_item_record[] = $location_id;
                }
            }
            // ## Gen Stocktake DONE End ###
        }
        
        // debug_pr("============ stocktake_done ");
        // debug_pr($stocktake_done);
        //
        // debug_pr("============ stocktake_done_record ");
        // debug_pr($stocktake_done_record);
        
        if (! empty($arr_tmp_group)) {
            // #### Build stocktake_not_done array
            foreach ($arr_tmp_group as $this_location_id => $this_groups) {
                // if not in stocktake_done_record, then append in stocktake_not_done_rec
                foreach ($this_groups as $temp_k => $this_group) {
                    $exists = 0;
                    if (sizeof($stocktake_done_record) > 0) {
                        foreach ($stocktake_done_record as $k => $d) {
                            list ($temp_location_id, $temp_admin_group_id, $temp_user_id, $temp_record_date) = $d;
                            if ($this_location_id == $temp_location_id && $this_group == $temp_admin_group_id) {
                                $exists = 1;
                                break;
                            }
                        }
                    }
                    if (! $exists) {
                        $stocktake_not_done_location[] = $this_location_id;
                        $stocktake_not_done_rec[$this_location_id][] = $this_group;
                    }
                }
            }
        }
        
        // debug_pr("============ stocktake_not_done_location ");
        // debug_pr($stocktake_not_done_location);
        //
        // debug_pr("============ stocktake_not_done_rec ");
        // debug_pr($stocktake_not_done_rec);
        
        // ## Gen Stocktake Not Done ###
        if (sizeof($arr_no_item_record) > 0) {
            if (sizeof($stocktake_done) > 0) {
                $stocktake_not_done = array_diff($arr_selected_location, $stocktake_done);
                if (is_array($arr_no_item_record))
                    $stocktake_not_done = array_diff($stocktake_not_done, $arr_no_item_record);
            } else {
                $stocktake_not_done = $arr_selected_location;
            }
            
            foreach ($stocktake_not_done as $not_done_location) {
                if (sizeof($arr_tmp_group[$not_done_location]) > 0) {
                    for ($i = 0; $i < sizeof($arr_tmp_group[$not_done_location]); $i ++) {
                        $arr_tmp_group[$not_done_location] = array_unique($arr_tmp_group[$not_done_location]);
                        $target_admin_group = $arr_tmp_group[$not_done_location][$i];
                        
                        // ## Check Stocktake not done location is belong to user or not ###
                        if (in_array($target_admin_group, $tmp_arr_group)) {
                            $stocktake_not_done_record[] = array(
                                $not_done_location,
                                $target_admin_group
                            );
                        }
                    }
                }
            }
        }
        
        if (sizeof($stocktake_not_done_location) > 0) {
            $stocktake_not_done_location = array_unique($stocktake_not_done_location);
            
            foreach ($stocktake_not_done_location as $stocktake_not_done_location_id) {
                if (sizeof($stocktake_not_done_rec[$stocktake_not_done_location_id]) > 0) {
                    $temp_stocktake_not_done_arr = array_unique($stocktake_not_done_rec[$stocktake_not_done_location_id]);
                    
                    foreach ($temp_stocktake_not_done_arr as $stocktake_not_done_group_id) {
                        $stocktake_not_done_record[] = array(
                            $stocktake_not_done_location_id,
                            $stocktake_not_done_group_id
                        );
                    }
                }
            }
        }
        // ## End of Gen stocktake not done ###
        
        $finished_table_content .= "<tr><td class=\"tablename\" colspan=\"4\">$i_InventorySystem_Stocktake_ProgressFinished</td></tr>";
        $finished_table_content .= "<tr class=\"tabletop\">";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_LastStocktakeBy . "</td>";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Last_Stock_Take_Time . "</td>";
        $finished_table_content .= "</tr>";
        
        if (sizeof($stocktake_done_record) > 0) {
            for ($i = 0; $i < sizeof($stocktake_done_record); $i ++) {
                list ($location_id, $admin_group_id, $user_id, $record_date) = $stocktake_done_record[$i];
                
                $sql = "SELECT 
							CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
							CONCAT(" . $linventory->getInventoryNameByLang("c.") . "),
							IFNULL(" . getNameFieldByLang2(".d.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
						FROM
							INVENTORY_LOCATION AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
							INVENTORY_ADMIN_GROUP AS c,
							INTRANET_USER AS d
						WHERE
							a.LocationID = $location_id AND
							c.AdminGroupID = $admin_group_id AND
							d.UserID = $user_id
						";
                $arr_result = $linventory->returnArray($sql, 3);
                
                if ($i % 2 == 0)
                    $css = " class=\"tablerow2\" ";
                else
                    $css = " class=\"tablerow1\" ";
                
                $finished_table_content .= "<tr $css>";
                
                if (sizeof($arr_result) > 0) {
                    list ($location, $admin_group, $user_name) = $arr_result[0];
                    $finished_table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                    $finished_table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($admin_group) . "</td>";
                    $finished_table_content .= "<td class=\"tabletext\">$user_name</td>";
                    $finished_table_content .= "<td class=\"tabletext\">$record_date</td>";
                }
                $finished_table_content .= "</tr>";
            }
        } else {
            $finished_table_content .= "<tr><td colspan=\"4\" class=\"tablerow2 tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
        
        $not_finished_table_content .= "<tr><td class=\"tablename\" colspan=\"4\"><font color=\"red\">$i_InventorySystem_Stocktake_ProgressNotFinished</font></td></tr>";
        $not_finished_table_content .= "<tr class=\"tabletop\">";
        $not_finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
        $not_finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $not_finished_table_content .= "</tr>";
        
        if (sizeof($stocktake_not_done_record) > 0) {
            for ($i = 0; $i < sizeof($stocktake_not_done_record); $i ++) {
                list ($location_id, $admin_group_id) = $stocktake_not_done_record[$i];
                $sql = "SELECT 
							CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
							CONCAT(" . $linventory->getInventoryNameByLang("c.") . ")
						FROM 
							INVENTORY_LOCATION AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
							INVENTORY_ADMIN_GROUP AS c
						WHERE 
							a.LocationID = $location_id AND
							c.AdminGroupID = $admin_group_id
						";
                $arr_result = $linventory->returnArray($sql, 2);
                if (sizeof($arr_result) > 0) {
                    list ($location, $admin_group) = $arr_result[0];
                    $not_finished_table_content .= "<tr class=\"inventory_warning_bg\">";
                    $not_finished_table_content .= "<td class=\"formfieldtitle tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                    $not_finished_table_content .= "<td class=\"formfieldtitle tabletext\">" . intranet_htmlspecialchars($admin_group) . "</td>";
                    $not_finished_table_content .= "</tr>";
                }
            }
        } else {
            $not_finished_table_content .= "<tr class=\"inventory_warning_bg\"><td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
    }
    if ($groupBy == 3) {
        $group_list = implode(",", $targetGroup);
        $groupByGroupid = $targetGroup;
        
        // # use to control what data will be shown ##
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
        }
        // # end ##
        
        // get all location with related Management group #
        $sql = "SELECT 
					DISTINCT a.AdminGroupID, b.LocationID, c.LocationID,
					b1.RecordStatus,c1.RecordStatus
				FROM 
					INVENTORY_ADMIN_GROUP AS a 
					LEFT OUTER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.AdminGroupID = b.GroupInCharge) 
					left join INVENTORY_ITEM as b1 on (b1.ItemID=b.ItemID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOCATION AS c ON (a.AdminGroupID = c.GroupInCharge and c.Quantity>0) 
					left join INVENTORY_ITEM as c1 on (c1.ItemID=c.ItemID)
				WHERE
					a.AdminGroupID IN ($group_list)
					and (b1.RecordStatus=1 or c1.RecordStatus=1)
					";
        $tmp_arr = $linventory->returnArray($sql);
        
        if (sizeof($tmp_arr) > 0) {
            for ($i = 0; $i < sizeof($tmp_arr); $i ++) {
                list ($admin_group_id, $single_location_id, $bulk_location_id, $single_status, $bulk_status) = $tmp_arr[$i];
                $arr_tmp_admin_group[] = $admin_group_id;
                if (($single_location_id != "") && ($bulk_location_id != "") && $single_status && $bulk_status) {
                    if ($single_location_id != $bulk_location_id) {
                        $arr_tmp_location[$admin_group_id][] = $single_location_id;
                        $arr_tmp_location[$admin_group_id][] = $bulk_location_id;
                        $arr_tmp_location_record_type[$admin_group_id]['type'] = 3; // mix
                    } else {
                        $arr_tmp_location[$admin_group_id][] = $single_location_id;
                        // $arr_tmp_location[$admin_group_id][] = $bulk_location_id;
                        $arr_tmp_location_record_type[$admin_group_id]['type'] = 3;
                    }
                } else 
                    if (($single_location_id != "") && ($bulk_location_id == "") && $single_status) {
                        $arr_tmp_location[$admin_group_id][] = $single_location_id;
                        $arr_tmp_location_record_type[$admin_group_id]['type'] = 1; // single only
                    } else 
                        if (($single_location_id == "") && ($bulk_location_id != "") && $bulk_status) {
                            $arr_tmp_location[$admin_group_id][] = $bulk_location_id;
                            $arr_tmp_location_record_type[$admin_group_id]['type'] = 2; // bulk only
                        }
            }
        }
        if (sizeof($arr_tmp_admin_group)) {
            $arr_tmp_admin_group = array_unique($arr_tmp_admin_group);
            
            // ## Start to Gen stocktake done ###
            foreach ($arr_tmp_admin_group as $admin_group_id) {
                $stocktake_done_rec = array();
                if ($arr_tmp_location_record_type[$admin_group_id]['type'] == 3) {
                    $arr_location_list = array_unique($arr_tmp_location[$admin_group_id]);
                    $location_list = implode(",", $arr_location_list);
                    $arrayLocationList = array_unique($arr_tmp_location[$admin_group_id]);
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT 
											a.LocationID, a.GroupInCharge, b.PersonInCharge, MAX(b.DateInput)
									FROM 
											INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
											INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) 
									WHERE 
											a.GroupInCharge = $admin_group_id AND 
											a.LocationID IN ($location_id) AND 
											b.Action IN (2,3,4) AND
											a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
											(b.RecordDate BETWEEN '$start_date' AND '$end_date') AND
											b.DateInput IN (SELECT MAX(b.DateInput) FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) WHERE a.LocationID = $location_id AND  a.GroupInCharge IN ($admin_group_id) AND b.Action IN (2,3,4) AND a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (b.RecordDate BETWEEN '$start_date' AND '$end_date'))
									GROUP BY 
											a.LocationID";
                        $temp_array1 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array1) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array1[0];
                            $single_stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    
                    if (sizeof($single_stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($single_stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $single_stocktake_done_rec[$i];
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT
										LocationID, GroupInCharge, PersonInCharge, MAX(DateInput)
								FROM
										INVENTORY_ITEM_BULK_LOG
								WHERE
										GroupInCharge = $admin_group_id AND
										LocationID IN ($location_id) AND 
										Action IN (2,3,4) AND
										ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
										(RecordDate BETWEEN '$start_date' AND '$end_date') AND 
										DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE GroupInCharge = $admin_group_id AND LocationID IN ($location_id) AND Action IN (2,3,4) AND ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (RecordDate BETWEEN '$start_date' AND '$end_date'))
								GROUP BY
										LocationID";
                        
                        $temp_array2 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array2) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array2[0];
                            $bulk_stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    if (sizeof($bulk_stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($bulk_stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $bulk_stocktake_done_rec[$i];
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    // hdebug_pr($admin_group_id);
                    // hdebug_pr("stocktake_done_location:");
                    // hdebug_pr($stocktake_done_location[$admin_group_id]);
                    if (sizeof($stocktake_done_location[$admin_group_id]) > 0) {
                        // hdebug_pr("arr_location_list:");
                        // hdebug_pr($arr_location_list);
                        $arr_not_done_location = array_diff($arr_location_list, $stocktake_done_location[$admin_group_id]);
                        if (sizeof($arr_not_done_location) > 0) {
                            foreach ($arr_not_done_location as $not_done_location_id) {
                                $stocktake_not_done_record[] = array(
                                    $admin_group_id,
                                    $not_done_location_id
                                );
                            }
                        }
                    }
                }
                if ($arr_tmp_location_record_type[$admin_group_id]['type'] == 2) {
                    // ## Stocktake done - only Bulk Item ###
                    $arr_location_list = array_unique($arr_tmp_location[$admin_group_id]);
                    $location_list = implode(",", $arr_location_list);
                    
                    $arrayLocationList = array_unique($arr_tmp_location[$admin_group_id]);
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT
										LocationID, GroupInCharge, PersonInCharge, MAX(DateInput)
								FROM
										INVENTORY_ITEM_BULK_LOG
								WHERE
										GroupInCharge = $admin_group_id AND
										LocationID IN ($location_id) AND 
										Action IN (2,3,4) AND
										ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
										(RecordDate BETWEEN '$start_date' AND '$end_date') AND 
										DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE GroupInCharge = $admin_group_id AND LocationID IN ($location_id) AND Action IN (2,3,4) AND ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (RecordDate BETWEEN '$start_date' AND '$end_date'))
								GROUP BY
										LocationID";
                        
                        $temp_array3 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array3) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array3[0];
                            $stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    if (sizeof($stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $stocktake_done_rec[$i];
                            
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    
                    if (sizeof($stocktake_done_location[$admin_group_id]) > 0) {
                        $arr_not_done_location = array_diff($arr_location_list, $stocktake_done_location[$admin_group_id]);
                        if (sizeof($arr_not_done_location) > 0) {
                            foreach ($arr_not_done_location as $not_done_location_id) {
                                $stocktake_not_done_record[] = array(
                                    $admin_group_id,
                                    $not_done_location_id
                                );
                            }
                        }
                    }
                }
                if ($arr_tmp_location_record_type[$admin_group_id]['type'] == 1) {
                    // ## Stocktake done - only Single Item ###
                    $arr_location_list = array_unique($arr_tmp_location[$admin_group_id]);
                    $location_list = implode(",", $arr_location_list);
                    
                    $arrayLocationList = array_unique($arr_tmp_location[$admin_group_id]);
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT 
										a.LocationID, a.GroupInCharge, b.PersonInCharge, MAX(b.DateInput)
								FROM 
										INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
										INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) 
								WHERE 
										a.GroupInCharge = $admin_group_id AND 
										a.LocationID IN ($location_id) AND 
										b.Action IN (2,3,4) AND
										a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
										(b.RecordDate BETWEEN '$start_date' AND '$end_date') AND
										b.DateInput IN (SELECT MAX(b.DateInput) FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) WHERE a.LocationID = $location_id AND  a.GroupInCharge IN ($admin_group_id) AND b.Action IN (2,3,4) AND a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (b.RecordDate BETWEEN '$start_date' AND '$end_date'))
								GROUP BY 
										a.LocationID";
                        $temp_array4 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array4) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array4[0];
                            $stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    
                    if (sizeof($stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $stocktake_done_rec[$i];
                            
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    
                    if (sizeof($stocktake_done_location[$admin_group_id]) > 0) {
                        $arr_not_done_location = array_diff($arr_location_list, $stocktake_done_location[$admin_group_id]);
                        if (sizeof($arr_not_done_location) > 0) {
                            foreach ($arr_not_done_location as $not_done_location_id) {
                                $stocktake_not_done_record[] = array(
                                    $admin_group_id,
                                    $not_done_location_id
                                );
                            }
                        }
                    }
                }
                if ($arr_tmp_location_record_type[$location_id]['type'] == 0) {
                    $arr_no_item_record[] = $location_id;
                }
            }
        }
        // ## Gen Stocktake DONE End ###
        
        // ## Gen Stocktake Not Done ###
        if (sizeof($stocktake_done) > 0) {
            $stocktake_not_done = array_diff($groupByGroupid, $stocktake_done);
            if (is_array($arr_no_item_record))
                $stocktake_not_done = array_diff($stocktake_not_done, $arr_no_item_record);
        } else {
            $stocktake_not_done = $targetGroup;
        }
        
        foreach ($stocktake_not_done as $not_done_group) {
            if (sizeof($arr_tmp_location[$not_done_group]) > 0)
                $arr_recorded_location = array_unique($arr_tmp_location[$not_done_group]);
            else
                $arr_recorded_location = $arr_tmp_location[$not_done_group];
            
            if (sizeof($arr_recorded_location) > 0) {
                foreach ($arr_recorded_location as $target_location) {
                    // ## Check Stocktake not done location is belong to user or not ###
                    if (in_array($not_done_group, $tmp_arr_group)) {
                        $stocktake_not_done_record[] = array(
                            $not_done_group,
                            $target_location
                        );
                    }
                }
            }
        }
        // ## End of Gen stocktake not done ###
        
        $finished_table_content .= "<tr><td class=\"tablename\" colspan=\"4\">$i_InventorySystem_Stocktake_ProgressFinished</td></tr>";
        $finished_table_content .= "<tr class=\"tabletop\">";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_LastStocktakeBy . "</td>";
        $finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Last_Stock_Take_Time . "</td>";
        $finished_table_content .= "</tr>";
        
        if (sizeof($stocktake_done_record) > 0) {
            for ($i = 0; $i < sizeof($stocktake_done_record); $i ++) {
                list ($location_id, $admin_group_id, $user_id, $record_date) = $stocktake_done_record[$i];
                
                $sql = "SELECT 
							CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
							CONCAT(" . $linventory->getInventoryNameByLang("c.") . "),
							IFNULL(" . getNameFieldByLang2(".d.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
						FROM
							INVENTORY_LOCATION AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
							INVENTORY_ADMIN_GROUP AS c,
							INTRANET_USER AS d
						WHERE
							a.LocationID = $location_id AND
							c.AdminGroupID = $admin_group_id AND
							d.UserID = $user_id
						";
                $arr_result = $linventory->returnArray($sql, 3);
                
                if ($i % 2 == 0)
                    $css = " class=\"tablerow2\" ";
                else
                    $css = " class=\"tablerow1\" ";
                
                $finished_table_content .= "<tr $css>";
                
                if (sizeof($arr_result) > 0) {
                    list ($location, $admin_group, $user_name) = $arr_result[0];
                    $finished_table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                    $finished_table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($admin_group) . "</td>";
                    $finished_table_content .= "<td class=\"tabletext\">$user_name</td>";
                    $finished_table_content .= "<td class=\"tabletext\">$record_date</td>";
                }
                $finished_table_content .= "</tr>";
            }
        } else {
            $finished_table_content .= "<tr><td colspan=\"4\" class=\"tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
        
        $not_finished_table_content .= "<tr><td class=\"tablename\" colspan=\"4\"><font color=\"red\">$i_InventorySystem_Stocktake_ProgressNotFinished</font></td></tr>";
        $not_finished_table_content .= "<tr class=\"tabletop\">";
        $not_finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
        $not_finished_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $not_finished_table_content .= "</tr>";
        
        if (sizeof($stocktake_not_done_record) > 0) {
            for ($i = 0; $i < sizeof($stocktake_not_done_record); $i ++) {
                list ($admin_group_id, $location_id) = $stocktake_not_done_record[$i];
                $sql = "SELECT 
							CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
							CONCAT(" . $linventory->getInventoryNameByLang("c.") . ")
						FROM 
							INVENTORY_LOCATION AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
							INVENTORY_ADMIN_GROUP AS c
						WHERE 
							a.LocationID = $location_id AND
							c.AdminGroupID = $admin_group_id
						";
                $arr_result = $linventory->returnArray($sql, 2);
                if (sizeof($arr_result) > 0) {
                    list ($location, $admin_group) = $arr_result[0];
                    $not_finished_table_content .= "<tr class=\"inventory_warning_bg\">";
                    $not_finished_table_content .= "<td class=\"formfieldtitle tabletext\">" . intranet_htmlspecialchars($location) . "</td>";
                    $not_finished_table_content .= "<td class=\"formfieldtitle tabletext\">" . intranet_htmlspecialchars($admin_group) . "</td>";
                    $not_finished_table_content .= "</tr>";
                }
            }
        } else {
            $not_finished_table_content .= "<tr class=\"inventory_warning_bg\"><td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
    }
}
?>

<?=$js_init;?>

<script language="javascript">

function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("TargetFloor")){
			document.getElementById("TargetFloor").value = "";
		}
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
}

function openPrintPage(StartDate, EndDate, TargetLocation, TargetGroup, TargetProgress,GroupBy, GroupByValue)
{
	newWindow(" stocktake_progress_print.php?start_date='"+StartDate+"'&end_date='"+EndDate+"'&targetLocation="+TargetLocation+"&targetGroup="+TargetGroup+"&targetProgress="+TargetProgress+"&groupBy="+GroupBy+"&selected_cb="+GroupByValue,10);
}

function checkForm()
{
	var groupby = document.getElementById('groupBy').value;
	var location_cb = 0;
	var group_cb = 0;
	
	if(groupby == 1) {
		if(document.getElementById('TargetRoom[]')) {
			for(i=0; i<document.getElementById('TargetRoom[]').length; i++) {
				if(document.getElementById('TargetRoom[]')[i].selected == true) {
					if(document.getElementById('TargetRoom[]')[i].value != "") {
						location_cb++;
					}
				}
			}
		}
	}
	if(groupby == 3){
		if(document.getElementById('targetGroup[]')) {
			for(i=0; i<document.getElementById('targetGroup[]').length; i++) {
				if(document.getElementById('targetGroup[]')[i].selected == true) {
					if(document.getElementById('targetGroup[]')[i].value != "") {
						group_cb++;
					}
				}
			}
		}
	}
		
	if(location_cb>0||group_cb>0||groupby==2){
		document.form1.step.value = "final";
		return true;
	}
	else if(groupby==1)
	{
		alert('<?=$i_InventorySystem['jsLocationCheckBox']?>');
		return false;
	}
	else if(groupby==3)
	{
		alert('<?=$i_InventorySystem['jsGroupCheckBox']?>');
		return false;	
	}
	else if(groupby=="")
	{
		alert('<?=$i_InventorySystem['jsLocationGroupCheckBox']?>');
		return false;	
	}		
}		

$(document).ready(function(){ 
	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
	$('#start_date').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
	$('#end_date').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
});			
</script>

<br>
<form name="form1" action="stocktake_progress.php" method="POST"
	onSubmit="return checkForm()">
	<table border="0" width="90%" align="center" cellspacing="0"
		cellpadding="5">
<?=$table_content;?>
</table>
	<br>

<? if($groupBy != "") { ?>
<table border="0" width="90%" cellspacing="0" cellpadding="5">
<?=$toolbar;?>
</table>
	<br>

	<table border="0" width="100%" align="center" cellpadding="5"
		cellspacing="0">
<?=$finished_table_content;?>
</table>

	<br>

	<table border="0" width="100%" align="center" cellpadding="5"
		cellspacing="0">
<?=$not_finished_table_content;?>
</table>
<? } ?>

<? if($groupBy==2) {?>	
<table border="0" width="100%" align="center" cellpadding="4"
		cellspacing="0">
<?=$single_table_content;?>
</table>
<?}?>

<? if($groupBy==2) {?>
<table border="0" width="100%" align="center" cellspacing="0"
		cellpadding="5">
<?=$bulk_table_content;?>
</table>
<?}?>

<?
if ($groupBy == 1)
    echo $location_table_content;
else 
    if ($groupBy == 3)
        echo $group_table_content;
?>
<? if(($groupBy != "") && ($step=="final")){ ?>
<table width="87%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td class="dotline"><img src="<?=$image_path?>/2007a/10x10.gif"
				width="10" height="5"></td>
		</tr>
	</table>
<? } ?>
<input type="hidden" name="flag" value="1"> <input type="hidden"
		name="step" value="">
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>