<?php
// Using:
// ############################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2015-11-02 Cameron
// Add filter by specified number of days for warranty expiry
//
// Date: 2012-03-07 YatWoon
// Fixed: Should be displayed all the expired items, not coming x days expired items
//
// ############################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();
$lexport = new libexporttext();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$curr_date = date("Y-m-d");
$DayPeriod = $linventory->getWarrantyExpiryWarningDayPeriod();
$date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $DayPeriod, date("Y")));

$file_content = "\"$i_InventorySystem_Item_Code\",";
$file_content .= "\"$i_InventorySystem_Report_ItemName\",";
$file_content .= "\"$i_InventorySystem_Group_Name\",";
$file_content .= "\"$i_InventorySystem_Item_Location\",";
$file_content .= "\"$i_InventorySystem_Report_WarrantyExpiryDate\"";
$file_content .= "\n";

$exportColumn = array(
    $i_InventorySystem_Item_Code,
    $i_InventorySystem_Report_ItemName,
    $i_InventorySystem_Group_Name,
    $i_InventorySystem_Item_Location,
    $i_InventorySystem_Report_WarrantyExpiryDate
);

if ($linventory->IS_ADMIN_USER($UserID))
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
else
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";

$tmp_arr_group = $linventory->returnVector($sql);
if (sizeof($tmp_arr_group) > 0) {
    $target_admin_group = implode(",", $tmp_arr_group);
}

$cond = '';
if ($target_admin_group != "") {
    $cond .= " AND c.AdminGroupID IN ($target_admin_group) ";
}

$warranty_expire = ($warranty_expire == '') ? 0 : $warranty_expire;
if ($warranty_expire == 0) {
    $cond .= " AND b.WarrantyExpiryDate <= '$curr_date' ";
} else {
    $cond .= " AND b.WarrantyExpiryDate <= DATE_ADD(CURDATE(),INTERVAL $warranty_expire DAY) ";
    $cond .= " AND b.WarrantyExpiryDate > '$curr_date' ";
}

$sql = "SELECT 
					a.ItemCode, 
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("d.") . "),
					b.WarrantyExpiryDate
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '0000-00-00') INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS floor ON (d.LocationLevelID = floor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
			WHERE 
					1
					$cond
			ORDER BY
					b.WarrantyExpiryDate";

$arr_expiry = $linventory->returnArray($sql, 4);

if (sizeof($arr_expiry) > 0) {
    for ($i = 0; $i < sizeof($arr_expiry); $i ++) {
        list ($item_code, $item_name, $item_group, $item_location, $item_warranty_expiry) = $arr_expiry[$i];
        
        $file_content .= "\"$item_code\",\"$item_name\",\"$item_group\",\"$item_location\",\"$item_warranty_expiry\"\n";
        
        $rows[] = array(
            $item_code,
            $item_name,
            $item_group,
            $item_location,
            $item_warranty_expiry
        );
    }
}
if (sizeof($arr_expiry) == 0) {
    $file_content .= "\"$i_no_record_exists_msg\"\n";
    
    $rows[] = array(
        $i_no_record_exists_msg
    );
}

$display = $file_content;

intranet_closedb();

$filename = "item_warranty_expiry_unicode.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>