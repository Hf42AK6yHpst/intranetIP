<?php
// using: 

// ############################
//
// Date: 2019-06-24 (Tommy)
// add funding source print out
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2013-02-19 YatWoon
// Fixed: Failed to display bulk item funding name [Case#2013-0131-1431-57073]
//
// Date: 2013-01-16 YatWoon
// Fixed: Display item's funding instead of write-off record's funding data [Case#:2013-0107-1503-41073]
//
// Date: 2012-07-10 YatWoon
// Enhanced: support 2 funding source for single item
//
// Date: 2012-05-29 [YatWoon]
// Improved: add funding column
//
// Date: 2011-11-30 [YatWoon]
// Improved: display deleted user name [Case#2011-1111-1515-17132]
//
// Date: 2011-11-07 YatWoon
// Improved: Add "Date Type" [Case#2011-1013-1705-11066]
//
// Date: 2011-06-15 YatWoon
// Improved: add sortby option [Case#2010-1214-1004-44096]
//
// Date: 2011-05-17 YatWoon
// add "Export Details"
//
// ############################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Report_WrittenOffItems";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();
$lexport = new libexporttext();

$start_date = str_replace("'", "", stripslashes($start_date));
$end_date = str_replace("'", "", stripslashes($end_date));
$targetLocation = str_replace("'", "", stripslashes($targetLocation));
$targetGroup = str_replace("'", "", stripslashes($targetGroup));
$targetFundSource = str_replace("'", "", stripslashes($targetFundSource));

if ($start_date == "") {
    $start_date = date("Y-m-d");
}
if ($end_date == "") {
    $end_date = date("Y-m-d");
}
if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND a.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND a.GroupInCharge = $targetGroup ";
}
if ($targetFundSource == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND a.GroupInCharge = $targetGroup ";
}

$exportColumn = array(
    $i_InventorySystem_Report_ItemWrittenOff,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);
$rows[] = array(
    $i_general_startdate,
    $start_date
);
$rows[] = array(
    $i_general_enddate,
    $end_date
);

if ($export_type == 2) {
    // ## Single ###
    $extra_fields1 = ", i.TagCode, b.NameChi, b.NameEng, b.DescriptionChi, b.DescriptionEng, ";
    $extra_fields1 .= "CONCAT(" . $linventory->getInventoryNameByLang("j.") . ",' > '," . $linventory->getInventoryNameByLang("k.") . ") as item_category";
    // $extra_fields1 .= ", ".$linventory->getInventoryNameByLang("l.") ." as funding ";
    $extra_fields1 .= ", case b.Ownership
						when 1 then '$i_InventorySystem_Ownership_School'
						when 2 then '$i_InventorySystem_Ownership_Government'
						when 3 then '$i_InventorySystem_Ownership_Donor'
						end as ownership
						";
    $extra_fields1 .= ", if(i.WarrantyExpiryDate ='0000-00-00', '', i.WarrantyExpiryDate)";
    $extra_fields1 .= ", i.SoftwareLicenseModel, i.SerialNumber, i.Brand,i.SupplierName, i.SupplierContact, i.SupplierDescription, i.QuotationNo, i.TenderNo, i.InvoiceNo, if(i.PurchaseDate ='0000-00-00', '', i.PurchaseDate)";
    $extra_fields1 .= ", i.UnitPrice, i.MaintainInfo, i.ItemRemark";
    
    $extra_from1 = "left join INVENTORY_ITEM_SINGLE_EXT as i on (i.ItemID=a.ItemID) ";
    $extra_from1 .= "left join INVENTORY_CATEGORY AS j ON (b.CategoryID = j.CategoryID) ";
    $extra_from1 .= "left join INVENTORY_CATEGORY_LEVEL2 AS k ON (b.Category2ID = k.Category2ID) ";
    // $extra_from1 .= "left join INVENTORY_FUNDING_SOURCE as l on (l.FundingSourceID = i.FundingSource) ";
    
    // ## Bulk ###
    $extra_fields2 = ", b.NameChi, b.NameEng, b.DescriptionChi, b.DescriptionEng, ";
    $extra_fields2 .= "CONCAT(" . $linventory->getInventoryNameByLang("j.") . ",' > '," . $linventory->getInventoryNameByLang("k.") . ") as item_category";
    // $extra_fields2 .= ", ".$linventory->getInventoryNameByLang("l.") ." as funding ";
    $extra_fields2 .= ", case b.Ownership
						when 1 then '$i_InventorySystem_Ownership_School'
						when 2 then '$i_InventorySystem_Ownership_Government'
						when 3 then '$i_InventorySystem_Ownership_Donor'
						end as ownership
						";
    $extra_fields2 .= ", " . $linventory->getInventoryNameByLang("o.") . " as var_group ";
    
    $extra_from2 .= "left join INVENTORY_CATEGORY AS j ON (b.CategoryID = j.CategoryID) ";
    $extra_from2 .= "left join INVENTORY_CATEGORY_LEVEL2 AS k ON (b.Category2ID = k.Category2ID) ";
    // $extra_from2 .= "left join INVENTORY_ITEM_BULK_LOCATION AS m ON (m.ItemID = a.ItemID and m.LocationID=c.LocationID) ";
    // $extra_from2 .= "left join INVENTORY_FUNDING_SOURCE as l on (l.FundingSourceID = m.FundingSourceID) ";
    $extra_from2 .= "left join INVENTORY_ITEM_BULK_EXT AS n ON (n.ItemID = a.ItemID) ";
    $extra_from2 .= "left join INVENTORY_ADMIN_GROUP AS o ON (o.AdminGroupID = n.BulkItemAdmin) ";
}

switch ($sortby) {
    case "ItemCode":
        $orderby = "b.ItemCode";
        break;
    case "ItemName":
        $orderby = "this_item_name";
        break;
    case "Location":
        $orderby = "building.DisplayOrder, floor.DisplayOrder, c.DisplayOrder";
        break;
    case "ResourceMgmtGroup":
        $orderby = "d.DisplayOrder";
        break;
    case "Qty":
        $orderby = "a.WriteOffQty";
        break;
    case "RequestDate":
        $orderby = "a.RequestDate";
        break;
    case "ApproveDate":
        $orderby = "a.ApproveDate";
        break;
}

if ($groupBy == 1) {
    $arr_tmp_location = explode(",", $selected_cb);
    
    if (sizeof($arr_tmp_location) > 0) {
        for ($a = 0; $a < sizeof($arr_tmp_location); $a ++) {
            $location_id = $arr_tmp_location[$a];
            
            $sql = "SELECT 
							CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("a.") . ")
					FROM
							INVENTORY_LOCATION AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID)
					WHERE
							a.LocationID = $location_id
					";
            $arr_tmp = $linventory->returnArray($sql, 1);
            
            if (sizeof($arr_tmp) > 0) {
                list ($location_name) = $arr_tmp[0];
            }
            
            // location
            $rows[] = array(
                ''
            );
            $rows[] = array(
                $location_name
            );
            
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }
            
            if ($target_admin_group != "") {
                $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
            }
            
            $RequestPerson = getNameFieldByLang2("f.");
            $ApprovePerson = getNameFieldByLang2("g.");
            $RequestArchivedPerson = getNameFieldByLang2("h2.");
            $ApproveArchivedPerson = getNameFieldByLang2("i2.");
            
            for ($k = 1; $k <= 2; $k ++) {
                /*
                 * $sql = "SELECT
                 * b.ItemCode,
                 * b.ItemType,
                 * ".$linventory->getInventoryItemNameByLang("b.")." as this_item_name,
                 * concat(".$linventory->getInventoryNameByLang("building.").",'>',".$linventory->getInventoryNameByLang("floor.").",'>',".$linventory->getInventoryNameByLang("c.")."),
                 * ".$linventory->getInventoryNameByLang("d.").",
                 * ".$linventory->getInventoryNameByLang("funding.").",
                 * a.WriteOffQty,
                 * a.RequestDate,
                 * IFNULL($RequestPerson,
                 * IF( $RequestArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('* ', $RequestArchivedPerson))
                 * ),
                 * a.ApproveDate,
                 * IFNULL($ApprovePerson,
                 * IF( $ApproveArchivedPerson is null, '".$Lang['eInventory']['FieldTitle']['DeletedStaff']."', concat('* ', $ApproveArchivedPerson))
                 * ),
                 *
                 * a.WriteOffReason
                 * ${'extra_fields'.$k},
                 * ".$linventory->getInventoryNameByLang("funding2.")."
                 * FROM
                 * INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
                 * INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
                 * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
                 * INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
                 * INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
                 * INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
                 * INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN
                 * INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
                 * left join INTRANET_ARCHIVE_USER as h2 on h2.UserID=a.RequestPerson
                 * left join INTRANET_ARCHIVE_USER as i2 on i2.UserID=a.ApprovePerson
                 * left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
                 * left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID or funding.FundingSourceID=s.FundingSource)
                 * left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
                 * ${'extra_from'.$k}
                 * WHERE
                 * a.RecordStatus = 1
                 * AND c.LocationID IN (".$location_id.")
                 * AND (a.". $DateType ." BETWEEN '$start_date' AND '$end_date')
                 * and b.ItemType=$k
                 * order by $orderby
                 * ";
                 */
                $sql = "SELECT 
								b.ItemCode, 
								b.ItemType, 
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $RequestArchivedPerson)) 
									),
								a.ApproveDate, 
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $ApproveArchivedPerson)) 
									),

								a.WriteOffReason
								${'extra_fields'.$k},
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
								INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
								INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
								INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
								INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
								left join INTRANET_ARCHIVE_USER as h2 on h2.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i2 on i2.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
								${'extra_from'.$k}
						WHERE
								a.RecordStatus = 1 
								AND c.LocationID IN (" . $location_id . ")
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
								and b.ItemType=$k
						order by $orderby
						";
                $arr_result = $linventory->returnArray($sql);
                
                $rows[] = array(
                    ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk)
                );
                
                // header
                if ($export_type == 1) // basic export
{
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Name,
                        $i_InventorySystem_Item_Location,
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_WriteOffQty,
                        $i_InventorySystem_WriteOffReason_Name,
                        $i_InventorySystem_RequestPerson,
                        $i_InventorySystem_Write_Off_RequestTime,
                        $i_InventorySystem_ApprovePerson,
                        $i_InventorySystem_ApproveDate
                    );
                } else // details export
{
                    if ($k == 1) // single
                        $rows[] = array(
                            $i_InventorySystem_Item_Code,
                            $i_InventorySystem_Category2_Barcode,
                            $i_InventorySystem_Item_ChineseName,
                            $i_InventorySystem_Item_EnglishName,
                            $i_InventorySystem_Item_ChineseDescription,
                            $i_InventorySystem_Item_EnglishDescription,
                            $i_InventorySystem['Category'],
                            $i_InventorySystem_Item_Location,
                            $i_InventorySystem['Caretaker'],
                            $i_InventorySystem['FundingSource'],
                            $i_InventorySystem_Item_Ownership,
                            $i_InventorySystem_Item_Warrany_Expiry,
                            $i_InventorySystem_Item_License_Type,
                            $i_InventorySystem_Item_Serial_Num,
                            $i_InventorySystem_Item_Brand_Name,
                            $i_InventorySystem_Item_Supplier_Name,
                            $i_InventorySystem_Item_Supplier_Contact,
                            $i_InventorySystem_Item_Supplier_Description,
                            $i_InventorySystem_Item_Quot_Num,
                            $i_InventorySystem_Item_Tender_Num,
                            $i_InventorySystem_Item_Invoice_Num,
                            $i_InventorySystem_Item_Purchase_Date,
                            $i_InventorySystem_Unit_Price,
                            $i_InventorySystemItemMaintainInfo,
                            $i_InventorySystem_Item_Remark,
                            $i_InventorySystem_WriteOffQty,
                            $i_InventorySystem_WriteOffReason_Name,
                            $i_InventorySystem_RequestPerson,
                            $i_InventorySystem_Write_Off_RequestTime,
                            $i_InventorySystem_ApprovePerson,
                            $i_InventorySystem_ApproveDate
                        );
                    else // bulk
                        $rows[] = array(
                            $i_InventorySystem_Item_Code,
                            $i_InventorySystem_Item_ChineseName,
                            $i_InventorySystem_Item_EnglishName,
                            $i_InventorySystem_Item_ChineseDescription,
                            $i_InventorySystem_Item_EnglishDescription,
                            $i_InventorySystem['Category'],
                            $i_InventorySystem_Item_Location,
                            $i_InventorySystem['Caretaker'],
                            $i_InventorySystem['FundingSource'],
                            $i_InventorySystem_Item_Ownership,
                            $i_InventorySystem_Item_BulkItemAdmin,
                            $i_InventorySystem_WriteOffQty,
                            $i_InventorySystem_WriteOffReason_Name,
                            $i_InventorySystem_RequestPerson,
                            $i_InventorySystem_Write_Off_RequestTime,
                            $i_InventorySystem_ApprovePerson,
                            $i_InventorySystem_ApproveDate
                        );
                }
                
                if (sizeof($arr_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_result); $i ++) {
                        if ($export_type == 1) // basic export
{
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                            
                            if ($k == 2) {
                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                            }
                            
                            $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                            $rows[] = array(
                                $item_code,
                                $item_name,
                                $location_name,
                                $group_name,
                                $funding_name_str,
                                $write_off_qty,
                                $write_off_reason,
                                $request_person,
                                $request_date,
                                $approve_person,
                                $approve_date
                            );
                        } else // details export
{
                            if ($k == 1) // single
{
                                list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_barcode, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $item_warranty_expiry, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_desc, $item_quo, $item_tender, $item_inv, $item_purchase_date, $this_uprice, $item_maintanence, $item_remarks, $funding_name2) = $arr_result[$i];
                                $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                $rows[] = array(
                                    $item_code,
                                    $item_barcode,
                                    $item_cname,
                                    $item_ename,
                                    $item_cdesc,
                                    $item_edesc,
                                    $item_category,
                                    $location_name,
                                    $group_name,
                                    $funding_name_str,
                                    $item_ownership,
                                    $item_warranty_expiry,
                                    $item_license,
                                    $item_serial,
                                    $item_brand,
                                    $item_supplier,
                                    $item_supplier_contact,
                                    $item_supplier_desc,
                                    $item_quo,
                                    $item_tender,
                                    $item_inv,
                                    $item_purchase_date,
                                    $this_uprice,
                                    $item_maintanence,
                                    $item_remarks,
                                    $write_off_qty,
                                    $write_off_reason,
                                    $request_person,
                                    $request_date,
                                    $approve_person,
                                    $approve_date
                                );
                            } else // bulk
{
                                list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $var_group, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                                
                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                                
                                $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                $rows[] = array(
                                    $item_code,
                                    $item_cname,
                                    $item_ename,
                                    $item_cdesc,
                                    $item_edesc,
                                    $item_category,
                                    $location_name,
                                    $group_name,
                                    $funding_name_str,
                                    $item_ownership,
                                    $var_group,
                                    $write_off_qty,
                                    $write_off_reason,
                                    $request_person,
                                    $request_date,
                                    $approve_person,
                                    $approve_date
                                );
                            }
                        }
                    }
                } else {
                    $rows[] = array(
                        $i_no_record_exists_msg
                    );
                }
            }
            
            $rows[] = array(
                ''
            );
            $rows[] = array(
                ''
            );
            $rows[] = array(
                $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'],
                "",
                "",
                $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate']
            );
        }
    }
} else 
    if ($groupBy == 2) {
        for ($k = 1; $k <= 2; $k ++) {
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }
            
            if ($target_admin_group != "") {
                $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
            }
            
            $RequestPerson = getNameFieldByLang2("f.");
            $ApprovePerson = getNameFieldByLang2("g.");
            $RequestArchivedPerson = getNameFieldByLang2("h2.");
            $ApproveArchivedPerson = getNameFieldByLang2("i2.");
            $sql = "SELECT 
							b.ItemCode, 
							b.ItemType, 
							" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
							concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
							" . $linventory->getInventoryNameByLang("d.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							a.WriteOffQty,
							a.RequestDate,
							IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $RequestArchivedPerson)) 
									),
							a.ApproveDate, 
							IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $ApproveArchivedPerson)) 
									),
							a.WriteOffReason
							${'extra_fields'.$k},
							" . $linventory->getInventoryNameByLang("funding2.") . ",
							a.ItemID, a.LocationID
					FROM
							INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
							INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
							INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
							INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
							left join INTRANET_ARCHIVE_USER as h2 on h2.UserID=a.RequestPerson
							left join INTRANET_ARCHIVE_USER as i2 on i2.UserID=a.ApprovePerson
							left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
							left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
							${'extra_from'.$k}
					WHERE
							a.RecordStatus = 1 AND
							b.ItemType = $k
							AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
					order by $orderby
					";
            // $arr_result = $linventory->returnArray($sql) or die(mysql_error());
            $arr_result = $linventory->returnArray($sql);
            
            $rows[] = array(
                ''
            );
            $rows[] = array(
                ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk)
            );
            
            // header
            if ($export_type == 1) // basic export
{
                $rows[] = array(
                    $i_InventorySystem_Item_Code,
                    $i_InventorySystem_Item_Name,
                    $i_InventorySystem_Item_Location,
                    $i_InventorySystem['Caretaker'],
                    $i_InventorySystem['FundingSource'],
                    $i_InventorySystem_WriteOffQty,
                    $i_InventorySystem_WriteOffReason_Name,
                    $i_InventorySystem_RequestPerson,
                    $i_InventorySystem_Write_Off_RequestTime,
                    $i_InventorySystem_ApprovePerson,
                    $i_InventorySystem_ApproveDate
                );
            } else // details export
{
                if ($k == 1) // single
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Category2_Barcode,
                        $i_InventorySystem_Item_ChineseName,
                        $i_InventorySystem_Item_EnglishName,
                        $i_InventorySystem_Item_ChineseDescription,
                        $i_InventorySystem_Item_EnglishDescription,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem_Item_Location,
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_Item_Ownership,
                        $i_InventorySystem_Item_Warrany_Expiry,
                        $i_InventorySystem_Item_License_Type,
                        $i_InventorySystem_Item_Serial_Num,
                        $i_InventorySystem_Item_Brand_Name,
                        $i_InventorySystem_Item_Supplier_Name,
                        $i_InventorySystem_Item_Supplier_Contact,
                        $i_InventorySystem_Item_Supplier_Description,
                        $i_InventorySystem_Item_Quot_Num,
                        $i_InventorySystem_Item_Tender_Num,
                        $i_InventorySystem_Item_Invoice_Num,
                        $i_InventorySystem_Item_Purchase_Date,
                        $i_InventorySystem_Unit_Price,
                        $i_InventorySystemItemMaintainInfo,
                        $i_InventorySystem_Item_Remark,
                        $i_InventorySystem_WriteOffQty,
                        $i_InventorySystem_WriteOffReason_Name,
                        $i_InventorySystem_RequestPerson,
                        $i_InventorySystem_Write_Off_RequestTime,
                        $i_InventorySystem_ApprovePerson,
                        $i_InventorySystem_ApproveDate
                    );
                else // bulk
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_ChineseName,
                        $i_InventorySystem_Item_EnglishName,
                        $i_InventorySystem_Item_ChineseDescription,
                        $i_InventorySystem_Item_EnglishDescription,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem_Item_Location,
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_Item_Ownership,
                        $i_InventorySystem_Item_BulkItemAdmin,
                        $i_InventorySystem_WriteOffQty,
                        $i_InventorySystem_WriteOffReason_Name,
                        $i_InventorySystem_RequestPerson,
                        $i_InventorySystem_Write_Off_RequestTime,
                        $i_InventorySystem_ApprovePerson,
                        $i_InventorySystem_ApproveDate
                    );
            }
            
            if (sizeof($arr_result) > 0) {
                for ($i = 0; $i < sizeof($arr_result); $i ++) {
                    if ($export_type == 1) // basic export
{
                        list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                        if ($k == 2) {
                            // retrieve bulk item latest funding source name
                            $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                            $result_bl = $linventory->returnVector($sql_bl);
                            $funding_name = $result_bl[0];
                        }
                        $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $location_name,
                            $group_name,
                            $funding_name_str,
                            $write_off_qty,
                            $write_off_reason,
                            $request_person,
                            $request_date,
                            $approve_person,
                            $approve_date
                        );
                    } else // details export
{
                        if ($k == 1) // single
{
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_barcode, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $item_warranty_expiry, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_desc, $item_quo, $item_tender, $item_inv, $item_purchase_date, $this_uprice, $item_maintanence, $item_remarks, $funding_name2) = $arr_result[$i];
                            $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                            $rows[] = array(
                                $item_code,
                                $item_barcode,
                                $item_cname,
                                $item_ename,
                                $item_cdesc,
                                $item_edesc,
                                $item_category,
                                $location_name,
                                $group_name,
                                $funding_name_str,
                                $item_ownership,
                                $item_warranty_expiry,
                                $item_license,
                                $item_serial,
                                $item_brand,
                                $item_supplier,
                                $item_supplier_contact,
                                $item_supplier_desc,
                                $item_quo,
                                $item_tender,
                                $item_inv,
                                $item_purchase_date,
                                $this_uprice,
                                $item_maintanence,
                                $item_remarks,
                                $write_off_qty,
                                $write_off_reason,
                                $request_person,
                                $request_date,
                                $approve_person,
                                $approve_date
                            );
                        } else // bulk
{
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $var_group, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                            
                            // retrieve bulk item latest funding source name
                            $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                            $result_bl = $linventory->returnVector($sql_bl);
                            $funding_name = $result_bl[0];
                            
                            $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                            $rows[] = array(
                                $item_code,
                                $item_cname,
                                $item_ename,
                                $item_cdesc,
                                $item_edesc,
                                $item_category,
                                $location_name,
                                $group_name,
                                $funding_name_str,
                                $item_ownership,
                                $var_group,
                                $write_off_qty,
                                $write_off_reason,
                                $request_person,
                                $request_date,
                                $approve_person,
                                $approve_date
                            );
                        }
                    }
                }
            } else {
                $rows[] = array(
                    $i_no_record_exists_msg
                );
            }
            
            $rows[] = array(
                ''
            );
            $rows[] = array(
                ''
            );
            $rows[] = array(
                $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'],
                "",
                "",
                $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate']
            );
        }
    } else 
        if ($groupBy == 3) {
            $arr_tmp_group = explode(",", $selected_cb);
            
            if (sizeof($arr_tmp_group) > 0) {
                for ($a = 0; $a < sizeof($arr_tmp_group); $a ++) {
                    $admin_group_id = $arr_tmp_group[$a];
                    
                    $sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID = '".$admin_group_id."'";
                    $arr_tmp_admin = $linventory->returnArray($sql, 1);
                    
                    if (sizeof($arr_tmp_admin) > 0) {
                        list ($admin_group_name) = $arr_tmp_admin[0];
                    }
                    
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                    }
                    
                    if ($target_admin_group != "") {
                        $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
                    }
                    
                    $RequestPerson = getNameFieldByLang2("f.");
                    $ApprovePerson = getNameFieldByLang2("g.");
                    $RequestArchivedPerson = getNameFieldByLang2("h2.");
                    $ApproveArchivedPerson = getNameFieldByLang2("i2.");
                    
                    $rows[] = array(
                        ''
                    );
                    $rows[] = array(
                        $admin_group_name
                    );
                    
                    for ($k = 1; $k <= 2; $k ++) {
                        $sql = "SELECT 
								b.ItemCode, 
								b.ItemType, 
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $RequestArchivedPerson)) 
									),
								a.ApproveDate, 
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $ApproveArchivedPerson)) 
									),
								a.WriteOffReason
								${'extra_fields'.$k},
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
								INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
								INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
								INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN 
								INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
								left join INTRANET_ARCHIVE_USER as h2 on h2.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i2 on i2.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
								${'extra_from'.$k}
						WHERE
								a.RecordStatus = 1
								AND d.AdminGroupID IN (" . $admin_group_id . ")
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
								and b.ItemType = $k
						order by $orderby
						";
                        $arr_result = $linventory->returnArray($sql);
                        
                        $rows[] = array(
                            ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk)
                        );
                        // $rows[] = array($i_InventorySystem_Item_Code,$i_InventorySystem_Item_Name,$i_InventorySystem_Item_Location,$i_InventorySystem['Caretaker'],$i_InventorySystem_WriteOffQty,$i_InventorySystem_WriteOffReason_Name,$i_InventorySystem_RequestPerson,$i_InventorySystem_Write_Off_RequestTime,$i_InventorySystem_ApprovePerson,$i_InventorySystem_ApproveDate);
                        // header
                        if ($export_type == 1) // basic export
{
                            $rows[] = array(
                                $i_InventorySystem_Item_Code,
                                $i_InventorySystem_Item_Name,
                                $i_InventorySystem_Item_Location,
                                $i_InventorySystem['Caretaker'],
                                $i_InventorySystem['FundingSource'],
                                $i_InventorySystem_WriteOffQty,
                                $i_InventorySystem_WriteOffReason_Name,
                                $i_InventorySystem_RequestPerson,
                                $i_InventorySystem_Write_Off_RequestTime,
                                $i_InventorySystem_ApprovePerson,
                                $i_InventorySystem_ApproveDate
                            );
                        } else // details export
{
                            if ($k == 1) // single
                                $rows[] = array(
                                    $i_InventorySystem_Item_Code,
                                    $i_InventorySystem_Category2_Barcode,
                                    $i_InventorySystem_Item_ChineseName,
                                    $i_InventorySystem_Item_EnglishName,
                                    $i_InventorySystem_Item_ChineseDescription,
                                    $i_InventorySystem_Item_EnglishDescription,
                                    $i_InventorySystem['Category'],
                                    $i_InventorySystem_Item_Location,
                                    $i_InventorySystem['Caretaker'],
                                    $i_InventorySystem['FundingSource'],
                                    $i_InventorySystem_Item_Ownership,
                                    $i_InventorySystem_Item_Warrany_Expiry,
                                    $i_InventorySystem_Item_License_Type,
                                    $i_InventorySystem_Item_Serial_Num,
                                    $i_InventorySystem_Item_Brand_Name,
                                    $i_InventorySystem_Item_Supplier_Name,
                                    $i_InventorySystem_Item_Supplier_Contact,
                                    $i_InventorySystem_Item_Supplier_Description,
                                    $i_InventorySystem_Item_Quot_Num,
                                    $i_InventorySystem_Item_Tender_Num,
                                    $i_InventorySystem_Item_Invoice_Num,
                                    $i_InventorySystem_Item_Purchase_Date,
                                    $i_InventorySystem_Unit_Price,
                                    $i_InventorySystemItemMaintainInfo,
                                    $i_InventorySystem_Item_Remark,
                                    $i_InventorySystem_WriteOffQty,
                                    $i_InventorySystem_WriteOffReason_Name,
                                    $i_InventorySystem_RequestPerson,
                                    $i_InventorySystem_Write_Off_RequestTime,
                                    $i_InventorySystem_ApprovePerson,
                                    $i_InventorySystem_ApproveDate
                                );
                            else // bulk
                                $rows[] = array(
                                    $i_InventorySystem_Item_Code,
                                    $i_InventorySystem_Item_ChineseName,
                                    $i_InventorySystem_Item_EnglishName,
                                    $i_InventorySystem_Item_ChineseDescription,
                                    $i_InventorySystem_Item_EnglishDescription,
                                    $i_InventorySystem['Category'],
                                    $i_InventorySystem_Item_Location,
                                    $i_InventorySystem['Caretaker'],
                                    $i_InventorySystem['FundingSource'],
                                    $i_InventorySystem_Item_Ownership,
                                    $i_InventorySystem_Item_BulkItemAdmin,
                                    $i_InventorySystem_WriteOffQty,
                                    $i_InventorySystem_WriteOffReason_Name,
                                    $i_InventorySystem_RequestPerson,
                                    $i_InventorySystem_Write_Off_RequestTime,
                                    $i_InventorySystem_ApprovePerson,
                                    $i_InventorySystem_ApproveDate
                                );
                        }
                        
                        if (sizeof($arr_result) > 0) {
                            for ($i = 0; $i < sizeof($arr_result); $i ++) {
                                if ($export_type == 1) // basic export
{
                                    list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                                    
                                    if ($item_type == 2) {
                                        // retrieve bulk item latest funding source name
                                        $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                        $result_bl = $linventory->returnVector($sql_bl);
                                        $funding_name = $result_bl[0];
                                    }
                                    $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                    $rows[] = array(
                                        $item_code,
                                        $item_name,
                                        $location_name,
                                        $group_name,
                                        $funding_name_str,
                                        $write_off_qty,
                                        $write_off_reason,
                                        $request_person,
                                        $request_date,
                                        $approve_person,
                                        $approve_date
                                    );
                                } else // details export
{
                                    if ($k == 1) // single
{
                                        list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_barcode, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $item_warranty_expiry, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_desc, $item_quo, $item_tender, $item_inv, $item_purchase_date, $this_uprice, $item_maintanence, $item_remarks, $funding_name2) = $arr_result[$i];
                                        $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                        $rows[] = array(
                                            $item_code,
                                            $item_barcode,
                                            $item_cname,
                                            $item_ename,
                                            $item_cdesc,
                                            $item_edesc,
                                            $item_category,
                                            $location_name,
                                            $group_name,
                                            $funding_name_str,
                                            $item_ownership,
                                            $item_warranty_expiry,
                                            $item_license,
                                            $item_serial,
                                            $item_brand,
                                            $item_supplier,
                                            $item_supplier_contact,
                                            $item_supplier_desc,
                                            $item_quo,
                                            $item_tender,
                                            $item_inv,
                                            $item_purchase_date,
                                            $this_uprice,
                                            $item_maintanence,
                                            $item_remarks,
                                            $write_off_qty,
                                            $write_off_reason,
                                            $request_person,
                                            $request_date,
                                            $approve_person,
                                            $approve_date
                                        );
                                    } else // bulk
{
                                        list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $var_group, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];
                                        
                                        // retrieve bulk item latest funding source name
                                        $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID=$this_item_id and a.FundingSource!='' and a.LocationID=$this_location_id order by a.RecordID desc limit 1";
                                        $result_bl = $linventory->returnVector($sql_bl);
                                        $funding_name = $result_bl[0];
                                        
                                        $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                        $rows[] = array(
                                            $item_code,
                                            $item_cname,
                                            $item_ename,
                                            $item_cdesc,
                                            $item_edesc,
                                            $item_category,
                                            $location_name,
                                            $group_name,
                                            $funding_name_str,
                                            $item_ownership,
                                            $var_group,
                                            $write_off_qty,
                                            $write_off_reason,
                                            $request_person,
                                            $request_date,
                                            $approve_person,
                                            $approve_date
                                        );
                                    }
                                }
                            }
                        } else {
                            $rows[] = array(
                                $i_no_record_exists_msg
                            );
                        }
                    }
                    
                    $rows[] = array(
                        ''
                    );
                    $rows[] = array(
                        ''
                    );
                    $rows[] = array(
                        $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'],
                        "",
                        "",
                        $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate']
                    );
                }
            }
        } 
    // funding source start
    else if ($groupBy == 4) {
    $arr_tmp_funding_source = explode(",", $selected_cb);

    if (sizeof($arr_tmp_funding_source) > 0) {
        for ($a = 0; $a < sizeof($arr_tmp_funding_source); $a ++) {
            $funding_source_id = $arr_tmp_funding_source[$a];

            $sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID = '" . $funding_source_id . "'";
            $arr_tmp_fs = $linventory->returnArray($sql, 1);

            if (sizeof($arr_tmp_fs) > 0) {
               list ($funding_source_name) = $arr_tmp_fs[0];
            }

            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '" . $UserID . "'";

            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }

            if ($target_admin_group != "") {
                $cond = " AND d.AdminGroupID IN ($target_admin_group) ";
            }

            $RequestPerson = getNameFieldByLang2("f.");
            $ApprovePerson = getNameFieldByLang2("g.");
            $RequestArchivedPerson = getNameFieldByLang2("h2.");
            $ApproveArchivedPerson = getNameFieldByLang2("i2.");

            $rows[] = array(
                ''
            );
            $rows[] = array(
                $funding_source_name
            );

            for ($k = 1; $k <= 2; $k ++) {
                $sql = "SELECT
								b.ItemCode,
								b.ItemType,
								" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
								concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
								" . $linventory->getInventoryNameByLang("d.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								a.WriteOffQty,
								a.RequestDate,
								IFNULL($RequestPerson,
									IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $RequestArchivedPerson))
									),
								a.ApproveDate,
								IFNULL($ApprovePerson,
									IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('* ', $ApproveArchivedPerson))
									),
								a.WriteOffReason
								${'extra_fields'.$k},
								" . $linventory->getInventoryNameByLang("funding2.") . ",
								a.ItemID, a.LocationID
						FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
								INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
								INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond) LEFT OUTER JOIN
								INTRANET_USER AS f ON (a.RequestPerson = f.UserID) LEFT OUTER JOIN
								INTRANET_USER AS g ON (a.ApprovePerson = g.UserID)
								left join INTRANET_ARCHIVE_USER as h2 on h2.UserID=a.RequestPerson
								left join INTRANET_ARCHIVE_USER as i2 on i2.UserID=a.ApprovePerson
								left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID OR funding.FundingSourceID=s.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
								${'extra_from'.$k}
						WHERE
								a.RecordStatus = 1
								AND funding.FundingSourceID IN (" . $funding_source_id . ")
								AND (a." . $DateType . " BETWEEN '$start_date' AND '$end_date')
								and b.ItemType = $k
						order by $orderby
						";
                $arr_result = $linventory->returnArray($sql);

                $rows[] = array(
                    ($k == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk)
                );
                // $rows[] = array($i_InventorySystem_Item_Code,$i_InventorySystem_Item_Name,$i_InventorySystem_Item_Location,$i_InventorySystem['Caretaker'],$i_InventorySystem_WriteOffQty,$i_InventorySystem_WriteOffReason_Name,$i_InventorySystem_RequestPerson,$i_InventorySystem_Write_Off_RequestTime,$i_InventorySystem_ApprovePerson,$i_InventorySystem_ApproveDate);
                // header
                if ($export_type == 1) // basic export
                {
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Name,
                        $i_InventorySystem_Item_Location,
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_WriteOffQty,
                        $i_InventorySystem_WriteOffReason_Name,
                        $i_InventorySystem_RequestPerson,
                        $i_InventorySystem_Write_Off_RequestTime,
                        $i_InventorySystem_ApprovePerson,
                        $i_InventorySystem_ApproveDate
                    );
                } else // details export
                {
                    if ($k == 1) // single
                        $rows[] = array(
                            $i_InventorySystem_Item_Code,
                            $i_InventorySystem_Category2_Barcode,
                            $i_InventorySystem_Item_ChineseName,
                            $i_InventorySystem_Item_EnglishName,
                            $i_InventorySystem_Item_ChineseDescription,
                            $i_InventorySystem_Item_EnglishDescription,
                            $i_InventorySystem['Category'],
                            $i_InventorySystem_Item_Location,
                            $i_InventorySystem['Caretaker'],
                            $i_InventorySystem['FundingSource'],
                            $i_InventorySystem_Item_Ownership,
                            $i_InventorySystem_Item_Warrany_Expiry,
                            $i_InventorySystem_Item_License_Type,
                            $i_InventorySystem_Item_Serial_Num,
                            $i_InventorySystem_Item_Brand_Name,
                            $i_InventorySystem_Item_Supplier_Name,
                            $i_InventorySystem_Item_Supplier_Contact,
                            $i_InventorySystem_Item_Supplier_Description,
                            $i_InventorySystem_Item_Quot_Num,
                            $i_InventorySystem_Item_Tender_Num,
                            $i_InventorySystem_Item_Invoice_Num,
                            $i_InventorySystem_Item_Purchase_Date,
                            $i_InventorySystem_Unit_Price,
                            $i_InventorySystemItemMaintainInfo,
                            $i_InventorySystem_Item_Remark,
                            $i_InventorySystem_WriteOffQty,
                            $i_InventorySystem_WriteOffReason_Name,
                            $i_InventorySystem_RequestPerson,
                            $i_InventorySystem_Write_Off_RequestTime,
                            $i_InventorySystem_ApprovePerson,
                            $i_InventorySystem_ApproveDate
                        );
                    else // bulk
                        $rows[] = array(
                            $i_InventorySystem_Item_Code,
                            $i_InventorySystem_Item_ChineseName,
                            $i_InventorySystem_Item_EnglishName,
                            $i_InventorySystem_Item_ChineseDescription,
                            $i_InventorySystem_Item_EnglishDescription,
                            $i_InventorySystem['Category'],
                            $i_InventorySystem_Item_Location,
                            $i_InventorySystem['Caretaker'],
                            $i_InventorySystem['FundingSource'],
                            $i_InventorySystem_Item_Ownership,
                            $i_InventorySystem_Item_BulkItemAdmin,
                            $i_InventorySystem_WriteOffQty,
                            $i_InventorySystem_WriteOffReason_Name,
                            $i_InventorySystem_RequestPerson,
                            $i_InventorySystem_Write_Off_RequestTime,
                            $i_InventorySystem_ApprovePerson,
                            $i_InventorySystem_ApproveDate
                        );
                }

                if (sizeof($arr_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_result); $i ++) {
                        if ($export_type == 1) // basic export
                        {
                            list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $funding_name2, $this_item_id, $this_location_id) = $arr_result[$i];

                            if ($item_type == 2) {
                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource 
                                           where a.ItemID=$this_item_id and a.FundingSource!='' and a.FundingSource != 0 and a.LocationID=$this_location_id 
                                           order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];
                            }
                            $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                            $rows[] = array(
                                $item_code,
                                $item_name,
                                $location_name,
                                $group_name,
                                $funding_name_str,
                                $write_off_qty,
                                $write_off_reason,
                                $request_person,
                                $request_date,
                                $approve_person,
                                $approve_date
                            );
                        } else // details export
                        {
                            if ($k == 1) // single
                            {
                                list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_barcode, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $item_warranty_expiry, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_desc, $item_quo, $item_tender, $item_inv, $item_purchase_date, $this_uprice, $item_maintanence, $item_remarks, $funding_name2) = $arr_result[$i];
                                $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                $rows[] = array(
                                    $item_code,
                                    $item_barcode,
                                    $item_cname,
                                    $item_ename,
                                    $item_cdesc,
                                    $item_edesc,
                                    $item_category,
                                    $location_name,
                                    $group_name,
                                    $funding_name_str,
                                    $item_ownership,
                                    $item_warranty_expiry,
                                    $item_license,
                                    $item_serial,
                                    $item_brand,
                                    $item_supplier,
                                    $item_supplier_contact,
                                    $item_supplier_desc,
                                    $item_quo,
                                    $item_tender,
                                    $item_inv,
                                    $item_purchase_date,
                                    $this_uprice,
                                    $item_maintanence,
                                    $item_remarks,
                                    $write_off_qty,
                                    $write_off_reason,
                                    $request_person,
                                    $request_date,
                                    $approve_person,
                                    $approve_date
                                );
                            } else // bulk
                            {
                                list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $item_cname, $item_ename, $item_cdesc, $item_edesc, $item_category, $item_ownership, $var_group, $funding_name2, $this_item_id, $this_funding_source_id) = $arr_result[$i];

                                // retrieve bulk item latest funding source name
                                $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource 
                                           where a.ItemID=$this_item_id and a.FundingSource!='' and a.FundingSource != 0 and a.FundingSource=$this_funding_source_id 
                                           order by a.RecordID desc limit 1";
                                $result_bl = $linventory->returnVector($sql_bl);
                                $funding_name = $result_bl[0];

                                $funding_name_str = $funding_name . ($funding_name2 ? ", " . $funding_name2 : "");
                                $rows[] = array(
                                    $item_code,
                                    $item_cname,
                                    $item_ename,
                                    $item_cdesc,
                                    $item_edesc,
                                    $item_category,
                                    $location_name,
                                    $group_name,
                                    $funding_name_str,
                                    $item_ownership,
                                    $var_group,
                                    $write_off_qty,
                                    $write_off_reason,
                                    $request_person,
                                    $request_date,
                                    $approve_person,
                                    $approve_date
                                );
                            }
                        }
                    }
                } else {
                    $rows[] = array(
                        $i_no_record_exists_msg
                    );
                }
            }
            $rows[] = array(
                ''
            );
            $rows[] = array(
                $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['PrincipalSignature'],
                "",
                "",
                $Lang['eInventory']['Report']['WrittenOffItems']['FieldTitle']['SignatureDate']
            );
            $rows[] = array(
                ''
            );
            $rows[] = array(
                ''
            );
        }
    }
}// funding source end

intranet_closedb();

$filename = "item_written_off_unicode.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>