<?php
//using: 

// #################################################
// Date: 2020-08-14 (Henry)
// display stocktake location in remarks field if not in correct location [Case#P186248]
//
// Date: 2019-07-15 (Henry)
// bug fix for wrong remark display after stocktake an item [Case#G189505]
//
// Date: 2019-09-02 (Tommy)
// add maximum width and height to school badge
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2017-08-16 Henry
// stocktake for bulk item with different location bug fix [Case#P121300]
//
// Date: 2016-02-02
// PHP 5.4 fix: change split to explode
//
// Date: 2012-08-24 YatWoon
// Missing to add stocktake mode checking
//
// Date: 2012-06-01 YatWoon
// display funding source for bulk item
// cater as multi "group" and "funding" for bulk item
//
// #################################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();

$school_data = explode("\n", get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root . "/file/schoolbadge.txt");
$school_data = explode("\n", get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root . "/file/schoolbadge.txt");
$badge_size = getimagesize($PATH_WRT_ROOT."/file/".$school_badge);
$badge_width = $badge_size[0];
$badge_height = $badge_siz[1];
$maxWidth = 120;
$maxHeight = 60;
if($badge_width > $maxWidth){
    $badge_width = $maxWidth;
}
if($badge_height > $maxHeight){
    $badge_height = $maxHeight;
}
if ($school_badge != "") {
    $badge_image = "<img src='/file/$school_badge' width='$badge_width'px height='$badge_height'px>";
}

$start_date = str_replace("'", "", stripslashes($start_date));
$end_date = str_replace("'", "", stripslashes($end_date));
$targetLocation = stripslashes($targetLocation);
$targetGroup = stripslashes($targetGroup);

if ($start_date == "") {
    $start_date = $linventory->getStocktakePeriodStart();
}
if ($end_date == "") {
    $end_date = $linventory->getStocktakePeriodEnd();
}

// get stock checked bulk item#
if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND c.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND c.GroupInCharge = $targetGroup ";
}
if (($targetProgress == "") || ($targetProgress == 1)) {
    $cond3 = " AND c.Action = " . ITEM_ACTION_STOCKCHECK;
}
if ($targetProgress == 2) {
    $cond3 = " AND c.Action != " . ITEM_ACTION_STOCKCHECK;
}

$sql = "SELECT
				DISTINCT b.RecordID
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN 
				INVENTORY_ITEM_BULK_LOG AS c ON (b.ItemID = c.ItemID)
		WHERE
				(c.RecordDate BETWEEN '$start_date' AND '$end_date')
				$cond1
				$cond2
				$cond3
		";

$arr_result = $linventory->returnArray($sql, 1);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($checked_bulk_item) = $arr_result[$i];
        $arr_checked_bulk_list[] = $checked_bulk_item;
    }
}
if (sizeof($arr_checked_bulk_list) > 0) {
    $checked_bulk_list = implode(",", $arr_checked_bulk_list);
} else {
    $checked_bulk_list = "";
}

// if($checked_bulk_list != "")
// {

// get stock checked single item #
if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND b.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND b.GroupInCharge = $targetGroup ";
}
if (($targetProgress == "") || ($targetProgress == 1)) {
    $cond3 = " AND a.Action = " . ITEM_ACTION_STOCKCHECK;
}
if ($targetProgress == 2) {
    $cond3 = " AND a.Action != " . ITEM_ACTION_STOCKCHECK;
}

$sql = "SELECT 
				DISTINCT a.ItemID 
		FROM 
				INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
		WHERE 
				(a.RecordDate BETWEEN '$start_date' AND '$end_date')
				$cond1
				$cond2
				$cond3
		";
// echo $sql;
$arr_result = $linventory->returnArray($sql, 1);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($checked_single_item) = $arr_result[$i];
        $arr_checked_single_list[$i] = $checked_single_item;
    }
}
if (sizeof($arr_checked_single_list) > 0) {
    $checked_single_list = implode(",", $arr_checked_single_list);
} else {
    $checked_single_list = "";
}

// ## Start to gen report ###
if ($groupBy == 1) {
    $cond = "WHERE LocationID in (" . $selected_cb . ")";
    
    $sql = "Select DISTINCT(CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",' > '," . $linventory->getInventoryItemNameByLang("b.") . ",' > '," . $linventory->getInventoryItemNameByLang("a.") . ")) as LocationName, LocationID 
				FROM INVENTORY_LOCATION AS a INNER JOIN 
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
				$cond 
				ORDER BY building.DisplayOrder, b.DisplayOrder, a.DisplayOrder";
    $arr_location = $linventory->returnArray($sql, 2);
    
    for ($a = 0; $a < sizeof($arr_location); $a ++) {
        list ($location_name, $locationID) = $arr_location[$a];
        
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
        }
        if ($target_admin_group != "") {
            $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
        }
        
        // ## Single Item ###
        $price_ceiling_cond = "";
        if ($sys_custom['eInventory_PriceCeiling']) {
            $price_ceiling_cond = "  AND (b.UnitPrice>=f.PriceCeiling) ";
        }
        
        $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						b.TagCode,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						b.UnitPrice,
						b.PurchasedPrice,
						a.StockTakeOption
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
				WHERE
						a.ItemType = 1 AND 
						d.LocationID = " . $locationID . " AND
						a.RecordStatus = 1
				ORDER BY
						a.ItemCode";
        $arr_single_result = $linventory->returnArray($sql);
        $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
        
        $location_table_content .= "<table border=\"0\" width=\"90%\" class=\"eSporttableborder\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
        $location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" colspan=\"9\">" . intranet_htmlspecialchars($location_name) . "</td></tr>";
        $location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" colspan=\"9\">$i_InventorySystem_ItemType_Single</td></tr>";
        
        $location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" width=\"2%\">&nbsp</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Barcode</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Category'] . "</td>";
        // $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystemItemStatus</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Last_Stock_Take_Time</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Remark</td>";
        $location_table_content .= "</tr>";
        if (sizeof($arr_single_result) > 0) {
            for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT 
								a.Action,
								a.DateInput,
								IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "'),
								IF(a.Remark != '', a.Remark, ' - ')
						FROM 
								INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
								INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
								a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
						ORDER BY
								a.DateInput DESC LIMIT 0,1
						";
                // echo $sql."<BR>";
                $arr_result = $linventory->returnArray($sql, 4);
                if (sizeof($arr_result) > 0) {
                    for ($j = 0; $j < sizeof($arr_result); $j ++) {
                        list ($item_action, $item_record_date, $user_name, $remark) = $arr_result[$j];
                        
                        $sql = "SELECT 
									CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
						 		FROM 
									INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
									INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
									INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
								WHERE 
									b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
								ORDER BY
									b.DateInput DESC LIMIT 0,1";
									
                        $location_result = $linventory->returnArray($sql, 1);
                            
                        if($location_result){
                            $location_found = $location_result[0][0];
                            
                            if(trim($remark) !='-'){
                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                            else{
                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                        }
                        
                        if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                            $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                        } else {
                            $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                        }
                        if ($user_name == "")
                            $user_name = " - ";
                        if ($date_input == "")
                            $date_input = " - ";
                        if ($item_remark == "")
                            $item_remark = " - ";
                        $location_table_content .= "<tr>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$action_img</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_code</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_name</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_barcode</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$user_name</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_record_date</td>";
                        $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$remark</td>";
                        $location_table_content .= "</tr>";
                        // $location_table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=\"10\"></td></tr>";
                    }
                } else {
                    $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                    $location_table_content .= "<tr>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$action_img</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_code</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_name</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_barcode</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                    
                    // if($user_name == "")
                    // $user_name = " - ";
                    // if($date_input == "")
                    // $date_input = " - ";
                    // if($item_remark == "")
                    // $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    $user_name = " - ";
                    $date_input = " - ";
                    $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$user_name</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$date_input</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_remark</td>";
                    $location_table_content .= "</tr>";
                }
            }
        }
        if (sizeof($arr_single_result) == 0) {
            $location_table_content .= "<tr><td colspan=\"11\" class=\"eSporttdborder eSportprinttext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
        
        // ## Bulk item ###
        if ($sys_custom['eInventory_PriceCeiling']) {
            $arr_targetSuccessBulkItem = array();
            $arr_targetFailBulkItem = array();
            $success_bulk_item_cond = "";
            $fail_bulk_item_cond = "";
            $sql = "SELECT 
							a.ItemID, a.CategoryID 
					FROM 
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 2 AND 
							d.LocationID = " . $locationID . "
					ORDER BY
							a.NameEng
					";
            $tmp_result = $linventory->returnArray($sql, 2);
            if (sizeof($tmp_result) > 0) {
                for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                    list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$tmp_item_id."' AND Action = " . ITEM_ACTION_PURCHASE . "";
                    $total_cost = $linventory->returnVector($sql);
                    
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$tmp_item_id."'";
                    $total_qty = $linventory->returnVector($sql);
                    
                    $unit_price = $total_cost[0] / $total_qty[0];
                    
                    $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = '".$tmp_cat_id."'";
                    $tmp_result2 = $linventory->returnArray($sql, 2);
                    if (sizeof($tmp_result2) > 0) {
                        for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                            list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                            if ($apply_to_bulk == 1) {
                                if ($unit_price >= $price_ceiling) {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                } else {
                                    $arr_targetFailBulkItem[] = $tmp_item_id;
                                }
                            } else {
                                $arr_targetSuccessBulkItem[] = $tmp_item_id;
                            }
                        }
                    }
                }
            }
            if (sizeof($arr_targetSuccessBulkItem) > 0) {
                $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
            }
            if (sizeof($arr_targetFailBulkItem) > 0) {
                $targetFailList = implode(",", $arr_targetFailBulkItem);
                $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
            }
        }
        $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.locationid,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						" . $linventory->getInventoryNameByLang("funding.") . ",
						b.GroupInCharge,
						b.FundingSourceID,
						b.Quantity,
						a.StockTakeOption
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
				WHERE
						a.ItemType = 2 AND 
						d.LocationID = " . $locationID . "
						$success_bulk_item_cond
						$fail_bulk_item_cond
				ORDER BY
						a.NameEng
						";
        $arr_bulk_result = $linventory->returnArray($sql, 7);
        $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
        
        $location_table_content .= "</table>";
        $location_table_content .= "<br>";
        $location_table_content .= "<table border=\"0\" width=\"90%\" class=\"eSporttableborder\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
        $location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" colspan=\"11\">$i_InventorySystem_ItemType_Bulk</td></tr>";
        $location_table_content .= "<tr>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Item_Code</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Item_Name</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >" . $i_InventorySystem['Category'] . "</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >" . $i_InventorySystem['FundingSource'] . "</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Group_Name</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Stocktake_ExpectedQty</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_TotalMissingQty</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_TotalSurplusQty</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Last_Stock_Take_Time</td>";
        $location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" >$i_InventorySystem_Item_Remark</td>";
        $location_table_content .= "</tr>";
        
        if (sizeof($arr_bulk_result) > 0) {
            for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                
                $bulk_lastmodified = "";
                $person_in_charge = "";
                $stocktake_remark = "";
                
                $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = '".$bulk_item_id."' AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                $MaxDateInput = $linventory->returnVector($sql);
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT
							IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
							IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
						FROM 
							INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
							a.ACTION = 2 AND 
							a.ItemID = '".$bulk_item_id."' AND
							a.LocationID = " . $bulk_item_locationid . " 
							and a.GroupInCharge=" . $bulk_group_id . " 
							and a.FundingSource =" . $bulk_funding_id . " and
							(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
							a.DateInput IN ('" . $MaxDateInput[0] . "')
						GROUP BY
							a.ItemID
						";
                $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                if (sizeof($arr_stock_take_time_person) > 0)
                    list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                    
                    /*
                 * $sql = "SELECT
                 * b.Quantity
                 * FROM
                 * INVENTORY_ITEM AS a INNER JOIN
                 * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                 * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                 * WHERE
                 * a.ItemID = ".$bulk_item_id." AND
                 * b.LocationID = ".$bulk_item_locationid;
                 *
                 * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                 * if(sizeof($arr_stock_take_quantity) > 0)
                 * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                 * if($bulk_item_quantity=="")
                 * $bulk_item_quantity=0;
                 */
                
                $sql = "SELECT 
								StockCheckQty, 
								IF(Remark='' OR Remark is null,'-',Remark),
								QtyChange
						FROM 
								INVENTORY_ITEM_BULK_LOG 
						WHERE 
								ItemID = '".$bulk_item_id."' AND 
								LocationID = $bulk_item_locationid AND
								GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
								Action IN (2,3,4) AND
								(RecordDate BETWEEN '$start_date' and '$end_date') AND
								DateInput IN ('" . $MaxDateInput[0] . "')
						ORDER BY
								DateInput DESC
						";
                $arr_stocktake_qty = $linventory->returnArray($sql);
                if (sizeof($arr_stocktake_qty) > 0) {
                    list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                } else {
                    $stocktake_remark = "";
                    $stocktake_qty_change = 0;
                    $stocktake_qty = 0;
                }
                
                // $item_different = $stocktake_qty - $bulk_item_quantity;
                $item_different = $stocktake_qty_change;
                // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                
                $location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$bulk_item_code</td>";
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$bulk_item_name</td>";
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_category) . "</td>";
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_group) . "</td>";
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$expect_qty</td>";
                if ($item_different > 0) {
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                }
                if ($item_different < 0) {
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                if ($item_different == 0) {
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                if ($person_in_charge == "")
                    $person_in_charge = " - ";
                if ($bulk_lastmodified == "")
                    $bulk_lastmodified = " - ";
                if ($stocktake_remark == "")
                    $stocktake_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$person_in_charge</td>";
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$bulk_lastmodified</td>";
                $location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$stocktake_remark</td></tr>";
                // $location_table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=9></td></tr>";
            }
        }
        if (sizeof($arr_bulk_result) == 0) {
            $location_table_content .= "<tr ><td colspan=\"11\" class=\"eSporttdborder eSportprinttext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
        $location_table_content .= "</table>";
        // $location_table_content .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
        // $location_table_content .= "<tr><td colspan=\"10\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
        // $location_table_content .="</table><br><br>";
    }
} else 
    if ($groupBy == 2) {
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
            $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
        }
        
        // ## single item ###
        $price_ceiling_cond = "";
        if ($sys_custom['eInventory_PriceCeiling']) {
            $price_ceiling_cond = "  AND (b.UnitPrice>=f.PriceCeiling) ";
        }
        $sql = "SELECT 
					a.ItemCode, 
					a.ItemID,
					b.TagCode,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					d.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
					b.UnitPrice,
					b.PurchasedPrice,
					a.StockTakeOption
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
					INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (e.BuildingID = building.BuildingID) INNER JOIN
					INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
			WHERE
					a.ItemType = 1 AND a.RecordStatus = 1
			ORDER BY
					a.ItemCode";
        $arr_single_result = $linventory->returnArray($sql);
        $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
        
        $single_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>";
        $single_table_content .= "<tr>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"2%\">&nbsp;</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Barcode</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Category'] . "</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Location</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Last_Stock_Take_Time</td>";
        $single_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Remark</td></tr>";
        
        if (sizeof($arr_single_result) > 0) {
            for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_item_locationid, $single_item_location, $single_category) = $arr_single_result[$i];
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT 
							a.Action,
							a.DateInput,
							IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "'),
							a.Remark
					FROM 
							INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
					WHERE 
							a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
					ORDER BY
							a.DateInput DESC LIMIT 0,1
					";
                $arr_result = $linventory->returnArray($sql, 4);
                
                if (sizeof($arr_result) > 0) {
                    for ($j = 0; $j < sizeof($arr_result); $j ++) {
                        list ($item_action, $date_input, $user_name, $item_remark) = $arr_result[$j];
                        
                        $sql = "SELECT 
									CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
						 		FROM 
									INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
									INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
									INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
									INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
								WHERE 
									b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
								ORDER BY
									b.DateInput DESC LIMIT 0,1";
									
                        $location_result = $linventory->returnArray($sql, 1);
                            
                        if($location_result){
                            $location_found = $location_result[0][0];
                            
                            if(trim($remark) !='-'){
                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                            else{
                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
                            }
                        }
                        
                        if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                            $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                        } else {
                            $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                        }
                        if ($user_name == "")
                            $user_name = " - ";
                        if ($date_input == "")
                            $date_input = " - ";
                        if ($item_remark == "")
                            $item_remark = " - ";
                        $single_table_content .= "<tr ><td class=\"eSporttdborder eSportprinttext\">$action_img</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_code</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_name</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_barcode</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_category) . "</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_location) . "</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$user_name</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$date_input</td>";
                        $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_remark</td></tr>";
                    }
                } else {
                    $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                    $single_table_content .= "<tr ><td class=\"eSporttdborder eSportprinttext\">$action_img</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_code</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_name</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_barcode</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_category) . "</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_location) . "</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                    // if($user_name == "")
                    // $user_name = " - ";
                    // if($date_input == "")
                    // $date_input = " - ";
                    // if($item_remark == "")
                    // $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    $user_name = " - ";
                    $date_input = " - ";
                    $item_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                    
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$user_name</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$date_input</td>";
                    $single_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_remark</td></tr>";
                }
            }
        }
        if (sizeof($arr_single_result) == 0) {
            $single_table_content .= "<tr ><td colspan=\"7\" class=\"class=\"eSporttdborder eSportprinttext\"\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
        
        // # bulk item ##
        if ($sys_custom['eInventory_PriceCeiling']) {
            $arr_targetSuccessBulkItem = array();
            $arr_targetFailBulkItem = array();
            $success_bulk_item_cond = "";
            $fail_bulk_item_cond = "";
            $sql = "SELECT 
						a.ItemID, a.CategoryID
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
				WHERE
						a.ItemType = 2
				ORDER BY
						a.ItemCode";
            $tmp_result = $linventory->returnArray($sql, 2);
            if (sizeof($tmp_result) > 0) {
                for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                    list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$tmp_item_id."' AND Action = " . ITEM_ACTION_PURCHASE . "";
                    $total_cost = $linventory->returnVector($sql);
                    
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$tmp_item_id."'";
                    $total_qty = $linventory->returnVector($sql);
                    
                    $unit_price = $total_cost[0] / $total_qty[0];
                    
                    $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = '".$tmp_cat_id."'";
                    $tmp_result2 = $linventory->returnArray($sql, 2);
                    if (sizeof($tmp_result2) > 0) {
                        for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                            list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                            if ($apply_to_bulk == 1) {
                                if ($unit_price >= $price_ceiling) {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                } else {
                                    $arr_targetFailBulkItem[] = $tmp_item_id;
                                }
                            } else {
                                $arr_targetSuccessBulkItem[] = $tmp_item_id;
                            }
                        }
                    }
                }
            }
            if (sizeof($arr_targetSuccessBulkItem) > 0) {
                $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
            }
            if (sizeof($arr_targetFailBulkItem) > 0) {
                $targetFailList = implode(",", $arr_targetFailBulkItem);
                $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
            }
        }
        $sql = "SELECT 
					a.ItemCode, 
					a.ItemID,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					d.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
					" . $linventory->getInventoryNameByLang("funding.") . ",
					b.GroupInCharge,
					b.FundingSourceID,
					b.Quantity,
					a.StockTakeOption
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (e.BuildingID = building.BuildingID) INNER JOIN
					INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
			WHERE
					a.ItemType = 2
					$success_bulk_item_cond
					$fail_bulk_item_cond
			ORDER BY
					a.ItemCode";
        $arr_bulk_result = $linventory->returnArray($sql, 4);
        $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
        
        $bulk_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"12\">$i_InventorySystem_ItemType_Bulk</td></tr>";
        $bulk_table_content .= "<tr ><td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Category'] . "</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Location</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['FundingSource'] . "</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_ExpectedQty</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_TotalMissingQty</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_TotalSurplusQty</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Last_Stock_Take_Time</td>";
        $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Remark</td></tr>";
        
        if (sizeof($arr_bulk_result) > 0) {
            for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_location, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                
                $bulk_lastmodified = "";
                $person_in_charge = "";
                $stocktake_remark = "";
                
                $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = '".$bulk_item_id."' AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id  AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                $MaxDateInput = $linventory->returnVector($sql);
                
                $namefield = getNameFieldByLang2("b.");
                $sql = "SELECT
						IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
						IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
					FROM 
						INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
						INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
					WHERE 
						a.ACTION = 2 AND 
						a.ItemID = '".$bulk_item_id."' AND
						a.LocationID = " . $bulk_item_locationid . " 
						and a.GroupInCharge=" . $bulk_group_id . " 
						and a.FundingSource =" . $bulk_funding_id . " and
						(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
						a.DateInput IN ('" . $MaxDateInput[0] . "')
					GROUP BY
						a.ItemID	
					";
                $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                if (sizeof($arr_stock_take_time_person) > 0)
                    list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                    
                    /*
                 * $sql = "SELECT
                 * b.Quantity
                 * FROM
                 * INVENTORY_ITEM AS a INNER JOIN
                 * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                 * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                 * WHERE
                 * a.ItemID = ".$bulk_item_id." AND
                 * b.LocationID = ".$bulk_item_locationid;
                 *
                 * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                 * if(sizeof($arr_stock_take_quantity) > 0)
                 * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                 * if($bulk_item_quantity=="")
                 * $bulk_item_quantity=0;
                 */
                
                $sql = "SELECT 
							StockCheckQty,
							IF(Remark='' OR Remark is null,'-',Remark),
							QtyChange
					FROM 
							INVENTORY_ITEM_BULK_LOG 
					WHERE 
							ItemID = '".$bulk_item_id."' AND 
							LocationID = $bulk_item_locationid AND
							GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
							Action IN (2,3,4) AND
							(RecordDate BETWEEN '$start_date' and '$end_date') AND
							DateInput IN ('" . $MaxDateInput[0] . "')
					ORDER BY
							DateInput DESC
					";
                $arr_stocktake_qty = $linventory->returnArray($sql);
                if (sizeof($arr_stocktake_qty) > 0) {
                    list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                } else {
                    $stocktake_remark = "";
                    $stocktake_qty_change = 0;
                    $stocktake_qty = 0;
                }
                
                // $item_different = $stocktake_qty - $bulk_item_quantity;
                $item_different = $stocktake_qty_change;
                // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                
                $table_row_css = "  ";
                $bulk_table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$bulk_item_code</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$bulk_item_name</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_category) . "</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_location) . "</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_group) . "</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$expect_qty</td>";
                if ($item_different > 0) {
                    $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                }
                if ($item_different < 0) {
                    $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                    $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                if ($item_different == 0) {
                    $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                    $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                }
                
                if ($person_in_charge == "")
                    $person_in_charge = " - ";
                if ($bulk_lastmodified == "")
                    $bulk_lastmodified = " - ";
                if ($stocktake_remark == "")
                    $stocktake_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$person_in_charge</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$bulk_lastmodified</td>";
                $bulk_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$stocktake_remark</td></tr>";
                // $location_table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=9></td></tr>";
            }
        }
        if (sizeof($arr_bulk_result) == 0) {
            $bulk_table_content .= "<tr><td colspan=\"10\" class=\"eSporttdborder eSportprinttext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
        }
    }     // $single_table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\"></td></tr>";
    else 
        if ($groupBy == 3) {
            
            $cond = "WHERE AdminGroupID in (" . $selected_cb . ")";
            
            if ($linventory->IS_ADMIN_USER($UserID)) {
                $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
            } else {
                $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
            }
            $arr_group = $linventory->returnArray($sql, 2);
            
            for ($a = 0; $a < sizeof($arr_group); $a ++) {
                list ($group_name, $admin_group_id) = $arr_group[$a];
                
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                    $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                }
                
                // ## Single Item ###
                $price_ceiling_cond = "";
                if ($sys_custom['eInventory_PriceCeiling']) {
                    $price_ceiling_cond = " AND (b.UnitPrice>=f.PriceCeiling) ";
                }
                $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						b.TagCode,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						b.UnitPrice,
						b.PurchasedPrice,
						a.StockTakeOption
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
				WHERE
						a.ItemType = 1 AND 
						c.AdminGroupID = " . $admin_group_id . " AND
						a.RecordStatus = 1
				ORDER BY
						a.ItemCode";
                $arr_single_result = $linventory->returnArray($sql);
                $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
                
                $group_table_content .= "<table border=\"0\" class=\"eSporttableborder\" align=\"center\" width=\"87%\" cellspacing=\"0\" cellpadding=\"5\">";
                $group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"10\">$group_name</td></tr>";
                $group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>";
                $group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" width=\"2%\">&nbsp;</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Barcode</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Category'] . "</td>";
                // $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystemItemStatus</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Last_Stock_Take_Time</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Remark</td>";
                $group_table_content .= "</tr>";
                if (sizeof($arr_single_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                        list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                        
                        $namefield = getNameFieldByLang2("b.");
                        $sql = "SELECT 
								a.Action,
								a.DateInput,
								IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "'),
								IF(a.Remark != '', a.Remark, ' - ')
						FROM 
								INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
								INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
								a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
						ORDER BY
								a.DateInput DESC LIMIT 0,1
						";
                        $arr_result = $linventory->returnArray($sql, 4);
                        if (sizeof($arr_result) > 0) {
                            for ($j = 0; $j < sizeof($arr_result); $j ++) {
                                list ($item_action, $item_record_date, $user_name, $remark) = $arr_result[$j];
                                
	                            $sql = "SELECT 
											CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
								 		FROM 
											INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
											INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
											INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
											INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
										WHERE 
											b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
										ORDER BY
											b.DateInput DESC LIMIT 0,1";
											
	                            $location_result = $linventory->returnArray($sql, 1);
	                            
	                            if($location_result){
		                            $location_found = $location_result[0][0];
		                            
		                            if(trim($remark) !='-'){
		                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
		                            }
		                            else{
		                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
		                            }
	                            }
                            
                                if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                                    $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                                } else {
                                    $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                                }
                                if ($user_name == "")
                                    $user_name = " - ";
                                if ($item_record_date == "")
                                    $item_record_date = " - ";
                                if ($remark == "")
                                    $remark = " - ";
                                $group_table_content .= "<tr>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$action_img</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_code</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_name</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_barcode</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$user_name</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_record_date</td>";
                                $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$remark</td>";
                                $group_table_content .= "</tr>";
                                // $group_table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=\"10\"></td></tr>";
                            }
                        } else {
                            $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                            
                            $group_table_content .= "<tr>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$action_img</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_code</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_name</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$single_item_barcode</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                            // if($user_name == "")
                            // $user_name = " - ";
                            // if($item_record_date == "")
                            // $item_record_date = " - ";
                            // if($remark == "")
                            // $remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                            
                            $user_name = " - ";
                            $item_record_date = " - ";
                            $remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                            
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$user_name</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_record_date</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$remark</td>";
                            $group_table_content .= "</tr>";
                        }
                    }
                }
                if (sizeof($arr_single_result) == 0) {
                    $group_table_content .= "<tr><td colspan=\"11\" class=\"eSporttdborder eSportprinttext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                }
                
                // ## Bulk item ###
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                    $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                }
                
                if ($sys_custom['eInventory_PriceCeiling']) {
                    $arr_targetSuccessBulkItem = array();
                    $arr_targetFailBulkItem = array();
                    $success_bulk_item_cond = "";
                    $fail_bulk_item_cond = "";
                    $sql = "SELECT 
							a.ItemID, a.CategoryID
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 2 AND 
							c.AdminGroupID = " . $admin_group_id . "
					ORDER BY
							a.NameEng
					";
                    $tmp_result = $linventory->returnArray($sql, 2);
                    if (sizeof($tmp_result) > 0) {
                        for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                            list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                            $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$tmp_item_id."' AND Action = " . ITEM_ACTION_PURCHASE . "";
                            $total_cost = $linventory->returnVector($sql);
                            
                            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$tmp_item_id."'";
                            $total_qty = $linventory->returnVector($sql);
                            
                            $unit_price = $total_cost[0] / $total_qty[0];
                            
                            $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = '".$tmp_cat_id."'";
                            $tmp_result2 = $linventory->returnArray($sql, 2);
                            if (sizeof($tmp_result2) > 0) {
                                for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                                    list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                                    if ($apply_to_bulk == 1) {
                                        if ($unit_price >= $price_ceiling) {
                                            $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                        } else {
                                            $arr_targetFailBulkItem[] = $tmp_item_id;
                                        }
                                    } else {
                                        $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                    }
                                }
                            }
                        }
                    }
                    if (sizeof($arr_targetSuccessBulkItem) > 0) {
                        $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                        $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
                    }
                    if (sizeof($arr_targetFailBulkItem) > 0) {
                        $targetFailList = implode(",", $arr_targetFailBulkItem);
                        $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
                    }
                }
                $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.locationid,
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						" . $linventory->getInventoryNameByLang("funding.") . ",
						b.GroupInCharge,
						b.FundingSourceID,
						b.Quantity,
						a.StockTakeOption
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
				WHERE
						a.ItemType = 2 AND 
						c.AdminGroupID = " . $admin_group_id . "
						$success_bulk_item_cond
						$fail_bulk_item_cond
				ORDER BY
						a.NameEng
						";
                $arr_bulk_result = $linventory->returnArray($sql);
                $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
                
                $group_table_content .= "</table>";
                $group_table_content .= "<br>";
                $group_table_content .= "<table border=\"0\" class=\"eSporttableborder\" width=\"87%\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
                $group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"11\">$i_InventorySystem_ItemType_Bulk</td></tr>";
                $group_table_content .= "<tr>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Code</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Name</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Category'] . "</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['Caretaker'] . "</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_InventorySystem['FundingSource'] . "</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_ExpectedQty</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_TotalMissingQty</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_TotalSurplusQty</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Last_Stock_Take_Time</td>";
                $group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Item_Remark</td>";
                $group_table_content .= "</tr>";
                
                if (sizeof($arr_bulk_result) > 0) {
                    for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                        list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                        
                        $bulk_lastmodified = "";
                        $person_in_charge = "";
                        $stocktake_remark = "";
                        
                        $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = '".$bulk_item_id."' AND LocationID = '".$bulk_item_locationid."' and GroupInCharge= '".$bulk_group_id."' and FundingSource = '".$bulk_funding_id."' AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                        $MaxDateInput = $linventory->returnVector($sql);
                        
                        $namefield = getNameFieldByLang2("b.");
                        $sql = "SELECT
							IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
							IFNULL($namefield,'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
						FROM 
							INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
							a.ACTION = 2 AND 
							a.ItemID = '".$bulk_item_id."' AND
							a.LocationID = " . $bulk_item_locationid . " 
							and a.GroupInCharge=" . $bulk_group_id . " 
							and a.FundingSource =" . $bulk_funding_id . " and
							(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
							a.DateInput IN ('" . $MaxDateInput[0] . "')
						GROUP BY
							a.ItemID
						";
                        $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                        if (sizeof($arr_stock_take_time_person) > 0)
                            list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                            
                            /*
                         * $sql = "SELECT
                         * b.Quantity
                         * FROM
                         * INVENTORY_ITEM AS a INNER JOIN
                         * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                         * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                         * WHERE
                         * a.ItemID = ".$bulk_item_id." AND
                         * b.LocationID = ".$bulk_item_locationid;
                         *
                         * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                         * if(sizeof($arr_stock_take_quantity) > 0)
                         * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                         * if($bulk_item_quantity=="")
                         * $bulk_item_quantity=0;
                         */
                        
                        $sql = "SELECT 
								StockCheckQty,
								IF(Remark='' OR Remark is null,'-',Remark),
								QtyChange
						FROM 
								INVENTORY_ITEM_BULK_LOG 
						WHERE 
								ItemID = '".$bulk_item_id."' AND 
								LocationID = $bulk_item_locationid AND
								GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
								Action IN (2,3,4) AND
								(RecordDate BETWEEN '$start_date' and '$end_date') AND
								DateInput IN ('" . $MaxDateInput[0] . "')
						ORDER BY
								DateInput DESC
						";
                        $arr_stocktake_qty = $linventory->returnArray($sql);
                        if (sizeof($arr_stocktake_qty) > 0) {
                            list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                        } else {
                            $stocktake_remark = "";
                            $stocktake_qty_change = 0;
                            $stocktake_qty = 0;
                        }
                        
                        // $item_different = $stocktake_qty - $bulk_item_quantity;
                        $item_different = $stocktake_qty_change;
                        // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                        
                        $group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$bulk_item_code</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$bulk_item_name</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_category) . "</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_item_group) . "</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$expect_qty</td>";
                        if ($item_different > 0) {
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                        }
                        if ($item_different < 0) {
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_different</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                        }
                        if ($item_different == 0) {
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                            $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">0</td>";
                        }
                        if ($person_in_charge == "")
                            $person_in_charge = " - ";
                        if ($bulk_lastmodified == "")
                            $bulk_lastmodified = " - ";
                        if ($stocktake_remark == "")
                            $stocktake_remark = "$i_InventorySystem_Report_Stocktake_NotDone";
                        
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$person_in_charge</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$bulk_lastmodified</td>";
                        $group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$stocktake_remark</td></tr>";
                    }
                }
                if (sizeof($arr_bulk_result) == 0) {
                    $group_table_content .= "<tr ><td colspan=\"9\" class=\"eSporttdborder eSportprinttext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
                }
                
                $group_table_content .= "</table>";
                $group_table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
                $group_table_content .= "<tr><td colspan=\"9\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
                $group_table_content .= "</table><br><br>";
            }
        }

include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");
?>

<br>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<br>
<table width="90%" align="center" border="0">
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$i_InventorySystem_Report_ItemFinishStockCheck?></font></td>
	</tr>
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$school_name?></font><?=$badge_image?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td class="eSportprinttext"><?echo $i_general_startdate.": ".$start_date."&nbsp;&nbsp;&nbsp;".$i_general_enddate.": ".$end_date;?></td>
	</tr>
</table>
<?if ($groupBy==2){?>
<table width="90%" border="0" cellspacing="0" cellpadding="5"
	align="center" class="eSporttableborder">
 <?=$single_table_content?>
</table>
<?}?>
<br>
<?if ($groupBy==2){?>
<table width="90%" border="0" cellspacing="0" cellpadding="5"
	align="center" class="eSporttableborder">
<?=$bulk_table_content;?>
</table>
<?}?>
<?


if ($groupBy == 1)
    echo $location_table_content;
if ($groupBy == 3)
    echo $group_table_content;
?>
<?

intranet_closedb();
?>