<?php

// using:

// #######################################################
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-07 Henry
// add access right checking [Case#E135442]
//
// Date: 2012-07-03 YatWoon
// Improved: display category with distinct from INVENTORY_ITEM
// #######################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Title ###
// $TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    0
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    1
);
// $TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/location.php", 0);
// $TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/group.php", 0);
// $TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/fundingsource.php", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// ## Get The Category ###
/*
 * $namefield = $linventory->getInventoryItemNameByLang();
 * $sql = "SELECT
 * CategoryID,
 * $namefield,
 * PhotoLink
 * FROM
 * INVENTORY_CATEGORY
 * ORDER BY
 * DisplayOrder ASC";
 */
$sql = "SELECT 
			distinct(a.CategoryID), 
			" . $linventory->getInventoryNameByLang("b.") . ", 
			b.PhotoLink
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY as b on a.CategoryID = b.CategoryID
		ORDER BY b.DisplayOrder";
$result = $linventory->returnArray($sql, 3);

if (sizeof($result) > 0) {
    for ($i = 0; $i < sizeof($result); $i ++) {
        list ($cat_id, $cat_name, $cat_photo_link) = $result[$i];
        
        if ($cat_photo_link != "") {
            $sql = "SELECT PhotoPath, PhotoName FROM INVENTORY_PHOTO_PART WHERE PartID = '".$cat_photo_link."'";
            $display_result = $linventory->returnArray($sql, 2);
            
            if (sizeof($display_result) > 0) {
                for ($j = 0; $j < sizeof($display_result); $j ++) {
                    list ($photo_path, $photo_name) = $display_result[$j];
                    $cat_photo = "<img src=\"$photo_path/$photo_name\" border=\"0\" width=\"100\" height=\"100\">";
                }
            }
        }
        
        $table_content .= ($i % 3 == 0) ? "<tr>\n" : "";
        /*
         * $table_content .= "<td width=\"10\">&nbsp</td>";
         * $table_content .= "<td width=\"33%\" align=\"center\" valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">";
         * $table_content .= "<table border=\"1\" width=\"100%\" bordercolordark=\"#1B91AF\" bordercolorlight=\"#FCF7E5\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\"><tr><td class=\"tablebluetop\">&nbsp;&nbsp;&nbsp;<a class=\"tabletoplink\" href=\"category_level2.php?category_id=$cat_id\">$cat_name</td></tr>";
         * $table_content .= "<tr><td align=\"center\"><a href=\"category_level2.php?category_id=$cat_id\">$cat_photo</td></tr></a></table>";
         * $table_content .= "</td>";
         * $table_content .= "<td width=\"10\">&nbsp</td>";
         */
        $table_content .= "<td><table border=\"0\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tr>
                                      <td><img src=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border1.gif\" width=\"12\" height=\"51\"></td>
                                      <td background=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border2.gif\" class=\"tabletopnolink\">$cat_name</td>
                                      <td><img src=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border3.gif\" width=\"17\" height=\"51\"></td>
                                    </tr>
                                    <tr>
                                      <td background=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border4.gif\">&nbsp;</td>
                                      <td align='center'><a href=\"category_level2.php?category_id=$cat_id\">$cat_photo</a></td>
                                      <td background=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border5.gif\">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td valign=\"top\" background=\"images\"><img src=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border6.gif\" alt=\"\" width=\"12\" height=\"13\"></td>
                                      <td background=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border7.gif\">&nbsp;</td>
                                      <td valign=\"top\"><img src=\"" . $image_path . "/" . $LAYOUT_SKIN . "/inventory/border8.gif\" alt=\"\" width=\"17\" height=\"13\"></td>
                                    </tr>
                                </table></td>";
        $table_content .= ($i % 3 == 2) ? "</tr>\n" : "";
    }
}

?>

<br>

<form name="form1" action="" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
		<tr>
			<td>
				<table width="90%" border="0" cellpadding="5" cellspacing="0"
					align="center">
	<?=$table_content?>
	</table>
			</td>
		</tr>
	</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>