<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
//$linterface->LAYOUT_START();

$sql = "INSERT INTO 
					INVENTORY_ITEM_BULK_LOCATION 
					(ItemID, GroupInCharge, LocationID, Quantity)
		VALUES
					($item_id, $target_admin_group, $target_location, $item_qty)";

$result = $linventory->db_db_query($sql);

if($result)
{
	header("location: category_show_items_location.php?type=2&item_id=$item_id&msg=1");
}
else
{
	header("location: category_show_items_location.php?type=2&item_id=$item_id&msg=4");
}

?>