<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory	= new libinventory();
$PAGE_NAVIGATION[] = array($button_new);

### Title ###
$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>
<form name="form1" action="" method="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>

<tr><td>

<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Type <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"><input name=cat_type type=radio value=1>Single <input name=cat_type type=radio value=2>Bulk</td></tr>

<tr><td class="dotline" colspan=2><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Item Name <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"><input name=item_name type=text size=80></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Description <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"><textarea name=item_description rows="2" cols="79"></textarea></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Category <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Location <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Group <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Price <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"><input name=item_price type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Supplier <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"><input name=itme_supplier type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Date Of Purchase</td>
<td class="tabletext" width="70%"><input name=item_DOP type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Funding</td>
<td class="tabletext" width="70%"></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Ownership</td>
<td class="tabletext" width="70%"><input name=item_owner type=radio value=1>School <input name=item_owner type=radio value=2>Government</td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Vocher No.</td>
<td class="tabletext" width="70%"><input name=item_vocher type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Quotation No.</td>
<td class="tabletext" width="70%"><input name=item_quotation type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Tender No.</td>
<td class="tabletext" width="70%"><input name=item_tender type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Photo</td>
<td class="tabletext" width="70%"></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Attachment</td>
<td class="tabletext" width="70%"></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">For eBooking</td>
<td class="tabletext" width="70%"><input name=item_booking type=checkbox value=(this.checked? 1 : 0)></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Remarks</td>
<td class="tabletext" width="70%"><textarea name=item_remark rows="2" cols="79"></textarea></td></tr>

<tr><td class="dotline" colspan=2><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Quantity <span class="tabletextrequire">*</span></td>
<td class="tabletext" width="70%"><input name=item_quantity type=text></td></tr>

<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">Expiry date of warranty</td>
<td class="tabletext" width="70%"><input name=item_expiry_date type=text></td></tr>

<tr><td colspan=2 align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
</td></tr>
</table>
</td></tr>
</table>
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>