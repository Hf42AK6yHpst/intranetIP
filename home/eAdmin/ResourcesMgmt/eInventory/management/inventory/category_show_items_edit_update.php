<?php
// editing by Henry
/**
 * ******************************* Modification Log *************************************
 * Date: 2019-06-05 (Henry)
 * bug fix for fail to display item photo when changing catagory 2 [Case#K162265]
 *
 * Date: 2019-04-30 (Henry)
 * security issue fix for SQL
 *
 * Date: 2018-02-07 (Henry)
 * add access right checking [Case#E135442]
 *
 * Date: 2018-02-06 (Henry)
 * resize photo to 1024 x 768 [Case#U131701]
 * 
 * Date: 2013-04-18 (YatWoon)
 * Improved: rename item photo name if item code with "/" symbol [Case#2013-0318-1019-37073]
 * 
 * Date: 2012-12-03 (YatWoon)
 * Improved: change the return link to "details page"
 * 
 * Date: 2012-07-10 (YatWoon)
 * Enhanced: support 2 funding source for single item
 * 
 * Date: 2012-05-15 YatWoon
 * Fixed: cater special character for quotation no, tender no and invoice no
 * 
 * Date: 2012-03-27 YatWoon
 * Fixed: cannot edit "Funding source" for bulk item in "item edit" page, moved to "location and resources mgmt group" for edit funding
 * 
 * Date: 2011-07-25 YatWoon
 * fixed: incorrect SQL query (update targetGroupInCharge which is empty data)
 *
 * Date: 2011-05-30 YatWoon
 * fixed attachment problem
 *
 * Date: 2011-04-27 YatWoon
 * add checking for serial number, license, warranty date
 *
 * 2011-03-31 (Carlos): Rename photo name as Item Code
 * **************************************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libimage.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$item_cat = IntegerSafe($item_cat);
$targetCat2 = IntegerSafe($targetCat2);
$targetVarianceManager = IntegerSafe($targetVarianceManager);

$CurrentPageArr['eInventory'] = 1;
$item_chi_name = intranet_htmlspecialchars($item_chi_name);
$item_eng_name = intranet_htmlspecialchars($item_eng_name);
$item_chi_discription = intranet_htmlspecialchars($item_chi_discription);
$item_eng_discription = intranet_htmlspecialchars($item_eng_discription);
// $item_code = intranet_htmlspecialchars(trim(addslashes($item_code)));
// $item_remark = intranet_htmlspecialchars(trim(addslashes($item_remark)));

$curr_date = date("Y-m-d");

// # Delete Current Item Photo ##
if ($delete_photo == 1) {
    $sql = "SELECT a.PhotoLink, b.PhotoPath, b.PhotoName FROM INVENTORY_ITEM AS a INNER JOIN INVENTORY_PHOTO_PART AS b ON a.ItemID = b.ItemID WHERE a.ItemID = '".$item_id."'";
    $arrResult = $linventory->returnArray($sql, 3);
    if (sizeof($arrResult) > 0) {
        list ($curr_photo_id, $curr_photo_path, $curr_photo_name) = $arrResult[0];
        
        $sql = "UPDATE INVENTORY_ITEM SET PhotoLink = '' WHERE ItemID = '".$item_id."'";
        $linventory->db_db_query($sql);
        
        $sql = "DELETE FROM INVENTORY_PHOTO_PART WHERE ItemID = '".$item_id."'";
        $linventory->db_db_query($sql);
        
        $lf = new libfilesystem();
        $full_path = "$intranet_root$curr_photo_path$curr_photo_name";
        $result = $lf->file_remove($full_path);
    }
}

// Upload Attachment #
$no_file = 5;

$re_path = "/file/inventory/attachment/$item_id/";
$path = "$intranet_root$re_path";

for ($i = 0; $i < $attachment_size; $i ++) {
    $file = stripslashes(${"hidden_item_attachment_" . $i});
    $re_file = stripslashes(${"item_attachment_" . $i});
    $target = "";
    
    if ($re_file == "none" || $re_file == "") {} else {
        $lf = new libfilesystem();
        if (! is_dir($path)) {
            $lf->folder_new($path);
        }
        
        $target = "$path/$file";
        $attachementname = "/$file";
        
        if ($lf->lfs_copy($re_file, $target) == 1) {
            $attachment[$i]['FileName'] = $attachementname;
            $attachment[$i]['Path'] = $re_path;
        }
    }
}

// Upload Photo #
$re_path = "/file/photo/inventory/";
$path = "$intranet_root$re_path";
$photo = stripslashes(${"hidden_item_photo"});
$target = "";

if ($item_photo == "none" || $item_photo == "") {} else {
    $sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE ItemID = '$item_id' ";
    $temp_item_code = $linventory->returnVector($sql);
    $item_code = $temp_item_code[0];
    
    $lf = new libfilesystem();
    if (! is_dir($path)) {
        $lf->folder_new($path);
    }
    
    $ext = $lf->file_ext($photo);
    $extUC = strtoupper($ext);
    if ($extUC == ".JPG" || $extUC == ".GIF" || $extUC == ".PNG") {
        // $target = "$path/$photo";
        // $filename .= "/$photo";
        
        $filename = $item_code . $ext;
        
        // check if item_code with "/" char which not support in filename
        if (strpos($item_code, "/") !== false) {
            $new_photo_code = str_replace("/", "_", $item_code);
            $filename = $new_photo_code . "_" . $item_id . $ext;
        } else {
            $filename = $item_code . $ext;
        }
        
        $target = "$path/" . $filename;
        
        $image_obj = new SimpleImage();
        $image_obj->load($item_photo);
        $image_obj->resizeToMax(1024, 768);
        $image_obj->save($target);
    }
}

$linventory->saveEntryTag($item_id, $Tags);

if ($targetItemType == 1) {
    if ($filename != "") {
        // check old photo exist or not #
        $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$item_cat."' AND Category2ID = '".$targetCat2."' AND ItemID = '".$item_id."'";
        
        $arr_PartID = $linventory->returnArray($sql, 1);
        
        if (sizeof($arr_PartID) > 0) {
            // old photo exist, update new photo #
            $sql = "UPDATE 
							INVENTORY_PHOTO_PART
					SET
							CategoryID = '".$item_cat."',
							Category2ID = '".$targetCat2."', 
							ItemID = '".$item_id."',
							PhotoPath = '$re_path', 
							PhotoName = '$filename', 
							DateModified = NOW()
					WHERE
							ItemID = $item_id
					";
            $linventory->db_db_query($sql);
        } else {
            // insert new photo #
            $sql = "INSERT INTO INVENTORY_PHOTO_PART
									(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
									('$item_cat', '$targetCat2', '$item_id', '$re_path', '$filename', NOW(), NOW())";
            // echo $sql."<BR>";
            $linventory->db_db_query($sql);
        }
    }
    
    if (sizeof($attachment) > 0) {
        for ($i = 0; $i < $attachment_size; $i ++) {
            $filename = $attachment[$i]['FileName'];
            $path = $attachment[$i]['Path'];
            
            if (($filename != "") && ($path != "")) {
                if (substr($filename, 0, 1) == "/") {
                    $filename = substr($filename, 1, strlen($filename) - 1);
                }
                $filename = addslashes($filename);
                
                $sql = "INSERT INTO 
								INVENTORY_ITEM_ATTACHMENT_PART (ItemID, AttachmentPath, Filename, DateInput, DateModified)
						VALUES
								('$item_id', '$path', '$filename', NOW(), NOW())";
                $linventory->db_db_query($sql);
            }
        }
    }
    
    $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$item_cat."' AND Category2ID = '".$targetCat2."' AND ItemID = '".$item_id."'";
    // echo $sql."<BR>";
    $arr_PartID = $linventory->returnArray($sql, 1);
    
    if (sizeof($arr_PartID) > 0) {
        list ($part_id) = $arr_PartID[0];
    } else {
        $part_id = "";
    }
    
    if($part_id){
    	$photoLink = "PhotoLink = '".$part_id."',";
    }
    
    $sql = "UPDATE 
					INVENTORY_ITEM 
			SET
					CategoryID = '$item_cat', 
					Category2ID = '$targetCat2', 
					NameChi = '$item_chi_name', 
					NameEng = '$item_eng_name',
					DescriptionChi = '$item_chi_discription',
					DescriptionEng = '$item_eng_discription',
					Ownership = '$item_ownership', 
					".$photoLink."
					StockTakeOption = '$StockTakeOption', 
					DateModified = NOW()
			WHERE
					ItemID = $item_id
			";
    // echo $sql."<BR>";
    $linventory->db_db_query($sql);
    
    $item_remark = intranet_htmlspecialchars($item_remark);
    $item_supplier = intranet_htmlspecialchars($item_supplier);
    $item_supplier_contact = intranet_htmlspecialchars($item_supplier_contact);
    $item_supplier_description = intranet_htmlspecialchars($item_supplier_description);
    $item_brand = intranet_htmlspecialchars($item_brand);
    $item_maintain_info = intranet_htmlspecialchars($item_maintain_info);
    
    $item_quotation = intranet_htmlspecialchars($item_quotation);
    $item_tender = intranet_htmlspecialchars($item_tender);
    $item_invoice = intranet_htmlspecialchars($item_invoice);
    
    // targetCat2
    $sql = "SELECT 
			HasSerialNumber,
			HasSoftwareLicenseModel, 
			HasWarrantyExpiryDate 
	FROM 
			INVENTORY_CATEGORY_LEVEL2 	
	WHERE 
			Category2ID = '".$targetCat2."'";
    $result = $linventory->returnArray($sql);
    
    if ($result[0][HasWarrantyExpiryDate] == 1)
        $extra .= "WarrantyExpiryDate = '$item_warranty_expiry_date',";
    if ($result[0][HasSoftwareLicenseModel] == 1)
        $extra .= "SoftwareLicenseModel = '$item_license',";
    if ($result[0][HasSerialNumber] == 1)
        $extra .= "SerialNumber = '$item_serial',";
        
        // re-calculate $item_unit_price
    $item_unit_price = $item_unit_price1;
    if ($targetFundingSource2)
        $item_unit_price += $item_unit_price2;
    else
        $item_unit_price2 = 0;
    
    $sql = "UPDATE 
					INVENTORY_ITEM_SINGLE_EXT
			SET
					PurchaseDate = '$item_purchase_date', 
					PurchasedPrice = '$item_purchase_price', 
					UnitPrice = '$item_unit_price',
					ItemRemark = '$item_remark',
					SupplierName = '$item_supplier', 
					SupplierContact = '$item_supplier_contact', 
					SupplierDescription = '$item_supplier_description', 
					MaintainInfo = '$item_maintain_info',
					InvoiceNo = '$item_invoice', 
					QuotationNo = '$item_quotation', 
					TenderNo = '$item_tender', 
					Brand = '$item_brand', 
					GroupInCharge = '$targetGroup', 
					LocationID = '$targetLocation', 
					$extra
					FundingSource = '$targetFundingSource',
					FundingSource2 = '$targetFundingSource2',
					UnitPrice1 = '$item_unit_price1',
					UnitPrice2 = '$item_unit_price2'
			WHERE
					ItemID = '".$item_id."'
			";
    
    $linventory->db_db_query($sql);
    
    $sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
			VALUES
					('$item_id', '$curr_date', " . ITEM_ACTION_EDIT_ITEM . ", $UserID, '', 0, 0, NOW(), NOW())
			";
    $linventory->db_db_query($sql);
}

if ($targetItemType == 2) {
    if ($filename != "") {
        // check old photo exist or not #
        $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$item_cat."' AND Category2ID = '".$targetCat2."' AND ItemID = '".$item_id."'";
        $arr_PartID = $linventory->returnArray($sql, 1);
        
        if (sizeof($arr_PartID) > 0) {
            // old photo exist, update new photo #
            $sql = "UPDATE 
							INVENTORY_PHOTO_PART
					SET
							CategoryID = '$item_cat',
							Category2ID = '$targetCat2', 
							ItemID = '$item_id',
							PhotoPath = '$re_path', 
							PhotoName = '$filename', 
							DateModified = NOW()
					WHERE
							ItemID = '$item_id'
					";
            $linventory->db_db_query($sql);
        } else {
            // insert new photo #
            $sql = "INSERT INTO INVENTORY_PHOTO_PART
									(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
									('$item_cat', '$targetCat2', '$item_id', '$re_path', '$filename', NOW(), NOW())";
            // echo $sql."<BR>";
            $linventory->db_db_query($sql);
        }
    }
    
    if (sizeof($attachment) > 0) {
        for ($i = 0; $i < sizeof($attachment); $i ++) {
            $filename = $attachment[$i]['FileName'];
            $path = $attachment[$i]['Path'];
            if (substr($filename, 0, 1) == "/") {
                $filename = substr($filename, 1, strlen($filename) - 1);
            }
            $sql = "INSERT INTO 
							INVENTORY_ITEM_ATTACHMENT_PART (ItemID, AttachmentPath, Filename, DateInput, DateModified)
					VALUES
							('$item_id', '$path', '$filename', NOW(), NOW())";
            // echo $sql;
            $linventory->db_db_query($sql);
        }
    }
    
    $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$item_cat."' AND Category2ID = '".$targetCat2."' AND ItemID = '".$item_id."'";
    // echo $sql."<BR>";
    $arr_PartID = $linventory->returnArray($sql, 1);
    
    if (sizeof($arr_PartID) > 0) {
        list ($part_id) = $arr_PartID[0];
    } else {
        $part_id = "";
    }
    
    if($part_id){
    	$photoLink = "PhotoLink = '".$part_id."',";
    }
    
    $sql = "UPDATE 
					INVENTORY_ITEM 
			SET
					CategoryID = '$item_cat', 
					Category2ID = '$targetCat2', 
					NameChi = '$item_chi_name', 
					NameEng = '$item_eng_name',
					DescriptionChi = '$item_chi_discription',
					DescriptionEng = '$item_eng_discription',
					Ownership = '$item_ownership', 
					".$photoLink."
					StockTakeOption = '$StockTakeOption', 
					DateModified = NOW()
			WHERE
					ItemID = '$item_id'
			";
    // echo $sql."<BR>";
    $linventory->db_db_query($sql) or die(mysql_error());
    
    $sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET BulkItemAdmin = '".$targetVarianceManager."' WHERE ItemID = '".$item_id."'";
    $linventory->db_db_query($sql);
    
    /*
     * //$sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION SET GroupInCharge = $targetGroupInCharge, FundingSourceID = $targetFundingSource WHERE ItemID = $item_id AND LocationID = $hiddenLocationID";
     * $sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION SET FundingSourceID = $targetFundingSource WHERE ItemID = $item_id AND LocationID = $hiddenLocationID";
     * $linventory->db_db_query($sql);
     */
    
    $sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
			VALUES
					('$item_id', '$curr_date', " . ITEM_ACTION_EDIT_ITEM . ", '$UserID', '', 0, 0, NOW(), NOW())
			";
    $linventory->db_db_query($sql) or die(mysql_error());
}

// header("location: items_full_list.php?msg=2");
header("location: category_show_items_detail.php?type=1&item_id=" . $item_id . "&xmsg=UpdateSuccess");
intranet_closedb();

?>