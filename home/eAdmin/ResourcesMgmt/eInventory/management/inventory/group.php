<?php
# using: 

#################################################
#	Date:	2016-02-04	Henry
#			php 5.4 issue move set cookies after includes file 
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_group_browsing_view_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_group_browsing_view_record_page_number", $pageNo, 0, "", "", 0);
	$ck_group_browsing_view_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_group_browsing_view_record_page_number!="")
{
	$pageNo = $ck_data_log_browsing_view_user_record_detail_page_number;
}

if ($ck_group_browsing_view_record_page_order!=$order && $order!="")
{
	setcookie("ck_group_browsing_view_record_page_order", $order, 0, "", "", 0);
	$ck_group_browsing_view_record_page_order = $order;
} else if (!isset($order) && $ck_group_browsing_view_record_page_order!="")
{
	$order = $ck_group_browsing_view_record_page_order;
}

if ($ck_group_browsing_view_record_page_field!=$field && $field!="")
{
	setcookie("ck_group_browsing_view_record_page_field", $field, 0, "", "", 0);
	$ck_group_browsing_view_record_page_field = $field;
} else if (!isset($field) && $ck_group_browsing_view_record_page_field!="")
{
	$field = $ck_group_browsing_view_record_page_field;
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

//$TAGS_OBJ[] = array($i_InventorySystem['Group'], "", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/items_full_list.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/category.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/location.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/group.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/fundingsource.php", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$sql = "SELECT AdminGroupID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_ADMIN_GROUP";
$arr_group = $linventory->returnArray($sql,2);

$opt_group = getSelectByArray($arr_group, " name=\"targetGroup\" onChange=\"this.form.submit();\" ", $targetGroup,1,0);

$table_content .= "<tr><td class=\"tabletext\" align=\"left\">".$i_InventorySystem['Caretaker'].":&nbsp;$opt_group</td></tr>";

/*
if($targetGroup == "")
{
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
	$arr_group_list = $linventory->returnVector($sql);
	$group_list = implode(",",$arr_group_list);
	$cond = " b.GroupInCharge IN ($group_list) ";
}
else
{
	$cond = " b.GroupInCharge IN ($targetGroup) ";
}

$sql1 = "SELECT 
				DISTINCT a.ItemID 
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
		WHERE
				$cond";

$arr_result1 = $linventory->returnVector($sql1);

$sql2 = "SELECT
				DISTINCT a.ItemID 
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID)
		WHERE
				$cond";

$arr_result2 = $linventory->returnVector($sql2);
$arr_result = array_merge($arr_result1, $arr_result2);
$result_list = implode(",",$arr_result);

$sql = "SELECT
				".$linventory->getInventoryItemNameByLang()."
		FROM
				INVENTORY_ITEM
		WHERE
				ItemID IN ($result_list)";
*/				
if($targetGroup == "")
{
	
	$sql = "SELECT
					CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">',".$linventory->getInventoryItemNameByLang("a.").",'</a>'),
					".$linventory->getInventoryNameByLang("d.").",
					".$linventory->getInventoryNameByLang("e.").",
					IF(a.ItemType = 2, c.Quantity, ' -- ')
			FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID) LEFT OUTER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID OR c.LocationID = d.LocationID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS e ON (b.GroupInCharge = e.AdminGroupID OR c.GroupInCharge = e.AdminGroupID)";
}
else
{
	$sql = "SELECT 
					CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">',".$linventory->getInventoryItemNameByLang("a.").",'</a>'),
					".$linventory->getInventoryNameByLang("d.").",
					".$linventory->getInventoryNameByLang("e.").",
					IF(a.ItemType = 2, c.Quantity, ' -- ')
			FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b On (b.GroupInCharge = $targetGroup AND a.ItemID = b.ItemID) LEFT OUTER JOIN 
					INVENTORY_ITEM_BULK_LOCATION AS c ON (c.GroupInCharge = $targetGroup AND a.ItemID = c.ItemID) LEFT OUTER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID OR c.LocationID = d.LocationID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS e ON (b.GroupInCharge = e.AdminGroupID OR c.GroupInCharge = e.AdminGroupID)
			WHERE
					( b.GroupInCharge = $targetGroup OR c.GroupInCharge = $targetGroup )";
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field=$field==""?1:$field;
/*
switch($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	case 4: $field = 4; break;
	default: $field = 1;
}
*/
if (!isset($order)) $order = 1;

				
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.NameEng","a.ItemID");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;
//echo $li->built_sql();

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class='tablebluetop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Category_Name)."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Location)."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Group_Name)."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Qty)."</td>\n";
//$li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";

?>

<br>

<form name="form1" action="" method="POST">

<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<?=$table_content?>
</table>
<br>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>
</table>

<?php echo $li->display("90%","blue"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>