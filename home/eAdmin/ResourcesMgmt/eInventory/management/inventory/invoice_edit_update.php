<?php
// using : Tommy

// ################################################
// Date: 2020-01-10 (Tommy)
// fix: change one item quantity change total location quantity of the item
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date : 2018-02-21 (Henry)
// add access right checking [Case#E135442]
// Date: 2016-04-29 (Henry)
// Modified logic to edit bulk quantity [Case#94913]
//
// Date: 2013-05-02 (YatWoon)
// add $special_feature['eInventory']['CanEditBulkQty'], allow user update bulk item's qty
//
// Date: 2013-03-12 (YatWoon)
// $sys_custom['CatholicEducation_eInventory'] >>> allow edit qty, purchase price and unit price
// add history log
//
// Date: 2012-12-03 (YatWoon)
// Add "Maintenance Details" and "Remarks" for edit
//
// Date: 2012-03-30 YatWoon
// - Allow to edit tender, supplier's contact, supplier description, quo# [Case#2012-0316-1635-31073]
//
// Date: 2011-11-17 YatWoon
// - Add a flag $sys_custom['eInventoryCanEditBulkInvoicePrice'] allow edit PurchasePrice and Unit Price [Case#2011-1108-1053-34073]
//
// ################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$log_data = array();

// retrieve original data
$sql_o = "select ItemID, InvoiceNo, PurchaseDate, QtyChange, PurchasedPrice, UnitPrice from INVENTORY_ITEM_BULK_LOG where RecordID='".$RecordID."'";
$reuslt_o = $linventory->returnArray($sql_o);

$InvoiceNo = intranet_htmlspecialchars($InvoiceNo);
$SupplierName = intranet_htmlspecialchars($SupplierName);
$item_quotation = intranet_htmlspecialchars($item_quotation);
$item_tender = intranet_htmlspecialchars($item_tender);
$item_supplier_contact = intranet_htmlspecialchars($item_supplier_contact);
$item_supplier_description = intranet_htmlspecialchars($item_supplier_description);
$item_maintain_info = intranet_htmlspecialchars($item_maintain_info);
$item_remark = intranet_htmlspecialchars($item_remark);

if ($sys_custom['eInventoryCanEditBulkInvoicePrice'] || $sys_custom['CatholicEducation_eInventory']) {
    $extra_updte = ", PurchasedPrice = '$PurchasePrice' ";
    $extra_updte .= ", UnitPrice = '$UnitPrice' ";
    
    if ($PurchasePrice != $reuslt_o[0]['PurchasedPrice']) {
        $log_data['PurchasedPriceFrom'] = $reuslt_o[0]['PurchasedPrice'];
        $log_data['PurchasedPriceTo'] = $PurchasePrice;
    }
    if ($UnitPrice != $reuslt_o[0]['UnitPrice']) {
        $log_data['UnitPriceFrom'] = $reuslt_o[0]['UnitPrice'];
        $log_data['UnitPriceTo'] = $UnitPrice;
    }
}
if ($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanEditBulkQty']) {
    $sql = "SELECT ItemID, GroupInCharge, LocationID, FundingSource FROM INVENTORY_ITEM_BULK_LOG WHERE RecordID >= $RecordID AND ItemID = '" . $reuslt_o[0]['ItemID'] . "' AND Action <> 11 ORDER BY RecordID";
    $result = $linventory->returnArray($sql);
    if (count($result) < 2) {
        $extra_updte .= ", QtyChange = '$QtyChange' ";
        // $extra_updte .= ", QtyNormal = QtyNormal + (".($QtyChange - $reuslt_o[0]['QtyChange']).") ";
        $extra_updte .= ", QtyNormal = '$QtyChange' ";
        
        if ($QtyChange != $reuslt_o[0]['QtyChange']) {
            $log_data['QtyChangeFrom'] = $reuslt_o[0]['QtyChange'];
            $log_data['QtyChangeTo'] = $QtyChange;
        }
        
        $sql = "SELECT SUM(QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 1 AND RecordStatus = 0 AND ItemID = ".$result[0]['ItemID']." AND LocationID = ".$result[0]['LocationID'];
        $locationQty = $linventory->returnVector($sql);

        // Quantity + (".($QtyChange - $reuslt_o[0]['QtyChange']).")
        $sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION Set Quantity = '$locationQty[0]' 
				WHERE ItemID = '" . $result[0]['ItemID'] . "' 
				AND GroupInCharge = '" . $result[0]['GroupInCharge'] . "' 
				AND LocationID = '" . $result[0]['LocationID'] . "' 
				AND FundingSourceID = '" . $result[0]['FundingSource'] . "'";
        $result = $linventory->db_db_query($sql);
        
        // $sql = "UPDATE INVENTORY_ITEM_BULK_EXT Set Quantity = '$QtyChange'
        // WHERE ItemID = '".$result[0]['ItemID']."'";

        $sql = "UPDATE INVENTORY_ITEM_BULK_EXT Set Quantity = Quantity + (" . ($QtyChange - $reuslt_o[0]['QtyChange']) . ") 
				WHERE ItemID = '" . $reuslt_o[0]['ItemID'] . "'";
        $result = $linventory->db_db_query($sql);
    }
}

$sql = "UPDATE 
			INVENTORY_ITEM_BULK_LOG 
		SET 
			InvoiceNo = '$InvoiceNo',
			PurchaseDate = '$PurchaseDate',
			SupplierName = '$SupplierName',
			QuotationNo = '$item_quotation',
			TenderNo = '$item_tender',
			SupplierContact = '$item_supplier_contact',
			SupplierDescription = '$item_supplier_description',
			Remark = '$item_remark',
			MaintainInfo = '$item_maintain_info' 
			$extra_updte
		WHERE
			RecordID = '".$RecordID."'";
$result = $linventory->db_db_query($sql);

// insert invoice history log
$log_data['LogID'] = $RecordID;
$log_data['ItemID'] = $reuslt_o[0]['ItemID'];
$log_data['Action'] = 1;

if ($InvoiceNo != $reuslt_o[0]['InvoiceNo']) {
    $log_data['InvoiceNoFrom'] = $reuslt_o[0]['InvoiceNo'];
    $log_data['InvoiceNoTo'] = $InvoiceNo;
}

if ($PurchaseDate != $reuslt_o[0]['PurchaseDate']) {
    $log_data['PurchaseDateFrom'] = $reuslt_o[0]['PurchaseDate'];
    $log_data['PurchaseDateTo'] = $PurchaseDate;
}
$linventory->insertBulkInvoiceHistoryLog($log_data);

if ($result != false) {
    header("location: category_show_items_detail.php?type=1&item_id=$ItemID&xmsg=UpdateSuccess");
} else {
    header("location: category_show_items_detail.php?type=1&item_id=$ItemID&xmsg=UpdateUnsuccess");
}

intranet_closedb();
?>