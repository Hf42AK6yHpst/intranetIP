<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

# Get the quantity not assigned yet #
$sql = "SELECT 
				a.Quantity - SUM(b.Quantity) AS ans
		FROM
				INVENTORY_ITEM_BULK_EXT AS a LEFT OUTER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID)
		WHERE
				a.ItemID = $item_id 
		GROUP BY
				b.ItemID";
	
$result = $linventory->returnArray($sql,1);

if(sizeof($result)>0)
{
	list($quantity_left) = $result[0];
	$table_content .= "<tr><td align=\"right\" class=\"tabletext\">Quantity Not Assign: $quantity_left</td></tr>";
}
	
# get the group selection #
$namefield = $linventory->getInventoryItemNameByLang();

$sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP";
$get_admin_result = $linventory->returnArray($sql,2);

$admin_group_selection = getSelectByArray($get_admin_result,"name=targetAdminGroup",$targetAdminGroup);

?>

<br>

<form name="form1" action="category_show_items_group_insert_update.php" method="post">

<table border="0">
<tr><td class="tabletext">Group</td><td><?=$admin_group_selection?></td></tr>
<tr><td class="tabletext">Quantity</td><td><input type="text" name="quantity_assigned"></td></tr>
</table>

<input type="hidden" name="item_id" value=<?=$item_id?>>

</form>

<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>