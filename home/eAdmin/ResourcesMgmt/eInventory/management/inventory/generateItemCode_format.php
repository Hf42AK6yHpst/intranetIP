<?
/*
 * using: 
 *
 * Log
 * Date: 2019-07-25 [Tommy] Add new settings for new setting item code combination
 * Date: 2019-05-13 [Henry] Security fix: SQL without quote
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");

intranet_auth();
intranet_opendb();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// #######################################################
// according to settings for item code combination
// For $sys_custom['eInventory_ItemCodeFormat_New'] only!
// #######################################################

// get category data
$sql = "SELECT CategoryID, Code FROM INVENTORY_CATEGORY";
$arr_result = $linventory->returnArray($sql, 2);
foreach ($arr_result as $k => $d)
    $CategoryCodeData[$d['CategoryID']] = $d['Code'];

// get category2 data
$sql = "SELECT Category2ID, Code FROM INVENTORY_CATEGORY_LEVEL2";
$arr_result = $linventory->returnArray($sql, 2);
foreach ($arr_result as $k => $d)
    $SubCategoryCodeData[$d['Category2ID']] = $d['Code'];

// get group code data
$sql = "SELECT AdminGroupID, Code FROM INVENTORY_ADMIN_GROUP";
$arr_result = $linventory->returnArray($sql, 2);
foreach ($arr_result as $k => $d)
    $GroupCodeData[$d['AdminGroupID']] = $d['Code'];

// get Funding code data
$sql = "SELECT FundingSourceID, Code FROM INVENTORY_FUNDING_SOURCE";
$arr_result = $linventory->returnArray($sql, 2);
foreach ($arr_result as $k => $d)
    $FundingCodeData[$d['FundingSourceID']] = $d['Code'];

// get Funding2 code data
$sql = "SELECT FundingSourceID, Code FROM INVENTORY_FUNDING_SOURCE";
$arr_result = $linventory->returnArray($sql, 2);
foreach ($arr_result as $k => $d)
    $Funding2CodeData[$d['FundingSourceID']] = $d['Code'];

// get Sub-Location code data
$sql = "SELECT LocationID, Code FROM INVENTORY_LOCATION";
$arr_result = $linventory->returnArray($sql, 2);
foreach ($arr_result as $k => $d)
    $SubLocationCodeData[$d['LocationID']] = $d['Code'];

$arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
$newItemCode = array();
if ($sys_custom['eInventory_ItemCodeFormat_New'] && $arr_itemCodeFormat) // according to settings for item code combination
{
    $itemCode_Prefix = "";
    for ($i = 0; $i < $item_total_qty; $i ++) {

        foreach ($arr_itemCodeFormat as $k => $d) {
            if ($d == 1)
                $itemCode_Prefix = substr($item_purchase_date, 0, 4) ;
            if ($d == 2)
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $GroupCodeData[${"targetGroup_" . $i}];
            if ($d == 3)
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $FundingCodeData[$item_funding];
            if ($d == 4)
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $SubLocationCodeData[${"targetLocation_" . $i}];
        }

        if ($sys_custom['eInventory']['ItemCodeNumberChecking'] == "ItemCodeCombination") // count the number according to the itemCode_Prefix
        {
            // search in INVENTORY_ITEM for max counting number
            $sql = "select max(ItemCode) from INVENTORY_ITEM where ItemCode like '" . $itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'] . "%'";
            $DB_ItemCode = $linventory->returnVector($sql);
            $maxNum = str_replace($itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'], "", $DB_ItemCode[0]);
        } else // default (count the number according to cateogry and sub-category)
        {
            // case 1: with delimiter
            if ($sys_custom['eInventory']['ItemCodeDelimiter']) {
                $sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE CategoryID = '" . $targetCategory . "' AND Category2ID = '" . $targetCategory2 . "' order by ItemID desc limit 1";
                $DB_ItemCode = $linventory->returnVector($sql);
                $last_pos = strrpos($DB_ItemCode[0], $sys_custom['eInventory']['ItemCodeDelimiter']);
                $maxNum = substr($DB_ItemCode[0], $last_pos + 1, strlen($DB_ItemCode[0]));
            } else // case 2: without delimiter
            {
                $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID = '" . $targetCategory . "' AND Category2ID = '" . $targetCategory2 . "'";
                $arr_result = $linventory->returnVector($sql);
                $maxNum = $arr_result[0];
            }
        }

        $cur_num = $maxNum + 1;

        // check with existing ItemCode in form
        if ($cur_num <= $existsCode[$itemCode_Prefix])
            $cur_num = $existsCode[$itemCode_Prefix] + 1;

        $existsCode[$itemCode_Prefix] = $cur_num;

        // add zero pad
        if ($sys_custom['eInventory']['ItemCodeNumberDigit'])
            $cur_num = str_pad($cur_num, $sys_custom['eInventory']['ItemCodeNumberDigit'], "0", STR_PAD_LEFT);

        $newItemCode[] = $itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'] . $cur_num;
    }
}

elseif ($sys_custom['eInventory_SysPpt'] && $arr_itemCodeFormat) { // according to settings for new setting item code combination
    $ItemCodeSeparator = $linventory->getItemCodeSeparator();
    $separator = "";
    if ($ItemCodeSeparator == 0) {
        $separator = '';
    } elseif ($ItemCodeSeparator == 1) {
        $separator = '_';
    } elseif ($ItemCodeSeparator == 2) {
        $separator = '/';
    } elseif ($ItemCodeSeparator == 3) {
        $separator = '-';
    }

    for ($i = 0; $i < $item_total_qty; $i ++) {
        $itemCode_Prefix = "";
        foreach ($arr_itemCodeFormat as $k => $d) {
            // debug_pr($arr_itemCodeFormat);
            if ($d == 'PurchaseYear'){
                if ($item_purchase_date != '')
                    $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . substr($item_purchase_date, 0, 4) . $separator;
            }
            if ($d == 'CategoryCode')
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $CategoryCodeData[$targetCategory] . $separator;
            if ($d == 'SubCategoryCode')
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $SubCategoryCodeData[$targetCategory2] . $separator;
            if ($d == 'ResourceManagementGroupCode')
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $GroupCodeData[${"targetGroup_" . $i}] . $separator;
            if ($d == 'FundingSource')
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $FundingCodeData[$item_funding] . $separator;
            if ($d == 'FundingSource2'){
                if ($item_funding2 != '')
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $Funding2CodeData[$item_funding2] . $separator;
            }
            if ($d == 'LocationCode')
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $SubLocationCodeData[${"targetLocation_" . $i}] . $separator;
        }

        if ($sys_custom['eInventory']['ItemCodeNumberChecking'] == "ItemCodeCombination") // count the number according to the itemCode_Prefix
        {
            // search in INVENTORY_ITEM for max counting number
            $sql = "select max(ItemCode) from INVENTORY_ITEM where ItemCode like '" . $itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'] . "%'";
            $DB_ItemCode = $linventory->returnVector($sql);
            $maxNum = str_replace($itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'], "", $DB_ItemCode[0]);
        } else // default (count the number according to cateogry and sub-category)
        {
            // case 1: with delimiter
            if ($sys_custom['eInventory']['ItemCodeDelimiter']) {
                $sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE CategoryID = '" . $targetCategory . "' AND Category2ID = '" . $targetCategory2 . "' order by ItemID desc limit 1";
                $DB_ItemCode = $linventory->returnVector($sql);
                $last_pos = strrpos($DB_ItemCode[0], $sys_custom['eInventory']['ItemCodeDelimiter']);
                $maxNum = substr($DB_ItemCode[0], $last_pos + 1, strlen($DB_ItemCode[0]));
            } else // case 2: without delimiter
            {
                $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID = '" . $targetCategory . "' AND Category2ID = '" . $targetCategory2 . "'";
                $arr_result = $linventory->returnVector($sql);
                $maxNum = $arr_result[0];
            }
        }

        $cur_num = $maxNum + 1;

        // check with existing ItemCode in form
        if ($cur_num <= $existsCode[$itemCode_Prefix])
            $cur_num = $existsCode[$itemCode_Prefix] + 1;

        // get item code number length setting in system properties
        $numOfDigit = $linventory->getItemCodeDigitLength();
        $numOfZero = "";
        $count = $numOfDigit - strlen((string) $cur_num);
        for ($i = 0; $i < $count; $i ++) {
            $numOfZero .= (string) 0;
        }

        $existsCode[$itemCode_Prefix] = $numOfZero . strlen((string) $cur_num);

        // add zero pad
        if ($sys_custom['eInventory']['ItemCodeNumberDigit'])
            $cur_num = str_pad($cur_num, $sys_custom['eInventory']['ItemCodeNumberDigit'], "0", STR_PAD_LEFT);

        $newItemCode[] = $itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'] . $numOfZero . $cur_num;
    }
}

intranet_closedb();

echo implode("::", $newItemCode);

?>