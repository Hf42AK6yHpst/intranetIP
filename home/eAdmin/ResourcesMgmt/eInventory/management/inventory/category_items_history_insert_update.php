<?php
// using:

// #############################################
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-07 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-01 Henry
// PHP 5.4 fix: change split to explode
// #############################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");

// ################################
// targetAction: #
// 1 - New Purchase #
// 2 - Write Off #
// 3 - Repairing #
// 4 - #
// ################################

if ($targetAction == 1) {
    for ($i = 0; $i < $total_supplier; $i ++) {
        $supplier_name = ${"supplier_name$i"};
        $supplier_contact = ${"supplier_contact$i"};
        $supplier_description = ${"supplier_description$i"};
        $invoice = ${"supplier_invoice$i"};
        $quotation = ${"supplier_quotation$i"};
        $tender = ${"supplier_tender$i"};
        
        $sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG (ItemID, RecordDate, Action, QtyChange, PurchaseDate, PurchasedPrice, 
						SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, PersonInCharge, 
						Remark, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($item_id, '$curr_date', $targetAction, $total_qty, '$purchase_date', $purchase_price, 
						'$supplier_name', '$supplier_contact', '$supplier_description', '$invoice', 
						'$quotation', '$tender', $UserID, '', '', '', NOW(), NOW())";
        
        // echo $sql."<BR><BR>";
        $result['BulkItemLog' . $i] = $linventory->db_db_query($sql);
    }
    
    $sql = "INSERT IGNORE INTO
					INVENTORY_ITEM_BULK_EXT (ItemID, Quantity)
			VALUES
					($item_id, $total_qty)";
    // echo $sql."<BR><BR>";
    $result['NewBulkItemExt'] = $linventory->db_db_query($sql);
    
    if ($linventory->db_affected_rows($sql) == 0) {
        $sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET Quantity = Quantity + $total_qty WHERE ItemID = $item_id";
        
        // echo $sql."<BR><BR>";
        $result['UpdateBulkItemExt'] = $linventory->db_db_query($sql);
    }
    
    $arr_targetGroup = explode(",", $group_list);
    
    if (sizeof($arr_targetGroup) > 0) {
        for ($i = 0; $i < sizeof($arr_targetGroup); $i ++) {
            list ($group_id) = $arr_targetGroup[$i];
            
            if (sizeof(${"location_$group_id"}) > 0) {
                for ($j = 0; $j < sizeof(${"location_$group_id"}); $j ++) {
                    list ($location_id) = ${"location_$group_id"}[$j];
                    $qty = ${"location_qty_" . $group_id . "_" . $location_id};
                    
                    $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $group_id AND LocationID = '".$location_id."'";
                    $arr_QtyExisted = $linventory->returnArray($sql, 1);
                    if (sizeof($arr_QtyExisted) > 0) {
                        $sql = "UPDATE 
										INVENTORY_ITEM_BULK_LOCATION 
								SET 
										Quantity = Quantity + $qty
								WHERE
										GroupInCharge = $group_id AND LocationID = '".$location_id."'";
                        
                        $result['UpdateBulkItemLocation'] = $linventory->db_db_query($sql);
                    } else {
                        $sql = "INSERT INTO 
										INVENTORY_ITEM_BULK_LOCATION (ItemID, GroupInCharge, LocationID, Quantity)
								VALUES
										($item_id, $group_id, $location_id, $qty)";
                        
                        // echo $sql;
                        $result['NewBulkItemLocation'] = $linventory->db_db_query($sql);
                    }
                }
            }
        }
    }
}

if ($targetAction == 2) {
    $arr_group = explode(",", $group_list);
    $arr_group = array_unique($arr_group);
    $arr_location = explode(",", $location_list);
    
    if ((sizeof($arr_group) > 0) && (sizeof($arr_location))) {
        for ($i = 0; $i < sizeof($arr_group); $i ++) {
            list ($group_id) = $arr_group[$i];
            for ($j = 0; $j < sizeof($arr_location); $j ++) {
                list ($location_id) = $arr_location[$j];
                $qty = ${"write_off_qty_" . $group_id . "_" . $location_id};
                
                if ($qty != "") {
                    $sql = "INSERT INTO 
									INVENTORY_ITEM_BULK_LOG (ItemID, RecordDate, Action, QtyChange, PurchasePrice, PurchaseDate, SupplierName, 
									SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, PersonInCharge, Remark, RecordType, 
									RecordStatus, DataInput, DateModified)
							VALUES
									($item_id, '$curr_date', $targetAction, $qty, '', '', '', '', '', '', '', '', $UserID, '', '', '', NOW(), NOW())";
                    // echo $sql."<BR>";
                    $result['NewBulkItemLog' . $i . $j] = $linventory->db_db_query($sql);
                    
                    $sql = "UPDATE
									INVENTORY_ITEM_BULK_EXT
							SET
									Quantity = Quantity - $qty
							WHERE
									ItemID = '".$item_id."'";
                    // echo $sql."<BR>";
                    $result['UpdateBulkItemExt' . $i . $j] = $linventory->db_db_query($sql);
                    
                    $sql = "UPDATE
									INVENTORY_ITEM_BULK_LOCATION
							SET
									Quantity = Quantity - $qty
							WHERE
									GroupInCharge = '".$group_id."' AND
									LocationID = '".$location_id."'";
                    // echo $sql."<BR>";
                    $result['UpdateBulkItemLocation' . $i . $j] = $linventory->db_db_query($sql);
                }
            }
        }
    }
    
    /*
     * if(sizeof($enableCheck)>0)
     * {
     * $arr_group = array_unique($enableCheck);
     * for($i=0; $i<sizeof($arr_group); $i++)
     * {
     * list($group_id) = $arr_group[$i];
     * for($j=0; $j<sizeof($arr_location); $j++)
     * {
     * list($location_id) = $arr_location[$j];
     * $qty = ${"write_off_qty_".$group_id."_".$location_id};
     *
     * if($qty != "")
     * {
     * $sql = "INSERT INTO
     * INVENTORY_ITEM_BULK_LOG (ItemID, RecordDate, Action, QtyChange, PurchasePrice, PurchaseDate, SupplierName,
     * SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, PersonInCharge, Remark, RecordType,
     * RecordStatus, DataInput, DateModified)
     * VALUES
     * ($item_id, '$curr_date', $targetAction, $qty, '', '', '', '', '', '', '', '', $UserID, '', '', '', NOW(), NOW())";
     * echo $sql."<BR>";
     * //$linventory->db_db_query($sql);
     *
     * $sql = "UPDATE
     * INVENTORY_ITEM_BULK_EXT
     * SET
     * Quantity = Quantity - $qty
     * WHERE
     * ItemID = $item_id";
     * echo $sql."<BR>";
     * //$linventory->db_db_query($sql);
     *
     * $sql = "UPDATE
     * INVENTORY_ITEM_BULK_LOCATION
     * SET
     * Quantity = Quantity - $qty
     * WHERE
     * GroupInCharge = $group_id AND
     * LocationID = $location_id";
     * echo $sql."<BR>";
     * //$linventory->db_db_query($sql);
     * }
     * }
     * }
     * }
     */
}

if ($targetAction == 3) {
    $arr_location = explode(",", $location_list);
    $arr_group = explode(",", $group_list);
    $arr_group = array_unique($arr_group);
    
    if ((sizeof($arr_group) > 0) && (sizeof($arr_location) > 0)) {
        for ($i = 0; $i < sizeof($arr_group); $i ++) {
            list ($group_id) = $arr_group[$i];
            for ($j = 0; $j < sizeof($arr_location); $j ++) {
                list ($location_id) = $arr_location[$j];
                $qty = ${"write_off_qty_" . $group_id . "_" . $location_id};
                if ($qty != "") {
                    $sql = "INSERT INTO 
									INVENTORY_ITEM_BULK_LOG (ItemID, RecordDate, Action, QtyChange, PurchasePrice, PurchaseDate, SupplierName, 
									SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, PersonInCharge, Remark, RecordType, 
									RecordStatus, DataInput, DateModified)
							VALUES
									($item_id, '$curr_date', $targetAction, $qty, '', '', '', '', '', '', '', '', $UserID, '', '', '', NOW(), NOW())";
                    // echo $sql."<BR>";
                    $result['NewBulkItemLog' . $i . $j] = $linventory->db_db_query($sql);
                    
                    $sql = "UPDATE
									INVENTORY_ITEM_BULK_EXT
							SET
									Quantity = Quantity - $qty
							WHERE
									ItemID = '".$item_id."'";
                    // echo $sql."<BR>";
                    $result['UpdateBulkItemExt' . $i . $j] = $linventory->db_db_query($sql);
                    
                    $sql = "UPDATE
									INVENTORY_ITEM_BULK_LOCATION
							SET
									Quantity = Quantity - $qty
							WHERE
									GroupInCharge = '".$group_id."' AND
									LocationID = '".$location_id."'";
                    // echo $sql."<BR>";
                    $result['UpdateBulkItemLocation' . $i . $j] = $linventory->db_db_query($sql);
                }
            }
        }
    }
    
    /*
     * if(sizeof($enableCheck)>0)
     * {
     * $arr_group = array_unique($enableCheck);
     * for($i=0; $i<sizeof($arr_group); $i++)
     * {
     * list($group_id) = $arr_group[$i];
     * for($j=0; $j<sizeof($arr_location); $j++)
     * {
     * list($location_id) = $arr_location[$j];
     * $qty = ${"write_off_qty_".$group_id."_".$location_id};
     *
     * if($qty != "")
     * {
     * $sql = "INSERT INTO
     * INVENTORY_ITEM_BULK_LOG (ItemID, RecordDate, Action, QtyChange, PurchasePrice, PurchaseDate, SupplierName,
     * SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, PersonInCharge, Remark, RecordType,
     * RecordStatus, DataInput, DateModified)
     * VALUES
     * ($item_id, '$curr_date', $targetAction, $qty, '', '', '', '', '', '', '', '', $UserID, '', '', '', NOW(), NOW())";
     * echo $sql."<BR>";
     * //$linventory->db_db_query($sql);
     *
     * $sql = "UPDATE
     * INVENTORY_ITEM_BULK_EXT
     * SET
     * Quantity = Quantity - $qty
     * WHERE
     * ItemID = $item_id";
     * echo $sql."<BR>";
     * //$linventory->db_db_query($sql);
     *
     * $sql = "UPDATE
     * INVENTORY_ITEM_BULK_LOCATION
     * SET
     * Quantity = Quantity - $qty
     * WHERE
     * GroupInCharge = $group_id AND
     * LocationID = $location_id";
     * echo $sql."<BR>";
     * //$linventory->db_db_query($sql);
     * }
     * }
     * }
     * }
     */
}

if ($targetAction == 4) {
    $arr_location = explode(",", $location_list);
    $arr_group = explode(",", $group_list);
    $arr_group = array_unique($arr_group);
    
    if ((sizeof($arr_group) > 0) && (sizeof($arr_location) > 0)) {
        for ($i = 0; $i < sizeof($arr_group); $i ++) {
            list ($group_id) = $arr_group[$i];
            for ($j = 0; $j < sizeof($arr_location); $j ++) {
                list ($location_id) = $arr_location[$i];
                $qty = ${"write_off_qty_" . $group_id . "_" . $location_id};
                if ($qty != "") {
                    $sql = "INSERT INTO 
									INVENTORY_ITEM_BULK_LOG (ItemID, RecordDate, Action, QtyChange, PurchasePrice, PurchaseDate, SupplierName, 
									SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, PersonInCharge, Remark, RecordType, 
									RecordStatus, DataInput, DateModified)
							VALUES
									($item_id, '$curr_date', $targetAction, $qty, '', '', '', '', '', '', '', '', $UserID, '', '', '', NOW(), NOW())";
                    // echo $sql."<BR>";
                    $result['NewBulkItemLog' . $i . $j] = $linventory->db_db_query($sql);
                    
                    $sql = "UPDATE
									INVENTORY_ITEM_BULK_EXT
							SET
									Quantity = Quantity - $qty
							WHERE
									ItemID = '".$item_id."'";
                    // echo $sql."<BR>";
                    $result['UpdateBulkItemExt' . $i . $j] = $linventory->db_db_query($sql);
                    
                    $sql = "UPDATE
									INVENTORY_ITEM_BULK_LOCATION
							SET
									Quantity = Quantity - $qty
							WHERE
									GroupInCharge = '".$group_id."' AND
									LocationID = '".$location_id."'";
                    // echo $sql."<BR>";
                    $result['UpdateBulkItemLocation' . $i . $j] = $linventory->db_db_query($sql);
                }
            }
        }
    }
}

if (! in_array(false, $result)) {
    header("location: category_show_items_history.php?type=5&item_id=$item_id&msg=1");
} else {
    header("location: category_show_items_history.php?type=5&item_id=$item_id&msg=12");
}
intranet_closedb();
?>