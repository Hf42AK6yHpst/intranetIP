<?php
/*
 * Log
 *
 * Date: 2019-05-13 (Henry) Security fix: SQL without quote
 * Date: 2018-02-21 [Henry] add access right checking [Case#E135442]
 * Date: 2015-04-16 [Cameron] use double quote for string "$image_path/$LAYOUT_SKIN"
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// Generate Page Tag #
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_UpdateStatus,
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_status_edit.php?item_id=$item_id",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_UpdateLocation,
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_location_edit.php?item_id=$item_id",
    0
);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_RequestWriteOff,
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_request_write_off.php?item_id=$item_id",
    0
);
// End #

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Generate Page Info #
$sql = "SELECT " . $linventory->getInventoryItemNameByLang() . " FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$arr_result = $linventory->returnVector($sql);

$temp[] = array(
    $i_InventorySystem_Item_Update_Status,
    ""
);
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";
// $infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($arr_result[0])."</td></tr>";
// End #

// echo $item_id."<BR>";
// echo $item_group_id."<BR>";
// echo $item_location_id."<BR>";

// echo $bulk_item_qty_found."<BR>";
// echo $bulk_item_qty_write_off."<BR>";

$sql = "SELECT " . $linventory->getInventoryItemNameByLang() . " FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$arr_result = $linventory->returnVector($sql);

$bulk_item_name = $arr_result[0];

$bulk_item_diff = $item_qty - $bulk_item_qty_found - $bulk_item_qty_write_off;

// echo $bulk_item_diff."<BR>";
$item_status_normal = $bulk_item_qty_found;
$item_status_surplus = "";
$item_status_missing = "";

if ($bulk_item_diff > 0)
    $item_status_missing = abs($bulk_item_diff);
if ($bulk_item_diff < 0)
    $item_status_surplus = abs($bulk_item_diff);

if ($item_status_missing == "")
    $item_status_missing = 0;
if ($item_status_surplus == "")
    $item_status_surplus = 0;

$table_content .= "<tr><td width=\"40%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Item Name</td><td class=\"tabletext\">$bulk_item_name</td></tr>";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_qty_found\" value=\"$bulk_item_qty_found\">";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_qty_write_off\" value=\"$bulk_item_qty_write_off\">";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_change\" value=$bulk_item_diff>";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_group_id[]\" value=\"$bulk_item_group_id\">";

$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Fine Qty</td><td class=\"tabletext\">$item_status_normal</td></tr>";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Missing Qty</td><td class=\"tabletext\">$item_status_missing</td></tr>";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Surplus Qty</td><td class=\"tabletext\">$item_status_surplus</td></tr>";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Remark</td><td class=\"tabletext\"><textarea name=\"bulk_item_remark\" rows=\"2\" cols=\"50\" class=\"textboxtext\"></textarea></td></tr>";

$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Write Off Qty</td><td class=\"tabletext\">$bulk_item_qty_write_off</td></tr>";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_write_off_qty\" value=\"$bulk_item_qty_write_off\">";

if ($bulk_item_qty_write_off > 0)
    $disable = " ";
else
    $disable = " DISABLED ";
    
    // Get Default Write Off Reason #
$sql = "SELECT ReasonTypeID, " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE";
$arr_write_off_reason = $linventory->returnArray($sql, 2);
$write_off_reason = getSelectByArray($arr_write_off_reason, " name=\"bulk_item_write_off_reason\" $disable");
// End #

$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Write Off Reason</td><td class=\"tabletext\">$write_off_reason</td></tr>";

$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">Write Off Attachment</td>";
$table_content .= "<td><input type=\"file\" class=\"file\" name=\"bulk_item_write_off_attachment\" $disable></td></tr>";
$table_content .= "<input type=\"hidden\" name=\"hidden_bulk_item_write_off_attachment\" value=\"\">";
$table_content .= "<input type=\"hidden\" name=\"item_group_id\" value=$item_group_id>\n";
$table_content .= "<input type=\"hidden\" name=\"item_location_id\" value=$item_location_id>\n";
$table_content .= "<input type=\"hidden\" name=\"item_id\" value=$item_id>\n";

echo generateFileUploadNameHandler("form1", "bulk_item_write_off_attachment", "hidden_bulk_item_write_off_attachment");
?>

<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	var bulk_check = 0;
	
	if(document.form1.bulk_item_write_off_qty.value > 0)
	{
		if(check_select(tmp_select,"Please select the reason",0))
		{
			bulk_check = 1;
		}
		else
		{
			bulk_check = 0;
			return false;
		}
	}
	else
	{
		bulk_check = 1;
	}

	if(bulk_check == 1)
	{
		Big5FileUploadHandler();
		document.form1.action = "stock_take_update.php";
		return true;
	}
}
</script>

<br>

<form name="form1" action="" method="post" onSubmit="return checkForm()">

	<table width="99%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$infobar?>
</table>

	<table border="0" width="96%" cellpadding="5" cellspacing="0">
<?=$table_content;?>
</table>

	<table width="95%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr height="10px">
			<td></td>
		</tr>
		<tr>
			<td class="dotline"><img
				src="<?="$image_path/$LAYOUT_SKIN" ?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='items_full_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>

</form>

<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
