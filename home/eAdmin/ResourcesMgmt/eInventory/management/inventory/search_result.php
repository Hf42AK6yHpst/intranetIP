<?php
// using: 

// #############################
//
// Date 2020-06-11 Cameron
// - fix: - should pass $hidWriteOffCriteria in hidden field so that Export(Code format) and Export(Name format) button can be shown
//      when navigate pages [case #N184694]
//
// Date 2019-05-16 Cameron
// - fix: - Purchase Price and Unit Price do not require to input both lower and upper amount [Case#N160963]
//        - Unit Price should compare unit price in db but not Purchase Price
//        - redirect to search page if $targetSearch is empty
//
// Date 2018-02-21 Henry
// - add access right checking [Case#E135442]
//
// Date: 2017-06-09 Henry
// added 2 export button [Case#M97011]
//
// Date: 2016-02-04 Henry
// php 5.4 issue move set cookies after includes file
//
// Date: 2015-04-14 Henry
// Fixed: show wrong Funding Source [Case#M76717]
//
// Date: 2014-09-05 YatWoon
// improved: add edit button for advanced search [Case#G39805]
// Deploy: EJv11.1
//
// Date: 2013-10-18 YatWoon
// fixed: Error for the table footer selection. [Case#2013-1017-1352-26170]
//
// Date: 2013-05-24 YatWoon
// add "Tag" [Case#2013-0524-1245-08054]
//
// Date: 2013-04-16 YatWoon
// support search for bulk item barcode
//
// Date: 2012-07-17 YatWoon
// add "Maintenance Details"
//
// #############################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

// ## set cookies
if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
// preserve table view
if ($ck_category_item_search_page_number != $pageNo && $pageNo != "") {
    setcookie("ck_category_item_search_page_number", $pageNo, 0, "", "", 0);
    $ck_category_item_search_page_number = $pageNo;
} else 
    if (! isset($pageNo) && $ck_category_item_search_page_number != "") {
        $pageNo = $ck_category_item_search_page_number;
    }

if ($ck_category_item_search_page_order != $order && $order != "") {
    setcookie("ck_category_item_search_page_order", $order, 0, "", "", 0);
    $ck_category_item_search_page_order = $order;
} else 
    if (! isset($order) && $ck_category_item_search_page_order != "") {
        $order = $ck_category_item_search_page_order;
    }

if ($ck_category_item_search_page_field != $field && $field != "") {
    setcookie("ck_category_item_search_page_field", $field, 0, "", "", 0);
    $ck_category_item_search_page_field = $field;
} else 
    if (! isset($field) && $ck_category_item_search_page_field != "") {
        $field = $ck_category_item_search_page_field;
    }

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (empty($targetSearch)) {
    header("location: search.php");
}

$TAGS_OBJ[] = array(
    $button_search,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// export column
$exportColumn = array(
    $i_InventorySystem_Item_Code,
    $i_InventorySystem_Item_Name,
    $i_InventorySystem_Item_Barcode,
    $i_InventorySystem['Category'],
    $i_InventorySystem_Item_Location,
    $i_InventorySystem_Group_Name,
    $i_InventorySystem_Item_Funding
);

$temp[] = array(
    $i_InventorySystem_Search_Result
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

$cond = stripslashes($cond);

if ($targetSearch == 1) {
    $sql = "SELECT
					a.ItemCode,
					CONCAT('<a href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">'," . $linventory->getInventoryItemNameByLang("a.") . ",'</a>'),
					IF(a.ItemType = 1, b.TagCode, bl.Barcode),
					" . $linventory->getInventoryNameByLang("e.") . ",
					" . $linventory->getInventoryNameByLang("f.") . "
			FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN 
					INVENTORY_ITEM_BULK_LOG AS c ON (a.ItemID = c.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
					INVENTORY_LOCATION AS e ON (b.LocationID = e.LocationID OR d.LocationID = e.LocationID) LEFT OUTER JOIN
					INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID) LEFT OUTER JOIN
					INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
			WHERE
					(a.NameChi LIKE '%$keyword%' OR a.NameEng LIKE '%$keyword%') OR
					(a.ItemCode LIKE '%$keyword%') OR
					(b.SupplierName LIKE '%$keyword%') OR
					(b.Brand LIKE '%$keyword%') OR
					(b.TagCode LIKE '%$keyword%') OR
					(e.NameChi LIKE '%$keyword%' OR e.NameEng LIKE '%$keyword%') OR
					(f.NameChi LIKE '%$keyword%' OR f.NameEng LIKE '%$keyword%') OR
					(g.NameChi LIKE '%$keyword%' OR g.NameEng LIKE '%$keyword%')
			GROUP BY
					a.ItemID";
}
if ($targetSearch == 2) {
    if ($WriteOffCriteria != "") {
        
        switch ($WriteOffCriteria) {
            case "All":
                $cond .= "(a.RecordStatus = 1 OR a.RecordStatus = 0)";
                break;
            case "WriteOff":
                $sql = "SELECT DISTINCT ItemID FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus = 1";
                
                $arr_target_item = $linventory->returnVector($sql);
                $target_list = implode(",", $arr_target_item);
                if ($target_list != "")
                    $cond .= "(a.RecordStatus = 0 OR a.ItemID IN ($target_list))";
                else
                    $cond .= "(a.RecordStatus = 0)";
                break;
            case "NonWriteOff":
                $cond .= "(a.RecordStatus = 1)";
                break;
            default:
                break;
        }

        $hidWriteOffCriteria = $WriteOffCriteria;
    }
    else {
        $hidWriteOffCriteria = $hidWriteOffCriteria;
    }

    if ($item_name != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (a.NameChi LIKE '%$item_name%' OR a.NameEng LIKE '%$item_name%') ";
        $exist_cond ++;
    }
    if (sizeof($categoryID) > 0) {
        $category_list = implode(",", $categoryID);
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " a.CategoryID IN ($category_list) ";
        $exist_cond ++;
    }
    if (sizeof($locationID) > 0) {
        $location_list = implode(",", $locationID);
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.LocationID In ($location_list) OR d.LocationID IN ($location_list)) ";
        $exist_cond ++;
    }
    if (sizeof($adminGroupID) > 0) {
        $admin_group_list = implode(",", $adminGroupID);
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.GroupInCharge In ($admin_group_list) OR d.GroupInCharge IN ($admin_group_list)) ";
        $exist_cond ++;
    }
    if ($item_brand != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " b.Brand LIKE '%$item_brand%' ";
        $exist_cond ++;
    }
    if ($item_code != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " a.ItemCode LIKE '%$item_code%' ";
        $exist_cond ++;
    }
    if (($purchase_date_start != "") && ($purchase_date_end != "")) {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " ((b.PurchaseDate BETWEEN '$purchase_date_start' AND '$purchase_date_end') OR (c.PurchaseDate BETWEEN '$purchase_date_start' AND '$purchase_date_end')) ";
        $exist_cond ++;
    }
    
    if (($price_start != "") && ($price_end != "")) {
        if ($exist_cond >= 0) {
            $cond .= " AND ";
        }
        $cond .= " ((b.PurchasedPrice BETWEEN '$price_start' AND '$price_end') OR (c.PurchasedPrice BETWEEN '$price_start' AND '$price_end')) ";
        $exist_cond ++;
    }
    else if (($price_start != "") && ($price_end == "")) {
        if ($exist_cond >= 0) {
            $cond .= " AND ";
        }
        $cond .= " ((b.PurchasedPrice>='$price_start') OR (c.PurchasedPrice>='$price_start')) ";
        $exist_cond ++;
    }
    else if (($price_start == "") && ($price_end != "")) {
        if ($exist_cond >= 0) {
            $cond .= " AND ";
        }
        $cond .= " ((b.PurchasedPrice<='$price_end') OR (c.PurchasedPrice<='$price_end')) ";
        $exist_cond ++;
    }

    if (($unit_price_start != "") && ($unit_price_end != "")) {
        if ($exist_cond >= 0) {
            $cond .= " AND ";
        }
        $cond .= " ((b.UnitPrice BETWEEN '$unit_price_start' AND '$unit_price_end') OR (c.UnitPrice BETWEEN '$unit_price_start' AND '$unit_price_end')) ";
        $exist_cond ++;
    }
    else if (($unit_price_start != "") && ($unit_price_end == "")) {
        if ($exist_cond >= 0) {
            $cond .= " AND ";
        }
        $cond .= " ((b.UnitPrice>='$unit_price_start') OR (c.UnitPrice>='$unit_price_start')) ";
        $exist_cond ++;
    }
    else if (($unit_price_start == "") && ($unit_price_end != "")) {
        if ($exist_cond >= 0) {
            $cond .= " AND ";
        }
        $cond .= " ((b.UnitPrice<='$unit_price_end') OR (c.UnitPrice<='$unit_price_end')) ";
        $exist_cond ++;
    }
    
    if ($supplier_name != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.SupplierName LIKE '%$supplier_name%' OR c.SupplierName LIKE '%supplier_name%') ";
        $exist_cond ++;
    }
    if ($invoice_no != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.InvoiceNo LIKE '%$invoice_no%' OR c.InvoiceNo LIKE '%$invoice_no%') ";
        $exist_cond ++;
    }
    if ($quotation_no != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.QuotationNo LIKE '%$quotation_no%' OR c.QuotationNo LIKE '%$quotation_no%') ";
        $exist_cond ++;
    }
    if ($tender_no != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.TenderNo LIKE '%$tender_no%' OR c.TenderNo LIKE '%$tender_no%') ";
        $exist_cond ++;
    }
    if ($tag_code != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        
        $barcode = $tag_code;
        if ((strpos($tag_code, "*") == 0) && (strrpos($tag_code, "*") == strlen($tag_code) - 1)) {
            $barcode = substr($tag_code, 1, strlen($tag_code) - 2);
        }
        $cond .= " ( b.TagCode LIKE '%$barcode%' or bl.Barcode LIKE '%$barcode%') ";
        $exist_cond ++;
    }
    if (sizeof($fundingID) > 0) {
        $funding_list = implode(",", $fundingID);
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " b.FundingSource IN ($funding_list) OR c.FundingSource IN ($funding_list) ";
        $exist_cond ++;
    }
    if(($warranty_start != "") && ($warranty_end != ""))
    {
        if($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " b.WarrantyExpiryDate BETWEEN '$warranty_start' AND '$warranty_end' ";
        $exist_cond++;
    }
    if ($license != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " b.SoftwareLicenseModel LIKE '%$license%' ";
        $exist_cond ++;
    }
    if ($serial != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " b.SerialNumber LIKE '%$serial%' ";
        $exist_cond ++;
    }
    
    if ($item_maintain_info != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $item_maintain_info = stripslashes($item_maintain_info);
        $item_maintain_info = str_replace("\\", "\\\\\\", $item_maintain_info);
        $item_maintain_info = str_replace("'", "\\\'", $item_maintain_info);
        $cond .= " (b.MaintainInfo LIKE '%$item_maintain_info%' OR c.Remark LIKE '%$item_maintain_info%') ";
        $exist_cond ++;
    }
    
    if ($remark != "") {
        if ($exist_cond >= 0)
            $cond .= " AND ";
        $cond .= " (b.ItemRemark LIKE '%$remark%' OR c.Remark LIKE '%$remark%') ";
        $exist_cond ++;
    }
    
    if ($tag != "") {
        $RecordIDbyTags_Arr = $linventory->getRecordIDsFromTagName($tag);
        if (is_array($RecordIDbyTags_Arr) && sizeof($RecordIDbyTags_Arr) > 0) {
            $RecordIDbyTags = implode(",", $RecordIDbyTags_Arr);
            $cond .= " AND a.ItemID IN ({$RecordIDbyTags}) ";
            $exist_cond ++;
        }
    }
    
    if ($linventory->IS_ADMIN_USER($UserID)) {
        $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        $arr_tmp_admin = $linventory->returnVector($sql);
        $target_admin_group = implode(",", $arr_tmp_admin);
        
        if ($target_admin_group != "") {
            if ($exist_cond >= 0) {
                $cond .= " AND ";
                $cond .= " ( b.GroupInCharge IN (" . $target_admin_group . ") or d.GroupInCharge IN (" . $target_admin_group . "))";
                $exist_cond ++;
            }
        }
    } else {
        if ($linventory->getInventoryAdminGroup() != "") {
            // $admin_condition1 = " AND b.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
            // $admin_condition2 = " AND d.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
            
            if ($exist_cond >= 0) {
                $cond .= " AND ";
                $cond .= " ( b.GroupInCharge IN (" . $linventory->getInventoryAdminGroup() . ") or d.GroupInCharge IN (" . $linventory->getInventoryAdminGroup() . "))";
                $exist_cond ++;
            }
        }
    }
    
    if ($cond == "") {
        $cond = "";
    } else {
        // $cond = stripslashes($cond);
        $cond = "WHERE " . $cond;
    }
    
    $sql = "SELECT 
					a.ItemCode,
					CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">'," . $linventory->getInventoryItemNameByLang("a.") . ",'</a>'),
					IF(a.ItemType = 1, b.TagCode, bl.Barcode),
					CONCAT(" . $linventory->getInventoryNameByLang("i.") . ",' > '," . $linventory->getInventoryNameByLang("h.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . "),
					" . $linventory->getInventoryNameByLang("f.") . ",
					" . $linventory->getInventoryNameByLang("j.") . ", 
					IF(a.ItemType = 2, 
						CONCAT('<input type=\"checkbox\" name=\"ItemID[]\" id=\"ItemID\" class=\"ItemID\" value=',a.ItemID,':',e.LocationID,':',d.GroupInCharge,':',d.FundingSourceID,' onClick=javascript:parseLocation(',e.LocationID,');>'),
						CONCAT('<input type=\"checkbox\" name=\"ItemID[]\" id=\"ItemID\" class=\"ItemID\" value=',a.ItemID,':',e.LocationID,' onClick=javascript:parseLocation(',e.LocationID,');>')),
					
					IF(a.ItemType = 1, CONCAT('single'), CONCAT('bulk')),
					a.ItemID
			FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN 
					INVENTORY_ITEM_SINGLE_STATUS_LOG as I on (a.ItemID = I.ItemID $single_action) LEFT OUTER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID ) LEFT OUTER JOIN 
					INVENTORY_ITEM_BULK_LOG AS c ON (a.ItemID = c.ItemID $bulk_action) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
					INVENTORY_LOCATION AS e ON (b.LocationID = e.LocationID OR d.LocationID = e.LocationID) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS g ON (e.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (g.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
					INVENTORY_CATEGORY_LEVEL2 AS h ON (a.Category2ID = h.Category2ID) LEFT OUTER JOIN
					INVENTORY_CATEGORY AS i ON (h.CategoryID = i.CategoryID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS j ON (b.FundingSource = j.FundingSourceID OR d.FundingSourceID = j.FundingSourceID) LEFT OUTER JOIN
					INVENTORY_PHOTO_PART AS k ON (a.PhotoLink = k.PartID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS f ON (b.GroupInCharge = f.AdminGroupID OR d.GroupInCharge = f.AdminGroupID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
					
					$cond
			GROUP BY
					a.ItemID, e.LocationID
			";
    
}

if (!empty($sql)) {
    $ReturnArr = $linventory->returnArray($sql);
}
else {
    $ReturnArr = array();
}

if (count($ReturnArr) == 1) {
    ?>
<html>
<head>
<title></title>
</head>
<body>
	<script language="javascript">
<!--
	var start_pos = 0;
	var end_pos = 0;
	var link = '<?=$ReturnArr[0][1]?>';
	start_pos = link.indexOf('"')+18;
	tmp_link = link.substring(start_pos);
	end_pos = tmp_link.indexOf('"');
	tmp_link2 = tmp_link.substring(end_pos);
	url = link.substring(start_pos,end_pos+start_pos);
	self.location=url;
//-->
</script>
</body>
</html>
<?php
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field = $field == "" ? 0 : $field;
/*
 * switch($field){
 * case 0: $field = 1; break;
 * case 1: $field = 1; break;
 * case 2: $field = 2; break;
 * case 3: $field = 3; break;
 * case 4: $field = 4; break;
 * case 5: $field = 5; break;
 * case 6: $field = 6; break;
 * case 7: $field = 7; break;
 * default: $field = 1;
 * }
 */
if (! isset($order))
    $order = 0;
    
    // TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo, true);
$li->field_array = array(
    "a.ItemCode",
    "a.NameEng",
    "b.TagCode",
    "h.NameEng",
    "e.NameEng",
    "f.NameEng",
    "j.NameEng"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 3;
$li->title = "";
$li->wrap_array = array();
$li->IsColOff = "eInventoryList";
// echo $li->built_sql();

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
// $li->column_list .= "<td>".$li->column($pos++, "Item ID")."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Item_Code) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Item_Name) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Item_Barcode) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem['Category']) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Location_Level) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Group_Name) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem['FundingSource']) . "</td>\n";
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'> </td>\n";
// $li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";
// echo $li->built_sql();
?>
<script language="javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
-->
</script>

<script language="javascript">
function exportCSV(val)
{
	if(val == 1)
	{
		obj = document.form1;
		obj.flag.value=0;
		obj.action = "search_result_export.php";
		obj.submit();
	}
	obj.action = "";
}

function checkExportItemDetail(obj,element,page){
        if(countChecked(obj,element)==0)
                if(confirm("<?=$i_InventorySystem_ExportWarning;?>")){
	                obj.action=page;
	                obj.submit();
                }else{
	                obj.action='';
                }
        else{
                obj.action=page;
                obj.submit();
        }
        obj.action="";
}
</script>

<br>

<form name="form1" action="" method="POST">
	<table width="100%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar1 ?></td>
		</tr>
	</table>

	<br>

	<table width="90%" border="0" cellpadding="3" cellspacing="0"
		align="center">
		<tr>
			<td width="30%" valign="top" nowrap="nowrap"
				class="formfieldtitle tabletext">
			<?=$i_InventorySystem_Export_Search_Result_Column;?>
		</td>
			<td>
				<table border="0" cellpadding="5" cellspacing="0" align="center">
				<?
    for ($i = 0; $i < sizeof($exportColumn); $i ++) {
        if ($i % 2 == 0)
            $row_css = " class=\"tablerow1\" ";
        else
            $row_css = " class=\"tablerow2\" ";
        
        if ((($i + 1) % 4) == 1 && $i != 0) {
            echo "<tr $row_css>";
        }
        
        echo "<td width=\"20%\" class=\"tabletext\"><input type=\"checkbox\" name=\"ColumnName[]\" value=\"$i\" checked> $exportColumn[$i]</td>";
        
        if ((($i + 1) % 4) == 0) {
            echo "</tr>";
        }
    }
    ?>
			</table>
			</td>
		</tr>
	</table>
	<table border="0" width="90%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr height="10px">
			<td></td>
		</tr>
		<tr>
			<td align="center">
	<?=$linterface->GET_ACTION_BTN($button_export, "button", "javascript:exportCSV(1)")?>
</td>
		</tr>
	</table>

	<br>

<?
// Admin or leader
if ($hidWriteOffCriteria == 'NonWriteOff' && ($linventory->getAccessLevel($UserID) == 1 || $linventory->getAccessLevel($UserID) == 2)) {
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkExportItemDetail(document.form1,'ItemID[]','items_detail_export.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$i_InventorySystem_ExportItemDetails1
						</a>
					</td>";
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkExportItemDetail(document.form1,'ItemID[]','items_detail_export2.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$i_InventorySystem_ExportItemDetails2
						</a>
					</td>";
}
if ($linventory->getAccessLevel($UserID) != 3) {
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ItemID[]','category_show_items_edit.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
}

?>

<table width="96%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr class="table-action-bar">
			<td colspan="" valign="bottom" align="right">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
								<?=$table_tool?>
							</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


<?=$li->display("96%");?>

<table border="0" width="96%" cellpadding="5" cellspacing="0">
		<tr>
			<td><table border="0" cellpadding="0">
					<tr>
						<td><table border="0">
								<tr>
									<td width="10" height="10"><table width="100%" border="1"
											cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
											<tr>
												<td class="single"><img
													src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"
													width="10" height="10"></td>
											</tr>
										</table></td>
									<td><span class="tabletext"><?=$i_InventorySystem_ItemType_Single?></span></td>
								</tr>
							</table></td>
						<td><table width="100%" border="0">
								<tr>
									<td width="10" height="10"><table width="100%" border="1"
											cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
											<tr>
												<td class="bulk"><img
													src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif"
													width="10" height="10"></td>
											</tr>
										</table></td>
									<td><span class="tabletext"><?=$i_InventorySystem_ItemType_Bulk?></span></td>
								</tr>
							</table></td>
					</tr>
				</table></td>
		</tr>
	</table>
	<input type="hidden" name="targetSearch" value="<?=$targetSearch;?>">
<?
if ($cond != "") {
    // $cond = stripslashes(str_replace("WHERE","",$cond));
    $cond = str_replace("WHERE", "", $cond);
}
$itemIDs = '';
if (count($ReturnArr) > 0) {
    foreach ($ReturnArr as $item) {
        $itemIDs .= $item['ItemID'] . ',';
    }
    $itemIDs = rtrim($itemIDs, ',');
}

?>
<input type="hidden" name="cond"
		value="<?=intranet_htmlspecialchars($cond);?>"> <input type="hidden"
		name="pageNo" value="<?php echo $li->pageNo; ?>"> <input type="hidden"
		name="order" value="<?php echo $li->order; ?>"> <input type="hidden"
		name="field" value="<?php echo $li->field; ?>"> <input type="hidden"
		name="page_size_change" value=""> <input type="hidden"
		name="numPerPage" value="<?=$li->page_size?>"> <input type="hidden"
		name="flag" value="0"> <input type="hidden" name="itemIDs"
		value="<?=$itemIDs?>">
<input type="hidden" name="hidWriteOffCriteria" value="<?php echo $hidWriteOffCriteria; ?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>