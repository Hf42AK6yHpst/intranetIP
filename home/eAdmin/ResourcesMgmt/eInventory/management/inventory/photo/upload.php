<?php
// modifying : yat
/****************************** Change Log *******************************
 * 
 * Date:	2014-05-23	YatWoon
 * 			add remark for SKH or flag $sys_custom['eInventoryCustForSKH'] || $special_feature['eInventoryItemFilenameHandling']
 * 
 ******************************* Change Log *********************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='../items_full_list.php'", "backBtn");

#step
$STEPS_OBJ[] = array($Lang['eInventory']['UploadPhoto']['StepArr'][0], 1);
$STEPS_OBJ[] = array($Lang['eInventory']['UploadPhoto']['StepArr'][1], 0);
$StepObj = $linterface->GET_STEPS($STEPS_OBJ);

# navigation
$PAGE_NAVIGATION[] = array($i_InventorySystem['FullList'], "../items_full_list.php");
$PAGE_NAVIGATION[] = array($Lang['eInventory']['UploadImages'], "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

# Remarks list
for($i=1; $i<=sizeof($Lang['eInventory']['RemarksList']);$i++)
	$remarks .= $i.'. '.$Lang['eInventory']['RemarksList'][$i-1]."<br>";

$TAGS_OBJ[] = array($button_upload);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$PageNavigation ?></td>
							</tr>
						</table>
					</td>
					<td align="right"></td>
				</tr>
				</table>
				<br>
			<form name="form1" method="post" action="upload_update.php" onsubmit="return checkform()"  enctype="multipart/form-data">
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
			</table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['eInventory']['File']?></td>
					<td class="tabletext"><input id="UploadFile" type="file" name="UploadFile"></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$Lang['eInventory']['Remarks']?></td>
					<td class="tabletext"><?=$remarks?>
					<?if($sys_custom['eInventoryCustForSKH'] || $special_feature['eInventoryItemFilenameHandling']) {?>
					<br><?=$Lang['eInventory']['BatchImage']['FilenameRemark'] ?>
					<? } ?>
					</td>
				</tr>

			</table>
			
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$submitBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
			<br><br>
		</td>
	</tr>
</table>
</form>
<script>
function checkform()
{
	var filename = $("#UploadFile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(!(fileext==".jpg" || fileext==".gif" || fileext==".png" || fileext==".zip"))
	{	
		alert("<?=$Lang['eInventory']['WarnMsg']['PleaseSelectIMAGEorZIP']?>");
		return false;
	}
	return true;
}
</script>
<?
$linterface->LAYOUT_STOP();		
?>