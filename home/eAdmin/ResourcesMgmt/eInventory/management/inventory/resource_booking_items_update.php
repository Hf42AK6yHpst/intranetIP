<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;

if($ExistingResource == 0)
{	
	$li = new libdb();
	
	$ResourceCode = intranet_htmlspecialchars(trim($resource_code));
	//$ResourceCategory = intranet_htmlspecialchars(trim($resource_category));
	$ResourceCategory = trim($resource_category);
	$Title = intranet_htmlspecialchars(trim($resource_name));
	$Description = intranet_htmlspecialchars(trim($resource_description));
	$Remark = intranet_htmlspecialchars(trim($resource_remark));
	$Attachment = intranet_htmlspecialchars(trim($Attachment));
	$daysbefore = intranet_htmlspecialchars(trim($day_in_advance));
	$batchID = intranet_htmlspecialchars(trim($batchID));
	$DefaultStatus = intranet_htmlspecialchars(trim($auto_approve));
	if ($DefaultStatus == 'yes')
	    $RecordType = 4;
	else $RecordType = 0;
	
	$sql = "INSERT INTO INTRANET_RESOURCE (ResourceCode, ResourceCategory, Title, Description, Remark, Attachment,RecordType, RecordStatus, DateInput, DateModified, DaysBefore, TimeSlotBatchID) VALUES ('$ResourceCode', '$ResourceCategory', '$Title', '$Description', '$Remark', '$Attachment','$RecordType', '$resource_status', now(), now(),'$daysbefore','$batchID')";
	$li->db_db_query($sql);
	$ResourceID = $li->db_insert_id();
	
	for($i=0; $i<sizeof($GroupID); $i++){
	     $sql = "INSERT INTO INTRANET_GROUPRESOURCE (GroupID, ResourceID) VALUES (".$GroupID[$i].", $ResourceID)";
	     $li->db_db_query($sql);
	}
	
	$sql = "INSERT INTO INTRANET_RESOURCE_INVENTORY_ITEM_LIST (ResourceID, ItemID) VALUES ($ResourceID, $InventoryItemID)";
	$li->db_db_query($sql);
	
	
	intranet_closedb();
	header("Location: items_full_list.php?msg=1");
}

if($ExistingResource == 1)
{
	$li = new libresource($ResourceID);
	$old_bid = $li->TimeSlotBatchID;
			
	$ResourceCode = intranet_htmlspecialchars(trim($resource_code));
	$ResourceCategory = intranet_htmlspecialchars(trim($resource_category));
	$Title = intranet_htmlspecialchars(trim($resource_name));
	$Description = intranet_htmlspecialchars(trim($resource_description));
	$Remark = intranet_htmlspecialchars(trim($resource_remark));
	$Attachment = intranet_htmlspecialchars(trim($Attachment));
	$daysbefore = intranet_htmlspecialchars(trim($day_in_advance));
	$batchID = intranet_htmlspecialchars(trim($batchID));
	$DefaultStatus = intranet_htmlspecialchars(trim($auto_approve));
	if ($DefaultStatus == 'yes')
	    $RecordType = 4;
	else $RecordType = 0;
		
	$sql = "UPDATE INTRANET_RESOURCE SET
	               ResourceCode = '$ResourceCode',
	               ResourceCategory = '$ResourceCategory',
	               Title = '$Title',
	               Description = '$Description',
	               Remark = '$Remark',
	               Attachment = '$Attachment',
	               RecordStatus = '$resource_status',
	               RecordType = '$RecordType',
	               DaysBefore = '$daysbefore',
	               TimeSlotBatchID = '$batchID',
	               DateModified = now()
	        WHERE
	               ResourceID = $ResourceID";
	$li->db_db_query($sql);
	
	$sql = "DELETE FROM INTRANET_GROUPRESOURCE WHERE ResourceID = $ResourceID";
	$li->db_db_query($sql);
		
	for($i=0; $i<sizeof($GroupID); $i++){
	     $sql = "INSERT INTO INTRANET_GROUPRESOURCE (GroupID, ResourceID) VALUES (".$GroupID[$i].", $ResourceID)";
	     $li->db_db_query($sql);
	}
	
	# Perform cancellation and archival
	if ($reset_record == 1 && $old_bid != $batchID)
	{
//	    # Archival
//	    $conds = "a.ResourceID = $ResourceID AND a.BookingDate < CURDATE()";
//	    $sql = "INSERT INTO INTRANET_BOOKING_ARCHIVE
//	            (BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
//	            SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
//	            CONCAT(b.EnglishName, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ),
//	            d.ResourceCategory, d.Title, d.ResourceCode,
//	            a.RecordStatus, a.TimeApplied, a.LastAction
//	            FROM INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d
//	            WHERE a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq
//	            AND $conds";
//	    $li->db_db_query($sql);
//	
//	    # Remove records
//	    $cond2 = "ResourceID = $ResourceID";
//	    $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE $cond2";
//	    $li->db_db_query($sql);
		include_once($PATH_WRT_ROOT."includes/librb.php");
		$librb = new librb();
		$librb->Archive_Booking_Record($ResourceID);
	}
	
	intranet_closedb();
	header("Location: items_full_list.php?&msg=2");
}
?>
