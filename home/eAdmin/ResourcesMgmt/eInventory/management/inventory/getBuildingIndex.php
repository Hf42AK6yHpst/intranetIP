<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linventory	= new libinventory();
$llocation = new Building();

$layer_content .= "<table border=0 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
if($item_type == 1)
	$layer_content .= "<tr class=tabletop><td colspan=6 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu2')></td></tr>";
else
	$layer_content .= "<tr class=tabletop><td colspan=6 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu3')></td></tr>";
$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['BuildingCode']."</td><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['BuildingName']."</td></tr>";

## get all building (have room only) ##
$arr_result = $llocation->Get_All_Building(0,1);
if(sizeof($arr_result>0))
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		$layer_content .= "<tr><td class=\"tabletext\">".$arr_result[$i]['Code']."</td>";
		if($intranet_session_language == 'b5'){
			$layer_content .= "<td class=\"tabletext\">".$arr_result[$i]['NameChi']."</td>";
		}else{
			$layer_content .= "<td class=\"tabletext\">".$arr_result[$i]['NameEng']."</td>";
		}
	}
}
if(sizeof($arr_result)==0)
{
	$layer_content .= "<tr><td class=\"tabletex\" colspan=3>$i_no_record_exists_msg</td></tr>";
}
			
$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\" class=\"tabletext\"><font color=\"red\">".$Lang['eInventory']['FieldTitle']['Import']['CorrecrBuildingIndexReminder']."</font></td></tr>";

$layer_content .= "</table>";

$layer_content = "<br /><br />".$layer_content;

//$response = iconv("Big5","UTF-8",$layer_content);
$response = $layer_content;

echo $response;
?>