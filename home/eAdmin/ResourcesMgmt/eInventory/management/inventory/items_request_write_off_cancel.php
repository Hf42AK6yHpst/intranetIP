<?php

####################################
#
#
####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linventory		= new libinventory();
$lf = new libfilesystem();

$ItemLocation = $ItemLocation ? $ItemLocation : $targetLocation;

## remove attachment
$attachment_folder = $file_path . "file/inventory/write_off/".$WriteOffRecordID;
$lf->lfs_remove($attachment_folder);

## remove record
$sql = "delete from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID = $WriteOffRecordID";
$linventory->db_db_query($sql);

$sql = "delete from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordID = $WriteOffRecordID";
$linventory->db_db_query($sql);

// header("location: items_request_write_off.php?item_id=$ItemID&targetLocation=$ItemLocation&xmsg=DeleteSuccess");
header("location: items_request_write_off.php?item_id=$item_id&targetLocation=$ItemLocation&targetGroupID=$targetGroupID&targetFundingID=$targetFundingID&xmsg=DeleteSuccess");


intranet_closedb();
?>