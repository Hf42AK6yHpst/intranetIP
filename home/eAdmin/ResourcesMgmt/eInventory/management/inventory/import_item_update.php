<?php
// using by: 

/**
 * ******************************** Change Log ********************************************
 * Date : 2019-07-26 (Tommy)
 * - add new item code format import update
 * 
 * Date : 2019-05-13 (Henry)
 * - Security fix: SQL without quote
 *
 * Date : 2018-02-07 (Henry)
 * - add access right checking [Case#E135442]
 *
 * Date: 2017-06-02 (Henry)
 * - item code cust for SPCCPS [Case#P115830]
 *
 * Date: 2015-09-21 (Henry)
 * - fixed: Single items - Failed to remove remark by import [Case#W85522]
 *
 * Date: 2014-04-03 (YatWoon)
 * improved: SKH eInventory ($sys_custom['eInventoryCustForSKH'])
 * - purchase date is mandatory field
 * - generate item cod format (function generateItemCode())
 *
 * Date: 2014-01-15 (YatWoon)
 * updated: generateItemCode(), cater if category with "_", this will cause infinity looping [Case#D57167]
 *
 * Date: 2013-07-30 (YatWoon)
 * updated: not allow import update bulk item qty!
 *
 * Date: 2013-07-08 (YatWoon)
 * Fixed: update generate item code logic [Case#2013-0704-1259-50071]
 *
 * Date: 2013-06-06 (Carlos)
 * Fixed update of INVENTORY_ITEM_BULK_EXT Quantity for new bulk item
 *
 * Date: 2013-04-25 (YatWoon)
 * Improved: revise generateItemCode()
 *
 * Date: 2013-04-10 (YatWoon)
 * Fixed: failed to insert more than 1 single item [Case#2013-0114-0933-42156]
 *
 * Date: 2013-03-25 (Carlos)
 * update barcode for bulk item if provided
 *
 * Date: 2013-02-25 (Carlos)
 * auto generate barcode for new bulk item
 *
 * Date: 2013-02-20 (YatWoon)
 * generate item code if item code is empty in csv (bulk)
 * set variance manager = resoruces group if variance manager data in csv is empty [Case#2013-0115-0922-11066]
 *
 * Date: 2012-12-12 (Yuen)
 * Support tags
 *
 * Date : 2012-12-04 (YatWoon)
 * Improved: add "createdby" data [case#2011-0124-1006-09067]
 *
 * Date : 2012-11-09 (Yuen)
 * added StocktakeOption field
 *
 * 2012-09-20 [YatWoon]: generate item code if item code is empty in csv (single)
 * Date : 2012-06-29 (Henry Chow)
 * allow user to add quantity by "Import > Insert bulk item" [CRM : 2012-0620-1102-42071]
 *
 * 2012-04-02 (Henry Chow) : insert/update item in location (with same Funding Source ID & Resource Mgmt Group ID)
 * 2011-08-02 [Henry Chow]: update "Funding Source" when updating bulk item
 * 2011-01-17 [Carlos]: Escape ' from imported data
 * *****************************************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$curr_date = date("Y-m-d");

if ($format == "")
    header("location: import_item.php");

$result = array();

// Item Code of existing items
$sql = "SELECT DISTINCT(ItemCode) FROM INVENTORY_ITEM";
$CurrentItemCodeAry = $linventory->returnVector($sql);

// ##############
// single item #
// ##############
if ($format == ITEM_TYPE_SINGLE) {
    $sql = "SELECT ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, TagCode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode1, UnitPrice1, FundingSourceCode2, UnitPrice2, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, Brand, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, ItemRemark, ExistItem, Quantity, StockTakeOption, Tags FROM TEMP_INVENTORY_ITEM ORDER BY Quantity";
    $arr_result = $linventory->returnArray($sql, 30);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_type, $category_id, $category2_id, $item_categoryCode, $item_category2Code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_code, $item_tag_code, $item_ownership, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode1, $item_unitprice1, $item_fundingCode2, $item_unitprice2, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier_name, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchased_price, $item_unit_price, $item_maintain_info, $item_remark, $exist_item, $item_qty, $stocktakeoption, $tags) = $arr_result[$i];
            
            unset($item_value);
            unset($item_ext_value);
            
            $item_chi_name = addslashes($item_chi_name);
            $item_eng_name = addslashes($item_eng_name);
            $item_chi_description = addslashes($item_chi_description);
            $item_eng_description = addslashes($item_eng_description);
            $item_remark = addslashes($item_remark);
            $item_supplier_name = addslashes($item_supplier_name);
            $item_supplier_contact = addslashes($item_supplier_contact);
            $item_supplier_description = addslashes($item_supplier_description);
            $item_maintain_info = addslashes($item_maintain_info);
            $item_brand = addslashes($item_brand);
            $item_tag_code = strtoupper($item_tag_code);
            $item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
            $item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
            $item_code = $linventory->Get_Safe_Sql_Query($item_code);
            $item_tag_code = $linventory->Get_Safe_Sql_Query($item_tag_code);
            $item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
            $item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
            $item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
            $item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
            $item_fundingCode1 = $linventory->Get_Safe_Sql_Query($item_fundingCode1);
            $item_unitprice1 = $linventory->Get_Safe_Sql_Query($item_unitprice1);
            $item_fundingCode2 = $linventory->Get_Safe_Sql_Query($item_fundingCode2);
            $item_unitprice2 = $linventory->Get_Safe_Sql_Query($item_unitprice2);
            $item_warranty = $linventory->Get_Safe_Sql_Query($item_warranty);
            $item_license = $linventory->Get_Safe_Sql_Query($item_license);
            $item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
            $item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
            $item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
            $item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
            $stocktakeoption = $linventory->Get_Safe_Sql_Query($stocktakeoption);
            
            if ($exist_item == 0) {
                for ($j = 0; $j < $item_qty; $j ++) {
                    
                    // generate item code if qty > 1
                    // if($item_qty>1) {
                    if ($j > 0 || $item_code == "") {
                        // $successItemCodeGenerate = ($item_code=="") ? false : true;
                        $successItemCodeGenerate = false;
                        while (! $successItemCodeGenerate) {
                            $item_code = generateItemCode($item_categoryCode, $item_category2Code, $item_purchase_date, $item_admin_group_code, $item_locationCode, $item_fundingCode1, $existItemCategory[$item_categoryCode . $item_category2Code]);
                            
                            if (! in_array($item_code, $CurrentItemCodeAry)) {
                                $CurrentItemCodeAry[] = $item_code;
                                $successItemCodeGenerate = true;
                                $existItemCategory[$item_categoryCode . $item_category2Code] = 0;
                            } else {
                                $existItemCategory[$item_categoryCode . $item_category2Code] += 1;
                            }
                        }
                    }
                    
                    // echo $j.'/'.$existItemCategory[$item_categoryCode.$item_category2Code].'/'.$item_code.'<br>';
                    // generate item code if empty
                    // if($item_qty>1) {
                    // $item_code = generateItemCode($item_categoryCode,$item_category2Code,$item_purchase_date,$item_admin_group_code,$item_locationCode,$item_fundingCode1,$existItemCategory[$item_categoryCode.$item_category2Code]);
                    // }
                    
                    // generate TagCode (BarCode) if empty
                    if ($j > 0 || $item_tag_code == "") {
                        // $successGenerate = ($item_tag_code=="") ? false : true;
                        $successGenerate = false;
                        while (! $successGenerate) {
                            $tmp_barcode = $linventory->random_barcode();
                            $sql = "SELECT COUNT(TagCode) FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '" . $linventory->Get_Safe_Sql_Query($tmp_barcode) . "'";
                            $arr_tmp_barcode = $linventory->returnVector($sql);
                            if ($arr_tmp_barcode[0] == 0) {
                                $item_tag_code = $tmp_barcode;
                                $successGenerate = true;
                                break;
                            }
                        }
                    }
                    // }
                    
                    $values = "$item_type, $category_id, $category2_id, '$item_chi_name', '$item_eng_name', '$item_chi_description', '$item_eng_description', '$item_code', $item_ownership, '', '', 1, '$stocktakeoption', NOW(), NOW(), '$UserID'";
                    $sql = "INSERT INTO INVENTORY_ITEM
									(ItemType, 
									CategoryID, 
									Category2ID, 
									NameChi, 
									NameEng, 
									DescriptionChi, 
									DescriptionEng, 
									ItemCode, 
									Ownership, 
									PhotoLink, 
									RecordType, 
									RecordStatus, 
									StockTakeOption, 
									DateInput,  
									DateModified,
									CreatedBy)
							VALUES
									($values)";
                    $result['NewInvItem' . $i . '_' . $j] = $linventory->db_db_query($sql);
                    $sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$item_code'";
                    $arr_item_id = $linventory->returnArray($sql, 1);
                    
                    if (sizeof($arr_item_id) > 0) {
                        list ($item_id) = $arr_item_id[0];
                        
                        $sql = "SELECT BuildingID from INVENTORY_LOCATION_BUILDING WHERE Code = '$item_buildingCode' AND RecordStatus = 1";
                        $BuildingID = $linventory->returnArray($sql, 1);
                        
                        $sql = "SELECT LocationLevelID FROM INVENTORY_LOCATION_LEVEL WHERE Code = '$item_locationLevelCode' AND BuildingID = '" . $BuildingID[0]['BuildingID'] . "' AND RecordStatus = 1";
                        $LocationLevelID = $linventory->returnArray($sql, 1);
                        
                        $sql = "select LocationID from INVENTORY_LOCATION where Code = '$item_locationCode' AND LocationLevelID = '" . $LocationLevelID[0]['LocationLevelID'] . "' AND RecordStatus = 1";
                        $LocationID = $linventory->returnArray($sql, 1);
                        
                        $sql = "select AdminGroupID from INVENTORY_ADMIN_GROUP where Code = '$item_admin_group_code'";
                        $AdminGroupID = $linventory->returnArray($sql, 1);
                        
                        $sql = "select FundingSourceID from INVENTORY_FUNDING_SOURCE where Code = '$item_fundingCode1'";
                        $FundingSourceID1 = $linventory->returnArray($sql, 1);
                        
                        if ($item_fundingCode2 == "") {
                            $FundingSourceID2 = array();
                        } else {
                            $sql = "select FundingSourceID from INVENTORY_FUNDING_SOURCE where Code = '$item_fundingCode2'";
                            $FundingSourceID2 = $linventory->returnArray($sql);
                        }
                        
                        $values = " $item_id, '$item_purchase_date', '$item_purchased_price', '$item_unit_price', '$item_remark', '$item_supplier_name', '$item_supplier_contact', '$item_supplier_description', '$item_maintain_info', '$item_invoice_no', '$item_quotation_no', '$item_tender_no', '$item_tag_code', '$item_brand', " . $AdminGroupID[0]['AdminGroupID'] . ", " . $LocationID[0]['LocationID'] . ", " . $FundingSourceID1[0]['FundingSourceID'] . ", '$item_unitprice1', '" . $FundingSourceID2[0]['FundingSourceID'] . "', '$item_unitprice2', '$item_warranty', '$item_license' , '$item_serial' ";
                        
                        $sql = "INSERT INTO INVENTORY_ITEM_SINGLE_EXT
										(ItemID, PurchaseDate, PurchasedPrice, UnitPrice, ItemRemark, SupplierName, SupplierContact, SupplierDescription, MaintainInfo, InvoiceNo, QuotationNo, TenderNo, TagCode, Brand, GroupInCharge, LocationID, FundingSource, UnitPrice1, FundingSource2, UnitPrice2, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber)
								VALUES
										($values)";
                        
                        $result['NewInvItemSingleExt' . $i . '_' . $j] = $linventory->db_db_query($sql);
                        
                        $sql = "INSERT INTO INVENTORY_ITEM_SINGLE_STATUS_LOG
										(ItemID, RecordDate, Action, PastStatus, NewStatus, 
										PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
								VALUES
										($item_id,'$curr_date'," . ITEM_ACTION_PURCHASE . ", '', " . ITEM_STATUS_NORMAL . ",
										$UserID,'','','',NOW(),NOW())";
                        
                        $result['NewInvItemSingleLog' . $i . '_' . $j] = $linventory->db_db_query($sql);
                        
                        // add tags
                        $linventory->saveEntryTag($item_id, $tags);
                    }
                }
            } else // UPDATE SINGLE ITEM
{
                if ($category_id != "" && $category_id != 0) {
                    $item_value[] = "CategoryID = '$category_id'";
                }
                
                if ($category2_id != "" && $category2_id != 0) {
                    $item_value[] = "Category2ID = '$category2_id'";
                }
                if ($item_chi_name != "") {
                    $item_value[] = "NameChi = '$item_chi_name'";
                }
                if ($item_eng_name != "") {
                    $item_value[] = "NameEng = '$item_eng_name'";
                }
                if ($item_chi_description != "") {
                    $item_value[] = "DescriptionChi = '$item_chi_description'";
                }
                if ($item_eng_description != "") {
                    $item_value[] = "DescriptionEng = '$item_eng_description'";
                }
                if ($item_ownership != "" && $item_ownership != 0) {
                    $item_value[] = "Ownership = '$item_ownership'";
                }
                $item_value[] = "DateModified = 'NOW()'";
                
                $item_value[] = "StockTakeOption = '$stocktakeoption'";
                
                if (sizeof($item_value) > 0) {
                    $value = implode(",", $item_value);
                    $sql = "UPDATE INVENTORY_ITEM SET $value WHERE ItemCode = '$item_code'";
                    $result['UpdateInvItem' . $i] = $linventory->db_db_query($sql);
                }
                
                $sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$item_code'";
                $arr_itemID = $linventory->returnArray($sql, 1);
                if (sizeof($arr_itemID) > 0) {
                    list ($existItemID) = $arr_itemID[0];
                }
                
                // add tags
                $linventory->saveEntryTag($existItemID, $tags);
                if ($item_purchase_date != "" && $item_purchase_date != "0000-00-00") {
                    $item_ext_value[] = "PurchaseDate = '$item_purchase_date'";
                    $arr_item_insert_field[] = "PurchaseDate";
                    $arr_item_insert_value[] = "'$item_purchase_date'";
                }
                if ($item_purchased_price != "") {
                    $item_ext_value[] = "PurchasedPrice = '$item_purchased_price'";
                    $arr_item_insert_field[] = "PurchasedPrice";
                    $arr_item_insert_value[] = "'$item_purchased_price'";
                }
                if ($item_unitprice1 != "") {
                    $item_ext_value[] = "UnitPrice1 = '$item_unitprice1'";
                    $arr_item_insert_field[] = "UnitPrice1";
                    $arr_item_insert_value[] = "'$item_unitprice1'";
                }
                if ($item_unitprice2 != "") {
                    $item_ext_value[] = "UnitPrice2 = '$item_unitprice2'";
                    $arr_item_insert_field[] = "UnitPrice2";
                    $arr_item_insert_value[] = "'$item_unitprice2'";
                }
                if ($item_unit_price != "") {
                    $item_ext_value[] = "UnitPrice = '$item_unit_price'";
                    $arr_item_insert_field[] = "UnitPrice";
                    $arr_item_insert_value[] = "'$item_unit_price'";
                }
                if ($item_admin_group_code != "") {
                    $sql = "select AdminGroupID from INVENTORY_ADMIN_GROUP where Code = '$item_admin_group_code'";
                    $arrAdminGroupID = $linventory->returnVector($sql);
                    $adminGroupID = $arrAdminGroupID[0];
                    if ($adminGroupID != 0 and $adminGroupID != "") {
                        $item_ext_value[] = "GroupInCharge = '$adminGroupID'";
                        $arr_item_insert_field[] = "GroupInCharge";
                        $arr_item_insert_value[] = "'$adminGroupID'";
                    }
                }
                if ($item_locationCode != "") {
                    $sql = "SELECT BuildingID FROM INVENTORY_LOCATION_BUILDING WHERE Code = '$item_buildingCode' AND RecordStatus = 1";
                    $BuildingID = $linventory->returnArray($sql, 1);
                    
                    $sql = "SELECT LocationLevelID FROM INVENTORY_LOCATION_LEVEL WHERE Code = '$item_locationLevelCode' AND BuildingID = '" . $BuildingID[0]['BuildingID'] . "' AND RecordStatus = 1";
                    $LocationLevelID = $linventory->returnArray($sql, 1);
                    
                    $sql = "select LocationID from INVENTORY_LOCATION where Code = '$item_locationCode' AND LocationLevelID = '" . $LocationLevelID[0]['LocationLevelID'] . "' AND RecordStatus = 1";
                    $arrLocationID = $linventory->returnVector($sql);
                    $locationID = $arrLocationID[0];
                    if ($locationID != 0 and $locationID != "") {
                        $item_ext_value[] = "LocationID = '$locationID'";
                        $arr_item_insert_field[] = "LocationID";
                        $arr_item_insert_value[] = "'$locationID'";
                    }
                }
                if ($item_fundingCode1 != "") {
                    $sql = "select FundingSourceID from INVENTORY_FUNDING_SOURCE where Code = '$item_fundingCode1'";
                    $arrFundingSourceID1 = $linventory->returnVector($sql);
                    $fundingSourceID1 = $arrFundingSourceID1[0];
                    if ($fundingSourceID1 != 0 and $fundingSourceID1 != "") {
                        $item_ext_value[] = "FundingSource = '$fundingSourceID1'";
                        $arr_item_insert_field[] = "FundingSource";
                        $arr_item_insert_value[] = "'$fundingSourceID1'";
                    }
                }
                if ($item_fundingCode2 != "") {
                    $sql = "select FundingSourceID from INVENTORY_FUNDING_SOURCE where Code = '$item_fundingCode2'";
                    $arrFundingSourceID2 = $linventory->returnVector($sql);
                    $fundingSourceID2 = $arrFundingSourceID2[0];
                    if ($fundingSourceID2 != 0 and $fundingSourceID2 != "") {
                        $item_ext_value[] = "FundingSource2 = '$fundingSourceID2'";
                        $arr_item_insert_field[] = "FundingSource2";
                        $arr_item_insert_value[] = "'$fundingSourceID2'";
                    }
                } else {
                    $item_ext_value[] = "FundingSource2 = ''";
                    // $arr_item_insert_field[] = "FundingSource2";
                    // $arr_item_insert_value[] = "''";
                }
                if ($item_remark != "") {
                    $item_ext_value[] = "ItemRemark = '$item_remark'";
                    $arr_item_insert_field[] = "ItemRemark";
                    $arr_item_insert_value[] = "'$item_remark'";
                } else {
                    $item_ext_value[] = "ItemRemark = ''";
                }
                if ($item_supplier_name != "") {
                    $item_ext_value[] = "SupplierName = '$item_supplier_name'";
                    $arr_item_insert_field[] = "SupplierName";
                    $arr_item_insert_value[] = "'$item_supplier_name'";
                }
                if ($item_supplier_contact != "") {
                    $item_ext_value[] = "SupplierContact = '$item_supplier_contact'";
                    $arr_item_insert_field[] = "SupplierContact";
                    $arr_item_insert_value[] = "'$item_supplier_contact'";
                }
                if ($item_supplier_description != "") {
                    $item_ext_value[] = "SupplierDescription = '$item_supplier_description'";
                    $arr_item_insert_field[] = "SupplierDescription";
                    $arr_item_insert_value[] = "'$item_supplier_description'";
                }
                if ($item_maintain_info != "") {
                    $item_ext_value[] = "MaintainInfo = '$item_maintain_info'";
                    $arr_item_insert_field[] = "MaintainInfo";
                    $arr_item_insert_value[] = "'$item_maintain_info'";
                }
                if ($item_brand != "") {
                    $item_ext_value[] = "Brand = '$item_brand'";
                    $arr_item_insert_field[] = "Brand";
                    $arr_item_insert_value[] = "'$item_brand'";
                }
                if ($item_warranty != "") {
                    $item_ext_value[] = "WarrantyExpiryDate = '$item_warranty'";
                    $arr_item_insert_field[] = "WarrantyExpiryDate";
                    $arr_item_insert_value[] = "'$item_warranty'";
                }
                if ($item_license != "") {
                    $item_ext_value[] = "SoftwareLicenseModel = '$item_license'";
                    $arr_item_insert_field[] = "SoftwareLicenseModel";
                    $arr_item_insert_value[] = "'$item_license'";
                }
                if ($item_serial != "") {
                    $item_ext_value[] = "SerialNumber = '$item_serial'";
                    $arr_item_insert_field[] = "SerialNumber";
                    $arr_item_insert_value[] = "'$item_serial'";
                }
                if ($item_quotation_no != "") {
                    $item_ext_value[] = "QuotationNo = '$item_quotation_no'";
                    $arr_item_insert_field[] = "QuotationNo";
                    $arr_item_insert_value[] = "'$item_quotation_no'";
                }
                if ($item_tender_no != "") {
                    $item_ext_value[] = "TenderNo = '$item_tender_no'";
                    $arr_item_insert_field[] = "TenderNo";
                    $arr_item_insert_value[] = "'$item_tender_no'";
                }
                if ($item_invoice_no != "") {
                    $item_ext_value[] = "InvoiceNo = '$item_invoice_no'";
                    $arr_item_insert_field[] = "InvoiceNo";
                    $arr_item_insert_value[] = "'$item_invoice_no'";
                }
                
                $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID = '".$existItemID."'";
                $arrExistItemExt = $linventory->returnVector($sql);
                
                if ($arrExistItemExt[0] > 0) {
                    // update the record in INVENTORY_ITEM_SINGLE_EXT
                    if (sizeof($item_ext_value) > 0) {
                        $ext_value = implode(",", $item_ext_value);
                        $sql = "UPDATE INVENTORY_ITEM_SINGLE_EXT SET $ext_value WHERE ItemID = '".$existItemID."'";
                        $result['UpdateInvItemExtInfo' . $i] = $linventory->db_db_query($sql);
                        // echo $sql;
                    }
                } else {
                    $insert_fields .= implode(",", $arr_item_insert_field);
                    if ($insert_fields != "") {
                        $insert_fields .= ",ItemID";
                    }
                    $insert_values = implode(",", $arr_item_insert_value);
                    if ($insert_values != "") {
                        $insert_values .= ",'$existItemID'";
                    }
                    $sql = "INSERT INTO INVENTORY_ITEM_SINGLE_EXT ($insert_fields) VALUES ($insert_values)";
                    $result['UpdateInvItemExtInfo' . $i] = $linventory->db_db_query($sql);
                }
                
                $curr_date = date("Y-m-d");
                // ## Insert a update item info record into INVENTORY_ITEM_SINGLE_STATUS_LOG ###
                $sql = "INSERT INTO
								INVENTORY_ITEM_SINGLE_STATUS_LOG
								(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
								($existItemID, '$curr_date', " . ITEM_ACTION_EDIT_ITEM . ", $UserID, '', 0, 0, NOW(), NOW())
						";
                $result['InsertUpdateItemInfoLog' . $i] = $linventory->db_db_query($sql);
            }
        }
    }
} else {
    // ############
    // bulk item #
    // ############
    
    $curr_date = date("Y-m-d");
    
    $sql = "SELECT ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, Barcode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode, VarianceManager, Quantity, ExistItem, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, Remarks, StockTakeOption, Tags FROM TEMP_INVENTORY_ITEM";
    
    $arr_result = $linventory->returnArray($sql, 12);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_type, $category_id, $category2_id, $item_categoryCode, $item_category2Code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_code, $item_barcode, $item_ownership, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_variance_manager, $item_qty, $item_existed, $item_supplier_name, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchased_price, $item_unit_price, $item_maintain_info, $item_remark, $stocktakeoption, $tags) = $arr_result[$i];
            $item_variance_manager = $item_variance_manager ? $item_variance_manager : $item_admin_group_code;
            
            unset($item_value);
            unset($item_ext_value);
            
            $sql = "SELECT BuildingID FROM INVENTORY_LOCATION_BUILDING WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_buildingCode) . "' AND RecordStatus = 1";
            $BuildingID = $linventory->returnArray($sql, 1);
            
            $sql = "SELECT LocationLevelID FROM INVENTORY_LOCATION_LEVEL WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "' AND BuildingID = '" . $BuildingID[0]['BuildingID'] . "' AND RecordStatus = 1";
            $LocationLevelID = $linventory->returnArray($sql, 1);
            
            $sql = "SELECT LocationID from INVENTORY_LOCATION where Code = '" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "' AND LocationLevelID = '" . $LocationLevelID[0]['LocationLevelID'] . "' AND RecordStatus = 1";
            $arr_location_id = $linventory->returnVector($sql);
            if (sizeof($arr_location_id) > 0) {
                $LocationID = $arr_location_id[0];
            }
            
            $sql = "SELECT FundingSourceID from INVENTORY_FUNDING_SOURCE where Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode) . "'";
            $arr_funding_source = $linventory->returnVector($sql);
            if (sizeof($arr_funding_source) > 0) {
                $FundingSourceID = $arr_funding_source[0];
            }
            
            if ($item_admin_group_code != "") {
                $sql = "SELECT AdminGroupID from INVENTORY_ADMIN_GROUP where Code = '" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "'";
                $arr_admin_group_id = $linventory->returnVector($sql);
            } else {
                $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION l INNER JOIN INVENTORY_ITEM i ON (i.ItemID=l.ItemID AND i.ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "') INNER JOIN INVENTORY_LOCATION il ON (il.LocationID=l.LocationID AND il.Code='" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "') AND FundingSourceID='$FundingSourceID'";
                // echo $sql;
                $arr_admin_group_id = $linventory->returnVector($sql);
                // debug_pr($arr_admin_group_id);
            }
            
            if (sizeof($arr_admin_group_id) > 0) {
                $AdminGroupID = $arr_admin_group_id[0];
            }
            
            if ($item_existed == 1) // update bulk item
{
                $sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '" . $linventory->Get_Safe_Sql_Query($item_code) . "'";
                $arr_item_id = $linventory->returnArray($sql, 1);
                
                if (sizeof($arr_item_id) > 0) {
                    list ($item_id) = $arr_item_id[0];
                }
                $linventory->saveEntryTag($item_id, $tags);
                
                $item_barcode = $linventory->Get_Safe_Sql_Query($item_barcode);
                $item_chi_name = addslashes($item_chi_name);
                $item_eng_name = addslashes($item_eng_name);
                $item_chi_description = addslashes($item_chi_description);
                $item_eng_description = addslashes($item_eng_description);
                $item_remark = addslashes($item_remark);
                $item_supplier_name = addslashes($item_supplier_name);
                $item_supplier_contact = addslashes($item_supplier_contact);
                $item_supplier_description = addslashes($item_supplier_description);
                $item_maintain_info = addslashes($item_maintain_info);
                // $item_brand = addslashes($item_brand);
                $item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
                $item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
                $item_code = $linventory->Get_Safe_Sql_Query($item_code);
                $item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
                $item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
                $item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
                $item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
                $item_fundingCode = $linventory->Get_Safe_Sql_Query($item_fundingCode);
                $item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
                $item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
                $item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
                
                if ($category_id != "" && $category_id != 0) {
                    $item_value[] = "CategoryID = '$category_id'";
                }
                if ($category2_id != "" && $category2_id != 0) {
                    $item_value[] = "Category2ID = '$category2_id'";
                }
                if ($item_chi_name != "") {
                    $item_value[] = "NameChi = '$item_chi_name'";
                }
                if ($item_eng_name != "") {
                    $item_value[] = "NameEng = '$item_eng_name'";
                }
                if ($item_chi_description != "") {
                    $item_value[] = "DescriptionChi = '$item_chi_description'";
                }
                if ($item_eng_description != "") {
                    $item_value[] = "DescriptionEng = '$item_eng_description'";
                }
                if ($item_ownership != "" && $item_ownership != 0) {
                    $item_value[] = "Ownership = '$item_ownership'";
                }
                $item_value[] = "DateModified = 'NOW()'";
                $item_value[] = "StockTakeOption = '$stocktakeoption'";
                
                if (sizeof($item_value) > 0) {
                    $value = implode(",", $item_value);
                    $sql = "UPDATE INVENTORY_ITEM SET $value WHERE ItemCode = '$item_code'";
                    $result['UpdateInvItem' . $i] = $linventory->db_db_query($sql);
                }
                
                $sql = "SELECT RecordID FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."' AND LocationID = '".$LocationID."' AND FundingSourceID='$FundingSourceID' AND GroupInCharge='$AdminGroupID'";
                $arr_exist_record_id = $linventory->returnVector($sql);
                
                if (sizeof($arr_exist_record_id) > 0) // update bulk item
{
                    // $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND LocationID = '$LocationID'";
                    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Code='" . (trim($item_admin_group_code)) . "'";
                    // echo '1) '.$sql.'<br>';
                    $arr_distination_admin_group_id = $linventory->returnVector($sql);
                    if (sizeof($arr_distination_admin_group_id) > 0) {
                        $distination_admin_group_id = $arr_distination_admin_group_id[0];
                    }
                    
                    // ## Get the destination Funding Source ID for the bulk item ###
                    // $sql = "SELECT FundingSource FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '$item_id' AND LocationID = '$LocationID' AND GroupInCharge = '$distination_admin_group_id' AND Action IN (".ITEM_ACTION_PURCHASE.",".ITEM_ACTION_CHANGELOCATION.") ORDER BY DateInput DESC LIMIT 0,1";
                    $sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE Code='" . trim($item_fundingCode) . "'";
                    // echo '2) '.$sql.'<br>';
                    $arr_destination_funding_source = $linventory->returnVector($sql);
                    if (sizeof($arr_destination_funding_source) > 0) {
                        $destination_funding_source = $arr_destination_funding_source[0];
                    }
                    
                    // #############################
                    // ### not allow update qty!!!!
                    // #############################
                    /*
                     * # check the INVENTORY_ITEM_BULK_LOCATION
                     * if($item_qty!=0) {
                     * $sql = "SELECT RecordID, Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND LocationID = '".$LocationID."' AND FundingSourceID='$FundingSourceID' AND GroupInCharge='$AdminGroupID'";
                     * //echo '3) '.$sql.'<br>';
                     * $arr_record_id = $linventory->returnArray($sql,2);
                     * if(sizeof($arr_record_id)>0)
                     * {
                     * list($record_id, $original_qty) = $arr_record_id[0];
                     * $sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION SET Quantity = '$item_qty', FundingSourceID='$FundingSourceID' WHERE RecordID = '$record_id'";
                     * //echo '4) '.$sql.'<br>';
                     * $result['UpdateInvItemBulkLocation'.$i] = $linventory->db_db_query($sql);
                     *
                     * }
                     * }
                     * $qty_different = $item_qty - $original_qty;
                     *
                     * if($qty_different!=0) {
                     * $action = ITEM_ACTION_UPDATE_LOCATION_ITEM_AMOUNT;
                     * $normal_item_qty = "NULL";
                     * } else {
                     * $action = ITEM_ACTION_EDIT_ITEM;
                     * //$normal_item_qty = $item_qty;
                     * $normal_item_qty = "NULL";
                     * }
                     * //echo $qty_different.'/';
                     * //echo 'Qty Different : '.$qty_different.'<br>';;
                     * if($qty_different!=0) {
                     *
                     * # insert log for qty change
                     * $sql = "INSERT INTO INVENTORY_ITEM_BULK_LOG
                     * (ItemID, RecordDate, Action, QtyChange, QtyNormal, PurchaseDate, PurchasedPrice, FundingSource,
                     * SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo,
                     * Remark, UnitPrice, MaintainInfo, LocationID, GroupInCharge, PersonInCharge,
                     * DateInput, DateModified)
                     * VALUES
                     * ('$item_id', '$curr_date', '$action', '$qty_different', $normal_item_qty, '$item_purchase_date', '$item_purchased_price', '$destination_funding_source',
                     * '$item_supplier_name', '$item_supplier_contact', '$item_supplier_description', '$item_invoice_no', '$item_quotation_no', '$item_tender_no',
                     * '$item_remark', '$item_unit_price', '$item_maintain_info', '$LocationID', '$distination_admin_group_id', '$UserID',
                     * NOW(), NOW())";
                     * $result['NewInvItemBulkLog'.$i] = $linventory->db_db_query($sql);
                     * //echo '5) '.$sql.'<br>';
                     *
                     * # Update Normal, Damage, Repair Qty in the location ###
                     * $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND LocationID = '$LocationID' AND GroupInCharge = '$AdminGroupID' AND FundingSourceID='$destination_funding_source'";
                     *
                     * $arr_tmp_result = $linventory->returnVector($sql);
                     * $original_location_qty = $arr_tmp_result[0];
                     *
                     * //$sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '$item_id' AND LocationID = '$LocationID' AND GroupInCharge = '$AdminGroupID' AND FundingSourceID = '$destination_funding_source' AND Action IN (".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_PURCHASE.") and QtyDamage is not null ORDER BY DateInput DESC , RecordID DESC LIMIT 1";
                     * $sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '$item_id' AND LocationID = '$LocationID' AND Action IN (".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_PURCHASE.") AND FundingSource='$destination_funding_source' AND GroupInCharge='$AdminGroupID' and QtyDamage is not null ORDER BY DateInput DESC , RecordID DESC LIMIT 1";
                     *
                     * $arr_tmp_status = $linventory->returnArray($sql,3);
                     * if(sizeof($arr_tmp_status)>0)
                     * {
                     * list($normal_qty, $damage_qty, $repair_qty) = $arr_tmp_status[0];
                     * ## normal qty = retrieve location qty - damage - repair
                     * $normal_qty = $original_location_qty - $damage_qty - $repair_qty;
                     * }
                     * else
                     * {
                     * $normal_qty = $original_location_qty;
                     * $damage_qty = 0;
                     * $repair_qty = 0;
                     * }
                     * $sql = "INSERT INTO
                     * INVENTORY_ITEM_BULK_LOG
                     * (ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, FundingSource, PersonInCharge, DateInput, DateModified)
                     * VALUES
                     * ('$item_id', CURDATE(), ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty','$damage_qty','$repair_qty','$LocationID','$AdminGroupID','$FundingSourceID', '$UserID',NOW(),NOW())
                     * ";
                     * //echo '6) '.$sql.'<br>';
                     * $linventory->db_db_query($sql) or die(mysql_error());
                     * }
                     */
                    
                    if ($item_variance_manager != "") {
                        $sql = "select AdminGroupID from INVENTORY_ADMIN_GROUP where Code = '$item_variance_manager'";
                        $VarianceManagerID = $linventory->returnVector($sql);
                        // echo $sql.'<br>';
                        if (sizeof($VarianceManagerID) > 0) {
                            $item_variance_manager = $VarianceManagerID[0];
                            $additionSql = "BulkItemAdmin = '$item_variance_manager'";
                        }
                    }
                    
                    $additionBarcodeSql = "";
                    if ($item_barcode != "") {
                        $additionBarcodeSql = ($additionSql == "" ? "" : ",") . "Barcode='$item_barcode' ";
                    }
                    
                    // update INVENTORY_ITEM_BULK_EXT
                    // $sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET Quantity = Quantity + $qty_different $additionSql $additionBarcodeSql WHERE ItemID = $item_id";
                    $sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET $additionSql $additionBarcodeSql WHERE ItemID = $item_id";
                    $result['UpdateInvItemBulkExt' . $i] = $linventory->db_db_query($sql);
                    // echo $sql.'<br>';
                } else {
                    // nothing to do
                }
                // echo '<br>';
            } else {
                // new bulk item
                $item_barcode = $linventory->Get_Safe_Sql_Query($item_barcode);
                $item_chi_name = addslashes($item_chi_name);
                $item_eng_name = addslashes($item_eng_name);
                $item_chi_description = addslashes($item_chi_description);
                $item_eng_description = addslashes($item_eng_description);
                $item_remark = addslashes($item_remark);
                $item_supplier_name = addslashes($item_supplier_name);
                $item_supplier_contact = addslashes($item_supplier_contact);
                $item_supplier_description = addslashes($item_supplier_description);
                $item_maintain_info = addslashes($item_maintain_info);
                $item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
                $item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
                $item_code = $linventory->Get_Safe_Sql_Query($item_code);
                $item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
                $item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
                $item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
                $item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
                $FundingSourceID = $linventory->Get_Safe_Sql_Query($FundingSourceID);
                $item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
                $item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
                $item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
                
                $successItemCodeGenerate = ($item_code == "") ? false : true;
                while (! $successItemCodeGenerate) {
                    $item_code = generateItemCode($item_categoryCode, $item_category2Code, $item_purchase_date, $item_admin_group_code, $item_locationCode, $item_fundingCode, $existItemCategory[$item_categoryCode . $item_category2Code]);
                    if (! in_array($item_code, $CurrentItemCodeAry)) {
                        $CurrentItemCodeAry[] = $item_code;
                        $successItemCodeGenerate = true;
                        $existItemCategory[$item_categoryCode . $item_category2Code] = 0;
                    } else {
                        $existItemCategory[$item_categoryCode . $item_category2Code] += 1;
                    }
                }
                // check whether the item already exists or not
                $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "'";
                $count = $linventory->returnVector($sql);
                $insertItem = ($count[0] == 0) ? 1 : 0;
                
                if ($insertItem) {
                    $sql = "INSERT INTO INVENTORY_ITEM
										(ItemType, CategoryID, Category2ID, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, Ownership, PhotoLink, RecordType, RecordStatus, StockTakeOption, DateInput, DateModified, CreatedBy)
							VALUES
										(2,'$category_id','$category2_id','$item_chi_name','$item_eng_name','$item_chi_description','$item_eng_description','$item_code','$item_ownership','','',1,'$stocktakeoption',NOW(),NOW(), '$UserID')";
                    $result['NewInvItem' . $i] = $linventory->db_db_query($sql);
                }
                
                $sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$item_code'";
                $arr_item_id = $linventory->returnArray($sql, 1);
                if (sizeof($arr_item_id) > 0) {
                    list ($item_id) = $arr_item_id[0];
                }
                
                $linventory->saveEntryTag($item_id, $tags);
                
                // sum up the amount of item if any existing itme in location
                // $sql = "SELECT Quantity as Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID='$item_id' AND LocationID='$LocationID' AND FundingSourceID='$FundingSourceID' AND GroupInCharge='$AdminGroupID' ORDER BY RecordID DESC LIMIT 1";
                $sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID='$item_id' AND LocationID='$LocationID' AND FundingSource='$FundingSourceID' AND GroupInCharge='$AdminGroupID' AND Action IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ") ORDER BY RecordID desc LIMIT 1";
                $tempResult = $linventory->returnArray($sql);
                
                // if already have item in same location, insert 1 more log for updated QTY
                if (count($tempResult) > 0) {
                    // $sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID='$item_id' AND LocationID='$LocationID' AND Action IN (".ITEM_ACTION_PURCHASE.",".ITEM_ACTION_UPDATESTATUS.") ORDER BY RecordID desc LIMIT 1";
                    // $tempResult = $linventory->returnArray($sql);
                    $thisNormalQty = $tempResult[0]['QtyNormal'] + $item_qty;
                    $thisDamageQty = ($tempResult[0]['QtyDamage'] > 0) ? $tempResult[0]['QtyDamage'] : "NULL";
                    $thisRepairQty = ($tempResult[0]['QtyRepair'] > 0) ? $tempResult[0]['QtyRepair'] : "NULL";
                } else {
                    $thisNormalQty = $item_qty;
                    $thisDamageQty = "NULL";
                    $thisRepairQty = "NULL";
                }
                
                $sql = "INSERT INTO INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, QtyNormal, PurchaseDate, PurchasedPrice, FundingSource, SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, Remark, UnitPrice, MaintainInfo, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
						VALUES
							($item_id, '$curr_date', " . ITEM_ACTION_PURCHASE . ", '$item_qty', '$item_qty', '$item_purchase_date', '$item_purchased_price', '$FundingSourceID', '$item_supplier_name', '$item_supplier_contact', '$item_supplier_description', '$item_invoice_no', '$item_quotation_no', '$item_tender_no', '$item_remark', '$item_unit_price', '$item_maintain_info', '$LocationID', '$AdminGroupID', '$UserID', NOW(), NOW())";
                $result['NewInvItemBulkLog' . $i] = $linventory->db_db_query($sql);
                // echo $sql.'<br>';
                
                if (count($tempResult) > 0) {
                    // $thisNormalQty = $tempResult[0]['QtyNormal'] + $item_qty;
                    // $thisDamageQty = ($tempResult[0]['QtyDamage']>0) ? $tempResult[0]['QtyDamage']: "NULL";
                    // $thisRepairQty = ($tempResult[0]['QtyRepair']>0) ? $tempResult[0]['QtyRepair']: "NULL";
                    // $thisStockCheckQty = ($tempResult[0]['StockCheckQty']>0) ? $tempResult[0]['StockCheckQty']: "NULL";
                    
                    $sql = "INSERT INTO INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, QtyNormal, QtyDamage, QtyRepair, PurchaseDate, PurchasedPrice, SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, Remark, UnitPrice, MaintainInfo, LocationID, GroupInCharge, PersonInCharge, FundingSource, DateInput, DateModified)
						VALUES
							($item_id, '$curr_date', " . ITEM_ACTION_UPDATESTATUS . ", '$item_qty', '$thisNormalQty', $thisDamageQty, $thisRepairQty, '$item_purchase_date', '$item_purchased_price', '$item_supplier_name', '$item_supplier_contact', '$item_supplier_description', '$item_invoice_no', '$item_quotation_no', '$item_tender_no', '$item_remark', '$item_unit_price', '$item_maintain_info', '$LocationID', '$AdminGroupID', '$UserID', '$FundingSourceID', NOW(), NOW())";
                    $result['NewInvItemBulkLog' . $i] = $linventory->db_db_query($sql);
                }
                
                // check the INVENTORY_ITEM_BULK_LOCATION
                $sql = "SELECT RecordID FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND GroupInCharge = '$AdminGroupID' AND LocationID = '$LocationID' AND FundingSourceID='$FundingSourceID' AND GroupInCharge='$AdminGroupID'";
                $arr_record_id = $linventory->returnArray($sql, 1);
                
                // insert if no existing record
                if (sizeof($arr_record_id) == 0) {
                    $sql = "INSERT INTO INVENTORY_ITEM_BULK_LOCATION
									(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
							VALUES
									('$item_id', '$AdminGroupID', '$LocationID', '$FundingSourceID', '$item_qty')";
                    $result['NewInvItemBulkLocation' . $i] = $linventory->db_db_query($sql);
                    // echo $sql.'<br>';
                } else {
                    // update quantity if any existing item for same location in same GroupInCharge and FundingSourceID
                    $sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION SET Quantity = Quantity + $item_qty WHERE ItemID='$item_id' AND LocationID='$LocationID' AND GroupInCharge='$AdminGroupID' AND FundingSourceID='$FundingSourceID'";
                    $linventory->db_db_query($sql);
                }
                
                if ($item_variance_manager != "") {
                    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Code = '$item_variance_manager'";
                    $VarianceManagerID = $linventory->returnVector($sql);
                    
                    if (sizeof($VarianceManagerID) > 0) {
                        $item_variance_manager = $VarianceManagerID[0];
                    }
                }
                
                if ($insertItem) {
                    // auto generate barcode if no barcode provided
                    if ($item_barcode == "") {
                        $item_barcode = $linventory->Get_Safe_Sql_Query(implode("", $linventory->getUniqueBarcode(1)));
                    }
                    // INVENTORY_ITEM_BULK_EXT
                    $sql = "INSERT INTO INVENTORY_ITEM_BULK_EXT 
									(ItemID, Quantity, BulkItemAdmin, Barcode)
							VALUES
									('$item_id','$item_qty','$item_variance_manager','$item_barcode')";
                    $result['NewInvItemBulkExt' . $i] = $linventory->db_db_query($sql);
                    // echo $sql.'<br>';
                } else {
                    // update new quantity
                    $sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET Quantity=Quantity+'$item_qty' WHERE ItemID='$item_id'";
                    $linventory->db_db_query($sql);
                }
            }
        }
    }
}
/*
 * if (in_array(false,$result)) {
 * header("location: import_item.php?msg=12");
 * }
 * else {
 * header("location: import_item.php?msg=1");
 * }
 */
header("location: import_item.php?msg=1");
intranet_closedb();

function generateItemCode($CatCode, $SubCatCode, $PurchaseDate, $GroupCode, $SubLocationCode, $FundingCode, $existPos = "")
{
    $linventory = new libinventory();
    global $sys_custom, $PATH_WRT_ROOT;
    
    if ($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH']) {
        // #########################################################
        // CatholicEducation_eInventory
        // var itemCode = groupCode + category + sub-category + 5 digits number
        // #########################################################
        if ($sys_custom['CatholicEducation_eInventory']) {
            $itemCode_Prefix = $GroupCode . "-" . $CatCode . "-" . $SubCatCode . "-";
            $deli = "-";
        } else 
            if ($sys_custom['eInventoryCustForSKH']) {
                include_once ($PATH_WRT_ROOT . "includes/libinventory_cust.php");
                $linventory_cust = new libinventory_cust();
                $purhcase_date_format = $linventory_cust->returnYearFormat($PurchaseDate);
                
                // #########################################################
                // eInventoryCustForSKH
                // var itemCode = PurchaseDateFormat / groupCode / category / sub-category / 5 digits number
                // #########################################################
                $itemCode_Prefix = $purhcase_date_format . "/" . $GroupCode . "/" . $CatCode . "/" . $SubCatCode . "/";
                $deli = "/";
            }
        
        $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($CatCode) . "'";
        $arrCatID = $linventory->returnVector($sql);
        $CatID = $arrCatID[0];
        
        $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $CatID AND Code = '" . $linventory->Get_Safe_Sql_Query($SubCatCode) . "'";
        $arrSubCatID = $linventory->returnVector($sql);
        $SubCatID = $arrSubCatID[0];
        
        // $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = $CatID AND Category2ID = $SubCatID AND ItemCode LIKE '".$itemCode_Prefix."%'";
        $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE ItemCode LIKE '" . $itemCode_Prefix . "%'";
        $arrLastItemCode = $linventory->returnVector($sql);
        if (sizeof($arrLastItemCode) > 0) {
            $LastItemCode = $arrLastItemCode[0];
            $tmp_str_pos = strrpos($LastItemCode, $deli);
            $postfix = substr($LastItemCode, $tmp_str_pos + 1, strlen($LastItemCode));
            
            // $postfix = $postfix+1;
            if ($existPos != "" && $existPos != 0) {
                $postfix += $existPos;
            } else {
                $postfix = $postfix + 1;
            }
            $length = strlen($postfix);
            
            if ($length < 5) {
                for ($j = 0; $j < (5 - $length); $j ++) {
                    $default_postfix = "0";
                    $ItemCodepostfix .= $default_postfix;
                }
                $TmpItemCode = $ItemCodepostfix . $postfix;
                $new_item_code = $itemCode_Prefix . $TmpItemCode;
            } else {
                $new_item_code = $itemCode_Prefix . $postfix;
            }
        } else {
            $new_item_code = $itemCode_Prefix . "00001";
        }
    } else {
        if ($sys_custom['eInventory_ItemCodeFormat_New']) {
            $PurchaseYear = substr($PurchaseDate, 0, 4);
            $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
            
            if (sizeof($arr_itemCodeFormat) > 0) {
                for ($i = 0; $i < sizeof($arr_itemCodeFormat); $i ++) {
                    if ($arr_itemCodeFormat[$i] == 1) {
                        $itemCodeFormat_Year = 1;
                    }
                    if ($arr_itemCodeFormat[$i] == 2) {
                        $itemCodeFormat_Group = 1;
                    }
                    if ($arr_itemCodeFormat[$i] == 3) {
                        $itemCodeFormat_Funding = 1;
                    }
                    if ($arr_itemCodeFormat[$i] == 4) {
                        $itemCodeFormat_Location = 1;
                    }
                }
            }
            
            if ($itemCodeFormat_Year == 1)
                $itemCode_Prefix .= $PurchaseYear;
            if ($itemCodeFormat_Group == 1)
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $GroupCode;
            if ($itemCodeFormat_Funding == 1)
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $FundingCode;
            if ($itemCodeFormat_Location == 1)
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $SubLocationCode;
            
        }else if($sys_custom['eInventory_SysPpt']){
            $PurchaseYear = substr($PurchaseDate,0,4);
            $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
            
            if(sizeof($arr_itemCodeFormat)>0){
                for($i=0; $i<sizeof($arr_itemCodeFormat); $i++){
                    if($arr_itemCodeFormat[$i] == 'PurchaseYear'){
                        $itemCodeFormat_Year = 1;
                    }
                    if($arr_itemCodeFormat[$i] == 'CategoryCode'){
                        $itemCodeFormat_Cat = 1;
                    }
                    if($arr_itemCodeFormat[$i] == 'SubCategoryCode'){
                        $itemCodeFormat_Cat2 = 1;
                    }
                    if($arr_itemCodeFormat[$i] == 'ResourceManagementGroupCode'){
                        $itemCodeFormat_Group = 1;
                    }
                    if($arr_itemCodeFormat[$i] == 'FundingSource'){
                        $itemCodeFormat_Funding = 1;
                    }
//                     if($arr_itemCodeFormat[$i] == 'FundingSource2'){
//                         $itemCodeFormat_Funding2 = 1;
//                     }
                    if($arr_itemCodeFormat[$i] == 'LocationCode'){
                        $itemCodeFormat_Location = 1;
                    }
                }
            }
            
            $ItemCodeSeparator = $linventory->getItemCodeSeparator();
            $separator = "";
            if ($ItemCodeSeparator == 0) {
                $separator = '';
            } else if ($ItemCodeSeparator == 1) {
                $separator = '_';
            } else if ($ItemCodeSeparator == 2) {
                $separator = '/';
            } else if ($ItemCodeSeparator == 3) {
                $separator = '-';
            }
            
            if($itemCodeFormat_Year == 1){
                if ($PurchaseYear != ''){
                    $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $PurchaseYear. $separator;
                }
            }
            if($itemCodeFormat_Cat == 1){
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $CatCode. $separator;
            }
            if($itemCodeFormat_Cat2 == 1){
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $SubCatCode. $separator;
            }
            if($itemCodeFormat_Group == 1){
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $GroupCode. $separator;
            }
            if($itemCodeFormat_Funding == 1){
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $FundingCode. $separator;
            }
//          if($itemCodeFormat_Funding2 == 1){
//              if($Funding2Code != ''){
//                 $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $Funding2Code;
//              }
//          }
            if($itemCodeFormat_Location == 1){
                $itemCode_Prefix .= ($itemCode_Prefix ? $sys_custom['eInventory']['ItemCodeDelimiter'] : "") . $SubLocationCode. $separator;
            }
            
        }else {
            $itemCode_Prefix = $CatCode . $SubCatCode;
            if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
                list ($y, $m, $d) = explode("-", date('Y-m-d'));
                $item_year = $m . $d >= "0901" ? substr($y, 2, 2) : substr($y - 1, 2, 2);
                $itemCode_Prefix = "SPCCPS" . $CatCode . $SubCatCode . $item_year;
            }
        }
        
        if ($sys_custom['eInventory']['ItemCodeNumberChecking'] == "ItemCodeCombination") {
            // search in INVENTORY_ITEM for max counting number
            $sql = "select max(ItemCode) from INVENTORY_ITEM where ItemCode like '" . $itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'] . "%'";
            $DB_ItemCode = $linventory->returnVector($sql);
            $maxNum = str_replace($itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'], "", $DB_ItemCode[0]);
        } else {
            $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($CatCode) . "'";
            $arrCatID = $linventory->returnVector($sql);
            $CatID = $arrCatID[0];
            
            $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $CatID AND Code = '" . $linventory->Get_Safe_Sql_Query($SubCatCode) . "'";
            $arrSubCatID = $linventory->returnVector($sql);
            $SubCatID = $arrSubCatID[0];
            
            // set the item code prefix
            if ($sys_custom['eInventory_ItemCodeNoUnderscore'] || $sys_custom['eInventory']['ItemCodeDelimiter']) {
                $prefixItemCode = $CatCode . $SubCatCode;
            } else 
                if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
                    list ($y, $m, $d) = explode("-", date('Y-m-d'));
                    $item_year = $m . $d >= "0901" ? substr($y, 2, 2) : substr($y - 1, 2, 2);
                    $prefixItemCode = "SPCCPS" . $CatCode . $SubCatCode . $item_year . "-";
                } else {
                    $prefixItemCode = $CatCode . $SubCatCode . "_";
                }
            
            $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE ItemCode LIKE '$prefixItemCode%' "; // ## new method - get later item code with base on the prefix
            $DB_ItemCode = $linventory->returnVector($sql, 1);
            
            // case 1: with delimiter
            if ($sys_custom['eInventory']['ItemCodeDelimiter']) {
                // $sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE CategoryID = $CatID AND Category2ID = $SubCatID order by ItemID desc limit 1";
                // $DB_ItemCode = $linventory->returnVector($sql);
                $last_pos = strrpos($DB_ItemCode[0], $sys_custom['eInventory']['ItemCodeDelimiter']);
                $maxNum = substr($DB_ItemCode[0], $last_pos + 1, strlen($DB_ItemCode[0]));
            } else // case 2: without delimiter
{
                /*
                 * $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID = $CatID AND Category2ID = $SubCatID";
                 * $arr_result = $linventory->returnVector($sql);
                 * $maxNum = $arr_result[0];
                 */
                if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore'])
                    $start_pos = strlen($prefixItemCode);
                else 
                    if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
                        $start_pos = strrpos($DB_ItemCode[0], "-");
                    } else {
                        // $start_pos = strpos($DB_ItemCode[0],"_");
                        $start_pos = strrpos($DB_ItemCode[0], "_");
                    }
                
                $num_ItemCode = substr($DB_ItemCode[0], $start_pos + 1, strlen($DB_ItemCode[0]));
                $maxNum = $num_ItemCode + 0;
            }
        }
        $cur_num = $maxNum + 1;
        
        if ($sys_custom['eInventory_ItemCodeFormat_New']) {
            // add zero pad
            if ($sys_custom['eInventory']['ItemCodeNumberDigit'])
                $cur_num = str_pad($cur_num, $sys_custom['eInventory']['ItemCodeNumberDigit'], "0", STR_PAD_LEFT);
            
            $new_item_code = $itemCode_Prefix . $sys_custom['eInventory']['ItemCodeDelimiter'] . $cur_num;
            
        } elseif($sys_custom['eInventory_SysPpt']){
            $numOfDigit = $linventory->getItemCodeDigitLength();
            $numOfZero = "";
            $count = $numOfDigit - strlen((string) $cur_num);
            for ($i = 0; $i < $count; $i ++) {
                $numOfZero .= (string) 0;
            }
            
            $new_item_code = $itemCode_Prefix . $numOfZero . $cur_num;
        } else { // default

            // set item code num length
            if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
                $itemCodeNumLength = 3;
            } else {
                $itemCodeNumLength = DEFAULT_ITEM_CODE_NUM_LENGTH;
            }
            
            // add zero pad
            $max_zero_pad = $sys_custom['eInventory']['ItemCodeNumberDigit'] > $itemCodeNumLength ? $sys_custom['eInventory']['ItemCodeNumberDigit'] : $itemCodeNumLength;
            $cur_num = str_pad($cur_num, $max_zero_pad, "0", STR_PAD_LEFT);
            
            $new_item_code = $itemCode_Prefix . ($sys_custom['eInventory_ItemCodeNoUnderscore'] ? "" : ($sys_custom['eInventory_ItemCodeFormat']['SPCCPS'] ? "-" : "_")) . $cur_num;
        }
        
        /*
         * $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = $CatID AND Category2ID = $SubCatID AND ItemCode LIKE '".$linventory->Get_Safe_Sql_Query($itemCode_Prefix)."%'";
         * $arrLastItemCode = $linventory->returnVector($sql);
         *
         * if(sizeof($arrLastItemCode)>0)
         * {
         * $LastItemCode = $arrLastItemCode[0];
         * $tmp_str_pos = strrpos($LastItemCode,"_");
         * $postfix = substr($LastItemCode,$tmp_str_pos+1,strlen($LastItemCode));
         *
         * if($existPos!="" && $existPos!=0) {
         * $postfix += $existPos;
         * }else {
         * $postfix = $postfix+1;
         * }
         * $length = strlen($postfix);
         *
         * if($sys_custom['eInventory_ItemCodeFormat_New'])
         * {
         * $new_item_code = $itemCode_Prefix.$postfix;
         * }else{
         * if($length < DEFAULT_ITEM_CODE_NUM_LENGTH)
         * {
         * for($j=0; $j<(DEFAULT_ITEM_CODE_NUM_LENGTH - $length); $j++)
         * {
         * $default_postfix = "0";
         * $ItemCodepostfix .= $default_postfix;
         * }
         * $TmpItemCode = $ItemCodepostfix.$postfix;
         * $new_item_code = $itemCode_Prefix."_".$TmpItemCode;
         * //array_push($arr_final_item_code,$new_item_code);
         * }else{
         * $new_item_code = $itemCode_Prefix."_".$postfix;
         * }
         * }
         * }
         */
    }
    return $new_item_code;
}
?>