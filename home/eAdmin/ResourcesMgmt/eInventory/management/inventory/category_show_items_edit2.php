<?php

// Using:

/**
 * **************************** Change Log [Start] *******************************
 * 2019-05-13 [Henry] Security fix: SQL without quote
 * 2018-02-07 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log [End] ********************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);
// $TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/location.php", 0);
// $TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/group.php", 0);
// $TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/fundingsource.php", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// show current title #
$temp[] = array(
    $button_edit,
    ""
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";
$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION2($i_InventorySystem_ItemDetails) . "</td></tr>";

$no_file = 5;

if ($item_type == 1) {
    $sql = "SELECT 
					PurchaseDate,
					PurchasedPrice,
					SupplierName,
					SupplierContact,
					SupplierDescription,
					MaintainInfo, 
					InvoiceNo,
					QuotationNo,
					TenderNo,
					TagCode,
					Brand,
					GroupInCharge,
					LocationID,
					FundingSource,
					WarrantyExpiryDate,
					SoftwareLicenseModel
			FROM
					INVENTORY_ITEM_SINGLE_EXT
			WHERE
					ItemID = '".$item_id."'";
    
    $arr_result = $linventory->returnArray($sql, 15);
    if (sizeof($arr_result) > 0) {
        list ($purchase_date, $purchase_price, $supplier_name, $supplier_contact, $supplier_desc, $item_maintain_info, $invoice_no, $quotation_no, $tender_no, $barcode, $brand_name, $targetGroup, $targetLocation, $targetFundingSource, $warranty_expiry, $license) = $arr_result[0];
    }
    
    // ## Get the group for the inventory ###
    $namefield = $linventory->getInventoryItemNameByLang();
    $sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP";
    $group_result = $linventory->returnArray($sql, 2);
    $group_selection = getSelectByArray($group_result, "name=targetGroup", $targetGroup, 0, 1);
    
    // ## Get the Funding ###
    $namefield = $linventory->getInventoryItemNameByLang();
    $sql = "SELECT FundingSourceID, $namefield FROM INVENTORY_FUNDING_SOURCE";
    $funding_result = $linventory->returnArray($sql, 2);
    $funding_selection = getSelectByArray($funding_result, "name=targetFundingSource", $targetFundingSource, 0, 1);
    
    // ## Get the location ###
    $location_namefield = $linventory->getInventoryNameByLang();
    $sql = "SELECT LocationID, $location_namefield FROM INVENTORY_LOCATION ORDER BY LocationLevelID, LocationID";
    $location_result = $linventory->returnArray($sql, 2);
    $location_selection = getSelectByArray($location_result, "name=targetLocation", $targetLocation, 0, 1);
    
    $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY_LEVEL2 WHERE Category2ID = '".$targetCategory2."'";
    $arr_category = $linventory->returnArray($sql, 1);
    
    if (sizeof($arr_category) > 0) {
        list ($category_id) = $arr_category[0];
        $table_content .= "<input type=\"hidden\" name=\"targetCategory\" value=$category_id>";
    }
    
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_AssignTo <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$group_selection</td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Location <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$level_selection$location_selection</td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Funding<span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$funding_selection</td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Barcode</td><td class=\"tabletext\" valign=\"top\">$barcode</td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Photo</td><td class=\"tabletext\" valign=\"top\"><input type=\"file\" name=\"item_photo\" class=\"file\" $disable><input type=\"hidden\" name=\"hidden_item_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Attachment</td><td class=\"tabletext\" valign=\"top\">";
    for ($i = 0; $i < $no_file; $i ++) {
        $table_content .= "<input class=\"file\" type=\"file\" name=\"item_attachment_$i\" /><br />";
        $table_content .= "<input type=\"hidden\" name=\"hidden_item_attachment_$i\" />";
    }
    $table_content .= "</td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Price</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_purchase_price\" type=\"text\" class=\"textboxtext\" value=$purchase_price></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Purchase_Date</td><td class=\"tabletext\" valign=\"top\">" . $linterface->GET_DATE_FIELD("item_purchase_date", "form1", "item_purchase_date", ($purchase_date == "") ? date('Y-m-d') : $purchase_date) . "</td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Supplier_Name</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier\" type=\"text\" class=\"textboxtext\" value=$supplier_name></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Supplier_Contact</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier_contact\" type=\"text\" class=\"textboxtext\" value=$supplier_contact></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Supplier_Description</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier_description\" type=\"text\" class=\"textboxtext\" value=$supplier_desc></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystemItemMaintainInfo</td><td class=\"tabletext\" valign=\"top\"><textarea name=\"item_maintain_info\" class=\"textboxtext\">$item_maintain_info</textarea></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Invoice_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_invoice\" type=\"text\" class=\"textboxtext\" value=$invoice_no></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Quot_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_quotation\" type=\"text\" class=\"textboxtext\" value=$quotation_no></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Tender_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_tender\" type=\"text\" class=\"textboxtext\" value=$tender_no></td></tr>\n";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Brand_Name</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_brand\" type=\"text\" class=\"textboxtext\" value=$brand_name></td></tr>\n";
    
    $sql = "SELECT 
					HasSoftwareLicenseModel, 
					HasWarrantyExpiryDate 
			FROM 
					INVENTORY_CATEGORY_LEVEL2 
			WHERE 
					Category2ID = '".$targetCat2."';";
    
    $result = $linventory->returnArray($sql, 2);
    
    if ($result[0][HasSoftwareLicenseModel] == 1) {
        $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_License</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_license\" type=\"text\" class=\"textboxnum\" value=\"$license\"></td></tr>\n";
    } else {
        $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_License</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_license\" type=\"text\" class=\"textboxnumt\" style=\"background-color:#CCCCCC;\" DISABLED></td></tr>\n";
    }
    if ($result[0][HasWarrantyExpiryDate] == 1) {
        $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_WarrantyExpiryDate</td><td class=\"tabletext\" valign=\"top\">" . $linterface->GET_DATE_FIELD("item_warranty_expiry_date", "form1", "item_warranty_expiry_date", ($warranty_expiry == "") ? date('Y-m-d') : $warranty_expiry) . "</td></tr>\n";
    } else {
        $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_WarrantyExpiryDate</td><td class=\"tabletext\" valign=\"top\">" . $linterface->GET_DATE_FIELD("item_warranty_expiry_date", "form1", "item_warranty_expiry_date", 0, 1, "DISABLED") . "</td></tr>\n";
    }
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Remark</td><td><textarea name=\"item_remark\" class=\"textboxtext\"></textarea></td></tr>\n";
}
if ($item_type == 2) {
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_AssignTo <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$group_selection</td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Location <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$level_selection$location_selection</td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Funding<span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$funding_selection</td></tr>\n";
    // if($targetExistItem == "")
    // {
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Photo</td><td class=\"tabletext\" valign=\"top\"><input type=\"file\" name=\"item_photo\" class=\"file\" $disable><input type=\"hidden\" name=\"hidden_item_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>\n";
    // }
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Attachment</td><td class=\"tabletext\" valign=\"top\">";
    for ($i = 0; $i < $no_file; $i ++) {
        $table_content .= "<input class=\"file\" type=\"file\" name=\"item_attachment_$i\" /><br />";
        $table_content .= "<input type=\"hidden\" name=\"hidden_item_attachment_$i\" />";
    }
    $table_content .= "</td></tr>\n";
    
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Qty<span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\"><input type=\"text\" name=\"item_qty\" class=\"textboxtext\"></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Price</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_purchase_price\" type=\"text\" class=\"textboxtext\" value=$item_purchase_price></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Purchase_Date</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_purchase_date\" type=\"text\" class=\"textboxnum\" maxlength=\"10\" value=$item_purchase_date>&nbsp;<span class=\"tabletextremark\">(yyyy-mm-dd)</span></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Purchase_Date</td><td class=\"tabletext\" valign=\"top\">".$linterface->GET_DATE_FIELD("item_purchase_date", "form1", "item_purchase_date", date('Y-m-d'))."</td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Supplier_Name</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier\" type=\"text\" class=\"textboxtext\" value=$item_supplier></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Supplier_Contact</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier_contact\" type=\"text\" class=\"textboxtext\" value=$item_supplier_contact></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Supplier_Description</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier_description\" type=\"text\" class=\"textboxtext\" value=$item_supplier_description></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Invoice_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_invoice\" type=\"text\" class=\"textboxtext\" value=$item_invoice></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Quot_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_quotation\" type=\"text\" class=\"textboxtext\" value=$item_quotation></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Tender_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_tender\" type=\"text\" class=\"textboxtext\" value=$item_tender></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Remark</td><td><textarea name=\"item_remark\" class=\"textboxtext\"></textarea></td></tr>\n";
}

if ($item_type != "") {
    $table_content .= "<tr><td colspan=\"2\" align=\"left\" class=\"tabletextremark\">&nbsp; &nbsp; &nbsp; $i_general_required_field2</td></tr>\n";
}

$table_content .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";
$table_content .= "<tr><td height=\"10px\"></td></tr>\n";
$table_content .= "<tr><td colspan=2 align=center>" . $linterface->GET_ACTION_BTN($button_submit, "submit") . " " . $linterface->GET_ACTION_BTN($button_reset, "reset", "", "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . " " . $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='items_full_list.php'", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>\n";

$table_content .= "<input type=\"hidden\" name=\"targetExistItem\" value=\"$targetExistItem\">\n";
$table_content .= "<input type=\"hidden\" name=\"purchaseType\" value=\"$purchaseType\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetItemType\" value=\"$item_type\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_id\" value=\"$item_id\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetCategory\" value=\"$item_cat\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetCategory2\" value=\"$targetCat2\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_chi_name\" value=\"$item_chi_name\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_eng_name\" value=\"$item_eng_name\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_chi_discription\" value=\"$item_chi_discription\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_eng_discription\" value=\"$item_eng_discription\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_code\" value=\"$item_code\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_ownership\" value=\"$item_ownership\">\n";

echo generateFileUploadNameHandler("form1", "item_photo", "hidden_item_photo", "item_attachment_0", "hidden_item_attachment_0", "item_attachment_1", "hidden_item_attachment_1", "item_attachment_2", "hidden_item_attachment_2", "item_attachment_3", "hidden_item_attachment_3", "item_attachment_4", "hidden_item_attachment_4");

?>
<script language="javascript">
<!--
function grapAttach(cform)
{
	var obj = document.form1.item_photo;	
	var key;
	var s="";
	key = obj.value;
	if (key!="")
		s += key;
	cform.attachStr.value = s;
}

function checkForm()
{
	var item_type = <?=$item_type?>;
	var checked = 0;
	var purchase_price = 0;
	var purchase_date = 0;
	var warranty_date = 0;
	
	if(item_type == 1)
	{
		if(check_select(document.form1.targetGroup,"<?=$i_InventorySystem_Input_Item_GroupSelection_Warning?>",0))
			if(check_select(document.form1.targetLocation,"<?=$i_InventorySystem_Input_Item_LocationSelection_Warning?>",0))
				if(check_select(document.form1.targetFundingSource,"<?=$i_InventorySystem_Input_Item_FundingSelection_Warning?>",0))
					checked = 1;
	}
	if(item_type == 1)
	{
		if(document.form1.item_purchase_price.value != "")
		{
			if(document.form1.item_purchase_price.value.indexOf(".") != -1)
			{
				var tmp_len = document.form1.item_purchase_price.value.length - document.form1.item_purchase_price.value.indexOf(".");
				if(tmp_len == 3)
				{
					if(check_positive_int(document.form1.item_purchase_price,"<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>",0,0))
						purchase_price = 1;
				}
				else
				{
					alert("<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>");
				}
			}
			else
			{
				if(check_positive_int(document.form1.item_purchase_price,"<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>",0,0))
					purchase_price = 1;
			}
		}
		else
		{
			purchase_price = 1;
		}
		
		if(document.form1.item_purchase_date.value != "")
		{
			if(check_date(document.form1.item_purchase_date,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning?>"))
				purchase_date = 1;
		}
		else
		{
			purchase_date = 1;
		}
		if(document.form1.item_warranty_expiry_date.value != "")
		{
			if(check_date(document.form1.item_warranty_expiry_date,"<?=$i_InventorySystem_Input_Item_WarrantyExpiryDate_Warning?>"))
				warranty_date = 1;
		}
		else
		{
			warranty_date = 1;
		}
	}
	
	if((item_type == 1) && (checked == 1) && (purchase_price == 1) && (purchase_date == 1) && (warranty_date == 1))
	{
		if(document.form1.flag.value == 1)
		{
			if(Big5FileUploadHandler())
			{
				document.form1.action = "category_show_items_edit_update.php";
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
	
	if(item_type == 2)
	{
		if(document.form1.flag.value == 1)
		{
			if(Big5FileUploadHandler())
			{
				document.form1.action = "category_show_items_edit_update.php";
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}
-->
</script>

<br>

<form name="form1" enctype="multipart/form-data" action="" method="POST"
	onSubmit="return checkForm()">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
	<?=$infobar?>
</table>

	<br>

	<table width="70%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content?>
</table>
	<input type="hidden" name="flag" value="1">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>