<?php
// modifying : Henry
/**
 * ******************************** Change Log ********************************************
 *  Date: 2019-05-13 (Henry)
 *  Security fix: SQL without quote
 * 
 * Date : 2018-07-06 (Isaac) 
 * fixed $item_unitprice2 values wrongly uses $item_unitprice1 [DM#3457]
 * Date : 2018-06-11 (Henry)
 * add remark for the information that will not update [Case#A138685]
 * Date : 2018-05-18 (Isaac)
 * added str_replace to $item_unitprice1 and $item_unitprice2 and $item_purchase_price to remove ',' from the price parameters. 
 * 
 * Date : 2018-02-21 (Henry)
 * - add access right checking [Case#E135442]
 * Date : 2016-04-07 (Henry)
 * modified checking the purchase date (SKH)
 * Date : 2016-02-04 (Henry)
 * php 5.4 issue move 'if($format == "" || $format == 2)' after includes file
 * Date : 2015-05-04 (Henry) [Case#L77666]
 * added checking $error[$i]["type"] before checking other error when insert items
 *
 * Date : 2014-12-23 (Henry) [Case#J73005]
 * $sys_custom['eInventory']['PurchaseDateRequired'] > Purchase date cannot be empty
 *
 * Date : 2014-04-03 (YatWoon)
 * enlarge the temp table's item code length from varchar(25) to varchar(100)
 * $sys_custom['eInventoryCustForSKH'] > Purchase date cannot be empty
 *
 * Date : 2013-03-22 (YatWoon)
 * support date format YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY [Case#2013-0319-0942-52158]
 * Date : 2013-02-27 (YatWoon)
 * change float to double(20,2) for TEMP TABLE [Case#2013-0226-1009-47073]
 * Date : 2012-12-18 (YatWoon)
 * display "-" for purchase date if "0000-00-00"
 * Date : 2012-12-12 (Yuen)
 * added Tag field
 * Date : 2012-11-28 (YatWoon)
 * add item_code is empty checking, if empty, no need check existing item's category and sub-category [Case#2012-1122-1509-00073]
 * Date : 2012-11-09 (Yuen)
 * added StocktakeOption field
 * Date : 2012-11-02 (YatWoon)
 * Fixed: incorrect date data checking for empty [Case#2012-1101-1617-28073]
 * Date : 2012-09-20 (YatWoon)
 * don't display / generate item code and barcode (if empty in csv) at the moment
 * Date : 2012-08-09 (YatWoon)
 * add "(",")" for Funding source and Unit Price [Case#2012-0806-1633-37132]
 * Date : 2012-06-10 (Henry Chow)
 * commented the checking on License, Warranty and SerialNumber according to category setting (they all are optional fields)
 * Date : 2012-06-27 (Henry Chow)
 * Detail : remove convert2unicode() when display the item name [CRM No. : 2012-0614-1112-57071]
 *
 * *****************************************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

// this page only handle import of single item
if ($format == "" || $format == 2) {
    header("Location: import_item.php");
    exit();
}

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;

$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();

$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeFormat = $linventory->getBarcodeFormat();

$filepath = $itemfile;
$filename = $itemfile_name;

$infobar .= "<tr><td colspan=\"2\" class=\"navigation_v30\">
				<a href=\"import_item.php\">" . $button_import . "</a>
				<span>" . $i_InventorySystem_ItemType_Single . " (" . ($actionType == 1 ? $Lang['eInventory']['Insert'] : $Lang['eInventory']['Update']) . ")</span>
			</td></tr>\n";

$file_format = array(
    "Item Code",
    "Barcode",
    "Item Chinese Name",
    "Item English Name",
    "Chinese Description",
    "English Description",
    "Category Code",
    "Sub-category Code",
    "Group Code",
    "Building Code",
    "Location Code",
    "Sub-location Code",
    "Funding Source Code (1)",
    "Unit Price (1)",
    "Funding Source Code (2)",
    "Unit Price (2)",
    "Ownership",
    "Warranty Expiry Date",
    "License",
    "Serial No",
    "Brand",
    "Supplier",
    "Supplier Contact",
    "Supplier Description",
    "Quotation No",
    "Tender No",
    "Invoice No",
    "Purchase Date",
    "Total Purchase Amount",
    "Maintanence Details",
    "Remarks",
    "Quantity",
    "Stocktake Option",
    "Tags"
);

// Create temp single item table
$sql = "DROP TABLE TEMP_INVENTORY_ITEM";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_ITEM
			(
			 ItemType int(11),
			 CategoryID int(11),
			 Category2ID int(11),
			 CategoryCode varchar(10),
			 Category2Code varchar(10),
			 NameChi varchar(255),
			 NameEng varchar(255),
			 DescriptionChi text,
			 DescriptionEng text,
			 ItemCode varchar(100),
			 TagCode varchar(100),
			 Ownership int(11),
			 GroupInChargeCode varchar(10),
			 BuildingCode varchar(10),
			 LocationLevelCode varchar(10),
			 LocationCode varchar(10),
			 FundingSourceCode1 varchar(10),
			 UnitPrice1 double(20,2),
			 FundingSourceCode2 varchar(10),
			 UnitPrice2 double(20,2),
			 WarrantyExpiryDate date,
			 SoftwareLicenseModel varchar(100),
			 SerialNumber varchar(100),
			 Brand varchar(100),
			 SupplierName varchar(255),
			 SupplierContact text,
			 SupplierDescription text,
			 QuotationNo varchar(255),
			 TenderNo varchar(255),
			 InvoiceNo varchar(255),
			 PurchaseDate date,
			 PurchasedPrice double(20,2),
			 UnitPrice double(20,2),
			 MaintainInfo text,
			 ItemRemark text,
			 ExistItem int(11),
			 Quantity int(11),
			 StockTakeOption varchar(16),
			 Tags text
		    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);

/*
 * # Enlarge item code size to varchar(100), same as INVENTORY_ITEM table
 * $sql = "alter table TEMP_INVENTORY_ITEM change ItemCode ItemCode varchar(100)";
 * $li->db_db_query($sql);
 */

$ext = strtoupper($lo->file_ext($filename));

if ($ext != ".CSV" && $ext != ".TXT") {
    header("location: import_item.php?msg=15");
    exit();
}

if ($limport->CHECK_FILE_EXT($filename)) {
    // read file into array
    // return 0 if fail, return csv array if success
    $data = $limport->GET_IMPORT_TXT($filepath);
    $col_name = array_shift($data); // drop the title bar
                                    
    // check the csv file's first row is correct or not
    $format_wrong = false;
    for ($i = 0; $i < sizeof($file_format); $i ++) {
        if ($col_name[$i] != $file_format[$i]) {
            $format_wrong = true;
            break;
        }
    }
    if ($format_wrong) {
        header("location: import_item.php?msg=15");
        exit();
    }
    
    // ## Remove Empty Row in CSV File ###
    for ($i = 0; $i < sizeof($data); $i ++) {
        if (sizeof($data[$i]) != 0) {
            $arr_new_data[] = $data[$i];
            // $record_row++;
        } else {
            // $empty_row++;
        }
    }
    
    $validRow = 0;
    $invalidRow = 0;
    
    $file_original_row = sizeof($data);
    $file_new_row = sizeof($arr_new_data);
    
    $arr_final_barcode = array();
    
    
    // Get existing single item details
    $sql = "SELECT ItemID, ItemCode	FROM INVENTORY_ITEM WHERE ItemType = '" . ITEM_TYPE_SINGLE . "'";
    
    $arr_exist_item = $linventory->returnArray($sql, 2);
    
    $newItem = array();
    $updateItem = array();
    $duplicateItem = array();
    
    // if(sizeof($arr_exist_item) >= 0)
    // {
    for ($i = 0; $i < sizeof($arr_exist_item); $i ++) {
        list ($exist_item_id, $exist_item_code) = $arr_exist_item[$i];
        $exist_item[$exist_item_code]['ItemID'] = $exist_item_id;
    }
    
    for ($i = 0; $i < sizeof($arr_new_data); $i ++) {
        list ($item_code, $item_tag_code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode1, $item_unitprice1, $item_fundingCode2, $item_unitprice2, $item_ownership, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_maintain_info, $item_remarks, $item_qty, $stocktakeoption, $tags) = $arr_new_data[$i];
        $item_unitprice1 = str_replace(',','',$item_unitprice1);
        $item_unitprice2 = str_replace(',','',$item_unitprice2);
        $item_purchase_price = str_replace(',','',$item_purchase_price);
        
        /*
         * // convert the date string in to date format of PHP
         * if(strpos($item_warranty,"/") > 0)
         * {
         * $WarrantyArray = Explode("/", $item_warranty);
         * $item_warranty = date("Y-m-d", mktime(0,0,0,$WarrantyArray[1],$WarrantyArray[0],$WarrantyArray[2]));
         * }
         *
         * if(strpos($item_purchase_date,"/") > 0)
         * {
         * $PurchaseDateArray = Explode("/", $item_purchase_date);
         * $item_purchase_date = date("Y-m-d", mktime(0,0,0,$PurchaseDateArray [1],$PurchaseDateArray [0],$PurchaseDateArray [2]));
         * }
         */
        // $arr_tmp_data[] = $data[$i];
        
        // check $actionType, 1 = insert; 2 = update
        
        if ($actionType == 2 && $exist_item[$item_code]['ItemID'] != "") {
            
            $updateItem[$i] = $data[$i];
            $validRow ++;
        } else 
            if ($actionType == 1 && $exist_item[$item_code]['ItemID'] == "") {
                $newItem[$i] = $data[$i];
                $validRow ++;
            } else {
                $duplicateItem[$i] = $data[$i];
                $error[$i]["type"] = ($item_code == "") ? 52 : 1;
                $invalidRow ++;
                $rowNo .= $delim . ($i + 1);
                $delim = ", ";
            }
    }
    
    // #####################################
    // UPDATE SINGLE ITEM
    // #####################################
    if ($actionType == 2 && sizeof($updateItem) > 0) // update single item
{
        // for($i=0; $i<sizeof($updateItem); $i++)
        foreach ($updateItem as $row => $key) {
            list ($item_code, $item_tag_code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode1, $item_unitprice1, $item_fundingCode2, $item_unitprice2, $item_ownership, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_maintain_info, $item_remarks, $item_qty, $stocktakeoption, $tags) = $updateItem[$row];
            
            $item_unitprice1 = str_replace(',','',$item_unitprice1);
            $item_unitprice2 = str_replace(',','',$item_unitprice2);
            $item_purchase_price = str_replace(',','',$item_purchase_price);
            $item_code = trim($item_code);
            $item_tag_code = trim($item_tag_code);
            $item_chi_name = trim($item_chi_name);
            $item_eng_name = trim($item_eng_name);
            $item_chi_description = trim($item_chi_description);
            $item_eng_description = trim($item_eng_description);
            $item_categoryCode = trim($item_categoryCode);
            $item_category2Code = trim($item_category2Code);
            $item_admin_group_code = trim($item_admin_group_code);
            $item_buildingCode = trim($item_buildingCode);
            $item_locationLevelCode = trim($item_locationLevelCode);
            $item_locationCode = trim($item_locationCode);
            $item_fundingCode1 = trim($item_fundingCode1);
            $item_unitprice1 = trim($item_unitprice1);
            $item_fundingCode2 = trim($item_fundingCode2);
            $item_unitprice2 = trim($item_unitprice2);
            $item_ownership = trim($item_ownership);
            $item_warranty = trim($item_warranty);
            $item_license = trim($item_license);
            $item_serial = trim($item_serial);
            $item_brand = trim($item_brand);
            $item_supplier = trim($item_supplier);
            $item_supplier_contact = trim($item_supplier_contact);
            $item_supplier_description = trim($item_supplier_description);
            $item_quotation_no = trim($item_quotation_no);
            $item_tender_no = trim($item_tender_no);
            $item_invoice_no = trim($item_invoice_no);
            $item_purchase_date = trim($item_purchase_date);
            $item_purchase_price = trim($item_purchase_price);
            $item_unit_price = trim($item_unit_price);
            $item_maintain_info = trim($item_maintain_info);
            $item_remarks = trim($item_remarks);
            $item_qty = trim($item_qty);
            if ($item_qty == "")
                $item_qty = 1;
                
                // ## Check Category Code ###
                // if(($item_categoryCode != "") && ($item_category2Code != ""))
                // {
            if ($item_categoryCode != "") {
                if ($item_category2Code != "") {
                    $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "' AND RecordStatus=1";
                    $arr_tmp_result = $linventory->returnVector($sql);
                    $cat_id = $arr_tmp_result[0];
                    
                    $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID='$cat_id' AND Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND RecordStatus=1";
                    $arr_tmp_result = $linventory->returnVector($sql);
                    $subcat_id = $arr_tmp_result[0];
                    
                    $sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND RecordStatus=1";
                    $arr_result = $linventory->returnVector($sql);
                    
                    $matched_row = $arr_result[0];
                    if ($matched_row == 0) {
                        $error[$row]['type'] = 30; // Invalid Category Code / Sub-category Code
                    } else {
                        // category is different from existing item
                        // $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID='$cat_id' AND Category2ID='$subcat_id' AND ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."'";
                        $sql = "SELECT CategoryID, Category2ID FROM INVENTORY_ITEM WHERE ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "'";
                        $tempResult = $linventory->returnArray($sql);
                        
                        if ($item_code != '' && $tempResult[0] != 0 && $tempResult[0]['CategoryID'] != $cat_id && $tempResult[0]['Category2ID'] != $subcat_id) {
                            $error[$row]['type'] = 40;
                        } else {
                            // check License, Warranty and SerialNumber according to category setting
                            /*
                             * $sql = "SELECT HasSoftwareLicenseModel, HasWarrantyExpiryDate, HasSerialNumber FROM INVENTORY_CATEGORY_LEVEL2 WHERE Category2ID = '$subcat_id' AND CategoryID = '$cat_id'";
                             * $tempresult = $linventory->returnArray($sql);
                             *
                             * list($_hasLicense, $_hasWarranty, $_hasSerialNumber) = $tempresult[0];
                             * if(!$_hasLicense && $item_license!="") {
                             * $error[$row]['type'] = 45;
                             * } else if($_hasLicense && $item_license=="") {
                             * $error[$row]['type'] = 48;
                             * } else if($_hasWarranty && $item_warranty=="") {
                             * $error[$row]['type'] = 49;
                             * } else if(!$_hasWarranty && $item_warranty!="") {
                             * $error[$row]['type'] = 46;
                             * } else if($_hasSerialNumber && $item_serial=="") {
                             * $error[$row]['type'] = 50;
                             * } else if(!$_hasSerialNumber && $item_serial!="") {
                             * $error[$row]['type'] = 47;
                             * }
                             */
                        }
                    }
                } else {
                    $error[$row]['type'] = 8; // Sub-category Code is empty
                }
            }
            /*
             * else
             * {
             * $error[$row]['type'] = 27; # Category Code is empty
             * }
             */
            // }
            
            /*
             * if($item_tag_code!="") {
             * $sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT e INNER JOIN INVENTORY_ITEM i ON (i.ItemID=e.ItemID AND i.ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."') AND e.TagCode='".$linventory->Get_Safe_Sql_Query($item_tag_code)."'";
             * $result = $linventory->returnVector($sql);
             * if(sizeof($result)==0) {
             * $error[$row]['type'] = 39; # Cannot update Barcode
             * }
             * }
             */
            
            if ($item_admin_group_code != "") {
                $sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "'";
                $arr_result = $linventory->returnArray($sql, 1);
                if (sizeof($arr_result) == 0) {
                    $error[$row]['type'] = 9;
                }
                if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-eInventory']) {
                    // check whether current user is the leader of group (only leader can insert record)
                    $sql = "SELECT * FROM INVENTORY_ADMIN_GROUP_MEMBER m INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=m.AdminGroupID AND g.Code='" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "') WHERE m.UserID='$UserID' AND m.RecordType=1";
                    $arr_result = $linventory->returnArray($sql);
                    
                    if (sizeof($arr_result) == 0) {
                        $error[$row]['type'] = 32;
                    }
                }
            } else {
                // $error[$row]['type'] = 10;
            }
            
            if (($item_buildingCode != "") && ($item_locationLevelCode != "") && ($item_locationCode != "")) {
                if ($item_buildingCode != "") {
                    if ($item_locationLevelCode != "") {
                        if ($item_locationCode != "") {
                            // $sql = "SELECT a.Code FROM INVENTORY_LOCATION_LEVEL AS a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) WHERE b.Code = '$item_locationCode'";
                            $sql = "SELECT 
											DISTINCT room.Code
										FROM 
											INVENTORY_LOCATION_BUILDING as building INNER JOIN 
											INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
											INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
										WHERE
											building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1 and 
											building.Code = '" . $linventory->Get_Safe_Sql_Query($item_buildingCode) . "' and floor.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "'and room.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "'";
                            $arr_result = $linventory->returnVector($sql);
                            if ($arr_result[0] != $item_locationCode) {
                                $error[$row]['type'] = 11;
                            }
                        } else {
                            $error[$row]['type'] = 28;
                        }
                    } else {
                        $error[$row]['type'] = 12;
                    }
                } else {
                    // # building code missed
                    $error[$row]['type'] = 31;
                }
            }
            /*
             * // check Funding Source Code 1
             * if($item_fundingCode1 != "")
             * {
             * $sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_fundingCode1)."' AND RecordStatus=1";
             *
             * $arr_result = $linventory->returnArray($sql,1);
             * if(sizeof($arr_result)==0){
             * $error[$row]['type'] = 13;
             * }
             * }
             * else
             * {
             * //$error[$row]['type'] = 14;
             * }
             */
            // check Funding Source Code 1
            if ($item_fundingCode1 != "") {
                $sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode1) . "' AND RecordStatus=1";
                
                $arr_result = $linventory->returnArray($sql, 1);
                if (sizeof($arr_result) == 0) {
                    $error[$row]['type'] = 13;
                }
            }
            // FundingSourceCode1 cannot equal to FundSourceCode2
            if ($item_fundingCode1 == $item_fundingCode2 && $item_fundingCode1 != "") {
                $error[$row]['type'] = 51;
            }
            
            // check Funding Source Code 1
            if ($item_fundingCode2 != "") {
                $sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode2) . "' AND RecordStatus=1";
                
                $arr_result = $linventory->returnArray($sql, 1);
                if (sizeof($arr_result) == 0) {
                    $error[$row]['type'] = 13;
                }
            }
            
            if ($item_ownership == "") {
                // $error[$row]['type'] = 15;
            } else {
                if ($item_ownership != 1) {
                    if ($item_ownership != 2) {
                        if ($item_ownership != 3) {
                            $error[$row]['type'] = 17;
                        }
                    }
                }
            }
            
            if (trim($item_warranty) != "" && ! checkDateIsValid(getDefaultDateFormat($item_warranty))) {
                $error[$row]['type'] = 54;
            }
            
            // check purchase date (SKH)
            if ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']) {
                if ($item_purchase_date == "" || ! checkDateIsValid(getDefaultDateFormat($item_purchase_date))) {
                    $error[$row]['type'] = 53;
                }
            }
            
            // check purchase price is empty or not #
            if ($item_purchase_price == "") {
                $item_purchase_price = 0;
            } else {
                if ($item_purchase_price < 0) {
                    $error[$row]['type'] = 22;
                } else {
                    if (strpos($item_purchase_price, ".") > 0) {
                        $tmp_purchase_price = substr($item_purchase_price, strpos($item_purchase_price, "."));
                        if (strlen($tmp_purchase_price) > 4) {
                            $error[$row]['type'] = 22;
                        }
                    }
                }
            }
            
            // check unit price is empty or not #
            if ($item_unit_price == "") {
                $item_unit_price = 0;
            } else {
                if ($item_unit_price < 0) {
                    $error[$row]['type'] = 24;
                } else {
                    if (strpos($item_unit_price, ".") > 0) {
                        $tmp_unit_price = substr($item_unit_price, strpos($item_unit_price, "."));
                        if (strlen($tmp_unit_price) > 4) {
                            $error[$row]['type'] = 24;
                        }
                    }
                }
            }
            
            // check item qty
            if ($item_qty != 1) {
                $error[$row]['type'] = 41;
            }
            
            $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "'";
            $tmp_category_id = $linventory->returnVector($sql);
            $category_id = $tmp_category_id[0];
            
            $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND b.CategoryID = $category_id) WHERE a.Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND a.RecordStatus=1 AND b.RecordStatus=1";
            $tmp_category2_id = $linventory->returnVector($sql);
            $category2_id = $tmp_category2_id[0];
            
            // $item_chi_name = addslashes(intranet_htmlspecialchars(convert2unicode($item_chi_name)));
            // $item_eng_name = addslashes(intranet_htmlspecialchars(convert2unicode($item_eng_name)));
            $item_chi_name = addslashes(intranet_htmlspecialchars($item_chi_name));
            $item_eng_name = addslashes(intranet_htmlspecialchars($item_eng_name));
            $item_chi_description = intranet_htmlspecialchars(addslashes($item_chi_description));
            $item_eng_description = intranet_htmlspecialchars(addslashes($item_eng_description));
            $item_brand = intranet_htmlspecialchars(addslashes($item_brand));
            $item_supplier = intranet_htmlspecialchars(addslashes($item_supplier));
            $item_supplier_contact = intranet_htmlspecialchars(addslashes($item_supplier_contact));
            $item_supplier_description = intranet_htmlspecialchars(addslashes($item_supplier_description));
            $item_maintain_info = intranet_htmlspecialchars(addslashes($item_maintain_info));
            $item_remarks = intranet_htmlspecialchars(addslashes($item_remarks));
            $item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
            $item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
            // $item_code = $linventory->Get_Safe_Sql_Query($item_code);
            $item_tag_code = $linventory->Get_Safe_Sql_Query($item_tag_code);
            $item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
            $item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
            $item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
            $item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
            $item_fundingCode1 = $linventory->Get_Safe_Sql_Query($item_fundingCode1);
            $item_unitprice1 = $linventory->Get_Safe_Sql_Query($item_unitprice1);
            $item_fundingCode2 = $linventory->Get_Safe_Sql_Query($item_fundingCode2);
            $item_unitprice2 = $linventory->Get_Safe_Sql_Query($item_unitprice2);
            // $item_warranty = $linventory->Get_Safe_Sql_Query($item_warranty);
            $item_warranty = $linventory->Get_Safe_Sql_Query(getDefaultDateFormat($item_warranty));
            $item_license = $linventory->Get_Safe_Sql_Query($item_license);
            $item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
            $item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
            $item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
            $item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
            $item_unitprice = $item_unitprice1 + $item_unitprice2;
            $stocktakeoption_value = $linventory->parseStockTakeOption($stocktakeoption);
            $tags_value = $linventory->Get_Safe_Sql_Query($tags);
            $item_purchase_date = $linventory->Get_Safe_Sql_Query(getDefaultDateFormat($item_purchase_date));
            
            $values = "(1,'$category_id','$category2_id','$item_categoryCode','$item_category2Code','$item_chi_name','$item_eng_name', '$item_chi_description', '$item_eng_description', '$item_code','$item_tag_code','$item_ownership','$item_admin_group_code','$item_buildingCode','$item_locationLevelCode','$item_locationCode','$item_fundingCode1','$item_unitprice1','$item_fundingCode2','$item_unitprice2','$item_warranty','$item_license', '$item_serial', '$item_brand', '$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_quotation_no', '$item_tender_no', '$item_invoice_no', '$item_purchase_date', '$item_purchase_price', '$item_unitprice', '$item_maintain_info', '$item_remarks', '1', '$item_qty', '$stocktakeoption_value', '$tags_value')";
            $sql = "INSERT INTO TEMP_INVENTORY_ITEM	(ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, TagCode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode1,UnitPrice1, FundingSourceCode2, UnitPrice2, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, Brand, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, ItemRemark, ExistItem, Quantity, StockTakeOption, Tags) VALUES $values";
            $linventory->db_db_query($sql);
        }
    }
    // }
    
    // ##################################
    // INSERT SINGLE ITEM
    // ##################################
    if ($actionType == 1 && sizeof($newItem) > 0) // insert single item ($actionType == 1)
{
        
        // foreach($newItem as $row=>$key)
        for ($i = 0; $i < count($arr_new_data); $i ++) {
            if ($error[$i]["type"]) {
                continue;
            }
            
            $row = $i;
            
            list ($item_code, $item_tag_code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode1, $item_unitprice1, $item_fundingCode2, $item_unitprice2, $item_ownership, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_maintain_info, $item_remarks, $item_qty, $stocktakeoption, $tags) = $arr_new_data[$row];
            
            $item_unitprice1 = str_replace(',','',$item_unitprice1);
            $item_unitprice2 = str_replace(',','',$item_unitprice2);
            $item_purchase_price = str_replace(',','',$item_purchase_price); 
            // check single item qty
            $item_qty = trim($item_qty);
            if ($item_qty == "") { // if qty is empty, assume qty = 1
                $item_qty = 1;
            } else 
                if (is_numeric($item_qty)) { // if qty is numeric
                    
                    if (floor($item_qty) == $item_qty) {
                        if (($item_code != "" || $item_tag_code != "") && $item_qty != 1) {
                            /*
                             * if qty > 1, then $item_code and $item_tag_code MUST be auto-generated;
                             * otherwise (if $item_code & $item_tag_code are not empty, qty must be 1)
                             */
                            $error[$row]['type'] = 41;
                        }
                    } else {
                        $error[$row]['type'] = 42; // if qty not a integer
                    }
                } else { // if qty is not a numeric
                    $error[$row]['type'] = 42;
                }
            
            /*
             * # Check Item Code Empty #
             * if($item_code == "")
             * {
             * if(isset($existItemCategory[$item_categoryCode.$item_category2Code])) {
             * $existItemCategory[$item_categoryCode.$item_category2Code] += 1;
             * } else {
             * $existItemCategory[$item_categoryCode.$item_category2Code] = 0;
             * }
             *
             * $item_code = generateItemCode($item_categoryCode,$item_category2Code,$item_purchase_date,$item_admin_group_code,$item_locationCode,$item_fundingCode1, $existItemCategory[$item_categoryCode.$item_category2Code]);
             * }
             */
            
            // Check Barcode
            if ($item_tag_code != "") {
                $item_tag_code = trim($item_tag_code);
                
                if (strlen($item_tag_code) > $BarcodeMaxLength) {
                    $error[$row]['type'] = 28; // Invalid Barcode
                } else {
                    if ($BarcodeFormat == 1) {
                        $patten = "/^[0-9\$\s\/.|-]+$/";
                    } else {
                        $patten = "/^[0-9A-Z\$\s\/.|-]+$/";
                    }
                    
                    if (preg_match($patten, $item_tag_code) == false) {
                        $error[$row]['type'] = 28; // Invalid Barcode
                    } else {
                        $sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '" . $linventory->Get_Safe_Sql_Query($item_tag_code) . "'";
                        $arr_result = $linventory->returnArray($sql, 1);
                        if (sizeof($arr_result) > 0) {
                            $error[$row]['type'] = 3; // Barcode already exists
                        }
                        $arr_tmp_barcode[] = strtoupper($item_tag_code);
                    }
                }
            }
            /*
             * else # generate Barcode if empty
             * {
             * while(sizeof($arr_final_barcode)!=sizeof($arr_new_data))
             * {
             * $tmp_barcode = $linventory->random_barcode();
             * $sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '".$linventory->Get_Safe_Sql_Query($tmp_barcode)."'";
             * $arr_tmp_barcode = $linventory->returnVector($sql);
             * if((sizeof($arr_tmp_barcode)==0) && (!array_search($tmp_barcode,$arr_final_barcode)))
             * array_push($arr_final_barcode,$tmp_barcode);
             * }
             * $item_tag_code = $arr_final_barcode[$row];
             *
             * }
             */
            
            // Check Item Chi Name #
            if ($item_chi_name == "") {
                $error[$row]['type'] = 5; // Item Chinese Name is empty
            }
            
            // Check Item Eng Name #
            if ($item_eng_name == "") {
                $error[$row]['type'] = 6; // Item English Name is empty
            }
            
            // ## Check Category Code ###
            if ($item_categoryCode != "") {
                if ($item_category2Code != "") {
                    $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "' AND RecordStatus=1";
                    $arr_tmp_result = $linventory->returnVector($sql);
                    $cat_id = $arr_tmp_result[0];
                    
                    $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID='$cat_id' AND Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND RecordStatus=1";
                    $arr_tmp_result = $linventory->returnVector($sql);
                    $subcat_id = $arr_tmp_result[0];
                    
                    $sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND RecordStatus=1";
                    $arr_result = $linventory->returnVector($sql);
                    
                    $matched_row = $arr_result[0];
                    if ($matched_row == 0) {
                        $error[$row]['type'] = 30; // Invalid Category Code / Sub-category Code
                    } else {
                        // category is different from existing item
                        // $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID='$cat_id' AND Category2ID='$subcat_id' AND ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."'";
                        $sql = "SELECT CategoryID, Category2ID FROM INVENTORY_ITEM WHERE ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "'";
                        $tempResult = $linventory->returnArray($sql);
                        if ($item_code != '' && $tempResult[0] != 0 && $tempResult[0]['CategoryID'] != $cat_id && $tempResult[0]['Category2ID'] != $subcat_id) {
                            $error[$row]['type'] = 40;
                        } else {
                            /*
                             * // check License, Warranty and SerialNumber according to category setting
                             * $sql = "SELECT HasSoftwareLicenseModel, HasWarrantyExpiryDate, HasSerialNumber FROM INVENTORY_CATEGORY_LEVEL2 WHERE Category2ID = '$subcat_id' AND CategoryID = '$cat_id'";
                             * $tempresult = $linventory->returnArray($sql);
                             *
                             * list($_hasLicense, $_hasWarranty, $_hasSerialNumber) = $tempresult[0];
                             * if(!$_hasLicense && $item_license!="") {
                             * $error[$row]['type'] = 45;
                             * } else if($_hasLicense && $item_license=="") {
                             * $error[$row]['type'] = 48;
                             * } else if($_hasWarranty && $item_warranty=="") {
                             * $error[$row]['type'] = 49;
                             * } else if(!$_hasWarranty && $item_warranty!="") {
                             * $error[$row]['type'] = 46;
                             * } else if($_hasSerialNumber && $item_serial=="") {
                             * $error[$row]['type'] = 50;
                             * } else if(!$_hasSerialNumber && $item_serial!="") {
                             * $error[$row]['type'] = 47;
                             * }
                             */
                        }
                    }
                } else {
                    $error[$row]['type'] = 8; // Sub-category Code is empty
                }
            } else {
                $error[$row]['type'] = 27; // Category Code is empty
            }
            
            if ($item_admin_group_code != "") {
                $sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "'";
                $arr_result = $linventory->returnArray($sql, 1);
                
                if (sizeof($arr_result) == 0) {
                    $error[$row]['type'] = 9; // Invalid Group Code
                } else {
                    if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-eInventory']) {
                        // check whether current user is the leader of group (only leader can insert record)
                        $sql = "SELECT * FROM INVENTORY_ADMIN_GROUP_MEMBER m INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=m.AdminGroupID AND g.Code='" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "') WHERE m.UserID='$UserID' AND m.RecordType=1";
                        $arr_result = $linventory->returnArray($sql);
                        
                        if (sizeof($arr_result) == 0) {
                            $error[$row]['type'] = 32; // User is not the leader of selected management group
                        }
                    }
                }
            } else {
                $error[$row]['type'] = 10; // Group Code is empty
            }
            
            if ($item_buildingCode != "") {
                if ($item_locationLevelCode != "") {
                    if ($item_locationCode != "") {
                        $sql = "SELECT 
									DISTINCT room.Code
								FROM 
									INVENTORY_LOCATION_BUILDING as building INNER JOIN 
									INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
									INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
								WHERE
									building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1 and 
									building.Code = '" . $linventory->Get_Safe_Sql_Query($item_buildingCode) . "' and floor.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "'and room.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "'";
                        $arr_result = $linventory->returnVector($sql);
                        if ($arr_result[0] != $item_locationCode) {
                            $error[$row]['type'] = 11; // Invalid Location Code / Sub-location Code
                        }
                    } else {
                        $error[$row]['type'] = 29; // Sub-location Code is empty
                    }
                } else {
                    $error[$row]['type'] = 12; // Location Code is empty
                }
            } else {
                $error[$row]['type'] = 31; // Building Code empty
            }
            
            if ($item_fundingCode1 != "") {
                $sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode1) . "' AND RecordStatus=1";
                
                $arr_result = $linventory->returnArray($sql, 1);
                if (sizeof($arr_result) == 0) {
                    $error[$row]['type'] = 13; // Invalid Funding Source Code
                }
            } else {
                $error[$row]['type'] = 14; // Funding Source Code is empty
            }
            
            if ($item_fundingCode1 == $item_fundingCode2) {
                $error[$row]['type'] = 51;
            }
            
            // check Funding Source Code 1
            if ($item_fundingCode2 != "") {
                $sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode2) . "' AND RecordStatus=1";
                
                $arr_result = $linventory->returnArray($sql, 1);
                if (sizeof($arr_result) == 0) {
                    $error[$row]['type'] = 13;
                }
            }
            
            if ($item_ownership == "") {
                $error[$row]['type'] = 15; // Ownership Code is empty
            } else {
                if ($item_ownership != 1) {
                    if ($item_ownership != 2) {
                        if ($item_ownership != 3) {
                            $error[$row]['type'] = 17; // Invalid Ownership Code
                        }
                    }
                }
            }
            
            if (trim($item_warranty) != "" && ! checkDateIsValid(getDefaultDateFormat($item_warranty))) {
                $error[$row]['type'] = 54;
            }
            
            // check purchase date (SKH)
            if ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']) {
                if ($item_purchase_date == "" || ! checkDateIsValid(getDefaultDateFormat($item_purchase_date))) {
                    $error[$row]['type'] = 53;
                }
            }
            
            // check purchase price is empty or not #
            if ($item_purchase_price == "") {
                $item_purchase_price = 0;
            } else {
                if ($item_purchase_price < 0) {
                    $error[$row]['type'] = 22; // Invalid Total Purchase Amount
                } else {
                    if (strpos($item_purchase_price, ".") > 0) {
                        $tmp_purchase_price = substr($item_purchase_price, strpos($item_purchase_price, "."));
                        if (strlen($tmp_purchase_price) > 4) {
                            $error[$row]['type'] = 22; // Invalid Total Purchase Amount
                        }
                    }
                }
            }
            
            // check unit price is empty or not #
            if ($item_unit_price == "") {
                $item_unit_price = 0;
            } else {
                if ($item_unit_price < 0) {
                    $error[$row]['type'] = 24; // Invalid Unit Price
                } else {
                    if (strpos($item_unit_price, ".") > 0) {
                        $tmp_unit_price = substr($item_unit_price, strpos($item_unit_price, "."));
                        if (strlen($tmp_unit_price) > 4) {
                            $error[$row]['type'] = 24; // Invalid Unit Price
                        }
                    }
                }
            }
            
            if ($exist_item[$item_code]['ItemID'] == "") {
                
                $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "' AND RecordStatus=1";
                $tmp_category_id = $linventory->returnVector($sql);
                $category_id = $tmp_category_id[0];
                
                $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND b.CategoryID = $category_id) WHERE a.Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND a.RecordStatus=1 AND b.RecordStatus=1";
                $tmp_category2_id = $linventory->returnVector($sql);
                $category2_id = $tmp_category2_id[0];
                
                $item_chi_name = intranet_htmlspecialchars(addslashes($item_chi_name));
                $item_eng_name = intranet_htmlspecialchars(addslashes($item_eng_name));
                $item_chi_description = intranet_htmlspecialchars(addslashes($item_chi_description));
                $item_eng_description = intranet_htmlspecialchars(addslashes($item_eng_description));
                $item_brand = intranet_htmlspecialchars(addslashes($item_brand));
                $item_supplier = intranet_htmlspecialchars(addslashes($item_supplier));
                $item_supplier_contact = intranet_htmlspecialchars(addslashes($item_supplier_contact));
                $item_supplier_description = intranet_htmlspecialchars(addslashes($item_supplier_description));
                $item_maintain_info = intranet_htmlspecialchars(addslashes($item_maintain_info));
                $item_remarks = intranet_htmlspecialchars(addslashes($item_remarks));
                $item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
                $item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
                $item_code = $linventory->Get_Safe_Sql_Query($item_code);
                $item_tag_code = $linventory->Get_Safe_Sql_Query($item_tag_code);
                $item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
                $item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
                $item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
                $item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
                $item_fundingCode1 = $linventory->Get_Safe_Sql_Query($item_fundingCode1);
                $item_unitprice1 = $linventory->Get_Safe_Sql_Query($item_unitprice1);
                $item_fundingCode2 = $linventory->Get_Safe_Sql_Query($item_fundingCode2);
                $item_unitprice2 = $linventory->Get_Safe_Sql_Query($item_unitprice2);
                // $item_warranty = $linventory->Get_Safe_Sql_Query($item_warranty);
                $item_warranty = $linventory->Get_Safe_Sql_Query(getDefaultDateFormat($item_warranty));
                $item_license = $linventory->Get_Safe_Sql_Query($item_license);
                $item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
                $item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
                $item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
                $item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
                $item_unitprice = $item_unitprice1 + $item_unitprice2;
                $stocktakeoption_value = $linventory->parseStockTakeOption($stocktakeoption);
                $tags_value = $linventory->Get_Safe_Sql_Query($tags);
                $item_purchase_date = $linventory->Get_Safe_Sql_Query(getDefaultDateFormat($item_purchase_date));

                $tempItemCodeAry[] = $item_code;
                $tempBarCodeAry[] = $item_tag_code;
                
                $values = "(1,'$category_id','$category2_id','$item_categoryCode','$item_category2Code','$item_chi_name','$item_eng_name', '$item_chi_description', '$item_eng_description', '$item_code','$item_tag_code','$item_ownership','$item_admin_group_code','$item_buildingCode','$item_locationLevelCode','$item_locationCode','$item_fundingCode1','$item_unitprice1','$item_fundingCode2','$item_unitprice2','$item_warranty','$item_license', '$item_serial', '$item_brand', '$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_quotation_no', '$item_tender_no', '$item_invoice_no', '$item_purchase_date', '$item_purchase_price', '$item_unitprice', '$item_maintain_info', '$item_remarks', '0', '$item_qty', '$stocktakeoption_value', '$tags_value')";
                $sql = "INSERT INTO TEMP_INVENTORY_ITEM	(ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, TagCode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode1, UnitPrice1, FundingSourceCode2, UnitPrice2, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, Brand, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, ItemRemark, ExistItem, Quantity, StockTakeOption, Tags) VALUES $values";
                
                $linventory->db_db_query($sql);
            }
            // echo $item_code.'<br>';
            // check any duplicate item code in the csv file
            if ($item_code != "") {
                $sql = "SELECT COUNT(ItemCode) FROM TEMP_INVENTORY_ITEM WHERE ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "'";
                $arr_tmp_existing_ItemCode = $linventory->returnVector($sql);
                if ($arr_tmp_existing_ItemCode[0] > 1)
                    $error[$row]['type'] = 19; // Item Code already used
            }
            
            // check any duplicate barcode in the csv file
            if ($item_tag_code != "") {
                $sql = "SELECT COUNT(TagCode) FROM TEMP_INVENTORY_ITEM WHERE TagCode='" . $linventory->Get_Safe_Sql_Query($item_tag_code) . "'";
                $arr_tmp_existing_Barcode = $linventory->returnVector($sql);
                if ($arr_tmp_existing_Barcode[0] > 1)
                    $error[$row]['type'] = 20; // Barcode already used
            }
        }
    }
    $rowNo = "";
    $tempError = $error;
    
    $invalidRow = count($tempError);
    if (count($tempError) > 0) {
        ksort($tempError);
        $delim = "";
        foreach ($tempError as $row => $value) {
            $rowNo .= $delim . ($row + 1);
            $delim = ", ";
        }
    }
    $validRow = $file_original_row - $invalidRow;
    
    // ## Show Import Result ###
    if ($invalidRow > 0) {
        $invalidRowMsg = $Lang['eInventory']['RowLine1'] . " " . $rowNo . " " . $Lang['eInventory']['RowLine2'] . " " . $Lang['eInventory']['InvalidRecordRow'];
        $invalidRowMsg = "<font color=\"red\">" . $Lang['eInventory']['InvalidRecord'] . " : $invalidRow ($invalidRowMsg)</font>";
    }
    
    $table_content .= "<tr>";
    $table_content .= "<td class=\"tabletext\" colspan=\"100%\">
							$i_InventorySystem_ImportItem_TotalRow : $file_original_row<br>
							" . $Lang['eInventory']['ValidRecord'] . " : $validRow<br>
							$invalidRowMsg							
						</td>";
    $table_content .= "</tr>\n";
    // ## END ###
    
    $table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseName</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishName</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseDescription</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishDescription</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_CategoryCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Category2Code</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_GroupCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['BuildingCode'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['FloorCode'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['RoomCode'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_FundingSourceCode (1)</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Unit_Price (1)</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_FundingSourceCode (2)</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Unit_Price (2)</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Ownership</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Warrany_Expiry</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_License</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Serial_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Brand_Name</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Name</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Contact</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Description</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Quot_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Tender_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Invoice_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Purchase_Date</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Price</td>";
    // $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Unit_Price</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystemItemMaintainInfo</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['StockTake'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eDiscipline']['Tag'] . "</td>";
    
    if (sizeof($error) > 0) {
        $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
    }
    $table_content .= "</tr>\n";
    
    if (sizeof($error) == 0 && $invalidRow == 0) {
        $sql = "SELECT 
					CategoryID, 
					Category2ID,
					CategoryCode, 
					Category2Code, 
					NameChi, 
					NameEng, 
					DescriptionChi,
					DescriptionEng,
					ItemCode, 
					TagCode,
					Ownership, 
					GroupInChargeCode, 
					BuildingCode,
					LocationLevelCode,
					LocationCode, 
					FundingSourceCode1, 
					UnitPrice1, 
					FundingSourceCode2, 
					UnitPrice2, 
					IF(WarrantyExpiryDate = '0000-00-00', '-', WarrantyExpiryDate), 
					IF(SoftwareLicenseModel = '', '-', SoftwareLicenseModel),
					IF(SerialNumber = '', '-', SerialNumber),
					IF(Brand = '','-',Brand),
					IF(SupplierName = '', '-', SupplierName),
					IF(SupplierContact = '', '-', SupplierContact),
					IF(SupplierDescription = '', '-', SupplierDescription),
					IF(QuotationNo = '','-',QuotationNo),
					IF(TenderNo = '','-',TenderNo),
					IF(InvoiceNo = '','-',InvoiceNo),
					IF(PurchaseDate = '0000-00-00', '-', PurchaseDate), 
					CONCAT('$',PurchasedPrice),
					CONCAT('$',UnitPrice),
					IF(MaintainInfo='','-',MaintainInfo),
					IF(ItemRemark='','-',ItemRemark),
					Quantity,
					StockTakeOption,
					Tags
				FROM
					TEMP_INVENTORY_ITEM";
        $arr_result = $linventory->returnArray($sql, 30);
        
        if (sizeof($arr_result) > 0) {
            $openFontColorTag = '';
        	$closeFontColorTag = '';
        	if($actionType == 2){
        		$openFontColorTag = '<font color="red">';
        		$closeFontColorTag = '</font>';
        	}
            for ($i = 0; $i < sizeof($arr_result); $i ++) {
                $j = $i + 1;
                if ($j % 2 == 0)
                    $table_row_css = " class=\"tablerow1\" ";
                else
                    $table_row_css = " class=\"tablerow2\" ";
                
                list ($category_id, $category2_id, $item_categoryCode, $item_category2Code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $this_item_code, $item_tag, $item_ownership, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode1, $item_unitprice1, $item_fundingCode2, $item_unitprice2, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_unit_price, $item_maintain_info, $item_remarks, $item_qty, $stocktakeoption, $tags) = $arr_result[$i];
                
                $item_unitprice1 = str_replace(',','',$item_unitprice1);
                $item_unitprice2 = str_replace(',','',$item_unitprice2);
                $item_purchase_price = str_replace(',','',$item_purchase_price);
                // $table_content .= "<tr $table_row_css><td class=\"tabletext\">".($item_qty==1 ? $this_item_code : "-")."</td>";
                // $table_content .= "<td class=\"tabletext\">".($item_qty==1 ? $item_tag : "-")."</td>";
                $table_content .= "<tr $table_row_css><td class=\"tabletext\">".$openFontColorTag . $this_item_code . $closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag . $item_tag . $closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_categoryCode.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
                $table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
                $table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_fundingCode1</td>";
                $table_content .= "<td class=\"tabletext\">$item_unitprice1</td>";
                $table_content .= "<td class=\"tabletext\">$item_fundingCode2</td>";
                $table_content .= "<td class=\"tabletext\">$item_unitprice2</td>";
                $table_content .= "<td class=\"tabletext\">$item_ownership</td>";
                $table_content .= "<td class=\"tabletext\">$item_warranty</td>";
                $table_content .= "<td class=\"tabletext\">$item_license</td>";
                $table_content .= "<td class=\"tabletext\">$item_serial</td>";
                $table_content .= "<td class=\"tabletext\">$item_brand</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
                $table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
                // $table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
                $table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
                $table_content .= "<td class=\"tabletext\">$item_remarks</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_qty.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">$stocktakeoption</td>";
                $table_content .= "<td class=\"tabletext\">$tags</td>";
            }
        }
        $table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"100%\"></td></tr>\n";
        
        if (sizeof($error) == 0 && $actionType == 2){
	    	$table_content .= '<tr><td colspan="100%" class="tabletextremark">'.$Lang['eInventory']['RedColorInfoWillNotBeUpdated'].'</td></tr>';
	    }
        
        $table_content .= "<tr><td colspan=\"100%\" align=\"right\">" . $linterface->GET_ACTION_BTN($button_submit, "submit", "") . " " . $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>\n";
    }
    
    if (sizeof($error) > 0 || $invalidRow > 0) {
        $affectedRow = ($actionType == 1) ? $newItem : $updateItem;
        if (sizeof($duplicateItem) > 0)
            $affectedRow = array_merge($affectedRow, $duplicateItem);
        
        for ($i = 0; $i < sizeof($arr_new_data); $i ++) {
            $j = $i + 1;
            if ($j % 2 == 0)
                $table_row_css = " class=\"tablerow1\" ";
            else
                $table_row_css = " class=\"tablerow2\" ";
            
            list ($item_code_raw, $item_tag_code_raw, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode1, $item_unitprice1, $item_fundingCode2, $item_unitprice2, $item_ownership, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_maintain_info, $item_remarks, $item_qty, $stocktakeoption, $tags) = $arr_new_data[$i];
            $_thisItemCode = $item_code_raw == "" ? $tempItemCodeAry[$i] : $item_code_raw;
            $_thisTagCode = $item_tag_code_raw == "" ? $tempBarCodeAry[$i] : $item_tag_code_raw;
            
            /*
             * if($item_qty>1 && $item_code_raw=="" && $item_tag_code_raw=="") {
             * $_thisItemCode = "-";
             * $_thisTagCode = "-";
             * }
             */
            
            $table_content .= "<tr $table_row_css><td class=\"tabletext\">" . $_thisItemCode . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $_thisTagCode . "</td>";
            $table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
            $table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
            $table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
            $table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
            $table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
            $table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
            $table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
            $table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
            $table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
            $table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
            $table_content .= "<td class=\"tabletext\">$item_fundingCode1</td>";
            $table_content .= "<td class=\"tabletext\">$item_unitprice1</td>";
            $table_content .= "<td class=\"tabletext\">$item_fundingCode2</td>";
            $table_content .= "<td class=\"tabletext\">$item_unitprice2</td>";
            $table_content .= "<td class=\"tabletext\">$item_ownership</td>";
            $table_content .= "<td class=\"tabletext\">$item_warranty</td>";
            $table_content .= "<td class=\"tabletext\">$item_license</td>";
            $table_content .= "<td class=\"tabletext\">$item_serial</td>";
            $table_content .= "<td class=\"tabletext\">$item_brand</td>";
            $table_content .= "<td class=\"tabletext\">$item_supplier</td>";
            $table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
            $table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
            $table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
            $table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
            $table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
            $table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
            $table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
            // $table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
            $table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
            $table_content .= "<td class=\"tabletext\">$item_remarks</td>";
            $table_content .= "<td class=\"tabletext\">$item_qty</td>";
            $table_content .= "<td class=\"tabletext\">$stocktakeoption</td>";
            $table_content .= "<td class=\"tabletext\">$tags</td>";
            
            if ($error[$i]["type"] == "") {
                $table_content .= "<td class=\"tabletext\"> - </td></tr>\n";
            } else 
                if ($error[$i]["type"] != "") {
                    $table_content .= "<td class=\"tabletext\"><font color=\"red\">" . $i_InventorySystem_ImportError[$error[$i]["type"]] . "</font></td></tr>\n";
                }
        }
        $table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"100%\"></td></tr>\n";
        $table_content .= "<tr><td colspan=\"100%\" align=\"right\">" . $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>\n";
    }
}
$TAGS_OBJ[] = array(
    $button_import,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>

<form name="form1" action="import_item_update.php" method="post">
	<table border="0" width="98%" cellspacing="0" cellpadding="0"
		align="center">
<?=$infobar?>
</table>
	<br>
	<table border="0" width="96%" cellspacing="0" cellpadding="5"
		align="center">
<?=$table_content;?>
</table>
	<input type="hidden" name="format" id="format" value=<?=$format;?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<?

/*
 * a little bit different coding in import_item_update.php
 */
function generateItemCode($CatCode, $SubCatCode, $PurchaseDate, $GroupCode, $SubLocationCode, $FundingCode, $existPos = "")
{
    $linventory = new libinventory();
    global $sys_custom;
    
    if ($sys_custom['CatholicEducation_eInventory']) {
        // var itemCode = groupCode + category + sub-category + 5 digits number
        $itemCode_Prefix = $GroupCode . "-" . $CatCode . "-" . $SubCatCode . "-";
        
        $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($CatCode) . "'";
        $arrCatID = $linventory->returnVector($sql);
        $CatID = $arrCatID[0];
        
        $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$CatID."' AND Code = '" . $linventory->Get_Safe_Sql_Query($SubCatCode) . "'";
        $arrSubCatID = $linventory->returnVector($sql);
        $SubCatID = $arrSubCatID[0];
        
        $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = '".$CatID."' AND Category2ID = '".$SubCatID."' AND ItemCode LIKE '" . $itemCode_Prefix . "%'";
        $arrLastItemCode = $linventory->returnVector($sql);
        if (sizeof($arrLastItemCode) > 0) {
            $LastItemCode = $arrLastItemCode[0];
            $tmp_str_pos = strrpos($LastItemCode, "-");
            $postfix = substr($LastItemCode, $tmp_str_pos + 1, strlen($LastItemCode));
            
            $postfix = $postfix + 1;
            if ($existPos != "" && $existPos != 0) {
                $postfix += $existPos;
            }
            $length = strlen($postfix);
            
            if ($length < 5) {
                for ($j = 0; $j < (5 - $length); $j ++) {
                    $default_postfix = "0";
                    $ItemCodepostfix .= $default_postfix;
                }
                $TmpItemCode = $ItemCodepostfix . $postfix;
                $new_item_code = $itemCode_Prefix . $TmpItemCode;
            } else {
                $new_item_code = $itemCode_Prefix . $postfix;
            }
        } else {
            $new_item_code = $itemCode_Prefix . "00001";
        }
    } else {
        if ($sys_custom['eInventory_ItemCodeFormat_New']) {
            $PurchaseYear = substr($PurchaseDate, 0, 4);
            $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
            
            if (sizeof($arr_itemCodeFormat) > 0) {
                for ($i = 0; $i < sizeof($arr_itemCodeFormat); $i ++) {
                    if ($arr_itemCodeFormat[$i] == 1) {
                        $itemCodeFormat_Year = 1;
                    }
                    if ($arr_itemCodeFormat[$i] == 2) {
                        $itemCodeFormat_Group = 1;
                    }
                    if ($arr_itemCodeFormat[$i] == 3) {
                        $itemCodeFormat_Funding = 1;
                    }
                    if ($arr_itemCodeFormat[$i] == 4) {
                        $itemCodeFormat_Location = 1;
                    }
                }
            }
            
            if ($itemCodeFormat_Year == 1) {
                $itemCode_Prefix .= $PurchaseYear;
            }
            if ($itemCodeFormat_Group == 1) {
                $itemCode_Prefix .= $GroupCode;
            }
            if ($itemCodeFormat_Funding == 1) {
                $itemCode_Prefix .= $FundingCode;
            }
            if ($itemCodeFormat_Location == 1) {
                $itemCode_Prefix .= $SubLocationCode;
            }
        } else {
            $itemCode_Prefix = $CatCode . $SubCatCode;
        }
        $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($CatCode) . "'";
        $arrCatID = $linventory->returnVector($sql);
        $CatID = $arrCatID[0];
        
        $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$CatID."' AND Code = '" . $linventory->Get_Safe_Sql_Query($SubCatCode) . "'";
        $arrSubCatID = $linventory->returnVector($sql);
        $SubCatID = $arrSubCatID[0];
        
        $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = '".$CatID."' AND Category2ID = '".$SubCatID."' AND ItemCode LIKE '" . $linventory->Get_Safe_Sql_Query($itemCode_Prefix) . "%'";
        $arrLastItemCode = $linventory->returnVector($sql);
        
        if (sizeof($arrLastItemCode) > 0) {
            $LastItemCode = $arrLastItemCode[0];
            $tmp_str_pos = strrpos($LastItemCode, "_");
            $postfix = substr($LastItemCode, $tmp_str_pos + 1, strlen($LastItemCode));
            
            $postfix = $postfix + 1;
            if ($existPos != "" && $existPos != 0) {
                $postfix += $existPos;
            }
            $length = strlen($postfix);
            
            if ($sys_custom['eInventory_ItemCodeFormat_New']) {
                $new_item_code = $itemCode_Prefix . $postfix;
            } else {
                if ($length < DEFAULT_ITEM_CODE_NUM_LENGTH) {
                    for ($j = 0; $j < (DEFAULT_ITEM_CODE_NUM_LENGTH - $length); $j ++) {
                        $default_postfix = "0";
                        $ItemCodepostfix .= $default_postfix;
                    }
                    $TmpItemCode = $ItemCodepostfix . $postfix;
                    $new_item_code = $itemCode_Prefix . "_" . $TmpItemCode;
                    // array_push($arr_final_item_code,$new_item_code);
                } else {
                    $new_item_code = $itemCode_Prefix . "_" . $postfix;
                }
            }
        }
    }
    return $new_item_code;
}
?>