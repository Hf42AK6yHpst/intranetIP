<?php
//using : yat

#################################################
#
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

foreach($RecordID as $k=>$d)
{
	# check: cannot delete all
	$sql2 = "select count(*) from INVENTORY_ITEM_BULK_LOG where ItemID=$ItemID and Action=1 and (RecordStatus=0 or RecordStatus is NULL)";
	$result2 = $linventory->returnVector($sql2);
	
	if($result2[0] > 1)
	{
		$sql = "update INVENTORY_ITEM_BULK_LOG set RecordStatus=-1, DateModified=now(), DateModifiedBy=$UserID where RecordID=$d and (RecordStatus!=-1 or RecordStatus is NULL)";
		$linventory->db_db_query($sql) or die(mysql_error());
		
		# add invoice history log
		if($linventory->db_affected_rows())
		{
			$log_data = array();
			$log_data['LogID'] = $d;
			$log_data['ItemID'] = $ItemID;
			$log_data['Action'] = 2;
			$linventory->insertBulkInvoiceHistoryLog($log_data);
		}
	}
}
header("location: category_show_items_detail.php?type=1&item_id=$ItemID&xmsg=DeleteSuccess");
intranet_closedb();
?>