<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory	= new libinventory();

$layer_content = "<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
//<input type=button value=' X ' onClick=hideMenu('ToolMenu2')>
$layer_content .= "<tr class=admin_bg_menu><td colspan=5 align=right><img src=\"$image_path/$LAYOUT_SKIN/iCalendar/layer_btn_close_on.gif\" onClick=hideMenu('ToolMenu2')></td></tr>";

if($Val == 1)
	$layer_content .= "<tr><td class=\"tabletext\">$i_InventorySystem_NewItem_PurchasePrice_Help</td>";
if($Val == 2)
	$layer_content .= "<tr><td class=\"tabletext\">$i_InventorySystem_NewItem_UnitPrice_Help</td>";
if($Val == 3)
	$layer_content .= "<tr><td class=\"tabletext\">$i_InventorySystem_NewItem_PurchaseType_Help</td>";
	
$layer_content .= "</tr></td></table>";

//$response = iconv("Big5","UTF-8",$layer_content);
//$response = convert2unicode($layer_content,1);
$response = $layer_content;

echo $response;
intranet_closedb();
?>