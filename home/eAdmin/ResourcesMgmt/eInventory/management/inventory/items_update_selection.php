<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

# Generate Page Tag #
$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/items_full_list.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/category.php", 0);
# End #

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if($item_id == "")
	$item_id = implode(",",$ItemID);
	
$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemId = $item_id";
$arr_item_type = $linventory->returnVector($sql);
$item_type = $arr_item_type[0];

$table_content .= "	<tr><td class=\"tabletext\">\n
						<a class=\"tablelink\" href=\"items_status_edit.php?item_id=$item_id\">Update Item Status</a><br>\n
						<a class=\"tablelink\" href=\"items_location_edit.php?item_id=$item_id\">Update Item Location</a><br>\n
						<a class=\"tablelink\" href=\"items_request_write_off.php?item_id=$item_id\">Request Write Off</a><br>\n
					</td></tr>\n";
?>

<br>
<form name="form1" action="" method="post">
<table border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content?>
</table>
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
