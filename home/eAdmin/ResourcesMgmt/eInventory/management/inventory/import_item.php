<?php
// modifying : yat
#############################
#
#	Date:	2013-11-14	YatWoon
#			Improved: revised ui
#
#############################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($button_import, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

/*
if($error==1)
	$SysMsg = $linterface->GET_SYS_MSG("",$i_StaffAttendance_import_invalid_entries);
else $SysMsg = $linventory->getResponseMsg($msg);
*/

$infobar = "<a href=\"items_full_list.php\">".$i_InventorySystem_BackToFullList."</a>
			<span>".$button_import."</span>";

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
	if ($msg == 15) $SysMsg = $linterface->GET_SYS_MSG("import_failed");
	if ($msg == 16) $SysMsg = $linterface->GET_SYS_MSG("import_failed2");
}

# single item format
$single_format_insert = "";
$single_format_update = "";
for($i=1; $i<=sizeof($Lang['eInventory']['FieldTitle']['Import']['Single']); $i++)
{ 
	$single_format_insert .= $Lang['eInventory']['FieldTitle']['Import']['Single'][$i]['FieldTitle'];
	$single_format_update .= $Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][$i]['FieldTitle'];
	
	if($Lang['eInventory']['FieldTitle']['Import']['Single'][$i]['AjaxLinkTitle'] !="")
	{
		$remark_icon = "&nbsp;<a href='javascript:showInfo(1, \"". $Lang['eInventory']['FieldTitle']['Import']['Single'][$i]['AjaxLink'] ."\")'><img id='img_". $Lang['eInventory']['FieldTitle']['Import']['Single'][$i]['AjaxLink'] ."' src='".$image_path."/".$LAYOUT_SKIN."/inventory/icon_help.gif' border=0></a>";
		$single_format_insert .= $remark_icon;
		$single_format_update .= $remark_icon;
		
		//$single_format_insert .= "&nbsp;[<a class=\"tablelink\" href=\"javascript:".$js_function."\">".$Lang['eInventory']['FieldTitle']['Import']['Single'][$i]['AjaxLinkTitle']."</a>]";
		//$single_format_update .= "&nbsp;[<a class=\"tablelink\" href=\"javascript:".$js_function."\">".$Lang['eInventory']['FieldTitle']['Import']['SingleUpdate'][$i]['AjaxLinkTitle']."</a>]";
	}
	$single_format_insert .= "<BR>";
	$single_format_update .= "<BR>";
}


# bulk item format
$bulk_format_insert = "";
$bulk_format_update = "";
for($i=1; $i<=sizeof($Lang['eInventory']['FieldTitle']['Import']['Bulk']); $i++)
{
	$bulk_format_insert .= $Lang['eInventory']['FieldTitle']['Import']['Bulk'][$i]['FieldTitle'];
	$bulk_format_update .= $Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][$i]['FieldTitle'];
	
	/*
	switch ($Lang['eInventory']['FieldTitle']['Import']['Bulk'][$i]['AjaxLink'])
	{
		case "ItemCode":
			$js_function = "retrieveItemCode(2)"; break;
		case "CategoryCode":
			$js_function = "retrieveCategoryIndex(2)"; break;
		case "SubCategoryCode":
			$js_function = "retrieveCategory2Index(2)"; break;
		case "GroupCode":
			$js_function = "retrieveGroupIndex(2)"; break;
		case "BuildingCode":
			$js_function = "retrieveBuildingIndex(2)"; break;
		case "LocationCode":
			$js_function = "retrieveLocationLevelIndex(2)"; break;
		case "SubLocationCode":
			$js_function = "retrieveLocationIndex(2)"; break;
		case "FundingCode":
			$js_function = "retrieveFundingIndex(2)"; break;
		case "VarianceManager":
			$js_function = "retrieveGroupIndex(2)"; break;
		default:
			$js_function = ""; break;
	}
	*/
	if($Lang['eInventory']['FieldTitle']['Import']['Bulk'][$i]['AjaxLinkTitle'] !="")
	{
		$remark_icon = "&nbsp;<a href='javascript:showInfo(2, \"". $Lang['eInventory']['FieldTitle']['Import']['Bulk'][$i]['AjaxLink'] ."\")'><img id='img_". $Lang['eInventory']['FieldTitle']['Import']['Bulk'][$i]['AjaxLink'] ."' src='".$image_path."/".$LAYOUT_SKIN."/inventory/icon_help.gif' border=0></a>";
		$bulk_format_insert .= $remark_icon;
		$bulk_format_update .= $remark_icon;
		
		//$bulk_format_insert .= "&nbsp;[<a class=\"tablelink\" href=\"javascript:".$js_function."\">".$Lang['eInventory']['FieldTitle']['Import']['Bulk'][$i]['AjaxLinkTitle']."</a>]";
		//$bulk_format_update .= "&nbsp;[<a class=\"tablelink\" href=\"javascript:".$js_function."\">".$Lang['eInventory']['FieldTitle']['Import']['BulkUpdate'][$i]['AjaxLinkTitle']."</a>]";
	}
	$bulk_format_insert .= "<BR>";
	$bulk_format_update .= "<BR>";
}

?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>
<iframe id='lyrShim2'  scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; visibility:hidden; display:none;'></iframe>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"></div>
<script language="javascript">
<!--

var imgObj=0;
var callback = {
        success: function ( o )
        {
                writeToLayer('ToolMenu2',o.responseText);
                showMenu2("img_"+imgObj,"ToolMenu2");
        }
}
function hideMenu2(menuName){
		objMenu = document.getElementById(menuName);
		if(objMenu!=null)
			objMenu.style.visibility='hidden';
		setDivVisible(false, menuName, "lyrShim2");
}
function showMenu2(objName,menuName)
{
  	hideMenu2('ToolMenu2');
	objIMG = document.getElementById(objName);
	//offsetX = (objIMG==null)?0:objIMG.width;
	//offsetY =0;       
	offsetX = -300;
	offsetY = 20;            	 	
    var pos_left = getPostion(objIMG,"offsetLeft");
	var pos_top  = getPostion(objIMG,"offsetTop");
	objDiv = document.getElementById(menuName);
	if(objDiv!=null){
		objDiv.style.visibility='visible';
		objDiv.style.top = pos_top+offsetY+"px";
		objDiv.style.left = pos_left+offsetX+"px";
		setDivVisible(true, menuName, "lyrShim2");
	}
}
function showInfo(type,val)
{
        obj = document.form1;

        var myElement = document.getElementById("ToolMenu2");
        writeToLayer('ToolMenu2','');
        imgObj = val;
        
		showMenu2("img_"+val,"ToolMenu2");
        YAHOO.util.Connect.setForm(obj);

        var path = "retrieveInfo.php?Type="+type+"&Val=" + val;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}


function checkForm() {
	var obj = document.form1;

	if(obj.itemfile.value == "") {
		alert("<?=$Lang['eInventory']['PleaseSelectFile']?>");
		obj.itemfile.select();
	} else {
		//obj.action = "import_item_confirm.php";
		//obj.submit();
		
		if ($('#format1').attr('checked')) { 
			obj.action = "import_single_confirm.php";
			obj.submit();
		} else if($('#format2').attr('checked')) {
			obj.action = "import_bulk_confirm.php";
			obj.submit();
		}
		
	} 
}

function click_item_type()
{
	obj = document.form1;
	var sample_link = document.getElementById('import_csv'); 
	var csv_format = document.getElementById("csv_format");
	
	if(obj.format[0].checked)	// Single item
	{
		sample_link.href = "<?= GET_CSV("sample_import_item1.csv")?>";
		
		if(obj.actionType[0].checked)	// insert
		{
			csv_format.innerHTML = "<?=addslashes($single_format_insert)?>";
		}
		else
		{
			csv_format.innerHTML = "<?=addslashes($single_format_update)?>";
		}
	}	
	else							// Bulk item
	{
		sample_link.href = "<?= GET_CSV("sample_import_item2.csv")?>";
		
		if(obj.actionType[0].checked)	// insert
		{
			csv_format.innerHTML = "<?=addslashes($bulk_format_insert)?>";
		}
		else
		{
			csv_format.innerHTML = "<?=addslashes($bulk_format_update)?>";
		}
	}
}
-->
</script>
<br>

<form name="form1" action="" method="POST" enctype="multipart/form-data">
<table border="0" width="96%" cellpadding="5" cellspacing="0">
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td width="30%" class="navigation_v30" nowrap><?=$infobar?></td>
					<td align="right" width="70%"><?=$SysMsg?></td>
				</tr>
				<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_InventorySystem['item_type']?></td>
	                <td class="tabletext" width="70%">
		                <?=$linterface->Get_Radio_Button("format1", "format", "1",1,"",$i_InventorySystem_ItemType_Single, "click_item_type()");?>
		                <?=$linterface->Get_Radio_Button("format2", "format", "2",0,"",$i_InventorySystem_ItemType_Bulk, "click_item_type()");?>
	    			</td> 
                </tr>
                
                <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['eInventory']['ActionType']?></td>
	                <td class="tabletext" width="70%">
	                	<?=$linterface->Get_Radio_Button("actionType1", "actionType", "1",1,"",$Lang['eInventory']['Insert'], "click_item_type()");?>
	                	<?=$linterface->Get_Radio_Button("actionType2", "actionType", "2",0,"",$Lang['eInventory']['Update'], "click_item_type()");?>
	    			</td>
                </tr>
                
                <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_select_file?></td>
	                <td class="tabletext" width="70%"><input class="file" type="file" name="itemfile" id="itemfile"></td>
                </tr>
				
                <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['General']['CSVSample']?></td>
	                <td class="tabletext" width="70%"><a class="tablelink" id="import_csv" href=""><?=$i_general_clickheredownloadsample?></a></td>
                </tr>
                
                <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
	                <td class="tabletext" width="70%" border="0">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="right" >
							<tr>
								<td valign="top"><span id="csv_format"></span></td>
								<td><span id="ToolMenu2xxxx"></span></td>
							</tr>
							</table>
					</td>
                </tr>
                
				<tr>
					<td colspan="2" class="tabletextremark">
						<?=$i_general_required_field2."<br>".
						$Lang['eInventory']['EditableFields'] ."<br>". 
						$Lang['eInventory']['BarCodeFormat'] ?>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkForm()") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>

<script language="javascript">
<!--
click_item_type();
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
