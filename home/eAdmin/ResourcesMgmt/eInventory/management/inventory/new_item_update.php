<?php
// Using: 
/********************************* Modification Log *************************************
 * 2019-04-30 [Henry]: security issue fix for SQL
 * 2018-02-21 [Henry]: add access right checking [Case#E135442]
 * 2014-05-09 [YatWoon]: Fixed: sql error if suppiler with special char (bulk > existings item) [Case#S61808]
 * 2013-04-18 [YatWoon]: Improved: rename item photo name if item code with "/" symbol [Case#2013-0318-1019-37073]
 * 2013-02-25 [Carlos]: Added Barcode to bulk item
 * 2012-12-06 [Yuen]: Enhanced: support tags 
 * 2012-11-22 [YatWoon]: Improved: [Bulk] set variance manager as resources group if variance manager data is empty 
 * 2012-07-10 [YatWoon]: Enhanced: support 2 funding source for single item
 * 2012-05-23 [YatWoon]:  Improved: allow user select different group and funding for bulk item with same location
 * 2011-06-16 [YatWoon]: User userid as attachment temp folder 
 * 2011-06-04 [YatWoon]: Improved: add "createdby" data [case#2011-0124-1006-09067]
 * 2011-06-02 [Yuen]: Fixed: cannot save record due to null purchase price of single item
 * 2011-05-31 [YatWoon]: Fixed: cannot save >1 record if quantity/invoice/tender include special characters
 * 2011-05-30 [YatWoon]: case #2011-0527-1216-26067, comment out: purchase price = purchase price / quantity
 * 2011-05-27 [YatWoon]: Fixed: use itemid as a folder to store the attachment (prevnet overwrite / delete file if same filename)
 * 						 Fixed: cannot store the attachment
 * 2011-05-18 [Carlos]: Fix upload item photo
 * 2011-03-31 (Carlos): Rename photo name as Item Code
 * 2011-02-24 (Yuen) : 
 * 		For single item, purchase price = purchase price / quantity
 ****************************************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lf				= new libfilesystem();
$curr_date = date("Y-m-d");

$final_item_chi_name = $item_chi_name;
$final_item_eng_name = $item_eng_name;
$final_item_chi_discription = $item_chi_discription;
$final_item_eng_discription = $item_eng_discription;

$item_ownership = IntegerSafe($item_ownership);
$targetItemType = IntegerSafe($targetItemType);
$targetCategory = IntegerSafe($targetCategory);
$targetCategory2 = IntegerSafe($targetCategory2);

# Upload Photo #
if($item_photo)
{
	$re_path = "/file/photo/inventory/";
	$path = $intranet_root.$re_path;
	$tmp_path = $intranet_root."/file/inventory/tmp_photo/".$UserID;
	$photo = stripslashes(${"hidden_item_photo"});
	$tmp_file = "$tmp_path/$photo";
	$ext = $lf->file_ext($photo);
	$extUC = strtoupper($ext);
}

if($targetItemType == ITEM_TYPE_SINGLE)
{	
	/*
	$final_item_chi_name = stripslashes($item_chi_name);
	$final_item_eng_name = stripslashes($item_eng_name);
	$final_item_chi_discription = stripslashes($item_chi_discription);
	$final_item_eng_discription = stripslashes($item_eng_discription);
	$item_supplier = stripslashes($item_supplier);
	$item_supplier_contact = stripslashes($item_supplier_contact);
	$item_supplier_description = stripslashes($item_supplier_description);
	$item_brand = stripslashes($item_brand);
	$item_maintain_info = stripslashes($item_maintain_info);
	$item_remark = stripslashes($item_remark);
	$item_quotation = stripslashes($item_quotation);
	$item_tender = stripslashes($item_tender);
	$item_invoice = stripslashes($item_invoice);
	*/
	
	/*
	# Upload Attachment #
	$re_attach_path = "/file/inventory/attachment/";
	$attach_path = "$intranet_root$re_attach_path";
	$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment";
	
	for($k=0; $k<$attachment_size; $k++)
	{
	    $attach_file = stripslashes(${"hidden_item_attachment_".$k});
	    $re_attach_file = stripslashes(${"item_attachment_".$k});
		$target_attachment = "";
				
		if($re_attach_file == "none" || $re_attach_file == ""){
		}
		else
		{
			$lf = new libfilesystem();
			if (!is_dir($attach_path))
			{
				$lf->folder_new($attach_path);
			}
						
			$target_attachment = "$attach_path/$attach_file";
			$attachment .= "$attach_file";			
			
			$tmp_attachment = "$tmp_attach_path/$attach_file";
			
			if($lf->lfs_move($tmp_attachment, $target_attachment) == 1)
			{
				$attachment = array();
				$attachment[$i]['FileName'] = $attachementname;
				$attachment[$i]['Path'] = $re_attach_path;
			}
		}
	}
	*/
	
	/* 	### refer case 2011-0527-1216-26067
	if ($item_total_qty>1)
	{
		# For single item, purchase price = purchase price / quantity
		$item_purchase_price = round($item_purchase_price / $item_total_qty, 2);
	}
	*/

	for($i=0; $i<$item_total_qty; $i++)
	{
		# insert a new record for a new single item
		$sql = "INSERT INTO 
					INVENTORY_ITEM 
					(ItemType, CategoryID, Category2ID, NameChi, NameEng, DescriptionChi, DescriptionEng, 
					ItemCode, Ownership, PhotoLink, StockTakeOption, RecordType, RecordStatus, DateInput, DateModified, CreatedBy)
				VALUES
					($targetItemType, $targetCategory, $targetCategory2, '$final_item_chi_name', '$final_item_eng_name', '$final_item_chi_discription', '$final_item_eng_discription',
					'${"item_code_$i"}', $item_ownership, '', '$StockTakeOption', 0, 1, NOW(), NOW(), '$UserID')
				";
		$result['NewSingleItem'] = $linventory->db_db_query($sql) or die(mysql_error()); 
		
		if(in_array(false,$result))
		{
			header("location: items_full_list.php?msg=12");
			exit();
		}

		# get the single item id
		$tmp_item_id = $linventory->db_insert_id();
		$linventory->saveEntryTag($tmp_item_id, $Tags);
		
		# insert the photo for the new single item
		if($item_photo != "")
		{
			$target = "";
			
			if($item_photo=="none" || $item_photo== ""){
			}
			else
			{
				$lf = new libfilesystem();
				if (!is_dir($path))
				{
					$lf->folder_new($path);
				}
				
				if ($extUC == ".JPG" || $extUC == ".GIF" || $extUC == ".PNG")
				{
					$filename = "";
					# check if item_code with "/" char which not support in filename
					if(strpos(${item_code_.$i}, "/") !== false)
					{
						$new_photo_code = str_replace("/","_", ${item_code_.$i});
						$filename = $new_photo_code."_". $tmp_item_id .$ext;
					}
					else
					{
						$filename = ${item_code_.$i}.$ext;
					}
					$target = $path.$filename;		
					$lf->lfs_copy($tmp_file, $target);
				}
			}
			
			$sql = "INSERT INTO 
							INVENTORY_PHOTO_PART 
							(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
							($targetCategory, $targetCategory2, $tmp_item_id, '$re_path', '".$filename."', NOW(), NOW())
					";
			$linventory->db_db_query($sql) or die(mysql_error()); 
			$tmp_photo_link = $linventory->db_insert_id();
			
			$sql = "UPDATE 
						INVENTORY_ITEM
					SET
						PhotoLink = $tmp_photo_link
					WHERE
						ItemID = $tmp_item_id
					";
			$linventory->db_db_query($sql) or die(mysql_error()); 
		}
		
		# insert ext info for the new single item
		/*
		$item_supplier = stripslashes($item_supplier);
		$item_supplier_contact = stripslashes($item_supplier_contact);
		$item_supplier_description = stripslashes($item_supplier_description);
		$item_brand = stripslashes($item_brand);
		$item_maintain_info = stripslashes($item_maintain_info);
		$item_remark = stripslashes($item_remark);
		*/
		
		$item_barcode = strtoupper(${"item_barcode_$i"});
		if(!$item_funding2)
		{
			$item_funding2 = 0;
			$item_unit_price2 = 0; 
			$item_unit_price = $item_unit_price1;
		}
		 
		$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_EXT
					(ItemID, PurchaseDate, PurchasedPrice, UnitPrice, ItemRemark, SupplierName, SupplierContact, SupplierDescription, 
					InvoiceNo, QuotationNo, TenderNo, TagCode, Brand, GroupInCharge, LocationID, 
					FundingSource, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, MaintainInfo,
					UnitPrice1, UnitPrice2,FundingSource2)
				VALUES
					($tmp_item_id, '$item_purchase_date', '$item_purchase_price', '$item_unit_price', '$item_remark', '$item_supplier', '$item_supplier_contact', '$item_supplier_description',
					'$item_invoice', '$item_quotation', '$item_tender', '$item_barcode', '$item_brand', ${"targetGroup_$i"}, ${"targetLocation_$i"},
					$item_funding, '${"item_warranty_expiry_date_$i"}', '${"item_license_$i"}', '${"item_serial_$i"}', '$item_maintain_info',
					'$item_unit_price1', '$item_unit_price2',$item_funding2)
				";
		$linventory->db_db_query($sql) or die(mysql_error());
		
		# insert log record for a new signle item
		$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, NewStatus, PersonInCharge, Remark, 
					RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($tmp_item_id, '$curr_date', ".ITEM_ACTION_PURCHASE.", ".ITEM_STATUS_NORMAL.", $UserID, '',
					0, 0, NOW(), NOW())
				";
		$linventory->db_db_query($sql) or die(mysql_error()); 
		
		
		# Upload Attachment #
		$re_attach_path = "/file/inventory/attachment/".$tmp_item_id."/";
		$attach_path = "$intranet_root$re_attach_path";
		$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment/".$UserID."/";
		
		for($k=0; $k<$attachment_size; $k++)
		{
// 		    $attach_file = stripslashes(${"hidden_item_attachment_".$k});
// 		    $re_attach_file = stripslashes(${"item_attachment_".$k});
		    $attach_file = ${"hidden_item_attachment_".$k};
		    $re_attach_file = ${"item_attachment_".$k};

			$target_attachment = "";
			
			if($re_attach_file == "none" || $re_attach_file == ""){
			}
			else
			{
				$lf = new libfilesystem();
				if (!is_dir($attach_path))
				{
					$lf->folder_new($attach_path);
				}
							
				$target_attachment = "$attach_path/$attach_file";
				$attachment = "$attach_file";			
				$tmp_attachment = "$tmp_attach_path/$attach_file";
				
				$tmp_attachment = stripslashes($tmp_attachment);
				$target_attachment = stripslashes($target_attachment);
				
				if($lf->lfs_copy($tmp_attachment, $target_attachment) == 1)
				{
					$attachment = array();
					$attachment[$i]['FileName'] = $attachementname;
					$attachment[$i]['Path'] = $re_attach_path;
				}
			}
		}
		
		# insert attachment into single item
		for($j=0; $j<$attachment_size; $j++)
		{		
// 			$attach_file = stripslashes(${"hidden_item_attachment_$j"});
// 			$re_attach_file = stripslashes(${"item_attachment_$j"});
			$attach_file = ${"hidden_item_attachment_$j"};
			$re_attach_file = ${"item_attachment_$j"};

			if($re_attach_file != "")
			{
				$sql = "INSERT INTO 
							INVENTORY_ITEM_ATTACHMENT_PART
							(ItemID, AttachmentPath, FileName, DateInput, DateModified)
						VALUES
							($tmp_item_id, '$re_attach_path', '$attach_file', NOW(), NOW())
						";
// 						debug_pr($sql);
				$linventory->db_db_query($sql) or die(mysql_error()); 
			}
		}
	}
	
	## remove attachment from temp folder
	for($k=0; $k<$attachment_size; $k++)
	{
// 	    $attach_file = stripslashes(${"hidden_item_attachment_".$k});
	    $attach_file = ${"hidden_item_attachment_".$k};
		$tmp_attachment = "$tmp_attach_path/$attach_file";
		$lf->lfs_remove($tmp_attachment);
	}
}

## For eBooking Used ##
/*
if($targetItemType == ITEM_TYPE_SINGLE)
{	
	if($SetAsResourceItem == 1)
	{
		for($i=0; $i<sizeof($arrResourceItem); $i++)
		{
			$ResourceCode = intranet_htmlspecialchars(trim(${"resource_code_$i"}));
			//$ResourceCategory = intranet_htmlspecialchars(trim(${"resource_category_$i"}));
			$ResourceCategory = trim(${"resource_category_$i"});
			$Title = intranet_htmlspecialchars(trim(${"resource_name_$i"}));
			$Description = intranet_htmlspecialchars(trim(${"resource_description_$i"}));
			$Remark = intranet_htmlspecialchars(trim(${"resource_remark_$i"}));
			$Attachment = intranet_htmlspecialchars(trim($Attachment));
			$daysbefore = intranet_htmlspecialchars(trim(${"day_in_advance_$i"}));
			$batchID = intranet_htmlspecialchars(trim(${"batchID_$i"}));
			$DefaultStatus = intranet_htmlspecialchars(trim(${"auto_approve_$i"}));
			if ($DefaultStatus == 'yes')
			    $RecordType = 4;
			else $RecordType = 0;
			$resource_status = ${"resource_status_$i"};
			
			$sql = "INSERT INTO INTRANET_RESOURCE (ResourceCode, ResourceCategory, Title, Description, Remark, Attachment,RecordType, RecordStatus, DateInput, DateModified, DaysBefore, TimeSlotBatchID) VALUES ('$ResourceCode', '$ResourceCategory', '$Title', '$Description', '$Remark', '$Attachment','$RecordType', '$resource_status', now(), now(),'$daysbefore','$batchID')";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
			$ResourceID = $linventory->db_insert_id();
			
			for($j=0; $j<sizeof(${"GroupID_$i"}); $j++){
			    $sql = "INSERT INTO INTRANET_GROUPRESOURCE (GroupID, ResourceID) VALUES (".${"GroupID_$i"}[$j].", $ResourceID)";
			    //echo $sql."<BR>";
			    $linventory->db_db_query($sql);
			}
			
			$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$ResourceCode'";
			//echo $sql."<BR>";
			$arr_item_id = $linventory->returnVector($sql);
			if(sizeof($arr_item_id)>0)
			{
				$item_id = $arr_item_id[0];
			}
			
			$sql = "INSERT INTO INTRANET_RESOURCE_INVENTORY_ITEM_LIST (ResourceID, ItemID) VALUES ($ResourceID, $item_id)";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
		}
	}
}
*/
## End ##

if($targetItemType == ITEM_TYPE_BULK)
{
	if($purchase_type == 1)		# New Item
	{
		//echo "New Item<BR><BR>";
		# insert new bulk item 
		/*
		$final_item_chi_name = stripslashes($item_chi_name);
		$final_item_eng_name = stripslashes($item_eng_name);
		$final_item_chi_discription = stripslashes($item_chi_discription);
		$final_item_eng_discription = stripslashes($item_eng_discription);
		
		$item_supplier = stripslashes($item_supplier);
		$item_supplier_contact = stripslashes($item_supplier_contact);
		$item_supplier_description = stripslashes($item_supplier_description);
		$item_maintain_info = stripslashes($item_maintain_info);
		
		$item_supplier = stripslashes($item_supplier);
		$item_supplier_contact = stripslashes($item_supplier_contact);
		$item_supplier_description = stripslashes($item_supplier_description);
		$item_maintain_info = stripslashes($item_maintain_info);
		*/
		$sql = "INSERT INTO 
					INVENTORY_ITEM 
					(ItemType, CategoryID, Category2ID, NameChi, NameEng, DescriptionChi, DescriptionEng, 
					ItemCode, Ownership, PhotoLink, StockTakeOption, RecordType, RecordStatus, DateInput, DateModified, CreatedBy)
				VALUES
					($targetItemType, $targetCategory, $targetCategory2, '$final_item_chi_name', '$final_item_eng_name', '$final_item_chi_discription', '$final_item_eng_discription',
					'$item_code_0', $item_ownership, '', '$StockTakeOption', 0, 1, NOW(), NOW(),'$UserID')
				";
		$result['NewBulkItem'] = $linventory->db_db_query($sql) or die(mysql_error()); 
		
		if(in_array(false,$result))
		{
			header("location: items_full_list.php?msg=12");
			exit();
		}
		
		# get bulk item id
		$tmp_item_id = $linventory->db_insert_id();
		$linventory->saveEntryTag($tmp_item_id, $Tags);
		
		# insert the photo for the new single item
		if($item_photo != "")
		{
			# Upload Photo #
			$target = "";
			
			if($item_photo=="none" || $item_photo== ""){
			}
			else
			{
				$lf = new libfilesystem();
				if (!is_dir($path))
				{
					$lf->folder_new($path);
				}
				
				if ($extUC == ".JPG" || $extUC == ".GIF" || $extUC == ".PNG")
				{
		// 			$filename = $item_code_0.$ext;
					
					# check if item_code with "/" char which not support in filename
					if(strpos($item_code_0, "/") !== false)
					{
						$new_photo_code = str_replace("/","_", $item_code_0);
						$filename = $new_photo_code."_". $tmp_item_id .$ext;
					}
					else
					{
						$filename = $item_code_0.$ext;
					}
							
					$target = $path.$filename;		
					$lf->lfs_move($tmp_file, $target);
				}
			}
			# End of upload photo #
			
			$sql = "INSERT INTO 
							INVENTORY_PHOTO_PART 
							(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
							($targetCategory, $targetCategory2, $tmp_item_id, '$re_path', '$filename', NOW(), NOW())
					";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
			$tmp_photo_link = $linventory->db_insert_id();
			
			$sql = "UPDATE 
						INVENTORY_ITEM
					SET
						PhotoLink = $tmp_photo_link
					WHERE
						ItemID = $tmp_item_id
					";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
		}
		
		$item_barcode = strtoupper($item_barcode_0);
		
		# insert ext info into bulk item
		$bulk_item_admin = $bulk_item_admin ? $bulk_item_admin : $targetGroup_0;
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_EXT
					(ItemID, Quantity, BulkItemAdmin, Barcode)
				VALUES
					($tmp_item_id, $item_total_qty, '$bulk_item_admin', '$item_barcode')
				";
		$linventory->db_db_query($sql);
		
		if($item_funding == "")
		{
			$item_funding = $targetBulkFunding;
		}
		
		# insert record into bulk location
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOCATION
					(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
				VALUES
					($tmp_item_id, $targetGroup_0, $targetLocation_0, $item_funding, $item_total_qty)
				";
		$linventory->db_db_query($sql);
		
		# insert log record into bulk 
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyChange, QtyNormal, PurchaseDate, PurchasedPrice, FundingSource, 
					SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, 
					PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, 
					LocationID, GroupInCharge, UnitPrice, MaintainInfo)
				VALUES
					($tmp_item_id, '$curr_date', ".ITEM_ACTION_PURCHASE.", $item_total_qty, $item_total_qty, '$item_purchase_date', '$item_purchase_price', $item_funding,
					'$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_invoice', '$item_quotation', '$item_tender',
					$UserID, '$item_remark', '', 0, 0, NOW(), NOW(),
					$targetLocation_0, $targetGroup_0, $item_unit_price, '$item_maintain_info')
				";
		//echo $sql."<BR>";
		$linventory->db_db_query($sql);
	}
	else		# Exist Item
	{		
		//echo "Exist Item<BR><BR>";
		$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$exist_item_code'";
		$arr_exist_item_id = $linventory->returnVector($sql);
		$exist_item_id = $arr_exist_item_id[0];
		
		$item_funding = $item_funding ? $item_funding : $targetBulkFunding;
		
		# update total qty
		$sql = "UPDATE
					INVENTORY_ITEM_BULK_EXT
				SET
					Quantity = Quantity + $item_total_qty
				WHERE
					ItemID = $exist_item_id
				";
		$linventory->db_db_query($sql);
		
		/*
		$sql = "SELECT 
					RecordID 
				FROM 
					INVENTORY_ITEM_BULK_LOCATION 
				WHERE 
					ItemID = $exist_item_id AND 
					GroupInCharge = $targetGroup_0 AND 
					LocationID = $targetLocation_0
				";
		*/
		# new? update? for bluk_location
		$sql = "SELECT 
					RecordID 
				FROM 
					INVENTORY_ITEM_BULK_LOCATION 
				WHERE 
					ItemID = $exist_item_id AND 
					LocationID = $targetLocation_0 and
					GroupInCharge = $targetGroup_0 and
					FundingSourceID = $item_funding
				";
		$arr_rec_id = $linventory->returnVector($sql);
		if(sizeof($arr_rec_id)>0)
		{
			$rec_id = $arr_rec_id[0];
			$sql = "UPDATE
						INVENTORY_ITEM_BULK_LOCATION
					SET
						Quantity = Quantity + $item_total_qty
					WHERE
						RecordID = $rec_id
					";
			$linventory->db_db_query($sql);
			
			# retrieve funding data
			$sql = "select GroupInCharge, FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where RecordID=$rec_id";
			$result = $linventory->returnArray($sql);
			list($targetGroup_0, $targetBulkFunding) = $result[0];
		}
		else
		{
			if($item_funding == "")
			{
				$item_funding = $targetBulkFunding;
			}
		
			$sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOCATION
						(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
					VALUES
						($exist_item_id, $targetGroup_0, $targetLocation_0, $item_funding, $item_total_qty)
					";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
		}
		
		/*
		$item_supplier = stripslashes($item_supplier);
		$item_supplier_contact = stripslashes($item_supplier_contact);
		$item_supplier_description = stripslashes($item_supplier_description);
		$item_maintain_info = stripslashes($item_maintain_info);
		*/
// 		if($item_funding == "")
// 		{
// 			$item_funding = $targetBulkFunding;
// 		}
		
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyChange, QtyNormal, PurchaseDate, PurchasedPrice, FundingSource, 
					SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, 
					PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, 
					LocationID, GroupInCharge, UnitPrice, MaintainInfo)
				VALUES
					($exist_item_id, '$curr_date', ".ITEM_ACTION_PURCHASE.", $item_total_qty, $item_total_qty, '$item_purchase_date', '$item_purchase_price', $item_funding,
					'$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_invoice', '$item_quotation', '$item_tender',
					$UserID, '$item_remark', '', 0, 0, NOW(), NOW(),
					$targetLocation_0, $targetGroup_0, $item_unit_price, '$item_maintain_info')
				";
		$linventory->db_db_query($sql) or die(mysql_error());
		
		# insert attachment into bulk item
		for($i=0; $i<$no_file; $i++)
		{
// 			$attach_file = stripslashes(${"hidden_item_attachment_$i"});
// 			$re_attach_file = stripslashes(${"item_attachment_$i"});
			$attach_file = ${"hidden_item_attachment_$i"};
			$re_attach_file = ${"item_attachment_$i"};

			if($attach_file != "")
			{
				$sql = "INSERT INTO 
							INVENTORY_ITEM_ATTACHMENT_PART
							(ItemID, AttachmentPath, FileName, DateInput, DateModified)
						VALUES
							($exist_item_id, '$re_attach_path', '$attach_file', NOW(), NOW())
						";
				//echo $sql."<BR>";
				$linventory->db_db_query($sql);
			}
		}
	}
	
	# Upload Attachment #
	$no_file = 5;
	
	$tmp_item_id = $exist_item_id ? $exist_item_id : $tmp_item_id;
	
	$re_attach_path = "/file/inventory/attachment/".$tmp_item_id."/";
	$attach_path = "$intranet_root$re_attach_path";
// 	$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment";
	$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment/".$UserID."/";
	for($i=0; $i<$no_file; $i++)
	{
// 	    $attach_file = stripslashes(${"hidden_item_attachment_".$i});
// 	    $re_attach_file = stripslashes(${"item_attachment_".$i});
	    $attach_file = ${"hidden_item_attachment_".$i};
	    $re_attach_file = ${"item_attachment_".$i};
		$target_attachment = "";
		
		if($re_attach_file == "none" || $re_attach_file == ""){
		}
		else
		{
			$lf = new libfilesystem();
			if (!is_dir($attach_path))
			{
				$lf->folder_new($attach_path);
			}
						
			$target_attachment = "$attach_path/$attach_file";
			$attachment .= "$attach_file";			
			$tmp_attachment = "$tmp_attach_path/$attach_file";
			
			$tmp_attachment = stripslashes($tmp_attachment);
			$target_attachment = stripslashes($target_attachment);
				
			if($lf->lfs_move($tmp_attachment, $target_attachment) == 1)
			{
				$attachment = array();
				$attachment[$i]['FileName'] = $attachementname;
				$attachment[$i]['Path'] = $re_attach_path;
			}
		}
	}
	# End of upload attachment #
	
	# insert attachment into bulk item
	for($i=0; $i<$no_file; $i++)
	{		
// 		$attach_file = stripslashes(${"hidden_item_attachment_$i"});
// 		$re_attach_file = stripslashes(${"item_attachment_$i"});
		$attach_file = ${"hidden_item_attachment_$i"};
		$re_attach_file = ${"item_attachment_$i"};

		if($re_attach_file != "")
		{
			$sql = "INSERT INTO 
						INVENTORY_ITEM_ATTACHMENT_PART
						(ItemID, AttachmentPath, FileName, DateInput, DateModified)
					VALUES
						($tmp_item_id, '$re_attach_path', '$attach_file', NOW(), NOW())
					";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
		}
	}
	
}

# remove temp image file
if($tmp_path)
{
	$lf->folder_remove_recursive($tmp_path);
}
	
## remove tmp attachment
if($attachment_size)
{
	$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment/".$UserID."/";
	for($k=0; $k<$attachment_size; $k++)
	{
		$attach_file = ${"hidden_item_attachment_".$k};
		$tmp_attachment = "$tmp_attach_path/$attach_file";
		$tmp_attachment = stripslashes($tmp_attachment);
		$lf->lfs_remove($tmp_attachment);
	}
}


header("location: items_full_list.php?msg=1");
intranet_closedb();
?>