<?php
/**
 * **************************** Change Log [Start] *******************************
 * 
 * 2019-03-26 Henry: bug fix for canont export single item if when there is keyword search
 * 2018-04-09 Isaac: Fixed sql conditions for both $targetItemType != 2 and $targetItemType != 1 cases when there is search keyword.
 *
 * ****************************** Change Log [End] ********************************
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$lexport = new libexporttext();
$linventory = new libinventory();

// ## Set SQL Condition ##

if ($BuildingSelected == "") {
    $sql = "SELECT
					LocationID
			FROM
					INVENTORY_LOCATION
			WHERE
					RecordStatus = 1
			ORDER BY
					LocationLevelID, LocationID";
    $result = $linventory->returnVector($sql);
} else {
    if ($FloorSelected == "") {
        $sql = "SELECT
        c.LocationID
        FROM
        INVENTORY_LOCATION_BUILDING AS a INNER JOIN
        INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
        INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
        WHERE
        a.BuildingID = $BuildingSelected AND
        a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
        $result = $linventory->returnVector($sql);
    } else {
        if ($RoomSelected == "") {
            $sql = "SELECT
            c.LocationID
            FROM
            INVENTORY_LOCATION_BUILDING AS a INNER JOIN
            INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
            INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
            WHERE
            a.BuildingID = $BuildingSelected AND
            b.LocationLevelID = $FloorSelected AND
            a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
                c.LocationID
                FROM
                INVENTORY_LOCATION_BUILDING AS a INNER JOIN
                INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
                INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
                WHERE
                a.BuildingID = $BuildingSelected AND
                a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        } else {
            
            $sql = "SELECT
            c.LocationID
            FROM
            INVENTORY_LOCATION_BUILDING AS a INNER JOIN
            INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
            INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
            WHERE
            a.BuildingID = $BuildingSelected AND
            b.LocationLevelID = $FloorSelected AND
            c.LocationID = $RoomSelected AND
            a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
                c.LocationID
                FROM
                INVENTORY_LOCATION_BUILDING AS a INNER JOIN
                INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
                INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
                WHERE
                a.BuildingID = $BuildingSelected AND
                b.LocationLevelID = $FloorSelected AND
                a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        }
    }
}
if (sizeof($result) > 0) {
    $location_list = implode(",", $result);
    
    $cond = " and (d.LocationID IN ($location_list)) ";
}

if ($location_list != "") {
    $location_condition1 = " AND d.LocationID IN ($location_list) ";
    // $location_condition2 = " AND g.LocationID IN ($location_list) ";
}
// ## End Of Location Condition ###

// ## Set SQL Condition - Keyword ###
if ($keyword != "") {
    if ((strpos($keyword, "*") == 0) && (strrpos($keyword, "*") == strlen($keyword) - 1)) {
        $barcode = substr($keyword, 1, strlen($keyword) - 2);
    }
    
    $keyword_cond_1 = " AND ((a.NameChi LIKE '%$keyword%' OR a.NameEng LIKE '%$keyword%') OR
    (a.ItemCode LIKE '%$keyword%') OR
    (b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
    (c.NameChi LIKE '%$keyword%' OR c.NameEng LIKE '%$keyword%') OR
    (d.TagCode LIKE '%$keyword%') OR
    (d.Brand LIKE '%$keyword%') OR
    (d.ItemRemark LIKE '%$keyword%') OR
    (a.DescriptionChi LIKE '%$keyword%') OR
    (a.DescriptionEng LIKE '%$keyword%')
    ) ";
    
    $keyword_cond_2 = " AND ((a.NameChi LIKE '%$keyword%' OR a.NameEng LIKE '%$keyword%') OR
    (a.ItemCode LIKE '%$keyword%') OR
    (b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
    (c.NameChi LIKE '%$keyword%' OR c.NameEng LIKE '%$keyword%') OR
    (bl.barcode LIKE '%$keyword%') OR
    (i.Remark LIKE '%$keyword%') OR
    (a.DescriptionChi LIKE '%$keyword%') OR
    (a.DescriptionEng LIKE '%$keyword%')
    ) ";
}

if ($targetGroup != "") {
    $cond_Group = " AND g.AdminGroupID IN ($targetGroup) ";
} else {
    $cond_Group = "";
}

if ($targetFunding != "") {;
    $cond_Funding = " AND h.FundingSourceID IN ($targetFunding) ";
} else {
    $cond_Funding = "";

}

if ($targetCategory != "") {
    $cond_Category = " AND b.CategoryID IN ($targetCategory) ";
} else {
    $cond_Category = "";
}

if ($targetSubCategory != "") {
    $cond_SubCategory = " AND c.Category2ID IN ($targetSubCategory) ";
} else {
    $cond_SubCategory = "";

}

if ($Tag != "") {
    $RecordIDbyTags_Arr = $linventory->getRecordIDsFromTagName($Tag);
    if (is_array($RecordIDbyTags_Arr) && sizeof($RecordIDbyTags_Arr) > 0) {
        $RecordIDbyTags = implode(",", $RecordIDbyTags_Arr);
        $cond_tag = " AND a.ItemID IN ({$RecordIDbyTags}) ";
    }
}

if (sizeof($ItemID) != 0 || $itemIDs != '') { // $itemIDs for search_result.php use
    
    if ($itemIDs != '' && sizeof($ItemID) == 0) {
        $ItemID = explode(',', $itemIDs);
    } else {
        // rebuild $ItemID
        $ItemID_New = array();
        foreach ($ItemID as $k => $d) {
            list ($item_id_temp, $item_locationid_temp) = explode(":", $d);
            $ItemID_New[] = $item_id_temp;
        }
        $ItemID = $ItemID_New;
    }
    
    if ($targetItemID != "") {
        $arr_itemID = explode(",", $targetItemID);
        $temp_arr = array_merge($arr_itemID, $ItemID);
        $targetItems = implode(",", $temp_arr);
        $cond_targetItem = " AND a.ItemID IN ($targetItems) ";
        $cond_targetItem2 = " AND i.ItemID IN ($targetItems) ";
    } else {
        $temp_arr = $ItemID;
        $targetItems = Implode(",", $temp_arr);
        $cond_targetItem = " AND a.ItemID IN ($targetItems) ";
        $cond_targetItem2 = " AND i.ItemID IN ($targetItems) ";
    }
} else {
    // # export all item ##
    $cond_targetItem = "";
}

if ($linventory->getAccessLevel($UserID) == 2) {
    $targetGroup = $linventory->getInventoryAdminGroup();
    $cond_targetGroup = " AND g.AdminGroupID IN ($targetGroup)";

} else {
    $cond_targetGroup = "";

}


// ## Set ecport headers
$exportColumnType1 = array(
    $i_InventorySystem_Item_Code,
    $Lang["eInventory"]["label"]["BarCode"],
    $Lang['eInventory']['ItemName'] ,
    $i_InventorySystem['Category'],
    $i_InventorySystem['SubCategory'],
    $i_InventorySystem_Item_Quot_Num,
    $i_InventorySystem_Item_Tender_Num,
    $i_InventorySystem_Item_Invoice_Num,
    $i_InventorySystem_Item_Supplier_Name,
    $i_InventorySystem_Item_Supplier_Contact,
    $i_InventorySystem_Item_Supplier_Description,
    $i_InventorySystem_Item_Purchase_Date,
    $i_InventorySystem_Item_Qty,
    $i_InventorySystem_Item_Price,
    $i_InventorySystem_Unit_Price,
    $i_InventorySystemItemMaintainInfo,
    $i_InventorySystem_Item_Remark,
);

$exportColumnType2 = array(
    $i_InventorySystem_Item_Code,
    $Lang["eInventory"]["label"]["BarCode"],
    $Lang['eInventory']['ItemName'] ,
    $i_InventorySystem['Category'],
    $i_InventorySystem['SubCategory'],
    $i_InventorySystem_Item_Quot_Num,
    $i_InventorySystem_Item_Tender_Num,
    $i_InventorySystem_Item_Invoice_Num,
    $i_InventorySystem_Item_Supplier_Name,
    $i_InventorySystem_Item_Supplier_Contact,
    $i_InventorySystem_Item_Supplier_Description,
    $i_InventorySystem_Item_Purchase_Date,
    $i_InventorySystem_Item_Qty,
    $i_InventorySystem_Item_Price,
    $i_InventorySystem_Unit_Price,
    $i_InventorySystemItemMaintainInfo,
    $i_InventorySystem_Item_Remark,
    $i_InventorySystem_Item_PIC
);

// # Set SQL Verables ##
$item_namefield1 = $linventory->getInventoryItemNameByLang("a.");
$item_namefield2 = $linventory->getInventoryItemNameByLang("b.");
$item_namefield3 = $linventory->getInventoryItemNameByLang("c.");

if($targetItemType != 2) {
    $sql = "SELECT 
					a.ItemCode,
                    d.TagCode,
					$item_namefield1,
					$item_namefield2,
					$item_namefield3,
                    d.QuotationNo,
					d.TenderNo,
					d.InvoiceNo,					
                    d.SupplierName,
					d.SupplierContact,
					d.SupplierDescription,
                    d.PurchaseDate,
                    1 as Quantity,
                    CONCAT('$',d.PurchasedPrice),
                    CONCAT('$',d.UnitPrice),
                    d.MaintainInfo,
                    d.ItemRemark
                                    
                                   
			FROM
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = 1 AND a.ItemType = 1) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID  AND a.RecordStatus = 1 AND a.ItemType = 1) 
                    INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS d ON (a.ItemID = d.ItemID $location_condition1) LEFT OUTER JOIN
                    INVENTORY_LOCATION AS e ON (d.LocationID = e.LocationID) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS f ON (e.LocationLevelID = f.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (building.BuildingID = f.BuildingID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS g ON (d.GroupInCharge = g.AdminGroupID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS h ON (d.FundingSource = h.FundingSourceID)
					LEFT JOIN INVENTORY_FUNDING_SOURCE AS h2 ON (d.FundingSource2 = h2.FundingSourceID)
					
            WHERE
                    a.ItemType = 1
                    $cond					
                    $keyword_cond_1
					$cond_targetItem
					$cond_targetGroup
					$cond_Location
					$cond_SubLocation
					$cond_Building
					$cond_Group
					$cond_Funding
					$cond_Category
					$cond_SubCategory
					$cond_tag
					
			ORDER BY
					a.ItemCode"
					;
    $resultType1 = $linventory->returnArray($sql);
    $export_content1 = $lexport->GET_EXPORT_TXT($resultType1, $exportColumnType1, "\t", "\n", "\t", 0, "11");
//     debug_pr($export_content1);
}

if ($targetItemType != 1){
    $sql = "SELECT
					a.ItemCode,
                    bl.barcode,
					$item_namefield1,
					$item_namefield2,
					$item_namefield3,
                    i.QuotationNo, 
					i.TenderNo,
					i.InvoiceNo, 
					i.SupplierName, 
					i.SupplierContact, 
					i.SupplierDescription,  
					i.PurchaseDate,
                    i.QtyChange, 
                    i.PurchasedPrice,
                    i.UnitPrice,
					i.MaintainInfo, 
					i.Remark,
                    if (j.UserID IS NOT NULL, IF(j.EnglishName IS NULL OR TRIM(j.EnglishName) = '',j.ChineseName,j.EnglishName), if(ij.UserID IS NOT NULL, IF(ij.EnglishName IS NULL OR TRIM(ij.EnglishName) = '',ij.ChineseName,ij.EnglishName), ''))
					
    
    
            FROM
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = 1 AND a.ItemType = 2) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID  AND a.RecordStatus = 1 AND a.ItemType = 2) 
                    INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID ) LEFT OUTER JOIN
                    INVENTORY_LOCATION AS e ON (d.LocationID = e.LocationID) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS f ON (e.LocationLevelID = f.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (building.BuildingID = f.BuildingID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS g ON (d.GroupInCharge = g.AdminGroupID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS h ON (d.FundingSourceID = h.FundingSourceID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
                    Right JOIN INVENTORY_ITEM_BULK_LOG AS i on  (a.ItemID = i.ItemID)
                    left JOIN INTRANET_USER AS j ON (i.PersonInCharge = j.UserID)
					left JOIN INTRANET_ARCHIVE_USER as ij ON (i.PersonInCharge = ij.UserID)
       
            WHERE
                    a.ItemType = 2
                    AND i.Action = 1
                    $cond					
                    $keyword_cond_2
					$cond_targetItem
					$cond_targetGroup
					$cond_Location
					$cond_SubLocation
					$cond_Building
					$cond_Group
					$cond_Funding
					$cond_Category
					$cond_SubCategory
					$cond_tag
            
            GROUP BY 
                    i.RecordID       
            ORDER BY
                    a.ItemCode" ;
    
    $resultType2 = $linventory->returnArray($sql);
    
//     debug_pr($sql);
//     if ($targetItemType == 2){
//         unset($export_content1);
//     }
     $export_content2 = $lexport->GET_EXPORT_TXT($resultType2, $exportColumnType2, "\t", "\n", "\t", 0, "11");
//      debug_pr($export_content2);
} 



$filename = "invoice_details.csv";
if($export_content1!= null){
    $export_content1 = $export_content1."\n"."\"\""."\n";
}

$export_content = $export_content1.$export_content2;
// debug_pr($export_content);
$lexport->EXPORT_FILE($filename, $export_content);

?>