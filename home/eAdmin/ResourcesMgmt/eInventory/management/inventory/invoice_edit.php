<?php
// using : Henry

// ################################################
// 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date : 2019-05-01 (Henry)
// security issue fix for SQL
//
// Date : 2018-02-21 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2017-06-27 (Henry)
// display the name of left staff [Case#V119089]
//
// Date: 2017-05-16 (Henry)
// Fixed logic to edit bulk quantity [Case#E117018]
//
// Date: 2017-05-05 (Henry)
// fixed: no information when the pic is not existed [Case#E116558]
//
// Date: 2016-04-29 (Henry)
// Modified logic to edit bulk quantity [Case#94913]
//
// Date: 2014-12-23 Henry [Case#J73005]
// $sys_custom['eInventory']['PurchaseDateRequired'] > Purchase date cannot be empty
//
// Date: 2014-04-03 YatWoon
// [Customization for SKH]
// Purchase Date becomes Mandatory field
//
// Date: 2013-08-13 YatWoon
// Fixed: allow empty purchase date [Case#2013-0813-1520-12156]

// Date: 2013-05-02 (YatWoon)
// add $special_feature['eInventory']['CanEditBulkQty'], allow user update bulk item's qty
//
// Date: 2013-03-12 (YatWoon)
// $sys_custom['CatholicEducation_eInventory'] >>> allow edit qty, purchase price and unit price
//
// Date: 2012-12-06 (YatWoon)
// view mode for member
//
// Date: 2012-12-03 (YatWoon)
// Improved: cancel button return to details page
// Add "Maintenance Details" and "Remarks" for edit
//
// Date: 2012-03-30 YatWoon
// - Allow to edit tender, supplier's contact, supplier description, quo# [Case#2012-0316-1635-31073]
//
// Date: 2011-11-17 YatWoon
// - IP25 layout standard
// - Add a flag $sys_custom['eInventoryCanEditBulkInvoicePrice'] allow edit PurchasePrice and Unit Price [Case#2011-1108-1053-34073]
//
// ################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID);

$llocation_ui = new liblocation_ui();

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array(
    $i_InventorySystem_Item_Invoice,
    "category_show_items_detail.php?type=1&item_id=$ItemID"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit']
);

$sql = "SELECT CONCAT(ItemCode,' - '," . $linventory->getInventoryNameByLang() . ") FROM INVENTORY_ITEM WHERE ItemID = '".$ItemID."'";
$ItemName = $linventory->returnVector($sql);
$targetRecordID = $RecordID[0];

$sql = "SELECT 
			a.InvoiceNo, if(a.PurchaseDate='0000-00-00','',a.PurchaseDate), a.SupplierName, a.PurchasedPrice, a.UnitPrice, a.QtyChange, " . getNameFieldByLang2("b.") . ", " . getNameFieldByLang2("ab.") . "  
			,a.TenderNo, a.SupplierContact, a.SupplierDescription, a.QuotationNo,a.MaintainInfo, a.Remark, a.RecordStatus, " . getNameFieldByLang2("c.") . ", " . getNameFieldByLang2("ac.") . ", a.dateModified
		FROM 
			INVENTORY_ITEM_BULK_LOG as a
			left JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID)
			left JOIN INTRANET_ARCHIVE_USER as ab ON (a.PersonInCharge = ab.UserID)
			left JOIN INTRANET_USER AS c ON (c.UserID = a.DateModifiedBy)
			left JOIN INTRANET_ARCHIVE_USER as ac ON (ac.UserID = a.DateModifiedBy)
		WHERE 
			RecordID = '".$targetRecordID."'";
$result = $linventory->returnArray($sql);
if (! empty($result))
    list ($org_invoice_no, $org_purchase_date, $org_supplier_name, $org_price, $ori_unitprice, $qty, $pic, $pic2, $org_tender, $org_contact, $org_description, $org_quo, $main, $remark, $status, $modifiedBy, $modifiedBy2, $modifiedDate) = $result[0];

$canEdit = ($linventory->getAccessLevel($UserID) != 3) ? 1 : 0;

// check if the invoice record is removed, view only
$canEdit = $status == - 1 ? 0 : $canEdit;

$sql = "SELECT ItemID, GroupInCharge, LocationID, FundingSource FROM INVENTORY_ITEM_BULK_LOG WHERE RecordID >= $targetRecordID AND ItemID = '" . $ItemID . "' AND Action <> 11 ORDER BY RecordID";
$result = $linventory->returnVector($sql);
$canEditBulkQuantity = false;
if (count($result) < 2) {
    $canEditBulkQuantity = true;
}
?>

<script language="javascript">
<!--
function reset_innerHtml()
{
	<?if($sys_custom['eInventoryCanEditBulkInvoicePrice']) {?>
 	document.getElementById('div_UnitPrice_err_msg').innerHTML = "";
 	document.getElementById('div_PurchasePrice_err_msg').innerHTML = "";
 	<?} ?>
 	
 	document.getElementById('div_PurchaseDate_err_msg').innerHTML = "";
}

function check_form()
{
	var obj = document.form1;
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	<? if($sys_custom['eInventoryCanEditBulkInvoicePrice']) {?>
	// Purchase Price
	if(obj.PurchasePrice.value!='' && !checkUnitPrice(obj.PurchasePrice,obj.PurchasePrice.value))
	{
		document.getElementById('div_PurchasePrice_err_msg').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidPurchasePrice']?></font>";
		error_no++;
		if(focus_field=="")	focus_field = "PurchasePrice";
	}
	
	// Unit Price
	if(obj.UnitPrice.value!='' && !checkUnitPrice(obj.UnitPrice,obj.UnitPrice.value))
	{
		document.getElementById('div_UnitPrice_err_msg').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidUnitPrice']?></font>";
		error_no++;
		if(focus_field=="")	focus_field = "UnitPrice";
	}
	<? } ?>
	
	<? if($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']) { ?>
	if(obj.PurchaseDate.value=="")
	{
		document.getElementById('div_PurchaseDate_err_msg').innerHTML = "<font color=red><?=$i_alert_pleasefillin . $i_InventorySystem_Item_Purchase_Date;?></font>";
		error_no++;
		if(focus_field=="")	focus_field = "PurchaseDate";
	}
	<? } ?>
	
	
	if(obj.PurchaseDate.value != "")
	{
		if(!check_date_without_return_msg(obj.PurchaseDate)){
			error_no++;
			if(focus_field=="")	focus_field = "PurchaseDate";
		}
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		obj.submit();
	}
	
}

function checkUnitPrice(fieldName, fieldValue) {
	decallowed = 2;  // how many decimals are allowed?
	
	if (isNaN(fieldValue) || fieldValue == "") {
		return false;
	}
	else {
		if (fieldValue.indexOf('.') == -1) fieldValue += ".";
		dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);

		if (dectext.length > decallowed)
		{
			return false;
      	}
		else {
			return true;
      	}
   	}
}

//-->
</script>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<?=$linterface->GET_NAVIGATION2($ItemName[0])?>

<form name="form1" action="invoice_edit_update.php" method="post">
	<div class="form_table_content">
		<table class="form_table_v30">

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Quot_Num?></td>
				<td><?=$canEdit ? "<input type='text' name='item_quotation' class='textboxtext' value='$org_quo'>" : $org_quo?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Tender_Num?></td>
				<td><?=$canEdit ? "<input type='text' name='item_tender' class='textboxtext' value='$org_tender'>" : $org_tender?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Invoice_Num?></td>
				<td><?=$canEdit ? "<input type='text' name='InvoiceNo' class='textboxtext' value='$org_invoice_no'>" : $org_invoice_no?></td>
			</tr>

			<tr>
				<td class="field_title"><? if($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']) {?><span
					class="tabletextrequire">*</span> <? } ?> <?=$i_InventorySystem_Item_Purchase_Date?></td>
				<td><?=$canEdit ? $linterface->GET_DATE_PICKER('PurchaseDate',$DefaultValue=$org_purchase_date,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField= ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']? 0:1), $Disable=false, $cssClass="textboxnum")  : $org_purchase_date?> <span
					id="div_PurchaseDate_err_msg"></span></td>
			</tr>
			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Supplier_Name?></td>
				<td><?=$canEdit ? "<input type='text' name='SupplierName' class='textboxtext' value='$org_supplier_name'>" : $org_supplier_name?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Supplier_Contact?></td>
				<td><?=$canEdit ? "<input type='text' name='item_supplier_contact' class='textboxtext' value='$org_contact'>" : $org_contact?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Supplier_Description?></td>
				<td><?=$canEdit ? "<input type='text' name='item_supplier_description' class='textboxtext' value='$org_description'>" : $org_description?></td>
			</tr>

<? if($canEditBulkQuantity && ($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanEditBulkQty'])) { ?>
	<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Qty?></td>
				<td><?=$canEdit ? " <input type='text' name='QtyChange' class='textboxnum' value='$qty'> <span id='div_QtyChange_err_msg'></span>" : $qty?></td>
			</tr>

<? } else {?>
	<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Qty?></td>
				<td><?=$qty?></td>
			</tr>
<? } ?>

<? if($sys_custom['eInventoryCanEditBulkInvoicePrice'] || $sys_custom['CatholicEducation_eInventory']) { ?>
	<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Price?></td>
				<td>$<?=$canEdit ? " <input type='text' name='PurchasePrice' class='textboxnum' value='$org_price'> <span id='div_PurchasePrice_err_msg'></span>" : $org_price?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Unit_Price?></td>
				<td>$<?=$canEdit ? " <input type='text' name='UnitPrice' class='textboxnum' value='$ori_unitprice'> <span id='div_UnitPrice_err_msg'></span>" : $ori_unitprice?></td>
			</tr>
<? } else {?>
	<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Price?></td>
				<td>$<?=$org_price?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Unit_Price?></td>
				<td>$<?=$ori_unitprice?></td>
			</tr>
<? } ?>

<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
<tr>
				<td class="field_title"><?=$i_InventorySystemItemMaintainInfo?></td>
				<td colspan="3"><?=$canEdit ? $linterface->GET_TEXTAREA("item_maintain_info", $main) : nl2br($main)?></td>
			</tr>
<? } ?>

<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Remark?></td>
				<td colspan="3"><?=$canEdit ? $linterface->GET_TEXTAREA("item_remark", $remark) : nl2br($remark)?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_PIC?></td>
				<td><?=$pic?$pic:($pic2?'<font color="red">*</font>'.$pic2:$Lang['General']['UserAccountNotExists'])?></td>
			</tr>

<? if($status==-1) {?>
<tr>
				<td class="field_title"><?=$Lang['eInventory']['DeletedBy']?></td>
				<td><?=$modifiedBy?$modifiedBy:($modifiedBy2?'<font color="red">*</font>'.$modifiedBy2:$Lang['General']['UserAccountNotExists'])?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$Lang['eInventory']['DeletedDate']?></td>
				<td><?=$modifiedDate?></td>
			</tr>
<? } ?>

</table>

		<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
<? if($canEdit) {?>
<?=$linterface->GET_ACTION_BTN($button_submit, "button", "check_form();")?> 
<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='category_show_items_detail.php?type=1&item_id=".$ItemID."'")?> 
<? } else {?>
<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:window.location='category_show_items_detail.php?type=1&item_id=".$ItemID."'")?> 
<? } ?>

<p class="spacer"></p>
		</div>


	</div>

	<input type='hidden' name='RecordID' value='<?=$targetRecordID?>'> <input
		type='hidden' name='Type' value='<?=$Type?>'> <input type='hidden'
		name='ItemID' value='<?=$ItemID?>'>
</form>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>