<?php
// modifying :

// #### Change Log [Start] #####
//
// Date	: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date : 2018-02-21 (Henry)
// add access right checking [Case#E135442]
//
// Date : 2012-10-25 (YatWoon)
// add eBooking integration, send email to eBooking admin
//
// ##### Change Log [End] ######
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");
// $re_path = "/file/inventory/write_off";
// $path = "$intranet_root$re_path";

if ($item_type == 1) {
    if ($resource_status != "") {
        $sql = "SELECT ResourceID FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ItemID = '".$item_id."'";
        $arr_resource_id = $linventory->returnVector($sql);
        
        if (sizeof($arr_resource_id) > 0) {
            $resource_id = $arr_resource_id[0];
            
            $sql = "UPDATE INTRANET_RESOURCE SET RecordStatus = '".$resource_status."' WHERE ResourceID = '".$resource_id."'";
            $linventory->db_db_query($sql);
        }
    }
    $sql = "INSERT INTO 
				INVENTORY_ITEM_SINGLE_STATUS_LOG 
				(ItemID, RecordDate, Action, PastStatus, NewStatus, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
			VALUES
				($item_id, '$curr_date', " . ITEM_ACTION_UPDATESTATUS . ", '$single_item_past_status', '$single_item_status', $UserID, '$single_item_remark', '', '', NOW(), NOW())";
    $log_result = $linventory->db_db_query($sql);
    
    $status_remark = $single_item_remark;
    $item_status = $single_item_status;
}

if ($item_type == 2) {
    $sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, PersonInCharge, Remark,
					LocationID, GroupInCharge, FundingSource, DateInput, DateModified)
			VALUES
					($item_id, '$curr_date', " . ITEM_ACTION_UPDATESTATUS . ",$bulk_item_normal_qty,$bulk_item_damage_qty,$bulk_item_repair_qty,$UserID,'$bulk_item_update_status_remark',
					$item_location_id,$item_group_id,$item_funding_id,NOW(),NOW())
			";
    $log_result = $linventory->db_db_query($sql);
    
    $status_remark = $bulk_item_update_status_remark;
    $item_status = $bulk_item_normal_qty . "-" . $bulk_item_damage_qty . "-" . $bulk_item_repair_qty;
}

if ($log_result == 1) {
    // eBooking integration - send email notification
    if ($item_type == 1) {
        $linventory->sendEmailNotification_eBooking($item_type, $item_id, $status_remark, $item_status);
    } else {
        $linventory->sendEmailNotification_eBooking($item_type, $item_id, $status_remark, $item_status, $item_location_id, $item_group_id, $item_funding_id);
    }
    
    header("location: items_full_list.php?msg=2");
} else {
    header("locattion: item_full_list.php?msg=13");
}

intranet_closedb();
?>