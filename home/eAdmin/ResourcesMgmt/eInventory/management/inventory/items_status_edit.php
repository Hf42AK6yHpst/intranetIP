<?
// using by:

// ################################
// Date: 2019-05-01 Henry
// security issue fix for SQL
//
// Date: 2018-02-21 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-01 Henry
// PHP 5.4 fix: change split to explode
//
// Date: 2015-04-16 Cameron
// - use double quote for string "$image_path/$LAYOUT_SKIN"
//
// Date : 2012-10-25 (YatWoon)
// add eBooking integration, display booking records data if necessary
//
// Date : 2012-04-02 (Henry Chow)
// prepare the admin group in hidden field, for the insertion of LOG
//
// Date: 2011-05-23 YatWoon
// display default normal, damage, repair value
// add "Location" selection
//
// Date: 2011-04-18 YatWoon
// update: Normal + Damaged + Reparing MUST BE equal to Quantity (Assigend)
//
// ################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$item_id = IntegerSafe($item_id);
$ItemID = IntegerSafe($ItemID,":");

if (! empty($ItemID)) {
    $item_id_temp = explode(":", $ItemID[0]);
    $item_id = $item_id_temp[0];
    $targetLocation = $item_id_temp[1];
    $targetGroupID = $item_id_temp[2];
    $targetFundingID = $item_id_temp[3];
}

// Generate Page Tag #
// $TAGS_OBJ[] = array($i_InventorySystem_Item_UpdateStatus, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_status_edit.php?item_id=$item_id&targetLocation=$targetLocation", 1);
// $TAGS_OBJ[] = array($i_InventorySystem_Item_UpdateLocation, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_location_edit.php?item_id=$item_id&targetOldLocation=$targetLocation", 0);
// $TAGS_OBJ[] = array($i_InventorySystem_Item_RequestWriteOff, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_request_write_off.php?item_id=$item_id&targetLocation=$targetLocation", 0);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_UpdateStatus,
    "javascript:click_tab('update_status');",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_UpdateLocation,
    "javascript:click_tab('change_location');",
    0
);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_RequestWriteOff,
    "javascript:click_tab('request_writeoff');",
    0
);

// End #

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Generate Page Info #
$sql = "SELECT 
				" . $linventory->getInventoryItemNameByLang("a.") . ",
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				" . $linventory->getInventoryItemNameByLang("c.") . "
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 3);

$temp[] = array(
    $result[0][1],
    ""
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";
$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION2($result[0][2] . " > " . $result[0][0] . " > " . $Lang['eInventory']['FieldTitle']['UpdateItemStatus']) . "</td></tr>";
// End #

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$arr_item_type = $linventory->returnVector($sql);
$item_type = $arr_item_type[0];

// $table_content .= '<div id="RemarkLayer" class="selectbox_layer" style="visibility:hidden; width:500px;">'."\n";
// $table_content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
// $table_content .= '<tbody>'."\n";
// $table_content .= '<tr>'."\n";
// $table_content .= '<td align="right" style="border-bottom: medium none;">'."\n";
// $table_content .= '<a href="javascript:js_Hide_Detail_Layer()"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
// $table_content .= '</td>'."\n";
// $table_content .= '</tr>'."\n";
// $table_content .= '<tr>'."\n";
// $table_content .= '<td align="left" style="border-bottom: medium none;">'."\n";
// $table_content .= '<div id="RemarkLayerContentDiv"></div>'."\n";
// $table_content .= '</td>'."\n";
// $table_content .= '</tr>'."\n";
// $table_content .= '</tbody>'."\n";
// $table_content .= '</table>'."\n";
// $table_content .= '</div>'."\n";

if ($item_type == 1) {
    // Get Item Past Status #
    $sql = "SELECT 
					a.ItemID, 
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					b.NewStatus,
					c.GroupInCharge,
					c.LocationID,
					b.Remark
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS c ON (a.ItemID = c.ItemID)
			WHERE
					a.ItemID = '".$item_id."' AND 
					b.Action IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
			ORDER BY
					b.RecordDate DESC, b.DateInput DESC
			LIMIT 
					0,1";
    $arr_result = $linventory->returnArray($sql);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_id, $item_name, $item_past_status, $item_group_id, $item_location_id, $item_remark) = $arr_result[$i];
            
            /*
             * # Get Default Write Off Reason #
             * $sql = "SELECT ReasonTypeID, ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE";
             * $arr_write_off_reason = $linventory->returnArray($sql,2);
             * $reason_selection = getSelectByArray($arr_write_off_reason, " name=\"single_item_write_off_reason\" DISABLED");
             * # End #
             */
            
            if (($item_past_status == 0) || ($item_past_status == ""))
                $str_item_past_status = " - ";
            if ($item_past_status == ITEM_STATUS_NORMAL)
                $str_item_past_status = $i_InventorySystem_ItemStatus_Normal;
            if ($item_past_status == ITEM_STATUS_DAMAGED)
                $str_item_past_status = $i_InventorySystem_ItemStatus_Damaged;
            if ($item_past_status == ITEM_STATUS_REPAIR)
                $str_item_past_status = $i_InventorySystem_ItemStatus_Repair;
            
            $new_status_selection = getSelectByArray($i_InventorySystem_ItemStatus_Array, " name=\"single_item_status\" ", $single_item_status);
            
            $sql = "SELECT ResourceID FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ItemID = '".$item_id."'";
            $arr_is_resource_item = $linventory->returnVector($sql);
            if (sizeof($arr_is_resource_item) > 0) {
                $resource_id = $arr_is_resource_item[0];
                
                if ($single_item_status == "") {
                    $sql = "SELECT RecordStatus FROM INTRANET_RESOURCE WHERE ResourceID = '".$resource_id."'";
                    $arr_resource_status = $linventory->returnVector($sql);
                    
                    if (sizeof($arr_resource_status) > 0) {
                        $resource_status = $arr_resource_status[0];
                        if ($resource_status == 1) {
                            $publish_checked = " CHECKED ";
                            $pending_checked = " ";
                        } else {
                            $publish_checked = " ";
                            $pending_checked = " CHECKED ";
                        }
                    }
                } else 
                    if (($single_item_status == 1) || ($single_item_status == 3)) {
                        $publish_checked = " CHECKED ";
                        $pending_checked = " ";
                    } else 
                        if ($single_item_status == 2) {
                            $publish_checked = " ";
                            $pending_checked = " CHECKED ";
                        }
                $resource_status_selection = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateStatus_ResourceItemStatus</td>";
                $resource_status_selection .= "<td><label class=\"tabletext\"><input type=\"radio\" name=\"resource_status\" value=\"1\" $publish_checked>&nbsp;$i_status_publish</label>&nbsp;<label class=\"tabletext\"><input type=\"radio\" name=\"resource_status\" value=\"0\" $pending_checked>&nbsp;$i_status_pending</label></td></tr>";
            }
            $table_content .= "<tr><td width=\"40%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Name</td><td class=\"tabletext\">$item_name</td></tr>";
            $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_StockCheck_LastStatus</td><td class=\"tabletext\">$str_item_past_status</td></tr>";
            $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_StockCheck_NewStatus</td>";
            $table_content .= "<input type=\"hidden\" name=\"single_item_past_status\" value=\"$item_past_status\">";
            $table_content .= "<td class=\"tabletext\">$new_status_selection</td></tr>";
            $table_content .= $resource_status_selection;
            $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Remark</td><td class=\"tabletext\"><textarea name=\"single_item_remark\" class=\"textboxtext\" rows=\"2\" cols=\"50\">" . $item_remark . "</textarea></td></tr>";
            
            // for eBooking integration
            if ($plugin['eBooking']) {
                include_once ($PATH_WRT_ROOT . "includes/libebooking.php");
                $lebooking = new libebooking();
                $bookingRecordAry = $lebooking->returnItemBookingRecords($item_type, $item_id);
                
                if (sizeof($bookingRecordAry) > 0) {
                    $thisLayerID = 'RemarkLinkDiv_' . $item_id;
                    $DataStr = $item_type . "-" . $item_id;
                    $remarkDisplay = "<a href='javascript: js_Show_Detail_Layer2(\"ajax_load_form.php\",\"BookingStatusList\",\"$thisLayerID\", \"$DataStr\");'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0'></a>";
                }
                
                if ($lebooking->returnIsItemAllowBooking($item_type, $item_id) || sizeof($bookingRecordAry) > 0) {
                    $table_content .= "<tr>";
                    $table_content .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['BookingRecords'] . "</td>";
                    $table_content .= "<td>" . sizeof($bookingRecordAry) . " " . $Lang['eInventory']['BookingRecordsNo'] . " " . $remarkDisplay;
                    
                    // Remark Layer
                    $table_content .= '<div id="RemarkLayer" class="selectbox_layer" style="visibility:hidden; width:500px;">' . "\n";
                    $table_content .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">' . "\n";
                    $table_content .= '<tbody>' . "\n";
                    $table_content .= '<tr>' . "\n";
                    $table_content .= '<td align="right" style="border-bottom: medium none;">' . "\n";
                    $table_content .= '<a href="javascript:js_Hide_Detail_Layer()"><img border="0" src="' . $PATH_WRT_ROOT . $image_path . '/' . $LAYOUT_SKIN . '/ecomm/btn_mini_off.gif"></a>' . "\n";
                    $table_content .= '</td>' . "\n";
                    $table_content .= '</tr>' . "\n";
                    $table_content .= '<tr>' . "\n";
                    $table_content .= '<td align="left" style="border-bottom: medium none;">' . "\n";
                    $table_content .= '<div id="RemarkLayerContentDiv"></div>' . "\n";
                    $table_content .= '</td>' . "\n";
                    $table_content .= '</tr>' . "\n";
                    $table_content .= '</tbody>' . "\n";
                    $table_content .= '</table>' . "\n";
                    $table_content .= '</div>' . "\n";
                    
                    $table_content .= "<br><span class=\"tabletextremark\">(" . $Lang['eInventory']['UpdateItemStatusEmailToeBookingAdminRemark'] . ")</span>";
                    $table_content .= "</td></tr>";
                }
            }
        }
        $table_content .= "<input type=\"hidden\" name=\"item_id\" value=$item_id>\n";
        $table_content .= "<input type=\"hidden\" name=\"item_group_id\" value=$item_group_id>\n";
        $table_content .= "<input type=\"hidden\" name=\"item_location_id\" value=$item_location_id>\n";
    }
}

if ($item_type == 2) {
    // get Item Location #
    if ($linventory->IS_ADMIN_USER($UserID)) {
        $sql = "SELECT 
						DISTINCT b.LocationID, 
						CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
				FROM 
						INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
						INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
						INVENTORY_LOCATION_LEVEL AS LocFloor ON (b.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID)
				WHERE
						a.ItemID = '".$item_id."'
				order by LocBuilding.DisplayOrder, LocFloor.DisplayOrder, b.DisplayOrder
				";
    } else {
        $admin_group_list = $linventory->getInventoryAdminGroup();
        $sql = "SELECT 
						b.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
				FROM
						INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
						INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
						INVENTORY_LOCATION_LEVEL AS LocFloor ON (b.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID)
				WHERE
						ItemID = '".$item_id."' AND
						GroupInCharge IN ($admin_group_list)
				order by LocBuilding.DisplayOrder, LocFloor.DisplayOrder, b.DisplayOrder
				";
    }
    $arr_location = $linventory->returnArray($sql, 2);
    
    // $location_selection = getSelectByArray($arr_location," name=\"targetLocation\" onChange=\"this.form.flag.value=0; this.form.submit();\" ", "$targetLocation", 0, 1);
    $location_selection = getSelectByArray($arr_location, " name=\"targetLocation\" onChange=\"change_filter('location');\" ", "$targetLocation", 0, 1);
    // End #
    
    // group #
    if (! $targetGroupID) {
        // retrieve the first groupid
        $check_own_group = empty($admin_group_list) ? "" : " and GroupInCharge in ($admin_group_list)";
        $sql = "select GroupInCharge from INVENTORY_ITEM_BULK_LOCATION where LocationID = '".$targetLocation."' and ItemID = '".$item_id."' $check_own_group limit 1";
        $result = $linventory->returnVector($sql);
        $targetGroupID = $result[0];
    }
    $check_own_group = empty($admin_group_list) ? "" : " and c.GroupInCharge in ($admin_group_list)";
    $sql = "SELECT 
				distinct(a.AdminGroupID), 
				" . $linventory->getInventoryNameByLang() . " as GroupName
			FROM 
				INVENTORY_ADMIN_GROUP as a 
				left join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
				left join INVENTORY_ITEM_BULK_LOCATION as c on (c.GroupInCharge=a.AdminGroupID and c.LocationID = '".$targetLocation."')
			WHERE 
				c.ItemID = '".$item_id."'
				$check_own_group
			 ORDER BY 
			 	a.DisplayOrder
			";
    
    $group_array = $linventory->returnArray($sql);
    $group_selection = getSelectByArray($group_array, "name=targetGroupID onChange=\"change_filter('group');\" ", $targetGroupID, 0, 1);
    // end group #
    
    // funding #
    if (! $targetFundingID) {
        // retrieve the first groupid
        $sql = "select 
					a.FundingSourceID 
				from 
					INVENTORY_ITEM_BULK_LOCATION as a
					left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSourceID
				where 
					a.LocationID = '".$targetLocation."' and 
					a.ItemID = '".$item_id."' and 
					a.GroupInCharge = '".$targetGroupID."' 
				ORDER BY 
					b.DisplayOrder 
				limit 1
				";
        $result = $linventory->returnVector($sql);
        $targetFundingID = $result[0];
    }
    
    $sql = "SELECT 
				a.FundingSourceID, 
				" . $linventory->getInventoryItemNameByLang() . "
			FROM 
				INVENTORY_FUNDING_SOURCE as a
				left join INVENTORY_ITEM_BULK_LOCATION as b on (b.FundingSourceID=a.FundingSourceID and b.LocationID = $targetLocation and b.GroupInCharge=$targetGroupID)
			where 	
				b.ItemID = '".$item_id."'
			order by 
				a.DisplayOrder";
    $arr_funding_source = $linventory->returnArray($sql);
    $funding_selection = getSelectByArray($arr_funding_source, " name=\"targetFundingID\" onChange=\"change_filter('funding');\" ", $targetFundingID, 0, 1);
    // end funding #
    
    /*
     * # get Admin Group Of The User#
     * if($targetLocation != "")
     * {
     * $admin_group_list = $linventory->getInventoryAdminGroup();
     *
     * if($linventory->IS_ADMIN_USER($UserID))
     * {
     * $sql = "SELECT
     * b.AdminGroupID, ".$linventory->getInventoryNameByLang("b.")."
     * FROM
     * INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
     * INVENTORY_ADMIN_GROUP AS b ON (a.GroupInCharge = b.AdminGroupID)
     * WHERE
     * a.ItemID = $item_id AND
     * a.LocationID = $targetLocation
     * ";
     * }
     * else
     * {
     * $sql = "SELECT
     * b.AdminGroupID, ".$linventory->getInventoryNameByLang("b.")."
     * FROM
     * INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
     * INVENTORY_ADMIN_GROUP AS b ON (a.GroupInCharge IN ($admin_group_list))
     * WHERE
     * a.ItemID = $item_id AND
     * a.LocationID = $targetLocation
     * ";
     * }
     * $arr_admin = $linventory->returnArray($sql,2);
     *
     * $group_selection = getSelectByArray($arr_admin," name=\"targetGroup\" onChange=\"this.form.flag.value=0; this.form.submit(); \" ", "$targetGroup" ,0,0);
     * # End #
     * }
     */
    
    // Get Assigned Item Qty #
    /*
     * $sql = "SELECT
     * a.ItemID,
     * ".$linventory->getInventoryItemNameByLang("a.").",
     * b.GroupInCharge,
     * ".$linventory->getInventoryNameByLang("c.").",
     * b.LocationID,
     * CONCAT(".$linventory->getInventoryNameByLang("LocBuilding.").",' > ',".$linventory->getInventoryNameByLang("LocFloor.").",' > ',".$linventory->getInventoryNameByLang("d.")."),
     * b.Quantity
     * FROM
     * INVENTORY_ITEM AS a INNER JOIN
     * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.GroupInCharge = $targetGroup AND b.LocationID = $targetLocation) INNER JOIN
     * INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
     * INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
     * INVENTORY_LOCATION_LEVEL AS LocFloor ON (d.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
     * INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID)
     * WHERE
     * a.ItemID = $item_id";
     */
    
    $sql = "SELECT 
					a.ItemID,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					b.GroupInCharge, 
					" . $linventory->getInventoryNameByLang("c.") . ",
					b.LocationID, 
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
					b.Quantity
			FROM 
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.LocationID = '".$targetLocation."' and b.GroupInCharge = '".$targetGroupID."' and b.FundingSourceID = '".$targetFundingID."') INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS LocFloor ON (d.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID) INNER JOIN
					INVENTORY_FUNDING_SOURCE fs ON (b.FundingSourceID=fs.FundingSourceID)
			WHERE 
					a.ItemID = '".$item_id."'";
    $arr_result = $linventory->returnArray($sql, 7);
    // End #
    
    /*
     * if(sizeof($arr_result)==0)
     * {
     * $table_content .= "<tr>";
     * $table_content .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"35%\">$i_InventorySystem_Item_Location</td>";
     * $table_content .= "<td class=\"tabletext\">$location_selection</td>";
     * $table_content .= "</tr>";
     *
     * $table_content .= "</table>";
     * $table_content .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
     * $table_content .= "<tr>";
     * $table_content .= "<td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>";
     * $table_content .= "</tr>";
     * }
     */
    
    if (sizeof($arr_result) > 0) {
        
        $table_content .= "<tr>";
        // $table_content .= "<td colspan='100%'>". $i_InventorySystem_Item_Location. ": " . $location_selection."</td>";
        $table_content .= "<td colspan='100%'>" . $i_InventorySystem_Item_Location . ": " . $location_selection . " " . $i_InventorySystem['Caretaker'] . ": " . $group_selection . " " . $i_InventorySystem['FundingSource'] . ": $funding_selection </td>";
        $table_content .= "</tr>";
        
        $table_content .= "<tr class=\"tabletop\">";
        // $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>\n";
        // $table_content .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Group_Name</td>\n";
        // $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>\n";
        $table_content .= "<td class=\"tabletopnolink\" width=\"10%\">$i_InventorySystem_StockAssigned</td>\n";
        $table_content .= "<td class=\"tabletopnolink\" width=\"10%\" >$i_InventorySystem_ItemStatus_Normal</td>";
        $table_content .= "<td class=\"tabletopnolink\" width=\"10%\">$i_InventorySystem_ItemStatus_Damaged</td>";
        $table_content .= "<td class=\"tabletopnolink\" width=\"10%\" >$i_InventorySystem_ItemStatus_Repair</td>";
        $table_content .= "<td class=\"tabletopnolink\" width=\"60%\">$i_InventorySystem_Item_Remark</td>";
        $table_content .= "</tr>";
        
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_id, $item_name, $item_group_id, $item_group_name, $item_location_id, $item_location_name, $item_qty) = $arr_result[$i];
            
            /*
             * # retrieve latest item status
             * $sql = "select QtyNormal, QtyDamage, QtyRepair from INVENTORY_ITEM_BULK_LOG where (Action=". ITEM_ACTION_UPDATESTATUS ." or Action=". ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING ." ) and ItemID=$item_id and LocationID=$targetLocation order by RecordID desc limit 0, 1";
             * $result = $linventory->returnArray($sql);
             *
             * if(!empty($result))
             * {
             * $QtyNormal = $result[0]['QtyNormal'];
             * $QtyDamage = $result[0]['QtyDamage'];
             * $QtyRepair = $result[0]['QtyRepair'];
             * }
             */
            // retrieve latest item status
            $sql = "select QtyNormal, QtyDamage, QtyRepair, Remark from INVENTORY_ITEM_BULK_LOG where (Action=" . ITEM_ACTION_UPDATESTATUS . " or Action=" . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . " ) and ItemID = '".$item_id."' and LocationID = '".$targetLocation."' and FundingSource = '".$targetFundingID."' and GroupInCharge = '".$targetGroupID."' order by RecordID desc limit 0, 1";
            $result = $linventory->returnArray($sql);
            $remark = $result[0]['Remark'];
            if (! empty($result)) {
                $QtyNormal = $result[0]['QtyNormal'];
                $QtyDamage = $result[0]['QtyDamage'];
                $QtyRepair = $result[0]['QtyRepair'];
            } else {
                $QtyNormal = $item_qty;
                $QtyDamage = 0;
                $QtyRepair = 0;
            }
            
            // $table_content .= "<tr $table_row_css><td class=\"tabletext\">";$item_name</td>\n";
            // $table_content .= "<td class=\"tabletext\">$item_group_name</td>\n";
            $table_content .= "<input type=\"hidden\" name=\"item_qty\" value=\"$item_qty\">";
            // $table_content .= "<td class=\"tabletext\">$item_location_name</td>\n";
            $table_content .= "<td class=\"tabletext\">$item_qty</td>\n";
            $table_content .= "<td><input type=\"text\" name=\"bulk_item_normal_qty\" class=\"textboxnum\" style=\"width: 45px\" value=\"" . $QtyNormal . "\"></td>";
            $table_content .= "<td><input type=\"text\" name=\"bulk_item_damage_qty\" class=\"textboxnum\" style=\"width: 45px\" value=\"" . $QtyDamage . "\"></td>";
            $table_content .= "<td><input type=\"text\" name=\"bulk_item_repair_qty\" class=\"textboxnum\" style=\"width: 45px\" value=\"" . $QtyRepair . "\"></td>";
            $table_content .= "<td><input type=\"text\" name=\"bulk_item_update_status_remark\" class=\"textboxtext\" value=\"" . intranet_htmlspecialchars($remark) . "\" ></td>";
        }
        $table_content .= "<input type=\"hidden\" name=\"item_group_id\" value=$item_group_id>\n";
        $table_content .= "<input type=\"hidden\" name=\"item_location_id\" value=$item_location_id>\n";
        $table_content .= "<input type=\"hidden\" name=\"item_id\" value=$item_id>\n";
        $table_content .= "<input type=\"hidden\" name=\"item_funding_id\" value=$targetFundingID>\n";
        
        $table_content .= "<tr><td colspan='5'><span class=\"tabletextremark\">(" . $Lang['eInventory']['UpdateItemStatusEmailToeBookingAdminRemark'] . ")</span></td></tr>";
    }
}
?>
<script language="javascript">
function enabledSingleItemWriteOffReason(val)
{
	tmp1 = eval("document.form1.single_item_write_off_reason");
	tmp2 = eval("document.form1.single_item_write_off_attachment");
	tmp1.disabled = val;
	tmp2.disabled = val;
}

function checkForm()
{
	var obj = document.form1;
	var checked = 0;
	var type = <?=$item_type;?>;
	
	if(obj.flag.value == 1)
	{
		if(type == 1)
		{
			var tmp_single_id = <?=$item_id;?>;
			if(check_select(obj.single_item_status,"<?=$i_InventorySystem_UpdateSingleItemStatusWarning;?>",0))
			{
				checked = 1;
				/*
				if(check_checkbox(document.form1,'write_off_item_id[]'))
				{
					if(returnChecked(document.form1,'write_off_item_id[]') == tmp_single_id)
					{
						tmp_single_select = eval("document.form1.single_item_write_off_reason");
						if(check_select(tmp_single_select,"Please select the reason",0))
						{
							checked = 1;
						}
						else
						{
							checked = 0;
							return false;
						}
					}
				}
				else
				{
					checked = 1;
				}
				*/
			}
			/*
			else
			{
				checked = 0;
			}
			*/
		}
		if(type == 2)
		{
			var item_id = <?=$item_id?>;
			<? if($item_group_id != "") { ?>
			var item_group_id = <?=$item_group_id?>;
			<? } ?>
			checked = 0;
			
			var total_input_qty = 0;
			var item_status_1 = eval("document.form1.bulk_item_normal_qty");
			var item_status_2 = eval("document.form1.bulk_item_damage_qty");
			var item_status_3 = eval("document.form1.bulk_item_repair_qty");
			
			total_input_qty = parseInt(item_status_1.value) + parseInt(item_status_2.value) + parseInt(item_status_3.value);
						
			if(check_positive_int(item_status_1, "<?=$i_InventorySystem_UpdateBulkItemStatusWarning_Normal;?>"))
			{
				if(check_positive_int(item_status_2, "<?=$i_InventorySystem_UpdateBulkItemStatusWarning_Damaged;?>"))
				{
					if(check_positive_int(item_status_3, "<?=$i_InventorySystem_UpdateBulkItemStatusWarning_Reparing;?>"))
					{
						checked = 1;
						if(total_input_qty != document.form1.item_qty.value)
						{
							alert("<?=$Lang['eInventory']['UpdateStatusErrorMsg'];?>");
							checked = 0;
						}
					}
				}
			}
		}
	}
	
	
	if(checked == 1)
	{
		obj.action = "items_status_edit_update.php";
		return true;
	}
	else
	{
		obj.action = "";
		return false;
	}
}

function change_filter(filter)
{
	var obj = document.form1;
	
	if(filter=="location")
	{
		obj.targetGroupID.selectedIndex=-1; 
		obj.targetFundingID.selectedIndex=-1; 
	}
	
	if(filter=="group")
	{
		obj.targetFundingID.selectedIndex=-1; 
	}
	
	obj.flag.value=0; 
	obj.action="items_status_edit.php";
	obj.submit();
}

function click_tab(tab)
{
	var obj = document.form1;
	if(tab=="update_status")
		obj.action="items_status_edit.php";
	if(tab=="change_location")
		obj.action="items_location_edit.php";
	if(tab=="request_writeoff")
		obj.action="items_request_write_off.php";
	obj.submit();
}

function js_Show_Detail_Layer2(ajax_php, task, jsClickedObjID, data_str)
{
	js_Hide_Detail_Layer();
	
	$('div#RemarkLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
	//js_Change_Layer_Position(jsClickedObjID);
	MM_showHideLayers('RemarkLayer','','show');
			
	$('div#RemarkLayerContentDiv').load(
		ajax_php, 
		{ 
			Action: task,
			DataStr: data_str
		},
		function(returnString)
		{
			$('div#RemarkLayerContentDiv').css('z-index', '999');
		}
	);
}

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('RemarkLayer','','hide');
}

function js_Change_Layer_Position(jsClickedObjID) {
	
	var jsOffsetLeft, jsOffsetTop;
	
	jsOffsetLeft = 450;
	jsOffsetTop = -15;
		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById('RemarkLayer').style.left = posleft + "px";
	document.getElementById('RemarkLayer').style.top = postop + "px";
	document.getElementById('RemarkLayer').style.visibility = 'visible';
}



</script>

<br>

<form name="form1" action="" method="POST"
	onSubmit="return checkForm();">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
<?=$infobar1?>
</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$infobar2?>
</table>

	<table width="100%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content;?>
</table>

<?php
if (($item_type == 1) || (($item_type == 2) && (sizeof($arr_result) > 0))) {
    ?>
<table width="100%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr height="10px">
			<td></td>
		</tr>
		<tr>
			<td class="dotline"><img
				src="<?="$image_path/$LAYOUT_SKIN" ?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "this.form.flag.value=1")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='items_full_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>
<?
}
?>
<input type="hidden" name="item_type" value="<?=$item_type?>"> <input
		type="hidden" name="item_id" value="<?=$item_id?>"> <input
		type="hidden" name="flag" value="0">
</form>

<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>