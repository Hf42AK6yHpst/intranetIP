<?php
// editing by 

/***************************** Modification Log *************************************
 * 2019-07-25 [Tommy]: add $sys_custom['eInventory_SysPpt'] for new item code setting
 * 2019-04-30 [Henry]: security issue fix
 * 2018-02-21 [Henry]: add access right checking [Case#E135442]
 * 2017-06-02 [Henry]: item code cust for SPCCPS [Case#P115830]
 * 2015-06-02 [Henry]: Disable submit button after clicking once to prevent double submission [Case#Z79029]
 * 2014-04-01 [YatWoon]: SKH customization > item code format  ($sys_custom['eInventoryCustForSKH'])
 * 2013-11-12 [YatWoon]: Check item code with written-off item [Case#2013-1111-1455-18156]
 * 2013-04-24 [YatWoon]: Improved: [Cust] Add "item code delimiter" and "nubmer digit" flag setting [HOLY FAMILY CANOSSIAN COLLEGE]
 *						js function: GenItemCode()
 *						flag: $sys_custom['eInventory']['ItemCodeDelimiter'], $sys_custom['eInventory']['ItemCodeNumberDigit']
 * 2013-04-18 [YatWoon]: Improved: create UserID folder for tmp photo
 * 2013-03-21 [YatWoon]: Fixed: updated js function checkForm(), incorrect set the item code array with barcode data [Case#2013-0321-1550-46156]
 * 2013-03-12 [Carlos]: Disable bulk item barcode for existing bulk item
 * 2013-02-25 [Carlos]: Added Barcode for bulk item
 * 2012-12-06 [Yuen]: Enhanced: support tags 
 * 2012-07-10 [YatWoon]: Enhanced: support 2 funding source for single item
 * 2012-05-23 [YatWoon]: allow user select different group and funding for bulk item with same location
 * 2012-03-28 [YatWoon]: Fixed: Existings bulk item NEED select mgmt group and funding (fixed for funding cost issue)
 * 2011-06-16 [YatWoon]: User userid as attachment temp folder 
 * 2011-06-08 [YatWoon]: Improved: Can back to step 1 from step 2 [Case#2011-0401-1247-08067]
 * 2011-05-30 [YatWoon]: Fixed: Missing set intranet_htmlspecialchars for quantity, invoice and tender field
 * 2011-05-26 [YatWoon]: Fixed: retrive incorrect resource management group (only retrieve own group with leader rights only - if not admin)
 * 2011-05-18 [YatWoon]: Improved: Existings bulk item, no need select mgmt group and funding
 * 2011-05-18 [Carlos]: Fix upload item photo
 * 
 *	Date:	2011-04-26	Yuen
 *			customized for Sacred Heart Canossian College (set expiry date to null)
 *
 * 2011-01-19 [Carlos]: Fix js location level_arr by sorting it with LocationLevelID
 ************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui	= new liblocation_ui();

$TAGS_OBJ[] = array($i_InventorySystem_Item_Registration, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

## Get var from step 1
$item_chi_name = stripslashes($item_chi_name);
$item_eng_name = stripslashes($item_eng_name);
$item_chi_discription = stripslashes($item_chi_discription);
$item_eng_discription = stripslashes($item_eng_discription);
$item_supplier = stripslashes($item_supplier);
$item_supplier_contact = stripslashes($item_supplier_contact);
$item_supplier_description = stripslashes($item_supplier_description);
$item_brand = stripslashes($item_brand);
$item_maintain_info = stripslashes($item_maintain_info);
$item_remark = stripslashes($item_remark);
$item_quotation = stripslashes($item_quotation);
$item_tender = stripslashes($item_tender);
$item_invoice = stripslashes($item_invoice);

$item_ownership = cleanHtmlJavascript($item_ownership);
$targetItemType = IntegerSafe($targetItemType);
$bulk_item_admin = IntegerSafe($bulk_item_admin);
$item_eng_name = cleanHtmlJavascript($item_eng_name);
$targetExistItem = cleanHtmlJavascript($targetExistItem);

for($i=0; $i<$attachment_size; $i++)
{
	${"hidden_item_attachment_$i"} = stripslashes(${"hidden_item_attachment_$i"});
}

$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeFormat = $linventory->getBarcodeFormat();


## Retrive the new ItemCode format setting ##
if($sys_custom['eInventory_ItemCodeFormat_New']){
	
	$js_ItemCodeFormat .= "var itemCodeFormat_Year = 0; \n";
	$js_ItemCodeFormat .= "var itemCodeFormat_Group = 0; \n";
	$js_ItemCodeFormat .= "var itemCodeFormat_Funding = 0; \n";
	$js_ItemCodeFormat .= "var itemCodeFormat_Location = 0; \n";
	
	if($linventory->checkItemCodeFormatSetting() == true){
		$arr_itemCodeFormat = $linventory->retriveItemCodeFormat();

		if(sizeof($arr_itemCodeFormat)>0){
			for($i=0; $i<sizeof($arr_itemCodeFormat); $i++){
				if($arr_itemCodeFormat[$i] == 1){
					$js_ItemCodeFormat .= "itemCodeFormat_Year = 1; \n";
				}
				if($arr_itemCodeFormat[$i] == 2){
					$js_ItemCodeFormat .= "itemCodeFormat_Group = 1; \n";
				}
				if($arr_itemCodeFormat[$i] == 3){
					$js_ItemCodeFormat .= "itemCodeFormat_Funding = 1; \n";
				}
				if($arr_itemCodeFormat[$i] == 4){
					$js_ItemCodeFormat .= "itemCodeFormat_Location = 1; \n";
				}
			}
		}
	}
}

## Retrive the new ItemCode format setting ##
if($sys_custom['eInventory_SysPpt']){
    
    $js_ItemCodeFormat .= "var itemCodeFormat_Year = 0; \n";
    $js_ItemCodeFormat .= "var itemCodeFormat_Cat = 0; \n";
    $js_ItemCodeFormat .= "var itemCodeFormat_Cat2 = 0; \n";
    $js_ItemCodeFormat .= "var itemCodeFormat_Group = 0; \n";
    $js_ItemCodeFormat .= "var itemCodeFormat_Funding = 0; \n";
    $js_ItemCodeFormat .= "var itemCodeFormat_Funding2 = 0; \n";
    $js_ItemCodeFormat .= "var itemCodeFormat_Location = 0; \n";
    
    if($linventory->checkItemCodeFormatSetting() == true){
        $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
        
        if(sizeof($arr_itemCodeFormat)>0){
            for($i=0; $i<sizeof($arr_itemCodeFormat); $i++){
                if($arr_itemCodeFormat[$i] == 'PurchaseYear'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Year = 1; \n";
                }
                if($arr_itemCodeFormat[$i] == 'CategoryCode'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Cat = 1; \n";
                }
                if($arr_itemCodeFormat[$i] == 'SubCategoryCode'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Cat2 = 1; \n";
                }
                if($arr_itemCodeFormat[$i] == 'ResourceManagementGroupCode'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Group = 1; \n";
                }
                if($arr_itemCodeFormat[$i] == 'FundingSource'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Funding = 1; \n";
                }
                if($arr_itemCodeFormat[$i] == 'FundingSource2'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Funding2 = 1; \n";
                }
                if($arr_itemCodeFormat[$i] == 'LocationCode'){
                    $js_ItemCodeFormat .= "itemCodeFormat_Location = 1; \n";
                }
            }
        }
    }
}

if($sys_custom['eInventoryCustForSKH'])
{
	include_once($PATH_WRT_ROOT."includes/libinventory_cust.php");
	$linventory_cust = new libinventory_cust();
	$purhcase_date_format = $linventory_cust->returnYearFormat($item_purchase_date);
}

if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH'])
{
	$sym = $sys_custom['CatholicEducation_eInventory'] ? "-" : "/";
	### Get the Code Of Category and Sub-Category ###
	$sql = "SELECT 
					CONCAT(a.Code,'$sym',b.Code)
			FROM 
					INVENTORY_CATEGORY AS a 
					INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) 
			WHERE
					b.CategoryID = '".$category_id."' AND b.Category2ID = '".$category2_id."'";
	$arr_ItemCodePrefix = $linventory->returnVector($sql);
	$item_code_prefix = $arr_ItemCodePrefix[0];
	### End ###
}

//$no_file = 5;

if($targetItemType == 1)
{
	$sql = "SELECT 
					HasSoftwareLicenseModel, 
					HasWarrantyExpiryDate,
					HasSerialNumber
			FROM 
					INVENTORY_CATEGORY_LEVEL2 
			WHERE 
					Category2ID = '".$targetCategory2."'";
	$result = $linventory->returnArray($sql,2);
	
// 	### Get the Code Of Category and Sub-Category ###
// 	$sql = "SELECT 
// 					CONCAT(a.Code,b.Code)
// 			FROM 
// 					INVENTORY_CATEGORY AS a INNER JOIN 
// 					INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) 
// 			WHERE
// 					b.CategoryID = $category_id AND b.Category2ID = $category2_id";
// 	$arr_ItemCodePrefix = $linventory->returnVector($sql);
// 	$item_code_prefix = $arr_ItemCodePrefix[0];
// 	### End ###
	### Get Totol num of the related item ###
	$sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID = '".$category_id."' AND Category2ID = '".$category2_id."'";
	$arr_TotalItem = $linventory->returnVector($sql);
	$total_item = $arr_TotalItem[0];
	### End ###
	
	$all_location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetLocation_all, "targetLocation_all", "setAllSubLocation(this.value);", 0, "", "", "", "", "");
	
	#get my resource groups #
	if($linventory->IS_ADMIN_USER($UserID))
	{
		$group_namefield = $linventory->getInventoryNameByLang();
		$sql = "SELECT AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
	}
	else
	{
		$group_namefield = $linventory->getInventoryNameByLang();
// 		$sql = "SELECT a.AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP a , 
// 		INVENTORY_ADMIN_GROUP_MEMBER b WHERE a.AdminGroupID = b.AdminGroupID AND b.UserID = $UserID ORDER BY a.DisplayOrder ";	
		$sql = "
				SELECT 
					a.AdminGroupID, 
					$group_namefield as GroupName
				FROM 
					INVENTORY_ADMIN_GROUP as a 
					inner join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
				WHERE 
					b.UserID =  '".$UserID."'
				 ORDER BY 
				 	a.DisplayOrder
				";
	} 
	$group_array = $linventory->returnArray($sql,2);
	
	$js.="group_arr = new Array(".(sizeof($group_array)).");\n";
	for($i=0; $i<sizeof($group_array); $i++)
	{	$js .="group_arr[$i] = new Array (2);\n";
		$js	.="group_arr[$i][1] = \"".$group_array[$i]['AdminGroupID']."\";\n";
		$js	.="group_arr[$i][2] = \"".$group_array[$i]['GroupName']."\";\n";
	}
	
	$master_group_selection = getSelectByArray($group_array, "name=targetGroup_all id=targetGroup_all onChange=\"setAllGroup(this.value)\"", $targetLocation_all);
	
	### Get the location ###
	
	### Get the group for the inventory ###
// 	$namefield = $linventory->getInventoryItemNameByLang();
// 	$sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
// 	$group_result = $linventory->returnArray($sql,2);
// 	$all_group_selection = getSelectByArray($group_result,"name=targetGroup_all id=targetGroup_all", $targetGroup_all);
	
	if($sys_custom['eInventory_ItemCodeFormat_New'] || $sys_custom['eInventory_SysPpt'] ){
		if($linventory->checkItemCodeFormatSetting() == true){
			$itemCodeBtnVisibility = "";
		}else{
			$itemCodeBtnVisibility = " DISABLED ";
		}
	}else{
		$itemCodeBtnVisibility = "";
	}
	
	
	$table_content .= "<tr class=\"tablegreentop\">";
	
	if(!($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH']))
	{
		$table_content .= "<td class=\"tabletopnolink\" width=\"10%\">
								$i_InventorySystem_Item_Code <br>".
								$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenItemCode($targetItemType,$targetCategory,$targetCategory2,$item_total_qty);", "", "$itemCodeBtnVisibility").
							"</td>";
		$table_content .= "<td class=\"tabletopnolink\" width=\"10%\">
								$i_InventorySystem_Item_Barcode <br>".
								$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Barcode","Button","javascript:GenItemBarcode($item_total_qty);").
							"</td>";
	}
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Location_Level $all_building_selection <br> $all_level_selection <br> $all_location_selection</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem['Caretaker']."<br>$master_group_selection</td>";
	
	if($result[0][HasSerialNumber] == 1)
	{
		$all_serial_num ="<input type=\"text\" class=\"textboxnum\" name=\"all_serial_num\" id=\"all_serial_num\" onChange=\"setAllSerialNum(this.value)\">";
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Serial_Num<br>$all_serial_num</td>";
	}
	if($result[0][HasSoftwareLicenseModel] == 1)
	{	
		$all_license_type ="<input type=\"text\" class=\"textboxnum\" name=\"all_license_type\" id=\"all_license_type\" onChange=\"setAllLicenseType(this.value)\">";
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_License_Type<br>$all_license_type</td>";
	}
	
	if (!$sys_custom['eInventoryCustForSMC'])
	{
		$expiry_date_value = date('Y-m-d');
	}
	if($result[0][HasWarrantyExpiryDate] == 1)
	{
		for($i=0; $i<$item_total_qty; $i++)
			$all_item_warranty_expiry_date_array[]= $i;
				
		$all_expiry_date .= $linterface->GET_DATE_FIELD_INVENTORY($all_item_warranty_expiry_date_array, "form1", $all_item_warranty_expiry_date_array, $expiry_date_value,"1","",'item_warranty_expiry_date_all');
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Warrany_Expiry<br>$all_expiry_date</td>";
	}
	### for eBooking Use ###
	##$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_SetAsResourceBookingItem<br><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'ResourceItem[]'):setChecked(0,this.form,'ResourceItem[]')\"></td>";
	### end ###
	
	if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH'])
	{
		$table_content .= "<td class=\"tabletopnolink\" width=\"10%\">
								$i_InventorySystem_Item_Code <br>".
								$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenItemCode($targetItemType,$targetCategory,$targetCategory2,$item_total_qty);", "", "$itemCodeBtnVisibility").
							"</td>";
		$table_content .= "<td class=\"tabletopnolink\" width=\"10%\">
								$i_InventorySystem_Item_Barcode <br>".
								$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Barcode","Button","javascript:GenItemBarcode($item_total_qty);").
							"</td>";
	}
	
	$table_content .= "</tr>";
	
	# get all location level #
	$location_level_namefield = $linventory->getInventoryNameByLang("a.");
	//$sql = "SELECT DISTINCT a.LocationLevelID, a.BuildingID, $location_level_namefield as LevName FROM INVENTORY_LOCATION_LEVEL as a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 ORDER BY a.BuildingID, a.LocationLevelID";
	$sql = "SELECT DISTINCT a.LocationLevelID, a.BuildingID, $location_level_namefield as LevName FROM INVENTORY_LOCATION_BUILDING AS bul INNER JOIN INVENTORY_LOCATION_LEVEL as a ON (bul.BuildingID = a.BuildingID) INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) WHERE bul.RecordStatus=1 AND a.RecordStatus = 1 AND b.RecordStatus = 1 ORDER BY bul.DisplayOrder, a.DisplayOrder, b.DisplayOrder";
	$location_level_array = $linventory->returnArray($sql,3);
	for($i=0; $i<sizeof($location_level_array); $i++)
	{	
		if($location_level_array[$i]['BuildingID']!=$location_level_array[$i-1]['BuildingID'])
		{
			$a=1;
		}
		$building_arr[$location_level_array[$i]['BuildingID']][$a]['LocationLevelID'] = $location_level_array[$i]['LocationLevelID'];
		$building_arr[$location_level_array[$i]['BuildingID']][$a]['desc'] = addslashes($location_level_array[$i]['LevName']);
		$a++;
		$buildingid[] = $location_level_array[$i]['BuildingID'];
	}

	$js_location_level ="building_arr = new Array(".(sizeof($building_arr)).");\n";
	$tmpid = (array_unique($buildingid));
	
	$a=1;
	if(is_array($tmpid) && sizeof($tmpid)>0)
	{
		foreach($tmpid as $Key=>$Value)
		{	
			
			$levelid[$a]=$Value;
			$a++;
		}
	}
		$a=1;
	if(is_array($building_arr) && sizeof($building_arr)>0)
	{
		foreach($building_arr as $Key=>$Value)
		{
			$js_location_level .="building_arr[".$levelid[$a]."] = new Array (".(sizeof($Value)).");\n";
			for($x=1;$x<=sizeof($Value);$x++)
				$js_location_level .="building_arr[".$levelid[$a]."][".$x."] = new Array (2);\n";	
		
			for($i=1; $i<sizeof($Value)+1; $i++) 
			{
				$js_location_level .= "building_arr[".$Key."][".$i."][1]=".$Value[$i]['LocationLevelID'].";\n";
				$js_location_level .= "building_arr[".$Key."][".$i."][2]= \"".addslashes($Value[$i]['desc'])."\";\n";
			}
			$a++;
			
		}
	}
	
	#get all location #
	$location_namefield = $linventory->getInventoryNameByLang("a.");
	$location_level_namefield = $linventory->getInventoryNameByLang("b.");
	$sql = "SELECT a.LocationLevelID, a.LocationID, $location_namefield as LocName, $location_level_namefield as LevName FROM INVENTORY_LOCATION a , INVENTORY_LOCATION_LEVEL b , INVENTORY_LOCATION_BUILDING AS bul WHERE bul.BuildingID = b.BuildingID AND a.LocationLevelID = b.LocationLevelID AND a.RecordStatus = 1 ORDER BY b.LocationLevelID, bul.DisplayOrder, b.DisplayOrder, a.DisplayOrder";
	$location_array = $linventory->returnArray($sql,3);
	
	for($i=0; $i<sizeof($location_array); $i++)
	{
			
		if($location_array[$i]['LocationLevelID']!=$location_array[$i-1]['LocationLevelID'])
		{
			$a=1;
		}
		$level_arr[$location_array[$i]['LocationLevelID']][$a]['LocationID'] = $location_array[$i]['LocationID'];
		$level_arr[$location_array[$i]['LocationLevelID']][$a]['desc'] = "(".$location_array[$i]['LevName'].") ".addslashes($location_array[$i]['LocName']);
		$a++;
		$locationlevelid[] = $location_array[$i]['LocationLevelID'];
	}
	
	$js ="level_arr = new Array(".(sizeof($level_arr)).");\n";
	$tmpid = (array_unique($locationlevelid));
	
	$a=1;
	if(is_array($tmpid) && sizeof($tmpid)>0)
	{
		foreach($tmpid as $Key=>$Value)
		{	
			
			$levelid[$a]=$Value;
			$a++;
		}
	}
		$a=1;
	if(is_array($level_arr) && sizeof($level_arr)>0)
	{
		foreach($level_arr as $Key=>$Value)
		{
			$js .="level_arr[".$levelid[$a]."] = new Array (".(sizeof($Value)).");\n";
			for($x=1;$x<=sizeof($Value);$x++)
				$js .="level_arr[".$levelid[$a]."][".$x."] = new Array (2);\n";	
		
			for($i=1; $i<sizeof($Value)+1; $i++) 
			{
				
				$js .= "level_arr[".$Key."][".$i."][1]=".$Value[$i]['LocationID'].";\n";
				$js .= "level_arr[".$Key."][".$i."][2]= \"".addslashes($Value[$i]['desc'])."\";\n";
			}
			$a++;
			
		}
	}
	#get all location ends#	
	
	for($i=0; $i<$item_total_qty; $i++)
	{	
		$location_selection = $llocation_ui->Get_Building_Floor_Room_Selection("", "targetLocation_{$i}", "", 0, "", "", "", "", "");
		
		### Get the group for the inventory ###
		if($linventory->IS_ADMIN_USER($UserID))
		{
			$namefield = $linventory->getInventoryItemNameByLang();
			$sql = "SELECT AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
		}
		else
		{
			$namefield = $linventory->getInventoryItemNameByLang();
// 			$sql = "SELECT a.AdminGroupID, $group_namefield as GroupName  FROM INVENTORY_ADMIN_GROUP a , 
// 			INVENTORY_ADMIN_GROUP_MEMBER b WHERE a.AdminGroupID = b.AdminGroupID AND b.UserID = $UserID ORDER BY a.DisplayOrder ";
			$sql = "
				SELECT 
					a.AdminGroupID, 
					$group_namefield as GroupName
				FROM 
					INVENTORY_ADMIN_GROUP as a 
					inner join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
				WHERE 
					b.UserID =  '".$UserID."'
				 ORDER BY 
				 	a.DisplayOrder
				";
		}
		$group_result = $linventory->returnArray($sql,2);
		$group_selection = getSelectByArray($group_result,"name=targetGroup_$i id=targetGroup_$i", ${"targetGroup_$i"});
		
		$j=$i+1;
		if($j%2 == 0)
			$row_css = " class=\"tablegreenrow1\" ";
		else
			$row_css = " class=\"tablegreenrow2\" ";
		
		$table_content .= "<tr $row_css>";
		if(!($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH']))
		{
			$table_content .= "<td class=\"tabletext\">
									<input type=\"text\" name=\"item_code_$i\" id=\"item_code_$i\" class=\"textboxnum\" value=\"${"item_code_$i"}\"> 
								</td>";
			$table_content .= " <td class=\"tabletext\">
									<input type=\"text\" name=\"item_barcode_$i\" id=\"item_barcode_$i\" class=\"textboxnum\" value=\"${"item_barcode_$i"}\">
								</td>";
		}
		$table_content .= "<td width=\"20%\" class=\"tabletext\">$building_selection $level_selection $location_selection</td>";
		$table_content .= "<td width=\"20%\" class=\"tabletext\">$group_selection</td>";
		$item_count++;
		
		if($result[0][HasSerialNumber] == 1)
		{
			$table_content .= "<td class=\"tabletext\" valign=\"top\"><input name=\"item_serial_$i\" id=\"item_serial_$i\" type=\"text\" class=\"textboxnum\" value=\"${"item_serial_$i"}\"></td>\n";
		}
		if($result[0][HasSoftwareLicenseModel] == 1)
		{
			$table_content .= "<td class=\"tabletext\" valign=\"top\"><input name=\"item_license_$i\" id=\"item_license_$i\" type=\"text\" class=\"textboxnum\" value=\"${"item_license_$i"}\"></td>\n";
		}
		
		if($result[0][HasWarrantyExpiryDate] == 1)
		{
			$table_content .= "<td nowrap class=\"tabletext\" valign=\"top\">".$linterface->GET_DATE_FIELD("item_warranty_expiry_date_$i", "form1", "item_warranty_expiry_date_$i", $expiry_date_value)."</td>\n";
		}
		
		### for eBooking Use ###
		##$table_content .= "<td class=\"tabletext\" valign=\"top\"><input type=\"checkbox\" name=\"ResourceItem[]\" value=\"$i\"></td>";
		### end ###
		
		if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH'])
		{
			$table_content .= "<td class=\"tabletext\">
									<input type=\"text\" name=\"item_code_$i\" id=\"item_code_$i\" class=\"textboxnum\" value=\"${"item_code_$i"}\"> 
								</td>";
			$table_content .= " <td class=\"tabletext\">
									<input type=\"text\" name=\"item_barcode_$i\" id=\"item_barcode_$i\" class=\"textboxnum\" value=\"${"item_barcode_$i"}\">
								</td>";
		}
		
		$table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=\"8\"></td></tr>";
	}
	$js.="var item_count = $item_count;\n";
}

###############################################
##### bulk item
###############################################
if($targetItemType == 2)
{ 
	# existings item?
	if(!empty($exist_item_code))
	{
		$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$exist_item_code'";
		$arr_tmp_item_id = $linventory->returnVector($sql);
		$exist_item_id = $arr_tmp_item_id[0];
		
		//if($purchase_type == 2) {
			$sql = "SELECT b.Barcode FROM INVENTORY_ITEM as a 
					INNER JOIN INVENTORY_ITEM_BULK_EXT as b ON a.ItemID=b.ItemID 
					WHERE a.ItemCode='$exist_item_code'";
			$arr_tmp_item_barcode = $linventory->returnVector($sql);
			$exist_item_barcode = $arr_tmp_item_barcode[0];
			
			$existing_bulk_item_barcode_cond = " AND c.Barcode <> '$exist_item_barcode' ";
		//}
	}
	
	$i=0;
	$sql = "SELECT 
					HasSoftwareLicenseModel, 
					HasWarrantyExpiryDate 
			FROM 
					INVENTORY_CATEGORY_LEVEL2 
			WHERE 
					Category2ID = '".$targetCategory2."'";
	$result = $linventory->returnArray($sql,2);
	
// 	### Get the Code Of Category and Sub-Category ###
// 	$sql = "SELECT 
// 					CONCAT(a.Code,b.Code)
// 			FROM 
// 					INVENTORY_CATEGORY AS a INNER JOIN 
// 					INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) 
// 			WHERE
// 					b.CategoryID = $category_id AND b.Category2ID = $category2_id";
// 	$arr_ItemCodePrefix = $linventory->returnVector($sql);
// 	$item_code_prefix = $arr_ItemCodePrefix[0];
// 	### End ###
	
	### Get Totol num of the related item ###
	$sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID = '".$category_id."' AND Category2ID = '".$category2_id."'";
	$arr_TotalItem = $linventory->returnVector($sql);
	$total_item = $arr_TotalItem[0];
	### End ###
	
	### Get the group for the inventory ###
// 	$namefield = $linventory->getInventoryItemNameByLang();
// 	$sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
// 	$group_result = $linventory->returnArray($sql,2);
// 	$all_group_selection = getSelectByArray($group_result,"name=targetGroup_all id=targetGroup_all", $targetGroup_all);
	
	# Item Code table header
	$table_content .= "<tr class=\"tablegreentop\">";
	
	if(!($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH']))
	{
		$table_content .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Code";
		if(!$exist_item_code) 
			$table_content .= " <br>".$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenItemCode($targetItemType,$targetCategory,$targetCategory2,1);");
		$table_content .= "</td>";
		$table_content .= "<td class=\"tabletopnolink\" width=\"20%\">
								$i_InventorySystem_Item_Barcode ";
		if($exist_item_barcode == "") {
			$table_content .= "<br>".$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Barcode","Button","javascript:GenItemBarcode(1);");
		}
		$table_content .= "</td>";
	}
							
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Location_Level</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem['Caretaker']."</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Funding</td>";

	if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH'])
	{
		$table_content .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Code";
		if(!$exist_item_code) 
			$table_content .= " <br>".$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenItemCode($targetItemType,$targetCategory,$targetCategory2,1);");
		$table_content .= "</td>";
		$table_content .= "<td class=\"tabletopnolink\" width=\"20%\">
								$i_InventorySystem_Item_Barcode ";
		if($exist_item_barcode == "") {
			$table_content .= "<br>".$linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Barcode","Button","javascript:GenItemBarcode(1);");
		}
		$table_content .= "</td>";
	}
	
	$location_selection = $llocation_ui->Get_Building_Floor_Room_Selection(${"targetLocation_$i"}, "targetLocation_{$i}", "check_exists($i,this.options[this.selectedIndex].value);", 0, "", "", "", "", "");

	$group_result = $linventory->returnOwnGroup();
	$group_selection = getSelectByArray($group_result,"name=targetGroup_$i id=targetGroup_$i", ${"targetGroup_$i"});
	
	## Get Funding For the bulk item ##
// 	if($purchase_type == 2)
//	{
//		$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$exist_item_code'";
//		$arr_tmp_item_id = $linventory->returnVector($sql);
//		
//		if(sizeof($arr_tmp_item_id)>0)
//		{
//			$exist_item_id = $arr_tmp_item_id[0];
//		}
//		$sql = "SELECT 
//						a.FundingSource, ".$linventory->getInventoryNameByLang("b.")."
//				FROM 
//						INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
//						INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
//				WHERE 
//						a.ItemID = $exist_item_id AND 
//						a.LocationID = ${"targetLocation_$i"}
//				ORDER BY 
//						b.DisplayOrder
//				";
//		$arr_funding_source = $linventory->returnArray($sql,2);
//			if(sizeof($arr_funding_source)>0)
//			{
//				$bulk_item_funding_selection = getSelectByArray($arr_funding_source," name=\"targetBulkFunding\" id=\"targetBulkFunding\" ",$targetBulkFunding,0,1);
//			}
//			else
//			{
//				$sql = "SELECT 
//								FundingSourceID, ".$linventory->getInventoryNameByLang()."
//						FROM
//								INVENTORY_FUNDING_SOURCE
//						ORDER BY
//								DisplayOrder
//						";
//				$arr_funding_source2 = $linventory->returnArray($sql,2);
//				if(sizeof($arr_funding_source2)>0)
//				{
//					$bulk_item_funding_selection = getSelectByArray($arr_funding_source2, " name=\"targetBulkFunding\" id=\"targetBulkFunding\" ",$targetBulkFunding,0);
//				}
//			}
//	}
//	else
//	{
		$arr_funding_source = $linventory->returnFundingSource();
		$bulk_item_funding_selection = getSelectByArray($arr_funding_source, " name=\"targetBulkFunding\" id=\"targetBulkFunding\" ",$targetBulkFunding,0);
//	}
	
// 	$sql = "SELECT FundingSourceID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_FUNDING_SOURCE ORDER BY DisplayOrder";
// 	$arr_funding_source = $linventory->returnArray($sql,2);
// 	$bulk_item_funding_selection = getSelectByArray($arr_funding_source, " name=\"targetBulkFunding\" id=\"targetBulkFunding\" ",$targetBulkFunding,0);
	## end ## 
	
	$j=$i+1;
	if($j%2 == 0)
		$row_css = " class=\"tablegreenrow1\" ";
	else
		$row_css = " class=\"tablegreenrow2\" ";
	
	$table_content .= "<tr $row_css>";
	
	if(!($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH']))
	{
		if($exist_item_code == "")
		{
			$table_content .= "<td class=\"tabletext\">
										<input type=\"text\" name=\"item_code_$i\" id=\"item_code_$i\" class=\"textboxnum\" value=\"${"item_code_$i"}\">
									</td>";
		}
		else
		{
			$table_content .= "<td class=\"tabletext\">
									<input type=\"text\" name=\"item_code_$i\" id=\"item_code_$i\" class=\"textboxnum\" value=\"$exist_item_code\" DISABLED>
								</td>";
		}
		if($exist_item_barcode == "") {
			$table_content .= " <td class=\"tabletext\">
									<input type=\"text\" name=\"item_barcode_$i\" id=\"item_barcode_$i\" class=\"textboxnum\" value=\"${"item_barcode_$i"}\">
								</td>";
		}else{
			$table_content .= " <td class=\"tabletext\">
									<input type=\"text\" name=\"item_barcode_$i\" id=\"item_barcode_$i\" class=\"textboxnum\" value=\"$exist_item_barcode\" DISABLED>
								</td>";
		}		
	}			
	
	$table_content .= "<td width=\"20%\" class=\"tabletext\">$location_selection</td>";
	$table_content .= "<td width=\"20%\" class=\"tabletext\"><div id='div_group'></div>$group_selection</td>";
	$table_content .= "<td width=\"20%\" class=\"tabletext\">$bulk_item_funding_selection</td>";
	
	if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH'])
	{
		if($exist_item_code == "")
		{
			$table_content .= "<td class=\"tabletext\">
										<input type=\"text\" name=\"item_code_$i\" id=\"item_code_$i\" class=\"textboxnum\" value=\"${"item_code_$i"}\">
									</td>";
		}
		else
		{
			$table_content .= "<td class=\"tabletext\">
									<input type=\"text\" name=\"item_code_$i\" id=\"item_code_$i\" class=\"textboxnum\" value=\"$exist_item_code\" DISABLED>
								</td>";
		}
		if($exist_item_barcode == "") {
			$table_content .= " <td class=\"tabletext\">
									<input type=\"text\" name=\"item_barcode_$i\" id=\"item_barcode_$i\" class=\"textboxnum\" value=\"${"item_barcode_$i"}\">
								</td>";
		}else{
			$table_content .= " <td class=\"tabletext\">
									<input type=\"text\" name=\"item_barcode_$i\" id=\"item_barcode_$i\" class=\"textboxnum\" value=\"$exist_item_barcode\" DISABLED>
								</td>";
		}		
	}
		
	$table_content .= "</tr>";
}
$table_content .= "<input type=\"hidden\" name=\"exist_item_code\" value=\"$exist_item_code\">\n"; 
$table_content .= "<input type=\"hidden\" name=\"purchase_type\" value=\"$purchase_type\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetCategory\" value=\"$targetCategory\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetCategory2\" value=\"$targetCategory2\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetItemType\" value=\"$targetItemType\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_total_qty\" value=\"$item_total_qty\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_chi_name\" value=\"".intranet_htmlspecialchars($item_chi_name)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_eng_name\" value=\"".intranet_htmlspecialchars($item_eng_name)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_chi_discription\" value=\"".intranet_htmlspecialchars($item_chi_discription)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_eng_discription\" value=\"".intranet_htmlspecialchars($item_eng_discription)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_admin\" value=\"$bulk_item_admin\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_ownership\" value=\"$item_ownership\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_photo\" value=\"$item_photo\">\n";
$table_content .= "<input type=\"hidden\" name=\"hidden_item_photo\" value=\"".urlencode($item_photo_name)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_funding\" value=\"$item_funding\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_funding2\" value=\"$item_funding2\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_brand\" value=\"".intranet_htmlspecialchars($item_brand)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_supplier\" value=\"".intranet_htmlspecialchars($item_supplier)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_supplier_contact\" value=\"".intranet_htmlspecialchars($item_supplier_contact)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_supplier_description\" value=\"".intranet_htmlspecialchars($item_supplier_description)."\">";
$table_content .= "<input type=\"hidden\" name=\"item_quotation\" value=\"".intranet_htmlspecialchars($item_quotation)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_tender\" value=\"".intranet_htmlspecialchars($item_tender)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_invoice\" value=\"".intranet_htmlspecialchars($item_invoice)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_purchase_date\" value=\"$item_purchase_date\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_purchase_price\" value=\"$item_purchase_price\">\n";
$table_content .= "<input type=\"hidden\" name=\"StockTakeOption\" value=\"$StockTakeOption\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_unit_price\" value=\"$item_unit_price\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_unit_price1\" value=\"$item_unit_price1\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_unit_price2\" value=\"$item_unit_price2\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_maintain_info\" value=\"".intranet_htmlspecialchars($item_maintain_info)."\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_remark\" value=\"".intranet_htmlspecialchars($item_remark)."\">\n";
for($i=0; $i<$attachment_size; $i++)
{
	$table_content .= "<input type=\"hidden\" name=\"item_attachment_$i\" value=\"${"item_attachment_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"hidden_item_attachment_$i\" value=\"".intranet_htmlspecialchars(${"hidden_item_attachment_$i"})."\">\n";
}
$table_content .= "<input type=\"hidden\" name=\"targetExistItem\" value=\"$targetExistItem\">\n";

# Upload Photo #
$lf = new libfilesystem();
$re_path = "/file/inventory/tmp_photo/".$UserID;;
$path = $intranet_root.$re_path;
//$photo = stripslashes(${"hidden_item_photo"});
$photo = urlencode($item_photo_name);
$target = "";

if($item_photo=="none" || $item_photo== ""){
}
else
{
	$lf = new libfilesystem();
	if (!is_dir($path))
	{
		$lf->folder_new($path);
	}
	
	$ext = strtoupper($lf->file_ext($photo));
	if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG")
	{
		$target = "$path/$photo";
		//$filename .= "/$photo";
		$lf->lfs_copy($item_photo, $target);
	}
}

# End of upload photo #

# Upload Attachment #
$re_path = "/file/inventory/tmp_attachment/".$UserID."/";
$path = "$intranet_root$re_path";

for($i=0; $i<$attachment_size; $i++)
{
    $file = stripslashes(${"hidden_item_attachment_".$i});
    $re_file = stripslashes(${"item_attachment_".$i});
	$target = "";
	
	if($re_file == "none" || $re_file == ""){
	}
	else
	{
		$lf = new libfilesystem();
		if (!is_dir($path))
		{
			$lf->folder_new($path);
		}
		
		$target = "$path/$file";
		$attachementname = "/$file";
		
		if($lf->lfs_copy($re_file, $target) == 1)
		{
			$attachment[$i]['FileName'] = $attachementname;
			$attachment[$i]['Path'] = $re_path;
		}
	}
}

# End of upload attachment #


# Generate Sub-Location Code JS Array #
$js2 .= "var arr_sub_location = new Array();\n";
$sql = "SELECT LocationID, Code FROM INVENTORY_LOCATION";
$arr_result = $linventory->returnArray($sql,2);
if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($sub_location_id, $sub_location_code) = $arr_result[$i];
		$js2 .= "if(arr_sub_location[$sub_location_id] == null) arr_sub_location[$sub_location_id] = new Array();\n";
		$js2 .= "arr_sub_location[$sub_location_id].push(new Array('".addslashes($sub_location_code)."'));\n";
	}
}

# Generate Group Code JS Array #
$js2 .= "var arr_group = new Array();\n";
$sql = "SELECT AdminGroupID, Code FROM INVENTORY_ADMIN_GROUP";
$arr_result = $linventory->returnArray($sql,2);
if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($group_id, $group_code) = $arr_result[$i];
		$js2 .= "if(arr_group[$group_id] == null) arr_group[$group_id] = new Array();\n";
		$js2 .= "arr_group[$group_id].push(new Array('".addslashes($group_code)."'));\n";
	}
}

# Generate Funding Source Code JS Array #
$js2 .= "var arr_funding = new Array();\n";
$sql = "SELECT FundingSourceID, Code FROM INVENTORY_FUNDING_SOURCE";
$arr_result = $linventory->returnArray($sql,2);
if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($funding_id, $funding_code) = $arr_result[$i];
		$js2 .= "if(arr_funding[$funding_id] == null) arr_funding[$funding_id] = new Array();\n";
		$js2 .= "arr_funding[$funding_id].push(new Array('".addslashes($funding_code)."'));\n";
	}
}


## Get all existing Barcode ##
$sql = "SELECT a.TagCode as Barcode FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) WHERE b.ItemType = 1 AND b.RecordStatus = 1";
$sql.= " UNION (SELECT c.Barcode FROM INVENTORY_ITEM_BULK_EXT AS c INNER JOIN INVENTORY_ITEM AS d ON c.ItemID=d.ItemID WHERE d.ItemType=2 AND d.RecordStatus=1 $existing_bulk_item_barcode_cond)";
$arr_result = $linventory->returnArray($sql,1);
$js3 .= "var arr_existing_barcode = new Array();\n";
if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($exist_barcode) = $arr_result[$i];
		$js3 .= "arr_existing_barcode.push('".$exist_barcode."');\n";
	}
}

## Get all Item Code (included written-off item)##
//$sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE RecordStatus = 1";
$sql = "SELECT ItemCode FROM INVENTORY_ITEM";
$arr_result = $linventory->returnArray($sql,1);
$js4 .= "var arr_existing_itemcode = new Array();\n";
if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($exist_itemcode) = $arr_result[$i];
		$js4 .= "arr_existing_itemcode.push('".$exist_itemcode."');\n";
	}
}

$js_item_total_qty = $targetItemType == 1? $item_total_qty : 1; // bulk item just count as 1

# show current title #
$PAGE_NAVIGATION[] = array($i_InventorySystem_PurchaseDetails);
$STEPS_OBJ[] = array($i_InventorySystem_AddNewItem_Step1, 0);
$STEPS_OBJ[] = array($i_InventorySystem_AddNewItem_Step2, 1);

### retrieve perivous data
$categoryName = $linventory->returnCategoryName($targetCategory);
$category2Name = $linventory->returnSubCategoryName($targetCategory2);
if(!empty($exist_item_code))
{
	$temp_name = $linventory->returnItemNameByItemCode($exist_item_code);
	$item_chi_name = $temp_name[0]['NameChi'];
	$item_eng_name = $temp_name[0]['NameEng'];
}

?>

<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul {padding:5px 0;width:100%;}
    #statescontainer li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight {background:#FFFFFF;}
	
    #statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<script language="javascript">
<? if($item_total_qty != ""){ ?>
var qty = <?=$item_total_qty;?>;
<? }else{ ?>
var qty = 0;
<? } ?>

<?echo $js_ItemCodeFormat;?>
<?echo $js_location_level;?>
<?echo $js;?>
<?echo $js2;?>
<?echo $js3;?>
<?echo $js4;?>

function setAllBuilding(val)
{
	eval("document.form1.targetLocationLevel_all").options.length = 1;
	if(val != "")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetLocationLevel_"+i).options.length = 1;
			eval("document.form1.BuildingSelected_"+i).value = val;
			if(typeof(building_arr[val])!="undefined")
			{
				for (var a=1; a<building_arr[val].length; a++)
				{
					eval("document.form1.targetLocationLevel_"+i).options[a] = new Option(building_arr[val][a][2],building_arr[val][a][1]);
					eval("document.form1.targetLocationLevel_all").options[a] = new Option(building_arr[val][a][2],building_arr[val][a][1]);
				}
			}
		}
	}
	else if (val=="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.BuildingSelected_"+i).value = '';
			eval("document.form1.targetLocationLevel_"+i).options.length = 1;
		}	
	}
}

function setAllLocation(val)
{
	eval("document.form1.targetLocation_all").options.length = 1;
	if (val!="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetLocation_"+i).options.length = 1;
			eval("document.form1.targetLocationLevel_"+i).value = val;
			if(typeof(level_arr[val])!="undefined")
			{
				for (var a=1; a<level_arr[val].length; a++)
				{
					eval("document.form1.targetLocation_"+i).options[a] = new Option(level_arr[val][a][2],level_arr[val][a][1]);
					eval("document.form1.targetLocation_all").options[a] = new Option(level_arr[val][a][2],level_arr[val][a][1]);
				}
			}
		}
	}
	else if (val=="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetLocationLevel_"+i).value = '';
			eval("document.form1.targetLocation_"+i).options.length = 1;
		}	
	}
}

function setAllSubLocation(val)
{
	if (val!="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetLocation_"+i).value = val;
		}
	} 
	else if (val=="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetLocation_"+i).value = '';
		}	
	}
	
}

function setLocation(target, val)
{
	eval("document.form1."+target).options.length = 1;
	if (val!="")
	{	
		if(typeof(building_arr[val])!="undefined")
		{
			for (var i=1; i<building_arr[val].length; i++)
				eval("document.form1."+target).options[i] = new Option(building_arr[val][i][2],building_arr[val][i][1]);
		}
	}
}

function setSubLocation(target, val)
{
	//document.getElementById(target).options.length = 1;
	eval("document.form1."+target).options.length = 1;
	if (val!="")
	{	
		if(typeof(level_arr[val])!="undefined")
		{
			for (var i=1; i<level_arr[val].length; i++)
				eval("document.form1."+target).options[i] = new Option(level_arr[val][i][2],level_arr[val][i][1]);
		}
	}
}

function setAllGroup(val)
{
	if (val!="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetGroup_"+i).value = val;
		}
	} 
	else if (val=="")
	{
		for(i=0; i<qty; i++)
		{
			eval("document.form1.targetGroup_"+i).value = '';
		}	
	}
	
}
function setAllSerialNum(val)
{
	for(var i=0; i<item_count; i++)
	{
		var obj = eval("document.form1.item_serial_"+i);
	        	obj.value = val;
	}
}
function setAllLicenseType(val)
{
	for(var i=0; i<item_count; i++)
	{
		var obj = eval("document.form1.item_license_"+i);
	        	obj.value = val;
	}
}
function setAllExpiryDate(val)
{
	for(var i=0; i<item_count; i++)
	{
		var obj = eval("document.form1.item_warranty_expiry_date_"+i);
	        	obj.value = val;
	}
}
function setAllResourceItem(val)
{
	for(var i=0; i<item_count; i++)
	{
		var obj = eval("document.form1.ResourceItem_"+i);
	        	obj.checked = val;
	}
}
</script>

<script language="javascript">
<!--
var tmp_item_num1 = 0;
var tmp_item_num2 = 0;

// start AJAX
var callback_barcode = {
		success: function ( o )
        {
	        var tmp_str = o.responseText;
	        var tmp_barcode = tmp_str.split(",");
	        
	        for(j=0; j<tmp_item_num1; j++)
	        {
	        	var tmp_obj = eval("document.form1.item_barcode_"+j);
	        	tmp_obj.value = tmp_barcode[j];
            }
        }
}
var callback_item_code = {
		success: function ( o )
        {
	        var tmp_str = o.responseText;
	        var tmp_item_code = tmp_str.split(",");
	        
	        for(j=0; j<tmp_item_num2; j++)
	        {
	        	var tmp_obj = eval("document.form1.item_code_"+j);
	        	tmp_obj.value = tmp_item_code[j];
            }
        }
}

function GenItemBarcode(item_num)
{
	obj = document.form2;
	var myElement = document.getElementById("item_chi_name");
    tmp_item_num1 = parseInt(item_num);
    YAHOO.util.Connect.setForm(obj);
        
    // page for processing and feedback
    var path = "generateItemBarcode.php?total_item="+tmp_item_num1;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_barcode);
}

function GenItemCode(item_type,item_cat1,item_cat2,item_num)
{
	<? if($sys_custom['CatholicEducation_eInventory'] || $sys_custom['eInventoryCustForSKH']) { ?>
		<? if($targetItemType == 2){ ?>
	    	var item_count = 1;
		<? } ?>
		
		var groupIDs = "";
		var group_count = [];
		for(var i=0; i<item_count; i++)
		{
			var objResourceGroup = eval("document.form1.targetGroup_"+i);
			if(typeof(objResourceGroup) == "undefined"){
				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>");
				return false;
			}
			var groupSelectedIndex = objResourceGroup.selectedIndex;
			if(groupSelectedIndex == ""){
				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>");
				return false;
			}
			var groupID = objResourceGroup[groupSelectedIndex].value;
			groupIDs = groupIDs + "," + groupID;
		}
			
		$.ajax({
			url: "ajax_load_form.php",
			type: "POST",
			async: false,
			<? if($sys_custom['eInventoryCustForSKH']) {?>
				data: 'Action=SKH_ItemCode_MaxNo&groupIDs='+ groupIDs +'&item_cat1='+ item_cat1+'&item_cat2='+ item_cat2+'&codes=<?=$item_code_prefix?>'+'&item_purchase_date=<?=$purhcase_date_format?>',
			<? } else { ?>
				data: 'Action=CountGroupCatSubCatMaxNo&groupIDs='+ groupIDs +'&item_cat1='+ item_cat1+'&item_cat2='+ item_cat2+'&codes=<?=$item_code_prefix?>',
			<? } ?>
			success: function(data){
				var temp=data.split(";");
				for(var i=0; i<temp.length-1; i++)
				{
					$gc = temp[i].split(":");
					group_count[$gc[0]] = $gc[1];
				}
			}
			});
			
		for(var i=0; i<item_count; i++)
		{
			var pos = parseInt(i)+1;
			var obj = document.form1;
			
			var objResourceGroup = eval("document.form1.targetGroup_"+i);
			var groupSelectedIndex = objResourceGroup.selectedIndex;
			var groupID = objResourceGroup[groupSelectedIndex].value;
			var groupCode = arr_group[groupID];
			
			var curr_num = ++group_count[groupID];
			curr_num2 = zeroPad(curr_num,5);
			
			var itemCode = "";
			
			<? if($sys_custom['eInventoryCustForSKH']) {?>
				/////////////////////////////////////////////////////////////////////////
				////// SKH eInventory
				//var itemCode = Purchase year / groupCode / category code /  sub-category code / 5 digits number
				/////////////////////////////////////////////////////////////////////////
				itemCode = "<?=$purhcase_date_format?>" + "/"+ groupCode + "/" + "<?=$item_code_prefix?>" + "/" + curr_num2;
			<? } else { ?>
				/////////////////////////////////////////////////////////////////////////
				////// CatholicEducation_eInventory
				//var itemCode = groupCode - category - sub-category - 5 digits number
				/////////////////////////////////////////////////////////////////////////
				itemCode = groupCode + "-" + "<?=$item_code_prefix?>" + "-" + curr_num2;
			<? } ?>
			
			var objItemCode = eval("document.form1.item_code_"+i);
			objItemCode.value = itemCode;
		}
	<? } else { ?>
	
    	<? if(!$sys_custom['eInventory_ItemCodeFormat_New'] || !$sys_custom['eInventory_SysPpt']){ ?>
    	   <? if($sys_custom['eInventory_ItemCodeFormat_New']){ ?>
    	    
        	    <? if($targetItemType == 2){ ?>
        	    	var item_count = 1;
        		<? } ?>
        	    for(var i=0; i<item_count; i++)
        		{
        			var pos = parseInt(i)+1;
        			var obj = document.form1;
        			
        			var objPurchaseYear = obj.item_purchase_date.value;
        			var purchaseYear = objPurchaseYear.substring(0,4);
        		
        			var objLocation = eval("document.form1.targetLocation_"+i);
        			if(typeof(objLocation) == "undefined"){
        				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectLocation;?>");
        				return false;
        			}
        			var locationSelectedIndex = objLocation.selectedIndex;
        			if(locationSelectedIndex == ""){
        				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectSubLocation;?>");
        				return false;
        			}
        			var subLocationID = objLocation[locationSelectedIndex].value;
        			var subLocationCode = arr_sub_location[subLocationID];
        			
        			var objResourceGroup = eval("document.form1.targetGroup_"+i);
        			if(typeof(objResourceGroup) == "undefined"){
        				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>");
        				return false;
        			}
        			var groupSelectedIndex = objResourceGroup.selectedIndex;
        			if(groupSelectedIndex == ""){
        				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>");
        				return false;
        			}
        			var groupID = objResourceGroup[groupSelectedIndex].value;
        			var groupCode = arr_group[groupID];
        			
        			<? if($targetItemType == 1){ ?>
        				var fundingSourceID = document.form1.item_funding.value;
        				var fundingSourceCode = arr_funding[fundingSourceID];
        			<? }else{ ?>
        				var objFunding = eval("document.form1.targetBulkFunding");
        				if(typeof(objFunding) == "undefined"){
        					alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectFunding;?>");
        					return false;
        				}
        				var fundingSelectedIndex = objFunding.selectedIndex;
        				if(fundingSelectedIndex == ""){
        					alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectFunding;?>");
        					return false;
        				}
        				var fundingSourceID = objFunding[fundingSelectedIndex].value;
        				var fundingSourceCode = arr_funding[fundingSourceID];
        			<? } ?>
        		}
        		
        		// gen item code
        		var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
        			$.ajax({  
        				type: "POST",  
        				url: "generateItemCode_format.php",
        				data: jsSubmitString,  
        				success: function(data) {
        					var d = data.split("::");
        					for(di=0;di<=d.length;di++)
        					{
        						var objItemCode = eval("document.form1.item_code_"+di);
        						objItemCode.value = d[di];
        					}
        				} 
        			});
        
        	<? }elseif ($sys_custom['eInventory_SysPpt']){ ?>
        	    <? if($targetItemType == 2){ ?>
            	var item_count = 1;
        	<? } ?>
            for(var i=0; i<item_count; i++)
        	{
        		var pos = parseInt(i)+1;
        		var obj = document.form1;
        		
        		var objPurchaseYear = obj.item_purchase_date.value;
        		var purchaseYear = objPurchaseYear.substring(0,4);
        	
        		var objLocation = eval("document.form1.targetLocation_"+i);
        		if(typeof(objLocation) == "undefined"){
        			alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectLocation;?>");
        			return false;
        		}
        		var locationSelectedIndex = objLocation.selectedIndex;
        		if(locationSelectedIndex == ""){
        			alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectSubLocation;?>");
        			return false;
        		}
        		var subLocationID = objLocation[locationSelectedIndex].value;
        		var subLocationCode = arr_sub_location[subLocationID];
        		
        		var objResourceGroup = eval("document.form1.targetGroup_"+i);
        		if(typeof(objResourceGroup) == "undefined"){
        			alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>");
        			return false;
        		}
        		var groupSelectedIndex = objResourceGroup.selectedIndex;
        		if(groupSelectedIndex == ""){
        			alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>");
        			return false;
        		}
        		var groupID = objResourceGroup[groupSelectedIndex].value;
        		var groupCode = arr_group[groupID];
        		
        		<? if($targetItemType == 1){ ?>
        			var fundingSourceID = document.form1.item_funding.value;
        			var fundingSourceCode = arr_funding[fundingSourceID];
        		<? }else{ ?>
        			var objFunding = eval("document.form1.targetBulkFunding");
        			if(typeof(objFunding) == "undefined"){
        				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectFunding;?>");
        				return false;
        			}
        			var fundingSelectedIndex = objFunding.selectedIndex;
        			if(fundingSelectedIndex == ""){
        				alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectFunding;?>");
        				return false;
        			}
        			var fundingSourceID = objFunding[fundingSelectedIndex].value;
        			var fundingSourceCode = arr_funding[fundingSourceID];
        		<? } ?>
        	}
        	
        	// gen item code
        	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
        		$.ajax({  
        			type: "POST",  
        			url: "generateItemCode_format.php",
        			data: jsSubmitString,  
        			success: function(data) {
        				var d = data.split("::");
        				for(di=0;di<=d.length;di++)
        				{
        					var objItemCode = eval("document.form1.item_code_"+di);
        					objItemCode.value = d[di];
        				}
        			} 
        		});
        	<?}else{?>
        			obj = document.form2;
        			var myElement = document.getElementById("item_chi_name");
        		    tmp_item_num2 = parseInt(item_num);
        		    YAHOO.util.Connect.setForm(obj);
        		        
        		    // page for processing and feedback
        		    var path = "generateItemCode.php?item_type="+item_type+"&item_cat1="+item_cat1+"&item_cat2="+item_cat2+"&total_item="+tmp_item_num2+"&item_purchase_date=<?=$item_purchase_date?>";
        		    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_item_code);
        	<? } ?>
    	<? }?>
	<? } ?>
}
		
function grapAttach(cform)
{
	var obj = document.form1.item_photo;	
	var key;
	var s="";
	key = obj.value;
	if (key!="")
		s += key;
	cform.attachStr.value = s;
}

function checkItemBarcodeFormat()
{
	var barcode_max_length = <?=$BarcodeMaxLength;?>;
	var barcode_format = <?=$BarcodeFormat;?>;
	var barcode_checking = 0;
	
	if(barcode_format == 1)
	{
		var ValidChars = new RegExp("[0-9\ \.\/\|\$\-]");
		
		for(j = 0; j < <?=$js_item_total_qty;?>; j++)
		{
			objBarcode = "item_barcode_"+j;
			var tmp_obj = document.getElementById(objBarcode);
			var tmp_length = tmp_obj.value.length;
			var IsNumber=true;
			var Char;
			if(tmp_obj.value != "")
			{
				if(tmp_length > barcode_max_length)
				{
					IsNumber=false;
				}
				for(i=0; i<tmp_length; i++)
				{
					if(ValidChars.test(Char = tmp_obj.value.charAt(i)) == false)
					{
						IsNumber=false;
					}
				}
			}
			else
			{
				IsNumber = false;
			}
			if(IsNumber == true)
			{
				barcode_checking++;
			}
			else
			{
				barcode_checking--;
			}
		}
	}
   
	if(barcode_format == 2)
	{
		var ValidChars = new RegExp("[0-9A-Z\ \.\/\|\$\-]");
		
		for(j = 0; j < <?=$js_item_total_qty;?>; j++)
		{
			objBarcode = "item_barcode_"+j;
			var tmp_obj = document.getElementById(objBarcode);
			var tmp_length = tmp_obj.value.length;
			var IsNumber = true;
			var Char;
			if(tmp_obj.value != "")
			{
				if(tmp_length > barcode_max_length)
				{
					IsNumber=false;
				}
				for(i=0; i<tmp_length; i++)
				{
					if(ValidChars.test(Char = tmp_obj.value.charAt(i)) == false)
					{
						IsNumber=false;
					}
				}
			}
			else
			{
				IsNumber = false;
			}
			if(IsNumber == true)
			{
				barcode_checking++;
			}
			else
			{
				barcode_checking--;
			}
		}
	}
	return barcode_checking;
}
function checkItemCode()
{
	var itemcode_checking = 0;
	
	for(i = 0; i < <?=$item_total_qty;?>; i++)
	{
		objItemCode = "item_code_"+i;
		var tmp_obj = document.getElementById(objItemCode);
		if(tmp_obj.value != "")
		{
			itemcode_checking++;
		}
	}
	return itemcode_checking;
}

function in_array(stringToSearch, arrayToSearch) {
	for (s = 0; s <arrayToSearch.length; s++) {
		thisEntry = arrayToSearch[s].toString();
		if (thisEntry == stringToSearch) {
			return true;
		}
	}
	return false;
}

function checkForm() {
	document.getElementById('new_item_submit').disabled = true;
	obj = document.form1;
	var item_type = <?=$targetItemType;?>;
	var tmp_check_barcode_format = 0;
	var tmp_check_item_code = 0;
	var tmp_check_location = 0;
	var tmp_check_group = 0;
	var tmp_final_checking = 0;
	
	//Big5FileUploadHandler();
	
	if(item_type == 1)
	{
		tmp_check_item_code = checkItemCode();
		tmp_check_barcode_format = checkItemBarcodeFormat();
		var arr_temp_barcode = new Array();
		var arr_temp_itemcode = new Array();
		
		if(tmp_check_item_code == <?=$item_total_qty;?>)
		{
			if(tmp_check_barcode_format == <?=$item_total_qty;?>)
			{
				for(i = 0; i < <?=$item_total_qty;?>; i++)
				{
					objBarcode = "item_barcode_"+i;
					var tmp_obj = document.getElementById(objBarcode);
					
					// check any duplicate barcode in DB
					result = in_array(tmp_obj.value, arr_existing_barcode);
					if(result){
						alert("<?=$i_InventorySystem_Input_Item_Barcode_Exist_Warning?>");
						document.getElementById('new_item_submit').disabled = false;
						return false;
					}
					
					// check any duplicate barcode in the form
					result2 = in_array(tmp_obj.value, arr_temp_barcode);
					if(result2){
						alert("<?=$i_InventorySystem_Input_Item_Barcode_Exist_Warning?>");
						document.getElementById('new_item_submit').disabled = false;
						return false;
					}	
					arr_temp_barcode.push(tmp_obj.value);
					
					objItemCode = "item_code_"+i;
					var tmp_obj2 = document.getElementById(objItemCode);
					// check any duplicate item code in DB
					result = in_array(tmp_obj2.value, arr_existing_itemcode);
					if(result){
						alert("<?=$i_InventorySystem_Input_ItemCode_Exist_Warning?>");
						document.getElementById('new_item_submit').disabled = false;
						return false;
					}
					
					// check any duplicate item code in the form
					result2 = in_array(tmp_obj2.value, arr_temp_itemcode);
					if(result2){
						alert("<?=$i_InventorySystem_Input_ItemCode_Exist_Warning?>");
						document.getElementById('new_item_submit').disabled = false;
						return false;
					}	
					arr_temp_itemcode.push(tmp_obj2.value);
										
					objLocation = "targetLocation_"+i;
					var tmp_obj1 = document.getElementById(objLocation);
					if(check_select(tmp_obj1,"<?=$i_InventorySystem_JSWarning_NewItem_SelectLocation;?>",0))
					{
						tmp_check_location++;
					}
					else
					{
						tmp_check_location--;
						break;
					}
				}
				if(tmp_check_location == <?=$item_total_qty;?>)
				{		
					for(i = 0; i < <?=$item_total_qty;?>; i++)
					{
						objGroup = "targetGroup_"+i;
						var tmp_obj2 = document.getElementById(objGroup);
						if(check_select(tmp_obj2,"<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>",0))
						{
							tmp_check_group++;
							tmp_final_checking++;
						}
						else
						{
							tmp_check_group--;
							tmp_final_checking--;
							break;
						}
					}
				}
			
			}
			else
			{
				alert("<?=$i_InventorySystem_JSWarning_NewItem_InvalidItemBarcode;?>");
				document.getElementById('new_item_submit').disabled = false;
				return false;
			}
		}
		else
		{
			alert("<?=$i_InventorySystem_JSWarning_NewItem_ItemCodeEmpty;?>");
			document.getElementById('new_item_submit').disabled = false;
			return false;
		}
		
 				
		if(tmp_final_checking == <?=$item_total_qty;?>)
		{
			if(countChecked(document.form1,'ResourceItem[]')>0)
				obj.action = "new_item3.php";
			else
				obj.action = "new_item_update.php";
			return true;
		}
		else
		{
			document.getElementById('new_item_submit').disabled = false;
			return false;
		}
	}
	
	if(item_type == 2)
	{
		tmp_check_barcode_format = checkItemBarcodeFormat();
		if(tmp_check_barcode_format == 1)
		{
			for(i = 0; i < 1; i++)
			{
				objBarcode = "item_barcode_"+i;
				var tmp_obj = document.getElementById(objBarcode);
				
				// check any duplicate barcode in DB
				result = in_array(tmp_obj.value, arr_existing_barcode);
				if(result){
					alert("<?=$i_InventorySystem_Input_Item_Barcode_Exist_Warning?>");
					document.getElementById('new_item_submit').disabled = false;
					return false;
				}
			}
		}else
		{
			alert("<?=$i_InventorySystem_JSWarning_NewItem_InvalidItemBarcode;?>");
			document.getElementById('new_item_submit').disabled = false;
			return false;
		}
		if(check_text(document.form1.item_code_0, "<?=$i_InventorySystem_JSWarning_NewItem_ItemCodeEmpty?>"))
		{
			<? if(!$exist_item_code) {?>
			objItemCode = "item_code_0";
			var tmp_obj2 = document.getElementById(objItemCode);
			// check any duplicate item code in DB
			result = in_array(tmp_obj2.value, arr_existing_itemcode);
			if(result){
				alert("<?=$i_InventorySystem_Input_ItemCode_Exist_Warning?>");
				document.getElementById('new_item_submit').disabled = false;
				return false;
			}
			<? } ?>
			
			//var objLocationLevel = document.getElementById("targetLocationLevel_0")
			//if(check_select(objLocationLevel,"<?=$i_InventorySystem_JSWarning_NewItem_SelectLocation;?>",0))
			//{
				var objLocation = document.getElementById("targetLocation_0");
				if(check_select(objLocation,"<?=$i_InventorySystem_JSWarning_NewItem_SelectLocation;?>",0))
				{
					var objGroup = document.getElementById("targetGroup_0");
					if(check_select(objGroup,"<?=$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup;?>",0))
					{
						var objBulkFunding = document.form1.targetBulkFunding;
						if(check_select(objBulkFunding,"<?=$i_InventorySystem_JSWarning_NewItem_SelectFunding;?>",0))
						{
							tmp_final_checking++;
						}
						else
						{
							tmp_final_checking--;
						}
					}
				}
			//}
		}

		if(tmp_final_checking > 0)
		{
			obj.action = "new_item_update.php";
			return true;
		}
		else
		{
			document.getElementById('new_item_submit').disabled = false;
			return false;
		}
	}
}

function check_exists(i, location_id)
{
	$('#div_group').load
	(
		'ajax_load_form.php', 
		{	
			Action: "Check_Resources_Mgt_Group",
			targetItemID: '<?=$exist_item_id?>',
			targetLocationID: location_id,
			targetLocation_index: i
		}, 
		function (data) { }
	); 
}
-->
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>

<form name="form2" id="form2" action="" method="post"></form>

<table class="form_table_v30">

<tr>
	<td class="field_title"><?=$i_general_Type?></td>
	<td><?=$targetItemType==1? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem['Category']?></td>
	<td><?=$categoryName?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem['SubCategory']?></td>
	<td><?=$category2Name?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem_Item_ChineseName?></td>
	<td><?=$item_chi_name?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem_Item_EnglishName?></td>
	<td><?=$item_eng_name?></td>
</tr>
</table>

<form name="form1" id="form1" enctype="multipart/form-data" action="" method="post" onSubmit="return checkForm();">
<? if($targetItemType == 1 || $targetItemType == 2) { 
	$warning_msg = $linterface->GET_SYS_MSG("",$i_InventorySystem_NewItem_ItemBarcodeRemark);
?>
<table width="100%" border="0" cellpadding="0" cellspacing="5" align="center">
<tr><td><?=$warning_msg;?></td></tr>
</table>
<? } ?>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content?>
</table>
<br>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "", "new_item_submit");?>
	<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2");?>
	<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='items_full_list.php'","cancelbtn");?>
	<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:document.form1.action='new_item.php';document.form1.submit();","backbtn");?>
<p class="spacer"></p>
</div>




<input type="hidden" name="category_id" value="<?=$category_id?>" />
<input type="hidden" name="category2_id" value="<?=$category2_id?>" />
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="attachment_size" value="<?=$attachment_size?>" />
<input type="hidden" name="Tags" value="<?=$Tags?>" />

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>