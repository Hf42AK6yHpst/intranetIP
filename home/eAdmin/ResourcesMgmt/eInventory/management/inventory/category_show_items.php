<?php
# using:

########################################################
#	Date:	2016-02-04	Henry
#			php 5.4 issue move set cookies after includes file 
########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_category_item_browsing_view_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_category_item_browsing_view_record_page_number", $pageNo, 0, "", "", 0);
	$ck_category_item_browsing_view_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_category_item_browsing_view_record_page_number!="")
{
	$pageNo = $ck_category_item_browsing_view_record_page_number;
}

if ($ck_category_item_browsing_view_record_page_order!=$order && $order!="")
{
	setcookie("ck_category_item_browsing_view_record_page_order", $order, 0, "", "", 0);
	$ck_category_item_browsing_view_record_page_order = $order;
} else if (!isset($order) && $ck_category_item_browsing_view_record_page_order!="")
{
	$order = $ck_category_item_browsing_view_record_page_order;
}

if ($ck_category_item_browsing_view_record_page_field!=$field && $field!="")
{
	setcookie("ck_category_item_browsing_view_record_page_field", $field, 0, "", "", 0);
	$ck_category_item_browsing_view_record_page_field = $field;
} else if (!isset($field) && $ck_category_item_browsing_view_record_page_field!="")
{
	$field = $ck_category_item_browsing_view_record_page_field;
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface		= new interface_html();
$linventory		= new libinventory();

### Get The Category Name ###
$namefield1 = $linventory->getInventoryItemNameByLang("a.");
$namefield2 = $linventory->getInventoryItemNameByLang("b.");

$sql = "SELECT 
				$namefield1,
				$namefield2
		FROM 
				INVENTORY_CATEGORY AS a INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID AND b.Category2ID = $cat2_id)
		WHERE
				a.CategoryID = $cat_id";

$result = $linventory->returnArray($sql,2);
### End Of Getting The Category Name ###

$temp[] = array($result[0][0],"");
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 
$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($result[0][1])."</td></tr>"; 

# Generate System Message #
if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

### Title ###
//$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php", 1);
//$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/location.php", 0);
//$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/group.php", 0);
//$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/fundingsource.php", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

//$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('category_item_insert.php?cat_id=$cat_id&cat2_id=$cat2_id')");

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'ItemID[]','category_show_items_remove.php?cat_id=$cat_id&cat2_id=$cat2_id')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ItemID[]','category_show_items_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field=$field==""?2:$field;
switch($field){
	case 0: $field = 0; break;
	case 1: $field = 2; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	default: $field = 2;
}

if (!isset($order)) $order = 0;

$namefield = $linventory->getInventoryItemNameByLang("a.");

/*
$sql = "SELECT 
				IF(d.PhotoName != '',CONCAT('<img src=\"../../../../../..',d.PhotoPath,'/',d.PhotoName,'\" height=\"100px\" width=\"100px\">'),'&nbsp;'),
				a.ItemCode,
				CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">',$namefield,'</a>'),
				IF(a.ItemType = 2,IF(b.Quantity = '', 0, b.Quantity),IF(a.ItemID != '','1','0')),
				IF(a.ItemType=1,CONCAT('single'),CONCAT('bulk'))
		FROM 
				INVENTORY_ITEM AS a LEFT OUTER JOIN
				INVENTORY_ITEM_BULK_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS c ON (a.ItemID = c.ItemID) LEFT OUTER JOIN
				INVENTORY_PHOTO_PART AS d ON (a.PhotoLink = d.PartID)
		WHERE
				a.RecordStatus = 1 AND
				a.CategoryID = $cat_id AND
				a.Category2ID = $cat2_id";
*/

## Item Status Condition
if (!isset($selectItemStatus))
{
	$selectItemStatus = 1;
}

## Location Condition
$sql = "SELECT 
				c.LocationID
		FROM
				INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
				INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
				INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
		WHERE
				a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
$result = $linventory->returnVector($sql);
if(sizeof($result)>0)
{
	$location_list = implode(",",$result);
	
	$location_condition1 = " AND e.LocationID IN ($location_list) ";
	$location_condition2 = " AND g.LocationID IN ($location_list) ";

	$cond .= " AND (e.LocationID IN ($location_list) OR g.LocationID IN ($location_list)) ";

}

## Admin Group Condition
if($linventory->IS_ADMIN_USER($UserID))
{
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
	$arr_tmp_admin = $linventory->returnVector($sql);
	$target_admin_group = implode(",",$arr_tmp_admin);
	
	if($target_admin_group != "")
	{
		$admin_condition1 = " AND e.GroupInCharge IN (".$target_admin_group.") ";
		$admin_condition2 = " AND g.GroupInCharge IN (".$target_admin_group.") ";
	}
	else
	{
		$admin_condition1 = " AND e.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
		$admin_condition2 = " AND g.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
	}
}
else
{
	if($linventory->getInventoryAdminGroup() != "")
	{
		$admin_condition1 = " AND e.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
		$admin_condition2 = " AND g.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
	}
}

$sql = "SELECT
				IF(d.PhotoName != '',CONCAT('<img src=\"../../../../../..',d.PhotoPath,'/',d.PhotoName,'\" height=\"100px\" width=\"100px\">'),'&nbsp;'),
				a.ItemCode,
				CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">',$namefield,'</a>'),
				IF(a.ItemType = 2,IF(g.Quantity = '', 0, g.Quantity),IF(a.ItemID != '','1','0')),
				IF(a.ItemType=1,CONCAT('single'),CONCAT('bulk'))
		FROM 
				INVENTORY_ITEM AS a INNER JOIN 
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = '$selectItemStatus' $item_type_condition) INNER JOIN 
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = '$selectItemStatus' $item_type_condition) LEFT OUTER JOIN
				INVENTORY_PHOTO_PART AS d ON (a.PhotoLink = d.PartID) LEFT OUTER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID $location_condition1 $admin_condition1) LEFT OUTER JOIN 
				INVENTORY_ITEM_BULK_LOCATION AS g ON (a.ItemID = g.ItemID $location_condition2 $admin_condition2) LEFT OUTER JOIN
				INVENTORY_LOCATION AS h ON (e.LocationID = h.LocationID OR g.LocationID = h.LocationID) LEFT OUTER JOIN
				INVENTORY_LOCATION_LEVEL AS j ON (h.LocationLevelID = j.LocationLevelID) LEFT OUTER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (j.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
				INVENTORY_ADMIN_GROUP AS i ON (e.GroupInCharge = i.AdminGroupID OR g.GroupInCharge = i.AdminGroupID) LEFT OUTER JOIN
				INVENTORY_FUNDING_SOURCE AS k ON (e.FundingSource = k.FundingSourceID OR g.FundingSourceID = k.FundingSourceID)
		WHERE
				a.RecordStatus = 1 AND
				a.CategoryID = $cat_id AND
				a.Category2ID = $cat2_id
				$cond";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo, true);
$li->field_array = array("a.NameEng","a.ItemType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+4;
$li->title = "";
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = "eInventoryList";

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class=' tabletopnolink'>#</td>\n";
$li->column_list .= "<td class=' tabletopnolink'>".$li->column($pos++, "")."</td>\n";
$li->column_list .= "<td class=' tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Code)."</td>\n";
$li->column_list .= "<td class=' tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Report_ItemName)."</td>\n";
$li->column_list .= "<td class=' tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Qty)."</td>\n";
//$li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";

?>

<br>

<form name="form1" action="" method="POST">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
</table>
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<?=$infobar2?>
</table>

<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right" class="tabletext"><?= $SysMsg ?></td>
	</tr>
</table>
<br>
<?php echo $li->display("90%"); ?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
