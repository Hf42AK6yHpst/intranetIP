<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$li = new libresource($ResourceID[0]);
$bid = $li->TimeSlotBatchID;
$lb = new libbatch($li->TimeSlotBatchID);
$lo = new libgrouping();
$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";
$RecordType = $li->RecordType;
$defaultChk = ($RecordType==4? " CHECKED":"");

if(is_array($ItemID))
	$item_list = implode(",",$ItemID);

### Check Selected Item is Single Item or not ###
$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID IN ($item_list)";
$arr_item_type = $linventory->returnArray($sql,1);

if(sizeof($arr_item_type)>0)
{
	for($i=0; $i<sizeof($arr_item_type); $i++)
	{
		list($item_type) = $arr_item_type[$i];
		if($item_type != ITEM_TYPE_SINGLE)
		{
			header("Location: items_full_list.php?msg=15");
			exit();
		}
	}
}
### end of checking ###

$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/admin/inventory/management/inventory/items_full_list.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/management/inventory/category.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

# show current location #
$temp[] = array($i_InventorySystem_ResourceItemsSetting,"");
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$sql = "SELECT 
				ResourceID
		FROM
				INTRANET_RESOURCE_INVENTORY_ITEM_LIST
		WHERE
				ItemID IN ($item_list)
		";
$arr_check_exist_resource = $linventory->returnVector($sql);

if(sizeof($arr_check_exist_resource)>0)
{
	$exist_resource_id = $arr_check_exist_resource[0];
	
	$sql = "SELECT 
					ResourceID, ResourceCode, ResourceCategory, Title, Description, Remark, DaysBefore, RecordType, RecordStatus
			FROM
					INTRANET_RESOURCE
			WHERE
					ResourceID = $exist_resource_id
			";
	$arr_result = $linventory->returnArray($sql,7);
	
	if(sizeof($arr_result)>0)
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($resource_id, $resource_code, $resource_category, $resource_name, $resource_description, $resource_remark, $resource_day_in_advance, $resource_type, $resource_status) = $arr_result[$i];
			
			$defaultChk = ($resource_type==4? " CHECKED":"");
			
			if($resource_status == 0)
			{
				$publish_checked = " ";
				$pending_checked = " CHECKED ";
			}
			if($resource_status == 1)
			{
				$publish_checked = " CHECKED ";
				$pending_checked = " ";
			}
			
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceCode</td>
									<td><input type=\"text\" name=\"resource_code\" class=\"textboxtext2\" value=\"$resource_code\"></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceCategory</td>
									<td><input type=\"text\" name=\"resource_category\" class=\"textboxtext2\" value=\"$resource_category\">".$li->displayResourceCategory("resource_category")."</td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceTitle</td>
									<td><input type=\"text\" name=\"resource_name\" class=\"textboxtext2\" value=\"$resource_name\"></td></tr>";
			
			if($resource_day_in_advance == "")
				$resource_day_in_advance = 0;
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceDaysBefore</td>
									<td><input type=\"text\" class=\"textboxnum\" name=\"day_in_advance\" value=\"$resource_day_in_advance\"></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceTimeSlot</td>
									<td>".$lb->returnBatchSelect("name=batchID")."&nbsp;<span class=\"tabletextremark\">[$i_ResourceViewScheme]</span></td></tr>";
			
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceResetBooking</td>
									<td class=\"tabletext\"><input type=\"checkbox\" name=\"reset_record\" value=\"1\"><br><span class=\"tabletextremark\">$i_ResourceResetBooking2</span></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceDescription</td>
									<td><textarea name=\"resource_description\" class=\"textboxtext2\">$resource_description</textarea></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceRemark</td>
									<td><textarea name=\"resource_remark\" class=\"textboxtext2\">$resource_remark</textarea></td></tr>";
			
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceAutoApprove</td>
									<td><input type=\"checkbox\" name=\"auto_approve\" value='yes' $defaultChk></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceRecordStatus</td>
									<td><label class=\"tabletext\"><input type=\"radio\" name=\"resource_status\" value=\"1\" $publish_checked>&nbsp;$i_status_publish</label>&nbsp;<label class=\"tabletext\"><input type=\"radio\" name=\"resource_status\" value=\"0\" $pending_checked>&nbsp;$i_status_pending</label></td></tr>";
									
			$group_selection = $lo->displayResourceGroups($exist_resource_id);
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_admintitle_group</td>
									<td><label class=\"tabletext\">$group_selection</td></tr>";
									
			$table_content .= "<input type=\"hidden\" name=\"ExistingResource\" value=\"1\">";
			$table_content .= "<input type=\"hidden\" name=\"ResourceID\" value=\"$exist_resource_id\">";
		}
	}
}
else
{
	$sql = "SELECT 
					a.ItemID, a.NameEng, a.DescriptionEng, a.ItemCode, CONCAT(b.NameEng,' > ',c.NameEng)
			FROM 
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID)
			WHERE
					a.ItemID IN ($item_list)
			";

	$arr_result = $linventory->returnArray($sql,5);
	
	if(sizeof($arr_result)>0)
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($item_id, $resource_name, $resource_description, $resource_code, $resource_category) = $arr_result[$i];
			
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceCode</td>
									<td><input type=\"text\" name=\"resource_code\" class=\"textboxtext2\" value=\"$resource_code\"></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceCategory</td>
									<td><input type=\"text\" name=\"resource_category\" class=\"textboxtext2\" value=\"$resource_category\">".$li->displayResourceCategory("resource_category")."</td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceTitle</td>
									<td><input type=\"text\" name=\"resource_name\" class=\"textboxtext2\" value=\"$resource_name\"></td></tr>";
			
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceDaysBefore</td>
									<td><input type=\"text\" class=\"textboxnum\" name=\"day_in_advance\" value=\"0\"></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceTimeSlot</td>
									<td>".$lb->returnBatchSelect("name=batchID")."&nbsp;<span class=\"tabletextremark\">[$i_ResourceViewScheme]</span></td></tr>";
												
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceDescription</td>
									<td><textarea name=\"resource_description\" class=\"textboxtext2\">$resource_description</textarea></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceRemark</td>
									<td><textarea name=\"resource_remark\" class=\"textboxtext2\"></textarea></td></tr>";
			
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceAutoApprove</td>
									<td><input type=\"checkbox\" name=\"auto_approve\" value=\"yes\" CHECKED></td></tr>";
									
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ResourceRecordStatus</td>
									<td><label class=\"tabletext\"><input type=\"radio\" name=\"resource_status\" value=\"1\" CHECKED>&nbsp;$i_status_publish</label>&nbsp;<label class=\"tabletext\"><input type=\"radio\" name=\"resource_status\" value=\"0\">&nbsp;$i_status_pending</label></td></tr>";
									
			$group_selection = $lo->displayResourceGroups();
			$table_content .= "<tr><td width=\"35%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_admintitle_group</td>
									<td><label class=\"tabletext\">$group_selection</td></tr>";
									
			$table_content .= "<input type=\"hidden\" name=\"InventoryItemID\" value=\"$item_id\">";
			$table_content .= "<input type=\"hidden\" name=\"ExistingResource\" value=\"0\">";
		}
	}
}

?>

<script language="javascript">
function checkForm()
{
	obj = document.form1;
	if(obj.ExistingResource.value == 0)
	{
		checkOptionAll(obj.elements["GroupID[]"]);
		obj.action = "resource_booking_items_update.php";
		return true;
	}
	if(obj.ExistingResource.value == 1)
	{
		checkOptionAll(obj.elements["GroupID[]"]);		
		obj.action = "resource_booking_items_update.php";
		return true;
	}
}
</script>

<br>
<form name="form1" action="" method="POST" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<?=$infobar?>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content;?>

<tr><td colspan="2" align="center">
<?
	echo $linterface->GET_ACTION_BTN($button_submit, "submit")." ";
	echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ";
	echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='items_full_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
?>
</td></tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>