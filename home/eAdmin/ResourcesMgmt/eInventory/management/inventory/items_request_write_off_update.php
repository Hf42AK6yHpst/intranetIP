<?php

// ###################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-21 Henry
// add access right checking [Case#E135442]
//
// Date: 2012-03-20 YatWoon
// Fixed: Don't remove the history request records, bulk items will miss the approved records and leading incorrect unit price calculation
//
// Date: 2011-05-26 YatWoon
// Fixed: [Single] cannot add more photo if item is requested write-off before
// Fixed: [Single & Bulk] store attachement under "RecordID" folder to prevent file overwritten by same filename
// Fixed: [Bulk] Incorrect sql query so that cannot check the item is requested before
//
// ###################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");

$re_path = "/file/inventory/write_off";
$path = "$intranet_root$re_path";

$ItemID = $item_id;
$ItemLocation = $targetLocation;

if ($ItemType == ITEM_TYPE_SINGLE) {
    $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$targetWriteOffReason."'";
    $arr_write_off_reason = $linventory->returnVector($sql);
    $write_off_reason = $arr_write_off_reason[0];
    $write_off_reason = addslashes($write_off_reason);
    
    /*
     * # Discussed with Ken, no need to remove the records
     * ### [OLD] Check For the write off record is exist or not, exist->update / not exist->insert (problem: cannot add more photo if 2nd time request write-off)###
     * ### [NEW] Check For the write off record is exist or not, exist->delete, then insert ###
     * $sql = "SELECT
     * RecordID
     * FROM
     * INVENTORY_ITEM_WRITE_OFF_RECORD
     * WHERE
     * ItemID = $ItemID AND
     * LocationID = $ItemLocation AND
     * AdminGroupID = $ItemAdminGroup AND
     * RecordStatus = 0
     * ";
     * $arr_record_exist = $linventory->returnVector($sql);
     * if(sizeof($arr_record_exist) > 0)
     * {
     * $exist_record_id = $arr_record_exist[0];
     *
     * ## delete INVENTORY_ITEM_WRITE_OFF_RECORD
     * $sql = "delete from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordID=$exist_record_id";
     * $linventory->db_db_query($sql);
     *
     * ## delete files
     * ## implement if necessary
     *
     * ## delete INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
     * $sql = "delete from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$exist_record_id";
     * $linventory->db_db_query($sql);
     * }
     */
    
    /*
     * if(sizeof($arr_record_exist) > 0)
     * {
     * ### Update write off record ###
     * $exist_record_id = $arr_record_exist[0];
     *
     * $sql = "UPDATE
     * INVENTORY_ITEM_WRITE_OFF_RECORD
     * SET
     * WriteOffReason = '$write_off_reason',
     * RequestDate = '$curr_date',
     * RequestPerson = '$UserID',
     * DateModified = NOW()
     * WHERE
     * ItemID = $ItemID AND
     * LocationID = $ItemLocation AND
     * AdminGroupID = $ItemAdminGroup
     * ";
     * $result['UpdateInvItemBulkLog'] = $linventory->db_db_query($sql);
     *
     * $no_of_file = $no_of_file_upload;
     * if($no_of_file == "")
     * {
     * $no_of_file = 1;
     * }
     *
     * for($i=0; $i<$no_of_file; $i++)
     * {
     * $file = stripslashes(${"hidden_item_write_off_attachment_$i"});
     * $re_file = stripslashes(${"item_write_off_attachment_$i"});
     *
     * if($re_file == "none" || $re_file == ""){
     * }
     * else
     * {
     * $lf = new libfilesystem();
     * if (!is_dir($path))
     * {
     * $lf->folder_new($path);
     * }
     *
     * $target = "$path/$file";
     * $attachementname = "/$file";
     *
     * if($lf->lfs_copy($re_file, $target) == 1)
     * {
     * $attachment['FileName'] = $attachementname;
     * $attachment['Path'] = $re_path;
     *
     * $sql = "UPDATE
     * INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
     * SET
     * PhotoPath = '$re_path',
     * PhotoName = '$file',
     * DateModified = NOW()
     * WHERE
     * RequestWriteOffID = $exist_record_id
     * ";
     *
     * //echo $sql."<BR><BR>";
     * $linventory->db_db_query($sql);
     * }
     * }
     * }
     * }
     * else
     * {
     */
    $sql = "INSERT INTO
						INVENTORY_ITEM_WRITE_OFF_RECORD
						(ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, 
						RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($ItemID, '$curr_date', $ItemLocation, $ItemAdminGroup, 1, '$write_off_reason', '$curr_date', $UserID, 
						0, 0, NOW(), NOW())
				";
    // echo $sql."<BR>";
    $result['NewInvItemSingleStatusLog'] = $linventory->db_db_query($sql);
    $write_off_logID = $linventory->db_insert_id();
    
    $no_of_file = $no_of_file_upload;
    if ($no_of_file == "") {
        $no_of_file = 1;
    }
    
    for ($i = 0; $i < $no_of_file; $i ++) {
        $file = stripslashes(${"hidden_item_write_off_attachment_$i"});
        $re_file = stripslashes(${"item_write_off_attachment_$i"});
        
        if ($re_file == "none" || $re_file == "") {} else {
            $photo_path = $path . "/" . $write_off_logID;
            $photo_re_path = $re_path . "/" . $write_off_logID;
            
            $lf = new libfilesystem();
            if (! is_dir($photo_path)) {
                $lf->folder_new($photo_path);
            }
            
            $target = "$photo_path/$file";
            $attachementname = "/$file";
            
            if ($lf->lfs_copy($re_file, $target) == 1) {
                // $attachment['FileName'] = $attachementname;
                // $attachment['Path'] = $re_path;
                
                $sql = "INSERT INTO 
									INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
									(ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
							VALUES
									($ItemID, $write_off_logID, '$photo_re_path', '$file', NOW(), NOW())
							";
                $linventory->db_db_query($sql);
            }
        }
    }
    // }
    
    // eBooking #
    if ($resource_item_status != "") {
        $sql = "SELECT ResourceID FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ItemID = '".$ItemID."'";
        $arr_resource_item = $linventory->returnVector($sql);
        if (sizeof($arr_resource_item) > 0) {
            $resource_id = $arr_resource_item[0];
            $sql = "CREATE TABLE IF NOT EXISTS INTRANET_RESOURCE_WRITE_OFF
					 (ItemID int(11) NOT NULL,
					  ResourceID int(11) NOT NULL,
					  PendingMode int(1) NOT NULL,
					  PRIMARY KEY (ItemID),
					  UNIQUE ResourceItemMapID (ItemID, ResourceID)
					 )";
            $linventory->db_db_query($sql);
            
            $sql = "INSERT INTO INTRANET_RESOURCE_WRITE_OFF (ItemID, ResourceID, PendingMode) VALUES ($ItemID, $resource_id, $resource_item_status)";
            $linventory->db_db_query($sql);
        }
    }
    // end of eBooking #
}
if ($ItemType == ITEM_TYPE_BULK) {
    /*
     * if(empty($ItemAdminGroup))
     * {
     * ### retrieve original location's admin group and funding src
     * $sql = "select GroupInCharge from INVENTORY_ITEM_BULK_LOCATION where ItemID=$ItemID and LocationID = $ItemLocation";
     * $result = $linventory->returnVector($sql);
     * $ItemAdminGroup = $result[0];
     * }
     */
    
    // write-off reason
    $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$targetWriteOffReason."'";
    $arr_write_off_reason = $linventory->returnVector($sql);
    $write_off_reason = $arr_write_off_reason[0];
    $write_off_reason = addslashes($write_off_reason);
    
    /*
     * # Discussed with Ken, no need to remove the records
     * ### Check any exist write off record ###
     * $sql = "SELECT
     * RecordID
     * FROM
     * INVENTORY_ITEM_WRITE_OFF_RECORD
     * WHERE
     * ItemID = $ItemID AND
     * LocationID = $ItemLocation AND
     * AdminGroupID = $ItemAdminGroup
     * ";
     * $arr_record_exist = $linventory->returnVector($sql);
     * if(sizeof($arr_record_exist) > 0)
     * {
     * foreach($arr_record_exist as $k=>$exist_record_id)
     * {
     * ## delete INVENTORY_ITEM_WRITE_OFF_RECORD
     * $sql = "delete from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordID=$exist_record_id";
     * $linventory->db_db_query($sql);
     *
     * ## delete files
     * ## implement if necessary
     *
     * ## delete INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
     * $sql = "delete from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$exist_record_id";
     * $linventory->db_db_query($sql);
     * }
     * }
     */
    
    $sql = "INSERT INTO
						INVENTORY_ITEM_WRITE_OFF_RECORD
						(ItemID, RecordDate, LocationID, AdminGroupID, FundingSourceID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, 
						RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($ItemID, '$curr_date', $targetLocation, $targetGroupID, $targetFundingID, $item_write_off_qty, '$write_off_reason', '$curr_date', $UserID,
						0, 0, NOW(), NOW())
				";
    $result['UpdateInvItemBulkLog'] = $linventory->db_db_query($sql);
    $write_off_logID = $linventory->db_insert_id();
    
    $no_of_file = $no_of_file_upload;
    if ($no_of_file == "") {
        $no_of_file = 1;
    }
    
    for ($i = 0; $i < $no_of_file; $i ++) {
        $file = stripslashes(${"hidden_item_write_off_attachment_$i"});
        $re_file = stripslashes(${"item_write_off_attachment_$i"});
        
        if ($re_file == "none" || $re_file == "") {} else {
            $photo_path = $path . "/" . $write_off_logID;
            $photo_re_path = $re_path . "/" . $write_off_logID;
            
            $lf = new libfilesystem();
            if (! is_dir($photo_path)) {
                $lf->folder_new($photo_path);
            }
            
            $target = "$photo_path/$file";
            $attachementname = "/$file";
            
            if ($lf->lfs_copy($re_file, $target) == 1) {
                $sql = "INSERT INTO 
									INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
									(ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
							VALUES
									($ItemID, $write_off_logID, '$photo_re_path', '$file', NOW(), NOW())
							";
                $linventory->db_db_query($sql);
            }
        }
    }
}

// header("location: items_request_write_off.php?item_id=$ItemID&targetLocation=$ItemLocation&xmsg=AddSuccess");
header("location: items_request_write_off.php?item_id=$ItemID&targetLocation=$ItemLocation&targetGroupID=$targetGroupID&targetFundingID=$targetFundingID&xmsg=AddSuccess");

intranet_closedb();
?>