<?
# using: 

/*************************************************************************
 *  modification log
 * 	Date:	2019-05-13 (Henry)
 * 			Security fix: SQL without quote
 * 
 * 	Date:	2019-04-30 (Henry)
 * 			security issue fix for SQL
 * 
 * 	Date:	2016-04-21 (Henry)
 * 			fixed js error if the item Description contain line break [Case#S95183]
 * 
 *  Date: 	2016-02-01 (Henry) 
 * 			PHP 5.4 fix: change split to explode
 * 
 *  Date:	2014-04-01	YatWoon
 * 			add SKH_ItemCode_MaxNo for SKH eInventory's item code format checking
 * 
 *  Date:	2012-10-25	YatWoon
 *			add BookingStatusList action (for eBooking integration)
 *
 *	Date:	2012-08-17	YatWoon
 *			Improved: For CountGroupCatSubCatMaxNo [CatholicEducation], double check the item code is exists [Case#2012-0813-1459-29132]
 * 
 *	Date:	2011-06-13	YatWoon
 *			Fixed: connect database before declare linventory
 * 
 *	Date:	2011-06-08	YatWoon
 *			Improved: Can back to step 1 from step 2 [Case#2011-0401-1247-08067]
 * 
 * ************************************************************************/

 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb(); 

$linventory = new libinventory();

$targetItemType = IntegerSafe($targetItemType);
$targetCategory = IntegerSafe($targetCategory);
$targetCategory2 = IntegerSafe($targetCategory2);
$ExistingItemID = IntegerSafe($ExistingItemID);
$item_cat1 = IntegerSafe($item_cat1);
$item_cat2 = IntegerSafe($item_cat2);

if($Action == "SubCategory")
{
	$arr_category2 = $linventory->getCategoryLevel2Name($CategoryID);
	$x = getSelectByArray($arr_category2, "name=targetCategory2 onChange='ajax_change_subcategory(1);'", $targetCategory2);
	$x .= '<span id="div_SubCategory_err_msg"></span>';
}

if($Action == "Purchase_Details")
{
	$sql = "SELECT ItemID, ".$linventory->getInventoryItemNameByLang()." FROM INVENTORY_ITEM WHERE ItemType = '".$targetItemType."' AND CategoryID = '".$targetCategory."' AND Category2ID = '".$targetCategory2."'";
	$arr_exist_item = $linventory->returnArray($sql,2);
	
	if(sizeof($arr_exist_item)>0)
	{
		$arr_purchase_type = array(array(1,$i_InventorySystem_Item_New),array(2,$i_InventorySystem_Item_Existing));
	}
	else if($targetCategory2)
	{
		$arr_purchase_type = array(array(1,$i_InventorySystem_Item_New));
	}
	else
	{
		$arr_purchase_type = array();
	}
	
	$x = getSelectByArray($arr_purchase_type, "name =\"purchaseType\" onChange='changePurchaseType(1);'",$purchaseType);

}

if($Action == "Retrieve_Existing_Item")
{
	$arr_exist_item = $linventory->returnExistingItems($targetItemType, $targetCategory, $targetCategory2);
	$x = getSelectByArray($arr_exist_item, "name =\"targetExistItem\" onChange='selectExistingItem();'",$targetExistItem);
}

if($Action == "Retrieve_Existing_Item_Info")
{
	$x1 = $linventory->returnItemInfo($ExistingItemID,'',$targetCategory,$targetCategory2);
	if(!empty($x1))
	{
		$x = "<script language='javascript'>";
		# Item Name (Chinese)
		$x .= "document.form1.item_chi_name.value = '". str_replace( array("&#039;" ), array("\'" ), htmlspecialchars_decode($x1[0]['NameChi'])) ."';";
		$x .= "document.form1.item_chi_name.disabled=true;";
		# Item Name (English)
		$x .= "document.form1.item_eng_name.value = '". str_replace( array("&#039;" ), array("\'" ), htmlspecialchars_decode($x1[0]['NameEng'])) ."';";
		$x .= "document.form1.item_eng_name.disabled=true;";
		
		if(!$sys_custom['eInventoryCustForSMC']) {
			# Description (Chinese)
			$x .= "document.form1.item_chi_discription.value = '". str_replace( array( "\n", "\r", "&#039;" ), array( "\\n", "\\r", "\'" ), htmlspecialchars_decode($x1[0]['DescriptionChi'])) ."';";
			$x .= "document.form1.item_chi_discription.disabled=true;";
			# Description (English)
			$x .= "document.form1.item_eng_discription.value = '". str_replace( array( "\n", "\r", "&#039;" ), array( "\\n", "\\r", "\'" ), htmlspecialchars_decode($x1[0]['DescriptionEng'])) ."';";
			$x .= "document.form1.item_eng_discription.disabled=true;";
		}
	
		# Ownership 
		$x .= "document.form1.item_ownership[". ($x1[0]['Ownership']-1) ."].checked  = true;";
		$x .= "
			var t = document.form1.item_ownership.length;
			for(i=0;i<document.form1.item_ownership.length;i++)
			{
				document.form1.item_ownership[i].disabled=true;
			}
		";
		# Photo
		$x .= "document.form1.item_photo.disabled=true;";
		# Variance Manager
		$sql = "SELECT a.BulkItemAdmin FROM INVENTORY_ITEM_BULK_EXT AS a INNER JOIN INVENTORY_ADMIN_GROUP AS b ON (a.BulkItemAdmin = b.AdminGroupID) WHERE a.ItemID = '".$ExistingItemID."'";
		$BulkItemAdminID_temp = $linventory->returnArray($sql);
		$BulkItemAdminID = $BulkItemAdminID_temp[0][0];
		$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
		$arr_bulk_item_admin = $linventory->returnArray($sql);
		for($i=0;$i<sizeof($arr_bulk_item_admin);$i++)
		{
			if($arr_bulk_item_admin[$i][0] == $BulkItemAdminID)
				break;
		}
		$x .= "document.form1.bulk_item_admin.selectedIndex=". ($i+1) .";";
		$x .= "document.form1.bulk_item_admin.disabled=true;";
		$x .= "document.form1.exist_item_code.value='". $x1[0]['ItemCode'] ."';";
		$x .= "</script>";
	}
}

if($Action == "Reset_New_Item")
{
	$x = "<script language='javascript'>";
	# Item Name (Chinese)
	$x .= "document.form1.item_chi_name.value='';";
	$x .= "document.form1.item_chi_name.disabled=false;";
	# Item Name (English)
	$x .= "document.form1.item_eng_name.value='';";
	$x .= "document.form1.item_eng_name.disabled=false;";
	
	if(!$sys_custom['eInventoryCustForSMC']) {
		# Description (Chinese)
		$x .= "document.form1.item_chi_discription.value = '';";
		$x .= "document.form1.item_chi_discription.disabled=false;";
		# Description (English)
		$x .= "document.form1.item_eng_discription.value = '';";
		$x .= "document.form1.item_eng_discription.disabled=false;";
	}
	
	# Ownership 
	$x .= "document.form1.item_ownership[0].checked  = true;";
	$x .= "
		var t = document.form1.item_ownership.length;
		for(i=0;i<document.form1.item_ownership.length;i++)
		{
			document.form1.item_ownership[i].disabled=false;
		}
	";
	# Photo
	$x .= "document.form1.item_photo.disabled=false;";
	# Variance Manager
	$x .= "document.form1.bulk_item_admin.selectedIndex=0;";
	$x .= "document.form1.bulk_item_admin.disabled=false;";
	
	
	$x .= "</script>";
	
}

if($Action == "Check_Resources_Mgt_Group")
{
	//targetLocation_index
	$targetLocation_index = IntegerSafe($targetLocation_index);
	$sql = "select GroupInCharge, FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where ItemID = '".$targetItemID."' and LocationID = '".$targetLocationID."'";
	$result = $linventory->returnArray($sql);
	list($this_group, $this_funding) = $result[0];
	
	$x = "<script language='javascript'>";
	if(empty($result))
	{
		$x .= " document.form1.targetGroup_". $targetLocation_index.".disabled = false;";
		$x .= " document.form1.targetGroup_". $targetLocation_index.".selectedIndex = 0;";
		
		$x .= " document.form1.targetBulkFunding.disabled = false;";
		$x .= " document.form1.targetBulkFunding.selectedIndex = 0;";
	}
	else
	{
		$x .= "for(i=0;i<document.form1.targetGroup_". $targetLocation_index.".length;i++) {";
		$x .= " if(document.form1.targetGroup_". $targetLocation_index."[i].value == ". $this_group .") {";
		$x .= " document.form1.targetGroup_". $targetLocation_index.".selectedIndex = i;";
// 		$x .= " document.form1.targetGroup_". $targetLocation_index.".disabled = true;";
		$x .= " }";
		$x .= "}";
		
		$x .= "for(i=0;i<document.form1.targetBulkFunding.length;i++) {";
		$x .= " if(document.form1.targetBulkFunding[i].value == ". $this_funding .") {";
		$x .= " document.form1.targetBulkFunding.selectedIndex = i;";
// 		$x .= " document.form1.targetBulkFunding.disabled = true;";
		$x .= " }";
		$x .= "}";
		//targetBulkFunding
	}
	$x .= "</script>";
}

if($Action == "CountGroupCatSubCatMaxNo")	# for CatholicEducation_eInventory only
{
	$x = "";
	$group_ary = array_unique(array_remove_empty(explode(",",$groupIDs)));
	foreach($group_ary as $k=>$groupid)
	{
		$itemCode_Prefix = $linventory->returnAdminGroupCode($groupid) . "-" .$codes."-";
		
		$sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = '".$item_cat1."' AND Category2ID = '".$item_cat2."' AND ItemCode LIKE '".$itemCode_Prefix."%'";
		$arrLastItemCode = $linventory->returnVector($sql);	
		
		$postfix = 0;
		if(sizeof($arrLastItemCode)>0)
		{
			$LastItemCode = $arrLastItemCode[0];
			$tmp_str_pos = strrpos($LastItemCode,"-");
			$postfix = substr($LastItemCode,$tmp_str_pos+1,strlen($LastItemCode));
		}
		
		$postfix = $postfix+0;
		
		# double check the item_code is exists or not 
		for($i=$postfix+1; $i<=99999; $i++)
		{
			$postfix_str = str_pad($i, 5, "0", STR_PAD_LEFT);
			$check_itemCode = $itemCode_Prefix . $postfix_str;
			$sql = "select count(*) from INVENTORY_ITEM where ItemCode = '$check_itemCode'";
			$result = $linventory->returnVector($sql);
			if(!$result[0])	break;
		}
		
		$x .= $groupid.":". ($i-1) .";";
	}
}

if($Action == "BookingStatusList")
{
	list($item_type, $item_id) = explode("-",$DataStr);
	
	include_once($PATH_WRT_ROOT."includes/libebooking.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	
	$lebooking = new libebooking();
	$lu = new libuser();
	
	$bookingRecordAry = $lebooking->returnItemBookingRecords($item_type, $item_id);
				
	if(!empty($bookingRecordAry))
	{
		$layer_content .= "<table border='0' cellpadding='3' cellspacing='0' width='100%'>";
		$layer_content .= "<tr class='tabletop'>";
		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']."</td>";
		$layer_content .= "<td>".$Lang['eBooking']['Mail']['FieldTitle']['BookingDate']."</td>";
		$layer_content .= "<td>".$Lang['eBooking']['Mail']['FieldTitle']['StartTime']."</td>";
		$layer_content .= "<td>".$Lang['eBooking']['Mail']['FieldTitle']['EndTime']."</td>";
		$layer_content .= "</tr>";
		
		foreach($bookingRecordAry as $k=>$d)
		{
			$layer_content .= "<tr class='tablerow1'>";
			$layer_content .= "<td>". $lu->getNameWithClassNumber($d['ResponsibleUID']) ."</td>";
			$layer_content .= "<td>". $d['Date'] ."</td>";
			$layer_content .= "<td>". $d['StartTime'] ."</td>";
			$layer_content .= "<td>". $d['EndTime'] ."</td>";
			$layer_content .= "</tr>";
		}
		$layer_content .= "<table><br>";
	}
			
			
	$x = $layer_content;	
}

if($Action == "SKH_ItemCode_MaxNo")	# for SKH only
{
	$x = "";
	$group_ary = array_unique(array_remove_empty(explode(",",$groupIDs)));
	foreach($group_ary as $k=>$groupid)
	{
		$this_group_code = $linventory->returnAdminGroupCode($groupid);
		$this_category_code = $linventory->returnCategoryCode($item_cat1);
		$this_subcategory_code = $linventory->returnSubCategoryCode($item_cat2);
		$itemCode_Prefix = $item_purchase_date . "/" . $this_group_code ."/" . $this_category_code . "/" . $this_subcategory_code. "/";
		
		//$sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = $item_cat1 AND Category2ID = $item_cat2 AND ItemCode LIKE '".$itemCode_Prefix."%'";
		$sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE ItemCode LIKE '".$itemCode_Prefix."%'";
		$arrLastItemCode = $linventory->returnVector($sql);	
		
		$postfix = 0;
		if(sizeof($arrLastItemCode)>0)
		{
			$LastItemCode = $arrLastItemCode[0];
			$tmp_str_pos = strrpos($LastItemCode,"/");
			$postfix = substr($LastItemCode,$tmp_str_pos+1,strlen($LastItemCode));
		}
		
		$postfix = $postfix+0;
		
		# double check the item_code is exists or not 
		for($i=$postfix+1; $i<=99999; $i++)
		{
			$postfix_str = str_pad($i, 5, "0", STR_PAD_LEFT);
			$check_itemCode = $itemCode_Prefix . $postfix_str;
			$sql = "select count(*) from INVENTORY_ITEM where ItemCode = '$check_itemCode'";
			$result = $linventory->returnVector($sql);
			if(!$result[0])	break;
		}
		
		$x .= $groupid.":". ($i-1) .";";
	}
}






intranet_closedb();

echo $x;
?>