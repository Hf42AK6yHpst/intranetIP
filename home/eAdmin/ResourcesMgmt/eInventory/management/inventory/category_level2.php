<?php
# using:

########################################################
#	Date:	2016-02-04	Henry
#			php 5.4 issue move set cookies after includes file 
#
#	Date:	2012-07-03	YatWoon
#			Improved: display sub-category with distinct from INVENTORY_ITEM
########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_category2_browsing_view_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_category2_browsing_view_record_page_number", $pageNo, 0, "", "", 0);
	$ck_category2_browsing_view_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_category2_browsing_view_record_page_number!="")
{
	$pageNo = $ck_data_log_browsing_view_user_record_detail_page_number;
}

if ($ck_category2_browsing_view_record_page_order!=$order && $order!="")
{
	setcookie("ck_category2_browsing_view_record_page_order", $order, 0, "", "", 0);
	$ck_category2_browsing_view_record_page_order = $order;
} else if (!isset($order) && $ck_category2_browsing_view_record_page_order!="")
{
	$order = $ck_category2_browsing_view_record_page_order;
}

if ($ck_category2_browsing_view_record_page_field!=$field && $field!="")
{
	setcookie("ck_category2_browsing_view_record_page_field", $field, 0, "", "", 0);
	$ck_category2_browsing_view_record_page_field = $field;
} else if (!isset($field) && $ck_category2_browsing_view_record_page_field!="")
{
	$field = $ck_category2_browsing_view_record_page_field;
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Title ###
$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php", 1);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### Get The Category ###
$namefield = $linventory->getInventoryItemNameByLang();
$sql = "SELECT 
				$namefield
		FROM 
				INVENTORY_CATEGORY 
		WHERE
				CategoryID = $category_id";

$result = $linventory->returnArray($sql,1);

$temp[] = array($result[0][0],"");
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($result)."</td></tr>";

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field=$field==""?2:$field;

switch($field){
	case 0: $field = 0; break;
	case 1: $field = 2; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	case 4: $field = 4; break;
	default: $field = 2;
}

if (!isset($order)) $order = 1;


### Get The Category ###
/*
$namefield = $linventory->getInventoryItemNameByLang("b.");
$sql = "SELECT 
				CONCAT('<img src=\"',c.PhotoPath,'/',c.PhotoName,'\" border=0 width=\"70\" height=\"70\">'),
				CONCAT(a.Code,b.Code) AS CODE,
				CONCAT('<a class=\"tablelink\" href=\"category_show_items.php?cat_id=',b.CategoryID,'&cat2_id=',b.Category2ID,'\">',$namefield,'</a>')
		FROM 
				INVENTORY_CATEGORY AS a INNER JOIN 
				INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) LEFT OUTER JOIN
				INVENTORY_PHOTO_PART AS c ON (b.PhotoLink = c.PartID)
		WHERE
				a.CategoryID = $category_id";
*/

$namefield = $linventory->getInventoryItemNameByLang("b.");
$sql = "SELECT 
			CONCAT('<img src=\"',c.PhotoPath,'/',c.PhotoName,'\" border=0 width=\"70\" height=\"70\">'),
			CONCAT(a.Code,b.Code) AS CODE,
			CONCAT('<a class=\"tablelink\" href=\"category_show_items.php?cat_id=',b.CategoryID,'&cat2_id=',b.Category2ID,'\">',$namefield,'</a>')
		FROM 
			INVENTORY_CATEGORY AS a INNER JOIN 
			INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) LEFT OUTER JOIN
			INVENTORY_PHOTO_PART AS c ON (b.PhotoLink = c.PartID)
			inner join INVENTORY_ITEM as item on (item.Category2ID=b.Category2ID)
		 WHERE a.CategoryID = $category_id 
		 group by item.Category2ID";
		
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.NameEng","a.Code","b.Code");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;
//echo $li->built_sql();

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class='tablebluetop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$li->column($pos++, "")."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_InventorySystem_Category_Code."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_InventorySystem_Category_Name."</td>\n";
	
?>

<br>

<form name="form1" action="" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$infobar?>
</table>
<br>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><?= $toolbar ?></td>
	</tr>
</table>

<?php echo $li->display("90%","blue"); ?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>