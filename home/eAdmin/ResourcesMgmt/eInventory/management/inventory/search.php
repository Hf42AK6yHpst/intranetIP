<?php
// using:

// #######################################################
//
// Date 2020-06-11 Cameron
// - add label to WriteOffCriteria so that it can be easily to select radio box
//
// Date 2018-02-21 Henry
// - add access right checking [Case#E135442]
//
// Date: 2017-05-08 Cameron
// - fix bug: StartDate and EndDate can be the same for Purchase Date and Warranty Date
//
// Date: 2015-09-25 Cameron
// - fix bug on comparing StartDate and EndDate for Purchase Date and Warranty Date
//
// Date: 2015-04-16 Cameron
// - use double quote for string "$image_path/$LAYOUT_SKIN"
//
// Date: 2013-05-24 YatWoon
// add "Tag" [Case#2013-0524-1245-08054]
//
// Date: 2012-07-17 YatWoon
// add "Maintenance Details"
//
// Date: 2012-07-03 YatWoon
// Improved: display category with distinct from INVENTORY_ITEM
// Improved: back button
// #######################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['Inventory'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Init JS for date picker #
$js_init .= '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.js"></script>';
$js_init .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.css" type="text/css" />';

// Page Title #
$temp[] = array(
    $i_InventorySystem_Avanced_Search,
    ""
);
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";
// End #

$arr_action = array(
    array(
        1,
        $i_InventorySystem_Simple_Search
    ),
    array(
        2,
        $i_InventorySystem_Avanced_Search
    )
);
$select_action = getSelectByArray($arr_action, "name=\"targetSearch\" onChange=\"this.form.flag.value=0; this.form.submit();\" ", $targetSearch);

if ($targetSearch == "")
    $targetSearch = 2;

if ($targetSearch == 1) // # simple search
{
    $search_option .= "<tr><td class=\"tabletext\" align=\"right\"><a href=\"search.php?targetSearch=2\">{$i_InventorySystem_Avanced_Search}</a></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Keyword}</td><td class=\"tabletext\" width=\"70%\"><input type=\"text\" name=\"keyword\" class=\"textboxtext\"></td></tr>";
}
if ($targetSearch == 2) // # advance search
{
    $search_option .= "<tr><td class=\"tabletext\" align=\"right\"><a href=\"search.php?targetSearch=1\">{$i_InventorySystem_Simple_Search}</a></td></tr>";

    $curr_date = date("Y-m-d");

    // $sql = "SELECT CategoryID, ".$linventory->getInventoryNameByLang("")." FROM INVENTORY_CATEGORY";
    $sql = "SELECT 
			distinct(a.CategoryID), 
			" . $linventory->getInventoryNameByLang("b.") . " 
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY as b on a.CategoryID = b.CategoryID
		ORDER BY b.DisplayOrder";
    $arr_category = $linventory->returnArray($sql, 2);

    $sql = "SELECT LocationID, CONCAT(" . $linventory->getInventoryNameByLang("a.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") FROM INVENTORY_LOCATION_BUILDING AS a INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1 ORDER BY a.DisplayOrder, b.DisplayOrder, c.DisplayOrder";
    $arr_location = $linventory->returnArray($sql, 2);

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"20%\">{$i_InventorySystem_Item_Name}</td><td colspan=\"3\"><input type=\"text\" name=\"item_name\" class=\"textboxtext\"></td></tr>";
    $table_content .= "<tr><td width=\"20%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Code}</td><td><input type=\"text\" name=\"item_code\" class=\"textboxnum\"></td>";
    $table_content .= "<td width=\"20%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Category2_Barcode}</td><td><input type=\"text\" name=\"tag_code\" class=\"textboxnum\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Category'] . "</td><td class=\"tabletext\">";
    $table_content .= getSelectByArray($arr_category, "name=\"categoryID[]\" MULTIPLE size=\"10\" ", 0, 0, 1);
    $table_content .= "</td></tr>";

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Location'] . "</td><td class=\"tabletext\">";
    $table_content .= getSelectByArray($arr_location, "name=\"locationID[]\" MULTIPLE size=\"10\" ", 0, 0, 1);
    $table_content .= "</td></tr>";

    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
    $arr_admin_group = $linventory->returnArray($sql, 2);
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Caretaker'] . "</td><td class=\"tabletext\">";
    $table_content .= getSelectByArray($arr_admin_group, "name=\"adminGroupID[]\" MULTIPLE size=\"10\" ", 0, 0, 1);
    $talbe_content .= "</td></tr>";

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Funding</td><td class=\"tabletext\">";
    $sql = "SELECT FundingSourceID, " . $linventory->getInventoryNameByLang("") . " FROM INVENTORY_FUNDING_SOURCE";
    $arr_funding = $linventory->returnArray($sql, 2);
    $table_content .= getSelectByArray($arr_funding, "name=\"fundingID[]\" MULTIPLE size=\"10\" ", 0, 0, 1);
    $table_content .= "</td></tr>";

    /* Code add on 16-01-2008 */
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystemItemStatus . "</td><td class=\"tabletext\" width=\"50%\">";
    $table_content .= "<input type=\"Radio\" name=\"WriteOffCriteria\" id=\"WriteOffCriteriaAll\" value=\"All\" Checked><label for=\"WriteOffCriteriaAll\">$i_InventorySystemItemStatusAll</label>&nbsp;";
    $table_content .= "<input type=\"Radio\" name=\"WriteOffCriteria\" id=\"WriteOffCriteriaYes\" value=\"WriteOff\"><label for=\"WriteOffCriteriaYes\">$i_InventorySystemItemStatusWriteOff</label>&nbsp;";
    $table_content .= "<input type=\"Radio\" name=\"WriteOffCriteria\" id=\"WriteOffCriteriaNo\" value=\"NonWriteOff\"><label for=\"WriteOffCriteriaNo\">$i_InventorySystemItemStatusNonWriteOff</label>&nbsp;";
    $table_content .= "<br>\n";
    $table_content .= "</td></tr>";
    /* Code End */

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Brand_Name}</td><td width=\"30%\"><input type=\"text\" name=\"item_brand\" class=\"textboxtext\"></td></tr>";

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Search_Warrany_Expiry_Between}</td><td colspan=\"3\" class=\"tabletext\">&nbsp;<input type=\"text\" name=\"warranty_start\" id=\"warranty_start\" class=\"textboxnum\" maxlength=\"10\"> $i_InventorySystem_Search_And&nbsp;<input type=\"text\" name=\"warranty_end\" id=\"warranty_end\" class=\"textboxnum\" maxlength=\"10\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_License_Type}</td><td><input type=\"text\" name=\"license\" class=\"textboxnum\"></td>";
    $table_content .= "<td width=\"20%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Serial_Num}</td><td><input type=\"text\" name=\"serial\" class=\"textboxnum\"></td>";
    $table_content .= "</tr>";

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Search_Purchase_Date_Between}</td><td colspan=\"3\" class=\"tabletext\" valign=\"top\">&nbsp;<input type=\"text\" name=\"purchase_date_start\" id=\"purchase_date_start\" class=\"textboxnum\" maxlength=\"10\"> $i_InventorySystem_Search_And&nbsp;<input type=\"text\" name=\"purchase_date_end\" id=\"purchase_date_end\" class=\"textboxnum\" maxlength=\"10\"> </td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Search_Purchase_Price_Range}</td><td class=\"tabletext\" colspan=\"3\">$&nbsp;<input type=\"text\" name=\"price_start\" class=\"textboxnum\">&nbsp;&nbsp;$i_InventorySystem_Search_And &nbsp;$&nbsp;<input type=\"text\" name=\"price_end\" class=\"textboxnum\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Search_Unit_Price_Range}</td><td class=\"tabletext\" colspan=\"3\">$&nbsp;<input type=\"text\" name=\"unit_price_start\" class=\"textboxnum\">&nbsp;&nbsp;$i_InventorySystem_Search_And &nbsp;$&nbsp;<input type=\"text\" name=\"unit_price_end\" class=\"textboxnum\"></td></tr>";

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Supplier_Name}</td><td><input type=\"text\" name=\"supplier_name\" class=\"textboxtext\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Invoice_Num}</td><td><input type=\"text\" name=\"invoice_no\" class=\"textboxnum\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Quot_Num}</td><td><input type=\"text\" name=\"quotation_no\" class=\"textboxnum\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Tender_Num}</td><td><input type=\"text\" name=\"tender_no\" class=\"textboxnum\"></td></tr>";

    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystemItemMaintainInfo}</td><td><input type=\"text\" name=\"item_maintain_info\" class=\"textboxtext\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Remark}</td><td><input type=\"text\" name=\"remark\" class=\"textboxtext\"></td></tr>";
    $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$Lang['eDiscipline']['Tag']}</td><td><input type=\"text\" name=\"tag\" class=\"textboxtext\"></td></tr>";
}

?>
<?=$js_init;?>
<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	var purchase_price_1 = 0;
	var purchase_price_2 = 0;
	var check_purchase_date = 0;
	var check_warranty_date = 0;
	
	<?
if ($targetSearch == 1) {
    ?>
		if(obj.flag.value == 1)
		{
			obj.action = "search_result.php";
			obj.submit();
		}
	<?
}
?>
	
	<?
if ($targetSearch == 2) {
    ?>
		if((obj.purchase_date_start.value != "") || (obj.purchase_date_end.value != ""))
		{
			if(check_date(obj.purchase_date_start,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning;?>"))
				if(check_date(obj.purchase_date_end,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning;?>"))
					if(compareDate(obj.purchase_date_end.value,obj.purchase_date_start.value)>=0)
					{
						check_purchase_date = 1;
					}
					else
					{
						check_purchase_date = 0;
						alert("<?=$Lang['eInventory']['WarningArr']['PurchaseEndDateCannotEarlierThanStartDate'];?>");
					}
		}
		else
		{
			if((obj.purchase_date_start.value == "") && (obj.purchase_date_end.value == ""))
			{
				check_purchase_date = 1;
			}
		}
					
		if((obj.warranty_start.value != "") || (obj.warranty_end.value != ""))
		{
			if(check_date(obj.warranty_start,"<?=$i_InventorySystem_Input_Item_WarrantyExpiryDate_Warning;?>"))
				if(check_date(obj.warranty_end,"<?=$i_InventorySystem_Input_Item_WarrantyExpiryDate_Warning;?>"))
					if(compareDate(obj.warranty_end.value,obj.warranty_start.value)>=0)
					{
						check_warranty_date = 1;
					}
					else
					{
						check_warranty_date = 0;
						alert("<?=$Lang['eInventory']['WarningArr']['WarrantyEndDateCannotEarlierThanStartDate'];?>");
					}
		}
		else
		{
			if((obj.warranty_start.value == "") && (obj.warranty_end.value == ""))
				check_warranty_date = 1;
		}
		
		if(obj.price_start.value != "")
		{
			if(obj.price_start.value.indexOf(".") != -1)
			{
				var tmp_len = obj.price_start.value.length - obj.price_start.value.indexOf(".");
				if(tmp_len == 3)
				{
					if(check_positive_int(obj.price_start,"<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>",0,0))
						purchase_price_1 = 1;
				}
				else
				{
					alert("<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>");
				}
			}
			else
			{
				if(check_positive_int(obj.price_start,"<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>",0,0))
					purchase_price_1 = 1;
			}
		}
		else
		{
			purchase_price_1 = 1;
		}
		
		if(obj.price_end.value != "")
		{
			if(obj.price_end.value.indexOf(".") != -1)
			{
				var tmp_len = obj.price_end.value.length - obj.price_end.value.indexOf(".");
				if(tmp_len == 3)
				{
					if(check_positive_int(obj.price_end,"<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>",0,0))
						purchase_price_2 = 1;
				}
				else
				{
					alert("<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>");
				}
			}
			else
			{
				if(check_positive_int(obj.price_end,"<?=$i_InventorySystem_Input_Item_PurchasePrice_Warning?>",0,0))
					purchase_price_2 = 1;
			}
		}
		else
		{
			purchase_price_2 = 1;
		}
		
		if((check_purchase_date == 1) && (check_warranty_date == 1) && (purchase_price_1 == 1) && (purchase_price_2 == 1) && (obj.flag.value == 1))
		{
			obj.action = "search_result.php";
			obj.submit();
		}
	<?
}
?>
	return false;	
}

$(document).ready(function(){ 
	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
	$('#warranty_start').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
	$('#warranty_end').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
	$('#purchase_date_start').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
	$('#purchase_date_end').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
});			
</script>

<br>

<form name="form1" action="" method="POST"
	onSubmit="return checkForm();">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
	<?=$infobar?>
</table>
	<br>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content;?>
</table>

	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="$image_path/$LAYOUT_SKIN" ?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='items_full_list.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>
	<input type="hidden" name="flag" value=1> <input type="hidden"
		name="targetSearch" value=<?=$targetSearch?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>