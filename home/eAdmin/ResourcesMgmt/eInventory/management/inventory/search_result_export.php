<?php
# using: Henry

##############################
#	Date:	2015-04-14	Henry
#			Fixed: show wrong Funding Source [Case#M76717]
#
#	Date:	2013-04-16 YatWoon
#			support search for bulk item barcode
#
#	Date:	2012-07-17 YatWoon
#			add "Maintenance Details" 
#
##############################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport 	= new libexporttext();
$linventory = new libinventory();

// $file_content="";

$exportColumn = array($i_InventorySystem_Item_Code, $i_InventorySystem_Item_Name, $i_InventorySystem_Item_Barcode, $i_InventorySystem['Category'], $i_InventorySystem_Item_Location, $i_InventorySystem_Group_Name, $i_InventorySystem_Item_Funding);

//get the column list index to export
if (!isset($ColumnName)) { 
	$SelectedColumn = $exportColumn;
// 	$file_content.="\"$i_InventorySystem_Item_Code\","; 
// 	$file_content.="\"$i_InventorySystem_Item_Name\","; 
// 	$file_content.="\"$i_InventorySystem_Item_Barcode\","; 
// 	$file_content.="\"".$i_InventorySystem['Category']."\","; 
// 	$file_content.="\"$i_InventorySystem_Item_Location\","; 
// 	$file_content.="\"$i_InventorySystem_Group_Name\","; 
// 	$file_content.="\"$i_InventorySystem_Item_Funding\""; 
// 	$file_content.="\n";
}
else {
	//get the column name to export
	for ($i=0; $i<sizeof($ColumnName); $i++) 
	{
		$SelectedColumn[] = $exportColumn[$ColumnName[$i]];
// 		if($i != 0)
// 			$file_content .= ",";
// 		$file_content.="\"".$exportColumn[$ColumnName[$i]]."\"";
// 		if ($i==(sizeof($ColumnName)-1)) 
// 		{
// 			$file_content .= "\n";		
// 		}
	}
}

switch ($field){
	case 1:
			$order_by = "a.ItemCode ";
			break;
	case 2:
			$order_by = "a.NameEng ";
			break;
	case 3:
			$order_by = "b.TagCode ";
			break;
	case 4:
			$order_by = "h.NameEng ";
			break;
	case 5:
			$order_by = "e.NameEng ";
			break;
	case 6:
			$order_by = "f.NameEng ";
			break;
	case 7:
			$order_by = "j.NameEng ";
			break;
	default:
			$order_by = "a.ItemCode ";
			break;
}

if(($order == 0)||($order == ""))
	$order_by .= "ASC";
else
	$order_by .= "DESC";

$order_cond = "a.ItemType, ".$order_by;
	
if($cond == "")
{
	$cond = "";
}
else
{
 	$cond = stripslashes($cond);
	$cond = "WHERE ".$cond;
}
$sql = "SELECT 
				a.ItemCode,
				".$linventory->getInventoryItemNameByLang("a.").",
				IF(a.ItemType = 1, b.TagCode, bl.Barcode),
				CONCAT(".$linventory->getInventoryNameByLang("i.").",' > ',".$linventory->getInventoryNameByLang("h.")."),
				CONCAT(".$linventory->getInventoryNameByLang("g.").",' > ',".$linventory->getInventoryNameByLang("e.")."),
				".$linventory->getInventoryNameByLang("f.").",
				".$linventory->getInventoryNameByLang("j.")."
		FROM
				INVENTORY_ITEM AS a LEFT OUTER JOIN 
				INVENTORY_ITEM_SINGLE_STATUS_LOG as I on (a.ItemID = I.ItemID $single_action) LEFT OUTER JOIN 
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN 
				INVENTORY_ITEM_BULK_LOG AS c ON (a.ItemID = c.ItemID $bulk_action) LEFT OUTER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
				INVENTORY_LOCATION AS e ON (b.LocationID = e.LocationID OR d.LocationID = e.LocationID) LEFT OUTER JOIN
				INVENTORY_LOCATION_LEVEL AS g ON (e.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS h ON (a.Category2ID = h.Category2ID) LEFT OUTER JOIN
				INVENTORY_CATEGORY AS i ON (h.CategoryID = i.CategoryID) LEFT OUTER JOIN
				INVENTORY_FUNDING_SOURCE AS j ON (b.FundingSource = j.FundingSourceID OR d.FundingSourceID = j.FundingSourceID) LEFT OUTER JOIN
				INVENTORY_PHOTO_PART AS k ON (a.PhotoLink = k.PartID) LEFT OUTER JOIN
				INVENTORY_ADMIN_GROUP AS f ON (b.GroupInCharge = f.AdminGroupID OR d.GroupInCharge = f.AdminGroupID)
				LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
				
				$cond
		GROUP BY
				a.ItemID, e.LocationID
		ORDER BY
				$order_cond
		";
$arr_result = $linventory->returnArray($sql) or die(mysql_error());

if(sizeof($arr_result)>0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($item_code, $item_name, $item_barcode, $item_category, $item_location, $item_admin_group, $item_funding) = $arr_result[$i];
		$temprows = array($item_code, $item_name, $item_barcode, $item_category, $item_location, $item_admin_group, $item_funding);
		
		if (!isset($ColumnName)) { 
			$rows[] = $temprows;
// 			$file_content .= "\"$item_code\",\"$item_name\",\"$item_barcode\",\"$item_category\",\"$item_location\",\"$item_admin_group\",\"$item_funding\"\n";
		}
		else {
			for ($j=0; $j< sizeof($ColumnName); $j++) {
				
// 				if($j!=0)
// 					$file_content .= ",";
				$rows[$i][] = $temprows[$ColumnName[$j]];
// 				$file_content .= "\"".$temprows[$ColumnName[$j]]."\"";
				
// 				if ($j == (sizeof($ColumnName)-1)) {
// 					$file_content .= "\n";
// 				}
			}
		} 
	}
}
if(sizeof($arr_result) == 0)
{
// 	$file_content .= "\"$i_no_record_exists_msg\"\n";
	
	$rows[] = $i_no_record_exists_msg;
}

// $display = $file_content;

intranet_closedb();

$filename = "search_result_export.csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $SelectedColumn, "\t", "\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);
	
// 	$export_content = $lexport->GET_EXPORT_TXT($rows, $SelectedColumn);
// 	$lexport->EXPORT_FILE($filename, $export_content);


?>