<?php
/*
 * Log
 * Date: 2019-05-13 [Henry] Security fix: SQL without quote
 * Date: 2018-02-07 [Henry] add access right checking [Case#E135442]
 * Date: 2015-04-16 [Cameron] use double quote for string "$image_path/$LAYOUT_SKIN"
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$temp[] = array(
    "insert item quantity",
    ""
);
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

// ## Get the item name ###
$sql = "SELECT ItemType, " . $linventory->getInventoryItemNameByLang() . " FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 2);
if (sizeof($result) > 0) {
    list ($item_type, $item_name) = $result[0];
}

// ## Get location level ###
$sql = "SELECT LocationLevelID, " . $linventory->getInventoryItemNameByLang() . " FROM INVENTORY_LOCATION_LEVEL";
$location_level_result = $linventory->returnArray($sql, 2);
$select_location_level = getSelectByArray($location_level_result, " name=\"target_location_level\" onChange=\"this.form.submit();\" ", $target_location_level);

// ## Get location ###
if ($target_location_level != "") {
    $sql = "SELECT LocationID, " . $linventory->getInventoryItemNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationID = '".$target_location_level."'";
    $location_result = $linventory->returnArray($sql, 2);
    $select_location = getSelectByArray($location_result, " name=\"target_location\" ");
}

// ## Get Group ###
$sql = "SELECT AdminGroupID, " . $linventory->getInventoryItemNameByLang() . " FROM INVENTORY_ADMIN_GROUP";
$admin_group_result = $linventory->returnArray($sql, 2);
$select_admin_group = getSelectByArray($admin_group_result, "name=\"target_admin_group\" ");

$table_content .= "<tr><td class=\"tabletext\">Item Name</td><td class=\"tabletext\">$item_name</td></tr>";
$table_content .= "<tr><td class=\"tabletext\">Location (Level)</td><td class=\"tabletext\">$select_location_level</td></tr>";
if ($target_location_level != "") {
    $table_content .= "<tr><td class=\"tabletext\">Location</td><td class=\"tabletext\">$select_location</td></tr>";
    $table_content .= "<tr><td class=\"tabletext\">Group In Charge</td><td class=\"tabletext\">$select_admin_group</td></tr>";
    $table_content .= "<tr><td class=\"tabletext\">Quantity</td><td class=\"tabletext\"><input type=\"text\" name=\"item_qty\"></td></tr>";
}

?>

<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	if(obj.target_location_level.value != "")
	{
		if(obj.target_location.value != "")
		{
			if(obj.target_admin_group.value != "")
			{
				if(check_positive_nonzero_int(obj.item_qty,"fail"))
				{
					obj.action = "category_add_bulk_item_quantity_update.php";
					obj.submit();
				}
				else
				{
					obj.action = "";
					obj.submit();
				}
			}
			else
			{
				alert("False 1");
			}
		}
		else
		{
			alert("False 2");
		}
	}
	else
	{
		alert("False 3");
	}
}
</script>

<br>
<form name="form1" action="" method="POST" onSubmit="checkForm();">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
	<?=$infobar?>
</table>
	<br>
	<table width="70%" border="0" cellpadding="5" cellspacing="0"
		align="center">
	<?=$table_content?>
</table>

	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="$image_path/$LAYOUT_SKIN" ?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>

	<input type="hidden" name="flag" value=1> <input type="hidden"
		name="item_id" value=<?=$item_id?>>

</form>
</br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>