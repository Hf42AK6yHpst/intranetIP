<?php
// Modifying by : henry chow

############ Change Log Start ###############
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();
$lexport = new libexporttext();

# check access right
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->isInvoiceCreator($RecordID, $UserID)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Academic Year ID
if(!isset($AcademicYearID) || $AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();

### Export Invoice Data

$dbtable = " LEFT JOIN INVOICEMGMT_ITEM t ON (t.InvoiceRecordID=i.RecordID AND t.RecordStatus=".RECORDSTATUS_ACTIVE.") LEFT JOIN INVENTORY_ADMIN_GROUP_MEMBER m ON (m.AdminGroupID=t.ResourceMgmtGroup AND m.UserID='$UserID')";
	
$conds = "";

$ayConds .= " AND i.AcademicYearID='$AcademicYearID'";

# for specific invoice
if($task=="item") {
	$conds .= " AND i.RecordID='$RecordID'";	
}

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->isViewerGroupMember) {
	$group_arr = $linvoice->returnAdminGroup($UserID, $leader=1);
	
	$ary = array();
	for($i=0; $i<count($group_arr); $i++) {
		$ary[] = $group_arr[$i][0];	
	}
	$addition = " OR i.InputBy='$UserID'";
	$conds .= " AND (t.ResourceMgmtGroup IN ('".implode('\',\'',$ary)."') OR i.InputBy='$UserID')";
	
}		
$groupBy = " GROUP BY i.RecordID";

$YearName = Get_Lang_Selection("ay.YearNameB5","ay.YearNameEN");
$namefield = getNameFieldWithClassNumberByLang("USR.");

$sql = "
		SELECT 
			$YearName as YearName, 
			i.InvoiceDate,
			i.Company,
			i.InvoiceNo,
			IF(i.InvoiceDescription='' OR i.InvoiceDescription IS NULL, '".$Lang['General']['EmptySymbol']."', i.InvoiceDescription) as InvoiceDescription,
			i.DiscountAmount,
			i.TotalAmount,
			IF(i.AccountDate='0000-00-00','".$Lang['General']['EmptySymbol']."',i.AccountDate) as AccountDate,
			$namefield as PicName,
			IF(i.Remarks='' OR i.Remarks IS NULL, '".$Lang['General']['EmptySymbol']."', i.Remarks) as Remarks,
			i.RecordID
		FROM 
			INVOICEMGMT_INVOICE i 
			LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=i.PIC) 
			INNER JOIN ACADEMIC_YEAR ay ON (ay.AcademicYearID=i.AcademicYearID $ayConds)
			$dbtable
		WHERE
			i.RecordStatus = ".RECORDSTATUS_ACTIVE."
			$conds
		$groupBy
		ORDER BY
			InvoiceDate DESC
	";
$result = $linvoice->returnResultSet($sql);
$NoOfRecord = count($result);

for($i=0; $i<$NoOfRecord; $i++)
{
 	$ExportArr[$i][0] = $result[$i]['YearName'];	
 	$ExportArr[$i][1] = $result[$i]['InvoiceDate'];		
	$ExportArr[$i][2] = $result[$i]['Company'];			
	$ExportArr[$i][3] = $result[$i]['InvoiceNo'];			
	$ExportArr[$i][4] = $result[$i]['InvoiceDescription'];	
	$ExportArr[$i][5] = $result[$i]['DiscountAmount'];
	$ExportArr[$i][6] = $result[$i]['TotalAmount'];	
	$ExportArr[$i][7] = $linvoice->getAllAdminGroupOfInvoice($result[$i]['RecordID']);
	$ExportArr[$i][8] = $result[$i]['AccountDate'];	
	$ExportArr[$i][9] = $result[$i]['PicName'];	
	$ExportArr[$i][10] = $result[$i]['Remarks'];			
}


if($task=="item") {
	
	$group_arr = $linvoice->returnAdminGroup();
	$arr_category = $linvoice->getCategoryName();
	$groupAry = array();
	for($a=0; $a<count($group_arr); $a++) {
		$groupAry[$group_arr[$a][0]] = $group_arr[$a][1];	
	}
	
	$catAry = array();
	for($a=0; $a<count($arr_category); $a++) {
		$catAry[$arr_category[$a][0]] = $arr_category[$a][1];	
	}
		
	$itemName = Get_Lang_Selection("i.NameChi", "i.NameEng");
	
	
	# item record
	
	$itemResult = $linvoice->Display_Invoice_Item_Table($RecordID);
	
	$ExportArr[$i][0] = ""; 
	$i++;
		
	for($a=0; $a<count($Lang['Invoice']['ExportIItem']['EN']); $a++) {
		$ExportArr[$i][$a] = $Lang['Invoice']['ExportIItem']['EN'][$a];
	}
	$i++;
	
	for($a=0; $a<count($Lang['Invoice']['ExportIItem']['B5']); $a++) {
		$ExportArr[$i][$a] = $Lang['Invoice']['ExportIItem']['B5'][$a];
	}
	$i++;
	
	$NoOfItem = count($itemResult);
	
	for($a=0; $a<$NoOfItem; $a++) {
		$ExportArr[$i][0] = Get_Lang_Selection($itemResult[$a]['NameChi'], $itemResult[$a]['NameEng']);
		$ExportArr[$i][1] = $itemResult[$a]['Price'];
		$ExportArr[$i][2] = $itemResult[$a]['Quantity'];
		$ExportArr[$i][3] = $catAry[$itemResult[$a]['CategoryID']];
		$ExportArr[$i][4] = $groupAry[$itemResult[$a]['ResourceMgmtGroup']];
		$ExportArr[$i][5] = ($itemResult[$a]['IsAssetItem']==1) ? $i_general_yes : $i_general_no;
		$i++;
	}
	
	
} 

intranet_closedb();

# invoice header
$exportColumn[0] = $Lang['Invoice']['ExportInvoice']['EN'];
$exportColumn[1] = $Lang['Invoice']['ExportInvoice']['B5'];

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");


$filename = 'Invoice_Record.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>
