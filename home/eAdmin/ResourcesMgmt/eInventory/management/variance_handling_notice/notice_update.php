<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory		= new libinventory();

if((isset($SingleRecordID)) || (isset($BulkRecordID)))
{
	if(isset($SingleRecordID))
		$record_list = implode(",",$SingleRecordID);
	if(isset($BulkRecordID))
		$record_list = implode(",",$BulkRecordID);
	
	$sql = "UPDATE 
					INVENTORY_VARIANCE_HANDLING_REMINDER
			SET
					RecordStatus = 1,
					FinishDate = NOW(),
					DateModified = NOW()
			WHERE
					ReminderID IN ($record_list)
			";
	$linventory->db_db_query($sql);
}

header("location: index.php?msg=2");
intranet_closedb();
?>