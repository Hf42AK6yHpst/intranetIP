<?php
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

if(sizeof($Code)==0) {
	header("Location: management_barcode_templates.php");	
}

$result = $linventory->REMOVE_LABEL_FORMAT($Code);

intranet_closedb();

header("Location: management_barcode_templates.php?xmsg=delete");

?>
