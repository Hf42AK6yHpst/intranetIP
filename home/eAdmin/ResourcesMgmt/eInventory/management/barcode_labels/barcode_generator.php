<?php
# using:
###################################################
#   Date: 2020-03-12 Tommy
#   - changed sql order by for sorting different columns
#
#   Date:   2019-02-18 Isaac
#           added ownership to the export data
#
#   Date: 2018-05-18 Cameron
#   - retrieve SubLocationCode [case #P138005]
#   - fix $numberOfOrderByFieldInLabel
#
#	Date:	2015-09-25 	Cameron
#			- add filter PurchaseDate range
#			- fix bug on showing error if memory is exhausted
#
#	Date:	2014-11-12	Henry > ip.2.5.5.12.1
#			add display_zero option [Case#T68957]
#
#   Date:   2014-05-28  Tiffany
#           add intranet_undo_htmlspecialchars() to the content 
###################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
require_once('LabelPrinterTCPDF.php');

intranet_auth();
intranet_opendb();

ini_set('memory_limit',-1);		// unlimited memory, avoid showing error if user didn't select any filter

$linterface 	= new interface_html();
$linventory		= new libinventory();


$label_id = $_REQUEST['label_id'];
$label_format = $linventory->SELECTFROMTABLE('INVENTORY_LABEL_FORMAT','*',array('id' => $label_id));
$label_format = $label_format[0];
$label_format['paper-size'] =array($label_format['paper_size_width'], $label_format['paper_size_height']);
 
$numberOfOrderByFieldInLabel = $linventory->getNumberOfOrderByFieldInLabel();

$pdf = new LabelPrinterTCPDF($label_format);
//for setting the start position
if (!empty($_REQUEST['startPosition'])){
	for($i=1; $i<$_REQUEST['startPosition'];$i++)
		$pdf->AddLabel();
}

if($target_item_list != "")
{
	$cond .= " AND a.ItemID IN ($target_item_list) ";
}

if(trim($search_code)!=''){
	$cond.= " And (a.ItemCode like '%".$search_code."%' or a.NameChi like '%".$search_code."%' or a.NameEng like '%".$search_code."%')";
}
if(is_array($location_id) && sizeof($location_id)>0){
	$cond.= " And c.LocationID in ('".implode("','",$location_id)."')";
}
if(is_array($category_id) && sizeof($category_id)>0){
	$cond.= " And d.CategoryID in ('".implode("','",$category_id)."')";
}
if(is_array($group_id) && sizeof($group_id)>0){
	$cond.= " And e.AdminGroupID in ('".implode("','",$group_id)."')";
}
if(isset($LocationLevelID)){
	$cond.= " And c.LocationLevelID = '".$LocationLevelID."'";
}

if ($purchase_date_start) {
	$cond .= " And b.PurchaseDate>='".$purchase_date_start."'";	
}
if ($purchase_date_end) {
	$cond .= " And b.PurchaseDate<='".$purchase_date_end."'";	
}

if(isset($display_zero) && !$display_zero){
	$cond_display_zero = " And b1.Quantity>0";
}

if($select_single_item && $select_bulk_item)
{				
	$sql .= "(";
}

if($select_single_item)
{
	$sql .= "SELECT 
				a.ItemType,
				a.ItemID,
				a.CategoryID,
				a.category2ID,
				".$linventory->getInventoryItemNameByLang("a.").",
				".$linventory->getInventoryDescriptionNameByLang("a.").",
				a.ItemCode,
				IF(a.Ownership = 1,'$i_InventorySystem_Ownership_School',IF(a.Ownership = 2,'$i_InventorySystem_Ownership_Government',' - ')),
				b.PurchaseDate,
				CONCAT('$',b.PurchasedPrice),
				IF(b.SupplierName = '', ' - ', b.SupplierName),
				IF(b.SupplierContact = '', ' - ', b.SupplierContact),
				IF(b.SupplierDescription = '', ' - ', b.SupplierDescription),
				IF(b.InvoiceNo = '', ' - ', b.InvoiceNo),
				IF(b.QuotationNo = '', ' - ', b.QuotationNo),
				IF(b.TenderNo = '', ' - ', b.TenderNo),
				b.TagCode,
				IF(b.Brand = '', ' - ', b.Brand),
				".$linventory->getInventoryNameByLang("e.").",
				CONCAT(".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("c.").") as location,
				if(b.FundingSource2 is NULL or b.FundingSource2 ='', ".$linventory->getInventoryNameByLang("f.").", concat(".$linventory->getInventoryNameByLang("f.").",', ',".$linventory->getInventoryNameByLang("f2.").")),
				IF(b.WarrantyExpiryDate = '', ' - ', b.WarrantyExpiryDate),
				IF(b.SoftwareLicenseModel = '', ' - ', b.SoftwareLicenseModel),
				b.SerialNumber,
				b.TenderNo,
				b.InvoiceNo,
				b.PurchaseDate,
				CONCAT('<img src=\"$PATH_WRT_ROOT',ipp.PhotoPath,'/',ipp.PhotoName,'\" width=\"100px\">'),
				". $linventory->getInventoryNameByLang("c.") .",
				e.Code,
				b.ItemRemark,
                c.Code AS SubLocationCode
		FROM 
				INVENTORY_ITEM AS a INNER JOIN 
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND a.RecordStatus = 1) LEFT OUTER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) LEFT OUTER JOIN
				INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) LEFT OUTER JOIN
				INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID) LEFT OUTER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.GroupInCharge = e.AdminGroupID) LEFT OUTER JOIN
				INVENTORY_FUNDING_SOURCE AS f ON (b.FundingSource = f.FundingSourceID)
				LEFT OUTER JOIN INVENTORY_FUNDING_SOURCE AS f2 ON (b.FundingSource2 = f2.FundingSourceID)
				LEFT OUTER JOIN INVENTORY_PHOTO_PART AS ipp ON (a.PhotoLink = ipp.PartID) 
		WHERE
				a.ItemType = 1
				$cond ";
} 

if($select_single_item && $select_bulk_item)
{				
	$sql .= " ) UNION (";
}

if($select_bulk_item) 
{
	$sql .= "SELECT 
			a.ItemType,
			a.ItemID,
			a.CategoryID,
			a.category2ID,
			".$linventory->getInventoryItemNameByLang("a.").",
			".$linventory->getInventoryDescriptionNameByLang("a.").",
			a.ItemCode,
			IF(a.Ownership = 1,'$i_InventorySystem_Ownership_School',IF(a.Ownership = 2,'$i_InventorySystem_Ownership_Government',' - ')),
			b.PurchaseDate,
			CONCAT('$',b.PurchasedPrice),
			IF(b.SupplierName = '', ' - ', b.SupplierName),
			IF(b.SupplierContact = '', ' - ', b.SupplierContact),
			IF(b.SupplierDescription = '', ' - ', b.SupplierDescription),
			IF(b.InvoiceNo = '', ' - ', b.InvoiceNo),
			IF(b.QuotationNo = '', ' - ', b.QuotationNo),
			IF(b.TenderNo = '', ' - ', b.TenderNo),
			ext.Barcode as TagCode,
			' - ' as Brand,
			".$linventory->getInventoryNameByLang("e.")." as group_name,
			CONCAT(".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("c.").") as location,
			if(b1.FundingSourceID is NULL or b1.FundingSourceID ='', ' - ', ".$linventory->getInventoryNameByLang("f.").") as funding,
			' - ' as WarrantyExpiryDate,
			' - ' as SoftwareLicenseModel,
			' - ' as SerialNumber,
			b.TenderNo,
			b.InvoiceNo,
			b.PurchaseDate,
			CONCAT('<img src=\"$PATH_WRT_ROOT',ipp.PhotoPath,'/',ipp.PhotoName,'\" width=\"100px\">'),
			". $linventory->getInventoryNameByLang("c.") .",
			e.Code,
			b.Remark,
            c.Code AS SubLocationCode
		FROM INVENTORY_ITEM AS a 
		INNER JOIN INVENTORY_ITEM_BULK_LOCATION as b1 on b1.ItemID=a.ItemID
		INNER JOIN INVENTORY_ITEM_BULK_LOG as b ON b.ItemID=a.ItemID
		INNER JOIN INVENTORY_ITEM_BULK_EXT as ext ON ext.ItemID=b.ItemID  
		LEFT JOIN INVENTORY_LOCATION AS c ON b1.LocationID = c.LocationID 
		LEFT JOIN INVENTORY_LOCATION_LEVEL AS floor ON c.LocationLevelID = floor.LocationLevelID 
		LEFT JOIN INVENTORY_CATEGORY AS d ON a.CategoryID = d.CategoryID 
		LEFT JOIN INVENTORY_ADMIN_GROUP AS e ON b1.GroupInCharge = e.AdminGroupID 
		LEFT JOIN INVENTORY_FUNDING_SOURCE AS f ON b1.FundingSourceID = f.FundingSourceID 
		LEFT JOIN INVENTORY_PHOTO_PART AS ipp ON a.PhotoLink = ipp.PartID 
		WHERE 
			a.ItemType=2 AND a.RecordStatus = 1
			$cond 
			$cond_display_zero 
		GROUP BY a.ItemID, group_name, location, funding
		";
} 

if($select_single_item && $select_bulk_item)
{
	$sql .= ")";
}

if($order_by == 1){
    $orderby = "ItemCode";
}elseif($order_by == 2){
    $orderby = "location";
}else{
    $orderby = "ItemCode";
}

$sql.= " ORDER BY ".$orderby;
$arr_result = $linventory->returnArray($sql);

if(sizeof($arr_result) > 0)
{

	for($i=0; $i<sizeof($arr_result); $i++)
	{

		list($item_type, $item_id, $item_cat, $item_cat2, $item_name, $item_desc, $item_code, $item_ownership, $item_purchase_date,
			$item_purchased_price, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_invoice,
			$item_quotation, $item_tender, $item_tag_code, $item_brand, $item_admin_group, $item_location, $item_funding, 
			$item_warranty_expiry_date, $item_license, $item_serial_num, $item_tender_num, $item_invoice_num, $item_purchase_date, 
		    $item_photo, $item_sublocation, $item_admin_group_code, $item_remark, $item_sublocation_code) = $arr_result[$i];
			
		if($item_purchase_date == "0000-00-00")
			$item_purchase_date = "";
		if($item_warranty_expiry_date == "0000-00-00")
			$item_warranty_expiry_date = "";
			
		if($item_type==2)
		{
			$item_remark = "";
		}	
					
		//$labeltext['item_tag_code'] = $item_tag_code;
		$labeltext['item_name'] = intranet_undo_htmlspecialchars($item_name);
		$labeltext['item_code'] = intranet_undo_htmlspecialchars($item_code);
		$labeltext['item_admin_group'] = intranet_undo_htmlspecialchars($item_admin_group);
		$labeltext['item_admin_group_code'] = intranet_undo_htmlspecialchars($item_admin_group_code);
		$labeltext['item_ownership'] = intranet_undo_htmlspecialchars($item_ownership);
		
		$labeltext['item_location'] = intranet_undo_htmlspecialchars($item_location);		
		$labeltext['item_sublocation'] = intranet_undo_htmlspecialchars($item_sublocation);
		$labeltext['item_sublocation_code'] = intranet_undo_htmlspecialchars($item_sublocation_code);
		$labeltext['item_funding'] = intranet_undo_htmlspecialchars($item_funding);
		$labeltext['item_brand'] = intranet_undo_htmlspecialchars($item_brand);
		$labeltext['item_warranty_expiry_date'] = intranet_undo_htmlspecialchars($item_warranty_expiry_date);
		
		$labeltext['item_license'] = intranet_undo_htmlspecialchars($item_license);		
		$labeltext['item_supplier_name'] = intranet_undo_htmlspecialchars($item_supplier_name);
		$labeltext['item_supplier_contact'] = intranet_undo_htmlspecialchars($item_supplier_contact);
		$labeltext['item_supplier_desc'] = intranet_undo_htmlspecialchars($item_supplier_desc);
		$labeltext['item_serial_num'] = intranet_undo_htmlspecialchars($item_serial_num);
		
		$labeltext['item_tender_num'] = intranet_undo_htmlspecialchars($item_tender_num);
		$labeltext['item_invoice_num'] = intranet_undo_htmlspecialchars($item_invoice_num);
		$labeltext['item_purchase_date'] = intranet_undo_htmlspecialchars($item_purchase_date);
		$labeltext['item_remark'] = intranet_undo_htmlspecialchars($item_remark);
		
		$labeltext['item_description'] = intranet_undo_htmlspecialchars($item_desc);
		
		$order=array();
		$k=0;
		for($j=0; $j<$numberOfOrderByFieldInLabel; $j++){
         	if($label_format['info_order_'.($j+1)]){
        		$order[$k] = $label_format['info_order_'.($j+1)];
        		$k++;
         	}
        }
		$pdf->AddLabel($labeltext,$item_tag_code,false, $order);
		unset($labeltext);		
	}
    $filename = 'label' . time() % 10000 . '.pdf';
    $pdf->Output('$filename', 'I');
} 
else {
	
	header('Content-Type: text/html; charset=utf-8');
	echo $i_InventorySystem['RecordNotFound'];
	exit;
}


intranet_closedb();
?>