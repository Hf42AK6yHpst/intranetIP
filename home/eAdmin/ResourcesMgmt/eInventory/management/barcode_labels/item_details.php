<?php
// using: 

// #############################################
// Date: 2020-03-12 Tommy
// - added display order selection box
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-01 Henry
// PHP 5.4 fix: change split to explode
//
// Date: 2015-12-16 Cameron [case#N89805]
// - change jquery live method to bind method in ScopeLocation, ScopeCategory and ScopeCaretaker
//
// Date: 2015-09-24 Cameron
// - re-arrange Location, Category, Resource Management Group and Particular Item to be "and" condition (case #Z77895)
// - add Filter Purchase Date range
//
// Date: 2015-06-08 Henry > ip.2.5.6.7.1
// add Funding Source Code Option [Case#W78607]
//
// Date: 2014-12-23 Henry > ip.2.5.5.12.1
// add QLabel format (GB) [Case#J72883]
//
// Date: 2014-11-12 Henry > ip.2.5.5.12.1
// add display_zero option [Case#T68957]
//
// Date: 2014-07-03 Tiffany
// fixed: alert message when don`t has barcode template [Case#G63826]
//
// Date: 2014-07-02 YatWoon > ip.2.5.5.8.1
// fixed: data of "date of purchase" is shifted [Case#G63740]
//
// Date: 2014-05-27 YatWoon
// hide "To use the CSV file:" if file format is selected as HTML
//
// Date: 2014-05-23 Tiffany
// add option "custom" to choose the label format.
//
// Date: 2014-04-11 YatWoon
// SKH > default option [Case#H60525]
//
// Date: 2014-04-04 YatWoon
// SKH eInventory, revised sequence ($sys_custom['eInventoryCustForSKH'])
//
// Date: 2013-10-30 YatWoon
// add option "Remarks" [Case#2013-0808-1349-47073]
//
// Date: 2013-05-10 YatWoon
// add option "sub-location code" with flag $special_feature['eDiscipline']['BarcodeDisplaySubLocationCode']
//
// Date: 2013-04-25 YatWoon
// add option "sub-location", "resources group code"
//
// Date: 2013-04-03 YatWoon
// add option "single item" and "bulk item" [Case#2013-0402-1200-31073]
//
// Date: 2013-02-21 Carlos
// Add navigation tag [Resource Mgmt Group Barcode] and [Funding Source Barcode]
//
// Date: 2012-11-26 YatWoon
// default checkbox checked [Case#2012-1121-1617-09054]
//
// Date: 2012-11-05 YatWoon
// Add bar code format, support QLabel (just export xxxxxx)
//
// Date: 2012-10-29 Rita
// Add "Purchase Date" Option
//
// Date: 2012-07-27 YatWoon
// Add "barcode format" [Case#2012-0223-1108-40073]
//
// #############################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

// ## set cookies
if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
// preserve table view
if ($ck_category_item_browsing_full_list_page_number != $pageNo && $pageNo != "") {
    setcookie("ck_category_item_browsing_full_list_page_number", $pageNo, 0, "", "", 0);
    $ck_category_item_browsing_full_list_page_number = $pageNo;
} else 
    if (! isset($pageNo) && $ck_category_item_browsing_full_list_page_number != "") {
        $pageNo = $ck_category_item_browsing_full_list_page_number;
    }

if ($ck_category_item_browsing_full_list_page_order != $order && $order != "") {
    setcookie("ck_category_item_browsing_full_list_page_order", $order, 0, "", "", 0);
    $ck_category_item_browsing_full_list_page_order = $order;
} else 
    if (! isset($order) && $ck_category_item_browsing_full_list_page_order != "") {
        $order = $ck_category_item_browsing_full_list_page_order;
    }

if ($ck_category_item_browsing_full_list_page_field != $field && $field != "") {
    setcookie("ck_category_item_browsing_full_list_page_field", $field, 0, "", "", 0);
    $ck_category_item_browsing_full_list_page_field = $field;
} else 
    if (! isset($field) && $ck_category_item_browsing_full_list_page_field != "") {
        $field = $ck_category_item_browsing_full_list_page_field;
    }

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$min_barcode_width = DEFAULT_MINIMUM_BARCODE_WIDTH;
$max_barcode_width = DEFAULT_MAXIMUN_BARCODE_WIDTH;

$CurrentPageArr['ResourcesManagement'] = 1;
$CurrentPage = "Management_BarcodeLabels";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$default_on = array(
    $i_InventorySystem_Item_Barcode,
    $i_InventorySystem_Item_Name,
    $i_InventorySystem_Item_Code,
    $i_InventorySystem_Group_Name,
    $i_InventorySystem_Item_Location,
    $i_InventorySystem_Item_Funding
);

// export column
if ($sys_custom['eInventoryCustForSKH']) {
    $exportColumn = array(
        $i_InventorySystem_Item_Code,
        $i_InventorySystem_Item_Funding,
        $i_InventorySystem_Item_FundingSourceCode,
        $Lang['eInventory']['GenearlDescription'],
        $i_InventorySystem_Item_Brand_Name,
        $i_InventorySystem_Item_Location,
        $i_InventorySystem_Location
    );
    array_push($exportColumn, $i_InventorySystem_Item_Barcode, $i_InventorySystem_Item_Name, $i_InventorySystem_Group_Name, $i_InventorySystem_Caretaker_Code);
    array_push($exportColumn, $i_InventorySystem_Item_WarrantyExpiryDate, $i_InventorySystem_Item_License, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Supplier_Contact, $i_InventorySystem_Item_Supplier_Description, $i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Purchase_Date);
    
    $default_on = array(
        $i_InventorySystem_Item_Code,
        $i_InventorySystem_Location,
        $i_InventorySystem_Item_FundingSourceCode,
        $i_InventorySystem_Item_Funding,
        $Lang['eInventory']['GenearlDescription'],
        $i_InventorySystem_Item_Brand_Name
    );
} else // default sequence
{
    $exportColumn = array(
        $i_InventorySystem_Item_Barcode,
        $i_InventorySystem_Item_Name,
        $i_InventorySystem_Item_Code,
        $i_InventorySystem_Group_Name,
        $i_InventorySystem_Caretaker_Code,
        $i_InventorySystem_Item_Location,
        $i_InventorySystem_Location,
        $i_InventorySystem_Item_Ownership
    );
    if ($special_feature['eDiscipline']['BarcodeDisplaySubLocationCode'])
        array_push($exportColumn, $i_InventorySystem_Location_Code);
    array_push($exportColumn, $i_InventorySystem_Item_Funding, $i_InventorySystem_Item_FundingSourceCode, $i_InventorySystem_Item_Brand_Name, $i_InventorySystem_Item_WarrantyExpiryDate, $i_InventorySystem_Item_License, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Supplier_Contact, $i_InventorySystem_Item_Supplier_Description, $i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Purchase_Date);
    
    if ($linventory->enablePhotoDisplayInBarcodeRight()) {
        array_push($exportColumn, $Lang['eInventory']['ItemPhoto']);
    }
}
array_push($exportColumn, $i_InventorySystem_Item_Remark . " (" . $Lang['eInventory']['ForSingleItemOnly'] . ")");
// debug_pr($exportColumn);

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_ItemDetails,
    "item_details.php",
    1
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['LocationBarcode'],
    "location_barcode.php",
    0
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'],
    "admin_group_barcode.php",
    0
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['FundingSourceBarcode'],
    "funding_source_barcode.php",
    0
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// rebuild
if (! empty($ItemID)) {
    $ItemID_New = array();
    foreach ($ItemID as $k => $d) {
        list ($item_id_temp, $item_locationid_temp) = explode(":", $d);
        $ItemID_New[] = $item_id_temp;
    }
    $ItemID = $ItemID_New;
}

if (is_array($ItemID))
    $target_item_list = implode(",", $ItemID);

$arr_file_type = array(
    array(
        1,
        "HTML"
    ),
    array(
        2,
        "CSV"
    )
);
$file_type_selection = getSelectByArray($arr_file_type, "name=\"file_type\" id=\"file_type\" onChange=\"chageFileFormat(this.value)\"", $file_type, 0, 1);

// # file format
$table_content1 .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">$i_InventorySystem_FileFormat</td>";
$table_content1 .= "<td class=\"tabletext\">$file_type_selection</td></tr>";

// ## label per page
$arr_label_per_page = array(
    array(
        1,
        1
    ),
    array(
        2,
        2
    ),
    array(
        3,
        3
    ),
    array(
        4,
        4
    ),
    array(
        5,
        5
    ),
    array(
        6,
        6
    ),
    array(
        7,
        7
    ),
    array(
        8,
        8
    )
);
$label_per_page_selection = getSelectByArray($arr_label_per_page, "name=\"label_per_page\"", 3, 0, 1);
$table_content1 .= "<tr id=\"labelperpage\" style=\"\">";
$table_content1 .= "<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">$i_InventorySystem_LabelPerPage</td>";
$table_content1 .= "<td class=\"tabletext\">$label_per_page_selection</td></tr>";

// ## barcode width
$table_content1 .= "<tr id=\"barcodewidth\" style=\"\">";
$table_content1 .= "<td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['FieldTitle']['BarcodeWidth'] . "</td>";
if ($barcodeWidth == "") {
    $barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
    do {
        $barcodeWidthSelection .= "<option value=" . $min_barcode_width . ">" . $min_barcode_width . "</option>";
        $min_barcode_width = $min_barcode_width + DEFAULT_BARCODE_WIDTH_INCREMENT;
    } while ($min_barcode_width <= $max_barcode_width);
    $barcodeWidthSelection .= "</select>";
    $table_content1 .= "<td>" . $barcodeWidthSelection . "</td>";
} else {
    $barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
    do {
        if ($barcodeWidth == $min_barcode_width) {
            $barcodeWidthSelection .= "<option value=" . $min_barcode_width . " selected>" . $min_barcode_width . "</option>";
        } else {
            $barcodeWidthSelection .= "<option value=" . $min_barcode_width . ">" . $min_barcode_width . "</option>";
        }
        $min_barcode_width = $min_barcode_width + DEFAULT_BARCODE_WIDTH_INCREMENT;
    } while ($min_barcode_width <= $max_barcode_width);
    $barcodeWidthSelection .= "</select>";
    $table_content1 .= "<td>" . $barcodeWidthSelection . "</td>";
}
$table_content1 .= "</tr>";

// ## export format
$table_content1 .= "<tr id=\"barcodeformat\" style=\"display:none\">";
$table_content1 .= "<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['BarcodeFormat2'] . "</td>";
$table_content1 .= "<td><input type='radio' name='barcodeformat' value=1 id='barcodeformat1' checked> <label for='barcodeformat1'>!xxxxxxxxx!</label> 
					<input type='radio' name='barcodeformat' value=2 id='barcodeformat2'> <label for='barcodeformat2'>(!xxxxxxxxx!)</label>
					<input type='radio' name='barcodeformat' value=3 id='barcodeformat3'> <label for='barcodeformat3'>" . $Lang['eInventory']['QLabelFormat'] . " (Big5)</label>
					<input type='radio' name='barcodeformat' value=4 id='barcodeformat4'> <label for='barcodeformat4'>" . $Lang['eInventory']['QLabelFormat'] . " (GB)</label>
					</td>";
$table_content1 .= "</td></tr>";

// ## item type
// $Lang['eInventory']['SingleItem']
$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">$i_general_Type</td>";
$table_both .= "<td>
					<input type='checkbox' name='select_single_item' id='single_item' value='1' checked> <label for='single_item'>" . $Lang['eInventory']['SingleItem'] . "</label> 
					<input type='checkbox' name='select_bulk_item' id='bulk_item' value='1' checked>  <label for='bulk_item'>" . $Lang['eInventory']['BulkItem'] . "</label> 
					</td></tr>";

// ## show item
// $table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">".$i_InventorySystem_Report_Col_Item."</td><td>";
// all items
// by location
// by category
// by management group
// by search

$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['ItemLocation'] . "</td>";
$table_both .= "<td><input type='radio' name='ScopeLocation' id='ScopeLocationAll' value='1' checked><label for='ScopeLocationAll'>" . $Lang['eInventory']['AllLocation'] . "</label> " . "<input type='radio' name='ScopeLocation' id='ScopeLocationPart' value='2'><label for='ScopeLocationPart'>" . $Lang['eInventory']['ParticularLocation'] . "</label>" . "<td></tr>";
$table_both .= "<tr id=\"scope_location_tr\" style=\"display:none\"><td>&nbsp;</td><td id=\"scope_location_td\"></td></tr>";
$table_both .= "<tr id=\"scope_sub_location_tr\" style=\"display:none\"><td>&nbsp;</td><td id=\"scope_sub_location_td\"></td></tr>";

$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['Category'] . "</td>";
$table_both .= "<td><input type='radio' name='ScopeCategory' id='ScopeCategoryAll' value='1' checked><label for='ScopeCategoryAll'>" . $Lang['eInventory']['AllCategory'] . "</label> " . "<input type='radio' name='ScopeCategory' id='ScopeCategoryPart' value='2'><label for='ScopeCategoryPart'>" . $Lang['eInventory']['ParticularCategory'] . "</label>" . "<td></tr>";
$table_both .= "<tr id=\"scope_category_tr\" style=\"display:none\"><td>&nbsp;</td><td id=\"scope_category_td\"></td></tr>";

$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['Caretaker'] . "</td>";
$table_both .= "<td><input type='radio' name='ScopeCaretaker' id='ScopeCaretakerAll' value='1' checked><label for='ScopeCaretakerAll'>" . $Lang['eInventory']['AllCaretaker'] . "</label> " . "<input type='radio' name='ScopeCaretaker' id='ScopeCaretakerPart' value='2'><label for='ScopeCaretakerPart'>" . $Lang['eInventory']['ParticularCaretaker'] . "</label>" . "<td></tr>";
$table_both .= "<tr id=\"scope_caretaker_tr\" style=\"display:none\"><td>&nbsp;</td><td id=\"scope_caretaker_td\"></td></tr>";

$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['ParticularItem'] . "</td>";
$table_both .= "<td><input type=\"textbox\" id=\"search_code\" name=\"search_code\"/> &nbsp; <span class=\"calendarday\" >" . $Lang['eInventory']['SearchByCodeDesc'] . "</span></td></tr>";

$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $i_InventorySystem_Search_Purchase_Date_Between . "</td>
				<td class=\"tabletext\" valign=\"top\">" . $linterface->GET_DATE_PICKER('purchase_date_start', $purchase_date = "", $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1, $Disable = false, $cssClass = "textboxnum") . " <span id='div_PurchaseDate_err_msg'></span>" . $i_InventorySystem_Search_And . "&nbsp;" . $linterface->GET_DATE_PICKER('purchase_date_end', $purchase_date = "", $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1, $Disable = false, $cssClass = "textboxnum") . " <span id='div_PurchaseDate_err_msg'></span>
				</td></tr>";

// $Item_Scope_Options[] = array("",$Lang['eInventory']['FieldTitle']['StocktakeType']['AllItem']);
// $Item_Scope_Options[] = array("LT",$i_InventorySystem['ByLocation']);
// $Item_Scope_Options[] = array("CA",$i_InventorySystem['ByCategory']);
// $Item_Scope_Options[] = array("MG",$i_InventorySystem['ByCaretaker']);
// $Item_Scope_Options[] = array("SO",$i_InventorySystem['BySearch']);
// $item_scope = $linterface->GET_SELECTION_BOX($Item_Scope_Options, " name='ItemScope' id='ItemScope' onChange=\"chageItemScope(this.value)\" ", "");
// $table_both .= $item_scope."</td></tr>\n";

// $table_both .= "</td></tr>\n";

// ## display zero
$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $Lang['eInventory']['DisplayZeroRecord'] . "</td>";
$table_both .= "<td>
					<input type='radio' name='display_zero' id='display_zero_yes' value='1'> <label for='display_zero_yes'>" . $i_general_yes . "</label> 
					<input type='radio' name='display_zero' id='display_zero_no' value='0' checked>  <label for='display_zero_no'>" . $i_general_no . "</label> 
					</td></tr>";

$table_both .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">" . $i_InventorySystem_Category_DisplayOrder . "</td>";
$order_by = array(
    array(
        1,
        $Lang['eInventory']['ParticularItem']
    ),
    array(
        2,
        $Lang['eInventory']['ItemLocation']
    )
);
$order_selection = getSelectByArray($order_by,  "id=\"display_order\" name=\"display_order\"", 1, 0, 1);
$table_both .= "<td class=\"tabletext\">$order_selection</td></tr>";

$table_both .= "<tr id=\"scope_tr\" style=\"display:none\"><td>&nbsp;</td><td id=\"scope_td\"></td></tr>";
$table_both .= "<tr id=\"scope_tr2\" style=\"display:none\"><td>&nbsp;</td><td id=\"scope_td2\"></td></tr>";
// $table_both .= "<tr id=\"search_code_tr\" style=\"display:none\"><td>&nbsp;</td><td><input type=\"textbox\" id=\"search_code\" name=\"search_code\"/> &nbsp; <span class=\"calendarday\" >".$Lang['eInventory']['SearchByCodeDesc']."</span></td></tr>";

$table_content2 .= "<tr id=\"csv_usage\" style=\"display:none\"><td></td>";
$table_content2 .= " <td>
						<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\">
							<tr>
                                <td align=\"left\" class=\"steptitlebg2\"><span class=\"calendarday\"><strong>$i_InventorySystem_ImportInstruction_Title:</strong></span></td>
                            </tr>
							<tr>
								<td bgcolor=\"#CCCCCC\">
									<div align=\"center\">
									<table border=\"0\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">
										<tr><td>
											<strong class=\"calendarday\">1. </strong><span class=\"calendarday\"><a href=\"http://www.bizfonts.com/free/IDAutomationCode39.zip\">$i_InventorySystem_ImportInstruction_Download</a>$i_InventorySystem_ImportInstruction_InstallFontType</span><br>
											&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"{$image_path}/{$LAYOUT_SKIN}/arrow.gif\"><br>
											<strong class=\"calendarday\">2. </strong><span class=\"calendarday\"><a href=\"item_tag_template.doc\">$i_InventorySystem_ImportInstruction_Download</a>$i_InventorySystem_ImportInstruction_ReportTemplate</span><br>
											&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"{$image_path}/{$LAYOUT_SKIN}/arrow.gif\"><br>
											<strong class=\"calendarday\">3. </strong><span class=\"calendarday\">$i_InventorySystem_ImportInstruction_ImportCSVFile</span><br>
											&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"{$image_path}/{$LAYOUT_SKIN}/arrow.gif\"><br>
											<strong class=\"calendarday\">4. </strong><span class=\"calendarday\">$i_InventorySystem_ImportInstruction_PrintReport</span>
										</td></tr>
									</table>
									</div>
								</td>
							</tr>
						</table>
					</td>";

for ($i = 0; $i < sizeof($exportColumn); $i ++) {
    if ($i % 2 == 0)
        $row_css = " class=\"tablerow1\" ";
    else
        $row_css = " class=\"tablerow2\" ";
    
    if ((($i + 1) % 3) == 1 && $i != 0) {
        $column_selection .= "<tr $row_css>";
    }
    
    $checked = in_array($exportColumn[$i], $default_on) ? "checked" : "";
    $column_selection .= "<td width=\"20%\" class=\"tabletext\"><input type=\"checkbox\" name=\"ColumnName[]\" value=\"$i\" " . $checked . " id=\"ColumnName" . $i . "\"> <label for=\"ColumnName" . $i . "\">$exportColumn[$i]</label></td>";
    
    if ((($i + 1) % 3) == 0) {
        $column_selection .= "</tr>";
    }
}
// Tiffany add-----------
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$field_array = array(
    "name",
    "paper_size_width",
    "paper_size_height",
    "NX",
    "NY",
    "metric"
);
$order_array = array(
    "DESC",
    "ASC"
);
$result = $linventory->SELECTFROMTABLE("INVENTORY_LABEL_FORMAT", array(
    'id',
    'name'
), "", "", "", "$field_array[$field]", "$order_array[$order]");

$LabelFormatOption = $linterface->GET_SELECTION_BOX($result, " name='label_id' id='select_label_id' ", '', '');

// Tiffany add-----------

?>
<script type="text/javascript"
	src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet"
	href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css"
	type="text/css" />
<script language="javascript">
$(document).ready(function() {

	$("#search_code").autocomplete("ajax_search_item.php",
		{
			formatItem: function(row) {

				return row[1] ;
			},
			maxItemsToShow: 50,
			minChars: 1,
			delay: 0,
			autoFill: false,
			overflow: true,
			divheight: 150,
			width: 300
			
		}
	);
	
	var format="<?echo $format?>";
    if(format=="auto"){
        $("input[type='radio'][name='format'][value='2']").attr("checked", "checked"); 
    }
    
    if($(':radio[name="format"]:checked').val()=="2"){
    	showDiv('custom');hideDiv('default1');hideDiv('default2');
    }

	$("input:radio[name='ScopeLocation']").bind( 'click', function(){
		if ($('#ScopeLocationPart').is(":checked")) {
			getScopeHTML('LT');
		}
		else {
			$('#scope_location_tr').hide();
			$('#scope_location_td').html('');
			$('#scope_sub_location_tr').hide();
			$('#scope_sub_location_td').html('');
		}		
	});
	
	$("input:radio[name='ScopeCategory']").bind( 'click', function(){
		if ($('#ScopeCategoryPart').is(":checked")) {
			getScopeHTML('CA');
		}
		else {
			$('#scope_category_tr').hide();
			$('#scope_category_td').html('');
		}		
	});
	
	$("input:radio[name='ScopeCaretaker']").bind( 'click', function(){
		if ($('#ScopeCaretakerPart').is(":checked")) {
			getScopeHTML('MG');
		}
		else {
			$('#scope_caretaker_tr').hide();
			$('#scope_caretaker_td').html('');
		}		
	});

});
function checkForm()
{
	var obj = document.form1;
	var objFileType = document.getElementById('file_type');
	var check_purchase_date = 0;
	
	if((obj.purchase_date_start.value != "") && (obj.purchase_date_end.value != ""))
	{
		if(check_date(obj.purchase_date_start,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning;?>")) {
			if(check_date(obj.purchase_date_end,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning;?>")) {
				if (compare_date(obj.purchase_date_start, obj.purchase_date_end, "<?=$Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate']?>"))
				{
					check_purchase_date = 1;
				}
			}
		}
	}
	else if ((obj.purchase_date_start.value != "") && (obj.purchase_date_end.value == "")) {
		if(check_date(obj.purchase_date_start,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning;?>")) {
			check_purchase_date = 1;
		}
	}
	else if ((obj.purchase_date_start.value == "") && (obj.purchase_date_end.value != "")) {
		if(check_date(obj.purchase_date_end,"<?=$i_InventorySystem_Input_Item_PurchaseDate_Warning;?>")) {
			check_purchase_date = 1;
		}
	}
	else {
		check_purchase_date = 1;
	}
	
	if (check_purchase_date == 1) {
	    if($(':radio[name="format"]:checked').val()=="2"){
	    
	    	 obj.action = "barcode_generator.php";
		     obj.target='_blank';
		     return true;
			 obj.action = "";
	    }
		else{
			if(objFileType.value == 1)
			{
				if(obj.flag.value == 1)
				{
					if(check_positive_nonzero_int(obj.barcodeWidth,"<?=$Lang['eInventory']['JSWarning']['InvalidBarcodeWidth'];?>"))
					{
						obj.action = "item_details_print.php";
						obj.target='intranet_popup10';
			        	newWindow('about:blank', 10);
						return true;
						obj.action = "";
					}
					else
					{
						return false;
					}
				}
			}
			if(objFileType.value == 2)
			{
				if(obj.flag.value == 1)
				{
					obj.action = "item_details_export.php";
					return true;
					obj.action = "";
				}
			}
		}
	}
	return false;
}
function showSubLocation(LocationLevelID)
{
	
	$.post("ajax_scope_html.php", {task: 'sub_location_html',LocationLevelID:LocationLevelID},  
		function(data, textStatus)
		{
			$('#scope_sub_location_tr').show();
			$('#scope_sub_location_td').html(data);
		}
			
	);	
}


function chageFileFormat(filetype)
{
	if(filetype==1)
	{
		$('#labelperpage').show();
		$('#barcodewidth').show();
		$('#barcodeformat').hide();
		$('#csv_usage').hide();
	}
	else
	{
		$('#labelperpage').hide();
		$('#barcodewidth').hide();
		$('#barcodeformat').show();
		$('#csv_usage').show();
	}
}

//function chageItemScope(scope){
//	
//	switch(scope)
//	{
//		case "LT":
//		case "CA":
//		case "MG":
//			$('#scope_tr2').hide();
//			$('#scope_td2').html('');
//			getScopeHTML(scope);
//			$('#search_code_tr').hide();
//			$('#search_code').val('');
//			
//		break;
//		case "SO":
//			$('#search_code_tr').show();
//			$('#scope_tr').hide();
//			$('#scope_td').html('');
//			$('#scope_tr2').hide();
//			$('#scope_td2').html('');
//		break;
//		
//		case "":
//			$('#scope_tr').hide();
//			$('#scope_tr2').hide();
//			$('#search_code_tr').hide();
//			$('#search_code').val('');
//		break;	
//		
//	}
//}

function getScopeHTML(scope){
	
	$.post("ajax_scope_html.php", {task: 'scope_html',scope:scope},  
		function(data, textStatus)
		{
			var name;
			switch(scope) {
				case 'LT':
					name = 'location';
					break;
				case 'CA':
					name = 'category';
					break;
				case 'MG':
					name = 'caretaker';
					break;
				default:
					name = '';
					break;
			}
			
			$('#scope_'+name+'_tr').show();
			$('#scope_'+name+'_td').html(data);
			
			if(scope=="LT"){
				showSubLocation($('#LocationLevelID').val());
			}
		}
			
	);	
}

	
		
function checkAllOption(checked,checkbox_id)
{
	var groupby = $('#ItemScope').val()
	checked?setChecked(1,document.form1,checkbox_id):setChecked(0,document.form1,checkbox_id);	
	
}


function showDiv(layername) {
	$('#'+layername).show();
	if(layername=="custom"){
        getLabelNum();
        $('#select_label_id').change(function(){
    		getLabelNum();
    	}); 
    }
}

function hideDiv(layername) {
	$('#'+layername).hide();
}

function getLabelNum(){
	var labelID = $('#select_label_id').val();
			//alert(labelID);
			$.post('ajax_get_label_number.php', { label_id : labelID},
					function(responseText){
						responseText = $.trim(responseText);
						if (responseText != ''){
							//result_table = $('#bookAdd');
							//result_table.append(responseText);
							$('#div_label_num').html(responseText);
						}else{
							alert("<?=$Lang["eInventory"]["label"]["msg"]["no_template"]?>");
						}
						//$('#td_loading').html('');
					}
	
			);
}


</script>

<br>

<form name="form1" action="" method="POST"
	onSubmit="return checkForm();">
	<table border="0" width="96%" cellspacing="0" cellpadding="5">
	<?=$toolbar?>
</table>
	<br>
	<table border="0" width="90%" cellpadding="5" cellspacing="0"
		align="center" class="form_table_v30">
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">    
       <?=$i_InventorySystem['Format']?>
    </td>
			<td><input type='radio' name='format' value=1 id='format1' checked
				onClick="showDiv('default1');showDiv('default2');hideDiv('custom');">
				<label for='format1'><?=$i_InventorySystem['Default']?></label> <input
				type='radio' name='format' value=2 id='format2'
				onClick="showDiv('custom');hideDiv('default1');hideDiv('default2');">
				<label for='format2'><?=$i_InventorySystem['Custom']?></label></td>
		</tr>
		<tbody id="default1">
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="field_title">
		<?=$i_InventorySystem_Export_Select_Column?>
	</td>
				<td>
					<table border="0" width="100%" cellpadding="5" cellspacing="0"
						align="center">
		<?=$column_selection?>
	</table>
				</td>
			</tr>
	<?=$table_content1?>
</tbody>
		<tbody id="custom" style="display: none;">
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="field_title">
		<?=$i_InventorySystem['BarcodeTemplates']?>
	</td>
				<td>
	    <?=$LabelFormatOption?><a href="management_barcode_templates.php"
					style="text-decoration: none; font-size: 12">[<?=$i_InventorySystem['ManagementBarcodeTemplates']?>]</a>
				</td>
			</tr>
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="field_title">
		<?=$i_InventorySystem['StartPrintPosition']?>
	</td>
				<td><div id="div_label_num"><?=$linterface->Get_Number_Selection("startPosition", 1, 1, 1, $Onchange='', $noFirst=0, $isAll=0, $FirstTitle='', $Disabled=0)?>
	</div></td>
			</tr>
		</tbody>
	<?=$table_both?>
<tbody id="default2">
	<?=$table_content2?>
	
</tbody>
	</table>
	<table border="0" width="90%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr height="10px">
			<td></td>
		</tr>
		<tr>
			<td align="center">
	<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
</td>
		</tr>
	</table>
	<input type="hidden" name="flag" value="1"> <input type="hidden"
		name="target_item_list" value="<?=$target_item_list;?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>