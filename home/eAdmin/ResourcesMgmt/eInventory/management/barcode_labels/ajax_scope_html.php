<?php
// using  

########################################################
#
#   Date: 	2019-05-13 Henry
#            Security fix: SQL without quote
# 
#	Date:	2015-09-30 Cameron
#			Change "All Locations" to "All Sub-locations" ($Lang['eInventory']['AllSubLocation'])  
#
#	Date:	2012-07-27	YatWoon
#			Improved: add <label> for option
#
#	Date:	2012-07-03	YatWoon
#			Improved: display category with distinct from INVENTORY_ITEM
########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_opendb();

$libdb 	= new libdb();
$lv 	= new libinventory();

switch($task){
	case "scope_html":
		switch($scope)
		{
			case "LT":
					//$sql = "SELECT LocationLevelID, ".$lv->getInventoryNameByLang()." as LevelName FROM INVENTORY_LOCATION_LEVEL ORDER BY DisplayOrder, NameEng";
					$sql = "SELECT distinct(a.LocationLevelID), concat(". $lv->getInventoryNameByLang("c.").", ' > ', ".$lv->getInventoryNameByLang("a.") .") as LevelName 
					FROM 
					INVENTORY_LOCATION_LEVEL as a 
					left join INVENTORY_LOCATION as b on b.LocationLevelID=a.LocationLevelID 
					left join INVENTORY_LOCATION_BUILDING as c on c.BuildingID=a.BuildingID 
					where b.LocationID is not null 
					and c.RecordStatus=1 and a.RecordStatus=1
					ORDER BY c.DisplayOrder, a.DisplayOrder, a.NameEng";
					$location_array = $lv->returnArray($sql,3);
					$html = getSelectByArray($location_array,"name=\"LocationLevelID\" id=\"LocationLevelID\"  onChange=\"showSubLocation(this.value)\" ",$LocationLevelID,0,1);
			break;
			case "CA":
// 					$sql = "SELECT CategoryID, ".$lv->getInventoryNameByLang()." as CatName FROM INVENTORY_CATEGORY ORDER BY DisplayOrder, NameEng";
					$sql = "SELECT 
								distinct(a.CategoryID), 
								".$lv->getInventoryNameByLang("b.")." as CatName
							FROM 
								INVENTORY_ITEM as a
								left join INVENTORY_CATEGORY as b on a.CategoryID = b.CategoryID
							ORDER BY b.DisplayOrder";
					$category_array = $lv->returnArray($sql,3);
					
					$html = '<table width="100%">';
					$html .= '<tr><td><label><input type="checkbox" name="all_location" onClick="checkAllOption(this.checked,\'category_id[]\');">'.$i_InventorySystem_ViewItem_All_Category.'</label></td></tr>';
					for($a=0;$a<sizeof($category_array);$a++){
						
// 						if($a%2 == 0)
// 							$row_css = " class=\"tablerow1\" ";
// 						else
// 							$row_css = " class=\"tablerow2\" ";
						
						if ((($a+1)%3) == 1 && $a!=0) 
							$html .= "<tr>";
						
						$html.='<td><label><input type="checkbox" id="category_id" name="category_id[]" value="'.$category_array[$a]['CategoryID'].'"> '.$category_array[$a]['CatName'].'</label></td>';
						if ((($a+1)%3) == 0)
							$html .= "</tr>";
					}
					$html .= "</table>";
			break;
			case "MG":
					$sql = "SELECT AdminGroupID, ".$lv->getInventoryNameByLang()." as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
					$group_array = $lv->returnArray($sql,2);
					
					$html = '<table width="100%">';
					$html .= '<tr><td><label><input type="checkbox" name="all_group" onClick="checkAllOption(this.checked,\'group_id[]\');">'.$i_InventorySystem_ViewItem_All_Resource_Management_Group.'</label></td></tr>';
					for($a=0;$a<sizeof($group_array);$a++){
						if($a%2 == 0)
							$row_css = " class=\"tablerow1\" ";
						else
							$row_css = " class=\"tablerow2\" ";	
							
						if ((($a+1)%3) == 1 && $a!=0) 
							$html .= "<tr $row_css>";
						
						$html.='<td><label><input type="checkbox" id="group_id" name="group_id[]" value="'.$group_array[$a]['AdminGroupID'].'"> '.$group_array[$a]['GroupName'].'</label></td>';
						if ((($a+1)%3) == 0)
							$html .= "</tr>";
					}
					$html .= "</table>";
			break;
		}	
	break;
	
	case "sub_location_html":
		$sql = "SELECT LocationID, ".$lv->getInventoryNameByLang()." as LocationName FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$LocationLevelID."' ORDER BY DisplayOrder, NameEng";
		$location_array = $lv->returnArray($sql,3);

		if(sizeof($location_array)>0){
			$html = '<table width="100%">';
			$html .= '<tr><td><label><input type="checkbox" name="all_location" onClick="checkAllOption(this.checked,\'location_id[]\');">'.$Lang['eInventory']['AllSubLocation'].'</label></td></tr>';
			for($a=0;$a<sizeof($location_array);$a++){
				if($a%2 == 0)
					$row_css = " class=\"tablerow1\" ";
				else
					$row_css = " class=\"tablerow2\" ";
				
				if ((($a+1)%3) == 1 && $a!=0) 
					$html .= "<tr $row_css>";
				
				$html.='<td><label><input type="checkbox" id="location_id" name="location_id[]" value="'.$location_array[$a]['LocationID'].'"> '.$location_array[$a]['LocationName'].'</label></td>';
				if ((($a+1)%3) == 0)
					$html .= "</tr>";
							
			}
			$html .= "</table>";
		}
	break;
}

echo $html;

intranet_closedb();
die;
?>