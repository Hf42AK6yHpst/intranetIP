<?php
// using : 
/*
 * Page created on 2013-02-20
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$min_barcode_width = DEFAULT_MINIMUM_BARCODE_WIDTH;
$max_barcode_width = DEFAULT_MAXIMUN_BARCODE_WIDTH;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_BarcodeLabels";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem_Report_ItemDetails, "item_details.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['LocationBarcode'], "location_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'], "admin_group_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['FundingSourceBarcode'], "funding_source_barcode.php", 1);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


# Admin groups selection
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$sql = "SELECT FundingSourceID, $namefield FROM INVENTORY_FUNDING_SOURCE ORDER BY DisplayOrder";
$fundings = $linventory->returnArray($sql);

$opt_fundings = $linterface->GET_SELECTION_BOX($fundings, 'id="targetFundingSource" name="targetFundingSource[]" multiple="multiple" size="10"' , "", $targetFundingSource);
$funding_source_selection = "<tr><td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_alert_pleaseselect."</td><td>".$opt_fundings."</td></tr>";


# Barcode width selection
$barcode_width_selection .= "<tr>";
$barcode_width_selection .= "<td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['eInventory']['FieldTitle']['BarcodeWidth']."</td>";
if($barcodeWidth == "")
{
	//$barcode_width_selection .= "<td><input type='text' name='barcodeWidth' id='barcodeWidth' value='".DEFAULT_BARCODE_WIDTH."'></td>";
	
	$barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
	do
	{
		$barcodeWidthSelection .= "<option value=".$min_barcode_width.">".$min_barcode_width."</option>";
		$min_barcode_width = $min_barcode_width+DEFAULT_BARCODE_WIDTH_INCREMENT;
	}
	while($min_barcode_width <= $max_barcode_width);
	$barcodeWidthSelection .= "</select>";
	$barcode_width_selection .= "<td>".$barcodeWidthSelection."</td>";
}
else
{
	//$barcode_width_selection .= "<td><input type='text' name='barcodeWidth' id='barcodeWidth' value='".$barcodeWidth."'></td>";
	$barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
	do
	{
		if($barcodeWidth == $min_barcode_width)
		{
			$barcodeWidthSelection .= "<option value=".$min_barcode_width." selected>".$min_barcode_width."</option>";
		}
		else
		{
			$barcodeWidthSelection .= "<option value=".$min_barcode_width.">".$min_barcode_width."</option>";
		}
		$min_barcode_width = $min_barcode_width+DEFAULT_BARCODE_WIDTH_INCREMENT;
	}
	while($min_barcode_width <= $max_barcode_width);
	$barcodeWidthSelection .= "</select>";
	$barcode_width_selection .= "<td>".$barcodeWidthSelection."</td>";
}
$barcode_width_selection .= "</tr>";


if(($flag == 1) && ($DisplayBarcode == 1) && count($targetFundingSource)>0)
{
	$fundingSourceIdCsv = implode(",",$targetFundingSource);
	
	$barcode_table .= "<br>";
	$barcode_table .= "<table border='0' width='87%' align='center' cellpadding='4' cellspacing='0'>";
	
	$barcode_table .= "<tr><td class=\"tabletext\">".$linterface->GET_LNK_PRINT("javascript:openPrintPage('".$fundingSourceIdCsv."','$barcodeWidth')","","","","",0)."</td><td></td></tr>";
	$barcode_table .= "<tr class=\"tabletop\">";
	$barcode_table .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Item_Barcode."</td>";
	$barcode_table .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Setting_Funding_Name."</td>";
	$barcode_table .= "</tr>";
	
	$sql = "SELECT FundingSourceID, $namefield, Barcode FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID IN ($fundingSourceIdCsv) ORDER BY DisplayOrder";
	$result = $linventory->returnArray($sql);
	
	for($i=0; $i<sizeof($result); $i++)
	{
		list($funding_source_id, $funding_name, $funding_barcode) = $result[$i];
		$css = ($i%2 == 0) ? " class=\"tablerow1\" " : " class=\"tablerow2\" ";
		
		$barcode_label = "<img src='barcode.php?barcode=".rawurlencode($funding_barcode)."&width=$barcodeWidth'>";
		
		$barcode_table .= "<tr $css>";
		$barcode_table .= "<td>$barcode_label</td>";
		$barcode_table .= "<td>$funding_name</td>";
		$barcode_table .= "</tr>";
	}
	
	$barcode_table .= "</table>";
}

?>
<script language="javascript">
	function openPrintPage(targetFundingSource,barcodeWidth)
	{
		newWindow("funding_source_barcode_print.php?targetFundingSource='"+targetFundingSource+"'&barcodeWidth='"+barcodeWidth+"'",10);
	}
	function checkForm()
	{
		var obj = document.form1;
		var fundingOptions = document.getElementById('targetFundingSource')
		var selectedCount = 0;
		
		if(fundingOptions)
		{
			for(var i=0; i<fundingOptions.options.length; i++)
			{
				if(fundingOptions.options[i].selected)
				{
					selectedCount+=1;
				}
			}
		}
		
		if(selectedCount == 0) {
			alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
			return false;
		}
		
		obj.action = "funding_source_barcode.php";
		document.form1.submit();
		return true;
	}
</script>
<br>
<form name="form1" action="" method="POST" onSubmit="return checkForm()">
	<table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
		<?=$funding_source_selection;?>
		<?=$barcode_width_selection;?>
		<tr>
			<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1; document.form1.DisplayBarcode.value=1;"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="dotline">
				<img src=\"<?=$image_path;?>/<?=$LAYOUT_SKIN;?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>		
		<input type="hidden" name="flag" value="">
		<input type="hidden" name="DisplayBarcode" value="0">
	</table>
	<?=$barcode_table;?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>