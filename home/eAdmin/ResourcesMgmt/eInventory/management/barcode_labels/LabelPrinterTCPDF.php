<?php
//using: 
/********************
 * Log :
 * Date     2019-10-03 [Tommy]
 *          added $barcode_style_num and $barcode_style_num checking for output barcode with barcode number
 * 
 * Date		2015-06-11 [Tiffany]
 * 			added setFontSubsetting(false)
 * 
 ********************/
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT.'home/library_sys/lib/tcpdf/tcpdf.php');

class LabelPrinterTCPDF extends TCPDF {

	public $LabelName  = '';       // Name of format
	public $marginLeft = 0;        // Left margin of labels
	public $marginTop  = 0;        // Top margin of labels
	public $xSpace     = 0;        // Horizontal space between 2 labels
	public $ySpace     = 0;        // Vertical space between 2 labels
	public $xNumber    = 0;        // Number of labels horizontally
	public $yNumber    = 0;        // Number of labels vertically
	public $width      = 0;        // Width of label
	public $height     = 0;        // Height of label
	public $charSize   = 10;       // Character size
	public $lineHeight = 0;        // Default line height
	public $metric     = 'mm';     // Type of metric for labels.. Will help to calculate good values
	public $metricDoc  = 'mm';     // Type of metric for the document
	public $fontName   = 'ukai'; // Name of the font
	public $smallest_bar = 0.3;		// width of the smallest bar in user units (empty string = default value = 0.4mm)
	public $countX     = 0;
	public $countY     = 0;
	public $printing_margin_h = 0;
	public $printing_margin_v = 0;
	public $max_barcode_width = 90;
	public $max_barcode_height = 20;
	
	// Listing of labels size
	protected  $averyLabels =
	array (
			'A200' => array('name' => 'A200', 'paper-size' => array(204, 165), 'metric' => 'mm',
					'lMargin' => 1.25, 'tMargin' => 3.5, 'NX' => 2, 'NY' => 4,
					'SpaceX' => 2, 'SpaceY' => 2, 'width' => 100, 'height' => 38,
					'font_size' => 10),
			
			'5160' => array('name' => '5160', 'paper-size' => 'letter', 'metric' => 'mm',
					'lMargin' => 4.7625, 'tMargin' => 12.7, 'NX' => 3, 'NY' => 10,
					'SpaceX' => 3.96875, 'SpaceY' => 0, 'width' => 65.875, 'height' => 25.4,
					'font_size' => 8),
			'5161' => array('name' => '5161', 'paper-size' => 'letter', 'metric' => 'mm',
					'lMargin' => 0.967, 'tMargin' => 10.7, 'NX' => 2, 'NY' => 10,
					'SpaceX' => 3.967, 'SpaceY' => 0, 'width' => 101.6,
					'height' => 25.4, 'font_size' => 8),
			'5162' => array('name' => '5162', 'paper-size' => 'letter', 'metric' => 'mm',
					'lMargin' => 2.97, 'tMargin' => 20.224, 'NX' => 2, 'NY' => 7,
					'SpaceX' => 4.762, 'SpaceY' => 1, 'width' => 100.807,
					'height' => 35.72, 'font_size' => 8),
			'5163' => array('name' => '5163', 'paper-size' => 'letter', 'metric' => 'mm',
					'lMargin' => 1.762,'tMargin' => 10.7, 'NX' => 2,
					'NY' => 5, 'SpaceX' => 3.175, 'SpaceY' => 0, 'width' => 101.6,
					'height' => 50.8, 'font_size' => 8),
			'5164' => array('name' => '5164', 'paper-size' => 'letter', 'metric' => 'in',
					'lMargin' => 0.148, 'tMargin' => 0.5, 'NX' => 2, 'NY' => 3,
					'SpaceX' => 0.2031, 'SpaceY' => 0, 'width' => 4.0, 'height' => 3.33,
					'font_size' => 12),
			'8600' => array('name' => '8600', 'paper-size' => 'letter', 'metric' => 'mm',
					'lMargin' => 7.1, 'tMargin' => 19, 'NX' => 3, 'NY' => 10,
					'SpaceX' => 9.5, 'SpaceY' => 3.1, 'width' => 66.6,
					'height' => 25.4, 'font_size' => 8),
			'L7160' => array('name' => 'L7160', 'paper-size' => 'A4', 'metric' => 'mm', 'lMargin' => 6,
					'tMargin' => 15.1, 'NX' => 3, 'NY' => 7, 'SpaceX' => 2.5, 'SpaceY' => 0,
					'width' => 63.5, 'height' => 38.1, 'font_size' => 9),
			'L7161' => array('name' => 'L7161', 'paper-size' => 'A4', 'metric' => 'mm', 'lMargin' => 6,
					'tMargin' => 9, 'NX' => 3, 'NY' => 6, 'SpaceX'=> 5, 'SpaceY' => 2,
					'width' => 63.5, 'height' => 46.6, 'font_size' => 9),
			'L7163' => array('name' => 'L7163', 'paper-size' => 'A4', 'metric' => 'mm', 'lMargin' => 5,
					'tMargin' => 15, 'NX' => 2, 'NY' => 7, 'SpaceX' => 2.5, 'SpaceY' => 0,
					'width' => 99.1, 'height' => 38.1, 'font_size' => 9)
	);
	protected 	$barcode_style = array(
		
		'position' => '',
		'align' => 'C',
	    'stretch' => true,
        //'stretch' => false,
		'fitwidth' => true,
		//'fitwidth' => false,
		'cellfitalign' => 'C',
		'border' => false,
		//'hpadding' => '',
		//'vpadding' => '',
		'hpadding' => 'auto',
		'vpadding' => 'auto',
		'fgcolor' => array(0,0,0),
		'bgcolor' => false, //array(255,255,255),
		'text' => true,
		'font' => 'arialuni', //Henry:2013/10/23 original: ukai
		'fontsize' => 8,
		'stretchtext' => 0

	); 
	
	protected 	$barcode_style_num = array(
	    
	    'position' => '',
	    'align' => 'C',
	    'stretch' => true,
	    //'stretch' => false,
	    'fitwidth' => true,
	    //'fitwidth' => false,
	    'cellfitalign' => 'C',
	    'border' => false,
	    //'hpadding' => '',
	    //'vpadding' => '',
	    'hpadding' => 'auto',
	    'vpadding' => 'auto',
	    'fgcolor' => array(0,0,0),
	    'bgcolor' => false, //array(255,255,255),
	    'text' => true,
	    'font' => 'arialuni', //Henry:2013/10/23 original: ukai
	    'fontsize' => 12,
	    'stretchtext' => 4
	    
	); 
	/**
	 * Constructor
	 *
	 * @param $format type of label 
	 * 
	 * @param unit type of unit used we can define your label properties in inches by setting metric to 'in'
	 *
	 * @access public
	 */

	function __construct ($format='A200', $smallest_bar =null) {
		if (is_array($format)) {
			// Custom format
			$tFormat = $format;
		} else {
			// Avery format
			$tFormat = $this->averyLabels[$format];
		}
		 
		parent::__construct('', $tFormat['metric'], $tFormat['paper-size'], true, 'UTF8');
		$this->metricDoc = $tFormat['metric'];
		
		$this->setFontSubsetting(false); // Tiffany added [20150611]
		$this->SetFormat($tFormat);
		$this->SetFontName('arialuni'); //uncomment this to use non-default font
		$this->SetMargins(0,0);
		$this->SetAutoPageBreak(false);
		$this->smallest_bar = $smallest_bar;
		
		
	}

	/*
	 * function to convert units (in to mm, mm to in)
	*
	*/
	function ConvertMetric ($value, $src, $dest) {
		if ($src != $dest) {
			$tab['in'] = 39.37008;
			$tab['mm'] = 1000;
			return $value * $tab[$dest] / $tab[$src];
		} else {
			return $value;
		}
	}
	/*
	 * function to Give the height for a char size given.
	*/
	function GetHeightChars($pt) {
		return (0.186 + 0.20 * $pt )*(0.186 + 0.20 * $pt );
// 		// Array matching character sizes and line heights
// 		$tableHauteurChars = array(6 => 2, 7 => 2.5, 8 => 3, 9 => 4, 10 => 5, 11 => 6, 12 => 7, 13 => 8, 14 => 9, 15 => 10);
// 		if (in_array($pt, array_keys($tableHauteurChars))) {
// 			return $tableHauteurChars[$pt];
// 		} else {
// 			return 100; // There is a prob..
// 		}
	}
	/*
	* Set Formating in member variable
	* $format Type of $averyName
	*/
	function SetFormat($format) {
		$this->metric     = $format['metric'];
		$this->LabelName  = $format['name'];
		$this->marginLeft = $this->ConvertMetric ($format['lMargin'], $this->metric, $this->metricDoc);
		$this->marginTop  = $this->ConvertMetric ($format['tMargin'], $this->metric, $this->metricDoc);
		$this->xSpace     = $this->ConvertMetric ($format['SpaceX'], $this->metric, $this->metricDoc);
		$this->ySpace     = $this->ConvertMetric ($format['SpaceY'], $this->metric, $this->metricDoc);
		$this->xNumber    = $format['NX'];
		$this->yNumber    = $format['NY'];
		$this->width      = $this->ConvertMetric ($format['width'], $this->metric, $this->metricDoc);
		$this->height     = $this->ConvertMetric ($format['height'], $this->metric, $this->metricDoc);
		$this->lineHeight     = $this->ConvertMetric ($format['lineHeight'], $this->metric, $this->metricDoc);
		if(!empty($format['printing_margin_h']))
			$this->printing_margin_h  = $this->ConvertMetric ($format['printing_margin_h'], $this->metric, $this->metricDoc);
		if(!empty($format['printing_margin_v']))
			$this->printing_margin_v  = $this->ConvertMetric ($format['printing_margin_v'], $this->metric, $this->metricDoc);
		if(!empty($format['max_barcode_width']))
			$this->max_barcode_width  = $this->ConvertMetric ($format['max_barcode_width'], $this->metric, $this->metricDoc);
		if(!empty($format['max_barcode_height']))
			$this->max_barcode_height  = $this->ConvertMetric ($format['max_barcode_height'], $this->metric, $this->metricDoc);
		
		
		$this->LabelSetFontSize($format['font_size']);
	}
	/*
	* function to set the character size
	* $pt weight of character
	*/
	function LabelSetFontSize($pt) {
		if ($pt > 3) {
			$this->charSize = $pt;
			if( $this->lineHeight == 0 )
				$this->lineHeight = $this->ConvertMetric ($this->GetHeightChars($pt), 'mm', $this->metricDoc);
			$this->SetFontSize($this->charSize);
		}
	}
	/*
	 * Method to change font name
	*
	* $fontname name of font
	*/
	function SetFontName($fontname) {
		if ($fontname != '') {
			$this->fontName = $fontname;
			$this->SetFont($this->fontName);
		}
	}

	/*
	 * function to Print a label
	*/
	function AddLabel($text=array(),$barcode='', $border=false, $order='') { //Henry 20131030 added $order = array()
				
		// We are in a new page, then we must add a page
		if (($this->countX ==0) and ($this->countY==0)) {
			$this->setPrintHeader(false); //Henry:2013/10/22
			$this->AddPage();
		}
		
		$printing_margin_h = $this->printing_margin_h;
		$printing_margin_v = $this->printing_margin_v;
		
		$posX = $this->marginLeft+($this->countX*($this->width+$this->xSpace));
		$posY = $this->marginTop+($this->countY*($this->height+$this->ySpace));
		
		if ($border){
			$this->Rect($posX, $posY,$this->width,$this->height);
			
		}
		$this->SetXY($posX+$printing_margin_h, $posY + $printing_margin_v);
		$this->maxwidth = $this->width - $printing_margin_h *2;
		//wrap the text if it's width is greater than maxwidth
		//      $this->wordWrap( $texte, $maxwidth); not supported by TCPDF, which does its own wrapping
		// define barcode style
		$line=0;
		$line1=0;
		//add book title, location and call number above the barcode
		if(!empty($text)){
			if(is_array($text)){
				if(empty($text['barcode_text'])){
					$this->barcode_style['text'] = false;
				}else
					unset($text['barcode_text']);
				$fontheight = $this->lineHeight;

				
				//-----------Henry added 20131030 for the ordering of the book info [start]
				if($order){
					$isInputBarCode = 0;
					$barcode_height = 0;
					for($i=0; $i<count($order); $i++){
						//if(!empty($text[$order[$i]])){
							if($order[$i] == 'item_tag_code'){
								//--------- Setting barcode image [Start]
								$barcode_width = $this->width - $printing_margin_h *2; // 6 for margins
								if ($barcode_width > $this->max_barcode_width){
									$barcode_width = $this->max_barcode_width;
									//centering
									$new_margin = (($this->maxwidth - $barcode_width )/2)+$printing_margin_h;
									
								}else{
									$new_margin = $printing_margin_h;
								}
						
								$this->SetXY($posX+$new_margin, $posY+($line * $this->lineHeight)+$printing_margin_v);
								
								$barcode_height =$this->height -(($line) * $this->lineHeight);
								$barcode_height -= $printing_margin_v; // 6 for margins
								
								if($barcode_height > $this->max_barcode_height)
									$barcode_height = $this->max_barcode_height;
								
								$this->write1DBarcode($barcode, 'C39', '', '', $barcode_width, $barcode_height, $this->smallest_bar, $this->barcode_style, 'N');
								//$this->write1DBarcode($barcode, 'C39', '', '', "50", "12", '', '','N');
								//--------- Setting barcode image [End]
								
								$isInputBarCode = 1;
							}
							else if($order[$i] == 'item_barcode_num'){
							    //--------- Setting barcode image [Start]
							    $barcode_width = $this->width - $printing_margin_h *2; // 6 for margins
							    if ($barcode_width > $this->max_barcode_width){
							        $barcode_width = $this->max_barcode_width;
							        //centering
							        $new_margin = (($this->maxwidth - $barcode_width )/2)+$printing_margin_h;
							        
							    }else{
							        $new_margin = $printing_margin_h;
							    }
							    
							    $this->SetXY($posX+$new_margin, $posY+($line * $this->lineHeight)+$printing_margin_v);
							    
							    $barcode_height =$this->height -(($line) * $this->lineHeight);
							    $barcode_height -= $printing_margin_v; // 6 for margins
							    
							    if($barcode_height > $this->max_barcode_height)
							        $barcode_height = $this->max_barcode_height;
							        
							        $this->write1DBarcode($barcode, 'C39', '', '', $barcode_width, $barcode_height, $this->smallest_bar, $this->barcode_style_num, 'N');
							        //$this->write1DBarcode($barcode, 'C39', '', '', "50", "12", '', '','N');
							        //--------- Setting barcode image [End]
							        $isInputBarCode = 1;
							}
							else if(trim($text[$order[$i]])){
								
								//$text[$order[$i]] = iconv("big5","utf-8",$text[$order[$i]]);
								//Change the position of school name and book code
								$printing_width = $this->width-$printing_margin_v*2;
								$printing_height = $this->lineHeight;
								
								if($isInputBarCode == 1){
									$this->SetXY($posX+$printing_margin_h, $posY+($line * $this->lineHeight)+$printing_margin_v+$barcode_height -1);
									$line +=$this->MultiCell($printing_width, $printing_height, $text[$order[$i]], 0 ,'C');
								}
								else{
									$line +=$this->MultiCell($printing_width, $printing_height, $text[$order[$i]], 0 ,'C');
									$this->SetXY($posX+$printing_margin_h, $posY+($line * $this->lineHeight)+$printing_margin_v);
								}
							}
						//}
					}
				}
				//-----------Henry added 20131030 for the ordering of the book info [end]
				
			}else{
				$line++;
				$this->MultiCell($this->width, $this->lineHeight, $text , 0 ,'C');
			}
		}
		

	//fill the label by row instead of column Henry added 20131030
		//shift to next label
		$this->countX++;

		if ($this->countX == $this->xNumber) {
			// End of column reached, we start a new one
			$this->countY++;
			$this->countX=0;
		}

		if ($this->countY == $this->yNumber) {
			// Page full, we start a new one
			$this->countY=0;
			$this->countX=0;
		}


	}

}
