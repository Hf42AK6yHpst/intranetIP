<?php
// using : 
/**
 * **************************** Change Log **************************************************
 * 2019-04-30 (Henry): security issue fix
 * 2018-02-07 (Henry): add access right checking [Case#E135442]
 * Page created on 2013-02-20
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$min_barcode_width = DEFAULT_MINIMUM_BARCODE_WIDTH;
$max_barcode_width = DEFAULT_MAXIMUN_BARCODE_WIDTH;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_BarcodeLabels";
$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$barcodeWidth = IntegerSafe($barcodeWidth);

$TAGS_OBJ[] = array($i_InventorySystem_Report_ItemDetails, "item_details.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['LocationBarcode'], "location_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'], "admin_group_barcode.php", 1);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['FundingSourceBarcode'], "funding_source_barcode.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


# Admin groups selection
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
$admin_groups = $linventory->returnArray($sql);

$opt_group = $linterface->GET_SELECTION_BOX($admin_groups, 'id="targetGroup" name="targetGroup[]" multiple="multiple" size="10"' , "", $targetGroup);
$admin_group_selection = "<tr><td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_alert_pleaseselect."</td><td>".$opt_group."</td></tr>";


# Barcode width selection
$barcode_width_selection .= "<tr>";
$barcode_width_selection .= "<td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['eInventory']['FieldTitle']['BarcodeWidth']."</td>";
if($barcodeWidth == "")
{
	//$barcode_width_selection .= "<td><input type='text' name='barcodeWidth' id='barcodeWidth' value='".DEFAULT_BARCODE_WIDTH."'></td>";
	
	$barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
	do
	{
		$barcodeWidthSelection .= "<option value=".$min_barcode_width.">".$min_barcode_width."</option>";
		$min_barcode_width = $min_barcode_width+DEFAULT_BARCODE_WIDTH_INCREMENT;
	}
	while($min_barcode_width <= $max_barcode_width);
	$barcodeWidthSelection .= "</select>";
	$barcode_width_selection .= "<td>".$barcodeWidthSelection."</td>";
}
else
{
	//$barcode_width_selection .= "<td><input type='text' name='barcodeWidth' id='barcodeWidth' value='".$barcodeWidth."'></td>";
	$barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
	do
	{
		if($barcodeWidth == $min_barcode_width)
		{
			$barcodeWidthSelection .= "<option value=".$min_barcode_width." selected>".$min_barcode_width."</option>";
		}
		else
		{
			$barcodeWidthSelection .= "<option value=".$min_barcode_width.">".$min_barcode_width."</option>";
		}
		$min_barcode_width = $min_barcode_width+DEFAULT_BARCODE_WIDTH_INCREMENT;
	}
	while($min_barcode_width <= $max_barcode_width);
	$barcodeWidthSelection .= "</select>";
	$barcode_width_selection .= "<td>".$barcodeWidthSelection."</td>";
}
$barcode_width_selection .= "</tr>";


if(($flag == 1) && ($DisplayBarcode == 1) && count($targetGroup)>0)
{
	$groupIdCsv = implode(",",$targetGroup);
	
	$barcode_table .= "<br>";
	$barcode_table .= "<table border='0' width='87%' align='center' cellpadding='4' cellspacing='0'>";
	
	$barcode_table .= "<tr><td class=\"tabletext\">".$linterface->GET_LNK_PRINT("javascript:openPrintPage('".$groupIdCsv."','$barcodeWidth')","","","","",0)."</td><td></td></tr>";
	$barcode_table .= "<tr class=\"tabletop\">";
	$barcode_table .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Item_Barcode."</td>";
	$barcode_table .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Group_Name."</td>";
	$barcode_table .= "</tr>";
	
	$sql = "SELECT AdminGroupID, $namefield, Barcode FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID IN ($groupIdCsv) ORDER BY DisplayOrder";
	$result = $linventory->returnArray($sql);
	
	for($i=0; $i<sizeof($result); $i++)
	{
		list($admin_group_id, $group_name, $group_barcode) = $result[$i];
		$css = ($i%2 == 0) ? " class=\"tablerow1\" " : " class=\"tablerow2\" ";
		
		$barcode_label = "<img src='barcode.php?barcode=".rawurlencode($group_barcode)."&width=$barcodeWidth'>";
		
		$barcode_table .= "<tr $css>";
		$barcode_table .= "<td>$barcode_label</td>";
		$barcode_table .= "<td>$group_name</td>";
		$barcode_table .= "</tr>";
	}
	
	$barcode_table .= "</table>";
}

?>
<script language="javascript">
	function openPrintPage(targetGroup,barcodeWidth)
	{
		newWindow("admin_group_barcode_print.php?targetGroup='"+targetGroup+"'&barcodeWidth='"+barcodeWidth+"'",10);
	}
	function checkForm()
	{
		var obj = document.form1;
		var groupOptions = document.getElementById('targetGroup');
		var selectedCount = 0;
		var i = 0;
		
		if(groupOptions)
		{
			for(i=0; i<groupOptions.options.length; i++)
			{
				if(groupOptions.options[i].selected)
				{
					selectedCount+=1;
				}
			}
		}
		
		if(selectedCount == 0) {
			alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
			return false;
		}
		
		obj.action = "admin_group_barcode.php";
		document.form1.submit();
		return true;
	}
</script>
<br>
<form name="form1" action="" method="POST" onSubmit="checkForm();return false;">
	<table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
		<?=$admin_group_selection;?>
		<?=$barcode_width_selection;?>
		<tr>
			<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1; document.form1.DisplayBarcode.value=1;"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="dotline">
				<img src=\"<?=$image_path;?>/<?=$LAYOUT_SKIN;?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>		
		<input type="hidden" name="flag" value="">
		<input type="hidden" name="DisplayBarcode" value="0">
	</table>
	<?=$barcode_table;?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>