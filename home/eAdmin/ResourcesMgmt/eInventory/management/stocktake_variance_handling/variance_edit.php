<?php

// using: 

// ##############################
// Date: 2019-10-16 (Tommy)
// add $GroupID and $FundingID
//
// Date: 2019-05-01 (Henry)
// security issue fix for SQL
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2015-06-24 Henry
// add option handled and writeoff table [Case#P80191]
//
// Date: 2015-04-01 Henry
// fix: No "Handler" and "Location" are found if user clicks "Request write-off " first. [Case#76952]
//
// Date: 2015-01-07 Henry
// Rename Write-off to Request Write-off, New Quantity cannot count the Request Write-off quantity
//
// Date: 2014-11-26 Henry
// Improvement: add writeoff reason
//
// Date: 2011-12-22 YatWoon
// Fixed: failed to display the bulk records if the stocktake user's account is deleted.
//
// Date: 2011-05-26 YatWoon
// Improved: display building name and change the location selection option
//
// Date: 2011-05-24 YAtWoon
// Fixed: bulk item - retrieve variance manager instead of resources mgmt group as handler
//
// Date: 2011-05-20 YAtWoon
// revised the logic according to the eInventory review meeting
//
// ##############################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_StocktakeVarianceHandling";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID);
$GroupID = IntegerSafe($GroupID);
$FundingID = IntegerSafe($FundingID);

$TAGS_OBJ[] = array(
    $i_InventorySystem_StocktakeVarianceHandling,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$sql = "SELECT 
				CONCAT(" . $linventory->getInventoryItemNameByLang("") . ",' (',ItemCode,')')
		FROM 
				INVENTORY_ITEM
		WHERE
				ItemID = '".$ItemID."'";

$result = $linventory->returnArray($sql, 1);

$temp[] = array(
    $result[0][0],
    ""
);

$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION2($temp[0][0]) . "</td></tr>";
// End #

// get item type #
$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$ItemID."'";
$arr_item_type = $linventory->returnVector($sql);
$item_type = $arr_item_type[0];
// end #

// Get Default Write Off Reason + default variance writeoff reason #
$sql = "SELECT ReasonTypeID, " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE";
$arr_write_off_reason = $linventory->returnArray($sql, 2);
array_unshift($arr_write_off_reason, array(
    "0",
    $i_InventorySystem_Action_Stocktake_Not_Found
));
$reason_selection = getSelectByArray($arr_write_off_reason, " name=\"writeoff_reason\" ", "", 0, 1);

// -- content of writeoff record table [Start]
// if($item_type == ITEM_TYPE_SINGLE)
// {
$RequestPerson = getNameFieldByLang2("f.");
$ApprovePerson = getNameFieldByLang2("g.");

$RequestArchivedPerson = getNameFieldByLang2("h.");
$ApproveArchivedPerson = getNameFieldByLang2("i.");

$sql = "SELECT 
				b.ItemCode, 
				b.ItemType, 
				" . $linventory->getInventoryItemNameByLang("b.") . " as this_item_name,
				concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("d.") . ",
				" . $linventory->getInventoryNameByLang("funding.") . ",
				a.WriteOffQty,
				a.RequestDate,
				IFNULL($RequestPerson,
					IF( $RequestArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $RequestArchivedPerson)) 
					),
				a.ApproveDate, 
				IFNULL($ApprovePerson,
					IF( $ApproveArchivedPerson is null, '" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "', concat('<font color=red>*</font> ', $ApproveArchivedPerson)) 
					),
				a.WriteOffReason,
				a.RecordID,
				" . $linventory->getInventoryNameByLang("funding2.") . ",
				a.ItemID, a.LocationID
		FROM
								INVENTORY_ITEM_WRITE_OFF_RECORD AS a 
				INNER JOIN 		INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) 
				INNER JOIN 		INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) 
				INNER JOIN 		INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) 
				INNER JOIN 		INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) 
				INNER JOIN 		INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID $cond)
				LEFT OUTER JOIN INTRANET_USER AS f ON (a.RequestPerson = f.UserID)
				LEFT OUTER JOIN INTRANET_USER AS g ON (a.ApprovePerson = g.UserID) 
				left join INTRANET_ARCHIVE_USER as h on h.UserID=a.RequestPerson
				left join INTRANET_ARCHIVE_USER as i on i.UserID=a.ApprovePerson
				left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
				left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
				left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=s.FundingSource2)
		WHERE
				a.RecordStatus = 1 
				AND a.ItemID IN ('".$ItemID."')
				
		order by a.ApproveDate desc
		";
$writeoff_arr_result = $linventory->returnArray($sql);

$writeoff_table_content .= "<tr class=\"tabletop\">";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffReason_Name</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_RequestPerson</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApprovePerson</td>";
$writeoff_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
$writeoff_table_content .= "</tr>";

for ($i = 0; $i < sizeof($writeoff_arr_result); $i ++) {
    list ($item_code, $item_type, $item_name, $location_name, $group_name, $funding_name, $write_off_qty, $request_date, $request_person, $approve_date, $approve_person, $write_off_reason, $request_id, $funding_name2, $this_item_id, $this_location_id) = $writeoff_arr_result[$i];
    if ($item_type == ITEM_TYPE_SINGLE) {
        $Style = 'single tabletext';
    } else {
        $Style = 'bulk tabletext';
        
        // retrieve bulk item latest funding source name
        $sql_bl = "select " . $linventory->getInventoryNameByLang("b.") . " from INVENTORY_ITEM_BULK_LOG as a left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSource where a.ItemID='".$this_item_id."' and a.FundingSource!='' and a.LocationID='".$this_location_id."' order by a.RecordID desc limit 1";
        $result_bl = $linventory->returnVector($sql_bl);
        $funding_name = $result_bl[0];
    }
    
    $writeoff_table_content .= "<tr $row_css>";
    $writeoff_table_content .= "<td class=\"$Style\">$item_code</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$item_name</td>";
    $writeoff_table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($location_name) . "</td>";
    $writeoff_table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($group_name) . "</td>";
    $writeoff_table_content .= "<td class=\"$Style\">" . intranet_htmlspecialchars($funding_name) . (($funding_name2) ? ", " . intranet_htmlspecialchars($funding_name2) : "") . "</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$write_off_qty</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$write_off_reason</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$request_person</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$request_date</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$approve_person</td>";
    $writeoff_table_content .= "<td class=\"$Style\">$approve_date</td>";
    
    $writeoff_table_content .= "</tr>";
    $writeoff_table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=11></td></tr>";
}
$showWriteoffTable = true;
if (sizeof($writeoff_arr_result) == 0) {
    $showWriteoffTable = false;
    $writeoff_table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"11\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}
$writeoff_table_content .= "<tr height=\"20px\"><td colspan=\"11\"></td></tr>";

// }
// -- content of writeoff record table [End]

if ($item_type == ITEM_TYPE_SINGLE) {
    $table_content .= "<tr class=\"tabletop\">";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_VarianceHandling_CurrentLocation . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_ExpectedLocation . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_LastStocktakeBy . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Setting_StockCheckPeriod . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>";
    $table_content .= "</tr>";
    
    $sql = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				b.AdminGroupID,
				" . $linventory->getInventoryNameByLang("e.") . ",
				b.PersonInCharge,
				" . getNameFieldByLang2("f.") . ",
				b.RecordDate,
				CONCAT('+',b.SurplusQty)
			FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
			WHERE	
				a.ItemID IN ('".$ItemID."')
			";
    $arr_surplus_result = $linventory->returnArray($sql, 8);
    
    $sql = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				b.AdminGroupID,
				" . $linventory->getInventoryNameByLang("e.") . ",
				b.PersonInCharge,
				" . getNameFieldByLang2("f.") . ",
				b.RecordDate,
				CONCAT('-',b.MissingQty)
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_MISSING_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
		WHERE	
				a.ItemID IN ('".$ItemID."')
		";
    $arr_missing_result = $linventory->returnArray($sql, 8);
    
    $sql = "SELECT
				b.LocationID, 
				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
			FROM
				INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
				INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) 
				inner join INVENTORY_LOCATION_BUILDING as d on (d.BuildingID = c.BuildingID)
			WHERE
				a.ItemID = '".$ItemID."'
			";
    $arr_original_location = $linventory->returnArray($sql, 2);
    list ($original_location_id, $original_location_name) = $arr_original_location[0];
    
    if (sizeof($arr_surplus_result) > 0) {
        for ($i = 0; $i < sizeof($arr_surplus_result); $i ++) {
            list ($surplus_location_id, $surplus_location_name, $surplus_admin_id, $surplus_admin_name, $surplus_user_id, $surplus_user_name, $surplus_date, $surplus_qty) = $arr_surplus_result[$i];
            
            $table_content .= "<tr class=\"single\">";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($surplus_location_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($original_location_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($surplus_admin_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_user_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_date</td>";
            $table_content .= "<td class=\"tabletext\"><font color=\"green\">$surplus_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"6\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"surplus_location_id\" value=\"$surplus_location_id\">";
        }
    }
    if (sizeof($arr_missing_result) > 0) {
        for ($i = 0; $i < sizeof($arr_missing_result); $i ++) {
            list ($miss_location_id, $miss_location_name, $miss_admin_id, $miss_admin_name, $miss_user_id, $miss_user_name, $miss_date, $missing_qty) = $arr_missing_result[$i];
            
            $table_content .= "<tr class=\"single\">";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($miss_location_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($original_location_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($miss_admin_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">$miss_user_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_date</td>";
            $table_content .= "<td class=\"tabletext\"><font color=\"red\">$missing_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"6\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"missing_location_id\" value=\"$miss_location_id\">";
        }
    }
    
    // $new_location_selection = getSelectByArray($arr_new_location,"name=\"targetNewLocation\"",0,0,1);
    // $old_location_selection = getSelectByArray($arr_old_location,"name=\"targetOldLocation\"",0,0,1);
    // $write_off_location = getSelectByArray($arr_old_location,"name=\"targetWriteOffLocation\"",0,0,1);
    
    switch ($variance_action) {
        case 1:
            $checked1 = "CHECKED";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            $checked8 = " ";
            break;
        case 2:
            $checked1 = "";
            $checked2 = "CHECKED";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            $checked8 = " ";
            break;
        case 3:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = "CHECKED";
            $checked4 = " ";
            $checked5 = " ";
            $checked8 = " ";
            break;
        case 4:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = "CHECKED";
            $checked5 = " ";
            $checked8 = " ";
            break;
        case 5:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = "CHECKED";
            $checked8 = " ";
            break;
        case 8:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            $checked8 = "CHECKED";
            break;
        default:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            $checked8 = " ";
            break;
    }
    
    // #### + -
    if (($surplus_qty != 0) && ($missing_qty != 0)) {
        $table_action .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_alert_pleaseselect:</td>
								<td class=\"tabletext\">
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action1\" value=\"1\" onClick=\"this.form.submit();\" $checked1><label for=\"variance_action1\">$i_InventorySystem_VarianceHandling_Action[1]</label><br>
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action2\" value=\"2\" onClick=\"this.form.submit();\" $checked2><label for=\"variance_action2\">$i_InventorySystem_VarianceHandling_Action[2]</label><br>
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action3\" value=\"3\" onClick=\"this.form.submit();\" $checked3><label for=\"variance_action3\">$i_InventorySystem_VarianceHandling_Action[3]</label>
									( " . $i_InventorySystem['WriteOffReason'] . ": " . $reason_selection . " )<br>";
        if ($showWriteoffTable)
            $table_action .= "<input type=\"radio\" name=\"variance_action\" id=\"variance_action8\" value=\"8\" onClick=\"" . (trim($checked1) || trim($checked2) || trim($checked3) ? "this.form.submit();" : "") . "\" $checked8><label for=\"variance_action8\">$i_InventorySystem_VarianceHandling_Action[8]</label><br>";
        $table_action .= "</td>
						  </tr>";
        $table_action .= "<input type=\"hidden\" name=\"single_condition\" value=\"1\">";
    }
    
    // #### + only
    if (($surplus_qty != 0) && ($missing_qty == 0)) {
        $table_action .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_alert_pleaseselect:</td>
								<td class=\"tabletext\">
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action1\" value=\"1\" onClick=\"this.form.submit();\" $checked1><label for=\"variance_action1\">$i_InventorySystem_VarianceHandling_Action[1]</label><br>
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action2\" value=\"2\" onClick=\"this.form.submit();\" $checked2><label for=\"variance_action2\">$i_InventorySystem_VarianceHandling_Action[2]</label><br>";
        if ($showWriteoffTable)
            $table_action .= "<input type=\"radio\" name=\"variance_action\" id=\"variance_action8\" value=\"8\" onClick=\"" . (trim($checked1) || trim($checked2) ? "this.form.submit();" : "") . "\" $checked8><label for=\"variance_action8\">$i_InventorySystem_VarianceHandling_Action[8]</label><br>";
        $table_action .= "</td>
						  </tr>";
        $table_action .= "<input type=\"hidden\" name=\"single_condition\" value=\"2\">";
    }
    
    // #### - only
    if (($surplus_qty == 0) && ($missing_qty != 0)) {
        $table_action .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_alert_pleaseselect:</td>
								<td class=\"tabletext\">
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action6\" value=\"6\"><label for=\"variance_action6\">$i_InventorySystem_VarianceHandling_Action[6]</label><br>
									<input type=\"radio\" name=\"variance_action\" id=\"variance_action3\" value=\"3\"><label for=\"variance_action3\">$i_InventorySystem_VarianceHandling_Action[3]</label>
									( " . $i_InventorySystem['WriteOffReason'] . ": " . $reason_selection . " )<br>";
        if ($showWriteoffTable)
            $table_action .= "<input type=\"radio\" name=\"variance_action\" id=\"variance_action8\" value=\"8\"><label for=\"variance_action8\">$i_InventorySystem_VarianceHandling_Action[8]</label><br>";
        $table_action .= "</td>
						  </tr>";
        $table_action .= "<input type=\"hidden\" name=\"single_condition\" value=\"3\">";
    }
    
    if ($variance_action == 1) {
        $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID = '".$ItemID."'";
        $arr_tmp_admin_group = $linventory->returnVector($sql);
        if (sizeof($arr_tmp_admin_group) > 0) {
            $admin_group_id = $arr_tmp_admin_group[0];
        }
        
        $sql = "SELECT DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID) AND b.AdminGroupID = '".$admin_group_id."'";
        $arr_handler_result = $linventory->returnArray($sql);
        $handler_selection = getSelectByArray($arr_handler_result, "name=\"targetHandler\"", $targetHandler, 0);
        
        $table_action .= "<tr>
							<td></td>
							<td class=\"tabletext\">
								<table width=\"100%\">
									<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><img src=\"$image_path/{$LAYOUT_SKIN}/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandlingNotice_Handler</td><td class=\"tabletext\">$handler_selection</td></tr>
								</table>
							</td>
						 </tr>
					 	";
    }
    
    if ($variance_action == 2) // Relocate
{
        include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");
        $llocation_ui = new liblocation_ui();
        
        $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID = '".$ItemID."'";
        $arr_tmp_admin_group = $linventory->returnVector($sql);
        if (sizeof($arr_tmp_admin_group) > 0) {
            $admin_group_id = $arr_tmp_admin_group[0];
        }
        
        $sql = "SELECT DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID) WHERE b.AdminGroupID = '".$admin_group_id."'";
        $arr_handler_result = $linventory->returnArray($sql);
        $handler_selection = getSelectByArray($arr_handler_result, "name=\"targetHandler\"", $targetHandler, 0);
        
        $location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetSublocation, "targetSublocation");
        
        $table_action .= "<tr>
							<td></td>
							<td class=\"tabletext\">
								<table width=\"100%\">
									<tr>
										<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" valign=\"top\"><img src=\"$image_path/{$LAYOUT_SKIN}/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandling_Action[2]</td>
										<td class=\"tabletext\">$i_InventorySystem_From: " . intranet_htmlspecialchars($original_location_name) . "<br>
										$i_InventorySystem_To: $location_selection</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><img src=\"$image_path/{$LAYOUT_SKIN}/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandlingNotice_Handler</td>
										<td class=\"tabletext\">$handler_selection</td>
									</tr>
								</table>
							</td>
						 </tr>
					 	";
    }
    
    /*
     * if($variance_action == 3) ## Write Off
     * {
     * $sql = "SELECT ReasonTypeID, ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE";
     * $arr_write_off_reason = $linventory->returnArray($sql,2);
     * $write_off_reason_selection = getSelectByArray($arr_write_off_reason,"name=\"targetWriteOffReason\"");
     * $table_action .= "<tr>
     * <td></td>
     * <td class=\"tabletext\">
     * <table width=\"100%\">
     * <tr>
     * <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><img src=\"$image_path/{$LAYOUT_SKIN}/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandling_Action[3]</td>
     * <td class=\"tabletext\">$i_InventorySystem_Write_Off_Reason: $write_off_reason_selection <br>
     * $i_InventorySystem_Write_Off_Attachment: <input type=\"file\" name=\"write_off_attachment\"></td>
     * </tr>
     * </table>
     * </td>
     * </tr>
     * ";
     * }
     */
}

if ($item_type == ITEM_TYPE_BULK) {
    $this_group_name = $linventory->returnGroupNameByGroupID($GroupID);
    $this_funding_name = $linventory->returnFundingSourceByFundingID($FundingID);
    
    $table_content .= "<tr class=\"tabletop\">";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_LastStocktakeBy . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Setting_StockCheckPeriod . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>";
    $table_content .= "</tr>";
    
    $sql1 = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") as this_location,
				" . $linventory->getInventoryNameByLang("e.") . ",
				IFNULL(" . getNameFieldByLang2("f.") . ",'" . $Lang['General']['UserAccountNotExists'] . "'),
				b.RecordDate,
				CONCAT('+',b.SurplusQty),
				LocBuilding.DisplayOrder as Building_DisplayOrder,
				d.DisplayOrder as Level_DisplayOrder,
				c.DisplayOrder as Room_DisplayOrder,
				b.AdminGroupID
			FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) LEFT JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
			WHERE	
				a.ItemID IN ('".$ItemID."')
				and b.AdminGroupID='".$GroupID."' and b.FundingSourceID='".$FundingID."'
			";
    
    $sql2 = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("e.") . ",
				IFNULL(" . getNameFieldByLang2("f.") . ",'" . $Lang['General']['UserAccountNotExists'] . "'),
				b.RecordDate,
				CONCAT('-',b.MissingQty),
				LocBuilding.DisplayOrder as Building_DisplayOrder,
				d.DisplayOrder as Level_DisplayOrder,
				c.DisplayOrder as Room_DisplayOrder,
				b.AdminGroupID
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_MISSING_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) LEFT JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
		WHERE	
				a.ItemID IN ('".$ItemID."')
				and b.AdminGroupID=$GroupID and b.FundingSourceID='".$FundingID."'
		";
    $sql = $sql1 . " union " . $sql2 . " order by Building_DisplayOrder, Level_DisplayOrder, Room_DisplayOrder";
    $arr_missing_result = $linventory->returnArray($sql);
    if (sizeof($arr_missing_result) > 0) {
        $var_qty_ary = array();
        
        for ($i = 0; $i < sizeof($arr_missing_result); $i ++) {
            list ($miss_location_id, $miss_location_name, $miss_admin_name, $miss_user_name, $miss_date, $missing_qty, $admin_id) = $arr_missing_result[$i];
            $table_content .= "<tr class=\"bulk\">";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($miss_location_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($miss_admin_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($this_funding_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($miss_user_name) . "</td>";
            $table_content .= "<td class=\"tabletext\">$miss_date</td>";
            $font_color = substr($missing_qty, 0, 1) == "+" ? "green" : "red";
            $table_content .= "<td class=\"tabletext\"><font color=\"" . $font_color . "\">$missing_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"6\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"missing_location_id\" value=\"$miss_location_id\">";
            
            // $arr_variance_location[] = $miss_location_id;
            // $var_qty_ary[$miss_location_id]['qty'] = $missing_qty;
            $var_qty_ary[$miss_location_id] = $missing_qty;
        }
    }
    
    // #### start "Re-assign Location Quantity" table
    // if(sizeof($arr_variance_location)>0)
    // {
    // $target_variance = implode(",",$arr_variance_location);
    // }
    $sql = "SELECT 
					a.ItemID, d.BulkItemAdmin, a.LocationID, 
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . "), 
					a.Quantity
				FROM 
					INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION AS b ON (b.LocationID = a.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS c ON (c.LocationLevelID = b.LocationLevelID) INNER JOIN 
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (c.BuildingID = LocBuilding.BuildingID)
					inner join INVENTORY_ITEM_BULK_EXT as d on (d.ItemID=a.ItemID)
				WHERE
					a.ItemID = '".$ItemID."' 
					and a.GroupInCharge='".$GroupID."' and a.FundingSourceID='".$FundingID."'
				order by LocBuilding.DisplayOrder, c.DisplayOrder, b.DisplayOrder
				";
    // a.LocationID IN ($target_variance)
    $arr_result = $linventory->returnArray($sql, 5);
    
    $total_origial_qty = 0;
    $total_new_qty = 0;
    
    if (sizeof($arr_result) > 0) {
        $sql = "SELECT 
					DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " 
				FROM 
					INTRANET_USER AS a 
					INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID) 
				WHERE 
					b.AdminGroupID = '".$GroupID."'
				";
        $arr_handler_result = $linventory->returnArray($sql);
        
        $table_action .= "<tr><td class=\"tabletext\">";
        $table_action .= "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"0\" border=\"0\">";
        $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"11\">$i_InventorySystem_VarianceHandle_ReAssignLocationQuantity</td></tr>";
        $table_action .= "<tr class=\"tabletop\">";
        $table_action .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Location</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">$i_InventorySystem_VarianceHandling_Original_Quantity</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">$i_InventorySystem_Stocktake_StocktakeQty</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">$i_InventorySystem_ItemStatus_Normal</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">$i_InventorySystem_ItemStatus_Damaged</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">$i_InventorySystem_ItemStatus_Repair</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">$i_InventorySystem_VarianceHandling_New_Quantity</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\"><font color=red>*</font>$i_InventorySystem_VarianceHandling_Action[3]</td>";
        // write-off reason
        $table_action .= "<td class=\"tabletopnolink\" width=\"6%\">" . $i_InventorySystem['WriteOffReason'] . "</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Remark</td>";
        $table_action .= "<td class=\"tabletopnolink\" width=\"1%\">$i_InventorySystem_VarianceHandlingNotice_Handler</td>";
        $table_action .= "</tr>";
        
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_id, $admin_group, $location_id, $location_name, $original_qty) = $arr_result[$i];
            
            $arr_variance_location[] = $location_id;
            
            // retrieve stocktake remark
            $sql = "select Remark from INVENTORY_ITEM_BULK_LOG where ItemID=$item_id and LocationID=$location_id and GroupInCharge=$GroupID and FundingSource=$FundingID and Action=" . ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION . " order by RecordID desc limit 1";
            $result = $linventory->returnVector($sql);
            $this_remark = intranet_htmlspecialchars($result[0]);
            
            // eval("\$new_qty_default = \$original_qty ". $var_qty_ary[$location_id]['qty'].";");
            eval("\$new_qty_default = \$original_qty " . $var_qty_ary[$location_id] . ";");
            $handler_selection = getSelectByArray($arr_handler_result, "name=\"targetHandler_$location_id\"", $targetHandler, 0);
            
            $table_action .= "<tr>";
            $table_action .= "<td class=\"tabletext\" nowrap>" . intranet_htmlspecialchars($location_name) . "</td>";
            $table_action .= "<td class=\"tabletext\"><span id='original_qty_$location_id'>$original_qty</span></td>";
            $table_action .= "<td class=\"tabletext\">$new_qty_default</td>";
            
            // normal / damaged / repairing
            $table_action .= "<td class=\"tabletext\"><input type=\"text\" name=\"new_qty_$location_id\" 	onChange='update_this_loction_qty($location_id)' class=\"textboxnum2\" value=\"" . $original_qty . "\"></td>";
            $table_action .= "<td class=\"tabletext\"><input type=\"text\" name=\"new_damage_$location_id\" onChange='update_this_loction_qty($location_id)' class=\"textboxnum2\" value=\"0\"></td>";
            $table_action .= "<td class=\"tabletext\"><input type=\"text\" name=\"new_repair_$location_id\" onChange='update_this_loction_qty($location_id)' class=\"textboxnum2\" value=\"0\"></td>";
            
            $table_action .= "<td class=\"tabletext\"><span id='new_qty_span_$location_id'>$original_qty</span></td>";
            
            // write-off
            $table_action .= "<td class=\"tabletext\"><input type=\"text\" name=\"new_write_off_$location_id\" onChange='update_this_loction_qty($location_id)' class=\"textboxnum2\" value=\"0\"></td>";
            
            // write-off reason
            $table_action .= "<td class=\"tabletext\">" . getSelectByArray($arr_write_off_reason, " name=\"new_write_off_reason_$location_id\" ", "", 0, 1) . "</td>";
            
            $table_action .= "<td class='tabletext'><input type='text' class='textboxtext' name='remark_$location_id' value='$this_remark'></td>";
            $table_action .= "<td class=\"tabletext\">$handler_selection</td>";
            $table_action .= "</tr>";
            
            $total_origial_qty = $total_origial_qty + $original_qty;
            $total_new_qty = $total_new_qty + $new_qty_default;
        }
        
        $table_action .= "<tr>";
        $table_action .= "<td class='dotline' colspan='11'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='10' height='5'></td>";
        $table_action .= "</tr>";
        
        $table_action .= "<tr>";
        $table_action .= "<td>$list_total</td>";
        $table_action .= "<td><span id='span_original_qty'>$total_origial_qty</span></td>";
        $table_action .= "<td><span id='span_new_qty'>$total_new_qty</span></td>";
        $table_action .= "<td>&nbsp;</td>";
        $table_action .= "<td>&nbsp;</td>";
        $table_action .= "<td>&nbsp;</td>";
        $table_action .= "<td><span id='span_total_new_qty'>$total_origial_qty</span></td>";
        $table_action .= "<td>&nbsp;</td>";
        $table_action .= "<td>&nbsp;</td>";
        $table_action .= "</tr>";
        
        $table_action .= "</table></td></tr>";
    }
    // $total_variance = $total_surplus_qty - $total_missing_qty;
    // $total_handling_qty = $total_origial_qty + ($total_surplus_qty - $total_missing_qty);
    
    if (sizeof($arr_variance_location) > 0) {
        $variance_list = implode(",", $arr_variance_location);
        $table_action .= "<input type=\"hidden\" name=\"variance_location_list\" value=\"$variance_list\">";
    }
    
    // $table_action .= "<input type=\"hidden\" name=\"total_surplus_qty\" value=\"$total_surplus_qty\">";
    // $table_action .= "<input type=\"hidden\" name=\"total_missing_qty\" value=\"$total_missing_qty\">";
    
    // $table_action .= "<tr><td><table border=\"0\" id=\"table_alert\" cellpadding=\"0\" cellspacing=\"0\" >";
    // $table_action .= "</table></td></tr>";
    
    // if($total_variance == 0)
    // {
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // ".$linterface->GET_BTN("$i_InventorySystem_VarianceHandle_ButtonCalculate","Button","calVariance()","Calculate","")."
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalAvaliableQuantity: <input type=\"text\" name=\"total_normal_summary\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"$total_handling_qty\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalAssignedQuantity: <input type=\"text\" name=\"total_assigned_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalRemainingQuantity: <input type=\"text\" name=\"total_remaining_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalIgnoreQuantity: <input type=\"text\" name=\"total_ignore_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // }
    
    // if($total_variance > 0)
    // {
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // ".$linterface->GET_BTN("$i_InventorySystem_VarianceHandle_ButtonCalculate","Button","calVariance()","Calculate","")."
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalAvaliableQuantity: <input type=\"text\" name=\"total_normal_summary\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"$total_handling_qty\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalAssignedQuantity: <input type=\"text\" name=\"total_assigned_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalRemainingQuantity: <input type=\"text\" name=\"total_remaining_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalIgnoreQuantity: <input type=\"text\" name=\"total_ignore_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // }
    // if($total_variance < 0)
    // {
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // ".$linterface->GET_BTN("$i_InventorySystem_VarianceHandle_ButtonCalculate","Button","calVariance()","Calculate","")."
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalAvaliableQuantity: <input type=\"text\" name=\"total_normal_summary\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"$total_handling_qty\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalAssignedQuantity: <input type=\"text\" name=\"total_assigned_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalRemainingQuantity: <input type=\"text\" name=\"total_remaining_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // $table_summary .= "<tr class=\"bulk\"><td align=\"right\" class=\"tabletext\">
    // <font class=\"sectiontitle\">$i_InventorySystem_VarianceHandle_TotalWriteOffQuantity: <input type=\"text\" name=\"total_write_off_qty\" class=\"inventory_bulknumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\">
    // </td></tr>";
    // }
}

?>

<script language="javascript">
var arr_write_off_location_id = new Array();
var arr_write_off_location_name = new Array();
var calculate_click = 0;

<? 
/*
    * ?>
    * function createWriteOffTable()
    * {
    * var table = document.getElementById("write_off");
    * var curr_total_row = table.rows.length;
    * if(curr_total_row > 0)
    * {
    * for(i=0; i<curr_total_row; i++)
    * {
    * var j=table.parentNode.parentNode.rowIndex;
    * table.deleteRow(i);
    * }
    * }
    * if (document.all)
    * {
    * for(i=0; i<arr_write_off_location_id.length; i++)
    * {
    * var row = table.insertRow(i);
    * var write_off_location_id = arr_write_off_location_id[i];
    * var max_row_index = arr_write_off_location_id.length-1;
    *
    * if(write_off_location_id != '')
    * {
    * var cell1 = row.insertCell(0);
    * var cell2 = row.insertCell(1);
    * var variance_location_name = arr_write_off_location_name[write_off_location_id];
    *
    * if(i==0)
    * {
    * x = '<font class="tabletext"><b><?=$i_InventorySystem_VarianceHandle_InputWriteOffQuantity;?>:</b></font><br>';
    * x += '<font class="tabletext">'+variance_location_name+'</font>';
    * }
    * else
    * {
    * x = '<br><font class="tabletext">'+variance_location_name+'</font>';
    * }
    * cell1.innerHTML = x;
    * cell1.width = "20%";
    *
    * x = '<br><input class="textboxnum" type="text" name="write_off_qty_'+write_off_location_id+'">';
    * x += '<input type="hidden" name="hidden_write_off_'+write_off_location_id+'">';
    * cell2.innerHTML = x;
    * }
    * }
    * }
    * }
    * <?
    */
?>

function checkForm()
{
	<?
if ($item_type == ITEM_TYPE_SINGLE) {
    ?>
		var action_selected = 0;
		var passed = 0;
		var condition = document.form1.single_condition.value;
		
		if(condition == 1)
		{
			if(document.form1.variance_action[0].checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetHandler,"<?=$i_InventorySystem_VarianceHandle_SelectHandlerWarning;?>",0))
				{
					passed = 1;
				}
				else
				{
					return false;
				}
			}
			if(document.form1.variance_action[1].checked == true)
			{
				action_selected = 1;
				//if(check_select(document.form1.targetLocation,"<?=$i_InventorySystem_Input_Item_LocationSelection_Warning;?>",0))
				//{
					if(check_select(document.form1.targetSublocation,"<?=$i_InventorySystem_Input_Item_LocationSelection_Warning;?>",0))
					{
						if(check_select(document.form1.targetHandler,"<?=$i_InventorySystem_VarianceHandle_SelectHandlerWarning;?>",0))
						{
							passed = 1;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
// 				}
// 				else
// 				{
// 					return false;
// 				}
			}
			if(document.form1.variance_action[2].checked == true)
			{
				action_selected = 1;
				passed = 1;
//				action_selected = 1;
//				if(check_select(document.form1.targetWriteOffReason,"<?=$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning;?>",0))
//				{
//					passed = 1;
//				}
//				else
//				{
//					return false;
//				}
			}
			<?if($showWriteoffTable){?>
			if(document.form1.variance_action[3].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			<?}?>
		}
		
		if(condition == 2)
		{
			if(document.form1.variance_action[0].checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetHandler,"<?=$i_InventorySystem_VarianceHandle_SelectHandlerWarning;?>",0))
				{
					passed = 1;
				}
				else
				{
					return false;
				}
			}
			if(document.form1.variance_action[1].checked == true)
			{
				action_selected = 1;
				//if(check_select(document.form1.targetLocation,"<?=$i_InventorySystem_Input_Item_LocationSelection_Warning;?>",0))
				//{
					if(check_select(document.form1.targetSublocation,"<?=$i_InventorySystem_Input_Item_LocationSelection_Warning;?>",0))
					{
						if(check_select(document.form1.targetHandler,"<?=$i_InventorySystem_VarianceHandle_SelectHandlerWarning;?>",0))
						{
							passed = 1;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				//}
				//else
				//{
				//	return false;
				//}
			}
			<?if($showWriteoffTable){?>
			if(document.form1.variance_action[2].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			<?}?>
		}
		
		if(condition == 3)
		{
			if(document.form1.variance_action[0].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			
			if(document.form1.variance_action[1].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			<?if($showWriteoffTable){?>
			if(document.form1.variance_action[2].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			<?}?>
			/*
			if(document.form1.variance_action.checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetWriteOffReason,"<?=$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning;?>",0))
				{
					passed = 1;
				}
				else
				{
					return false;
				}
			}
			*/
		}
	
		if(action_selected == 1)
		{
			if(passed == 1)
			{
				document.form1.action = "variance_edit_update.php";
				return true;
			}
		}
		else
		{
			alert("<?=$i_InventorySystem_VarianceHandle_SelectActionWarning;?>");
			return false;
		}
	
	<?
}
?>
	
	<?
if ($item_type == ITEM_TYPE_BULK) {
    ?>
		var x=0;
		var non_select_handler = 0;
		var span_new_qty = parseInt(document.getElementById('new_qty_span_<?=$location_id?>').innerHTML);
		<?
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($item_id, $admin_group, $location_id, $location_name, $original_qty) = $arr_result[$i];
        ?>
			row_new = parseInt(document.getElementById('new_qty_span_<?=$location_id?>').innerHTML);
			x = x + row_new;
			
			location_id = <?=$location_id;?>;
			var obj2 = eval("document.form1.targetHandler_"+location_id);
			if(Trim(obj2.options[obj2.selectedIndex].value)=='' && row_new>0)
			{
				non_select_handler++;
		    }
		    
		    var row_writeoff = parseInt(document.form1.new_write_off_<?=$location_id?>.value);
			if(row_writeoff > row_new){
				alert("<?=$i_InventorySystem_UpdateItemRequestWriteOffQty_Warning?>");
				return false;
			}
		<? } ?>
		
		if(x != <?=$total_origial_qty?>)
		{
			update_total_new_qty();
			alert("<?=$Lang['eInventory']['NewQtyNotMatchOriginalQty']?>");
			//if(!confirm("<?=$Lang['eInventory']['NewQtyNotMatchOriginalQty'].'\n'.$Lang['eInventory']['Confirm']['Submit']?>"))
				return false;
		}
		
// 		if(x ==0)
// 		{
// 			alert("<?=$Lang['eInventory']['NewQtyCannotZero']?>");
// 			return false;
// 		}	
	
		if(non_select_handler > 0)
		{
			alert("<?=$i_InventorySystem_VarianceHandle_SelectHandlerWarning;?>");
			return false;
		}
		
		document.form1.action = "variance_edit_update.php";
		return true;
	<?
}
?>
	
	
}

<? if($item_type == ITEM_TYPE_BULK) {?>
function update_this_loction_qty(this_location)
{
	var obj=document.form1;
	
	new_normal = eval("obj.new_qty_"+ this_location +".value");
	new_damage = eval("obj.new_damage_"+ this_location +".value");
	new_repair = eval("obj.new_repair_"+ this_location +".value");
	new_write_off = eval("obj.new_write_off_"+ this_location +".value");
	
	if(isNaN(new_normal) || new_normal<0 || Trim(new_normal)=='')
	{
		new_normal = 0;
		eval("obj.new_qty_"+ this_location +".value=0");
	}
	
	if(isNaN(new_damage) || new_damage<0 || Trim(new_damage)=='')
	{
		new_damage = 0;
		eval("obj.new_damage_"+ this_location +".value=0");
	}
	
	if(isNaN(new_repair) || new_repair<0 || Trim(new_repair)=='')
	{
		new_repair = 0;
		eval("obj.new_repair_"+ this_location +".value=0");
	}
	
	if(isNaN(new_write_off) || new_write_off<0 || Trim(new_write_off)=='')
	{
		new_write_off = 0;
		eval("obj.new_write_off_"+ this_location +".value=0");
	}
	
	var total_new = parseInt(new_normal) + parseInt(new_damage) + parseInt(new_repair)/* + parseInt(new_write_off)*/;
	//var total_new = parseInt(new_normal) + parseInt(new_damage) + parseInt(new_repair);
	eval("document.getElementById('new_qty_span_"+ this_location +"').innerHTML = total_new;");
	
	update_total_new_qty();
}

function click_reset()
{
	var obj=document.form1;
	obj.reset();
	
	var x=0;
	<?
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($item_id, $admin_group, $location_id, $location_name, $original_qty) = $arr_result[$i];
        ?>
	update_this_loction_qty(<?=$location_id?>);
	<? } ?>
}

<? 
/*
        * ?>
        * function reset_new_qty()
        * {
        * var obj=document.form1;
        * obj.reset();
        *
        * <?
        * for($i=0; $i<sizeof($arr_result); $i++)
        * {
        * list($item_id, $admin_group, $location_id, $location_name, $original_qty) = $arr_result[$i];
        * ?>
        *
        * new_normal = obj.new_qty_<?=$location_id?>.value;
        * new_damage = obj.new_damage_<?=$location_id?>.value;
        * new_repair = obj.new_repair_<?=$location_id?>.value;
        * var total_new = parseInt(new_normal) + parseInt(new_damage) + parseInt(new_repair);
        * document.getElementById('new_qty_span_<?=$location_id?>').innerHTML = total_new;
        *
        * <? } ?>
        *
        * update_total_new_qty();
        * }
        * <?
        */
    ?>

function update_total_new_qty()
{
	var x=0;
	<?
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($item_id, $admin_group, $location_id, $location_name, $original_qty) = $arr_result[$i];
        ?>
	row_new = parseInt(document.getElementById('new_qty_span_<?=$location_id?>').innerHTML);
	x = x + row_new;
	<? } ?>
	
// 	var o = parseInt(document.getElementById('span_original_qty').innerHTML);
	
	if(x != <?=$total_origial_qty?>)
	{
		document.getElementById('span_total_new_qty').innerHTML = "<font color='red'>"+ x + "</font>";
		document.getElementById('span_original_qty').innerHTML = "<font color='red'>"+ <?=$total_origial_qty?> + "</font>";
	}
	else
	{
		document.getElementById('span_total_new_qty').innerHTML = x;
		document.getElementById('span_original_qty').innerHTML = <?=$total_origial_qty?>;
	}
}

<? } ?>

</script>

<br>
<form name="form1" action="" method="post" onSubmit="return checkForm()">

	<table id="html_body_frame" width="96%" border="0" cellspacing="0"
		cellpadding="5" align="center">
<?=$infobar1?>
</table>

	<table width="96%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$infobar2?>
</table>

	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content?>
</table>
	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?=$image_path?>/<?=$LAYOUT_SKIN;?>/10x10.gif" width="10"
				height="5"></td>
		</tr>
	</table>
	<br>
	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_action?>
<tr>
			<td colspan="5">
				<table id="write_off" border="0" width="100%" cellpadding="2"
					cellspacing="0" align="center">
				</table>
			</td>
		</tr>
<?if($item_type == ITEM_TYPE_BULK){?>
<tr>
			<td colspan="5" class="tabletextremark">
		<?=$Lang['eInventory']['TotalQtyMustMatchOriginalQty']?><br /> <font
				color='red'>*</font><?=$Lang['eInventory']['RequestWriteoffRemarks']?>
	</td>
		</tr>
<?}?>
</table>

<?if($showWriteoffTable){?>
<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td><a href="javascript:void(0);"
				onclick="$('#writeoff_table').toggle();"><?=$Lang['eInventory']['ShowHideWriteoffRecord']?></a></td>
		</tr>
	</table>
	<table id="writeoff_table" style="display: none" border="0" width="96%"
		cellpadding="5" cellspacing="0" align="center">
<?=$writeoff_table_content?>
</table>
<?}?>

<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit");?>
		<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:click_reset();");?>
		<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='outstanding_items.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");?>
</td>
		</tr>
	</table>

	<input type="hidden" name="ItemID" value="<?=$ItemID?>"> <input
		type="hidden" name="GroupID" value="<?=$GroupID?>"> <input
		type="hidden" name="FundingID" value="<?=$FundingID?>">

</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
