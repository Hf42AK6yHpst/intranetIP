<?php
// modifying :
/**
 * **************************** Change Log **************************************************
 * Date: 2019-05-13 (Henry): Security fix: SQL without quote
 * 2018-02-22 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_StocktakeVarianceHandling";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem_StocktakeVarianceHandling,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Generate Page Info #
$sql = "SELECT 
				CONCAT(" . $linventory->getInventoryItemNameByLang("") . ",' (',ItemCode,')')
		FROM 
				INVENTORY_ITEM
		WHERE
				ItemID = '".$ItemID."'";

$result = $linventory->returnArray($sql, 1);

$temp[] = array(
    $result[0][0],
    ""
);

$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION2($temp[0][0]) . "</td></tr>";
// End #

// get item type #
$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$ItemID."'";
$arr_item_type = $linventory->returnVector($sql);
$item_type = $arr_item_type[0];
// end #

if ($item_type == ITEM_TYPE_SINGLE) {
    $table_content .= "<tr class=\"tabletop\">";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_ExpectedLocation . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_LastStocktakeBy . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Setting_StockCheckPeriod . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>";
    $table_content .= "</tr>";
    
    $sql = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				b.AdminGroupID,
				" . $linventory->getInventoryNameByLang("e.") . ",
				b.PersonInCharge,
				" . getNameFieldByLang2("f.") . ",
				b.RecordDate,
				CONCAT('+',b.SurplusQty)
			FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
			WHERE	
				a.ItemID IN ('".$ItemID."')
			";
    $arr_surplus_result = $linventory->returnArray($sql, 8);
    
    $sql = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				b.AdminGroupID,
				" . $linventory->getInventoryNameByLang("e.") . ",
				b.PersonInCharge,
				" . getNameFieldByLang2("f.") . ",
				b.RecordDate,
				CONCAT('-',b.MissingQty)
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_MISSING_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
		WHERE	
				a.ItemID IN ('".$ItemID."')
		";
    $arr_missing_result = $linventory->returnArray($sql, 8);
    
    $sql = "SELECT
				b.LocationID, 
				CONCAT(" . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
			FROM
				INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
				INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
			WHERE
				a.ItemID = '".$ItemID."'
			";
    $arr_original_location = $linventory->returnArray($sql, 2);
    list ($original_location_id, $original_location_name) = $arr_original_location[0];
    
    if (sizeof($arr_surplus_result) > 0) {
        for ($i = 0; $i < sizeof($arr_surplus_result); $i ++) {
            list ($surplus_location_id, $surplus_location_name, $surplus_admin_id, $surplus_admin_name, $surplus_user_id, $surplus_user_name, $surplus_date, $surplus_qty) = $arr_surplus_result[$i];
            
            $table_content .= "<tr class=\"single\">";
            $table_content .= "<td class=\"tabletext\">$surplus_location_name</td>";
            $table_content .= "<td class=\"tabletext\">$original_location_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_admin_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_user_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_date</td>";
            $table_content .= "<td class=\"tabletext\"><font color=\"green\">$surplus_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"6\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"surplus_location_id\" value=\"$surplus_location_id\">";
        }
    }
    if (sizeof($arr_missing_result) > 0) {
        for ($i = 0; $i < sizeof($arr_missing_result); $i ++) {
            list ($miss_location_id, $miss_location_name, $miss_admin_id, $miss_admin_name, $miss_user_id, $miss_user_name, $miss_date, $missing_qty) = $arr_missing_result[$i];
            
            $table_content .= "<tr class=\"single\">";
            $table_content .= "<td class=\"tabletext\">$miss_location_name</td>";
            $table_content .= "<td class=\"tabletext\">$original_location_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_admin_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_user_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_date</td>";
            $table_content .= "<td class=\"tabletext\"><font color=\"red\">$missing_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"6\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"missing_location_id\" value=\"$miss_location_id\">";
        }
    }
    
    $new_location_selection = getSelectByArray($arr_new_location, "name=\"targetNewLocation\"", 0, 0, 1);
    $old_location_selection = getSelectByArray($arr_old_location, "name=\"targetOldLocation\"", 0, 0, 1);
    $write_off_location = getSelectByArray($arr_old_location, "name=\"targetWriteOffLocation\"", 0, 0, 1);
    
    switch ($variance_action) {
        case 1:
            $checked1 = "CHECKED";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            break;
        case 2:
            $checked1 = "";
            $checked2 = "CHECKED";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            break;
        case 3:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = "CHECKED";
            $checked4 = " ";
            $checked5 = " ";
            break;
        case 4:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = "CHECKED";
            $checked5 = " ";
            break;
        case 5:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = "CHECKED";
            break;
        default:
            $checked1 = " ";
            $checked2 = " ";
            $checked3 = " ";
            $checked4 = " ";
            $checked5 = " ";
            break;
    }
    
    if (($surplus_qty != 0) && ($missing_qty != 0)) {
        $table_action .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_alert_pleaseselect:</td>
								<td class=\"tabletext\">
									<input type=\"radio\" name=\"variance_action\" value=\"1\" onClick=\"this.form.submit();\" $checked1>$i_InventorySystem_VarianceHandling_Action[1]<br>
									<input type=\"radio\" name=\"variance_action\" value=\"2\" onClick=\"this.form.submit();\" $checked2>$i_InventorySystem_VarianceHandling_Action[2]<br>
									<input type=\"radio\" name=\"variance_action\" value=\"3\" onClick=\"this.form.submit();\" $checked3>$i_InventorySystem_VarianceHandling_Action[3]<br>
								</td>
						  </tr>";
        $table_action .= "<input type=\"hidden\" name=\"single_condition\" value=\"1\">";
    }
    if (($surplus_qty != 0) && ($missing_qty == 0)) {
        $table_action .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_alert_pleaseselect:</td>
								<td class=\"tabletext\">
									<input type=\"radio\" name=\"variance_action\" value=\"4\" onClick=\"this.form.submit();\" $checked4>$i_InventorySystem_VarianceHandling_Action[4]<br>
									<input type=\"radio\" name=\"variance_action\" value=\"5\" onClick=\"this.form.submit();\" $checked5>$i_InventorySystem_VarianceHandling_Action[5]<br>
								</td>
						  </tr>";
        $table_action .= "<input type=\"hidden\" name=\"single_condition\" value=\"2\">";
    }
    if (($surplus_qty == 0) && ($missing_qty != 0)) {
        
        $table_action .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_alert_pleaseselect:</td>
								<td class=\"tabletext\">
									<input type=\"radio\" name=\"variance_action\" value=\"3\" onClick=\"this.form.submit();\" $checked3>$i_InventorySystem_VarianceHandling_Action[3]<br>
								</td>
						  </tr>";
        $table_action .= "<input type=\"hidden\" name=\"single_condition\" value=\"3\">";
    }
    
    if ($variance_action == 1) {
        $sql = "SELECT DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID)";
        $arr_handler_result = $linventory->returnArray($sql);
        $handler_selection = getSelectByArray($arr_handler_result, "name=\"targetHandler\"", $targetHandler, 0);
        
        $table_action .= "<tr>
							<td></td>
							<td class=\"tabletext\">
								<table width=\"100%\">
									<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><img src=\"$image_path/2007a/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandlingNotice_Handler</td><td class=\"tabletext\">$handler_selection</td></tr>
								</table>
							</td>
						 </tr>
					 	";
    }
    if ($variance_action == 2) {
        $sql = "SELECT DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID)";
        $arr_handler_result = $linventory->returnArray($sql);
        $handler_selection = getSelectByArray($arr_handler_result, "name=\"targetHandler\"", $targetHandler, 0);
        
        $sql = "SELECT LocationLevelID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION_LEVEL";
        $arr_location = $linventory->returnArray($sql, 2);
        $location_selection = getSelectByArray($arr_location, "name=\"targetLocation\" onChange=\"this.form.submit();\"", $targetLocation, 0, 0);
        if ($targetLocation != "") {
            $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = $targetLocation";
            $arr_sub_location = $linventory->returnArray($sql, 2);
            $sub_location_selection = getSelectByArray($arr_sub_location, "name=\"targetSublocation\"");
        }
        
        $table_action .= "<tr>
							<td></td>
							<td class=\"tabletext\">
								<table width=\"100%\">
									<tr>
										<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" valign=\"top\"><img src=\"$image_path/2007a/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandling_Action[2]</td>
										<td class=\"tabletext\">$i_InventorySystem_From: $original_location_name<br>
										$i_InventorySystem_To: $location_selection $sub_location_selection</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><img src=\"$image_path/2007a/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandlingNotice_Handler</td>
										<td class=\"tabletext\">$handler_selection</td>
									</tr>
								</table>
							</td>
						 </tr>
					 	";
    }
    if ($variance_action == 3) {
        $sql = "SELECT ReasonTypeID, " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE";
        $arr_write_off_reason = $linventory->returnArray($sql, 2);
        $write_off_reason_selection = getSelectByArray($arr_write_off_reason, "name=\"targetWriteOffReason\"");
        $table_action .= "<tr>
							<td></td>
							<td class=\"tabletext\">
								<table width=\"100%\">
									<tr>
										<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><img src=\"$image_path/2007a/icon_nextstep.gif\"> $i_InventorySystem_VarianceHandling_Action[3]</td>
										<td class=\"tabletext\">$i_InventorySystem_Write_Off_Reason: $write_off_reason_selection <br>
										$i_InventorySystem_Write_Off_Attachment: <input type=\"file\" name=\"write_off_attachment\"></td>
									</tr>
								</table>
							</td>
						 </tr>
					 	";
    }
}

if ($item_type == ITEM_TYPE_BULK) {
    $table_content .= "<tr class=\"tabletop\">";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Stocktake_LastStocktakeBy . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Setting_StockCheckPeriod . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>";
    $table_content .= "</tr>";
    
    $sql = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				b.AdminGroupID,
				" . $linventory->getInventoryNameByLang("e.") . ",
				b.PersonInCharge,
				" . getNameFieldByLang2("f.") . ",
				b.RecordDate,
				CONCAT('+',b.SurplusQty)
			FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
			WHERE	
				a.ItemID IN ('".$ItemID."')
			";
    $arr_surplus_result = $linventory->returnArray($sql, 8);
    
    $sql = "SELECT 
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				b.AdminGroupID,
				" . $linventory->getInventoryNameByLang("e.") . ",
				b.PersonInCharge,
				" . getNameFieldByLang2("f.") . ",
				b.RecordDate,
				CONCAT('-',b.MissingQty)
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_MISSING_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
		WHERE	
				a.ItemID IN ('".$ItemID."')
		";
    $arr_missing_result = $linventory->returnArray($sql, 8);
    if (sizeof($arr_surplus_result) > 0) {
        for ($i = 0; $i < sizeof($arr_surplus_result); $i ++) {
            list ($surplus_location_id, $surplus_location_name, $surplus_admin_id, $surplus_admin_name, $surplus_user_id, $surplus_user_name, $surplus_date, $surplus_qty) = $arr_surplus_result[$i];
            $table_content .= "<tr class=\"bulk\">";
            $table_content .= "<td class=\"tabletext\">$surplus_location_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_admin_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_user_name</td>";
            $table_content .= "<td class=\"tabletext\">$surplus_date</td>";
            $table_content .= "<td class=\"tabletext\"><font color=\"green\">$surplus_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"5\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"surplus_location_id\" value=\"$surplus_location_id\">";
            
            $total_surplus_qty = $total_surplus_qty + abs($surplus_qty);
            $arr_surplus_location[$i]["id"] = $surplus_location_id;
            $arr_surplus_location[$i]["name"] = $surplus_location_name;
            $arr_surplus_location[$i]["qty"] = abs($surplus_qty);
            $arr_surplus_location_id[] = $surplus_location_id;
        }
    }
    if (sizeof($arr_missing_result) > 0) {
        for ($i = 0; $i < sizeof($arr_missing_result); $i ++) {
            list ($miss_location_id, $miss_location_name, $miss_admin_id, $miss_admin_name, $miss_user_id, $miss_user_name, $miss_date, $missing_qty) = $arr_missing_result[$i];
            $table_content .= "<tr class=\"bulk\">";
            $table_content .= "<td class=\"tabletext\">$miss_location_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_admin_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_user_name</td>";
            $table_content .= "<td class=\"tabletext\">$miss_date</td>";
            $table_content .= "<td class=\"tabletext\"><font color=\"red\">$missing_qty</font></td>";
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"5\"></td></tr>";
            $table_content .= "<input type=\"hidden\" name=\"missing_location_id\" value=\"$miss_location_id\">";
            
            $total_missing_qty = $total_missing_qty + abs($missing_qty);
            $arr_missing_location[$i]["id"] = $miss_location_id;
            $arr_missing_location[$i]["name"] = $miss_location_name;
            $arr_missing_location[$i]["qty"] = abs($missing_qty);
            $arr_missing_location_id[] = $miss_location_id;
        }
    }
    // echo $total_surplus_qty." ".$total_missing_qty;
    
    if ($total_surplus_qty == "")
        $total_surplus_qty = 0;
    if ($total_missing_qty == "")
        $total_missing_qty = 0;
    
    $total_diff_qty = $total_surplus_qty - $total_missing_qty;
    $special_handling_qty = abs($total_diff_qty);
    
    if ($total_surplus_qty > $total_missing_qty)
        $normal_handling_qty = $total_missing_qty;
    else
        $normal_handling_qty = $total_surplus_qty;
    
    if (($total_surplus_qty != 0) && ($total_missing_qty == 0)) {
        // Surplus handling #
        $table_action .= "<tr><td class=\"tabletext\">";
        $table_action .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
        $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"3\">Surplus Handling</td></tr>";
        $table_action .= "<tr class=\"tabletop\">";
        $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
        $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandling_AddAsNewItemQty</td>";
        $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandling_IgnoreQty</td></tr>";
        
        for ($i = 0; $i < sizeof($arr_surplus_location); $i ++) {
            $location_id = $arr_surplus_location[$i]["id"];
            $location_name = $arr_surplus_location[$i]["name"];
            
            $table_action .= "<tr>";
            $table_action .= "<td class=\"tabletext\">$location_name</td>";
            $table_action .= "<td><input type=\"text\" name=\"surplus_qty_$location_id\" class=\"textboxnum\" value=\"0\" onChange=\"calcVariance()\"></td>";
            $table_action .= "<td><input type=\"text\" name=\"ignore_qty_$location_id\" class=\"textboxnum\" value=\"0\" onChange=\"calcVariance()\"></td>";
            $table_action .= "</tr>";
            $table_action .= "<tr class=\"inventory_row_underline\"><td colspan=\"3\"></td></tr>";
        }
        $table_action .= "</table>";
        $table_action .= "</tr></td>";
    }
    if (($total_surplus_qty == 0) && ($total_missing_qty != 0)) {
        // Auto Write-off, reason = Missing #
        $table_action .= "<tr><td class=\"tabletext\">";
        $table_action .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
        $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"2\">$i_InventorySystem_RequestWriteOff</td></tr>";
        $table_action .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_From</td>";
        $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td></tr>";
        for ($i = 0; $i < sizeof($arr_missing_location); $i ++) {
            $location_id = $arr_missing_location[$i]["id"];
            $location_name = $arr_missing_location[$i]["name"];
            $location_missing_qty = $arr_missing_location[$i]["qty"];
            
            $table_action .= "<tr>";
            $table_action .= "<td class=\"tabletext\">$location_name</td>";
            $table_action .= "<td><input type=\"text\" name=\"write_off_qty_$location_id\" class=\"textboxnum\"></td>";
            $table_action .= "</tr>";
            $table_action .= "<tr class=\"inventory_row_underline\"><td colspan=\"2\"></td></tr>";
        }
        $table_action .= "</table>";
        $table_action .= "</tr></td>";
    }
    if (($total_surplus_qty != 0) && ($total_missing_qty != 0)) {
        // Move back #
        $table_action .= "<tr><td class=\"tabletext\">";
        $table_action .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
        $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"5\">$i_InventorySystem_VarianceHandling_Action[1]</td></tr>";
        $table_action .= "<tr class=\"tabletop\">
								<td class=\"tabletopnolink\">$i_InventorySystem_From</td>
								<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>
								<td class=\"tabletopnolink\">$i_InventorySystem_To</td>
								<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>
								<td class=\"tabletopnolink\">Handler</td>
						  </tr>";
        
        for ($i = 0; $i < sizeof($arr_surplus_location); $i ++) {
            $surplus_location_id = $arr_surplus_location[$i]["id"];
            $surplus_location = $arr_surplus_location[$i]["name"];
            $surplus_location_qty = $arr_surplus_location[$i]["qty"];
            $table_action .= "<tr>";
            $table_action .= "<td class=\"tabletext\">$surplus_location</td>";
            $table_action .= "<td class=\"tabletext\">$surplus_location_qty</td>";
            for ($j = 0; $j < sizeof($arr_missing_location); $j ++) {
                $missing_location_id = $arr_missing_location[$j]["id"];
                $missing_location = $arr_missing_location[$j]["name"];
                if ($j > 0) {
                    $table_action .= "<tr>";
                    $table_action .= "<td></td>";
                    $table_action .= "<td></td>";
                }
                
                $sql = "SELECT DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID) WHERE b.UserID != '".$UserID."' ORDER BY a.EnglishName";
                $arr_target_handler = $linventory->returnArray($sql, 2);
                $handler_selection = getSelectByArray($arr_target_handler, "name=\"target_handler_" . $surplus_location_id . "_" . $missing_location_id . "\"");
                
                $table_action .= "<td class=\"tabletext\">$missing_location</td>";
                $table_action .= "<td class=\"tabletext\"><input type=\"text\" name=\"move_to_qty_" . $surplus_location_id . "_" . $missing_location_id . "\" class=\"textboxnum\" onChange=\"calcVariance()\" value=\"0\"></td>";
                $table_action .= "<td class=\"tabletext\">$handler_selection</td>";
                if ($j > 0) {
                    $table_action .= "</tr>";
                }
            }
            $table_action .= "</tr>";
            $table_action .= "<tr class=\"inventory_row_underline\"><td colspan=\"5\"></td></tr>";
        }
        $table_action .= "</table>";
        $table_action .= "</td></tr>";
        $table_action .= "<tr><td colspan=\"5\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
        $table_action .= "<tr height=\"10px\"><td colspan=\"5\"></td></tr>";
        
        // Relocation #
        $table_action .= "<tr><td class=\"tabletext\">";
        $table_action .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
        $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"5\">$i_InventorySystem_VarianceHandling_Action[2]</td></tr>";
        $table_action .= "<tr class=\"tabletop\">
								<td class=\"tabletopnolink\">$i_InventorySystem_From</td>
								<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>
								<td class=\"tabletopnolink\">$i_InventorySystem_To</td>
								<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>
								<td class=\"tabletopnolink\">Handler</td>
							</tr>";
        for ($i = 0; $i < sizeof($arr_missing_location); $i ++) {
            $missing_location_id = $arr_missing_location[$i]["id"];
            $missing_location = $arr_missing_location[$i]["name"];
            $missing_location_qty = $arr_missing_location[$i]["qty"];
            $table_action .= "<tr>";
            $table_action .= "<td class=\"tabletext\">$missing_location</td>";
            $table_action .= "<td class=\"tabletext\">$missing_location_qty</td>";
            for ($j = 0; $j < sizeof($arr_surplus_location); $j ++) {
                $surplus_location_id = $arr_surplus_location[$j]["id"];
                $surplus_location = $arr_surplus_location[$j]["name"];
                if ($j > 0) {
                    $table_action .= "<tr>";
                    $table_action .= "<td></td>";
                    $table_action .= "<td></td>";
                }
                
                $sql = "SELECT DISTINCT a.UserID, " . getNameFieldByLang2("a.") . " FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID) WHERE b.UserID != '".$UserID."' ORDER BY a.EnglishName";
                $arr_target_handler = $linventory->returnArray($sql, 2);
                $handler_selection = getSelectByArray($arr_target_handler, "name=\"target_handler_" . $missing_location_id . "_" . $surplus_location_id . "\"");
                
                $table_action .= "<td class=\"tabletext\">$surplus_location</td>";
                $table_action .= "<td class=\"tabletext\"><input type=\"text\" name=\"relocate_to_qty_" . $missing_location_id . "_" . $surplus_location_id . "\" class=\"textboxnum\" value=\"0\" onChange=\"calcVariance()\"></td>";
                $table_action .= "<td class=\"tabletext\">$handler_selection</td>";
                if ($j > 0) {
                    $table_action .= "</tr>";
                }
            }
            $table_action .= "</tr>";
            $table_action .= "<tr class=\"inventory_row_underline\"><td colspan=\"5\"></td></tr>";
        }
        $table_action .= "</table>";
        $table_action .= "</tr></td>";
        $table_action .= "<tr><td colspan=\"5\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
        $table_action .= "<tr height=\"10px\"><td colspan=\"5\"></td></tr>";
        // ## End ###
        
        if (($total_surplus_qty - $total_missing_qty) > 0) {
            // Surplus handling #
            $table_action .= "<tr><td class=\"tabletext\">";
            $table_action .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
            $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"3\">Surplus Handling</td></tr>";
            $table_action .= "<tr class=\"tabletop\">";
            $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
            $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandling_AddAsNewItemQty</td>";
            $table_action .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandling_IgnoreQty</td></tr>";
            for ($i = 0; $i < sizeof($arr_surplus_location); $i ++) {
                $location_id = $arr_surplus_location[$i]["id"];
                $location_name = $arr_surplus_location[$i]["name"];
                
                $table_action .= "<tr>";
                $table_action .= "<td class=\"tabletext\">$location_name</td>";
                $table_action .= "<td><input type=\"text\" name=\"surplus_qty_$location_id\" class=\"textboxnum\" value=\"0\" onChange=\"calcVariance()\"></td>";
                $table_action .= "<td><input type=\"text\" name=\"ignore_qty_$location_id\" class=\"textboxnum\" value=\"0\" onChange=\"calcVariance()\"></td>";
                $table_action .= "</tr>";
                $table_action .= "<tr class=\"inventory_row_underline\"><td colspan=\"3\"></td></tr>";
            }
            $table_action .= "</table>";
            $table_action .= "</tr></td>";
        }
        if (($total_surplus_qty - $total_missing_qty) < 0) {
            // Write-Off, reason = Missing #
            $table_action .= "<tr><td class=\"tabletext\">";
            $table_action .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">";
            $table_action .= "<tr><td class=\"tabletext tablename\" colspan=\"2\">$i_InventorySystem_RequestWriteOff</td></tr>";
            $table_action .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_From</td><td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td></tr>";
            for ($i = 0; $i < sizeof($arr_missing_location); $i ++) {
                $missing_location_id = $arr_missing_location[$i]["id"];
                $missing_location_name = $arr_missing_location[$i]["name"];
                
                $table_action .= "<tr>";
                $table_action .= "<td class=\"tabletext\">$missing_location_name</td>";
                $table_action .= "<td><input type=\"text\" name=\"write_off_qty_$missing_location_id\" class=\"textboxnum\" value=\"0\" onChange=\"calcVariance()\"></td>";
                $table_action .= "</tr>";
                $table_action .= "<tr class=\"inventory_row_underline\"><td colspan=\"2\"></td></tr>";
            }
            $table_action .= "</table>";
            $table_action .= "</tr></td>";
        }
    }
    if ($arr_surplus_location_id != "")
        $tmp_surplus_list = implode(",", $arr_surplus_location_id);
    if ($arr_missing_location_id != "")
        $tmp_missing_list = implode(",", $arr_missing_location_id);
    $table_action .= "<input type=\"hidden\" name=\"surplus_location_list\" value=\"$tmp_surplus_list\">";
    $table_action .= "<input type=\"hidden\" name=\"missing_location_list\" value=\"$tmp_missing_list\">";
    $table_action .= "<input type=\"hidden\" name=\"total_surplus_qty\" value=\"$total_surplus_qty\">";
    $table_action .= "<input type=\"hidden\" name=\"total_missing_qty\" value=\"$total_missing_qty\">";
    $table_summary .= "<tr class=\"bulk\" height=\"35px\"><td align=\"right\" class=\"tabletext\"><font class=\"sectiontitle\">$i_InventorySystem_Stocktake_TotalQty: +<input type=\"text\" name=\"total_surplus_summary\" class=\"inventory_bulknumbox\" value=\"0\">&nbsp;&nbsp;&nbsp; -<input type=\"text\" name=\"total_missing_summary\" class=\"inventory_bulknumbox\" value=\"0\"></font></td></tr>";
}
?>
<script language="javascript">

function calcVariance()
{
	<?
if ($item_type == ITEM_TYPE_BULK) {
    ?>
		var tmp_total_surplus = <?=$total_surplus_qty;?>;
		var tmp_total_missing = <?=$total_missing_qty;?>;
		var tmp_surplus_summary = 0;
		var tmp_missing_summary = 0;
		var tmp_variance = 0;
		tmp_variance = parseInt(tmp_total_surplus) - parseInt(tmp_total_missing);
		
		<?
    for ($i = 0; $i < sizeof($arr_surplus_location_id); $i ++) {
        ?>
			var tmp_surplus_location_id = <?=$arr_surplus_location_id[$i];?>;
			<?
        for ($j = 0; $j < sizeof($arr_missing_location_id); $j ++) {
            ?>
				var tmp_missing_location_id = <?=$arr_missing_location_id[$j];?>;
				var tmp_ans1 = eval("document.form1.move_to_qty_"+tmp_surplus_location_id+"_"+tmp_missing_location_id+".value");
				tmp_surplus_summary = parseInt(tmp_surplus_summary) + parseInt(tmp_ans1);
				tmp_missing_summary = parseInt(tmp_missing_summary) + parseInt(tmp_ans1);
			<?
        }
        ?>	
		<?
    }
    ?>
		
		<?
    for ($i = 0; $i < sizeof($arr_missing_location_id); $i ++) {
        ?>
			var tmp_missing_location_id = <?=$arr_missing_location_id[$i];?>;
			<?
        for ($j = 0; $j < sizeof($arr_surplus_location_id); $j ++) {
            ?>
				var tmp_surplus_location_id = <?=$arr_surplus_location_id[$j];?>;
				var tmp_ans2 = eval("document.form1.relocate_to_qty_"+tmp_missing_location_id+"_"+tmp_surplus_location_id+".value");
				tmp_missing_summary = parseInt(tmp_missing_summary) + parseInt(tmp_ans2);
				tmp_surplus_summary = parseInt(tmp_surplus_summary) + parseInt(tmp_ans2);
			<?
        }
        ?>
		<?
    }
    ?>
		
		if(tmp_variance < 0)
		{
			<?
    for ($i = 0; $i < sizeof($arr_missing_location_id); $i ++) {
        ?>
				var tmp_missing_location_id = <?=$arr_missing_location_id[$i];?>;
				var tmp_ans3 = eval("document.form1.write_off_qty_"+tmp_missing_location_id+".value");
				tmp_missing_summary = parseInt(tmp_missing_summary) + parseInt(tmp_ans3);
			<?
    }
    ?>
		}
		
		if(tmp_variance > 0)
		{
			<?
    for ($i = 0; $i < sizeof($arr_surplus_location_id); $i ++) {
        ?>
				var tmp_surplus_location_id = <?=$arr_surplus_location_id[$i];?>;
				var tmp_ans4 = eval("document.form1.surplus_qty_"+tmp_surplus_location_id+".value");
				var tmp_ans5 = eval("document.form1.ignore_qty_"+tmp_surplus_location_id+".value");
				tmp_surplus_summary = parseInt(tmp_surplus_summary)+parseInt(tmp_ans4)+parseInt(tmp_ans5);
			<?
    }
    ?>
		}
		
		document.form1.total_surplus_summary.value = tmp_surplus_summary;
		document.form1.total_missing_summary.value = tmp_missing_summary;
	<?
}
?>
}

function checkForm()
{
	<?
if ($item_type == ITEM_TYPE_SINGLE) {
    ?>
		var action_selected = 0;
		var passed = 0;
		var condition = document.form1.single_condition.value;
		
		if(condition == 1)
		{
			if(document.form1.variance_action[0].checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetHandler,"Please select a handler",0))
				{
					passed = 1;
				}
				else
				{
					return false;
				}
			}
			if(document.form1.variance_action[1].checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetLocation,"Please select a location",0))
				{
					if(check_select(document.form1.targetSublocation,"Please select a location",0))
					{
						if(check_select(document.form1.targetHandler,"Please select a handler",0))
						{
							passed = 1;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			if(document.form1.variance_action[2].checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetWriteOffReason,"Please select a write-off reason",0))
				{
					passed = 1;
				}
				else
				{
					return false;
				}
			}
		}
		
		if(condition == 2)
		{
			if(document.form1.variance_action[0].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			else
			{
				return false;
			}
			if(document.form1.variance_action[1].checked == true)
			{
				action_selected = 1;
				passed = 1;
			}
			else
			{
				return false;
			}
		}
		
		if(condition == 3)
		{
			if(document.form1.variance_action.checked == true)
			{
				action_selected = 1;
				if(check_select(document.form1.targetWriteOffReason,"Please select a write-off reason",0))
				{
					passed = 1;
				}
				else
				{
					return false;
				}
			}
		}
	<?
}
?>
	
	<?
if ($item_type == ITEM_TYPE_BULK) {
    ?>
		var tmp_total_surplus = <?=$total_surplus_qty;?>;
		var tmp_total_missing = <?=$total_missing_qty;?>;
		var tmp_move_back_total_qty = 0;
		var tmp_surplus_handling_total_qty = 0;
		var tmp_relocate_total_qty = 0;
		var tmp_write_off_total_qty = 0;
		var tmp_variance = 0;
		var move_back_checking = 0;
		var relocate_checking = 0;
		var surplus_checking = 0;
		var missing_checking = 0;
		
		tmp_variance = parseInt(tmp_total_surplus) - parseInt(tmp_total_missing);
		
		<?
    for ($i = 0; $i < sizeof($arr_surplus_location); $i ++) {
        ?>
			var tmp_surplus_location_id = <?=$arr_surplus_location[$i]["id"];?>;
			var tmp_surplus_location_qty = <?=$arr_surplus_location[$i]["qty"];?>;
			
			<?
        for ($j = 0; $j < sizeof($arr_missing_location); $j ++) {
            ?>
				var tmp_missing_location_id = <?=$arr_missing_location[$j]["id"];?>;
				var tmp_missing_location_qty = <?=$arr_missing_location[$j]["qty"];?>;
				var tmp_move_back = eval("document.form1.move_to_qty_"+tmp_surplus_location_id+"_"+tmp_missing_location_id+".value");
				var tmp_target_MoveBackHandler = eval("document.form1.target_handler_"+tmp_surplus_location_id+"_"+tmp_missing_location_id);
				
				if(!isNaN(tmp_move_back))
				{
					tmp_move_back_total_qty = parseInt(tmp_move_back_total_qty) + parseInt(tmp_move_back);				
					if(tmp_move_back_total_qty > tmp_missing_location_qty)
					{
						if(check_select(tmp_target_MoveBackHandler,"Please select a handler",0))
						{
							move_back_checking++;
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					alert("Please input a valid quantity");
					eval("document.form1.move_to_qty_"+tmp_surplus_location_id+"_"+tmp_missing_location_id+".focus()");
					return false;
				}
			<?
        }
        ?>
			if(tmp_variance > 0)
			{
				var tmp_surplus_handling_qty = eval("document.form1.surplus_qty_"+tmp_surplus_location_id+".value");
				var tmp_ignore_handling_qty = eval("document.form1.ignore_qty_"+tmp_surplus_location_id+".value");
				tmp_surplus_handling_total_qty = parseInt(tmp_surplus_handling_total_qty)+parseInt(tmp_surplus_handling_qty)+parseInt(tmp_ignore_handling_qty);
			}
		<?
    }
    ?>
		
		<?
    for ($i = 0; $i < sizeof($arr_missing_location); $i ++) {
        ?>
			var tmp_missing_location_id = <?=$arr_missing_location[$i]["id"];?>;
			var tmp_missing_location_qty = <?=$arr_missing_location[$i]["qty"];?>;
			<?
        for ($j = 0; $j < sizeof($arr_surplus_location); $j ++) {
            ?>
				var tmp_surplus_location_id = <?=$arr_surplus_location[$j]["id"];?>;
				var tmp_surplus_location_qty = <?=$arr_surplus_location[$j]["qty"];?>;
				var tmp_relocate = eval("document.form1.relocate_to_qty_"+tmp_missing_location_id+"_"+tmp_surplus_location_id+".value");
				var tmp_target_RelocateHandler = eval("document.form1.target_handler_"+tmp_missing_location_id+"_"+tmp_surplus_location_id);
				
				if(!isNaN(tmp_relocate))
				{
					tmp_relocate_total_qty = parseInt(tmp_relocate_total_qty) + parseInt(tmp_relocate);
					if(tmp_relocate_total_qty > tmp_missing_location_qty)
					{
						if(check_select(tmp_target_RelocateHandler,"Please select a handler",0))
						{
							relocate_checking++;
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					alert("Please input a valid quantity");
					eval("document.form1.relocate_to_qty_"+tmp_missing_location_id+"_"+tmp_surplus_location_id+".focus()");
					return false;
				}
			<?
        }
        ?>
			if(tmp_variance < 0)
			{
				var tmp_write_off_qty = eval("document.form1.write_off_qty_"+tmp_missing_location_id+".value");
				tmp_write_off_total_qty = parseInt(tmp_write_off_total_qty)+parseInt(tmp_write_off_qty);
			}
		<?
    }
    ?>
		if(move_back_checking > 0)
		{
			alert("False - Move Back");
			return false;
		}
		if(relocate_checking > 0)
		{
			alert("False - Relocation");
			return false;
		}
		if(tmp_variance > 0)
		{
			if(tmp_surplus_handling_total_qty != tmp_variance)
			{
				alert("False - Surplus Handling");
				return false;
			}
		}
		
		if(tmp_variance < 0)
		{
			var tmp_val = -1 * parseInt(tmp_variance);
			if(tmp_write_off_total_qty != tmp_val)
			{
				alert("False - Write-off handling");
				return false;
			}
		}
		if(document.form1.total_surplus_summary.value == tmp_total_surplus)
		{
			surplus_checking = 1;
		}
		if(document.form1.total_missing_summary.value == tmp_total_missing)
		{
			missing_checking = 1;
		}
	<?
}
?>

	<?
if ($item_type == ITEM_TYPE_SINGLE) {
    ?>
		if(action_selected == 1)
		{
			if(passed == 1)
			{
				document.form1.action = "outstanding_items_edit_update.php";
				return ture;
			}
		}
		else
		{
			alert("Please select an action");
			return false;
		}
	<?
}
?>
	<?
if ($item_type == ITEM_TYPE_BULK) {
    ?>
		if((surplus_checking == 1) && (missing_checking == 1))
		{
			document.form1.action = "outstanding_items_edit_update.php";
			return true;
		}
		else
		{
			alert("Please check all the quantity again");
			return false;
		}
	<?
}
?>
}
</script>

<br>
<form name="form1" action="" method="post" onSubmit="return checkForm()">

	<table id="html_body_frame" width="96%" border="0" cellspacing="0"
		cellpadding="5" align="center">
<?=$infobar1?>
</table>

	<table width="96%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$infobar2?>
</table>

	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content?>
</table>
	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img src="<?=$image_path?>/2007a/10x10.gif"
				width="10" height="5"></td>
		</tr>
	</table>
	<br>
	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_action?>
</table>

	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" class="dotline"><img
				src="<?=$image_path/$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr height="10px">
			<td></td>
		</tr>
<?=$table_summary?>
<tr class="tabletop" height="10px">
			<td></td>
		</tr>
	</table>
	<table border="0" width="96%" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit");?>
		<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");?>
		<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='outstanding_items.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");?>
</td>
		</tr>
	</table>

	<input type="hidden" name="ItemID" value="<?=$ItemID?>">

</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>