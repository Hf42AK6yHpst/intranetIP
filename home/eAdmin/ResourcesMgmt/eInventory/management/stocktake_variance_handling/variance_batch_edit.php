<?php

# using: 

/*
 * 	Log
 * 
 * 	2015-12-21 Cameron
 * 	- copied from variance_edit.php and modify to handle batch edit single items for resuming original location
 * 	
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_StocktakeVarianceHandling";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem_StocktakeVarianceHandling, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem_ItemType_Single . ' - ' . $Lang['eInventory']['MoveToOriginalLocationByBatch'])."</td></tr>";

$table_content .= "<tr class=\"tabletop\">";
$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_VarianceHandling_CurrentLocation."</td>";
$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Stocktake_ExpectedLocation."</td>";
$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem['Caretaker']."</td>";
$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Stocktake_LastStocktakeBy."</td>";
$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Setting_StockCheckPeriod."</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandlingNotice_Handler</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>";
$table_content .= "</tr>";

if (count($ItemID) > 0) {
	$sql = "SELECT 
				b.LocationID,
				CONCAT(".$linventory->getInventoryNameByLang("LocBuilding.").", ' > ' ,".$linventory->getInventoryNameByLang("d.").",' > ',".$linventory->getInventoryNameByLang("c.")."),
				b.AdminGroupID,
				".$linventory->getInventoryNameByLang("e.").",
				b.PersonInCharge,
				".getNameFieldByLang2("f.").",
				b.RecordDate,
				CONCAT('+',b.SurplusQty),
				CONCAT(".$linventory->getInventoryNameByLang("d2.").",' > ',".$linventory->getInventoryNameByLang("c2.").",' > ',".$linventory->getInventoryNameByLang("b2.")."),
				a2.GroupInCharge,
				a.ItemID
			FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) INNER JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID) INNER JOIN 
				INVENTORY_ITEM_SINGLE_EXT AS a2 ON (a2.ItemID=a.ItemID) INNER JOIN 
				INVENTORY_LOCATION AS b2 ON (a2.LocationID = b2.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS c2 ON (b2.LocationLevelID = c2.LocationLevelID) 
				inner join INVENTORY_LOCATION_BUILDING as d2 on (d2.BuildingID = c2.BuildingID)
			WHERE	
				a.ItemID IN ('".implode("','",$ItemID)."')
			";
	$arr_surplus_result = $linventory->returnArray($sql,8);
}
if(sizeof($arr_surplus_result) > 0)
{
	for($i=0; $i<sizeof($arr_surplus_result); $i++)
	{
		list($surplus_location_id, $surplus_location_name, $surplus_admin_id, $surplus_admin_name, $surplus_user_id, $surplus_user_name, $surplus_date, $surplus_qty, $original_location_name, $group_in_Charge, $item_id) = $arr_surplus_result[$i];
		
		$sql = "SELECT DISTINCT a.UserID, ".getNameFieldByLang2("a.")." FROM INTRANET_USER AS a INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER AS b ON (a.UserID = b.UserID) AND b.AdminGroupID = '$group_in_Charge'";
		$arr_handler_result = $linventory->returnArray($sql);
		$handler_selection = getSelectByArray($arr_handler_result,"name=\"targetHandler_$item_id\"",$surplus_user_id,0);
		
		$table_content .= "<tr class=\"single\">";
		$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($surplus_location_name)."</td>";
		$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($original_location_name)."</td>";
		$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($surplus_admin_name)."</td>";
		$table_content .= "<td class=\"tabletext\">$surplus_user_name</td>";
		$table_content .= "<td class=\"tabletext\">$surplus_date</td>";
		$table_content .= "<td class=\"tabletext\">$handler_selection</td>";
		$table_content .= "<td class=\"tabletext\"><font color=\"green\">$surplus_qty</font></td>";
		$table_content .= "</tr>";
		$table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"7\"></td></tr>";
		$table_content .= "<input type=\"hidden\" name=\"SurplusLocationID[]\" value=\"$surplus_location_id\">";
		$table_content .= "<input type=\"hidden\" name=\"ItemID[]\" value=\"$item_id\">";
	}
}
	
		
?>

<script language="javascript">

function checkForm()
{
	var passed = 1;
	$(":input[name^='targetHandler_']").each(function(){
		if (($(this).val() == '') && (passed == 1)) {
			alert("<?=$i_InventorySystem_VarianceHandle_SelectHandlerWarning?>");
			passed = 0;
		}
	});
	
	if(passed == 1)
	{
		document.form1.action = "variance_batch_edit_update.php";
		document.form1.submit();
	}
}

function click_reset() {
	$(":input[name^='targetHandler_']").each(function(){
		$(this).val('');
	});
}

</script>

<br>
<form name="form1" action="" method="post" >

<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$infobar1?>
</table>

<table border="0" width="96%" cellpadding="5" cellspacing="0" align="center">
<?=$table_content?>
</table>
<table border="0" width="96%" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN;?>/10x10.gif" width="10" height="5"></td>
</tr>
</table>
<br>
<table border="0" width="96%" cellpadding="5" cellspacing="0" align="center">
<?=$table_action?>
</table>

<table border="0" width="96%" cellpadding="5" cellspacing="0" align="center">
<tr><td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_submit, "button","javascript:checkForm();");?>
		<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:click_reset();");?>
		<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='outstanding_items.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");?>
</td></tr>
</table>

</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
