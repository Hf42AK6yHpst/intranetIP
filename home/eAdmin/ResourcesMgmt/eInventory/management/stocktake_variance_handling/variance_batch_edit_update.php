<?php
# using: 
/*
 * 	Log
 * 
 * 	2015-12-21 Cameron
 * 	- copied from variance_edit_update.php and modify to handle batch edit single items for resuming original location
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linventory		= new libinventory();
$curr_date = date("Y-m-d");
for ($i=0, $iMax=count($ItemID); $i < $iMax; $i++) {
	$item_id = $ItemID[$i];
	$surplus_location_id = $SurplusLocationID[$i];
	$targetHandler = ${"targetHandler_$item_id"}; 
	
	# remove the old non-handling reminder records
	$sql = "delete from INVENTORY_VARIANCE_HANDLING_REMINDER where ItemID='$id' and RecordStatus=0";
	$linventory->db_db_query($sql);
		
		$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					('$item_id', '$curr_date', ".ITEM_ACTION_MOVEBACKTOORGINIAL.", $UserID, '', 0, 0, NOW(), NOW())
				";
		$linventory->db_db_query($sql);
				
		$sql = "SELECT
					b.LocationID
				FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
				WHERE
					a.ItemID = '$item_id'
				";
		$arr_new_location_id = $linventory->returnVector($sql);
		$new_location_id = $arr_new_location_id[0];
			
		$sql = "INSERT INTO 
						INVENTORY_VARIANCE_HANDLING_REMINDER
						(HandlerID, SenderID, ItemID, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						('$targetHandler', '$UserID', '$item_id', 1, '$surplus_location_id', '$new_location_id', NOW(), 0, 0, NOW(), NOW())
				";
		$linventory->db_db_query($sql);
		
		$sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '$item_id'";
		$linventory->db_db_query($sql);
		
		$sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '$item_id'";
		$linventory->db_db_query($sql);
}
	



#### Remove qty = 0 record (double check)
$sql = "delete from INVENTORY_ITEM_SURPLUS_RECORD where SurplusQty=0";
$linventory->db_db_query($sql);
$sql = "delete from INVENTORY_ITEM_MISSING_RECORD where MissingQty=0";
$linventory->db_db_query($sql);

intranet_closedb();
header("location: outstanding_items.php?msg=2");
?>