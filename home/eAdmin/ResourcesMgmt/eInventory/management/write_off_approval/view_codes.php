<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");


intranet_auth();
intranet_opendb();
$linventory	= new libinventory();


switch ($CodeType)
{
	case "ItemCode":
		$sql = "SELECT 
						ItemCode, " . $linventory->getInventoryItemNameByLang() . " 
				FROM 
						INVENTORY_ITEM 
				WHERE 	
						ItemType = '$ItemType' 
				AND		
						RecordStatus<>'0' ORDER BY ItemCode";
			break;
	case "WriteOffReason":
		$sql = "SELECT 
						" . $linventory->getInventoryItemWriteOffReason() . " 
				FROM 
						INVENTORY_WRITEOFF_REASON_TYPE"; 
			break;
	case "LocationCode":
		$sql = "SELECT 
					DISTINCT floor.Code, ". $linventory->getInventoryNameByLang("floor.") ."
				FROM 
					INVENTORY_LOCATION_LEVEL as floor
					INNER JOIN INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
				WHERE
					floor.RecordStatus = 1 and room.RecordStatus = 1
				Order by 
					floor.DisplayOrder";
			break;
	case "SubLocationCode":
		$sql = "SELECT 
					DISTINCT room.Code, CONCAT(".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
				FROM 
					INVENTORY_LOCATION_LEVEL as floor 
					INNER JOIN INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
				WHERE
					floor.RecordStatus = 1 and room.RecordStatus = 1
				Order by 
					floor.DisplayOrder, room.DisplayOrder";
			break;			
	case "FundingCode":
		$sql = "SELECT Code, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_FUNDING_SOURCE WHERE RecordStatus=1 order by DisplayOrder";
			break;
			
	case "GroupCode":
		$sql = "SELECT Code, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_ADMIN_GROUP order by DisplayOrder";
			break;	
}

$table_contents = "";
if ($sql!="")
{
	$Rows = $linventory->returnArray($sql);
}

if ($CodeType=="WriteOffReason") {
	for ($i=0,$iMax=count($Rows); $i<$iMax; $i++) {	
		$tr_class = ($i%2==0) ? 'class="row_waiting"' : '';
		$table_contents .= "<tr {$tr_class} ><td>".($i+1)."</td><td>".$Rows[$i][0]."</td></tr>\n";
	}
	$table_header = "<tr>
	   	  				<th class='num_check'>#</th>	   	  
	   	  				<th>".$i_InventorySystem['WriteOffReason']."</th>
       				</tr>";
       				
}
else
{
	for ($i=0,$iMax=count($Rows); $i<$iMax; $i++) {
		$tr_class = ($i%2==0) ? 'class="row_waiting"' : '';
		$table_contents .= "<tr {$tr_class} ><td>".($i+1)."</td><td>".$Rows[$i][0]."</td><td>".$Rows[$i][1]."</td></tr>\n";
	}
	$table_header = "<tr>
	   	  				<th class='num_check'>#</th>	   	  
	   	  				<th>".$Lang['General']['Code']."</th>
						<th>".$Lang['General']['Name']."</th>
       				</tr>";
}


?>

<table class="common_table_list_v30 view_table_list_v30">
	<thead><?=$table_header?></thead>
   <tbody>
   <?= $table_contents ?>
   </tbody>
</table>

<?php

intranet_closedb();


?>
