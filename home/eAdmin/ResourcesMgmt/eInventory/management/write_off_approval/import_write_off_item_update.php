<?php
// modifying : 
/*
 * 	Log
 * 
 * 	2016-01-20 Cameron
 * 		create this file
 * 
 */


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Management_WriteOffApproval";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$li 			= new libdb();
$curr_date = date("Y-m-d");
$result = array();

$infobar .= "<tr><td colspan=\"2\" class=\"navigation_v30\">
				<a href=\"import_write_off_item.php\">".$Lang['eInventory']['ImportWriteoffItem']."</a>
				<span>".($format==1?$i_InventorySystem_ItemType_Single:$i_InventorySystem_ItemType_Bulk)."</span>
			</td></tr>\n"; 


if($format == ITEM_TYPE_SINGLE) {
	$sql = "SELECT 	
					i.ItemID,
					i.ItemCode,
					e.LocationID, 
					e.GroupInCharge, 
					t.WriteOffReason, 
					t.RequestDate, 
					t.ApproveDate, 
					t.ItemRemark 
			FROM 	
					TEMP_INVENTORY_WRITE_OFF_ITEM t
			INNER JOIN 
					INVENTORY_ITEM i ON i.ItemCode=t.ItemCode
			INNER JOIN 
					INVENTORY_ITEM_SINGLE_EXT e ON e.ItemID=i.ItemID
			WHERE 
					t.ItemType=1 
			ORDER BY 
					t.ItemCode";
	$rs = $linventory->returnArray($sql);
	$numOfImportData = count($rs);
	
	if ($numOfImportData > 0) {
		for ($i=0;$i<$numOfImportData;$i++) {
			$r = $rs[$i];
			$_ItemID = $r['ItemID'];
			$_ItemCode = $linventory->Get_Safe_Sql_Query($r['ItemCode']);
			$_LocationID = $r['LocationID'];
			$_GroupInCharge = $r['GroupInCharge'];
			$_WriteOffReason = $linventory->Get_Safe_Sql_Query($r['WriteOffReason']);
			$_RequestDate = $r['RequestDate'];
			$_ApproveDate = $r['ApproveDate'];
			$_ItemRemark =$linventory->Get_Safe_Sql_Query($r['ItemRemark']);
			
			$insertWriteOffAry[] = " ('".$_ItemID."', '".$curr_date."', '".$_LocationID."', '".$_GroupInCharge."', 1, '".$_WriteOffReason."', '".
							$_RequestDate."', '".$_SESSION['UserID']."', '".$_ApproveDate."', '".$_SESSION['UserID']."', 0, 1, now(), now()) ";
			$updateItemAry[] = $_ItemID;

			$insertLogAry[] = " ('".$_ItemID."', '".$curr_date."', '".ITEM_ACTION_WRITEOFF."', '".$_SESSION['UserID'].
							"', '".$_ItemRemark."', 0, 0, NOW(), NOW()) ";
		}	// end for loop
		
		## Step 1: add record to INVENTORY_ITEM_WRITE_OFF_RECORD, set RecordStatus=1 (approved)
		if (count($insertWriteOffAry) > 0) {
			$field = "ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, ".
					"ApproveDate, ApprovePerson, RecordType, RecordStatus, DateInput, DateModified";
			$insertChunkAry = array_chunk($insertWriteOffAry, 1000);
			$numOfChunk = count($insertChunkAry);
			
			for ($i=0; $i<$numOfChunk; $i++) {
				$_insertAry = $insertChunkAry[$i];
				$sql = "INSERT INTO INVENTORY_ITEM_WRITE_OFF_RECORD ($field) VALUES ".implode(', ', $_insertAry);
				$result[] = $linventory->db_db_query($sql);
			}
		}
		
		## Step 2: update Item RecordStatus, set it to "Write-Off"(0)
		if (count($updateItemAry) > 0) {
			$sql = "UPDATE INVENTORY_ITEM SET RecordStatus = 0 WHERE ItemID IN ('".implode("','",$updateItemAry)."')";
			$result[] = $linventory->db_db_query($sql);
		}

		## Step 3: add action log to INVENTORY_ITEM_SINGLE_STATUS_LOG
		if (count($insertLogAry) > 0) {
			$field = "ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified";
			$insertChunkAry = array_chunk($insertLogAry, 1000);
			$numOfChunk = count($insertChunkAry);
			
			for ($i=0; $i<$numOfChunk; $i++) {
				$_insertAry = $insertChunkAry[$i];
				$sql = "INSERT INTO INVENTORY_ITEM_SINGLE_STATUS_LOG ($field) VALUES ".implode(', ', $_insertAry);
				$result[] = $linventory->db_db_query($sql);
			}
		}
	}
	else {
		$result[] = false;
	}		
}


###################################################################################################
else {	// Bulk Items

	$sql = "SELECT 
					i.ItemID,
					t.LocationID,
					t.GroupInChargeID,
					t.FundingSourceID,
					t.Quantity,
					t.WriteOffReason,
					t.RequestDate,
					t.ApproveDate,		
					t.ItemRemark
			FROM 
					TEMP_INVENTORY_WRITE_OFF_ITEM t
			INNER JOIN 
					INVENTORY_ITEM i ON i.ItemCode=t.ItemCode
			WHERE 
					i.ItemType=2 
			ORDER BY 
					i.ItemID";
	
	$rs = $linventory->returnArray($sql);
	$numOfImportData = count($rs);
	
	if ($numOfImportData > 0) {
		for ($i=0;$i<$numOfImportData;$i++) {
			$r = $rs[$i];
			$_ItemID = $r['ItemID'];
			$_LocationID = $r['LocationID'];
			$_GroupInChargeID = $r['GroupInChargeID'];
			$_FundingSourceID = $r['FundingSourceID'];
			$_Quantity = $r['Quantity'];
			$_WriteOffReason = $linventory->Get_Safe_Sql_Query($r['WriteOffReason']);
			$_RequestDate = $r['RequestDate'];
			$_ApproveDate = $r['ApproveDate'];
			$_ItemRemark =$linventory->Get_Safe_Sql_Query($r['ItemRemark']);

			## Step 1: add record to INVENTORY_ITEM_WRITE_OFF_RECORD, set RecordStatus=1 (approved) 			
			$_insertAry = " ('".$_ItemID."', '".$curr_date."', '".$_LocationID."', '".$_GroupInChargeID."', $_Quantity, '".$_WriteOffReason."', '".
							$_RequestDate."', '".$_SESSION['UserID']."', '".$_ApproveDate."', '".$_SESSION['UserID']."', 0, 1, now(), now(),'".
							$_FundingSourceID."') ";

			$field = "ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, ".
					"ApproveDate, ApprovePerson, RecordType, RecordStatus, DateInput, DateModified, FundingSourceID";
			$sql = "INSERT INTO INVENTORY_ITEM_WRITE_OFF_RECORD ($field) VALUES ".$_insertAry;
			$result[] = $linventory->db_db_query($sql);
//			$_write_off_id = $linventory->db_insert_id();

			## Step 2: check if inventory quantity is enough to write-off
			## 2.1 Total qty of specific location, group, funding source of an item 
			$sql = "SELECT 
							Quantity 
					FROM 
							INVENTORY_ITEM_BULK_LOCATION 
					WHERE 
							ItemID = '$_ItemID' 
					AND 
							GroupInCharge = '$_GroupInChargeID'
					AND 
							LocationID = '$_LocationID'
					AND	
							FundingSourceID='$_FundingSourceID'";
			$arr_tmp_qty1 = $linventory->returnVector($sql);
			if (count($arr_tmp_qty1) > 0) {
				$tmp_check_1 = ($_Quantity <= $arr_tmp_qty1[0])? true : false;
			}
			else {
				$tmp_check_1 = false;
			}
			
			## 2.2 Total qty of an item
			$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_EXT WHERE ItemID = '$_ItemID'";
			$arr_tmp_qty2 = $linventory->returnVector($sql);
			if (count($arr_tmp_qty2) > 0) {
				$tmp_check_2 = ($_Quantity <= $arr_tmp_qty2[0])? true : false;
			}
			else {
				$tmp_check_2 = false;
			}
			
			if($tmp_check_1 && $tmp_check_2)
			{
				## Step 3: update qty of specific location, group, funding source of an item
				$sql = "UPDATE 
								INVENTORY_ITEM_BULK_LOCATION 
								SET Quantity = Quantity - $_Quantity 
						WHERE 
								ItemID = '$_ItemID' 
						AND 
								GroupInCharge = '$_GroupInChargeID'
						AND 
								LocationID = '$_LocationID'
						AND	
								FundingSourceID='$_FundingSourceID'";
				$result[] = $linventory->db_db_query($sql);
				
				## Step 4: update total qty of an item
				$sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET Quantity = Quantity - $_Quantity WHERE ItemID = '$_ItemID'";
				$result[] = $linventory->db_db_query($sql);
				
				## Step 5: write log
				$sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG 
							(ItemID, RecordDate, Action, QtyChange, PersonInCharge, LocationID, GroupInCharge, FundingSource, 
								RecordType, RecordStatus, DateInput, DateModified)
						VALUES
							('$_ItemID', '$curr_date', ".ITEM_ACTION_WRITEOFF.", $_Quantity, '".$_SESSION['UserID']."',
							 '$_LocationID', '$_GroupInChargeID', '$_FundingSourceID', 0, 0, NOW(), NOW())";
				$result[] = $linventory->db_db_query($sql);
				
				## Step 6: udpate item status qty, why doing so ???
				# retrieve latest item status
				$sql = "select QtyNormal, QtyDamage, QtyRepair, Remark from INVENTORY_ITEM_BULK_LOG where (Action=". ITEM_ACTION_UPDATESTATUS .
						" or Action=". ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING ." or Action=". ITEM_ACTION_PURCHASE ." ) 
						AND
								ItemID = '$_ItemID' 
						AND 
								GroupInCharge = '$_GroupInChargeID'
						AND 
								LocationID = '$_LocationID'
						AND	
								FundingSource='$_FundingSourceID'
						order by RecordID desc limit 0, 1";
				$rs2 = $linventory->returnArray($sql);
				if(!empty($rs2))
				{
					$QtyNormal = $rs2[0]['QtyNormal'];
					$QtyDamage = $rs2[0]['QtyDamage'];
					$QtyRepair = $rs2[0]['QtyRepair'];
				}
				$QtyNormal = ($QtyDamage + $QtyRepair + $QtyNormal == 0) ? $arr_tmp_qty1[0] : $QtyNormal;
				$qty_ary = array($QtyDamage, $QtyRepair, $QtyNormal);
				$qty_remain = $_Quantity;
				for($j=0;$j<=2;$j++)
				{
					if($qty_ary[$j] >= $qty_remain)
					{
						$qty_ary[$j] -= $qty_remain;
						break;
					}
					else
					{
						$qty_remain -= $qty_ary[$j];
						$qty_ary[$j] = 0;
					}
				}
				$sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, PersonInCharge, Remark,
						LocationID, GroupInCharge, FundingSource, DateInput, DateModified)
				VALUES
						('$_ItemID', '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'".$qty_ary[2]."','".$qty_ary[0]."','".$qty_ary[1].
						"','".$_SESSION['UserID']."','',
						'$_LocationID', '$_GroupInChargeID', '$_FundingSourceID',NOW(),NOW())";
				$result[] = $linventory->db_db_query($sql);
				# udpate item status qty - end

				## comment this first, not require here?
				# eBooking integration - send email notification
//				$linventory->sendEmailNotification_eBooking(2,$_ItemID, $r['WriteOffReason'], 5, $_LocationID, $_GroupInChargeID, $_FundingSourceID, $_Quantity);
			}
			else
			{
				$result[] = false;
			}

		}	// end for loop
	}		// $numOfImportData > 0		
}			// end bulk item



###################################################################################################
## show result
if (in_array(false,(array)$result)) {
	$importResult = $Lang['eInventory']['ImportWriteoffItemFail'];
}
else {
	$importResult = $numOfImportData . ' ' . $Lang['General']['ImportArr']['RecordsImportedSuccessfully'];
}


$TAGS_OBJ[] = array($button_import, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### step display
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['eInventory']['ImportStep'][2], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);

?>

<br>

<form name="form1" action="" method="post">
<table border="0" width="99%" cellspacing="0" cellpadding="0">
<?=$infobar?>
</table>
<br>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>

	<tr>
		<td><?=$importResult?></td>
	</tr>
	
	<tr>
		<td colspan=100% align=center>
		<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='write_off_item.php'","backbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>	
</table>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
