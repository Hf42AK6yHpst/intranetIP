<?php
// modifying :
/*
 * Log
 *
 * 2018-12-17 Henry
 * Bug fix for the wring error msg [Case#W154473]
 *
 * 2018-02-22 Henry
 * add access right checking [Case#E135442]
 *
 * 2016-01-21 Cameron
 * create this file
 *
 */

// this page only handle import of single item
// format = 1 Single Item
// format = 2 Bulk Items
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
if ($format == "" || $format == 1) {
    header("Location: import_write_off_item.php?returnMsgKey=WrongFileFormat&format=2");
    exit();
}
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Management_WriteOffApproval";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();

// $_FILES is an array storing name, type, tmp_name, error and size
$filepath = $itemfile; // tmp_name
$filename = $itemfile_name; // original file name

$infobar .= "<tr><td colspan=\"2\" class=\"navigation_v30\">
				<a href=\"import_write_off_item.php\">" . $Lang['eInventory']['ImportWriteoffItem'] . "</a>
				<span>" . $i_InventorySystem_ItemType_Bulk . "</span>
			</td></tr>\n";

$file_format = array(
    "Item Code",
    "Item Chinese Name",
    "Item English Name",
    "Location Code",
    "Sub location Code",
    "Funding Source Code",
    "Resource Mgmt Group Code",
    "Quantity",
    "Write-off Reason",
    "Request Date",
    "Approve Date",
    "Remarks"
);

// Create temp bulk item table
$sql = "DROP TABLE IF EXISTS TEMP_INVENTORY_WRITE_OFF_ITEM";
$result = $li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_WRITE_OFF_ITEM
			(
			ItemType int(11),
			NameChi varchar(255),
			NameEng varchar(255),
			ItemCode varchar(100),			 
			LocationLevelCode varchar(10),
			LocationCode varchar(10),
			GroupInChargeCode varchar(10),
			FundingSourceCode varchar(10),
			LocationLevelID int(11),
			LocationID int(11),
			GroupInChargeID int(11),
			FundingSourceID int(11),
			Quantity int(11),
			WriteOffReason varchar(255),
			RequestDate date,
			ApproveDate date,		
			ItemRemark text
		    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$result = $li->db_db_query($sql);

$ext = strtoupper($lo->file_ext($filename));

if ($ext != ".CSV" && $ext != ".TXT") {
    header("location: import_write_off_item.php?returnMsgKey=WrongFileFormat&format=2");
    exit();
}

if ($limport->CHECK_FILE_EXT($filename)) {
    // read file into array
    // return 0 if fail, return csv array if success
    $data = $limport->GET_IMPORT_TXT($filepath);
    $col_name = array_shift($data); // drop the title bar
                                    
    // check the csv file's first row is correct or not
    $format_wrong = false;
    for ($i = 0, $iMax = count($file_format); $i < $iMax; $i ++) {
        if ($col_name[$i] != $file_format[$i]) {
            $format_wrong = true;
            break;
        }
    }
    if ($format_wrong) {
        header("location: import_write_off_item.php?returnMsgKey=ImportUnsuccess_IncorrectHeaderFormat&format=2");
        exit();
    }
    
    // ## Remove Empty Row in CSV File ###
    for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
        if (count($data[$i]) != 0) {
            $arr_new_data[] = $data[$i];
        }
    }
    
    // ##########################################################################
    // Start validating data
    $file_original_row = count($arr_new_data);
    
    // # item_code
    $item_code_array = array();
    for ($i = 0, $iMax = count($arr_new_data); $i < $iMax; $i ++) {
        $item_code_array[] = trim($linventory->Get_Safe_Sql_Query($arr_new_data[$i][0]));
    }
    
    /*
     * array( [2/F] => 2
     * [3/F] => 5
     * )
     */
    // # location
    $sql = "SELECT LocationLevelID, Code FROM INVENTORY_LOCATION_LEVEL ORDER BY Code";
    $rs = $linventory->returnArray($sql);
    $location_array = BuildMultiKeyAssoc($rs, array(
        'Code'
    ), 'LocationLevelID', 1);
    $location_code_array = array_keys($location_array);
    
    // # sub-location
    $sql = "SELECT il.LocationID, il.Code, ill.Code as LevelCode FROM INVENTORY_LOCATION il JOIN INVENTORY_LOCATION_LEVEL ill ON il.LocationLevelID = ill.LocationLevelID ORDER BY Code";
//    $sql = "SELECT LocationLevelID, LocationID, Code FROM INVENTORY_LOCATION ORDER BY Code";
    $rs = $linventory->returnArray($sql);
    $sub_location_array = BuildMultiKeyAssoc($rs, array(
        'Code', 'LevelCode',
    ), 'LocationID', 1);
    $sub_location_code_array = array_keys($sub_location_array);
    
    // # group
    $sql = "SELECT AdminGroupID, Code FROM INVENTORY_ADMIN_GROUP ORDER BY Code";
    $rs = $linventory->returnArray($sql);
    $group_array = BuildMultiKeyAssoc($rs, array(
        'Code'
    ), 'AdminGroupID', 1);
    $group_code_array = array_keys($group_array);
    
    // # funding source
    $sql = "SELECT FundingSourceID, Code FROM INVENTORY_FUNDING_SOURCE ORDER BY Code";
    $rs = $linventory->returnArray($sql);
    $funding_source_array = BuildMultiKeyAssoc($rs, array(
        'Code'
    ), 'FundingSourceID', 1);
    $funding_source_code_array = array_keys($funding_source_array);
    
    // # write-off reason
    $sql = "SELECT ReasonTypeNameEng, ReasonTypeNameChi FROM INVENTORY_WRITEOFF_REASON_TYPE";
    $rs = $linventory->returnArray($sql);
    $write_off_reason_array = array();
    for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
        $write_off_reason_array[] = $rs[$i]['ReasonTypeNameEng'];
        $write_off_reason_array[] = $rs[$i]['ReasonTypeNameChi'];
    }
    
    if (count($item_code_array) > 0) {
        // Get existing bulk item that match the code
        $sql = "SELECT 
						i.ItemID, i.ItemCode, b.LocationID, b.GroupInCharge, b.FundingSourceID, b.Quantity
				FROM 
						INVENTORY_ITEM i
				INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION b ON b.ItemID=i.ItemID
				WHERE 
						i.ItemType = '" . ITEM_TYPE_BULK . "' 
				AND 	i.ItemCode IN ('" . implode("','", $item_code_array) . "')";
        $arr_exist_item = $linventory->returnArray($sql);
        
        // Get existing write-off (including waiting for approval) record that match the code
        $sql = "SELECT 
						i.ItemID, i.ItemCode, w.LocationID, w.AdminGroupID, w.FundingSourceID, SUM(w.WriteOffQty) AS WriteOffQty 
				FROM 
						INVENTORY_ITEM i 
				INNER JOIN 
						INVENTORY_ITEM_WRITE_OFF_RECORD w ON w.ItemID=i.ItemID 
				WHERE 
						i.ItemType = '" . ITEM_TYPE_BULK . "' 
				AND 
						w.RecordStatus='0' 
				AND 
						i.ItemCode IN ('" . implode("','", $item_code_array) . "')
				GROUP BY i.ItemID, i.ItemCode, w.LocationID, w.AdminGroupID, w.FundingSourceID 
				ORDER BY i.ItemCode, w.LocationID, w.AdminGroupID, w.FundingSourceID";
        
        $arr_exist_write_off_item = $linventory->returnArray($sql);
    } else {
        header("location: import_write_off_item.php?returnMsgKey=ImportUnsuccess_NoRecord&format=2");
        exit();
    }
    
    $validRow = 0;
    $invalidRow = 0;
    $rowNo = '';
    $delim = '';
    $table_content = '';
    $newItem = array();
    $exist_item = array();
    $exist_write_off_item = array();
    $error_code = array();
    $error_css = "style=\"color:red\"";
    
    // 5D array
    $exist_item = BuildMultiKeyAssoc($arr_exist_item, array(
        'ItemCode',
        'LocationID',
        'GroupInCharge',
        'FundingSourceID'
    ), 'Quantity');
    
    $exist_write_off_item = BuildMultiKeyAssoc($arr_exist_write_off_item, array(
        'ItemCode',
        'LocationID',
        'AdminGroupID',
        'FundingSourceID'
    ), 'WriteOffQty');
    
    for ($i = 0, $iMax = count($arr_new_data); $i < $iMax; $i ++) {
        $error = false;
        list ($item_code, $item_chi_name, $item_eng_name, $item_location_code, $item_sub_location_code, $item_funding_source_code, $item_group_code, $item_qty, $item_write_off_reason, $item_request_date, $item_approve_date, $item_remark) = $arr_new_data[$i];
        
        // check item code
        if (isset($exist_item[$item_code]) && in_array($item_code, (array) $item_code_array)) {
            
            // check location
            if (in_array($item_location_code, (array) $location_code_array)) {
                
                // check sub-location
                if (isset($exist_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]]) && in_array($item_sub_location_code, (array) $sub_location_code_array)) {
                    
                    // check group
                    if (isset($exist_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]][$group_array[$item_group_code]]) && in_array($item_group_code, (array) $group_code_array)) {
                        
                        // check funding source
                        if (isset($exist_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]][$group_array[$item_group_code]][$funding_source_array[$item_funding_source_code]]) && in_array($item_funding_source_code, (array) $funding_source_code_array)) {
                            
                            if (is_numeric($item_qty) && ($item_qty > 0)) {
                                
                                // check qty
                                if (isset($exist_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]][$group_array[$item_group_code]][$funding_source_array[$item_funding_source_code]]['Quantity'])) {
                                    $exist_quantity = $exist_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]][$group_array[$item_group_code]][$funding_source_array[$item_funding_source_code]]['Quantity'];
                                    if (isset($exist_write_off_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]][$group_array[$item_group_code]][$funding_source_array[$item_funding_source_code]]['WriteOffQty'])) {
                                        $exist_write_off_qty = $exist_write_off_item[$item_code][$sub_location_array[$item_sub_location_code][$item_location_code]][$group_array[$item_group_code]][$funding_source_array[$item_funding_source_code]]['WriteOffQty'];
                                    } else {
                                        $exist_write_off_qty = 0;
                                    }
                                    
                                    if ($item_qty > $exist_quantity - $exist_write_off_qty) {
                                        $error_code[$i][] = 8; // Item qty > available qty in that location for specific group with specific funding source
                                        $error = true;
                                    }
                                } else {
                                    $error_code[$i][] = 9; // Item qty does not exist
                                    $error = true;
                                }
                            } else {
                                $error_code[$i][] = 10; // Item qty is not numeric
                                $error = true;
                            }
                        } else {
                            $error_code[$i][] = 11; // Item funding source code invalid
                            $error = true;
                        }
                    } else {
                        $error_code[$i][] = 12; // Item group code invalid
                        $error = true;
                    }
                } else {
                    $error_code[$i][] = 13; // Item sub-location code invalid
                    $error = true;
                }
            } else {
                $error_code[$i][] = 14; // Item location code invalid
                $error = true;
            }
        } else {
            $error_code[$i][] = 1; // Item does not exist
            $error = true;
        }
        
        if (trim($item_write_off_reason) == '') {
            $error_code[$i][] = 2; // Write-off reason is empty
            $error = true;
        } else {
            if (! in_array($item_write_off_reason, (array) $write_off_reason_array)) {
                $error_code[$i][] = 7; // Write-off reason not in list
                $error = true;
            }
        }
        
        if (! checkDateIsValid(getDefaultDateFormat($item_request_date))) {
            $error_code[$i][] = 3; // Request Date is empty or invalid
            $error = true;
        }
        
        if (! checkDateIsValid(getDefaultDateFormat($item_approve_date))) {
            $error_code[$i][] = 4; // Approve Date is empty or invalid
            $error = true;
        }
        
        if ($error) {
            $invalidRow ++;
            $rowNo .= $delim . ($i + 1);
            $delim = ", ";
        } else {
            $validRow ++;
            
            // add valid data to db
            $item_chi_name = addslashes(intranet_htmlspecialchars($item_chi_name));
            $item_eng_name = addslashes(intranet_htmlspecialchars($item_eng_name));
            $item_code = intranet_htmlspecialchars(addslashes($item_code));
            $item_location_code = intranet_htmlspecialchars(addslashes($item_location_code));
            $item_sub_location_code = intranet_htmlspecialchars(addslashes($item_sub_location_code));
            $item_group_code = intranet_htmlspecialchars(addslashes($item_group_code));
            $item_funding_source_code = intranet_htmlspecialchars(addslashes($item_funding_source_code));
            $item_qty = intranet_htmlspecialchars(addslashes($item_qty));
            $item_write_off_reason = intranet_htmlspecialchars($item_write_off_reason);
            $item_request_date = getDefaultDateFormat($item_request_date);
            $item_approve_date = getDefaultDateFormat($item_approve_date);
            $item_remark = intranet_htmlspecialchars(addslashes($item_remark));
            
            $importAry[] = " (2,'" . $item_chi_name . "', '" . $item_eng_name . "', '" . $item_code . "', '" . $item_location_code . "', '" . $item_sub_location_code . "', '" . $item_group_code . "', '" . $item_funding_source_code . "', '" . $location_array[$item_location_code] . "', '" . $sub_location_array[$item_sub_location_code][$item_location_code] . "', '" . $group_array[$item_group_code] . "', '" . $funding_source_array[$item_funding_source_code] . "', '" . $item_qty . "', '" . $item_write_off_reason . "', '" . $item_request_date . "', '" . $item_approve_date . "', '" . $item_remark . "') ";
        }
    } // loop number of rows in data
      
    // insert record in TEMP table
    if (count($importAry) > 0) {
        $field = "ItemType, NameChi, NameEng, ItemCode, LocationLevelCode, LocationCode, GroupInChargeCode, FundingSourceCode, LocationLevelID, LocationID, GroupInChargeID, FundingSourceID, Quantity, WriteOffReason, RequestDate, ApproveDate, ItemRemark";
        $insertChunkAry = array_chunk($importAry, 1000);
        $numOfChunk = count($insertChunkAry);
        
        for ($i = 0; $i < $numOfChunk; $i ++) {
            $_insertAry = $insertChunkAry[$i];
            $sql = "INSERT INTO TEMP_INVENTORY_WRITE_OFF_ITEM ($field) VALUES " . implode(', ', $_insertAry);
            $result = $linventory->db_db_query($sql);
        }
    }
    
    // Need to check if quantity is enough to write-off
    if ($invalidRow == 0) {
        $sql = "SELECT 
						a.ItemCode, a.LocationCode, a.GroupInChargeCode, a.FundingSourceCode
				FROM
				(
					SELECT 
							t.ItemCode, t.LocationCode, t.GroupInChargeCode, t.FundingSourceCode, 
							b.LocationID, b.GroupInCharge, b.FundingSourceID, b.Quantity, t.Quantity AS ImportWriteOffQty
					FROM 
							INVENTORY_ITEM i
					INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION b ON b.ItemID=i.ItemID
					INNER JOIN 
							TEMP_INVENTORY_WRITE_OFF_ITEM t ON t.LocationID=b.LocationID
							AND t.GroupInChargeID=b.GroupInCharge
							AND t.FundingSourceID=b.FundingSourceID
					WHERE 
							t.ItemCode=i.ItemCode
					AND		
							i.ItemType = '" . ITEM_TYPE_BULK . "') AS a
				INNER JOIN 
				(
					SELECT 
							i.ItemCode, w.LocationID, w.AdminGroupID, w.FundingSourceID, SUM(w.WriteOffQty) AS WriteOffQty 
					FROM 
							INVENTORY_ITEM i 
					INNER JOIN 
							INVENTORY_ITEM_WRITE_OFF_RECORD w ON w.ItemID=i.ItemID 
					INNER JOIN 
							TEMP_INVENTORY_WRITE_OFF_ITEM t ON t.LocationID=w.LocationID
							AND t.GroupInChargeID=w.AdminGroupID
							AND t.FundingSourceID=w.FundingSourceID
					WHERE 
							t.ItemCode=i.ItemCode
					AND
							i.ItemType = '" . ITEM_TYPE_BULK . "' 
					AND 
							w.RecordStatus='0' 
					GROUP BY i.ItemCode, w.LocationID, w.AdminGroupID, w.FundingSourceID) AS b 
				ON 
					a.ItemCode=b.Itemcode
				AND 
					a.LocationID=b.LocationID
				AND 
					a.GroupInCharge=b.AdminGroupID
				AND 
					a.FundingSourceID=b.FundingSourceID
				WHERE a.Quantity-b.WriteOffQty < a.ImportWriteOffQty";
        $check_qty = $linventory->returnArray($sql);
        $no_check_qty = count($check_qty);
        if ($no_check_qty > 0) {
            $invalidRow += $no_check_qty;
        }
    } else {
        $no_check_qty = 0;
    }
    
    // show validate result
    if ($invalidRow > 0) {
        $invalidRowMsg = $Lang['eInventory']['RowLine1'] . " " . $rowNo . " " . $Lang['eInventory']['RowLine2'] . " " . $Lang['eInventory']['InvalidRecordRow'];
        $invalidRowMsg = $Lang['eInventory']['InvalidRecord'] . " : <span $error_css> $invalidRow ($invalidRowMsg)</span>";
    }
    
    $table_content .= "<tr>";
    $table_content .= "<td class=\"tabletext\" colspan=\"" . ($invalidRow > 0 ? '14' : '13') . "\">
							$i_InventorySystem_ImportItem_TotalRow : $file_original_row<br>
							" . $Lang['eInventory']['ValidRecord'] . " : $validRow<br>
							$invalidRowMsg							
						</td>";
    $table_content .= "</tr>\n";
    
    $table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">#</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseName</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishName</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_LocationCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location2Code</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_GroupCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_FundingSourceCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Reason</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
    if ($invalidRow > 0) {
        $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
    }
    $table_content .= "</tr>\n";
    
    for ($i = 0, $iMax = count($arr_new_data); $i < $iMax; $i ++) {
        $item_code_css = '';
        $write_off_reason_css = '';
        $request_date_css = '';
        $approve_date_css = '';
        $write_off_qty_css = '';
        $funding_source_css = '';
        $group_css = '';
        $sub_location_css = '';
        $location_css = '';
        $error_cell = '';
        
        list ($item_code, $item_chi_name, $item_eng_name, $item_location_code, $item_sub_location_code, $item_funding_source_code, $item_group_code, $item_qty, $item_write_off_reason, $item_request_date, $item_approve_date, $item_remark) = $arr_new_data[$i];
        
        if ($no_check_qty > 0) {
            for ($j = 0, $jMax = count($check_qty); $j < $jMax; $j ++) {
                $cur_check = $check_qty[$j];
                if (($cur_check['ItemCode'] == $item_code) && ($cur_check['LocationCode'] == $item_sub_location_code) && ($cur_check['GroupInChargeCode'] == $item_group_code) && ($cur_check['FundingSourceCode'] == $item_funding_source_code)) {
                    $error_code[$i][] = 8; // Item qty > available qty in that location for specific group with specific funding source
                }
            }
        }
        
        if ($invalidRow > 0) {
            $item_error = "";
            if (count($error_code[$i]) > 0) {
                $delimiter = "- ";
                for ($j = 0, $jMax = count($error_code[$i]); $j < $jMax; $j ++) {
                    $item_error .= $delimiter . $Lang['eInventory']['ImportWriteoffError'][$error_code[$i][$j]];
                    $delimiter = "<br>- ";
                    
                    switch ($error_code[$i][$j]) {
                        case 1:
                            $item_code_css = $error_css;
                            break;
                        case 2:
                        case 7:
                            $write_off_reason_css = $error_css;
                            break;
                        case 3:
                            $request_date_css = $error_css;
                            break;
                        case 4:
                            $approve_date_css = $error_css;
                            break;
                        case 8:
                        case 9:
                        case 10:
                            $write_off_qty_css = $error_css;
                            break;
                        case 11:
                            $funding_source_css = $error_css;
                            break;
                        case 12:
                            $group_css = $error_css;
                            break;
                        case 13:
                            $sub_location_css = $error_css;
                            break;
                        case 14:
                            $location_css = $error_css;
                            break;
                    }
                }
            }
            $error_cell = "<td class=\"tabletext\">$item_error</td>";
        }
        
        $j = $i + 1;
        $table_row_css = " class=\"tablerow" . (($j % 2 == 0) ? "1" : "2") . "\" ";
        
        $table_content .= "<tr $table_row_css><td class=\"tabletext\">$j</td>";
        $table_content .= "<td class=\"tabletext\" $item_code_css>" . (empty($item_code) ? "***" : $item_code) . "</td>";
        $table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
        $table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
        $table_content .= "<td class=\"tabletext\" $location_css>" . (empty($item_location_code) ? "***" : $item_location_code) . "</td>";
        $table_content .= "<td class=\"tabletext\" $sub_location_css>" . (empty($item_sub_location_code) ? "***" : $item_sub_location_code) . "</td>";
        $table_content .= "<td class=\"tabletext\" $group_css>" . (empty($item_group_code) ? "***" : $item_group_code) . "</td>";
        $table_content .= "<td class=\"tabletext\" $funding_source_css>" . (empty($item_funding_source_code) ? "***" : $item_funding_source_code) . "</td>";
        $table_content .= "<td class=\"tabletext\" $write_off_qty_css>" . (empty($item_qty) ? "***" : $item_qty) . "</td>";
        $table_content .= "<td class=\"tabletext\" $write_off_reason_css>" . (empty($item_write_off_reason) ? "***" : $item_write_off_reason) . "</td>";
        $table_content .= "<td class=\"tabletext\" $request_date_css>" . (empty($item_request_date) ? "***" : $item_request_date) . "</td>";
        $table_content .= "<td class=\"tabletext\" $approve_date_css>" . (empty($item_approve_date) ? "***" : $item_approve_date) . "</td>";
        $table_content .= "<td class=\"tabletext\">$item_remark</td>";
        $table_content .= $error_cell;
        $table_content .= "</tr>";
    }
    $table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"100%\"></td></tr>\n";
    $table_content .= "<tr><td colspan=100% align=center>";
    if ($invalidRow <= 0) {
        $table_content .= $linterface->GET_ACTION_BTN($button_submit, "submit", "");
    }
    $table_content .= " " . $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_write_off_item.php?format=2'", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>\n";
}

$TAGS_OBJ[] = array(
    $button_import,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// ## step display
$STEPS_OBJ[] = array(
    $Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'],
    0
);
$STEPS_OBJ[] = array(
    $Lang['eInventory']['ImportStep'][2],
    1
);
$STEPS_OBJ[] = array(
    $Lang['General']['ImportArr']['ImportStepArr']['ImportResult'],
    0
);

?>

<br>

<form name="form1" action="import_write_off_item_update.php"
	method="post">
	<table border="0" width="99%" cellspacing="0" cellpadding="0">
<?=$infobar?>
</table>
	<br>
	<table border="0" width="96%" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="<?=($invalidRow > 0 ? '14':'13')?>"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
		</tr>
<?=$table_content;?>
</table>
	<input type="hidden" name="format" id="format" value=<?=$format;?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
