<?php
// modifying : 

// if you want to upload it to 149 or client, pls update write_off_item_export.php & write_off_item_print.php as well!

// ########################################
// Date : 2020-09-18 (Tommy)
// fix single item $ItemTotal not output 0 if purchased_price is not 0
//
// Date : 2019-06-18 (Tommy)
// add "Quotation No" and "Maintenance Details" column 
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// - add access right checking [Case#E135442]
//
// Date: 2017-11-28 Cameron
// - set z-index to status_option layer so that it can float on top in PowerClass
//
// Date: 2016-11-02 Henry [case#Z107197]
// - allow view this page when group leader not allow writeoff item
//
// Date: 2016-02-19 Cameron [case#E77178]
// - add linking to import_write_off_item.php (import write-off record with RequestDate and ApprovedDate)
// - correct path of 10x10.gif dotline image
//
// Date: 2015-10-09 Henry [Case#W87058] #ip.2.5.6.10.1
// fixed: missing purchase date of bulk items
//
// Date: 2015-09-18 Henry [case# K85540] #ip.2.5.6.10.1
// show archived user's name
//
// Date: 2014-12-08 Henry [case# E72413] #ip.2.5.5.12.1
// add targetItemType filter
// modified the sorting of table records
//
// Date: 2014-11-06 Henry
// fixed: Export write off approval - Item price incorrect [case# P70944] #ip.2.5.5.12.1
// $ItemTotal = $Cost_Ary[ITEM_SCHOOL_FUNDING] + $Cost_Ary[ITEM_GOVERNMENT_FUNDING] + $Cost_Ary[ITEM_SPONSORING_BODY_FUNDING]
// WriteOffQty -> SUM(WriteOffQty)
//
// Date: 2013-07-25 YatWoon
// add filter and restore function
//
// Date: 2013-01-16 YatWoon
// Fixed: display item's funding source instead of check with write-off record's fundiont source [Case#:2013-0110-1149-03156]
//
// Date: 2012-07-10 YatWoon
// Enhanced: support 2 funding source for single item
//
// Date: 2012-05-25 YatWoon
// add "Funding" column
//
// Date: 2012-03-07 YatWoon
// fixed: failed to retrieve fields selection
//
// Date: 2011-07-21 YatWoon
// add "Sponsoring body" for funding type
// update the "Cost" field, government/school/sponsoring cost should be according to funding src category (not ownership)
//
// Date: 2011-06-23 (Yuen)
// corrected the initial quantity of bulk item by not counting the pending write-off items
//
// Date: 2011-06-02 (Yuen)
// calculate price for bulk item base on average unit price * quantity
//
// Date: 2011-05-27 Yuen
// add funding cost in selection fields for display, print and export
//
// Date: 2011-05-25 YatWoon
// Fixed: Incorrect write-off attachment
// Improved: Add checking for "Do not allow Group Leader to write-off items"
//
// Date: 2011-04-21 YatWoon
// add search function
//
// Date: 2011-04-07 Yuen
// allow user to select fields for display, print and export
//
// Date: 2011-03-09 YatWoon
// Fixed: cannot display requested records.
// Display "User account not exists." if requester is deleted.
//
// ##########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// preset fields for different customizations and general
if (! isset($FieldLocationTmp) && ! $sys_custom['eInventoryCustForHKBTS']) {
    $FieldLocation = true;
}
if (! isset($FieldCaretakerTmp)) {
    $FieldCaretaker = true;
}

if (! isset($FieldPurchaseDateTmp) && ($sys_custom['eInventoryCustForSMC'] || $sys_custom['eInventoryCustForHKBTS'])) {
    $FieldPurchaseDate = true;
}

if (! isset($FieldFundingCostTmp) && $sys_custom['eInventoryCustForSMC']) {
    $FieldFundingCost = true;
}
if (! isset($FieldDescriptionTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldDescription = true;
}

if (! isset($FieldSerialNumberTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldSerialNumber = true;
}
if (! isset($FieldLicenseModelTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldLicenseModel = true;
}
if (! isset($FieldInvoiceTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldInvoice = true;
}
if (! isset($FieldUnitPriceTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldUnitPrice = true;
}
if (! isset($FieldFundingSourceTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldFundingSource = true;
}

if(! isset($FieldQuotationNoTmp) && $sys_custom['eInventoryCustForHKBTS']){
    $FieldQuotationNo = true;
}

if(! isset($FieldMaintainInfoTmp) && $sys_custom['eInventoryCustForHKBTS']){
    $FieldMaintainInfo = true;
}

$FieldCategoryChecked = ($FieldCategory) ? "checked='checked'" : "";
$FieldDescriptionChecked = ($FieldDescription) ? "checked='checked'" : "";
$FieldLocationChecked = ($FieldLocation) ? "checked='checked'" : "";
$FieldCaretakerChecked = ($FieldCaretaker) ? "checked='checked'" : "";
$FieldPurchaseDateChecked = ($FieldPurchaseDate) ? "checked='checked'" : "";
$FieldInvoiceChecked = ($FieldInvoice) ? "checked='checked'" : "";
$FieldSerialNumberChecked = ($FieldSerialNumber) ? "checked='checked'" : "";
$FieldLicenseModelChecked = ($FieldLicenseModel) ? "checked='checked'" : "";
$FieldUnitPriceChecked = ($FieldUnitPrice) ? "checked='checked'" : "";
$FieldTotalPriceChecked = ($FieldTotalPrice) ? "checked='checked'" : "";
$FieldFundingCostChecked = ($FieldFundingCost) ? "checked='checked'" : "";
$FieldFundingSourceChecked = ($FieldFundingSource) ? "checked='checked'" : "";
$FieldQuotationNoChecked = ($FieldQuotationNo) ? "checked = 'checked'": "";
$FieldMaintainInfoChecked = ($FieldMaintainInfo) ? "checked = 'checked'": "";



// for print and export
$selected_fields = "dummy=1";
$selected_fields .= ($FieldCategory) ? "&FieldCategory=1" : "";
$selected_fields .= ($FieldDescription) ? "&FieldDescription=1" : "";
$selected_fields .= ($FieldLocation) ? "&FieldLocation=1" : "";
$selected_fields .= ($FieldCaretaker) ? "&FieldCaretaker=1" : "";
$selected_fields .= ($FieldPurchaseDate) ? "&FieldPurchaseDate=1" : "";
$selected_fields .= ($FieldInvoice) ? "&FieldInvoice=1" : "";
$selected_fields .= ($FieldSerialNumber) ? "&FieldSerialNumber=1" : "";
$selected_fields .= ($FieldLicenseModel) ? "&FieldLicenseModel=1" : "";
$selected_fields .= ($FieldUnitPrice) ? "&FieldUnitPrice=1" : "";
$selected_fields .= ($FieldTotalPrice) ? "&FieldTotalPrice=1" : "";
$selected_fields .= ($FieldFundingCost) ? "&FieldFundingCost=1" : "";
$selected_fields .= ($FieldFundingSource) ? "&FieldFundingSource=1" : "";
$selected_fields .= ($FieldQuotationNo) ? "&FieldQuotationNo=1" : "";
$selected_fields .= ($FieldMaintainInfo) ? "&FieldMaintainInfo=1" : "";
$selected_fields .= "&keyword=" . $keyword;
$selected_fields .= "&RecordStatus=" . $RecordStatus;
$selected_fields .= "&targetItemType=" . $targetItemType;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_WriteOffApproval";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// get group leader #
if ($linventory->IS_ADMIN_USER($UserID)) {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
} else {
    $groupLeaderNowAllowWriteOffItem = $linventory->retriveGroupLeaderNotAllowWriteOffItem();
    // if(!$groupLeaderNowAllowWriteOffItem)
    // {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."' AND RecordType = 1";
    // $arr_admin_group_leader = $linventory->returnVector($sql);
    // }
}
$arr_admin_group_leader = $linventory->returnVector($sql);

if (sizeof($arr_admin_group_leader) > 0) {
    $target_group_list = implode(",", $arr_admin_group_leader);
}
// end #

// if($target_group_list == "")
// {
// header("location: ../inventory/items_full_list.php");
// exit();
// }

$TAGS_OBJ[] = array(
    $i_InventorySystem_WriteOffItemApproval,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Generate System Message #
if ($xmsg != "") {
    $SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
    if ($msg == 1)
        $SysMsg = $linterface->GET_SYS_MSG("add");
    if ($msg == 2)
        $SysMsg = $linterface->GET_SYS_MSG("update");
    if ($msg == 3)
        $SysMsg = $linterface->GET_SYS_MSG("delete");
    if ($msg == 12)
        $SysMsg = $linterface->GET_SYS_MSG("add_failed");
    if ($msg == 13)
        $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
    if ($msg == 14)
        $SysMsg = $linterface->GET_SYS_MSG("update_failed");
    if ($msg == - 1)
        $SysMsg = $linterface->GET_SYS_MSG("", $Lang['eInventory']['ReturnMessage']['DeleteUnsuccessNotEnoughQty']);
}
// End #

// Create Search box #
$searchbox .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\">&nbsp;";
$searchbox .= "<input class=\"textboxnum\" type=\"text\" id=\"keyword\" name=\"keyword\" maxlength=\"50\" value=\"" . stripslashes($keyword) . "\"  onKeyPress=\"return submitenter(this,event)\">&nbsp;\n";
// End #
$search_option .= $searchbox;

$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()", "", "", "", "", 0) . "&nbsp;&nbsp;&nbsp;";
if (! $groupLeaderNowAllowWriteOffItem) {
    $toolbar .= $linterface->GET_LNK_IMPORT("import_write_off_item.php", "", "", "", "", 0) . "&nbsp;";
}
$toolbar .= $linterface->GET_LNK_EXPORT("write_off_item_export.php?" . $selected_fields, "", "", "", "", 0);

// filtering #
$filterbar = "<select name='RecordStatus' onChange='document.form1.submit();'>";
$filterbar .= "<option value='0' " . ($RecordStatus < 1 ? "selected" : "") . ">" . $Lang['eInventory']['WaitingForApproval'] . "</option>";
$filterbar .= "<option value='3'" . ($RecordStatus > 0 ? "selected" : "") . ">" . $Lang['eInventory']['Handled'] . "</option>";
$filterbar .= "</select> ";

$filterbar .= getSelectByArray($i_InventorySystem_ItemType_Array, "name=\"targetItemType\" onChange=\"document.form1.submit();\"", $targetItemType, 1, 0, "$i_InventorySystem_ItemType_All");
// End #

// Generate Table Tool Bar #
if ($RecordStatus == 0) // Approve / Reject
{
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkApprove(document.form1,'RecordID[]','write_off_item_approve.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_approve
						</a>
					</td>";
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkReject(document.form1,'RecordID[]','write_off_item_reject.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_reject
						</a>
					</td>";
} else {
    $table_tool .= "<td nowrap=\"nowrap\">
				<a href=\"javascript:checkRestore(document.form1,'RecordID[]','write_off_item_restore.php')\" class=\"tabletool\">
					<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_undo_b.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
					$button_restore
				</a>
			</td>";
}
// End #

// ## Set SQL Condition - Keyword ###
if ($keyword != "") {
    $cond .= " AND ";
    
    $cond .= " ((b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
				(b.ItemCode LIKE '%$keyword%') 
				)";
}

$namefield1 = getNameFieldByLang2("f.");
$namefield2 = getNameFieldByLang2("f2.");
$namefield3 = getNameFieldByLang2("f3.");

$archivename1 = "(select " . getNameFieldByLang2("af.") . " from INTRANET_ARCHIVE_USER as af WHERE a.RequestPerson = af.UserID)";
$archivename2 = "(select " . getNameFieldByLang2("af2.") . "from INTRANET_ARCHIVE_USER as af2 WHERE a.ApprovePerson = af2.UserID)";
$archivename3 = "(select " . getNameFieldByLang2("af3.") . "from INTRANET_ARCHIVE_USER as af3 WHERE a.RejectPerson = af3.UserID)";

$cond_RecordStatus = $RecordStatus > 0 ? "a.RecordStatus <> 0" : "a.RecordStatus = 0";
// #### handling $targetItemType
$cond_ItemType = $targetItemType > 0 ? " AND b.ItemType =  $targetItemType " : "";

// #### handling ordering
$order = $RecordStatus > 0 ? "if(a.RecordStatus=1,a.ApproveDate,a.RejectDate) desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder" : "a.RequestDate desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder";

$sql = "SELECT  
				a.RecordID,
				a.ItemID,
				b.ItemType,
				b.ItemCode,
				" . $linventory->getInventoryItemNameByLang("b.") . " AS ItemName,
				a.RequestDate,
				a.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") AS ItemLocation,
				a.AdminGroupID,
				" . $linventory->getInventoryNameByLang("d.") . " AS MgmtGroup,
				" . $linventory->getInventoryNameByLang("funding.") . ",
				a.WriteOffQty,
				a.WriteOffReason,
				a.RequestPerson,
				if (b.DescriptionEng<>'', DescriptionEng, DescriptionChi) AS ItemDescription,
				if($namefield1 is null or trim($namefield1)='', IF($archivename1 IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>',$archivename1)), $namefield1) AS Requestor,
				if(a.RecordStatus=1,'" . $i_Discipline_System_Award_Punishment_Approved . "','" . $i_Discipline_System_Award_Punishment_Rejected . "'),
				if(a.RecordStatus=1,a.ApproveDate,a.RejectDate),
				if(a.RecordStatus=1, 
					if(f2.UserID is NULL, IF($archivename2 IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>',$archivename2)), $namefield2),
					if(a.RejectPerson is NULL, ' ',if(f3.UserID is NULL, IF($archivename3 IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>',$archivename3)), $namefield3))
				)
		FROM
				INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID AND a.AdminGroupID IN ($target_group_list)) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS g ON (c.LocationLevelID = g.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (g.BuildingID = building.BuildingID) 
				left join INTRANET_USER AS f ON (a.RequestPerson = f.UserID)	
				left join INTRANET_USER AS f2 ON (a.ApprovePerson = f2.UserID) 
				left join INTRANET_USER AS f3 ON (a.RejectPerson = f3.UserID) 	
				left join INVENTORY_ITEM_SINGLE_EXT as s on s.ItemID=a.ItemID
				left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=s.FundingSource)
		WHERE
				$cond_RecordStatus
				$cond_ItemType
				$cond
		ORDER BY 
				$order
		";
$arr_result = $linventory->returnArray($sql);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $itemIDs_found .= (($itemIDs_found == "") ? "" : ",") . $arr_result[$i]['ItemID'];
    }
}

// find all itemIDs
if (trim($itemIDs_found) != "") {
    // find category and sub-category if needed
    $sql_others = "SELECT ii.ItemID, CONCAT(" . $linventory->getInventoryNameByLang("ic.") . ",' > '," . $linventory->getInventoryNameByLang("icl.") . ") AS ItemCategory
					FROM INVENTORY_ITEM AS ii
						INNER JOIN INVENTORY_CATEGORY AS ic ON ii.CategoryID = ic.CategoryID
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS icl ON ii.Category2ID = icl.Category2ID
					WHERE ii.ItemID IN ($itemIDs_found) ";
    
    $arr_result_others = $linventory->returnArray($sql_others);
    
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['ItemCategory'] = $arr_result_others[$i]['ItemCategory'];
        }
    }
    
    // single
    $sql_single = "SELECT 
					iise.ItemID, 
					iise.InvoiceNo, 
					iise.UnitPrice, 
					iise.PurchaseDate, 
					iise.FundingSource, 
					iise.SerialNumber, iise.SoftwareLicenseModel, 
					" . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , 
					" . $linventory->getInventoryNameByLang("ifs2.") . " AS FundingSrcName2 , 
					ifs.FundingType as FundingType1,
					iise.UnitPrice1,
					iise.UnitPrice2,
					ifs2.FundingType as FundingType2,
                    iise.QuotationNo,
                    iise.MaintainInfo
					FROM 
						INVENTORY_ITEM_SINGLE_EXT AS iise
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (iise.FundingSource = ifs.FundingSourceID)
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs2 ON (iise.FundingSource2 = ifs2.FundingSourceID)
					WHERE iise.ItemID IN ($itemIDs_found) ";
    $arr_result_others = $linventory->returnArray($sql_single);
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['PurchaseDate'] = $arr_result_others[$i]['PurchaseDate'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['InvoiceNo'] = $arr_result_others[$i]['InvoiceNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice'] = $arr_result_others[$i]['UnitPrice'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice1'] = $arr_result_others[$i]['UnitPrice1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice2'] = $arr_result_others[$i]['UnitPrice2'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSrcName'] = $arr_result_others[$i]['FundingSrcName'] . ($arr_result_others[$i]['FundingSrcName2'] ? ", " . $arr_result_others[$i]['FundingSrcName2'] : "");
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['SerialNumber'] = $arr_result_others[$i]['SerialNumber'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['LicenseModel'] = $arr_result_others[$i]['SoftwareLicenseModel'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType'] = $arr_result_others[$i]['FundingType1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType2'] = $arr_result_others[$i]['FundingType2'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['QuotationNo'] = $arr_result_others[$i]['QuotationNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['MaintainInfo'] = $arr_result_others[$i]['MaintainInfo'];
        }
    }
}

$ShowField['Category'] = $FieldCategory;
$ShowField['Description'] = $FieldDescription;
$ShowField['Location'] = $FieldLocation;
$ShowField['Caretaker'] = $FieldCaretaker;
$ShowField['PurchaseDate'] = $FieldPurchaseDate;
$ShowField['Invoice'] = $FieldInvoice;
$ShowField['SerialNumber'] = $FieldSerialNumber;
$ShowField['LicenseModel'] = $FieldLicenseModel;
$ShowField['UnitPrice'] = $FieldUnitPrice;
$ShowField['TotalPrice'] = $FieldTotalPrice;
$ShowField['FundingCost'] = $FieldFundingCost;
$ShowField['FundingSource'] = $FieldFundingSource;
$ShowField['QuotationNo'] = $FieldQuotationNo;
$ShowField['MaintainInfo'] = $FieldMaintainInfo;

if ($ShowField['FundingCost']) {
    $rowspan = "rowspan='2'";
}

$fieldsTotal = 3; // item code, item name & quantity
$fieldsTotalEnd = 6; // Write-off Reason, Requestor, Request Date, Supervisor's / Principal's Signature
$table_content .= "<tr class=\"tabletop\">";
$table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Item_Code</td>";
if ($ShowField['Category']) {
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem['Category'] . "</td>";
    $fieldsTotal ++;
}
$table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Item_Name</td>";
if ($ShowField['Description']) {
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Item_Description</td>";
    $fieldsTotal ++;
}
if ($ShowField['Location']) {
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Item_Location</td>";
    $fieldsTotal ++;
}
if ($ShowField['Caretaker']) {
    $group_new_title = ($sys_custom['eInventoryCustForSMC']) ? "Team / Department" : $i_InventorySystem['Caretaker'];
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>" . $group_new_title . "</td>";
    $fieldsTotal ++;
}
// $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>".$i_InventorySystem['FundingSource']."</td>";
if ($ShowField['PurchaseDate']) {
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem_Report_Col_Purchase_Date . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['Invoice']) {
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem_Item_Invoice . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['SerialNumber']) {
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem_Item_Serial_Num . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['LicenseModel']) {
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem_Category2_License . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['FundingCost']) {
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" colspan='3' style='border-bottom:1px #CCCCCC solid;'>" . $i_InventorySystem_Report_Col_Cost . "</td>";
    $fieldsTotal ++;
    $fieldsTotal ++;
    $fieldsTotal ++;
}
if ($ShowField['FundingSource']) {
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem['FundingSource'] . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['UnitPrice']) {
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem_Unit_Price . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['QuotationNo']){
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystem_Item_Quot_Num. "</td>";
    $fieldsTotal ++;
}
if ($ShowField['MaintainInfo']){
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $i_InventorySystemItemMaintainInfo . "</td>";
    $fieldsTotal ++;
}
$table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Write_Off_RequestQty</td>";
if ($ShowField['TotalPrice']) {
    $table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>" . $Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'] . "</td>";
}
$table_content .= "<td width=\"15%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Write_Off_Reason</td>";
$table_content .= "<td class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Write_Off_Attachment</td>";
$table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_RequestPerson</td>";
$table_content .= "<td width=\"5%\" class=\"tabletopnolink\" $rowspan>$i_InventorySystem_Write_Off_RequestTime</td>";
// $table_content .= "<td width=\"5%\" class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Attachment</td>";
if ($RecordStatus == 3) {
    $fieldsTotal += 3;
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>" . $Lang['eInventory']['ApprovedBy'] . "</td>";
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>" . $Lang['eInventory']['ApprovedDate'] . "</td>";
    $table_content .= "<td width=\"10%\" class=\"tabletopnolink\" $rowspan>" . $Lang['eInventory']['ApprovedStatus'] . "</td>";
}
if (! $groupLeaderNowAllowWriteOffItem) {
    $table_content .= "<td class=\"tabletopnolink\" $rowspan><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'RecordID[]'):setChecked(0,this.form,'RecordID[]')\"></td>";
}
$table_content .= "</tr>";

if ($ShowField['FundingCost']) {
    $table_content .= "<tr class=\"tabletop\">
		<td class=\"tabletopnolink\">" . $Lang['eInventory']['FundingType']['School'] . "</td>
		<td class=\"tabletopnolink\">" . $Lang['eInventory']['FundingType']['Government'] . "</td>
		<td class=\"tabletopnolink\">" . $Lang['eInventory']['FundingType']['SponsoringBody'] . "</td></tr>";
}

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $item_now = $arr_result[$i];
        $symbol_for_null = ($item_now['ItemType'] != ITEM_TYPE_SINGLE) ? "n/a" : "--";
        
        $thisLocatioinID = $item_now['LocationID'];
        $thisItemID = $item_now['ItemID'];
        
        // found out funding type (funding type of single item is retrieved above)
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            $sql_bulk = "SELECT " . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , ifs.FundingType
							FROM INVENTORY_ITEM_BULK_LOCATION AS a
								 LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (a.FundingSourceID = ifs.FundingSourceID)
							WHERE a.ItemID = $thisItemID and a.LocationID=$thisLocatioinID ";
            $arr_result_others = $linventory->returnArray($sql_bulk);
            if (sizeof($arr_result_others) > 0) {
                $thisFundingSrcName = $arr_result_others[0]['FundingSrcName'];
                $thisFundingType = $arr_result_others[0]['FundingType'];
            }
            
            $sql_bulk = "SELECT PurchaseDate FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$thisItemID."' AND PurchaseDate>0 ORDER BY PurchaseDate";
            $arr_result_others = $linventory->returnArray($sql_bulk);
            $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] = (count($arr_result_others) > 1 ? '<font style="color:red">^</font>' : '') . $arr_result_others[0]['PurchaseDate'];
        } else {
            $purchased_price = $ItemOtherInfo[$thisItemID]['PurchasedPrice'];

            $thisFundingSrcName = $ItemOtherInfo[$thisItemID]['FundingSrcName'];
            $thisFundingType = $ItemOtherInfo[$thisItemID]['FundingType'];
        }
        
        if ($item_now['ItemType'] == ITEM_TYPE_BULK && $AVG_UNIT_PRICE[$item_now['ItemID']] == '') {
            // find the average unit price
            $total_school_cost = 0;
            $final_unit_price = 0;
            $final_purchase_price = 0;
            $total_qty = 0;
            $written_off_qty = 0;
            $ini_total_qty = 0;
            
            // # get Total purchase price ##
            // $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
            $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_now['ItemID'] . "' AND Action = 1";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($total_purchase_price) = $tmp_result[0];
            }
            
            // # get Total Qty Of the target Item ##
            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_now['ItemID']."'";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($totalQty) = $tmp_result[0];
            }
            
            // # get written-off qty ##
            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_now['ItemID']."'";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($written_off_qty) = $tmp_result[0];
            }
            $ini_total_qty = $totalQty + $written_off_qty;
            
            if ($ini_total_qty > 0)
                $AVG_UNIT_PRICE[$item_now['ItemID']] = round($total_purchase_price / $ini_total_qty, 2);
        }
        
        if (trim($item_now['ItemDescription']) == "") {
            $item_now['ItemDescription'] = "--";
        }
        
        if ($ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] = $symbol_for_null;
        }
        
        $Cost_Ary = array();
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            $ItemTotal = 0;
            if ($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] == "") {
                $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $AVG_UNIT_PRICE[$item_now['ItemID']];
            }
            
            $ItemTotal = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] * $item_now['WriteOffQty'];
            $ItemTotalSum += $ItemTotal;
            $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'];
            
            $Cost_Ary[$thisFundingType] = $ItemTotal;
            // $ItemTotal = number_format($ItemTotal,2);
        } else {
            $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice1'];
            if ($ItemOtherInfo[$item_now['ItemID']]['FundingType2'])
                $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType2']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice2'];
        }
        
        if ($thisFundingSrcName == "") {
            $thisFundingSrcName = $symbol_for_null;
        }
        
        list ($record_id, $item_id, $item_type, $item_code, $item_name, $request_date, $location_id, $location_name, $group_id, $group_name, $funding_name, $request_qty, $request_reason_name, $itemdesc, $requestor_id, $requestor_name, $record_status, $approved_date, $approved_by) = $arr_result[$i];
        
        if ($item_now['ItemType'] == ITEM_TYPE_SINGLE)
            $row_css = " class=\"single\" ";
        else
            $row_css = " class=\"bulk\" ";
        
        $table_content .= "<tr $row_css>";
        
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemCode'] . "</td>";
        
        if ($ShowField['Category']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['ItemCategory'] . "</td>";
        }
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemName'] . "</td>";
        if ($ShowField['Description']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemDescription'] . "</td>";
        }
        if ($ShowField['Location']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemLocation'] . "</td>";
        }
        if ($ShowField['Caretaker']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['MgmtGroup'] . "</td>";
        }
        // $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$thisFundingSrcName."</td>";
        if ($ShowField['PurchaseDate']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] . "</td>";
        }
        if ($ShowField['Invoice']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] . "</td>";
        }
        if ($ShowField['SerialNumber']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] . "</td>";
        }
        
        if ($ShowField['LicenseModel']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] . "</td>";
        }
        if ($ShowField['FundingCost']) {
            /*
             * if($thisFundingType == ITEM_SCHOOL_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".$ItemTotal."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * } elseif($thisFundingType== ITEM_GOVERNMENT_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".$ItemTotal."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * } elseif($thisFundingType== ITEM_SPONSORING_BODY_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".$ItemTotal."</td>";
             * } else
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".number_format(0, 2)."</td>";
             * }
             */
            
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2) . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2) . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2) . "</td>";
        }
        $ItemTotal = $Cost_Ary[ITEM_SCHOOL_FUNDING] + $Cost_Ary[ITEM_GOVERNMENT_FUNDING] + $Cost_Ary[ITEM_SPONSORING_BODY_FUNDING];
        if ($ShowField['FundingSource']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $thisFundingSrcName . "</td>";
        }
        if ($ShowField['UnitPrice']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . number_format($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'], 2) . "</td>";
        }
        if ($ShowField['QuotationNo']){
            if($ItemOtherInfo[$item_now['ItemID']]['QuotationNo'] != ""){
                $table_content .= "<td class=\"eSporttdboder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['QuotationNo'] . "</td>"; 
            }else{
                $table_content .= "<td class=\"eSporttdboder eSportprinttext\">" ." -- ".  "</td>";   
            }
        }
        if ($ShowField['MaintainInfo']){
            if($ItemOtherInfo[$item_now['ItemID']]['MaintainInfo'] != ""){
                $table_content .= "<td class=\"eSporttdboder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['MaintainInfo'] . "</td>";
            }else{
                $table_content .= "<td class=\"eSporttdboder eSportprinttext\">". " -- ".  "</td>";
            }
        }
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['WriteOffQty'] . "</td>";
        if ($ShowField['TotalPrice']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . number_format($ItemTotal, 2) . "</td>";
        }
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['WriteOffReason'] . "</td>";
        
        $table_content .= "<td class=\"tabletext\">";
        // $sql = "SELECT PhotoPath, PhotoName FROM INVENTORY_ITEM_WRITE_OFF_ATTACHMENT WHERE RequestWriteOffID = ".$item_now['ItemID'];
        $sql = "SELECT PhotoPath, PhotoName FROM INVENTORY_ITEM_WRITE_OFF_ATTACHMENT WHERE RequestWriteOffID = '" . $record_id."'";
        
        $arr_attachment = $linventory->returnArray($sql, 2);
        if (sizeof($arr_attachment) > 0) {
            for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                list ($photo_path, $photo_name) = $arr_attachment[$j];
                $write_off_photo = $intranet_root . "/" . $photo_path . "/" . $photo_name;
                // $table_content .= "<a class=\"tablelink\" href=\"".$write_off_photo."\" target=\"blank\">$photo_name</a><br>";
                $table_content .= "<a class=\"tablelink\"  href=\"/home/download_attachment.php?target_e=" . getEncryptedText($write_off_photo) . "\" >$photo_name</a><br>";
            }
        } else {
            $table_content .= " -- ";
        }
        $table_content .= "</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['Requestor'] . "</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . $item_now['RequestDate'] . "</td>";
        // $table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"RecordID[]\" value=\"".$item_now['ItemID']."\">";
        
        if ($RecordStatus == 3) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $approved_by . "</td>";
            $table_content .= "<td nowrap class=\"eSporttdborder eSportprinttext\">" . $approved_date . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $record_status . "</td>";
        }
        
        if (! $groupLeaderNowAllowWriteOffItem) {
            $table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"RecordID[]\" value=\"" . $record_id . "\">";
        }
        
        /*
         *
         * $table_content .= "<td class=\"tabletext\">$item_code</td>";
         * $table_content .= "<td class=\"tabletext\">$item_name</td>";
         *
         * $table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($location_name)."</td>";
         * $table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($group_name)."</td>";
         * $table_content .= "<td class=\"tabletext\">$request_qty</td>";
         * $table_content .= "<td class=\"tabletext\">$request_reason_name</td>";
         *
         * $table_content .= "<td class=\"tabletext\">";
         * $sql = "SELECT PhotoPath, PhotoName FROM INVENTORY_ITEM_WRITE_OFF_ATTACHMENT WHERE RequestWriteOffID = $record_id";
         * $arr_attachment = $linventory->returnArray($sql,2);
         * if(sizeof($arr_attachment)>0)
         * {
         * for($j=0; $j<sizeof($arr_attachment); $j++)
         * {
         * list($photo_path, $photo_name) = $arr_attachment[$j];
         * $write_off_photo = $photo_path."/".$photo_name;
         * $table_content .= "<a class=\"tablelink\" href=\"".$write_off_photo."\" target=\"blank\">$photo_name</a><br>";
         * }
         * }
         * else
         * {
         * $table_content .= " -- ";
         * }
         * $table_content .= "</td>";
         * $table_content .= "<td class=\"tabletext\">$requestor_name</td>";
         * $table_content .= "<td class=\"tabletext\">$request_date</td>";
         * $table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"RecordID[]\" value=\"$record_id\">";
         */
        $table_content .= "</tr>";
        $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"" . ($fieldsTotal + $fieldsTotalEnd) . "\"></td></tr>";
    }
}

if (sizeof($arr_result) == 0) {
    $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"16\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

?>

<script language="javascript">
function openPrintPage()
{
     newWindow("write_off_item_print.php?<?=$selected_fields?>",10);
}

function submitenter(myfield,e)
{
var keycode;
if (window.event) keycode = window.event.keyCode;
else if (e) keycode = e.which;
else return true;

if (keycode == 13)
{
myfield.form.submit();
return false;
}
else
return true;
}

function check_form()
{
	var obj = document.form1;
	if(!obj.FieldCategory.checked)		obj.FieldCategoryTmp.value = 0;
	if(!obj.FieldDescription.checked)	obj.FieldDescriptionTmp.value = 0;
	if(!obj.FieldCaretaker.checked)		obj.FieldCaretakerTmp.value = 0;
	if(!obj.FieldPurchaseDate.checked)	obj.FieldPurchaseDateTmp.value = 0;
	if(!obj.FieldInvoice.checked)		obj.FieldInvoiceTmp.value = 0;
	if(!obj.FieldSerialNumber.checked)	obj.FieldSerialNumberTmp.value = 0;
	if(!obj.FieldLicenseModel.checked)	obj.FieldLicenseModelTmp.value = 0;
	if(!obj.FieldUnitPrice.checked)		obj.FieldUnitPriceTmp.value = 0;
	if(!obj.FieldFundingSource.checked)	obj.FieldFundingSourceTmp.value = 0;
	if(!obj.FieldLocation.checked)		obj.FieldLocationTmp.value = 0;
	if(!obj.FieldQuotationNo.checked)   obj.FieldQuotationNo.value = 0;
	if(!obj.FieldMaintainInfo.checked)   obj.FieldMaintainInfo.value = 0;
	
// 	return false;
	obj.submit();
}

</script>


<form name="form1" action="" method="post"
	onSubmit="return check_form()">


	<table width="100%" border="0" cellpadding="3" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right" class="tabletext"><?= $SysMsg ?></td>
		</tr>
		<tr>
			<td align="left"><?=$toolbar?></td>
			<td align="right"><?=$search_option?></td>
		</tr>
	</table>

<? if($groupLeaderNowAllowWriteOffItem) {?> 
<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Remark']?></legend>
		<?=$Lang['eInventory']['DoNotAllowGroupLeaderWriteOffItem']?>
</fieldset>
<? } ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td align="left">
				<div class="selectbox_group">
					<a href="javascript:;"
						onClick="MM_showHideLayers('status_option','','show')"><?=$Lang['eInventory']['select_more_fields']?></a>
				</div>
		<?=$filterbar; ?> 
												<br style="clear: both">
				<div id="status_option" class="selectbox_layer"
					style="z-index: 1000;">
					<table width="" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td>&nbsp;<input type="checkbox" name="FieldCategory"
								id="FieldCategory" value="1" <?=$FieldCategoryChecked?>><input
								type="hidden" name="FieldCategoryTmp" value=""> <label
								for="FieldCategory"><?=$i_InventorySystem['Category']?></label><br />
								&nbsp;<input type="checkbox" name="FieldDescription"
								id="FieldDescription" value="1" <?=$FieldDescriptionChecked?>><input
								type="hidden" name="FieldDescriptionTmp" value=""> <label
								for="FieldDescription"><?=$i_InventorySystem_Item_Description?></label><br />
								&nbsp;<input type="checkbox" name="FieldLocation"
								id="FieldLocation" value="1" <?=$FieldLocationChecked?>><input
								type="hidden" name="FieldLocationTmp" value=""> <label
								for="FieldLocation"><?=$i_InventorySystem_Item_Location?></label><br />
								&nbsp;<input type="checkbox" name="FieldCaretaker"
								id="FieldCaretaker" value="1" <?=$FieldCaretakerChecked?>><input
								type="hidden" name="FieldCaretakerTmp" value=""> <label
								for="FieldCaretaker"><?=$i_InventorySystem['Caretaker']?></label><br />
								&nbsp;<input type="checkbox" name="FieldPurchaseDate"
								id="FieldPurchaseDate" value="1" <?=$FieldPurchaseDateChecked?>><input
								type="hidden" name="FieldPurchaseDateTmp" value=""> <label
								for="FieldPurchaseDate"><?=$i_InventorySystem_Report_Col_Purchase_Date?></label><br />
								&nbsp;<input type="checkbox" name="FieldInvoice"
								id="FieldInvoice" value="1" <?=$FieldInvoiceChecked?>><input
								type="hidden" name="FieldInvoiceTmp" value=""> <label
								for="FieldInvoice"><?=$i_InventorySystem_Item_Invoice?></label><br />
								&nbsp;<input type="checkbox" name="FieldSerialNumber"
								id="FieldSerialNumber" value="1" <?=$FieldSerialNumberChecked?>><input
								type="hidden" name="FieldSerialNumberTmp" value=""> <label
								for="FieldSerialNumber"><?=$i_InventorySystem_Item_Serial_Num?></label><br />
								&nbsp;<input type="checkbox" name="FieldLicenseModel"
								id="FieldLicenseModel" value="1" <?=$FieldLicenseModelChecked?>><input
								type="hidden" name="FieldLicenseModelTmp" value=""> <label
								for="FieldLicenseModel"><?=$i_InventorySystem_Category2_License?></label><br />
								&nbsp;<input type="checkbox" name="FieldUnitPrice"
								id="FieldUnitPrice" value="1" <?=$FieldUnitPriceChecked?>><input
								type="hidden" name="FieldUnitPriceTmp" value=""> <label
								for="FieldUnitPrice"><?=$i_InventorySystem_Unit_Price?></label><br />
								&nbsp;<input type="checkbox" name="FieldTotalPrice"
								id="FieldTotalPrice" value="1" <?=$FieldTotalPriceChecked?>> <label
								for="FieldTotalPrice"><?=$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']?></label><br />
								&nbsp;<input type="checkbox" name="FieldFundingCost"
								id="FieldFundingCost" value="1" <?=$FieldFundingCostChecked?>> <label
								for="FieldFundingCost"><?=$i_InventorySystem_Report_Col_Cost?></label><br />
								&nbsp;<input type="checkbox" name="FieldFundingSource"
								id="FieldFundingSource" value="1"
								<?=$FieldFundingSourceChecked?>><input type="hidden"
								name="FieldFundingSourceTmp" value=""> <label
								for="FieldFundingSource"><?=$i_InventorySystem['FundingSource']?></label><br />
								&nbsp;<input type="checkbox" name="FieldQuotationNo"
								id="FieldQuotationNo" value="1" <?=$FieldQuotationNoChecked?>><input type="hidden" 
								name="FieldQuotationNoTmp" value=""> <label
								for="FieldQuotationNo"><?=$i_InventorySystem_Item_Quot_Num?></label><br />
								&nbsp;<input type="checkbox" name="FieldMaintainInfo"
								id="FieldMaintainInfo" value="1" <?=$FieldMaintainInfoChecked?>><input type="hidden" 
								name="FieldMaintainInfoTmp" value=""> <label
								for="FieldMaintainInfo"><?=$i_InventorySystemItemMaintainInfo?></label><br />
						
						</tr>
						<!--
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="waived" id="waived" value="1" <?=$waivedChecked?>>
																<label for="waived"><?=$i_Discipline_System_Award_Punishment_Waived?></label></td>
														</tr>
														-->
						<tr>
							<td align="left" valign="top" class="dotline" height="2"><img
								src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
						</tr>
						<tr>
							<td align="center" class="tabletext">
																<?//= $linterface->GET_BTN($button_apply, "button", "javascript:document.form1.submit();")?>
																<?= $linterface->GET_BTN($button_apply, "button", "check_form();")?>
																<?= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');")?>
															</td>
						</tr>
					</table>
				</div>
			
			<?=$searchbar?>
		</td>
			<td valign="bottom" align="right">
		<? if(!$groupLeaderNowAllowWriteOffItem) { ?>
			<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
								<?=$table_tool?>
							</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			<? } ?>
		</td>
		</tr>
	</table>
	<table border="0" width="100%" cellpadding="5" cellspacing="0">
<?=$table_content?>
</table>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="dotline"><img
				src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
				height="5"></td>
		</tr>
	</table>

	<table border="0" width="100%" cellpadding="5" cellspacing="0">
		<tr>
			<td><table border="0" cellpadding="0">
					<tr>
						<td><table border="0">
								<tr>
									<td width="10" height="10"><table width="100%" border="1"
											cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
											<tr>
												<td class="single"><img
													src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
													width="10" height="10"></td>
											</tr>
										</table></td>
									<td><span class="tabletext"><?=$i_InventorySystem_ItemType_Single?></span></td>
								</tr>
							</table></td>
						<td><table width="100%" border="0">
								<tr>
									<td width="10" height="10"><table width="100%" border="1"
											cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
											<tr>
												<td class="bulk"><img
													src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
													width="10" height="10"></td>
											</tr>
										</table></td>
									<td><span class="tabletext"><?=$i_InventorySystem_ItemType_Bulk?></span></td>
								</tr>
							</table></td>
					</tr>
				</table><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?><br />
			<font style="color: red">^</font> <?=$Lang['eInventory']['DisplayEarliestPurchaseDateOnly']?></td>
		</tr>
	</table>
</form>

<br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>