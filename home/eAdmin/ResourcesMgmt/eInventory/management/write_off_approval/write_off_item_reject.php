<?php
#############################
#	Date:	2016-02-04 (Henry)
#			fixed: php 5.4 issue move "if(!isset($RecordID)){}" after includes file 
#
#	Date:	2013-07-25	YatWoon
#			Add reject date / person data
#
#############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if(!isset($RecordID))
{
	header("location: write_off_item.php");
	exit;
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface 	= new interface_html();
$linventory		= new libinventory();

$arr_success_rec = Array();
$curr_date = date("Y-m-d");

if($RecordID != "")
{
	$record_list = implode(",",$RecordID);
	
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordID IN ($record_list)";
	$arr_tmp_item = $linventory->returnVector($sql);
	
	if(sizeof($arr_tmp_item) > 0)
	{
		$item_list = implode(",",$arr_tmp_item);
		
		$sql = "DELETE FROM INTRANET_RESOURCE_WRITE_OFF WHERE ItemID IN ($item_list)";
		$linventory->db_db_query($sql);
	}
	
	$sql = "UPDATE 
					INVENTORY_ITEM_WRITE_OFF_RECORD
			SET
					RejectDate = '$curr_date',
					RejectPerson = $UserID,
					RecordStatus = 2,
					DateModified = NOW()
			WHERE
					RecordID IN ($record_list)
			";
	
	$result = $linventory->db_db_query($sql);
	
	if($result)
	{
		$x = "2";
	}
	else
	{
		$x = "14";
	}
}

intranet_closedb();
header("location: write_off_item.php?msg=$x");
?>