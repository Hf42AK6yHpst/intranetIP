<?php

// using: Henry

// #################################
// Date: 2019-05-13 Henry
// Security fix: SQL without quote
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-01
// PHP 5.4 fix: change split to explode
//
// Date: 2015-01-05 Henry
// Created this file
//
// #################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// $CurrentPageArr['eInventory'] = 1;
// $CurrentPage = "Management_Stocktake";
// $linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['StockCheck'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");

$arr_bulk_item = explode(",", $bulk_item_list);

$arr_single_item = explode(",", $single_item_list);

// $re_path = "/file/inventory/write_off";
$path = "$intranet_root$re_path";

if (sizeof($unlisted_item_id) > 0) {
    for ($i = 0; $i < sizeof($unlisted_item_id); $i ++) {
        $target_unlisted_item = $unlisted_item_id[$i];
        $sql = "SELECT ItemID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$target_unlisted_item."'";
        $arr_exist_result = $linventory->returnArray($sql);
        
        if (sizeof($arr_exist_result) > 0) {
            $sql = "UPDATE 
						INVENTORY_ITEM_SURPLUS_RECORD
					SET
						LocationID = $targetLocation,
						RecordDate = '$curr_date',
						PersonInCharge = $UserID,
						DateModified = NOW()
					WHERE 
						ItemID = '".$target_unlisted_item."'";
            $linventory->db_db_query($sql);
            
            $sql = "INSERT INTO 
							INVENTORY_ITEM_SINGLE_STATUS_LOG
							(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType,
							RecordStatus, DateInput, DateModified)
					VALUES
							($target_unlisted_item, '$curr_date', " . ITEM_ACTION_STOCKTAKE_OTHER_LOCATION . ", $UserID, '',
							0, 0, NOW(), NOW())
							";
            $linventory->db_db_query($sql);
        } else {
            $sql = "SELECT 
						b.GroupInCharge
					FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
					WHERE
						a.ItemID = '".$target_unlisted_item."'
					";
            
            $arr_unlisted_admin_group = $linventory->returnVector($sql);
            
            $unlisted_item_admin_group = $arr_unlisted_admin_group[0];
            
            $sql = "INSERT INTO 
							INVENTORY_ITEM_SURPLUS_RECORD
							(ItemID, LocationID, AdminGroupID, RecordDate, SurplusQty, PersonInCharge, 
							RecordType, RecordStatus, DateInput, DateModified)
					VALUES
							($target_unlisted_item, $targetLocation, $unlisted_item_admin_group, '$curr_date', 1, $UserID,
							0, 0, NOW(), NOW())
					";
            
            $linventory->db_db_query($sql);
            
            $sql = "INSERT INTO 
							INVENTORY_ITEM_SINGLE_STATUS_LOG
							(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType,
							RecordStatus, DateInput, DateModified)
					VALUES
							($target_unlisted_item, '$curr_date', " . ITEM_ACTION_STOCKTAKE_OTHER_LOCATION . ", $UserID, '',
							0, 0, NOW(), NOW())
							";
            $linventory->db_db_query($sql);
        }
    }
}

// ### Single Item ####
if (sizeof($arr_single_item) > 0) {
    for ($i = 0; $i < sizeof($arr_single_item); $i ++) {
        $single_item_id = $arr_single_item[$i];
        $single_item_group_id = ${"single_item_group_id_$single_item_id"};
        $single_item_location_id = ${"single_item_location_id_$single_item_id"};
        /*
         * if(!empty($write_off_item_id))
         * {
         * ### Need write-off?
         * if(in_array($single_item_id,$write_off_item_id))
         * {
         * ### Check For the write off record is exist or not, exist->delete, then insert ###
         * $sql = "SELECT
         * RecordID
         * FROM
         * INVENTORY_ITEM_WRITE_OFF_RECORD
         * WHERE
         * ItemID = $single_item_id AND
         * LocationID = $targetLocation AND
         * AdminGroupID = $single_item_group_id AND
         * RecordStatus = 0
         * ";
         * $arr_record_exist = $linventory->returnVector($sql);
         *
         * if(sizeof($arr_record_exist) > 0)
         * {
         * foreach($arr_record_exist as $k1 => $d1)
         * {
         * $exist_record_id = $d1;
         *
         * ## delete INVENTORY_ITEM_WRITE_OFF_RECORD
         * $sql = "delete from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordID=$exist_record_id";
         * $linventory->db_db_query($sql);
         *
         * ## delete files
         * ## implement if necessary
         *
         * ## delete INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
         * $sql = "delete from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID=$exist_record_id";
         * $linventory->db_db_query($sql);
         * }
         * }
         * ### [End] Check For the write off record is exist or not, exist->delete, then insert [End] ###
         *
         * $write_off_reason_id = ${"single_item_write_off_reason_$single_item_id"};
         * if($write_off_reason_id != 999)
         * {
         * $sql = "SELECT ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = $write_off_reason_id";
         * $arr_reason = $linventory->returnVector($sql);
         *
         * if($arr_reason != "")
         * {
         * $write_off_reason = $arr_reason[0];
         * $write_off_reason = addslashes($write_off_reason);
         * }
         * }
         * else
         * {
         * $write_off_reason = $i_InventorySystem_ItemStatus_Lost;
         * $write_off_reason = addslashes($write_off_reason);
         * }
         *
         * $sql = "INSERT INTO
         * INVENTORY_ITEM_WRITE_OFF_RECORD
         * (ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason,
         * RequestDate, RequestPerson, RecordType, RecordStatus, DateInput, DateModified)
         * VALUES
         * ($single_item_id, '$curr_date', $targetLocation, $single_item_group_id,
         * 1, '$write_off_reason', '$curr_date', $UserID,
         * 0, 0, NOW(), NOW())";
         * $write_off_result = $linventory->db_db_query($sql) or die(mysql_error());
         * $write_off_logID = $linventory->db_insert_id();
         *
         * $no_of_file = ${"no_of_file_upload_$single_item_id"};
         *
         * for($j=0; $j<$no_of_file; $j++)
         * {
         * $file = stripslashes(${"hidden_single_item_write_off_attachment_".$single_item_id."_".$j});
         * $re_file = stripslashes(${"single_item_write_off_attachment_".$single_item_id."_".$j});
         *
         * if($re_file == "none" || $re_file == ""){
         * }
         * else
         * {
         * $photo_path = $path . "/".$write_off_logID;
         * $photo_re_path = $re_path . "/".$write_off_logID;
         *
         * $lf = new libfilesystem();
         * if (!is_dir($photo_path))
         * {
         * $lf->folder_new($photo_path);
         * }
         *
         * $target = "$photo_path/$file";
         * $attachementname = "/$file";
         *
         * if($lf->lfs_copy($re_file, $target) == 1)
         * {
         * $sql = "INSERT INTO
         * INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
         * (ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
         * VALUES
         * ($single_item_id, $write_off_logID, '$photo_re_path', '$file', NOW(), NOW())
         * ";
         * $linventory->db_db_query($sql);
         * }
         * }
         * }
         * }
         * }
         */
        $single_item_found = ${"single_item_hidden_status_$single_item_id"};
        
        if ($single_item_found == 0)
            $item_status = 3; /* 3 - Missing */
        else
            $item_status = ${"single_item_status_$single_item_id"};
        
        $item_past_status = ${"single_item_past_status_$single_item_id"};
        $item_remark = ${"single_item_remark_$single_item_id"};
        
        if ($single_item_found == 0) {
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$single_item_id."' AND RecordStatus = 0";
            $arr_exist_record = $linventory->returnVector($sql);
            if (sizeof($arr_exist_record) > 0) {
                $exist_record = implode(",", $arr_exist_record);
                
                $sql = "UPDATE
							INVENTORY_ITEM_MISSING_RECORD
						SET
							ItemID = $single_item_id,
							LocationID = $single_item_location_id, 
							AdminGroupID = $single_item_group_id,
							RecordDate = '$curr_date',
							MissingQty = 1,
							PersonInCharge = '$UserID',
							RecordType = 0,
							RecordStatus = 0,
							DateModified = NOW()
						WHERE
							RecordID IN ($exist_record)
						";
                $linventory->db_db_query($sql);
            } else {
                $sql = "INSERT INTO
								INVENTORY_ITEM_MISSING_RECORD
								(ItemID, LocationID, AdminGroupID, RecordDate, MissingQty, PersonInCharge, 
								RecordType, RecordStatus, DateInput, DateModified)
						VALUES
								($single_item_id, $single_item_location_id, $single_item_group_id, '$curr_date', 1, '$UserID', 
								0, 0 , NOW(), NOW())
						";
                $linventory->db_db_query($sql);
            }
            
            $sql = "INSERT INTO 
						INVENTORY_ITEM_SINGLE_STATUS_LOG
						(ItemID, RecordDate, Action, PastStatus, NewStatus, PersonInCharge, Remark, RecordType,
						RecordStatus, DateInput, DateModified)
					VALUES
						($single_item_id, '$curr_date', " . ITEM_ACTION_STOCKTAKE_NOT_FOUND . ", '', '', $UserID, '$item_remark', '', '', NOW(), NOW())
				";
            $linventory->db_db_query($sql);
        } else {
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$single_item_id."' AND RecordStatus = 0";
            $arr_exist_record = $linventory->returnVector($sql);
            if (sizeof($arr_exist_record) > 0) {
                $exist_record = implode(",", $arr_exist_record);
                $sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE RecordID IN ($exist_record)";
                $linventory->db_db_query($sql);
            }
            
            $sql = "INSERT INTO 
						INVENTORY_ITEM_SINGLE_STATUS_LOG
						(ItemID, RecordDate, Action, PastStatus, NewStatus, PersonInCharge, Remark, RecordType,
						RecordStatus, DateInput, DateModified)
				VALUES
						($single_item_id, '$curr_date', " . ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION . ", '', '', $UserID, '$item_remark', '', '', NOW(), NOW())
				";
            $linventory->db_db_query($sql);
        }
    }
}

// ### Bulk Item ####
if (sizeof($arr_bulk_item) > 0) {
    for ($i = 0; $i < sizeof($arr_bulk_item); $i ++) {
        $item_remark = "";
        $bulk_item_id = $arr_bulk_item[$i];
        $bulk_item_group = $bulk_item_group_id[$i];
        $item_admin_group = $bulk_item_group;
        $bulk_funding_id = $funding_id_ary[$i];
        
        /*
         * $stock_check_qty = ${"bulk_item_qty_found_".$bulk_item_id."_".$bulk_item_group.""};
         * $item_qty_change = ${"bulk_item_change_".$bulk_item_id."_".$bulk_item_group.""};
         * $item_user_remark = ${"bulk_item_remark_".$bulk_item_id."_".$bulk_item_group.""};
         * $bulk_item_write_off = ${"bulk_item_write_off_".$bulk_item_id."_".$bulk_item_group.""};
         * $bulk_item_write_off_reason = ${"bulk_item_write_off_reason_".$bulk_item_id."_".$bulk_item_group.""};
         * $bulk_item_write_off_qty = ${"bulk_item_write_off_qty_".$bulk_item_id."_".$bulk_item_group.""};
         * $item_admin_group = $bulk_item_group;
         */
        // henry added
        $bulk_item_location_id = $bulk_item_location_id_ary[$i];
        $stock_check_qty = ${"bulk_item_qty_found_" . $bulk_item_id . "_" . $bulk_item_group . "_" . $bulk_funding_id . "_" . $bulk_item_location_id . ""};
        $item_qty_change = ${"bulk_item_change_" . $bulk_item_id . "_" . $bulk_item_group . "_" . $bulk_funding_id . "_" . $bulk_item_location_id . ""};
        $item_user_remark = ${"bulk_item_remark_" . $bulk_item_id . "_" . $bulk_item_group . "_" . $bulk_funding_id . "_" . $bulk_item_location_id . ""};
        /*
         * ## delete all the old request write-off record ##
         * $sql = "DELETE FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $bulk_item_id and LocationID=$targetLocation AND RecordStatus = 0";
         * $linventory->db_db_query($sql);
         *
         * ## Handle the request of the write-off item
         * if($bulk_item_write_off_qty > 0)
         * {
         * $sql = "SELECT ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = $bulk_item_write_off_reason";
         * $arr_write_off_reason = $linventory->returnVector($sql);
         * $write_off_reason = $arr_write_off_reason[0];
         * $write_off_reason = addslashes($write_off_reason);
         *
         * $sql = "INSERT INTO
         * INVENTORY_ITEM_WRITE_OFF_RECORD
         * (ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason,
         * RequestDate, RequestPerson, RecordType, RecordStatus, DateInput, DateModified)
         * VALUES
         * ($bulk_item_id, '$curr_date', $targetLocation, $bulk_item_group,
         * $bulk_item_write_off_qty, '$write_off_reason', '$curr_date', $UserID,
         * 0,0,NOW(),NOW())";
         * $write_off_result = $linventory->db_db_query($sql);
         * $write_off_logID = $linventory->db_insert_id();
         *
         * $no_of_file = ${"no_of_file_upload_$bulk_item_id"};
         *
         * for($j=0; $j<$no_of_file; $j++)
         * {
         * $file = stripslashes(${"hidden_bulk_item_write_off_attachment_".$bulk_item_id."_".$j});
         * $re_file = stripslashes(${"bulk_item_write_off_attachment_".$bulk_item_id."_".$j});
         *
         * if($re_file == "none" || $re_file == ""){
         * }
         * else
         * {
         * $photo_path = $path . "/".$write_off_logID;
         * $photo_re_path = $re_path . "/".$write_off_logID;
         *
         * $lf = new libfilesystem();
         * if (!is_dir($photo_path))
         * {
         * $lf->folder_new($photo_path);
         * }
         *
         * $target = "$photo_path/$file";
         * $attachementname = "/$file";
         *
         * if($lf->lfs_copy($re_file, $target) == 1)
         * {
         * // $attachment['FileName'] = $attachementname;
         * // $attachment['Path'] = $re_path;
         *
         * $sql = "INSERT INTO
         * INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
         * (ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
         * VALUES
         * ($bulk_item_id, '$write_off_logID', '$photo_re_path', '$file', NOW(), NOW())
         * ";
         * $linventory->db_db_query($sql);
         * }
         * }
         * }
         * }
         */
        
        if ($stock_check_qty != "") {
            $item_status = $i_InventorySystem_ItemStatus_Normal . " - " . $stock_check_qty;
        }
        // if($item_qty_change > 0)
        if ($item_qty_change < 0) {
            $item_status .= "<br>" . $i_InventorySystem_ItemStatus_Lost . " - " . abs($item_qty_change);
        }
        // if($item_qty_change < 0)
        if ($item_qty_change > 0) {
            $item_status .= "<br>" . $i_InventorySystem_ItemStatus_Surplus . " - " . abs($item_qty_change);
        }
        /*
         * if($bulk_item_write_off_qty > 0)
         * {
         * $item_status .= "<br>".$i_InventorySystem_RequestWriteOff." - ".$bulk_item_write_off_qty;
         * }
         */
        
        if ($item_qty_change > 0) // Item Surplus #
{
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$bulk_item_id."' AND LocationID = '".$bulk_item_location_id."' AND AdminGroupID = '".$item_admin_group."' and FundingSourceID='".$bulk_funding_id."' AND RecordStatus = 0";
            $arr_record_exist = $linventory->returnVector($sql);
            if (sizeof($arr_record_exist) > 0) {
                $exist_record = implode(",", $arr_record_exist);
                
                $sql = "UPDATE 
							INVENTORY_ITEM_SURPLUS_RECORD 
						SET
							ItemID = $bulk_item_id,
							LocationID = $bulk_item_location_id,
							AdminGroupID = $item_admin_group,
							FundingSourceID = $bulk_funding_id,
							RecordDate = '$curr_date',
							SurplusQty = " . abs($item_qty_change) . ",
							PersonInCharge = $UserID,
							RecordType = 0,
							RecordStatus = 0,
							DateModified = NOW()
						WHERE
							RecordID IN ($exist_record)
						";
                $linventory->db_db_query($sql);
            } else {
                $sql = "INSERT INTO 
								INVENTORY_ITEM_SURPLUS_RECORD 
								(ItemID, LocationID, AdminGroupID, FundingSourceID,RecordDate, SurplusQty, PersonInCharge, 
								RecordType, RecordStatus, DateInput, DateModified)
						VALUES
								($bulk_item_id, $bulk_item_location_id, $item_admin_group, $bulk_funding_id, '$curr_date', " . abs($item_qty_change) . ", $UserID,
								0, 0, NOW(), NOW())
						";
                $linventory->db_db_query($sql);
            }
        } else {
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$bulk_item_id."' AND LocationID = '".$bulk_item_location_id."' AND AdminGroupID = '".$item_admin_group."' and FundingSourceID='".$bulk_funding_id."' AND RecordStatus = 0";
            $arr_record_exist = $linventory->returnVector($sql);
            if (sizeof($arr_record_exist) > 0) {
                $exist_record = implode(",", $arr_record_exist);
                $sql = "DELETE FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE RecordID IN ($exist_record)";
                $linventory->db_db_query($sql);
            }
        }
        
        if ($item_qty_change < 0) // Item Missing #
{
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$bulk_item_id."' AND LocationID = '".$bulk_item_location_id."' AND AdminGroupID = '".$item_admin_group."' AND FundingSourceID='".$bulk_funding_id."' and RecordStatus = 0";
            $arr_record_exist = $linventory->returnVector($sql);
            if (sizeof($arr_record_exist) > 0) {
                $exist_record = implode(",", $arr_record_exist);
                
                $sql = "UPDATE 
							INVENTORY_ITEM_MISSING_RECORD
						SET
							ItemID = $bulk_item_id,
							LocationID = $bulk_item_location_id,
							AdminGroupID = $item_admin_group,
							FundingSourceID = $bulk_funding_id,
							RecordDate = '$curr_date',
							MissingQty = " . abs($item_qty_change) . ",
							PersonInCharge = $UserID,
							RecordType = 0,
							RecordStatus = 0,
							DateModified = NOW()
						WHERE 
							RecordID IN ($exist_record)
						";
                $linventory->db_db_query($sql);
            } else {
                $sql = "INSERT INTO 
								INVENTORY_ITEM_MISSING_RECORD
								(ItemID, LocationID, AdminGroupID, FundingSourceID, RecordDate, MissingQty, PersonInCharge, 
								RecordType, RecordStatus, DateInput, DateModified)
						VALUES
								($bulk_item_id, $bulk_item_location_id, $item_admin_group, $bulk_funding_id, '$curr_date', " . abs($item_qty_change) . ", $UserID,
								0, 0, NOW(), NOW())
						";
                $linventory->db_db_query($sql);
            }
        } else {
            $sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$bulk_item_id."' AND LocationID = '".$bulk_item_location_id."' AND AdminGroupID = '".$item_admin_group."' and FundingSourceID='".$bulk_funding_id."' AND RecordStatus = 0";
            $arr_record_exist = $linventory->returnVector($sql);
            if (sizeof($arr_record_exist) > 0) {
                $exist_record = implode(",", $arr_record_exist);
                $sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE RecordID IN ($exist_record)";
                $linventory->db_db_query($sql);
            }
        }
        
        // $stock_check_qty = $stock_check_qty + $bulk_item_write_off_qty;
        $sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG 
						(ItemID, RecordDate, Action, QtyChange, PurchaseDate, PurchasedPrice, FundingSource, 
						SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo,
						PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, LocationID,
						GroupInCharge, StockCheckQty)
				VALUES
						($bulk_item_id, '$curr_date', " . ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION . ", $item_qty_change, '', '', $bulk_funding_id, 
						'', '', '', '', '', '', 
						$UserID, '$item_user_remark',
						'', '', '', NOW(), NOW(), $bulk_item_location_id, $item_admin_group, $stock_check_qty)
				";
        $linventory->db_db_query($sql);
    }
}

header("location: index.php?msg=1");
?>