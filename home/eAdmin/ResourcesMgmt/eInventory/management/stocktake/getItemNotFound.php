<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_Stocktake";
$linterface 	= new interface_html();
$linventory	= new libinventory();

$arr_missing_item = array();


$layer_content .= "<table border=0 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
//$layer_content .= "<tr class=tabletop><td colspan=5 align=right><input type=button value=' X ' onClick=hideMenu('ToolMenu2')></td></tr>";
$layer_content .= "<tr class=tabletop><td colspan=5 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu2')></td></tr>";
$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_Name."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Category2_TagCode."</td><td class=\"tabletopnolink\">".$i_InventorySystem['Location']."</td></tr>";

$NotFoundArr = array();
if($MissItemList != "")
{	

	$arr_temp = explode(",",$MissItemList);	
	if(sizeof($arr_temp)>0)
	{
		for($i=0; $i<sizeof($arr_temp); $i++)
		{
			$missing_item = $arr_temp[$i];
			
			$sql = "SELECT ItemID  FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '$missing_item'";
			$arr_result = $linventory->returnVector($sql);
			
			if(sizeof($arr_result)>0)
			{
				for($j=0; $j<sizeof($arr_result); $j++)
				{
					$missing_item_id = $arr_result[$j];
					
					array_push($arr_missing_item,$missing_item_id);
				}
			}
			else
			{	
				if (!in_array($missing_item,$NotFoundArr))			
				{
					$NotFoundArr[] = $missing_item;				
				}
			}
		}
	}
	
	
	if(sizeof($arr_missing_item)>0)
	{
		$missing_item_list = implode(",",$arr_missing_item);
	
		$sql = "SELECT
						a.ItemID,
						".$linventory->getInventoryItemNameByLang("a.").",
						b.TagCode,
						".$linventory->getInventoryNameByLang("c.")."
				FROM 
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)	INNER JOIN
						INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
				WHERE
						a.ItemID IN ($missing_item_list)";
					
		$arr_result = $linventory->returnArray($sql,4);
		if(sizeof($arr_result)>0)
		{
			for($i=0; $i<sizeof($arr_result); $i++)
			{
				list($item_id, $item_name, $item_tag, $item_location) = $arr_result[$i];
				$layer_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\">$item_name</td><td class=\"tabletext\">$item_tag</td><td class=\"tabletext\">$item_location</td></tr>";
			}			
		}

									
		if ((sizeof($arr_result)==0) && (sizeof($NotFoundArr)==0))
		{
			$layer_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>";
		}
	}	
	
	if ((sizeof($arr_missing_item)==0) && (sizeof($NotFoundArr)==0))
	{
		$layer_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	}
}

for($i=0;$i<sizeof($NotFoundArr); $i++)
{			
	$layer_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\"> - </td><td class=\"tabletext\">".$NotFoundArr[$i]."</td><td class=\"tabletext\"> - </td></tr>";
}			

if($MissItemList == "")
{
	$layer_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}
$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\"></td></tr>";
$layer_content .= "</table>";

$layer_content = "<br /><br />".$layer_content."<br /><br />" ;

$response = iconv("Big5","UTF-8",$layer_content);

echo $response;

?>