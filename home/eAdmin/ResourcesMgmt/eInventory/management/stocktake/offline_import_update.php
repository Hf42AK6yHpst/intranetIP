<?php
# using: 

############################################
# Date: 	2019-05-13 Henry
# 			Security fix: SQL without quote
#
#	Date:	2018-02-07	Henry
#			add access right checking [Case#E135442]
#
#	Date:	2013-11-01	YatWoon
#			fixed: Insert incorrect record date for bulk item offline import [Case#2013-0704-1540-47073]
#
#	Date:	2013-03-19	YatWoon
#			improved: bulk item offline stocktake with "ITEM_ACTION_STOCKTAKE_OTHER_LOCATION"
#
#	Date:	2013-03-18	Carlos
#			revise bulk item handling			
#
#	Date:	2013-02-22	Carlos
#			record funding source id to bulk log
#
#	Date:	2011-12-15	YatWoon
#			add UserID checking for temp table
#
#	Date:	2011-07-15	YatWoon
#			add a temp variable $HandleOthers, to cater "ignore other items which not included in the txt file"
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_Stocktake";
$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li 			= new libdb();
$llocation_ui	= new liblocation_ui();

$linventory->Start_Trans();

$StocktakePeriod_StartDate = $linventory->getStocktakePeriodStart();
$StocktakePeriod_EndDate = $linventory->getStocktakePeriodEnd();

$curr_date = date("Y-m-d");

$HandleOthers = 0;

### Generate the location list
$sql = "SELECT DISTINCT b.LocationID FROM TEMP_INVENTORY_STOCKTAKE_RESULT AS a INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) WHERE a.ItemType = ".ITEM_TYPE_SINGLE ." and a.UserID='".$UserID."'";
$SingleItemLocation = $linventory->returnArray($sql,1);

if(sizeof($SingleItemLocation)>0){
	for($i=0; $i<sizeof($SingleItemLocation); $i++){
		list($single_location_id) = $SingleItemLocation[$i];
		$single_temp[] = $single_location_id;
	}
}else{
	$single_temp = array();
}

$sql = "SELECT DISTINCT b.LocationID FROM TEMP_INVENTORY_STOCKTAKE_RESULT AS a INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) WHERE a.ItemType = ".ITEM_TYPE_BULK ." and a.UserID='".$UserID."'";
$BulkItemLocation = $linventory->returnArray($sql,1);
if(sizeof($BulkItemLocation)>0){
	for($i=0; $i<sizeof($BulkItemLocation); $i++){
		list($bulk_location_id) = $BulkItemLocation[$i];
		$bulk_temp[] = $bulk_location_id;
	}
}else{
	$bulk_temp = array();
}

$tempLocationArray = array_merge($single_temp,$bulk_temp);
$LocationArray = array_unique($tempLocationArray);
$target_location_id = implode("','",$LocationArray);

## Single Item ##
## Check CSV's Single Item is in Original Location
$sql = "SELECT 
			a.ItemID, c.NameEng, 
			IF(a.LocationID = b.LocationID,'".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION."','".ITEM_ACTION_STOCKTAKE_OTHER_LOCATION."') AS StocktakeResult,
			b.LocationID AS OriginalLocation,
			a.LocationID AS NewLocation,
			b.GroupInCharge AS AdminGroup,
			a.StocktakeDate,
			a.StocktakeTime
		FROM 
			TEMP_INVENTORY_STOCKTAKE_RESULT AS a INNER JOIN 
			INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
			INVENTORY_ITEM AS c ON (a.ItemID = c.ItemID)
		WHERE 
			a.ItemType = ".ITEM_TYPE_SINGLE ." and a.UserID='".$UserID."'";

$single_stocktake_result = $linventory->returnArray($sql,8);

### Single Item Is Found
if(sizeof($single_stocktake_result)>0)
{
	for($i=0; $i<sizeof($single_stocktake_result); $i++)
	{
		list($item_id, $item_name, $stocktake_result, $original_location_id, $new_location_id, $admin_group_id, $stocktake_date, $stocktake_time) = $single_stocktake_result[$i];
		$stocktake_datetime = $stocktake_date." ".$stocktake_time;
		
		if($stocktake_result == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)
		{
			## As item is found in other location, check any surplus record before
			## If surplus record found, update the surplus record.
			$sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$item_id."'";
			$surplus_record_exist = $linventory->returnArray($sql,1);
			if(sizeof($surplus_record_exist)>0){
				list($record_id) = $surplus_record_exist[0];
				$sql = "UPDATE 
							INVENTORY_ITEM_SURPLUS_RECORD 
						SET 
							LocationID = '$new_location_id',
							RecordDate = '$stocktake_date',
							PersonInCharge = $UserID,
							DateModified = '$stocktake_datetime'
						WHERE 
							RecordID = '".$record_id."'";
				//echo "1: ".$sql."<BR><br>";
				$linventory->db_db_query($sql);
			}
			else
			{
				## If surplus record NOT found, creata a new surplus record.
				$sql = "INSERT INTO 
							INVENTORY_ITEM_SURPLUS_RECORD 
							(ItemID, LocationID, AdminGroupID, RecordDate, SurplusQty, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
							('$item_id', '$new_location_id', '$admin_group_id', '$stocktake_date', 1, '$UserID', 0, 0, '$stocktake_datetime', '$stocktake_datetime')
						";
				//echo "2: ".$sql."<BR><br>";
				$linventory->db_db_query($sql);
			}
		}
		
		## Check any missing record before
		$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$item_id."' AND RecordStatus = 0";
		$missing_record_exist = $linventory->returnVector($sql);
		if(sizeof($missing_record_exist)>0)
		{
			## if missing record is found, delete it as the item is found now
			$exist_missing_record = implode("','",$missing_record_exist);
			$sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE RecordID IN ('".$exist_missing_record."')";
			//echo "3: ".$sql."<BR><br>";
			$linventory->db_db_query($sql);
		}
		
		## Create a stocktake found / found in other location record into the log table
		$sql = "INSERT INTO 
					INVENTORY_ITEM_SINGLE_STATUS_LOG 
					(ItemID, RecordDate, Action, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified) 
				VALUES
					('$item_id','$stocktake_date','$stocktake_result','$UserID','0','0','$stocktake_datetime','$stocktake_datetime')
				";
		//echo "4: ".$sql."<BR><br>";
		$single_result[] = $linventory->db_db_query($sql);
	}
}

if($HandleOthers)
{
	$sql = "SELECT 
				a.ItemID,
				b.LocationID,
				b.GroupInCharge
			FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
			WHERE 
				a.ItemID NOT IN (SELECT c.ItemID FROM TEMP_INVENTORY_STOCKTAKE_RESULT AS c WHERE c.ItemType = ".ITEM_TYPE_SINGLE ." and c.UserID='".$UserID."') AND
				a.ItemID NOT IN (
									SELECT 
											DISTINCT d.ItemID 
									FROM 
											INVENTORY_ITEM_SINGLE_EXT AS d INNER JOIN
											INVENTORY_ITEM_SINGLE_STATUS_LOG AS e ON (d.ItemID = e.ItemID)
											
									WHERE 
											e.Action IN (".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION.",".ITEM_ACTION_STOCKTAKE_OTHER_LOCATION.",".ITEM_ACTION_STOCKTAKE_NOT_FOUND.")
											AND d.LocationID IN ('".$target_location_id."')
											AND e.RecordDate BETWEEN '$StocktakePeriod_StartDate' AND '$StocktakePeriod_EndDate'
								) AND
				a.ItemType = ".ITEM_TYPE_SINGLE." AND
				b.LocationID IN ('".$target_location_id."');
			";
	
	$SingleItemNotFoundArray = $linventory->returnArray($sql,3);
	## Single Item Not Found
	if(sizeof($SingleItemNotFoundArray)>0)
	{	
		for($i=0; $i<sizeof($SingleItemNotFoundArray); $i++)
		{
			list($item_id, $location_id, $admin_group_id) = $SingleItemNotFoundArray[$i];
			if($stocktake_datetime == "")
			{
				$stocktake_datetime = date("Y-m-d H:i:s");
			}
			## Check any missing record exist
			$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$item_id."' AND RecordStatus = 0";
			$missing_record_exist = $linventory->returnArray($sql,1);
			
			if(sizeof($missing_record_exist)>0)
			{
				## if missing record is exist, update the missing record.
				for($j=0; $j<sizeof($missing_record_exist); $j++)
				{
					list($record_id) = $missing_record_exist[$j];
					$sql = "UPDATE 
								INVENTORY_ITEM_MISSING_RECORD
							SET
								ItemID = '$item_id',
								LocationID = '$location_id', 
								AdminGroupID = '$admin_group_id',
								RecordDate = '$curr_date',
								MissingQty = 1,
								PersonInCharge = '$UserID',
								RecordType = 0,
								RecordStatus = 0,
								DateModified = '$stocktake_datetime'
							WHERE 
								RecordID = '".$record_id."'
							";
					$linventory->db_db_query($sql);
					//echo "5: ".$sql."<BR><br>";
				}
			}
			else
			{
				## if missing record NOT exist, create a new missing record.
				$sql = "INSERT INTO 
							INVENTORY_ITEM_MISSING_RECORD
							(ItemID, LocationID, AdminGroupID, RecordDate, MissingQty, PersonInCharge, 
							RecordType, RecordStatus, DateInput, DateModified)
						VALUES
							('$item_id','$location_id','$admin_group_id','$curr_date',1,'$UserID',0,0,'$stocktake_datetime','$stocktake_datetime')
						";
				//echo "6: ".$sql."<BR><br>";
				$linventory->db_db_query($sql);
			}
			
			## create a stocktake not found into the log table.
			$sql = "INSERT INTO 
						INVENTORY_ITEM_SINGLE_STATUS_LOG 
						(ItemID, RecordDate, Action, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified) 
					VALUES
						('$item_id', '$curr_date','".ITEM_ACTION_STOCKTAKE_NOT_FOUND."','$UserID','0','0','$stocktake_datetime','$stocktake_datetime')
					";
			//echo "7: ".$sql."<BR><br>";
			$single_result[] = $linventory->db_db_query($sql);
		}
	}
}


## Bulk Item - Handle the bulk item occur in the CSV only ##
$sql = "SELECT 
			a.ItemID, c.NameEng, 
			IF(a.LocationID = b.LocationID,'".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION."','".ITEM_ACTION_STOCKTAKE_OTHER_LOCATION."') AS StocktakeResult,
			b.LocationID AS OriginalLocation,
			if(b.Quantity='' or b.Quantity is null, '0', b.Quantity) AS OriginalQty,
			a.LocationID AS NewLocation,
			a.Quantity AS NewQuantity,
			(a.Quantity - if(b.Quantity='' or b.Quantity is null, '0', b.Quantity)) as QtyDiff,
			if(b.GroupInCharge='' or b.GroupInCharge is null, a.AdminGroupID, b.GroupInCharge) AS AdminGroup, 
			a.StocktakeDate,
			a.StocktakeTime,
			if(b.FundingSourceID='' or b.FundingSourceID is null, a.FundingSourceID, b.FundingSourceID) AS FundingSourceID
		FROM 
			TEMP_INVENTORY_STOCKTAKE_RESULT AS a 
			LEFT JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND a.AdminGroupID=b.GroupInCharge AND a.FundingSourceID=b.FundingSourceID AND a.LocationID=b.LocationID) 
			LEFT JOIN INVENTORY_ITEM AS c ON (a.ItemID = c.ItemID) 
		WHERE 
			a.ItemType = ".ITEM_TYPE_BULK ." and a.UserID='".$UserID."'";
$bulk_stocktake_result = $linventory->returnArray($sql,8);
// debug_pr($bulk_stocktake_result);
// exit;
if(sizeof($bulk_stocktake_result)>0)
{
	for($i=0; $i<sizeof($bulk_stocktake_result); $i++)
	{
		list($item_id, $item_name, $stocktake_result, $original_location_id, $original_qty, $new_location_id, $new_qty, $qty_diff, $admin_group_id, $stocktake_date, $stocktake_time, $funding_source_id) = $bulk_stocktake_result[$i];
		
		$stocktake_datetime = $stocktake_date." ".$stocktake_time;
		
		if($stocktake_result == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION)
		{
			if($qty_diff > 0)
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FundingSourceID='$funding_source_id' AND RecordStatus = 0";
				$surplus_record_exist = $linventory->returnVector($sql);
				
				if(sizeof($surplus_record_exist)>0)
				{
					$target_surplus_recode = implode(",",$surplus_record_exist);
					$sql = "UPDATE 
								INVENTORY_ITEM_SURPLUS_RECORD 
							SET
								ItemID = $item_id,
								LocationID = $new_location_id,
								AdminGroupID = $admin_group_id,
								RecordDate = '$curr_date',
								SurplusQty = ".abs($qty_diff).",
								PersonInCharge = $UserID,
								RecordType = 0,
								RecordStatus = 0,
								DateModified = '$stocktake_datetime',
								FundingSourceID = '$funding_source_id' 
							WEHRE
								RecordID IN ($target_surplus_recode)
							";
					//echo "8: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}else{
					$sql = "INSERT INTO 
									INVENTORY_ITEM_SURPLUS_RECORD 
									(ItemID, LocationID, AdminGroupID, RecordDate, SurplusQty, PersonInCharge, 
									RecordType, RecordStatus, DateInput, DateModified, FundingSourceID)
							VALUES
									('$item_id', '$new_location_id', '$admin_group_id', '$curr_date', ".abs($qty_diff).", '$UserID',
									0, 0, '$stocktake_datetime', '$stocktake_datetime', '$funding_source_id')
							";
					//echo "9: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			else
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FundingSourceID='$funding_source_id' AND RecordStatus = 0";
				$arr_record_exist = $linventory->returnVector($sql);
				if(sizeof($arr_record_exist)>0)
				{
					$exist_record = implode(",",$arr_record_exist);
					$sql = "DELETE FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE RecordID IN ($exist_record)";
					//echo "10: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			
			if($qty_diff < 0)
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FudningSourceID='$funding_source_id' AND RecordStatus = 0";
				$arr_record_exist = $linventory->returnVector($sql);
				
				if(sizeof($arr_record_exist)>0)
				{
					$exist_record = implode(",",$arr_record_exist);
	
					$sql = "UPDATE 
								INVENTORY_ITEM_MISSING_RECORD
							SET
								ItemID = $item_id,
								LocationID = $new_location_id,
								AdminGroupID = $admin_group_id,
								RecordDate = '$curr_date',
								MissingQty = ".abs($qty_diff).",
								PersonInCharge = $UserID,
								RecordType = 0,
								RecordStatus = 0,
								DateModified = '$stocktake_datetime',
								FundingSourceID = '$funding_source_id' 
							WHERE
								RecordID IN ($exist_record)
							";
					$linventory->db_db_query($sql);
				}
				else
				{					
					$sql = "INSERT INTO 
									INVENTORY_ITEM_MISSING_RECORD
									(ItemID, LocationID, AdminGroupID, RecordDate, MissingQty, PersonInCharge, 
									RecordType, RecordStatus, DateInput, DateModified, FundingSourceID)
							VALUES
									('$item_id', '$new_location_id', '$admin_group_id', '$curr_date', ".abs($qty_diff).", '$UserID',
									0, 0, '$stocktake_datetime', '$stocktake_datetime','$funding_source_id')
							";
					//echo "11: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			else
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FundingSOurceID='$funding_source_id' AND RecordStatus = 0";
				$arr_record_exist = $linventory->returnVector($sql);
				if(sizeof($arr_record_exist)>0)
				{
					$exist_record = implode(",",$arr_record_exist);
					$sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE RecordID IN ($exist_record)";
					//echo "12: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			
			$sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG 
							(ItemID, RecordDate, Action, QtyChange, PurchaseDate, PurchasedPrice, FundingSource, 
							SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo,
							PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, LocationID,
							GroupInCharge, StockCheckQty)
					VALUES
							('$item_id', '$stocktake_date', '".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION."', '', '', '', '$funding_source_id',
							'', '', '', '', '', '', 
							'$UserID', '', '', '', '', '$stocktake_datetime', '$stocktake_datetime', '$new_location_id', 
							'$admin_group_id', '$new_qty')
					";
			//echo "13: ".$sql."<BR><br>";
			$bulk_result[] = $linventory->db_db_query($sql);
		}
		else if($stocktake_result == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)
		{
			if($qty_diff > 0)
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FundingSourceID='$funding_source_id' AND RecordStatus = 0";
				$surplus_record_exist = $linventory->returnVector($sql);
				
				if(sizeof($surplus_record_exist)>0)
				{
					$target_surplus_recode = implode(",",$surplus_record_exist);
					$sql = "UPDATE 
								INVENTORY_ITEM_SURPLUS_RECORD 
							SET
								ItemID = $item_id,
								LocationID = $new_location_id,
								AdminGroupID = $admin_group_id,
								RecordDate = '$curr_date',
								SurplusQty = ".abs($qty_diff).",
								PersonInCharge = $UserID,
								RecordType = 0,
								RecordStatus = 0,
								DateModified = '$stocktake_datetime',
								FundingSourceID = '$funding_source_id' 
							WEHRE
								RecordID IN ($target_surplus_recode)
							";
					//echo "8: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}else{
					$sql = "INSERT INTO 
									INVENTORY_ITEM_SURPLUS_RECORD 
									(ItemID, LocationID, AdminGroupID, RecordDate, SurplusQty, PersonInCharge, 
									RecordType, RecordStatus, DateInput, DateModified, FundingSourceID)
							VALUES
									('$item_id', '$new_location_id', '$admin_group_id', '$curr_date', ".abs($qty_diff).", '$UserID',
									0, 0, '$stocktake_datetime', '$stocktake_datetime', '$funding_source_id')
							";
					//echo "9: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			else
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FundingSourceID='$funding_source_id' AND RecordStatus = 0";
				$arr_record_exist = $linventory->returnVector($sql);
				if(sizeof($arr_record_exist)>0)
				{
					$exist_record = implode(",",$arr_record_exist);
					$sql = "DELETE FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE RecordID IN ($exist_record)";
					//echo "10: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			
			if($qty_diff < 0)
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FudningSourceID='$funding_source_id' AND RecordStatus = 0";
				$arr_record_exist = $linventory->returnVector($sql);
				
				if(sizeof($arr_record_exist)>0)
				{
					$exist_record = implode(",",$arr_record_exist);
	
					$sql = "UPDATE 
								INVENTORY_ITEM_MISSING_RECORD
							SET
								ItemID = $item_id,
								LocationID = $new_location_id,
								AdminGroupID = $admin_group_id,
								RecordDate = '$curr_date',
								MissingQty = ".abs($qty_diff).",
								PersonInCharge = $UserID,
								RecordType = 0,
								RecordStatus = 0,
								DateModified = '$stocktake_datetime',
								FundingSourceID = '$funding_source_id' 
							WHERE
								RecordID IN ($exist_record)
							";
					$linventory->db_db_query($sql);
				}
				else
				{					
					$sql = "INSERT INTO 
									INVENTORY_ITEM_MISSING_RECORD
									(ItemID, LocationID, AdminGroupID, RecordDate, MissingQty, PersonInCharge, 
									RecordType, RecordStatus, DateInput, DateModified, FundingSourceID)
							VALUES
									('$item_id', '$new_location_id', '$admin_group_id', '$curr_date', ".abs($qty_diff).", '$UserID',
									0, 0, '$stocktake_datetime', '$stocktake_datetime','$funding_source_id')
							";
					//echo "11: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			else
			{
				$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '$item_id' AND LocationID = '$new_location_id' AND AdminGroupID = '$admin_group_id' AND FundingSOurceID='$funding_source_id' AND RecordStatus = 0";
				$arr_record_exist = $linventory->returnVector($sql);
				if(sizeof($arr_record_exist)>0)
				{
					$exist_record = implode(",",$arr_record_exist);
					$sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE RecordID IN ($exist_record)";
					//echo "12: ".$sql."<BR><br>";
					$linventory->db_db_query($sql);
				}
			}
			
			$sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG 
							(ItemID, RecordDate, Action, QtyChange, PurchaseDate, PurchasedPrice, FundingSource, 
							SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo,
							PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, LocationID,
							GroupInCharge, StockCheckQty)
					VALUES
							('$item_id', '$stocktake_date', '".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION."', '', '', '', '$funding_source_id',
							'', '', '', '', '', '', 
							'$UserID', '', '', '', '', '$stocktake_datetime', '$stocktake_datetime', '$new_location_id', 
							'$admin_group_id', '$new_qty')
					";
			//echo "13: ".$sql."<BR><br>";
			$bulk_result[] = $linventory->db_db_query($sql);
		}
	}
}


if($HandleOthers)
{
	//echo "<B>Handle other bulk item not in csv but in the csv's location</b><BR>";
	## Handle other bulk item not in csv but in the csv's location
	$sql = "SELECT 
				a.ItemID,
				b.LocationID,
				b.GroupInCharge,
				b.Quantity,
				b.FundingSourceID 
			FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID)
			WHERE 
				a.ItemID NOT IN (SELECT c.ItemID FROM TEMP_INVENTORY_STOCKTAKE_RESULT AS c WHERE c.ItemType = ".ITEM_TYPE_BULK ." and c.UserID='".$UserID."') AND
				a.ItemID NOT IN (
									SELECT 
										d.ItemID 
									FROM 
										INVENTORY_ITEM_BULK_LOCATION AS d INNER JOIN
										INVENTORY_ITEM_BULK_LOG AS e ON (d.ItemID = e.ItemID)
									WHERE
										e.Action IN (".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION.",".ITEM_ACTION_STOCKTAKE_OTHER_LOCATION.",".ITEM_ACTION_STOCKTAKE_NOT_FOUND.")
										AND d.LocationID IN ('".$target_location_id."')
								) AND
				a.ItemType = ".ITEM_TYPE_BULK." AND
				b.LocationID IN ('".$target_location_id."');
			";
	$BulkItemNotFoundArray = $linventory->returnArray($sql,4);
	if(sizeof($BulkItemNotFoundArray)>0)
	{
		for($i=0; $i<sizeof($BulkItemNotFoundArray); $i++){
			list($item_id,$location_id,$admin_group_id,$new_qty,$funding_source_id) = $BulkItemNotFoundArray[$i];
			if($stocktake_datetime == "")
			{
				$stocktake_datetime = date("Y-m-d H:i:s");
			}
			$sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$item_id."' AND LocationID = '".$location_id."' AND AdminGroupID = '".$admin_group_id."' AND RecordStatus = 0";
			$surplus_record_exist = $linventory->returnVector($sql);
			if(sizeof($surplus_record_exist)>0){
				$target_surplus_record = implode(",",$surplus_record_exist);
				$sql = "DELETE FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE RecordID IN ($target_surplus_record)";
				//echo "14: ".$sql."<BR><br>";
				$linventory->db_db_query($sql);
			}
			
			$sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$item_id."' AND LocationID = '".$location_id."' AND AdminGroupID = '".$admin_group_id."' AND RecordStatus = 0";
			$missing_record_exist = $linventory->returnVector($sql);
			if(sizeof($missing_record_exist)>0){
				$exist_record = implode(",",$missing_record_exist);
				$sql = "UPDATE 
							INVENTORY_ITEM_MISSING_RECORD
						SET
							ItemID = $item_id,
							LocationID = $location_id,
							AdminGroupID = $admin_group_id,
							RecordDate = '$curr_date',
							MissingQty = ".abs($new_qty).",
							PersonInCharge = $UserID,
							RecordType = 0,
							RecordStatus = 0,
							DateModified = '$stocktake_datetime',
							FundingSourceID = '$funding_source_id' 
						WHERE 
							RecordID IN ($exist_record)
						";
				//echo "15: ".$sql."<BR><br>";
				$linventory->db_db_query($sql);						
			}
			else
			{
				$sql = "INSERT INTO 
								INVENTORY_ITEM_MISSING_RECORD
								(ItemID, LocationID, AdminGroupID, RecordDate, MissingQty, PersonInCharge, 
								RecordType, RecordStatus, DateInput, DateModified, FundingSourceID)
						VALUES
								('$item_id', '$location_id', '$admin_group_id', '$curr_date', ".abs($new_qty).", '$UserID',
								0, 0, '$stocktake_datetime', '$stocktake_datetime', '$funding_source_id')
						";
				//echo "16: ".$sql."<BR><br>";
				$linventory->db_db_query($sql);
			}
						
			$sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG 
							(ItemID, RecordDate, Action, QtyChange, PurchaseDate, PurchasedPrice, FundingSource, 
							SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo,
							PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, LocationID,
							GroupInCharge, StockCheckQty)
					VALUES
							('$item_id', '$stocktake_date', ".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION.", '', '', '', '$funding_source_id', '', '', '', '', '', '', '$UserID', '',
							'', '', '', '$stocktake_datetime', '$stocktake_datetime', '$location_id', '$admin_group_id', '0')
					";
			//echo "17: ".$sql."<BR><br>";
			$bulk_result[] = $result = $linventory->db_db_query($sql);
		}
	}
}

if(is_array($single_result))
{
	if(!in_array(false,$single_result))
	{
		$pass_checking_single = 1;
	}
	else
	{
		$pass_checking_single = 0;
	}
}
else
{
	$pass_checking_single = 1;
}

if(is_array($bulk_result))
{
	if(!in_array(false,$bulk_result))
	{
		$pass_checking_bulk = 1;
	}
	else
	{
		$pass_checking_bulk = 0;
	}
}
else
{
	$pass_checking_bulk = 1;
}

if( ($pass_checking_single==1) && ($pass_checking_bulk==1) )
{
	$linventory->Commit_Trans();
	header("location: offline.php?xmsg=".urlencode($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ImportMessage']['ReturnImportSuccess']));
}
else
{
	$linventory->RollBack_Trans();
	header("location: offline.php?xmsg=".urlencode($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ImportMessage']['ReturnImportFail']));
}

intranet_closedb();
?>