<?php
# using: 

######################################
# Date: 	2019-05-13 Henry
# 			Security fix: SQL without quote
#
#   Date:	 2019-05-01 (Henry) 
# 			security issue for SQL
#
#	Date:	2018-02-07	Henry
#			add access right checking [Case#E135442]
#
#	Date:	2015-01-05	Henry
#			added link to handle non-stocktake items page
#
#	Date:	2014-12-15	Henry
#			display remarks if not in stockstake period
#
#	Date:	2013-04-29	YatWoon
#			add "group" filter 
######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_Stocktake";
$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui	= new liblocation_ui();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$curr_date = date("Y-m-d");
if(!(($linventory->getStocktakePeriodStart() <= $curr_date) && ($curr_date <= $linventory->getStocktakePeriodEnd()))){
	//header("location: ../inventory/items_full_list.php");
	$TAGS_OBJ[] = array($i_InventorySystem['StockTake'], '', 1);
	$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START();
	
	$x ="<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">
			<tr>
				<td colspan=\"2\" align=\"center\" class=\"tabletext\">".$SysMsg."</td>
			</tr>
		</table>
		<fieldset class='instruction_box_v30'>
		<legend>".$Lang['General']['Remark']."</legend>
			- ".$Lang['eInventory']['StocktakeConductedDuringPeriod']."<br/>
			- ".$Lang['eInventory']['Report']['StocktakePeriod'].": ".$linventory->getStocktakePeriodStart()." ".$Lang['General']['To']." ".$linventory->getStocktakePeriodEnd()."<br/>
			- <a href='offline_stocktake_application.AGX'>".$Lang['eInventory']['FieldTitle']['DownloadOfflineReaderApplication']."</a>";
		
		if($curr_date > $linventory->getStocktakePeriodEnd()){
			$x .="<br/>- <a href='stock_take1_follow.php?itemType=1'>".$Lang['eInventory']['HandleNotUndergoneStocktakeSingle']."</a>";
			$x .="<br/>- <a href='stock_take1_follow.php?itemType=2'>".$Lang['eInventory']['HandleNotUndergoneStocktakeBulk']."</a>";
		}
		$x .="</fieldset>";
	
	echo $x;
	$linterface->LAYOUT_STOP();
	intranet_closedb();
	exit();
}


$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/index.php", 1);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/offline.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step1,1);
$STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step2,0);
$STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step3,0);

### check user is eInventory admin or not ###
$isAdmin = $linventory->IS_ADMIN_USER($UserID);

# get the user group #
if($linventory->IS_ADMIN_USER($UserID))
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
else
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
$arr_result = $linventory->returnVector($sql);

if(sizeof($arr_result)>0)
{
	$targetGroup = implode(",",$arr_result);
}
# end #
$sql = "SELECT AdminGroupID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_ADMIN_GROUP where AdminGroupID in ('". implode("','",$arr_result)."') ORDER BY DisplayOrder";
$admin_group = $linventory->returnArray($sql);
$admin_group_selection = getSelectByArray($admin_group, "name=\"admin_group_id\"", $admin_group_id, 1, 0);


if ($isAdmin)
{
	/*
	## Location Selection - Building ##
	$opt_LocationBuilding = $llocation_ui->Get_Building_Selection($targetLocationBuilding, "targetLocationBuilding", "document.form1.flag.value=0; javascript:checkForm();", 0, 0, "");
	$location_selection = $opt_LocationBuilding;
	
	# Generate Location Selection Box - Floor #
	if($targetLocationBuilding != "") {
		$opt_LocationLevel = $llocation_ui->Get_Floor_Selection($targetLocationBuilding, $targetLocationLevel, "targetLocationLevel", "document.form1.flag.value=0; javascript:checkForm();", 0, 0, "");
		$location_selection .= $opt_LocationLevel;
	}
	# Generate Location Selection Box - Room #
	if(($targetLocationLevel != "") && ($targetLocationBuilding != "")) {
		$opt_Location = $llocation_ui->Get_Room_Selection($targetLocationLevel, $targetLocation, "targetLocation", "document.form1.flag.value=1; javascript:checkForm();", 0, "", "");
		$location_selection .= $opt_Location;
	}
	# End #
	*/
	$location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetLocation, "targetLocation", "", 0, "", "", "", "", "");
	
}
else
{
	## Location
	$sql = "(SELECT DISTINCT LocationID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge IN ('".implode("','",$arr_result)."')) UNION (SELECT DISTINCT LocationID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge IN ('".implode("','",$arr_result)."'))";
	$tempLocationID = $linventory->returnVector($sql);
	
	if(sizeof($tempLocationID)>0)
	{
		$targetLocationID = implode("','",$tempLocationID);
	}
	
	/*
	## Building query
	$sql = "SELECT DISTINCT a.BuildingID, ".$linventory->getInventoryNameByLang("a.")." FROM INVENTORY_LOCATION_BUILDING AS a INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1 AND c.LocationID IN ($targetLocationID)";
	$arrBuilding = $linventory->returnArray($sql,2);
	
	$location_selection = getSelectByArray($arrBuilding,"name='targetLocationBuilding' id='targetLocationBuilding' onChange='document.form1.flag.value=0; javascript:checkForm();'",$targetLocationBuilding);
	
	if($targetLocationBuilding != "")
	{
		## Floor query
		$sql = "SELECT DISTINCT a.LocationLevelID, ".$linventory->getInventoryNameByLang("a.")." FROM INVENTORY_LOCATION_LEVEL AS a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND b.LocationID IN ($targetLocationID) AND a.BuildingID = $targetLocationBuilding";
		$arrFloor = $linventory->returnArray($sql,2);
		$location_selection .= getSelectByArray($arrFloor,"name='targetLocationLevel' id='targetLocationLevel' onChange='document.form1.flag.value=0; javascript:checkForm();'",$targetLocationLevel);
	}
	
	if(($targetLocationBuilding != "") && ($targetLocationLevel != ""))
	{
		## Room query
		$sql = "SELECT DISTINCT LocationID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_LOCATION WHERE RecordStatus = 1 AND LocationID IN ($targetLocationID) AND LocationLevelID = $targetLocationLevel";
		$arrRoom = $linventory->returnArray($sql,2);
		$location_selection .= getSelectByArray($arrRoom,"name='targetLocation' id='targetLocation' onChange='document.form1.flag.value=1; javascript:checkForm();'",$targetLocation);
	}
	*/
	
	$sql = "SELECT DISTINCT a.BuildingID FROM INVENTORY_LOCATION_BUILDING AS a INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1 AND c.LocationID IN ('".$targetLocationID."')";
	$arrBuildingOneD = $linventory->returnVector($sql);
	
	$sql = "SELECT DISTINCT a.LocationLevelID FROM INVENTORY_LOCATION_LEVEL AS a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND b.LocationID IN ('".$targetLocationID."') ";
	$arrLevelOneD = $linventory->returnVector($sql);
	
	$location_selection .= $llocation_ui->Get_Building_Floor_Room_Selection($targetLocation, "targetLocation", "", 0, "", "", $arrBuildingOneD, $arrLevelOneD, $tempLocationID);
}

?>

<script language="javascript">
<?echo $js_arr;?>

function checkForm()
{
	var obj = document.form1;
	var jChecking = true;	
	
	if(check_select(document.form1.targetLocation,"<?=$Lang['General']['PleaseSelect']?> <?=$i_InventorySystem['Location']?>",""))
	{
		return true;
	}
	
	return false;
}
</script>

<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="center" class="tabletext"><?= $SysMsg ?></td>
	</tr>
</table>

<?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>

<form name="form1" action="stock_take1.php" method="POST" onSubmit="return checkForm();">
<div class="this_table">
<table class="form_table_v30">

<tr>
	<td class="field_title"><?=$i_InventorySystem['Location']?></td>
	<td colspan="3"><?=$location_selection?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_InventorySystem['Caretaker']?></td>
	<td colspan="3"><?=$admin_group_selection?></td>
</tr>

</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_next, "submit");?> 
<p class="spacer"></p>
</div>

</div>

<input type="hidden" name="group_list" value="<?=$targetGroup?>">
<input type="hidden" name="location" value="<?=$targetLocation?>">
<input type="hidden" name="targetType" value="<?=$i_InventorySystem_ItemType_Array[0][0];?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>