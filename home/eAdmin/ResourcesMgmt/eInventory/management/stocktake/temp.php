<?php
$targetLocation = 11;
$targetType = 1;
$group_list = "1,2,3,4,5,15";
    

if($targetLocation == "")
{
	header("location: index.php");
	exit();
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Management_Stocktake";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Get The Category Name ###
$sql = "
			SELECT 
				".$linventory->getInventoryNameByLang()."
			FROM 
				INVENTORY_LOCATION
			WHERE
				LocationID = $targetLocation
		";
$result = $linventory->returnArray($sql,1);
$infobar .= $linterface->GET_NAVIGATION2($result[0][0]." - ".$i_InventorySystem_Stocktake_List);
### End Of Getting The Category Name ###

$TAGS_OBJ[] = array($i_InventorySystem['StockCheck'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step1, 0);
$STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step2, 1);
$STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step3, 0);

# get the user group #
$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
$arr_result = $linventory->returnVector($sql);

if(sizeof($arr_result)>0)
{
	$targetGroup = implode(",",$arr_result);
}
# end #

if($targetType == 1)
{	
	# Single Item Start #
	$sql = "SELECT
					DISTINCT a.ItemID,
					a.ItemCode,
					".$linventory->getInventoryItemNameByLang("a.").",
					b.TagCode
			FROM 
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_ITEM_SINGLE_STATUS_LOG AS c ON (a.ItemID = c.ItemID AND c.Action != 3)
			WHERE
					a.ItemType = $targetType AND
					a.RecordStatus = 1 AND
					b.LocationID IN ($targetLocation) AND
					b.GroupInCharge IN ($targetGroup)
			ORDER BY
					a.NameEng, a.ItemID";

	$arr_result = $linventory->returnArray($sql,3);
	
	//$tag_code_table .= "<tr><td align=\"right\" class=\"tabletext\">{$i_InventorySystem_Category2_Barcode} &nbsp; <input type=\"text\" name=\"item_tag_code\" class=\"textboxnum\" onKeyPress=\"return disableEnterKey(event)\" ></td></tr>\n";
	
	//$table_content .= "<tr><td class=\"tabletext\" align=\"left\">$i_InventorySystem_ItemType_Single</td><td class=\"tablelink\" colspan=\"3\" align=\"right\"><a href=javascript:retrieveItemNotFound()><img src=\"$image_path/2007a/icon_view.gif\" border=0 alt='$button_select'>{$i_InventorySystem_Item_NotFound_Display}</a></td></tr>\n";
	$table_content .= "<tr><td colspan=\"7\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$table_content .= "<tr class=\"tablename\">";
	$table_content .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_ItemType_Single</td>";
	$table_content .= "<td class=\"tabletext\" colspan=\"6\" align=\"right\">{$i_InventorySystem_Stocktake_ReadBarcode} &nbsp; <input type=\"text\" name=\"item_tag_code\" class=\"textboxnum\" onKeyPress=\"return disableEnterKey(event)\" ></td>";
	$table_content .= "</tr>\n";
	$table_content .= "</table></td></tr>\n";
	$table_content .= "<tr class=\"tabletop\" valign=\"bottom\">";
	$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Item_Code."</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Item_Barcode."</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem_Item_Name."</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Status</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_StockCheck_Found <input type=checkbox onClick=(this.checked)?setAllFound(1):setAllFound(0) </td>";
	$table_content .= "</tr>\n";
	
	if(sizeof($arr_result)>0)
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list ($item_id, $item_code, $item_name, $item_tag) = $arr_result[$i];
			# get latest stock take time #
			$namefield = getNameFieldByLang2("b.");
			$sql = "SELECT IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), $namefield FROM INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) WHERE a.ACTION = 2 AND a.ItemID = $item_id GROUP BY a.ItemID";
			$arr_stock_take_time_person = $linventory->returnArray($sql,2);
			if(sizeof($arr_stock_take_time_person) > 0)
			{
				list($single_item_latest_stock_take_time, $person_in_charge) = $arr_stock_take_time_person[0];
				if($single_item_latest_stock_take_time == " ")
					$single_item_latest_stock_take_time = " - ";
				if($person_in_charge == " ")
					$person_in_charge = " - ";
			}
			$sql = "SELECT NewStatus FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE ItemID = '".$item_id."' ORDER BY DateInput DESC LIMIT 0,1";
			$arr_item_status = $linventory->returnVector($sql);
			if($arr_item_status[0] == ITEM_STATUS_NORMAL)
				$item_curr_status = $i_InventorySystem_ItemStatus_Normal;
			if($arr_item_status[0] == ITEM_STATUS_DAMAGED)
				$item_curr_status = $i_InventorySystem_ItemStatus_Damaged;
			if($arr_item_status[0] == ITEM_STATUS_REPAIR)
				$item_curr_status = $i_InventorySystem_ItemStatus_Repair;
						
			$j = $i+1;
			if($j%2 == 0)
			{
				$table_row_css = " class=\"tablerow1\" ";
				$textboxcolor = " #FFFFFF ";
			}
			else
			{
				$table_row_css = " class=\"tablerow2\" ";
				$textboxcolor = " #F5F5F5 ";
			}
		
			$table_content .= "<tr bgcolor=\"#FFFFCC\">";
			$table_content .= "<td class=\"tabletext\">$item_code</td>";
			$table_content .= "<td class=\"tabletext\">$item_tag</td>";
			$table_content .= "<td class=\"tabletext\">$item_name</td>";
			$table_content .= "<td class=\"tabletext\">$person_in_charge</td>";
			$table_content .= "<td class=\"tabletext\">$single_item_latest_stock_take_time</td>";
			$table_content .= "<td class=\"tabletext\">$item_curr_status</td>";
			$table_content .= "<td>";
			$table_content .= "<input type=\"hidden\" name=\"status_".$item_tag."\"  value=\"\" DISABLED >\n";
			$table_content .= "<span id=\"status_".$item_tag."_div\"value=\"\" class=\"tabletext\" ></span>";
			$table_content .= "<input type=\"checkbox\" name=\"ItemTag_".$item_tag."\" value=\"$item_tag\" onClick=\"(this.checked)?setItemFound(1,'$item_tag'):setItemFound(0,'$item_tag')\">\n";
			$table_content .= "</td>";
			$table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=7></td></tr>";
			
			$table_content .= "<input type=\"hidden\" name=\"hidden_status_$item_tag\" value=\"\">\n";
			$table_content .= "<input type=\"hidden\" name=\"item_id_$item_tag\" value=$item_id>\n";
			$arr_item_id[$i] = $item_id;
			$arr_item_tag[$i] = $item_tag;
		}
		$temp_id_list = implode(",",$arr_item_id);
		$temp_tag_list = implode(",",$arr_item_tag);
	}
	
	$table_content .= "<input type=\"hidden\" name=\"item_id_list\" value=\"$temp_id_list\">\n";
	$table_content .= "<input type=\"hidden\" name=\"item_tag_list\" value=\"$temp_tag_list\">\n";
	$table_content .= "<input type=\"hidden\" name=\"wrong_item_list\" value=\"\">\n";
	$table_content .= "<input type=\"hidden\" name=\"itemType\" value=$targetType>\n";
	
	if(sizeof($arr_result)==0)
	{
		$table_content .= "<tr class=\"tablerow1\"><td colspan=\"4\" class=\"tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	}
	$table_content .= "<tr class=\"tabletop\" height=\"20px\">
						<td colspan=\"7\" align=\"right\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_TotalQty: $i_InventorySystem_Stocktake_FoundQty <input type=\"text\" name=\"single_found_total\" class=\"stocktakenumbox2\" style=\"border: 0px 0px 0px 0px; background-color:#A6A6A6;\" READONLY value=\"0\"> / $i_InventorySystem_Stocktake_UnlistedQty <input type=\"text\" name=\"single_unlisted_total\" class=\"stocktakenumbox2\" style=\"border: 0px 0px 0px 0px; background-color:#A6A6A6;\" READONLY value=\"0\"> </td></tr>";
	$table_content .= "<tr><td class=\"dotline\" colspan=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
	# End Of Signel Item #
	
	# Bulk Item Start #
	$sql = "SELECT
					a.ItemID,
					a.ItemCode,
					".$linventory->getInventoryItemNameByLang("a.").",
					b.Quantity,
					b.GroupInCharge
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
					INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
			WHERE
					a.ItemType = 2 AND
					a.RecordStatus = 1 AND
					b.LocationID IN ($targetLocation) AND
					b.GroupInCharge IN ($targetGroup)
			ORDER BY
					a.NameEng, a.ItemID";
	//echo $sql;
	$arr_bulk_result = $linventory->returnArray($sql,5);
	
	//$bulk_table .= "<tr><td class=\"tabletext\" colspan=\"2\">$i_InventorySystem_ItemType_Bulk</td></tr>";
	//$bulk_table .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\" >".$i_InventorySystem_Item_Name."</td>";
	$bulk_table .= "<tr><td colspan=\"9\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$bulk_table .= "<tr class=\"tablename\">";
	$bulk_table .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_ItemType_Bulk</td>";
	$bulk_table .= "</tr>\n";
	$bulk_table .= "</table></td></tr>\n";
	$bulk_table .= "<tr class=\"tabletop\">
	                    <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>
	                    <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>
	                    <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>
	                    <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>
	                    <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>
	                    <td colspan=\"4\" align=\"center\" class=\"whiteborder tabletopnolink\"><DIV align=\"center\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_StocktakeQty</DIV></td>
	                  </tr>
	                  <tr class=\"tablerow2\">
	                    <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_NormalQty</td>
	                    <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_RequestWriteOffQty</td>
	                    <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_TotalQty</td>
	                    <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_VarianceQty</td>
	                  </tr>";
                                  
	//$bulk_table .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_PIC</td>";
	//$bulk_table .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
	//$bulk_table .= "<td class=\"tabletopnolink\">$i_InventorySystem_StockFound</td>";
	//$bulk_table .= "<td class=\"tabletopnolink\">$i_InventorySystem_WriteOffQty</td>";
	$bulk_table .= "</tr>\n";
				
	if(sizeof($arr_bulk_result)>0)
	{
		for($i=0; $i<sizeof($arr_bulk_result); $i++)
		{
			list($bulk_item_id, $bulk_item_code, $bulk_item_name, $bulk_item_qty, $bulk_item_group_id) = $arr_bulk_result[$i];
			
			# get the latest stock take time #
			$namefield = getNameFieldByLang2("b.");
			$sql = "SELECT IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), $namefield FROM INVENTORY_ITEM_BULK_LOG AS a INNER JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) WHERE a.ACTION = 2 AND a.ItemID = $bulk_item_id GROUP BY a.ItemID";
			$arr_stock_take_time_person = $linventory->returnArray($sql,2);
			if(sizeof($arr_stock_take_time_person) > 0)
				list($bulk_item_latest_stock_take_time, $person_in_charge) = $arr_stock_take_time_person[0];
			# end #
									
			$j=$i+1;
			if($j%2 == 0)
				$table_row_css = " class = \"tablerow1\" ";
			else
				$table_row_css = " class = \"tablerow2\" ";
				
			$bulk_table .= "<tr bgcolor=\"#E5F2FA\"><td class=\"bulk tabletext\">$bulk_item_code</td>";
			$bulk_table .= "<td class=\"tabletext\">$bulk_item_name</td>";
			$bulk_table .= "<td class=\"tabletext\">$person_in_charge</td>";
			$bulk_table .= "<td class=\"tabletext\">$bulk_item_latest_stock_take_time</td>";
			//$bulk_table .= "<td width=\"10%\"><input type=\"text\" name=\"bulk_item_qty_found_".$bulk_item_id."_".$bulk_item_group_id."\" class=\"textboxnum\" value=\"$bulk_item_qty\"></td>";
			$bulk_table .= "<td class=\"tabletext\">$bulk_item_qty</td>";
			$bulk_table .= "<td width=\"10%\"><input type=\"text\" name=\"bulk_item_qty_found_".$bulk_item_id."_".$bulk_item_group_id."\" class=\"stocktakenumbox\" value=\"$bulk_item_qty\"></td>";
			$bulk_table .= "<td width=\"5%\"><input type=\"text\" name=\"bulk_item_qty_write_off_".$bulk_item_id."_".$bulk_item_group_id."\" class=\"stocktakenumbox\" value=\"0\" ></td>";
			$bulk_table .= "<td width=\"5%\"><input type=\"text\" name=\"\" class=\"stocktakenumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\"></td>";
			$bulk_table .= "<td width=\"5%\"><input type=\"text\" name=\"\" class=\"stocktakenumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\"></td>";
			$bulk_table .= "</tr>\n";
			$bulk_table .= "<input type=\"hidden\" name=\"bulk_item_group_id_$bulk_item_id\" value=$bulk_item_group_id>";
			$bulk_table .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=9></td></tr>";
			
			if($bulk_item_list != "")
				$bulk_item_list = $bulk_item_list.",".$bulk_item_id;
			else
				$bulk_item_list = $bulk_item_list.$bulk_item_id;
		}
		$bulk_table .= "<input type=\"hidden\" name=\"bulk_item_id\" value=\"$bulk_item_list\">\n";

	}
	if(sizeof($arr_bulk_result) == 0)
	{
		$bulk_table .= "<tr><td class=\"tabletext\" colspan=\"9\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	}
	//$bulk_table .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"9\"></td></tr>";
	# End Of Bulk Item #
}
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;};
</style>
<script language="JavaScript">
isMenu = true;
total_single_found = 0;
total_single_unlisted = 0;
</script>

<script language="javascript">
<!--
function setAllFound(val)
{
	var tmp_tag_list2 = document.getElementById("item_tag_list");
    var tmp_tag_code2 = tmp_tag_list2.value.split(",");
    for(i=0; i<tmp_tag_code2.length; i++)
    {
	    var tmp_str = eval("document.form1.ItemTag_"+tmp_tag_code2[i]);
	    var pass_val = tmp_tag_code2[i];
	    tmp_str.checked = val;
	    setItemFound(val,pass_val);
	}
}
function setItemFound(val,item_tag_code)
{
	var tmp_obj = document.getElementById("status_"+item_tag_code+"_div");	    	
	if(val == 1)
	{
		var tmp_obj2 = eval("document.form1.hidden_status_"+item_tag_code);
		var tmp_obj3 = eval("document.form1.ItemTag_"+item_tag_code);
		var tmp_obj4 = eval("document.form1.single_found_total");
		
		//tmp_obj.value = "<?=$i_InventorySystem_Found?>";
		tmp_obj.innerHTML = "<font color=\"#006600\"><?=$i_InventorySystem_Found?></font>";
		tmp_obj2.value = 1;
		tmp_obj3.checked = true;
		total_single_found++;
		tmp_obj4.value = total_single_found;
	}
	if(val == 0)
	{
		var tmp_obj5 = eval("document.form1.single_found_total");
		total_single_found--;
    	tmp_obj.innerHTML = "";
    	tmp_obj5.value = total_single_unlisted;
	}
}
-->
</script>

<div id="ToolMenu" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
<!--
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
-->

<script language="javascript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveItemNotFound()
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("ToolMenu2");
        
        //showMenu2("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        myElement.innerHTML = "<table border=1 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$i_InventorySystem_Loading?></td></tr></table>";
        
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var miss_item_list = obj.wrong_item_list.value;
        var path = "getItemNotFound.php?MissItemList=" + miss_item_list;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function disableEnterKey(e)
{
	var key;      
	if(window.event)
	{
		key = window.event.keyCode; //IE
	}
	else
	{
		key = e.which; //firefox
	}
	
	if(key == 13)
	{
		var tag_code = document.form1.item_tag_code.value;
    	
    	var tmp_id_list = document.getElementById("item_id_list")
    	var tmp_tag_list = document.getElementById("item_tag_list");
    	var tmp_check = 0;
    	var tmp_result = '';
    	
    	
    	var tmp_tag_code = tmp_tag_list.value.split(",");

    	for (i=0; i<tmp_tag_code.length; i++)
    	{
	    	var TmpStr1  = new String(tag_code).toUpperCase();	    	
	    	var TmpStr2  = new String(tmp_tag_code[i]).toUpperCase();

	    	if (TmpStr1 == TmpStr2)
	    	{
		    	tmp_check = 1;		    	
		    	tag_code = tmp_tag_code[i];
	    	}	 
	    	else if (tag_code == tmp_tag_code[i])
	    	{
	    		tmp_check = 1;
    		}
    		else
    		{

    		}
    	}
    	
    	if(tmp_check == 1)
    	{
	    	play('found.wav');
	    	//var tmp_obj = eval("document.form1.status_"+tag_code);
	    	var tmp_obj = document.getElementById("status_"+tag_code+"_div");	    	
	    	var tmp_obj2 = eval("document.form1.hidden_status_"+tag_code);
	    	var tmp_obj3 = eval("document.form1.ItemTag_"+tag_code);
	    	var tmp_obj4 = eval("document.form1.single_found_total");
	    	
	    	//tmp_obj.value = "<?=$i_InventorySystem_Found?>";
	    	tmp_obj.innerHTML = "<font color=\"#006600\"><?=$i_InventorySystem_Found?></font>";
	    	tmp_obj2.value = 1;
	    	tmp_obj3.checked = true;
	    	total_single_found++;
	    	tmp_obj4.value = total_single_found;
	    	
    	}
    	if(tmp_check == 0)
    	{   
	    	play('notfound.wav');
	    	var tmp_obj5 = eval("document.form1.single_unlisted_total");
	    	total_single_unlisted++;
	    	tmp_obj5.value = total_single_unlisted;
	    	//alert("<?=$i_InventorySystem_Item_NotFound?>");
	    	
	    	if(document.form1.wrong_item_list.value == "")
	    	{
		    	document.form1.wrong_item_list.value = tag_code;
	    	}
	    	else
	    	{
		    	document.form1.wrong_item_list.value = document.form1.wrong_item_list.value + "," + tag_code;
	    	}
    	}    	
    	document.form1.item_tag_code.value="";
	}
    
    return (key != 13);
}

function play(file) {
    var embed = document.createElement("embed");
    
    embed.setAttribute('src', file);
    embed.setAttribute('hidden', true);
    embed.setAttribute('autostart', true);
 
    document.body.appendChild(embed);
}

function checkForm()
{
	var obj = document.form1;
	var tmpCheck = 0;
	<?
	if(sizeof($arr_bulk_result)>0)
	{
		for($i=0; $i<sizeof($arr_bulk_result); $i++)
		{
			list($bulk_item_id, $bulk_item_code, $bulk_item_name, $bulk_item_qty, $bulk_item_group_id) = $arr_bulk_result[$i];
	?>
			var item_assigned_qty = <?=$bulk_item_qty;?>;
			var targetFound = "bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>;
			var targetWriteOff = "bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>;
			
			tmp_1 = eval("document.form1.bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".value");
			tmp_2 = eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".value");
			
			if((tmp_1 != "") && (tmp_2 != ""))
			{
				if((tmp_1 >= 0) && (tmp_2 >= 0))
				{
					ans = parseInt(tmp_1) + parseInt(tmp_2);
					tmpChecktextbox = 1;
				}
				else
				{
					alert("Please input a valid quantity");
					eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
					tmpChecktextbox = 0;
				}
			}
			else
			{
				alert("Please input a quantity");
				tmpChecktextbox = 0;
				if(tmp_1 == "")
					eval("document.form1.bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
				if(tmp_2 == "")
					eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
			}
			
			if(tmpChecktextbox == 1)
			{
				//if(item_assigned_qty < ans)
				//{
				//	alert("Please input a valid quantity");
				//	eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
				//	tmpCheck--;
				//}
				//else
				//{
					tmpCheck++;
				//}
			}
			
	<?
		}
	}
	?>
	
	if((<?=$targetType;?> == 1) && (tmpCheck == <?=sizeof($arr_bulk_result);?>))
	{
		obj.action = "stock_take2.php";
		obj.submit();
	}
	
}

function jHIDE_DIV(InputTable)
{
	jChangeContent( "ToolMenu2","");
}


-->
</script>

<br>

<form name="form1" method="POST" onSubmit="checkForm();">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td><?=$infobar; ?></td>
</tr>
</table>

<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$tag_code_table;?>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content;?>
</table>
<!--
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" >
<tr><td><span id="ToolMenu2"></span></td></tr>
</table>
-->
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$bulk_table;?>
</table>




<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?=$image_path/$LAYOUT_SKIN ?>/10x10.gif" width="10" height="1" /></td></tr>
<tr><td height="10px"></td></tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_next, "button", "checkForm()") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?targetLocation=$location'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	</td>
</tr>
</table>

<input type="hidden" name="item_list" value="<?=$item_list?>">
<input type="hidden" name="location" value="<?=$targetLocation?>">
<input type="hidden" name="group_list" value="<?=$group_list?>">
<input type="hidden" name="targetType" value=<?=$targetType?>>

</form>

<script language="javascript">
document.getElementById("item_tag_code").focus();
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>