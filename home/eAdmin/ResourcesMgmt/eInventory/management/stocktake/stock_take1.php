<?php
// using: 

// ##################################################
// Date: 2019-05-13 Henry
// Security fix: SQL without quote
// 
// Date: 2019-04-30 Henry
// security issue fix
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2018-02-13 Henry
// added column item description [Case#T135772]
//
// Date: 2016-02-02 Henry
// PHP 5.4 fix: move $targetLocation after include file
//
// Date: 2014-11-20 Henry
// Change the order　by item code instead [case# D71836]
//
// Date: 2013-07-10 Carlos
// Change Single item filter default to [Show items that have not undergone stocktake only]
//
// Date: 2013-02-26 Carlos
// Display bulk item barcode
//
// Date: 2012-10-24 YatWoon
// Enhanced: [Single] eBooking integration (display "Borrowed")
//
// Date: 2012-05-29 YatWoon
// Improved: [Bulk] don't display "request write-off" and "Total".
// Improved: [Bulk] display group and funding name
//
// Date: 2011-06-30 YatWoon
// Fixed: cannot display "Last stocktake by" and "Last stocktake on" data if stocktake "today"
//
// Date: 2011-04-27 YatWoon
// Improved: add management group filter, prevent hang for huge amount items
//
// Date: 2011-04-12 YatWoon
// Fixed: display the default "Normal" value according to the LocationID
//
// Date: 2011-03-31 YatWoon
// Display empty instead of "NaN" in stocktake quantity's total
//
// ##################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

if ($targetLocation == "") {
    header("location: index.php");
    exit();
}

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_Stocktake";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$targetLocation = IntegerSafe($targetLocation);
$admin_group_id = IntegerSafe($admin_group_id);

// ## Get The Category Name ###
/*
 * $sql = "
 * SELECT
 * ".$linventory->getInventoryNameByLang()."
 * FROM
 * INVENTORY_LOCATION
 * WHERE
 * LocationID = $targetLocation
 * ";
 * $result = $linventory->returnArray($sql,1);
 */
$liblocation = new Room($targetLocation);
$LocationRoomNow = $liblocation->BuildingName . " > " . $liblocation->FloorName . " > " . $liblocation->RoomName;
$StocktakePeriodStart = $linventory->getStocktakePeriodStart() . " 00:00:00";
$StocktakePeriodEnd = $linventory->getStocktakePeriodEnd() . " 23:59:59";

// $infobar .= $linterface->GET_NAVIGATION2($result[0][0]." - ".$i_InventorySystem_Stocktake_List);
$infobar .= $linterface->GET_NAVIGATION2($LocationRoomNow . " - " . $i_InventorySystem_Stocktake_List);
// ## End Of Getting The Category Name ###

$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/index.php",
    1
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/offline.php",
    0
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$STEPS_OBJ[] = array(
    $i_InventorySystem_StockCheck_Step1,
    0
);
$STEPS_OBJ[] = array(
    $i_InventorySystem_StockCheck_Step2,
    1
);
$STEPS_OBJ[] = array(
    $i_InventorySystem_StockCheck_Step3,
    0
);

if ($linventory->IS_ADMIN_USER($UserID))
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
else
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
$arr_result = $linventory->returnVector($sql);

if (sizeof($arr_result) > 0) {
    $targetGroupAll = implode(",", $arr_result);
}
// end #

if (empty($admin_group_id)) {
    $targetGroup = $targetGroupAll;
} else {
    $targetGroup = $admin_group_id;
}

if (! isset($targetItemStocktakeType)) {
    $targetItemStocktakeType = 1;
}

$sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP where AdminGroupID in (" . $targetGroupAll . ") ORDER BY DisplayOrder";
$admin_group = $linventory->returnArray($sql);
$admin_group_selection = getSelectByArray($admin_group, "name=\"admin_group_id\" onChange=\"document.form1.submit();\"", $admin_group_id, 1, 0);

// if($targetType == 1)
// {
// Single Item Start #
$price_ceiling_cond = "";
if ($sys_custom['eInventory_PriceCeiling']) {
    $price_ceiling_cond = " INNER JOIN INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID AND (b.UnitPrice>=d.PriceCeiling)) ";
}
if ($targetItemStocktakeType == 1) {
    // $sql = "SELECT CONCAT(\"'\",a.DateInput,\"'\") FROM INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) WHERE b.LocationID = $targetLocation AND a.Action IN (2,3,4)";
    $sql = "SELECT a.ItemID, MAX(a.DateInput) FROM INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) WHERE b.LocationID = '".$targetLocation."' AND a.Action IN (2,3,4) GROUP BY a.ItemID";
    $arrResult = $linventory->returnArray($sql, 2);
    
    if (sizeof($arrResult) > 0) {
        for ($i = 0; $i < sizeof($arrResult); $i ++) {
            list ($item_id, $last_stocktake_time) = $arrResult[$i];
            $sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE Action = 2 AND (DateInput BETWEEN '" . $StocktakePeriodStart . "' AND '" . $StocktakePeriodEnd . "') AND DateInput = '" . $last_stocktake_time . "' AND ItemID = $item_id";
            $arrTempResult = $linventory->returnVector($sql);
            
            if (sizeof($arrTempResult) > 0) {
                $arrTargetItem[] = $item_id;
            }
        }
    }
    $cond = "";
    if (sizeof($arrTargetItem) > 0) {
        $targetItem = implode(",", $arrTargetItem);
        $cond = " AND a.ItemID NOT IN (" . $targetItem . ")";
    }
}

$sql = "SELECT
					DISTINCT a.ItemID,
					a.ItemCode,
					" . $linventory->getInventoryItemNameByLang("a.") . " as item_name,
					b.TagCode, b.PurchasedPrice, b.UnitPrice,
					a.StockTakeOption,
					" . $linventory->getInventoryDescriptionNameByLang("a.") . " as item_description
			FROM 
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) 
					$price_ceiling_cond
			WHERE
					a.ItemType = $targetType AND
					a.RecordStatus = 1 AND
					b.LocationID IN ($targetLocation) AND
					b.GroupInCharge IN ($targetGroup)
					$cond
			ORDER BY
					a.ItemCode";
$arr_result = $linventory->returnArray($sql);

// sort list the items needed to be stock-taken
$arr_result = $linventory->FilterForStockTakeSingle($arr_result);

// for eBooking integration
if ($plugin['eBooking']) {
    include_once ($PATH_WRT_ROOT . "includes/libebooking.php");
    $lebooking = new libebooking();
    // build array for check item's checkin status
    if (! empty($arr_result)) {
        $arr_result_item_ary = array();
        foreach ($arr_result as $k => $d) {
            $arr_result_item_ary[] = $d['ItemID'];
        }
    }
    $checkInStatusAry = $lebooking->returnIsItemCheckedIn($arr_result_item_ary);
}

$ArrItemStockTaked[] = array(
    0,
    $Lang['eInventory']['FieldTitle']['StocktakeType']['AllItem']
);
$ArrItemStockTaked[] = array(
    1,
    $Lang['eInventory']['FieldTitle']['StocktakeType']['NotDone']
);

$ItemStocktakeSelection = getSelectByArray($ArrItemStockTaked, "name='targetItemStocktakeType' id='targetItemStocktakeType' onChange='this.form.submit();'", $targetItemStocktakeType, 1, 1);

$table_content .= "<tr><td colspan=\"8\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
$table_content .= "<tr class=\"tablename\">";
$table_content .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_ItemType_Single &nbsp; $ItemStocktakeSelection</td>";
$table_content .= "<td class=\"tabletext\" colspan=\"6\" align=\"right\">{$i_InventorySystem_Stocktake_ReadBarcode} &nbsp; <input type=\"text\" id=\"item_tag_code\" name=\"item_tag_code\" class=\"textboxnum\" onKeyPress=\"return disableEnterKey(event)\" ></td>";
$table_content .= "</tr>\n";
$table_content .= "</table></td></tr>\n";
$table_content .= "<tr class=\"tabletop\" valign=\"bottom\">";
$table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Item_Code . "</td>";
$table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Item_Barcode . "</td>";
$table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Item_Name . "</td>";
$table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Item_Description . "</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Status</td>";
$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_StockCheck_Found <input id=ItemAllFound type=checkbox onClick=(this.checked)?setAllFound(1):setAllFound(0) </td>";
$table_content .= "</tr>\n";

if (sizeof($arr_result) > 0) {
    $js_arr .= "arrItemBarcordToID = new Array(" . sizeof($arr_item_tag) . ");\n";
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($item_id, $item_code, $item_name, $item_tag, $item_purchased_price, $item_unit_price, $stocktake_option, $item_description) = $arr_result[$i];
        // get latest stock take time #
        $namefield = getNameFieldByLang2("b.");
        $sql = "SELECT 
							IF(a.DateInput != '', a.DateInput, ' - '), $namefield , 
							IF(a.Action = 2, 1, 0)
					FROM 
							INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
					WHERE
							a.ACTION IN (2,3,4) AND a.ItemID = $item_id AND 
							a.DateInput BETWEEN '" . $StocktakePeriodStart . "' AND '" . $StocktakePeriodEnd . "'
					ORDER BY 
							a.DateInput DESC
					LIMIT 0,1";
        $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
        $ItemFoundLastTime = "";
        if (sizeof($arr_stock_take_time_person) > 0) {
            list ($single_item_latest_stock_take_time, $person_in_charge, $ItemFoundLastTime) = $arr_stock_take_time_person[0];
            if ($single_item_latest_stock_take_time == " ")
                $single_item_latest_stock_take_time = " - ";
            if ($person_in_charge == " ")
                $person_in_charge = " - ";
        } else {
            $single_item_latest_stock_take_time = " - ";
            $person_in_charge = " - ";
        }
        $sql = "SELECT NewStatus FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE ItemID = $item_id AND (NewStatus IS NOT NULL AND NewStatus != '' AND NewStatus != '0') ORDER BY DateInput DESC LIMIT 0,1";
        $arr_item_status = $linventory->returnVector($sql);
        if ($arr_item_status[0] == ITEM_STATUS_NORMAL)
            $item_curr_status = $i_InventorySystem_ItemStatus_Normal;
        if ($arr_item_status[0] == ITEM_STATUS_DAMAGED)
            $item_curr_status = $i_InventorySystem_ItemStatus_Damaged;
        if ($arr_item_status[0] == ITEM_STATUS_REPAIR)
            $item_curr_status = $i_InventorySystem_ItemStatus_Repair;
            
            // for eBooking integration
        if ($plugin['eBooking']) {
            $item_curr_status = $checkInStatusAry[$item_id] ? $Lang['eInventory']['ItemStatus']['Borrowed'] : $item_curr_status;
        }
        
        $j = $i + 1;
        if ($j % 2 == 0) {
            $table_row_css = " class=\"tablerow1\" ";
            $textboxcolor = " #FFFFFF ";
        } else {
            $table_row_css = " class=\"tablerow2\" ";
            $textboxcolor = " #F5F5F5 ";
        }
        
        $table_content .= "<tr bgcolor=\"#FFFFCC\">";
        $table_content .= "<td class=\"tabletext\">$item_code</td>";
        $table_content .= "<td class=\"tabletext\">$item_tag</td>";
        $table_content .= "<td class=\"tabletext\">$item_name</td>";
        $table_content .= "<td class=\"tabletext\">" . ($item_description ? $item_description : " - ") . "</td>";
        $table_content .= "<td class=\"tabletext\">$person_in_charge</td>";
        $table_content .= "<td class=\"tabletext\">$single_item_latest_stock_take_time</td>";
        $table_content .= "<td class=\"tabletext\">$item_curr_status</td>";
        $table_content .= "<td>";
        $table_content .= "<input type=\"hidden\" name=\"status_" . $item_id . "\"  id=\"status_" . $item_id . "\"  value=\"\" DISABLED >\n";
        $table_content .= "<span id=\"status_" . $item_id . "_div\"value=\"\" class=\"tabletext\" ></span>";
        if ($ItemFoundLastTime == 1) {
            $ItemFound = " checked ";
            $jsItemFound .= "setItemFound(1,'$item_id');\n";
        } else {
            $ItemFound = "";
        }
        $table_content .= "<input type=\"checkbox\" name=\"ItemTag_" . $item_id . "\" id=\"ItemTag_" . $item_id . "\" value=\"$item_tag\" onClick=\"(this.checked)?setItemFound(1,'$item_id'):setItemFound(0,'$item_id')\" $ItemFound>\n";
        $table_content .= "<input type=\"hidden\" name=\"hidden_status_$item_id\" id=\"hidden_status_$item_id\" value=\"\">\n";
        $table_content .= "<input type=\"hidden\" name=\"item_id_$item_tag\" id=\"item_id_$item_tag\" value=$item_id>\n";
        $table_content .= "</td>";
        $table_content .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=8></td></tr>";
        
        $arr_item_id[$i] = $item_id;
        $arr_item_tag[$i] = $item_tag;
        
        $js_arr .= "arrItemBarcordToID['" . $item_tag . "'] = $item_id ;\n";
    }
    $temp_id_list = implode(",", $arr_item_id);
    $temp_tag_list = implode(",", $arr_item_tag);
} else {
    $js_arr .= "arrItemBarcordToID = new Array(0);\n";
}

if (sizeof($arr_result) == 0) {
    $table_content .= "<tr class=\"tablerow2\"><td colspan=\"8\" class=\"tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}
$table_content .= "<tr class=\"tabletop\" height=\"20px\">
						<td colspan=\"8\" align=\"right\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_TotalQty: $i_InventorySystem_Stocktake_FoundQty <input type=\"text\" name=\"single_found_total\" id=\"single_found_total\" class=\"stocktakenumbox2\" style=\"border: 0px 0px 0px 0px; background-color:#A6A6A6;\" READONLY value=\"0\"> / $i_InventorySystem_Stocktake_UnlistedQty <input type=\"text\" name=\"single_unlisted_total\" class=\"stocktakenumbox2\" style=\"border: 0px 0px 0px 0px; background-color:#A6A6A6;\" READONLY value=\"0\"> </td></tr>";
$table_content .= "<tr><td class=\"dotline\" colspan=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" />";
$table_content .= "<input type=\"hidden\" name=\"item_id_list\" id=\"item_id_list\" value=\"$temp_id_list\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_tag_list\" id=\"item_tag_list\" value=\"$temp_tag_list\">\n";
$table_content .= "<input type=\"hidden\" name=\"wrong_item_list\" id=\"wrong_item_list\" value=\"\">\n";
$table_content .= "<input type=\"hidden\" name=\"itemType\" id=\"itemType\" value=$targetType>\n";
$table_content .= "</td></tr>";
// End Of Signel Item #

// Bulk Item Start #
$success_bulk_item_cond = "";
$fail_bulk_item_cond = "";

if ($sys_custom['eInventory_PriceCeiling']) {
    $arr_targetSuccessBulkItem = array();
    $arr_targetFailBulkItem = array();
    $success_bulk_item_cond = "";
    $fail_bulk_item_cond = "";
    $sql = "SELECT 
						a.ItemID, a.CategoryID,
						" . $linventory->getInventoryItemNameByLang("a.") . " as item_name
				FROM 
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
						INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
				WHERE
						a.ItemType = 2 AND
						a.RecordStatus = 1 AND
						b.LocationID IN ($targetLocation) AND
						b.GroupInCharge IN ($targetGroup)
				ORDER BY
						a.ItemCode";
    $arr_tmp_result = $linventory->returnArray($sql, 2);
    
    if (sizeof($arr_tmp_result) > 0) {
        for ($i = 0; $i < sizeof($arr_tmp_result); $i ++) {
            list ($tmp_item_id, $tmp_cat_id) = $arr_tmp_result[$i];
            $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$tmp_item_id."' AND Action = " . ITEM_ACTION_PURCHASE . "";
            $total_cost = $linventory->returnVector($sql);
            
            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$tmp_item_id."'";
            $total_qty = $linventory->returnVector($sql);
            
            $unit_price = $total_cost[0] / $total_qty[0];
            
            $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = '".$tmp_cat_id."'";
            $tmp_result2 = $linventory->returnArray($sql, 2);
            if (sizeof($tmp_result2) > 0) {
                for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                    list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                    if ($apply_to_bulk == 1) {
                        if ($unit_price >= $price_ceiling) {
                            $arr_targetSuccessBulkItem[] = $tmp_item_id;
                        } else {
                            $arr_targetFailBulkItem[] = $tmp_item_id;
                        }
                    } else {
                        $arr_targetSuccessBulkItem[] = $tmp_item_id;
                    }
                }
            }
        }
    }
    if (sizeof($arr_targetSuccessBulkItem) > 0) {
        $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
        $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
    }
    if (sizeof($arr_targetFailBulkItem) > 0) {
        $targetFailList = implode(",", $arr_targetFailBulkItem);
        $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
    }
}

$sql = "SELECT
					a.ItemID,
					a.ItemCode,
					" . $linventory->getInventoryItemNameByLang("a.") . " as item_name,
                    " . $linventory->getInventoryDescriptionNameByLang("a.") . " as item_description,
					b.Quantity,
					b.GroupInCharge,
					" . $linventory->getInventoryItemNameByLang("g.") . ",
					" . $linventory->getInventoryItemNameByLang("funding.") . ",
					b.FundingSourceID,
					a.StockTakeOption,
					c.Barcode 
			FROM
					INVENTORY_ITEM AS a 
					INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) 
					INNER JOIN INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
					left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
					left join INVENTORY_ADMIN_GROUP AS g ON (g.AdminGroupID=b.GroupInCharge)
			WHERE
					a.ItemType = 2 AND
					a.RecordStatus = 1 AND
					b.LocationID IN ($targetLocation) AND
					b.GroupInCharge IN ($targetGroup)
					$success_bulk_item_cond
					$fail_bulk_item_cond
			ORDER BY
					a.ItemCode";
$arr_bulk_result = $linventory->returnArray($sql);
$arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);

$bulk_table .= "<tr><td colspan=\"11\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
$bulk_table .= "<tr class=\"tablename\">";
$bulk_table .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_ItemType_Bulk</td>";
$bulk_table .= "</tr>\n";
$bulk_table .= "</table></td></tr>\n";
/*
 * $bulk_table .= "<tr class=\"tabletop\">
 * <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>
 * <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>
 * <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>
 * <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>
 * <td rowspan=\"2\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>
 * <td colspan=\"4\" align=\"center\" class=\"whiteborder tabletopnolink\"><DIV align=\"center\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_StocktakeQty</DIV></td>
 * </tr>\n
 * <tr class=\"tablerow2\">
 * <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_NormalQty<br>($i_InventorySystem_Stocktake_NotIncludeWriteOffQty)</td>
 * <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_RequestWriteOffQty</td>
 * <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_TotalQty</td>
 * <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_VarianceQty</td>
 * </tr>\n";
 */
$bulk_table .= "<tr class=\"tabletop\">
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>
						<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Item_Description</td>
	                    <td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>
	                    <td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_StocktakeQty</td>
	                    <td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>
	                  </tr>\n
	                  ";

$bulk_table .= "</tr>\n";

if (sizeof($arr_bulk_result) > 0) {
    for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
        // list($bulk_item_id, $bulk_item_code, $bulk_item_name, $bulk_item_qty, $bulk_item_group_id) = $arr_bulk_result[$i];
        list ($bulk_item_id, $bulk_item_code, $bulk_item_name, $bulk_item_description, $bulk_item_qty, $bulk_item_group_id, $group_name, $funding_name, $funding_id, $stocktake_option, $bulk_item_barcode) = $arr_bulk_result[$i];
        
        // get the latest stock take time #
        $namefield = getNameFieldByLang2("b.");
        $sql = "SELECT 
							IF(a.DateInput != '', a.DateInput, CONCAT(' - ')), $namefield, a.StockCheckQty
					FROM 
							INVENTORY_ITEM_BULK_LOG AS a 
							INNER JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
					WHERE 
							a.ACTION = 2 AND a.ItemID = $bulk_item_id 
							and a.LocationID = $targetLocation
							and a.GroupInCharge=$bulk_item_group_id and a.FundingSource=$funding_id
					ORDER BY 
							a.DateInput DESC
					LIMIT 0,1";
        $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
        if (sizeof($arr_stock_take_time_person) > 0) {
            list ($bulk_item_latest_stock_take_time, $person_in_charge, $LastStocktakeQty) = $arr_stock_take_time_person[0];
        } else {
            $bulk_item_latest_stock_take_time = " - ";
            $person_in_charge = " - ";
            $LastStocktakeQty = $bulk_item_qty;
        }
        // end #
        
        /*
         * ### Get the latest write off request qty
         * //$sql = "SELECT WriteOffQty from INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $bulk_item_id AND RecordStatus = 0 ORDER BY DateInput desc limit 0,1";
         * $sql = "SELECT WriteOffQty from INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $bulk_item_id AND LocationID = $targetLocation and RecordStatus = 0 ORDER BY DateInput desc limit 0,1";
         *
         * $result = $linventory->returnVector($sql);
         * if(sizeof($result)>0){
         * $LastTimeRequestWriteOffQty = $result[0];
         * $sql = "SELECT SUM(MissingQty) FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = $bulk_item_id AND LocationID = $targetLocation";
         * $MissingQty = $linventory->returnVector($sql);
         *
         * $sql = "SELECT SUM(SurplusQty) FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = $bulk_item_id AND LocationID = $targetLocation";
         * $SurplusQty = $linventory->returnVector($sql);
         *
         * if(sizeof($MissingQty)>0)
         * $LastStocktakeQty = $bulk_item_qty - $MissingQty[0] - $LastTimeRequestWriteOffQty;
         * else if(sizeof($SurplusQty)>0)
         * $LastStocktakeQty = $bulk_item_qty + $SurplusQty[0] - $LastTimeRequestWriteOffQty;
         * }else{
         * $LastTimeRequestWriteOffQty = 0;
         * $LastStocktakeQty = $LastStocktakeQty;
         * }
         */
        $jsUpdateBulkItemQty .= "bulkStockQtyCal($bulk_item_qty, $bulk_item_id, $bulk_item_group_id, $funding_id);\n";
        
        // $j=$i+1;
        // if($j%2 == 0)
        // $table_row_css = " class = \"tablerow1\" ";
        // else
        // $table_row_css = " class = \"tablerow2\" ";
        
        $bulk_table .= "<tr bgcolor=\"#E5F2FA\"><td class=\"tabletext\">$bulk_item_code</td>";
        $bulk_table .= "<td class=\"tabletext\">$bulk_item_barcode</td>";
        $bulk_table .= "<td class=\"tabletext\">$bulk_item_name</td>";
        $bulk_table .= "<td class=\"tabletext\">" . ($bulk_item_description ? $bulk_item_description : " - ") . "</td>";
        $bulk_table .= "<td class=\"tabletext\">$group_name</td>";
        $bulk_table .= "<td class=\"tabletext\">$funding_name</td>";
        $bulk_table .= "<td class=\"tabletext\">$person_in_charge</td>";
        $bulk_table .= "<td class=\"tabletext\">$bulk_item_latest_stock_take_time</td>";
        $bulk_table .= "<td class=\"tabletext\">$bulk_item_qty</td>";
        
        $bulk_table .= "<td width=\"10%\"><input type=\"text\" name=\"bulk_item_qty_found_" . $bulk_item_id . "_" . $bulk_item_group_id . "_" . $funding_id . "\" class=\"stocktakenumbox\" value=\"$LastStocktakeQty\" onChange=\"bulkStockQtyCal($bulk_item_qty, $bulk_item_id,$bulk_item_group_id, $funding_id);\"></td>";
        // $bulk_table .= "<td width=\"5%\"><input type=\"text\" name=\"bulk_item_qty_write_off_".$bulk_item_id."_".$bulk_item_group_id."\" class=\"stocktakenumbox\" value=\"$LastTimeRequestWriteOffQty\" onChange=\"bulkStockQtyCal($bulk_item_qty, $bulk_item_id,$bulk_item_group_id);\"></td>";
        // $bulk_table .= "<td width=\"5%\"><input type=\"text\" name=\"bulk_item_total_".$bulk_item_id."_".$bulk_item_group_id."\" class=\"stocktakenumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"$bulk_item_qty\"></td>";
        $bulk_table .= "<td width=\"5%\"><input type=\"text\" name=\"bulk_item_variance_" . $bulk_item_id . "_" . $bulk_item_group_id . "_" . $funding_id . "\" class=\"stocktakenumbox\" style=\"border: 0px 0px 0px 0px; background-color:#E5F2FA;\" READONLY value=\"0\"></td>";
        $bulk_table .= "</tr>\n";
        $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_group_id_$bulk_item_id\" id=\"bulk_item_group_id_$bulk_item_id\" value=$bulk_item_group_id>";
        $bulk_table .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=11></td></tr>\n";
        
        if ($bulk_item_list != "")
            $bulk_item_list = $bulk_item_list . "," . $bulk_item_id;
        else
            $bulk_item_list = $bulk_item_list . $bulk_item_id;
    }
    $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_id\" id=\"bulk_item_id\" value=\"$bulk_item_list\">\n";
}
if (sizeof($arr_bulk_result) == 0) {
    $bulk_table .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"11\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
}
// End Of Bulk Item #
// }
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
#ToolMenu {
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 4;
	visibility: show;
}
;
</style>
<script language="JavaScript">
isMenu = true;
total_single_found = 0;
total_single_unlisted = 0;
</script>

<script language="javascript">
<? echo $js_arr;?>
<!--
function bulkStockQtyCal(original_qty, item_id, group_id, funding_id)
{
	var tmp_bulk_item_qty_found = eval("document.form1.bulk_item_qty_found_"+item_id+"_"+group_id+"_"+funding_id+".value");
// 	var tmp_bulk_item_qty_write_off = eval("document.form1.bulk_item_qty_write_off_"+item_id+"_"+group_id+".value");
// 	var tmp_bulk_item_total2 = eval("document.form1.bulk_item_total_"+item_id+"_"+group_id);
// 	var tmp_bulk_item_total = tmp_bulk_item_total2;
	var tmp_bulk_item_variance = eval("document.form1.bulk_item_variance_"+item_id+"_"+group_id+"_"+funding_id);
	
// 	var tmp_ans = parseInt(tmp_bulk_item_qty_found) + parseInt(tmp_bulk_item_qty_write_off);
	var tmp_ans = parseInt(tmp_bulk_item_qty_found);
	var tmp_variance = tmp_ans - parseInt(original_qty);
	
// 	tmp_bulk_item_total.value = tmp_ans;
	if(tmp_variance<0)
	{
		tmp_bulk_item_variance.style.color = "red";
		tmp_bulk_item_variance.value = tmp_variance;
	}
	if(tmp_variance>0)
	{
		tmp_bulk_item_variance.style.color = "green";
		tmp_bulk_item_variance.value = '+'+tmp_variance;
	}
	if(tmp_variance==0)
	{
		tmp_bulk_item_variance.style.color = "black";
		tmp_bulk_item_variance.value = tmp_variance;
	}	
}
function setAllFound(val)
{
	if(val == 1)
	{
		total_single_found = 0;
		total_single_unlisted = 0;	
		
		var tmp_tag_list2 = document.getElementById("item_tag_list");
	    var tmp_tag_code2 = tmp_tag_list2.value.split(",");
	    for(i=0; i<tmp_tag_code2.length; i++)
	    {
		    var tmp_item_code = tmp_tag_code2[i];
		    var tmp_item_id = eval("arrItemBarcordToID['"+tmp_item_code+"']");
		    var tmp_str = eval("document.form1.ItemTag_"+tmp_item_id);
		 
		    var pass_val = tmp_item_id ;
		    
		    tmp_str.checked = val;
		    setItemFound(val,pass_val);
		}
	}
	
	if(val == 0)
	{
		total_single_found = 0;
		var tmp_tag_list2 = document.getElementById("item_tag_list");
	    var tmp_tag_code2 = tmp_tag_list2.value.split(",");    
		var tmp_obj = eval("document.form1.single_found_total");
		tmp_obj.value = total_single_found;
		
		for(i=0; i<tmp_tag_code2.length; i++)
	    {   
		    var tmp_item_code = tmp_tag_code2[i];
		    var tmp_item_id = eval("arrItemBarcordToID['"+tmp_item_code+"']");
		    var tmp_str = eval("document.form1.ItemTag_"+tmp_item_id);
		    var tmp_obj2 = document.getElementById("status_"+tmp_item_id+"_div");  	
			var tmp_obj3 = eval("document.form1.hidden_status_"+tmp_item_id);
		    
		    var pass_val = tmp_tag_code2[i];
		    tmp_str.checked = val;
		    tmp_obj2.innerHTML = "";
    		tmp_obj3.value = 0;
		}
	}
}
function setItemFound(val,item_tag_code)
{
	var tmp_obj = document.getElementById("status_"+item_tag_code+"_div");
	var tmp_obj2 = eval("document.form1.hidden_status_"+item_tag_code);
	var tmp_obj3 = eval("document.form1.ItemTag_"+item_tag_code);
	var tmp_obj4 = eval("document.form1.single_found_total");
	if(val == 1)
	{
		//tmp_obj.value = "<?=$i_InventorySystem_Found?>";
		tmp_obj.innerHTML = "<font color=\"#006600\"><?=$i_InventorySystem_Found?></font>";
		tmp_obj2.value = 1;
		tmp_obj3.checked = true;
		total_single_found++;
		tmp_obj4.value = total_single_found;
	}
	if(val == 0)
	{
		//var tmp_obj5 = eval("document.form1.single_found_total");
		document.getElementById('ItemAllFound').checked = false;
		total_single_found--;
    	tmp_obj.innerHTML = "";
    	tmp_obj2.value = 0;
		tmp_obj3.checked = false;
    	tmp_obj4.value = total_single_found;
	}
}
-->
</script>

<div id="ToolMenu"
	style="position: absolute; width =0px; height =0px; visibility: hidden"></div>

<script language="javascript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveItemNotFound()
{
        obj = document.form1;
        var myElement = document.getElementById("ToolMenu2");

        myElement.innerHTML = "<table border=1 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$i_InventorySystem_Loading?></td></tr>\n</table>";
        
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var miss_item_list = obj.wrong_item_list.value;
        var path = "getItemNotFound.php?MissItemList=" + miss_item_list;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function disableEnterKey(e)
{
	var key;      
	if(window.event)
	{
		key = window.event.keyCode; //IE
	}
	else
	{
		key = e.which; //firefox
	}
	
	if(key == 13)
	{
		var tag_code = document.form1.item_tag_code.value;
		
		// check if the Barcode is start & end with '*'
		if((tag_code.indexOf("*") == 0) && (tag_code.lastIndexOf("*") == (tag_code.length-1)))
		{
			tag_code = tag_code.substring(1,tag_code.length-1);
		}
		
    	tag_code = String(tag_code).toUpperCase();
    	var tmp_item_id = eval("arrItemBarcordToID['"+tag_code+"']");
    	var tmp_id_list = document.getElementById("item_id_list")
    	var tmp_tag_list = document.getElementById("item_tag_list");
    	var tmp_check = 0;
    	var tmp_result = '';
    	
    	var tmp_tag_code = tmp_tag_list.value.split(",");
    	for (i=0; i<tmp_tag_code.length; i++)
    	{
	    	var TmpStr1  = new String(tag_code).toUpperCase();	
	    	var TmpStr2  = new String(tmp_tag_code[i]).toUpperCase();

	    	if (TmpStr1 == TmpStr2)
	    	{
		    	tmp_check = 1;		    	
		    	tag_code = tmp_tag_code[i];
	    	}	 
	    	else if (tag_code == tmp_tag_code[i])
	    	{
	    		tmp_check = 1;
    		}
    		else
    		{

    		}
    	}
    	
    	if(tmp_check == 1)
    	{
	    	var tmp_obj = document.getElementById("status_"+tmp_item_id+"_div");	    	
	    	var tmp_obj2 = document.getElementById('hidden_status_'+tmp_item_id);
	    	var tmp_obj3 = document.getElementById('ItemTag_'+tmp_item_id);
	    	var tmp_obj4 = document.getElementById('single_found_total');
	    
	    	tmp_obj.innerHTML = "<font color=\"#006600\"><?=$i_InventorySystem_Found?></font>";
	    	tmp_obj2.value = 1;
	    	total_single_found++;
	    	tmp_obj4.value = total_single_found;
	    	tmp_obj3.checked = true;
    	}
    	if(tmp_check == 0)
    	{   	
	    	var tmp_obj5 = eval("document.form1.single_unlisted_total");
	    	total_single_unlisted++;
	    	tmp_obj5.value = total_single_unlisted;
	    
	    	if(document.form1.wrong_item_list.value == "")
	    	{
		    	document.form1.wrong_item_list.value = tag_code;
	    	}
	    	else
	    	{
		    	document.form1.wrong_item_list.value = document.form1.wrong_item_list.value + "," + tag_code;
	    	}
    	}    	
    	document.form1.item_tag_code.value="";
    	document.form1.item_tag_code.focus();
	}
    
    return (key != 13);
}

function play(file) {
    var embed = document.createElement("embed");
    
    embed.setAttribute('src', file);
    embed.setAttribute('hidden', true);
    embed.setAttribute('autostart', true);
 
    document.body.appendChild(embed);
}

function checkForm()
{
	var obj = document.form1;
	var tmpCheck = 0;
	<?
if (sizeof($arr_bulk_result) > 0) {
    for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
        list ($bulk_item_id, $bulk_item_code, $bulk_item_name, $bulk_item_description, $bulk_item_qty, $bulk_item_group_id, $group_name, $funding_name, $funding_id) = $arr_bulk_result[$i];
        ?>
// 			var item_assigned_qty = <?=$bulk_item_qty;?>;
// 			var targetFound = "bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>;
// 			var targetWriteOff = "bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>;
			
			tmp_1 = eval("document.form1.bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+"_"+<?=$funding_id?>+".value");
// 			tmp_2 = eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".value");
			
// 			if((tmp_1 != "") && (tmp_2 != ""))
			if(tmp_1 != "") 
			{
// 				if((tmp_1 >= 0) && (tmp_2 >= 0))
				if(tmp_1 >= 0)
				{
// 					ans = parseInt(tmp_1) + parseInt(tmp_2);
					ans = parseInt(tmp_1);
					tmpChecktextbox = 1;
				}
				else
				{
// 					alert("Please input a valid quantity");
					alert("<?=$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidQty']?>");
// 					eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
					eval("document.form1.bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+"_"+<?=$funding_id?>+".focus()");
					tmpChecktextbox = 0;
				}
			}
			else
			{
// 				alert("Please input a quantity");
				alert("<?=$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidQty']?>");
				tmpChecktextbox = 0;
// 				if(tmp_1 == "")
					eval("document.form1.bulk_item_qty_found_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
// 				if(tmp_2 == "")
// 					eval("document.form1.bulk_item_qty_write_off_"+<?=$bulk_item_id?>+"_"+<?=$bulk_item_group_id?>+".focus()");
			}
			
			if(tmpChecktextbox == 1)
			{
				tmpCheck++;
			}
			
	<?
    }
}
?>
	
	if((<?=$targetType;?> == 1) && (tmpCheck == <?=sizeof($arr_bulk_result);?>))
	{
		obj.action = "stock_take2.php";
		obj.submit();
	}
	
}

function jHIDE_DIV(InputTable)
{
	jChangeContent( "ToolMenu2","");
}


-->
</script>

<br>

<form name="form1" method="POST">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
	</table>
	<br>
	<table width="90%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td><?=$infobar; ?></td>
		</tr>
		<tr class="tablename">
			<td class="tablename2"><?=$i_InventorySystem['Caretaker']; ?> &nbsp; <?=$admin_group_selection?></td>
		</tr>
	</table>

	<br>

	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content;?>
</table>

	<br>

	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$bulk_table;?>
</table>

	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?=$image_path/$LAYOUT_SKIN ?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_next, "button", "checkForm()")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>

	<input type="hidden" name="item_list" value="<?=$item_list?>"> <input
		type="hidden" name="location" value="<?=$targetLocation?>"> <input
		type="hidden" name="group_list" value="<?=cleanHtmlJavascript($group_list)?>"> <input
		type="hidden" name="targetType" value=<?=$targetType?>> <input
		type="hidden" name="targetLocation" value=<?=$targetLocation?>>
</form>

<script language="javascript">
document.getElementById("item_tag_code").focus();
<?=$jsItemFound;?>
<?=$jsUpdateBulkItemQty;?>
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>