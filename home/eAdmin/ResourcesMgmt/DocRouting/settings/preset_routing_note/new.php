<?php
// using : 

/******************************
 * Change Log:
 * 
 ******************************/
 # Get PresetID & targetUser
$PresetID = $_POST['PresetID'][0];
$PresetType = $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'];

# Page Title
if($indexVar['drUserIsAdmin']){	
	if(isset($_POST['targetUser'])){
		$targetUser =  $_POST['targetUser'];
	}else{	
		$targetUser =  $indexVar['paramAry']['targetUser'];
	}
	
	include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");
	
	$libDocRouting_preset = new libDocRouting_preset();
	$accessRightCheckingArr = $libDocRouting_preset->getAccessRightTagControl($PresetType, $targetUser);
	
	$TAGS_OBJ = $accessRightCheckingArr[2];
	$goBackLink = $accessRightCheckingArr[3];
	$submitUpdateLink = $accessRightCheckingArr[4];
		
}
else{
	$TAGS_OBJ[] = array($Lang['DocRouting']['PresetRoutingNote']);
	
	# Add New Routing Link
	$submitUpdateLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'new_update');
	$goBackLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'index');	
		
}


//
//# Page Title
//$TAGS_OBJ[] = array($Lang['DocRouting']['PresetRoutingNote']);

# Layout Start
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Settings_PresetRoutingNote');


# Navigation
//$PAGE_NAVIGATION[] = array($Lang['DocRouting']['PresetRoutingNote'], "javascript:js_Go_Back();");
if($PresetID==''){
	$PAGE_NAVIGATION[] = array($Lang['DocRouting']['NewDocRouting'], "");
}else{
	$PAGE_NAVIGATION[] = array($Lang['DocRouting']['EditDocRouting'], "");
}

# Display Item Form
$htmlAry['NewPresetDocDisplay'] = $indexVar['libDocRouting_ui']->getNewPresetItemDisplay($submitUpdateLink, $PAGE_NAVIGATION, $PresetType, $PresetID, $targetUser);
//$htmlAry['NewPresetDocDisplay'] = $indexVar['libDocRouting_ui']->getNewPresetItemDisplay($submitUpdateLink, $PAGE_NAVIGATION, $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'], $PresetID, $targetUser);
			
include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>