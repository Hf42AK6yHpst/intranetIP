<?php
// using : 

/******************************
 * Change Log:
 * 2017-07-11 Anna
 * - added AuthenticationSetting
 ******************************/
# Page Title
$TAGS_OBJ[] = array($Lang['General']['SystemProperties']);

# Layout Start
$returnMsgKey = $indexVar['paramAry']['returnMsgKey'];
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Settings_SystemProperties', $Lang['General']['ReturnMessage'][$returnMsgKey]);

$settingAssoAry = $indexVar['libDocRouting']->returnSettingAry();

$x = '';
### email notification
$x .= $indexVar['libDocRouting_ui']->GET_NAVIGATION2_IP25($Lang['DocRouting']['EmailNotification']);
$x .= '<table class="form_table_v30">'."\r\n";
	// new route received by users
	$display = ($settingAssoAry['emailNotification_newRouteReceived'] == $docRoutingConfig['settings']['emailNotification_newRouteReceived']['enable'])? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['DocRouting']['NewRouteReceived'].'</td>'."\r\n";
		$x .= '<td>'.$display.'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// new feedback in route
	$display = ($settingAssoAry['emailNotification_newFeedbackInRoute'] == $docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['enable'])? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['DocRouting']['NewFeedbackReceivedInOwnRouting'].'</td>'."\r\n";
		$x .= '<td>'.$display.'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";

$x .= $indexVar['libDocRouting_ui']->GET_NAVIGATION2_IP25($Lang['DocRouting']['Security']);
$x .= '<table class="form_table_v30">'."\r\n";
// new route received by users
	$display = ($settingAssoAry['AuthenticationSetting'] == $docRoutingConfig['settings']['AuthenticationSetting']['enable'])? $Lang['General']['Enabled'] : $Lang['General']['Disabled'];
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['DocRouting']['AuthenticationSetting'].'</td>'."\r\n";
		$x .= '<td>'.$display.'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";

$htmlAry['contentTable'] = $x;

$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/system_properties', 'edit');
$htmlAry['editButton'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'docRoutingObj.goToPage(\''.$pe.'\');');

include_once($indexVar['templateScript']);
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>