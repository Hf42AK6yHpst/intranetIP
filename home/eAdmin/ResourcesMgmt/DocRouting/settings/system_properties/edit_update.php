<?php
$successAry = array();

$indexVar['libDocRouting']->Start_Trans();
$successAry['emailNotification_newRouteReceived'] = $indexVar['libDocRouting']->updateSetting('emailNotification_newRouteReceived', $indexVar['paramAry']['emailNotification_newRouteReceived']);
$successAry['emailNotification_newFeedbackInRoute'] = $indexVar['libDocRouting']->updateSetting('emailNotification_newFeedbackInRoute', $indexVar['paramAry']['emailNotification_newFeedbackInRoute']);
$successAry['AuthenticationSetting'] = $indexVar['libDocRouting']->updateSetting('AuthenticationSetting', $indexVar['paramAry']['AuthenticationSetting']);

if (in_array(false, $successAry)) {
	$indexVar['libDocRouting']->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
else {
	$indexVar['libDocRouting']->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

$paramAry = array();
$paramAry['returnMsgKey'] = $returnMsgKey;
$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/system_properties', 'index', $paramAry);

header('Location: ?pe='.$pe);
?>