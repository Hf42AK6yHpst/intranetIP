<?php
// using :
/*
 * Change Log:
 *  2020-01-24 [Sam]: Added checking to avoid student access the document routing page [2020-0123-1450-48206]  
 *  2015-04-24 [Carlos][ip2.5.6.5.1] : Added flag $sys_custom['DocRouting']['RequirePasswordToSign'] to require password validation for signing.
 *  2014-11-11 [Carlos][ip2.5.5.12.1]: Added sign page token checking
 * 	2012-10-05 [Ivan]: created this file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
include_once($PATH_WRT_ROOT."lang/dr_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/DocRouting/docRoutingConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting.php");
include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting_ui.php");

$indexVar['libDocRouting'] = new libDocRouting();

//intranet_auth();
//intranet_opendb();
//$indexVar['libDocRouting']->checkAccessRight();

// Avoid student access
if($_SESSION['UserType'] != USERTYPE_STAFF){
    header("Location: /home");
}

if(isset($_REQUEST['tok']) && $_REQUEST['tok']!=''){
	$sign_record = $indexVar['libDocRouting']->getSignEmailDocumentByToken($_REQUEST['tok']);
	if(count($sign_record)>0 && 
		(!$sys_custom['DocRouting']['RequirePasswordToSign'] || 
			($sys_custom['DocRouting']['RequirePasswordToSign'] && isset($_SESSION['DR_TMP_USERID'])
		 	&& $_SESSION['DR_TMP_USERID'] == $sign_record[0]['UserID']) 
		 )
	){ // it is a valid token
		$indexVar['tok'] = $_REQUEST['tok'];
		if(isset($_SESSION['UserID'])){ // if it is already logined user
			$_SESSION['DR_TMP_USERID'] = $_SESSION['UserID'];
			$_SESSION['DR_TMP_USERTYPE'] = $_SESSION['UserType'];
			$_SESSION['UserID'] = $sign_record[0]['UserID'];
			$_SESSION['UserType'] = $sign_record[0]['UserType'];
		}else{
			$_SESSION['UserID'] = $sign_record[0]['UserID'];
			$_SESSION['UserType'] = $sign_record[0]['UserType'];
		}
	}
}

if(isset($_REQUEST['tok']) && $_REQUEST['tok']!='' && isset($indexVar['tok'])){
	// skip checking access right
}else{
	intranet_auth();
	intranet_opendb();
	$indexVar['libDocRouting']->checkAccessRight();
}

$indexVar['libDocRouting_ui'] = new libDocRouting_ui();

$indexVar['drUserId'] = $_SESSION['UserID'];
$indexVar['drUserIsAdmin'] = ($indexVar['libDocRouting']->returnIsAdminUser($indexVar['drUserId']))? true : false;


### handle all http post/get value by urldeocde, stripslashes, trim
array_walk_recursive($_REQUEST, 'handleFormPost', 'REQUEST');
array_walk_recursive($_POST, 'handleFormPost', 'POST');

$fromEncryptedParameter = false;
if ($_REQUEST['pe'] != '') {
	$p = getDecryptedText($_REQUEST['pe'], $docRoutingConfig['encryptKey']);
	$fromEncryptedParameter = true;
}


### Split the parameter value into file path, file name and variable string
$paramAry = explode($docRoutingConfig['urlPathParamSeparator'], $p);
$filePath = $paramAry[0];
$fileName = $paramAry[1];
$variableText = $paramAry[2];

### Build Parameter Array
$indexVar['paramAry'] = array();
$separator = $docRoutingConfig['urlVariableSeparator'];
$separator = str_replace('/', '\\/', $separator);
$variableTextAry = preg_split('['.$separator.']', $variableText, -1, PREG_SPLIT_NO_EMPTY);
$numOfVariable = count($variableTextAry);


// convert the parameter to normal behaviour parameter
$parameterTextAry = array();
for ($i=0; $i<$numOfVariable; $i++) {
	$_paramAry = explode($docRoutingConfig['urlVariableKeyValueSeparator'], $variableTextAry[$i], 2);
	$_paramKey = $_paramAry[0];
	$_paramValue = ($fromEncryptedParameter)? $_paramAry[1] : normalizeFormParameter($_paramAry[1]);
	
	$parameterTextAry[] = $_paramKey.'='.$_paramValue;
}
$parameterText = implode('&', $parameterTextAry);
// convert the parameter text to array
parse_str($parameterText, $indexVar['paramAry']);
$indexVar['paramAry'] = array_merge($indexVar['paramAry'], $_POST);

### Include corresponding php files
$indexVar['taskScript'] = '';
if ($filePath=='' && $fileName=='') {
	// go to module index page if not defined
	$indexVar['taskScript'] .= 'management/index.php';
}
else {
	$indexVar['taskScript'] .= $filePath;
	if ($fileName != '') {
		$filePathLastChar = substr($filePath, -1, 1);
		if ($filePathLastChar != '/') {
			$indexVar['taskScript'] .= '/';
		} 
		$indexVar['taskScript'] .= $fileName.'.php';
	}
}
if (file_exists($indexVar['taskScript'])){
	$indexVar['templateScript'] = 'templates/'.str_replace('.php', '.tmpl.php', $indexVar['taskScript']);
	include_once($indexVar['taskScript']);
}
else {
	No_Access_Right_Pop_Up();
}

$indexVar['libDocRouting']->unsetSignDocumentSession();

intranet_closedb();
?>
