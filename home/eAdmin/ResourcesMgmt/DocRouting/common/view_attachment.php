<?php
/*
 * @param string $LinkToType
 * @param int $LinkToID
 * @param int $FileID
 */
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$ldocrouting = $indexVar['libDocRouting'];
$libfs = $ldocrouting->getLibFileSystemInstance();

$attachment = $ldocrouting->getDRAttachment($indexVar['paramAry']['LinkToType'], $indexVar['paramAry']['LinkToID'], $indexVar['paramAry']['FileID']);

$indexVar['libDocRouting']->unsetSignDocumentSession();

if(count($attachment)>0){
	$filePath = str_replace($docRoutingConfig['dbFilePath'],"",$attachment[0]['FilePath']);
	$fileName = $attachment[0]['FileName'];
	$fullpath = $docRoutingConfig['filePath'].$filePath;
	//debug_r($fullpath);
	if(file_exists($fullpath)){
		$content = $libfs->file_read($fullpath);
		output2browser($content, $fileName);
	}
}else{
	echo "File does not exist.";
}
?>