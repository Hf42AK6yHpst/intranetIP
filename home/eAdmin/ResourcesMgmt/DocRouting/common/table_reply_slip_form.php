<?php
// using: 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$replySlipFileField = $_GET['ReplySlipFileField'];
$replySlipId = $_GET['replySlipId'];

$libimport = new libimporttext();
$libTableReplySlipMgr = new libTableReplySlipMgr();
$libTableReplySlipMgr->setReplySlipId($replySlipId);

$indexVar['libDocRouting_ui']->echoModuleLayoutStart('', '', $forPopup=true);

$navigationAry = array();
$navigationAry[] = array($Lang['DocRouting']['PreviewReplySlip'], "");
$htmlAry['navigation'] = $indexVar['libDocRouting_ui']->GET_NAVIGATION_IP25($navigationAry);

if ($replySlipId) {
	$libTableReplySlipMgr->setReplySlipId($replySlipId);
	$html['replySlip'] = $libTableReplySlipMgr->returnReplySlipHtml($forCsvPreview=false, $showUserAnswer=false, $disabledAnswer=true);
}
else {
	$libTableReplySlipMgr->setCsvFilePath($_FILES[$replySlipFileField]['tmp_name']);
	$libTableReplySlipMgr->setCurUserId($indexVar['drUserId']);
	$html['replySlip'] = $libTableReplySlipMgr->returnReplySlipHtmlByCsvData();
}

?>
<br />
<?=$htmlAry['navigation']?>
<br />
<?=$html['replySlip']?>
<?php
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>