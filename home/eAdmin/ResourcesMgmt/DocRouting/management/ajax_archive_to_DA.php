<?php
// editing by 
/******************************
 * Change Log:
 * 2014-05-13 (Carlos): add the fourth parameter to $ldocrouting->getDRAttachment() to get starred files
 * 2013-10-03 (Henry): add $LinkToType to handle the feedback file
 ******************************/
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];
$ldamu = new libdigitalarchive_moduleupload();
$indexVar['libdigitalarchive_moduleupload'] = $ldamu;

$DocumentID = $_REQUEST['DocumentID'];
$LinkToType = $_REQUEST['LinkToType'];

if(!$ldamu->User_Can_Archive_File()){
	exit;
}

$libfs = $ldocrouting->getLibFileSystemInstance();

$drUserID = $indexVar['drUserId'];
$tmp_folder = $docRoutingConfig['archiveToDigitalArchiveTempPath'];
$dir_path = $tmp_folder."/u".$drUserID;

$parentFolderPath = substr($tmp_folder,0,strrpos($tmp_folder,'/'));
if (!file_exists($parentFolderPath)) {
	$libfs->folder_new($parentFolderPath);
	$libfs->chmod_R($parentFolderPath,0777);
}
if (!file_exists($tmp_folder)) {
	$libfs->folder_new($tmp_folder);
	$libfs->chmod_R($tmp_folder,0777);
}

if(file_exists($dir_path)){
	$libfs->folder_remove_recursive($dir_path);
}

$libfs->folder_new($dir_path);
$libfs->chmod_R($dir_path, 0777);

if($LinkToType == "feedback"){
	$LinkToType = $docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'];
}
else{
	$LinkToType = $docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'];
}

$attachment = $ldocrouting->getDRAttachment($LinkToType, $DocumentID, '', $indexVar['paramAry']['Starred']==1);

for($i=0;$i<count($attachment);$i++) 
{
	$filePath = str_replace($docRoutingConfig['dbFilePath'],"",$attachment[$i]['FilePath']);
	$fileName = $attachment[$i]['FileName'];
	$fullpath = $docRoutingConfig['filePath'].$filePath;
	$copyToPath = $dir_path.'/'.$fileName;
	
	while(file_exists($copyToPath)){
		$fileName = $ldamu->Rename_File($fileName);
		$copyToPath = $dir_path."/".$fileName;
	}
	
	if(!file_exists($copyToPath))
	{
		$libfs->file_copy($fullpath, $copyToPath);
	}
}

echo $ldamu->Get_Module_Files_Group_Folder_List_TB_Form($docRoutingConfig['moduleCode'],$dir_path);

?>