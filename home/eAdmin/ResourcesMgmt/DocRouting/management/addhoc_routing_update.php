<?php
// Editing by 
/*
 * 2016-03-15 (Carlos): Fixed use empty route id to send notification email issue.  
 */
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$libReplySlipMgr = new libReplySlipMgr();
$libTableReplySlipMgr = new libTableReplySlipMgr();
$indexVar['libReplySlipMgr'] = $libReplySlipMgr;
$indexVar['libTableReplySlipMgr'] = $libTableReplySlipMgr;
$ldocrouting = $indexVar['libDocRouting'];

$success = $ldocrouting->addDRRoute($_POST, $_FILES, true);

if($success){
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	
	if ($ldocrouting->returnSettingValueByName('emailNotification_newRouteReceived')) {
		$display_order = $_POST['DisplayOrder'];
		if(isset($_POST['RouteID_'.$display_order]) && $_POST['RouteID_'.$display_order] != '' && $_POST['RouteID_'.$display_order] > 0){
			$adhocRouteID = $_POST['RouteID_'.$display_order];
			$adhocRouteUsers = $ldocrouting->stripWordFromArray($_POST['users_'.$display_order],"U");
			$result['Email'] = $ldocrouting->sendNotificationEmailForNewRouteReceived($adhocRouteID,$adhocRouteUsers);
		}else if(isset($_POST['DocumentID']) && $_POST['DocumentID'] != '' && $_POST['DocumentID'] > 0){
			$routeAry = $ldocrouting->getRouteData($_POST['DocumentID']);
			$routeAryCount = count($routeAry);
			if($routeAryCount > 0){
				$adhocRouteID = $routeAry[$routeAryCount-1]['RouteID'];
				if($adhocRouteID != '' && $adhocRouteID > 0){
					$result['Email'] = $ldocrouting->sendNotificationEmailForNewRouteReceived($adhocRouteID);
				}
			}
		}
	}
	
}else{
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

echo '<script>';
echo 'if(window.parent.js_Finish_Update_Addhoc_Routing){
		window.parent.js_Finish_Update_Addhoc_Routing("'.$msg.'");
		}';
echo '</script>';

?>