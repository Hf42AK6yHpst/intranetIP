<?php
// using: 
/*
 * 2016-02-12 (Carlos): add Followed Routings.
 */
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");

$ldocrouting = $indexVar['libDocRouting'];

$docDataAry = array();
$docDataAry['DocumentID'] = $_POST['DocumentID'];
$numOfDocument = count($docDataAry['DocumentID']);

$pageType = $_POST['pageType'];

$docDataAry['DocumentStatus'] = $_POST['documentStatus'];
$docDataAry['DocumentTitle'] = $_POST['documentTitle'];


$result = array();
for($i=0;$i<$numOfDocument;$i++){
	$thisDocumentID = $docDataAry['DocumentID'][$i];
	$thisDocumentTitle = $docDataAry['DocumentTitle'][$i];
	$thisDocumentStatus =  $docDataAry['DocumentStatus'][$i];
	
	# Copy Document
	$result[]= $ldocrouting->copyDocument($thisDocumentID, $thisDocumentTitle, $thisDocumentStatus);
}

if(!in_array(false,$result)){
	$msg = 'UpdateSuccess';
}else{
	$msg = 'UpdateUnsuccess';
}

if ($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']) {
	$location = "index";
} else if ($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']) {
	$location = "draft_routings";
} else if ($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings']) {
	$location = "completed_routings";
}else if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
	$location = "followed_routings";	
}

$param = $ldocrouting->getEncryptedParameter('management',$location,array('returnMsgKey'=>$msg));

header("Location: index.php?pe=".$param);
?>