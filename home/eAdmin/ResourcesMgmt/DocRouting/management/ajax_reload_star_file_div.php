<?php
// Editing by 
/*
 * 2014-05-13 (Carlos): Created
 */
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];
$ldamu = new libdigitalarchive_moduleupload();
$indexVar['libdigitalarchive_moduleupload'] = $ldamu;

$html = $ldocrouting_ui->getStarredFilesDiv($DocumentID);

echo $html;

$indexVar['libDocRouting']->unsetSignDocumentSession();
?>