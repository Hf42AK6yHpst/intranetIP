<?php
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
$ldocrouting = $indexVar['libDocRouting'];

$RouteID = $_REQUEST['RouteID'];
$TargetUserID = $_REQUEST['TargetUserID'];

$success = $ldocrouting->sendNotificationEmailForNewRouteReceived($RouteID,$TargetUserID);
$indexVar['libDocRouting']->unsetSignDocumentSession();

if($success){
	echo $Lang['General']['ReturnMessage']['EmailSent'];
}else{
	echo $Lang['DocRouting']['ReturnMessage']['EmailSentUnsuccess'];
}

?>