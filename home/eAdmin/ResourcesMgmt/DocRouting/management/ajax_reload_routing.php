<?php
// using : 

/******************************
 * Change Log:
 * 2018-03-28 (Carlos): added Deleted Routings page.
 * 2016-02-19 (Carlos): when page type is completed routing, apply academic year filter value to getCurrentRoutingDisplayItem().
 * 2016-02-12 (Carlos): add Followed Routings.
 * 2013-10-03 (Henry): add advance serch parameter $advanceSearchArray
 * 2013-04-15 (Carlos): add filter expireStatus
 ******************************/

//$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Mgmt_CurrentRoutings');

$linterface = new interface_html();

$starStatus = trim($_REQUEST['starStatus']);
$createStatus = $_REQUEST['createStatus'];
$followStatus = $_REQUEST['followStatus'];
$expireStatus = $_REQUEST['expireStatus'];

//$routeEntryStatus = $_REQUEST['statusCheckBox'];
$keyword  = $_REQUEST['keyword'];
$Action = $_REQUEST['Action'];
//Henry Added
$advanceSearchArray = array();
$advanceSearchArray['flag'] = $_REQUEST['flag'];
$advanceSearchArray['routingTitle'] = $_REQUEST['routingTitle'];
$advanceSearchArray['byDateRange'] = $_REQUEST['byDateRange'];
$advanceSearchArray['StartDate'] = $_REQUEST['StartDate'];
$advanceSearchArray['EndDate'] = $_REQUEST['EndDate'];
$advanceSearchArray['byDateRange2'] = $_REQUEST['byDateRange2'];
$advanceSearchArray['StartDate2'] = $_REQUEST['StartDate2'];
$advanceSearchArray['EndDate2'] = $_REQUEST['EndDate2'];
$advanceSearchArray['createdBy'] = $_REQUEST['createdBy'];
$advanceSearchArray['tag'] = $_REQUEST['tag'];
$advanceSearchArray['academic_year'] = $_REQUEST['academic_year'];


$selectedTagID = $_REQUEST['selectedTag'];

//debug_pr($_REQUEST);

//debug_pr($Action);

if($Action == 'CurrentRoutings'){
	$status = array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']);
	$pageType = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings'];	
}
elseif($Action == 'DraftRoutings'){
	$status = array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']) ;	
	$pageType = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings'];
}
elseif($Action == 'CompletedRoutings'){
	$status = array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']) ;
	$pageType = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'];
	$selectedAcademicYear = $_REQUEST['academic_year'];
}elseif($Action == 'FollowedRoutings'){
	$status = array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']) ;
	$pageType = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings'];
}elseif($Action == 'DeletedRoutings'){
	$status = array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted']) ;
	$pageType = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings'];
	$selectedAcademicYear = $_REQUEST['academic_year'];
}


$resultTable = $indexVar['libDocRouting_ui']-> getCurrentRoutingDisplayItem($status, $keyword, $accessRight, $starStatus,$createStatus, $followStatus, $pageType, $selectedTagID, $expireStatus, $advanceSearchArray, $selectedAcademicYear);

echo $resultTable;
?>