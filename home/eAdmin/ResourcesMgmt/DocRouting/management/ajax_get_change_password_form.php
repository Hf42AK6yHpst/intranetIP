<?php
// Editing by 
/*
 * 2015-04-24 (Carlos): Created to handle change password. Update password logic reuse /home/iaccount/account/login_password_update.php
 */
$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];

$sign_record = $indexVar['libDocRouting']->getSignEmailDocumentByToken($indexVar['tok']);
if(count($sign_record)==0 || !isset($_SESSION['DR_TMP_USERID']) ||  $_SESSION['DR_TMP_USERID'] != $sign_record[0]['UserID'])
{
	echo $Lang['DocRouting']['WarningMsg']['UnauthorizedAccess'];
	$indexVar['libDocRouting']->unsetSignDocumentSession();
	exit;
}

$li = new libuser($sign_record[0]['UserID']);

$thisUserType = $li->RecordType;
        		
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;

$SettingArr = $li->RetrieveUserInfoSettingInArrary($SettingNameArr);

//$password_change_disabled = !$SettingArr['CanUpdatePassword_'.$thisUserType];
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6 && $PasswordLength>0)
{
	# at least 6 by default
	$PasswordLength = 6;
}

$UserName = ($intranet_default_lang=='en'? $li->EnglishName : $li->ChineseName);

# Get Btns
$SubmitBtn = $ldocrouting_ui->GET_ACTION_BTN($Lang['Btn']['Submit'],"Button","jsCheckChangedPassword(document.TBChangePasswordForm);");
$CloseBtn = $ldocrouting_ui->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");

$form_param = $ldocrouting->getEncryptedParameter('management','ajax_change_password_update',array());
//$update_link = 'index.php?pe='.$form_param.'&tok='.$indexVar['tok'];
$update_link = '/home/iaccount/account/login_password_update.php';

$x = '';
$x .= '<form id="TBChangePasswordForm" name="TBChangePasswordForm" action="" method="post" onsubmit="return false;">'."\n";
	$x .= '<div style="overflow-x:hidden;overflow-y:auto;height:200px;">'."\n";
	$x .= '<br />';
	$x .= '<table width="98%" cellpadding=0px cellspacing=0px align="center">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td valign="top">'."\n";	
				$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
					$x .= '<col class="field_title">';
					$x .= '<col class="field_c">';
					
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title" nowrap>'.$Lang['DocRouting']['UserName'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= $UserName."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title" nowrap><span class="tabletextrequire">*</span>'.$i_frontpage_myinfo_password_old.'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<input class="textboxnum" type="password" name="OldPassword" maxlength="20"> ('.$i_UserProfilePwdUse.')';
							$x .= $ldocrouting_ui->Get_Thickbox_Warning_Msg_Div("OldPasswordWarningDiv",'', "WarnMsgDiv");
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title" nowrap><span class="tabletextrequire">*</span>'.$i_frontpage_myinfo_password_new.'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<input class="textboxnum" type="password" name="NewPassword" maxlength="20">';
							$x .= $ldocrouting_ui->Get_Thickbox_Warning_Msg_Div("NewPasswordWarningDiv",'', "WarnMsgDiv");
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title" nowrap><span class="tabletextrequire">*</span>'.$i_frontpage_myinfo_password_retype.'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<input class="textboxnum" type="password" name="ReNewPassword" maxlength="20">';
							$x .= $ldocrouting_ui->Get_Thickbox_Warning_Msg_Div("RetypePasswordWarningDiv",'', "WarnMsgDiv");
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
				$x .= '</table>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .= $ldocrouting_ui->MandatoryField();		
	
	# Btns
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $SubmitBtn;
		$x .= "&nbsp;".$CloseBtn;
	$x .= '</div>'."\n";
	$x .= '<input type="hidden" name="UserID" value="'.$li->UserID.'" />';
	$x .= '<input type="hidden" name="FromDR" value="1" />';
	
$x .= '</form>';
$x .= '<iframe id="ProcessFrame" name="ProcessFrame" style="display:none;"></iframe>'."\n";

$x .= '<script type="text/javascript" language="javascript">
		function jsCheckChangedPassword(obj){
		      if(!check_text(obj.OldPassword, "'.$i_alert_pleasefillin.$i_frontpage_myinfo_password_old.'")) return false;
		      if(!check_text(obj.NewPassword, "'.$i_alert_pleasefillin.$i_frontpage_myinfo_password_new.'")) return false;';
		if ($PasswordLength>=6) {
		$x .= ' if (obj.NewPassword.value.length < '.$PasswordLength.')
				{
					alert("'.str_replace("[6]", $PasswordLength, $Lang['PasswordNotSafe'][-2]).'");
					obj.NewPassword.focus();
					return false;
				}
				var re = /[a-zA-Z]/;
				if (!re.test(obj.NewPassword.value))
				{
					alert("'.$Lang['PasswordNotSafe'][-3].'");
					obj.NewPassword.focus();
					return false;
				}
				re = /[0-9]/;
				if (!re.test(obj.NewPassword.value))
				{
					alert("'.$Lang['PasswordNotSafe'][-4].'");
					obj.NewPassword.focus();
					return false;
				}';
		}
		$x .= 'if(!check_text(obj.ReNewPassword, "'.$i_alert_pleasefillin.$i_frontpage_myinfo_password_retype.'")) return false;
		      
		      if (obj.NewPassword.value != "" || obj.ReNewPassword.value != "")
		      {
		          if(obj.NewPassword.value!=obj.ReNewPassword.value){
		              alert("'.$i_frontpage_myinfo_password_mismatch.'");
		              obj.NewPassword.value="";
		              obj.ReNewPassword.value="";
		              obj.NewPassword.focus();
		              return false;
		          }
		      }
			
			Block_Thickbox();
			TBChangePasswordForm.action = "'.$update_link.'";
			TBChangePasswordForm.target = "ProcessFrame";
			TBChangePasswordForm.method = "post";
			TBChangePasswordForm.submit();
		}
		
		function jsFinishChangePassword(ReturnMsg)
		{
			var msg = {"UpdateSuccess":"'.$Lang['General']['ReturnMessage']['UpdateSuccess'].'",
						"UpdateUnsuccess":"'.$Lang['General']['ReturnMessage']['UpdateUnsuccess'].'",
						"InvalidPassword":"'.$Lang['General']['ReturnMessage']['InvalidPassword'].'"};
			Get_Return_Message(msg[ReturnMsg]);
			window.tb_remove();
			window.Scroll_To_Top();
		}
		</script>'."\n";

echo $x;

$indexVar['libDocRouting']->unsetSignDocumentSession();
?>