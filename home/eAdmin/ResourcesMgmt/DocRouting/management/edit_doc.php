<?php
// editing by 
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting_preset.php");

# Page Title
$pageType = ($indexVar['paramAry']['pageType']=='')? $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings'] : $indexVar['paramAry']['pageType'];
if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
	$TAGS_OBJ[] = array($Lang['DocRouting']['CurrentRoutings']);
	$pageCode = 'Mgmt_CurrentRoutings';
}
else{
	$TAGS_OBJ[] = array($Lang['DocRouting']['DraftRoutings']);
	$pageCode = 'Mgmt_DraftRoutings';
}
$returnMsgKey = $indexVar['paramAry']['returnMsgKey'];
$indexVar['libDocRouting_ui']->echoModuleLayoutStart($pageCode, $Lang['General']['ReturnMessage'][$returnMsgKey]);


$lfilesystem = new libfilesystem();
$indexVar['libfilesystem'] = $lfilesystem;
$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];

$dataAry = array();

if(isset($indexVar['paramAry']['DocumentID'])){
	$dataAry['DocumentID'] = $indexVar['paramAry']['DocumentID'];
}else{
	$dataAry['DocumentID'] = $_REQUEST['DocumentID'];
}
$dataAry['PageType'] = $pageType;
$dataAry['Title'] = $_POST['Title'];
$dataAry['Instruction'] = $_POST['Instruction'];
$dataAry['DocumentType'] = $_POST['DocumentType'];
$dataAry['PhysicalLocation'] = $_POST['PhysicalLocation'];
$dataAry['AllowAdHocRoute'] = $_POST['AllowAdHocRoute'];
//$dataAry['DocumentUploaded'] = $_POST['DocumentUploaded'];
$dataAry['DocTags'] = $_POST['DocTags'];
$dataAry['RecordStatus'] = $_POST['RecordStatus'];
$dataAry['tmpRecordStatus'] = $_POST['tmpRecordStatus'];

// create temp document record from iMail plus or iFolder
if($indexVar['paramAry']['createDocFromModule'] != '') {
	$dataAry['DocumentID'] = $ldocrouting->createDocFromModule($indexVar['paramAry']['createDocFromModule'],$indexVar['paramAry']);
	if($dataAry['DocumentID'] != '') {
		$dataAry['DocumentType'] = $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'];
		$dataAry['tmpRecordStatus'] = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'];
	}
}

$indexVar['dataAry'] = $dataAry;

$htmlAry['ajaxGetPresetContentPath'] = $ldocrouting->getEncryptedParameter('management', 'ajax_get_preset_content');
//Henry Added
$htmlAry['ajaxLocationSuggestionPath'] = $ldocrouting->getEncryptedParameter('management', 'ajax_PhysicalLocationTb_suggestion');

include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>