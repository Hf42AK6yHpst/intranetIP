<?php
// Editing by 
/*
 * 2014-11-18 (Carlos)[ip2.5.5.12.1] : Archive the whole document to Digital Archive
 */
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];
$ldamu = new libdigitalarchive_moduleupload();
$indexVar['libdigitalarchive_moduleupload'] = $ldamu;

$DocumentID = $_REQUEST['DocumentID'];

if(!$ldamu->User_Can_Archive_File()){
	exit;
}

$libfs = $ldocrouting->getLibFileSystemInstance();

$drUserID = $indexVar['drUserId'];
$tmp_folder = $docRoutingConfig['archiveToDigitalArchiveTempPath'];
$dir_path = $tmp_folder."/u".$drUserID;

$parentFolderPath = substr($tmp_folder,0,strrpos($tmp_folder,'/'));
if (!file_exists($parentFolderPath)) {
	$libfs->folder_new($parentFolderPath);
	$libfs->chmod_R($parentFolderPath,0777);
}
if (!file_exists($tmp_folder)) {
	$libfs->folder_new($tmp_folder);
	$libfs->chmod_R($tmp_folder,0777);
}

if(file_exists($dir_path)){
	$libfs->folder_remove_recursive($dir_path);
}

$libfs->folder_new($dir_path);
$libfs->chmod_R($dir_path, 0777);

$docData = $ldocrouting_ui->getArchivedDocument($DocumentID);
$title_for_doc_file = $docData['Title'];
$html_content = $docData['Content'];

/*
$docAry = $ldocrouting->getEntryData($DocumentID);
$title = $docAry[0]['Title'];
$title_for_doc_file = str_replace(array('\\','/','<','>','?',':','*','"','|'),'',$title);


$doc_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],array($DocumentID));
$routeAry = $ldocrouting->getRouteData(array($DocumentID));
$routeIdAry = Get_Array_By_Key($routeAry,'RouteID');
$feedbackAry = $ldocrouting->getRouteUserFeedbacks($routeIdAry);
$feedbackIdAry = Get_Array_By_Key($feedbackAry,'FeedbackID');
$feedback_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'],$feedbackIdAry);

//$chars_to_remove = array('#','%','&', '{', '}', '\\', '<', '>' ,'*', '?', '/', ' ', '$', '!', '\'', '"', ':', '@', '+', '`', '|', '=');
//$chars_to_replacewith = array('_');

$attachAry = array(); // 0=> id, 1=> final file name, 2=> tmp full file path
$rawAttachAry = array(); // 0=> id, 1=> final file name, 2=> full file path
for($i=0;$i<count($doc_attachments);$i++) 
{
	$filePath = str_replace($docRoutingConfig['dbFilePath'],"",$doc_attachments[$i]['FilePath']);
	$fileName = $doc_attachments[$i]['FileName'];
	$fullpath = $docRoutingConfig['filePath'].$filePath;
	
	$rawAttachAry[] = array($doc_attachments[$i]['FileID'],$fileName,$fullpath);
}
for($i=0;$i<count($feedback_attachments);$i++) 
{
	$filePath = str_replace($docRoutingConfig['dbFilePath'],"",$feedback_attachments[$i]['FilePath']);
	$fileName = $feedback_attachments[$i]['FileName']; 
	$fullpath = $docRoutingConfig['filePath'].$filePath;
	
	$rawAttachAry[] = array($feedback_attachments[$i]['FileID'],$fileName,$fullpath);
}


$http = $_SERVER['SERVER_PORT'] == '443'? "https" : "http"; 
$server_link = "$http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");

$pe = $ldocrouting->getEncryptedParameter('management','print_document_detail',array('DocumentID'=>$DocumentID,'IsArchive'=>1));

$doc_url = $server_link.'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe='.$pe;

$html_content = getHtmlByUrl($doc_url);

for($i=0;$i<count($rawAttachAry);$i++){
	$mime_type = mime_content_type($rawAttachAry[$i][2]);
	$base64_data = base64_encode(file_get_contents($rawAttachAry[$i][2]));
	$data_uri = "data:$mime_type;base64,".$base64_data;
	
	$html_content = str_replace('###DR_FILE_'.$rawAttachAry[$i][0].'###',$data_uri,$html_content);
}

*/

$doc_file_full_path = $dir_path.'/'.$title_for_doc_file.'.html';

$libfs->file_write($html_content,$doc_file_full_path);

echo $ldamu->Get_Module_Files_Group_Folder_List_TB_Form($docRoutingConfig['moduleCode'],$dir_path);
?>