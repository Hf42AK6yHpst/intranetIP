<?php

$ldocrouting = $indexVar['libDocRouting'];

$routeID = $_REQUEST['RouteID'];
$user_Id = $indexVar['drUserId'];
$comment = trim(rawurldecode(stripslashes($_REQUEST['Comment'])));

$success = $ldocrouting->updateRouteCommentDraft($routeID, $user_Id, $comment); 

$ldocrouting->releaseRouteLock($routeID);

$indexVar['libDocRouting']->unsetSignDocumentSession();

echo $success?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
?>