<?php
// editing by : Henry

/*****************************************************************************
 * Modification Log:
 * Date:	2014-05-28 (Henry)
 * Details:	File Created
 ****************************************************************************/
 
$ldocrouting_ui = $indexVar['libDocRouting_ui'];

$DocumentID = $_REQUEST['DocumentID'];

echo $ldocrouting_ui->getEditRoutingPeriodAndSendEmailForm($DocumentID);

?>