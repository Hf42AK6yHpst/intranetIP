<?php

// Editing by 

/*****************************************************************************
 * Modification Log:
 *
 * Date:	2019-11-15 Sam: Modified parameters taken from form submission as well
 *****************************************************************************/

if($indexVar['paramAry']) {
    $documentId = $indexVar['paramAry']['documentId'];
    $pageType = $indexVar['paramAry']['pageType'];
} else {
    $documentId = $_REQUEST['documentId'];
    $pageType = $_REQUEST['pageType'];
}

$successAry = array();
$indexVar['libDocRouting']->Start_Trans();

$successAry['deleteDocument'] = $indexVar['libDocRouting']->deleteDREntry($documentId);

if (in_array(false, $successAry)) {
	$indexVar['libDocRouting']->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
else {
	$indexVar['libDocRouting']->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

$paramAry = array();
$paramAry['returnMsgKey'] = $returnMsgKey;
if($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
	$pe = $indexVar['libDocRouting']->getEncryptedParameter('management', 'index', $paramAry);
}
elseif($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']){
	$pe = $indexVar['libDocRouting']->getEncryptedParameter('management', 'draft_routings', $paramAry);
}
elseif($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings']){
	$pe = $indexVar['libDocRouting']->getEncryptedParameter('management', 'completed_routings', $paramAry);
}


header('Location: ?pe='.$pe);
?>
