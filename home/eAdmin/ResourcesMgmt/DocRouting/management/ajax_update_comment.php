<?php
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

$ldocrouting = $indexVar['libDocRouting'];

$documentID = $_REQUEST['DocumentID'];
$routeID = $_REQUEST['RouteID'];
//$feedbackID = $_REQUEST['FeedbackID'];
$user_Id = $indexVar['drUserId'];
$comment = trim(rawurldecode(stripslashes($_REQUEST['Comment'])));

$success = array();
//$success['updateComment'] = $ldocrouting->updateComment($routeID,$userId,$Comment);
$newFeedbackID = $ldocrouting->insertComment($documentID,$routeID,$user_Id,$comment);
$ldocrouting->deleteRouteCommentDraft($routeID, $user_Id);

$ldocrouting->releaseRouteLock($routeID);

if($newFeedbackID){
	if ($ldocrouting->returnSettingValueByName('emailNotification_newFeedbackInRoute')) {
		$success['sendMail'] = $ldocrouting->sendNotificationEmailForNewFeedbackReceived($newFeedbackID);
	}	
}else{
		$success['insertComment'] = false;
}

$indexVar['libDocRouting']->unsetSignDocumentSession();

echo !in_array(false,$success)?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
?>