<?php
// using : 
/*
 * Change Log:
 * 	
 */
 
?>
<script language="javascript">
$(document).ready(function () {
	$('input#presetTitle').focus();
});

function js_Go_Back(){	
	window.location='?pe=<?=$goBackLink?>';
}

function js_submit_form(){
	var submitValid = true;
//	var FCKGetInstance = FCKeditorAPI.GetInstance('presetContents');  
//	var getText = FCKGetInstance.EditorDocument.body.innerHTML; 
//	var StripTag = getText.replace(/(<([^>]+)>)/ig,"");
	var content = getEditorValue('presetContents');
	content = $.trim(content.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
	
		
	if($('#presetTitle').val()==''){
		submitValid = false;		
		alert('<?=$Lang['General']['PleaseFillIn'] .' ' . $Lang['DocRouting']['TargetType']['Title'] ?>');			
	}
	
	if(content=='') {
		submitValid = false;
		alert('<?=$Lang['General']['PleaseFillIn'] .' ' . $Lang['DocRouting']['PresetDocInstructionContent'] ?>');			
	}
	
	if(submitValid){	
		$('form').submit();
	
	}

}

</script>

<?=$htmlAry['NewPresetDocDisplay']?>
