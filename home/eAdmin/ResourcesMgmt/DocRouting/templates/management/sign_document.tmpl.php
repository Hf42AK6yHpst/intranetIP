<?php
// Editing by 
/*
 * 2015-04-24 Carlos: Added get change password form js.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<? if (stristr($_SERVER['REQUEST_URI'],'/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/') !== false){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<? } 
 elseif (!$home_header_no_EmulateIE7){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<? } ?>

<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css" rel="stylesheet" type="text/css">
<?if($sys_custom['OnlineRegistry']) {?> 
	<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/parent_reg.css" rel="stylesheet" type="text/css">
<? } ?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/brwsniff.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/SpryValidationTextField.js"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-image: none; background-color: #FAEFD2;">

<table id="eclass_main_frame_table" name="eclass_main_frame_table" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <!--<tr>
          <td height="50" valign="top"></td>
        </tr>-->
        <tr> 
			<td valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
			       <tr>
			       		<td>
						<div id="header">
							<div class="header_title">	
								<span id="module_title" class="menu_closed"><?=$Lang['Header']['Menu']['DocRouting']?></span>
							</div>
						</div>
						</td>
					</tr>
				</table> 
			    <div id="system_message_box" class="SystemReturnMessage" style="display:block; visibility: hidden;"> 
					<div class="msg_board_left">
					  	<div class="msg_board_right" id="message_body"> 
					  		<a href="#" onclick="document.getElementById('system_message_box').style.display = 'none'; return false;">[<?=$Lang['Btn']['Clear']?>]</a>
					  	</div>
					</div>
				</div>
			</td>
		</tr>
        <tr>
          <td align="center">
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
              	<td>
              		<!-- content -->
              		<!--<div id="content" style="padding-left: 150px; width=100%">-->
              		<div id="content" style="padding-left: 10px;width=100%">
		                <table width="99%" border="0" cellspacing="0" cellpadding="0">
		                    <tr>
		                      <td height="27" background="<?=$PATH_WRT_ROOT?>images/2009a/content_01_bg.gif"><img src="<?=$PATH_WRT_ROOT?>images/2009a/10x10.gif" width="13" height="27"></td>
		                      <td background="<?=$PATH_WRT_ROOT?>images/2009a/content_02_bg.gif" height="33" valign="middle" style="padding-left: 5px">
		                      	
		                      	<span class="contenttitle" style="vertical-align: bottom; "><?=$Lang['DcoRouting']['SignRoutings']?></span>
		                      	
		                      </td>
		                      <td height="27" background="<?=$PATH_WRT_ROOT?>images/2009a/content_03_bg.gif"><img src="<?=$PATH_WRT_ROOT?>images/2009a/10x10.gif" width="11" height="27"></td>
		                    </tr>
		                    <tr>
		                      <td width="13" background="<?=$PATH_WRT_ROOT?>images/2009a/content_04.gif"><img src="<?=$PATH_WRT_ROOT?>images/2009a/content_04.gif" width="13" height="13"></td>
		                      <td class="main_content">

<?php
echo $indexVar['libDocRouting_ui']->returnModuleCss();
echo $indexVar['libDocRouting_ui']->returnModuleJs();
echo $indexVar['libDocRouting_ui']->Include_ReplySlip_JS_CSS();
echo $indexVar['libDocRouting_ui']->Include_TableReplySlip_JS_CSS();
?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<?=$indexVar['libDocRouting_ui']->Include_SuperTable_JS_CSS()?>
<script type="text/javascript" language="JavaScript">
var tok = '<?=$indexVar['tok']?>';

function js_Reload_Routing_Detail_Block(routeID)
{
	Block_Element('DivRoute_'+routeID);
	$('#DivRoute_'+routeID).load(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_routing_detail',array())?>&tok='+tok,
		{
			'RouteID':routeID
		},
		function(data){
			UnBlock_Element('DivRoute_'+routeID);
		}
	);
}

function js_Get_Upload_Form(routeID,feedbackID)
{
	tb_show("<?=$Lang['DocRouting']['UploadAttachment']?>","#TB_inline?height=300&width=450&inlineId=FakeLayer");
	
	var editorName = 'Comment_'+routeID;
	var comment = "";
	if(typeof(FCKeditorAPI) == "object"){
		comment = getEditorValue(editorName);
	}
	
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_upload_attachment_form',array())?>&tok='+tok,
		{
			'DocumentID':'<?=$DocumentID?>',
			'RouteID':routeID,
			'FeedbackID':feedbackID,
			'Comment':comment
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		});
}

function js_Show_Hide_Member_Status(obj,routeID,selectedType)
{
	var thisObj = $(obj);
	var dv = $('#member_status_list_'+routeID+'_'+selectedType);
	var isVisible = dv.is(':visible');
	$('#MemberListBtn_'+routeID+' li').removeClass();
	
	$('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']?>').hide();
	$('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']?>').hide();
	$('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']?>').hide();
	if(isVisible){
		dv.hide();
	}else{
		thisObj.closest('li').removeClass().addClass('selected');
		dv.show();
	}
}

function js_Show_Hide_Comment(routeID, isFocus)
{
	isFocus = isFocus || false;
	
	var dv = $('#write_comment_'+routeID);
	var draft_li = $('#LiRouteDraft_'+routeID); 	   
    if(dv.is(':visible')){
		dv.hide();
		if(draft_li.length > 0){
			draft_li.show();
		}
	}else{
		dv.show();
		
		if (isFocus) {
			var oEditor = FCKeditorAPI.GetInstance('Comment_'+routeID);  
			oEditor.Focus();  
		}
		
		if(draft_li.length > 0){
			draft_li.hide();
		}
	}
}


function js_Check_All_Users(routeID,checked)
{
	var inProgressUsers = document.getElementsByName('InprogressUsers_'+routeID+'[]');
	var notViewedUsers = document.getElementsByName('NotViewedUsers_'+routeID+'[]');
	
	for(var i=0;i<inProgressUsers.length;i++){
		inProgressUsers[i].checked = checked;
	}
	for(var i=0;i<notViewedUsers.length;i++){
		notViewedUsers[i].checked = checked;
	}
}

function js_Submit_Comment(documentID,routeID)
{
	var editorName = 'Comment_'+routeID;
	var comment =getEditorValue(editorName);
	//if(comment == ''){	
	if(FCK_Get_Length(editorName)<=0){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInComment']?>');
	}else{
		Block_Element('DivRoute_'+routeID);
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_request_lock',array())?>&tok='+tok,
			{
				'RouteID':routeID
			},
			function(result){
				var resultAry = result.split(',');
				if(resultAry[0] == '1') {
					$.post(
						'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_comment',array())?>&tok='+tok,
						{
							'DocumentID':documentID,
   					    	'RouteID':routeID,
							'Comment':encodeURIComponent(comment)
						},
						function(response){
							UnBlock_Element('DivRoute_'+routeID);
							Get_Return_Message(response);
							js_Reload_Routing_Detail_Block(routeID);
							window.Scroll_To_Top();
						}
					);
				} else {
					alert('<?=$Lang['DocRouting']['WarningMsg']['RouteLockTiemout']?>');
					UnBlock_Element('DivRoute_'+routeID);
				}
			}
		);
		
		
	}
	
}

function FCK_Get_Length(FCKName)
{ 
    var oEditor = FCKeditorAPI.GetInstance(FCKName) ; 
    var checkContent= oEditor.EditorDocument ; 
    var contentLength ; 
    if ( document.all ){ 
        contentLength= checkContent.body.innerText.trim().length ;
	} 
	else{ 
	    var r = checkContent.createRange() ; 
	    r.selectNodeContents( checkContent.body ) ; 
	    contentLength= r.toString().trim().length ; 
	}
 
    return contentLength; 
} 

String.prototype.trim = function() 
{ 
    return this.replace(/(^[\s]*)|([\s]*$)/g, ""); 
} 


function js_Send_Reminder(routeID)
{
	var inProgressUsers = document.getElementsByName('InprogressUsers_'+routeID+'[]');
	var notViewedUsers = document.getElementsByName('NotViewedUsers_'+routeID+'[]');
	var users = [];
	var dv_inprogress = $('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']?>');
	var dv_notviewed = $('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']?>');
	
	if(dv_inprogress.is(':visible')){
		for(var i=0;i<inProgressUsers.length;i++){
			if(inProgressUsers[i].checked){
				users.push(inProgressUsers[i].value);
			}
		}
	}
	if(dv_notviewed.is(':visible')){
		for(var i=0;i<notViewedUsers.length;i++){
			if(notViewedUsers[i].checked){
				users.push(notViewedUsers[i].value);
			}
		}
	}
	
	if(users.length == 0){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseSelectUsers']?>');
	}else{
		Block_Element('DivRoute_'+routeID);
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_send_reminder',array())?>&tok='+tok,
			{
				'RouteID':routeID,
				'TargetUserID[]':users 
			},
			function(response){
				Get_Return_Message(response);
				UnBlock_Element('DivRoute_'+routeID);
				window.Scroll_To_Top();
			}
		);
	}
}



function js_Show_Hide_Replyslip_Stat(routeID,replySlipID,replySlipType)
{
	var dv_stat = $('#reply_slip_stat_'+routeID);
	var dv_replyslip = $('#reply_slip_content_'+routeID);	
	if(dv_stat.is(':visible')){
		dv_stat.hide();
		dv_replyslip.show();
	}else{
		js_Get_Reply_Slip_Stat(routeID,replySlipID,replySlipType);
		dv_stat.show();
		dv_replyslip.hide();

	}
}

function js_Sign_Routing(routeID,replySlipID,replySlipType)
{
	var success = true;
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['AreYouSureWantToSign']?>')){
		Block_Element('DivRoute_'+routeID);
		
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_check_draft',array())?>&tok='+tok,
			{
				'RouteID':routeID 
			},
			function(data) {
				var canSubmit = true;
				if(data > 0) {
					if(confirm('<?=$Lang['DocRouting']['WarningMsg']['SignIgnoreDraftedComment']?>')) {
						canSubmit = true;
					}else{
						canSubmit = false;
					}
				}
				if(canSubmit) {
					if(replySlipID){
						if(replySlipType=='<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'){
							success = tableReplySlipObj.submitReplySlip(replySlipID);
						} else {
							success = replySlipObj.submitReplySlip(replySlipID);
						}
					}
			
					if(success){
						$.post(
							'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_sign_routing',array())?>&tok='+tok,
							{
								'DocumentID':<?=$DocumentID?>,
								'RouteID':routeID 
							},
							function(response){
								Get_Return_Message(response);
								js_Reload_Routing_Detail_Block(routeID);
								window.Scroll_To_Top();
							}
						);
					}else{
						UnBlock_Element('DivRoute_'+routeID);
					}
				}else{
					UnBlock_Element('DivRoute_'+routeID);
				}
			}
		);
	}
}

function js_Update_Doc_Status(documentID,status)
{
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_doc_status',array())?>&tok='+tok,
		{
			'DocumentID':documentID,
			'RecordStatus':status
		},
		function(response){
			//window.location.href = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','sign_document',array('documentId'=>$DocumentID,'returnMsgKey'=>'UpdateSuccess'))?>&tok='+tok;
		}
	);
}

function js_Add_File_Field()
{
	var divUploadFile = $('div#DivUploadFile');
	var num = $('input[type=file]').length;
	divUploadFile.append('<input name="UploadFile_'+num+'" type="file" class="Mandatory"><br />');
}

function js_Check_Upload_Form()
{
	var TBEditForm = document.getElementById('TBEditForm');
	var Files = $('input[name*=UploadFile_]');
	var WarningDiv = $('div#UploadFileWarningDiv');
	var CountFile = 0;
	for(var i=0;i<Files.length;i++){
		if($.trim(Files[i].value) != ''){
			CountFile++;
		}
	}
	if(CountFile==0){
		WarningDiv.show();
		return;
	}else{
		WarningDiv.hide();
	}
	
	Block_Thickbox();
	
	TBEditForm.action = "index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_feedback_upload_attachment',array())?>&tok="+tok;
	TBEditForm.target = "FileUploadFrame";
	TBEditForm.encoding = "multipart/form-data";
	TBEditForm.method = 'post';
	TBEditForm.submit();
}

function js_Finish_Upload_Files(routeID,returnMsg)
{
	Get_Return_Message(returnMsg);
	window.tb_remove();
	js_Reload_Routing_Detail_Block(routeID);
	window.Scroll_To_Top();
}

function js_Delete_File(routeID,fileID,feedbackID)
{
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisFile']?>')){
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_delete_file',array())?>&tok='+tok,
			{
				'FileID':fileID,
				'FeedbackID':feedbackID 
			},
			function(response){
				Get_Return_Message(response);
				js_Reload_Routing_Detail_Block(routeID);
				js_Reload_Star_File_Div();
				window.Scroll_To_Top();
			}
		);
	}
}

function js_Get_Reply_Slip_Stat(routeID,replySlipID,replySlipType)
{
	if($('#reply_slip_stat_'+routeID).html()==''){
		Block_Element('reply_slip_stat_'+routeID);
		var listTypeLink = "<?=$PATH_WRT_ROOT?>home/common_reply_slip/ajax_get_reply_slip_stat_div.php";
		var tableTypeLink = "<?=$PATH_WRT_ROOT?>home/common_table_reply_slip/ajax_get_reply_slip_stat_div.php";
		var replySlipLink = replySlipType == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
		
		$('#reply_slip_stat_'+routeID).load(
			replySlipLink, 
			{ 
				ReplySlipID: replySlipID,
				DefaultShowDiv: 1
			},
			function(ReturnData)
			{
				initSuperTable(replySlipID, true);
				UnBlock_Element('reply_slip_stat_'+routeID);
			}
		);
	}
}

function js_Get_Reply_Slip_Answer(routeID,userID,replySlipID,disableAnswer,replySlipType)
{
	Block_Element('reply_slip_content_'+routeID);
	var listTypeLink = "<?=$PATH_WRT_ROOT?>home/common_reply_slip/ajax_get_reply_slip_div.php";
	var tableTypeLink = "<?=$PATH_WRT_ROOT?>home/common_table_reply_slip/ajax_get_reply_slip_div.php";
	var replySlipLink = replySlipType == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
	
	$('#reply_slip_content_'+routeID).load(
		replySlipLink, 
		{ 
			ReplySlipID: replySlipID,
			ReplySlipUserID: userID,
			ShowUserAnswer: 1,
			DisableAnswer: disableAnswer 
		},
		function(ReturnData)
		{
			initSuperTable(replySlipID, false);
			UnBlock_Element('reply_slip_content_'+routeID);
		}
	);
}

function js_Update_Star_Status(obj)
{
	var thisObj = $(obj);
	var star_class = thisObj.attr('class');
	var classes = ['','btn_set_important','btn_set_important_on'];
	var starDisplayCss = star_class == 'btn_set_important'?2:1;
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_star_status',array())?>&tok='+tok,
		{
			'Action':'UpdateStarStatus',
			'documentID':'<?=$DocumentID?>',
			'userID':'<?=$indexVar['drUserId']?>',
			'starDisplayCss':starDisplayCss
		},
		function(ReturnData){
			thisObj.attr('class',classes[starDisplayCss]);
		}
	);
}

function js_Request_Lock(routeId)
{
	var dv = $('#write_comment_'+routeId);
	if(dv.is(':visible')){
		js_Release_Lock(routeId);
		js_Show_Hide_Comment(routeId,true);
	}else {

		Block_Element('DivRoute_'+routeId,'<?=$Lang['DocRouting']['CheckingRoutingStatus']?>');
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_request_lock',array())?>&tok='+tok,
			{
				'RouteID':routeId
			},
			function(result){
				UnBlock_Element('DivRoute_'+routeId);
				var resultAry = result.split(',');
				if(resultAry[0] == '1') {
					js_Show_Hide_Comment(routeId,true);
				} else {
					var msg = '<?=$Lang['DocRouting']['WarningMsg']['AnotherPersonGivingFeedback']?>';
					msg = msg.replace('<!--TIMEOUT-->',resultAry[1]);
					alert(msg);
				}
			}
		);
	
	}
}

function js_Release_Lock(routeId)
{
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_release_lock',array())?>&tok='+tok,
		{
			'RouteID':routeId
		},
		function(result){
			
		}
	);
}

function js_Save_Draft(routeID)
{
	var editorName = 'Comment_'+routeID;
	var comment = getEditorValue(editorName);
	
//	if(comment == ''){
    if(FCK_Get_Length(editorName)<=0){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInComment']?>');
	}else{
		Block_Element('DivRoute_'+routeID);
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_save_draft',array())?>&tok='+tok,
			{
				'RouteID':routeID,
				'Comment':encodeURIComponent(comment)
			},
			function(response){
				UnBlock_Element('DivRoute_'+routeID);
				Get_Return_Message(response);
				js_Reload_Routing_Detail_Block(routeID);
				window.Scroll_To_Top();
			}
		);
	}
}

function initSuperTable(replySlipId, statTable) {
	var jsScreenWidth = parseInt(Get_Screen_Width());
	var jsTableWidth = jsScreenWidth - 300;
	var jsScreenHeight = parseInt(Get_Screen_Height());
	var jsTableHeight = jsScreenHeight - 100;
	
	var tablePrefix = (statTable)? 'replySlipStatTbl_' : 'replySlipTbl_';
	var hiddenFieldPrefix = (statTable)? 'tableStatReplySlip' : 'tableReplySlip';
	
	var tableId = tablePrefix + replySlipId;
	var numOfHeaderRow = $('input#' + hiddenFieldPrefix + 'NumOfHeaderRow_' + replySlipId).val(); 
	var numOfFixedCol = $('input#' + hiddenFieldPrefix + 'NumOfFixedCol_' + replySlipId).val();
	var originalTableHeight = $('table#' + tableId).height();
	
	if (numOfHeaderRow > 0 && numOfFixedCol > 0) {
		$('table#' + tableId).toSuperTable({
	    	width: 		jsTableWidth + "px", 
	    	height: 	jsTableHeight + "px", 
	    	headerRows:	numOfHeaderRow,
	    	fixedCols: 	numOfFixedCol,
	    	onFinish: 	function () {
	    					var _tableId = this.sDataTable.id;
	    					var _replySlipId = parseInt(_tableId.replace(tablePrefix, ''));
	    					
	    					if (originalTableHeight < jsTableHeight) {
	    						fixSuperTableHeight(_tableId, originalTableHeight + 10);
	    					}
	    				}
		});
	}
}


<?php
	if ($plugin['power_voice']){ 
?>
		var NewRecordBefore = false;
		var CurRecordRouteID = 0;
		
		function listenPVoice2(fileName)
		{		
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet2.php?from_intranet=1&location="+fileName, 18);
		}	
		
		function editPVoice(routeID)
		{
			var fileName = $('input[name=voiceFile_'+routeID+']').val();		
			CurRecordRouteID = routeID;
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/index.php?from_intranet=1&fileName="+fileName, 17);
		}	
		
		function clearPVoice(routeID)
		{		
			var voiceObj = document.getElementById("voiceDiv_"+routeID);
			var clearObj = document.getElementById("clearDiv_"+routeID);
			
			voiceObj.innerHTML = "";
			$('input[name=voiceFile_'+routeID+']').val("");		
			clearObj.style.display = "none";
		}

		function file_exists()
		{
			alert("<?=$classfiles_alertMsgFile12?>");
		}
		
		function file_saved(fileName,filePath,fileHttpPath,fileEmpty)
		{		
			var voiceObj = document.getElementById("voiceDiv_"+CurRecordRouteID);
			var clearObj = document.getElementById("clearDiv_"+CurRecordRouteID);
			
			if (fileName != "")
			{
				if ((fileHttpPath != undefined) &&  (fileHttpPath != ""))
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice2('"+fileHttpPath+"')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
				else				
					voiceObj.innerHTML = fileName+"&nbsp;&nbsp;";
				if (fileEmpty=="1")
				{
					$('input[name=is_empty_voice_'+CurRecordRouteID+']').val(1);
				}
				else
				{
					NewRecordBefore = true;
					$('input[name=is_empty_voice_'+CurRecordRouteID+']').val("");
				}
					
				$('input[name=voiceFile_'+CurRecordRouteID+']').val(fileName);
				$('input[name=voicePath_'+CurRecordRouteID+']').val(filePath);
				clearObj.style.display = "block";
				
				if(fileEmpty != "1"){
					js_Save_Powervoice_File(CurRecordRouteID);
				}
			}
		}
		
		function js_Save_Powervoice_File(routeID)
		{
			Block_Element('DivRoute_'+routeID);
			var voiceFile = $('input[name=voiceFile_'+routeID+']').val();
			var voicePath = $('input[name=voicePath_'+routeID+']').val();
			
			$.post(
				'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_save_powervoice_file',array())?>&tok='+tok,
				{
					'DocumentID':'<?=$DocumentID?>',
					'RouteID':routeID,
					'VoiceFile':encodeURIComponent(voiceFile),
					'VoicePath':voicePath 
				},
				function(response){
					Get_Return_Message(response);
					js_Reload_Routing_Detail_Block(routeID);
					window.Scroll_To_Top();
				}
			);
			
		}
		
<? 
	} 
?>

function js_Star_File(obj,fileId)
{
	var update_url = '';
	var change_class = '';
	var obj_class = $(obj).attr('className');
	if(obj_class == 'btn_set_important'){
		change_class = 'btn_set_important_on';
		update_url = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_file_star_update',array('ajax_task'=>'addFileStar'))?>&tok='+tok;
	}else if(obj_class == 'btn_set_important_on'){
		change_class = 'btn_set_important';
		update_url = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_file_star_update',array('ajax_task'=>'deleteFileStar'))?>&tok='+tok;
	}
	
	$.post(
		update_url,
		{
			'FileID':fileId 
		},
		function(response){
			if(response == "1"){
				$(obj).attr('className',change_class);
				js_Reload_Star_File_Div();
			}
		}
	);
}

function js_Reload_Star_File_Div()
{
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_reload_star_file_div',array())?>&tok='+tok,
		{
			'DocumentID':'<?=$DocumentID?>'
		},
		function(data){
			$('div#StarFileDiv').replaceWith(data);
		}
	);
}

function jsGetChangePasswordForm()
{
	tb_show("<?=$i_admintitle_sa_password?>","#TB_inline?height=300&width=450&inlineId=FakeLayer");
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_change_password_form',array())?>&tok='+tok,
		{
			
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		}
	);
}
<?php
/*
if(isset($indexVar['paramAry']['returnMsgKey']) && $Lang['DocRouting']['ReturnMessage'][$indexVar['paramAry']['returnMsgKey']]!='' && $indexVar['paramAry']['updateTime']>=time()){
	echo '$(document).ready(function(){'."\n";
		echo 'Get_Return_Message(\''.$Lang['DocRouting']['ReturnMessage'][$indexVar['paramAry']['returnMsgKey']].'\');'."\n";
	echo '});'."\n";
}
*/
?>
</script>
<?php
if($indexVar['signCountData']['TotalSigned'] < $indexVar['signCountData']['TotalRoute']){
	echo $indexVar['libDocRouting_ui']->getDocDetailForm($DocumentID);
}else{
	echo $Lang['DocRouting']['SignedAllRoutingMessage'];
}
?>
								<td width="11" background="<?=$PATH_WRT_ROOT?>images/2009a/content_06.gif"><img src="<?=$PATH_WRT_ROOT?>images/2009a/content_06.gif" width="11" height="13"></td>
		                    </tr>
		                    <tr>
		                      <td width="13" height="10"><img src="<?=$PATH_WRT_ROOT?>images/2009a/content_07.gif" width="13" height="10"></td>
		                      <td background="<?=$PATH_WRT_ROOT?>images/2009a/content_08.gif" height="10"><img src="<?=$PATH_WRT_ROOT?>images/2009a/content_08.gif" width="13" height="10"></td>
		                      <td width="11" height="10"><img src="<?=$PATH_WRT_ROOT?>images/2009a/content_09.gif" width="11" height="10"></td>
		                    </tr>
		                </table>
		              </div>
		         </td>
		       </tr>
		     </table>
		  </td>
	    </tr>
	</table>
	</td>
  </tr>
  <tr> 
    <td class="footer"> 
    <?php
    if($eclass_school_copright_footer != ''){
    	echo $eclass_school_copright_footer;
    }else{
     	echo '<span>Powered by <a href="http://www.eclass.com.hk" target="_blank"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/logo_eclass_footer.gif" width="39" height="15" border="0" align="absmiddle"></a></span>';
    }
    ?>
    </td>
  </tr>
  <!--
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td class="footer"></td>
			</tr>
		</table>
	</td>
  </tr>
  -->
</body>
</html>						