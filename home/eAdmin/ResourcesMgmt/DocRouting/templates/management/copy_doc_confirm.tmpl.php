<?php
//using: 

/*******************************
 * 2017-07-06 (Carlos): Disable the submit button after submit form to prevent cloned repeatedly.
 * Date:	2013-02-20 (Rita)
 * Details:	add js_Check_Form()
 ******************************/

?>

<script type="text/javascript" language="JavaScript">
function js_Check_Form(){
	$('#submit2').attr('disabled', true);
	var error = 0;
	$('.documentTitle').each(function(){
	
		if($(this).val()==''){		
			error++; 
		}	
	});
	
	if(error>0){
		alert('<?php echo $Lang['General']['PleaseFillIn'] . ' ' . $Lang['DocRouting']['TargetType']['Title']?>');
		$('#submit2').attr('disabled', false);	
		return false;
	}else{
		return true;
	}
}

</script>

<?php
	echo $ldocrouting_ui->getCopyDocumentsStep2DisplayTable($CopyDoc, $CopyRoute, $pageType);
?>