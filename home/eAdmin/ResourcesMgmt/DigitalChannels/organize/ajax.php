<?
// using: Henry

/*
 * 2020-03-27 (Henry):
 * 		- modified ffmpeg command to support resolution flag $sys_custom['DigitalChannels']['custScale'] [Case#V182856]
 * 2019-06-27 (Henry):
 * 		- modified memory_limit to 500M for case 'addphoto'
 * 2019-04-30 (Henry)
 *      - command injection handling
 * 2019-01-08 (Cameron)
 *      - add case updatephotoChiDescription and updateChiEventTitle
 * 2018-03-16 (Cameron)
 *      - pass album_id to case reorderAlbumPhotos() so that it can reorder quick-add photos only
 * 2018-03-09 (Cameron)
 *      - add case updateEventTitle and updateEventDate
 * 2016-07-25 (Paul)
 * 		- updated case addphoto, removed deprecated ffmpeg flag
 * 2016-05-26 (Henry)
 * 		- file format error msg amendment [Case#D96212]
 * 2015-12-16 (Henry):
 * 		- add ini_set('memory_limit','200M') for case 'addphoto'
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2015-07-13 (Henry): Added checking the orientation of the video
 * 2015-05-06 (Henry): support MOV, RM, FLV video format
 * 2015-04-22 (Henry): rename json_encode to json_encode_cust
 */

$PATH_WRT_ROOT = "../../../../../";
//$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once ($PATH_WRT_ROOT."includes/libvod.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
$ldc = new libdigitalchannels();
$lfs = new libfilesystem();

if($junior_mck > 0){
	$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
}

switch ($action){
    
    case 'setalbum':
	
	$params = array(
		'CategoryCode'=>$CategoryCode,
	    'title'=>htmlspecialchars($title),
	    'description'=>htmlspecialchars($description),
	    'cover_photo_id'=>$cover_photo_id,
	    'place'=>htmlspecialchars($place),
	    'date'=>$AccessDate,
	    'since'=>$StartDate,
	    'until'=>$EndDate,
	    'album_id'=>$AlbumID
	);
	
	if ($AlbumID){
	    
	    $kis_data['album_id'] = $AlbumID;
	    $kis_data['album_title'] = $title? $title: '('.$Lang['DigitalChannels']['Organize']['UntitledAlbum'].')';
	    $kis_data['share_to'] = $share_to?$share_to:'myself'; //for displaying current selected target in edit
	    $select_groups = $share_to?$select_groups:array();
	    //$ldc->Update_Album($params);
	    //$libkis_album->resetAlbumUsersGroups($share_to=='all', $select_groups);
	    $album = $ldc->getAlbum($AlbumID);
	    if (!file_exists($file_path.libdigitalchannels::getAlbumFileName('photo', $album))){
	    	$lfs->createFolder($file_path.libdigitalchannels::getAlbumFileName('photo', $album));
	    	$lfs->createFolder($file_path.libdigitalchannels::getAlbumFileName('thumbnail', $album));
	    	$lfs->createFolder($file_path.libdigitalchannels::getAlbumFileName('original', $album));
//		    mkdir($file_path.libdigitalchannels::getAlbumFileName('photo', $album), 0755, true);
//		    mkdir($file_path.libdigitalchannels::getAlbumFileName('thumbnail', $album), 0755, true);
//		    mkdir($file_path.libdigitalchannels::getAlbumFileName('original', $album), 0755, true);
	    }
	}
//	else{
//	    $kis_data['album_title'] = $title? $title: '('.$kis_lang['untitledalbum'].')';
//	    $kis_data['share_to'] = $share_to?$share_to:'myself'; //for displaying current selected target in edit
//	    $select_groups = $share_to?$select_groups:array();
//	    //$ldc->Update_Album($params);
//	    //$libkis_album->resetAlbumUsersGroups($share_to=='all', $select_groups);
//    	$params = array(
//			'CategoryCode'=>$CategoryCode
//		);
//		$AlbumID = $ldc->Add_Album($params);
//		$kis_data['album_id'] = $AlbumID;
//	    $album = $ldc->getAlbum($AlbumID);
//	    if (!file_exists($file_path.libdigitalchannels::getAlbumFileName('photo', $album))){
//	    	$lfs->createFolder($file_path.libdigitalchannels::getAlbumFileName('photo', $album));
//	    	$lfs->createFolder($file_path.libdigitalchannels::getAlbumFileName('thumbnail', $album));
//	    	$lfs->createFolder($file_path.libdigitalchannels::getAlbumFileName('original', $album));
////		    mkdir($file_path.libdigitalchannels::getAlbumFileName('photo', $album), 0755, true);
////		    mkdir($file_path.libdigitalchannels::getAlbumFileName('thumbnail', $album), 0755, true);
////		    mkdir($file_path.libdigitalchannels::getAlbumFileName('original', $album), 0755, true);
//	    }
//	}

//	else if ($libkis_album->hasAlbumCreatePermission()){
//	    
//	    $kis_data['album_id'] = $libkis_album->createAlbum($params);
//	    $kis_data['album_title'] = $title? $title: '('.$kis_lang['untitledalbum'].')';
//	    $kis_data['share_to'] = $share_to?$share_to:'myself'; //for displaying current selected target in edit
//	    $select_groups = $share_to?$select_groups:array();
//	    $album = $libkis_album->getAlbum();
//	    	    
//	    mkdir($file_path.libdigitalchannels::getAlbumFileName('photo', $album), 0755, true);
//	    mkdir($file_path.libdigitalchannels::getAlbumFileName('thumbnail', $album), 0755, true);
//	    mkdir($file_path.libdigitalchannels::getAlbumFileName('original', $album), 0755, true);
//	    
//	    $libkis_album->resetAlbumUsersGroups($share_to=='all', $select_groups);
//	    
//	}
	else{
	    $kis_data['error'] = 1;
	}
	
	echo json_encode_cust($kis_data);
	
    break;

    case 'addphoto':
    
    $max_upload_size = libdigitalchannels::$max_upload_size;
    if($max_upload_size > 0){
    	ini_set('memory_limit','500M');
    }
    else{
    	ini_set('memory_limit','-1');
    }
    ini_set ('gd.jpeg_ignore_warning', 1);
	include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
	$lgs = new libgeneralsettings();
	$settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'"));
	
	$img_path = $_FILES['file']['tmp_name'];
	$size = $_FILES['file']['size'];
	$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	
	//$videoFormat = array('3GP','WMV','MP4');
	$videoFormat = libdigitalchannels::$videoFormat;
	
	list($width, $height, $img_type) = getimagesize($img_path);
	
	$imgFormatNotMatch = false;
	switch ($img_type) {
	    case IMAGETYPE_GIF:
		$image = imagecreatefromgif($img_path);
	    break;
	    case IMAGETYPE_JPEG:
		$image = imagecreatefromjpeg($img_path);
	    break;
	    case IMAGETYPE_PNG:
		$image = imagecreatefrompng($img_path);
	    break;
	    default:
        $imgFormatNotMatch = true;
	}
	
	if ($image){
	    
	    $exif = @exif_read_data($img_path);
	    
	    //orientation handling [Start]
	    if (!empty($exif['Orientation'])) {
	        switch ($exif['Orientation']) {
	            case 3:
	                $image = imagerotate($image, 180, 0);
	                break;
	
	            case 6:
	                $image = imagerotate($image, -90, 0);
	                $temp_width = $width;
	                $width = $height;
	                $height = $temp_width;
	                break;
	
	            case 8:
	                $image = imagerotate($image, 90, 0);
	                $temp_width = $width;
	                $width = $height;
	                $height = $temp_width;
	                break;
	        }
	    }
	    //orientation handling [End]
	    
	    $kis_data['title'] = $_FILES['file']['name'];
	    $kis_data['date_taken'] = (int)strtotime($exif['DateTime']);
	    $kis_data['date_uploaded'] = time();
	    
	    $photo = array(
		'title'=>htmlspecialchars($kis_data['title']),
		'date'=>$kis_data['date_taken'],
		'size'=>$size,
		'album_id'=>$album_id
	    );

	    $photo_id = $ldc->Add_Album_Photo($photo);
	
	    $album = $ldc->getAlbum($album_id);
	    $photo = $ldc->getAlbumPhoto($photo_id);
	    
	    $photo_url 		= libdigitalchannels::getPhotoFileName('photo', $album, $photo);
	    $thumbnail_url 	= libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo);
	    
	    $ext = pathinfo($kis_data['title'], PATHINFO_EXTENSION);
	    $original_url 	= libdigitalchannels::getPhotoFileName('original', $album, $photo, $ext);

	    $resized = libdigitalchannels::getResizedImage($image, $width, $height, libdigitalchannels::$photo_max_size);
	    imagejpeg($resized, $file_path.$photo_url, 100);
	    imagedestroy($resized);
	    
	    $thumbnail = libdigitalchannels::getResizedImage($image, $width, $height, libdigitalchannels::$thumbnail_max_size);
	    imagejpeg($thumbnail, $file_path.$thumbnail_url, 100);
	    imagedestroy($thumbnail);
	    
	    if($settings['KeepOriginalPhoto']=='1'){
	    	move_uploaded_file($img_path, $file_path.$original_url);
	    }  	
	    imagedestroy($image);
	    
	    $kis_data['thumbnail'] = $thumbnail_url;
	    $kis_data['photo_id']  = $photo_id;
	    
	}
	# handle video file
	else if(in_array(strtoupper($ext),$videoFormat)){
		$current_ext = '.'.trim($ext);

		# Import records
		$kis_data['title'] = $_FILES['file']['name'];
		$kis_data['date_uploaded'] = time();
		$photo = array(
			'title'=>htmlspecialchars($kis_data['title']),
			'date'=>$kis_data['date_uploaded'],
			'size'=>$size,
			'album_id'=>$album_id
		);
			
		# Add record to DB
		$photo_id = $ldc->Add_Album_Photo($photo);
		
		# Get required data
		$album = $ldc->getAlbum($album_id);
		$photo = $ldc->getAlbumPhoto($photo_id);

		# Get details of video
		$video_details = libdigitalchannels::getVideoDetails($img_path);
		
		# Get target path name of mp4 video
		$photo_url 	= libdigitalchannels::getPhotoFileName('photo', $album, $photo, 'mp4');

		# Keep original video
		if($settings['KeepOriginalPhoto']=='1'){
			# Get target path name of original video
			$original_url 	= libdigitalchannels::getPhotoFileName('original', $album, $photo, $ext);
			
			# Store original video
			$original_url = $file_path.$original_url;
			move_uploaded_file($img_path, $original_url);
			
			# Check if original file exist
			if(!file_exists($original_url)){
				$kis_data['error'] = $Lang['DigitalChannels']['Remarks']['FailToUpload'].'!';
				$ldc->removeAlbumPhoto($photo_id);
				break;
			}
		} else {
			$original_url = $img_path;
		}
		
		# check the orientation of the video [start]
		$cmd = OsCommandSafe("mediainfo '$original_url' | grep Rotation");
		$ret = @exec($cmd);
		
		$rotate_cmd = '';
		if($ret){
			if(!(strpos($ret, ': 90')===false)){
				$rotate_cmd = " transpose=1 ";
			}
			else if(!(strpos($ret, ': 180')===false)){
				$rotate_cmd = " vflip,hflip ";
			}
			else if(!(strpos($ret, ': 270')===false)){
				$rotate_cmd = " transpose=2 ";
			}
		}
		
		# check the orientation of the video [end]
		
		# Convert to mp4 - H.264/MPEG-4 & FAAC
//		$video_half = "-s ".round($video_details['Width'] / 2)."x".round($video_details['Height'] / 2)." ";
		if(strpos(trim($video_details['Codec']), 'h264') === false || $rotate_cmd || isset($sys_custom['DigitalChannels']['custBitRate'])){
			$cmd = OsCommandSafe("/usr/local/bin/ffmpeg -version");
			$ffmpegInfo = @exec($cmd, $version);
			if($version[0] == "ffmpeg 0.7.1"){
				$acodec = "-acodec libfaac";
			}else{
				$acodec = "-acodec aac";
			}
			
			if ($sys_custom['DigitalChannels']['custBitRate']) {
				$bitrate_cmd = "-maxrate ".$sys_custom['DigitalChannels']['custBitRate']." -bufsize ".$sys_custom['DigitalChannels']['custBitRate'];
			}
			
			if ($sys_custom['DigitalChannels']['custScale']) {
				 $rotate_cmd = " \"".($rotate_cmd?$rotate_cmd.",":"")."scale=".$sys_custom['DigitalChannels']['custScale'].":ceil(ih/iw*".$sys_custom['DigitalChannels']['custScale']."/2)*2\"";
			}
			if($rotate_cmd){
				$rotate_cmd ='-vf '.$rotate_cmd;
			}
			//$cmd_mp4 = "/usr/local/bin/ffmpeg -y -i '".$original_url."' -sameq -acodec libfaac -ar 22050 -f mp4 -vcodec libx264 -preset ultrafast -crf 25 ".$rotate_cmd." '".$file_path.$photo_url."' 2>&1";
			//debug_pr("/usr/local/bin/ffmpeg -y -i '".$original_url."' $acodec -ar 22050 -f mp4 -vcodec libx264 -preset ultrafast -crf 24 ".$bitrate_cmd." ".$rotate_cmd." -tune zerolatency '".$file_path.$photo_url."' 2>&1");die();
			$cmd_mp4 = OsCommandSafe("/usr/local/bin/ffmpeg -y -i '".$original_url."' $acodec -ar 22050 -f mp4 -vcodec libx264 -preset ultrafast -crf 24 ".$bitrate_cmd." ".$rotate_cmd." -tune zerolatency '".$file_path.$photo_url."' 2>&1");
			$tmp = exec($cmd_mp4, $encodeContent);
		}
//		"/usr/local/bin/ffmpeg -y -i '".$original_url."' -sameq -acodec libfaac -ar 22050 -f mp4 -vcodec libx264 -s 640x480 -aspect 4:3 '".$file_path.$photo_url."' 2>&1"
//		/usr/local/bin/ffmpeg -y -i '".$file_path.$original_url."' -f mp4 -sameq -codec:v libx264 -crf 23 -preset medium -codec:a libmp3lame -ar 22050 -vcodec libx264 '".$file_path.$photo_convert_url."'
		else {
			copy($original_url, $file_path.$photo_url);
		}
				
		# Check if converted mp4 file exist
		if(!file_exists($file_path.$photo_url)){
			$kis_data['error'] = $Lang['DigitalChannels']['Remarks']['FailToUpload'].'!';
			$ldc->removeAlbumPhoto($photo_id);
			break;
		}

		#################  Integrate VOD Start  #################
		$libvod = new libvod;
		$vod_uri = null;
		if ($libvod->enabled()) {
			// Fetch the school folder
			$folder_uri = $libvod->findOrCreateSchoolFolder($config_school_code);

			// Upload to Vimeo
			$vod_uri = $libvod->upload($file_path.$photo_url,
				htmlspecialchars($kis_data['title']),
				''
			);

			$vod_info = $libvod->videoInfo($vod_uri);

			// Move video to school folder
			$libvod->move($folder_uri, $vod_info[1]);

			// Update records
			$ldc->updateVodSrc($vod_uri, $photo_id);

			// Set Embed Privacy
			$libvod->setEmbedWhiteList($vod_info[1]);
		}
		#################  Integrate VOD End  #################
		
		# Generate thumbnail
		$thumbnail_url = str_replace(".mp4", ".jpg", $photo_url);
		$thumbnail_url = str_replace("/photo/", "/thumbnail/", $thumbnail_url);
		$thumbnail_time = round($video_details['Total_s'] / 2) + 5;
		$cmd_thumbnail = OsCommandSafe("/usr/local/bin/ffmpeg -i '".$original_url."' -vframes 1 -an -ss ".$thumbnail_time." ".$rotate_cmd." '".$file_path.$thumbnail_url."'");
		exec($cmd_thumbnail);
//		/usr/local/bin/ffmpeg -y -i 'home2/web/eclass40/intranetdata/file/digital_channels/2014/159/original/MTU5XzE0MTM3ODgyODBfMQ==/MjYyXzE0MTQwNTM1MjJfMQ==.wmv' -sameq -acodec libfaac -ar 22050 -f mp4 -vcodec libx264 -s 640x480 -aspect 4:3 'home2/web/eclass40/intranetdata/file/digital_channels/2014/159/photo/MTU5XzE0MTM3ODgyODBfMQ==/MjU5XzE0MTQwNDQyMzdfMQ==.mp4'

		# Set Thumbnail Size
		if(file_exists($file_path.$thumbnail_url)){
			list($width, $height, $img_type) = getimagesize($file_path.$thumbnail_url);
			$image = imagecreatefromjpeg($file_path.$thumbnail_url);
			$thumbnail = libdigitalchannels::getResizedImage($image, $width, $height, libdigitalchannels::$thumbnail_max_size);
		    imagejpeg($thumbnail, $file_path.$thumbnail_url, 100);
		    imagedestroy($thumbnail);
		    imagedestroy($image);
		}

		# Remove video in uploaded folder if not keep original photo
		if($settings['KeepOriginalPhoto'] !== '1'){
			unlink($original_url);
		}
		
		$kis_data['thumbnail'] = libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo);
	    $kis_data['photo_id']  = $photo_id;
	}
	else{
		if($img_path && $imgFormatNotMatch){
		    //display the file not match function
		    $kis_data['error'] = $Lang['DigitalChannels']['Organize']['UnsupportedFormat'].'!';
		}
		else{
			//display the file too large function
		    $kis_data['error'] = $Lang['DigitalChannels']['Organize']['FileSizeTooLarge'].'!';
		}
	}
	
	echo json_encode_cust($kis_data);
	
    break;

    case 'updatephotodescription':
	
	//if ($libkis_album->isAlbumEditable()){
        $ldc->updateAlbumPhoto($photo_id, array('description'=>standardizeFormPostValue($_POST['description'])));
	//}
	
    break;

    case 'updatePhotoChiDescription':
        $ldc->updateAlbumPhoto($photo_id, array('chiDescription'=>standardizeFormPostValue($_POST['chiDescription'])));
        break;
        
        
    case 'updateEventTitle':
        $ldc->updateAlbumPhoto($photo_id, array('eventTitle'=>standardizeFormPostValue($_POST['eventTitle'])));
        break;

    case 'updateChiEventTitle':
        $ldc->updateAlbumPhoto($photo_id, array('chiEventTitle'=>standardizeFormPostValue($_POST['chiEventTitle'])));
        break;
        
    case 'updateEventDate':
        $ldc->updateAlbumPhoto($photo_id, array('EventDate'=>$_POST['eventDate']));
        break;
        
    case 'reorderphotos':
	
	//if ($libkis_album->isAlbumEditable()){
	    $ldc->reorderAlbumPhotos($photo_ids,$_POST['album_id']);
	//}
	
    break;

    case 'updatecoverphoto':
	
	//if ($libkis_album->isAlbumEditable()){
	    $ldc->updateAlbumCoverPhoto($photo_id, $album_id);
	//}
	
    break;

    case 'removephoto':
	
	$album = $ldc->getAlbum($album_id);
	$photo = $ldc->getAlbumPhoto($photo_id);
	$vod = $ldc->hasVod($photo_id);

	if ($ldc->removeAlbumPhoto($photo_id)){
		
	    # Remove photo / video
	    unlink($file_path.libdigitalchannels::getPhotoFileName('photo', $album, $photo));  
//	    $o_ext = pathinfo($photo['title'], PATHINFO_EXTENSION);
//		if(!in_array(strtoupper($o_ext),libdigitalchannels::$videoFormat)){
		# Remove Thumbnail if not default photo
		$thumbnail_url = trim(libdigitalchannels::getPhotoFileName('thumbnail', $album, $photo));
		if ($thumbnail_url != '/images/2009a/icon_files/avi_l.png'){
	    	unlink($file_path.$thumbnail_url);
		}
		# Remove original photo / video
	    unlink($file_path.libdigitalchannels::getPhotoFileName('original', $album, $photo));

		#### Delete Vod START ####
		$libvod = new libvod();
		if($libvod->enabled() && $vod){
			$vod_info = $libvod->videoInfo($vod);
			$libvod->deleteVideo($vod_info[1]);
		}
		#### Delete Vod END ####
	}
	
    break;

//    case 'removealbum':
//
//	if ($libkis_album->isAlbumEditable()){
//	    
//	    $album = $libkis_album->getAlbum();
//	    $photos = $libkis_album->getAlbumPhotos();
//	    
//	    foreach ($photos as $photo){
//		
//		unlink($file_path.kis_album::getPhotoFileName('photo', $album, $photo));
//		unlink($file_path.kis_album::getPhotoFileName('thumbnail', $album, $photo));
//		unlink($file_path.kis_album::getPhotoFileName('original', $album, $photo));
//		
//	    }
//	    
//	    rmdir($file_path.kis_album::getAlbumFileName('photo', $album));
//	    rmdir($file_path.kis_album::getAlbumFileName('thumbnail', $album));
//	    rmdir($file_path.kis_album::getAlbumFileName('original', $album));
//	    
//	    $libkis_album->removeAlbum();
//	    
//	}
//	
//    break;
		
	case 'enjoyphoto':
		$ldc->Update_Album_Photo_Favorites($PhotoID);
		echo count($ldc->Get_Album_Photo_Favorites($PhotoID));
	break;
	
	case 'getEnjoyUsers':
		include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
		$ldcUI = new libdigitalchannels_ui();
		
		if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
			header("Location: ../index.php");
			exit;
		}
		
//		if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
//			header("Location: ../index.php");
//			exit;
//		}
	
		$photoEnjoyUserArray = $ldc->Get_Album_Photo_Favorites_User($AlbumPhotoID);
		
		if(count($photoEnjoyUserArray) > 0){
			$x.='<div class="comment_content">';
			
			foreach($photoEnjoyUserArray as $aEnjoyUser){
				$x.='<a href="#" >'.$aEnjoyUser['UserName'].'</a>, ';
			}
			$x = rtrim($x, ", ");
			$x.=' '.$Lang['DigitalChannels']['Organize']['EnjoyThis'].'</div>';
		}
		
		echo $x;
		
	break;
	
	case 'getComments':
		include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
		$ldcUI = new libdigitalchannels_ui();
		
		if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
			header("Location: ../index.php");
			exit;
		}
		
//		if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
//			header("Location: ../index.php");
//			exit;
//		}
		
		$photoCommentArray = $ldc->Get_Album_Photo_Comment($AlbumPhotoID);
		
		$numOfHide = count($photoCommentArray) - 10;
		if($numOfHide > 0){
			$x.='<div class="previous_content">
					<a href="javascript: void(0);" onclick="$(\'.hidden_comment\').show();$(\'.previous_content\').hide();" >'.sprintf($Lang['DigitalChannels']['Organize']['PreviousComments'],$numOfHide).'</a>
				</div>';
		}
		
		$x.='<ul class="comment_list">';
		
		$count = 0;
		foreach($photoCommentArray as $aComment){
			$hide_css = '';
			if($count < $numOfHide){
				$hide_css = 'style="display:none" class="hidden_comment"';
			}
			$x.='<li '.$hide_css.' onmouseover="MM_showHideLayers(\'comment_edit'.$aComment['RecordID'].'\',\'\',\'show\')"  onmouseout="MM_showHideLayers(\'comment_edit'.$aComment['RecordID'].'\',\'\',\'hide\')"> 
				<div class="comment_user"><img style="width:32px" src="'.$aComment['PersonalPhotoLink'].'" alt="" /></div>
				<div class="comment_info">
					<div class="comment_name"><a href="#">'.$aComment['UserName'].'</a><span class="comment_time" title="'.$aComment['DateInput'].'">'.$ldcUI->time_passed(strtotime($aComment['DateInput'])).'</span> '.($aComment['DateModified'] > 0?'<span class="comment_time" title="'.$aComment['DateModified'].'">'.$Lang['DigitalChannels']['Organize']['Edited'].'</span>':'').'</div>
					<div class="comment_meta" id="comment_edit'.$aComment['RecordID'].'" style="visibility:hidden"> '.($aComment['UserID'] == $UserID?'<a href="javascript:void(0);" onclick="editCommentClick('.$aComment['RecordID'].')" class="icon_edit"></a>':'').' '.($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']?'<a href="javascript:void(0);" onclick="deleteComments('.$aComment['RecordID'].')" class="icon_delete"></a>':'').'</div>
					<div class="comment_text" id="comment_text'.$aComment['RecordID'].'">'.$aComment['Content'].'</div>
				</div>
			</li>';
			$count++;
		}
		
		$x.='</ul>';
		
		echo $x;
//		$offset = $offset? $offset: 0;
//		$amount = $offset==0?10:5;
//		
//		$reviews_data=$lelibplus->getBookReviews($offset, $amount);
//		
//		include_once("templates/detail_reviews.php");
		
	break;
	
	case 'addComments':
		if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
			header("Location: ../index.php");
			exit;
		}
		
		if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
			header("Location: ../index.php");
			exit;
		}
		
		$result = false;
		
		if($hidPhotoID && $txtComment !=''){
			$result = $ldc->Add_Album_Photo_Comment($hidPhotoID, nl2br($txtComment));
		}
		
		echo $result;

	break;
	
	case 'deleleComments':
		if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
			header("Location: ../index.php");
			exit;
		}
		
		if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
			header("Location: ../index.php");
			exit;
		}
		
		if (!$AlbumID && !$ldc->isAlbumNewable()){
			header("Location: ../index.php");
			exit;
		}
		
		$result = false;
		
		if($CommentID !=''){
			$result = $ldc->Delete_Album_Photo_Comment($CommentID);
		}
		
		echo $result;

	break;
	
	case 'editComments':
		if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
			header("Location: ../index.php");
			exit;
		}
		
		if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
			header("Location: ../index.php");
			exit;
		}
		
		$result = false;
		
		if($CommentID !='' && $txtEditComment !=''){
			$result = $ldc->Update_Album_Photo_Comment($CommentID, nl2br($txtEditComment));
		}
		
		echo $result;

	break;
	
	case 'updateViewFavCommentsTotal':
		if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
			header("Location: ../index.php");
			exit;
		}
		
		if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
			header("Location: ../index.php");
			exit;
		}
		
		//$result = false;
		
		if($hidPhotoID !=''){
			$result['NumComment'] = $ldc->Get_Album_Photo_Comment_Total($hidPhotoID);
			$result['NumFav'] = $ldc->Get_Album_Photo_Favorites_Total($hidPhotoID);
			$result['NumView'] = $ldc->Get_Album_Photo_View_Total($hidPhotoID);
		}
		
		echo json_encode_cust($result);

	break;
}

function json_encode_cust($a=false)
 {
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a))
    {
      if (is_float($a))
      {
        // Always use "." for floats.
        return '"' .floatval(str_replace(",", ".", strval($a))). '"';
      }

      if (is_string($a))
      {
        static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
      }
      else
        return '"' .$a. '"';
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a)) 
 	{       
 		if (key($a) !== $i){  
			$isList = false; 
			break;       
		}     
 	}     
 	
 	$result = array();     
 	if ($isList){
		foreach ($a as $v) 
			$result[] = json_encode_cust($v);      
		return '[' . join(',', $result) . ']';     
	}else{ 
		foreach ($a as $k => $v) 
			$result[] = json_encode_cust($k).':'.json_encode_cust($v);
			
      return '{' . join(',', $result) . '}';
    }
  }

?>
