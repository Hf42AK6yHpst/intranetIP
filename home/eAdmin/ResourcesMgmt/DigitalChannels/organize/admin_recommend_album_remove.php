<?php
// Editing by 
/*
 * 2015-03-03 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

//debug_pr($_REQUEST);

if (!$RecommendID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

if($RecommendID){
	$result = $ldc->Remove_Recommend($RecommendID);
}

$msg = ($result > 0? "UpdateSuccess" : "UpdateUnsuccess");

intranet_closedb();
header("Location: admin_recommend_album.php?&msg=$msg");
?>