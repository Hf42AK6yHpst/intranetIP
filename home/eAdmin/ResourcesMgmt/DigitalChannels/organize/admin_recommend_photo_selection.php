<?php
// Editing by Pun
/*
 * Change Log:
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2015-03-04 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

//$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();
$ldc = new libdigitalchannels();

//echo $ldcUI->Include_JS_CSS();

header('Content-type: text/plain; charset=utf-8');
?>
<!--<script type="text/javascript" src="recommend_photo_selection/js/jquery-1.11.2.min.js"></script>-->
<script type="text/javascript" src="recommend_photo_selection/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="recommend_photo_selection/js/jquery.scrollTo.min.js"></script>

<style>
.btn_area{
	z-index: 5;
	/*margin-left: -7px;
	width: 99%;*/
	bottom: -135px;
	position: relative;
	background: #e4e4e4;
	text-align: center;
	padding: 5px 5px 5px 0;
	width: 825px;
	left: -15px;
}

.photo_thumb_current{
	border: 4px solid #206EFF;
	margin-left: -4px;
}

.btn_feature{
	height: 35px; 
	width: 35px;
	background-image: url(../../../../../images/2009a/digital_channels/tool_icon.png);
	background-repeat: no-repeat;
	background-position: 0px -980px;
	z-index: 5;
	display: inline-table;
	margin-bottom: -12px;
	margin-top: -12px;
}
</style>
<script>
var just_wording = '<?=$Lang['DigitalChannels']['Recommend']['just']?>';
var a_few_seconds_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['a_few_seconds_ago']?>';
var a_minute_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['a_minute_ago']?>';
var minutes_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['minutes_ago']?>';
var hour_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['1_hour_ago']?>';
var hours_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['hours_ago']?>';
var day_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['day_ago']?>';
var year_ago_wording = '<?=$Lang['DigitalChannels']['Recommend']['year_ago']?>';

var preview_inaction = false;
var current_video_id = -1;
var preview_video_id = -1;
var selected_photo_array = [];
var currentAlbumID = 0;

$(document).ready(function(){
});
	get_album();
	
	function search_photo(keyword){
		if($("#video_container").css('display') == 'none'){
			get_album(keyword);
		}
		else{
			get_video(keyword, currentAlbumID);
		}
	}
	
	function get_album(search){
		$('#nav_box').html("<?=$Lang['DigitalChannels']['General']['Album2']?> > " + "<a href='javascript:get_album();'>"+$("#CategoryCode option:selected").text()) + "</a>";
		
		$('#video_container').hide();
		$('#album_container').show().html("Loading...");
		$.ajax({
		    url: 'ajax_get_photo.php',
		    type: 'POST',    
		    data: {
		    	"action" : "get_album",
		    	"keyword" : search,
		    	"orderby" : $("#orderby").val()
		    },
		    error: function(xhr) {
		      alert('Ajax request error');
		    },
		    success: function(response) {	    	
		    	var unpackArr = jQuery.parseJSON( response );
		    	var html_test='';
		    	
		    	html_test+='<ul class="album_list">';
		    	
		    	for(var i = 0; i<unpackArr.length; i++){
		    		if(unpackArr[i]["CategoryCode"] != $("#CategoryCode").val()){
		    			continue;
		    		}
		    		if(unpackArr[i]["Title"] == ""){
		    			unpackArr[i]["Title"] = '<?=$Lang['DigitalChannels']['Organize']['UntitledAlbum']?>';
		    		}
		    		
		    		var d = new Date(unpackArr[i]["DateModified"].replace(" ", "T"));
		    		//alert(unpackArr[i]["DateModified"]);
		    	    var n = d.getTime();
		    	    unpackArr[i]["DateModified"] = time_diff(n);
		    	    
		     		html_test+='<li id="'+unpackArr[i]["AlbumID"]+'" title="'+unpackArr[i]["Title"]+'" class="album" style="zoom: 90%;">';
		     			html_test+='<a href="javascript:get_video(\'\','+unpackArr[i]["AlbumID"]+');">';
		     			html_test+='<span class="photo_pic">';
		     			if(unpackArr[i]["cover_photo_path"])
							html_test+='<img src="'+unpackArr[i]["cover_photo_path"]+'">';
						html_test+='</span>';
						html_test+='<div>'+unpackArr[i]["Title"];
							html_test+='<span class="photo_number">('+unpackArr[i]["num_of_video"]+')</span>';
	                  		//html_test+='<span class="date_taken photo_date">'+unpackArr[i]["DateTaken"]+'</span>';     
	                  		html_test+='<span class="last_modify photo_date">'+unpackArr[i]["DateModified"]+'</span>';                           
	                  	html_test+='</div>';
	                 	html_test+='</a>';                    
	                html_test+='</li>';
		    	}
		    	
                html_test+='</ul>';
                
                $('#album_container').html(html_test);

		    }
	  });
	}
	
//	function get_video(search,albumid){
//		alert("sdf");
//		$('#album_container').hide();
//		$('#video_container').show().html('Loading...');
//		$.ajax({
//		    url: 'ajax_get_photo.php',
//		    type: 'POST',    
//		    data: {
//		    	"action" : "get_video",
//		    	"keyword" : search,
//		    	"orderby" : $("#orderby").val(),
//		    	"albumid" : albumid
//		    },
//		    error: function(xhr) {
//		      alert('Ajax request error');
//		    },
//		    success: function(response) {	    	
//		    	var unpackArr = jQuery.parseJSON(response);
//		    	var html_test='';
//		    	
//		    	html_test+='<div id="photo_thumb_list" class="photo_thumb_list" style="height: 100%;overflow-y: scroll">';
//	     			html_test+='<ul>';
//	 
//	     			for(var i=0; i<unpackArr.length; i++){
//	     				html_test+='<li id="video_'+unpackArr[i]["id"]+'">';
//						    html_test+='<div href="javascript:void(0);" class="photo_thumb_block photo_thumb_effect">';
//				               html_test+='<div class="photo_thumb_details">';				               	
//					               html_test+='<div class="photo_thumb_overlay" style="left: 0px; bottom: 0px;">';					                
//						                html_test+='<div class="title" style="width: 170px;">';			            
//						                	html_test+='<a>{{video.title}}</a>';
//						                	html_test+='<a href="javascript:void(0);"  class="btn_feature" title="Preview" value="'+unpackArr[i]["id"]+'"></a>';	    
//						                html_test+='</div>';
//					                html_test+='</div>';
//				               html_test+='</div>';
//				                    
//				    	 		html_test+='<div onclick="select_photo('+unpackArr[i]["id"]+')" class="photo_thumb_images photo_thumb_info" style="display:block" id="'+unpackArr[i]["id"]+'">';
//					     			html_test+='<a href="javascript:void(0);">';				     			
//					     				html_test+='<img src="'+unpackArr[i]["FilePath"]+'" title="'+unpackArr[i]["title"]+'">';
//					     			html_test+='</a>';
//				     			html_test+='</div>';
//				             html_test+='</div>';
//						     html_test+='<div class="clearfix"></div>';  
//						html_test+='</li>';
//	     			}
//	     			
//	     			html_test+='</ul>';
//	     		html_test+='</div>';
//	     		
//	     		
//	     		$('#video_container').html(html_test);
//		    }
//	  });
//	}
	
	$('#CategoryCode').off('change').on('change', function(){
		//current_cat = $(this).val();
		get_album();
	});
	
	$(".album").off('click tap').on('click tap', function(){	
		alert("hihi");
	});
	
//	$('#orderby').off('change').on('change', function(){
//		get();
//	});
	
//	$('.navi').off('click tap').on('click tap', function(){	
//		
//		var current = this;		
//		if($(this).attr("folder") == ""){			
//			get_album();
//		}
//		
//	});
	
	function time_diff(unix){
		var d = new Date();
		var now = d.getTime();
	    var difference = (now - unix)/1000;
	    var message = "";
	        
	    if (difference < 10)
	    {
	        message = just_wording;
	    }
	    else if (difference < 60)
	    {
	        message = a_few_seconds_ago_wording;
	    }
	    else if (difference < 60 * 2) {
	        message = a_minute_ago_wording;
	    }
	    else if (difference < 60 * 60) {
	        message = parseInt(difference / 60) + " " + minutes_ago_wording;
	    }
	    else  if (difference < 60 * 60 * 2) {
	    	message = hour_ago_wording;
	    }
	    else  if (difference < 60 * 60 * 24) {
	    	message = parseInt(difference / 60 / 60 ) + " " + hours_ago_wording;
	    }else if (difference < 60 * 60 * 24 * 365){
	    	message = parseInt(difference / 60 / 60 /24) + " " + day_ago_wording;
	    }else{
	    	message = parseInt(difference / 60 / 60 /24 /365) + " " + year_ago_wording;
	    }
	    
	    return message;
	}


function get_video(search,albumid){
		currentAlbumID = albumid;
		$('#album_container').hide();
		$('#video_container').show();
		$('#photo_thumb_list').html('Loading...');
		$.ajax({
		    url: 'ajax_get_photo.php',
		    type: 'POST',    
		    data: {
		    	"action" : "get_photo_video",
		    	"keyword" : search,
		    	"orderby" : $("#orderby").val(),
		    	"albumid" : albumid
		    },
		    error: function(xhr) {
		      alert('Ajax request error');
		    },
		    success: function(response) {
		    	$('#nav_box').html("<?=$Lang['DigitalChannels']['General']['Album2']?> > " + "<a href='javascript:get_album();'>"+$("#CategoryCode option:selected").text() + "</a> > "
		    	 + $("#"+albumid).attr('title'));
		    	 	
		    	var unpackArr = jQuery.parseJSON(response);
		    	var html_test='';
		    	
	     			html_test+='<ul>';
	 
	     			for(var i=0; i<unpackArr.length; i++){
	     				html_test+='<li id="video_'+unpackArr[i]["id"]+'">';
	     				
	     				var isSelected = false;
	     				for(var j in selected_photo_array) {
					        if(selected_photo_array[j] == unpackArr[i]["id"]){
					        	isSelected = true;
					        	break;
					        }
					    }
	     				
						    html_test+='<div href="javascript:void(0);" class="photo_thumb_block photo_thumb_effect '+(isSelected?"photo_thumb_current":"")+'">';
				               html_test+='<div class="photo_thumb_details">';				               	
					               html_test+='<div class="photo_thumb_overlay" style="left: 0px; bottom: 0px;">';					                
						                html_test+='<div class="title" style="width: 170px;">';			            
						                	html_test+=unpackArr[i]["title"];
						                	html_test+='<a href="javascript:get_preview('+unpackArr[i]["id"]+', \''+unpackArr[i]["OriginalFilePath"]+'\', \''+unpackArr[i]["type"]+'\');"  class="btn_feature" title="Preview" value="'+unpackArr[i]["id"]+'"></a>';	    
						                html_test+='</div>';
					                html_test+='</div>';
				               html_test+='</div>';
				                    
				    	 		html_test+='<div onclick="select_photo('+unpackArr[i]["id"]+')" class="photo_thumb_images photo_thumb_info" style="display:block" id="'+unpackArr[i]["id"]+'">';
					     			html_test+='<a href="javascript:void(0);">';				     			
					     				html_test+='<img src="'+unpackArr[i]["FilePath"]+'" title="'+unpackArr[i]["title"]+'">';
					     			html_test+='</a>';
				     			html_test+='</div>';
				             html_test+='</div>';
						     html_test+='<div class="clearfix"></div>';  
						html_test+='</li>';
	     			}
	     			
	     			html_test+='</ul>';
	     		
	     		
	     		
	     		$('#photo_thumb_list').html(html_test);
		    }
	  });
	}
	
//$('.btn_feature').off('click tap').on('click tap', function(){
function get_preview(current_id, OriginalFilePath, type){
	
		if(preview_inaction){
			event.preventDefault();
		}
		preview_inaction = true;
		//alert("fdfd");
		$('.btn_feature_current').toggleClass("btn_feature_current");
		//alert(thisObj);
		//var current_id = thisObj.attr("value");
		//alert(thisObj);
		var current_offsetTop = $("#video_"+current_id)[0]["offsetTop"];
		var current = $("#video_"+current_id);
		var i = 0, isLastRow = false;
		console.log("A");
		var currentElementSibling =  $("#video_"+current_id);
		var lastElement = "";
		while(true){
			i++;
			nextElementSibling = $(currentElementSibling[0]["nextElementSibling"]);
			if(nextElementSibling[0] == undefined){
				lastElement = currentElementSibling;
				isLastRow = true;
				break;
			}
			var nextOffsetTop = nextElementSibling[0]["offsetTop"];
			if(nextOffsetTop != current_offsetTop){
				break;
			}else{
				currentElementSibling = nextElementSibling;
			}
			if(i>3)break;
		}
		i = 0;
		if(isLastRow){
			while(true){
				i++;
				previousElementSibling = $(currentElementSibling[0]["previousElementSibling"]);
				if(previousElementSibling[0] == undefined){
					currentElementSibling = lastElement;
					break;
				}
				
				var previousOffsetTop = previousElementSibling[0]["offsetTop"];
				currentElementSibling = previousElementSibling;
				if(previousOffsetTop != current_offsetTop || i>3){
					break;
				}
			}
		}
		
		$("#photo_thumb_list > ul > .preview_video").remove();
		
		if(preview_video_id == current_id){
			preview_inaction = false;
			preview_video_id = -1;
			event.preventDefault();
		}else{
			$("#"+current_id).toggleClass("btn_feature_current");
		}
		var clone_preview_video = $(".preview_video").clone();
		currentElementSibling.after(clone_preview_video);		
		var current_preview = $(currentElementSibling[0]["nextElementSibling"]);
		
		preview_video_id = current_id;
		
//		$("#photo_thumb_list").scrollTo( clone_preview_video ,800, {offset: function() { return {top:-100}; }} );
		console.log("A");
		setTimeout(function(){
			$("#photo_thumb_list").scrollTo( clone_preview_video ,{  offset: -50, duration: 1000});
			console.log("A");
			current_preview.toggleClass("current_preview_video");		
//			var OriginalFilePath = "";
//			for(var j =0; j < digich_scope.videoarr.length; j++){
//				if(current_id == digich_scope.videoarr[j]["id"]){
//					OriginalFilePath = digich_scope.videoarr[j]["OriginalFilePath"];
//					break;
//				}
//			}		    	
	    	console.log(OriginalFilePath);
			current_preview.animate({
	 		   "height" : "284px",
	 		   "opacity" : 1
			  }, {
			    duration: 500,
			    specialEasing: {
			      height: "easeInQuart"
			    },
			    complete: function() {
					preview_inaction = false;
					if(type == 'Photo'){
						$(".current_preview_video").html('<img src="'+OriginalFilePath+'" style="max-width:573px; max-height:284px;" />');
					}
					else{
//						$(".current_preview_video").html('<video style="height: 100%; z-index: 1;" poster="http://192.168.0.146:31002/" controls="" autoplay="autoplay">'+
//						    '<source type="video/mp4" src="http://192.168.0.146:31002/">'+					   
//						    'Your browser does not support HTML5 video.'+
//						'</video>');
				    	$(".current_preview_video video source").attr("src", OriginalFilePath);
				    	$(".current_preview_video video")[0].load();
			    	}
			    }
			  });
		},100);
}
	//});
	
function select_photo(current_id){
	//alert("sdfsd");
	$('#video_'+current_id+' div.photo_thumb_block').toggleClass("photo_thumb_current");
	if($('#video_'+current_id+' div.photo_thumb_current').length > 0){
		//$('#num_selected_video').html(parseInt($('#num_selected_video').html()) + 1);
		selected_photo_array[selected_photo_array.length] = current_id;
	}
	else{
		//$('#num_selected_video').html(parseInt($('#num_selected_video').html()) - 1);
		for(var i = 0; i < selected_photo_array.length; i++) {
          if(selected_photo_array[i] === current_id) {
              selected_photo_array.splice(i, 1);
          }
      }
	}
	$('#num_selected_video').html(selected_photo_array.length);
}
	
$('.photo_thumb_info').off('click tap').on('click tap', function(){
		alert('222');
		var current = this;		
//		$(".photo_thumb_block").removeClass("photo_thumb_current", 200, "easeInBack", function(){});
		if(is_edit){
			digich_scope.selected_video_arr = [];
			
			for(var j =0; j < digich_scope.videoarr.length; j++){
				if($(current).attr("id") == digich_scope.videoarr[j]["id"]){
					digich_scope.$apply(function(){	    	//video_title
						var current_video_obj = digich_scope.videoarr[j];
						current_video_obj["temp_index"] = j;
			    		digich_scope.selected_video_arr.push(digich_scope.videoarr[j]);
			    		digich_scope.videoarr[j]["current"] = "photo_thumb_current";
			    	});
				}else{
					digich_scope.$apply(function(){	
						digich_scope.videoarr[j]["current"] = "";
					});
				}
			}			
		}else{
			if($(current).parent().hasClass("photo_thumb_current")){
				for(var j =0; j < digich_scope.selected_video_arr.length; j++){
					if($(current).attr("id") == digich_scope.selected_video_arr[j]["id"]){					
						digich_scope.$apply(function(){	    	//video_title
							var temp_index = digich_scope.selected_video_arr[j]["temp_index"];
				    		digich_scope.videoarr[temp_index]["current"] = "";
				    		digich_scope.selected_video_arr.splice(j, 1);
				    	});
					}
				}				
			}else{
				for(var j =0; j < digich_scope.videoarr.length; j++){
					if($(current).attr("id") == digich_scope.videoarr[j]["id"]){
						digich_scope.$apply(function(){	    	//video_title
							var current_video_obj = digich_scope.videoarr[j];
							current_video_obj["temp_index"] = j;
				    		digich_scope.selected_video_arr.push(digich_scope.videoarr[j]);
				    		digich_scope.videoarr[j]["current"] = "photo_thumb_current";
				    	});
					}
				}
			}		
		}
		
//		$(current).toggleClass("photo_thumb_current", 500, "easeInBack");
		current_video_id = $(this).attr("id");
	});
	
function add_photo(){
		if(selected_photo_array.length <= 0){
			return false;
		}
		$('#NeedUpdatePhoto').val('1');
		//add ajax to update the selected photo selected_photo_array
		$.ajax({
		    url: 'ajax_get_photo.php',
		    type: 'POST',    
		    data: {
		    	"action" : "get_applied_photo",
		    	"photo_id" : selected_photo_array.join(',')
		    },
		    error: function(xhr) {
		      alert('Ajax request error');
		    },
		    success: function(response) {    	
		    	var unpackArr = jQuery.parseJSON( response );
		    	
		    	var html_test='';
		    	
		    	for(var i = 0; i<unpackArr.length; i++){
		    	    
		     		html_test+='<li class="uploaded">';
								html_test+='<span title="'+unpackArr[i]["title"]+'" style="background-image:url('+unpackArr[i]["FilePath"]+')"/></span>';
								html_test+='<p class="spacer"></p>';
							    
								html_test+='<textarea maxlength="255" readonly placeholder="<?=$Lang['DigitalChannels']['Organize']['DescriptionHere']?>..." name="" class="input_desc" wrap="virtual" rows="2">'+unpackArr[i]["title"]+'</textarea>';
								html_test+='<input type="hidden" name="photo_ids[]" class="photo_ids" value="'+unpackArr[i]["id"]+'"/>';
								html_test+='<input type="hidden" class="date_taken" value="'+unpackArr[i]["date_taken"]+'"/>';
								html_test+='<input type="hidden" class="date_uploaded" value="'+unpackArr[i]["date_uploaded"]+'"/>';
								html_test+='<input type="hidden" class="title" value="'+unpackArr[i]["title"]+'"/>';
								html_test+='<p class="spacer"></p>';
								html_test+='<div class="table_row_tool">';
						
								    html_test+='<a href="#" class="copy_dim" title="<?=$Lang['DigitalChannels']['Organize']['SetAsCoverPhoto']?>"></a>';
								    html_test+='<a href="#" class="delete_dim" title="<?=$Lang['DigitalChannels']['Organize']['RemoveFile']?>"></a>';
								html_test+='</div>';
							    html_test+='</li>';
		    	}
                
                $('#selected_photo_list').html($('#selected_photo_list').html()+html_test);
                
				//$('#selected_photo_list').html('<li class="uploaded">testing</li>');
				//$('#selected_photo_list').html('<li class="uploaded"><span title="" style="background-image:url()"/></span><p class="spacer"></p><textarea maxlength="255" placeholder="..." name="descriptions[]" class="input_desc" wrap="virtual" rows="2"></textarea><input type="hidden" name="photo_ids[]" class="photo_ids" value=""/><input type="hidden" class="date_taken" value=""/><input type="hidden" class="date_uploaded" value=""/><input type="hidden" class="title" value=""/><p class="spacer"></p><div class="table_row_tool"><a href="#" class="delete_dim" title=""></a></div></li>');
		    }
	  });
		tb_remove();
}
</script> 
<div id="fileupload_content_dialog" class="dialog_content_fileupload" style="height: 435px;width: 100%;">		
  <!--<div style="float:left;width:20%;">
    <div style="height:50px;border-bottom:1px solid #e5e5e5;vertical-align:middle;text-align:center;line-height:50px;">All videos
</div>
<div style="height:50px;border-bottom:1px solid #e5e5e5;vertical-align:middle;text-align:center;line-height:50px;border-right:3px solid #206EEF;color:#206EEF" valign="middle">Album</div><div style="height:50px;border-bottom:1px solid #e5e5e5;vertical-align:middle;text-align:center;line-height:50px;">Favorites
</div>
  
  </div>-->

  <div style="float:left;width:100%;height:100%;border-left:1px solid #e5e5e5">
  <div id="nav_box" style="padding-left:10px;float:left;vertical-align:middle;line-height:30px;">&nbsp;<?=$Lang['DigitalChannels']['General']['Album2']?><!--[Place the "Album Navigation Bar" here]--></div>
  <div style="float:right;text-align:right;">
  <form>
    <input id="search_box" type="text" size="40" style="border: 1px solid #e5e5e5; line-height:24px;" value="">
  	<input value="<?=$Lang['DigitalChannels']['General']['Search']?>" type="submit" onclick="search_photo($('#search_box').val());return false;" style="height:28px;"> 
  </form>
  </div>
  <br style="clear:both;">
  <div style="padding:10px">
  <div style="border-bottom:1px solid #e5e5e5;height:30px;">
  <div style="float:left;"> <!--[Place the pull down here]-->
 
  	<div id="cat_box" style="float:left;">
  		<?=$Lang['DigitalChannels']['Recommend']['NumberOfSelect']?>: <span id="num_selected_video">0</span> |  
  		<?=$Lang['DigitalChannels']['General']['Category']?>: 
  		<?=$ldcUI->Get_Category_Selection($CategoryCode)?>
  	</div>  
  </div>
  <div style="float:right">
	<?=$Lang['DigitalChannels']['Recommend']['SortBy']?> : <select id="orderby" onchange="search_photo($('#search_box').val())">
	  <option value="Title"><?=$Lang['DigitalChannels']['Recommend']['RecommendTitle']?></option>
	  <option value="LastModified"><?=$Lang['DigitalChannels']['Recommend']['LastModified']?></option>
	  <option value="DateTaken"><?=$Lang['DigitalChannels']['Recommend']['DateUploaded']?></option>
	</select>
	<!--[Place the "Sort by" pull down here]-->
  </div>
</div>
<div class="template" style="display:none">
		     	<ul>
		     		<li class="preview_video" style="width: 573px;margin-left: 10px;opacity:0;height:0px;transform: translate3d(0px, 0px, 0px);background: #222;text-align: center;">
	 					<video style="height: 100%; z-index: 1;" poster="" controls="" autoplay="autoplay">
						    <source type="video/mp4" src="">					   
						    Your browser does not support HTML5 video.
						</video>
	 				</li>
		     	</ul>
	     	</div>
<div id="video_container" style="padding:10px;display:none;"><div id="photo_thumb_list" class="photo_thumb_list" style="width:100%;height: 480px;overflow-y: scroll">
</div><!--[Pleace the thumbnail of video here - suggested 3 records in each row]--></div>
<div id="album_container" class="container album_list" style="padding:10px;width:100%;height: 480px;overflow-y: scroll;"></div>
</div>
</div>
</div>
<div class="btn_area">
	<span>
		<input value="<?=$Lang['DigitalChannels']['General']['Add']?>" class="formsubbuttonon" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'"  id="retrieve_path_btn"  type="submit" onclick="add_photo();" >
	</input>
	</span>
</div>
<?php
intranet_closedb();
?>