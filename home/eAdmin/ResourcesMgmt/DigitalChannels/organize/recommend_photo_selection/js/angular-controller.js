var app = angular.module("Chatroom", ['ngTouch']);

app.controller("chatboxCtrl", function($scope) {
    $scope.msg_time = ""; 
});


function time_diff(unix){
	var now = getTime();
    var difference = now - unix;
    var message = "";
    
    if (difference < 10)
    {
        message = 'just';
    }
    else if (difference < 60)
    {
        message = 'a few seconds ago';
    }
    else if (difference < 60 * 2) {
        message = 'a minute ago';
    }
    else if (difference < 60 * 60) {
        message = parseInt(difference / 60) + ' minutes ago';
    }
    else  if (difference < 60 * 60 * 2) {
    	message = ' 1 hour ago';
    }
    else  if (difference < 60 * 60 * 12) {
    	message = parseInt(difference / 60 / 60 ) +' hours ago';
    }else{
    	message = ' test ';
    }
    
    return message;
}


window.setInterval(function(){    
    var chatbox_scope = angular.element($("#chatbox")).scope();
    
    chatbox_scope.$apply(function(){
    	for(var i =0; i < chatbox_scope.msgarr.length; i++){
    		chatbox_scope.msgarr[i]["msg_time"] = time_diff(chatbox_scope.msgarr[i]["time"]);
    	}
    });
    
    return false;
}, 60);