<?php
// Editing by 
/*
 * 2015-04-22 (Henry): rename json_encode to json_encode_cust
 * 2015-01-26 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldc = new libdigitalchannels();

switch ($action) {
	
    case "get_album": //get the album list that user can read ($keyword, $orderby(Title|LastModified|DateTaken) for searching)
    	if($orderby == 'Title'){
    		$orderby = 'Title, Description, DateModified desc, DateInput desc';
    	}
    	else if($orderby == 'LastModified'){
    		$orderby = 'DateModified desc, DateInput desc';
    	}
    	else if($orderby == 'DateTaken'){
    		$orderby = 'DateTaken desc, DateModified desc, DateInput desc';
    	}
    	
        $albumInfoAry = $ldc->Get_Album_Search('',array('keyword'=>$keyword, 'orderby'=>$orderby), 0);
		$tempAlbumInfoAry = array();
		foreach($albumInfoAry as $aAlbumInfo){
			if(!$ldc->isAlbumReadable($aAlbumInfo['AlbumID'])){
        		continue;
        	}
        	
        	$ablumPhotoInfoAry = $ldc->Get_Album_Photo('',$aAlbumInfo['AlbumID']);
			
			$album = array(
			    "id" => $aAlbumInfo['AlbumID'],
			    "input_date" => strtotime($aAlbumInfo['DateInput']),
			    "input_by" => $aAlbumInfo['InputBy']
			);
			
			//get cover photo info
			$coverPhotoInfo = current($ablumPhotoInfoAry);
			
			$numOfPhoto=0;
			foreach($ablumPhotoInfoAry as $aPhoto){
				$o_ext = pathinfo($aPhoto['title'], PATHINFO_EXTENSION);
				//if(in_array(strtoupper($o_ext),libdigitalchannels::$videoFormat)){
					$numOfPhoto++;
				//}
				if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
					$coverPhotoInfo = $aPhoto;
				}
			}
			
			$aAlbumInfo['num_of_video'] = $numOfPhoto;
			$aAlbumInfo['cover_photo_path'] = $coverPhotoInfo?libdigitalchannels::getPhotoFileName('thumbnail', $album, $coverPhotoInfo):'';
			
			//if($numOfPhoto > 0)
        		$tempAlbumInfoAry[] = $aAlbumInfo;
		}
		$albumInfoAry = $tempAlbumInfoAry;
		
		$returnArray = $albumInfoAry;
        break;
        
    case "get_category": //get all category
    	$categoryArr = $ldc->Get_Category();
		
		//get num of album in category
		for($i = 0; $i < count($categoryArr); $i++){
			$countAlbums = 0;
			$catAlbums = $ldc->Get_Album("", $categoryArr[$i]['CategoryCode'], 0);
			foreach($catAlbums as $currentAlbum){
				if($ldc->isAlbumReadable($currentAlbum['AlbumID'])){
					$countAlbums++;
				}
				$categoryArr[$i]['NumOfAlbum'] = $countAlbums;
			}
		}
		
		$returnArray = $categoryArr;
        break;
        
    case "get_video": //get all video that user can read ($albumid, $keyword, $orderby(Title|LastModified) for searching)
    	if($orderby == 'Title'){
    		$orderby = 'p.Description, p.Title, ';
    	}
    	else if($orderby == 'LastModified'){
    		$orderby = 'p.DateModified desc, p.DateInput desc, ';
    	}
    	else if($orderby == 'DateTaken'){
    		$orderby = 'p.DateTaken desc, p.DateModified desc, p.DateInput desc, ';
    	}
    	
        $videoArr = $ldc->Get_Album_Photo_Search(array('file_type'=>'video','keyword'=>$keyword,'album_id'=>$albumid, 'sort'=>$orderby), 0);
        
        $returnArray = $videoArr;
        break;
        
    case "get_photo_video": //get all video that user can read ($albumid, $keyword, $orderby(Title|LastModified) for searching)
    	if($orderby == 'Title'){
    		$orderby = 'p.Description, p.Title, ';
    	}
    	else if($orderby == 'LastModified'){
    		$orderby = 'p.DateModified desc, p.DateInput desc, ';
    	}
    	else if($orderby == 'DateTaken'){
    		$orderby = 'p.DateTaken desc, p.DateModified desc, p.DateInput desc, ';
    	}
    	
        $videoArr = $ldc->Get_Album_Photo_Search(array('keyword'=>$keyword,'album_id'=>$albumid, 'sort'=>$orderby), 0);
        
        $returnArray = $videoArr;
        break;
        
    case "get_favorite": //get all user favorites video ($keyword, $orderby(Title|LastModified) for searching)
    	if($orderby == 'Title'){
    		$orderby = 'p.Description, p.Title, ';
    	}
    	else if($orderby == 'LastModified'){
    		$orderby = 'p.DateModified desc, p.DateInput desc, ';
    	}
    	else if($orderby == 'DateTaken'){
    		$orderby = 'p.DateTaken desc, p.DateModified desc, p.DateInput desc, ';
    	}
    	
        $ablumPhotoInfoAry = $ldc->Get_Album_Photo_Favorites('',array('keyword'=>$keyword,'sort'=>$orderby));

		$tempArr = array();
	    foreach($ablumPhotoInfoAry as $aMostHit){
			$photo_ext = pathinfo($aMostHit['title'], PATHINFO_EXTENSION);
			$isVideo = (in_array(strtoupper($photo_ext),libdigitalchannels::$videoFormat))? true : false;
			
			# add path to the photo array
			$album = array(
			    "id" => $aMostHit['album_id'],
			    "input_date" => $aMostHit['a_input_date'],
			    "input_by" => $aMostHit['a_input_by']
			);
			
			# get the file path
			$filePath = libdigitalchannels::getPhotoFileName('thumbnail', $album, $aMostHit);
			$originalFilePath = libdigitalchannels::getPhotoFileName('photo', $album, $aMostHit);
			$aMostHit['FilePath'] = $filePath;
			$aMostHit['OriginalFilePath'] = $originalFilePath;
			$aMostHit['type'] = ($isVideo?'Video':'Photo');
			
			if($isVideo && $ldc->isAlbumReadable($aMostHit['album_id'])){
				$tempArr[] = $aMostHit;
			}
	    }
	    
	    $returnArray = $tempArr;
        break;
        
    case "get_applied_photo": //get all user favorites video ($keyword, $orderby(Title|LastModified) for searching)
    	$returnArray = $ldc->Get_Album_Photo_Search(array('photo_id' => explode(',',$photo_id)),0);
        break;    
        
    default:
        $returnArray = false;
}
function json_encode_cust($a=false)
 {
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a))
    {
      if (is_float($a))
      {
        // Always use "." for floats.
        return '"' .floatval(str_replace(",", ".", strval($a))). '"';
      }

      if (is_string($a))
      {
        static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
      }
      else
        return '"' .$a. '"';
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a)) 
 	{       
 		if (key($a) !== $i){  
			$isList = false; 
			break;       
		}     
 	}     
 	
 	$result = array();     
 	if ($isList){
		foreach ($a as $v) 
			$result[] = json_encode_cust($v);      
		return '[' . join(',', $result) . ']';     
	}else{ 
		foreach ($a as $k => $v) 
			$result[] = json_encode_cust($k).':'.json_encode_cust($v);
			
      return '{' . join(',', $result) . '}';
    }
  }
//debug_pr($returnArray);
//echo serialize($returnArray);
echo json_encode_cust($returnArray);

intranet_closedb();
?>