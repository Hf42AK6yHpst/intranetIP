<?php
// Editing by Henry
/*
 * 2015-03-02 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

//if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
//	header("Location: ../index.php");
//	exit;
//}
//
//if ($AlbumID && !$ldc->isAlbumEditable($AlbumID)){
//	header("Location: ../index.php");
//	exit;
//}

if (!$RecommendID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

//debug_pr($_POST);
//debug_pr($_REQUEST);

$params = array(
		//'CategoryCode'=>$CategoryCode,
	    'title'=>htmlspecialchars($title),
	    'description'=>htmlspecialchars($description),
	    //'cover_photo_id'=>$cover_photo_id,
	    //'place'=>htmlspecialchars($place),
	    //'date'=>$AccessDate,
	    'since'=>$StartDate,
	    'until'=>$EndDate,
	    'recommend_id'=>$RecommendID,
	    'photo_ids'=>$photo_ids,
	    'NeedUpdatePhoto'=>$NeedUpdatePhoto
	);

if(!$RecommendID){
	$result = $ldc->Add_Recommend_Album($params);
	$RecommendID = $result;
}
else{
	$result = $ldc->Update_Recommend_Album($params);
}

//$ldc->updateAlbumPICs($PIC, $AlbumID);

//$select_groups = $share_to?$select_groups:array();
//$ldc->resetAlbumUsersGroups($share_to=='all', $select_groups, $user_ids=array(), $AlbumID);

$msg = ($result > 0? "UpdateSuccess" : "UpdateUnsuccess");

intranet_closedb();
header("Location: admin_recommend_album_photo.php?RecommendID=$RecommendID&msg=$msg");
?>