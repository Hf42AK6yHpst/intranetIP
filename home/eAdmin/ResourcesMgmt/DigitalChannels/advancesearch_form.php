<script>
$(function(){

    $('.advancesearch').draggable({cancel:'dl, input', cursor: "move"});
    $('.advancesearch .dropdown li a').click(function(){
	
	$(this).parent().addClass('selected').siblings('li').removeClass('selected').parent().slideUp().siblings('input').val($(this).find('.id').html()).closest('.dropdown').find('dt span').html($(this).html());
	
	return false;
    });
    $('.advancesearch .formsubbutton').click(function(){
	$('.advancesearch').fadeOut();
    });
    
    $('.advancesearch .dropdown li.selected:first a').click();
    
    
});
</script>
<form class="advancesearch" <?=$page!='search'||$keyword?'style="display:none"':''?> method="post" action="./?page=search">
			
    <div class="fields"><?=$eLib["html"]["title"]?>:</div>
    <input type="text" name="title" class="input" value="<?=$title?>">
    
    <p class="spacer"></p>
    
    <div class="fields"><?=$eLib["html"]["author"]?>:</div>
    <input type="text" name="author" class="input" value="<?=$author?>">
    
    <p class="spacer"></p>
    
    <div class="fields"><?=$eLib["html"]["category"]?>:</div>
  
    <dl class="dropdown">
	<dt><a href="#"><span>
	    <?=$category_selected_name?$category_selected_name:$eLib_plus["html"]["all"]?>
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
	     <li <?=!$category_selected_name?'class="selected"':''?>><a href="#"><?=$eLib_plus["html"]["all"]?><span class="id" style="display:none;">,,</span></a></li>
	    <? foreach ($book_catalog as $l=>$book_language) : ?>
		  
		   <? foreach ($book_language['categories'] as $cat):?>
			<? if ($cat['title']): ?>
			<li <?=$category_selected_name===$cat['title']?'class="selected"':''?>>
			    <a href="#"><?=$cat['title']?><span class="id" style="display:none;"><?=$l.','.$cat['title'].','?></span></a>
			</li>
			<? endif; ?>
			<? foreach ($cat['subcategories'] as $subcat):?>
			<li <?=$category_selected_name===$cat['title'].' -> '.$subcat['title']?'class="selected"':''?>>
			    <a href="#"><?=$cat['title'].' -> '.$subcat['title']?><span class="id" style="display:none;"><?=$l.','.$cat['title'].','.$subcat['title']?></span></a>
			</li>
			<? endforeach; ?>
		   <? endforeach; ?>
	    <? endforeach; ?>
	    </ul>
	    <input type="hidden" value="<?=$categories?>" name="categories"/>
	</dd>
    </dl>
    
    
    <p class="spacer"></p>
    
    <div class="fields"><?=$eLib['Book']["Level"]?>:</div>
    <dl class="dropdown">
	<dt><a href="#"><span>
	    <?=$level?$level:$eLib_plus["html"]["all"]?>
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
	     <li <?=!$level?'class="selected"':''?>><a href="#"><?=$eLib_plus["html"]["all"]?><span class="id" style="display:none;"></span></a></li>
	<? foreach ($book_catalog_levels as $lv) : ?>
	       <li <?=$level==$lv?'class="selected"':''?>><a href="#"><?=$lv?><span class="id" style="display:none;"><?=$lv?></span></a></li>
	<? endforeach; ?>
	    </ul>
	    <input type="hidden" value="<?=$level?>" name="level"/>
	</dd>
    </dl>
    
    <p class="spacer"></p>
    
    <div class="fields"><?=$eLib_plus["html"]["tag"]?>:</div>
    <dl class="dropdown">
	<dt><a href="#"><span>
	    <?=$tag_id?$tag_selected_name:$eLib_plus["html"]["all"]?>
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
	     <li <?=!$tag_id?'class="selected"':''?>><a href="#"><?=$eLib_plus["html"]["all"]?><span class="id" style="display:none;"></span></a></li>
	<? foreach ($book_catalog_tags as $tag) : ?>
	       <li <?=$tag_id==$tag?'class="selected"':''?>><a href="#"><?=$tag['title']?><span class="id" style="display:none;"><?=$tag['tag_id']?></span></a></li>
	<? endforeach; ?>
	    </ul>
	    <input type="hidden" value="<?=$tag_id?>" name="tag_id"/>
	</dd>
    </dl>
    
    <p class="spacer"></p>
    
    <div class="fields"><?=$eLib_plus["html"]["order"]?>:</div>
    <dl class="dropdown">
	<dt><a href="#"><span>
	    <?=$eLib["html"]["title"]?>
	</span></a></dt>
	<dd>
	    <ul style="display: none;">
		<li <?=!$sortby||$sortby=='title'?'class="selected"':''?>><a href="#"><?=$eLib["html"]["title"]?><span class="id" style="display:none;">title</span></a></li>
		<li <?=$sortby=='author'?'class="selected"':''?>><a href="#"><?=$eLib["html"]["author"]?><span class="id" style="display:none;">author</span></a></li>
		<li <?=$sortby=='hitrate'?'class="selected"':''?>><a href="#"><?=$eLib_plus["html"]["hitrate"]?><span class="id" style="display:none;">hitrate</span></a></li>
		<li <?=$sortby=='rating'?'class="selected"':''?>><a href="#"><?=$eLib["html"]["rating"]?><span class="id" style="display:none;">rating</span></a></li>
		<li <?=$sortby=='source'?'class="selected"':''?>><a href="#"><?=$eLib["html"]["source"]?><span class="id" style="display:none;">source</span></a></li>
		<li <?=$sortby=='level'?'class="selected"':''?>><a href="#"><?=$eLib["html"]["level"]?><span class="id" style="display:none;">level</span></a></li>
	    </ul>
	    <input type="hidden" value="<?=$sortby?$sortby:'title'?>" name="sortby"/>
	</dd>
    </dl>
    
    <div class="edit_bottom">
	<input type="submit" class="formbutton" value="<?=$eLib["html"]["search"]?>">
	<input type="button" value="<?=$button_cancel?>" class="formsubbutton">
    </div>

    
    
</form>