<?php
// Editing by Henry
/*
 * 2015-01-26 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
$lgs = new libgeneralsettings();
$settings = $lgs->Get_General_Setting("DigitalChannels", array("'AllowDownloadOriginalPhoto'"));

$ldc = new libdigitalchannels();

$photoInfoArr = explode('_',getDecryptedText($_REQUEST['id'],libdigitalchannels::$encrypt_key));

$photo = $ldc->getAlbumPhoto($photoInfoArr[0]);
$album = $ldc->getAlbum($photo['album_id']);
//$id = urlencode(getEncryptedText("ApplicationID=".$applicationID."&sus_status=".$_REQUEST['sus_status']."&SchoolYearID=".$_REQUEST['SchoolYearID'],$admission_cfg['FilePathKey']));
//parse_str(getDecryptedText(urldecode($_REQUEST['id']),$admission_cfg['FilePathKey']), $output);
?>
<style type="text/css">
body {
	padding-top:0;
	padding-bottom:0;
	padding-left:0;
	padding-right:0;
	margin-top:0;
	margin-bottom:0;
	margin-left:0;
	margin-right:0;
}
</style>
<video style="width:100%;height:100%" controls <?=$settings['AllowDownloadOriginalPhoto']?'':'controlsList="nodownload"'?>>
    <source type="video/mp4" src="<?=libdigitalchannels::getAlbumFileName('photo', $album).'/'.base64_encode(getDecryptedText($_REQUEST['id'],libdigitalchannels::$encrypt_key)).'.mp4';?>">
    <!--<source type="video/ogg" src="mov_bbb.ogg"></source>-->
    Your browser does not support HTML5 video.
</video>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script>
<?if(!$settings['AllowDownloadOriginalPhoto']){?>
    //disable the right click on the photo
    $('video').live('contextmenu', function(e) {
	    return false;
	});
    
    //disable drag and drop of the image
    $('video').live('mousedown', function (e) {
           return false;
    });
<?}?>
</script>
<?php
intranet_closedb();
?>