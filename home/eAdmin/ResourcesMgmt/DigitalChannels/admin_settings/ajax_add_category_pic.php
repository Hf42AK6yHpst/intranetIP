<?php
// Editing by Henry
/*
 * 2017-01-04 (Henry):
 * 		- file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");

$CategoryCode = $_REQUEST['CategoryCode'];
$AddUserID = $_REQUEST['AddUserID'];

intranet_opendb();

$ldc = new libdigitalchannels();

if($junior_mck > 0){
	$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
}

$ldc->Start_Trans();

$Result = $ldc->Add_Category_Pic($CategoryCode,$AddUserID);

if ($Result) {
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
	$ldc->Commit_Trans();
}
else {
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$ldc->RollBack_Trans();
}

intranet_closedb();
?>
