<?php
// Editing by Henry
/*
 * 2017-01-04 (Henry):
 * 		- file created
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$IdentityType = $_REQUEST['IdentityType'];
$AddUserID = (is_array($_REQUEST['AddUserID']))? $_REQUEST['AddUserID']:array();
$ParentStudentID = $_REQUEST['ParentStudentID'];
$CategoryCode = $_REQUEST['CategoryCode'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$ldcUI = new libdigitalchannels_ui();

if($junior_mck > 0){
	$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
}

echo $ldcUI->Get_User_Selection($AddUserID,$IdentityType,$YearClassID,$ParentStudentID,$CategoryCode);

intranet_closedb();
?>
