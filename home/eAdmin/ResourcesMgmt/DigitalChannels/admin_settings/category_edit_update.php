<?php
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

$ldu = new libdigitalchannels();
if($CategoryCode=="") {
	header("Location: category.php");	
}

$Code = intranet_htmlspecialchars($Code);
$dataAry['CategoryCode'] = intranet_htmlspecialchars($CategoryCode);
$dataAry['DescriptionEn'] = intranet_htmlspecialchars($DescriptionEn);
$dataAry['DescriptionChi'] = intranet_htmlspecialchars($DescriptionChi);


if($Code)
	$result = $ldu->Update_Category($Code, $dataAry);
else
	$result = $ldu->Add_Category($dataAry);

if (!$result) {
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
	$ReturnMsgKey = 'UpdateSuccess';
}

intranet_closedb();

header("Location: category.php?ReturnMsgKey=$ReturnMsgKey");

?>
