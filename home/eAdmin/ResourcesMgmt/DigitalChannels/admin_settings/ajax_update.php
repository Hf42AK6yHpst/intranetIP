<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
$ldu = new libdigitalchannels();

$Action = $_REQUEST['Action'];
if ($Action == 'Reorder_Category') {
	$DisplayOrderString = $_POST['DisplayOrderString'];
	$displayOrderArr = $ldu->Get_Update_Display_Order_Arr($DisplayOrderString);
	$success = $ldu->Update_Category_Record_DisplayOrder($displayOrderArr);
	echo $success? '1' : '0';
}

intranet_closedb($Action);
?>