<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");

$map['cat'] = array( 'table' => 'INTRANET_DC_CATEGORY', 'key' => 'CategoryCode');

$needle = $_REQUEST['needle'];
$type =  $_REQUEST['type'];


$return['success'] = FALSE; 
if (isset($map[$type])){
	//echo 1;
	$tableinfo = $map[$type];
	$return['unique'] = checkUniqueness($tableinfo['table'],$tableinfo['key'], $needle);
	$return['success'] = TRUE;
}

echo json_encode($return);


function checkUniqueness($table, $kayStackKey, $needle){
	global $Lang;
	
	intranet_auth();
	intranet_opendb();
	
	if ( empty($table) || empty($kayStackKey) ){
		return false;
	}
	
	$ldu = new libdigitalchannels();
	$filter[$kayStackKey] = "'".$needle."'";
	//dump($filter);
	$result = $ldu-> SELECTFROMTABLE( $table,$kayStackKey,$filter);
	//dump($result);
	return ( empty($result) )? TRUE : FALSE;
		
}
?>