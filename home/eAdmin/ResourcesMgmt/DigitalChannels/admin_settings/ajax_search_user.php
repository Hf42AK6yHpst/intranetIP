<?php
// Editing by Henry
/*
 * 2017-01-04 (Henry):
 * 		- file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");

intranet_opendb();

$AddUserID = $_REQUEST['AddUserID'][0];

$SearchValue = stripslashes(urldecode($_REQUEST['q']));

/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$ldc = new libdigitalchannels();

if($junior_mck > 0){
	$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
}

//Search_User parameter two is a string
$Result = $ldc->Search_User($SearchValue,$AddUserID);

for ($i=0; $i< sizeof($Result); $i++) {
	// Show ClassName and ClassNumber for Student only
	if ($Result[$i]['UserType'] != 'Student')
	{
		$Result[$i]['ClassName'] = '';
		$Result[$i]['ClassNumber'] = '';
	}
	echo $Result[$i]['Name']."|".$Result[$i]['ClassName']."|".$Result[$i]['ClassNumber']."|".$Result[$i]['UserType']."|".$Result[$i]['UserID']."\n";
}

intranet_closedb();
die;
?>
