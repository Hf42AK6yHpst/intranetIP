<?php
// Editing by Henry
/*
 * 2017-01-04 (Henry):
 * 		- support category pic
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2014-10-09 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();
$ldc = new libdigitalchannels();

# Display Admin Menu
$CurrentPageArr['DigitalChannelsAdminMenu'] = true;

$CurrentPageArr['DigitalChannels'] = 1;
$CurrentPage = "Settings_Category";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}
//$ldcUI->LAYOUT_START($Msg);

//echo $ldcUI->Include_JS_CSS();

$categoryInfoAry = $ldc->Get_Category();
$numOfCategory = count($categoryInfoAry);
$ReturnMsgKey = $_GET['ReturnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];

//$oleCategoryAssoAry = $libenroll->Get_Ole_Category_Array($returnAsso=true);
//$categoryTypeAssoAry = $libenroll->Get_CategoryType_Array($returnAsso=true);

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$ldcUI->LAYOUT_START($ReturnMsg);
echo $linterface->Include_JS_CSS();
echo $ldcUI->Include_JS_CSS();	

## Edit/ Delete Buttons	
$ActionBtnArr = array();
$ActionBtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'CategoryID[]\',\'category_edit.php\')');
$ActionBtnArr[] = array('delete', 'javascript:checkRemove2(document.form1,\'CategoryID[]\',\'goDeleteCategory();\')');

## Display Result
/////>>>>>
$widthCatTitle = 89;
$widthOleCategory = 40;
$widthCategoryType = 20;
if($plugin['iPortfolio']) {
	$widthCatTitle -= $widthOleCategory; 
}
if ($sys_custom['eEnrolment']['CategoryType']) {
	$widthCatTitle -= $widthCategoryType;
	$widthOleCategory -= $widthCategoryType;
}
/////<<<<<
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="get" action="category2.php">'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div class="content_top_tool">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Category();")."\n";
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);			
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th style="width:2%;">#</th>'."\n";
									$x .= '<th style="width:15%;">'.$Lang['DigitalChannels']['Settings']['CategoryCode'].'</th>'."\n";
									$x .= '<th style="width:15%;">'.$Lang['DigitalChannels']['Settings']['DescriptionEn'].'</th>'."\n";
									$x .= '<th style="width:15%;">'.$Lang['DigitalChannels']['Settings']['DescriptionChi'].'</th>'."\n";								
									$x .= '<th style="width:40%;">'.$Lang['DigitalChannels']['Settings']['Helper'].'</th>'."\n";
									$x .= '<th style="width:10%;">&nbsp;</th>'."\n";
									$x .= '<th style="width:3%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'CategoryID[]\'):setChecked(0,this.form,\'CategoryID[]\')" name="checkmaster"></th>'."\n";
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";
					
							$x .= '<tbody>'."\n";
								if ($numOfCategory == 0) {
									$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else {
									for ($i=0; $i<$numOfCategory; $i++) {
										$thisCategoryID  = $categoryInfoAry[$i]['CategoryCode'];					
										$thisDescriptionEn = $categoryInfoAry[$i]['DescriptionEn'];
										$thisDescriptionChi = $categoryInfoAry[$i]['DescriptionChi'];
										
										// -- get category pic [start]
										$CategoryPicList = $ldc->Get_Category_Pic($thisCategoryID);
										$AddUserID = '';
										for($j=0; $j< sizeof($CategoryPicList); $j++) {
											$class_info = ($CategoryPicList[$i]['ClassName']!="" && $CategoryPicList[$i]['ClassNumber']!="") ?  '('.$CategoryPicList[$i]['ClassName']."-".$CategoryPicList[$i]['ClassNumber']. ")" : "";
											$AddUserID[] = $CategoryPicList[$j]['PicName'].$class_info;
										}
										// -- get category pic [end]
										
										$x .= '<tr id="tr_'.$thisCategoryID .'">'."\n";
											$x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
											$x .= '<td>'.$thisCategoryID.'</td>'."\n";
											$x .= '<td>'.$thisDescriptionEn.'</td>'."\n";
											$x .= '<td>'.$thisDescriptionChi.'</td>'."\n";
											$x .='<td>
													<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Group_Add_Pic_Form(\''.$thisCategoryID.'\'); return false;" class="new thickbox" title="'.$Lang["libms"]["general"]["edit"].' '.$Lang["libms"]["GroupManagement"]["ClassManagementMembers"].'">
													<div id="helper_list_'.$thisCategoryID.'" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);">
														'.($AddUserID?implode(', ',$AddUserID):'--').'
													</div></a>
												</td>';
											$x .= '<td class="Dragable">'."\n";
												$x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move'])."\n";
											$x .= '</td>'."\n";
											$x .= '<td>'."\n";
												$x .= '<input type="checkbox" id="CategoryChk" class="CategoryChk" name="CategoryID[]" value="'.$thisCategoryID.'">'."\n";
											$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									}					
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<span class="tabletextremark" style="float: left;"><span class="tabletextrequire">*</span> '.$Lang['DigitalChannels']['Remarks']['CategoryPicSelection'].'</span>'."\n";
	$x .= '</div>'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="javascript">
$(document).ready( function() {	
	js_Init_DND_Table();
});

function js_Go_New_Category() {
	window.location = 'category_edit.php';
}

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "ContentTable") {
				Block_Element(table.id);
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					{
						var thisID = rows[i].id;
						var thisIDArr = thisID.split('_');
						var thisObjectID = thisIDArr[1];
						RecordOrder += thisObjectID + ",";
					}
				}

				// Update DB
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_Category",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						js_Reset_Display_Range();
						
						// Get system message
						if (ReturnData=='1') {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess']?>';			
						}
						else {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
	
//Delete Category	
function goDeleteCategory() {
	var jsSelectedCategoryIDArr = new Array();
	$('input.CategoryChk:checked').each( function() {
		jsSelectedCategoryIDArr[jsSelectedCategoryIDArr.length] = $(this).val();
	});
	var jsSelectedCategoryIDList = jsSelectedCategoryIDArr.join(',');

	$.post(
		"ajax_validate.php", 
		{ 
			Action: "Is_Category_Record",
			CategoryIDList: jsSelectedCategoryIDList	
		},
		
		function(ReturnData) {
			var jsCanSubmit = false;
			if (ReturnData == '1') {
//				if (confirm('<?=$Lang['eEnrolment']['Settings']['WarningArr']['CategoryLinkedToDataAlready']?>')) {
//					jsCanSubmit = true;
//				}
//				else {
//					jsCanSubmit = false;
//				}
				alert('<?=$Lang['DigitalChannels']['Msg']['CannotRemoveAlbum']?>');
				jsCanSubmit = false;
			}
			else {
				jsCanSubmit = true;
			}
			if (jsCanSubmit) {
				$('form#form1').attr('action', 'category_remove.php').submit();
			}
		}
	);
}

//Reset the ranking display
function js_Reset_Display_Range(){
	var jsRowCounter = 0;
	$('span.rowNumSpan').each( function () {
		$(this).html(++jsRowCounter);
	});
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

function Get_Group_Add_Pic_Form(CategoryCode) {
	GroupAddMemberAjax = GetXmlHttpObject();

  if (GroupAddMemberAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_add_category_pic_form.php';
  var postContent = 'CategoryCode='+CategoryCode;
	GroupAddMemberAjax.onreadystatechange = function() {
		if (GroupAddMemberAjax.readyState == 4) {
			ResponseText = Trim(GroupAddMemberAjax.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
		 	Init_JQuery_AutoComplete('UserSearch_'+CategoryCode);
		}
	};
  GroupAddMemberAjax.open("POST", url, true);
	GroupAddMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GroupAddMemberAjax.send(postContent);
}

var PreviousClass = "";
function Get_User_List() {
	var CategoryCode = CategoryCode || document.getElementById('CategoryCode').value;
	var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	//Select_All_Options('AddUserID[]',true);
	var AddUserID = Get_Selection_Value('AddUserID[]','QueryString',true);

	if (IdentityType != "Student" || (document.getElementById('YearClassSelect').style.display != "none" && document.getElementById('YearClassSelect').selectedIndex != 0)) {

		// hide the select class selection if switced to teacher/ staff/parent
		if (IdentityType != "Student") {
			document.getElementById('YearClassSelect').selectedIndex = 0;
			$('#YearClassSelect').hide();
		}

		// get the form class ID
		var YearClassID = document.getElementById('YearClassSelect').options[document.getElementById('YearClassSelect').selectedIndex].value;

		var GetFinalList = false;
		if (PreviousClass == YearClassID)
			GetFinalList = true;

		// get student selected if the selection element exists
		var ParentStudentID = "";
		if (document.getElementById('ParentStudentID') && IdentityType == "Parent" && GetFinalList)
			ParentStudentID = document.getElementById('ParentStudentID').options[document.getElementById('ParentStudentID').selectedIndex].value;

		// setting global variable to prevent parent selection problem
		PreviousClass = YearClassID;

		StudListAjax = GetXmlHttpObject();

	  if (StudListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_get_user_list.php';
	  var postContent = AddUserID;
	  postContent += '&YearClassID='+YearClassID;
	  postContent += '&IdentityType='+IdentityType;
	  postContent += '&ParentStudentID='+ParentStudentID;
	  postContent += '&CategoryCode='+CategoryCode;
	  
		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
		  		document.getElementById('ParentStudentLayer').innerHTML = "";
			  	document.getElementById('AvalUserLayer').innerHTML = ResponseText;
			}
		};
	  StudListAjax.open("POST", url, true);
		StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		StudListAjax.send(postContent);
	}
	else {
		$('#YearClassSelect').show('fast');
		document.getElementById('ParentStudentLayer').innerHTML = "";

		StudListAjax = GetXmlHttpObject();
	  if (StudListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_get_class_mgt_user_list.php';
	  var postContent = AddUserID;
	  postContent += '&IdentityType='+IdentityType;
	  postContent += '&CategoryCode='+CategoryCode;
	  
		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
			 	document.getElementById('AvalUserLayer').innerHTML = ResponseText;
			}
		};
	  StudListAjax.open("POST", url, true);
		StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		StudListAjax.send(postContent);
	}
}

function Add_Category_Pic(CategoryCode) {
	
	var SelectedUser = Get_Selection_Value("AddUserID[]","QueryString",true);
	
		document.getElementById('AddMemberSubmitBtn').disabled = true;
		document.getElementById('AddMemberCancelBtn').disabled = true;
		Block_Thickbox();
		AddGroupMemberAjax = GetXmlHttpObject();

	  if (AddGroupMemberAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_add_category_pic.php';
	  var postContent = SelectedUser;
	  postContent += '&CategoryCode='+CategoryCode;
		AddGroupMemberAjax.onreadystatechange = function() {
			if (AddGroupMemberAjax.readyState == 4) {
				Update_Group_Member_List(CategoryCode);
				ResponseText = Trim(AddGroupMemberAjax.responseText);
			  Get_Return_Message(ResponseText);
			  UnBlock_Thickbox();
			  Scroll_To_Top();
			  window.top.tb_remove();
			}
		};
	  AddGroupMemberAjax.open("POST", url, true);
		AddGroupMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		AddGroupMemberAjax.send(postContent);
}

function Update_Group_Member_List(CategoryCode) {
	GroupMemberListAjax = GetXmlHttpObject();

  if (GroupMemberListAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_category_pic_list.php';
  var postContent = 'CategoryCode='+CategoryCode;
	GroupMemberListAjax.onreadystatechange = function() {
		if (GroupMemberListAjax.readyState == 4) {
			ResponseText = Trim(GroupMemberListAjax.responseText);
		  document.getElementById('helper_list_'+CategoryCode).innerHTML = ResponseText;
		  Thick_Box_Init();
		}
	};
  GroupMemberListAjax.open("POST", url, true);
	GroupMemberListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GroupMemberListAjax.send(postContent);
}

// UI function for add/ remove class teacher/ student in add/edit class
{
function Add_Selected_User(UserID,UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('AddUserID[]');

	UserSelected.options[UserSelected.length] = new Option(UserName,UserID);

	Update_Auto_Complete_Extra_Para();
}

function Remove_Selected_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		if (UserSelected.options[i].selected)
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
}

function Remove_All_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_Class_User() {
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
	    $(elOptNew).attr('has_group',$(User).attr('has_group'));
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	Update_Auto_Complete_Extra_Para();
}

function Add_All_User() {
	
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	// union selected and newly add user list
	if (ClassUser.length > UserSelected.length) {
		var cloneCell = document.getElementById('SelectedUserCell');

		// clone element from avaliable user list
		var cloneFrom = ClassUser.cloneNode(1);
		cloneFrom.setAttribute('id','AddUserID[]');
		cloneFrom.setAttribute('name','AddUserID[]');
		var j=0;
		for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			User = UserSelected.options[i];
			var elOptNew = document.createElement('option');
			
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			$(elOptNew).attr('has_group',$(User).attr('has_group'));

	    
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      cloneFrom.add(elOptNew, cloneFrom.options[j]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      cloneFrom.add(elOptNew, j); // IE only
	    }
			UserSelected.options[i] = null;
			j++;
		}

		cloneCell.replaceChild(cloneFrom,UserSelected);
	}
	else {
		for (var i = (ClassUser.length -1); i >= 0 ; i--) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	// update auto complete parameter list
	Update_Auto_Complete_Extra_Para();

	// empty the avaliable user list
	document.getElementById('AvalUserLayer').innerHTML = '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true"></select>';
}
}

// jquery autocomplete function
{
var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_search_user.php",
		{
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[3],li.selectValue+'('+li.extra[2]+')');
				Get_User_List();
			},
			formatItem: function(row) {
				// Hide Class and Class Number if both are empty
				if ((row[1] == 'NA' || $.trim(row[1]) == '') && (row[2] == 'NA' || $.trim(row[2]) == ''))
					return row[0] + " (Identity: " + row[3] + ")";
				else
					return row[0] + " (Identity: " + row[3] + "[" + row[1] + "-" + row[2] + "])";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 280,
			scrollLayer: 'EditLayer'
		}
	);

	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = Get_Selected_Value('AddUserID[]');
	//for passing the group id to the ajax page
//	ExtraPara['GroupID'] = document.getElementById('GroupID').value;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
	
}

function Get_Selected_Value(SelectID,ReturnType) {
	var ReturnType = ReturnType || "JQuery";
	var str = "";
	var isFirst = true;
	var Obj = document.getElementById(SelectID);

	if (ReturnType == "JQuery") {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.options[i].value;
			isFirst = false;
		}

		var resultArray = new Array();
		resultArray[SelectID] = str.split('&');
		return resultArray;
	}
	else {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.name + '=' +Obj.options[i].value;
			isFirst = false;
		}

		return str;
	}
}
}

// thick box function
{
//on page load call tb_init
function Thick_Box_Init(){
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

Thick_Box_Init();
</script>
<?php
echo $x;
?>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>
