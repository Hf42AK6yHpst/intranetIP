<?php
//using: 

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

$lduUI = new libdigitalchannels_ui();
$ldu = new libdigitalchannels();
if($CategoryCode=="") {
	header("Location: category.php");	
}

$successAry = array();

for ($i = 0; $i < sizeof($CategoryID); $i++) {
	$successAry['delete'][] = $ldu->Delete_Category($CategoryID[$i]);
}
$successAry['reorder'] = $ldu->Update_Category_Order();

if (in_array(false, $successAry)) {
	$ReturnMsgKey = 'DeleteUnsuccess'; 
}
else {
	$ReturnMsgKey = 'DeleteSuccess';
}

intranet_closedb();
header("Location: category.php?ReturnMsgKey=$ReturnMsgKey");
?>