<?php
// Editing by
/*
 * Change Log:
 *
 * 2018-03-16 (Cameron)
 * - add standardizeFormPostValue() to text fields(album_title, album_description, photo_description)
 * 2018-03-15 (Cameron)
 * - add params: photo_event_title, photo_event_date_from and photo_event_date_to
 * 2015-11-25 (Pun):
 * - Added force encoding to UTF-8 (for EJ)
 * 2014-11-20 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
// header("Location: ../index.php");
// exit;
// }

$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();
$ldc = new libdigitalchannels();

$arrCookies = array();
$arrCookies[] = array(
    "ck_resourcesmanagment_digitalchannels_from_eService",
    "From_eService"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}
if ($From_eService) {
    $CurrentPageArr['eServiceDigitalChannels'] = 1;
} else {
    $CurrentPageArr['DigitalChannels'] = 1;
}

// if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
// header("Location: ../index.php");
// exit;
// }
//
// if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
// header("Location: ../index.php");
// exit;
// }

// Display tab pages
$TAGS_OBJ[] = array(
    $Lang['DigitalChannels']['Settings']['Favorites'],
    "",
    1
);

// $CurrentPage = "Favorites";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if ($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])) {
    $Msg = $Lang['General']['ReturnMessage'][$msg];
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$home_header_no_EmulateIE7 = true;
$ldcUI->LAYOUT_START($Msg);

echo $ldcUI->Include_JS_CSS();
// debug_pr($CategoryCode);


$params = array(
    'keyword' => standardizeFormPostValue($keyword),
    'advance' => $advance,
    'album_title' => standardizeFormPostValue($album_title),
    'album_description' => standardizeFormPostValue($album_description),
    'photo_description' => standardizeFormPostValue($photo_description),
    'category_id' => $category_id,
    'file_type' => $file_type,
    'type' => $type,
    'photo_event_title' => standardizeFormPostValue($photo_event_title),
    'photo_event_date_from' => $photo_event_date_from,
    'photo_event_date_to' => $photo_event_date_to
);
?>
<script type="text/javascript" src="script/photo_thumb.js"></script>
<script>
$(function(){		
	jQuery(function($){
		$('.photo_thumb_effect').mosaic({
			animation	:	'slide'
		});
	});
});
</script>

<div class="digital_channels_content">
	<div id="container">
<?echo $ldcUI->Display_Title_Menu($params);?>
<?
if (! $advance || $type == 'album')
    echo $ldcUI->Display_Category_Album_Search($params);
else 
    echo $ldcUI->Display_Category_Album_Photo_Search($params);

?>         
	</div>
</div>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>