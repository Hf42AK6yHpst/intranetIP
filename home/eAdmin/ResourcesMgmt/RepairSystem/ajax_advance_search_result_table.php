<?php
# modifying :
/*
 * 	Note: map single item only
 * 
 * 	2017-06-05 Cameron
 * 		- also show item code next to item name when return
 * 
 * 	2017-05-31 Cameron
 *		- retrieve inventory photo 
 *		- fix bug to handle special characters for ItemName and search keyword
 * 
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

$keyword = standardizeFormPostValue($_POST['keyword']);

intranet_auth();
intranet_opendb(); 
$lrepairsystem = new librepairsystem();
$linterface = new interface_html();
$BuildingNeme = Get_Lang_Selection("LocBuilding.NameChi", "LocBuilding.NameEng") ;
$FloorName = Get_Lang_Selection("LocLevel.NameChi", "LocLevel.NameEng") ;
$RoomName = Get_Lang_Selection("g.NameChi", "g.NameEng");

$ukw = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));				// A&<>'"\B ==> A&<>\'\"\\\\B
$ckw = intranet_htmlspecialchars(str_replace("\\","\\\\",$_POST['keyword']));	// A&<>'"\B ==> A&amp;&lt;&gt;\&#039;&quot;\\\\B

$sql= 

		"SELECT 
			a.ItemID,
			a.ItemCode,
			a.NameChi AS InventoryItemName_b5,
			a.NameEng AS InventoryItemName_eng,
			CONCAT($BuildingNeme,' > ', $FloorName,' > ', $RoomName) AS LocationName,
			p.PhotoPath,
			p.PhotoName
		 FROM 
		 	INVENTORY_ITEM AS a 
			INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS d ON (a.ItemID = d.ItemID) 
			INNER JOIN INVENTORY_LOCATION AS g ON (d.LocationID = g.LocationID) 
			INNER JOIN INVENTORY_LOCATION_LEVEL AS LocLevel ON (g.LocationLevelID = LocLevel.LocationLevelID) 
			INNER JOIN INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocLevel.BuildingID = LocBuilding.BuildingID)
			LEFT JOIN INVENTORY_PHOTO_PART p ON p.ItemID=a.ItemID
		 WHERE			
			a.NameEng LIKE '%$ukw%' or a.NameChi LIKE '%$ukw%' or
			a.NameEng LIKE '%$ckw%' or a.NameChi LIKE '%$ckw%'
";
$result = $lrepairsystem->returnResultSet($sql);
$numOfResult = count($result);

//debug_pr($result);


$x = '';
if ($numOfResult > 0) {
	$x .= "<table class='common_table_list'>";
	$x .= "<thead><tr>";
	$x .= "<th>".$Lang['Btn']['Select']."</th>";
	$x .= "<th>".$i_InventorySystem_Item_Code."</th>";
	$x .= "<th>".$i_InventorySystem_Item_Name." (".$Lang['General']['Chinese'].")</th>";
	$x .= "<th>".$i_InventorySystem_Item_Name." (".$Lang['General']['English'].")</th>";
	$x .= "<th>".$i_InventorySystem_Item_Location."</th>";
	$x .= "<th>".$Lang['RepairSystem']['ItemPhoto']."</th>";
	$x .= "</tr></thead>";
	for ($i = 0; $i < sizeOf($result); $i++) {
		$_itemId = $result[$i]['ItemID'];
		$_itemName = Get_Lang_Selection($result[$i]['InventoryItemName_b5'], $result[$i]['InventoryItemName_eng']);
		$_itemName = intranet_undo_htmlspecialchars($_itemName);
		$_itemName = str_replace(array("\\","'",'"'),array("\\\\","\\'","&quot;"),$_itemName);
		$photoPath=$result[$i]['PhotoPath'];
		$photoName=$result[$i]['PhotoName'];
		if ($photoPath && $photoName) {
			$photoPath=substr($photoPath,1);	// remove left /
			$inventoryPhoto = '<img src="'.$PATH_WRT_ROOT.$photoPath.$photoName.'" width="100px">';
			$paraInventoryPhoto = "<img src=\'".$PATH_WRT_ROOT.$photoPath.$photoName."\' width=\'100px\'>";
		}
		else {
			$inventoryPhoto = '-';
			$paraInventoryPhoto = '';
		}
		$dispName = $_itemName.' ('.$result[$i]['ItemCode'].')';
		
		$x .= "<tr>";
			//$x .= "<td>".$linterface->Get_Radio_Button("Inv_$i", 'InvMappingResult', $Value=$result[$i]['ItemID'], '' , $Class="InvMappingResult", '' , "", $isDisabled=0)."</td>";
			
			$x .= '<td><a href="javascript:void(0);" onclick="selectedResultItem(\''.$_itemId.'\', \''.$dispName.'\', \''.$paraInventoryPhoto.'\');">[Select]</a></td>';
			$x .= "<td>".$result[$i]['ItemCode']."</td>";
			$x .= "<td>".$result[$i]['InventoryItemName_b5']."</td>";
			$x .= "<td>".$result[$i]['InventoryItemName_eng']."</td>";
			$x .= "<td>".$result[$i]['LocationName']."</td>";
			$x .= "<td>".$inventoryPhoto."</td>";
	
		$x .= "</tr>";
	}
	$x .= "</table>";
}
else {
	$x .= $Lang['RepairSystem']['NoResults'];
}
$htmlAry['resultTable'] = $x;
?>
<script language="javascript">

	function selectedResultItem(parItemId, parName, photo) {
		$('input#eInventoryItemID').val(parItemId);
		$('span#mappedItemNameSpan').html(parName);
		$('#mappedInvPhotoDiv').html(photo);
		js_Hide_ThickBox();
	}
</script>
<?php echo $htmlAry['resultTable'] ?>