<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(isset($RecordID) && !is_array($RecordID)) {
	$temp = $RecordID;
	unset($RecordID);
	$RecordID = array();
	$RecordID[0] = $temp;

	if($additionRemark!="") {
		$back_url = "edit_update.php?RecordID=$temp&additionRemark=$additionRemark";	
	} else {
		$back_url = "edit.php?RecordID=$temp";
	}
} else {
	$back_url = "index.php?";	
}

if(sizeof($RecordID)==0) {
	header("Location: index.php");	
}

$rowAffected = $lrepairsystem->archiveRepairRecord($RecordID);

intranet_closedb();

if($rowAffected==sizeof($RecordID))
	$msg = "ArchivedSuccess";
else if ($rowAffected==0)
	$msg = "ArchivedUnsuccess";
else 
	$msg = "ArchivedPartiallySuccess";

header("Location: $back_url&xmsg=$msg&CatID=$CatID&Status=$Status&year=$year&month=$month&CompleteDate=$CompleteDate");

?>