<?php
/*
 *  Using:
 *
 *  purpose:    get group members by GroupID and return the selection list
 *
 *  2020-10-27 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$groupID = IntegerSafe($_POST['GroupID']);
$recordID = IntegerSafe($_POST['RecordID']);
$groupMemberAry = $lrepairsystem->getGroupMembers($groupID, $recordID);

$x = '<select name=AvailableSysPersonID[] id=AvailableSysPersonID style="min-width:200px; height:156px;" multiple>'."\n";

for ($i=0, $iMax=count($groupMemberAry); $i<$iMax; $i++) {
    $userID = $groupMemberAry[$i]['UserID'];
    $userName = $groupMemberAry[$i]['Name'];
    $x .= '<option value="' . $userID . '">' . $userName . '</option>'."\n";
}

$x .= '</select>'."\n";

echo $x;

?>