<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($RecordID)==0) {
	header("Location: index.php");	
}

if($archivePage)
	$back_url = "archive.php";
else 
	$back_url = "index.php";

$rowAffected = $lrepairsystem->removeRepairRecord($RecordID, $archivePage);

intranet_closedb();

if($rowAffected==sizeof($RecordID))
	$msg = "DeleteSuccess";
else if ($rowAffected==0)
	$msg = "DeleteUnsuccess";
else 
	$msg = "DeletePartiallySuccess";


header("Location: $back_url?xmsg=$msg&CatID=$CatID&Status=$Status&year=$year&month=$month");

?>