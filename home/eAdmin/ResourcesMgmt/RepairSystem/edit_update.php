<?php
# using: 

####### Change log [Start] #######
#
#   Date    :   2020-10-22  (Cameron)
#               - handle upload photo(s) [case #X190085]
#
#   Date    :   2020-09-21  (Henry)
#                - Bug fix for status change to cancelled when update PIC [Case#X196734]
#
#   Date    :   2020-07-03  (Cameron)
#               Fix: use transaction and return the real sql executed result
#               redirect to listview rather than current page after update data
#
#	Date	:	2017-06-05 	(Cameron)
#				Fix bug to support special characters like '"<>&\/ for FollowUpPerson
#
#	Date	:	2017-05-24  (Cameron)
#				move send email function to part of $lrepairsystem->sendNotifyToReporter
#
#	Date	: 	2016-06-30	(Cara)
#				added the part of change follow up person
#
######## Change log [End] ########

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/eClassAppConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}

$resultAry = array();
$returnMsg = '';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// handle file directory
if (!empty($TargetFolder) || !empty($_FILES)) {
    $libfilesystem = new libfilesystem();

    $FileFormatAry = $repair_conf['FileFormatAry'];

    $MaxFileSize = $repair_conf['MaxFileSize']; // MB
    $RestrictFileSize = is_numeric($MaxFileSize) && $MaxFileSize > 0;
    $MaxFileSizeInBytes = floatval($MaxFileSize) * 1024 * 1024;
    $RestrictFileFormat = count($FileFormatAry) > 0;

    # create folder
    $targetPath = "/repair/";
    $targetPath_2ndLevel = "/final/";
    $target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath);

    if ( !file_exists($target1stPath) || !is_dir($target1stPath)) {
        $resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
    }

    $target2ndPath =  str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath.$targetPath_2ndLevel);
    if ( !file_exists($target2ndPath) || !is_dir($target2ndPath)) {
        $resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
    }

    $targetFullPath = $target2ndPath;
    chmod($targetFullPath, 0777);
}


// handle uploaded photos
if (!empty($TargetFolder)) {	// by plupload method

    $tempPath = str_replace('//', '/', $PATH_WRT_ROOT."file/repair/temp/".$TargetFolder);

    $tempFileAry = $libfilesystem->return_folderlist($tempPath);

    $numOfFiles = count($tempFileAry);

    for($i=0;$i<$numOfFiles;$i++) {

        # get file name
        $origFileName = $libfilesystem->get_file_basename($tempFileAry[$i]);
        if (empty($origFileName)) continue;

        $fileSize = filesize($tempFileAry[$i]);

        # Check file size
        if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
            $resultAry[] = false;
            $returnMsg = 'FileSizeExceedLimit';
            continue;
        }

        # Extension
        $ext = substr($libfilesystem->file_ext($origFileName),1);

        # Check file format
        if($RestrictFileFormat && !in_array(strtolower($ext),$FileFormatAry)){
            $resultAry[] = false;
            $returnMsg = 'FileTypeIsNotSupported';
            continue;
        }

        $timestamp = date('YmdHis');

        # encode file name
        $targetFileHashName = "r".$RecordID."_".$timestamp.$i;
        $targetFile = $targetFullPath.$targetFileHashName;

        # upload file
        $uploadResult = $libfilesystem->lfs_copy($tempFileAry[$i], $targetFile);
        if(file_exists($targetFile)){
            chmod($targetFile, 0777);
        }

        # save to db
        unset($dataAry);
        $dataAry['RecordID'] = $RecordID;
        $dataAry['FileName'] = $origFileName;
        $dataAry['FileHashName'] = $targetFileHashName;
        $dataAry['SizeInBytes'] = $fileSize;
        $dataAry['RecordStatus'] = ($newStatus == 4) ? -4 : $newStatus;         // 4 is pending, -4 is used to distinguish from that uploaded by reporter
        $photoID = $lrepairsystem->insertRepairPhoto($dataAry);

    }

    // Do clean up
    $libfilesystem->deleteDirectory($tempPath);
    $sql = "DELETE FROM REPAIR_SYSTEM_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND Folder='".$TargetFolder."'";
    $lrepairsystem->db_db_query($sql);

}
else if (!empty($_FILES)){	// traditional method

    # get file name
    $tempFile = $_FILES['File']['tmp_name'];

    $origFileName =  $_FILES['File']['name'];
    if (!empty($origFileName)) {

        $fileSize = $_FILES['File']['size'];

        # Check file size
        if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
            $resultAry[] = false;
            $returnMsg = 'FileSizeExceedLimit';
        }

        # Extension
        $ext = substr($libfilesystem->file_ext($origFileName),1);

        # Check file format
        if($RestrictFileFormat && !in_array(strtolower($ext),$FileFormatAry)){
            $resultAry[] = false;
            $returnMsg = 'FileTypeIsBanned';
        }

        $timestamp = date('YmdHis');

        # encode file name
        $targetFileHashName = "r".$RecordID."_".$timestamp;
        $targetFile = $targetFullPath.$targetFileHashName;

        # upload file
        $uploadResult = move_uploaded_file($tempFile, $targetFile);

        chmod($targetFile, 0777);

        # save to db
        unset($dataAry);
        $dataAry['RecordID'] = $RecordID;
        $dataAry['FileName'] = $origFileName;
        $dataAry['FileHashName'] = $targetFileHashName;
        $dataAry['SizeInBytes'] = $fileSize;
        $dataAry['RecordStatus'] = ($newStatus == 4) ? -4 : $newStatus;
        $photoID = $lrepairsystem->insertRepairPhoto($dataAry);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$lrepairsystem->Start_Trans();

$messageWay = IntegerSafe($messageWay);

# change record status
if(isset($newStatus)){
    $resultAry[] = $lrepairsystem->changeRequestStatus($RecordID, $newStatus);
}

$FollowUpPerson = standardizeFormPostValue($FollowUpPerson);
#change follow up person
if ($_POST['selectFollowUpUser'] == 1){

    // delete follow-up person id first
    $resultAry[] = $lrepairsystem->deleteFollowUpPersonID($RecordID);

    // add follow-up person id
    foreach((array)$SysPersonID as $personID) {
        $resultAry[] = $lrepairsystem->insertFollowupPersonID($RecordID, $personID);
    }

//    $resultAry[] = $lrepairsystem->changeFollowUpPersonID($RecordID, $followUpUser);
    $resultAry[] = $lrepairsystem->resetFollowUpPerson($RecordID);
} else if ($_POST['selectFollowUpUser'] == 2){
    $resultAry[] = $lrepairsystem->changeFollowUpPerson($RecordID, $FollowUpPerson);

    $resultAry[] = $lrepairsystem->deleteFollowUpPersonID($RecordID);
//    $resultAry[] = $lrepairsystem->resetFollowUpPersonID($RecordID);
}

#change inventory item mapping result
if ($_POST['mappedInventoryItem'] == 1){
	if ($eInventoryItemID > 0){
        $resultAry[] = $lrepairsystem->changeInventoryItemID($RecordID, $eInventoryItemID);
	} else{
        $resultAry[] = $lrepairsystem->resetInventoryItemID($RecordID);
	}
} else if ($_POST['mappedInventoryItem'] == 0){
    $resultAry[] = $lrepairsystem->resetInventoryItemID($RecordID);
}

$dataAry = array();
if(isset($CompleteDate)){
    $dataAry['CompleteDate'] = $CompleteDate;
}
if(isset($FollowUpPerson)){
	$dataAry['FollowUpPerson'] = $lrepairsystem->Get_Safe_Sql_Query($FollowUpPerson);
}
$dataAry['LastFollowupGroupID'] = $GroupID;
$resultAry[] = $lrepairsystem->updateRepairRecord($RecordID, $dataAry);

if($additionRemark!="") {	# add remark
	$stdInfo = $lrepairsystem->getStudentNameByID($UserID);
	$userName = $stdInfo[0][1];

    $resultAry[] = $lrepairsystem->addRecordRemark($RecordID, $additionRemark, $userName." (".$i_Sports_AdminConsole_UserType['ADMIN'].")");
}


if($returnMsg != '') {
    $returnMsgKey = $returnMsg;
} else {
    if (!in_array(false, $resultAry)) {
        $lrepairsystem->Commit_Trans();
        $informReporter = IntegerSafe($_POST['InformReporter']);
        $informFollowupPerson = IntegerSafe($_POST['InformFollowupPerson']);
        if ( $informReporter == 1 || $informFollowupPerson == 1) {
//        if ($submitMode == 'submitAndNotify') {
            $notifyResult = $lrepairsystem->sendNotifyToRelatedPerson($RecordID, $informReporter, $informFollowupPerson, $messageWay);
            $returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
        } else {
            $returnMsgKey = 'UpdateSuccess';
        }
    } else {
        $lrepairsystem->RollBack_Trans();
        $returnMsgKey = 'UpdateUnsuccess';
    }
}
//# if completed and send email to reporter
//if($newStatus==REPAIR_RECORD_STATUS_COMPLETED && $EmailToReporter==1) {
//
//	$recordData = $lrepairsystem->getRepairSystemEmailContent($RecordID);
//
//	if(count($recordData)>0) {
//		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
//		include_once($PATH_WRT_ROOT."lang/email.php");
//		$lwebmail = new libwebmail();
//
//		$receiver[] = $recordData['UserID'];
//		$subject = $Lang['RepairSystem']['RequestCompleted'];
//		$content =
//			$Lang['RepairSystem']['RequestDate']." : ".substr($recordData['DateInput'],0,10)."<br>".
//			$Lang['RepairSystem']['Category']." : ".$recordData['CatName']."<br>".
//			$Lang['RepairSystem']['Location']." : ".$recordData['LocationName']."<br>".
//			$Lang['RepairSystem']['RequestSummary']." : ".$recordData['Title']."<br>".
//			$Lang['RepairSystem']['RequestDetails']." : ".$recordData['Content']."<br>".
//			$Lang['RepairSystem']['MgmtGroup']." : ".$recordData['GroupTitle']."<br>".
//			$Lang['General']['Remark']." : ".($recordData['Remark']=="" ? $Lang['General']['EmptySymbol'] : $recordData['Remark'])."<br>".
//			$Lang['General']['Status2']." : ".$Lang['General']['Completed'];
//
//		$exmail_success = $lwebmail->sendModuleMail($receiver, $subject, $content, 1);
//	}
//}

intranet_closedb();

header("Location: index.php?returnMsgKey=".$returnMsgKey);
//header("Location: edit.php?xmsg=UpdateSuccess&RecordID=$RecordID&CatID=$CatID2&Status=$Status&year=$year&month=$month");

?>