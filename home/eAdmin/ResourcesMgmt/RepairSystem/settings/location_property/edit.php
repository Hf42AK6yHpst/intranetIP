<?php
// using :   
/*
 *  2020-08-22 Cameron
 *      - copy from eBooking and modify
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("targetBuilding", "targetBuilding");
$arrCookies[] = array("targetFloor", "targetFloor");
$arrCookies[] = array("targetRoom", "targetRoom");
$arrCookies[] = array("pageNo_RepairSystem_Settings_Room_View", "pageNo_RepairSystem_Settings_Room_View");
$arrCookies[] = array("numPerPage_RepairSystem_Settings_Room_View", "numPerPage_RepairSystem_Settings_Room_View");

if(isset($clearCoo) && $clearCoo == 1)
{
	if(isset($_COOKIE['targetBuilding'])) {
		$_COOKIE['targetBuilding'] = "";
		$targetBuilding = "";
	}
	if(isset($_COOKIE['targetFloor'])) {
		$_COOKIE['targetFloor'] = "";
		$targetFloor = "";
	}
	if(isset($_COOKIE['targetRoom'])) {
		$_COOKIE['targetRoom'] = "";
		$targetRoom = "";
	}
	if(isset($_COOKIE['numPerPage_RepairSystem_Settings_Room_View'])) {
		$_COOKIE['numPerPage_RepairSystem_Settings_Room_View'] = "";
		$numPerPage = "";
	}
	if(isset($_COOKIE['pageNo_RepairSystem_Settings_Room_View'])) {
		$_COOKIE['pageNo_RepairSystem_Settings_Room_View'] = "";
		$pageNo = "";
	}
	if(isset($_POST['pageNo_RepairSystem_Settings_Room_View'])) {
		$_POST['pageNo_RepairSystem_Settings_Room_View'] = "";
		$pageNo = "";
	}
}
else
{
	// for click 'Next page' & 'Prev page' use
	if(isset($_POST['pageNo_RepairSystem_Settings_Room_View'])) {
		$pageNo_RepairSystem_Settings_Room_View = $_POST['pageNo_RepairSystem_Settings_Room_View'];
		$pageNo = $_POST['pageNo_RepairSystem_Settings_Room_View'];
	} else {
		$pageNo_RepairSystem_Settings_Room_View = $_COOKIE['pageNo_RepairSystem_Settings_Room_View'];
		$pageNo = $_COOKIE['pageNo_RepairSystem_Settings_Room_View'];
	}

	if(isset($_POST['numPerPage_RepairSystem_Settings_Room_View'])) {
		$numPerPage_RepairSystem_Settings_Room_View = $_POST['numPerPage_RepairSystem_Settings_Room_View'];
		$numPerPage = $_POST['numPerPage_RepairSystem_Settings_Room_View'];
	} else {
		$numPerPage_RepairSystem_Settings_Room_View = $_COOKIE['numPerPage_RepairSystem_Settings_Room_View'];
		$numPerPage = $_COOKIE['numPerPage_RepairSystem_Settings_Room_View'];
	}
		
	if(isset($_COOKIE['targetRoom']))
	{
		if($_COOKIE['targetRoom'] === "undefined")
		{
			$targetRoom = "";
		}
	}
	
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$linterface 	= new interface_html();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$targetBuilding = (isset($_POST['targetBuilding'])) ? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor'])) ? $_POST['targetFloor'] : $targetFloor;
$targetRoom		= (isset($_POST['targetRoom'])) ? $_POST['targetRoom'] : $targetRoom;
$action			= (isset($action) && $action != "") ? $action : "";

# Top menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageLocationProperties";

$TAGS_OBJ[] = array($Lang['RepairSystem']['LocationProperties'], "", 0);
# Left menu
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
# Start layout
$linterface->LAYOUT_START();

$arrBuilding = $lrepairsystem->Get_All_Building_Array();
$building_selection = getSelectByArray($arrBuilding, " name='targetBuilding' id='targetBuilding' onChange='javascript: Reload_Location_Selection(\"Floor\",this.value)' DISABLED",$targetBuilding,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Building']." -- ");
$building_filter .= "<div id='BuildingDiv' style='display: inline;'>".$building_selection."</div>";

$floor_filter .= "<div id='FloorDiv' style='display: inline;'>".$floor_selection."</div>";

$room_filter .= "<div id='RoomDiv' style='display: inline;'>".$room_selection."</div>";

$ModeArr = '';
$ModeArr[] = array($Lang['Btn']['View'], "icon_view.gif", "javascript:Go_View_Mode();", 0);
$ModeArr[] = array($Lang['Btn']['Edit'], "icon_edit_b.gif", "", 1);
$mode_toolbar = $lrepairsystem->Get_Mode_Toolbar($ModeArr);

$toolbar .= "<table width='96%' border='0' cellpadding='3' cellspacing='0'>";
$toolbar .= "<tr>";
$toolbar .= "<td align='left'>".$building_filter.$floor_filter.$room_filter."</td>";
$toolbar .= "</tr>";
$toolbar .= "</table>";

$table_content .= $lrepairsystem->initJavaScript();
$table_content .= $lrepairsystem->getRoomPropertyEditTable($targetBuilding,$targetFloor,$targetRoom,$pageNo,$numPerPage);
?>

<script language="javascript">
	
	var targetBuilding = "<?=$targetBuilding;?>";
	var targetFloor = "<?=$targetFloor;?>";
	var targetRoom = "<?=$targetRoom;?>";
	var action = "<?=$action;?>";
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "targetBuilding";
	arrCookies[arrCookies.length] = "targetFloor";
	arrCookies[arrCookies.length] = "targetRoom";
	arrCookies[arrCookies.length] = "pageNo_RepairSystem_Settings_Room_View";
	arrCookies[arrCookies.length] = "numPerPage_RepairSystem_Settings_Room_View";
	
	$(document).ready( function() {	
		SetCookies();
		if(action != "") {
			Reload_Location_Selection("Floor",targetBuilding,targetFloor);
			Reload_Location_Selection("Room",targetFloor,targetRoom);
		}else{
			Reload_Location_Selection("Floor",targetBuilding,targetFloor);
			Reload_Location_Selection("Room",targetFloor,targetRoom);
		}
	});
	
	function ShowHideLayers(div_name,act)
	{
		if(act == "show"){
			$("#"+div_name).show();
		}else{
			$("#"+div_name).hide();
		}
	}
	
	function js_Show_Detail_Layer(jsDetailType, jsSubLocationID, jsClickedObjID)
	{
		js_Hide_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'ShowAllIsShowRoom'){
			jsAction = 'ShowEdit_AllIsShowRoom';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowIsShowRoom'){
			jsAction = 'ShowEdit_IsShowRoom';
			jsOffsetLeft = 360;
			jsOffsetTop = -25;
		}
		else if(jsDetailType == 'ShowLocationDescription'){
			jsAction = 'ShowLocationDescription';
			jsOffsetLeft = 0;
			jsOffsetTop = -10;
		}
		
		$('div#DetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>');
		
		js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop);
		MM_showHideLayers('DetailsLayer','','show');
				
		$('div#DetailsLayerContentDiv').load(
			"ajax_reload.php", 
			{ 
				Action: jsAction,
				SubLocationID: jsSubLocationID
			},
			function(returnString)
			{
				$('div#DetailsLayerContentDiv').css('z-index', '999');
				$('div#DetailsLayerContentDiv').css('overflow', 'auto');
				
				if($('div#DetailsLayerContentDiv').height() > 400)
				{
					$('div#DetailsLayerContentDiv').height(400);
				}
				else
				{
					$('div#DetailsLayerContentDiv').css('height', 'auto');
				}
			}
		);
	}
	
	function js_Hide_Detail_Layer()
	{
		MM_showHideLayers('DetailsLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop){
		var ClickedLeft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft');
		<?php
		if($sys_custom['PowerClass']){
		?>
			var posleft = ClickedLeft-400;
		<?php
		}else{
		?>
			var posleft = (ClickedLeft+400)>$(window).width()? ($(window).width()-400) : ClickedLeft;
		<?php	
		}	
		?>
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
		
		document.getElementById('DetailsLayer').style.left = posleft + "px";
		document.getElementById('DetailsLayer').style.top = postop + "px";
		document.getElementById('DetailsLayer').style.visibility = 'visible';
	}
	
	function Go_View_Mode()
	{
		$('#targetBuilding').attr('disabled',false);
		$('#targetFloor').attr('disabled',false);
		$('#targetRoom').attr('disabled',false);
		
		var targetBuilding = $('#targetBuilding').val();
		var targetFloor = $('#targetFloor').val();
		
		if($('#targetFloor').length != 0)
			var targetFloor = $('#targetFloor').val();
		else
			var targetFloor = "#";
		
		if($('#targetRoom').length != 0)
			var targetRoom = $('#targetRoom').val();
		else
			var targetRoom = "#";
		
		// Set the Cookies
		AssignCookies();
		
		document.form1.action = "index.php";
		document.form1.submit();
	}
	
	function Reload_Location_Selection(ModuleName,val,selected_val)
	{
		selected_val = selected_val || '';
		
		// Set Cookies
		SetCookies();
		AssignCookies();
		
		Block_Document();
		
		if(ModuleName == "Floor")
		{
			$.post(
				"ajax_load_location_selection.php",
				{
					"ModuleName" : ModuleName,
					"targetBuilding" : val,
					"targetFloor" : selected_val,
					"disabled" : 1
				},
				function(responseText){
					$("#FloorDiv").html(responseText);
					UnBlock_Document();
					initThickBox();
				}
			);
		}
		else if(ModuleName == "Room")
		{
			if($('#targetRoom').length != 0)
				$('#targetRoom').val('');
			var targetBuilding = $('#targetBuilding').val();
			$.post(
				"ajax_load_location_selection.php",
				{
					"ModuleName" : ModuleName,
					"targetBuilding" : targetBuilding,
					"targetFloor" : val,
					"targetRoom" : selected_val,
					"disabled" : 1
				},
				function(responseText){
					$("#RoomDiv").html(responseText);
					UnBlock_Document();
					initThickBox();
				}
			);
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				//alert($('#'+arrCookies[i]).val());
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				//alert($.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}

	// AJAX function - reload the Room Property Edit View Table
	function reloadMainContent(targetBuilding, targetFloor, targetRoom, pageNo, numPerPage, Order, SortField)
	{
		targetBuilding = targetBuilding || $('#targetBuilding').val();
		targetFloor = targetFloor || $('#targetFloor').val();
		
		if($('#pageNo_RepairSystem_Settings_Room_View').length)
			pageNo = pageNo || $('#pageNo_RepairSystem_Settings_Room_View').val();
		else
			pageNo = 1; 
			
		if($('#numPerPage_RepairSystem_Settings_Room_View').length)	
			numPerPage = numPerPage || $('#numPerPage_RepairSystem_Settings_Room_View').val();
		else
			numPerPage = 20;
		
		if($('#targetRoom').length == 0) {
			targetRoom = targetRoom || '';
		} else {
			targetRoom = targetRoom || $('#targetRoom').val();
		}
		
		AssignCookies();
		SetCookies();
		
		if(targetBuilding == ""){
			$.cookies.del("targetFloor");
			$.cookies.del("targetRoom");
		}
		
		Block_Document();
		$.post(
			"ajax_load_room_property_edit_view.php",
			{
				"targetBuilding":targetBuilding,
				"targetFloor":targetFloor,
				"targetRoom":targetRoom,
				"pageNo":pageNo, 
				"numPerPage":numPerPage
			},
			function(responseText){
				$('#room_permission_edit_view').html(responseText);
				UnBlock_Document();
				initThickBox();
			}
		);
	}
	
	function ApplyToAll(div_name)
	{
		
		if(div_name == "All_IsShowRoom")
		{
			if(! $('input[name=All_IsShowRoom]').is(':checked') ) {
				alert("<?=$Lang['RepairSystem']['Settings']['PleaseSelectOneOption'];?>");
				return false;
			}
			$.post(
				"ajax_update_all_IsShowRoom.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : $('#targetRoom').val(),
					"IsShow" : $('input[name=All_IsShowRoom]:checked').val(),
					"displayedSubLocationIdList" : $('input#displayedSubLocationIdList').val()
				},
				function(responseText){
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		} 
		js_Hide_Detail_Layer();
	}

	function ApplyToSelected(div_name)
	{
		var _locationID;
		var selectedLocationID = '';
		
		$('input.locationChk:checked').each(function(){
			_locationID = $(this).attr('id').replace('locationChk_', '');
			if (selectedLocationID != '') {
				selectedLocationID += ',' + _locationID;
			}
			else {
				selectedLocationID = _locationID;
			}
		});

		if (selectedLocationID == '') {
			alert("<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>");
			return false;
		}
		
		if(div_name == "SelectedIsShowRoom")
		{
			if(! $('input[name=All_IsShowRoom]').is(':checked') ) {
				alert("<?=$Lang['RepairSystem']['Settings']['PleaseSelectOneOption'];?>");
				return false;
			}
			
			$.post(
				"ajax_update_all_IsShowRoom.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : $('#targetRoom').val(),
					"IsShow" : $('input[name=All_IsShowRoom]:checked').val(),
					"displayedSubLocationIdList" : selectedLocationID
				},
				function(responseText){
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		} 
		js_Hide_Detail_Layer();
	}
	
	function ApplyToSelectedLocation(ModuleName,targetLocation)
	{
		if(ModuleName == 'IsShowRoom')
		{		
			if(! $('input[name=IsShowRoom_'+targetLocation+']').is(':checked') ) {
				alert("<?=$Lang['RepairSystem']['Settings']['PleaseSelectOneOption'];?>");
				return false;
			}
			
			$.post(
				"ajax_update_all_IsShowRoom.php",
				{
					"targetBuilding" : $('#targetBuilding').val(),
					"targetFloor" : $('#targetFloor').val(),
					"targetRoomID" : targetLocation,
					"IsShow" : $('input[name=IsShowRoom_'+targetLocation+']:checked').val()
				},
				function(responseText){
					if(responseText == 0){
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}else {
						msg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						Get_Return_Message(msg);
						reloadMainContent($('#targetBuilding').val(),$('#targetFloor').val(),$('#targetRoom').val());
					}
				}
			);
		}
		js_Hide_Detail_Layer();
	}
	
</script>

<form name="form1" action="edit.php" method="POST" >
	<br>
	<?=$toolbar;?>
	<div id="room_permission_edit_view">
		<?=$table_content;?>
	</div>
	
	<div id="DetailsLayer" class="selectbox_layer" style="visibility:hidden; width:400px; overflow:auto;">
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				<tr>
					<td align="left" style="border-bottom: medium none;">
						<div id="DetailsLayerContentDiv"></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
?>