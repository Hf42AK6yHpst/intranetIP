<?php
/*
 * 	2017-06-13 Cameron
 * 		- add header characterset
 * 
 *	2017-05-26 Cameron 
 *		- create this file
 */
  
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$groupID = $_REQUEST['GroupID'];

$grouName = $lrepairsystem->getInventoryGroupName($groupID);
$PAGE_NAVIGATION[] = array($i_InventorySystem['Caretaker'], "");
$PAGE_NAVIGATION[] = array(intranet_htmlspecialchars($grouName), "");
$PAGE_NAVIGATION[] = array($i_InventorySystem_Group_Member, "");

$rs = $lrepairsystem->getInventoryGroupMember($groupID);

$htmlAry['closeBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$i = 0;

$characterset = ($junior_mck) ? 'big5' : 'utf-8';
header('Content-Type: text/html; charset='.$characterset);

?> 

<script language="javascript">


	
</script>
<br />
<form name="form1" method="post" action="">

<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<br>

<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<? if (count($rs)):?>
	<tr class="tabletop">
		<th class="tabletoplink" width="1">#</th>
		<th class="tabletoplink" width="95%"><?=$i_InventorySystem_Group_MemberName?></th>
	</tr>
	<? foreach((array)$rs as $r):
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$i++;
?>		
		<tr class="<?=$css?>">
			<td class="tabletext"><?=$i?></td>
			<td class="tabletext"><?=$r['UserName']?></td>
		</tr>
	<? endforeach;?>
<? else:?>
	<tr class="tablerow2 tabletext">
		<td class="tabletext" colspan="2" align="center"><?=$i_no_record_exists_msg?></td>
	</tr>
<? endif;?>	
</table>

<div class="table_board">
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['closeBtn']?>
		<p class="spacer"></p>
	</div>
</div>

</form>
<?php

//$linterface->LAYOUT_STOP();
intranet_closedb();

?>