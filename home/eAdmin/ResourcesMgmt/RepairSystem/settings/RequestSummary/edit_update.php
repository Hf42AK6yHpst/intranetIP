<?php 

##########################################
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}

$Title2 = intranet_htmlspecialchars($RequestSummaryTitle);
# check duplicate within same Category
$dup_sql = "select count(*) from REPAIR_SYSTEM_REQUEST_SUMMARY where Title = '$Title2' and CategoryID=$CatID and RecordID<>$RecordID";
$result = $lrepairsystem->returnVector($dup_sql);

if(!$result[0])
{			
	# retrieve orginal CatID and Title
	$sql1 = "select CategoryID, Title from REPAIR_SYSTEM_REQUEST_SUMMARY where RecordID=$RecordID";
	$result1 = $lrepairsystem->returnArray($sql1);
	$Old_CatID = $result1[0]['CategoryID'];
	$Old_Title = $result1[0]['Title'];
	
	# update existing records
	$sql2 = "update REPAIR_SYSTEM_RECORDS set CategoryID=$CatID, Title='$Title2' where CategoryID=$Old_CatID and Title='$Old_Title'";
	$lrepairsystem->db_db_query($sql2);
	
	# update Request Summary Settings
	$dataAry['CategoryID'] = $CatID;
	$dataAry['Title'] = $Title2;
	$dataAry['RecordStatus'] = $in_use;
	
	$result = $lrepairsystem->updateRequestSummary($RecordID, $dataAry);
	$xmsg = "UpdateSuccess";
	
}
else
{
	$xmsg = "RecordDuplicated";
}


intranet_closedb();

header("Location: index.php?CatID=$CatID&in_use=$in_use&xmsg=$xmsg");

?>