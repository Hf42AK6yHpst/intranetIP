<?php 

##########################################
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}


$Title2 = intranet_htmlspecialchars($RequestSummaryTitle);
# check duplicate within same Category
$dup_sql = "select count(*) from REPAIR_SYSTEM_REQUEST_SUMMARY where Title = '$Title2' and CategoryID=$CatID";
$result = $lrepairsystem->returnVector($dup_sql);

if(!$result[0])
{			
	# retrieve Sequence
	$sql2 = "select max(Sequence) from REPAIR_SYSTEM_REQUEST_SUMMARY where CategoryID=$CatID";
	$result2 = $lrepairsystem->returnVector($sql2);
	$Sequence = $result2[0]+1;
	 
	$dataAry['CategoryID'] = $CatID;
	$dataAry['Title'] = $Title2;
	$dataAry['RecordStatus'] = $in_use;
	$dataAry['Sequence'] = $Sequence;
	
	$result = $lrepairsystem->insertRequestSummary($dataAry);
	$xmsg = "AddSuccess";
}
else
{
	$xmsg = "RecordDuplicated";
} 
intranet_closedb();

header("Location: index.php?CatID=$CatID&in_use=$in_use&xmsg=$xmsg"); 

?>