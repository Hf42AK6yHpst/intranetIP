<?php 
/*
 * 	2017-06-01 Cameron
 * 		- apply standardizeFormPostValue to keyword
 * 		- set action to self so that previous action message disappear when trigger other action (e.g. sorting after edit)
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php"); 

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem(); 

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$arrCookies[] = array("ck_request_summary_keyword", "keyword");
$arrCookies[] = array("ck_request_summary_in_use", "in_use");
$arrCookies[] = array("ck_request_summary_catid", "CatID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}
if(!isset($in_use)) $in_use = -1;

$linterface = new interface_html();

$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageRequestSummary";
$TAGS_OBJ[] = array($Lang['RepairSystem']['RequestSummary']);
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]); 

## add/edit/delte btn
$add_btn ='<a href="new.php?CatID='.$CatID.'&in_use='.$in_use.'" class="new"> '.$button_new.'</a>';
$edit_btn = '<a href="javascript:checkEdit(document.form1,\'RecordID[]\',\'edit.php\')" title="'. $button_edit.'" class="tool_edit">'.$button_edit.'</a>';
$del_btn = '<a href="javascript:removeRecord(document.form1,\'RecordID[]\',\'remove_update.php\')" title="'. $button_delete.'" class="tool_delete">'.$button_delete.'</a>';

$CategorySelect = $lrepairsystem->getCategorySelection($CatID, " onChange='document.form1.submit()'", 1, "",1);

$StatusArray[] = array(-1, $Lang['RepairSystem']['AllInUseStatus']);
$StatusArray[] = array(1, $Lang['SysMgr']['FormClassMapping']['InUse']);
$StatusArray[] = array(0, $Lang['SysMgr']['FormClassMapping']['NotInUse']);

$in_use_selection = $linterface->GET_SELECTION_BOX($StatusArray, " name='in_use' id='in_use' onChange='document.form1.submit()' ", "", $in_use);

$keyword = standardizeFormPostValue($keyword);
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". intranet_htmlspecialchars($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

																	
?>
<script language="javascript">
<!--
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function removeRecord(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$Lang['RepairSystem']['DeleteRequestSummaryConfirm']?>")){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}

function change_order(o, id)
{
	$('#div_content').load(
		'ajax_reloadTable.php', 
		{move: o, 
		RecordID:id,
		CatID:'<?=$CatID?>',
		in_use:'<?=$in_use?>',
		keyword:'<?=$keyword?>'}, 
		function (data){
		}); 
} 
//-->
</script>

<form name="form1" method="post" action="index.php">

<div class="content_top_tool">
        <div class="Conntent_tool"><?=$add_btn?></div>
        <div class="Conntent_search"><?=$searchTag?></div>
<br style="clear:both" />
</div>
    
<div class="table_board">
 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<div class="table_filter"><?=$CategorySelect?> <?=$in_use_selection?></div>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
			<?=$edit_btn?>
			<?=$del_btn?>
		</div>
	</td>
</tr>
</table>

<div id="div_content">

<?=$lrepairsystem->displayRequestSummary_ui($CatID, $in_use, $keyword);?>

</div>

<?//= $li->display() ?>


<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				

</div>

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>