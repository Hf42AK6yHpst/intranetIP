<?php 

##########################################
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}

if(!empty($RecordID))
{
	$RecordID_str = implode(",",$RecordID);
	$sql = "delete from REPAIR_SYSTEM_REQUEST_SUMMARY where RecordID in (". $RecordID_str .")";
	$lrepairsystem->db_db_query($sql);
	$xmsg = "DeleteSuccess";
}
else
{
	$xmsg = "DeleteUnsuccess";
}

intranet_closedb();

header("Location: index.php?xmsg=$xmsg");

?>