<?php 
# using: 

##########################################
#
#	Date:	2017-05-31 [Cameron]
#			Fix: disable button before submit to avoid adding duplicate record
#
#	Date:	2014-11-06 [Omas]
#			Add new column, CategoryCasePrefix for generating Case Number
#
#	Date:	2010-10-27 [YatWoon]
#			set default of "Request Details" (flag: $sys_custom['RepairSystem_DefaultDetailsContent'])
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$linterface = new interface_html();

$groupSelection = $lrepairsystem->getGroupSelection();

# Top menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageCategorySettings";

$TAGS_OBJ[] = array($Lang['RepairSystem']['Category']);

$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['CategoryList'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['CategoryNew'], "");

# Left menu 
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function checkForm(form1) {
	if(form1.CategoryName.value=='')	 {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['RepairSystem']['CategoryName'] ?>");	
		form1.CategoryName.focus();
		return false;
	} else if(form1.CaseNumberPrefix.value=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['RepairSystem']['CategoryPrefix']['Title']?>");
		form1.CaseNumberPrefix.focus();
		return false;
	} else if(!validatePrefix(form1.CaseNumberPrefix.value)){
		alert("<?=$Lang['RepairSystem']['CategoryPrefix']['Title'].' '.$Lang['RepairSystem']['CategoryPrefix']['Alert']?>");
		form1.CaseNumberPrefix.focus();
		return false;
	} else if(form1.CaseNumberPrefix.value.length > 4) {
		alert("<?=$Lang['RepairSystem']['CategoryPrefix']['Title'].' '.$Lang['RepairSystem']['CategoryPrefix']['Alert2']?>");
		form1.CaseNumberPrefix.focus();
		return false;
	} else if(form1.GroupID.value=="") {
		alert("<?= $i_alert_pleaseselect.' '.$Lang['RepairSystem']['ResponsibleGroup'] ?>");
		form1.GroupID.focus();
		return false;
	} else {
		return true;
	}
}

function validatePrefix(str){
        var RE1 = new RegExp("[^a-zA-Z]");
        if (RE1.test(str)){
                return false;
        }else{
                return true;
        }
}

$(document).ready(function(){

	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

		if (checkForm(document.form1)) {
			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

});

//-->
</script>


<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" id="form1" method="post" action="new_update.php">

<table width="90%" align="center" border="0">
<tr><td>

	<table class="form_table_v30">
	<tr valign="top">
		<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['RepairSystem']['Name']?></td>
		<td><input name="CategoryName" type="text" class="textboxtext" value=""></td>
	</tr>
	<tr valign="top">
		<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['RepairSystem']['CategoryPrefix']['Title'].'&nbsp;'?>
		<span class="tabletextremark"><?=$Lang['RepairSystem']['CategoryPrefix']['Remarks']?></span>
		</td>
		<td><input name="CaseNumberPrefix" type="text" class="textboxtext" value=""></td>
	</tr>	
	<tr valign="top">
		<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['RepairSystem']['ResponsibleGroup']?></td>
		<td><?=$groupSelection?></td>
	</tr>
	
	<? if($sys_custom['RepairSystem_DefaultDetailsContent']) {?>
	<tr valign="top">
		<td class='field_title'><?=$Lang['RepairSystem']['DefaultDetailsContent']?></td>
		<td><?=$linterface->GET_TEXTAREA("DetailsContent", $DetailsContent)?></td>
	</tr>
	<? } ?>
	
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "", "btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'", "btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
	</div>	

</td></tr>
</table>

</form>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.CategoryName"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>