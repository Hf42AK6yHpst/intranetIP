<?php 

##########################################
#	Date:	2014-11-06 [Omas]
#			Add new column, CategoryCasePrefix for generating Case Number
#
#	Date:	2010-10-27 [YatWoon]
#			set default of "Request Details" (flag: $sys_custom['RepairSystem_DefaultDetailsContent'])
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
}

$dataAry['Name'] = intranet_htmlspecialchars($CategoryName);
$dataAry['CaseNumberPrefix'] = $CaseNumberPrefix;
$dataAry['GroupID'] = $GroupID;

if($sys_custom['RepairSystem_DefaultDetailsContent'])
	$dataAry['DetailsContent'] = intranet_htmlspecialchars($DetailsContent);

$result = $lrepairsystem->insertCategory($dataAry);

intranet_closedb();

header("Location: index.php?xmsg=add");

?>