<?php
# modifying : 
/*
 * 	Note: map single item only
 * 
 * 	2017-06-05 Cameron
 * 		- also show item code next to item name when return
 * 
 * 	2017-05-31 Cameron
 *		- retrieve inventory photo 
 *		- fix bug to handle special characters for ItemName 
 * 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

$RecordID = IntegerSafe(standardizeFormPostValue($_POST['RecordID']));
$passingMark = 40;


intranet_auth();
intranet_opendb(); 
$linterface = new interface_html();
$lrepairsystem = new librepairsystem();


$sql= 

		"SELECT 
				a.NameChi,
				a.NameEng,
				g.LocationID, 
				LocLevel.LocationLevelID, 
				LocBuilding.BuildingID,
				REC.RecordID,
				REC.DetailsLocation,
				a.ItemCode,
				a.ItemID,
				p.PhotoPath,
				p.PhotoName
		 FROM 
				INVENTORY_ITEM AS a 
				INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS d ON (a.ItemID = d.ItemID) 
				INNER JOIN INVENTORY_LOCATION AS g ON (d.LocationID = g.LocationID) 
				INNER JOIN INVENTORY_LOCATION_LEVEL AS LocLevel ON (g.LocationLevelID = LocLevel.LocationLevelID) 
				INNER JOIN INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocLevel.BuildingID = LocBuilding.BuildingID)
				INNER JOIN REPAIR_SYSTEM_RECORDS AS REC ON (REC.LocationBuildingID=LocBuilding.BuildingID) and (REC.LocationLevelID=LocLevel.LocationLevelID) and (REC.LocationID=g.LocationID)
				LEFT JOIN INVENTORY_PHOTO_PART p ON p.ItemID=a.ItemID
		 WHERE
				REC.RecordID = '$RecordID' 
				
";


$result = $lrepairsystem->returnArray($sql);
$DetailsLocation=$result[0]['DetailsLocation'];
//  debug_pr($result);
$resultlength=sizeof($result);


	
$htmlAry['mappedInventoryItemRadio'] = $linterface->Get_Radio_Button('mappedInventoryItemRadio', 'mappedInventoryItem', $Value=$InventoryItemID, !$hasMappedInventoryItem, $Class="","", "clickedMappedInventoryItemRadio(this.value);", $isDisabled=0);


$html .= "<table width='95%' class='common_table_list'>";

$html .= "<thead><tr>";
$html .= "<th>select</th>";
$html .= "<th>$i_InventorySystem_Item_Code</th>";
$html .= "<th>".$i_InventorySystem_Item_Name."(".$Lang['General']['Chinese'].")</th>";
$html .= "<th>".$i_InventorySystem_Item_Name."(".$Lang['General']['English'].")</th>";
$html .= "<th>".$Lang['RepairSystem']['ItemPhoto']."</th>";
$html .= "</tr></thead>";



if ($DetailsLocation !== '' ){
	$itemScoreAssoAry = array();
	$DetailsLocationLanguage = str_lang($DetailsLocation);
//		debug_pr($DetailsLocationLanguage);
		for ($i = 0; $i < $resultlength; $i++) {
	//	
		if ($DetailsLocationLanguage =='CHI'){
			similar_text($DetailsLocation,$result[$i]['NameChi'],$p);
		}
		elseif($DetailsLocationLanguage =='ENG') {
			similar_text($DetailsLocation,$result[$i]['NameEng'],$p);
		}
		else
			{
				# mixed case!
				$itemName = Get_Lang_Selection($result[$i]['NameChi'],$result[$i]['NameEng']);
				similar_text($DetailsLocation, $itemeName, $p_full);
				
				similar_text(substr_lang($DetailsLocation, "ENG"), $result[$i]['NameEng'], $p_eng);
				similar_text(substr_lang($DetailsLocation, "CHI"), $result[$i]['NameChi'], $p_chi);
				$p = ($p_full>$p_eng)? $p_full : $p_eng;
				$p = ($p>$p_chi)? $p : $p_chi;
				
				
			}
	//		debug_pr($p);
		if ($p >= $passingMark) {
			$itemScoreAssoAry[$p][] = $result[$i];
			
			$InventoryItemCodeGreater[$i]=$result[$i]['ItemCode'];
		}
	}
			ksort($itemScoreAssoAry, SORT_NUMERIC);
			
			$itemScoreAssoAry = array_reverse($itemScoreAssoAry);
			
		//	debug_pr($itemScoreAssoAry);
	
	$numOfGreater = count($InventoryItemCodeGreater);
 	if ($numOfGreater == 0) {
 			$html ='<p>';
 			$html .=	$Lang['RepairSystem']['NoResults'];
 			$html .='</p>';										
	}
	else {
		foreach ((array)$itemScoreAssoAry as $_score => $_itemAry) {
 			$_numOfItem = count($_itemAry);
 			//	debug_pr($_itemAry);
 			
			for ($j=0;$j<$_numOfItem;$j++) {
				//debug_pr($_itemAry);
 				$itemChiName = $_itemAry[$j]['NameChi'];
 				$itemEngName = $_itemAry[$j]['NameEng'];	
 				$itemName = Get_Lang_Selection($_itemAry[$j]['NameChi'],$_itemAry[$j]['NameEng']);
 				$itemName = intranet_undo_htmlspecialchars($itemName);
 				$itemName = str_replace(array("\\","'",'"'),array("\\\\","\\'","&quot;"),$itemName);

 				//	$itemName = $_itemAry[$j]['InventoryName'];
				$itemCode=$_itemAry[$j]['ItemCode'];
				$itemID=$_itemAry[$j]['ItemID'];
				$photoPath=$_itemAry[$j]['PhotoPath'];
				$photoName=$_itemAry[$j]['PhotoName'];
				if ($photoPath && $photoName) {
					$photoPath=substr($photoPath,1);	// remove left /
					$inventoryPhoto = '<img src="'.$PATH_WRT_ROOT.$photoPath.$photoName.'" width="100px">';
					$paraInventoryPhoto = "<img src=\'".$PATH_WRT_ROOT.$photoPath.$photoName."\' width=\'100px\'>";
				}
				else {
					$inventoryPhoto = '-';
					$paraInventoryPhoto = '';
				}
				$dispName = $itemName.' ('.$itemCode.')'; 
				
				$html .='<tr>';
				$html .='<td><a href="javascript:void(0);" onclick="selectedResultItem(\''.$itemID.'\', \''.$dispName.'\', \''.$paraInventoryPhoto.'\');">[Select]</a></td>';
				$html .='<td>';
				$html .=$itemCode;
				$html .='</td>';
				$html .='<td>';
				$html .=$itemChiName;
				$html .='</td>';
				$html .='<td>';
				$html .=$itemEngName;
				$html .='</td>';
				$html .='<td>';
				$html .=$inventoryPhoto;
				$html .='</td>';
				$html .='</tr>';
			
			}
			
		}
	}
}	elseif($DetailsLocation ==  '') {
 	 for ($i = 0; $i < $resultlength; $i++) {
 	 		$itemChiName = $result[$i]['NameChi'];
 	 		$itemEngName = $result[$i]['NameEng'];
 			$InventoryItemCode=$result[$i]['ItemCode'];
 		    $InventoryItemName = Get_Lang_Selection($result[$i]['NameChi'],$result[$i]['NameEng']);
 		    $InventoryItemName = intranet_undo_htmlspecialchars($InventoryItemName);
 		    $InventoryItemName = str_replace(array("\\","'",'"'),array("\\\\","\\'","&quot;"),$InventoryItemName);
 		    $InventoryItemID=$result[$i]['ItemID'];
			$photoPath=$result[$i]['PhotoPath'];
			$photoName=$result[$i]['PhotoName'];
			if ($photoPath && $photoName) {
				$photoPath=substr($photoPath,1);	// remove left /
				$inventoryPhoto = '<img src="'.$PATH_WRT_ROOT.$photoPath.$photoName.'" width="100px">';
				$paraInventoryPhoto = "<img src=\'".$PATH_WRT_ROOT.$photoPath.$photoName."\' width=\'100px\'>";
			}
			else {
				$inventoryPhoto = '-';
				$paraInventoryPhoto = '';
			}
 		    $dispName = $InventoryItemName.' ('.$InventoryItemCode.')';
 		    
 		    $html .='<tr>';
 		    	$html .= '<td><a href="javascript:void(0);" onclick="selectedResultItem(\''.$InventoryItemID.'\', \''.$dispName.'\', \''.$paraInventoryPhoto.'\');">[Select]</a></td>';
 		   		$html .='<td>';
 		   			 $html .=$InventoryItemCode;
 		   		$html .='</td>';
 		 		$html .='<td>';
 					$html .=$itemChiName;
 				$html .='</td>';
 				$html .='<td>';
 					$html .=$itemEngName;
 				$html .='</td>';
 				$html .='<td>';
 					$html .=$inventoryPhoto;
 				$html .='</td>';
 		    $html .='</tr>';
 	} 
}
$html .= '</table>';
$htmlAry['resultTable'] = $html;

// if ($numOfGreater > 0) {
// 	$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm()");
	
// }
$htmlAry['closeBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "backtoEditPage()");
?>
<script language="javascript">
function backtoEditPage() {

	js_Hide_ThickBox();
}
function selectedResultItem(parID,parName,photo){
	$('input#eInventoryItemID').val(parID);
	$('span#mappedItemNameSpan').html(parName);
	$('#mappedInvPhotoDiv').html(photo);
	js_Hide_ThickBox();
	
	
}

</script>

<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<?php echo $htmlAry['resultTable'] ?>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			
			<?php echo $htmlAry['closeBtn'] ?>
			<p class="spacer"></p>
		</div>
	</form>
</div>
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">

