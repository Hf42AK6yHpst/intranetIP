<?php
// Modifying by : 

####### Change log [Start] #######
#
#	Date:	2017-05-23	Cameron
#			add Location filter
#
#	Date:	2010-12-23	YatWoon
#			add status selection
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

if(sizeof($_POST)==0 && sizeof($_GET)==0 && $Status=="")
	$Status = 1;
	
if($Status!="")
	$conds .= " AND a.RecordStatus=$Status";
	
$conds .= $TargetFloor ? " and a.LocationLevelID='".$TargetFloor."'" : ""; 	
$conds .= $TargetRoom ? " and a.LocationID IN ('".str_replace(",","','",$TargetRoom)."')" : "";
	
$sql = "select 
			c.Name as CategoryName,
			a.Title as Title,
			count(a.Title) as c,
			a.CategoryID
		from
			REPAIR_SYSTEM_RECORDS as a
			left join REPAIR_SYSTEM_REQUEST_SUMMARY as b on (b.Title = a.Title)
			left join REPAIR_SYSTEM_CATEGORY as c on (c.CategoryID=b.CategoryID)
		where 
			left(a.DateInput,10) >='$startdate' and
			left(a.DateInput,10) <='$enddate' and
			b.CategoryID in ($CatID_str) and
			b.RecordStatus=1 AND a.RecordStatus != ".REPAIR_SYSTEM_STATUS_DELETED."
			$conds
		Group by 
			c.CategoryID, a.Title
		order by 
			$orderby
";

$result = $lrepairsystem->returnArray($sql);

if($withArchive) $text = " (".$Lang['RepairSystem']['NoOfArchivedRecordIncluded'].")";

$display = "<div class='table_board'>";
$display .="<table class='common_table_list_v30 view_table_list_v30'>";
$display .= "<tr>";
$display .= "<th>&nbsp;</th>";
$display .= "<th>". $Lang['RepairSystem']['Category'] ."</th>";
$display .= "<th>". $Lang['RepairSystem']['RequestSummary'] ."</th>";
$display .= "<th>". $Lang['RepairSystem']['NoOfRecords'] .$text."</th>";
$display .= "</tr>";

for ($i=0; $i<sizeof($result); $i++)
{
	list($thisCatName, $thisTitle, $thisCount, $thisCategory) = $result[$i];
	
	if($withArchive) {
		$sql = "SELECT COUNT(RecordID) FROM REPAIR_SYSTEM_RECORDS WHERE (left(DateInput,10) >='$startdate' AND
			left(DateInput,10) <='$enddate') AND CategoryID='$thisCategory' AND Title='$thisTitle' AND ArchivedBy IS NOT NULL AND RecordStatus!=".REPAIR_RECORD_STATUS_DELETED;
		
		$data = $lrepairsystem->returnVector($sql);
	}
	$archivedRecord = (($data[0]==0) ? "" : " (".$data[0].")");
	
	$display .= '<tr>';
	$display .= '<td valign="top">'.($i+1).'</td>';
	$display .= '<td valign="top">'.$thisCatName.'</td>';
	$display .= '<td valign="top">'.$thisTitle.'</td>';
	$display .= '<td valign="top">'.$thisCount.$archivedRecord.'</td>';
	$display .= '</tr>';
	
}

$display .= '</table>';
$display .= "</div>"; 	
?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<?=$display?>

<?
intranet_closedb();
?>
