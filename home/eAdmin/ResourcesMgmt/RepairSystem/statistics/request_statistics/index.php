<?php
// Modifying by : 
/*
 * 	2017-05-23 Cameron
 * 		- add filter by Location
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();
$llocation_ui	= new liblocation_ui();

$CurrentPage = "PageRequestStatistics";
$CurrentPageArr['eAdminRepairSystem'] = 1;
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['RepairSystem']['RequestStatistics']);
$linterface->LAYOUT_START();

$category_selection = $lrepairsystem->getCategorySelection($CatID," multiple size=10 ", "", 0, 1, "CatID[]");
$select_all_btn = $linterface->GET_BTN($button_select_all, "button", "SelectAll();");

$form_display = $lrepairsystem->returnRequestStatisticsForm_ui();

echo $form_display;

?>

<script language="javascript">
<!--
SelectAll();
//-->
</script>
<?
$linterface->LAYOUT_STOP();
?>
 