<?php
// Modifying by: 

###################
#
#	Date:	2017-08-02 	(Cameron)
#	Fix:	Add mandatory field indicator to column "Request Summary" and "Request Details" 	
#	
#	Date:	2014-11-06	(Omas)
#	Improved data column description - Column1,2,3 -> ColumnA,B,C
#
####################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

$lrepairsystem = new librepairsystem();

# access right checking
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();

### data column
$DataColumnTitleArr = $Lang['RepairSystem']['ImportDataCol_New'];
$DataColumnPropertyArr = array(1,2,1,1,1,3,3,3,1,1,3,3);
$DataColumnRemarksArr = array();
$DataColumnRemarksArr[3] = "<a class=\"tablelink\" href=javascript:show_ref_list('catName','cc_click')>".'['.$Lang['RepairSystem']['ClicktoCheck']. $Lang['RepairSystem']['Category_RequestSummary'].']' ."</a><span id=\"cc_click\">&nbsp;</span>";
$DataColumnRemarksArr[4] = "<a class=\"tablelink\" href=javascript:show_ref_list('buildingName','bu_click')>".'['.$Lang['RepairSystem']['ClicktoCheck'].$Lang['RepairSystem']['Building'].','.$Lang['RepairSystem']['Location'].','.$i_InventorySystem_Location.']'."</a><span id=\"bu_click\">&nbsp;</span>";
//$DataColumnRemarksArr[5] = "<a class=\"tablelink\" href=javascript:document.getElementById('loadBuildingID').value='';document.getElementById('loadLevelID').value='';show_ref_list('levelName','lvl_click')>".'['.$Lang['RepairSystem']['ClicktoCheck'].$Lang['RepairSystem']['Location'].']'."</a><span id=\"lvl_click\">&nbsp;</span>";
//$DataColumnRemarksArr[6] = "<a class=\"tablelink\" href=javascript:document.getElementById('loadBuildingID').value='';document.getElementById('loadLevelID').value='';show_ref_list('locName2','loc_click')>".'['.$Lang['RepairSystem']['ClicktoCheck'].$i_InventorySystem_Location.']'."</a><span id=\"loc_click\">&nbsp;</span>";
$DataColumnRemarksArr[8] = "<a class=\"tablelink\" href=javascript:show_ref_list('catName','cc_click2')>".'['. $Lang['RepairSystem']['ClicktoCheck'].$Lang['RepairSystem']['Category_RequestSummary'].']' ."</a><span id=\"cc_click2\">&nbsp;</span>";
$DataColumnRemarksArr[11] = "<a class=\"tablelink\" href=javascript:show_ref_list('statusName','sc_click')>".'['.$Lang['RepairSystem']['ClicktoCheck'].$Lang['RepairSystem']['ImportDataCol'][8].']'."</a><span id=\"sc_click\">&nbsp;</span></p>";
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);

$reference_str = "<div id=\"ref_list\" style='position:absolute; height:150px; width:400px; z-index:1; visibility: hidden;'></div>";

//$reference_str = "<div id=\"ref_list\" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>";
//$reference_str .= "<a class=\"tablelink\" href=javascript:show_ref_list('catName','cc_click')>". $Lang['RepairSystem']['Category_RequestSummary'] ."</a>,<span id=\"cc_click\">&nbsp;</span>
//				<!--<a class=\"tablelink\" href=javascript:show_ref_list('locName','lc_click')>".$Lang['RepairSystem']['Location']."</a>,<span id=\"lc_click\">&nbsp;</span>-->
//				<a class=\"tablelink\" href=javascript:show_ref_list('buildingName','bu_click')>".$Lang['RepairSystem']['Building']."</a>,<span id=\"bu_click\">&nbsp;</span>
//				<a class=\"tablelink\" href=javascript:document.getElementById('loadBuildingID').value='';document.getElementById('loadLevelID').value='';show_ref_list('levelName','lvl_click')>".$i_InventorySystem_Location_Level."</a>,<span id=\"lvl_click\">&nbsp;</span>
//				<a class=\"tablelink\" href=javascript:document.getElementById('loadBuildingID').value='';document.getElementById('loadLevelID').value='';show_ref_list('locName2','loc_click')>".$i_InventorySystem_Location."</a>,<span id=\"loc_click\">&nbsp;</span>
//				<a class=\"tablelink\" href=javascript:show_ref_list('statusName','sc_click')>".$Lang['RepairSystem']['ImportDataCol'][8]."</a><span id=\"sc_click\">&nbsp;</span></p>";	
//
//$csv_format = "";
//$delim = "<br>";
//for($i=0; $i<sizeof($Lang['RepairSystem']['ImportDataCol_New']); $i++){
//	if($i!=0) $csv_format .= $delim;
//	$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['RepairSystem']['ImportDataCol_New'][$i];
//}


# menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageList";

# Left menu
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['RepairSystem']['AllRequests']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$sample_file = "repair_system_unicode.csv";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";



$linterface->LAYOUT_START();
?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_repair.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

var callback_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_type_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_repair.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_list);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
  document.getElementById('ref_list').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') + 'px';
  document.getElementById('ref_list').style.top = getPosition(document.getElementById(ClickID),'offsetTop')+10 + 'px';
  document.getElementById('ref_list').style.visibility='visible';
}
</script>
<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: 
						<span class="tabletextremark">
						<?=$Lang['General']['CSVFileFormat']?></span>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="csvfile">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<!--tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr-->
				<!--tr>
					<td class="formfieldtitle" align="left" height="30"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference']?></td>
					<td class="tabletext"><?=$reference_str?></td>
				</tr-->
				<tr>
					<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
					<td class="tabletext"><?=$DataColumn?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']."<br>".$Lang['General']['InputEitherOneField']."<br>".$Lang['General']['ReferenceField']?></td>
				</tr>
				
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DateFormat']?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'") ?>
		</td>
	</tr>
</table>
<?=$reference_str?>
<input type="hidden" id="task" name="task"/>
<input type="hidden" id="loadBuildingID" name="loadBuildingID"/>
<input type="hidden" id="loadLevelID" name="loadLevelID"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
