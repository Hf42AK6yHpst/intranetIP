<?php
# using:  

##### Change Log [Start] #####
#
#   Date    :   2020-10-27 (Cameron)
#               change follow-up person selection to multiple [case X190081]
#               add notify person selection checkbox [case #X190077]
#
#	Date	:	2020-10-22 (Cameron)
#				Improvement: add upload photo(s) function [case X190085]
#
#   Date    :   2020-08-02 (Cameron)
#               Modify: reorder item to: Location, Location Details, Category, Request Summary [case #X190080]
#
#	Date	:	2017-12-28  (Cameron)
#				Fix: buttons do not work in IE11 due to incorrect place of closing comment for javascript (case #N133250)
#
#	Date	:	2017-10-10  (Cameron)
#				Fix: use left join INTRANET_USER to include deleted user (case #N128259)
#
#	Date	:	2017-08-21 (Cameron)
#				Show case number in this page (case #Z118886)
#
#	Date	:	2017-06-19 (Cameron)
#				Improvement: change view_photo to use load_dyn_size_thickbox_ip()
#
#	Date	:	2017-06-05 	(Cameron)
#				Fix bug to support special characters like '"<>&\/ for FollowUpPerson
#				- also show inventory item code next to item name
#
#	Date	:	2017-05-31  (Cameron)
#				Fix bug:  	- remove excessive parameter yearTermId in onloadSystemSuggestThickbox() and onloadManualMappingThickbox()
#							- show apply intranet_undo_htmlspecialchars to $ItemNameArr to handle special characters				
#
#	Date	:	2017-05-26  (Cameron)
#				Improvement: show item photo from inventory if it's been mapped to inventory
#
#	Date	:	2017-05-24  (Cameron)
#				Improvement: add SubmitAndNotifyReporter button
#
#	Date	:	2017-05-10	(Cameron)
#				Improvement: show photo(s) of the request
#
#	Date	:	2016-07-07	(Anna)
#				Add onloadSystemSuggestThickbox
#
#	Date	:	2016-06-30	(Cara)
#				Add system user drop-down list for followUpPerson selection
#				Add new field FollowUpPersonID
#				Add onloadManualMappingThickbox()
#
#	Date	:	2015-04-29	(Omas)
#				Fixed : show wrong person archived 
#
#	Date	:	2015-02-12	(Omas)
#				Improved : Add new field FollowUpPerson
#
#	Date	:	2014-12-02	(Omas)
#				fixed not auto check status check-box when status is in process
#
#	Date	:	2014-11-06	(Omas)
#				add New Status Pending (value = 4 in db)
#
#	Date	:	2011-03-28 (Henry Chow)
#				Improved : select "Location" from "School Setting > Campus"
#
#	Date	:	2011-01-03 [YAtWoon]
#				- set Location Detail is a optional field
#				- add "reject" status
#
#	Date	:	2010-11-24	YatWoon
#				- IP25 UI standard
#
#	Date	:	2010-11-01 (Henry Chow)
#				add checking on access right of particular record
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();
$linvertory = new libinventory();
$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$isRecordAccessAllowed = $lrepairsystem->checkRecordAccessRight($RecordID);


if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || !$isRecordAccessAllowed )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$use_plupload = $lrepairsystem->isUsePlupload();

if($use_plupload) {
    $lrepairsystem->updateTempPluploadInfo();
}


$name_field = getNameFieldByLang("USR.");
$name_field2 = getNameFieldByLang("USR2.");
$buildingName = Get_Lang_Selection("bu.NameChi", "bu.NameEng");
$levelName = Get_Lang_Selection("lv.NameChi", "lv.NameEng");
$LocationName = Get_Lang_Selection("l.NameChi", "l.NameEng");
$ItemName= Get_Lang_Selection("INV.NameChi", "INV.NameEng");

$sql = "SELECT
			REC.CaseNumber, 
			REC.UserID,
			CAT.Name, 
			IF((REC.LocationBuildingID!=0),	
				CONCAT(CONCAT(IF($buildingName IS NOT NULL, $buildingName, '".$Lang['RepairSystem']['MainBuilding']."'), IF($levelName!='', CONCAT(' &gt; ', $levelName), ''))),
				LOC.LocationName) as LocationName, 
			CONCAT(IF(REC.LocationBuildingID!=0 AND $LocationName!='', $LocationName, ''), IF($LocationName!='' AND REC.DetailsLocation!='', CONCAT(IF(REC.LocationBuildingID!=0, ' &gt; ', ''), REC.DetailsLocation), REC.DetailsLocation)) as DetailLocation, 
			REC.Title, 
			REC.Content, 
			GP.GroupTitle,
			REC.RecordStatus, 
			REC.Remark,
			REC.DateModified,
			$name_field as lastModifiedBy,
			LEFT(REC.CompleteDate,10),
			REC.CategoryID,
			REC.DateInput,
			$name_field2,
			LEFT(REC.DateArchived,10),
			REC.FollowUpPerson,
			GP.GroupID,
			REC.FollowUpPersonID,
			REC.InventoryItemID,
			INV.ItemID,
			INV.ItemCode,
			$ItemName as itemName
		FROM 
			REPAIR_SYSTEM_RECORDS REC 
			LEFT OUTER JOIN INVENTORY_ITEM AS INV ON (INV.ItemID=REC.InventoryItemID)		
			LEFT OUTER JOIN REPAIR_SYSTEM_CATEGORY CAT ON (CAT.CategoryID=REC.CategoryID) 
			LEFT OUTER JOIN INVENTORY_LOCATION l ON (l.LocationID=REC.LocationID) 
			LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL lv ON (lv.LocationLevelID=REC.LocationLevelID) 
			LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING as bu on (bu.BuildingID=REC.LocationBuildingID) 
			LEFT OUTER JOIN REPAIR_SYSTEM_LOCATION LOC ON (LOC.LocationID=REC.LocationID) 
			LEFT OUTER JOIN REPAIR_SYSTEM_GROUP GP ON (GP.GroupID=REC.LastFollowupGroupID) 
			LEFT JOIN INTRANET_USER USR ON (USR.UserID=REC.LastModifiedBy) 
			LEFT OUTER JOIN INTRANET_USER USR2 ON (USR2.UserID=REC.ArchivedBy)
		WHERE 
			REC.RecordID=$RecordID";
			

$result = $lrepairsystem->returnArray($sql);

$InventoryItemID=$result[0]['InventoryItemID'];
//$CurrnetFollowUpPersonID = $result[0]['FollowUpPersonID'];
$CurrnetFollowUpPerson = $result[0]['FollowUpPerson'];
$dispItemCode = $result[0]['ItemCode'] ? ' ('.$result[0]['ItemCode'].')' : '';
$ItemNameArr = intranet_undo_htmlspecialchars($result[0]['itemName'].$dispItemCode);

$inventoryPhoto = '';
if ($InventoryItemID > 0) {
	// edit
	$hasMappedInventoryItem = true;
	$invPhoto = $lrepairsystem->getInventoryPhoto($InventoryItemID);
	if (count($invPhoto)) {
		$invPhoto = current($invPhoto);
		$inventoryPhoto = '<img src="'.$PATH_WRT_ROOT.$invPhoto['PhotoPath'].'/'.$invPhoto['PhotoName'].'" width="100px">';
	}
}
else {
	// new
	$hasMappedInventoryItem = false;
}
$htmlAry['mappedInventoryItemRadio_yes'] = $linterface->Get_Radio_Button('mappedInventoryItemRadio_yes', 'mappedInventoryItem', $Value=1, $hasMappedInventoryItem, $Class="", $Lang['General']['Yes'], "clickedMappedInventoryItemRadio(this.value);", $isDisabled=0);
$htmlAry['mappedInventoryItemRadio_no'] = $linterface->Get_Radio_Button('mappedInventoryItemRadio_no', 'mappedInventoryItem', $Value=0, !$hasMappedInventoryItem, $Class="", $Lang['General']['No'], "clickedMappedInventoryItemRadio(this.value);", $isDisabled=0);



//debug_pr($ItemNameArr);
//debug_pr('$InventoryItemID =' .$InventoryItemID);

list($caseNumber, $userid, $catName, $locationName, $detailsLocation, $title, $content, $groupTitle, $recordStatus, $remark, $dateModified, $lastModifiedBy, $completeDate, $recCatID, $record_date, $archivedBy, $archiveDate,$FollowUpPerson, $groupId, $FollowUpPersonID) = $result[0];


$managementGroupSelection = $lrepairsystem->getManagementGroupSelection($groupId);

//// get group members from follow up group
//$RepairUserArr = array();
//$RepairUserArr[0][] = '';
//$RepairUserArr[0][] = '--- '.$button_select. ' ---';
//$groupMemberAry = $lrepairsystem->getGroupMemberByGroupId($groupId);
//$numOfMember = sizeof($groupMemberAry);
//for ($i=0; $i < $numOfMember ; $i++){
//	$_memberUserId = $groupMemberAry[$i]['UserID'];
//	$_memberEnglishName = $groupMemberAry[$i]['EnglishName'];
//	$_memberChineseName = $groupMemberAry[$i]['ChineseName'];
//
//	$RepairUserArr[$i+1][] = $_memberUserId;
//	$RepairUserArr[$i+1][] = Get_Lang_Selection($_memberChineseName, $_memberEnglishName);
//}

$groupMemberAry = $lrepairsystem->getGroupMembers($groupId, $RecordID);
$followupPersonIDAry = $lrepairsystem->getFollowupPersonID($RecordID);
$followupPersonSelectionUI = $lrepairsystem->getFollowupPersonSelection($groupMemberAry, $followupPersonIDAry);
$nrFollowupPerson = count($followupPersonIDAry);

$name_field = getNameFieldByLang();
$sql = "SELECT $name_field as name FROM INTRANET_USER WHERE UserID='$userid'";
$result = $lrepairsystem->returnVector($sql);
$userName = ($result[0]!="") ? $result[0] : $Lang['General']['EmptySymbol'];

if (($nrFollowupPerson == 0) && $FollowUpPerson != '') {
    $nonSystemUserChecking=1;
    $systemUserChecking=0;
}
else {
    $nonSystemUserChecking=0;
    $systemUserChecking=1;
}

//debug_pr($systemUserChecking);
//debug_pr($nonSystemUserChecking);

//if($FollowUpPersonID !== NULL){
//	$systemUserChecking=1;
//}else{
//	$systemUserChecking=0;
//}
//
//if($FollowUpPersonID == NULL){
//	$nonSystemUserChecking=1;
//}else{
//	$nonSystemUserChecking=0;
//}

//followupPerson
//$followupField = '<label for=\"followUpPerson2\"><input id="FollowUpPerson" name="FollowUpPerson" type="text" maxlength="32" value="'.$FollowUpPerson.'"></label>';
$FollowUpPerson = Get_String_Display($FollowUpPerson); // Empty string -> ---
$ItemNameArr = Get_String_Display($ItemNameArr);

$htmlAry['systemUserRadio'] = $linterface->Get_Radio_Button('followUpPerson_systemUser', 'selectFollowUpUser', $Value=1, $isChecked="$systemUserChecking", $Class="", $Lang['RepairSystem']['SystemUser'], "clickedSystemUserRadio(this.checked);", $isDisabled=0);
//$htmlAry['systemUserSelection'] = $linterface->GET_SELECTION_BOX($RepairUserArr, 'name="followUpUser" id="followUpUserSel"', '', $FollowUpPersonID);
$htmlAry['systemUserSelection'] = $followupPersonSelectionUI;
$htmlAry['nonSystemUserRadio'] = $linterface->Get_Radio_Button('followUpPerson_nonSystemUser', 'selectFollowUpUser', $Value=2, $isChecked="$nonSystemUserChecking", $Class="", $Lang['RepairSystem']['NonSystemUser'], "clickedNonSystemUserRadio(this.checked);", $isDisabled=0);
$htmlAry['nonSystemUserSelection'] = '<input id="FollowUpPerson" name="FollowUpPerson" type="text" maxlength="32" value="'.$FollowUpPerson.'">';

if($recordStatus==REPAIR_RECORD_STATUS_CANCELLED) {			# cancelled
	$status = $i_status_cancel;
	$followupField = $FollowUpPerson;
	$followupSelection = $FollowUpPersonID;
} else if($recordStatus==REPAIR_RECORD_STATUS_COMPLETED) {	# completed
	$status = $Lang['RepairSystem']['Completed'];
	$status .= ($completeDate!="") ? " (".$Lang['RepairSystem']['CompleteDate']." : $completeDate)" : "";
	$followupField = $FollowUpPerson;
	$followupSelection = $FollowUpPersonID;
} else if($recordStatus==REPAIR_RECORD_STATUS_REJECTED) {			# rejected
	$status = $i_status_rejected;
	$followupField = $FollowUpPerson;
	$followupSelection = $FollowUpPersonID;
} else {
	if($recordStatus==REPAIR_RECORD_STATUS_PENDING){		# pending
		$status4 = " checked";
	}
	else if ($recordStatus==REPAIR_RECORD_STATUS_PROCESS){
		$status1 = "checked";
	}
		
	$status = "	<input type=\"radio\" name=\"newStatus\" id=\"newStatus4\" value=\"4\" $status4 onclick=\"hideSpan('spanComplete');\"> <label for=\"newStatus4\">".$Lang['RepairSystem']['Pending']."</label>
				<input type=\"radio\" name=\"newStatus\" id=\"newStatus1\" value=\"1\" $status1 onclick=\"hideSpan('spanComplete');\"> <label for=\"newStatus1\">".$Lang['RepairSystem']['Processing']."</label> 
				<input type=\"radio\" name=\"newStatus\" id=\"newStatus3\" value=\"3\" $status3 onclick=\"hideSpan('spanComplete');\"> <label for=\"newStatus3\">".$button_reject."</label> 
				<input type=\"radio\" name=\"newStatus\" id=\"newStatus2\" value=\"2\" $status2 onclick=\"showSpan('spanComplete');\"><label for=\"newStatus2\">".$Lang['RepairSystem']['Completed']."</label>
				<div id='spanComplete' style=\"position:relative;display:none;left:10px\">(".$Lang['RepairSystem']['CompleteDate']." : ".$linterface->GET_DATE_PICKER("CompleteDate",$CompleteDate).")</div>";
				
//	$tr = "	<tr id=\"followUpDiv\" style=\"display:none\">
//				<td class=\"field_title\">".$Lang['RepairSystem']['FollowupAction']."</td>
//				<td><input type='checkbox' name='EmailToReporter' id='EmailToReporter' value='1'><label for='EmailToReporter'>".$Lang['RepairSystem']['SendEmailToReporter']."</label></td>
//			</tr>";			
}

# archive
if($archivedBy != "") { 
	if($status!="") $status .= "<br>";
	if($intranet_session_language=="en") {
		$status .= "[".$Lang['eDiscipline']['Archived']." ".$Lang['RepairSystem']['by']."  ".$archivedBy." ".$Lang['RepairSystem']['on']." ".$archiveDate."]";
	} else {
		$status .= "[".$Lang['RepairSystem']['by']." ".$archivedBy." ".$Lang['RepairSystem']['on']." ".$archiveDate." ".$button_archive."]";
	}
}

if(($recordStatus==REPAIR_RECORD_STATUS_COMPLETED || $recordStatus==REPAIR_RECORD_STATUS_REJECTED) && $archivedBy=="") {
	$additionButton = $linterface->GET_ACTION_BTN($button_archive, "button", "archiveRecord()");
} 

if($archivedBy!="") {
	$back_url = "archive.php";
	$allFlag = 0;
	$archiveFlag = 1;
} else {
	$back_url = "index.php";
	$allFlag = 1;
	$archiveFlag = 0;
}


# menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageList";

# Left menu 
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['RepairSystem']['AllRequests'], "index.php", $allFlag);
$TAGS_OBJ[] = array($Lang['RepairSystem']['ArchivedRequests'], "archive.php", $archiveFlag);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
echo $linterface->Include_Thickbox_JS_CSS();
echo $lrepairsystem->getPhotoCSS();

$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['EditViewRequest']);

#map inventory

//$_inventory_ITEM = new libinventory();

// if ($InventoryItemID !="")
// {
// 	//	$('input#mapping').attr('checked', true);
// 	//	$IsInventoryMapped = this->getItemNameFromInventory($InventoryItemID);
// 	// 	  debug_pr($IsInventoryMapped);
// 	//	if ($IsInventoryMapped && $_inventory_ITEM->getOEA_ProgramCode()==$oea_cfg["OEA_Default_Category_Others"])
// 		//	{
// 		// 		$_inventory_ITEM->setTitle( $Lang['iPortfolio']['OEA']['UseOleName']. " (".$engTitle.")");
// 		//	}
// 	$InventoryMappingChecked =  " checked='checked' ";

// } else
// {
// 	//	$_inventory_ITEM->setNewRecord($isNewRecord=1);
// }
// $h_result = $_inventory_ITEM->getOeaItemTable(true, $engTitle);

if($plugin['eClassTeacherApp']){
    $message_selection = "<tr><td class=\"field_title\">".$Lang['RepairSystem']['MessageToReporter']."</td>";
    $message_selection .= "<td>";
    $message_selection .= "<label><input type='radio' id='message_app' name='messageWay' value='1' checked>".$Lang['RepairSystem']['MessageApp']."</label> ";
    $message_selection .= "<label><input type='radio' id='message_email' name='messageWay' value='0'>".$Lang['RepairSystem']['MessageEmail']."</label> ";
    $message_selection .= "</td></tr>";
}
?>

<script language="javascript">
<!--
function goBack() {
	document.form1.action = "<?=$back_url?>";
	document.form1.submit();
}

function checkForm() {
	var obj = document.form1;

    var error_no = 0;
    var focus_field = "";

    <? if ($use_plupload): ?>
    if(!jsIsFileUploadCompleted()) {
        error_no++;
        $('#FileWarnDiv').html('<?=$Lang['RepairSystem']['jsWaitAllPhotosUploaded']?>').show();
        setTimeout(hideFileWarningTimerHandler,5000);
    }
    <? endif;?>

	if($('#newStatus2').is(':checked')){
		if(!check_date(obj.CompleteDate,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
            error_no++;
		}
	}

    if(error_no>0){
        if (focus_field != '') {
            eval("obj." + focus_field +".focus();");
        }
        $('input.actionBtn').attr('disabled', '');
        return false;
    }
    else {
        <? if (!$use_plupload): ?>
        $('#FileName').val(document.getElementById('File').value);
        <? endif;?>
        if ($('#followUpPerson_systemUser').is(':checked')) {
            $('#SysPersonID option').attr('selected',true);
        }
        obj.submit();
        return true;
    }

}

function showSpan(spanName) {
	document.getElementById(spanName).style.display = 'inline';
}

function hideSpan(spanName) {
	document.getElementById(spanName).style.display = 'none';
}

function showHideSpan(layername, flag) 
{
	var browserName = navigator.appName; 
	var doAction = "";
	if (browserName=="Microsoft Internet Explorer")
	{ 
		doAction = "inline";
	}
	else 
	{ 
		doAction = "table-row";
	}

	if(flag==1) {
		document.getElementById(layername).style.display = doAction;;
	} else {
		document.getElementById(layername).style.display = "none";
	}
}

function archiveRecord(){
	
	var alertConfirmRemove = '<?=$Lang['RepairSystem']['JSWarning']['AreYouSureArchiveRecord']?>';
	
	if(confirm(alertConfirmRemove)){
		document.form1.action = "archive_update.php";
		document.form1.method = "post";
		document.form1.submit();
	}
	
}


function clickedSystemUserRadio(parChecked) {
	if (parChecked) {
        enableFollowupSysPerson();
	} else {
        disableFollowupSysPerson();
    }
}

function clickedNonSystemUserRadio(parChecked) {
	if (parChecked) {
        disableFollowupSysPerson();
	} else {
        enableFollowupSysPerson();
    }
}

function enableFollowupSysPerson()
{
    $('#followUpPerson_systemUser').attr('checked', true);
    $('select#AvailableSysPersonID').attr('disabled', false);
    $('select#SysPersonID').attr('disabled', false);
    $('input#FollowUpPerson').attr('disabled', true);
    $('input#FollowUpPerson').attr("value", "");
}

function disableFollowupSysPerson()
{
    $('#followUpPerson_nonSystemUser').attr('checked', true);
    $('select#AvailableSysPersonID').attr('disabled', true);
    $('select#SysPersonID').attr('disabled', true);
    $('input#FollowUpPerson').attr('disabled', false);
    $('select#SysPersonID').find('option').remove();
}


function clickedMappedInventoryItemRadio(parChecked) {
//	alert('clicked mapping radio = ' + parChecked);

if (parChecked=="0")
	{
		$('#mappingOptionDiv').hide();
		$('#mappedInvPhotoDiv').hide();
	
	} else
	{
		$('#mappingOptionDiv').show();
		$('#mappedInvPhotoDiv').show();
	}

}

function clickedItemSystemSuggest() {
	load_dyn_size_thickbox_ip('<?php echo $Lang['RepairSystem']['SuggestedItemsCheck']?>', 'onloadSystemSuggestThickbox();', inlineID='', defaultHeight=400, defaultWidth=600);
}
function onloadSystemSuggestThickbox() {
	$('div#TB_ajaxContent').load(
		"ajax_reload.php", 
		{ 
			RecordID: '<?php echo $RecordID?>'
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
	
}

function clickedItemManualMapping() {
	load_dyn_size_thickbox_ip('<?php echo $Lang['RepairSystem']['AdvanceSearch']?>', 'onloadManualMappingThickbox();', inlineID='', defaultHeight=400, defaultWidth=1000);
}
function onloadManualMappingThickbox() {
	$('div#TB_ajaxContent').load(
			"ajax_manual.php", 
			{ 
				RecordID: '<?php echo $RecordID?>'
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}

function checkOptionTransferUnique(from, to)
{
    checkOption(from);
    checkOption(to);
    i = from.selectedIndex;
    while(i!=-1){
        for (j=0; j<to.length; j++){
            if (to.options[j].value == from.options[i].value) {
                from.options[i] = null;
                break;
            }
        }
        to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
        from.options[i] = null;
        i = from.selectedIndex;
    }
}

$(document).ready( function () {

 	var isChecked = $('input#mappedInventoryItemRadio_yes').attr('checked');
 	if (isChecked == true){
 		$('#mappingOptionDiv').show();
 		$('#mappedInvPhotoDiv').show();
 	} 		
 	else{
 		$('#mappingOptionDiv').hide();
 		$('#mappedInvPhotoDiv').hide();
 	}

	//var currentFollowUpPersonID = '<?//=$CurrnetFollowUpPersonID?>//';
	var currentFollowUpPerson = '<?=$lrepairsystem->Get_Safe_Sql_Query($CurrnetFollowUpPerson)?>';
	// if (currentFollowUpPersonID == ''){
    <?php if ($systemUserChecking):?>
        enableFollowupSysPerson();
    <?php else:?>
        disableFollowupSysPerson();
    <?php endif;?>

	$('#btnSubmit').click(function() {
	 	$('input.actionBtn').attr('disabled', 'disabled');
		document.form1.submitMode.value = 'submit';
		checkForm();
	});

	$('#btnSubmitAndNotify').click(function() {
		$('input.actionBtn').attr('disabled', 'disabled');
		document.form1.submitMode.value = 'submitAndNotify';
		checkForm();
	});

    $('#GroupID').change(function(){
        enableFollowupSysPerson();
        $.post(
            "ajax_get_management_group_members.php",
            {
                "GroupID" : $('#GroupID').val(),
                'RecordID' : '<?php echo $RecordID;?>'
            },
            function(responseText){
                if(responseText){
                    $('#PersonSelectionSpan').html(responseText);
                }
            }
        );
    });
});

function view_photo(photoID) {
//	tb_show('<?=$Lang['RepairSystem']['ViewOriginalPhoto']?>','ajax_view_photo.php?PhotoID='+photoID+'&height=500&width=800');
	load_dyn_size_thickbox_ip('<?=$Lang['RepairSystem']['ViewOriginalPhoto']?>', 'onloadImageThickBox("'+photoID+'");', inlineID='', defaultHeight=400, defaultWidth=800);
}

function onloadImageThickBox(photoID) {
	$('div#TB_ajaxContent').load(
		"ajax_view_photo.php", 
		{ 
			PhotoID: photoID
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

-->
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" method="POST" action="edit_update.php">
<table class="form_table_v30">
<tr>
	<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$Lang['RepairSystem']['RequestInformation']?> -</i></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['CaseNumber']?></td>
	<td><?=$caseNumber?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestDate']?></td>
	<td><?=substr($record_date,0,10)?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['Reporter']?></td>
	<td><?=$userName?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['Location']?></td>
	<td><?=$locationName?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['LocationDetail']?></td>
	<td><?=$detailsLocation ? $detailsLocation : $Lang['General']['EmptySymbol']?></td>
</tr>

<tr>
    <td class="field_title"><?=$Lang['RepairSystem']['Category']?></td>
    <td><?=$catName?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestSummary']?></td>
	<td><?=$title?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestDetails']?></td>
	<td><?=nl2br($content)?></td>
</tr>

<!-- photos of request -->
<?=$lrepairsystem->getRepairReqPhotoUI($RecordID)?>

<tr>
	<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><br><i> - <?=$Lang['RepairSystem']['FollowupInformation']?> -</i></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['MgmtGroup']?></td>
	<td><?=$managementGroupSelection?></td>
</tr>

<tr>
	<td rowspan="2" class="field_title"><?=$Lang['RepairSystem']['FollowUpPerson']?></td>
	<td><?php echo $htmlAry['systemUserRadio']?> <?php echo $htmlAry['systemUserSelection']?></td>   
</tr>
<tr>
	<td><?php echo $htmlAry['nonSystemUserRadio']?> <?php echo $htmlAry['nonSystemUserSelection']?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_general_status?></td>
	<td><?=$status?></td>
</tr>

<!-- upload photoes -->
<tr>
    <td class="field_title">
        <?=$Lang['RepairSystem']['UploadPhotos']?>
    </td>
    <td>
        <? if($use_plupload){ ?>
            <div id="<?=$pluploadContainerId?>"></div>
            <?=$linterface->GET_SMALL_BTN($Lang['RepairSystem']['SelectPhotos'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='')?>
            <span class="tabletextremark">(<?=$Lang['RepairSystem']['PhotoRemark']?>)</span>
            <div id="<?=$pluploadDropTargetId?>" style="display:none;" class="DropFileArea"><?=$Lang['RepairSystem']['OrDragAndDropPhotosHere']?></div>
            <div id="<?=$pluploadFileListDivId?>"></div>
            <?=$linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv')?>
        <? }else{ ?>
            <input type="file" name="File" id="File" class="textbox"/>
        <? } ?>
        <span id="div_err_File"></span>
    </td>

</tr>

<!-- photos of follow-up -->
<?=$lrepairsystem->getPhotoListUI($RecordID,2)?>

<?=$tr?>

<tr>
	<td class="field_title"><?=$i_UserRemark?></td>
	<td><?= nl2br(($remark) ? intranet_htmlspecialchars(intranet_undo_htmlspecialchars(intranet_undo_htmlspecialchars($remark))) : $Lang['General']['EmptySymbol']);?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['AddRemark']?></td>
	<td>
		<?=$linterface->GET_TEXTAREA("additionRemark", "")?>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['LastModify']?></td>
	<td><?=$dateModified." ".$lastModifiedBy?></td>
</tr>

<tr>
	<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><br><i> - <?=$Lang['RepairSystem']['MappingSettings']?> -</i></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['MapToInventory']?></td>
	<td>
		<table width="100%">
			<tr>
				<td width="50%" valign="top">
				
					<?php echo $htmlAry['mappedInventoryItemRadio_yes'] ?>
					<?php echo $htmlAry['mappedInventoryItemRadio_no'] ?>
					
					<div id="mappingOptionDiv">
						<div id="mappedInfoDiv" >
							<?=$Lang['RepairSystem']['Mapped']?> <span id="mappedItemNameSpan"> <?php echo $ItemNameArr?></span>
						</div>
						<a href="javascript:void(0);" class="tablelink" onclick="clickedItemSystemSuggest();">- <?=$Lang['RepairSystem']['SuggestedItemsCheck']?></a>
						<br>
						<a href="javascript:void(0);" class="tablelink" onclick="clickedItemManualMapping();">- <?=$Lang['RepairSystem']['AdvanceSearch']?></a>
					</div>
				</td>
				
				<td width="50%" valign="top">
					<div id="mappedInvPhotoDiv" >
						<?=$inventoryPhoto?>
					</div>
				</td>
			</tr>
		</table>			
		
	</td>
</tr>


<tr>
    <td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><br><i> - <?=$Lang['RepairSystem']['Notify']?> -</i></td>
</tr>
<tr>
    <td class="field_title"><?=$Lang['RepairSystem']['NotifyPerson']?></td>
    <td>
        <?=$linterface->Get_Checkbox("InformReporter", "InformReporter", "1", $isChecked=0, $Class='', $Lang['RepairSystem']['Reporter'])?>
        <?=$linterface->Get_Checkbox("InformFollowupPerson", "InformFollowupPerson", "1", $isChecked=0, $Class='', $Lang['RepairSystem']['SystemFollowUpPerson'])?>
    </td>
</tr>
    <tr>
        <?=$message_selection?>
    </tr>

</table>


<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?=$additionButton?>
	<?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
<!--	--><?//= $linterface->GET_ACTION_BTN($Lang['RepairSystem']['SubmitAndNotifyReporter'], "button", "", "btnSubmitAndNotify", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
<p class="spacer"></p>
<br>
<span class="tabletextremark">
	<span style="color:red; font-weight:string;">#</span>
	<?=$Lang['RepairSystem']['SubmitWiNotifyRemark']?>
</span>

</div>

<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
<input type="hidden" name="CatID2" id="CatID2" value="<?=$CatID?>">
<input type="hidden" name="Status" id="Status" value="<?=$Status?>">
<input type="hidden" name="year" id="year" value="<?=$year?>">
<input type="hidden" name="month" id="month" value="<?=$month?>">
<input type="hidden" name="eInventoryItemID" id="eInventoryItemID" value="<?=$InventoryItemID?>">
<input type="hidden" name="submitMode" id="submitMode" value="submit">

<input type="hidden" name="FileName" id="FileName" value="">
<? if($use_plupload) { ?>
    <input type="hidden" name="TargetFolder" id="TargetFolder" value="<?=$tempFolderPath?>">
<? } ?>

</form>

<?
if($use_plupload) {
    include_once($PATH_WRT_ROOT."home/eService/RepairSystem/plupload_script.php");
}

$linterface->LAYOUT_STOP();
intranet_closedb();

?>