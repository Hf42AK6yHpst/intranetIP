<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$lda->Start_Trans();

if($lda->Delete_Subject_Resource_Record($RecordID)) {
	$lda->Commit_Trans();
	$msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
} else {
	$lda->RollBack_Trans();
	$msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];	
}

if($advanceSearchFlag!="")
	$string .= "&advanceSearchFlag=$advanceSearchFlag";
if($keyword!="")
	$string .= "&keyword=$keyword";
if($SubjectID1!="")
	$string .= "&SubjectID1=$SubjectID1";	
if($str!="")
	$string .= "&str=$str";
if(sizeof($ClassLevelID)>0)
	for($i=0, $i_max=sizeof($ClassLevelID); $i<$i_max; $i++)
		$string .= "&ClassLevelID[]=".$ClassLevelID[$i];
if(sizeof($SubjectID)>0)
	for($i=0, $i_max=sizeof($SubjectID); $i<$i_max; $i++)
		$string .= "&SubjectID[]=".$SubjectID[$i];
	
intranet_closedb();

header("Location: displayMore.php?flag=$flag&msg=$msg".$string);
?>