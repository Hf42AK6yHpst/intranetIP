<?php

//using: rita

/************************************************
 * Date:	2013-04-08 (Rita)
 * Details:	add search box 
 * 
 * Date:	2013-04-02 (Rita)
 * Details:	create this page for displaying group members
 *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Set Cookies
$arrCookies = array();
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_Member_page_size", "numPerPage");
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_Member_page_number", "PageNumber");
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_Member_page_order", "order");
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_Member_page_field", "field");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$pic_view = '';
	$helper_view = '';
}
else {
	updateGetCookies($arrCookies);
}
//debug_pr($_SESSION['SSV_USER_ACCESS']);

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$CurrentPageArr['DigitalArchiveSubjecteResourcesAdminMenu'] = true;
$CurrentPageArr['DigitalArchive_SubjectReference'] = true;

$CurrentPage = "Settings_Group";

$TAGS_OBJ[] = array($Lang['Menu']['DigitalArchive']['Settings']['AccessRight'] );

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$groupID = $_GET['GroupID'][0];

if(!$groupID){
	
	header("Location:index.php");
}

# Get Group Name
$groupSql = $lda->Get_Subject_eResources_Group_Sql($groupID);
$groupInfoArr =  $lda->returnArray($groupSql);
$GroupTitle = $groupInfoArr[0]['GroupTitle'];

# Page Navigation 
$PAGE_NAVIGATION[] = array($GroupTitle);

# Tool Bar
$toolbar = '';	
$toolbar = $linterface->GET_LNK_ADD("javascript:checkNew('new_member.php?groupId=$groupID')", '', '', '', '', 0);
//$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')", '', '', '', '', 0);
//$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php')", '', '', '', '', 0);

//$searchBox = $linterface->Get_Search_Box_Div('keyword', $keyword);

$html['DisplayTable'] = $ldaUI->Get_Subject_eResources_Group_Member_DBTable($groupID, $keyword);

$ldaUI->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
?>

<form name="form1" method="get">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION);?></td>	
	</tr>
	<tr>
		<td align="left"><?=$toolbar?><?=$searchBox?></td>	
	</tr>
	<tr>
		<td align="left"><?= $html['DisplayTable'];?></td>	
	</tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='index.php'") ?>
		</td>
	</tr>
		
</table>

</form>


<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>