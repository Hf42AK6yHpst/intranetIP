<?php

//using: 

/**********************************************
 * Date: 	2013-04-02 (Rita)
 * Details:	Create this page
 ************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
//include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$subject = new subject();

$ClassLevelIDArrString = $_POST['ClassLevelIDArrString'];
$selectedSubjectID = $_POST['selectedSubjectID'];

$ClassLevelID = explode(',', $ClassLevelIDArrString);


$formDataArray = $subject->Get_Form_By_Subject($selectedSubjectID);
$formDisplay = getSelectByArray($formDataArray, 'name="ClassLevelID[]" id="ClassLevelID" class="ClassLevelID" multiple size="10"', $ClassLevelID, 0, 1);

$formDisplay = str_replace("'", '"', $formDisplay);

echo $formDisplay;

intranet_closedb();
?>
