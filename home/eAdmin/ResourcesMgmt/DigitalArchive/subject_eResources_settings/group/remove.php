<?php
// Using: 
/********************************************************
 *  Modification Log
 * Date: 2013-03-26 (Rita)
 * Detail: add this page for remove group
 ********************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$lda = new libdigitalarchive();
$groupID = $_POST['GroupID'];

$successResult = array();

$lda->Start_Trans();

if(count($groupID)>0){	
	$successResult =  $lda->Remove_Subject_eResources_Group($groupID);	
}


$xmsg ='';
if(!in_array(false, $successResult, true)){
	$xmsg = 'DeleteSuccess';
	$lda->Commit_Trans();
}else{
	$xmsg = 'DeleteUnsuccess';
	$lda->RollBack_Trans();
}
header("Location:index.php?xmsg=$xmsg");

?>