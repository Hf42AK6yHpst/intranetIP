<?php

//using: 

/**********************************************
 * Date:	2013-04-08 (Rita)
 * Details: add search box
 * Date: 	2013-04-02 (Rita)
 * Details:	add this page for displaying group
 **********************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


# Set Cookies
$arrCookies = array();
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_page_size", "numPerPage");
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_page_number", "PageNumber");
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_page_order", "order");
$arrCookies[] = array("ck_Digital_Archive_Subject_eResources_Group_page_field", "field");
$arrCookies[] = array("ck_Archive_Subject_eResources_Group_page_keyword", "keyword");	

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$pic_view = '';
	$helper_view = '';
}
else {
	updateGetCookies($arrCookies);
}



# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}



$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$CurrentPageArr['DigitalArchiveSubjecteResourcesAdminMenu'] = true;
$CurrentPageArr['DigitalArchive_SubjectReference'] = true;

$CurrentPage = "Settings_Group";

$TAGS_OBJ[] = array($Lang['Menu']['DigitalArchive']['Settings']['AccessRight'] );

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();
$ldaUI->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);



# Tool Bar
$toolbar = '';
$toolbar .= $linterface->GET_LNK_NEW("javascript:checkNew('new.php')", '', '', '', '', 0);
//$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')", '', '', '', '', 0);
//$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php')", '', '', '', '', 0);

$keyword = isset($keyword)? standardizeFormPostValue($keyword): '';
$searchBox = $linterface->Get_Search_Box_Div('keyword', $keyword);


$html['DisplayTable'] = $ldaUI->Get_Subject_eResources_Group_DBTable($keyword);


?>

<form name="form1" method="get">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?><?=$searchBox?></td>
		
	</tr>
	
</table>

<?= $html['DisplayTable'];?>

</form>


<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>