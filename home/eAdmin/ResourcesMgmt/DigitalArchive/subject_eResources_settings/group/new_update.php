<?php
// Using: 

/***************************************
 * Date:	2013-04-02 (Rita)
 * Details:  Create this page for adding group
 *
 ****************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$groupID = $_POST['groupID'];
$groupCode = standardizeFormPostValue($_POST['groupCode']);
$groupName = standardizeFormPostValue($_POST['groupName']);
$description = standardizeFormPostValue($_POST['description']);
$SubjectID = $_POST['SubjectID'][0];
$ClassLevelIDArr = $_POST['ClassLevelID'];  

$lda->Start_Trans();

if($groupID!=''){	
	$successResult = $lda->Update_Subject_eResources_Group($groupID,$groupCode,$groupName,$description,$SubjectID,$ClassLevelIDArr);	
}else{	
	$successResult = $lda->Add_Subject_eResources_Group($groupCode,$groupName,$description,$SubjectID,$ClassLevelIDArr);
}

$xmsg ='';
if(!in_array(false, $successResult, true)){
	$xmsg = 'UpdateSuccess';
	$lda->Commit_Trans();	
}else{
	$xmsg = 'UpdateUnsuccess';
	$lda->RollBack_Trans();
}

header("Location:index.php?xmsg=$xmsg");

?>