<?php

//using: 

/***********************************************
 * Date:	2013-04-02	(Rita) 
 * Details:	add this page for updating new group member
 ************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

//debug_pr($_SESSION['SSV_USER_ACCESS']);

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$lsr = new libdigitalarchive();

$GroupID = $_POST['GroupID'];

$tempAry = array();
for($i=0, $i_max=sizeof($student); $i<$i_max; $i++) {
	
	$temp = is_numeric($student[$i]) ? $student[$i] : substr($student[$i],1);
	
	if($temp!="")
		$tempAry[] = $temp; 	
}

if(sizeof($tempAry)>0)
	$result = $lsr->Add_Subject_eResources_Group_Member($GroupID, $tempAry);

intranet_closedb();

$msg = ($result) ? "AddSuccess" : "AddUnsuccess";

header("Location: group_member.php?msg=$msg&GroupID[]=".$GroupID);

?>