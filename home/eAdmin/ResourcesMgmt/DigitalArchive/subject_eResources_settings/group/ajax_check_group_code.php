<?php

//using: 

/******************************************
 * Date: 	2013-04-02 (Rita)
 * Details:	create this page for checking if there are any exisiting group code
 ******************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$groupID = $_POST['groupID'];
$groupCode = standardizeFormPostValue($_POST['groupCode']);
$orignalGroupCode = standardizeFormPostValue($_POST['orignalGroupCode']);




$numOfResultArray = 0;
if($groupCode!='' && $orignalGroupCode==''){ # New Record
	$Sql = $lda->Get_Subject_eResources_Group_Sql('',$groupCode);
	$resultArray = $lda->returnArray($Sql);
	$numOfResultArray = count($resultArray);
	
	

}elseif($groupCode!='' && $orignalGroupCode!=''){ # Existing Record
	if($groupCode==$orignalGroupCode){
		// do nth	
	}elseif($groupCode!=$orignalGroupCode){
		
		$Sql = $lda->Get_Subject_eResources_Group_Sql('',$groupCode);
		$resultArray = $lda->returnArray($Sql);
		$numOfResultArray = count($resultArray);
	}	
}

if($numOfResultArray>0){
	echo 'GroupCodeAlreadyExist';	
}else{
	echo 'GroupCodeNotExist';	
}


intranet_closedb();
?>
