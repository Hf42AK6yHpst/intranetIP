<?php
# modifying : 
##### Change Log [Start] #####
#   Date    :   2014-02-24  (Tiffany)
#               Add function loadCampusTV
#
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$x = $ldaUI->Get_My_Favourite_Resource_More_Record($tagid);

$CurrentPageArr['DigitalArchive_SubjectReference'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(1);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();
echo $ldaUI->Include_JS_CSS();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" />
<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";


function goBack() {
	self.location.href = "index.php";
}

function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	
	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			Get_My_Like_More_Table();
		}
	)
	
}

function Get_My_Like_More_Table() {
	
	//$("#resource_new").html(loading);
	Block_Element("myLikeContentDiv");
	
	$.post(
		"ajax_get_my_like_more_table.php","",
		function(ReturnData)
		{
			$('#myLikeContentDiv').html(ReturnData);
			UnBlock_Element("myLikeContentDiv");
		}
	);		
}

function confirmCancelAllLike() {
	if(confirm('<?=$Lang['DigitalArchive']['jsCancelAllLikeRecord']?>')) {
		Cancel_All_Like_Action();
	}	
}

function Cancel_All_Like_Action() {
	Block_Element("myLikeContentDiv");
	
	$.post(
		"ajax_cancel_all_like_action.php","",
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			Get_My_Like_More_Table();
			UnBlock_Element("myLikeContentDiv");
		}
	);		
	
}
function loadCampusTV(id)
{
 	js_Show_ThickBox('',450,750);
	$.post(
		'ajax_load_campus_tv.php',
		{
			'tvID': id
		},
		function(data){
			$('#TB_ajaxContent').html(data);	
		}
	)		
}

$(document).ready(function(){
	initThickBox();	
});
</script>

<script language="javascript" src="digitalarchive.js"></script>


<form name="form1" id="form1">
<br style="clear:both">
<div id="content" style="padding-left: 10px; width=100%;">
	<div class="navigation_v30 navigation_2"  >
		<a href="javascript:goBack();"><?=$i_frontpage_menu_home?></a>
	</div>
	<p class="spacer"></p>
	<br style="clear:both">
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
	
				<div class="reading_board" id="resource_fav">
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span><?=$Lang["DigitalArchive"]["MyLike"]?></span></h1>
					</div></div>
					<div class="reading_board_left"><div class="reading_board_right">
				                                   
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="bottom">
								  
									<div class="table_filter">
										<p class="spacer"></p>
									</div>
									<p class="spacer"></p>
								</td>
								<td valign="bottom">
									<div class="common_table_tool">
										<a href="javascript:confirmCancelAllLike()" class="tool_delete"><?=$Lang["DigitalArchive"]["UnlikeAll"] ?></a>
									</div>
								</td>
							</tr>
						</table> 
				
						<div id="myLikeContentDiv"></div>
					
					</div></div>
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
</div>
</form>
<div id="1">
</div>

<script language="javascript">
	Get_My_Like_More_Table();
</script>
<iframe id="download_frame" name="download_frame" width="0" height="0" tabindex="-1" style="display:none;"></iframe>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>