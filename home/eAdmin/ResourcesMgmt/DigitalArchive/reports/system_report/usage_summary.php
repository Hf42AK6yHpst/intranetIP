<?php
# modifying : 

/**********************
 * Date:	2013-02-14 (Rita)
 * 			add $DigitalArchiveAdminMenu
 * Date:	2013-01-07 (Rita)
 * Details: Comment CSS
 * 
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Reports_UsageSummary";

//$TAGS_OBJ = $ldaUI->PageTagObj(2);

$TAGS_OBJ[] = array($Lang['DigitalArchive']['ReportUsage']);
$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();


$Filters = $ldaUI->Get_Category_Filter($CategoryID);

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">

<!--<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">-->
<script language="javascript">
<?
if($msg!="")
	echo "Get_Return_Message(\"".$Lang['General']['ReturnMessage'][$msg]."\")";
?>

function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
}

function showSpan(layername) {
	$('#'+layername).show();
}

function Get_Access_Right_Group_Table() {

	Block_Element("groupTableDiv");
	$.post(
		"ajax_get_usage_summary.php", 
		{ 
			dummy: "nil"
		},
		function(ReturnData)
		{
			$('#groupTableDiv').html(ReturnData);
			UnBlock_Element("groupTableDiv");		
		}
	);			
}

function goRemove(id) {
	if(confirm("<?=$Lang['DigitalArchive']['jsDeleteGroup']?>")) {
		self.location.href = "access_right_access_right_remove_update.php?GroupID="+id;
	}	
}


</script>
<div id="tempDiv"></div>

<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="access_right.php">
<!-- Content [Start] -->

<!-- navigation [Start] -->
<br />

<!--
<div class="navigation_v30 navigation_2">
	<a href="javascript:goBack();"><?=$Lang["DigitalArchive"]["SchoolAdminDoc"]?></a>
	<span><?=$Lang['DigitalArchive']['AccessRightSettings']?></span>
</div>

<div class="content_top_tool">
	<div class="Conntent_tool"><?= $Filters ?></div>
	<div class="Conntent_search"><div id="resource_search" class="resource_search_simple">
	<div class="resource_search_keywords" align="right"> 
		<input name="keyword" id="keyword" type="text"  class="keywords" style=""  value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>"/>
	</div>
</div></div><br style="clear:both" />
</div>


-->


<p class="spacer"></p>
<br style="clear:both" />
      
<!-- Access Right Group Table [Start] -->

<div class="table_board" id="groupTableDiv"></div>

<!-- Access Right Group Table [End] -->


<!-- Content [End] -->


<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="status_option_value" id="status_option_value" value="">
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="">
</form>
<br>
</div>

<script language="javascript">
Get_Access_Right_Group_Table();

<?
if($msg!="")
	echo "Get_Return_Message(\"".$Lang['StaffAttendance'][$msg]."\")";
?>
</script>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>