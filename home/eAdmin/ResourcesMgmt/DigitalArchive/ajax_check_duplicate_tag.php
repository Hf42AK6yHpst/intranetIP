<?php
// editing by 

/*************************************************
 *	Date:		2013-04-02 (Rita) 
 *  Details:	create this page for checking duplicated tag
 *************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# 1 = duplicated
$result = $lda->Is_Duplicate_Tag($TagName, $TagID);

echo $result;

intranet_closedb();
?>