<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

if(sizeof($_POST)==0 || $TagID == "") {
	echo "<script language='javascript'>window.tb_remove();</script>";
	exit;	
} 



$tagName = returnModuleTagNameByTagID($TagID);

$FormButton = $ldaUI->GET_ACTION_BTN($button_submit, "button", "checkForm()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_cancel, "button", "window.tb_remove()");

?>


<script language="javascript">
function checkForm() {
	
	$("#div_err_Tag").html();
	
	var obj = document.editForm;
	var focus_field = "";
	var error_no = 0;
	
	if(!check_text_30(obj.Tag, "<?=$i_alert_pleasefillin.$Lang["DigitalArchive"]["Tag"]?>.","div_err_Tag"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Tag";  
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		checkTagNameDuplication();
	}
}

function checkTagNameDuplication() {
	var obj = document.editForm;
	
	var PostVar = Get_Form_Values(document.getElementById("editForm"));

	$.post(
		"ajax_check_duplicate_tag.php",
		{
			TagName: document.getElementById('Tag').value,
			TagID: document.getElementById('TagID').value
		},
		function(ReturnData)
		{
			
			if(ReturnData==1) {
				$('#div_err_Tag').html('<font color="red"><?=$Lang["DigitalArchive"]["Tag"].$Lang['DigitalArchive']['jsIsDuplicated']?></font>');
				obj.Tag.select();
				return false;	
				
			} else {
				if(confirm("<?=$Lang['DigitalArchive']['jsUpdateTag']?>")) {
					Update_Edit_Info();
				} else {
					return false;	
				}
			}	
		}
	);		
}

function Update_Edit_Info() {
	document.editForm.action = "tag_update.php";	
	document.editForm.submit();
}

</script>

<br style="clear:both" />

<form name="editForm" id="editForm" method="POST" action="">

<div class="table_board">
	<table class="form_table_v30 ">
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Tag"] ?></td>
			<td>
				<input name="Tag" id="Tag" type="text" class="textboxtext" value="<?=intranet_htmlspecialchars($tagName[0])?>" />
				<span id="div_err_Tag"></span>
			</td>
		</tr>
	</table>
	<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span> 
	<p class="spacer"></p>
</div>			

<div class="edit_bottom">
	<p class="spacer"></p>
	<?=$FormButton?>
	<p class="spacer"></p>
</div>			


<input type="hidden" name="TagID" id="TagID" value="<?=$TagID?>">

</form>

<?
echo $ldaUI->FOCUS_ON_LOAD("editForm.Tag"); 
intranet_closedb();
?>