<?php
# modifying : 
/*
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: added libdigitalarchive->Authenticate_DigitalArchive()
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

if(!isset($tagid) || $tagid=="") {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$lda->Authenticate_DigitalArchive();

$x = $ldaUI->Get_Admin_Doc_Item_By_TagID($tagid);


$tagName = returnModuleTagNameByTagID($tagid);

$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";


function goCancel() {
	document.form1.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function checkboxSelect(field, val) {
	if(val==true) {
		$('input[name='+field+']').attr('checked', true);
	} else {
		$('input[name='+field+']').attr('checked', false);	
	}
}

function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	
	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			//Get_Resource_MyFavourite_Table();
		}
	)
	
}

function goBack() {
	self.location.href = "index.php";
}

</script>


<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1">
<br style="clear:both">
<div id="content" style="padding-left: 10px; width=100%;">
	<div class="navigation_v30 navigation_2">
		<a href="javascript:goBack();"><?=$i_frontpage_menu_home?></a>
		<a href="displayResult.php?flag=<?=FUNCTION_TAG?>"><?=$Lang["DigitalArchive"]["Tag"]?></a>
	</div>
	<p class="spacer"></p>
	<br style="clear:both">
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
	
				<div class="reading_board" id="resource_tag">
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span><?=$tagName[0]?></span></h1>
					</div></div>
					
					<div class="reading_board_left"><div class="reading_board_right">
						<div class="tag_list"></div>						
						<?=$x?>
					
					</div></div>
					
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
</div>


</form>
</div>


<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>