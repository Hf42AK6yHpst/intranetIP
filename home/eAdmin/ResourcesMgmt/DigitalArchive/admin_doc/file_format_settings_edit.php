<?php


# modifying : 

#####################################################
#
#	This is the page to edit the file format setting
#
#####################################################

/**********************
 * 
 * Date:	2013-07-08 (Henry Chan)
 * Details: File Created and set the user interface
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

if(!isset($comeFrom) || $comeFrom=="")
	$comeFrom = "file_format_settings.php";

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$lgs = new libgeneralsettings();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_UploadPolicy";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalArchive']['MaxFileSize'],"maximum_file_size.php",0);
$TAGS_OBJ[] = array($Lang['DigitalArchive']['FileFormatSettings'],$comeFrom,1);
$TAGS_OBJ[] = array($Lang['DigitalArchive']['UploadNotice'],"upload_notice.php",0);

# Display Navigation Bar
//$PAGE_NAVIGATION[] = array($Lang['DigitalArchive']['FileFormatSettings'], "javascript:goURL('$comeFrom')");
//$PAGE_NAVIGATION[] = array($Lang['DigitalArchive']['NewFileFormatSettings'], "");

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

# Display Tools Bar Manu
//$toolbar .= $linterface->GET_LNK_NEW("javascript:editSize()",$button_new,"","","",0);
?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">

<!--<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">-->
<script language="javascript">
<?


if ($msg != "")
	echo "Get_Return_Message(\"" . $Lang['General']['ReturnMessage'][$msg] . "\")";
?>

//-- New functions [Start] --
function goSubmit(urlLink) {
	//Get the input of the file_format_list 
	var obj = document.form1;
	var letterNumber = /^[0-9a-zA-Z\n\r]+$/;
	var inputStr = obj.txt_file_format_list.value;
	inputStr = inputStr.replace(/\s+/g, "");
	
	if (inputStr==""){
		alert ("<?=$Lang['General']['JS_warning']['CannotBeBlank']?>");
		obj.txt_file_format_list.select();
		return false;
	}
	else if (!inputStr.match(letterNumber)){
		alert ("<?=$Lang['DigitalArchive']['jsFileFormatWarning']?>");
		obj.txt_file_format_list.select();
		return false;
	}
	
	obj.action = urlLink;
	obj.submit();
}

function goURL(urlLink, id) {
	if(id!=undefined) {
		document.form1.uid.value = id;
		document.form1.fromParent.value = 1;
	}
	document.form1.action = urlLink;
	document.form1.submit();
}

//function editSize() {
//	document.form1.action = "file_format_settings_edit.php";
//	document.form1.submit();
//}
//-- New functions [End] --

/*function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
}

function showSpan(layername) {
	$('#'+layername).show();
}

function Get_Access_Right_Group_Table() {

	Block_Element("groupTableDiv");
	$.post(
		"ajax_get_access_right_group_category_table.php", 
		{ 
			keyword: $('#keyword').val()
		},
		function(ReturnData)
		{
			$('#groupTableDiv').html(ReturnData);
			UnBlock_Element("groupTableDiv");		
		}
	);			
}

function goRemove(id) {
	if(confirm("<?=$Lang['DigitalArchive']['jsDeleteCategory']?>")) {
		self.location.href = "access_right_access_right_remove_category_update.php?GroupCategoryID="+id;
	}	
}*/
</script>

<!--<div id="tempDiv"></div>-->
<div class="navigation_v30"><span><?=$Lang['DigitalArchive']['NewFileFormatSettings']?></span></div>
<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="">
<!-- Content [Start] -->

<!-- navigation [Start] -->


<!--
<div class="navigation_v30 navigation_2">
	<a href="javascript:goBack();"><?=$Lang["DigitalArchive"]["SchoolAdminDoc"]?></a>
	<span><?=$Lang['DigitalArchive']['AccessRightSettings']?></span>
</div>
-->
<!--<div id="resource_search" class="resource_search_simple">
	<div class="resource_search_keywords" align="right"> 
		<input name="keyword" id="keyword" type="text"  class="keywords" style=""  value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>"/>
	</div>
</div>

<p class="spacer"></p>
<br style="clear:both" />-->


<!-- File Size Edit Tool Bar [End] -->
      
<!-- File Size Edit Form [Start] -->

<div class="table_board">
	<table class="form_table_v30">
		<!--<tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>-->
		<tr>
	     <td class="field_title"><?=$Lang['DigitalArchive']['ListofFileFormats']?></td>
	     <td><label><textarea cols="40" rows="25" name="txt_file_format_list"><?
//if ($_REQUEST['txt_file_format_list'] != '') {
//	$fileFormatArr = explode("\n", $_REQUEST['txt_file_format_list']);
//	if ($lda->Save_File_Format_Setting($fileFormatArr)){
//		//$linterface->LAYOUT_START("Update Success!");
//		echo "Update Success!";
//	}
//	else{
//		//$linterface->LAYOUT_START("Error or File Format Exist!");
//		echo "Error or File Format Exist!";
//	}
//}

//$settingArr = $lda->Get_File_Format_Setting();
////$settingArr['FileFormat'] = array('JPG', 'PNG', 'DOC');
//if ($settingArr != '') {
//	$tempArr = array ();
//	foreach ($settingArr as $subarray) {
//		array_push($tempArr, $subarray['FileFormat']);
//	}
//	$fileFormatStr = implode("\n", $tempArr);
//	echo intranet_htmlspecialchars($fileFormatStr);
//}
?></textarea></label><div class="tabletextremark"><?=$Lang['DigitalArchive']['ListofFileFormatsRemark']?></div></td>
	   </tr>		
	</table>
</div>
<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","goSubmit('file_format_settings_update.php')") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom')") ?>
</div>


<!-- File Size Edit Form [End] -->


<!-- Content [End] -->

</form>
<br>
</div>

<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>