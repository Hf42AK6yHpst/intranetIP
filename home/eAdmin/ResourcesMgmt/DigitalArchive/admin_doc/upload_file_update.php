<?php
// Editing by 
/*
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 * 2014-09-04 (Carlos): Base on security consideration, do not use sudo command to create root directory. Will create manully when install module. 
 * 2014-08-29 (Carlos): Use shell command to create root directory /digital_archive/admin_doc/ if php file operation fail
 * 2013-07-10 (Carlos): Added file size and file format restriction checking. Would also check files inside zip file. 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();
$lgs = new libgeneralsettings();
$lda = new libdigitalarchive();

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';

$FileName = substr(strrchr($FileName, '\\\\'),1);

$resultCreatedFolder = false;
$linux_username = get_current_user(); // usually is junior on EJ, eclass if IP25

$result = array();
$returnMsg = '';
if (!empty($_FILES)) {
	
	# Restricted file formats and maximum file size
	$tempFileFormatAry = $lda->Get_File_Format_Setting();
	$FileFormatAry = array();
	$maxFileSizeSettings = $lgs->Get_General_Setting("DigitalArchive", array("'MaximumFileSize'"));
	$MaxFileSize = $maxFileSizeSettings['MaximumFileSize']; // MB
	$RestrictFileSize = is_numeric($MaxFileSize) && $MaxFileSize > 0;
	$MaxFileSizeInBytes = floatval($MaxFileSize) * 1024 * 1024;
	for($i=0;$i<count($tempFileFormatAry);$i++) {
		$file_format = strtolower(trim($tempFileFormatAry[$i]['FileFormat']));
		if($file_format != '') {
			$FileFormatAry[] = $file_format;
		}
	}
	$RestrictFileFormat = count($FileFormatAry) > 0;
	
	$targetPath = "/digital_archive/";
	$targetPath_2ndLevel = "/admin_doc/";
	# create folder

	$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath);
	
	if ( file_exists($target1stPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
		/*
		if(!$resultCreatedFolder){ // try use shell command if php file operation fail
			$absolute_target1stPath = str_replace('//', '/', $intranet_root."/../../".$targetPath);
			shell_exec("sudo mkdir $absolute_target1stPath");
			shell_exec("sudo chown ".$linux_username.".".$linux_username." $absolute_target1stPath");
			shell_exec("sudo chmod 0777 $absolute_target1stPath");
		}
		*/
	}
	
	$targetFullPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath.$targetPath_2ndLevel);
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
		/*
		if(!$resultCreatedFolder){
			$absolute_targetFullPath = str_replace('//', '/', $intranet_root."../../".$targetPath.$targetPath_2ndLevel);
			shell_exec("sudo mkdir $absolute_targetFullPath");
			shell_exec("sudo chown ".$linux_username.".".$linux_username." $absolute_targetFullPath");
			shell_exec("sudo chmod 0777 $absolute_targetFullPath");
		}
		*/
	}
	
	chmod($targetFullPath, 0777);
	
	$numOfFiles = count($_FILES['File']['name']);

	# get file name
	$tempFile = $_FILES['File']['tmp_name'];
	
	$origFileName =  $_FILES['File']['name'];
	if (empty($origFileName)) continue;
	
	$fileSize = $_FILES['File']['size'];
	
	# Check file size
	if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
		$result[] = false;
		$returnMsg = 'FileSizeExceedLimit';
	}
	# Extension
	$ext = substr($libfilesystem->file_ext($origFileName),1);
	
	# Check file format
	if($RestrictFileFormat && in_array(strtolower($ext),$FileFormatAry)){
		$result[] = false;
		$returnMsg = 'FileTypeIsBanned';
	}
	
	# Check if zip files contains restricted file types
	if(!in_array(false,$result) && $RestrictFileFormat && $ext == 'zip') {
		$isZipValid = true;
		
		$tempUserFolder = "/u".$_SESSION['UserID']."_".time();
		$tempUnzipFolderLevel1 = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp");
		if ( !file_exists($tempUnzipFolderLevel1) || !is_dir($tempUnzipFolderLevel1)) {
			$libfilesystem->folder_new($tempUnzipFolderLevel1);
		}
		$tempUnzipFolderLevel2 = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$tempUserFolder);
		if ( !file_exists($tempUnzipFolderLevel2) || !is_dir($tempUnzipFolderLevel2)) {
			$libfilesystem->folder_new($tempUnzipFolderLevel2);
		}
		
		$tempUnzipfolder = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$tempUserFolder."/tmp_unzip");
		if(file_exists($tempUnzipfolder)) {
			$libfilesystem->deleteDirectory($tempUnzipfolder);
		}
		$libfilesystem->folder_new($tempUnzipfolder);
		if(file_exists($tempUnzipfolder)) {
			chmod($tempUnzipfolder, 0777);
		}
		
		$tempCopyZipFile = str_replace('//', '/', $tempUnzipFolderLevel2.'/'.$origFileName);
		$libfilesystem->lfs_copy($tempFile, $tempCopyZipFile);
		
		$libfilesystem->file_unzip($tempCopyZipFile,$tempUnzipfolder);
		$tempUnzipFileAry = $libfilesystem->return_folderlist($tempUnzipfolder);
		
		for($j=0;$j<count($tempUnzipFileAry);$j++) {
			$tempUnzipFileExt = strtolower( substr($libfilesystem->file_ext($tempUnzipFileAry[$j]),1) );
			
			if(in_array($tempUnzipFileExt, $FileFormatAry)) {
				$isZipValid = false;
				break;
			}
		}
		
		if(file_exists($tempUnzipfolder)) {
			$libfilesystem->deleteDirectory($tempUnzipfolder);
		}
		if(file_exists($tempUnzipFolderLevel2)){
			$libfilesystem->deleteDirectory($tempUnzipFolderLevel2);
		}
		
		if(!$isZipValid) {
			$result[] = false;
			$returnMsg = 'ZipFileContainsRestrictedFileType';
		}
	}
	
	# valid file
	if(!in_array(false, $result)) 
	{
		$timestamp = date('YmdHis');
		
		# encode file name
		$targetFileHashName = "u".$UserID."_".$timestamp;
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# upload file
		$uploadResult = move_uploaded_file($tempFile, $targetFile);
		
		chmod($targetFile, 0777);
		
		# Extension
		//$ext = substr($origFileName, strrpos($origFileName, '.') + 1);
	//	$ext = substr($libfilesystem->file_ext($origFileName),1);
		
		if($uploadResult) {
			# add record to DB
			$dataAry = array();
			$dataAry['Title'] = ($Title=="") ? $origFileName : $Title;
			$dataAry['Description'] = $Description;
			$dataAry['FileFolderName'] = $origFileName;
			$dataAry['FileHashName'] = $targetFileHashName;
			$dataAry['FileExtension'] = strtolower($ext);
			$dataAry['VersionNo'] = 1;
			$dataAry['IsLatestFile'] = 1;
			$dataAry['ParentFolderID'] = $FolderID;
			$dataAry['GroupID'] = $GroupID;
			$dataAry['SizeInBytes'] = $_FILES['File']['size'];
			
			if(isset($RecordID) && $RecordID!="") {	# update
				//$RecordID = $lda->updateResourceRecord($RecordID, $dataAry);
			} else {								# insert
				$RecordID = $lda->insertAdminRecord($dataAry);
			}
			
			if($TagName != "") {
				$lda->Update_Admin_Doc_Tag_Relation($TagName, $RecordID, $lda->AdminModule);
			} else {
				$inHeritTag = 1;	
			}
			
			if($IsDuplicateFlag) {
				$lda->Update_Duplicate_Record($RecordID, $GroupID, $FolderID, $Title, $origFileName, $inHeritTag);
				
			} else {
				
			}
		}
	}
}

intranet_closedb();

if($returnMsg != '') {
	$msg = $returnMsg;
}else if($RecordID) {
	$msg = "AddSuccess";
}else {
	$msg = "AddUnsuccess";
}
if($viewMode == "icon") {
	header("Location: fileList_Icon.php?GroupID=$GroupID&FolderID=$FolderID&folderTreeSelect=$folderTreeSelect&msg=$msg");
}else{
	header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&folderTreeSelect=$folderTreeSelect&msg=$msg");
}
?>