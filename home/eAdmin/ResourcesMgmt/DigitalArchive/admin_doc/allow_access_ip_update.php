<?php 
// modifing:
##################
#	2017-07-10 Anna
#	- created this file to update computer IP access right 
#
##################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$lda = new libdigitalarchive();

$lda->Authenticate_DigitalArchive();

// post data
$AllowAccessIP = trim($_POST['AllowAccessIP']);

// get current IP settings
$currentIP = $lda->GetDigitalArchiveGeneralSetting("AllowAccessIP");

// update
if(sizeof($currentIP)>0){
	$sql = "Update GENERAL_SETTING
	Set Module='DigitalArchive', SettingValue='$AllowAccessIP', DateInput=NOW(), ModifiedBy='".$_SESSION['UserID']."'
			WHERE SettingName='AllowAccessIP' AND Module='DigitalArchive'";
}
// insert
else{
	$sql = "INSERT INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput,InputBy, DateModified, ModifiedBy)
	VALUES ('DigitalArchive', 'AllowAccessIP', '$AllowAccessIP', NOW(),'".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
}
$result = $lda->db_db_query($sql);

if($result)
	$msg = "update";
	else
	$msg = "update_failed";
		
intranet_closedb();
		
header('Location: allow_access_ip_index.php');
?>