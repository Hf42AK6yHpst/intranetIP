<?php
// editing by 
/*
 * 2013-01-16 (Carlos): added checking for duplication with others file
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();


$lda = new libdigitalarchive();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$FileName = substr(strrchr($FileName, '\\\\'),1);


if($Title=="") $Title = $FileName;

$result = $lda->Is_Duplicate_Admin_Document($GroupID, $FolderID, $Title, $FileName, $documentID);

if($result == 1) { // if has duplication, maybe duplicate my own file or others file
	$canManageOthers = 0;
	$adminInGroup = $lda->returnAdmin();
	if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
		// is super admin ?
		$canManageOthers = 1;
	} else if($adminInGroup[$GroupID]) {
		// does this admin user has group manage others file access right?
		if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE")) {
			$canManageOthers = 1;	
		}
	} else if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE")) {
		// does this member user has group manage others file access right?
		$canManageOthers = 1;	
	}	
	// no manage others access right, check any duplication with others file
	if($canManageOthers == 0){
		$isDuplicateOthers = $lda->Is_Duplicate_Others_Document($GroupID, $FolderID, $Title, $FileName, $documentID);
		
		if($isDuplicateOthers) {
			$result = 2;
		}
	}
}

/*
switch ($result) {
	case 1 : duplicated "Title" or "File name";
	case 0 : no duplication;
	case 2 : no manage others file access file but uploadd file name duplicate with others file; 
} 
*/

echo $result;

intranet_closedb();
?>