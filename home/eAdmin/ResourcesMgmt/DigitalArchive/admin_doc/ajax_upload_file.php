<?
// editing by 
/*
 * 2019-11-12 (Sam): Added max. file upload size remark 
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 * 2014-05-29 (Tiffany): Add group title as tags when upload new file
 * 2014-04-07 (Tiffany): Use exist tags to upload new file
 * 2013-07-12 (Henry): added upload instruction on the page
 * 2013-07-08 (Carlos): added plupload support for multiple file upload
 * 2013-01-16 (Carlos): modified js checkFileNameDuplication() - added return code 2 for unable to overwrite others file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$FolderID = IntegerSafe($FolderID);
$GroupID = IntegerSafe($GroupID);
$DocumentID = IntegerSafe($DocumentID);

if($viewMode != 'icon' || $viewMode != 'list')  {
    $viewMode = 'list';
}
if($folderTreeSelect != '' && $folderTreeSelect != 'selected') {
    $folderTreeSelect = '';
}
### Handle SQL Injection + XSS [END]

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();
$lgs = new libgeneralsettings();
$libdb = new libdb(); 

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';

$is_below_IE8 = ($userBrowser->browsertype == "MSIE" && intval($userBrowser->version) < 8);
$is_older_firefox = ($userBrowser->browsertype == "Firefox" && intval($userBrowser->version) < 4);
$is_mobile_tablet_platform = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod");
$is_chrome = $userBrowser->isChrome();
$use_plupload = !$is_mobile_tablet_platform && !$is_below_IE8 && !$is_older_firefox;

$FormButton = $ldaUI->GET_ACTION_BTN($button_submit, "button", "checkForm()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_reset, "reset", "resetAllSpanInnerHtml()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_cancel, "button", "window.tb_remove()");

$tagAry = returnModuleAvailableTag($lda->AdminModule, 1);
$tagNameAry = array_values($tagAry);

$delim = "";
if(sizeof($tagNameAry>0)) {
	foreach($tagNameAry as $tag) {
		$availableTagName .= $delim."\"".addslashes($tag)."\"";
		$delim = ", ";
	}
}

if($use_plupload)
{
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$lfs = new libfilesystem();
	
	$pluploadButtonId = 'UploadButton';
	$pluploadDropTargetId = 'DropFileArea';
	$pluploadFileListDivId = 'FileListDiv';
	$pluploadContainerId = 'pluploadDiv';
	
	$tempFolderPath = '/u'.$_SESSION['UserID'].'_'.time();
	
	// Clean old temp folders
	$sql = "SELECT * FROM DIGITAL_ARCHIVE_TEMP_UPLOAD_FOLDER WHERE UserID = '".$_SESSION['UserID']."' AND TIMESTAMPDIFF(SECOND,InputDate,NOW()) > 86400";
	$tempFolderAry = $lda->returnResultSet($sql);
	$folderCount = count($tempFolderAry);
	
	$folderIdToRemove = array();
	for($i=0;$i<$folderCount;$i++) {
		$tempFolder = $tempFolderAry[$i]['Folder'];
		
		$tempDeleteFolderPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$tempFolder);
		if(file_exists($tempDeleteFolderPath)) {
			$lfs->deleteDirectory($tempDeleteFolderPath);
		}
		$folderIdToRemove[] = $tempFolderAry[$i]['FolderID'];
	}
	if(count($folderIdToRemove) > 0){
		$sql = "DELETE FROM DIGITAL_ARCHIVE_TEMP_UPLOAD_FOLDER WHERE UserID = '".$_SESSION['UserID']."' AND FolderID IN (".implode(",",$folderIdToRemove).")";
		$lda->db_db_query($sql);
	}
	
	// Store the new temp folder
	$sql = "INSERT INTO DIGITAL_ARCHIVE_TEMP_UPLOAD_FOLDER (UserID,Folder,InputDate) VALUES ('".$_SESSION['UserID']."','".$tempFolderPath."',NOW())";
	$lda->db_db_query($sql);
}

# add exist tags
$default_tags = "";
if($DocumentID == "")
{
    $folders = "";
    $file_folderID = $FolderID;
    while ($file_folderID != 0){
        $sql = "select ParentFolderID, FileFolderName from DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD where DocumentID = '".$file_folderID."'";
        $folder_array = $libdb->returnArray($sql);
        if(!isset($exist_tag[$folder_array[0][1]])){
            $folders .= $folder_array[0][1].","; 
            $exist_tag[$folder_array[0][1]] = true;
        }
        $file_folderID = $folder_array[0][0];
    }
    
    $sql = "select GroupTitle from DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP where GroupID = '".$GroupID."'";
    $groupTitle_return = $libdb->returnVector($sql);
    $groupTitle = $groupTitle_return[0];
    
    if($folders==""){
        $newTags = $groupTitle;
    }
    else{
        $folders = $folders.$groupTitle;
        $newTags = $folders;
    }
    $default_tags = intranet_htmlspecialchars($newTags);
}
else
{
    $DocumentID = explode(',', $DocumentID);
    for($i=0; $i<count($DocumentID); $i++){
        $tagDisplay = $lda->Display_Tag_By_Admin_Document_ID($DocumentID[$i], 'textWithoutLink');
        $tagAarry = explode(',', $tagDisplay);
        for($j=0; $j<count($tagAarry); $j++){
            $this_tag = trim($tagAarry[$j]);
            if(!isset($exist_tag[$this_tag]))
            {
                $exist_tag[$this_tag] = true;
                if($this_tag!="")$default_tags .= $this_tag.",";
            }
        }
    }
    $default_tags = intranet_htmlspecialchars(rtrim($default_tags,","));
}
?>

<script language="">
<?php if($use_plupload) { ?>
function checkForm() {
	resetAllSpanInnerHtml();
	
	var obj = document.formUploadFile;	
	var warnDv = $('#FileWarnDiv');
	error_no = 0;
	focus_field = "";
	
	if(jsNumberOfFileUploaded() == 0) {
		warnDv.html('<?=$Lang['DigitalArchive']['jsSelectFiles']?>').show();
		setTimeout(hideFileWarningTimerHandler,5000);
		return false;
	}
	
	if(!jsIsFileUploadCompleted()) {
		warnDv.html('<?=$Lang['DigitalArchive']['jsWaitAllFilesUploaded']?>').show();
		setTimeout(hideFileWarningTimerHandler,5000);
		return false;
	}
	
	warnDv.hide();
	
	//obj.submit();
	checkSubmit();
}

function checkSubmit()
{
	Block_Thickbox();
	$.post(
		'ajax_check_multiple_upload.php',
		{
			'TargetFolder':$('#TargetFolder').val()
		},
		function(ReturnMsg){
			var warnDv = $('#FileWarnDiv');
			UnBlock_Thickbox();
			if(ReturnMsg != ''){
				warnDv.html(ReturnMsg).show();
			}else{
				warnDv.hide();
				document.formUploadFile.submit();
			}
		}
	);
}

function hideFileWarningTimerHandler()
{
	$('#FileWarnDiv').hide();
}
<?php }else{ ?>
function checkForm() {
	
	resetAllSpanInnerHtml();
	
	var obj = document.formUploadFile;	
	error_no = 0;
	focus_field = "";
	
	if(!check_text_30(obj.File, "<?=$i_alert_pleaseselect.$Lang["DigitalArchive"]["File"]?>.","div_err_File"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "File";  
	}
	if(obj.Title.value.length > <?=$lda->MaxLengthOfFileName?>) {
		alert("<?=$Lang['DigitalArchive']['FileNameWarning']?>");
		if(focus_field=="")	focus_field = "File";
		error_no++;
	}
	if(obj.Title.value!="") {
		if(!validateFilename(obj.Title.value, '<?=$Lang['DigitalArchive']['jsDisplayNameCannotWithSpecialCharacters']?>')) 
		{
			return false;	
		}
		
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		//obj.submit();
		checkFileNameDuplication();
	}
}
<?php } ?>
function checkFileNameDuplication() {
	
	var obj = document.formUploadFile;

	$('#FileName').val(document.getElementById('File').value);
	var PostVar = Get_Form_Values(document.getElementById("formUploadFile"));

	$.post(
		"ajax_check_duplicate_record.php",PostVar,
		function(ReturnData)
		{
			if(ReturnData==2) {
				alert('<?=$Lang['DigitalArchive']['jsDuplicatedOthersFileName']?>');
			}else if(ReturnData==1) {
				//$('#tempdiv2').html(ReturnData);
				//return false;
				if(confirm("<?=$Lang['DigitalArchive']['jsDuplicatedTitleOrFileName']?>")) {
					$('#IsDuplicateFlag').val(1);
					obj.submit();	
				}
			} else {
				obj.submit();	
			}	
		}
	);
}

function resetAllSpanInnerHtml() {
	$('#div_err_Title').html('');
	$('#div_err_File').html('');
}

function displayTitle(val) {
	var s = document.getElementById('File').value;
	var pos = s.lastIndexOf("\\");
	document.getElementById('Title').value = s.substring(pos+1);
	document.getElementById('Title').select();
}

</script>
<div id=tempdiv2></div>

<? $settingNameValue = $lgs->Get_General_Setting("DigitalArchive", array (
	"'UploadNotice'"
));
	$enableStr = $lgs->Get_General_Setting("DigitalArchive", array ("'UploadNoticeEnable'"));
	
if(trim($settingNameValue['UploadNotice']) != '' && $enableStr['UploadNoticeEnable'] == '1'){
?>
<fieldset class="instruction_box">
<legend class="instruction_title"><?=$Lang['DigitalArchive']['UploadInstruction']?></legend>
<?
	//$strArr = explode("\n", $settingNameValue['UploadNotice']);
	$strArr = trim(nl2br($settingNameValue['UploadNotice']));
	echo $strArr;
//	foreach($strArr as $tempStr){
//		if(trim($tempStr) != '')
//			echo "<li>".stripslashes($tempStr)."</li>";
//	}
?>
</fieldset>
<?}?>

<br style="clear:both" />

<form name="formUploadFile" id="formUploadFile" method="POST" action="<?=$use_plupload?"upload_multiple_files_update.php":"upload_file_update.php"?>" enctype="multipart/form-data">

			<div class="table_board">
				<table class="form_table_v30 ">
					<tr>
						<td class="field_title">
							<span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Source"]?>
						</td>
						<td> 
				<?php if($use_plupload){ ?>
							<div id="<?=$pluploadContainerId?>"></div>
							<?=$ldaUI->GET_SMALL_BTN($Lang['DigitalArchive']['SelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='')?>
							<div id="<?=$pluploadDropTargetId?>" style="display:none;" class="DropFileArea"><?=$Lang['DigitalArchive']['OrDragAndDropFilesHere']?></div>
							<div id="<?=$pluploadFileListDivId?>"></div>
							<?=$ldaUI->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv')?>
				<?php }else{ ?>
							<input type="file" name="File" id="File" class="textbox" onChange="displayTitle(this.value)"/>
				<?php } ?>
							<span id="div_err_File"></span>
						</td>
					
					</tr>
					
	<?php if(!$use_plupload){ ?>
					<tr>
						<td class="field_title"><?=$Lang["DigitalArchive"]["DisplayName"]?></td>
						<td>
							<input name="Title" id="Title" type="text" class="textboxtext" value="" />
							<span id="div_err_Title"></span>
						</td>
					</tr>
	<?php } ?>						
					<tr>
						<td class="field_title"><?=$Lang["DigitalArchive"]["Description"]?></td>
						<td><?=$ldaUI->GET_TEXTAREA("Description", $Description, 70, 5, "", "", "", "textboxtext");?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$Lang["DigitalArchive"]["Tag"]?></td>
						<td><?/*=$ldaUI->GET_TEXTAREA("Tag", $Tag, 70, 5, "", "", "", "textboxtext");*/?>
							<input type="text" name="TagName" id="TagName" class="textbox" value="<?=$default_tags?>"/>
						<span class="tabletextremark">(<?=$Lang['StudentRegistry']['CommaAsSeparator'].", ".$Lang["DigitalArchive"]["Max10Words"]?>)</span></td>
					</tr>
				</table>
				<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span> 
				<p class="spacer"></p>
		<?php 
			$fileSettingNameValue = $lgs->Get_General_Setting("DigitalArchive", array ("'MaximumFileSize'"));
			$maxFileSize = trim($fileSettingNameValue['MaximumFileSize']);
			if($maxFileSize != '0'):
		?>
				<span class="tabletextrequire">*</span>
				<span class="tabletextremark"><?=str_replace('<!--SIZE-->', $maxFileSize, $Lang['DigitalArchive']['jsMaxFileSizeWarning'])?></span>
				<p class="spacer"></p>
		<?php endif; ?>
				</div>			
				<div id="supplementaryInfoDiv"></div>
				<div class="edit_bottom">
				<p class="spacer"></p>
				<?=$FormButton?>
				<p class="spacer"></p>
			</div>			
<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="IsDuplicateFlag" id="IsDuplicateFlag" value="">
<input type="hidden" name="FileName" id="FileName" value="">
<?php if($use_plupload) { ?>
<input type="hidden" name="TargetFolder" id="TargetFolder" value="<?=$tempFolderPath?>">
<?php } ?>
<input type="hidden" name="viewMode" id="viewMode" value="<?=$viewMode?>" />
<input type="hidden" name="folderTreeSelect" id="folderTreeSelect" value="<?=$folderTreeSelect?>" />
</form>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js" type="text/javascript"></script>

<script language="javascript">
$('#TagName').multicomplete({
	source: [<?=$availableTagName?>]
});
</script>

<?
if($use_plupload) {
	include_once("plupload_script.php");
}
echo $ldaUI->FOCUS_ON_LOAD("formUploadFile.File"); 
intranet_closedb();
?>
