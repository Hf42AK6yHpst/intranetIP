<?php
# modifying : 
/*
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: added libdigitalarchive->Authenticate_DigitalArchive()
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

//debug_pr($_GET);
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);


/*
if ($page_size_change == 1)
{
    //setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    //$ck_page_size = $numPerPage;
}
*/

if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$lda->Authenticate_DigitalArchive();

# update time of last visit time of group
if(!isset($_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]) || $_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]=="" || $_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]!=date('Y-m-d H:i:s')) {
	$lda->updateLastVisitOfDigitalArchive($GroupID);	
}

$groupInfo = $lda->getGroupInfo($GroupID);

$navTitle = $Lang["DigitalArchive"]["SearchResult"];

$flag = ($flag=="") ? FUNCTION_ADVANCE_SEARCH : $flag;

if($advanceSearchFlag) {
	if(strlen($StartDate)==10) $StartDate .= " 00:00:00";
	if(strlen($EndDate)==10) $EndDate .= " 23:59:59";
	
	if($StartDate!='' && $EndDate!='') $conds .= " AND (rec.DateInput BETWEEN '$StartDate' AND '$EndDate')";	
	if($str!='') $conds .= " AND (rec.Title LIKE '%$str%' OR rec.FileFolderName LIKE '%$str%' OR rec.Description LIKE '%$str%')";
	if($createdBy!='') $conds .= " AND (USR.ChineseName LIKE '%$createdBy%' OR USR.EnglishName LIKE '%$createdBy%' OR USR.UserLogin LIKE '%$createdBy%')";
	if(sizeof($TagID)>0) $conds .= " AND tu.TagID IN (".implode(',',$TagID).")";

} else {

	if($keyword!='') $conds .= " AND (rec.Title LIKE '%$keyword%' OR rec.FileFolderName LIKE '%$keyword%' OR rec.Description LIKE '%$keyword%')";
	//$conds .= " AND rec.GroupID='$GroupID'";	
	
}

$table = $ldaUI->Get_Display_AdvanceSearchResult_Table($field, $order, $pageNo, $flag, $conds, $GroupID);

$tagInfo = $lda->Get_Tag_Used_In_Group($GroupID);
$TagSelection = getSelectByArray($tagInfo, 'name="TagID[]" id="TagID[]" multiple size="5"', $TagID, 0, 1);

$searchOption = "
	<div id=\"resource_search\" class=\"resource_search_simple\">
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 1)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
		<div class=\"resource_search_keywords\"><input name=\"keyword\" id=\"keyword\" type=\"text\"  class=\"keywords\" style=\"\" value=\"$keyword\" />
		</div>
		
	</div>";

$searchOptionAdv = "	
	<div id=\"resource_search_adv\" class=\"resource_search_advance\" style=\"display:none\">
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 0)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
		<div class=\"resource_search_keywords\"><input name=\"str\" id=\"str\" type=\"text\" class=\"keywords\" style=\"\" value=\"$str\" /></div>
			<p class=\"spacer\"></p>
			<div class=\"resource_search_advance_form\">
				<div class=\"table_board\">
					<table class=\"form_table \">
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["UploadDate"]."</td>
							<td> 
								<input type=\"checkbox\" name=\"byDateRange\" id=\"byDateRange\" onClick=\"getCheckDateRange(this.checked)\" ".($byDateRange ? "checked" : "")."/> 
								".$linterface->GET_DATE_PICKER("StartDate", substr($StartDate,0,10))." $i_To ".$linterface->GET_DATE_PICKER("EndDate", substr($EndDate,0,10))."
							</td>
						</tr>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["CreatedBy"]."</td>
							<td>
								<input type=\"text\" class=\"tabletext\" name=\"createdBy\" ID=\"createdBy\" value=\"$createdBy\">
							</td>
						</tr>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["Tag"]."</td>
							<td>
								$TagSelection".$ldaUI->GET_BTN($Lang['Btn']['SelectAll'],"button","checkOptionAll(document.form1.elements['TagID[]'])")."<br><span class=\"tabletextremark\">".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."</span>
							</td>
						</tr>
					</table>
				</div>
				<div class=\"edit_bottom\">
						".$linterface->GET_ACTION_BTN($button_search,"button","submitSearch(1)")."
						".$linterface->GET_ACTION_BTN($button_cancel,"button","goCancel()")."
				</div>
			</div>
		</div>
	</div>
	
	";

	
# action button
$actionBtn = '
<td valign="bottom"><div class="common_table_tool">';
//$actionBtn .= '<a href="javascript:unlike_record(document.form1, \'RecordID[]\')" class="tool_delete">'.$Lang["DigitalArchive"]["Unlike"].'</a>';
if($flag!=FUNCTION_TAG) {
	$actionBtn .= '<a href="#TB_inline?height=600&width=750&inlineId=FakeLayer" onClick="Display_Edit_File();return false;" class="tool_edit setting_row thickbox" title="'.$button_edit.'">'.$button_edit.'</a>';
	$actionBtn .= '<a href="javascript:removeAction(document.form1, \'RecordID[]\')" class="tool_delete">'.$button_delete.'</a>';
} else {
	$actionBtn .= '<a href="#TB_inline?height=600&width=750&inlineId=FakeLayer" onClick="Display_Edit_Tag();return false;" class="tool_edit setting_row thickbox" title="'.$button_edit.'">'.$button_edit.'</a>';
	$actionBtn .= '<a href="javascript:remove_tag(document.form1, \'TagID[]\')" class="tool_delete">'.$button_delete.'</a>';	
}


$actionBtn .= '</div></td>';

$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">


<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";


function goCancel() {
	document.form1.reset();
	document.form2.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	
	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			//Get_Resource_MyFavourite_Table();
		}
	)
	
}

function showHideLayer(flag, val) {
	var obj = document.form1;
	if(val==1) {
		$('#'+flag).val(val);
		$('#resource_search').hide();	
		$('#resource_search_adv').show();
	} else {
		$('#'+flag).val(val);
		$('#resource_search_adv').hide();
		$('#resource_search').show();	
	}
	obj.reset();
}

function goBack() {
	self.location.href = "index.php";
}


function Display_Edit_File() {

	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	if(DocumentID.length==1) {
		$.post(
			"ajax_edit_file.php", 
			{ 
				DocumentID: DocumentID,
				flag: '<?=$flag?>',
				SubjectID: '<?=$SubjectID?>'
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				//Get_Directory_List_Table();	
			}
		);	
	} else {
		alert(globalAlertMsg1);	
	}		
}

function Display_Edit_Tag() {

	var TagID = Get_Check_Box_Value('TagID[]','Array');

	if(TagID.length==1) {
		$.post(
			"ajax_edit_tag.php", 
			{ 
				TagID: TagID,
				flag: '<?=$flag?>'
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				//Get_Directory_List_Table();	
			}
		);	
	} else {
		alert(globalAlertMsg1);	
	}		
}

function removeAction() {
	
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();
	
	if(DocumentID.length>0) {
		
		$('#deleteWholeThread').val(1);
		
		var PostVar = Get_Form_Values(document.getElementById("form1"));
		
		if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
			$.post(
				"ajax_delete_record.php", PostVar,
				function(ReturnData)
				{
					
					//$('#tempdiv').html(ReturnData);
					$('#msg').val(ReturnData);
					Get_Return_Message(ReturnData);
					//Get_Directory_List_Table();
				}
			);
		}		
	} else {
		alert(globalAlertMsg2);
	}	
}

function Get_Directory_List_Table() {
	document.form1.submit();
}	

function submitSearch(advSearch) {
	$('#advanceSearchFlag').val(advSearch);
	document.form1.action = "displayResult.php";
	document.form1.submit();
		
}

function remove_tag(obj, emt) {
	if(countChecked(obj, emt)==0) {
		alert(globalAlertMsg2);
	} else {
		if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
			obj.action = "remove_tag_update.php";
			obj.method = "POST";
			obj.submit();
		}
	}
}

function getCheckDateRange(flag) {
	if(flag) {
		$('#StartDate').attr("disabled", false);
		$('#EndDate').attr("disabled", false);
	} else {
		$('#StartDate').attr("disabled", true);
		$('#EndDate').attr("disabled", true);		
	}
}

<?
if($msg!="")
	echo "Get_Return_Message(\"$msg\")";
?>
</script>

<div id="content" style="padding-left: 10px; width=100%;">

<br style="clear:both">
<div id="content" style="padding-left: 10px; width=100%;">
	<div class="navigation_v30 navigation_2">
		<a href="javascript:goBack();"><?=$i_frontpage_menu_home?></a>
		<? if(sizeof($groupInfo)>0) {?>
		<a href="list.php?GroupID=<?=$GroupID?>&FolderID=<?=$FolderID?>"><?=intranet_htmlspecialchars($groupInfo['GroupTitle'])?></a>
		<? } ?>
		<span><?=$navTitle?></span>
	</div>
	<br style="clear:both">
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<form name="form2" id="form2" action="displayResult.php" method="GET">
				<? if($flag!=FUNCTION_TAG) echo $searchOption; ?>
<input type='hidden' name='msg' id='msg' value='' />
<input type='hidden' name='flag' id='flag' value='<?=$flag?>' />
<input type='hidden' name='GroupID' id='GroupID' value='<?=$GroupID?>' />
<input type='hidden' name='FolderID' id='FolderID' value='<?=$FolderID?>' />
<input type='hidden' name='deleteWholeThread' id='deleteWholeThread' value='' />
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="0">
				</form>
				
			</td>
		</tr>
	
		<tr>
			<td>
				<form name="form1" id="form1">
				
				<? if($flag!=FUNCTION_TAG) echo $searchOptionAdv; ?>
				<div class="reading_board" id="resource_popular_list">
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span><?=$Lang["DigitalArchive"]["List"]?></span></h1>
					</div></div>
					
					<div class="reading_board_left"><div class="reading_board_right">
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<?=$actionBtn?>
							</tr>
						</table> 
						
						<div id="tableContentDiv">
						<?=$table?>
						</div>

					
					</div></div>
					
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				</div>
<input type='hidden' name='msg' id='msg' value='' />
<input type='hidden' name='flag' id='flag' value='<?=$flag?>' />
<input type='hidden' name='GroupID' id='GroupID' value='<?=$GroupID?>' />
<input type='hidden' name='FolderID' id='FolderID' value='<?=$FolderID?>' />
<input type='hidden' name='deleteWholeThread' id='deleteWholeThread' value='' />
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="1">

</fo				</form>
			</td>
		</tr>
	</table>
</div>


</div>

<script language="javascript">
	<?=$jsFunction?>
	showHideLayer('advanceSearchFlag', '<?=$advanceSearchFlag?>');
	getCheckDateRange('<?=$byDateRange?>');
</script>

<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>