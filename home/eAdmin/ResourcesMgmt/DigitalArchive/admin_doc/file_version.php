<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$Thread'";
$result = $lda->returnVector($sql);
$GroupID = $result[0];

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}


$titleOfCurrentThread = $lda->Get_Title_Of_Current_Thread($Thread);

$MODULE_OBJ['title'] = $titleOfCurrentThread;


$linterface->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

$table = $ldaUI->Get_File_Version_List($Thread);

?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript">
function removeAction(DocumentID) {

	var GroupID = $('#GroupID').val();

	if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
		$.post(
			"ajax_delete_record.php",
			{
				GroupID: GroupID,
				DocumentID: DocumentID
			} 
			,
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				Get_Version_Table();
				//window.location.reload();
				//$('#tempdiv').html(ReturnData);
			}
		);
	}		
}

function Get_Version_Table() {
	Block_Element("tableDiv");
		$.post(
			"ajax_get_version_table.php",
			{
				Thread: "<?=$Thread?>"
			} 
			,
			function(ReturnData)
			{
				$('#tableDiv').html(ReturnData);
				//Get_Return_Message(ReturnData);
				//window.location.reload();
				UnBlock_Element("tableDiv");
			}
		);
	
}

var js_LAYOUT_SKIN = "<?php echo $LAYOUT_SKIN;?>";
var js_PATH_WRT_ROOT = "";
var loading = "<img src='"+js_PATH_WRT_ROOT + "/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";

function js_Show_Version_Description(obj, Thread, VersionNo) {
	$(obj).after('<div class="like_list_layer" onClick="js_Hide_ClassLevel()">'+loading+'</div>');
	
	var pos = $(obj).position()
	$(".like_list_layer").css(
		{
			'position':'absolute',
			'left': pos.left+15,
			'top': pos.top+20
		}
	)	
	
	$.post(
		"ajax_get_version_description.php",
		{
			Thread: Thread,
			VersionNo: VersionNo
		},
		function(ReturnData){
			$(".like_list_layer").html(ReturnData);
		}
	)
}

function js_Hide_ClassLevel() {
	$(".like_list_layer").remove();
}
</script>

<div id="tableDiv"><?=$table?></div>
<br style="clear:both" />
<div class="edit_bottom">
<?=$ldaUI->GET_ACTION_BTN($button_close, "button", "self.close()");?>
</div>
<div id=tempdiv></div>
<?

$linterface->LAYOUT_STOP("");

intranet_closedb();

?>