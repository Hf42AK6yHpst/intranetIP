<?php
// editing by 
/*
 * 2016-01-20 (Carlos): Added [Allow IP] field.
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$data = $lda->Get_Admin_Doc_Info($DocumentID);


$dataAry['Title'] = $Title;  
$dataAry['Description'] = $Description; 
if($data['IsFolder']) {
	$dataAry['FileFolderName'] = $Title;
	$dataAry['AllowIP'] = trim($AllowIP);	
} else {
	$dataAry['FileFolderName'] = $FileName;
	$pos = strripos($FileName, ".");
	$dataAry['FileExtension'] = substr($FileName, ($pos+1));	
}


$lda->Start_Trans();

$result = $lda->Update_Admin_Doc_Data($dataAry, $TagName, $DocumentID);

if ($result) {
	$lda->Commit_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else {
	$lda->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];	
}

intranet_closedb();
?>