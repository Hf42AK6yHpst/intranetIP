<?php
/// Editing by: 
/*
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 * 2016-01-21 (Carlos): [ip2.5.7.3.1] - added checking of IP addresses restriction. 
 * 2015-02-03 (Carlos): [ip2.5.6.3.1] - enlarge memory limit and set no time limit, changed to read file by chunks to avoid fail to read large file.
 * 2014-10-03 (Carlos): [ip2.5.5.10.1] - added libdigitalarchive->Authenticate_DigitalArchive()
 * 2014-09-15 (Carlos): Fix missing file name in Chrome browser agent case
 */
set_time_limit(0);
ini_set("memory_limit", "1024M");
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lfs = new libfilesystem();
$linterface = new interface_html();
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

$lda->Authenticate_DigitalArchive();

if(sizeof($DocumentID)==0 && $DocID!="") {
	$DocumentID = explode(',', $DocID);
}
if($fileHashName!="") {
	$sql = "SELECT DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE FileHashName = '$fileHashName'";
	
	$DocumentID = $ldb->returnVector($sql);
} 

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';
$FolderPath = $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/admin_doc/";

$DocumentSize = sizeof($DocumentID);


# check access right
if($DocumentSize>0) {
	$sql = "SELECT GroupID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID IN (".implode(',',$DocumentID).")";
	$result = $lda->returnVector($sql);
	$thisGroupID = $result[0];
}

if((!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$thisGroupID)) || $DocumentSize==0) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

// Check whether any one of the documents are restricted to be downloaded in certain IP addresses 
$isAllDocumentsAllowToDownload = $lda->isDocumentsAllowToDownload($thisGroupID, $DocumentID, getRemoteIpAddress());
if(!$isAllDocumentsAllowToDownload)
{
	include_once ($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
	intranet_closedb();
	header("Content-Type:text/html;charset=".returnCharset());
	echo $Lang['DigitalArchive']['ForbiddenToDownload']."\n";
	if($_SERVER['HTTP_REFERER'] != '')
	{
		echo '<br />'."\n";
		echo str_replace('<!--LINK-->',$_SERVER['HTTP_REFERER'],$Lang['DigitalArchive']['RedirectBackInstruction'])."\n";
		echo '<script type="text/javascript" language="javascript">'."\n";
		echo 'window.setTimeout(function(){window.location.href="'.$_SERVER['HTTP_REFERER'].'";},5000);'."\n";
		echo '</script>'."\n";
	}
	exit();
}

if($DocumentSize==1) {
	$data = $lda->Get_Admin_Doc_Info($DocumentID[0]);
	$isFolder = $data['IsFolder'];
}

if($DocumentSize==1)	{
	
	$docID = $DocumentID[0];
	
	if($isFolder) {
		$uFolder = "u".$UserID;
		$tempFolder = $FolderPath.$uFolder;
	
		if (!file_exists($tempFolder)) {
			$lfs->folder_new($tempFolder);
		}
		
		$ZipFileName = $uFolder.".zip";
		$ZipFilePath = $FolderPath.$ZipFileName;
		
		
		$currentDir = getcwd();
		$lda->buildFileHierachy($GroupID, $FolderID, $tempFolder, $DocumentID);
		chdir($currentDir);
		
		$lfs->chmod_R($tempFolder, 0777);
		
		$lfs->file_remove($ZipFilePath);
		$lfs->file_zip($uFolder, $ZipFileName, $FolderPath);
		
		# remove the copy location
		$lfs->folder_remove_recursive($uFolder);
		
		if(file_exists($ZipFileName)){
			//output2browser(get_file_content($ZipFileName), $ZipFileName);
			$file_content = '';
			$fh = fopen($ZipFileName,"rb");
			if($fh)
			{
				session_write_close(); // end the script session to allow other pages to access the session data and avoid blocking
				while(!feof($fh))
				{
					$buffer = fread($fh,8192);
					$file_content .= $buffer;
				}
				fclose($fh);
			}
			output2browser($file_content, $ZipFileName);
		}
		
	} else {
		
		$sql = "SELECT Title, FileFolderName, CONCAT('$FolderPath', FileHashName), IsFolder, FileExtension FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID='$docID'";
		
		list($title, $raw_file_name, $file_abspath, $isFolder, $extension) = current($ldb->returnArray($sql));
		
//		debug_pr($title);
//		$title = iconv("utf-8", "big5", $title);
//		$raw_file_name = iconv("utf-8", "big5", $raw_file_name);
		
		if(file_exists($file_abspath)) {
			
			$lda->UpdateViewHistoryLog($docID, $UserID);
			
			// Check browser agent
			$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
			if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
			elseif (preg_match('/msie/', $userAgent) || preg_match('/trident/', $userAgent) || preg_match('/edge/', $userAgent)) { $agentName = 'msie'; } 
			elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
			elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
			else { $agentName = 'unrecognized'; }
			
			// Output file to browser
			$mime_type = mime_content_type($file_abspath);
			//$file_content = file_get_contents($file_abspath);
			$file_size = filesize($file_abspath);
			
			if(strtolower(substr($title,"-".strlen($extension)))==strtolower($extension)) {
				$revisedTitle = $title;
			} else {
				$revisedTitle = $title.".".$extension;
			}
			
			switch ($agentName) {
				case "msie":
					$file_name = urlencode($revisedTitle);
					$file_name = str_replace('+', ' ', $file_name);
					//$file_name = $revisedTitle;
					break;
				case "mozilla":
					//$file_name = $raw_file_name;
					$file_name = $revisedTitle;
					break;
				default:
					$file_name = $revisedTitle;
			}
			
			# header to output
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-type: $mime_type");
			header("Content-Length: ".$file_size);
			header("Content-Disposition: attachment; filename=\"".$file_name."\"");
			
			//echo $file_content;
			$fh = fopen($file_abspath,"rb");
			if($fh)
			{
				session_write_close(); // end the script session to allow other pages to access the session data and avoid blocking
				while(!feof($fh))
				{
					$buffer = fread($fh,8192);
					echo $buffer;
				}
				fclose($fh);
			}
			//output2browser(get_file_content($FolderPath), $file_name);
			
		} else {
			header("Location: index.php?msg=FileNotExist");	
		}
	}
	
} else if($DocumentSize>1) {
	
	$uFolder = "u".$UserID;
	$tempFolder = $FolderPath.$uFolder;
	
	if (!file_exists($tempFolder)) {
		$lfs->folder_new($tempFolder);
	}
		
	$ZipFileName = $uFolder.".zip";
	$ZipFilePath = $FolderPath.$ZipFileName;

	
	$sql = "SELECT CONCAT('$FolderPath', FileHashName), FileFolderName, IsFolder, DocumentID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID IN (".implode(',', $DocumentID).")";
	$result = $ldb->returnArray($sql);
	
	for($i=0; $i<sizeof($result); $i++) { 
		list($FileFullPath, $FileName, $IsFolder, $docID) = $result[$i];
		$lda->UpdateViewHistoryLog($docID, $UserID);
		
		$currentDir = getcwd();
		$r = $lda->buildFileHierachy($GroupID, $FolderID, $tempFolder, $DocumentID);
		//echo $ldaUI->Get_Folder_Navigation($docID, "/").'<br>';
		chdir($currentDir);
	}
	
	$lfs->chmod_R($tempFolder, 0777);
	
	$lfs->file_remove($ZipFilePath);
	$lfs->file_zip($uFolder, $ZipFileName, $FolderPath);
	
	# remove the copy location
	$lfs->folder_remove_recursive($uFolder);
	
	if(file_exists($ZipFileName)){
		//output2browser(get_file_content($ZipFileName), $ZipFileName);
		
		$file_content = '';
		$fh = fopen($ZipFileName,"rb");
		if($fh)
		{
			session_write_close(); // end the script session to allow other pages to access the session data and avoid blocking
			while(!feof($fh))
			{
				$buffer = fread($fh,8192);
				$file_content .= $buffer;
			}
			fclose($fh);
		}
		output2browser($file_content, $ZipFileName);
	}
		
}

intranet_closedb();
?>