<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$GroupTitle = trim(urldecode(stripslashes($_REQUEST['GroupTitle'])));
$GroupID = $_REQUEST['GroupID'];

$lda = new libdigitalarchive();

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}



if ($lda->Rename_Group($GroupTitle,$GroupID)) {
	echo $Lang['StaffAttendance']['GroupRenameSuccess'];
}
else {
	echo $Lang['StaffAttendance']['GroupRenameFail'];
}

intranet_closedb();
?>