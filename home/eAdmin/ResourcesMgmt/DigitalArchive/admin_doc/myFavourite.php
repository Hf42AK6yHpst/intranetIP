<?php
# modifying : 
/*
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: added libdigitalarchive->Authenticate_DigitalArchive()
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!isset($GroupID) || $GroupID=="") {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$subject = new subject();

$lda->Authenticate_DigitalArchive();

if(!isset($FolderID) || $FolderID=="") {
	$FolderID = 0;
}

$subTagDisplay = $ldaUI->PageSubTagObj($GroupID, 2);

$groupInfo = $lda->getGroupInfo($GroupID);

$allSubjects = $subject->Get_All_Subjects();
$subjectSimpleDisplay = getSelectByArray($allSubjects, 'name="SubjectID" id="SubjectID"', $SubjectID, 0, 0, $Lang['SysMgr']['SubjectClassMapping']['All']['Subject']);
$subjectAdvanceDisplay = getSelectByArray($allSubjects, 'name="SubjectID" id="SubjectID" multiple size="10"', $SubjectID, 0, 1);


$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript">

function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
}

function showSpan(layername) {
	$('#'+layername).show();
}

function Display_New_Folder() {

	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();

	$.post(
		"ajax_create_folder.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);			
}

function Display_Upload_New_File() {

	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();

	$.post(
		"ajax_upload_file.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);			
}

function Get_Directory_List_Table() {
	
	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();

	Block_Element("DirectoryListDiv");
	$.post(
		"ajax_get_directory_list_table.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			$('#DirectoryListDiv').html(ReturnData);
			UnBlock_Element("DirectoryListDiv");		
		}
	);			
}

function checkSpanDisplay(layername) {
	var hiddenField = layername+"_value"; 
	
	if(document.getElementById(hiddenField).value==1) {
		document.getElementById(hiddenField).value = 0;
		hideSpan(layername);
	} else {
		document.getElementById(hiddenField).value = 1;
		showSpan(layername);	
	}
	
}

function Create_Folder_Action() {
	
	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();
	var Title = document.getElementById('Title').value;
	var Description = document.getElementById('Description').value;
	
	$.post(
		"ajax_create_folder_update.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID,
			Title: Title,
			Description: Description
		},
		function(ReturnData)
		{
			window.tb_remove();
			Get_Directory_List_Table();
			Get_Return_Message(ReturnData);
		}
	);			

}



</script>
<div id="tempDiv"></div>

<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="" enctype="multipart/form-data">
<!-- Content [Start] -->

<!-- navigation [Start] -->
<br>
<div class="navigation_v30 navigation_2">
	<a href="index.php"><?=$Lang["DigitalArchive"]["GroupList"]?></a>
	<span><?=intranet_htmlspecialchars($groupInfo['GroupTitle'])?></span>
</div>
<p class="spacer"></p>
<!-- navigation [End] -->

<!-- Search [Start] -->
<div id="resource_search" class="resource_search_simple">
	<a href="javascript:void(0);" class="link_advanced_search"><?=$Lang["DigitalArchive"]["AdvanceSearch"]?></a>
	<div class="resource_search_keywords">
		<?=$Lang["DigitalArchive"]["SearchFor"]?> : <input name="" type="text"  class="keywords" style=""/> 
		<?=$subjectSimpleDisplay?>
		<input type="button" value="<?=$Lang["DigitalArchive"]["Search"]?>" class="formsmallbutton" />
	</div>
</div>
<!-- Search [End] -->
                    
<br />
<p class="spacer"></p>


<!-- Sub navigation [Start] -->
<?=$subTagDisplay?>
<!-- Sub navigation [End] -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td valign="bottom"></td>
										<td valign="bottom">
										 <div class="common_table_tool">
											
                                          
											<a href="#" class="tool_delete"><?=$Lang['Btn']['RemoveAll']?></a>
                                           
										</div>
                                     
										</td>
									</tr>
					</table> 




                                       
                               <div class="resource_detail">
                      		<div class="book_detail_cover">
                      		<span class="file_pdf">&nbsp;</span>
                        </div>
        					<div class="book_detail">
                            	<div class="book_detail_info">
                                
                                <h1>感人分享</h1>
                                
                                
                               
                                 <p class="spacer"></p>
                                </div>
                                 <h2 class="book_level"><em><?=$Lang["DigitalArchive"]["Level"]?>:</em><span>P6</span></h2>
                                <h2 class="book_cat"><em><?=$Lang["DigitalArchive"]["Subject"]?>:</em><span>Chinese Language</span></h2>
                                 <p class="spacer"></p>
                              <h2 class="book_desc"><em><?=$Lang["DigitalArchive"]["Description"]?>:</em><span>This is useful</span></h2>
                                <p class="spacer"></p>
                                 	<div class="like_comment">
                                                                	<a href="#"><?=$Lang["DigitalArchive"]["Unlike"]?></a>
                                                                    <p class="spacer"></p>
                                                                	<span class="like_num"><?=$Lang["DigitalArchive"]["You"].$Lang["DigitalArchive"]["AndSign"]?> <a href="#">30</a> <?=$Lang["DigitalArchive"]["ChooseLike"]?></span><p class="spacer"></p>
                           	  </div>
                             <p class="spacer"></p> <div>
                             <div class="tag_list"><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a></div>
                            </div>
        					</div>
        					<p class="spacer"></p>
                       
                      </div>
                      <p class="spacer"></p>
                                            			<br />
                                                              <div class="resource_detail">
                      		<div class="book_detail_cover">
                      		<span class="file_doc">&nbsp;</span>
                        </div>
        					<div class="book_detail">
                            	<div class="book_detail_info">
                                
                                <h1>Personal file</h1>
                                
                                
                               
                                 <p class="spacer"></p>
                                </div>
                                 <h2 class="book_level"><em><?=$Lang["DigitalArchive"]["Level"]?>:</em><span>P6</span></h2>
                                <h2 class="book_cat"><em><?=$Lang["DigitalArchive"]["Subject"]?>:</em><span>Chinese Language</span></h2>
                                 <p class="spacer"></p>
                              <h2 class="book_desc"><em><?=$Lang["DigitalArchive"]["Description"]?>:</em><span>This is useful</span></h2>
                                <p class="spacer"></p>
                                 	<div class="like_comment">
                                                                	<a href="#"><?=$Lang["DigitalArchive"]["Unlike"]?></a>
                                                                    <p class="spacer"></p>
                                                                	<span class="like_num"><?=$Lang["DigitalArchive"]["You"].$Lang["DigitalArchive"]["AndSign"]?> <a href="#">30</a> <?=$Lang["DigitalArchive"]["ChooseLike"]?></span><p class="spacer"></p>
                           	  </div>
                             <p class="spacer"></p> <div>
                             <div class="tag_list"><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a></div>
                            </div>
        					</div>
        					<p class="spacer"></p>
                       
                      </div>
                      <p class="spacer"></p>
                                            			<br />
                                                                <div class="resource_detail">
                      		<div class="book_detail_cover">
                      		<span class="file_ppt">&nbsp;</span>
                        </div>
        					<div class="book_detail">
                            	<div class="book_detail_info">
                                
                                <h1>班級統計表</h1>
                                
                                
                               
                                 <p class="spacer"></p>
                                </div>
                                 <h2 class="book_level"><em><?=$Lang["DigitalArchive"]["Level"]?>:</em><span>P6</span></h2>
                                <h2 class="book_cat"><em><?=$Lang["DigitalArchive"]["Subject"]?>:</em><span>Chinese Language</span></h2>
                                 <p class="spacer"></p>
                              <h2 class="book_desc"><em><?=$Lang["DigitalArchive"]["Description"]?>:</em><span>This is useful</span></h2>
                                <p class="spacer"></p>
                                 	<div class="like_comment">
                                                                	<a href="#"><?=$Lang["DigitalArchive"]["Unlike"]?></a>
                                                                    <p class="spacer"></p>
                                                                	<span class="like_num"><?=$Lang["DigitalArchive"]["You"].$Lang["DigitalArchive"]["AndSign"]?> <a href="#">30</a> <?=$Lang["DigitalArchive"]["ChooseLike"]?></span><p class="spacer"></p>
                           	  </div>
                             <p class="spacer"></p> <div>
                             <div class="tag_list"><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a></div>
                            </div>
        					</div>
        					<p class="spacer"></p>
                       
                      </div>
                      <p class="spacer"></p>
                                            			<br />
                        <div class="resource_detail">
                      		<div class="book_detail_cover">
                      		<span class="file_image">&nbsp;</span>
                        </div>
        					<div class="book_detail">
                            	<div class="book_detail_info">
                                
                                <h1>統計圖</h1>
                                
                                
                               
                                 <p class="spacer"></p>
                                </div>
                                 <h2 class="book_level"><em><?=$Lang["DigitalArchive"]["Level"]?>:</em><span>P6</span></h2>
                                <h2 class="book_cat"><em><?=$Lang["DigitalArchive"]["Subject"]?>:</em><span>Chinese Language</span></h2>
                                 <p class="spacer"></p>
                              <h2 class="book_desc"><em><?=$Lang["DigitalArchive"]["Description"]?>:</em><span>This is useful</span></h2>
                                <p class="spacer"></p>
                                 	<div class="like_comment">
                                                                	<a href="#"><?=$Lang["DigitalArchive"]["Unlike"]?></a>
                                                                    <p class="spacer"></p>
                                                                	<span class="like_num"><?=$Lang["DigitalArchive"]["You"].$Lang["DigitalArchive"]["AndSign"]?> <a href="#">30</a> <?=$Lang["DigitalArchive"]["ChooseLike"]?></span><p class="spacer"></p>
                           	  </div>
                             <p class="spacer"></p> <div>
                             <div class="tag_list"><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a><a href="#">123 </a></div>
                            </div>
        					</div>
        					<p class="spacer"></p>
                       
                      </div>
                      <p class="spacer"></p></td>
 
                      
<!-- Content [End] -->


<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">

</form>
</div>

<script language="javascript">
Get_Directory_List_Table();
</script>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>