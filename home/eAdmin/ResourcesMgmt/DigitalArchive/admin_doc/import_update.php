<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

if(sizeof($CopyGroup) > 0) {
	
	$Result = $lda->ImportAccessGroupFromIntranetGroup($CopyGroup, $CopyGroupMember);
	
	if(in_array(false, $Result)) { # copy action not complete (maybe partially complete
		$msg = "CopyAccessRightGroupUnsuccess";	
	} else {
		$msg = "CopyAccessRightGroupSuccess";					# copy action complete successfully
	}
}

intranet_closedb();

header("Location: access_right.php?msg=$msg");
?>