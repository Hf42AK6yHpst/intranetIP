<?php


# modifying : 

/**********************
 * 
 * Date:	2013-07-16 (Henry Chan)
 * Details: Change the layout of the editing part with edit button
 * 			Check the input of the maximum size should be a positive integer
 * 
 * Date:	2013-07-11 (Henry Chan)
 * Details: The development of the maximum file size page is almost finished
 * 
 * Date:	2013-07-04 (Henry Chan)
 * Details: File Created and set the user interface
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$lgs = new libgeneralsettings();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_UploadPolicy";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalArchive']['MaxFileSize'],"maximum_file_size.php",1);
$TAGS_OBJ[] = array($Lang['DigitalArchive']['FileFormatSettings'],"file_format_settings.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['DigitalArchive']['UploadNotice'],"upload_notice.php",0);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

# Display Tools Bar Manu
//$toolbar .= $linterface->GET_LNK_NEW("javascript:editSize()",$button_new,"","","",0);
?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">

<!--<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">-->
<script language="javascript">
<?


if ($msg != "")
	echo "Get_Return_Message(\"" . $Lang['General']['ReturnMessage'][$msg] . "\")";
?>

//-- New functions [Start] --
function goSubmit(urlLink) {
	var obj = document.form1;
	var intRegex = /^\d+$/;
	if (obj.max_file_size.value==""){
		alert ("<?=$Lang['General']['JS_warning']['CannotBeBlank']?>");
		obj.max_file_size.select();
		return false;
	}
	else if (obj.max_file_size.value=="" || !IsNumeric(obj.max_file_size.value) || !intRegex.test(obj.max_file_size.value) || parseInt(obj.max_file_size.value) < 0){
		alert ("<?=$Lang['General']['JS_warning']['MustBePositiveNumber']?>");
		obj.max_file_size.select();
		return false;
	}

	obj.action = urlLink;
	obj.submit();
}

function editSize() {
	document.form1.action = "file_format_settings_edit.php";
	document.form1.submit();
}

//Disable form submission when enter key is pressed
function disableEnterKey(e)
{
     var key;      
     if(window.event)
   	      //For IE
          key = window.event.keyCode;
     else
     	  //For firefox
          key = e.which;

     return (key != 13);
}

function js_Display_View()
{
	$(".DisplayView").show();
	$(".EditView").hide();
}

function js_Edit_View()
{
	$(".DisplayView").hide();
	$(".EditView").show();
}

//-- New functions [End] --

</script>

<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="">
<!-- Content [Start] -->

<!-- navigation [Start] -->
      
<!-- File Size Edit Form [Start] -->

<div class="table_board">
<?
//$x .= '<form id="form1" name="form1" action="maximum_file_size_update.php" method="POST" onsubmit="return js_Check_Form();">'."\n";
	$x .= '<div style="text-align:right; height:20px;">'.$linterface->Get_Small_Btn($Lang['Btn']['Edit'], "button", "js_Edit_View()", "", "", "DisplayView").'</div>'."\n";
	$x .= '<table class="form_table_v30">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$Lang['DigitalArchive']['MaxFileSize'].'</td>'."\n";
			$x .= '<td>'."\n";
				$x .= '<div class="EditView" style="display:none">'."\n";
					$x .= '<input name="max_file_size" id="max_file_size" type="text" class="" style="" value="';

$settingNameValue = $lgs->Get_General_Setting("DigitalArchive", array (
	"'MaximumFileSize'"
));
$x .= $settingNameValue['MaximumFileSize'];

$x .= '" onKeyPress="return disableEnterKey(event)"/></label> MB<br/><div class="tabletextremark">'.$Lang['DigitalArchive']['MaximumFileSizeRemark'];
				$x .= '</div></div>'."\n";
				$x .= '<div class="DisplayView">'."\n";
				if($settingNameValue['MaximumFileSize'] != '0')
					$x .= $settingNameValue['MaximumFileSize'].' MB';
				else
					$x .= $Lang['eEnrolment']['NoLimit'];
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";	
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
	
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= '<div class="EditView" style="display:none">'."\n";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","goSubmit('maximum_file_size_update.php')")."\n";
			$x .= $linterface->Get_Action_Btn($Lang['Btn']['Cancel'],"button","js_Display_View()")."\n";
		$x .= '</div>'."\n";
	$x .= '</div>'."\n";
//$x .= '</form>';

echo $x;
?>
</div>	

<!-- File Size Edit Form [End] -->

<!-- Content [End] -->

</form>
<br>
</div>



<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>