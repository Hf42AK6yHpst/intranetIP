<?php

# modifying : 

/**********************
 * Date:	2013-07-10 (Henry Chan)
 * Details: File Created
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

intranet_auth();
intranet_opendb();

$lgs = new libgeneralsettings();

if ($_REQUEST['max_file_size'] != '') {
	//if ((int)$_REQUEST['max_file_size'] == '0')
		$tempInput = (int)$_REQUEST['max_file_size'];
	//else
	//	$tempInput = ltrim($_REQUEST['max_file_size'], '0');
	if ($lgs->Save_General_Setting("DigitalArchive", array (
			"MaximumFileSize" => $tempInput
		))) {
		$msg = "UpdateSuccess";
	} else {
		$msg = "UpdateUnsuccess";
	}
}

intranet_closedb();

header("Location: maximum_file_size.php?msg=$msg");
?>