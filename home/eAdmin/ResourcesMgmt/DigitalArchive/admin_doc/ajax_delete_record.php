<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$DocumentIDAry = array();
/*
if(!is_array($DocumentID)) {
	$DocumentIDAry[] = $DocumentID;
} else {
	$DocumentIDAry = $DocumentID;	
}
*/
if($DocIDList=="") {
	$DocIDList = is_array($DocumentID) ? implode(',',$DocumentID) : $DocumentID;	
}

$DocumentIDAry = explode(',',$DocIDList);

$lda ->Start_Trans();
$result = $lda->deleteAdminRecord($GroupID,$DocumentIDAry, $skipRearrangeLatest=0, $deleteWholeThread);

if (in_array(false, $result)) {
	
	$lda->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	
}
else {
	
	$lda->Commit_Trans();
	echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
	
}

intranet_closedb();
?>