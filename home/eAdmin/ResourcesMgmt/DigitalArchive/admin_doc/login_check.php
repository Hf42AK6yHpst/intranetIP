<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$password = stripslashes(trim($_REQUEST['password']));

if($_SESSION['eclass_session_password'] == $password){
	$_SESSION['DigitalArchive_session_password'] = $password;
	intranet_closedb();
	header("Location: index.php");
	exit;
}else{
	intranet_closedb();
	header("Location: login.php?msg=AccessDenied");
	exit;
}

intranet_closedb();
?>