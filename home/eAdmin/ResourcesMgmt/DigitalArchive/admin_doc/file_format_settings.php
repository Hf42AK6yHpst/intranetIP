<?php


# modifying : 

/**********************
 * 
 * Date:	2013-07-16 (Henry Chan)
 * Details:	Added the checking of the input file extension
 * 
 * Date:	2013-07-11 (Henry Chan)
 * Details: The development of the file format settings page is almost finished
 * 
 * Date:	2013-07-04 (Henry Chan)
 * Details: File Created and set the user interface
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array (
	"ck_page_size",
	"numPerPage"
);
$arrCookies[] = array (
	"ck_page_no",
	"pageNo"
);
$arrCookies[] = array (
	"ck_page_order",
	"order"
);
$arrCookies[] = array (
	"ck_page_field",
	"field"
);

$arrCookies[] = array (
	"ck_parent_targetClass",
	"targetClass"
);
$arrCookies[] = array (
	"ck_parent_recordstatus",
	"recordstatus"
);
$arrCookies[] = array (
	"ck_parent_keyword",
	"keyword"
);
if (isset ($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
} else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_UploadPolicy";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalArchive']['MaxFileSize'],"maximum_file_size.php",0);
$TAGS_OBJ[] = array($Lang['DigitalArchive']['FileFormatSettings'],"file_format_settings.php?clearCoo=1",1);
$TAGS_OBJ[] = array($Lang['DigitalArchive']['UploadNotice'],"upload_notice.php",0);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

# Display Tools Bar Manu
$toolbar .= $linterface->GET_LNK_NEW("javascript:editFormat()", $button_new, "", "", "", 0);

//-- Table Management [Start]
//if ($page_size_change == 1)
//{
//    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
//    $ck_page_size = $numPerPage;
//}
if ($order == "")
	$order = 1;
if ($field == "")
	$field = 0;
$pageSizeChangeEnabled = true;

if (isset ($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("b.");
//start setting the table
if ($keyword != "") {
	//Something should be changed
	$conds = " (a.FileFormat like '%$keyword%' OR
					$name_field like '%$keyword%' OR
					a.DateInput like '%$keyword%')";
} else
	$conds = "TRUE";
//$conds .= " AND yc.AcademicYearID=".get_Current_Academic_Year_ID();	

//if ($_REQUEST['record_id']) {
//	foreach ($_REQUEST['record_id'] as $rid) {
//		$lda->Delete_File_Format_Setting($rid);
//	}
//}

$sql = "SELECT
			a.FileFormat,
			$name_field as InputBy,
			a.DateInput,
			CONCAT('<input type=\'checkbox\' name=\'record_id[]\' id=\'record_id[]\' value=', RecordID ,'>') as checkbox 
		FROM 
			DIGITAL_ARCHIVE_FILE_FORMAT as a LEFT JOIN INTRANET_USER as b ON a.InputBy=b.UserID
		WHERE
			$conds ";

$li->sql = $sql;
$li->field_array = array (
	"FileFormat",
	"InputBy",
	"DateInput"
);
$li->no_col = sizeof($li->field_array) + 2;

$li->IsColOff = "Digital_Archive_File_Format_Table";

$pos = 0;

$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='32%' >" . $li->column($pos++, $Lang['DigitalArchive']['FileFormat']) . "</th>\n";
$li->column_list .= "<th width='32%' >" . $li->column($pos++, $Lang['General']['InputBy']) . "</th>\n";
$li->column_list .= "<th width='32%' >" . $li->column($pos++, $Lang['General']['DateInput']) . "</th>\n";
$li->column_list .= "<th width='1'>" . $li->check("record_id[]") . "</th>\n";
// --Table Management [End]
?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">


<script language="javascript">
<?


if ($msg != "")
	echo "Get_Return_Message(\"" . $Lang['General']['ReturnMessage'][$msg] . "\")";
?>

//-- New functions [Start] --
function goSubmit(urlLink) {
	var obj = document.form1;
	if (obj.max_file_size.value==""){
		alert ("<?=$Lang['General']['JS_warning']['CannotBeBlank']?>");
		obj.max_file_size.select();
		return false;
	}
	else if (obj.max_file_size.value=="" || !IsNumeric(obj.max_file_size.value) || parseInt(obj.max_file_size.value) <= 0){
		alert ("<?=$Lang['General']['JS_warning']['MustBePositiveNumber']?>");
		obj.max_file_size.select();
		return false;
	}

	obj.action = urlLink;
	obj.submit();
}

function editFormat() {
	document.form1.action = "file_format_settings_edit.php";
	document.form1.submit();
}
//-- New functions [End] --

</script>

<div id="content" style="padding-left: 10px; width=100%;">
	<form name="form1" method="post" action="">
	
		<div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$toolbar?>
			</div>
			<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
			<br style="clear:both" />
		</div>
		
		<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
				<div class="common_table_tool">
		        	<a href="javascript:checkRemove(document.form1,'record_id[]','file_format_settings_update.php')" class="tool_delete"><?=$button_delete?></a> 
				</div>
			</td>
		</tr>
		</table>
		
		<?= $li->display()?>
		
		<input type="hidden" name="TabID" id="TabID" value="" />
		<input type="hidden" name="uid" id="uid" value="" />
		<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
		<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
		<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
		<input type="hidden" name="page_size_change" id="page_size_change" value="" />
		<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
		<input type="hidden" name="comeFrom" value="/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/file_format_settings.php">
		<input type="hidden" name="clearCoo" id="clearCoo" value="" />
				
	</form>
</div>

<?

$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>