<?

/*********************************************************
 * Date: 20130214 (Rita)
 * Details: add $CurrentPageArr['DigitalArchiveAdminMenu'] for menu display
 *********************************************************/

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$ldaUI = new libdigitalarchive_ui();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

$AcademicYearID = Get_Current_Academic_Year_ID();

$groupTable = $ldaUI->Get_Intranet_Group_Table($AcademicYearID);
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">

<script language="javascript">

function goBack() {
	self.location.href = "access_right.php";	
}

function CheckAll(CheckBoxType)
{
	var checked = $("input#CheckAll"+CheckBoxType).attr("checked");
	$("input."+CheckBoxType).attr("checked",checked);
}

function UpdateMemberCheckBox(GroupID)
{
	if(GroupID)
	{
		if($("input#Group"+GroupID).attr("checked"))
			$("input#Member"+GroupID).attr("disabled","")
		else
			$("input#Member"+GroupID).attr("disabled","disabled")
	}
	else
	{
		if($("input#CheckAllGroup").attr("checked"))
			$("input.Member").attr("disabled","")
		else
			$("input.Member").attr("disabled","disabled")
	}
}

function displayAlert() {
	if(confirm("<?=$Lang['DigitalArchive']['jsCopyFromGroupMsg']?>")) {
		return true;
	} else {
		return false;	
	}	
}
</script>

<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="import_update.php" onSubmit="return displayAlert()">
<br>
<div class="navigation_v30 navigation_2">
	<a href="index.php"><?=$Lang["DigitalArchive"]["GroupList"]?></a>
	<a href="javascript:goBack();"><?=$Lang["DigitalArchive"]["AccessRightSettings"]?></a>
	<span><?=$Lang['DigitalArchive']['InitializeFromGroup']?></span>
</div>
<br style="clear:both" />
<p class="spacer"></p>

<?=$groupTable?>

<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
</div>

<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>