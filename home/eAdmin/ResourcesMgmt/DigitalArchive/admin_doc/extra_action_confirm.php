<?php
# modifying : henry chow

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!isset($GroupID) || $GroupID=="") {
	header("Location: index.php");
	exit;	
}

if(sizeof($DocumentID)==0) {
	$DocumentID = explode(',', $DocID);
}

if($FolderID==$TargetFolderID || sizeof($DocumentID)==0) {
	header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&msg=FileMoveUnsuccess");
	exit;			
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

if($action=="extract") {
	
	for($i=0; $i<sizeof($DocumentID); $i++) {
		$data = $lda->Get_Admin_Doc_Info($DocumentID[$i]);
		
		if($data['FileExtension'] != "zip") {
			header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&msg=FileMoveUnsuccess");
			exit;
		}
	}
}

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$subTagDisplay = $ldaUI->PageSubTagObj($GroupID, 1);

$groupInfo = $lda->getGroupInfo($GroupID);

### content ### 

# action name
if($action=="move") {
	$actionName = $Lang["DigitalArchive"]["MoveFile"];
	$TargetFolderID = $TargetFolderID;
	$actionURL = "ajax_move_document_update.php";
} else if($action=="extract") {
	$actionName = $Lang["DigitalArchive"]["ExtractFile"];
	$TargetFolderID = $FolderID;	
	$actionURL = "unzip_action_update.php";
	//$moreAction = " onClick=\"batchUpdate();\"";
}

# doucment list
if(sizeof($DocumentID)>0) {
	$fileList = "";
	$delim = "- ";
	for($i=0, $i_max=sizeof($DocumentID); $i<$i_max; $i++) {
		$data = $lda->Get_Admin_Doc_Info($DocumentID[$i]);
		$fileList .= $delim.$data['Title']." (".$data['FileFolderName'].")";	
		$delim = "<br>- ";
	}
}


# source folder name
$sourceFolderName = $ldaUI->Get_Folder_Navigation($FolderID, "/");
if($sourceFolderName=="") $sourceFolderName = "/";



# target folder name
if($action=="extract") {
	$array = $lda->Get_Folder_Hierarchy_Array($GroupID);
	$folderHierarchySwap = $ldaUI->Display_Folder_Hierarchy_For_Swap($array, 0, $FolderID, $NoReload=1, $moveTo=0);
	
	$CurrentFolderInfo = $lda->Get_Admin_Doc_Info($TargetFolderID);
	$currentDirectoryName = (sizeof($CurrentFolderInfo)==0) ? "/" : $CurrentFolderInfo['Title'];
		
	$targetFolderName = '<div class="table_filter">
							<div class="selectbox_group selectbox_group_filter" id="CurrentDirectoryName">
								<a href="javascript:;" onClick="checkSpanDisplay(\'status_option\');" alt="/">'.$currentDirectoryName.'</a>
							</div>
							<p class="spacer"></p>
							<div id="status_option" class="selectbox_layer select_folder_tree" '.$moreAction.'>

								'.$folderHierarchySwap.'
							</div>
						</div>';
} else {
	$targetFolderName = $ldaUI->Get_Folder_Navigation($TargetFolderID, "/");
	if($targetFolderName=="") $targetFolderName = "/";
}



# Consequence Action
$consequenceAction = "
	<input type='radio' name='consequence' id='consequence1' value='1' checked><label for='consequence1'>".$Lang['DigitalArchive']['OverwriteFile']."</label>
	<input type='radio' name='consequence' id='consequence2' value='2'><label for='consequence2'>".$Lang['DigitalArchive']['RenameFile']."</label>
	<input type='radio' name='consequence' id='consequence3' value='3'><label for='consequence3'>".$Lang['DigitalArchive']['ExcludeDuplicateRecord']."</label>
";

$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();

?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">

<script language="javascript">

function batchUpdate(obj, displayId)
{
	if ( $.browser.msie ) { 
		var displayName = obj.alt;
	} else {
		var displayName = obj.text;
	}
	//alert(document.getElementById("TargetFolderID").value);
	checkSpanDisplay("status_option");
	document.getElementById("CurrentDirectoryName").innerHTML = "<a onClick=checkSpanDisplay(\"status_option\")>"+displayName+"</a>";
	document.getElementById("TargetFolderID").value = displayId;
	//alert(document.getElementById("TargetFolderID").value);
}

function goBack() {
	self.location.href = "index.php";	
}

function checkForm() {
	/*
	if(document.form1.consequence[2].checked==true) {
		self.location.href = "fileList.php?GroupID=<?=$GroupID?>&FolderID=<?=$FolderID?>";
		return false;	
	}
	*/
}

function goCancel() {
	self.location.href = "fileList.php?GroupID=<?=$GroupID?>&FolderID=<?=$FolderID?>";
}

function checkSpanDisplay(layername) {
	
	var hiddenField = layername+"_value"; 
	
	if(document.getElementById(hiddenField).value==1) {
		
		document.getElementById(hiddenField).value = 0;
		//hideSpan(layername);
		document.getElementById(layername).style.visibility = "hidden";
		
	} else {
		
		document.getElementById(hiddenField).value = 1;
		//showSpan(layername);
		document.getElementById(layername).style.visibility = "visible";
		
	}
	
}

</script>

<div id="content" style="padding-left: 10px; width=100%;">

<!-- Content [Start] -->

<!-- navigation [Start] -->
<br>
<div class="navigation_v30 navigation_2">
	<a href="javascript:goBack();"><?=$Lang["DigitalArchive"]["GroupList"]?></a>
	<a href="fileList.php?GroupID=<?=$GroupID?>&FolderID=<?=$FolderID?>"><?=intranet_htmlspecialchars($groupInfo['GroupTitle'])?></a>
</div>
<p class="spacer"></p>
<!-- navigation [End] -->

                    
<p class="spacer"></p>




<!-- Content [Start] -->
<div class="content_top_tool">
<form name="form1" id="form1" method="GET" action="<?=$actionURL?>" onSubmit="return checkForm()">

<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang["DigitalArchive"]["Action"]?></td>
		<td><?=$actionName?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang["DigitalArchive"]["FileInvolved"]?></td>
		<td><?=$fileList?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang["DigitalArchive"]["SourceFolder"]?></td>
		<td><?=$sourceFolderName?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang["DigitalArchive"]["TargetFolder"]?></td>
		<td><?=$targetFolderName?></td>
	</tr>
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["IfFileExistInTargetFolder"]?></td>
		<td><?=$consequenceAction?></td>
	</tr>
</table>

<div class="edit_bottom">
	<? echo $linterface->GET_ACTION_BTN($button_submit, "Submit")."&nbsp;".$linterface->GET_ACTION_BTN($button_cancel, "button", "goCancel()")?>
</div>

<input type="hidden" name="action" id="action" value="<?=$action?>">
<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="TargetFolderID" id="TargetFolderID" value="<?=$TargetFolderID?>">
<input type="hidden" name="status_option_value" id="status_option_value" value="">
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="0">
<? for ($i=0, $i_max=sizeof($DocumentID); $i<$i_max; $i++) {
	echo "<input type='hidden' name='DocumentID[]' id='DocumentID[]' value='".$DocumentID[$i]."'>\n";
}?>


</form>	
</div>

<!-- Content [End] -->

<!-- Content [End] -->


<?

$ldaUI->LAYOUT_STOP();
intranet_closedb();


?>
