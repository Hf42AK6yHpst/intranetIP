<?

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

if(!isset($GroupID) || $GroupID=="") {
	header("Location: access_right.php");
	exit;	
}

intranet_auth();
intranet_opendb();

$lda = new libdigitalArchive();

$result = $lda->Delete_Access_Right_Group($GroupID);

intranet_closedb();

if($result)
	$msg = "DeleteSuccess";
else 
	$msg = "DeleteUnsuccess";

header("Location: access_right.php?msg=$msg");
?>