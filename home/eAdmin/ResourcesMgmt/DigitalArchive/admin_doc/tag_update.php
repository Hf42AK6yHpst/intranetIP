<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

# Steps #
# 1) check existing TAG in table "TAG", 
#  1a)if no, then add a new Tag in "TAG"
#  1b) if exist, remove original Tag relationship in table "TAG_RELATIONSHIP"
# 2) add entry in "TAG_RELATIONSHIP" (since not in use in this module before)
# 3) update TagID in table "DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE"


$TagIdAry = returnModuleTagIDByTagName($Tag);
if(sizeof($TagIdAry)==0) {	# no exist, then create a new one
	$updatedTagID = insertModuleTag($Tag, $lda->AdminModule);
} else {					
	$updatedTagID = $TagIdAry[0];
	removeTagModuleRelation($TagID, $lda->AdminModule);
	createTagModuleRelation($updatedTagID, $lda->AdminModule);
}

$result = $lda->updateRecordTagRelation($originalTagID=$TagID, $updatedTagID);

intranet_closedb();

//header("Location: displayResult.php?flag=".FUNCTION_TAG);
header("Location: index.php?msg=".$Lang['General']['ReturnMessage']['UpdateSuccess']);

?>