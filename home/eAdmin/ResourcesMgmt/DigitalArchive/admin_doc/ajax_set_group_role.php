<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$GroupID = $_REQUEST['GroupID'];
$StaffID = $_REQUEST['StaffID'];

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}



$lda ->Start_Trans();
if ($lda->Set_Access_Right_Group_Role($GroupID,$StaffID, $MemberType)) {
	$lda->Commit_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else {
	$lda->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

intranet_closedb();
?>