<?php
// Editing by 
/*
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 */
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();

$TargetFolder = $_REQUEST['TargetFolder'];
$FileName = $_REQUEST['FileName'];

if(empty($TargetFolder) || empty($FileName)) {
	echo "0";
	intranet_closedb();
	exit;
}

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';

$targetPath_Level1 = "/digital_archive/";
$targetPath_Level2 = "/temp/";

$plupload_tmp_path = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath_Level1.$targetPath_Level2.$TargetFolder.'/'.$FileName);

$success = $libfilesystem->lfs_remove($plupload_tmp_path);
echo $success? "1":"0";

intranet_closedb();
?>