<?php
# Editings : 
/**************************************************
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: created
 **************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

unset($_SESSION['DigitalArchive_session_password']);

$linterface = new interface_html();
//$cloud = new wordCloud();
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

//if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList())==0) {
	//header("Location: /home/eAdmin/ResourcesMgmt/DigitalArchive/index.php");
//	header("Location: /");
//	exit;
//}
# check Scaner Ready
$scannerReady = "";
if(file_exists($PATH_WRT_ROOT."rifolder/global.php")) {
	include_once($PATH_WRT_ROOT."rifolder/global.php");
	if(sizeof($PrinterIPLincense)>0) {
		$scannerReady = 1;	
	}
} 

if($scannerReady) {
	$DivId = " bg_scanready";
	$DivId2 = "resource_adminsetting";
} else { 
	$DivId = "";
	$DivId2 = "resource_subject";
}

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Overview";

//$TAGS_OBJ = $ldaUI->PageTagObj(2);

# display elements on top right corner
//$right_element = $ldaUI->Get_Right_Element();
//$TAGS_OBJ_RIGHT[] = array($right_element);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

if($msg!="") {
	if($Lang['General']['ReturnMessage'][$msg]=="") {
		$displayMsg = $msg;	
	} else {
		$displayMsg = $Lang['General']['ReturnMessage'][$msg];	
	}
}

$ldaUI->LAYOUT_START($displayMsg);

echo $ldaUI->Include_JS_CSS();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript">
function checkSubmit()
{
	var pw = document.getElementById('password').value.Trim();
	$('#WarningPassword').hide();
	if(pw == ''){
		$('#WarningPassword').show();
		$('#password').focus();
		return false;
	}
	document.form1.submit();
}

$(document).ready(function(){
	$('input#password').focus();
});
</script>
<div class="table_board">
<br />
<form id="form1" name="form1" method="post" action="login_check.php" onsubmit="return false;">
<?=$linterface->Get_Warning_Message_Box($Lang['DigitalArchive']['Authentication'], $Lang['DigitalArchive']['AuthenticationInstruction'], $others_="");?>
<table class="form_table_v30" cellpadding="10" cellspacing="0" border="0" align="center">
	<tbody>
		<tr>
			<td class="field_title"><label for="password"><span class="tabletextrequire">*</span><?=$i_UserPassword?></label></td>
			<td><input type="password" name="password" id="password" class="user_password" /><?=$linterface->Get_Form_Warning_Msg('WarningPassword',$Lang['eClassAPI']['WarningMsg']['InputPassword'],'Warning')?></td>
		</tr>
		<tr>
			<td colspan="2"><?=$linterface->MandatoryField()?></td>
		</tr>
	</tbody>
</table>
<p class="spacer"></p>
<div class="edit_bottom_v30">
	<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","checkSubmit();").'&nbsp;'.$linterface->GET_ACTION_BTN($Lang['Btn']['Reset'],"reset","");?>
</div>
</form>
</div>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>