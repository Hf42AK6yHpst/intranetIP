<?php
// Editing by 
/*
 * 2020-05-01 (Bill): Added file formats checking
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 * 2013-07-16 (Carlos): Created for checking upload policy and overwrite file right
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();
$lgs = new libgeneralsettings();
$lda = new libdigitalarchive();

$intranetdata_foldername = isKIS() ? $intranet_db.'data' : 'intranetdata';
$TargetFolder = $_REQUEST['TargetFolder'];

$result = array();
$returnMsg = '';
if (!empty($TargetFolder))
{
	# Restricted file formats and maximum file size
	$tempFileFormatAry = $lda->Get_File_Format_Setting();
	$maxFileSizeSettings = $lgs->Get_General_Setting("DigitalArchive", array("'MaximumFileSize'"));
	$MaxFileSize = $maxFileSizeSettings['MaximumFileSize']; // MB
	$RestrictFileSize = is_numeric($MaxFileSize) && $MaxFileSize > 0;
	$MaxFileSizeInBytes = floatval($MaxFileSize) * 1024 * 1024;

    $FileFormatAry = array();
	for($i=0; $i<count($tempFileFormatAry); $i++) {
		$file_format = strtolower(trim($tempFileFormatAry[$i]['FileFormat']));
		if($file_format != '') {
			$FileFormatAry[] = $file_format;
		}
	}
    $forbidFileFormatAry = array_merge((array)$____block_file_upload_extensions____, array('.stm'));
	foreach($forbidFileFormatAry as $thisFileFormat) {
        if($thisFileFormat != '') {
            $FileFormatAry[] = str_replace('.', '', $thisFileFormat);
        }
    }
	$RestrictFileFormat = count($FileFormatAry) > 0;
	
	# Create folder
	$targetPath = "/digital_archive/";
	$targetPath_2ndLevel = "/admin_doc/";
	$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath);
	if ( !file_exists($target1stPath) || !is_dir($target1stPath)) {
		$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
	}
	
	$target2ndPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath.$targetPath_2ndLevel);
	if ( !file_exists($target2ndPath) || !is_dir($target2ndPath)) {
		$resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
	}
	
	$targetFullPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath.$targetPath_2ndLevel);
	$tempPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$TargetFolder);
	chmod($targetFullPath, 0777);
	
	$tempFileAry = $libfilesystem->return_folderlist($tempPath);
	$numOfFiles = count($tempFileAry);
	for($i=0; $i<$numOfFiles; $i++)
	{
		# Get file name
		$origFileName = $libfilesystem->get_file_basename($tempFileAry[$i]);
		if (empty($origFileName)) {
		    continue;
        }

		# Check file size
        $fileSize = filesize($tempFileAry[$i]);
		if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
			$result[] = false;
			$returnMsg .= str_replace(array('<!--FILENAME-->','<!--SIZE-->'),array($origFileName,$MaxFileSize),$Lang['DigitalArchive']['jsFileSizeExceedLimit']).'<br />';
		}

		# Check file format
        $ext = substr($libfilesystem->file_ext($origFileName),1);
		if($RestrictFileFormat && in_array(strtolower($ext),$FileFormatAry)) {
			$result[] = false;
			$returnMsg .= str_replace(array('<!--FILENAME-->','<!--EXT-->'),array($origFileName,$ext),$Lang['DigitalArchive']['jsFileTypeNotAllowed']).'<br />';
		}

		# Check if zip files contains restricted file types
		if($RestrictFileFormat && $ext == 'zip') {
			$banExtAry = array();
			$isZipValid = true;
			$tempUnzipfolder = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$TargetFolder."/tmp_unzip");
			if(file_exists($tempUnzipfolder)) {
				$libfilesystem->deleteDirectory($tempUnzipfolder);
			}
			$libfilesystem->folder_new($tempUnzipfolder);
			if(file_exists($tempUnzipfolder)) {
				chmod($tempUnzipfolder, 0777);
			}
			
			$libfilesystem->file_unzip($tempFileAry[$i],$tempUnzipfolder);
			$tempUnzipFileAry = $libfilesystem->return_folderlist($tempUnzipfolder);
			for($j=0; $j<count($tempUnzipFileAry); $j++) {
				$tempUnzipFileExt = strtolower( substr($libfilesystem->file_ext($tempUnzipFileAry[$j]),1) );
				if(in_array($tempUnzipFileExt, $FileFormatAry)) {
					$isZipValid = false;
					$banExtAry[] = $tempUnzipFileExt;
				}
			}
			
			if(file_exists($tempUnzipfolder)) {
				$libfilesystem->deleteDirectory($tempUnzipfolder);
			}
			
			if(!$isZipValid) {
				$result[] = false;
				$bannedExt = implode(", ",array_values(array_unique($banExtAry)));
				$returnMsg .= str_replace(array('<!--FILENAME-->','<!--EXT-->'),array($origFileName,$bannedExt),$Lang['DigitalArchive']['jsZipFileContainRestrictedFileType']).'<br />';
			}
		}
		
		$timestamp = date('YmdHis');
		$sqlSafeOrigFileName = $lda->Get_Safe_Sql_Query($origFileName);
		$Title = $sqlSafeOrigFileName;
		
		# Encode file name
		$targetFileHashName = "u".$UserID."_".$timestamp.$i;
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# Check duplication
		$IsDuplicateFlag = $lda->Is_Duplicate_Admin_Document($GroupID, $FolderID, $Title, $sqlSafeOrigFileName);
		
		$canManageOthers = 0;
		$adminInGroup = $lda->returnAdmin();
		if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
			// is super admin ?
			$canManageOthers = 1;
		} else if($adminInGroup[$GroupID]) {
			// does this admin user has group manage others file access right?
			if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE")) {
				$canManageOthers = 1;	
			}
		} else if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE")) {
			// does this member user has group manage others file access right?
			$canManageOthers = 1;	
		}
		
		if($IsDuplicateFlag && !$canManageOthers) {
			$result[] = false;
			$returnMsg .= str_replace('<!--FILENAME-->',$origFileName,$Lang['DigitalArchive']['jsDuplicatedOtherUserFile']).'<br />';
		}	
	}
}

echo $returnMsg;

intranet_closedb();
exit;
?>