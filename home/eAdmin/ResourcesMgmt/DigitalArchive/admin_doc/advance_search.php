<?php
//using:
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
global $i_To;

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

//get the string of the groupid(s)
if ($_GET["GroupIDArr"] != '')
	$groupIds = implode(",", $_GET["GroupIDArr"]);
else
	if ($_REQUEST["selectedGroups"] != '')
		$groupIds = $_REQUEST["selectedGroups"];

//get the tag array
$tagAry = returnModuleAvailableTag($lda->AdminModule, 1);
$tagNameAry = array_values($tagAry);    
foreach ($tagNameAry as $value) $Taglist .="'".addslashes($value)."', ";
$Taglist = rtrim($Taglist,', ');


/*
$SelectedTaglist = trim($_GET['tag']);
if($SelectedTaglist!=""){
   $SelectedTaglist_handle = explode(",", $SelectedTaglist);
   for($i=0;$i<count($SelectedTaglist_handle);$i++){
    	$SelectedTaglist_handle_new[$i] = trim($SelectedTaglist_handle[$i],'[]"\\');
    	$SelectedTaglist_handle_new[$i] = "'".$SelectedTaglist_handle_new[$i]."'";
   }
	$SelectedTaglist = implode(",", $SelectedTaglist_handle_new);		
}
*/
// echo $ldaUI->Include_JS_CSS();

?>

<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/ereportcard.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/parent_reg.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css"/>
<!--<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>-->
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" />
<script language="javascript" src="scrolltext.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="./TextExt.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>

<style type="text/css">
body{
	background-color:#f3f3f3;
}
</style>
<script language="javascript">		

$().ready( function(){

// autocomplete book tag
	$("#tag").textext({
			plugins : "tags autocomplete"
	}).bind("getSuggestions", function(e, data){
		var list = [ <?=$Taglist?>],
			textext = $(e.target).textext()[0],
			query = (data ? data.query : "") || ""
			;

		$(this).trigger(
			"setSuggestions",
			{ result : textext.itemManager().filter(list, query) }
		);
	});

});

function getCheckDateRange(flag) {
	if(flag) {
		$('#StartDate').attr("disabled", false);
		$('#EndDate').attr("disabled", false);
	} else {
		$('#StartDate').attr("disabled", true);
		$('#EndDate').attr("disabled", true);		
	}
}

function submitSearch(advSearch) {
	$('#advance').val(advSearch);
	if(advSearch==1 && $('#byDateRange').is(':checked')){
		var isValid = true; 
		var startdateObj = document.getElementById('StartDate');
		var enddateObj = document.getElementById('EndDate');
		if(!check_date_without_return_msg(startdateObj)) {
			$('#DPWL-StartDate').html('<?= $Lang['General']['InvalidDateFormat']?>').show();
			startdateObj.focus();
			isValid = false;
		}
		if(!check_date_without_return_msg(enddateObj)) {
			$('#DPWL-EndDate').html('<?= $Lang['General']['InvalidDateFormat'] ?>').show();
			enddateObj.focus();
			isValid = false;
		}
		if(isValid && startdateObj.value.Trim() > enddateObj.value.Trim()) {
			$('#DPWL-StartDate').html('<?= $Lang['General']['JS_warning']['InvalidDateRange']  ?>').show();
			startdateObj.focus();
			isValid = false;
		}
		if(!isValid) return;
	}
	document.AdvanceSearchForm.action = "searchResult.php";
	document.AdvanceSearchForm.submit();
		
}
 
function goCancel() {
	document.SearchForm.reset();
}

</script>

 <div class="resource_search_advance_form">
   <form name="AdvanceSearchForm" id="AdvanceSearchForm" method="get" target="_parent"> 
	 <div class="table_board">
	    <br/>
		<table class="form_table " style="width:600px;margin:auto">
			<tr>
             <td class="field_title"><?php echo $Lang['DigitalArchive']['Keywords'];?></td>
             <td><label>
               <input name="str" id="str" type="text" class="keywords" style="" value="<?= intranet_htmlspecialchars($_GET['str']) ?>" />
             </label></td>
           </tr>

			<tr>
				<td class="field_title"><?php echo $Lang["DigitalArchive"]["UploadDate"] ;?></td>
				<td>
					<input type="checkbox" name="byDateRange" id="byDateRange" onClick="getCheckDateRange(this.checked)" value="1" <?= ($_GET['byDateRange'] ? "checked" : "") ?>/> 
					<?= $linterface->GET_DATE_PICKER("StartDate", substr($_GET['StartDate'], 0, 10), "disabled") .' '. $i_To .' '. $linterface->GET_DATE_PICKER("EndDate", substr($_GET['EndDate'], 0, 10), "disabled") ?>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?= $Lang["DigitalArchive"]["CreatorName"] ?></td>
				<td>
					<input type="text" class="tabletext" name="createdBy" ID="createdBy" value="<?=intranet_htmlspecialchars($_GET['createdBy']) ?>">
				</td>
			</tr>
			<tr>
				<td class="field_title"><?= $Lang["DigitalArchive"]["GroupName"] ?></td>
				<td>
					<?= $ldaUI->Get_My_Group_Select_Menu("GroupIDArr[]", intranet_htmlspecialchars($groupIds), "", "", $IsMultiple = 1)  ?>
					<br/>
					<span class="tabletextremark">
						<?=  $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'] ?>
					</span>										
				</td>
			</tr>
			<tr>
				<td class="field_title" rowspan="3" ><?= $Lang["DigitalArchive"]["Tag"] ?></td>
				<td>					
                    <input type="text" name="tag" id="tag" class="textboxtext" style="width:300px;height:21px"/>
				</td>	
			</tr>
			<tr>
			    <td><span class="tabletextremark"><?= $Lang['DigitalArchive']['TagInput']?></span></td>					   
			</tr>
			<tr>
				<td>
					<label><input type="radio" name="choose_tag" value="all" <?=($_GET['choose_tag']=="all"||$_GET['choose_tag']=="" ? "checked" : "")?> ><?=$Lang["DigitalArchive"]["AllMatches"]?></label>
					<label><input type="radio" name="choose_tag" value="part" <?=($_GET['choose_tag']=="part" ? "checked" : "")?> ><?=$Lang["DigitalArchive"]["PartialMatches"]?></label>
				</td>	
			</tr>	
		</table>
		<br/>
	</div>
	<div class="edit_bottom">
			 <?= $linterface->GET_ACTION_BTN($Lang['Btn']['Search'], "button", "submitSearch(1)") ?>
			 <?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.parent.tb_remove();") ?>
	</div>
	<input type="hidden" name="advance" id="advance" value="">
  </form>
</div>
<?php

intranet_closedb();

?>