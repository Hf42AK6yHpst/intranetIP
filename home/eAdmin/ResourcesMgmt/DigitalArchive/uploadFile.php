<?php
$libfilesystem = new libfilesystem();
$lda = new libdigitalarchive();

$resultCreatedFolder = false;

if (!empty($_FILES)) {
	$targetPath = "/digital_archive/";
	# create folder

	$targetFullPath = str_replace('//', '/', $PATH_WRT_ROOT."../intranetdata/".$targetPath);
	
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
	}
	
	chmod($targetFullPath, 0777);
	
	$numOfFiles = count($_FILES['File']['name']);

	# get file name
	$tempFile = $_FILES['File']['tmp_name'];
	
	$origFileName =  $_FILES['File']['name'];
	if (empty($origFileName)) continue;
	
	$timestamp = date('YmdHis');
	
	# encode file name
	$targetFileHashName = "u".$UserID."_".$timestamp;
	$targetFile = $targetFullPath.$targetFileHashName;
	
	# upload file
	$uploadResult = move_uploaded_file($tempFile, $targetFile);
	
	chmod($targetFile, 0777);
	
	# Extension
	$ext = substr($origFileName, strrpos($origFileName, '.') + 1);
	

	# add record to DB
	$dataAry = array();
	$dataAry['Title'] = $Title;
	$dataAry['Description'] = $Description;
	$dataAry['FileName'] = $origFileName;
	$dataAry['FileHashName'] = $targetFileHashName;
	$dataAry['SourceExtension'] = strtolower($ext);
	$dataAry['SubjectID'] = $SubjectID;
	$dataAry['SizeInBytes'] = $_FILES['File']['size'];
	
	if(isset($RecordID) && $RecordID!="") {	# update
		//$RecordID = $lda->updateResourceRecord($RecordID, $dataAry);
	} else {								# insert
		$RecordID = $lda->insertResourceRecord($dataAry);
	}

	$lda->updateRecordYearRelation($YearID, $RecordID);
	
	# Update Tag relationship
	$lda->Update_Resource_Tag_Relation($Tag, $RecordID);
}
?>