<?
/************************************
 * Date: 2014-02-21 (Tiffany)
 * Details: Modified  checkThisForm() and handle Campus TV Url
 * 
 ************************************/
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();
$fcmy = new Year();
$subject = new subject();

$RecordID = is_array($RecordID) ? $RecordID[0] : $RecordID;

$FormButton = $ldaUI->GET_ACTION_BTN($button_submit, "button", "checkThisForm()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_reset, "reset", "resetAllSpanInnerHtml()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_cancel, "button", "window.tb_remove()");

$tagAry = returnModuleAvailableTag($lda->Module, 1);
$tagNameAry = array_values($tagAry);

$delim = "";
if(sizeof($tagNameAry>0)) {
	foreach($tagNameAry as $tag) {
		$availableTagName .= $delim."\"".addslashes($tag)."\"";
		$delim = ", ";
	}
}


$allForms = $fcmy->Get_All_Year_List();
$allSubjects = $subject->Get_All_Subjects();

$tempAry = array();
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$tempAry[$i] = array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));
}
$allSubjectsRevised = $tempAry;

$data = $lda->Get_Subject_Resource_Data_By_ID($RecordID);
if($data['SourceExtension']=='tvurl'){
	$sourceDisplay=$data['Url'];   
}elseif($data['SourceExtension']=='url'){
	$sourceDisplay = "<input type='text' name='Url' id='Url' value='".intranet_htmlspecialchars($data['Url'])."' class='textboxtext2'>";
}else{
    $sourceDisplay = $data['FileName'];
}
//if($data['Url']!="") {
//	$sourceDisplay = "<input type='text' name='Url' id='Url' value='".intranet_htmlspecialchars($data['Url'])."' class='textboxtext2'>";	
//} else {
//	$sourceDisplay = $data['FileName'];
//}

$YearID = $lda->Get_ClassLevel_By_RecordID($RecordID);
$SubjectID = $data['SubjectID'];
$tagDisplay = $lda->Display_Tag_By_Resource_Record_ID($RecordID, 'textWithoutLink');

$formDisplay = getSelectByArray($allForms, 'name="YearID[]" id="YearID[]" multiple size="10"', $YearID, 0, 1);
$subjectDisplay = getSelectByArray($allSubjectsRevised, 'name="SubjectID" id="SubjectID"', $SubjectID, 0, 1);


//echo $ldaUI->Include_JS_CSS();
?>
 

<script language="javascript">
function checkThisForm() {

	resetAllSpanInnerHtml();

	var obj = document.editForm;	
	var error_no = 0;
	var focus_field = "";

	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang["DigitalArchive"]["Title"]; ?>.", "title_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	}
		if(!validateFilename(obj.Title.value, '<?=$Lang['DigitalArchive']['jsNameCannotWithSpecialCharacters']?>')) 
	{
		return false;	
	}
	
	<?if($data['SourceExtension']=='url') {?>
	if(!check_text_30(obj.Url, "<?php echo $i_alert_pleasefillin.$Lang["DigitalArchive"]["Url"]; ?>.", "source_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Url";
	}		
	<? } ?>		
	if(countOption(document.editForm.elements['YearID[]'])==0) {
		$('#level_err_msg').html('<font color=red><?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["Level"]; ?>.</font>');
		error_no++;
	}

	if(countOption(document.editForm.elements['SubjectID'])==0) {
		$('#subject_err_msg').html('<font color=red><?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["Subject"]; ?>.</font>');
		error_no++;
		if(focus_field=="")	focus_field = "SubjectID";
		
	}

	if(error_no>0)
	{
		if(focus_field!="")	
			eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		obj.action = "edit_update.php";
		obj.submit();
	}
}



function resetAllSpanInnerHtml() {
 	$('#title_err_msg').html('');
 	$('#source_err_msg').html('');
 	$('#level_err_msg').html('');
 	$('#subject_err_msg').html('');
}

</script>

<br style="clear:both" />

<form name="editForm" id="editForm" method="POST" action="">

<div class="table_board">
	<table class="form_table_v30 ">
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Title"]?></td>
			<td>
				<input name="Title" id="Title" type="text" class="textboxtext" value="<?=intranet_htmlspecialchars($data['Title'])?>" />
				<span id="title_err_msg"></span>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang["DigitalArchive"]["Description"]?></td>
			<td><?=$ldaUI->GET_TEXTAREA("Description", $data['Description'], 70, 5, "", "", "", "textboxtext");?></td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Source"]?>
			</td>
			<td> 
				<?=$sourceDisplay?>
				<br><span id="source_err_msg"></span>
			</td>
		
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Level"]?></td>
			<td><?=$formDisplay?><span id="level_err_msg"></span>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Subjects"]?></td>
			<td><?=$subjectDisplay?><span id="subject_err_msg"></span></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang["DigitalArchive"]["Tag"]?></td>
			<td><input type="text" name="Tag" id="Tag" value="<?=intranet_htmlspecialchars($tagDisplay)?>" class="textboxtext2"><br>
			<span class="tabletextremark">(<?=$Lang['StudentRegistry']['CommaAsSeparator'].", ".$Lang["DigitalArchive"]["Max10Words"]?>)</span></td>
		</tr>
	</table>
	<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span> 
	<p class="spacer"></p>
	</div>			

	<div class="edit_bottom">
	<p class="spacer"></p>
	<?=$FormButton?>
	<p class="spacer"></p>
</div>			
			
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
<input type="hidden" name="flag" id="flag" value="<?=$flag?>">

</form>


<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script language="javascript">

$('#Tag').multicomplete({
	source: [<?=$availableTagName?>]
});

</script>

<?
echo $ldaUI->FOCUS_ON_LOAD("editForm.Title"); 
intranet_closedb();
?>