<?php

// Using: 

/************************************
 * Date: 2014-02-20 (Tiffany)
 * Details: handle $FileType==3, $FileType==4
 * 
 * Date: 2013-04-11 (Rita)
 * Details: fix tag duplication issue
 * 
 ************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# Fix Duplication of Tag
$TagArr = explode(',', $Tag);

for($i=0;$i<count($TagArr);$i++){
	$trimTagArr[]= trim($TagArr[$i]);
}
$uniqueTagArr = array_unique($trimTagArr);
$Tag = implode(',',$uniqueTagArr);


if($FileType==1) {
	
	# upload with file (update to database in uploadFile.php)
	include_once("uploadFile.php");
} 
elseif($FileType==3){
	# upload with DA file (update to database in uploadFileDA.php)
	include_once("uploadFileDA.php");
}
elseif($FileType==4){
    # upload with DA file (update to database in uploadFileDA.php)
    include_once("uploadCampusTV.php");    
}
else {
	
	# upload "URL" only (update to database here)
	# add record to DB
	$dataAry = array();
	$dataAry['Title'] = $Title;
	$dataAry['Description'] = $Description;
	$dataAry['Url'] = $Url;
	$dataAry['SubjectID'] = $SubjectID;
	$dataAry['SourceExtension'] = strtolower("url");
	
	$RecordID = $lda->insertResourceRecord($dataAry);	
	$lda->updateRecordYearRelation($YearID, $RecordID);
	
	

	
	# Update Tag relationship
	$lda->Update_Resource_Tag_Relation($Tag, $RecordID);
}	

if($RecordID)
	$msg = "AddSuccess";
else 
	$msg = "AddUnsuccess";
	
intranet_closedb();


header("Location: index.php?msg=$msg");
?>