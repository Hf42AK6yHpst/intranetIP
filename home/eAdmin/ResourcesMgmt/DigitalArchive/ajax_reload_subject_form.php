<?php

//using:  
/**********************************************
 * Date: 	2013-05-27 (Rita)
 * Details: amend array for form 
 * 
 * Date:	2013-05-22 (Rita)
 * Details:	add term 
 * 
 * Date:	2013-04-08 (Rita)
 * Details:	Modified get group info
 * 
 * Date: 	2013-04-03 (Rita)
 * Details:	Create this page
 ************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$subject = new subject();
$subject_class_mapping = new subject_class_mapping();

$action = $_POST['action'];

# Reload Subject Selection

# Get Group Admin Info of Current User
//$isGroupAdmin = false;
//$adminGroupMemberInfoSql = $lda->Get_Subject_eResources_Group_Member_Sql('',$_SESSION['UserID']);
//$adminGroupMemberInfo = $lda->returnArray($adminGroupMemberInfoSql);
//
//$numOfAdminGroupMemberInfo = count($adminGroupMemberInfo);			
//if($numOfAdminGroupMemberInfo>0){				
//	$isGroupAdmin = true;	
//	$adminGroupIDArray = array_keys(BuildMultiKeyAssoc($adminGroupMemberInfo, 'GroupID'));				
//	$Sql = $lda->Get_Subject_eResources_Group_Sql($adminGroupIDArray);
//	$groupInfoArr = $lda->returnArray($Sql);	
//}

$Sql = $lda-> Get_Subject_eResources_Group_Sql('','',$_SESSION['UserID']);
$groupInfoArr = $lda->returnArray($Sql);
$isGroupAdmin = false;
if(count($groupInfoArr)>0){
	$isGroupAdmin = true;
}

if($action=='reloadSubject'){
	
	# Get All Subjects
	$allSubjects = $subject->Get_All_Subjects();

	# Remarks
	/* TBD: should handle 3 cases
	 * 1. for module admin, provide him/her all subjects
	 * 2. for subject admin, provide him/her related subjects (and subjects in case 3 below)
	 * 3. for subject teacher, provide him/her related subjects
	 */
	 
	$tempAry = array();
	for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
		
		### Set Module Admin Can View All Subjects ###
		if($_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) 
		{
			$tempAry[]= array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));		
		}
		else
		{		
			### Group Admin ###
			if($isGroupAdmin==true){		
				$subjectIDArrayofAdmin = array_keys(BuildMultiKeyAssoc($groupInfoArr, 'SubjectID'));
				//debug_pr($subjectIDArrayofAdmin);
			}
			
			### General Teaching Staff - Show Related Subjects ###
			$subjectsIDArrayOfTeacher = array();
			//$AcademicYearID = Get_Current_Academic_Year_ID();
		
			$yearTermArr = getCurrentAcademicYearAndYearTerm();			
			$thisAcademicYearID = $yearTermArr['AcademicYearID'];
			$thisYearTermID = $yearTermArr['YearTermID'];
			
			$subjectInfoArrayOfTeacher = $subject_class_mapping->Get_User_Accessible_Subject_Group($_SESSION['UserID'], $thisAcademicYearID, $thisYearTermID);
			$subjectsIDArrayOfTeacher = array_keys(BuildMultiKeyAssoc($subjectInfoArrayOfTeacher, 'SubjectID'));
			
			//debug_pr($subjectsIDArrayOfTeacher);
			# Merge Group Admin Subject with Accessible Subject
			$subjectsIDArray = array();
			if($isGroupAdmin==true){ ### Group Admin
				if(count($subjectsIDArrayOfTeacher)>0){
					$subjectsIDArray = array_merge($subjectsIDArrayOfTeacher, $subjectIDArrayofAdmin);
					//debug_pr($subjectsIDArray);
					$subjectsIDArray = array_unique($subjectsIDArray);
				}else{
					$subjectsIDArray = $subjectIDArrayofAdmin;
				}
				
			}else
			{ ### Gernal Teaching Staff
			
				$subjectsIDArray = $subjectsIDArrayOfTeacher;
			}
			
			if(in_array($allSubjects[$i]['RecordID'],$subjectsIDArray)){		
				$tempAry[]= array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));
			}			
		}
		
		$allSubjectsRevised = $tempAry;	
	}

	$subjectSelectionDisplay = sizeof($allSubjectsRevised)>0?getSelectByArray($allSubjectsRevised, 'name="SubjectID" id="SubjectID" onchange="js_reload_form()";', '', 0, 1):'';
	
	echo $subjectSelectionDisplay;
}
elseif($action=='reloadForm') ## Reload Form Selection
{ 
	
	$selectedSubjectID = $_POST['selectedSubjectID'];
	
	$formDataArrayForSelection = array();
	$formDataArray = array();
	
	# Get All Forms
	$formDataArray = $subject->Get_Form_By_Subject($selectedSubjectID);
		
	# Module Admin Show All Form
	if($_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
		
		$formDataArrayForSelection = $formDataArray;
		
	}else
	{	
		# Group Admin
		if($isGroupAdmin == true){			
			$groupSubjectArrayAssoc = BuildMultiKeyAssoc($groupInfoArr, 'SubjectID');
			$classLevelIDStringOfAdmin = $groupSubjectArrayAssoc[$selectedSubjectID]['FormIDArrSting'];	
			$classLevelIDArrayOfAdmin = explode(',',$classLevelIDStringOfAdmin);		
			//debug_pr($classLevelIDArrayOfAdmin);
		}
				
		# Teaching Class Level
		
		//$AcademicYearID = Get_Current_Academic_Year_ID();
		$yearTermArr = getCurrentAcademicYearAndYearTerm();			
		$thisAcademicYearID = $yearTermArr['AcademicYearID'];
		$thisYearTermID = $yearTermArr['YearTermID'];
			
		$subjectInfoArrayOfTeacher = $subject_class_mapping->Get_User_Accessible_Subject_Group($_SESSION['UserID'], $thisAcademicYearID, $thisYearTermID, $selectedSubjectID);
		
		
//		$subjectGroupID = $subjectInfoArrayOfTeacher[0]['SubjectGroupID'];
//		$SubjectTermClass = new subject_term_class($subjectGroupID,true,true);
//		$teachingClasslevelIDArray = $SubjectTermClass->YearID;
		
		
		$thisFormId='';
		$teachingClasslevelIDArray = array();
		for($i=0;$i<count($subjectInfoArrayOfTeacher);$i++){
			$subjectGroupID = $subjectInfoArrayOfTeacher[$i]['SubjectGroupID'];
			
			$SubjectTermClass = new subject_term_class($subjectGroupID,true,true);
			$thisFormId = $SubjectTermClass->YearID;
			
			$teachingClasslevelIDArray[]= $thisFormId[0];
			
		}
		$teachingClasslevelIDArray = array_unique($teachingClasslevelIDArray);
		
		//debug_pr($teachingClasslevelIDArray);
		
		$mergeClassLevelIDArr = array();
		if($isGroupAdmin == true){
			if(count($teachingClasslevelIDArray)>0){
				$mergeClassLevelIDArr = array_merge($classLevelIDArrayOfAdmin, $teachingClasslevelIDArray);
				$mergeClassLevelIDArr = array_unique($mergeClassLevelIDArr);		
			}else{
				$mergeClassLevelIDArr = $classLevelIDArrayOfAdmin;
			}
			
		}else{
			$mergeClassLevelIDArr = $teachingClasslevelIDArray;
		}
	
		
		
		for($i=0;$i<count($formDataArray);$i++){
			if(in_array($formDataArray[$i]['YearID'], $mergeClassLevelIDArr)){	
				$formDataArrayForSelection[]= $formDataArray[$i];
			}
		}
		
	}
	$formDisplay = sizeof($formDataArrayForSelection)>0?getSelectByArray($formDataArrayForSelection, 'name="YearID[]" id="YearID" class="YearID" multiple size="10"', $ClassLevelID, 0, 1):'';
	$formDisplay = str_replace("'", '"', $formDisplay);
	
	echo $formDisplay;

}
intranet_closedb();
?>
