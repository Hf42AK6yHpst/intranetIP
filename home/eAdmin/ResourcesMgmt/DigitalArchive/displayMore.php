<?php
# modifying : 

##### Change Log [Start] #####
#   Date    :   2014-02-24  (Tiffany)
#               Add function loadCampusTV()
#
#	Date	:	2014-02-18	(Yuen)
#				support filter by class level
#
###### Change Log [End] ######



$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if(!isset($flag) || $flag=="") {
	header("Location: index.php");
	exit;	
}


# cannot use Cookies since default column sorting is required 

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
//$arrCookies[] = array("ck_page_field", "field");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);



if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 0;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$fcmy = new Year();
$subject = new subject();

if((isset($SubjectID) && !is_array($SubjectID))) { $SubjectID1 = $SubjectID; }
if(!isset($SubjectID) && isset($SubjectID1) && $SubjectID1!="") { $SubjectID = array(); $SubjectID[0] = $SubjectID1; }
if((!isset($SubjectID1) && is_array($SubjectID))) { $SubjectID1 = $SubjectID[0]; }

switch($flag) {
	case FUNCTION_NEWEST :
		$navTitle = $Lang["DigitalArchive"]["Newest"];
		$field = ($field=="") ? 5 : $field; 
		break;
	case FUNCTION_FAVOURITE :
		$navTitle = $Lang["DigitalArchive"]["Favourite"]; 
		$field = ($field=="") ? 4 : $field; 
		break;
	case FUNCTION_MY_SUBJECT :
		$sql = "SELECT CONCAT(' (',".Get_Lang_Selection("CH_DES","EN_DES").",')') as Subject FROM ASSESSMENT_SUBJECT WHERE RecordID='$SubjectID'";
		$resultSubject = $lda->returnVector($sql);
		$navTitle = $Lang["DigitalArchive"]["Subject"].$resultSubject[0]; 
		$conds = " AND rec.SubjectID='$SubjectID1'";
		$field = ($field=="") ? 0 : $field; 
		break;
	case FUNCTION_TAG :
		$navTitle = $Lang["DigitalArchive"]["Tag"];
		$field = ($field=="") ? 0 : $field; 
		break;
	case FUNCTION_ADVANCE_SEARCH :
		$navTitle = $Lang["DigitalArchive"]["SearchResult"];
		$field = ($field=="") ? 0 : $field; 
		if($advanceSearchFlag) {
			if($str!='') $conds .= " AND (rec.Title LIKE '%$str%' OR rec.Description LIKE '%$str%')";
			if(sizeof($SubjectID)>0 && $SubjectID[0]>0) $conds .= " AND rec.SubjectID IN (".implode(',', $SubjectID).")";
			if(sizeof($ClassLevelID)>0) $conds .= " AND lvl.ClassLevelID IN (".implode(',', $ClassLevelID).")";
			if($mylike) $conds .= " AND l.UserID='$UserID'";
		} else {
			if($keyword!='') $conds .= " AND (rec.Title LIKE '%$keyword%' OR rec.Description LIKE '%$keyword%')";	
			if($SubjectID1!="" && $SubjectID1!=0) $conds .= " AND rec.SubjectID='$SubjectID1'";
		}
		break;

	default :
		header("Location: index.php");
		exit; 
		break;	
}
$arrCookies[] = array("ck_page_field", "field");


if ($FilterClassLevelID=="" || $FilterClassLevelID<1)
{
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$sql = "SELECT
					yc.YearID
			FROM
					YEAR_CLASS_USER as ycu
					INNER JOIN
					YEAR_CLASS as yc
			WHERE
					ycu.YearClassID = yc.YearClassID 
				AND 
					yc.AcademicYearID = '".$AcademicYearID."'
				AND
					ycu.UserID = '".$UserID."'
			";
	$resultSet = $lda->returnVector($sql);
	$FilterClassLevelID = $resultSet[0];
}

if ($FilterClassLevelID!="" && $FilterClassLevelID>0)
{
	$conds .= " AND lvl.ClassLevelID='{$FilterClassLevelID}' ";
}

$table = $ldaUI->Get_Display_More_Table($field, $order, $pageNo, $flag, $conds);


$allForms = $fcmy->Get_All_Year_List();
$allSubjects = $subject->Get_All_Subjects();

$tempAry = array();
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$tempAry[$i] = array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));
}
$allSubjectsRevised = $tempAry;

$formDisplay = getSelectByArray($allForms, 'name="ClassLevelID[]" id="ClassLevelID[]" multiple size="10"', $ClassLevelID, 0, 1);

$subjectDisplay = getSelectByArray($allSubjectsRevised, 'name="SubjectID[]" id="SubjectID[]" multiple size="10"', $SubjectID, 0, 1);


# add form filter
$formDisplayForStudents = getSelectByArray($allForms, 'name="FilterClassLevelID" id="FilterClassLevelID" ', $FilterClassLevelID, 0, 0, $Lang['SysMgr']['FormClassMapping']['All']['Form']);

$searchOption = "
	<div id=\"resource_search\" class=\"resource_search_simple\">
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 1)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
		<div class=\"resource_search_keywords\"> ".$Lang["DigitalArchive"]["Search"]." : <input name=\"keyword\" id=\"keyword\" type=\"text\"  class=\"keywords\" style=\"\" value=\"".$keyword."\" /> 
			<select name=\"SubjectID1\" id=\"SubjectID1\">
				<option value=\"0\">".$Lang['SysMgr']['SubjectClassMapping']['All']['Subject']."</option>";
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$selected = ($allSubjects[$i]['RecordID']==$SubjectID1) ? " selected" : "";
	
	$subjectName = Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']);
	$searchOption .= "<option value=\"".$allSubjects[$i]['RecordID']."\" $selected>$subjectName</option>";
}
$searchOption .= "
			</select> {$formDisplayForStudents}
			<input type=\"button\" value=\"".$Lang["DigitalArchive"]["Search"]."\" class=\"formsmallbutton\" onClick=\"submitSearch(0)\" />
		</div>
	</div>



	<div id=\"resource_search_adv\" class=\"resource_search_advance\" style=\"display:none\" >
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 0)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
			<p class=\"spacer\"></p>
			<div class=\"resource_search_advance_form\" >
				<div class=\"table_board\">
					<table width='570' border='0' align='center'>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["SearchFor"]."</td>
							<td><input name=\"str\" id=\"str\" type=\"text\" class=\"keywords\" style=\"\" value=\"$str\" /></span>								
							</td>
						</tr>
						<tr>
							<td class=\"field_title\" width=\"20\" >".$Lang["DigitalArchive"]["Level"]."</td>
							<td >$formDisplay".
								$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "checkOptionAll(document.form1.elements['ClassLevelID[]'])")
							."<br><span class=\"tabletextremark\">".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."								
							</td>
						</tr>
						<tr>
							<td class=\"field_title\" >".$Lang["DigitalArchive"]["Subject"]."</td>
							<td>$subjectDisplay".
								$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "checkOptionAll(document.form1.elements['SubjectID[]'])")
							."<br><span class=\"tabletextremark\">".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."
							</td>
						</tr>
						<tr>
							<td class=\"field_title\">&nbsp;</td>
							<td><input type='checkbox' name='mylike' id='mylike' value='1'".($mylike ? " checked" : "")."><label for='mylike'>".$Lang["DigitalArchive"]["MyLike"]."</label></td>
						</tr>
					</table>
				</div>
				<div class=\"edit_bottom\">
						".$linterface->GET_ACTION_BTN($button_search,"button","submitSearch(1)")."
						".$linterface->GET_ACTION_BTN($button_cancel,"button","goCancel()")."
				</div>
			</div>
		</div>
	</div>
	";
	
if($_SESSION['UserType']==USERTYPE_STAFF) {	
	$actionBtn = '
	<td valign="bottom"><div class="common_table_tool">';
	if($flag!=FUNCTION_TAG) {
		//$actionBtn .= '<a href="#TB_inline?height=600&width=750&inlineId=FakeLayer" onClick="Display_Edit_File();return false;" class="tool_edit setting_row thickbox" title="'.$button_edit.'">'.$button_edit.'</a>';
		$actionBtn .= '<a href="javascript:;" onClick="Display_Edit();" class="tool_edit setting_row" title="'.$button_edit.'">'.$button_edit.'</a>';
		$actionBtn .= '<a href="javascript:remove_record(document.form1, \'RecordID[]\')" class="tool_delete">'.$button_delete.'</a>';
	} else {
		$actionBtn .= '<a href="javascript:remove_tag(document.form1, \'TagID[]\')" class="tool_delete">'.$button_delete.'</a>';	
	}
	$actionBtn .= '
		
	';

	$actionBtn .= '</div><a href="#TB_inline?height=500&width=750&inlineId=FakeLayer;" onClick="Display_Edit_File();" class="setting_row thickbox" title="'.$button_edit.'" id="editLink2">&nbsp;</a></td>';
}
#----Tiffany-----
$showCampusTV.='<div id="ShowCampusTVDiv">';



$showCampusTV.='</div>';
#----Tiffany-----

$CurrentPageArr['DigitalArchive_SubjectReference'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(1);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript" src="digitalarchive.js"></script>

<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";

function goCancel() {
	document.form1.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function goBack() {
	self.location.href = "index.php";
}


function Display_Edit_File() {

	var RecordID = Get_Check_Box_Value('RecordID[]','Array');

	if(RecordID.length==1) {
		$.post(
			"ajax_edit_file.php", 
			{ 
				RecordID: RecordID,
				flag: '<?=$flag?>',
				SubjectID: '<?=$SubjectID?>'
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);	
			}
		);	
	} else {
		alert(globalAlertMsg1);	
	}		
}

function unlike_record(obj, emt) {
	var RecordID = Get_Check_Box_Value('RecordID[]','Array');

	if(RecordID.length>0) {
		obj.action = "unlike_update.php";
		obj.submit();
	} else {
		alert(globalAlertMsg1);	 
	}
}


function remove_record(obj, emt) {
	if(countChecked(obj, emt)==0) {
		alert(globalAlertMsg2);
	} else {
		if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
			obj.action = "remove_update.php";
			obj.method = "POST";
			obj.submit();
		}
	}
}

function remove_tag(obj, emt) {
	if(countChecked(obj, emt)==0) {
		alert(globalAlertMsg2);
	} else {
		if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
			obj.action = "remove_tag_update.php";
			obj.method = "POST";
			obj.submit();
		}
	}
}

function showHideLayer(flag, val) {
	var obj = document.form1;
	if(val==1) {
		$('#'+flag).val(val);
		$('#resource_search').hide();	
		$('#resource_search_adv').show();
	} else {
		$('#'+flag).val(val);
		$('#resource_search_adv').hide();
		$('#resource_search').show();	
	}
	obj.reset();
}

function submitSearch(advSearch) {
	$('#flag').val('<?=FUNCTION_ADVANCE_SEARCH?>');
	$('#advanceSearchFlag').val(advSearch);
	document.form1.action = "displayMore.php";
	document.form1.submit();
		
}

function Display_Edit() {
	initThickBox();
	var DocumentID = Get_Check_Box_Value('RecordID[]','Array');

	if(DocumentID.length==1) {
		$('#editLink2').click();
		
	} else {
		alert(globalAlertMsg1);
		window.tb_remove();
		return false;
	}		
}

function Display_Edit_Layer() {

	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	var DocumentID = Get_Check_Box_Value('RecordID[]','Array');

	$.post(
		"ajax_edit_file.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID,
			DocumentID: DocumentID
		},
		function(ReturnData)
		{
			
			$('#TB_ajaxContent').html(ReturnData);	
		}
	);

		
	
}

function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	
	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID,
			Delimiter: "|"
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			//Get_Resource_MyFavourite_Table();
		}
	)
	
}

function loadCampusTV(id)
{
 
	$.post(
		'ajax_load_campus_tv.php',
		{
			'tvID': id
		},
		function(data){
			$('#TB_ajaxContent').html(data);	
		}
	)		
}

<?
if($msg!="")
	echo "Get_Return_Message(\"$msg\")";
?>
</script>
<div id=tempdiv></div>
<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="GET">
<div id="content" style="padding-left: 10px; width=100%;">
	<div class="navigation_v30 navigation_2">
		<a href="javascript:goBack();"><?=$i_frontpage_menu_home?></a>
		<span><?=$navTitle?></span>
	</div>
	<p class="spacer"></p>
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<? if($flag != FUNCTION_TAG) {?>
		<tr>
			<td>
				<?=$searchOption?>
			</td>
		</tr>
		<? } ?>
		<tr>
			<td>
	
				<div class="reading_board" id="resource_popular_list">
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span><?=$Lang["DigitalArchive"]["List"]?></span></h1>
					</div></div>
					
					<div class="reading_board_left"><div class="reading_board_right">
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<?=$actionBtn?>
							</tr>
						</table> 
						
						<div id="tableContentDiv">
						<?=$table?>
						</div>

					
					</div></div>
					
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
</div>
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="<?=$advanceSearchFlag?>">
<input type='hidden' name='flag' id='flag' value='<?=$flag?>' />


</form>
</div>

<script language="javascript">
	<?=$jsFunction?>
	showHideLayer('advanceSearchFlag', '<?=$advanceSearchFlag?>');
</script>
<iframe id="download_frame" name="download_frame" width="0" height="0" tabindex="-1" style="display:none;"></iframe>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>