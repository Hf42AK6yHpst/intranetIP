<?php
$libfilesystem = new libfilesystem();
$lda = new libdigitalarchive();

$resultCreatedFolder = false;
$FolderPath = $PATH_WRT_ROOT."../intranetdata/digital_archive/admin_doc/";
if (!empty($chosen_file)) {
 	$sql = "SELECT Title, FileFolderName, CONCAT('$FolderPath', FileHashName),SizeInBytes FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID = '$chosen_file'";
	$array = $lda->returnArray($sql);                             
    list($title, $name, $path, $size)=$array[0];
	
	$targetPath = "/digital_archive/";
	# create folder

	$targetFullPath = str_replace('//', '/', $PATH_WRT_ROOT."../intranetdata/".$targetPath);
	
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
	}
	
	chmod($targetFullPath, 0777);
	
	# get file name
	$tempFile = $path;
	
	$origFileName =  $name;
	if (empty($origFileName)) continue;
	
	$timestamp = date('YmdHis');
	
	# encode file name
	$targetFileHashName = "u".$UserID."_".$timestamp;
	$targetFile = $targetFullPath.$targetFileHashName;
   
	# upload file
	
	$uploadResult =  $libfilesystem->lfs_copy($tempFile, $targetFile);
	chmod($targetFile, 0777);
	
	# Extension
	$ext = substr($origFileName, strrpos($origFileName, '.') + 1);
	

	# add record to DB
	$dataAry = array();
	$dataAry['Title'] = $Title;
	$dataAry['Description'] = $Description;
	$dataAry['FileName'] = $origFileName;
	$dataAry['FileHashName'] = $targetFileHashName;
	$dataAry['SourceExtension'] = strtolower($ext);
	$dataAry['SubjectID'] = $SubjectID;
	$dataAry['SizeInBytes'] = $size;
	
	if(isset($RecordID) && $RecordID!="") {	# update
		//$RecordID = $lda->updateResourceRecord($RecordID, $dataAry);
	} else {								# insert
		$RecordID = $lda->insertResourceRecord($dataAry);
	}

	$lda->updateRecordYearRelation($YearID, $RecordID);
	
	# Update Tag relationship
	$lda->Update_Resource_Tag_Relation($Tag, $RecordID);
    
}
?>