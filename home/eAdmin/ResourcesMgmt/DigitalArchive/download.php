<?php
/// Editing by: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();

$FolderPath = $PATH_WRT_ROOT."../intranetdata/digital_archive/";

$FileHashName = $_GET["FileHashName"];		
$sql = "SELECT FileName, CONCAT('$FolderPath', FileHashName) FROM DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD WHERE FileHashName = '{$FileHashName}' AND RecordStatus=1";
list($raw_file_name, $file_abspath) = current($ldb->returnArray($sql));

if(file_exists($file_abspath)) {
	// Check browser agent
	$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
	if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
	elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
	elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
	elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
	else { $agentName = 'unrecognized'; }
	
	// Output file to browser
	$mime_type = mime_content_type($file_abspath);
	$file_content = file_get_contents($file_abspath);
	
	switch ($agentName) {
		default:
		case "msie":
			$file_name = urlencode($raw_file_name);
			break;
		case "mozilla":
			$file_name = $raw_file_name;
			break;
	}
	
	# header to output
	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-type: $mime_type");
	header("Content-Length: ".strlen($file_content));
	header("Content-Disposition: attachment; filename=\"".$file_name."\"");
	
	echo $file_content;
} else {
	header("Location: index.php?msg=FileNotExist");	
}
?>