<?php
/*
#######	Change Log ############################################################
#	Date:	2016-09-19	Omas
#	- add js for reload supplier type
#
#	Date:	2016-07-05	Kenneth
#	- add search function
#
###############################################################################
*/

$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$requiredSymbol = $linterface->RequiredSymbol();
$supplierTypeInfoAryForAdd = $postVar['supplierTypeAryForAdd'];

$supplierTypeSel_addNewSupplier = getSelectByAssoArray($supplierTypeInfoAryForAdd, $tags='name="typeID_addNew" id="typeID_addNew"', $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
if($_PAGE['libPCM']->enableGroupSupplierType()){
// 	$newSupplierTypeBtn = '<a href="#" class="tablelink" onclick="showAddNewSupplierType();">[+]'.'</a>';
	$newSupplierTypeBtn = $linterface->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Case']['Supplier']['NewSupplierType'], 'button', $ParOnClick="showAddNewSupplierType();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
}
?>
<script>
function showAddNewSupplier(){
	$('#addNewSupplierDiv').show('fast');
	$('div.Conntent_tool, div.Conntent_search').hide();
	$('#dbTablediv').hide('fast');
}

function addNewSupplier(){
	$('.warnMsgDiv').hide();
	var canAddSupplier = true;
	//Check form
	var s_name_chi = $('#supplierNameChi_addNew').val();
	var s_name = $('#supplierName_addNew').val();
	var s_type = $('#typeID_addNew option:selected').val();
	var description = $('#description_addNew').val();

	if(!s_name_chi){
		canAddSupplier = false;
		$('#supplierNameChiEmptyWarnDiv').show();
	}
	if(!s_name){
		canAddSupplier = false;
		$('#supplierNameEmptyWarnDiv').show();
	}
	if(!s_type){
		canAddSupplier = false;
        $('#supplierTypeEmptyWarnDiv').show();
	}
	if(canAddSupplier){
		
		$.ajax({
			url: "index.php?p=mgmt.supplier.addByAjax",
			type: "POST",
			data: {
				supplierNameChi: s_name_chi,
				supplierName:  s_name,
				type: s_type,
				description: description
			},
			dataType: "html",
			success: function(response){
				if(response=='SUCCESS'){
					hideAddNewSupplier();
					clearAll_addNew();
					afterAddSupplierJs();
				}
			}
		});
		
// 		hideAddNewSupplier();
	}
}
function hideAddNewSupplier(){
	$('#addNewSupplierDiv').hide('fast');
	$('div.Conntent_tool, div.Conntent_search').show();
	$('#dbTablediv').show('fast');
	clearAll_addNew();
}
function clearAll_addNew(){
	$('.warnMsgDiv').hide();
	$('#supplierNameChi').val('');
	$('#supplierName').val('');
}
function reloadSupplierType(){
	$('#typeTD').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$('#typeDiv').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');

	$.ajax({
		url: "index.php?p=mgmt.reloadSupplierType",
		type: "POST",
		data: {
			mode : "1",
			addBtn : "1",
			  },
		success: function(response){
			$('#typeTD').html(response);
		}
	});

	$.ajax({
		url: "index.php?p=mgmt.reloadSupplierType",
		type: "POST",
		data: {
			mode : "2",
			  },
		success: function(response){
			$('#typeDiv').html(response);
		}
	});
}

</script>
<style>
#addNewSupplierDiv{
	margin: 30px;
}
</style>
<?php echo $htmlAry['sectionNavigation'] = $linterface->GET_NAVIGATION2_IP25($Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier'])?>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><label for="supplierNameChi"><?= $requiredSymbol ?><?= $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name Chi'] ?></label></td>
		<td>
		
		<?= $linterface->GET_TEXTBOX('supplierNameChi_addNew', 'supplierNameChi_addNew', '', $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)) ?>
		<?= $linterface->Get_Form_Warning_Msg('supplierNameChiEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv') ?>
	</td>
	</tr>
	<tr>
		<td class="field_title"><label for="supplierName_addNew"><?= $requiredSymbol ?><?= $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name'] ?></label></td>
		<td>
			
			<?= $linterface->GET_TEXTBOX('supplierName_addNew', 'supplierName_addNew', '', $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)) ?>
			<?=$linterface->Get_Form_Warning_Msg('supplierNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv') ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><label for="description_addNew"><?= $Lang['ePCM']['SettingsArr']['SupplierArr']['Description'] ?></label></td>
		<td>
			<?= $linterface->GET_TEXTAREA('description_addNew', '', $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='') ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><label for="typeID_addNew"><?= $requiredSymbol ?><?= $Lang['ePCM']['SettingsArr']['SupplierArr']['Type']?> </label></td>
		<td id="typeTD">
			
			<?= $supplierTypeSel_addNewSupplier . $newSupplierTypeBtn?>
			<?=$linterface->Get_Form_Warning_Msg('supplierTypeEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose a supplier type'], $Class='warnMsgDiv') ?>
		</td>
	</tr>
</table>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="addNewSupplier();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="") ?>
		<?= $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "hideAddNewSupplier();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn") ?>
	<p class="spacer"></p>
</div>