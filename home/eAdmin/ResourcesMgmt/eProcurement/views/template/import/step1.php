<?php 
extract($_PAGE['views_data']);
$linterface = $_PAGE['libPCM_ui'];

if(isset($navigationAry)){
	$htmlAry['Navigation'] = $linterface->getNavigation_PCM($navigationAry);
}
$htmlAry['StepsBox'] = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');
if(isset($ColumnTitleArr)){
	$htmlAry['FileDescription'] = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
}

switch ($_GET['error']){
	case 1:
	case 2:
		$msg = 'import_failed';
		break;
}
?>
<script>
var backLocation = '<?php echo $js['backLocation']?>';
function checkForm(){
	var canSubmit = true;
	$('form#form1').attr({'action':'<?php echo $js['submitLocation']?>'});
	
	if (document.form1.elements["userfile"].value == "") {
		 alert('<?php echo $Lang['ePCM']['Import']['Error']['NO_FILE']?>');
		 document.form1.elements["userfile"].focus();
		 return false;
	}
	
	return canSubmit;
}
function Load_Reference(ref) {
	$('#remarkDiv_type').html('');
	$.ajax({
        type:"GET",
        url: "index.php?p=setting.import.loadref." + "<?php echo $type ?>&refType="+ref,
        success:function(ReturnData)
        {
        	$('#remarkDiv_type').html(ReturnData);
        }
      })
	displayReference(ref);
}
function displayReference(ref) {
	var p = $("#"+ref);
	var position = p.position();
	
	var t = position.top + 15;
	var l = position.left + p.width()+5;
	$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
}
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}
function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}
</script>
<?php echo $htmlAry['Navigation']?>
<br>
<div>
<table width="100%">
<tr>
<td width="85%"></td>
<td><?php echo $linterface->GET_SYS_MSG($msg);?></td>
</tr>
</table>
</div>
<br>
<?php echo $htmlAry['StepsBox']?>
<br>
<div class="table_board">
	<table  class="form_table_v30" style="width:85%; margin:0 auto;">
		<tr>
			<td class="field_title"><?=$requiredSymbol.$Lang['General']['SourceFile'] ?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat'] ?></span></td>
			<td><input class="file" type="file" name="userfile"></td>
		</tr>
		<tr>
			<td class="field_title"><?=	$Lang['General']['CSVSample'] ?></td>
			<td><?=$sampleLink ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['General']['ImportArr']['DataColumn'] ?></td>
			<td><?=$htmlAry['FileDescription'] ?></td>
		</tr>
	</table>
</div>
<div id="remarkDiv_type" class="selectbox_layer" style="width:400px"></div>