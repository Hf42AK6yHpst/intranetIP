<?php
/*
#######	Change Log ############################################################
#	Date:	2016-09-19	Omas
#	- create this page
#
###############################################################################
*/
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$requiredSymbol = $linterface->RequiredSymbol();

?>
<script>
function showAddNewSupplierType(){
	$('#addNewSupplierTypeDiv').show('fast');
	$('div.Conntent_tool, div.Conntent_search').hide();
	$('#dbTablediv').hide('fast');
	$('#addNewSupplierDiv').hide('fast');
}

function addNewSupplierType(){
	
	$('.warnMsgDiv').hide();
	var canAddSupplier = true;
	//Check form
	var s_name_chi = $('#supplierTypeNameChi_addNew').val();
	var s_name = $('#supplierTypeName_addNew').val();

	if(!s_name_chi){
		canAddSupplier = false;
		$('#supplierTypeNameChiEmptyWarnDiv').show();
	}
	if(!s_name){
		canAddSupplier = false;
		$('#supplierTypeNameEmptyWarnDiv').show();
	}

	if(canAddSupplier){
		
		$.ajax({
			url: "index.php?p=setting.supplierType.update.0.1",
			type: "POST",
			data: {
				TypeName: s_name,
				TypeNameChi:  s_name_chi,
				// from caseDetails
				GroupMappingID : $("#groupID").val(), 
			},
			dataType: "html",
			success: function(response){
				
				if(response=='SUCCESS'){
					hideAddNewSupplierType();
					clearAll_addNewSupplierType();
					reloadSupplierType();
				}
			}
		});
// 		hideAddNewSupplierType();
	}
}
function hideAddNewSupplierType(){
	$('#addNewSupplierTypeDiv').hide('fast');
// 	$('div.Conntent_tool, div.Conntent_search').show();
	$('#dbTablediv').hide('fast');
	$('#addNewSupplierDiv').show('fast');
	clearAll_addNewSupplierType();
}
function clearAll_addNewSupplierType(){
	$('.warnMsgDiv').hide();
	$('#supplierTypeNameChi').val('');
	$('#supplierTypeName').val('');
}
</script>
<style>
#addNewSupplierTypeDiv{
	margin: 30px;
}
</style>
<?php echo $htmlAry['sectionNavigation'] = $linterface->GET_NAVIGATION2_IP25($Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier Type'])?>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><label for="supplierTypeNameChi"><?= $requiredSymbol ?><?= $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'] ?></label></td>
		<td>
		
		<?= $linterface->GET_TEXTBOX('supplierTypeNameChi_addNew', 'supplierTypeNameChi_addNew', '', $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)) ?>
		<?= $linterface->Get_Form_Warning_Msg('supplierTypeNameChiEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'], $Class='warnMsgDiv') ?>
	</td>
	</tr>
	<tr>
		<td class="field_title"><label for="supplierTypeName_addNew"><?= $requiredSymbol ?><?= $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName'] ?></label></td>
		<td>
			
			<?= $linterface->GET_TEXTBOX('supplierTypeName_addNew', 'supplierTypeName_addNew', '', $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)) ?>
			<?=$linterface->Get_Form_Warning_Msg('supplierTypeNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'], $Class='warnMsgDiv') ?>
		</td>
	</tr>
</table>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="addNewSupplierType();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="") ?>
		<?= $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "hideAddNewSupplierType();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn") ?>
	<p class="spacer"></p>
</div>