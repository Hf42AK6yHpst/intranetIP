<?php
$linterface = $_PAGE['libPCM_ui'];
$parData = $_PAGE['views_data'];
$caseInfoAry = $parData['CaseInfoAry'];
$quotations = $parData['Quotations'];
$result = $parData['Result'];
$priceItem = $parData['priceItem'];
$remarks = BuildMultiKeyAssoc($parData['Remarks'], 'StageID');
$caseApprovalInfo = $parData['caseApprovalInfo'];
$custCaseApprovalInfo = $parData['custCaseApprovalInfo'];

$caseApprovalAssoc = BuildMultiKeyAssoc($caseApprovalInfo, 'ApprovalRoleID', array('ApproverName','DateModified'), 0, 1);
$custCaseApprovalAssoc = BuildMultiKeyAssoc($custCaseApprovalInfo, 'ApprovalRoleID', array('ApproverName','DateModified'), 0, 1);

$approvedByInfo['Name'] = '';
$approvedByInfo['Date'] = '';
$endorseddByInfo['Name'] = '';
$endorseddByInfo['Date'] = '';

function getUnderline($length = 12, $string = '')
{
    $fullSpace = '&#09;'; #tab space
    $output = '';
    if ($string != '') {
        $stringLenth = strlen($string);
        //$newLength = ($length - $stringLenth) < 0 ? 1 : $length - $stringLenth;
        $newLength = $length - number_format($stringLenth/4, 0);
        $output = $string;
        for ($x = 0; $x < $newLength; $x+=2) {
            $output = $fullSpace . $output . $fullSpace;
        }
    } else {
        for ($x = 0; $x < $length; $x++) {
            $output .= $fullSpace;
        }
    }
    return '<span class="underline">' . $output . '</span>';
}

function getSpecialSpacing(){
    return '&#09;&#09;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
}

if(false && $caseInfoAry['QuotationType'] == 'T') {
    $html = $_PAGE['Controller']->getViewContent('/template/summary_approval_record_report_doc.php');
    echo $html;
}else{
?>
<html>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'/>
    <style>
        div.Section1 {
            page: Section1;
        }

        @page Section1 {
            margin: 1cm 1.25cm 0cm 2cm;
        }

        body {
            font-size: 11pt;
            font-family: "Times New Roman";
        }

        table.officeuse {
            font-size: 8pt;
            border: 1pt solid black;
            font-family: "Arial Narrow";
        }

        td.title {
            font-size: 13pt;
            text-align: center;
            font-weight: bold;
            text-decoration: underline;
        }

        div.subtitle {
            font-size: 12pt;
            font-weight: bold;
            text-decoration: underline;
            margin-top: 6pt;
            margin-bottom: 6pt;
        }

        table.itemListTable th {
            text-align: center;
            font-weight: normal;
        }

        table.itemListTable td, th {
            border-collapse: collapse;
            border: 1pt solid black;
        }

        table.itemListTable td.noBorder {
            border: none;
        }

        table.itemListTable td.total {
            height: 1cm;
            border: none;
            text-align: right;
            font-weight: bold;
        }

        table.wideRow tr {
            line-height: 150%;
        }

        table.noteTable {
            font-size: 10pt;
            font-family: "Arial Narrow";
        }

        table.noteTable tr {
            line-height: 100%;
        }

        span.narrow {
            font-family: "Arial Narrow";
        }

        span.small {
            font-size: 9pt;
        }

        span.underline {
            text-decoration: underline;
        }

        span.bold {
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="Section1">
    <table>
        <tr>
            <td width="25%"></td>
            <td width="50%" class="title">Maryknoll Fathers’ School</td>
            <td width="25%" rowspan="2">
                <table class="officeuse">
                    <tr>
                        <td>Ref. No.:</td>
                    </tr>
                    <tr>
                        <td>(office use)</td>
                    </tr>
                </table>
            </td>
        <tr>
            <td></td>
            <td class="title">Procurement Confirmation Form</td>
        </tr>
    </table>
    <div class="subtitle">Part A (To be completed before purchasing/placing order)</div>
    <table>
        <tr>
            <td width="1">1.</td>
            <td>Department: <?php echo getUnderline(13, $caseInfoAry['GroupName']) ?></td>
        </tr>
        <tr>
            <td>2.</td>
            <td> Budget Reference Code: <span class="underline"><?php echo getUnderline(20,
                        '<span class="bold">20</span>&nbsp;&nbsp;-&nbsp;&nbsp;&#09;/&#09;') ?></span></td>
        </tr>
    </table>
    <table class="itemListTable">
        <tr>
            <th width="5.84%">Item No.</th>
            <th width="54.08%">Goods or services to be purchased/ordered<br><span class="narrow">(If necessary, use a supplementary sheet and carry total forward.)</span>
            </th>
            <th width="8.95%">Quantity</th>
            <th width="10.12%">Unit Price</th>
            <th width="11.28%">Amount</th>
            <th width="9.73%">Budget<br>Ref.<sup>(Note 5)</sup></th>
        </tr>
        <tr style="line-height: 150%;">
            <td><?php echo $caseInfoAry['Code']?></td>
            <td><?php echo $caseInfoAry['CaseName'].'<br>Description: '.$caseInfoAry['Description']?></td>
            <td></td>
            <td></td>
            <td><?php echo $caseInfoAry['Budget']?></td>
            <td></td>
        </tr>
        <tr>
            <td class="noBorder"></td>
            <td class="noBorder"></td>
            <td class="noBorder"></td>
            <td class="total">Total&nbsp;</td>
            <td><?php echo $caseInfoAry['Budget']?></td>
            <td class="noBorder"></td>
        </tr>
    </table>
    <table>
        <tr style="line-height: 120%">
            <td>3.</td>
            <td>Expenditure covered in approved budget? (<span class="narrow">Please circle.</span>) <span class="bold">Yes / No</span>
            </td>
        </tr>
        <tr style="line-height: 120%">
            <td>4.</td>
            <td>If “No” to the above question, this form should be countersigned by the
                Principal. <?php echo getUnderline(4) ?></td>
        </tr>
    </table>
    <table class="wideRow">
        <tr>
            <td width="2%">&nbsp;</td>
            <td width="67.46%"></td>
            <td width="30.54%"></td>
        </tr>
        <tr>
            <td width="2%">5.</td>
            <td width="67.46%">Proposed by<sup>(Note 6)</sup> <?php echo getUnderline(8) ?></td>
            <td width="30.54%">Date: <?php echo '&#09;'.date('Y-m-d',strtotime($caseInfoAry['DateInput'])).'&#09;' ?></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="small"><?php echo getSpecialSpacing()?>(Name in block letters: <?php echo $caseInfoAry['ApplicantUserNameEn'] ?>)</span>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Endorsed by<sup>(Note 6)</sup><?php echo getUnderline(8) ?></td>
            <td>Date: <?php echo getUnderline(4) ?></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="small"><?php echo getSpecialSpacing()?>(Name in block letters: &#09;&#09;&#09;&#09;&#09;&#09;)</span>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>7.</td>
            <td>Approved by<sup>(Note 6)</sup><?php echo getUnderline(8) ?></td>
            <td>Date: <?php echo getUnderline(4) ?></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="small"><?php echo getSpecialSpacing()?>(Principal / Vice Principal)</span></td>
            <td></td>
        </tr>
    </table>
    <div class="subtitle">Part B (To be completed after purchasing/placing order)</div>
    <table class="wideRow">
        <tr>
            <td width="2%">8.</td>
            <td width="67.46%">Item(s) purchased/ordered by<sup>(Note 7)</sup><?php echo getUnderline(5) ?></td>
            <td width="30.54%">Date: <?php echo getUnderline(4) ?></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="small"><?php echo getSpecialSpacing()?>(Name in block letters: &#09;&#09;&#09;&#09;&#09;&#09;)</span>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>9.</td>
            <td>Item(s) inspected by<sup>(Note 7)</sup><?php echo getUnderline(7) ?></td>
            <td>Date: <?php echo getUnderline(4) ?></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="small"><?php echo getSpecialSpacing()?>(Name in block letters: &#09;&#09;&#09;&#09;&#09;&#09;)</span>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>10.</td>
            <td>Item(s) kept/used by<sup>(Note 7)</sup><?php echo getUnderline(7) ?></td>
            <td>Date: <?php echo getUnderline(4) ?></td>
        </tr>
        <tr>
            <td></td>
            <td><span class="small"><?php echo getSpecialSpacing()?>(Name in block letters: &#09;&#09;&#09;&#09;&#09;&#09;)</span>
            </td>
            <td></td>
        </tr>
    </table>
    <table class="noteTable">
        <tr>
            <td colspan="2">Notes for completing:</td>
        </tr>
        <tr>
            <td width="5%">(1)</td>
            <td>This form serves to ensure that the goods or services rendered or furnished are for the balanced
                benefits of stakeholders of the
                school, and the above information is correct and just.
            </td>
        </tr>
        <tr>
            <td>(2)</td>
            <td>This form should be duly completed for procuring <span class="underline">goods or services of any amount.</span>
            </td>
        </tr>
        <tr>
            <td>(3)</td>
            <td>Items included in this form should be of a similar nature.</td>
        </tr>
        <tr>
            <td>(4)</td>
            <td>This form should be submitted to the school’s account clerk together with receipt(s) and invoice(s)
                within <span class="underline">two months</span> from the date
                of issue of the receipt(s) or date of delivery of goods/services.
            </td>
        </tr>
        <tr>
            <td>(5)</td>
            <td>The Budget Reference is the item number in the Budget Plan approved by the Principal for the school year
                concerned.
            </td>
        </tr>
        <tr>
            <td>(6)</td>
            <td>Procurement must be <span class="underline">proposed [5]</span> and <span
                        class="underline">endorsed [6]</span> by different members of the same department and one of
                them must be the
                department head. If this condition cannot be achieved in practice, the procurement should be endorsed by
                a member of the School
                Finance Management Committee.
            </td>
        </tr>
        <tr>
            <td>(7)</td>
            <td><span class="underline">Purchasing/ordering [8]</span>, <span class="underline">inspecting [9]</span>
                and <span class="underline">keeping/using [10]</span> the item(s) should involve at least two different
                staff members. Each
                task requires only one staff member.
            </td>
        </tr>
    </table>
</div>
</body>
</html>
<?php
}
?>