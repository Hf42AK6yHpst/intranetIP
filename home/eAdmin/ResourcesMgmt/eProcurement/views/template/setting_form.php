<?php
$linterface = $_PAGE['libPCM_ui'];

### action buttons
$htmlAry['editBtn']   = $linterface->Get_Small_Btn($Lang['Btn']['Edit'], "button", "toggleView(2)", "", "", "DisplayView");
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', "style='display: none;'", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn EditView");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "toggleView(1)", 'cancelBtn', "style='display: none;'", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn EditView");
?>
<script>
function toggleView(opt){
	if(opt == 2){
		$('.EditView').show();
		$('.DisplayView').hide();
	}
	else{
		$('.EditView').hide();
		$('.DisplayView').show();
	}	
}

var canSubmit = true;
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    alert(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult == true && formAction != '' ){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}

$(document).ready( function() {
	$('.EditView').hide();
});
</script>
<form name='form1' id='form1' method="POST">
<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $htmlAry['editBtn']?>
	</div>
	<!--FRAME CONTENT-->
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>