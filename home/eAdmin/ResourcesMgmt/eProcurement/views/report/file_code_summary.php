<?php

$linterface = $_PAGE['libPCM_ui'];

//---Group
$group_option='';
foreach((array)$_PAGE['select_group'] as $select_group){
	$group_option.='<option value="'.$select_group['GroupID'].'">'.$select_group['GroupName'].'</option>';
}

$select_group='<select id="select_group" name="select_group[]" multiple="multiple" size="7">' .
		$group_option .
		'</select>';
$selectAllBtn_group=$linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('select_group', 1);");

//---AmountRange
$check_amount_range='';
$i=0;
$check_amount_range.=$linterface->Get_Checkbox('select_all_rule'.$i, 'select_all_rule', $Value='', $isChecked=0, $Class='', $Display='', $Onclick='', $Disabled='');
foreach((array)$_PAGE['select_rule'] as $select_rule){
	$check_amount_range.=$linterface->Get_Checkbox('select_rule_'.$i, 'select_rule[]', $Value=$select_rule['RuleID'], $isChecked=0, $Class='', $Display='$'.number_format($select_rule['FloorPrice'],2).(($select_rule['CeillingPrice']=='')?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($select_rule['CeillingPrice'],2))), $Onclick='', $Disabled='');
	$i++;
}

$checkboxAry = array();
$i=0;
foreach((array)$_PAGE['select_rule'] as $select_rule){
	$checkboxAry[] = array('select_rule_'.$i, 'select_rule[]', $Value=$select_rule['RuleID'], $isChecked=0, $Class='checkboxGroup', $Display='$'.number_format($select_rule['FloorPrice'],2).(($select_rule['CeillingPrice']=='')?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($select_rule['CeillingPrice'],2))));
	$i++;
}
$check_amount_range=$linterface->Get_Checkbox_Table('checkboxGroup', $checkboxAry, $itemPerRow=1,$defaultCheck=false);


$fields=array(
	array(
		'id'=>'DateRange',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Report']['FCS']['DateRange'],
		'control'=>$linterface->GET_DATE_PICKER('DateFrom', date('Y-m-d',getStartOfAcademicYear())).' '.$Lang['ePCM']['Report']['FCS']['to'].' '.$linterface->GET_DATE_PICKER('DateTo', date('Y-m-d',getEndOfAcademicYear())),
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('DateEmptyWarnDiv', $Lang['ePCM']['Report']['FCS']['Please input Date'], $Class='warnMsgDiv'),
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('DateOrderWarnDiv', $Lang['ePCM']['Report']['FCS']['Invalid Date Range'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'Group',
		'label'=>$Lang['ePCM']['Report']['FCS']['Group'],
		'control'=>$linterface->Get_MultipleSelection_And_SelectAll_Div($select_group, $selectAllBtn_group, $SpanID=''),
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('GroupEmptyWarnDiv', $Lang['ePCM']['Report']['FCS']['Please choose a Group'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'AmountRange',
		'label'=>$Lang['ePCM']['Report']['FCS']['AmountRange'],
		'control'=>$check_amount_range,
		'error'=>array(),
	),
	array(
		'id'=>'Status',
		'label'=>$Lang['ePCM']['Report']['FCS']['Status'],
		'control'=>'' .
				$linterface->Get_Checkbox('select_status_1', 'select_status[]', $Value=1, $isChecked=0, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['Application in progress'], $Onclick='', $Disabled='').
				$linterface->Get_Checkbox('select_status_2', 'select_status[]', $Value=2, $isChecked=1, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['Completed'], $Onclick='', $Disabled='').
				$linterface->Get_Checkbox('select_status_3', 'select_status[]', $Value=3, $isChecked=0, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['Rejected'], $Onclick='', $Disabled=''),
		'error'=>array(),
	),
);

$btn_submit=$linterface->Get_Action_Btn($Lang['ePCM']['Report']['FCS']['Get Report'], "button", "", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<script>
$(function(){
	$('#submitBtn').bind('click',function(event){
		event.preventDefault();
		var canSubmit=true;
		
		$('.warnMsgDiv').hide();
		
		//check date
		var date_from=$('#DateFrom').val();
		var date_to=$('#DateTo').val();
		
		if(date_from=='' || date_to==''){
			$('#DateEmptyWarnDiv').show();
			canSubmit=false;
		}else{
			if(date_from>date_to){
				$('#DateOrderWarnDiv').show();
				canSubmit=false;
			}
		}
		
		//check group
		var groups=$('#select_group').val();
		if(groups==null || groups.length<1){
//			$('#GroupEmptyWarnDiv').show();
//			canSubmit=false;
			groups=new Array();
			$('#select_group option').each(function(){
				groups.push($(this).val());
			});
		}
		
		if(canSubmit){
			$('.ajax_loading').show();
		
			//check rule
			var rules=new Array();
			$('input[name*="select_rule"]:checked').each(function(i){
				var rule=$(this).val();
				rules[rules.length]=rule;
			});
			
			//check status
			var status_list=new Array();
			$('input[name*="select_status"]:checked').each(function(i){
				var status=$(this).val();
				status_list[status_list.length]=status;
			});

			var str_groups=groups.join();
			var str_rules=rules.join();
			var str_status_list=status_list.join();

			//post
			$.ajax({
				'type':'POST',
				'url':'index.php?p=report.fcs_ajax',
				'data':{
					'date_from':date_from,
					'date_to':date_to,
					'groups':str_groups,
					'rules':str_rules,
					'status_list':str_status_list,
				},
				'success':function(data){
					if(data=='SUCCESS'){
						window.open('./views/report/file_code_summary_pdf.php?'+Math.random());
						
//						$('div.pdf_place').html('');
//						$('div.pdf_place').html('<div class="div_pdf" style="display:none;"><object class="obj_pdf" data="" type="application/pdf"><embed class="embed_pdf" src="" type="application/pdf" /></object></div>');
//					
//						$('.obj_pdf').css({'width':'100%','height':'1024px'}).attr('data','./views/report/file_code_summary_pdf.php?'+Math.random());
//						$('.embed_pdf').css({'width':'100%','height':'1024px'}).attr('src','./views/report/file_code_summary_pdf.php?'+Math.random());
//						$('.div_pdf').css({'display':'block','width':'100%'});
						
						$('.ajax_loading').hide();
					}
				}
			});
		}
	});
});
</script>
<?php echo $navigation; ?>
<br/>
<table class="form_table_v30">
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr>
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach((array)$field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<br style="clear:both;" />
<?php echo $linterface->MandatoryField(); ?>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $btn_submit;?>
	<div class="ajax_loading" style="display:none;width:100%;text-align:left;"><?php echo $linterface->Get_Ajax_Loading_Image(); ?></div>
	<p class="spacer"></p>
</div>
<div class="pdf_place">
</div>