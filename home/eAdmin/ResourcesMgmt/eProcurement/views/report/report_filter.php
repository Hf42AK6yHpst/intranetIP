<?php

/**************************************************************
 * Using: 
 * 2017-08-11 Simon update ui by adding the order dropdown box.
 * 
 * 2016-11-09	Villa
 *  - hide Application option if Enable Misc Expenditure is close
 * 2016-10-24	Omas
 *  - fix IncludeZeroExpenses checkbox missing label problem 
 * 
 * 2016-10-05 Villa
 *  - add two $fields IncludeZeroExpenses, Application for ber
 *  
 * 2016-09-02 Omas
 * - ajax changed to form submit to new tab directly prevent browser blocked
 * 2016-07-22 Henry HM
 * - Add field include_zero_expenses
 * 
 **************************************************************/

$type = $_PAGE['view_data']['type'];

switch($type){
	case 'pcmrs':
// 		$form_action = 'index.php?p=report.pcmrs_ajax';
		$form_action = 'index.php?p=report.pcmrs_pdf';
		$report_path = 'report.pcmrs_pdf';
		$display_fields=array(
			'AcademicYearID',
			'Category',
			'Group',
			'AmountRange',
			'Status',
			$_SESSION['ePCM']['Setting']['General']['MiscExpenditure']?'Application':'',
			'PrintType',
			'Order'
		);
		break;
	case 'fcs':
// 		$form_action = 'index.php?p=report.fcs_ajax';
		$form_action = 'index.php?p=report.fcs_pdf';
		$report_path = 'report.fcs_pdf';
		$display_fields=array(
			'AcademicYearID',
			'Category',
			'Group',
			'AmountRange',
			'Status',
			'PrintType'
		);
		break;
	case 'ber':
// 		$form_action = 'index.php?p=report.ber_ajax';
		$form_action = 'index.php?p=report.ber_pdf';
		$report_path = 'report.ber_pdf';
		$display_fields=array(
			'AcademicYearID',
			'Category',
			'Group',
			'IncludeZeroExpenses',
			$_SESSION['ePCM']['Setting']['General']['MiscExpenditure']?'Application':'',
			'PrintType'
		);
		break;
}


$linterface = $_PAGE['libPCM_ui'];

//--Category
$category_option='';
foreach((array)$_PAGE['select_category'] as $select_category){
	$category_option.='<option value="'.$select_category['CategoryID'].'">'.$select_category[Get_Lang_Selection('CategoryNameChi','CategoryName')].'</option>';
}

$select_category='<select id="select_category" name="select_category[]" multiple="multiple" size="7">' .
		$category_option .
		'</select>';
$selectAllBtn_category=$linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('select_category', 1);");

//---Group
$group_option='';
foreach((array)$_PAGE['select_group'] as $select_group){
	$group_option.='<option value="'.$select_group['GroupID'].'">'.$select_group[Get_Lang_Selection('GroupNameChi','GroupName')].'</option>';
}

$select_group='<select id="select_group" name="select_group[]" multiple="multiple" size="7">' .
		$group_option .
		'</select>';
$selectAllBtn_group=$linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('select_group', 1);");

//---AmountRange
$check_amount_range='';
// $i=0;
// $check_amount_range.=$linterface->Get_Checkbox('select_all_rule'.$i, 'select_all_rule', $Value='', $isChecked=0, $Class='', $Display='', $Onclick='', $Disabled='');
// foreach((array)$_PAGE['select_rule'] as $select_rule){
// 	$check_amount_range.=$linterface->Get_Checkbox('select_rule_'.$i, 'select_rule[]', $Value=$select_rule['RuleID'], $isChecked=0, $Class='', $Display='$'.number_format($select_rule['FloorPrice'],2).(($select_rule['CeillingPrice']=='')?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($select_rule['CeillingPrice'],2))), $Onclick='', $Disabled='');
// 	$i++;
// }

$checkboxAry = array();
$i=0;
foreach((array)$_PAGE['select_rule'] as $select_rule){
	$checkboxAry[] = array('select_rule_'.$i, 'select_rule[]', $Value=$select_rule['RuleID'], $isChecked=1, $Class='checkboxGroup', $Display='$'.number_format($select_rule['FloorPrice'],2).(($select_rule['CeillingPrice']=='')?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($select_rule['CeillingPrice'],2))));
	$i++;
}
$check_amount_range=$linterface->Get_Checkbox_Table('checkboxGroup', $checkboxAry, $itemPerRow=1,$defaultCheck=true);


$fields=array(
	array(
		'id'=>'AcademicYearID',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Report']['FCS']['Academic Year'],
		'control'=>$linterface->getSelectAcademicYear('AcademicYearID','', 1, 0, $_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID']).'',
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('AcademicYearIDEmptyWarnDiv', $Lang['ePCM']['Report']['FCS']['Please input Academic Year'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'DateRange',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Report']['FCS']['DateRange'],
		'control'=>$linterface->GET_DATE_PICKER('DateFrom', date('Y-m-d',getStartOfAcademicYear())).' '.$Lang['ePCM']['Report']['FCS']['to'].' '.$linterface->GET_DATE_PICKER('DateTo', date('Y-m-d',getEndOfAcademicYear())),
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('DateEmptyWarnDiv', $Lang['ePCM']['Report']['FCS']['Please input Date'], $Class='warnMsgDiv'),
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('DateOrderWarnDiv', $Lang['ePCM']['Report']['FCS']['Invalid Date Range'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'Category',
		'label'=>$Lang['ePCM']['Report']['FCS']['Category'],
		'control'=>$linterface->Get_MultipleSelection_And_SelectAll_Div($select_category, $selectAllBtn_category, $SpanID=''),
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('CategoryEmptyWarnDiv', $Lang['ePCM']['Report']['FCS']['Please choose a Category'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'Group',
		'label'=>$Lang['ePCM']['Report']['FCS']['Group'],
		'control'=>$linterface->Get_MultipleSelection_And_SelectAll_Div($select_group, $selectAllBtn_group, $SpanID=''),
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('GroupEmptyWarnDiv', $Lang['ePCM']['Report']['FCS']['Please choose a Group'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'AmountRange',
		'label'=>$Lang['ePCM']['Report']['FCS']['AmountRange'],
		'control'=>$check_amount_range,
		'error'=>array(),
	),
	array(
		'id'=>'Status',
		'label'=>$Lang['ePCM']['Report']['FCS']['Status'],
		'control'=>'' .
				$linterface->Get_Checkbox('select_status_1', 'select_status[]', $Value=1, $isChecked=0, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['Application in progress'], $Onclick='', $Disabled='').'&nbsp;&nbsp;&nbsp;'.
				$linterface->Get_Checkbox('select_status_2', 'select_status[]', $Value=2, $isChecked=1, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['Completed'], $Onclick='', $Disabled='').'&nbsp;&nbsp;&nbsp;'.
				$linterface->Get_Checkbox('select_status_3', 'select_status[]', $Value=3, $isChecked=0, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['Rejected'], $Onclick='', $Disabled=''),
		'error'=>array(),
	),
	array(
		'id'=>'IncludeZeroExpenses',
		//'label'=>$Lang['ePCM']['Report']['FCS']['IncludeZeroExpenses'],
		'label'=>$Lang['ePCM']['Report']['BER']['Items'],
		'control'=>'' .
				$linterface->Get_Checkbox('include_zero_expenses', 'include_zero_expenses', $Value=1, $isChecked=1, $Class='', $Display=$Lang['ePCM']['Report']['FCS']['IncludeZeroExpenses'], $Onclick='', $Disabled=''),
		'error'=>array(),
	),
	array(
		'id'=>'Application',
		'label'=> $Lang['ePCM']['Report']['BER']['ApplicationType'],
		'control'=>'' .
			$linterface->Get_Checkbox('select_application', 'applicationType[]', $Value='select_application', $isChecked=(	$_SESSION['ePCM']['Setting']['General']['MiscExpenditure']?'1':''), $Class='applicationType', $Display=	$Lang['ePCM']['Report']['BER']['ProcurementRecords'], $Onclick='', $Disabled='').'&nbsp;&nbsp;&nbsp;'
			.$linterface->Get_Checkbox('select_otherApplication', 'applicationType[]', $Value='select_otherApplication', $isChecked=(	$_SESSION['ePCM']['Setting']['General']['MiscExpenditure']?'1':''), $Class='applicationType', $Display=$Lang['ePCM']['Report']['BER']['OtherRecords'], $Onclick='', $Disabled=''),
			'error'=>array(),
	),
	array(
		'id'=>'PrintType',
		'label'=> $Lang['ePCM']['Report']['ReportFormat']['ReportFormat'],
		'control'=>'' .
		'<input type="radio" name="ReportFormat" id="ReportFormat_pdf" value="pdf" checked>'.'<label for="ReportFormat_pdf">'.$Lang['ePCM']['Report']['ReportFormat']['Pdf'].'</label>'.'&nbsp;'.'&nbsp;'
		.'<input type="radio" name="ReportFormat" id="ReportFormat_csv" value="csv">'.'<label for="ReportFormat_csv">'.$Lang['ePCM']['Report']['ReportFormat']['Csv'].'</label>',
		'error'=>array(),
	),
	array(
		'id'=>'Order',
		'label'=> $Lang['ePCM']['Report']['Order'],
		'control'=>
			'<select name="Orders" id="Orders">
				<option value="Code">'.$Lang["ePCM"]["Report"]["Code"].'</option>
				<option value="ApplyDate">'.$Lang["ePCM"]["Report"]["ApplyDate"].'</option>
			</select>' ,
		'error'=>array(),
	),
);

$btn_submit = $linterface->Get_Action_Btn($Lang['ePCM']['Report']['FCS']['Get Report'], "button", "", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<script>
$(function(){
	$('#submitBtn').bind('click',function(event){
		
		event.preventDefault();
		var canSubmit=true;
		
		$('.warnMsgDiv').hide();
		
		//check date
		var date_from=$('#DateFrom').val();
		var date_to=$('#DateTo').val();
		
		if(date_from=='' || date_to==''){
			$('#DateEmptyWarnDiv').show();
			canSubmit=false;
		}else{
			if(date_from>date_to){
				$('#DateOrderWarnDiv').show();
				canSubmit=false;
			}
		}
		
		//check category
		var categories=$('#select_category').val();
		if(categories==null || categories.length<1){
			categories=new Array();
			$('#select_category option').each(function(){
				categories.push($(this).val());
			});
		}
		
		//check group
		var groups=$('#select_group').val();
		if(groups==null || groups.length<1){
//			$('#GroupEmptyWarnDiv').show();
//			canSubmit=false;
			groups=new Array();
			$('#select_group option').each(function(){
				groups.push($(this).val());
			});
		}

		//check application type
		<?php if($_SESSION['ePCM']['Setting']['General']['MiscExpenditure']){?>
			var applicationType = $('.applicationType:checked').val();
			if(!applicationType){
				$('.applicationType').each(function(){
					$(this).attr('checked',true);
				});
			}
		<?php }?>

		
		if(canSubmit){
// 			$('.ajax_loading').show();
		
			//check rule
			var rules=new Array();
			$('input[name*="select_rule"]:checked').each(function(i){
				var rule=$(this).val();
				rules[rules.length]=rule;
			});
			
			//check status
			var status_list=new Array();
			$('input[name*="select_status"]:checked').each(function(i){
				var status=$(this).val();
				status_list[status_list.length]=status;
			});
			

			var str_categories=categories.join();
			var str_groups=groups.join();
			var str_rules=rules.join();
			var str_status_list=status_list.join();

			
			if($('input[name="ReportFormat"]:checked').val() == 'csv'){
				$('form#form1').attr('action','<?php echo str_replace('pdf', 'csv', $form_action)?>').submit();
			}else{
				$('form#form1').attr('action','<?php echo $form_action?>').submit();
			}

			
			//post
// 			$.ajax({
// 				'type':'POST',
//				'url':'<?=$form_action ?>',
// 				'data':{
// 					'academic_year':$('#AcademicYearID').val(),
// 					'date_from':date_from,
// 					'date_to':date_to,
// 					'categories':str_categories,
// 					'groups':str_groups,
// 					'rules':str_rules,
// 					'status_list':str_status_list,
// 					'include_zero_expenses':$('#include_zero_expenses').is(':checked')?'1':'0',
// 				},
// 				'success':function(data){
// 					if(data=='SUCCESS'){
//						window.open('index.php?p=<?php echo $report_path; ?>&'+Math.random());
						
// //						$('div.pdf_place').html('');
// //						$('div.pdf_place').html('<div class="div_pdf" style="display:none;"><object class="obj_pdf" data="" type="application/pdf"><embed class="embed_pdf" src="" type="application/pdf" /></object></div>');
// //					
// //						$('.obj_pdf').css({'width':'100%','height':'1024px'}).attr('data','./views/report/file_code_summary_pdf.php?'+Math.random());
// //						$('.embed_pdf').css({'width':'100%','height':'1024px'}).attr('src','./views/report/file_code_summary_pdf.php?'+Math.random());
// //						$('.div_pdf').css({'display':'block','width':'100%'});
						
// 						$('.ajax_loading').hide();
// 					}
// 				}
// 			});
		}
	});
});

$(function() {
	//select all for select box
	js_Select_All('select_category', 1, '');
	js_Select_All('select_group', 1, '');
});
</script>
<?php echo $navigation; ?>
<br/>
<form name="form1" id="form1" method="POST" target="_blank">
<table class="form_table_v30">
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr style="display:<?php echo in_array($field['id'],$display_fields)?'table-row':'none'; ?>">
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach((array)$field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
</form>
<br style="clear:both;" />
<?php echo $linterface->MandatoryField(); ?>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $btn_submit;?>
	<!-- div class="ajax_loading" style="display:none;width:100%;text-align:left;"><?php echo $linterface->Get_Ajax_Loading_Image(); ?></div-->
	<p class="spacer"></p>
</div>
<div class="pdf_place">
</div>