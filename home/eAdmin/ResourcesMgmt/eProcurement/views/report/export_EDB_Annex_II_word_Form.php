<?php
$postVar= $_PAGE['POST'];
header("Content-type: application/vnd.ms-word");
# replace Wordfile.doc with whatever you want the filename to default to
header('Content-Disposition: attachment;Filename="'.$postVar['codeEdit'].'_'.$postVar['nameEdit'].'_Purchase-by-Oral_Quotation_Form _ANNEX II.doc"');
header("Pragma: no-cache");
header("Expires: 0");

$current_date = date('d-m-Y');
$resultExportAry = $postVar['resultForExportAry'];

$QuotationsAry = array_filter((array)$postVar['QuotationsAry']);
if(!empty($QuotationsAry)){
    $QuotationsAry = $postVar['QuotationsAry'];
} elseif(!empty($resultExportAry)){
    $QuotationsAry = array($resultExportAry,array());
} else{
    $QuotationsAry[0]['Quantity']= 1;
    $QuotationsAry[0]['Price']= $postVar['budget'];
};
count($QuotationsAry)<2? array_push($QuotationsAry, array()):'';

$lang = $postVar['EDB_export_lang'];

//echo "<div style='font-size: 1em; line-height: 1.6em; color: #4E6CA3; padding:10px;' align='right'>Report Date: $current_date</div>";
//echo "<div style='font-size: 1em; line-height: 1.6em; color: #4E6CA3; padding:10px;' align='left'>$heading</div>";
//echo "<div style='font-size: 1em; line-height: 1.6em; color: #4E6CA3; padding:10px;' align='left'>$content</div>";

?>


<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=<?=returnCharset()?>>
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 15">
    <meta name=Originator content="Microsoft Word 15">
    <link rel=File-List
          href="EDB_annex%20II_Oral%20Quotation%20Form.files/filelist.xml">
    <!--[if gte mso 9]><xml>
        <o:DocumentProperties>
            <o:Author>emywychan</o:Author>
            <o:Template>Normal</o:Template>
            <o:LastAuthor>IsaacCheng</o:LastAuthor>
            <o:Revision>2</o:Revision>
            <o:TotalTime>14</o:TotalTime>
            <o:Created>2018-08-20T09:53:00Z</o:Created>
            <o:LastSaved>2018-08-20T09:53:00Z</o:LastSaved>
            <o:Pages>2</o:Pages>
            <o:Words>308</o:Words>
            <o:Characters>1757</o:Characters>
            <o:Lines>14</o:Lines>
            <o:Paragraphs>4</o:Paragraphs>
            <o:CharactersWithSpaces>2061</o:CharactersWithSpaces>
            <o:Version>16.00</o:Version>
        </o:DocumentProperties>
    </xml><![endif]-->
    <link rel=themeData
          href="EDB_annex%20II_Oral%20Quotation%20Form.files/themedata.thmx">
    <link rel=colorSchemeMapping
          href="EDB_annex%20II_Oral%20Quotation%20Form.files/colorschememapping.xml">
    <!--[if gte mso 9]><xml>
        <w:WordDocument>
            <w:SpellingState>Clean</w:SpellingState>
            <w:GrammarState>Clean</w:GrammarState>
            <w:TrackMoves>false</w:TrackMoves>
            <w:TrackFormatting/>
            <w:PunctuationKerning/>
            <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
            <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
            <w:ValidateAgainstSchemas/>
            <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
            <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
            <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
            <w:DoNotPromoteQF/>
            <w:LidThemeOther>EN-US</w:LidThemeOther>
            <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
            <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
            <w:Compatibility>
                <w:SpaceForUL/>
                <w:BalanceSingleByteDoubleByteWidth/>
                <w:DoNotLeaveBackslashAlone/>
                <w:ULTrailSpace/>
                <w:DoNotExpandShiftReturn/>
                <w:AdjustLineHeightInTable/>
                <w:BreakWrappedTables/>
                <w:SnapToGridInCell/>
                <w:WrapTextWithPunct/>
                <w:UseAsianBreakRules/>
                <w:UseWord2010TableStyleRules/>
                <w:DontGrowAutofit/>
                <w:SplitPgBreakAndParaMark/>
                <w:UseFELayout/>
            </w:Compatibility>
            <m:mathPr>
                <m:mathFont m:val="Cambria Math"/>
                <m:brkBin m:val="before"/>
                <m:brkBinSub m:val="&#45;-"/>
                <m:smallFrac m:val="off"/>
                <m:dispDef/>
                <m:lMargin m:val="0"/>
                <m:rMargin m:val="0"/>
                <m:defJc m:val="centerGroup"/>
                <m:wrapIndent m:val="1440"/>
                <m:intLim m:val="subSup"/>
                <m:naryLim m:val="undOvr"/>
            </m:mathPr></w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]><xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
                        DefSemiHidden="false" DefQFormat="false" DefPriority="99"
                        LatentStyleCount="375">
            <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 9"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 1"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 2"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 3"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 4"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 5"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 6"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 7"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 8"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footnote text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="header"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footer"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index heading"/>
            <w:LsdException Locked="false" Priority="35" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="caption"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="table of figures"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="envelope address"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="envelope return"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footnote reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="line number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="page number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="endnote reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="endnote text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="table of authorities"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="macro"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toa heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 5"/>
            <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Closing"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Signature"/>
            <w:LsdException Locked="false" Priority="1" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Default Paragraph Font"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Message Header"/>
            <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Salutation"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Date"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text First Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text First Indent 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Note Heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Block Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Hyperlink"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="FollowedHyperlink"/>
            <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
            <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Document Map"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Plain Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="E-mail Signature"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Top of Form"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Bottom of Form"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal (Web)"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Acronym"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Address"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Cite"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Code"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Definition"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Keyboard"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Preformatted"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Sample"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Typewriter"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Variable"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal Table"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation subject"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="No List"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Contemporary"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Elegant"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Professional"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Subtle 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Subtle 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Balloon Text"/>
            <w:LsdException Locked="false" Priority="0" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Table Grid"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Theme"/>
            <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
            <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
            <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
            <w:LsdException Locked="false" Priority="34" QFormat="true"
                            Name="List Paragraph"/>
            <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
            <w:LsdException Locked="false" Priority="30" QFormat="true"
                            Name="Intense Quote"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="19" QFormat="true"
                            Name="Subtle Emphasis"/>
            <w:LsdException Locked="false" Priority="21" QFormat="true"
                            Name="Intense Emphasis"/>
            <w:LsdException Locked="false" Priority="31" QFormat="true"
                            Name="Subtle Reference"/>
            <w:LsdException Locked="false" Priority="32" QFormat="true"
                            Name="Intense Reference"/>
            <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
            <w:LsdException Locked="false" Priority="37" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Bibliography"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
            <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
            <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
            <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
            <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
            <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
            <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
            <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
            <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
            <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 1"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 2"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 3"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 4"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 5"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 6"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
            <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
            <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 1"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 2"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 3"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 4"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 5"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 6"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Mention"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Smart Hyperlink"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Hashtag"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Unresolved Mention"/>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:Wingdings;
            panose-1:5 0 0 0 0 0 0 0 0 0;
            mso-font-charset:2;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:0 268435456 0 0 -2147483648 0;}
        @font-face
        {font-family:SimSun;
            panose-1:2 1 6 0 3 1 1 1 1 1;
            mso-font-alt:宋体;
            mso-font-charset:134;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:3 680460288 22 0 262145 0;}
        @font-face
        {font-family:新細明體;
            panose-1:2 2 5 0 0 0 0 0 0 0;
            mso-font-alt:PMingLiU;
            mso-font-charset:136;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:細明體;
            panose-1:2 2 5 9 0 0 0 0 0 0;
            mso-font-alt:MingLiU;
            mso-font-charset:136;
            mso-generic-font-family:modern;
            mso-font-pitch:fixed;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:"Cambria Math";
            panose-1:2 4 5 3 5 4 6 3 2 4;
            mso-font-charset:0;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:"\@細明體";
            panose-1:2 2 5 9 0 0 0 0 0 0;
            mso-font-charset:136;
            mso-generic-font-family:modern;
            mso-font-pitch:fixed;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:"\@SimSun";
            panose-1:2 1 6 0 3 1 1 1 1 1;
            mso-font-charset:134;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:3 680460288 22 0 262145 0;}
        @font-face
        {font-family:"\@新細明體";
            panose-1:2 2 5 0 0 0 0 0 0 0;
            mso-font-charset:136;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:"";
            margin:0cm;
            margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:12.0pt;
            mso-bidi-font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        .MsoChpDefault
        {mso-style-type:export-only;
            mso-default-props:yes;
            font-family:"Calibri",sans-serif;
            mso-bidi-font-family:"Times New Roman";
            mso-bidi-theme-font:minor-bidi;}
        /* Page Definitions */
        @page
        {mso-page-border-surround-header:no;
            mso-page-border-surround-footer:no;}
        @page WordSection1
        {size:21.0cm 842.0pt;
            margin:49.65pt 70.9pt 42.55pt 70.9pt;
            mso-header-margin:42.55pt;
            mso-footer-margin:23.8pt;
            mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        @page WordSection2
        {size:595.3pt 841.9pt;
            margin:72.0pt 90.0pt 72.0pt 90.0pt;
            mso-header-margin:42.55pt;
            mso-footer-margin:49.6pt;
            mso-paper-source:0;
            layout-grid:18.0pt;}
        div.WordSection2
        {page:WordSection2;}
        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable
        {mso-style-name:表格內文;
            mso-tstyle-rowband-size:0;
            mso-tstyle-colband-size:0;
            mso-style-noshow:yes;
            mso-style-priority:99;
            mso-style-parent:"";
            mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
            mso-para-margin:0cm;
            mso-para-margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            font-size:12.0pt;
            mso-bidi-font-size:11.0pt;
            font-family:"Calibri",sans-serif;
            mso-ascii-font-family:Calibri;
            mso-ascii-theme-font:minor-latin;
            mso-hansi-font-family:Calibri;
            mso-hansi-theme-font:minor-latin;
            mso-bidi-font-family:"Times New Roman";
            mso-bidi-theme-font:minor-bidi;
            mso-font-kerning:1.0pt;}
        table.MsoTableGrid
        {mso-style-name:表格格線;
            mso-tstyle-rowband-size:0;
            mso-tstyle-colband-size:0;
            mso-style-unhide:no;
            border:solid windowtext 1.0pt;
            mso-border-alt:solid windowtext .5pt;
            mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
            mso-border-insideh:.5pt solid windowtext;
            mso-border-insidev:.5pt solid windowtext;
            mso-para-margin:0cm;
            mso-para-margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:10.0pt;
            font-family:"Times New Roman",serif;
            mso-fareast-font-family:新細明體;}
    </style>
    <![endif]--><!--[if gte mso 9]><xml>
        <o:shapedefaults v:ext="edit" spidmax="1026"/>
    </xml><![endif]--><!--[if gte mso 9]><xml>
        <o:shapelayout v:ext="edit">
            <o:idmap v:ext="edit" data="1"/>
        </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW style='tab-interval:24.0pt;text-justify-trim:punctuation'>
<?php if($lang == 'en'){?>
    <div class=WordSection1>

        <p class=MsoNormal align=right style='text-align:right;line-height:15.0pt;
    mso-line-height-rule:exactly'><b style='mso-bidi-font-weight:normal'><span
                    lang=EN-US style='font-size:13.0pt;font-family:"Times New Roman",serif'>Annex II<o:p></o:p></span></b></p>

        <p class=MsoNormal align=center style='margin-right:1.4pt;text-align:center;
    line-height:normal'><span lang=EN-US style='font-size:13.0pt;font-family:"Times New Roman",serif'>(for
    school's reference only)<o:p></o:p></span></p>

        <p class=MsoNormal style='line-height:17.0pt;mso-line-height-rule:exactly;
    tab-stops:27.0pt 72.0pt'><span lang=EN-US style='font-size:13.0pt;font-family:
    "Times New Roman",serif'>School Ref.:<o:p></o:p></span></p>

        <p class=MsoNormal style='line-height:17.0pt;mso-line-height-rule:exactly;
    tab-stops:27.0pt 72.0pt'><span lang=EN-US style='font-size:13.0pt;font-family:
    "Times New Roman",serif'>To : * Principal/Deputy Head<o:p></o:p></span></p>

        <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
    mso-line-height-rule:exactly;tab-stops:27.0pt 72.0pt'><b style='mso-bidi-font-weight:
    normal'><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman",serif'>Purchase-by-Oral
    Quotation Form<o:p></o:p></span></b></p>

        <p class=MsoNormal align=center style='margin-right:1.4pt;text-align:center;
    line-height:normal'><span lang=EN-US style='font-size:13.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:15.0pt;mso-line-height-rule:exactly'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><span
                    style='mso-tab-count:1'>        </span><span style='mso-tab-count:1'>        </span>I
    have invited the following oral quotations (above $5,000 to $50,000) for supply
    of stores or services.<span style='mso-spacerun:yes'>  </span>After comparing
    the price and quality of the stores or services offered by the suppliers and
    those available on the market, I wish to recommend acceptance of the * lower/higher
    offer from <u>&nbsp;<?=$resultExportAry['SupplierName'] ?>&nbsp;</u>.<span style='mso-spacerun:yes'>
    </span>The reasons for not accepting the lowest offer are as follows :
                <u>&nbsp;<?= (isset($resultExportAry['NotCheapestReason'])&&$resultExportAry['NotCheapestReason']!='')? $postVar['resultForExportAry']['NotCheapestReason'] :  "______________________________________________________________________"; ?></u><o:p></o:p></span></p>
    <?php if(empty($postVar['resultForExportAry']['NotCheapestReason'])){ ?>
        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:15.0pt;mso-line-height-rule:exactly'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'> ______________________________________________________________________________<o:p></o:p></span></p>

        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:15.0pt;mso-line-height-rule:exactly'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>______________________________________________________________________________<o:p></o:p></span></p>
        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:15.0pt;mso-line-height-rule:exactly'><span
                    lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
    <?php } ?>

        <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=640
               style='width:480.3pt;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .75pt;
     mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.75pt solid windowtext;
     mso-border-insidev:.75pt solid windowtext'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                <td width=46 valign=top style='width:34.8pt;border:solid windowtext 1.0pt;
      mso-border-alt:solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Item
      No.<o:p></o:p></span></p>
                </td>
                <td width=147 valign=top style='width:110.6pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Brief
      Description of Item<o:p></o:p></span></p>
                </td>
                <td width=79 valign=top style='width:59.5pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Quantity<o:p></o:p></span></p>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Required<o:p></o:p></span></p>
                </td>
                <td width=155 valign=top style='width:116.15pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Name and
      Phone Number of Supplier<o:p></o:p></span></p>
                </td>
                <td width=80 valign=top style='width:40.6pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Unit
      Price<o:p></o:p></span></p>
                </td>
                <td width=80 valign=top style='width:57.4pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Total<o:p></o:p></span></p>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:58.5pt'><span lang=EN-US
                                                           style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Amount<o:p></o:p></span></p>
                </td>
                <td width=82 valign=top style='width:61.25pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:14.0pt;
      mso-line-height-rule:exactly'><span lang=EN-US style='mso-bidi-font-size:
      12.0pt;font-family:"Times New Roman",serif'>Offer Accepted "&#10003;"</span></p>
                </td>
            </tr>
            <?php $quotationItemNo = 0;
            foreach((array)$QuotationsAry as $quotation){
                $quotationItemNo += 1; ?>
                <tr style='mso-yfti-irow:1'>
                    <td width=46 valign=top style='width:34.8pt;border:solid windowtext 1.0pt;
          border-top:none;mso-border-top-alt:solid windowtext .75pt;mso-border-alt:
          solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
          tab-stops:58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;
          line-height:150%;font-family:"Times New Roman",serif'><o:p><?= $quotationItemNo; ?></o:p></span></p>
                    </td>
                    <td width=147 valign=top style='width:110.6pt;border-top:none;border-left:
          none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal align=center style='text-align:center;tab-stops:58.5pt'><span
                                lang=EN-US style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:
          "Times New Roman",serif'><o:p><?=isset($quotation['Quantity'])?$postVar['nameEdit']:''; ?></o:p><br><o:p><?= isset($quotation['Quantity'])?$postVar['description']:''; ?></o:p></span></p>
                    </td>
                    <td width=79 valign=top style='width:59.5pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='tab-stops:58.5pt'><span lang=EN-US
                                                                          style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'><o:p><?= $quotation['Quantity'] ?></o:p></span></p>
                    </td>
                    <td width=155 valign=top style='width:116.15pt;border-top:none;border-left:
          none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='tab-stops:58.5pt'><span lang=EN-US
                                                                          style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'>(<?= $quotationItemNo; ?>)<o:p><?= $quotation['SupplierName']; ?></o:p><br><o:p><?= $quotation['Phone']; ?></o:p></span></p>
                    </td>
                    <td width=54 valign=top style='width:40.6pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='tab-stops:58.5pt'><span lang=EN-US
                                                                          style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'><o:p><?= $quotation['Accepted']?$resultExportAry['Price']:$quotation['Price']; ?></o:p></span></p>
                    </td>
                    <td width=77 valign=top style='width:57.4pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='tab-stops:58.5pt'><span lang=EN-US
                                                                          style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'><o:p><?= $quotation['Accepted']?$resultExportAry['Price']:$quotation['Price']; ?></o:p></span></p>
                    </td>
                    <td width=82 valign=top style='width:61.25pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal align=center style='text-align:center;line-height:14.0pt;
      mso-line-height-rule:exactly'><span lang=EN-US style='mso-bidi-font-size:12.0pt;
      font-family:"Segoe UI Symbol",sans-serif;mso-bidi-font-family:"Segoe UI Symbol";
      mso-font-kerning:1.0pt'><?= $quotation['Accepted']?'&#10003;':''; ?></span></p>
                    </td>
                </tr>
            <?php } ?>
        </table>

        <p class=MsoNormal style='margin-right:-20.4pt;line-height:12.0pt;mso-line-height-rule:
    exactly'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

        <p class=MsoNormal style='margin-right:-20.4pt;line-height:14.0pt;mso-line-height-rule:
    exactly'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>If
    less than two quotations are invited/received, please provide reasons in the
    box below : <o:p></o:p></span></p>

        <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=640
               style='width:480.3pt;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
     mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                <td width=640 valign=top style='width:480.3pt;border:solid windowtext 1.0pt;
      mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-right:-20.4pt;line-height:14.0pt;mso-line-height-rule:
    exactly'><span lang=EN-US style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
      font-family:"Times New Roman",serif'>Less than two quotations are
      invited/received because <u><?= isset($postVar['supplierLessThenReason'])? $postVar['supplierLessThenReason'] :'___________________________' ?></u><o:p></o:p></span></p>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-20.4pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-20.4pt;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                   style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><span
                                style='mso-spacerun:yes'>                         </span>Endorsed by : __________________
                            (Rank ________/ Post _________)<o:p></o:p></span></p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:14.0pt;mso-line-height-rule:exactly;tab-stops:49.65pt'><b
                style='mso-bidi-font-weight:normal'><span lang=EN-US style='mso-bidi-font-size:
    12.0pt;font-family:"Times New Roman",serif'><span
                        style='mso-spacerun:yes'> </span><span style='mso-tab-count:1'>               </span><o:p></o:p></span></b></p>

        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:49.65pt'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><span
                    style='mso-tab-count:1'>                 </span>As the item is required by
    <?= $postVar['applicantNameEn']? "<u> ".$postVar['applicantNameEn']." </u>":"________________________"?>, I have requested the supplier to deliver the item on
    or before __________________________, if approved.<span
                    style='mso-spacerun:yes'>  </span>Payment may be effected after satisfactory
    receipt of the stores on ________________________.<o:p></o:p></span></p>

        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:49.65pt'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><span
                    style='mso-tab-count:1'>                 </span>I confirm that the above item
    is a single procurement and not part of a large consignment to be procured by
    instalments.<o:p></o:p></span></p>

        <p class=MsoNormal style='margin-right:-20.4pt;text-align:justify;text-justify:
    inter-ideograph;line-height:11.0pt;mso-line-height-rule:exactly;tab-stops:49.65pt'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=646
               style='width:484.65pt;border-collapse:collapse;mso-padding-alt:0cm 5.35pt 0cm 5.35pt'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                <td width=168 valign=top style='width:125.85pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Quotation
      invited by :<o:p></o:p></span></p>
                </td>
                <td width=181 valign=top style='width:136.05pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-14.45pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=24 valign=top style='width:18.0pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-14.45pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=84 valign=top style='width:63.0pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-14.45pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Signature
      :<o:p></o:p></span></p>
                </td>
                <td width=189 valign=top style='width:5.0cm;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:16.7pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
            <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
                <td width=168 valign=top style='width:125.85pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-2.4pt;margin-bottom:
      0cm;margin-left:48.9pt;margin-bottom:.0001pt;mso-para-margin-top:6.0pt;
      mso-para-margin-right:-.2gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      48.9pt;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      text-indent:-48.9pt;line-height:17.0pt;mso-line-height-rule:exactly;
      tab-stops:58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;
      font-family:"Times New Roman",serif'>Rank <span
                                style='mso-spacerun:yes'>                         </span>:<o:p></o:p></span></p>
                </td>
                <td width=181 valign=top style='width:136.05pt;border-top:solid windowtext 1.0pt;
      border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
      mso-border-top-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .75pt;
      padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-14.45pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=24 valign=top style='width:18.0pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-14.45pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=84 valign=top style='width:63.0pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-14.45pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>Date
      <span style='mso-spacerun:yes'>        </span>:<o:p></o:p></span></p>
                </td>
                <td width=189 valign=top style='width:5.0cm;border-top:solid windowtext 1.0pt;
      border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
      mso-border-top-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .75pt;
      padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:16.7pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:
      inter-ideograph;line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:
      58.5pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-top:3.6pt;margin-right:-14.45pt;margin-bottom:
    0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.3gd;mso-para-margin-right:
    -14.45pt;mso-para-margin-bottom:0cm;mso-para-margin-left:0cm;mso-para-margin-bottom:
    .0001pt;text-align:justify;text-justify:inter-ideograph;line-height:normal;
    tab-stops:58.5pt'><b style='mso-bidi-font-weight:normal'><span lang=EN-US
                                                                   style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>-----------------------------------------------------------------------------------------------------------------</span></b><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>

        <p class=MsoNormal style='margin-top:0cm;margin-right:-25.2pt;margin-bottom:
    0cm;margin-left:8.4pt;margin-bottom:.0001pt;text-align:justify;text-justify:
    inter-ideograph;text-indent:-8.4pt;mso-char-indent-count:-.7;line-height:12.0pt'><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'>*<span
                    style='mso-tab-count:1'> </span>I certify that the oral quotation procedures
    are in order and approve the above recommendation; or<o:p></o:p></span></p>

        <p class=MsoNormal style='margin-right:10.5pt;text-align:justify;text-justify:
    inter-ideograph;line-height:12.0pt'><span lang=EN-US style='mso-bidi-font-size:
    12.0pt;font-family:"Times New Roman",serif'>* I consider that re-inviting the
    oral quotation is required; or<b style='mso-bidi-font-weight:normal'><o:p></o:p></b></span></p>

        <p class=MsoNormal style='margin-right:10.5pt;text-align:justify;text-justify:
    inter-ideograph;line-height:12.0pt'><span lang=EN-US style='mso-bidi-font-size:
    12.0pt;font-family:"Times New Roman",serif'>* I disapprove the recommendation
    because <?=$postVar['rejectedReason']? '<u>&nbsp;'.$postVar['rejectedReason'].'&nbsp;</u>':'________________________________' ?>  <o:p></o:p></span></p>

        <p class=MsoNormal style='margin-right:10.5pt;text-align:justify;text-justify:
    inter-ideograph;line-height:12.0pt'><span lang=EN-US style='mso-bidi-font-size:
    12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=403
               style='width:302.3pt;margin-left:182.55pt;border-collapse:collapse;mso-padding-alt:
     0cm 5.35pt 0cm 5.35pt'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                <td width=161 valign=top style='width:120.5pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='tab-stops:27.0pt 72.0pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'>Signature
      of :<o:p></o:p></span></p>
                </td>
                <td width=242 valign=top style='width:181.8pt;border:none;border-bottom:solid windowtext 1.0pt;
      mso-border-bottom-alt:solid windowtext .75pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='line-height:normal;tab-stops:27.0pt 72.0pt'><span
                            lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
            <tr style='mso-yfti-irow:1'>
                <td width=161 valign=top style='width:120.5pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='tab-stops:27.0pt 72.0pt'><span lang=EN-US style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=242 valign=top style='width:181.8pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal align=center style='text-align:center;tab-stops:27.0pt 72.0pt'><span
                            lang=EN-US style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:
      "Times New Roman",serif'>* Principal/Deputy Head<o:p></o:p></span></p>
                </td>
            </tr>
            <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
                <td width=161 valign=top style='width:120.5pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='tab-stops:27.0pt 72.0pt'><span lang=EN-US
                                                                             style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif'>Date
      <span style='mso-spacerun:yes'>            </span>:<o:p></o:p></span></p>
                </td>
                <td width=242 valign=top style='width:181.8pt;border:none;border-bottom:solid windowtext 1.0pt;
      mso-border-bottom-alt:solid windowtext .75pt;padding:0cm 5.35pt 0cm 5.35pt'>
                    <p class=MsoNormal style='line-height:normal;tab-stops:27.0pt 72.0pt'><span
                            lang=EN-US style='mso-bidi-font-size:12.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal><span lang=EN-US style='mso-bidi-font-size:12.0pt;
    line-height:150%;font-family:"Times New Roman",serif;letter-spacing:-.3pt'>*</span><span
                lang=EN-US style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif;
    letter-spacing:-.2pt'>Please delete as appropriate</span><span lang=EN-US
                                                                   style='mso-bidi-font-size:12.0pt;line-height:150%;font-family:"Times New Roman",serif;
    mso-fareast-font-family:SimSun;letter-spacing:-.2pt;mso-fareast-language:ZH-CN'><o:p></o:p></span></p>

    </div>

<?php } if($lang == 'b5'){?>
    <div class=WordSection1>

        <p class=MsoNormal align=right style='text-align:right;tab-stops:9.0cm right 446.55pt'><b
                    style='mso-bidi-font-weight:normal'><span style='mso-ascii-font-family:"Times New Roman";
    letter-spacing:1.5pt'>附件</span></b><b style='mso-bidi-font-weight:normal'><span
                        lang=EN-US style='font-family:"Times New Roman",serif'>II<span
                            style='letter-spacing:1.5pt'><o:p></o:p></span></span></b></p>

        <p class=MsoNormal align=center style='text-align:center;tab-stops:9.0cm right 446.55pt'><span
                    style='font-size:11.0pt;line-height:150%;mso-hansi-font-family:細明體;letter-spacing:
    1.5pt'>（只供學校參考之用）</span><b style='mso-bidi-font-weight:normal'><span
                        lang=EN-US style='font-family:"Times New Roman",serif;letter-spacing:1.5pt'><o:p></o:p></span></b></p>

        <p class=MsoNormal style='line-height:12.0pt;tab-stops:9.0cm right 446.55pt'><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>學校檔號：<span lang=EN-US><span
                            style='mso-tab-count:1'>                         </span><br>
    </span>致：<span lang=EN-US>*</span>校長<span lang=EN-US>/</span>副校長<span
                        lang=EN-US><o:p></o:p></span></span></p>

        <p class=MsoNormal align=center style='margin-top:12.0pt;text-align:center;
    line-height:15.0pt;mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><b
                    style='mso-bidi-font-weight:normal'><span style='mso-bidi-font-size:12.0pt;
    mso-hansi-font-family:細明體;letter-spacing:1.5pt'>按口頭報價購貨表格<span lang=EN-US><o:p></o:p></span></span></b></p>

        <p class=MsoNormal align=center style='margin-bottom:6.0pt;mso-para-margin-bottom:
    .5gd;text-align:center;line-height:15.0pt;mso-line-height-rule:exactly;
    tab-stops:9.0cm right 446.55pt'><span lang=EN-US style='font-size:11.0pt;
    mso-hansi-font-family:細明體;letter-spacing:1.5pt'><span
                        style='mso-spacerun:yes'> </span><o:p></o:p></span></p>

        <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
    line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                    lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><span
                        style='mso-tab-count:1'>     </span></span><span style='font-size:11.0pt;
    letter-spacing:1.5pt'>本校邀請下列供應商，提供物料或服務的口頭報價（</span><span lang=EN-US
                                                              style='font-size:11.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
    新細明體'>5,000</span><span style='font-size:11.0pt;mso-hansi-font-family:細明體;
    letter-spacing:1.0pt'>元以上至</span><span lang=EN-US style='font-size:11.0pt;
    font-family:"Times New Roman",serif;mso-fareast-font-family:新細明體'>50,000</span><span
                    style='font-size:11.0pt;mso-hansi-font-family:細明體;letter-spacing:1.5pt'>元</span><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>），</span><span style='font-size:
    11.0pt;mso-ascii-font-family:"Times New Roman";letter-spacing:1.5pt'>就</span><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>供應商所提供的物料或服務的價錢和質素與市場的供應情況作出適當的比較後，現推薦採納由<u><span
                            lang=EN-US><span style='mso-spacerun:yes'><?=$resultExportAry['SupplierNameChi'] ?> </span></span></u><span
                        lang=EN-US><span style='mso-spacerun:yes'> </span></span>提出的<span lang=EN-US>*</span>較低報價<span
                        lang=EN-US>/</span>較高報價。不採納最低報價的原因是：<u><?= (isset($resultExportAry['NotCheapestReason'])&&$resultExportAry['NotCheapestReason']!='')? $postVar['resultForExportAry']['NotCheapestReason'] : "<span lang=EN-US><span
                                style='mso-tab-count:2'>                                                                                           
                                                
                                             。</span></span>" ?></u>

        <p class=MsoNormal style='line-height:9.0pt;mso-line-height-rule:exactly;
    tab-stops:right 248.1pt left 403.25pt'><span lang=EN-US style='font-size:11.0pt;
    letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>

        <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=616
               style='width:462.1pt;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .75pt;
     mso-padding-alt:0cm 1.4pt 0cm 1.4pt;mso-border-insideh:.75pt solid windowtext;
     mso-border-insidev:.75pt solid windowtext'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                <td width=49 valign=top style='width:36.85pt;border:solid windowtext 1.0pt;
      mso-border-alt:solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>項目<span lang=EN-US><br>
      </span>編號<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>物品說明<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=57 valign=top style='width:42.5pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>所需 數量<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=204 valign=top style='width:153.05pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>供應商名稱及電話號碼<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=80 valign=top style='width:38.3pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>單價<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=80 valign=top style='width:2.0cm;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>總價<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=85 valign=top style='width:63.8pt;border:solid windowtext 1.0pt;
      border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:6.0pt;mso-para-margin-bottom:
      .5gd;text-align:center;line-height:17.0pt;mso-line-height-rule:exactly;
      tab-stops:9.0cm right 446.55pt'><span style='font-size:11.0pt;letter-spacing:
      1.5pt'>採納出價「</span><span lang=EN-US style='font-size:11.0pt;font-family:Wingdings;
      mso-ascii-font-family:細明體;mso-hansi-font-family:"Times New Roman";letter-spacing:
      1.5pt;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                                    style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>ü</span></span><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>」<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
            </tr>
            <?php $quotationItemNo = 0;
            foreach((array)$QuotationsAry as $quotation){
                $quotationItemNo += 1; ?>
                <tr style='mso-yfti-irow:1;height:19.5pt'>
                    <td width=49 valign=top style='width:36.85pt;border:solid windowtext 1.0pt;
          border-top:none;mso-border-top-alt:solid windowtext .75pt;mso-border-alt:
          solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
          margin-bottom:3.0pt;margin-left:0cm;text-align:center;line-height:18.0pt;
          mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                    lang=EN-US style='font-size:11.0pt;'><o:p><?= $quotationItemNo; ?></o:p></span></p>
                    </td>
                    <td width=95 valign=top style='width:70.9pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
          margin-bottom:3.0pt;margin-left:0cm;text-align:center;line-height:18.0pt;
          mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                    lang=EN-US style='font-size:11.0pt;'><o:p><?=isset($quotation['Quantity'])?$postVar['nameEdit']:''; ?></o:p><br><o:p><?= isset($quotation['Quantity'])?$postVar['description']:''; ?></o:p></span></p>
                    </td>
                    <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
          margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                    lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><o:p><?= $quotation['Quantity'] ?></o:p></span></p>
                    </td>
                    <td width=204 style='width:153.05pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal style='margin-top:3.0pt;margin-right:0cm;margin-bottom:
          3.0pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
          tab-stops:9.0cm right 446.55pt'><span lang=EN-US style='font-size:11.0pt;
          line-height:150%;'>(<?= $quotationItemNo; ?>)<o:p><?= $quotation['SupplierNameChi']; ?></o:p><br><o:p><?= $quotation['Phone']; ?></o:p></span></p>
                    </td>
                    <td width=51 valign=top style='width:38.3pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
          margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                    lang=EN-US style='font-size:11.0pt;line-height:150%;'><o:p><?= $quotation['Accepted']?$resultExportAry['Price']:$quotation['Price']; ?></o:p></span></p>
                    </td>
                    <td width=76 valign=top style='width:2.0cm;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
          margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                    lang=EN-US style='font-size:11.0pt;line-height:150%;'><o:p><?= $quotation['Accepted']?$resultExportAry['Price']:$quotation['Price']; ?></o:p></span></p>
                    </td>
                    <td width=85 valign=top style='width:63.8pt;border-top:none;border-left:none;
          border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
          mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
          mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
          padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                        <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
          margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                    lang=EN-US style='font-size:11.0pt;line-height:150%;'><o:p><?= $quotation['Accepted']?'&#10003;':''; ?></o:p></span></p>
                    </td>
                </tr>
            <?php } ?>
        </table>

        <p class=MsoNormal style='margin-top:12.0pt;text-align:justify;text-justify:
    inter-ideograph;line-height:18.0pt;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>如邀請<span lang=EN-US>/</span>收到少於兩名供應商報價，請在方格內加以說明：<span
                        lang=EN-US><o:p></o:p></span></span></p>

        <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=621
               style='width:466.1pt;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
     mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:
     .5pt solid windowtext;mso-border-insidev:.5pt solid windowtext'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
                <td width=621 valign=top style='width:466.1pt;border:solid windowtext 1.0pt;
      mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:right 248.1pt left 403.25pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>邀請<span lang=EN-US>/</span>收到少於兩名供應商報價的原因是：<u><?= isset($postVar['supplierLessThenReason'])? $postVar['supplierLessThenReason'] :"<span
                                        lang=EN-US><span style='mso-tab-count:2'>                       </span><span
                                            style='mso-spacerun:yes'>  </span><span style='mso-tab-count:1'>  </span><span
                                            style='mso-spacerun:yes'> </span></span>" ?></u><span lang=EN-US><span
                                        style='mso-spacerun:yes'> 。 </span><o:p></o:p></span></span></p>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
      6.0pt;margin-left:0cm;mso-para-margin-top:.5gd;mso-para-margin-right:0cm;
      mso-para-margin-bottom:.5gd;mso-para-margin-left:0cm;text-indent:156.25pt;
      mso-char-indent-count:12.5;mso-line-height-alt:0pt;tab-stops:right 248.1pt left 294.0pt 403.25pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>批簽：<u><span lang=EN-US><span
                                            style='mso-spacerun:yes'>          </span></span></u><span lang=EN-US>(</span>職級<u><span
                                        lang=EN-US><span style='mso-spacerun:yes'>        </span></span></u><span
                                    lang=EN-US>/</span>職位<u><span lang=EN-US><span
                                            style='mso-spacerun:yes'>        </span></span></u><span lang=EN-US>)<o:p></o:p></span></span></p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-top:12.0pt;text-align:justify;text-justify:
    inter-ideograph;line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                    lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><span
                        style='mso-tab-count:1'>     </span></span><span style='font-size:11.0pt;
    letter-spacing:1.5pt'>由於上述物品須供應<u><?= $postVar['applicantNameCh']? " ".$postVar['applicantNameCh']." ": "<span lang=EN-US><span
                                style='mso-spacerun:yes'>               </span><span
                                style='mso-spacerun:yes'>          </span><span
                                style='mso-spacerun:yes'>    </span></span>"?></u>使用，因此，如獲批准，本人會要求供應商在<u><span
                            lang=EN-US><span style='mso-spacerun:yes'>  </span><span
                                style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span><span
                                style='mso-spacerun:yes'>  </span><span style='mso-spacerun:yes'>   </span></span></u>年<u><span
                            lang=EN-US><span style='mso-spacerun:yes'>   </span><span
                                style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'>  </span></span></u>月<u><span
                            lang=EN-US><span style='mso-spacerun:yes'>     </span></span></u>日或之前送貨。物品驗收無誤後，即可在<u><span
                            lang=EN-US><span style='mso-spacerun:yes'>    </span><span
                                style='mso-spacerun:yes'>   </span><span style='mso-spacerun:yes'>  </span></span></u>年<u><span
                            lang=EN-US><span style='mso-spacerun:yes'>    </span><span
                                style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span></span></u>月<u><span
                            lang=EN-US><span style='mso-spacerun:yes'>     </span></span></u>日付款。<span
                        lang=EN-US><o:p></o:p></span></span></p>

        <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;
    margin-left:0cm;mso-para-margin-top:.5gd;mso-para-margin-right:0cm;mso-para-margin-bottom:
    .5gd;mso-para-margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
    line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                    lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><span
                        style='mso-tab-count:1'>     </span></span><span style='font-size:11.0pt;
    letter-spacing:1.5pt'>特此聲明，上述物品只屬單一的採購。校方並非把多個部分所組成的物品分單採購。<span lang=EN-US><o:p></o:p></span></span></p>

        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=625
               style='width:468.6pt;border-collapse:collapse;mso-yfti-tbllook:480;mso-padding-alt:
     0cm 5.4pt 0cm 5.4pt'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                <td width=64 valign=top style='width:47.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:0cm right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>報價由<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=19 valign=top style='width:14.15pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-9.5pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-.79gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:0cm right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=158 valign=top style='width:118.35pt;border:none;border-bottom:
      solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=97 valign=top style='width:73.05pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>邀請<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=47 valign=top style='width:35.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>簽署<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=221 valign=top style='width:165.5pt;border:none;border-bottom:solid windowtext 1.0pt;
      mso-border-bottom-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
            <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
                <td width=64 valign=top style='width:47.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>職級 <span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=19 valign=top style='width:14.15pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-9.5pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-.79gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=158 valign=top style='width:118.35pt;border:none;border-bottom:
      solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:
      solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
      0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=97 valign=top style='width:73.05pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
                </td>
                <td width=47 valign=top style='width:35.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
      0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
      mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
      0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
      line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>日期<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
                </td>
                <td width=221 valign=top style='width:165.5pt;border:none;border-bottom:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
      mso-border-bottom-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
      text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
      mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
                </td>
            </tr>
        </table>

        <p class=MsoNormal style='margin-top:12.0pt;margin-right:-.05pt;margin-bottom:
    0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:1.0gd;mso-para-margin-right:
    -.05pt;mso-para-margin-bottom:0cm;mso-para-margin-left:0cm;mso-para-margin-bottom:
    .0001pt;text-align:justify;text-justify:inter-ideograph;line-height:9.0pt;
    mso-line-height-rule:exactly;tab-stops:35.45pt'><span lang=EN-US
                                                          style='font-size:11.0pt;letter-spacing:1.5pt'>------------------------------------------------------------<o:p></o:p></span></p>

        <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
    line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt'><span
                    lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'>*</span><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>本人證實，口頭報價的程序妥當及對於上述推薦給予批准；或<span
                        lang=EN-US><o:p></o:p></span></span></p>

        <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
    line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt'><span
                    lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'>*</span><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>本人認為須重新邀請口頭報價；或<span lang=EN-US><o:p></o:p></span></span></p>

        <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
    line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt'><span
                    lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'>*</span><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>本人不批准上述推薦，原因是<u><?=$postVar['rejectedReason']? $postVar['rejectedReason']:"<span lang=EN-US><span
                                style='mso-spacerun:yes'>               </span><span
                                style='mso-spacerun:yes'>          </span><span
                                style='mso-spacerun:yes'>    </span></span>" ?>。</u><span lang=EN-US><o:p></o:p></span></span></p>

        <p class=MsoNormal align=right style='margin-top:24.0pt;margin-right:0cm;
    margin-bottom:0cm;margin-left:354.4pt;margin-bottom:.0001pt;mso-para-margin-top:
    2.0gd;mso-para-margin-right:0cm;mso-para-margin-bottom:0cm;mso-para-margin-left:
    354.4pt;mso-para-margin-bottom:.0001pt;text-align:right;text-indent:-4.0cm;
    mso-line-height-alt:0pt;tab-stops:212.65pt 9.0cm;word-break:break-all'><span
                    style='font-size:11.0pt;letter-spacing:1.5pt'>簽署：<u><span lang=EN-US><span
                                style='mso-spacerun:yes'>                 </span><o:p></o:p></span></u></span></p>

        <p class=MsoNormal align=right style='margin-left:354.4pt;text-align:right;
    text-indent:-99.25pt;tab-stops:212.65pt 269.35pt;word-break:break-all'><span
                    lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><span
                        style='mso-spacerun:yes'> </span>*</span><span style='font-size:11.0pt;
    line-height:150%;letter-spacing:1.5pt'>校長<span lang=EN-US>/</span>副校長<span
                        lang=EN-US><span style='mso-spacerun:yes'>   </span><o:p></o:p></span></span></p>

        <p class=MsoNormal align=right style='margin-left:9.0cm;text-align:right;
    line-height:15.0pt;mso-line-height-rule:exactly;tab-stops:212.65pt 269.35pt 318.95pt;
    word-break:break-all'><span style='font-size:11.0pt;letter-spacing:1.5pt'>日期：<u><span
                            lang=EN-US><span style='mso-tab-count:1'>   </span><span
                                style='mso-spacerun:yes'>             </span></span></u><span lang=EN-US><o:p></o:p></span></span></p>

        <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly;
    tab-stops:212.65pt 269.35pt 318.95pt'><span lang=EN-US style='font-size:11.0pt;
    letter-spacing:1.5pt'>*</span><span style='font-size:11.0pt;letter-spacing:
    1.5pt'>請刪去不適用者<span lang=EN-US><o:p></o:p></span></span></p>

    </div>

<?php } ?>

</body>

</html>
