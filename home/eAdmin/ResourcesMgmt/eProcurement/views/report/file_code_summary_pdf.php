<?php
$PATH_WRT_ROOT=$_PAGE['PATH_WRT_ROOT'];
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

function print_report_start(&$pdf,$academic_year,$str_rule,$quotation_type){
	global $Lang;
	
	$pdf->WriteHTML('        <div class="title">'.$academic_year.$Lang['ePCM']['Report']['FCS']['AcademicYear'].'</div>');
	$pdf->WriteHTML('        <div class="title">'.$Lang['ePCM']['Report']['FCS']['File Code Summary'].'</div>');
	$pdf->WriteHTML('        <div class="budget">'.$str_rule.'</div>');
	$pdf->WriteHTML('        <table class="data">');
	$pdf->WriteHTML('            <thead>');
	$pdf->WriteHTML('                <tr>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Application Date'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Code'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Name'].'</th>');
	if($quotation_type!='N'){
		$pdf->WriteHTML('                    <th>'.($quotation_type=='T'?$Lang['ePCM']['Report']['FCS']['TStart']:$Lang['ePCM']['Report']['FCS']['QStart']).'</th>');
		$pdf->WriteHTML('                    <th>'.($quotation_type=='T'?$Lang['ePCM']['Report']['FCS']['TEnd']:$Lang['ePCM']['Report']['FCS']['QEnd']).'</th>');
	}
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Applicant'].'</th>');
	//$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Budget'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Remarks'].'</th>');
	$pdf->WriteHTML('                </tr>');
	$pdf->WriteHTML('            </thead>');
	$pdf->WriteHTML('            <tbody>');
}

function print_report_end(&$pdf){
	$pdf->WriteHTML('            </tbody>');
	$pdf->WriteHTML('        </table>');
}

function print_page_break(&$pdf){
	$pdf->WriteHTML('        <pagebreak />');
}

$rows_case=$_PAGE['rows_case'];

$year_from=substr($_PAGE['data']['date_from'],0,4);
$year_to=substr($_PAGE['data']['date_to'],0,4);
$month_day_from=substr($_PAGE['data']['date_from'],4,6);
$month_day_to=substr($_PAGE['data']['date_to'],4,6);
if($month_day_from<'-09-01'){
	$year_from--;
}
if($month_day_to>='-09-01'){
	$year_to++;
}
if($_PAGE['data']['academic_year']!=''){
	$academic_year=date('Y',getStartOfAcademicYear('',$_PAGE['data']['academic_year'])).' - '.date('Y',getEndOfAcademicYear('',$_PAGE['data']['academic_year']));
}else{
	$academic_year=$year_from.' - '.$year_to;
}

//$rows_rule=$db->getRuleInfo($array_rule);
//$str_rule='';
//foreach((array)$rows_rule as $row_rule){
//	$str_rule.='$'.number_format($row_rule['FloorPrice'],2).($row_rule['CeillingPrice']==''?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($row_rule['CeillingPrice'],2))).'<br/>';
//}

//start creating PDF
//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
$margin_left = 5; //mm
$margin_right = 5;
$margin_top = 5;
$margin_bottom = 10;
$margin_header = 0;
$margin_footer = 5;
$orientation='L';
$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation);
$pdf->setHTMLFooter('<div style="width:100%;text-align:center;">{PAGENO} / {nb}</div>');
$pdf->AddPage($orientation);

// Chinese use mingliu 
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

//$pdf->WriteHTML('body{font-family:serif;}');
$pdf->WriteHTML('.nobr{white-space:nowrap;word-break:keep-all;}',1);
$pdf->WriteHTML('.canBr{word-wrap:break-word;}',1);
$pdf->WriteHTML('.title{font-size:24px;margin:auto;width:100%;text-align:center;font-weight:bold;}',1);
$pdf->WriteHTML('.budget{margin:auto;width:100%;text-align:center;}',1);
$pdf->WriteHTML('table.data{border-collapse:collapse;border:1px solid #000000;margin-top:30px;table-layout:fixed;width:100%;}',1);
$pdf->WriteHTML('table.data th{border:1px solid #000000;padding:10px;font-size:14px;}',1);
$pdf->WriteHTML('table.data td{border:1px solid #000000;padding:5px;font-size:11px;}',1);
$pdf->WriteHTML('table.data td.case_no{}',1);
$pdf->WriteHTML('table.data td.amount{text-align:right;}',1);
$pdf->WriteHTML('table.data td.remarks{}',1);
$pdf->WriteHTML('table.data td.applicant{}',1);
$pdf->WriteHTML('table.data td.date{text-align:center;}',1);
$pdf->WriteHTML('table.data td.center{text-align:center;}',1);

$pdf->WriteHTML('<html>');
$pdf->WriteHTML('    <head>');
$pdf->WriteHTML('    </head>');
$pdf->WriteHTML('    <body>');
$current_rule_range='###';
foreach((array)$rows_case as $row_case){
	if($row_case['rule_range']!=$current_rule_range){
		if($current_rule_range!='###'){
			print_report_end($pdf);
			print_page_break($pdf);
		}
		$str_rule='$'.number_format($row_case['FloorPrice'],2).($row_case['CeillingPrice']=='' || $row_case['CeillingPrice']=='0.00'?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($row_case['CeillingPrice'],2))).'<br/>';
		$quotation_type=$row_case['QuotationType'];
		print_report_start($pdf,$academic_year,$str_rule,$quotation_type);
	}
	
	if($row_case['ApplicantType']=='I'){
		$applicant=$row_case['ApplicantUserName'];
	}elseif($row_case['ApplicantType']=='G'){
		$applicant=$row_case[Get_Lang_Selection('GroupNameChi','GroupName')].'&nbsp;('.$row_case['ApplicantUserName'].')';
	}
	
	$remarks = '';
	if($row_case['rejected']=='1'){
		$remarks .= $Lang['ePCM']['Report']['FCS']['Rejected'];
	}else{
		if($row_case['CurrentStage']=='10'){
			$remarks .= $Lang['ePCM']['Report']['FCS']['Completed'];
		}else{
			$remarks .= $Lang['ePCM']['Report']['FCS']['Application in progress'];
		}
	}
	
	$pdf->WriteHTML('                <tr>');
	$pdf->WriteHTML('                    <td class="date nobr" width="8%">'.substr($row_case['DateInput'],0,10).'</td>');
	$pdf->WriteHTML('                    <td class="case_no nobr" width="14%">'.$row_case['Code'].'</td>');
	$pdf->WriteHTML('                    <td class="case_name canBr" width="28%">'.$row_case['CaseName'].'</td>');
	if($quotation_type!='N'){
		$pdf->WriteHTML('                    <td class="qt_start date" width="11%">'.substr($quotation_type=='T'?$row_case['TenderOpeningStartDate']:$row_case['QuotationStartDate'],0,10).'</td>');
		$pdf->WriteHTML('                    <td class="qt_end date" width="11%">'.substr($quotation_type=='T'?$row_case['TenderOpeningEndDate']:$row_case['QuotationEndDate'],0,10).'</td>');
	}
	$pdf->WriteHTML('                    <td class="applicant canBr" width="18%">'.$applicant.'</td>');
	//$pdf->WriteHTML('                    <td class="amount nobr" width="15%">$'.number_format($row_case['Budget'],2).'</td>');
	$pdf->WriteHTML('                    <td class="remarks canBr center" width="10%">'.$remarks.'</td>');
	$pdf->WriteHTML('                </tr>');
	$current_rule_range=$row_case['rule_range'];
}
if(count($rows_case)<1){
	print_report_start($pdf,$academic_year,$str_rule);
	print_report_end($pdf);
	$pdf->WriteHTML('        '.$Lang['ePCM']['Report']['FCS']['No Applications']);
}else{
	print_report_end($pdf);
}
$pdf->WriteHTML('    </body>');
$pdf->WriteHTML('</html>');
	
$pdf->Output('File_Code_Summary_'.date('YmdHis').'.pdf', 'I');

?>