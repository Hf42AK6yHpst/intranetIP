<?php
$linterface = $_PAGE['libPCM_ui'];
unset($_SESSION['ePCM']['session_password']);

$instruction=$linterface->Get_Warning_Message_Box($Lang['ePCM']['Index']['Login']['Authentication'],$Lang['ePCM']['Index']['Login']['Please login with your user password.']);
?>
<script>
function checkForm(){
	$('.Warning').hide();
	if($('input.user_password').val()!=''){
		return true;
	}else{
		$('#WarningPassword').show();
		return false;
	}
}

$(function(){
	$('form#form1').attr({'action':'index.php?p=index.login'});
	
	$('form#form1').bind('submit',function(event){
		event.preventDefault();
		
		$.ajax({
			'type':'POST',
			'url':'index.php?p=index.login.ajax',
			'data':{
				'user_password':$('input.user_password').val()
			},
			'success':function(data){
				//alert(data);
				location.reload();
			}
		});
	});
});
</script>
<link href="<?php echo $PATH_WRT_ROOT; ?>/templates/2009a/css/reading_scheme.css" rel="stylesheet" type="text/css">
<?php echo $instruction; ?>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><label for="password"><span class="tabletextrequire">*</span><?php echo $Lang['ePCM']['Index']['Login']['Password']; ?></label></td>
			<td>
				<input type="password" name="password" id="password" class="user_password" />
				<?php echo $linterface->Get_Form_Warning_Msg('WarningPassword',$Lang['ePCM']['Index']['Login']['InputPassword'],'Warning'); ?>
			</td>
		</tr>
	</tbody>
</table>