<?php
$postVar = $_PAGE['views_data'];
$sql = $postVar['sql'];
$btnAry = $postVar['btnAry'];
$keyword = $postVar['keyword'];
$filter = $postVar['filter'];
$dbTableBtnAry = $postVar['dbTableBtnAry'];
$academicYear = $postVar['academicYear'];

$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];

$columns = array();
$columns[] = array('Code',$Lang['ePCM']['Group']['Code'],'10%');
$columns[] = array('GroupName',$Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'],'30%');
$columns[] = array('ItemCount',$Lang['ePCM']['Report']['BER']['Item'],'20%');
$columns[] = array('BudgetAmount',$Lang['ePCM']['Group']['GroupBudget'].' ('.$academicYear.')','30%');
//$columns[] = array('BudgetRemain',$Lang['ePCM']['Group']['BudgetRemain'] ,'15%');
$columns[] = array('MemberCount',$Lang['ePCM']['Group']['Member'],'10%');
$columnCustArr = array(0,22,0,0,0,0);
$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, true, $columnCustArr);

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);
$htmlAry['hiddenField'] = $hiddenF;


### Buttons
## Button Array
$btnAry = array();
$btnAry[] = array('new', 'javascript: addNew();');
// $btnAry[] = array('import', 'index.php?p=setting.import.step1.group');
$subBtnAry = array();
$subBtnAry[] = array('index.php?p=setting.import.step1.group', $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroup']);
$subBtnAry[] = array('index.php?p=setting.import.step1.groupitem', $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupItem']);
$subBtnAry[] = array('index.php?p=setting.import.step1.groupitembudget', $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupBudget']);
$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
if($plugin['Inventory']){
	$btnAry[] = array('import', 'index.php?p=setting.group.import_eInv',$Lang['ePCM']['Group']['Import']['ImportFromInv']);
}
$subBtnAry = array();
$subBtnAry[] = array("javascript: goExport('index.php?p=setting.export.group')", $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroup']);
$subBtnAry[] = array("javascript: goExport('index.php?p=setting.export.groupitem')", $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupItem']);
$subBtnAry[] = array("javascript: goExport('index.php?p=setting.export.groupitembudget')", $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupBudget']);
$btnAry[] = array('export', 'javascript: void(0);','', $subBtnAry);
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

### DB table action buttons
##dbTable button array
// $dbTableBtnAry[] = array($btnClass, $btnHref, $displayLang);
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

?>

<script>
function addNew(){
	location.href = "index.php?p=setting.group.new";
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?= $Lang['ePCM']['Group']['Warning']['ChooseAGroup'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroup'] ?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.group.edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroup'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.group.delete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}

function goExport(actionUrl){
    $('form#form1').attr('action', actionUrl);
    $('form#form1').submit();
    $('form#form1').attr('action', 'index.php?p=setting.group');
}
</script>
<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
<div class="table_filter">
<?= $filter ?>
</div>
<p class="spacer"></p>
<br style="clear:both;" />
<br style="clear:both;" />
<?=$htmlAry['dbTableActionBtn']?>
<?= $libdbtable?>

<?= $htmlAry['hiddenField']?>
</form>