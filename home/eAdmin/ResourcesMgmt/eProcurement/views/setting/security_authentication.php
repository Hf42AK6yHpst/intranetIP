<?php

$linterface = $_PAGE['libPCM_ui'];

$active_id=$_PAGE['AccessSetting']=='1'?"AccessSettingY":"AccessSettingN";
$label=$Lang['ePCM']['SettingsArr']['SecurityArr']['AuthenticationArr']['SettingTitle'];

$button_yes=$linterface->Get_Radio_Button("AccessSettingY", "AccessSetting", "1", $_PAGE['AccessSetting']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0);
$button_no=$linterface->Get_Radio_Button("AccessSettingN", "AccessSetting", "0", $_PAGE['AccessSetting']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0);

$display_value=$_PAGE['AccessSetting']=='1'?$Lang['General']['Yes']:$Lang['General']['No'];
?>
<script language="javascript" type="text/javascript">
function checkForm(){
	return true;
}
$(document).ready( function() {
	$('form#form1').attr({'action':'index.php?p=setting.securityupdate.authentication'});
});
</script>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><label for="<?php echo $active_id; ?>"><?php echo $label; ?></label></td>
		<td>
			<div class="EditView" style="display:none">
				<?php echo $button_yes; ?>
				<?php echo $button_no; ?>
			</div>
			<div class="DisplayView"><?php echo $display_value; ?></div>
		</td>
	</tr>
</table>