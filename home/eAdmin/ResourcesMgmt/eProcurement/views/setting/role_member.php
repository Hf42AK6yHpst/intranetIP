<?php

echo $_PAGE['libPCM_ui']->Include_Thickbox_JS_CSS();
echo $_PAGE['libPCM_ui']->Include_AutoComplete_JS_CSS();
echo $_PAGE['libPCM_ui']->Include_JS_CSS();

$postVar = $_PAGE['views_data'];
$sql = $postVar['sql'];
$roleInfo = $postVar['roleInfo'];
$roleMemberInfo = $postVar['roleMemberInfo'];
$roleMemberUserIDAry = Get_Array_By_Key($roleMemberInfo,'UserID');
$roleID = $roleInfo['RoleID'];
$roleName = $roleInfo['RoleName'];
$roleNameChi = $roleInfo['RoleNameChi'];
$yearID =$roleMemberInfo[0]['AcademicYearID'];
if(empty($yearID)){$yearID = $_GET['academicYearId'];};

$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];


$columns = array();
$columns[] = array('UserID',$Lang['ePCM']['Setting']['Role']['Member'],'100%');

$_PAGE['libPCM']->isEJ()? $columnEncodingConvert = array(25) : $columnEncodingConvert = ''; #case 25 check if not utf-8 then convert
$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size,1, $columnEncodingConvert);
	
### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);

$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('roleID', 'roleID', $roleID);
foreach($roleMemberUserIDAry as $userID){
	$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('groupMemberUserIDAry[]', 'groupMemberUserIDAry[]', $userID);
}
$htmlAry['hiddenField'] = $hiddenF;


### Buttons
## Button Array
$btnAry[] = array('new', 'javascript: addNew();');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

//## Search Box
//$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

### DB table action buttons
$dbTableBtnAry = array();
//$dbTableBtnAry[] = array('other', 'javascript: goSetAdmin();', $Lang['ePCM']['Group']['SetGroupHead']);
//$dbTableBtnAry[] = array('other', 'javascript: goRemoveAdmin();', $Lang['ePCM']['Group']['RemoveGroupHead']);
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['RoleArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.role&academicYearId='.$yearID.'\';');
// $navigationAry[] = array($roleName);
$navigationAry[] = array($Lang['ePCM']['Setting']['Role']['Member']);
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

?>



<script>
function addNew(){
	//Get thick box ui
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Setting']['Group']['AddMember']?>', 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=600);
}

function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRoleMember'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.roleMemberDelete.<?= $roleID ?>&academicYearId=<?= $yearID ?>');
		$('form#form1').submit();
	}
}

function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function onloadThickBox() {

	$('div#TB_ajaxContent').load(
		//"index.php?p=ajax.new.roleMember", 
		"index.php?p=setting.roleAddMember", 
		{ 
			roleID: <?= $roleID ?>,
            academicYearId: <?= $yearID ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			Init_JQuery_AutoComplete('UserSearch');
		}
	);
}
function Add_All_User(){
//	alert('Add_All_User');
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	// union selected and newly add user list
	if (ClassUser.length > UserSelected.length) {
		var cloneCell = document.getElementById('SelectedUserCell');

		// clone element from avaliable user list
		var cloneFrom = ClassUser.cloneNode(1);
		cloneFrom.setAttribute('id','AddUserID[]');
		cloneFrom.setAttribute('name','AddUserID[]');
		var j=0;
		for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			User = UserSelected.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      cloneFrom.add(elOptNew, cloneFrom.options[j]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      cloneFrom.add(elOptNew, j); // IE only
	    }
			UserSelected.options[i] = null;
			j++;
		}

		cloneCell.replaceChild(cloneFrom,UserSelected);
	}
	else {
		for (var i = (ClassUser.length -1); i >= 0 ; i--) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	// update auto complete parameter list
	Update_Auto_Complete_Extra_Para();

	// empty the avaliable user list
	document.getElementById('AvalUserLayer').innerHTML = '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true"></select>';
	/*for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		User = ClassUser.options[i];
		UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
		ClassUser.options[i] = null;
	}

	Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();*/
	
//	Init_JQuery_AutoComplete('UserSearch');
}
function Add_Selected_Class_User(){
//	alert('Add_Selected_Class_User');
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	//Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
function Remove_Selected_User(){
//	alert('Remove_Selected_User');
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		if (UserSelected.options[i].selected)
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
function Remove_All_User(){
//	alert('Remove_All_User');
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
// 	Init_JQuery_AutoComplete('UserSearch');
}

function Add_Selected_User(UserID, UserName){
//	alert(UserID);
//	alert(UserName);
	UserID = UserID.replace("U","");
//	alert(UserID);
	$('select[name="AddUserID[]"]').append("<option value="+ UserID +">"+ UserName +"</option>");

	//clear search box
	$('#UserSearch').val('');
	Get_User_List();
    Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
function getNotAvalUser(){
	var notavalUser = [];
	$('select[name="AddUserID[]"] option').each(function(){
		notavalUser.push($(this).val());
	});

	$('input[name="groupMemberUserIDAry[]"]').each(function(){
		notavalUser.push($(this).val());
	});
	
	
// 	alert(notavalUser);
	return notavalUser;
}

</script>
<?=$navigation?>
<form id="form1" name="form1" method="POST" action="">
	<br style="clear:both;">
<div class="table_board">
	
	<table class="form_table_v30">
		<tr> 
			<td class="field_title"><?=$Lang['ePCM']['Setting']['Role']['RoleName']?></td>
			<td><?=$roleName?></td>
		</tr>
		<tr> 
			<td class="field_title"><?=$Lang['ePCM']['Setting']['Role']['RoleNameChi']?></td>
			<td><?=$roleNameChi?></td>
		</tr>
	</table>
	
	<div class="table_filter">
	<?= $filter ?>
	</div>
			
	<p class="spacer"></p>
	<br style="clear:both;" />
	<?=$htmlAry['contentTool']?>
	<br style="clear:both;" />
	<?=$htmlAry['dbTableActionBtn']?>
	<?= $libdbtable?>
    <div style="text-align: left;">
        <?= $Lang['ePCM']['Setting']['Role']['DeletedUserLegend']?>
    </div>
	
</div>
<?= $htmlAry['hiddenField']?>
</form>
