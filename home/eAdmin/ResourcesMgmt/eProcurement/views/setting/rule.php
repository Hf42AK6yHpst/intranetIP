<?php
$postVar = $_PAGE['views_data'];
$invalidAry = $postVar['invalidAry'];
// debug_pr($invalidAry);
$ruleInfo = $postVar['ruleInfo'];
$table = $_PAGE['libPCM_ui']->getRulesTable($ruleInfo,$invalidAry);

### Instruction Box
$htmlAry['instructionBox'] = $_PAGE['libPCM_ui']->getRuleInstructionBox();

### Warning Box
$htmlAry['warningBox'] = $_PAGE['libPCM_ui']->getRuleInvalidWarningBox($invalidAry);



### Buttons
$btnAry[] = array('new', 'javascript: addNew();');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB table action buttons
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);
?>
<style>
.common_table_list_v30 tr td.error{
	background-color: #FFE0E1;
}
</style>
<script>

function addNew(){
	location.href = "index.php?p=setting.rule.new";
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseARule'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseNoMoreThanARule'] ?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.rule.edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseARule'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.rule.delete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
</script>
<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	<!-- 
	<div class="table_filter">
		<?= $filter ?>
	</div>
	 -->
	<?=$htmlAry['instructionBox']  ?>
	<?=$htmlAry['warningBox']?>
	<?=$htmlAry['dbTableActionBtn']?>
	
	<?= $table?>
	
	<?= $htmlAry['hiddenField']?>
</form>