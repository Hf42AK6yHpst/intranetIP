<?php
//Editing by 

//debug_pr($_POST);
### db table
$postVar = $_PAGE['views_data'];
$order = $postVar['order'];
$field = $postVar['field'];
$pageNo = $postVar['pageNo'];
$page_size = $postVar['numPerPage'];
$typeID = $postVar['typeID'];
$isActive = $postVar['isActive'];
$sql = $postVar['sql'];
$columns = $postVar['columns'];
$filter = $postVar['filter'];
$btnAry = $postVar['btnAry'];
$dbTableBtnAry = $postVar['dbTableBtnAry'];
$keyword = $postVar['keyword'];

$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($page_size == '') ? 50 : $page_size;

$columns = array();
$columns[] = array('SupplierName',$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'],'30%');
$columns[] = array('Type',$Lang['ePCM']['SettingsArr']['SupplierArr']['Type'],'10%');
$columns[] = array('Description',$Lang['ePCM']['SettingsArr']['SupplierArr']['Description'],'25%');
$columns[] = array('QuoteInvitation',$Lang['ePCM']['SettingsArr']['SupplierArr']['Quote Invitation'],'5%');
$columns[] = array('Quote',$Lang['ePCM']['SettingsArr']['SupplierArr']['Quote'],'5%');
$columns[] = array('BidInvitation',$Lang['ePCM']['SettingsArr']['SupplierArr']['Bid Invitation'],'5%');
$columns[] = array('Bid',$Lang['ePCM']['SettingsArr']['SupplierArr']['Bid'],'5%');
$columns[] = array('Deal',$Lang['ePCM']['SettingsArr']['SupplierArr']['Deal'],'5%');
$columns[] = array('LastDealDate',$Lang['ePCM']['SettingsArr']['SupplierArr']['Last Deal Date'],'10%');

$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size,$checkBox=true, $columnCustArr=array(0,0,18,0,0,0,0,0,22));

//### Filter Post val
//$typeID = $_POST['typeID'];
//$isActive = $_POST['isActive'];

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);

### filter hidden field
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('typeID', 'typeID', $typeID);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('isActive', 'isActive', $isActive);

$htmlAry['hiddenField'] = $hiddenF;



### Buttons
//$subBtnAry = array();
//$subBtnAry[] = array('javascript: addNew();', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier']);
//$subBtnAry[] = array('javascript: addNewType();', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier Type']);
$btnAry[] = array('new', 'javascript: addNew();');
$btnAry[] = array('import', 'index.php?p=setting.supplier.import');
$btnAry[] = array('export', 'javascript:goExport();');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);


### DB table action buttons
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('other', 'javascript: goActivate();', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Activate']);
$dbTableBtnAry[] = array('other', 'javascript: goInactivate();', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Inactivate']);
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

### Supply Type Filter
$supplyTypeInfoAry = $_PAGE['supplyTypeInfoAry'];
$htmlAry['filter'] = getSelectByAssoArray($supplyTypeInfoAry, $tags='name="sel_type" id="sel_type" onChange="selectFilter(this);"', $selected=$typeID, $all=0, $noFirst=0, '- '.$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All Types'].' -', $ParQuoteValue=1);
// by omas 2016-09-19 
// $idArray = Get_Array_By_Key($supplyTypeInfoAry,'TypeID');
// $showTextAry = Get_Array_By_Key($supplyTypeInfoAry,Get_Lang_Selection('TypeNameChi','TypeName'));

// $selectionUI = $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$ParSelectedStr=$typeID, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All Types'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_type');
// $htmlAry['filter'] = $selectionUI.' ';

### isActive Filter
$idArray = array('A','1','0');
$showTextAry = array($Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All'],$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Active'],$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Inactive']);
$selectionUI = $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$ParSelectedStr=$isActive, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang='Active / Inactive', $noFirst=1,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_isActive');
$htmlAry['filter'].=$selectionUI;

?>

<script>
function selectFilter(obj){
	switch(obj.name){
		case 'sel_type':
			$('#typeID').val(obj.value);
			//alert(obj.value);
			form1.submit();
		break;
		case 'sel_isActive':
			$('#isActive').val(obj.value);
			//alert(obj.value);
			form1.submit();
		break;
	}
}
function addNew(){
	location.href = ("index.php?p=setting.supplier.new");
}
function addNewType(){
	location.href = ("index.php?p=setting.supplierType.new");
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose a supplier'];?>');
		return;
	}else if(checked.length!=1){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose not more than one supplier'];?>');
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.supplier.edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose at least one supplier'];?>');
		return;
	}else{
		if(!confirm('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'];?>')){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.supplier.delete');
		$('form#form1').submit();
	}
}
function goActivate(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose at least one supplier'];?>');
		return;
	}else{
		if(!confirm('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Activate?'];?>')){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.supplier.activate');
		$('form#form1').submit();
	}
}
function goInactivate(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose at least one supplier'];?>');
		return;
	}else{
		if(!confirm('<?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Inactivate?'];?>')){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.supplier.inactivate');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function goExport(){
	$('form#form1').attr('action', 'index.php?p=setting.supplier.export');
	$('form#form1').submit();
	$('form#form1').attr('action', 'index.php?p=setting.supplier');
}
function editSupplier(supplierID){
	$('input[name=targetIdAry[]]').each(function(){
		if($(this).val()==supplierID){
			$(this).attr('checked','checked');
		}else{
			$(this).attr('checked','');
		}
	});
	$('form#form1').attr('action', 'index.php?p=setting.supplier.edit');
	$('form#form1').submit();
}
</script>
<form id="form1" name="form1" method="POST" action="index.php?p=setting.supplier">
	<div class="content_top_tool">
		<?php echo $htmlAry['contentTool']?>
		<?php echo $htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
<div class="table_filter">
<?php echo  $htmlAry['filter'] ?>
</div>
<p class="spacer"></p>
<br style="clear:both;" />
<br style="clear:both;" />
		
<?php echo $htmlAry['dbTableActionBtn']?>
<?php echo  $libdbtable ?>
<?php echo  $htmlAry['hiddenField']?>
</form>