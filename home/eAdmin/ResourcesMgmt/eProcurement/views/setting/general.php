<?php
// using by : 
/**.
 * Date: 2016-11-09 Villa
 * -add MiscExpenditure() - hide/show btn
 * Date: 2016-11-03 Villa
 * -add MiscExpenditureApproval
 * 
 * Date: 2016-09-27	Villa
 * -add GroupCode option into the select
 *
 **/
//get ui
$lPCM_ui = $_PAGE['libPCM_ui'];

//debug_pr($_PAGE['views_data']['general_settings']);
$generalSettingsArray = $_PAGE['views_data']['general_settings'];
### construct table content
$x = '';
	
	$x .= '<p class="spacer"></p>';
	$x .= $lPCM_ui->Get_Form_Sub_Title_v30($Lang['ePCM']['GeneralSetting']['ControlArr']['General']);
	
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		//defaultAcademicYearID
		$ePCMAcademicYearID = $generalSettingsArray['defaultAcademicYearID'];
		$ePCMAcademicYearName = getAcademicYearByAcademicYearID($ePCMAcademicYearID);
		$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>'."\r\n";
		$x .= '<td class="DisplayView">'.$ePCMAcademicYearName.'</td>'."\r\n";
		$x .= '<td class="EditView">'."\r\n";
		$x .= $lPCM_ui->getSelectAcademicYear('generalSettings[defaultAcademicYearID]','', 1, 0, $ePCMAcademicYearID)."\r\n";
		$x .= $lPCM_ui->Get_Form_Warning_Msg('defaultAcademicYearIDMissingWarnDiv', $Lang['ePCM']['GeneralSetting']['ControlArr']['Please choose Academic Year'], $Class='warnMsgDiv');
		$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		//caseCodeGeneration
		$caseCodeGenVal = $generalSettingsArray['caseCodeGeneration'];
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['CodeGeneration'].'</td>'."\r\n";
			$x .= '<td class="DisplayView"><span class="spanCaseNumberFormat"></span></td>'."\r\n";
			$x .= '<td class="EditView">';
			for($i=0;$i<6;$i++){
				$x .= '    <select class="CaseNumberFormat">';
				$x .= '        <option value="NotUsing">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['N/A'].'</option>';
				$x .= '        <option value="AcademicYear">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Academic Year'].'</option>';
				$x .= '        <option value="RuleCode">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Rule Code'].'</option>';
				$x .= '        <option value="CategoryCode">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Category Code'].'</option>';
				$x .= '		   <option value="GroupCode">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Group code'].'</option>';//2016-09-27 Villa
				$x .= '        <option value="GroupItemCode">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Financial Item Code'].'</option>';
				$x .= '        <option value="SequenceNumber">'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Sequence Number'].'</option>'; 
				$x .= '    </select>';
			}
			$x .= '    <input type="hidden" name="generalSettings[caseCodeGeneration]" id="generalSettings[caseCodeGeneration]" class="txtCaseNumberFormat" value="'.$caseCodeGenVal.'" >';
			$x .= $lPCM_ui->Get_Form_Warning_Msg('CaseCodeMissingWarnDiv', $Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code must include Sequence Number'], $Class='warnMsgDiv');
			$x .= $lPCM_ui->Get_Form_Warning_Msg('CaseCodeDuplicateWarnDiv', $Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code partials must not be duplicated'], $Class='warnMsgDiv');
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		//caseCodeNumberOfDigit
		$caseCodeNumberOfDigitVal = $generalSettingsArray['caseCodeNumberOfDigit'];
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['CodeNumberOfDigit'].'</td>'."\r\n";
			$x .= '<td class="DisplayView">'.($caseCodeNumberOfDigitVal==''?$Lang['General']['EmptySymbol']:$caseCodeNumberOfDigitVal).'</td>'."\r\n";
			$x .= '<td class="EditView">';
			$x .= '    <select class="" name="generalSettings[caseCodeNumberOfDigit]" id="generalSettings[caseCodeNumberOfDigit]">';
			for($i=4;$i<=10;$i++){
				$x .= '        <option value="'.$i.'" '.($caseCodeNumberOfDigitVal==$i?'selected="selected"':'').'>'.$i.'</option>';
			}
			$x .= '    </select>';
			//$x .= '    <input type="number" name="generalSettings[caseCodeNumberOfDigit]" id="generalSettings[caseCodeNumberOfDigit]" value="'.$caseCodeNumberOfDigitVal.'" >';
			$x .= $lPCM_ui->Get_Form_Warning_Msg('caseCodeNumberOfDigitMissingWarnDiv', $Lang['ePCM']['GeneralSetting']['ControlArr']['Please enter case code sequence digits'], $Class='warnMsgDiv');
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		//Case Approval Method (Removed)

//case Code generation sequence
empty($generalSettingsArray['caseCodeGenerationSequence'])?$caseCodeGenSeqVal='General':$caseCodeGenSeqVal = $generalSettingsArray['caseCodeGenerationSequence'];
$GeneralSelected=$AcademicYearSelected=$RuleCodeSelected=$CategoryCodeSelected=$FinancialItemCodeSelected=$GroupCodeSelected='';
if($caseCodeGenSeqVal=='AcademicYear'){
    $caseCodeGenSeqName='Academic Year';
    $AcademicYearSelected= "selected";
}else if($caseCodeGenSeqVal=='RuleCode'){
    $caseCodeGenSeqName='Rule Code';
    $RuleCodeSelected= "selected";
}else if($caseCodeGenSeqVal=='CategoryCode'){
    $caseCodeGenSeqName='Category Code';
    $CategoryCodeSelected= "selected";
}else if($caseCodeGenSeqVal=='FinancialItemCode'){
    $caseCodeGenSeqName='Financial Item Code';
    $FinancialItemCodeSelected= "selected";
}else if($caseCodeGenSeqVal=='GroupCode'){
    $caseCodeGenSeqName='Group code';
    $GroupCodeSelected= "selected";
}else {
    $caseCodeGenSeqName = 'General';
    $GeneralSelected = "selected";
}
$SeqOptAry=array();
$SeqOptAry['General']='<option value="General" '.$GeneralSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['General'].'</option>';
$SeqOptAry['AcademicYear']='<option value="AcademicYear" '.$AcademicYearSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Academic Year'].'</option>';
$SeqOptAry['RuleCode']='<option value="RuleCode" '.$RuleCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Rule Code'].'</option>';
$SeqOptAry['CategoryCode']='<option value="CategoryCode" '.$CategoryCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Category Code'].'</option>';
$SeqOptAry['GroupCode']='<option value="GroupCode" '.$GroupCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Group code'].'</option>';
$SeqOptAry['FinancialItemCode']='<option value="FinancialItemCode" '.$FinancialItemCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Financial Item Code'].'</option>';
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['CodeGenerationSequence'].'</td>'."\r\n";
$x .= '<td class="DisplayView">'.$Lang['ePCM']['GeneralSetting']['ControlArr']["$caseCodeGenSeqName"].'</td>'."\r\n";
$x .= '<td class="EditView">';
    $x .= '    <select class="CaseSequenceNumberFormat" onchange="CaseNumberSequenceInit(this.value)">';
//    $x .= '        <option value="General" '.$GeneralSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['General'].'</option>';
//    $x .= '        <option value="AcademicYear" '.$AcademicYearSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Academic Year'].'</option>';
//    $x .= '        <option value="RuleCode" '.$RuleCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Rule Code'].'</option>';
//    $x .= '        <option value="CategoryCode" '.$CategoryCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Category Code'].'</option>';
//    $x .= '		   <option value="GroupCode" '.$FinancialItemCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Group code'].'</option>';
//    $x .= '        <option value="FinancialItemCode" '.$GroupCodeSelected.'>'.$Lang['ePCM']['GeneralSetting']['ControlArr']['Financial Item Code'].'</option>';
    $x .= '    </select>';

$x .= '    <input type="hidden" name="generalSettings[caseCodeGenerationSequence]" id="generalSettings[caseCodeGenerationSequence]" class="txtCaseNumberSequence" value="'.$caseCodeGenSeqVal.'" >';

$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";


	$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
	
	$x .= $lPCM_ui->Get_Form_Sub_Title_v30($Lang['ePCM']['GeneralSetting']['ControlArr']['Notification']);
	
	$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		
		//emailNotification
		$button_email_yes=$lPCM_ui->Get_Radio_Button("EmailNotificationY", "generalSettings[emailNotification]", "1", $generalSettingsArray['emailNotification']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0);
		$button_email_no=$lPCM_ui->Get_Radio_Button("EmailNotificationN", "generalSettings[emailNotification]", "0", $generalSettingsArray['emailNotification']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0);
		$email_display_value=$generalSettingsArray['emailNotification']=='1'?$Lang['General']['Yes']:$Lang['General']['No'];
		
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['emailNotification'].'</td>'."\r\n";
			$x .= '<td class="DisplayView">'.$email_display_value.'</td>'."\r\n";
			$x .= '<td class="EditView">';
			$x .= '    '.$button_email_yes.' '.$button_email_no;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		if($_PAGE['allow_send_app']){
			//eClassAppNotification
			$button_app_yes=$lPCM_ui->Get_Radio_Button("eClassAppNotificationY", "generalSettings[eClassAppNotification]", "1", $generalSettingsArray['eClassAppNotification']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0);
			$button_app_no=$lPCM_ui->Get_Radio_Button("eClassAppNotificationN", "generalSettings[eClassAppNotification]", "0", $generalSettingsArray['eClassAppNotification']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0);
			$app_display_value=$generalSettingsArray['eClassAppNotification']=='1'?$Lang['General']['Yes']:$Lang['General']['No'];
			
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['eClassAppNotification'].'</td>'."\r\n";
				$x .= '<td class="DisplayView">'.$app_display_value.'</td>'."\r\n";
				$x .= '<td class="EditView">';
				$x .= '    '.$button_app_yes.' '.$button_app_no;
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
		
		
		$exceed_percentage_array = array(array('',$Lang['ePCM']['GeneralSetting']['ControlArr']['Unlimited']),array('0.00','0% ('.$Lang['ePCM']['GeneralSetting']['ControlArr']['CannotExceed'].')'),array('0.01','1%'),array('0.02','2%'),array('0.03','3%'),array('0.04','4%'),array('0.05','5%'),array('0.06','6%'),array('0.07','7%'),array('0.08','8%'),array('0.09','9%'),array('0.10','10%'),array('0.15','15%'),array('0.20','20%'),array('0.30','30%'),array('0.40','40%'),array('0.50','50%'),array('0.60','60%'),array('0.70','70%'),array('0.80','80%'),array('0.90','90%'),array('1.00','100%'),array('1.50','150%'),array('2.00','200%'));
		$exceed_percentage_selector = getSelectByArray($exceed_percentage_array,'name="generalSettings[exceed_budget_percentage]"',$generalSettingsArray['exceed_budget_percentage'],0,1);
		$exceed_percentage_display_value = ($generalSettingsArray['exceed_budget_percentage']=='')?$Lang['ePCM']['GeneralSetting']['ControlArr']['Unlimited']:(floatval($generalSettingsArray['exceed_budget_percentage'])*100).'%';
		
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['ExceedBudgetPercentage'].'</td>'."\r\n";
			$x .= '<td class="DisplayView">'.$exceed_percentage_display_value.'</td>'."\r\n";
			$x .= '<td class="EditView">';
				$x .= $exceed_percentage_selector;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		
	$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
	$x .= $lPCM_ui->Get_Form_Sub_Title_v30($Lang['ePCM']['ManagementArr']['FinishedViewArr']['OtherApplication']);
	
	$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";

		//MiscExpenditureEnable_display_value
		$button_app_yes=$lPCM_ui->Get_Radio_Button("MiscExpenditureY", "generalSettings[MiscExpenditure]", "1", $generalSettingsArray['MiscExpenditure']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="MiscExpenditure('MiscExpenditureY')",$isDisabled_=0);
		$button_app_no=$lPCM_ui->Get_Radio_Button("MiscExpenditureN", "generalSettings[MiscExpenditure]", "0", $generalSettingsArray['MiscExpenditure']!='1', $Class_="", $Lang['General']['No'], $Onclick_="MiscExpenditure('MiscExpenditureY')",$isDisabled_=0);
		$MiscExpenditureEnable_display_value=$generalSettingsArray['MiscExpenditure']=='1'?$Lang['General']['Yes']:$Lang['General']['No'];
	
		$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['MiscExpenditure'].'</td>'."\r\n";
		$x .= '<td class="DisplayView">'.$MiscExpenditureEnable_display_value.'</td>'."\r\n";
		$x .= '<td class="EditView">';
		$x .= '    '.$button_app_yes.' '.$button_app_no;
		$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
		//MiscExpenditureApproval
		if($generalSettingsArray['MiscExpenditure']==1){
			$display = "";
		}else{
			$display = "style='display:none'";
		}
		$button_app_yes=$lPCM_ui->Get_Radio_Button("MiscExpenditureApprovalY", "generalSettings[MiscExpenditureApprove]", "1", $generalSettingsArray['MiscExpenditureApprove']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0);
		$button_app_no=$lPCM_ui->Get_Radio_Button("MiscExpenditureApprovalN", "generalSettings[MiscExpenditureApprove]", "0", $generalSettingsArray['MiscExpenditureApprove']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0);
		$MiscExpenditureApproval_display_value=$generalSettingsArray['MiscExpenditureApprove']=='1'?$Lang['General']['Yes']:$Lang['General']['No'];
		$x .= '<tr id="MiscExpenditureBtn"'.$display.'>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['ePCM']['GeneralSetting']['MiscExpenditureApproval'].'</td>'."\r\n";
		$x .= '<td class="DisplayView">'.$MiscExpenditureApproval_display_value.'</td>'."\r\n";
		$x .= '<td class="EditView">';
		$x .= '    '.$button_app_yes.' '.$button_app_no;
		$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	

	$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
$htmlAry['formTb'] = $x;


?>
<script>
function checkForm(){
	var canSubmit=true;
	
	$('.warnMsgDiv').hide();
	$('form#form1').attr('action', 'index.php?p=setting.general.update');
	
	//defaultAcademicYearID
	var defaultAcademicYearID=$('#generalSettings\\[defaultAcademicYearID\\]').val();
	if(defaultAcademicYearID==''){
		canSubmit=false;
		$('#defaultAcademicYearIDMissingWarnDiv').show();
	}
	
	//caseCodeNumberOfDigit
	var caseCodeNumberOfDigit=$('#generalSettings\\[caseCodeNumberOfDigit\\]').val();
	if(caseCodeNumberOfDigit=='' || caseCodeNumberOfDigit<'1'){
		canSubmit=false;
		$('#caseCodeNumberOfDigitMissingWarnDiv').show();
	}
	//debugger;
	//Case Code Settings
	var case_code_have_sequence_number=false;
	var case_code_is_not_duplicated=true;
	var case_code_array=new Array();
	$('.CaseNumberFormat').each(function(i){
		var value=$(this).val();
		if(value=='SequenceNumber'){
			case_code_have_sequence_number=true;
		}
		if(value!='NotUsing'){
			if(!case_code_array[value]){
				case_code_array[value]=true;
			}else{
				case_code_is_not_duplicated=false;
			}
		}
	});
	
	
	if(case_code_have_sequence_number && case_code_is_not_duplicated){
	}else{
		if(!case_code_have_sequence_number){
			$('#CaseCodeMissingWarnDiv').show();
		}
		if(!case_code_is_not_duplicated){
			$('#CaseCodeDuplicateWarnDiv').show();
		}
		canSubmit=false;
	}


	
	return canSubmit;
}
function CaseNumberSequenceInit(sequence){
        $('.txtCaseNumberSequence').val(sequence);
}

function init(){
	var display_value='';
    var sequence_display_value='<?php echo $SeqOptAry['General']; ?>';
	if($('.txtCaseNumberFormat').val()==''){
		display_value='<?php echo $Lang['General']['EmptySymbol']; ?>';
	}else{
		var array_format=$('.txtCaseNumberFormat').val().split(';');
		$('.CaseNumberFormat').each(function(i){
			if(array_format[i]!=''){
				var value='NotUsing';
				var format=array_format[i];
				if(format=='FUNCTION::AcademicYear'){
					value='AcademicYear';
					display_value+='(<?php echo $Lang['ePCM']['GeneralSetting']['ControlArr']['Academic Year']; ?>)';
                    sequence_display_value+='<?php echo $SeqOptAry['AcademicYear']; ?>';
				}else if(format=='CODE::Rule'){
					value='RuleCode';
					display_value+='(<?php echo $Lang['ePCM']['GeneralSetting']['ControlArr']['Rule Code']; ?>)';
                    sequence_display_value+='<?php echo $SeqOptAry['RuleCode']; ?>';
				}else if(format=='CODE::Category'){
					value='CategoryCode';
					display_value+='(<?php echo $Lang['ePCM']['GeneralSetting']['ControlArr']['Category Code']; ?>)';
                    sequence_display_value+='<?php echo $SeqOptAry['CategoryCode']; ?>';
				}else if(format=='CODE::GroupItem'){
					value='GroupItemCode';
					display_value+='(<?php echo $Lang['ePCM']['GeneralSetting']['ControlArr']['Financial Item Code']; ?>)';
                    sequence_display_value+='<?php echo $SeqOptAry['FinancialItemCode']; ?>';
				}else if(format=='FUNCTION::Sequence'){
					value='SequenceNumber';
					display_value+='(<?php echo $Lang['ePCM']['GeneralSetting']['ControlArr']['Sequence Number']; ?>)';
				}else if(format=='CODE::GroupCode'){
					value='GroupCode';//2016-09-27 Villa
					display_value+='(<?php echo $Lang['ePCM']['GeneralSetting']['ControlArr']['Group code']; ?>)';
                    sequence_display_value+='<?php echo $SeqOptAry['GroupCode']; ?>';
				}
				$(this).val(value);
			}
		});
	}
	$('.spanCaseNumberFormat').html(display_value);
    $('.CaseSequenceNumberFormat').html(sequence_display_value);
}

function getTimeFromTimePicker(settingsName){
	var hour = pad($('#'+settingsName+'_hour option:selected').val(),2);
	var min = pad($('#'+settingsName+'_min option:selected').val(),2);
	var sec = pad($('#'+settingsName+'_sec option:selected').val(),2);
	var time = hour+':'+min+':'+sec;
	return time;
}
function pad (str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

$(function(){
	$('.CaseNumberFormat').bind('change',function(){
		var format='';
        var sequence_display_value='<?php echo $SeqOptAry['General']; ?>';
		$('.CaseNumberFormat').each(function(){
			var value=$(this).val();
			if(value=='NotUsing'){
				
			}else if(value=='AcademicYear'){
				format+='FUNCTION::AcademicYear;';
                sequence_display_value+='<?php echo $SeqOptAry['AcademicYear']; ?>';
			}else if(value=='RuleCode'){
				format+='CODE::Rule;';
                sequence_display_value+='<?php echo $SeqOptAry['RuleCode']; ?>';
			}else if(value=='CategoryCode'){
				format+='CODE::Category;';
                sequence_display_value+='<?php echo $SeqOptAry['CategoryCode']; ?>';
			}else if(value=='GroupItemCode'){
				format+='CODE::GroupItem;';
                sequence_display_value+='<?php echo $SeqOptAry['FinancialItemCode']; ?>';
			}else if(value=='SequenceNumber'){
				format+='FUNCTION::Sequence;';
			}else if(value=='GroupCode'){//2016-09-27 Villa
				format+='CODE::GroupCode;';
                sequence_display_value+='<?php echo $SeqOptAry['GroupCode']; ?>';
			}
		});
		$('.txtCaseNumberFormat').val(format);
        $('.CaseSequenceNumberFormat').html(sequence_display_value);
	});


	
	init();


});

function MiscExpenditure(buttonID){
	var Button = document.getElementById(buttonID);
	if(Button.checked){
		$('#MiscExpenditureBtn').show();
	}else{
		$('#MiscExpenditureBtn').hide();
	}
	
}
</script>
<?= $htmlAry['formTb'] ?>
<!-- 
<form name="form1" id="form1" method="POST" action="index.php?p=setting.general.update">
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<?= $htmlAry['submitBtn'] ?>
		<?= $htmlAry['cancelBtn'] ?>
		<p class="spacer"></p>
	</div>

</form>
 -->