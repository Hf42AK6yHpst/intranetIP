<?php
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$eInvGroupAry = $postVar['eInvGroupAry'];
$needAddMember = $postVar['needAddMember'];
$needSetGroupAdmin = $postVar['needSetGroupAdmin'];

############################################
#	Start: For EJ Only
############################################
if($_PAGE['libPCM']->isEJ()){
	foreach($eInvGroup as $key => &$value){
		foreach($value as $_key => &$_value){
			$_value = convert2unicode(trim($_value), 1);
		}
	}
}
############################################
#	End: For EJ Only
############################################

$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');

// debug_pr($eInvGroupAry);

$headerAry = array($Lang['ePCM']['Group']['Import']['GroupNameChi'],$Lang['ePCM']['Group']['Import']['GroupNameEng'],$Lang['ePCM']['Group']['Import']['Code']);
if($needAddMember){
	if($needSetGroupAdmin){
		$needSetGroupAdminDesc = $Lang['ePCM']['Group']['Import']['GroupHead'];
	}
	$headerAry[] = $Lang['ePCM']['Group']['Import']['GroupMember'].$needSetGroupAdminDesc;
}
$headerAry[] = $Lang['ePCM']['Group']['Import']['ImportStatus'];

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('', 'needAddMember', $needAddMember);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('', 'needSetGroupAdmin', $needSetGroupAdmin);

$PAGE_NAVIGATION[] = array($Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'],'javascript: window.location=\'index.php?p=setting.group\';');
$PAGE_NAVIGATION[] = array($Lang['ePCM']['Group']['Import']['ImportFromInv']);
$htmlAry['navigation'] = $linterface->getNavigation_PCM($PAGE_NAVIGATION);

$stepAry = array();
$stepAry[] = $Lang['ePCM']['Group']['Import']['Step1'];
$stepAry[] = $Lang['ePCM']['Group']['Import']['Step2'];
$stepAry[] = $Lang['ePCM']['Group']['Import']['Step3'];
$htmlAry['customizedImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=2, $stepAry);

$haveError = false;
?>
<script>
var backLocation = 'index.php?p=setting.group.import_eInv';
function checkForm(){

	$('form#form1').attr('action', 'index.php?p=setting.group.update_step3_import_group_eInv');
	
	
	return true;
}
var canSubmit = true;
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    console.log(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}

function goCancel(){
	if(typeof backLocation != 'undefined'){
		window.location = backLocation;
	}
	else{
		alert('please add a js global var backLocation');
	}
}
</script>
<form name='form1' id='form1' method="POST">
<?=$htmlAry['navigation'] ?>
<?=$htmlAry['customizedImportStepTbl'] ?>
<table width="90%" border="0" cellspacing="0" cellpadding="0" style="margin: auto;border-collapse: collapse;" class="form_table_v30">
	<tr  class="tablebluetop tabletopnolink">
		<?php
			foreach($headerAry as $_header){
				echo '<td>'.$_header.'</td>';
			}
		?>
	</tr>
	<?php
		$counter = 0;
		foreach((array)$eInvGroupAry as $_key => $_group){
			echo $linterface->GET_HIDDEN_INPUT('', 'eInvGroupAry['.$_key.'][AdminGroupID]', $_group['AdminGroupID']);
			echo $linterface->GET_HIDDEN_INPUT('', 'eInvGroupAry['.$_key.'][Code]', $_group['Code']);
			echo $linterface->GET_HIDDEN_INPUT('', 'eInvGroupAry['.$_key.'][NameChi]', $_group['NameChi']);
			echo $linterface->GET_HIDDEN_INPUT('', 'eInvGroupAry['.$_key.'][NameEng]', $_group['NameEng']);
			
			$isUnique = 0;
			$isUnique = $db->checkDuplicate($table='INTRANET_PCM_GROUP',$field='Code',$value=$_group['Code'],$excluded_value='',$having_is_deleted=true,$allow_time_of_duplication=0);
			
			echo '<tr class="tablebluerow'.(((++$counter)%2)+1).' ">';
			echo '<td class="tabletext">'.$_group['NameChi'].'</td>';
			echo '<td class="tabletext">'.$_group['NameEng'].'</td>';
			echo '<td class="tabletext">'.$_group['Code'].'</td>';
			if($needAddMember){
				echo '<td class="tabletext">';
					foreach((array)$_group['UserAry'] as $_key2 => $_user){
						echo $linterface->GET_HIDDEN_INPUT('', 'eInvGroupAry['.$_key.'][UserAry]['.$_key2.'][UserID]', $_user['UserID']);
						echo Get_Lang_Selection($_user['ChineseName'],$_user['EnglishName']);
						if($needSetGroupAdmin){
							echo $linterface->GET_HIDDEN_INPUT('', 'eInvGroupAry['.$_key.'][UserAry]['.$_key2.'][RecordType]', $_user['RecordType']);
							if($_user['RecordType']==1){
								echo '(#)';
							}
						}
						echo '<br>';
					}
				echo '</td>';
			}
			echo '<td>';
			if(!$isUnique){
				echo '<span class="tabletextrequire">'.$Lang['ePCM']['Group']['Import']['Error']['CodeExist'].'</span>';
				$haveError = true;
			}
			echo '</td>';
			echo '<tr>';
		}
	?>
	
</table>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	
	<?=($haveError)?'':$htmlAry['submitBtn']?>
	<?=$htmlAry['cancelBtn']?>
	<p class="spacer"></p>
</div>
<?= $hiddenF ?>

</form>
