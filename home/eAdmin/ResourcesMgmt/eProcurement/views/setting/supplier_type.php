<?php
//Editing by 

//debug_pr($_POST);
### db table
$postVar = $_PAGE['views_data'];
$order = $postVar['order'];
$field = $postVar['field'];
$pageNo = $postVar['pageNo'];
$page_size = $postVar['numPerPage'];
$sql = $postVar['sql'];
$columns = $postVar['columns'];
$btnAry = $postVar['btnAry'];
$dbTableBtnAry = $postVar['dbTableBtnAry'];
$keyword = $postVar['keyword'];

$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($page_size == '') ? 50 : $page_size;

$columns = array();
$columns[] = array('TypeName',$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TabTitle'],'100%');

$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size,$checkBox=true, $columnCustArr=array(0));

//### Filter Post val
//$typeID = $_POST['typeID'];
//$isActive = $_POST['isActive'];

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);

$htmlAry['hiddenField'] = $hiddenF;

### Buttons
$btnAry[] = array('new', 'javascript: addNewType();');
$btnAry[] = array('import', 'index.php?p=setting.supplierType.import');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);


### DB table action buttons
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

?>

<script>
function addNewType(){
	location.href = ("index.php?p=setting.supplierType.new");
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose a supplier type'];?>');
		return;
	}else if(checked.length!=1){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose not more than one supplier type'];?>');
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.supplierType.edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert('<?php echo $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose at least one supplier type'];?>');
		return;
	}else{
		if(!confirm('<?php echo $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Confrim Delete?'];?>')){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.supplierType.delete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function editType(typeID){
	$('input[name=targetIdAry[]]').each(function(){
		if($(this).val()==typeID){
			$(this).attr('checked','checked');
		}else{
			$(this).attr('checked','');
		}
	});
	$('form#form1').attr('action', 'index.php?p=setting.supplierType.edit');
	$('form#form1').submit();
}
</script>
<form id="form1" name="form1" method="POST" action="index.php?p=setting.supplierType">
	<div class="content_top_tool">
		<?php echo $htmlAry['contentTool']?>
		<?php echo $htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
<div class="table_filter">
</div>
<p class="spacer"></p>
<br style="clear:both;" />
<br style="clear:both;" />
		
<?php echo $htmlAry['dbTableActionBtn']?>
<?php echo  $libdbtable ?>
<?php echo  $htmlAry['hiddenField']?>
</form>