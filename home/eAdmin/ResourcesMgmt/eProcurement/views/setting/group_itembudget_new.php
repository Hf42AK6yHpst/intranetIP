<?php
$row_group_budget = $_PAGE['rows_group_budget'][0];

// build funding source arr
$fundingSourceAssoc = array();
if(!empty($_PAGE['fundingSource'])){
	foreach($_PAGE['fundingSource'] as $_fundingCatInfoArr){
		$_fundingCatName = Get_Lang_Selection($_fundingCatInfoArr['FundingCategoryNameCHI'],$_fundingCatInfoArr['FundingCategoryNameEN']);
		$_fundingArr = $_fundingCatInfoArr['Funding'];
		if(!empty($_fundingArr)){
			foreach($_fundingArr as $__fundingId => $__fundingInfo){
				$fundingSourceAssoc[$_fundingCatName][$__fundingId] = Get_Lang_Selection($__fundingInfo['FundingNameCHI'],$__fundingInfo['FundingNameEN']);;
			}
		}
	}
}

$budget_value=array();
$item_budget_id_value=array();
foreach((array)$_PAGE['rows_group_item_budget'] as $row_group_item_budget){
	$_itemId = $row_group_item_budget['ItemID'];
	$budget_value[$_itemId] = $row_group_item_budget['Budget'];
	$item_budget_id_value[$_itemId] = $row_group_item_budget['ItemBudgetID'];
	$funding_value[$_itemId] = $row_group_item_budget['FundingID'];
}
unset($_itemId);

$linterface = $_PAGE['libPCM_ui'];
$fields=array(
	array(
		'id'=>'AcademicYearID',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Group']['BudgetArr']['Academic Year'],
		'control'=>$linterface->getSelectAcademicYear('generalSettings[defaultAcademicYearID]','', 1, 0, $ePCMAcademicYearID=$_PAGE['view_data']['academicYearID']).' <span class="ajax_loading" style="display:none;">'.$linterface->Get_Ajax_Loading_Image().'</span>',
		'display'=>$value['AcademicYearID'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('AcademicYearIDEmptyWarnDiv', $Lang['ePCM']['Group']['BudgetArr']['Please input Academic Year'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'gBudget',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Group']['GroupBudget'],
		'control'=>$linterface->GET_TEXTBOX('gBudget', 'gBudget', $row_group_budget['Budget'], $OtherClass='requiredField amount_money', $OtherPar=array('maxlength'=>255)),
		'display'=>$value['gBudget'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('CodeEmptyWarnDiv', 'Please input Group Budget', $Class='warnMsgDiv'),
			'ValidatePositiveAmountWarnDiv'=>$linterface->Get_Form_Warning_Msg('gBudgetValidatePositiveAmountWarnDiv', $Lang['ePCM']['Group']['BudgetArr']['Budget must be positive'], $Class='warnMsgDiv')
		),
	),
);

//Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('GroupBudgetID', 'GroupBudgetID', $row_group_budget['GroupBudgetID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('GroupID', 'GroupID', $_PAGE['mode']=='edit'?$row_group_budget['GroupID']:$_PAGE['GroupID']);

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.group\';');
//$navigationAry[] = array($_PAGE['rows_group'][0]['GroupName']);
$navigationAry[] = array($Lang['ePCM']['Group']['BudgetArr']['ControlArr']['Edit Budget']);
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

?>
<script language="javascript" type="text/javascript">
var backLocation='index.php?p=setting.group';

function checkForm(){	
	var canSubmit = true;
	var isFocused = false;
	
	$('div[id*=\'WarnDiv\']').hide();
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (_elementId!='' && isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
		if(_elementId==''){
			var _elementId = $(this).attr('id');
		}
	});
	
	//defaultAcademicYearID
	var defaultAcademicYearID=$('#generalSettings\\[defaultAcademicYearID\\]').val();
	if(defaultAcademicYearID==''){
		canSubmit=false;
		$('#AcademicYearIDEmptyWarnDiv').show();
	}
	
	//amount_money
	$('.amount_money').each(function(){
		var _elementId = $(this).attr('id');
        var test_value = parseFloat($(this).val().replace(new RegExp(',','g'),''));
        if (test_value != '' && test_value < 0) {
			canSubmit = false;
			if(_elementId!=''){
				$('div#' + _elementId + 'ValidatePositiveAmountWarnDiv').show();
			}else{
				$(this).closest('td').find('.ValidatePositiveAmountWarnDiv').show();
			}
        }
	});
	
	return canSubmit;
}

function calculateGroupItemBudgetSum(){
	var sum=0;
	$('input.GroupItemBudget').each(function(){
		sum+=parseFloat($(this).val().replace(new RegExp(',','g'),''));
	});
	var group_budget=parseFloat($('#gBudget').val().replace(new RegExp(',','g'),''));
	var final_value=number_format(sum,2);

	if(sum>group_budget){
		final_value='<span style="color:red;">'+final_value+'</span>';
	}
	$('span#group_item_budget_sum').html(final_value);
}

function init(){
	$('.amount_money').trigger('change');
	calculateGroupItemBudgetSum();
}

$(document).ready( function() {
	
	<?php if($_PAGE['mode']=='new'){ ?>
		$('form#form1').attr({'action':'index.php?p=setting.group.budget.new'});
	<?php } ?>
	<?php if($_PAGE['mode']=='edit'){ ?>
		$('form#form1').attr({'action':'index.php?p=setting.group.budget.edit'});
	<?php } ?>
	
	$('.amount_money').bind('change',function(){
		$(this).val(number_format($(this).val(),2));
		calculateGroupItemBudgetSum();
	});
	
	$('input.GroupItemBudget').bind('change',function(){
		calculateGroupItemBudgetSum();
	});
	
	$('select#generalSettings\\[defaultAcademicYearID\\]').bind('change',function(){
		$('.ajax_loading').show();
		$('#submitBtn').attr({'disabled':'disabled'});
		$.ajax({
			'type':'POST',
			'url':'index.php?p=ajax.groupBudget',
			'data':{
				'GroupID':$('input[name=GroupID]').val(),
				'AcademicYear':$(this).val(),
			},
			'success':function(data){
				//alert(data);
				var array_str_data=explode(';',data);
				var array_key=explode(',',array_str_data[0]);
				var array_value=explode(',',array_str_data[1]);
				
				var array_data=new Array();
				for(var i=0;i<array_key.length;i++){
					array_data[array_key[i]]=array_value[i];
				}
				
				$('input#gBudget').val(array_data['GroupBudget']);
				$('input[name=GroupBudgetID]').val(array_data['GroupBudgetID']);
				$('input[name=GroupID]').val(array_data['GroupID']);
				$('input[name*=Budget_]').each(function(){
					var value=array_data[$(this).attr('name')];
					if(typeof(value) == "undefined"){
						value='';
					}
					$(this).val(value);
				});
				$('input[name*=ItemBudgetID_]').each(function(){
					var value=array_data[$(this).attr('name')];
					if(typeof(value) == "undefined"){
						value='';
					}
					$(this).val(value);
				});
				init();
				$('.ajax_loading').hide();
				$('#submitBtn').removeAttr('disabled');
			}
		});
	});
	
	init();
});

//php.js//////////////////////////////////////////
function number_format (number, decimals, dec_point, thousands_sep) {
  //  discuss at: http://phpjs.org/functions/number_format/

  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '')
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec)
      return '' + (Math.round(n * k) / k)
        .toFixed(prec)
    }
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1)
      .join('0')
  }
  return s.join(dec)
}
function explode (delimiter, string, limit) {
  //  discuss at: http://phpjs.org/functions/explode/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //   example 1: explode(' ', 'Kevin van Zonneveld');
  //   returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}

  if (arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined') return null
  if (delimiter === '' || delimiter === false || delimiter === null) return false
  if (typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string ===
    'object') {
    return {
      0: ''
    }
  }
  if (delimiter === true) delimiter = '1'

  // Here we go...
  delimiter += ''
  string += ''

  var s = string.split(delimiter)

  if (typeof limit === 'undefined') return s

  // Support for limit
  if (limit === 0) limit = 1

  // Positive limit
  if (limit > 0) {
    if (limit >= s.length) return s
    return s.slice(0, limit - 1)
      .concat([s.slice(limit - 1)
        .join(delimiter)
      ])
  }

  // Negative limit
  if (-limit >= s.length) return []

  s.splice(s.length + limit)
  return s
}
//php.js //END ////////////////////////////////////////
</script>
<?php echo $navigation; ?>
<br/>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?php echo $Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle']; ?> </td>
		<td>
			<?php echo $_PAGE['rows_group'][0][Get_Lang_Selection('GroupNameChi','GroupName')]; ?>
		</td>
	</tr>
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr>
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach($field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<?php echo $hiddenF; ?>

<fieldset class="instruction_box_v30 fieldset_group_item_budget">
	<legend><?php echo $Lang['ePCM']['Group']['BudgetArr']['Item Budget']; ?></legend>
	<div>
		<table class="form_table_v30">
			<?php foreach((array)$_PAGE['rows_group_financial_item'] as $row_group_financial_item){
					
					$_itemId = $row_group_financial_item['ItemID'];
			?>
				<tr>
					<td class="field_title"><label for="Budget_<?php echo $_itemId; ?>"><?php echo $row_group_financial_item[Get_Lang_Selection('ItemNameChi','ItemName')];?></label></td>
					<td>
						<?php echo $linterface->GET_TEXTBOX('Budget_'.$_itemId, 'Budget_'.$_itemId, (isset($budget_value[$_itemId])?$budget_value[$_itemId]:'0.00'), $OtherClass='requiredField GroupItemBudget amount_money', $OtherPar=array('maxlength'=>255));?>
						<?php echo $linterface->Get_Form_Warning_Msg('Budget_'.$_itemId.'ValidatePositiveAmountWarnDiv', $Lang['ePCM']['Group']['BudgetArr']['Budget must be positive'], $Class='warnMsgDiv'); ?>
						<?php echo $linterface->GET_HIDDEN_INPUT('ItemBudgetID_'.$_itemId, 'ItemBudgetID_'.$_itemId, (isset($item_budget_id_value[$_itemId])?$item_budget_id_value[$_itemId]:'')); ?>
					</td>
					<td>
						<?php
						if($_PAGE['libPCM']->enableFundingSource() ){
							echo $Lang['ePCM']['Group']['Item']['Budget']['defaultFunding'].': '.getSelectByAssoArray($fundingSourceAssoc, 'name="funding['.$_itemId.']" id="funding_'.$_itemId.'"', $funding_value[$_itemId],0,0,$Lang['ePCM']['Mgmt']['FundingName']['NotApplicable']);
						}
						?>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td class="field_title" style="text-align:right;"><?php echo $Lang['ePCM']['Group']['BudgetArr']['Total']; ?>:</td>
				<td>
					<span id="group_item_budget_sum">0.00</span>
				</td>
			</tr>
		</table>
	</div>
</fieldset>