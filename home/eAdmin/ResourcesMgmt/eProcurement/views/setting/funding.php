<?php 
/*
 * Change Log:
 * Date 2017-04-18 Villa E114398 
 * - add Print/ Edit Related
 * Date 2017-03-16 Villa
 * - getFundingSourceTableData change numOfFundingUse 
 * Date 2017-03-15 Villa
 * - Add RecordStatus Filter
 * Date 2017-03-02 Villa
 * - Open the file by Prototype
 */
// $postVar = $_PAGE['views_data'];
echo  $_PAGE['libPCM_ui']->Include_Thickbox_JS_CSS();
$TableData = $_PAGE['view_data']['TableData'];
$btnAddCategory = str_replace("'","\'", $_PAGE['libPCM_ui']->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Add_Category();", "NewAcademicYearSubmitBtn"));
$btnCancelCategory = str_replace("'","\'", $_PAGE['libPCM_ui']->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Delete_Category_Row();", "NewAcademicYearCancelBtn"));
$FundingFilter = $_POST['FundingFilter'];
$FundingFilterSelection = $_PAGE['libPCM_ui']->getFundingFilter($FundingFilter);

$btnAry = array();
$btnAry[] = array('import', 'index.php?p=setting.import.step1.funding');
$btnAry[] = array('export', 'index.php?p=setting.export.funding');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$table = '';
$table .= '<form id="form1" action="#" method="POST">';
$table .= '<div align="left">';
$table .= $FundingFilterSelection;
$table .= '</div>';
$table .= '<table class="common_table_list ClassDragAndDrop">';
### Table Head Start
	$table .= '<thead>';
		$table .= '<tr>';
			$table .= '<th style="width: 15%">';
			$table .= '<span class="row_content">'.$Lang['ePCM']['FundingSource']['Category'] .'</span>';
			$table .= '<span class="table_row_tool row_content_tool" style="float:right;">';
				$table .=  $_PAGE['libPCM_ui']->Get_Thickbox_Link('420', '620', "setting_row",$Lang['ePCM']['FundingSource']['Add']."/ ".$Lang['ePCM']['FundingSource']['Edit'].$Lang['ePCM']['FundingSource']['Category'], "js_onloadCategoryThickBox();");
			$table .=  '</span>';
			$table .= '</th>';
			$table .= '<th class="sub_row_top" style="width: 18%;">'.$Lang['ePCM']['FundingSource']['Funding'] .'</th>';
			$table .= '<th class="sub_row_top" style="width: 12%;">'.$Lang['ePCM']['FundingSource']['Budget'] .'</th>';
			$table .= '<th class="sub_row_top" style="width: 12%;">'.$Lang['ePCM']['FundingSource']['RemainingBudget'].'</th>';
			$table .= '<th class="sub_row_top" style="width: 13%;">'.$Lang['ePCM']['FundingSource']['DeadLine'].'</th>';
			$table .= '<th class="sub_row_top" style="width: 15%;">'.$Lang['ePCM']['FundingSource']['CaseUsingFunding'] .'</th>';
			$table .= '<th class="sub_row_top" style="width: 5%;">'.$Lang['ePCM']['FundingSource']['RecordStatus']['RecordStatus'].'</th>';
			$table .= '<th class="sub_row_top" style="width: 4%;">&nbsp;</th>';
		$table .= '</tr>';
	$table .= '</thead>';
### Table Head End
### Table Body Start
	$table .= '<tbody>';
	if($TableData){
		foreach ($TableData as $_CategoryID => $_Category){
			$NumOfFunding = count($_Category['Funding']);
			$rowSpan = $NumOfFunding + 2;
			$table .= '<tr>';
			$table .= '<td rowspan="'.$rowSpan.'">';
				$table .= Get_Lang_Selection($_Category['FundingCategoryNameCHI'], $_Category['FundingCategoryNameEN']);
			$table .= '</td>';
			for($i=0;$i<7;$i++){
				$table .= '<td style="display:none;">';
					$table .= '&nbsp;';
				$table .= '</td>';
			}
			$table .= '</tr>';
			###Printing Funding
			if($_Category['Funding']){
				foreach ($_Category['Funding'] as $_FundingID => $_Funding){
					$table .= '<tr class="sub_row" id="'.$_FundingID.'">';
						$table .= '<td>';
							$table .= Get_Lang_Selection($_Funding['FundingNameCHI'], $_Funding['FundingNameEN']);
						$table .= '</td>';
						$table .= '<td>';
							$table .= $_Funding['Budget'];
						$table .= '</td>';
						$table .= '<td>';
							$table .= $_Funding['BudgetRemain'];
						$table .= '</td>';
						$table .= '<td>';
							$table .= $_Funding['DeadLineDate'];
						$table .= '</td>';
						$table .= '<td>';
							$table .= $_PAGE['libPCM_ui']->Get_Thickbox_Link('420', '620', "",Get_Lang_Selection($_Funding['FundingNameCHI'], $_Funding['FundingNameEN']), "js_onloadFundingCaseThickBox($_FundingID);","FakeLayer",$_Funding['NumOfUsingFunding']);
// 							$table .= sizeof( $_Funding['Case']);
						$table .= '</td>';
						$table .= '<td>';
							$table .= $Lang['ePCM']['FundingSource']['RecordStatus'][$_Funding['RecordStatus']];
						$table .= '</td>';
						$table .= '<td>';
							$table .= '	<div class="table_row_tool row_content_tool">';
								$table .= $_PAGE['libPCM_ui']->Get_Thickbox_Link('420', '620', "edit_dim thickbox",$Lang['ePCM']['FundingSource']['Edit'].$Lang['ePCM']['FundingSource']['Funding'], "js_onloadFundingThickBox('$_CategoryID', '$_FundingID');");
// 							$table .= '</div>';
// 							$table .= '	<div class="table_row_tool row_content_tool">';
								$table .= '<a href="#" class="delete_dim" title="刪除學期" onclick="js_Delete_Funding('.$_FundingID.');"></a>';
							$table .= '</div>';
						$table .= '</td>';
					$table .= '</tr>';
				}
			}
			for($i=0;$i<6;$i++){
				$table .= '<td>';
					$table .= '&nbsp;';
				$table .= '</td>';
			}
			$table .= '<td>';
				$table .= '	<div class="table_row_tool row_content_tool">';
					$table .= $_PAGE['libPCM_ui']->Get_Thickbox_Link('420', '620', "add_dim",$Lang['ePCM']['FundingSource']['Add'].$Lang['ePCM']['FundingSource']['Funding'], "js_onloadFundingThickBox('$_CategoryID', '');");
				$table .= '</div>';
			$table .= '</td>';
			$table .= '</tr>';
		}
	}else{
		
			$table .= '<tr>';
				$table .= '<td colspan="8" align="center">';
					$table .= $Lang['General']['NoRecordAtThisMoment'];
				$table .= '</td>';
			$table .= '</tr>';
		
	}
	$table .= '</tbody>';
### Table Body End
$table .= '</table>';
$table .= '</form>';
?>
<script>
//Funding Filter JS
$( document ).ready(function(){
	$('#FundingFilter').change(function(){
		$('#form1').submit();
	});
});


//Funding AddEdit ThickBox Related Start
function js_onloadFundingThickBox(CategoryID, FundingID){
	$.ajax({
		type: "POST",
		url: "index.php?p=ajax.getFundingSourceThickBox", 
		data: {
			 "CategoryID" : CategoryID,
			 "FundingID" : FundingID
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}
function js_Checking_Funding(){
	var check = true;
	$('.Alert').hide();
	if($('#FundingThichBox_FundingChineseName').val()==''){
		check = false;
		$('#FundingThichBox_FundingChineseName').focus();
		$('#FundingThichBox_FundingChineseName_Alert').show();
	}
	if($('#FundingThichBox_FundingEnglishName').val()==''){
		check = false;
		$('#FundingThichBox_FundingEnglishName').focus();
		$('#FundingThichBox_FundingEnglishName_Alert').show();
	}
	if($('#FundingThichBox_FundingBudget').val()==''){
		check = false;
		$('#FundingThichBox_FundingBudget').focus();
		$('#FundingThichBox_FundingBudget_Alert').show();
	}
	if($('#FundingThichBox_FundingDeadline').val()==''){
		check = false;
		$('#FundingThichBox_FundingDeadline').focus();
		$('#FundingThichBox_FundingDeadline_Alert').show();
	}
	return check;
}
function js_updateFunding(){
	if(js_Checking_Funding()){
		$.ajax({
			type: "POST",
			url: "index.php?p=setting.funding.fundingUpdate",  
			data: $("#FundingThockBox").serialize(),
			success: function(ReturnData){
				location.reload();
			}
		});
	}
}
function js_Delete_Funding(FundingID){
	var FundingID = FundingID;
	var r = confirm("<?=$Lang['ePCM']['FundingSource']['DeleteAlert'] ?>");
	if(r){
		$.ajax({
			type: "POST",
			url: "index.php?p=setting.funding.fundingDelete",  
			data: { "FundingID": FundingID },
			success: function(ReturnData){
				location.reload();
			}
		});
	}
}
//Funding AddEdit ThickBox Related END

//Category AddEdit ThickBox Related Start
function js_onloadCategoryThickBox(){
	$.ajax({
		type: "POST",
		url: "index.php?p=ajax.getFundingCategoryThickBox", 
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}
function js_Add_FundingCategory_Row() {
	if (!document.getElementById('GenAddCategoryRow')) {
		var TableBody = document.getElementById("CategoryTable").tBodies[0];
		var RowIndex = document.getElementById("AddCategoryRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddCategoryRow";
		NewRow.style.textAlign = "left";
				
		// Title (Eng)
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="CategoryNameCHI" id="CategoryNameCHI" value="" class="textbox"/>';
			tempInput += '<div id="CategoryThick_Add_CHIAlter" class="Alert" style="display:none;color:red;font-size:10px;">';
			tempInput += '<?=$Lang['ePCM']['FundingSource']['EditAlert']?>';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="CategoryNameEN" id="CategoryNameEN" value="" class="textbox"/>';
			tempInput += '<div id="CategoryThick_Add_ENAlter" class="Alert" style="display:none;color:red;font-size:10px;">';
			tempInput += '<?=$Lang['ePCM']['FundingSource']['EditAlert']?>';
			tempInput += '</div>';
			
		NewCell1.innerHTML = tempInput;
		
		// Add and Reset Buttons
		var NewCell2 = NewRow.insertCell(2);
		var temp = '<?=$btnAddCategory?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelCategory?>';
		NewCell2.innerHTML = temp;
		
		$('#CategoryNameCHI').focus();
		
		// Code validation
	}
}
function js_Checking_Category(mode,CategoryID=''){
	var check = true;
	//Reset All the alert msg 
	$('.Alert').hide();
	if(mode=='add'){
	if($('#CategoryNameCHI').val()==''){
			check = false;
			$('#CategoryThick_Add_CHIAlter').show();
			$('#CategoryNameCHI').focus();
		}
		if($('#CategoryNameEN').val()==''){
			check = false;
			$('#CategoryThick_Add_ENAlter').show();
			$('#CategoryNameEN').focus();
		}
	}else if(mode=='edit'){
		if($('#CategoryThick_Edit_CHIRow_'+CategoryID).val()==''){
			check = false;
			$('#CategoryThick_Edit_CHIAlert_'+CategoryID).show();
			$('#CategoryThick_Edit_CHIRow_'+CategoryID).focus();
		}
		if($('#CategoryThick_Edit_ENRow_'+CategoryID).val()==''){
			check = false;
			$('#CategoryThick_Edit_ENAlert_'+CategoryID).show();
			$('#CategoryThick_Edit_ENRow_'+CategoryID).focus();
		}
	}
	
	return check;
}
function js_Delete_Category_Row() 
{
	// Delete the Row
	var TableBody = document.getElementById("CategoryTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddCategoryRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}
function js_Add_Category(){
	//insert the record
	var CategoryNameEN = $('#CategoryNameEN').val();
	var CategoryNameCHI = $('#CategoryNameCHI').val();
	if(js_Checking_Category('add')){
		$.ajax({
			type: "POST",
			url: "index.php?p=setting.funding.categoryUpdate", 
			data: {
				"CategoryNameEN": CategoryNameEN,
				"CategoryNameCHI": CategoryNameCHI
			},
			//reload the thickBox
			success: function(ReturnData){
				js_onloadCategoryThickBox()
			}
		});
	}

}
function js_Delete_FundingCategory(CategoryID){
	var r = confirm("<?=$Lang['ePCM']['FundingSource']['DeleteAlert'] ?>");
	if(r){
		$.ajax({
			type: "POST",
			url: "index.php?p=setting.funding.categoryDelete", 
			data: {
				"CategoryID": CategoryID
			},
			//reload the thickBox
			success: function(ReturnData){
				js_onloadCategoryThickBox()
			}
		});
	}
}
function js_Edit_FundingCategory_Row(CategoryID){
	$('.CategoryThick_Edit_'+CategoryID).show();
	$('.CategoryThick_View_'+CategoryID).hide();
	
}
function js_CancelEdit_Category(CategoryID){
	js_onloadCategoryThickBox()
}
function js_Edit_Category(CategoryID){
	var CategoryNameCHI = $('#CategoryThick_Edit_CHIRow_'+CategoryID).val();
	var CategoryNameEN = $('#CategoryThick_Edit_ENRow_'+CategoryID).val();
	if(js_Checking_Category('edit',CategoryID)){
		$.ajax({
			type: "POST",
			url: "index.php?p=setting.funding.categoryUpdate", 
			data: {
				"CategoryID": CategoryID,
				"CategoryNameEN": CategoryNameEN,
				"CategoryNameCHI": CategoryNameCHI
			},
			//reload the thickBox
			success: function(ReturnData){
				js_onloadCategoryThickBox()
			}
		});
	}
}
//Category AddEdit ThickBox Related End
//Funding Case Related Start
function js_onloadFundingCaseThickBox(FundingID){
	$.ajax({
		type: "POST",
		url: "index.php?p=ajax.getFundingCaseThichkBox", 
		data: {
			"FundingID" : FundingID
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}
//Funding Case Related End
</script>
<?php echo $htmlAry['contentTool']?>
<br style="clear:both">
<?php echo $table?>