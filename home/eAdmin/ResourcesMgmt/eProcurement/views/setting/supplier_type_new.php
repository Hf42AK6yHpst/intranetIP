<?php
$contact_array=array();

if($_PAGE['mode']=='edit'){
	$main=$_PAGE['views_data']['supplierTypeInfo'][0];
	
	$value=array();
	foreach((array)$main as $field=>$field_value){
		$value[$field]=$field_value;
	}
}

$linterface = $_PAGE['libPCM_ui'];

$fields=array(
	array(
		'id'=>'TypeName',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName'],
		'control'=>$linterface->GET_TEXTBOX('TypeName', 'TypeName', $value['TypeName'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)),
		'display'=>$value['TypeName'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'], $Class='warnMsgDiv'),
			'DuplicateWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameDuplicateWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Duplicated Name'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'TypeNameChi',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'],
		'control'=>$linterface->GET_TEXTBOX('TypeNameChi', 'TypeNameChi', $value['TypeNameChi'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)),
		'display'=>$value['TypeNameChi'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'], $Class='warnMsgDiv'),
			'DuplicateWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameChiDuplicateWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Duplicated Name'], $Class='warnMsgDiv'),
		),
	),
);

//Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('TypeID', 'TypeID', $value['TypeID']);

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['SupplierArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.supplier\';');
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['SupplierArr']['Type']);
if($_PAGE['mode']=='new'){
	$navigationAry[] = array($Lang['ePCM']['SettingsArr']['New']);
}else{
//	$navigationAry[] = array($value['SupplierName']);
	$navigationAry[] = array($Lang['ePCM']['SettingsArr']['Edit']);
}
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

?>
<script language="javascript" type="text/javascript">
var backLocation='index.php?p=setting.supplierType';

function doBeforeCheckForm(){
	var TypeName = $('#TypeName').val();
	var TypeNameChi = $('#TypeNameChi').val();
	if(TypeName=='' && TypeNameChi!=''){
		$('#TypeName').removeClass('requiredField');
	}
	if(TypeName!='' && TypeNameChi==''){
		$('#TypeNameChi').removeClass('requiredField');
	}
}

function checkForm(){
	doBeforeCheckForm();
	
	var canSubmit = true;
	var isFocused = false;
	
	$('div[id*=\'WarnDiv\']').hide();
	$('.warnMsgDiv').hide();
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (_elementId!='' && isObjectValueEmpty(_elementId)) {
			canSubmit = false;

			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
		if(_elementId=='' && $(this).val()==''){
			canSubmit = false;
			
			$(this).closest('td').find('.EmptyWarnDiv').show();
		}
	});
	
	//check duplicate name
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'4',
			'value':$('#TypeName').val().trim(),
			'excluded_value':'<?php echo addslashes($value['TypeName']); ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#TypeNameDuplicateWarnDiv').show();
			}
		},
		'async':false
	});
	
	//check duplicate name chi
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'4',
			'language':'b5',
			'value':$('#TypeName').val().trim(),
			'excluded_value':'<?php echo addslashes($value['TypeNameChi']); ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#TypeNameChiDuplicateWarnDiv').show();
			}
		},
		'async':false
	});
	
	return canSubmit;
}

$(document).ready( function() {
	<?php if($_PAGE['mode']=='new'){ ?>
		$('form#form1').attr({'action':'index.php?p=setting.supplierType.update'});
	<?php }else{ ?>
		$('form#form1').attr({'action':'index.php?p=setting.supplierType.update'});
	<?php } ?>
});
</script>
<?php echo $navigation; ?>
<br/>
<table class="form_table_v30">
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr>
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach((array)$field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<?php echo $hiddenF; ?>