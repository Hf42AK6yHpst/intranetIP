<?php
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['view_data'];
$data = $postVar['data'];
$error = $postVar['error'];
$error_with_key_row = BuildMultiKeyAssoc($error,1,array(),0,1);
$headings = $postVar['headings']; // user entered
$heading_checking_array = $postVar['heading_checking_array']; //default
$backLocation = $postVar['back_location'];
$formAction = $postVar['form_action'];
$navigationAry = $postVar['navigationAry'];

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

### navigation
$navigationAry = $navigationAry;
$htmlAry['navigation'] = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);
$htmlAry['StepsBox'] = $linterface->GET_IMPORT_STEPS($CurrStep=2, $CustStepArr='');


?>
<script>
var backLocation = '<?= $backLocation ?>';
function checkForm(){
	$('form#form1').attr('action', '<?=$formAction ?>');
	return true;
}
var canSubmit = true;
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    console.log(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}

function goCancel(){
	if(typeof backLocation != 'undefined'){
		window.location = backLocation;
	}
	else{
		alert('please add a js global var backLocation');
	}
}
</script>

<form name='form1' id='form1' method="POST">
<?=$htmlAry['navigation'] ?>
<?=$htmlAry['StepsBox'] ?>
<?php if(!empty($data)){ ?>
<table  border="0" cellspacing="0" cellpadding="0" style="margin: auto;border-collapse: collapse;width:90%" class="form_table_v30">

	<tr class="tablebluetop tabletopnolink">
		<td width="10px">#</td>
		<?php foreach ($headings as $_heading){ ?>
			<td><?= $_heading?></td>
		<?php } ?>
		<td><?=$Lang['ePCM']['Group']['Import']['ImportStatus'] ?></td>
	</tr>
	<?php
	$counter = 0;
	foreach($data as $_data){ 
	?>
	<tr  class="tablebluerow<?=(((++$counter)%2)+1) ?>">
		<td><?=$counter ?></td> 
		<?php 
		foreach($_data as $__datum){
		?>
		<td><?=$__datum ?></td>
		<?php } ?>
		<?php 
		$errorTextMsg = '';
		foreach((array)$error_with_key_row[$counter-1] as $_key => $_rowErrorAry){
			$errorTextMsg .= ($_key!=0)? '<br>':'';
			$errorTextMsg .= $Lang['ePCM']['Import']['Error'][$_rowErrorAry[0]];
		}
		?>
		<td><span class="tabletextrequire"><?=$errorTextMsg ?></span></td>
	</tr>
	<?php } ?>
</table>
<p></p>
<?php } ?>
<?php if(!empty($error)){ ?>
<table  border="0" cellspacing="0" cellpadding="0" style="margin: auto;border-collapse: collapse;width:90%" class="form_table_v30">

	<tr class="tablebluetop tabletopnolink">
		<td width="10px">#</td>
		<td><?=$Lang['ePCM']['General']['ErrorCode'] ?></td>
		<td><?=$Lang['ePCM']['General']['Description'] ?></td>
		<td><?=$Lang['ePCM']['General']['LineNumber'] ?></td>
	</tr>
	
	<?php 
	$counter = 0;
	foreach($error as $_error){ ?>
		<tr class="tablebluerow<?=(((++$counter)%2)+1) ?> tabletextrequire">
			<td><?=$counter ?></td> 
			<td><?= is_array($_error)?$_error[0]:$_error ?></td>
			<td><?= $Lang['ePCM']['Import']['Error'][is_array($_error)?$_error[0]:$_error] ?></td>
			<td><?= is_array($_error)?($_error[1]+1):'--' ?></td>
		</tr>
	<?php } ?>
</table>
<p></p>
<?php } ?>
<?php if(in_array('INCORRECT_HEADING',$error)){ ?>
<table border="0" cellspacing="0" cellpadding="0" style="margin: auto;border-collapse: collapse;;width:90%" class="form_table_v30">
	
	<tr class="tablebluetop tabletopnolink">
		<td width="10px">#</td>
		<td><?=$Lang['ePCM']['General']['DataColumnDefault'] ?></td>
		<td><?=$Lang['ePCM']['General']['DataColumnUser'] ?></td>
	</tr>
	<?php 
	$counter = 0;
	foreach($heading_checking_array as $_key=>$_default_heading){ 
	?>
		<tr class="tablebluerow<?=(((++$counter)%2)+1) ?>">
			<td><?=$counter ?></td> 
			<?php if($_default_heading!=$headings[$_key]){$class = "tabletextrequire";}else{$class="";} ?>
			<td class="<?=$class ?>"><?=$_default_heading ?></td> 
			<td class="<?=$class ?>"><?=$headings[$_key] ?></td> 
		</tr>
	<?php } ?>
</table>
<?php } ?>


<div class="edit_bottom_v30">
	<p class="spacer"></p>
	
	<?=(empty($error))?$htmlAry['submitBtn']:''?>
	<?=$htmlAry['cancelBtn']?>
	<p class="spacer"></p>
</div>
<?= $hiddenF ?>
</form>