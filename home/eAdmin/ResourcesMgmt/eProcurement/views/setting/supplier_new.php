<?php
$contact_array=array();

if($_PAGE['mode']=='edit'){
	$main=$_PAGE['views_data']['supplierInfo']['MAIN'][0];
	$type_mapping=$_PAGE['views_data']['supplierInfo']['TYPE_MAPPING'][0];
	if(count($_PAGE['views_data']['supplierInfo']['CONTACT'])>0){
		foreach($_PAGE['views_data']['supplierInfo']['CONTACT'] as $contact){
			if($contact['SupplierID']==$main['SupplierID']){
				array_push($contact_array,$contact);
			}
		}
	}

	$value=array();
	foreach($main as $field=>$field_value){
		$value[$field]=$field_value;
	}
	$value['Type']=$type_mapping['TypeID'];
		
}


$linterface = $_PAGE['libPCM_ui'];
$btn = '<a href="#" class="tablelink" onclick="document.getElementById(\'dynSizeThickboxLink\').click();">[+]'.'</a>';
$fields=array(
// 	array(
// 		'id'=>'Code',
// 		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierArr']['Code'],
// 		'control'=>$linterface->GET_TEXTBOX('Code', 'Code', $value['Code'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)),
// 		'display'=>$value['Code'],
// 		'error'=>array(
// 			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('CodeEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Code'], $Class='warnMsgDiv')
// 		),
// 	),
	array(
		'id'=>'SupplierName',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name'],
		'control'=>$linterface->GET_TEXTBOX('SupplierName', 'SupplierName', $value['SupplierName'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>128)),
		'display'=>$value['SupplierName'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('SupplierNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'SupplierNameChi',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name Chi'],
		'control'=>$linterface->GET_TEXTBOX('SupplierNameChi', 'SupplierNameChi', $value['SupplierNameChi'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>128)),
		'display'=>$value['SupplierNameChi'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('SupplierNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'Website',
		'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Website'],
		'control'=>$linterface->GET_TEXTBOX('Website', 'Website', $value['Website'], $OtherClass='validateURL', $OtherPar=array('maxlength'=>255)),
		'display'=>$value['Website'],
		'error'=>array(
			//'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('WebsiteEmptyWarnDiv', 'Please input Website', $Class='warnMsgDiv'),
			'ValidateURLWarnDiv'=>$linterface->Get_Form_Warning_Msg('WebsiteValidateURLWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an URL'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'Description',
		'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Description'],
		'control'=>$linterface->GET_TEXTAREA('Description', $taContents=$value['Description'], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength=''),
		'display'=>$value['Description'],
		'error'=>array(
			//'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('DescriptionEmptyWarnDiv', 'Please input Description', $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'Phone',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'],
		'control'=>$linterface->GET_TEXTBOX('Phone', 'Phone', $value['Phone'], $OtherClass='requiredField validateTelephone', $OtherPar=array('maxlength'=>45)),
		'display'=>$value['Phone'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('PhoneEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Phone'], $Class='warnMsgDiv'),
			'ValidateTelephoneWarnDiv'=>$linterface->Get_Form_Warning_Msg('PhoneValidateTelephoneWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Phone'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'Fax',
		'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'],
		'control'=>$linterface->GET_TEXTBOX('Fax', 'Fax', $value['Fax'], $OtherClass='validateTelephone', $OtherPar=array('maxlength'=>45)),
		'display'=>$value['Fax'],
		'error'=>array(
			//'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('FaxEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Fax'], $Class='warnMsgDiv'),
			'ValidateTelephoneWarnDiv'=>$linterface->Get_Form_Warning_Msg('FaxValidateTelephoneWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Fax'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'Email',
		'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'],
		'control'=>$linterface->GET_TEXTBOX('Email', 'Email', $value['Email'], $OtherClass='validateEmail', $OtherPar=array('maxlength'=>45)),
		'display'=>$value['Email'],
		'error'=>array(
			//'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('EmailEmptyWarnDiv', 'Please input Email', $Class='warnMsgDiv'),
			'ValidateEmailWarnDiv'=>$linterface->Get_Form_Warning_Msg('EmailValidateEmailWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an Email Address'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'Type',
		'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Type'],
		'control'=>getSelectByAssoArray($_PAGE['views_data']['supplier_type'], 'name="Type" id="Type" '.(count($_PAGE['views_data']['supplier_type'])<1?'style="display:none;"':''), $value['Type'], $all=0, $noFirst=1).$btn,
		'display'=>$_PAGE['views_data']['supplier_type'][$value['Type']],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Type'], $Class='warnMsgDiv'),
		),
	),
);

$contact_fields=array();
if($_PAGE['mode']=='edit'){
	if(count($contact_array)>0){
		
	}else{
		$contact_array=array('a'=>'a');
	}
}else if($_PAGE['mode']=='new'){
	$contact_array=array('a'=>'a');
}
if(count($contact_array)>0){
	foreach($contact_array as $contact){
		foreach((array)$contact as $field=>$field_value){
			$value['c'.$field]=$field_value;
		}
		
// 		array_push($contact_fields,array(
// 			array(
// 				'id'=>'cContactName',
// 				'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Contact Name'],
// 				'control'=>$linterface->GET_TEXTBOX('', 'cContactName[]', $value['cContactName'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>128)).$linterface->GET_HIDDEN_INPUT('', 'cContactID[]', $value['cContactID']),
// 				'display'=>$value['cContactName'],
// 				'error'=>array(
// 					'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Contact Name'], $Class='warnMsgDiv EmptyWarnDiv'),
// 				),
// 			),
// 			array(
// 				'id'=>'cPosition',
// 				'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Position'],
// 				'control'=>$linterface->GET_TEXTBOX('', 'cPosition[]', $value['cPosition'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)),
// 				'display'=>$value['cPosition'],
// 				'error'=>array(
// 					'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Position'], $Class='warnMsgDiv EmptyWarnDiv'),
// 				),
// 			),
// 			array(
// 				'id'=>'cPhone',
// 				'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'],
// 				'control'=>$linterface->GET_TEXTBOX('', 'cPhone[]', $value['cPhone'], $OtherClass='requiredField validateTelephone', $OtherPar=array('maxlength'=>45)),
// 				'display'=>$value['cPhone'],
// 				'error'=>array(
// 					'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Phone'], $Class='warnMsgDiv EmptyWarnDiv'),
// 					'ValidateTelephoneWarnDiv'=>$linterface->Get_Form_Warning_Msg('', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Phone'], $Class='warnMsgDiv ValidateTelephoneWarnDiv'),
// 				),
// 			),
// 			array(
// 				'id'=>'cEmail',
// 				'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'],
// 				'control'=>$linterface->GET_TEXTBOX('', 'cEmail[]', $value['cEmail'], $OtherClass='validateEmail', $OtherPar=array('maxlength'=>128)),
// 				'display'=>$value['cEmail'],
// 				'error'=>array(
// 					'ValidateEmailWarnDiv'=>$linterface->Get_Form_Warning_Msg('', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an Email Address'], $Class='warnMsgDiv ValidateEmailWarnDiv'),
// 				),
// 			),
// 			array(
// 				'id'=>'cDescription',
// 				'label'=>$Lang['ePCM']['SettingsArr']['SupplierArr']['Description'],
// 				'control'=>$linterface->GET_TEXTAREA('cDescription[]', $taContents=$value['cDescription'], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength=''),
// 				'display'=>$value['cDescription'],
// 			),
// 		));
	}
}

$btn_add_contact='<a href="#" class="addContactPerson tablelink">[+] '.$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Add Contact Person'].'</a> <a href="#" class="removeContactPerson">[-] '.$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Remove Contact Person'].'</a>';

//Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('SupplierID', 'SupplierID', $value['SupplierID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('IsActive', 'IsActive', ($_PAGE['mode']=='new'?'1':$value['IsActive']));

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['SupplierArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.supplier\';');
if($_PAGE['mode']=='new'){
	$navigationAry[] = array($Lang['ePCM']['SettingsArr']['New']);
}else{
//	$navigationAry[] = array($value['SupplierName']);
	$navigationAry[] = array($Lang['ePCM']['SettingsArr']['Edit']);
}
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

?>
<?= $linterface->Include_Thickbox_JS_CSS()?>
<script language="javascript" type="text/javascript">
var backLocation='index.php?p=setting.supplier';

function updateClassBeforeCheck(){
	var obj_fieldset=$('fieldset.fieldset_contact');
	obj_fieldset.each(function(i){
		if(i==0 && obj_fieldset.length==1){
			var contact_name=$(this).find('[name*="cContactName"]').val();
			var position=$(this).find('[name*="cPosition"]').val();
			var phone=$(this).find('[name*="cPhone"]').val();
			
			if(contact_name!='' || position!='' || phone!=''){
				$(this).find('[name*="cContactName"]').addClass('requiredField');
				$(this).find('[name*="cPosition"]').addClass('requiredField');
				$(this).find('[name*="cPhone"]').addClass('requiredField');
			}else{
				$(this).find('[name*="cContactName"]').removeClass('requiredField');
				$(this).find('[name*="cPosition"]').removeClass('requiredField');
				$(this).find('[name*="cPhone"]').removeClass('requiredField');
			}
		}
	});
}

function doBeforeCheckForm(){
	var SupplierName = $('#SupplierName').val();
	var SupplierNameChi = $('#SupplierNameChi').val();
	if(SupplierName=='' && SupplierNameChi!=''){
		$('#SupplierName').removeClass('requiredField');
	}
	if(SupplierName!='' && SupplierNameChi==''){
		$('#SupplierNameChi').removeClass('requiredField');
	}
}

function checkForm(){
	updateClassBeforeCheck();
	doBeforeCheckForm();
	
	var canSubmit = true;
	var isFocused = false;
	
	$('div[id*=\'WarnDiv\']').hide();
	$('.warnMsgDiv').hide();
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (_elementId!='' && isObjectValueEmpty(_elementId)) {
			canSubmit = false;

			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
		if(_elementId=='' && $(this).val()==''){
			canSubmit = false;
			
			$(this).closest('td').find('.EmptyWarnDiv').show();
		}
	});
	
	$('.validateEmail').each(function(){
		var _elementId = $(this).attr('id');
        var test_value = $(this).val();
        var re = /^([\w_\.\-])+\@(([\w_\.\-])+\.)+([\w]{2,4})+$/;	
        if (test_value!='' && !re.test(test_value)) {
			canSubmit = false;
			if(_elementId!=''){
				$('div#' + _elementId + 'ValidateEmailWarnDiv').show();
			}else{
				$(this).closest('td').find('.ValidateEmailWarnDiv').show();
			}
        }
	});
	
	$('.validateURL').each(function(){	
		var _elementId = $(this).attr('id');
        var test_value = $(this).val();
        var re = /http(s?):\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/;
        if (test_value!='' && !re.test(test_value)) {
			canSubmit = false;
			if(_elementId!=''){
				$('div#' + _elementId + 'ValidateURLWarnDiv').show();
			}else{
				$(this).closest('td').find('.ValidateUrlWarnDiv').show();
			}
        }
	});
	
	$('.validateTelephone').each(function(){
		var _elementId = $(this).attr('id');
        var test_value = $(this).val();
        var re = /^[0-9]{8,9}$/;
        if (test_value!='' && !re.test(test_value)) {
			canSubmit = false;
			if(_elementId!=''){
				$('div#' + _elementId + 'ValidateTelephoneWarnDiv').show();
			}else{
				$(this).closest('td').find('.ValidateTelephoneWarnDiv').show();
			}
        }
	});
	
	return canSubmit;
}

function addContactPerson_onClick(event,$this){
	event.preventDefault();
	event.stopPropagation();
	
	var clone=$this.closest('fieldset.fieldset_contact').clone();
	clone.insertBefore('a#target_place');
	clone.find('input').val('');
	clone.find('textarea').val('');
	
	init();
}

function addContactPerson_bind(){
	$('.addContactPerson').unbind('click');
	$('.addContactPerson').bind('click',function(event){
		addContactPerson_onClick(event,$(this));
	});
}

function removeContactPerson_onClick(event,$this){
	event.preventDefault();
	event.stopPropagation();
	
	if($('fieldset.fieldset_contact').length>1){
		$this.closest('fieldset.fieldset_contact').remove();
		init();
	}
}

function removeContactPerson_bind(){
	$('.removeContactPerson').unbind('click');
	$('.removeContactPerson').bind('click',function(event){
		removeContactPerson_onClick(event,$(this));
	});
}

function showLastAddContactPerson(){
	$('fieldset.fieldset_contact a.addContactPerson').css({'display':'none'});
	$('fieldset.fieldset_contact:last a.addContactPerson').css({'display':'inline'});
}

function showRemoveContactPerson(){
	if($('fieldset.fieldset_contact').length>1){
		$('fieldset.fieldset_contact a.removeContactPerson').css({'display':'inline'});
	}else{
		$('fieldset.fieldset_contact a.removeContactPerson').css({'display':'none'});
	}
}

function renameFieldsetContact(){
	$('fieldset.fieldset_contact').each(function(i){
		$(this).find('legend span').html(i+1);
	});
}

function resetMandateField(){
	var obj_fieldset=$('fieldset.fieldset_contact');
	obj_fieldset.each(function(i){
		if(i==0 && obj_fieldset.length==1){
			$(this).find('[name*="cContactName"]').removeClass('requiredField');
			$(this).find('[name*="cPosition"]').removeClass('requiredField');
			$(this).find('[name*="cPhone"]').removeClass('requiredField');
		}
		if(obj_fieldset.length>1){
			$(this).find('[name*="cContactName"]').addClass('requiredField');
			$(this).find('[name*="cPosition"]').addClass('requiredField');
			$(this).find('[name*="cPhone"]').addClass('requiredField');
		}
	});
}

function init(){
	addContactPerson_bind();
	removeContactPerson_bind();
	showLastAddContactPerson();
	showRemoveContactPerson();
	renameFieldsetContact();
	resetMandateField();
}

function onloadThickBox() {
	$('div#TB_ajaxContent').load(
			"index.php?p=setting.supplierTypeTb.new", 
			{ 
				academicYearId: 1,
				yearTermId: 2
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}

function ajax_submit(serialize_form_data, url, reloadElementId){
	$.ajax({
        type:"POST",
        url: url,
        data: serialize_form_data,
        success:function(data)
        {
        	var array_data=data.split('!=!');
        	if(array_data[0]=='Saved'){
      			ajax_reload_element(reloadElementId,array_data[1]);
	        	js_Hide_ThickBox();
	        	canSubmit = true;
        	}	
        }
      });
}

function ajax_reload_element(reloadElementId,default_value){
	switch(reloadElementId){
		case 'Type':
			$.ajax({
				'type':'POST',
				'url':'index.php?p=ajax.getSupplierTypeList',
				'data':{},
				'success':function(data){
					//alert(data);
					var obj_select=$('#'+reloadElementId);
					obj_select.hide();
					obj_select.empty();
					
					var value_pairs=data.split('###');
					for(i in value_pairs){
						var value_pair=''+value_pairs[i];
						var value_pair_array=value_pair.split('===');
						var key=value_pair_array[0];
						var value=value_pair_array[1];
						if(typeof value != 'undefined'){
							var new_option=$("<option></option>").attr("value", key).text(value);
							if(value==default_value){
								new_option.attr({'selected':'selected'});
							}
							obj_select.append(new_option);
						}
					}
					obj_select.show();
				}
			});
			break;
	}
}

$(document).ready( function() {
	init();
	
	<?php if($_PAGE['mode']=='new'){ ?>
		$('form#form1').attr({'action':'index.php?p=setting.supplier.update'});
	<?php }else{ ?>
		$('form#form1').attr({'action':'index.php?p=setting.supplier.update'});
	<?php } ?>
});
</script>
<?php echo $navigation; ?>
<br/>
<table class="form_table_v30">
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr>
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach((array)$field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<?php echo $hiddenF; ?>
<?php if(count($contact_fields)>0){?>
	<?php foreach($contact_fields as $key=>$contact_table){?>
		<fieldset class="instruction_box_v30 fieldset_contact">
			<legend><?php echo $Lang['ePCM']['SettingsArr']['SupplierArr']['Contact Person'];?> <span><?php echo $key+1; ?></span></legend>
			<div>
				<table class="form_table_v30">
					<?php foreach($contact_table as $contact_field){ ?>
						<tr>
							<td class="field_title"><label for="<?php echo $contact_field['id']; ?>"><?php echo $contact_field['label']; ?></label></td>
							<td>
								<?php echo $contact_field['control']; ?>
								<?php foreach((array)$contact_field['error'] as $error){?>
									<?php echo $error ?>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</table>
			</div>
			<div class="EditView">
				<?php echo $btn_add_contact; ?>
			</div>
		</fieldset>
	<?php } ?>
<?php } ?>
<a id="target_place"></a>
<a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);" onClick="load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier Type']?>', 'onloadThickBox();',inlineID='', defaultHeight=200, defaultWidth=800)" style="display:none;">Dynamic size</a>