<?php 
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$groupInfo = $postVar['groupInfo'];

$groupID = $groupInfo['GroupID'];
$groupName = $groupInfo['GroupName'];
$groupNameChi = $groupInfo['GroupNameChi'];
$code = $groupInfo['Code'];

$actionLang = $Lang['Btn']['New'];
if($groupID != ''){
	$actionLang = $Lang['Btn']['Edit'];
}

$PAGE_NAVIGATION[] = array($Lang['ePCM']['Mgmt']['Case']['Group'],'javascript: window.location=\'index.php?p=setting.group\';');
//$PAGE_NAVIGATION[] = array($groupName);
$PAGE_NAVIGATION[] = array($actionLang);
$htmlAry['navigation'] = $linterface->getNavigation_PCM($PAGE_NAVIGATION);


//Hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('groupID', 'groupID', $groupID);


//Table field
$tableContent = ''; 
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['General']['Code'],
	'content'=>  $linterface->GET_TEXTBOX('code', 'code', $code, $OtherClass='requiredField', $OtherPar=array('maxlength'=>5))
				.$linterface->Get_Form_Warning_Msg('codeEmptyWarnDiv', $Lang['ePCM']['Group']['Please input Code'], $Class='warnMsgDiv')  
				.$linterface->Get_Form_Warning_Msg('codeDuplicateWarnDiv', $Lang['ePCM']['Group']['Duplicated code'], $Class='warnMsgDiv') 
	);
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Group']['GroupName'],
	'content'=>  $linterface->GET_TEXTBOX('groupName', 'groupName', $groupName, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
				.$linterface->Get_Form_Warning_Msg('groupNameEmptyWarnDiv', $Lang['ePCM']['Group']['Please input Group Name'], $Class='warnMsgDiv') 
	);
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Group']['GroupNameChi'],
	'content'=>  $linterface->GET_TEXTBOX('groupNameChi', 'groupNameChi', $groupNameChi, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
				.$linterface->Get_Form_Warning_Msg('groupNameEmptyWarnDiv', $Lang['ePCM']['Group']['Please input Group Name'], $Class='warnMsgDiv') 
	);



echo $linterface->Include_Thickbox_JS_CSS();
?>
<script>
var backLocation = 'index.php?p=setting.group';

function doBeforeCheckForm(){
	var groupName = $('#groupName').val();
	var groupNameChi = $('#groupNameChi').val();
	if(groupName=='' && groupNameChi!=''){
		$('#groupName').removeClass('requiredField');
	}
	if(groupName!='' && groupNameChi==''){
		$('#groupNameChi').removeClass('requiredField');
	}
}

function checkForm(){
	$('form#form1').attr('action', 'index.php?p=setting.group.update');
	
	doBeforeCheckForm();
	
	canSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	
	//check duplicate code
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'2',
			'value':$('#code').val().trim(),
			'excluded_value':'<?php echo addslashes($code); ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#codeDuplicateWarnDiv').show();
			}
		},
		'async':false
	});
	
	//alert(canSubmit);
	return canSubmit;
}
</script>
<?=$htmlAry['navigation']?>
<table class="form_table_v30">
	<?php foreach($tableContent as $content){ ?>
	<tr>
		<td class="field_title"><?php echo $content['title'] ?></td>
		<td>
			<?php echo $content['content'] ?>
		</td>
	</tr>
	<?php } ?>
</table>
<?= $hiddenF ?>
<a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);" onClick="load_dyn_size_thickbox_ip('Title here', 'onloadThickBox();')" style="display:none;">Dynamic size</a>