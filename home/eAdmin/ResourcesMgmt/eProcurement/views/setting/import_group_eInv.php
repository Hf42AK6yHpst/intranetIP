<?php

$postVar = $_PAGE['views_data'];
$eInvGroup = $postVar['eInvGroup'];
$linterface = $_PAGE['libPCM_ui'];
$requiredSymbel = $linterface->RequiredSymbol();

############################################
#	Start: For EJ Only
############################################
if($_PAGE['libPCM']->isEJ()){
	foreach($eInvGroup as $key => &$value){
		foreach($value as $_key => &$_value){
			$_value = convert2unicode(trim($_value), 1);
		}
	}
}
############################################
#	End: For EJ Only
############################################

$groupSelection = $linterface->GET_SELECTION_BOX($eInvGroup, $ParTags='id="groupIdSel" name="groupIdAry[]" multiple="multiple" size="7"', $ParDefault, $ParSelected="", $CheckType=false);
$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('groupIdSel', 1);");
$groupSelectionDiv = $linterface->Get_MultipleSelection_And_SelectAll_Div($groupSelection, $selectAllBtn, $SpanID='');

$additionalCheckBox = $linterface->Get_Checkbox('needAddMember', 'needAddMember', $Value=1, $isChecked=1, $Class='', $Display=$Lang['ePCM']['Group']['Import']['ImportGroupMember'], $Onclick='onClickImportGroupMemberChk(this);', $Disabled='').' ';
$additionalCheckBox .= $linterface->Get_Checkbox('needSetGroupAdmin', 'needSetGroupAdmin', $Value=1, $isChecked=1, $Class='', $Display=$Lang['ePCM']['Group']['Import']['ImportGroupHead'], $Onclick='', $Disabled='');

$PAGE_NAVIGATION[] = array($Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'],'javascript: window.location=\'index.php?p=setting.group\';');
$PAGE_NAVIGATION[] = array($Lang['ePCM']['Group']['Import']['ImportFromInv']);
$htmlAry['navigation'] = $linterface->getNavigation_PCM($PAGE_NAVIGATION);

$stepAry = array();
$stepAry[] = $Lang['ePCM']['Group']['Import']['Step1'];
$stepAry[] = $Lang['ePCM']['Group']['Import']['Step2'];
$stepAry[] = $Lang['ePCM']['Group']['Import']['Step3'];
$htmlAry['customizedImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=1, $stepAry);

?>
<?=$htmlAry['navigation']?>
<script>
var backLocation = 'index.php?p=setting.group';
function checkForm(){
	$('.warnMsgDiv').hide();
	$('form#form1').attr('action', 'index.php?p=setting.group.update_step2_import_group_eInv');

	var canSubmit = true;

	if($('#groupIdSel').val()==null){
		$('#groupSelectionEmptyWarnDiv').show();
		canSubmit = false;
	}
	
	return canSubmit;
}

function onClickImportGroupMemberChk(chk){
	var isChecked = chk.checked;
	if(isChecked){
		$('#needSetGroupAdmin').removeAttr('disabled');
	}else{
		$('#needSetGroupAdmin').attr('disabled','disabled');
		$('#needSetGroupAdmin').removeAttr('checked');
	}
}

</script>
<?=$htmlAry['customizedImportStepTbl']  ?>
<table class="form_table_v30">

	<tr>
		<td class="field_title"><?=$requiredSymbel ?><?=$Lang['ePCM']['Group']['Import']['ChooseGroup'] ?></td>
		<td><?=$groupSelectionDiv ?><?=$linterface->Get_Form_Warning_Msg('groupSelectionEmptyWarnDiv', $Lang['ePCM']['Group']['Import']['Warning']['ChooseOneOrMoreGroup'], $Class='warnMsgDiv') ?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePCM']['Group']['Import']['AdditionalImport'] ?></td>
		<td><?=$additionalCheckBox ?></td>
	</tr>

</table>
<?= $hiddenF ?>