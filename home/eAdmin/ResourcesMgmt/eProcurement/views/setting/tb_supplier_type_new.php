<?php
$contact_array=array();

if($_PAGE['mode']=='edit'){
		
}

$linterface = $_PAGE['libPCM_ui'];

$fields=array(
	array(
		'id'=>'TypeName',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName'],
		'control'=>$linterface->GET_TEXTBOX('TypeName', 'TypeName', $value['TypeName'], $OtherClass='tb_requiredField', $OtherPar=array('maxlength'=>255)),
		'display'=>$value['TypeName'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'], $Class='warnMsgDiv'),
			'DuplicateWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameDuplicateWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Duplicated Name'], $Class='warnMsgDiv'),
		),
	),
	array(
		'id'=>'TypeNameChi',
		'label'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'],
		'control'=>$linterface->GET_TEXTBOX('TypeNameChi', 'TypeNameChi', $value['TypeNameChi'], $OtherClass='tb_requiredField', $OtherPar=array('maxlength'=>255)),
		'display'=>$value['TypeNameChi'],
		'error'=>array(
			'EmptyWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'], $Class='warnMsgDiv'),
			'DuplicateWarnDiv'=>$linterface->Get_Form_Warning_Msg('TypeNameChiDuplicateWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Duplicated Name'], $Class='warnMsgDiv'),
		),
	),
);

//Hidden field
// $hiddenF = '';
// $hiddenF .= $linterface->GET_HIDDEN_INPUT('SupplierTypeID', 'SupplierTypeID', $value['SupplierTypeID']);

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "tb_goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script language="javascript" type="text/javascript">

function tb_doBeforeCheckForm(){
	var TypeName = $('#TypeName').val();
	var TypeNameChi = $('#TypeNameChi').val();
	if(TypeName=='' && TypeNameChi!=''){
		$('#TypeName').removeClass('requiredField');
	}
	if(TypeName!='' && TypeNameChi==''){
		$('#TypeNameChi').removeClass('requiredField');
	}
}

function tb_checkForm(){
	tb_doBeforeCheckForm();
	
	var canSubmit = true;
	var isFocused = false;
	
	$('div[id*=\'WarnDiv\']').hide();
	$('.warnMsgDiv').hide();
	
	$('.tb_requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (_elementId!='' && isObjectValueEmpty(_elementId)) {
			canSubmit = false;

			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
		if(_elementId=='' && $(this).val()==''){
			canSubmit = false;
			
			$(this).closest('td').find('.EmptyWarnDiv').show();
		}
	});
	
	//check duplicate name
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'4',
			'value':$('#TypeName').val().trim(),
			'excluded_value':'<?php echo $value['TypeName']; ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#TypeNameDuplicateWarnDiv').show();
			}
		},
		'async':false
	});
	
	//check duplicate name chi
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'4',
			'language':'b5',
			'value':$('#TypeName').val().trim(),
			'excluded_value':'<?php echo $value['TypeNameChi']; ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#TypeNameChiDuplicateWarnDiv').show();
			}
		},
		'async':false
	});

	return canSubmit;
}

var canSubmit = true;
function tb_goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  tb_checkForm();
	}
	catch(err) {
	    console.log(err);
	}

	if(checkResult){
		formData = $('form#ajax_form').serialize();
		ajax_submit(formData,'index.php?p=setting.supplierType.update.0', 'Type');
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}

$(document).ready( function() {
	
});
</script>
<br/>
<form name='ajax_form' id='ajax_form' method="POST">
<div class="table_board">
	<table class="form_table_v30">
		<?php if(count($fields)>0){?>
			<?php foreach($fields as $field){ ?>
				<tr>
					<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
					<td>
						<?php echo $field['control']; ?>
						<?php foreach((array)$field['error'] as $error){?>
							<?php echo $error ?>
						<?php } ?>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
	</table>
	<?php echo $hiddenF; ?>
	<br style="clear:both;" />
	<?=$linterface->MandatoryField()?>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>

