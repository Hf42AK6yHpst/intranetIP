

<?php 
/*
 *  Villa Date: 2016-10-07 update the checking to allow the budget can be float.2 in stead of int
 */
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$catParData = $postVar['catParData'];
$groupParData = $postVar['groupParData'];
$caseInfo = $postVar['caseInfo'];
$currentStage = $postVar['currentStage'];
$editReord = $postVar['EditRecord'];
// debug_pr($editReord);
$editMode = false;

if(isset($editReord)){
	$editMode = true;
	$caseID = $editReord['caseID'];
	$CaseName = $editReord['CaseName'];
	$CategoryID = $editReord['CategoryID'];
	$Description = $editReord['Description'];
	$ApplicantGroupID = $editReord['ApplicantGroupID'];
	$ItemID = $editReord['ItemID'];
	$Budget = $editReord['Budget'];
	$ApplicantType = $editReord['ApplicantType'];
	$ApplicantUserID = $editReord['ApplicantUserID'];
	$DateInput = $editReord['DateInput'];
	
}else{
	$editMode = false;
	$caseID = '';
	$CaseName = '';
	$CategoryID = '';
	$Description = '';
	$ApplicantGroupID ='';
	$ItemID = '';
	$Budget = '';
	$ApplicantType = '';
	$ApplicantUserID = '';
	$DateInput = '';
}




$isRuleValid = $postVar['isRuleValid'];
$gs_exceed_budget_percentage = $postVar['GeneralSetting']['exceed_budget_percentage'];
// If rule is not valid => warning table and stop submit
if(!$isRuleValid){
	$htmlAry['WarningBox'] = $linterface->getStopNewApplicationWarningBox();
	$buttonDisable = 1;
}

$sessionToken = time();

$requiredSymbol = $linterface->RequiredSymbol();

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=$buttonDisable, $ParClass="", $ParExtraClass="actionBtn");
	// do nothing
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


$gNameTextBox = $linterface->GET_TEXTBOX('groupName', 'groupName', $groupName, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255));
$gNameWarn = $linterface->Get_Form_Warning_Msg('groupNameEmptyWarnDiv', 'Please input Group Name', $Class='warnMsgDiv');

## navigation
$navigationAry[] = array($Lang['ePCM']['ManagementArr']['FinishedViewArr']['OtherApplication'], 'javascript: window.location=\'index.php?p=mgmt.otherfinished\';');
//if($roleID == ''){
if($editMode){
	$navigationAry[] = array($Lang['Btn']['Edit']);
}else{
$navigationAry[] = array($Lang['Btn']['New']);
}

$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

//Selection Box
$htmlAry['caseName'] = $linterface->GET_TEXTBOX('caseName', 'caseName', $CaseName, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)).$linterface->Get_Form_Warning_Msg('caseNameEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['CaseName'], $Class='warnMsgDiv');

foreach($catParData as $_key => $_cat){
	if($_key%3==0&&$_key!=0){
		$htmlAry['categoryRadio'].= '<br>';
	}
	if($caseInfo['CategoryID']==$_cat[0]){
		$checked = true;
	}
	$htmlAry['categoryRadio'].= $linterface->Get_Radio_Button('cat_'.$_cat[0], 'categoryID', $Value=$_cat[0], $checked, $Class="", $Display=Get_Lang_Selection($_cat[2],$_cat[1]), $Onclick="",$isDisabled=0).'&nbsp;&nbsp;';
	$checked = false;
}
$htmlAry['categoryRadio'].=$linterface->Get_Form_Warning_Msg('categoryIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Category'], $Class='warnMsgDiv');

$htmlAry['groupSelect'] = $linterface->GET_SELECTION_BOX($groupParData, 'name="groupID" id="groupID" onchange="ajax_financial_item(this.value);"', $Lang['ePCM']['General']['Select'], $ApplicantGroupID, $CheckType=false).$linterface->Get_Form_Warning_Msg('groupIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Group'], $Class='warnMsgDiv');
if($caseInfo['ApplicantGroupID']>0){
	$itemArray = $_PAGE['views_data']['itemSelectArray'];
}else{
	$itemArray = array();
}
$htmlAry['itemSelect'] = $linterface->GET_SELECTION_BOX($itemArray, 'name="itemID" id="itemID" onchange="ajax_budget(this.value);"', $Lang['ePCM']['General']['Select'], $ItemID, $CheckType=false);
$htmlAry['itemSelectWarning'] = $linterface->Get_Form_Warning_Msg('itemIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Item'], $Class='warnMsgDiv');
//input
$htmlAry['description'] = $linterface->GET_TEXTAREA('description', $Description, $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='requiredField', $taID='', $CommentMaxLength='').$linterface->Get_Form_Warning_Msg('descriptionEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Reason'], $Class='warnMsgDiv');
$htmlAry['budget'] = $linterface->GET_TEXTBOX_NUMBER('budget', 'budget', $Budget, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255,'oninput'=>'checkBudgetExceed();','onchange'=>'checkBudgetExceed();')).$linterface->Get_Form_Warning_Msg('budgetEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Budget'], $Class='warnMsgDiv').$linterface->Get_Form_Warning_Msg('budgetNotIntWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DecimalPoint'], $Class='warnMsgDiv');
//$htmlAry['remark'] = $linterface->GET_TEXTAREA('remark', $taContents='', $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='');
//Radio 
if($caseInfo['ApplicantType']=='G'){
	$isGroupApplicant = 1;
}else if($caseInfo['ApplicantType']=='I'){
	$isIndividualApplicant = 1;
}

$htmlAry['applicantType'] = $linterface->Get_Radio_Button('applicantType_individual', 'applicantType', $Value='I', $isIndividualApplicant, $Class="", $Display=$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Individual'], $Onclick="onSelectApplicantType(this.value)",$isDisabled=0).'&nbsp;&nbsp;';
//$htmlAry['applicantType'] .= $linterface->Get_Radio_Button('applicantType_admin', 'applicantType', $Value='A',$isAdmin, $Class="", $Display='Choose others', $Onclick="onSelectApplicantType(this.value)",$isDisabled=0).'&nbsp;&nbsp;';
$htmlAry['applicantType'] .= $linterface->Get_Radio_Button('applicantType_group', 'applicantType', $Value='G',$isGroupApplicant, $Class="", $Display=$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Group'], $Onclick="onSelectApplicantType(this.value)",$isDisabled=0);
$htmlAry['applicantType'] .= $linterface->Get_Form_Warning_Msg('applicantTypeEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['ApplicantType'], $Class='warnMsgDiv');

//Choose individual ***** FOR ADMIN ONLY  *********
if($caseInfo['ApplicantUserName']==''){
	$chooseIndividualName = Get_Lang_Selection($_SESSION['ChineseName'],$_SESSION['EnglishName']);
}else{
	$chooseIndividualName = $caseInfo['ApplicantUserName'];
}
$htmlAry['chooseIndividual'] .= '<span id="chooseIndividualSpan">'.$chooseIndividualName.'</span>'.'&nbsp;&nbsp;';
$htmlAry['chooseIndividual'] .= '<a href="javascript:addUser();" >['.$Lang['Btn']['Edit'].']</a>';

//Budget notice
if($gs_exceed_budget_percentage===''){
	$budget_notice = "";
}else if ($gs_exceed_budget_percentage==0){
	$budget_notice = $Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['ZeroPercent'];
}else{
	$budget_notice = $Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['NPercent'];
	$budget_notice = str_replace('<!--percent-->',intval($gs_exceed_budget_percentage*100),$budget_notice);
}


//Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('session', 'session', $sessionToken);
if($caseInfo['ApplicantUserID']==''){
	$chooseIndividual = $_SESSION['UserID'];
}else{
	$chooseIndividual = $caseInfo['ApplicantUserID'];
}
$hiddenF .= $linterface->GET_HIDDEN_INPUT('chooseIndividual', 'chooseIndividual', $chooseIndividual);
$htmlAry['hidden'] = $hiddenF;

echo $linterface->Include_Thickbox_JS_CSS();
echo $linterface->Include_AutoComplete_JS_CSS();
echo $linterface->Include_JS_CSS();

if($DateInput==''){
	$nowTime = strtotime("now");
	$selectionBoxDate = date('Y-m-d', $nowTime);
}else{
	$selectionBoxDate = strtotime($DateInput);
	$selectionBoxDate = date('Y-m-d', $selectionBoxDate);

}

$datePicker = $linterface->GET_DATE_PICKER("RecordTime", $selectionBoxDate);

?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<script>
$(document).ready( function() {
	<?php if ($editMode){?>
	$("<?= "#cat_".$CategoryID?>").attr('checked',true);
	$("<?= "#applicantType_".($ApplicantType=='I'? 'individual':'group')?>").attr('checked',true);
	ajax_financial_item(<?= $ApplicantGroupID?>)
	ajax_budget(<?= $ItemID?>);
	<?php }?>
});

var backLocation = 'index.php?p=mgmt.otherfinished';

var over_budget = false;

function checkForm(){
// 	debugger;
	$('.warnMsgDiv').hide();
	
	$('form#form1').attr('action', 'index.php?p=mgmt.case.insertOtherConfirm');

	
	canSubmit = true;
	var isFocused = false;
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});

	
	// Check selection box
// 	if(!isSelectionBoxValid('categoryID')){
// 		$('div#' + 'categoryID' + 'EmptyWarnDiv').show();
// 		canSubmit = false;
// 	}
	if(!isSelectionBoxValid('groupID')){
		$('div#' + 'groupID' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}
// 	if(!isSelectionBoxValid('itemID')){
// 		$('div#' + 'itemID' + 'EmptyWarnDiv').show();
// 		canSubmit = false;
// 	}

	//check radio
	var applicantType = $('input[name=applicantType]:checked').val();
	if(!applicantType){
		$('div#' + 'applicantType' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}
	var categoryID = $('input[name=categoryID]:checked').val();
	if(!categoryID){
		$('div#' + 'categoryID' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}
	var itemID = $('input[name=itemID]:checked').val();
	if(!itemID){
		$('div#' + 'itemID' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}

	

	//Check Budget must be INTEGER
	var budget = $('#budget').val();
// 	if(Math.floor(budget) == budget ) {
// 		// Is integer
// 	}else{
// 		$('#budgetNotIntWarnDiv').show();
// 		canSubmit = false;
// 	}
//	Villa 2016-10-07 update the checking to allow the budget can be float.2 in stead of int
// 	if((budget*100%1 ) == 0){
		
// 	}else{
// 		$('#budgetNotIntWarnDiv').show();
// 		canSubmit = false;
// 	}
	budget = budget.split(".");
	if((budget.length ) == 1){

		if(budget[0]%1==0){
			//do nothing
		}else{
			$('#budgetNotIntWarnDiv').show();
			canSubmit = false;
		}
		
		
	}else{
		
		if((budget[1].length ) < 3 && budget.length < 3){
			
			if(budget[0]%1==0 && budget[1]%1==0){
				//do nothing
			}else{
				$('#budgetNotIntWarnDiv').show();
				canSubmit = false;
			}
			
		}else{
				$('#budgetNotIntWarnDiv').show();
				canSubmit = false;
			}
	}

	if(over_budget){
		canSubmit = false;
		$('#budget').focus();
	}
	
	return canSubmit;
}
function isSelectionBoxValid(id){
	var value = $('#'+id+' option:selected').val();
	if(value==''){
		return false;
	}else{
		return true;
	}
}

//Start: for admin only

function addUser(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['ChooseIndividual'] ?>', 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=600);
}
function onloadThickBox(){
	$('div#TB_ajaxContent').load(
			"index.php?p=mgmt.case.chooseIndividual", 
			{ 
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
				Init_JQuery_AutoComplete('UserSearch');
			}
		);
}
function getNotAvalUser(){
	//return empty array for template/add_user.php
	var notavalUser = [];
	
	return notavalUser;
}
function Add_Selected_User(userID,name){
	userID = userID.substring(1);
	addToChooseIndividual(userID,name);
}
function Add_Role_Member(){
	var userID = $('[name="AvalUserList[]"] option:selected').val();
	var name = $('[name="AvalUserList[]"] option:selected').text();
	addToChooseIndividual(userID,name);
}
function addToChooseIndividual(userID,name){
	$('#chooseIndividual').val(userID);
	$('#chooseIndividualSpan').html(name);	
	window.top.tb_remove(); 
}
//End: for admin only

var canSubmit = true;
function goSubmit(){
	var isConfrim = true;
	<?php if($currentStage>=1){?>
	//current stage>=1 => need start new case
	isConfrim = confirm("<?= $Lang['ePCM']['Mgmt']['Case']['Warning']['NeedStartNewCase'] ?>");
	if(!isConfrim){
		return;
	}
	<?php }?>

	
	
	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
// 	    alert(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}
function ajax_financial_item(value){
	//reset check budget 
	checkBudgetExceed();
	
	$('#ajax_financial_item').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		type:"POST",
		url:"index.php?p=ajax.financialItem",
		data:{
			groupID: value,
			itemID: '<?=$ItemID ?>'
		},
		success: function(response){
			$('#ajax_financial_item').html(response);
		}
	});

	//hide budget statment
	$('#ajax_budget').html('');
	$('#ajax_budget_exceed').html('');
	
}
function ajax_budget(itemID){
	//reset check budget 
	checkBudgetExceed();
	
	$('#ajax_budget').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		type:"POST",
		url:"index.php?p=ajax.financialItemBudget",
		data:{
			itemID: itemID
		},
		success: function(response){
			$('#ajax_budget').html(response);
		}
	});
}
function goCancel(){
	if(typeof backLocation != 'undefined'){
		ajax_recover_tempRemove();
		window.location = backLocation;
	}
	else{
// 		alert('please add a js global var backLocation');
	}
}

function ajax_recover_tempRemove(){
	postdata = $('form#form1').serialize()
	$.post(
			'index.php?p=file.recover',
			postdata,
			function(response){
			}
	);
}
// below are for plupload
var FileCount = 0;
var file_uploader = {};


function checkBudgetExceed(){
	var budget = $('#budget').val();
	var groupID = $('#groupID option:selected').val();
	var itemID = $('input[name="itemID"]:checked').val();
	if(!(budget>0&&groupID>0&&itemID>0)){
		//skip
		return;
	}else{
		$.ajax({
	        type:"POST",
	        url: "index.php?p=ajax.checkExceedBudget",
	        data: {
				"budget" : budget,
				"groupID"  : groupID,
				"itemID" : itemID,
				},
	        success:function(data)
	        {
		        switch(data){
		        case 'OK':
			        $('#ajax_budget_exceed').html('');
			        over_budget = false;
			        break;
		        case 'GROUP':
		        	$('#ajax_budget_exceed').html('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group'] ?>');
		        	over_budget = true;
			        break;
		        case 'ITEM':
		        	$('#ajax_budget_exceed').html('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item'] ?>');
		        	over_budget = true;
			        break;
		        case 'GROUP:ITEM':
		        	$('#ajax_budget_exceed').html('<?= $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem']?>');
		        	over_budget = true;
			        break;
		        }
	        }
	      });
	}
}

function onSelectApplicantType(){
	
}
</script>
<form name='form1' id='form1' method="POST">
<div class="table_board">
<?= $navigation ?>
<?=$htmlAry['WarningBox'] ?>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['CaseName'] ?></td>
			<td><?= $htmlAry['caseName'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['Category']?></td>
			<td><?= $htmlAry['categoryRadio'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Result']['Details'] ?></td>
			<td><?= $htmlAry['description'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['Group'] ?></td>
			<td><?= $htmlAry['groupSelect'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['FinancialItem'] ?></td>
			<td><div style="display:inline-block" id="ajax_financial_item"><?= $htmlAry['itemSelect'] ?></div> <span id="ajax_budget"></span> <?=$htmlAry['itemSelectWarning'] ?> </td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=	$Lang['ePCM']['Mgmt']['Case']['Result']['Amount']?></td>
			<td>
				<?= $htmlAry['budget'] ?> <span id="ajax_budget_exceed" style="color:red"></span>
				<br>
				<span class="tabletextremark"><?=$budget_notice ?></span>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Result']['RecordDay']?></td>
			<td> <?= $datePicker?> </td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Applicant']?></td>
			<td><?= $htmlAry['applicantType'] ?></td>
		</tr>
		<?php if($_SESSION['ePCM']['isAdmin']){ ?>
		<tr>
			<td class="field_title"><?= $Lang['ePCM']['Mgmt']['Case']['ChooseIndividual'] ?></td>
			<td><?= $htmlAry['chooseIndividual'] ?></td>
		</tr>
		<?php } ?>
		<!-- 
		<tr>
			<td class="field_title"><?=$requiredSymbol?> aaa </td>
			<td>
				<?= $gNameTextBox ?>
				<?= $gNameWarn ?>
			</td>
		</tr>
		 -->
		<?=$sessionIdentifierHidden?>
	</tbody>
</table>
<?= $htmlAry['hidden']  ?>
<br style="clear:both;" />
	<div style="text-align:left">
		<?=$linterface->MandatoryField()?>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>