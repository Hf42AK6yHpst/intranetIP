<?php 
/*
 * Using: 
 * Change Log: 
 * Date: 2017-04-18 Villa E114398
 * 		- add parameter to skip budget related
 * Date: 2017-04-13 Villa E114398
 * 		- support funding source in edit mode
 * Date: 2017-04-05 Villa
 * 		- add flag to control ON/OFF funding source
 * Date: 2017-03-16 Villa
 * 		- Modified checkForm(): Ajax checking async
 * Date: 2017-03-07 Villa K113812 
 * 		- Modified checkForm() : Add Ajax Checking
 * 
 * Date: 2017-03-06 Villa K113812 
 * 		- Add Funding Source UI 
 */

$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$catParData = $postVar['catParData'];
$groupParData = $postVar['groupParData'];
$caseInfo = $postVar['caseInfo'];
$currentStage = $postVar['currentStage'];
// debug_pr($caseInfo);
$isRuleValid = $postVar['isRuleValid'];
$gs_exceed_budget_percentage = $postVar['GeneralSetting']['exceed_budget_percentage'];
// If rule is not valid => warning table and stop submit
if(!$isRuleValid){
	$htmlAry['WarningBox'] = $linterface->getStopNewApplicationWarningBox();
	$buttonDisable = 1;
}
$ruleID = $caseInfo['RuleID'];
$caseInfo['CaseID']? $caseID = $caseInfo['CaseID']: $caseID=-1;
// for plupload
$editMode = (empty($caseInfo))? false:true;
$sessionToken = time();
$pluploadButtonId = 'UploadButton';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';
$deleteFileDivId = 'willDeleteFileDiv';
$pluploadBtn = $linterface->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
$templateBox = $linterface->getTemplateFileUI($ePCMcfg['Template']['Application'], $ruleID, $getAttachWithRuleIDAndRuleID=true, $caseID);
$fileWarningDiv = $linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv tabletextrequire');

$requiredSymbol = $linterface->RequiredSymbol();

$skipBudget = $postVar['skipBudget'];

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=$buttonDisable, $ParClass="", $ParExtraClass="actionBtn");
if($currentStage>=1){ // if currentStage >= 1 => cannot save as draft
	// do nothing
}else{
	$htmlAry['darftBtn'] = $linterface->Get_Action_Btn($Lang['ePCM']['General']['SaveAsDraft'], "button", "goSave()", 'draftBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
}
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


$gNameTextBox = $linterface->GET_TEXTBOX('groupName', 'groupName', $groupName, $OtherClass='requiredField', $OtherPar=array());
$gNameWarn = $linterface->Get_Form_Warning_Msg('groupNameEmptyWarnDiv', 'Please input Group Name', $Class='warnMsgDiv');

## navigation
$navigationAry[] = array($Lang['ePCM']['ManagementArr']['ViewArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=mgmt.view\';');
//if($roleID == ''){
	$navigationAry[] = array($Lang['Btn']['New']);
//}
//else{
//	$navigationAry[] = array($roleName);
//	$navigationAry[] = array($Lang['Btn']['Edit']);
//}
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

//Selection Box
$htmlAry['caseName'] = $linterface->GET_TEXTBOX('caseName', 'caseName', $caseInfo['CaseName'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)).$linterface->Get_Form_Warning_Msg('caseNameEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['CaseName'], $Class='warnMsgDiv');
//$htmlAry['categorySelect'] = $linterface->GET_SELECTION_BOX($catParData, 'name="categoryID" id="categoryID"', $Lang['ePCM']['General']['Select'], $caseInfo['CategoryID'], $CheckType=false).$linterface->Get_Form_Warning_Msg('categoryIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Category'], $Class='warnMsgDiv');
// debug_pr($catParData);
foreach($catParData as $_key => $_cat){
	if($_key%3==0&&$_key!=0){
		$htmlAry['categoryRadio'].= '<br>';
	}
	if($caseInfo['CategoryID']==$_cat[0]){
		$checked = true;
	}
	$htmlAry['categoryRadio'].= $linterface->Get_Radio_Button('cat_'.$_cat[0], 'categoryID', $Value=$_cat[0], $checked, $Class="", $Display=Get_Lang_Selection($_cat[2],$_cat[1]), $Onclick="",$isDisabled=0).'&nbsp;&nbsp;';
	$checked = false;
}
$htmlAry['categoryRadio'].=$linterface->Get_Form_Warning_Msg('categoryIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Category'], $Class='warnMsgDiv');

$htmlAry['groupSelect'] = $linterface->GET_SELECTION_BOX($groupParData, 'name="groupID" id="groupID" onchange="ajax_financial_item(this.value);"', $Lang['ePCM']['General']['Select'], $caseInfo['ApplicantGroupID'], $CheckType=false).$linterface->Get_Form_Warning_Msg('groupIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Group'], $Class='warnMsgDiv');
if($caseInfo['ApplicantGroupID']>0){
	$itemArray = $_PAGE['views_data']['itemSelectArray'];
}else{
	$itemArray = array();
}
$htmlAry['itemSelect'] = $linterface->GET_SELECTION_BOX($itemArray, 'name="itemID" id="itemID" onchange="ajax_budget(this.value);"', $Lang['ePCM']['General']['Select'], $caseInfo['ItemID'], $CheckType=false);
$htmlAry['itemSelectWarning'] = $linterface->Get_Form_Warning_Msg('itemIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Item'], $Class='warnMsgDiv');
//input
$htmlAry['description'] = $linterface->GET_TEXTAREA('description', $caseInfo['Description'], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='requiredField', $taID='', $CommentMaxLength='').$linterface->Get_Form_Warning_Msg('descriptionEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Reason'], $Class='warnMsgDiv');

if(!$skipBudget){
	$htmlAry['budget'] = $linterface->GET_TEXTBOX_NUMBER('budget', 'budget', $caseInfo['Budget'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255)).$linterface->Get_Form_Warning_Msg('budgetEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Budget'], $Class='warnMsgDiv').$linterface->Get_Form_Warning_Msg('budgetNotIntWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DecimalPoint'], $Class='warnMsgDiv');
}else{
	$htmlAry['budget'] = $caseInfo['Budget'].'<input type="hidden" name="budget" id="budget" value="'.$caseInfo['Budget'].'"></input>';
}
//$htmlAry['remark'] = $linterface->GET_TEXTAREA('remark', $taContents='', $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='');
//Radio 
if($caseInfo['ApplicantType']=='G'){
	$isGroupApplicant = 1;
}else if($caseInfo['ApplicantType']=='I'){
	$isIndividualApplicant = 1;
}

if($sys_custom['ePCM']['mfs']['hideApplicantType']){
    $isIndividualApplicant = true;
}

$htmlAry['applicantType'] = $linterface->Get_Radio_Button('applicantType_individual', 'applicantType', $Value='I', $isIndividualApplicant, $Class="", $Display=$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Individual'], $Onclick="onSelectApplicantType(this.value)",$isDisabled=0).'&nbsp;&nbsp;';
//$htmlAry['applicantType'] .= $linterface->Get_Radio_Button('applicantType_admin', 'applicantType', $Value='A',$isAdmin, $Class="", $Display='Choose others', $Onclick="onSelectApplicantType(this.value)",$isDisabled=0).'&nbsp;&nbsp;';
$htmlAry['applicantType'] .= $linterface->Get_Radio_Button('applicantType_group', 'applicantType', $Value='G',$isGroupApplicant, $Class="", $Display=$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Group'], $Onclick="onSelectApplicantType(this.value)",$isDisabled=0);
$htmlAry['applicantType'] .= $linterface->Get_Form_Warning_Msg('applicantTypeEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['ApplicantType'], $Class='warnMsgDiv');

//Choose individual ***** FOR ADMIN ONLY  *********
if($caseInfo['ApplicantUserName']==''){
	$chooseIndividualName = Get_Lang_Selection($_SESSION['ChineseName'],$_SESSION['EnglishName']);
}else{
	$chooseIndividualName = $caseInfo['ApplicantUserName'];
}
$htmlAry['chooseIndividual'] .= '<span id="chooseIndividualSpan">'.$chooseIndividualName.'</span>'.'&nbsp;&nbsp;';
$htmlAry['chooseIndividual'] .= '<a href="javascript:addUser();" >['.$Lang['Btn']['Edit'].']</a>';

//Budget notice
if($gs_exceed_budget_percentage===''){
	$budget_notice = "";
}else if ($gs_exceed_budget_percentage==0){
	$budget_notice = $Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['ZeroPercent'];
}else{
	$budget_notice = $Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['NPercent'];
	$budget_notice = str_replace('<!--percent-->',intval($gs_exceed_budget_percentage*100),$budget_notice);
}

//Funding Source
if($_PAGE['libPCM']->enableFundingSource()){
	$FundingSourceArr = $_PAGE['views_data']['FundingSourceArr'];
	foreach ((array)$FundingSourceArr as $_FundingSourceArr){ //loop every funding
		foreach ((array)$_FundingSourceArr['Case'] as $_Case){ //loop every case
			if($_Case['CaseID']==$caseInfo['CaseID']){ //print only this case
				$FundingID['FundingID'][] = $_FundingSourceArr['FundingID'];
				$FundingID['Budget'][] = $_Case['Amount'];
			}
		}
	}
	if(sizeof($FundingID)){
		foreach ($FundingID['FundingID']as $FundingKey => $_FundingID){
			$BudgetUsed = $FundingID['Budget'][$FundingKey];
			$htmlAry['FundingSource_SelectionBox'] .= '<tr><td>'.$linterface->getFundingByCategorySelectionBox('Funding_Selection_Box_'.$FundingKey,'Funding_Selection_Box',$_FundingID,$BudgetUsed).'</td></tr>';
		}
	}else{
		$htmlAry['FundingSource_SelectionBox'] = '<tr><td>'.$linterface->getFundingByCategorySelectionBox().'</td></tr>';
	}
	$htmlAry['FundingSource_AddBtn'] = "<div class='table_row_tool row_content_tool' style='float:left;'><a href='javascript:void(0)' class='add_dim' onclick='AddFundingSourceNewRow();'></a></div>";
}
//Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseInfo['CaseID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('session', 'session', $sessionToken);
if($caseInfo['ApplicantUserID']==''){
	$chooseIndividual = $_SESSION['UserID'];
}else{
	$chooseIndividual = $caseInfo['ApplicantUserID'];
}
$hiddenF .= $linterface->GET_HIDDEN_INPUT('chooseIndividual', 'chooseIndividual', $chooseIndividual);
$htmlAry['hidden'] = $hiddenF;

echo $linterface->Include_Thickbox_JS_CSS();
echo $linterface->Include_AutoComplete_JS_CSS();
echo $linterface->Include_JS_CSS();
?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<script>
$(document).ready( function() {

	<?php if($editMode){ ?>
    	ajax_reload_saved_attachment();
    	ajax_financial_item(<?=$caseInfo['ApplicantGroupID']?>);
	<?php } ?>
	initUploadAttachment('<?=$pluploadContainerId?>','<?=$pluploadButtonId?>');

	var delayTimer;
	$('#budget').bind('input', function(){
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function() {
            // Will do after 0.5 s
            checkBudgetExceed();
            getBudgetRuleFile();
            if($('#Funding_Selection_Box_0').val() > 0){
                $('#Funding_Selection_Box_0_usingBudget').val($('#budget').val());
            }
        }, 500);
    });
});

var backLocation = 'index.php?p=mgmt.view';

var over_budget = false;

function checkForm(){
	$('.warnMsgDiv').hide();
	$('#FileWarnDiv').hide();
	if(!'<?=$skipBudget?>'){
		$('form#form1').attr('action', 'index.php?p=mgmt.case.insertConfrim');
	}else{
		$('form#form1').attr('action', 'index.php?p=mgmt.case.insertConfrimSkipBudget');
	}
	//ajax checking budget
	<?php  if($_PAGE['libPCM']->enableFundingSource()){?>
	$.ajax({
		type: "POST",
		url: "index.php?p=ajax.fundingSource.BudgetVaildChecking",
		data: $("form#form1").serialize(),
		async: false,
		success: function(ReturnJson){
			var json = JSON.parse(ReturnJson);
			if(json){
				canSubmit = false;
				var keyArr = Object.keys(json);
				for(var i=0;i<keyArr.length; i++){
					var msg = "<span class='tabletextrequire'>* "+json[keyArr[i]]+"</span>";
					$('#AlertMsg_Funding_Selection_Box_'+keyArr[i]).show();
					$('#AlertMsg_Funding_Selection_Box_'+keyArr[i]).html(msg);
				}
			}else{
				canSubmit = true;
// 				json
			}
			
		}
	});
	<?php }else{ ?>
	canSubmit = true;
	<?php }?>
	var isFocused = false;
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});

	if(!isSelectionBoxValid('groupID')){
		$('div#' + 'groupID' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}

	//check radio
	var applicantType = $('input[name=applicantType]:checked').val();
	if(!applicantType){
		$('div#' + 'applicantType' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}
	var categoryID = $('input[name=categoryID]:checked').val();
	if(!categoryID){
		$('div#' + 'categoryID' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}
	var itemID = $('input[name=itemID]:checked').val();
	if(!itemID){
		$('div#' + 'itemID' + 'EmptyWarnDiv').show();
		canSubmit = false;
	}

	// check file upload
	var warnDv = $('#FileWarnDiv');
	if(!jsIsFileUploadCompleted()) {
		warnDv.html('<?=$Lang['ePCM']['General']['jsWaitAllFilesUploaded']?>').show();
		canSubmit = false;
	}
	if(!'<?=$skipBudget?>'){
		//Check Budget must be INTEGER
		var budget = $('#budget').val();

		budget = budget.split(".");
		if((budget.length ) == 1){
	
			if(budget[0]%1==0){
				//do nothing
			}else{
				$('#budgetNotIntWarnDiv').show();
				canSubmit = false;
			}
			
			
		}else{
			
			if((budget[1].length ) < 3 && budget.length < 3){
				
				if(budget[0]%1==0 && budget[1]%1==0){
					//do nothing
				}else{
					$('#budgetNotIntWarnDiv').show();
					canSubmit = false;
				}
				
			}else{
					$('#budgetNotIntWarnDiv').show();
					canSubmit = false;
				}
		}
		<?php if(!empty($_SESSION['ePCM']['Setting']['General']['exceed_budget_percentage'])):?>
		if(over_budget){
			canSubmit = false;
			$('#budget').focus();
		}
		<?php endif;?>
	}
	return canSubmit;
}
function isSelectionBoxValid(id){
	var value = $('#'+id+' option:selected').val();
	if(value==''){
		return false;
	}else{
		return true;
	}
}

//Start: for admin only

function addUser(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['ChooseIndividual'] ?>', 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=600);
}
function onloadThickBox(){
	$('div#TB_ajaxContent').load(
			"index.php?p=mgmt.case.chooseIndividual", 
			{ 
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
				Init_JQuery_AutoComplete('UserSearch');
			}
		);
}
function getNotAvalUser(){
	//return empty array for template/add_user.php
	var notavalUser = [];
	
	return notavalUser;
}
function Add_Selected_User(userID,name){
	userID = userID.substring(1);
	addToChooseIndividual(userID,name);
}
function Add_Role_Member(){
	var userID = $('[name="AvalUserList[]"] option:selected').val();
	var name = $('[name="AvalUserList[]"] option:selected').text();
	addToChooseIndividual(userID,name);
}
function addToChooseIndividual(userID,name){
	$('#chooseIndividual').val(userID);
	$('#chooseIndividualSpan').html(name);	
	window.top.tb_remove(); 
}
//End: for admin only
function goSave(){
	if(checkForm()){
		$('form#form1').attr('action', 'index.php?p=mgmt.case.saveDraft');
		$('form#form1').submit();
	}
}
var canSubmit = true;
function goSubmit(){
// 	debugger;
	var isConfrim = true; 
	<?php if($currentStage>=1){?>
	//current stage>=1 => need start new case
	isConfrim = confirm("<?= $Lang['ePCM']['Mgmt']['Case']['Warning']['NeedStartNewCase'] ?>");
	if(!isConfrim){
		return;
	}
	<?php }?>

	
// 	var canSubmit = true;
// 	if(canSubmit == false){
// 		return false;
// 	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
// 	    alert(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
		canSubmit = true;
		return false;
	}
}
function ajax_financial_item(value){
	//reset check budget 
	checkBudgetExceed();
	
	$('#ajax_financial_item').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		type:"POST",
		url:"index.php?p=ajax.financialItem",
		data:{
			groupID: value,
			itemID: '<?=$caseInfo['ItemID'] ?>'
		},
		success: function(response){
			$('#ajax_financial_item').html(response);
			bindFundingMappingHandler();
		}
	});

	//hide budget statment
	$('#ajax_budget').html('');
	$('#ajax_budget_exceed').html('');
	
}
function bindFundingMappingHandler(){
	$('.financialItemRadio').click(function(){
		var fundingId = $(this).attr("data-fund");

		if(fundingId > 0){
			$('select#Funding_Selection_Box_0').val(fundingId);
		}else{
			$('select#Funding_Selection_Box_0').val(0);
		}
	});
}
function ajax_budget(itemID){
	//reset check budget 
	checkBudgetExceed();
	
	$('#ajax_budget').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		type:"POST",
		url:"index.php?p=ajax.financialItemBudget",
		data:{
			itemID: itemID
		},
		success: function(response){
			$('#ajax_budget').html(response);
		}
	});
}
function goCancel(){
	if(typeof backLocation != 'undefined'){
		ajax_recover_tempRemove();
		window.location = backLocation;
	}
	else{
// 		alert('please add a js global var backLocation');
	}
}

function ajax_recover_tempRemove(){
	postdata = $('form#form1').serialize()
	$.post(
			'index.php?p=file.recover',
			postdata,
			function(response){
			}
	);
}
// below are for plupload
var FileCount = 0;
var file_uploader = {};
function initUploadAttachment(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;
	
	file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'index.php?p=file.process&session=<?=$sessionToken?>',
			max_file_size : '200mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>
	    });
	    
	file_uploader.init();
	
	file_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	file_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	
    file_uploader.bind('UploadComplete', function(up, files) {
		var postData = $('form#form1').serialize();
		var fileCount = files.length;
		files.reverse();
		ajax_update_attachment(fileCount, postData, files);
	});
}

function ajax_update_attachment(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'index.php?p=file.attach.update',
			finalPostData,
			function(data){
				eval(data);
				if(file_uploader.removeFile) {
					$('#' + files[index].id).remove();
					file_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_attachment(numberLeft, postData, files);
				}
			}
		);
	}
}

// for hide original plupload file list, real file list is return from  ajax_update_attachment
function bindClickEventToElements()
{
	$('a').bind('click',afterClickedLinksButtons);
}
var isCloseWindow = true;
function afterClickedLinksButtons()
{
	isCloseWindow = false;
	setTimeout(function(){isCloseWindow=true;},1);
}

function Check_Total_Filesize(CheckNewOnly){
	return true;
}

function ajax_delete_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.attach.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_delete_saved_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function download_attachment(target){
	window.location = "index.php?p=file.download."+target;
}

function ajax_reload_saved_attachment(){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.caseList",
        data: {
			"session" : "<?=$sessionToken?>",
			"caseID"  : "<?=$caseInfo['CaseID']?>",
			"stageID" : "0",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function jsIsFileUploadCompleted(){
	var totalSelected = file_uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<file_uploader.files.length;i++) {
		if(file_uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	return totalDone == totalSelected;
}

function getBudgetRuleFile(){
    var budget = $('#budget').val();
    if(budget=='' || budget==null){budget='<?= $caseInfo['Budget'] ?>';}
    $('#templateBox').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
    $.ajax({
        type:"POST",
        url: "index.php?p=ajax.checkRuleApplicationForm",
        data: {
            "budget" : budget,
            "caseID" : "<?=$caseInfo['CaseID']?>",
        },
        success:function(data) {
            $('#templateBox').html(data);
        }
    });
}

function checkBudgetExceed(){
	var budget = $('#budget').val();
	var groupID = $('#groupID option:selected').val();
	var itemID = $('input[name="itemID"]:checked').val();
	if(!(budget>0&&groupID>0&&itemID>0)){
		//skip
		return;
	}else{
        $('#ajax_budget_exceed').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
		$.ajax({
	        type:"POST",
	        url: "index.php?p=ajax.checkExceedBudget",
	        data: {
				"budget" : budget,
				"groupID"  : groupID,
				"itemID" : itemID,
				},
	        success:function(data)
	        {
		        switch(data){
		        case 'OK':
			        $('#ajax_budget_exceed').html('');
			        over_budget = false;
			        break;
		        case 'GROUP':
		        	$('#ajax_budget_exceed').html('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group'] ?>');
		        	over_budget = true;
			        break;
		        case 'ITEM':
		        	$('#ajax_budget_exceed').html('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item'] ?>');
		        	over_budget = true;
			        break;
		        case 'GROUP:ITEM':
		        	$('#ajax_budget_exceed').html('<?= $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem']?>');
		        	over_budget = true;
			        break;
		        }
	        }
	      });
	}
}

function AddFundingSourceNewRow(){
	var TableBody = document.getElementById("fundingSourceTable").tBodies[0];
	var RowIndex = document.getElementById("fundingSourceNewBtn").rowIndex ;
// 	var id = "Funding_Selection_Box_"+RowIndex;
  	var row = TableBody.insertRow(RowIndex);
	var cell1 = row.insertCell(0);
	cell1.innerHTML = '<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">';
	$.ajax({
		type: "POST",
		url: "index.php?p=ajax.fundingSource.getSelectionBox",
		data: {
			"id": "Funding_Selection_Box_"+RowIndex
			},
		success: function(ReturnData){
			   cell1.innerHTML = ReturnData
		}
	});
}
</script>
<form name='form1' id='form1' method="POST">
<div class="table_board">
<?= $navigation ?>
<?=$htmlAry['WarningBox'] ?>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['CaseName'] ?></td>
			<td><?= $htmlAry['caseName'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['Category']?></td>
			<td><?= $htmlAry['categoryRadio'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Purpose'] ?></td>
			<td><?= $htmlAry['description'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['Group'] ?></td>
			<td><?= $htmlAry['groupSelect'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['FinancialItem'] ?></td>
			<td><div style="display:inline-block" id="ajax_financial_item"><?= $htmlAry['itemSelect'] ?></div> <span id="ajax_budget"></span> <?=$htmlAry['itemSelectWarning'] ?> </td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Budget']?></td>
			<td>
				<?= $htmlAry['budget'] ?> <span id="ajax_budget_exceed" style="color:red"></span>
				<br>
				<span class="tabletextremark"><?=$budget_notice ?></span>
			</td>
		</tr>
		<?php if($sys_custom['ePCM']['enableForcingToTenderOption'] && ($_PAGE['GET']['p'] == 'mgmt.new' || $_PAGE['GET']['p'] == 'mgmt.case.edit')):?>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['MustTender']?></td>
			<td>
				<?php echo $linterface->Get_Radio_Button('forceTender_yes', 'forceTender', 1, '', $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0) ?>
				<?php echo $linterface->Get_Radio_Button('forceTender_no', 'forceTender', 0, 1, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0) ?>
			</td>
		</tr>
		<?php endif;?>
		<?php  if($_PAGE['libPCM']->enableFundingSource()){?>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['FundingSource']?></td>
			<td>
			
				<table id='fundingSourceTable' width="100%" class="inside_form_table_v30 inside_form_table">
							<?=$htmlAry['FundingSource_SelectionBox']?>
					<tr id='fundingSourceNewBtn'>
						<td>
							<?=$htmlAry['FundingSource_AddBtn']?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php }?>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Document']?></td>
			<td >
                <div id="templateBox">
				<?=$templateBox?>
                </div>
				<?=$pluploadBtn?>
                <div id="<?=$pluploadContainerId?>"></div>
                <div id="<?=$pluploadFileListDivId?>"></div>
                <div id="<?=$deleteFileDivId?>"></div>
                <?=$fileWarningDiv?>
			</td>
		</tr>
        <?php if($sys_custom['ePCM']['mfs']['hideApplicantType']):?>
		    <tr style="display:none;">
        <?php else:?>
            <tr>
        <?php endif;?>

			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Applicant']?></td>
			<td><?= $htmlAry['applicantType'] ?></td>
		</tr>
		<?php if($_SESSION['ePCM']['isAdmin']){ ?>
		<tr>
			<td class="field_title"><?= $Lang['ePCM']['Mgmt']['Case']['ChooseIndividual'] ?></td>
			<td><?= $htmlAry['chooseIndividual'] ?></td>
		</tr>
		<?php } ?>
		<!-- 
		<tr>
			<td class="field_title"><?=$requiredSymbol?> aaa </td>
			<td>
				<?= $gNameTextBox ?>
				<?= $gNameWarn ?>
			</td>
		</tr>
		 -->
		<?=$sessionIdentifierHidden?>
	</tbody>
</table>
<?= $htmlAry['hidden']  ?>
<br style="clear:both;" />
	<div style="text-align:left">
		<?=$linterface->MandatoryField()?>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['darftBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>