<?php

$postVar = $_PAGE['views_data'];
$linterface = $_PAGE['libPCM_ui'];
$code = $postVar['code'];
$caseID = $postVar['caseID'];
$approvalRule = $postVar['approvalRule'];
$roleInfo = $postVar['roleInfo'];

$approvalGroupHeadAry = $postVar['approvalGroupHeadAry'];
$approvalRoleUserAssocAry = BuildMultiKeyAssoc($postVar['approvalRoleUserAry'],'RoleID',array(),0,1);

$displayApprovalUserArr = array();

foreach($approvalRule as $_roleId => $_approveNum){
	if($_roleId == '-1'){
		foreach((array)$approvalGroupHeadAry as $_userInfo){
            $userID = $_userInfo['UserID'];
			$_displayName = Get_Lang_Selection($_userInfo['ChineseName'], $_userInfo['EnglishName']);
			$_displayRole = ' ('.$Lang['ePCM']['Group']['GroupHead'].')';
			$displayApprovalUserArr[$userID] = $_displayName.$_displayRole;
		}
	}else{
		foreach((array)$approvalRoleUserAssocAry[$_roleId] as $_userInfo){
            unset($num);
            $userID = $_userInfo['UserID'];
			$_displayName = Get_Lang_Selection($_userInfo['ChineseName'], $_userInfo['EnglishName']);
			$_displayRole = ' ('.Get_Lang_Selection($roleInfo[$_roleId]['RoleNameChi'], $roleInfo[$_roleId]['RoleName']).')';
            if (isset($displayApprovalUserArr[$userID])){
                $displayApprovalUserArr[$userID].=$_displayRole;
            } else{
                $displayApprovalUserArr[$userID]=$_displayName.$_displayRole;
            }
		}
	}
}

$htmlAry['approvalUser'] = '<ol>';
$htmlAry['approvalUser'] .= '<li>'.implode('</li><li>',$displayApprovalUserArr).'</li>';
$htmlAry['approvalUser'] .= '<ol>';

### action buttons
$htmlAry['emailBtn'] = $linterface->Get_Action_Btn($Lang['ePCM']['Mgmt']['Case']['EmailNoti'], "button", "sendEmailNoti()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
if($_PAGE['allow_send_app']){
	$htmlAry['pushNotiBtn'] = $linterface->Get_Action_Btn($Lang['ePCM']['Mgmt']['Case']['PushNoti'], "button", "sendPushNoti()", 'draftBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
}
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

### hidden
$hiddenF='';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $postVar['caseID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('code', 'code', $postVar['code']);
?>
<script>
function sendNoti(method){
	$('.send_result').html('<?php echo $linterface->Get_Ajax_Loading_Image() ?>');
	$.ajax({
		'type':'POST',
		'url':'index.php?p=mgmt.case.sendNotification.'+method,
		'data':{
			'caseID':$('#caseID').val(),
			'code':$('#code').val()
		},
		'success':function(data){
                $('.send_result').html(data);
		}
	});
}

function sendEmailNoti(){
	sendNoti('email');
}
function sendPushNoti(){
	sendNoti('app');
}
function goCancel(){
	var backLocation = 'index.php?p=mgmt.caseDetials.<?= $caseID?>';
	if(typeof backLocation != 'undefined'){
		window.location = backLocation;
	}
	else{
		alert('please add a js global var backLocation');
	}
}
</script>
<div class="table_board eproc_detail">
	<div class="content_box">
		<table cellpadding="7" cellspacing="0" border="0" width="100%" class="info">
			<colgroup>
                <col width="120" />
                <col width="30" />
            </colgroup>
            <tbody valign="top">
            	<tr>
            		<th><?=$Lang['ePCM']['Mgmt']['Case']['ApplicationReceived']?></th>
            	</tr>
            	<tr>
            		<th><?=$Lang['ePCM']['Mgmt']['Case']['CaseCode']?>：　<?= $code ?></th>
            	</tr>
                <?php if(!empty($displayApprovalUserArr)){ ?>
            	<tr>
            		<td><?=$Lang['ePCM']['Mgmt']['Case']['ApplicationWillBeApprovedBy']?>：
            		<br><?= $htmlAry['approvalUser']  ?>
            		</td>
            	</tr>
                <?php }?>
            </tbody>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
        <?php if(!empty($displayApprovalUserArr)){ ?>
		<?=$htmlAry['emailBtn']?>
		<?=$htmlAry['pushNotiBtn']?>
        <?php }?>
		<?=$htmlAry['cancelBtn']?>
	</div>
    <div class="send_result" style="text-align:center;"></div>
</div>
<?php echo $hiddenF; ?>