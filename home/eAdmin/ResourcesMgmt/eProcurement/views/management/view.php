<?php 
/**
 * Date: 2016-11-09 Villa
 * add filter by academic year
 * 
 * Date: 2016-07-04	Kenneth
 * add search filter by quotation type
 */
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$caseInfoAry = $postVar['caseInfoAry'];
$keyword = $postVar['keyword'];
$currentStage = $postVar['currentStage'];
$currentType = $postVar['currentType'];
$currentStatus = $postVar['currentStatus'];
$AcademicYearfilterValue = $_POST['AcademicYearfilterValue']? $_POST['AcademicYearfilterValue']:$_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
$AcademicYearFilter = $_PAGE['libPCM_ui']->getSelectAcademicYear('AcademicYearID','OnChange="selectAcademicYearFilter(this)"', 1, 0, $AcademicYearfilterValue).'';



$btnAry[] = array('new', 'javascript: addNew();');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

//$htmlAry['contentTool'] = '<a href="javascript: addNew();" class="add_new pull_left"><strong>+</strong>New</a>';

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

### filter hidden field
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentStage', 'currentStage', $currentStage);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentType', 'currentType', $currentType);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentStatus', 'currentStatus', $currentStatus);
$htmlAry['hiddenField'] = $hiddenF;

### Supply Type Filter
if($sys_custom['ePCM_skipCaseApproval']){
   $approvalStageNo = 5;
} elseif($sys_custom['ePCM']['extraApprovalFlow']){
    $approvalStageNo = 1;
    $endorsementStageNo = 6;
    $endorsementStage=array($endorsementStageNo, $Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][$endorsementStageNo]);
}else{
    $approvalStageNo = 1;
    $endorsementStageNo = 5;
    $endorsementStage=array($endorsementStageNo, $Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][$endorsementStageNo]);
}
$stageAry = array(
    array('0',$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][0]),
    array($approvalStageNo,$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][$approvalStageNo]),
    array('2',$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][2]),
    array('3',$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][3]),
    array('4',$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][4])
);
if($endorsementStage){array_push($stageAry,$endorsementStage);}
$idArray = Get_Array_By_Key($stageAry,'0');
$showTextAry = Get_Array_By_Key($stageAry,'1');
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentStage, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage']['AllStage'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_stage')."\r\n";
### Quotation Type filter
$quotationTypeAry = array(array('N',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['N']),array('V',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['V']),array('Q',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['Q']),array('T',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['T']));
$idArray = Get_Array_By_Key($quotationTypeAry,'0');
$showTextAry = Get_Array_By_Key($quotationTypeAry,'1');
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentType, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['AllType'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_quotationType')."\r\n";
### Approval Status filter
if($sys_custom['ePCM_skipCaseApproval'] || $sys_custom['ePCM']['extraApprovalFlow']) {
    $EndorsementStatusAry = array(
        array('0',$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CasePendingForYourApproval']),
        array('1',$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CaseYouApproved']),
        array('-1',$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CaseYouRejected'])
    );
    $idArray = Get_Array_By_Key($EndorsementStatusAry,'0');
    $showTextAry = Get_Array_By_Key($EndorsementStatusAry,'1');
    $filter .= $_PAGE['libPCM_ui']->getFilter($idArray, $showTextAry, $currentStatus, $ParSelected = true,
            $ParOnChange = 'selectFilter(this);',
            $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['AllStatus'], $noFirst = 0,
            $CategoryTypeID = 0, $IsMultiple = 0, $ParDefaultSelectedAll = false,
            $DeflautName = 'sel_EndorsementStatus') . "\r\n";
}
//Case Summary Table

if(empty($caseInfoAry)){
	//debug_pr($caseInfoAry);
	$htmlAry['noRecord'] .= $linterface->Get_Case_Summary_UI();
}

foreach($caseInfoAry as $caseInfo){
	$htmlAry['caseSummary'] .= $linterface->Get_Case_Summary_UI($caseInfo);
}
?>
<script>
$(document).ready( function() {

});

function addNew(){
	location.href = "index.php?p=mgmt.new";
}
function selectFilter(obj){
	switch(obj.name){
		case 'sel_stage':
            if(obj.value!=6 && obj.value!=5){
                $('#currentStatus').val("");
            }
			$('#currentStage').val(obj.value);
			form1.submit();
		break;
		case 'sel_quotationType':
			$('#currentType').val(obj.value);
			form1.submit();
		break;
        case 'sel_EndorsementStatus':
            var StatusAry=[-1,0,1];
            if(StatusAry.indexOf(obj.value)!=-1){
                if($("#sel_stage option[value=6]").length != 0){
                    $('#currentStage').val(6);
                }else if($("#sel_stage option[value=5]").length != 0){
                    $('#currentStage').val(5);
                }
            }
            $('#currentStatus').val(obj.value);
            form1.submit();
        break;
	}
}
function selectAcademicYearFilter(obj){
	$('#AcademicYearfilterValue').val(obj.value);
	form1.submit();
}

</script>
<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
	</div>
	
	<div class="table_filter">
			<?=$AcademicYearFilter?><?= $filter ?>
	</div>
		<p class="spacer"></p>
		<br style="clear:both;" />
		<br style="clear:both;" />
	<?= $htmlAry['caseSummary'] ?>
	<?= $htmlAry['noRecord'] ?>
	<?= $htmlAry['hiddenField']?>
	<input type='hidden' id='AcademicYearfilterValue' name='AcademicYearfilterValue' value='<?=$AcademicYearfilterValue?>'>
	
</form>
