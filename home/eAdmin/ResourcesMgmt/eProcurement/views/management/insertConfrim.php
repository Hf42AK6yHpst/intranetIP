<?php
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$parVar = $_PAGE['views_data'];
$caseInfoAry = $parVar['caseInfoAry'];
$requiredSymbol = $linterface->RequiredSymbol();

$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseInfoAry[0]['CaseID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('action', 'action', 'sendCaseApproverInvitation');
$hiddenF .= $linterface->GET_HIDDEN_INPUT('data', 'data[]', '');
$htmlAry['hiddenField'] = $hiddenF;

?>
<script>
var backLocation = 'index.php?p=mgmt.view';
function checkForm(){
	$('form#form1').attr('action', 'index.php?p=mgmt.case.update');
	
	canSubmit = true;
	var isFocused = false;
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	//alert(canSubmit);
	return canSubmit;
	
}

</script>
<h1>THIS PAGE SHOULD BE USELESS!!!! Tell KENNETH IF YOU SEE IT!!!</h1>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['CaseName'] ?></td>
			<td><?= $caseInfoAry[0]['CaseName'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['Category']?></td>
			<td><?= $caseInfoAry[0]['CategoryName'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Purpose'] ?></td>
			<td><?= $caseInfoAry[0]['Description'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['Group'] ?></td>
			<td><?= $caseInfoAry[0]['GroupName'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?= $Lang['ePCM']['Mgmt']['Case']['FinancialItem'] ?></td>
			<td><?= $caseInfoAry[0]['ItemName'] ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Budget']?></td>
			<td><?= $linterface->displayMoneyFormat($caseInfoAry[0]['Budget']) ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Document']?></td>
			<td>
			<div id="DivUploadPanel">
				abc.docx (30MB)
			</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol?><?=$Lang['ePCM']['Mgmt']['Case']['Applicant']?></td>
			<td><?= ($caseInfoAry[0]['ApplicantType']=='I')? $caseInfoAry[0]['ApplicantUserName']:$caseInfoAry[0]['GroupName'] ?></td>
		</tr>
		<tr class="hidden">
			<td class="field_title"><?= $Lang['ePCM']['Mgmt']['Case']['Remarks'] ?></td>
			<td>Not yet mark(later do it)</td>
		</tr>
		
		<!-- 
		<tr>
			<td class="field_title"><?=$requiredSymbol?> aaa </td>
			<td>
				<?= $gNameTextBox ?>
				<?= $gNameWarn ?>
			</td>
		</tr>
		 -->
		<?=$sessionIdentifierHidden?>
	</tbody>
</table>
<?=$htmlAry['hiddenField']?>