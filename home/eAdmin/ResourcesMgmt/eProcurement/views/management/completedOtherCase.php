<?php
/*
 * Change Log: 
 * DateL 2016-11-11	Villa
 * - modified goReject() - fix non-group leader can reject case
 * - default sort by Record Date
 * 
 * Date: 2016-11-09 Villa
 * - add selectFilter() for filter
 * - add Academic Year Filter
 * - fix the sorting problem
 * 
 * Date: 2016-11-03 Villa 
 * - add goApprove() for the Approve case setting 
 * - add goReject() for the Reject case setting
 * - ui Improvement -> hide the approve/ reject btn if the user is not grouphead
 * 
 * Date: 2016-10-xx Villa 
 * -Open the file
 */
$postVar = $_PAGE['views_data'];
$sql = $postVar['sql'];
$filterValue = $_POST['filterValue']?$_POST['filterValue']:'';
$AcademicYearfilterValue = $_POST['AcademicYearfilterValue']? $_POST['AcademicYearfilterValue']:$_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
if(!empty($filterValue)){
	$sql .= " AND ipc.CurrentStage = '".$filterValue."'";
}
if(!empty($AcademicYearfilterValue)){
	$sql .= " AND ipc.AcademicYearID = '".$AcademicYearfilterValue."'";
}

$order = ($postVar['order'] == '') ? 0 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 5 : $postVar['field'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];

$columns = array();
$columns[] = array('CaseName',$Lang['ePCM']['Mgmt']['Case']['ItemName'],'18%');
$columns[] = array('CategoryName',$Lang['ePCM']['Category']['Category'],'14%');
$columns[] = array('Department',$Lang['ePCM']['Mgmt']['Case']['Group'],'14%');
$columns[] = array('ItemName',	$Lang['ePCM']['Mgmt']['Case']['FinancialItem'],'12%');
// $columns[] = array('ApplicationDate',$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'],'10%');
$columns[] = array('Budget',$Lang['ePCM']['Mgmt']['Case']['Result']['Amount'],'12%');
$columns[] = array('DealDate',$Lang['ePCM']['Mgmt']['Case']['Result']['RecordDay'],'12%');
$columns[] = array('Applicant',$Lang['ePCM']['Mgmt']['Case']['Applicant'],'10%');
$columns[] = array('RecordStatus',$Lang['ePCM']['Mgmt']['Case']['Status']['Status'],'10%');
// $columns[] = array('checkBox','checkBox','15%');

$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, true);

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('filterType', 'filterType', $filterType);
$htmlAry['hiddenField'] = $hiddenF;

$addBtnAry[] = array('new', 'javascript: addNew();');
$addBtn['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($addBtnAry);

$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');

$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');

//Villa 2016-11-03
$result = $db->getGroupHeadByUserID($_SESSION['UserID']);
if( (!empty($result)|| $auth->isAdmin()) && $_SESSION['ePCM']['Setting']['General']['MiscExpenditureApprove']){
	$dbTableBtnAry[] = array('approve','javascript: goApprove();');
	$dbTableBtnAry[] = array('reject','javascript: goReject();');
}
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);
### Supply Type Filter
$stageAry = array(array('2',$Lang['ePCM']['Mgmt']['Case']['Status']['Pending']),array('10',$Lang['ePCM']['Mgmt']['Case']['Status']['Approved'] ),array('1',$Lang['ePCM']['Mgmt']['Case']['Status']['Reject'] ));
$idArray = Get_Array_By_Key($stageAry,'0');
$showTextAry = Get_Array_By_Key($stageAry,'1');
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$filterValue, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Status']['AllStatus'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_stage')."\r\n";

$AcademicYearFilter = $_PAGE['libPCM_ui']->getSelectAcademicYear('AcademicYearID','OnChange="selectAcademicYearFilter(this)"', 1, 0, $AcademicYearfilterValue).'';
?>
<script>
function addNew(){
	location.href = "index.php?p=mgmt.newOther";
}

function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseOneOrMoreItem']?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=mgmt.newOther');
		$('form#form1').submit();
	}
// 	location.href = "index.php?p=mgmt.newOther";
}

function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRole'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=mgmt.deleteOther');
		$('form#form1').submit();
	}
}
function goApprove(){
	var checked = targetIDAry();
	var checked2 = targetIDAry2();
	var checked3 =  targetIDAry3();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] ?>");
		return;
	}else{
		if(checked2.length=='0'){
			if(checked3){
				$('form#form1').attr('action', 'index.php?p=mgmt.ApproveOther');
				$('form#form1').submit();
			}else{
				alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['ApproveRight'] ?>");
			}
		}else{
			alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['Approved'] ?>");
		}		
	}
}
function goReject(){
	var checked = targetIDAry();
	var checked3 =  targetIDAry3();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] ?>");
		return;
	}else{
		if(checked3){
			$('form#form1').attr('action', 'index.php?p=mgmt.RejectOther');
			$('form#form1').submit();
		}else{
			alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['RejectRight'] ?>");
		}
	}
		
}

function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function targetIDAry2(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		if($(this).attr('isapprove')){
			checked.push($(this).attr('isapprove'));
		}
		
	});
	return checked;
}
function targetIDAry3(){
	var checked = true;
	$('input[name=targetIdAry[]]:checked').each(function(){
		if($(this).attr('canapprove')){
			 checked = true;
		}else{
			checked =false;
		}
	});
	return checked;
}

function selectFilter(obj){
	$('#filterValue').val(obj.value);
	form1.submit();
}
function selectAcademicYearFilter(obj){
	$('#AcademicYearfilterValue').val(obj.value);
	form1.submit();
}

</script>
<?php echo $addBtn['contentTool']?>
<form id="form1" name="form1" method="POST" action="">


<p class="spacer"></p>
<br style="clear:both;" />
<br style="clear:both;" />
<div class="table_filter">
<?= $AcademicYearFilter?><?= $filter ?>
</div>
<?=$htmlAry['dbTableActionBtn']?>
<?= $libdbtable?>

<?= $htmlAry['hiddenField']?>
<input type='hidden' id='filterValue' name='filterValue' value='<?=$filterValue?>'>
<input type='hidden' id='AcademicYearfilterValue' name='AcademicYearfilterValue' value='<?=$AcademicYearfilterValue?>'>
</form>