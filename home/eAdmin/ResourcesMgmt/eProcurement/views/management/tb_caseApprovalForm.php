<?php
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];

$isCustApproval = $postVar['custApproval'];
$isApproved = $postVar['isApproved'];
$caseApprovalInfo = $postVar['caseApprovalInfo'];
$requiredSymbol = $linterface->RequiredSymbol();

if(!$isApproved){
	$approveStatus = array(array('-1',$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Reject']),array('1',$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approve']));
	$hideStatus ='';
}else{
	$approveStatus = array(array('1',$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved']));
	$hideStatus =' style="display:none" ';
}
$recordStatusSel = getSelectByArray($approveStatus, $tags='name="recordStatus" id="recordStatus"', $caseApprovalInfo['RecordStatus'], $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
foreach($approveStatus as $_value){
	if($caseApprovalInfo['RecordStatus']==$_value[0]){
		$checked = true;
	}
	$recordStatusRadio .= $linterface->Get_Radio_Button('recordStatus_'.$_value[0], 'recordStatus', $Value=$_value[0], $checked, $Class="", $Display=$_value[1], $Onclick="",$isDisabled=0).'&nbsp;&nbsp;';
	$checked = false;
}
$feedbackTA = $linterface->GET_TB_TEXTAREA('feedback', $caseApprovalInfo['Feedback'], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='');
if($isCustApproval){
	$htmlAry['submitBtn']=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="sumbitCustCaseApproval(".$caseApprovalInfo['ApproverUserID'].",".$caseApprovalInfo['ApprovalRoleID'].");", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");	
}else{
	$htmlAry['submitBtn']=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="sumbitCaseApproval(".$caseApprovalInfo['ApproverUserID'].",".$caseApprovalInfo['ApprovalRoleID'].");", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
}
$htmlAry['cancelBtn']=$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<div id="thickboxContainerDiv">
<div id="thickboxContentDiv">
<table class="form_table_v30">
	<tr <?=$hideStatus?> >
		<td class="field_title"><?=$requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['ApprovalStatus']?></td>
		<td><?=$recordStatusRadio?><?=$linterface->Get_Form_Warning_Msg('recordStatusEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['SelectApprovalStatus'], $Class='warnMsgDiv') ?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Feedback']?></td>
		<td><?=$feedbackTA?></td>
	</tr>
</table>
</div>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $htmlAry['submitBtn'] ?>
		<?= $htmlAry['cancelBtn'] ?>
	<p class="spacer"></p>
</div>
<div id="ajax_case_approval"></div>
</div>
<div id="ajax_cust_case_approval"></div>
</div>