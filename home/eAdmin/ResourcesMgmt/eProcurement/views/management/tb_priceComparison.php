<?php 
################# Change Log ###############
#
#	Date:	20160715	Kenneth
#	- Lang Fix for supplier name
#
############################################

$linterface = $_PAGE['libPCM_ui'];
$caseID = $_PAGE['POST']['caseID'];
$quotations = $_PAGE['view_data']['quotations'];
$priceItem = $_PAGE['view_data']['priceItem'];

$priceItem = BuildMultiKeyAssoc($priceItem,array('PriceItemID','QuotationID'));

// debug_pr($priceItem);
// debug_pr($quotations);

$htmlAry['hiddenField'] = $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);



$htmlAry['submitBtn']=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="submitPriceComparison();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$htmlAry['cancelBtn']=$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$supplierCounter = 0;
?>

<script>
var newItemCounter = 0;
function addPriceItem(itemName){
	var id = '';
    $('#mtable thead tr>th:last').before($('<th class="new_item_'+ (newItemCounter)+'">'));

    // $('<th class="new_item_'+ (newItemCounter)+'">').insertBefore($('#mtable thead tr[2]'));
    // $('<td class="new_item_'+ (newItemCounter)+'">').insertBefore($('#mtable tbody tr[2]'));
    $('#mtable thead tr>th:nth-last-child(2)').html(itemName+'<input type="hidden" name="newItemName[]" value="'+itemName+'"><a href="javascript:removeNew('+newItemCounter+');" class="delete_dim"></a>');
	$('#mtable tbody tr').each(function(){
        id = ($(this).attr('id')).split('_')[1];
        $(this).children('td:last').before($('<td class="new_item_'+ (newItemCounter)+'"><input type="text"  onchange="javascript:quotationSumUpdateByQuotationID('+id+')" name="price[new]['+id+'][]" style="width: 100%"/>'));


		});
	newItemCounter++;
}
function addNewItemName(){
	var name = $('#new_name').val();
	if(name!==''){
		addPriceItem(name);
	}
}
function removeNew(num){
	remove('new_item_'+num);
}
function removeExist(priceItemID){
	remove('exist_item_'+priceItemID);
}
function remove(class_name){
	$('.'+class_name).remove();
}
function markRemoveExisting(priceItemID){
	removeExist(priceItemID);
	$('#hidden_area').append('<input type="hidden" name="removePriceItemID[]" value="'+priceItemID+'" >');
    $('input[class="total"]').each(function () {
        quotationSumUpdateByQuotationID(this.attributes["id"].value);
    });
}
function quotationSumUpdateByQuotationID(QuotationID) {
    var sum=0;
    var itemPrice=0;
    $('input[name^="price[existing]['+QuotationID+']"]').each(function () {
        if(this.attributes["name"].value!='price[existing]['+QuotationID+'][sum]'){
            if(!$(this).val()){
                itemPrice=0;
            }else{
                $(this).val(Math.abs($(this).val()));
                itemPrice=parseFloat($(this).val());
            }
            sum+=itemPrice;
        }
    });
    $('input[name^="price[new]['+QuotationID+']"]').each(function () {
        if(this.attributes["name"].value!='price[existing]['+QuotationID+'][sum]'){
            if(!$(this).val()){
                itemPrice=0;
            }else{
                $(this).val(Math.abs($(this).val()));
                itemPrice=parseFloat($(this).val());
            }
            sum+=itemPrice;
        }
    });
    $("input[name='price[existing]["+QuotationID+"][sum]']").val(sum);
    // console.log($("input[name='price[existing]["+QuotationID+"][sum]']").val(sum));
}
</script>
<style>
.delete_dim{
background:url(../../../../images/2009a/icon_table_row_tool.gif) no-repeat; height:20px; width:20px; display: inline-block; vertical-align:middle;background-position:-40px -20px;
}
a.delete_dim:hover {
	background:url(../../../../images/2009a/icon_table_row_tool.gif);background-position:0px -20px
}

</style>
<div id="thickboxContainerDiv" class="edit_pop_board">

<form id="ajax_form" method="post" enctype="multipart/form-data">

<table class="form_table_v30" >
	<tr>
		<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['AddNewPriceItem'] ?></td>
		<td><input type="text" id="new_name"><input type="button" value="+" onclick="addNewItemName();"></td>
	</tr>
</table>

<table class="form_table_v30" id="mtable">

	<colgroup>
		<col width="200px" />
	</colgroup>
	<thead>
		<tr style="text-align: left;">
			<th><?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'] ?></th>
			<?php foreach((array)$priceItem as $_priceItem_ID=>$_item){ ?>
				<th class="exist_item_<?=$_priceItem_ID ?>"><?=$_item[key($_item)]['PriceItemName'] ?><a href="javascript:markRemoveExisting(<?=$_priceItem_ID ?>);" class="delete_dim"></a></th>
			<?php } ?>
            <th><?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Total'] ?></th>
		</tr>
	</thead>
    <tbody>
    
    	<?php foreach($quotations as $_quotation){
            $sum=0;?>
    	<tr  id="row_<?= $_quotation['QuotationID'] ?>">
    		<td><?= Get_Lang_Selection($_quotation['SupplierNameChi'],$_quotation['SupplierName']) ?></td>
    		<?php foreach((array)$priceItem as $_priceItem_ID=>$_item){
    		    $sum+= abs($_item[$_quotation['QuotationID']]['Price']);?>
				<td class="exist_item_<?=$_priceItem_ID ?>"><input type="text" name="price[existing][<?= $_quotation['QuotationID'] ?>][<?=$_priceItem_ID ?>]" value="<?=$_item[$_quotation['QuotationID']]['Price'] ?>" onchange="javascript:quotationSumUpdateByQuotationID(<?= $_quotation['QuotationID'] ?>)" style="width: 100%"/></td>
			<?php } ?>
            <td class="sum"><input id="<?=$_quotation['QuotationID']?>" class="total" disabled type="text" name="price[existing][<?= $_quotation['QuotationID'] ?>][sum]" value="<?=$sum ?>" style="width: 100%"/></td>
		</tr>
    	
    	<?php } ?>
    	
		
 	</tbody>
	
</table>

<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $htmlAry['submitBtn'] ?>
		<?= $htmlAry['cancelBtn'] ?>
	<p class="spacer"></p>
</div>
<div id="ajax_price_comparison"></div>
<div id="hidden_area">
<?=$htmlAry['hiddenField']?>
</div>

</form>
</div>