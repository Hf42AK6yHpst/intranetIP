<?php
/*
 * Date: 2017-06-22 Villa
 * - avoid popup window being blocked by the browser
 *  
 * Date: 2017-04-18 Villa #E114398 
 * - add editCase2() - for the entrance of editmode2
 * Date: 2017-04-13 Villa #E114398 
 * - add PrintApplcationFrom() - for print btn
 *  
 * Date: 2016-10-12 Villa
 * - modified finishStage3() - add params remarks 
 * 
 * Date: 2016-10-07 Omas
 * - added confirm for js sumbitCaseApproval()
 * 
 * Date: 2016-07-06	Kenneth
 * - edit js, add email preview function
 * 
 * Date: 2016-07-05	Kenneth
 * - edit js, add supplier search function
 * 
 * Date: 2016-07-04	Kenneth
 * - edit js, add edit Case code and name function
 */
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$caseAry = $postVar['caseAry'];
$gs = $postVar['generalSetting'];
$ruleID = $postVar['ruleID'];

$htmlAry['caseDetails']=$linterface->Get_Case_Detail_UI($caseAry,$gs);
#E114398 
include_once($intranet_root."/includes/json.php");
$json = new JSON_obj();
$caseAry_JSON = $caseAry;
unset($caseAry_JSON['fundingList_arr']);

$caseAry_JSON = $json->encode($caseAry_JSON);
$caseAry_JSON = str_replace("'", "\'", $caseAry_JSON);

// debug_pr($caseAry);
### navigation
if($caseAry['IsDeleted']||$caseAry['CurrentStage']==10){
	//if the case is deleted or finished -> set to finished view

	if(!$caseAry['IsDeleted']){
		$navigationAry[] = array($Lang['ePCM']['ManagementArr']['FinishedViewArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=mgmt.finished\';');
	}else{
		$navigationAry[] = array($Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Terminated'], 'javascript: window.location=\'index.php?p=mgmt.finished.terminated\';');
	}
}else{
	$navigationAry[] = array($Lang['ePCM']['ManagementArr']['ViewArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=mgmt.view\';');
}

// $navigationAry[] = array($roleName);
$navigationAry[] = array($caseAry['CaseName']);
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);
// debug_pr($caseAry['QuotationType']);
echo $linterface->Include_Thickbox_JS_CSS();
?>
<!--    <script language="JavaScript" src="/templates/jquery/jquery-3.3.1.min.js"></script>-->
<!--<script language="JavaScript" src="/templates/jquery/jquery.scrollTo_2.1.2.min.js"></script>-->
<script>

$( document ).ready(function() {
        auto_scroll();
	<?php if($sys_custom['ePCM']['SummaryDocCustTemplate'] == 'mfs'):?>
        $('#printMfsForm').click(function(){
            downloadDocument('mfsConfirmationForm');
        });
    <?php endif; ?>
    var remarks_2 = localStorage.getItem(<?= $postVar['caseAry']['CaseID'] ?>+"_remarks_2");
    if (remarks_2 !== null) $('#remarks_2').val(remarks_2);
});

function auto_scroll(){
	var currentStage = <?=$caseAry['CurrentStage']?>;
	if(currentStage>0&&currentStage<=6){
		Scroll_To_Element('step_box_'+currentStage);
	}
}

$('html').click(function() {
	if(!$(event.target).is('.selReplyStatus')){
		resetEditReplyStatus();
	}
	if(!$(event.target).is('#datePickerQuotationEndDate_div')){
		//resetDatePicker('QuotationEndDate');
	}
	if(!$(event.target).is('#datePickerTenderOpeningStartDate_div')){
		resetDatePicker('TenderOpeningStartDate');
	}
	if(!$(event.target).is('#datePickerTenderOpeningEndDate_div')){
		resetDatePicker('TenderOpeningEndDate');
	}
	if(!$(event.target).is('#datePickerTenderApprovalStartDate_div')){
		resetDatePicker('TenderApprovalStartDate');
	}
	if(!$(event.target).is('#datePickerTenderApprovalEndDate_div')){
		resetDatePicker('TenderApprovalEndDate');
	}
	if(!$(event.target).is('.selContactMethod')){
		resetEditContactMethod();
	}
});

/**
 * Start: Basic Info
 */
function clickQuotationDoc(quotationID){
	alert('quotationID: '+ quotationID);
}
function editCase(){
	$('form#form1').attr('action', 'index.php?p=mgmt.case.edit');
	$('form#form1').submit();
}
function editCase2(){
	$('form#form1').attr('action', 'index.php?p=mgmt.case.edit2');
	$('form#form1').submit();
}
function goToCase(caseID){
	$(location).attr('href', 'index.php?p=mgmt.caseDetials.'+caseID);
}

function terminationThickBox(){
	if(confirm("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['Termination']?>")){
		load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['Termination']?>', 'onloadTBTermination();', inlineID='', defaultHeight=200, defaultWidth=800);
	}
}
function undoTermination(){

	if(confirm('Are you sure to undo termination?')){
		$.ajax({
			url: "index.php?p=mgmt.case.undoDelete",
			type: "POST",
			data: { caseID: <?= $caseAry['CaseID'] ?>  },
			dataType: "html",
			success: function(response){
				location.reload();
			}
		});
	}
}
function onloadTBTermination(){
	$('div#TB_ajaxContent').load(
			"index.php?p=mgmt.termination.form", 
			{ 
				caseID: <?= $caseAry['CaseID'] ?>
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}

function goTermination(){
	var delReason =  $('#delReason').val();
	if(delReason==''){
		$('#reasonEmptyWarnDiv').show();
		return;
	}
	if(confirm("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['Termination']?>")){

		//check reason is exist
		
		var ajaxFormData = $('form#ajax_form').serialize();	
		$.post(
				"index.php?p=mgmt.case.delete", 
				ajaxFormData,
				function(ReturnData)
				{
					$('div#ajax_termination').html(ReturnData);
					if(ReturnData=='Saved'){
						js_Hide_ThickBox();
						window.scrollTo(0, 0);
						location.reload();
					}
				}
			);

	}
}
function goSubmitDraft(){
	if(confirm('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['SubmitDraft']?>')){

		var ajaxFormData = $('form#form1').serialize();
		$.post(
				"index.php?p=mgmt.case.insertConfrim", 
				ajaxFormData,
				function(ReturnData)
				{
					window.location.href="index.php?p=mgmt.case.showConfirm.<?= $caseAry['CaseID'] ?>";
				}
			);

	}
}
function editCaseCodeAndName(){
	$('#ajax_caseCode').val();
	$('#codeAndName_show').hide();
	$('#codeAndName_edit').show();
	$('#warningDiv_codeAndName').html('');
}
function onClickCancelCodeAndName(){
	$('#codeAndName_show').show();
	$('#codeAndName_edit').hide();
}
function onClickCodeAndName(){
	//check code duplicated
	var code = $('#codeEdit').val().trim();
	var name = $('#nameEdit').val().trim();

	var isCodeValid = codeChecking(code);
	var isNameValid = nameChecking(name);
	var isAllValid = true;
	if(!isCodeValid){
		isAllValid = false;
		$('#warningDiv_codeAndName').html('<?= $Lang['ePCM']['Mgmt']['Case']['Warning']['Duplicated']['Code'] ?>');
		return;
	}
	if(!isNameValid){
		isAllValid = false;
		$('#warningDiv_codeAndName').html('Name invalid');
		return;
	}
	if(isAllValid){
		$.ajax({
			'type':'POST',
			'url':'index.php?p=mgmt.codeAndName.update',
			'data':{
				'name':name,
				'code':code,
				'caseID': <?= $caseAry['CaseID'] ?>
			},
			'success':function(data){
				if(data=='SUCCESS'){
					onClickCancelCodeAndName();
					refreshName();
					refreshCode();
				}else{
					$('#updateFailed_codeAndName').html('<?= $Lang['ePCM']['Mgmt']['Case']['UpdateFailed'] ?>');
					setTimeout(function () {
				       $('#updateFailed_codeAndName').html('');
				    }, 2000);
					onClickCancelCodeAndName();
				}
			},
			'async':false,
		});
	}
}
function codeChecking(code){
	//check duplicate code
	var isValid = false;
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'5',
			'value':code,
			'excluded_value':'<?php echo addslashes($caseAry['Code']); ?>',
		},
		'success':function(data){
			if(data=='DUPLICATED'){
				isValid = false;
			}else{
				isValid = true;
			}
		},
		'async':false
	});
	return isValid;
}
function nameChecking(name){
	//no checking right now
	return true;
}
function refreshCode(){
	$('#ajax_caseCode').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?p=ajax.caseCode.refresh",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>},
		  dataType: "html",
		  success: function(response){
			  $('#ajax_caseCode').html(response);
		  }
		});
}
function refreshName(){
	$('#ajax_caseName').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?p=ajax.caseName.refresh",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>},
		  dataType: "html",
		  success: function(response){
			  $('#ajax_caseName').html(response);
		  }
		});
}
/**
 * End: Basic Info
 */


/**
*	Start: Case Approval
*/
function editCaseApproval(ApproverUserID,ApproverRoleID){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['CaseApproval']?>', 'onloadTBCaseApproval('+ApproverUserID+','+ApproverRoleID+');', inlineID='', defaultHeight='220', defaultWidth=800);
}
function onloadTBCaseApproval(ApproverUserID,ApproverRoleID) {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.caseApproval.form", 
		{ 
			approverUserID: ApproverUserID,
			approverRoleID: ApproverRoleID,
			caseID: <?= $caseAry['CaseID'] ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
function sumbitCaseApproval(ApproverUserID, AppproverRoleID){

	var recordStatus = $('input[name="recordStatus"]:checked').val();
	
	var feedback = $('#feedback').val();

	if( recordStatus == -1){
		if(!confirm('<? echo $Lang['ePCM']['Mgmt']['Case']['Warning']['ConfirmReject']?>')){
			return false;
		}
	} 
	
	//Checking
	if(!(recordStatus=='1'||recordStatus=='-1')){
		$('#recordStatusEmptyWarnDiv').show();
	}else{
		$('div#ajax_case_approval').load(
			"index.php?p=mgmt.caseApproval.update", 
			{ 
				caseID: <?= $caseAry['CaseID'] ?>,
				approverUserID: ApproverUserID,
				approverRoleID: AppproverRoleID,
				recordStatus: recordStatus,
				feedback: feedback
			},
			function(ReturnData) {
				$('div#ajax_case_approval').html(ReturnData);
				if(ReturnData=='Saved'){
					js_Hide_ThickBox();
					location.reload();
				}
			}
		);
	}
}
/**
*	End: Case Approval
*/

/**
*	Start: Cust Case Approval
*/
function editCustCaseApproval(ApproverUserID,ApproverRoleID){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['CaseApproval']?>', 'onloadTBCustCaseApproval('+ApproverUserID+','+ApproverRoleID+');', inlineID='', defaultHeight='220', defaultWidth=800);
}
function onloadTBCustCaseApproval(ApproverUserID,ApproverRoleID) {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.custCaseApproval.form", 
		{ 
			approverUserID: ApproverUserID,
			approverRoleID: ApproverRoleID,
			caseID: <?= $caseAry['CaseID'] ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
function sumbitCustCaseApproval(ApproverUserID, AppproverRoleID){

	var recordStatus = $('input[name="recordStatus"]:checked').val();
	
	var feedback = $('#feedback').val();

	if( recordStatus == -1){
		if(!confirm('<? echo $Lang['ePCM']['Mgmt']['Case']['Warning']['ConfirmReject']?>')){
			return false;
		}
	} 
	//Checking
	if(!(recordStatus=='1'||recordStatus=='-1')){
		$('#recordStatusEmptyWarnDiv').show();
	}else{
		$('div#ajax_cust_case_approval').load(
			"index.php?p=mgmt.custCaseApproval.update", 
			{ 
				caseID: <?= $caseAry['CaseID'] ?>,
				approverUserID: ApproverUserID,
				approverRoleID: AppproverRoleID,
				recordStatus: recordStatus,
				feedback: feedback
			},
			function(ReturnData) {
				$('div#ajax_cust_case_approval').html(ReturnData);
				if(ReturnData=='Saved'){
					js_Hide_ThickBox();
					location.reload();
				}
			}
		);
	}
}
/**
*	End: Cust Case Approval
*/

/**
*	Start: Send Invitation / Tender
*/
function addNewQuote(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['Supplier']['AddInvitation']?>', 'onloadAddSupplier();', inlineID='', defaultHeight=400, defaultWidth=800);
	onChangeSupplierType('');
}
function onloadAddSupplier() {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.addSupplier.form", 
		{ 
			caseID: <?= $caseAry['CaseID'] ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
function submitAddSupplier(){
	var supplierIDAry = targetIDAry();
// 	$('#supplierSel option:selected').each(function(i,selected){
// 		supplierIDAry[i]=$(selected).val();
// 	});
	if(!(supplierIDAry.length>0)){
		$('#supplierIDEmptyWarnDiv').show();
	}else{
		$('div#ajax_add_supplier').load(
			"index.php?p=mgmt.supplier.add", 
			{ 
				caseID: <?= $caseAry['CaseID'] ?>,
				supplierIDAry: supplierIDAry.join(',')
			},
			function(ReturnData) {
				$('div#ajax_add_supplier').html(ReturnData);
				if(ReturnData=='Saved'){
					js_Hide_ThickBox();
					location.reload();
				}
			}
		);
	}
}
//upload document
function goUploadInvitationDocument(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['UploadDocument']?>', 'onloadUploadInvitationDocumentForm()', inlineID='', defaultHeight='250', defaultWidth=800);
}
function onloadUploadInvitationDocumentForm() {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.uploadInvitationDoc.form", 
		{ 
			caseID: '<?= $caseAry['CaseID'] ?>',
            ruleID: '<?= $ruleID ?>'
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
//upload final doc
function goUploadFinalDocument(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['UploadDocument']?>', 'onloadUploadFinalDocumentForm()', inlineID='', defaultHeight='250', defaultWidth=800);
}
function onloadUploadFinalDocumentForm() {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.uploadFinalDoc.form", 
		{ 
			caseID: <?= $caseAry['CaseID'] ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

//Edit Contact Method
function resetEditContactMethod(){
	$('.selContactMethod').hide();
	$('.ajax_edit_contact_method').css('display', 'inline-block');
	$('.edit_icon_contact_method').css('display', 'inline-block');
}
function editContactMethod(quotationID){
	resetEditContactMethod();
	$('#selContactMethod_'+quotationID).show();
	$('#ajax_edit_contact_method_'+quotationID).hide();
	$('#edit_icon_contact_method_'+quotationID).hide();
}
function saveContactMethod(quotationID){
	resetEditContactMethod();
	$('#ajax_edit_contact_method_'+quotationID).html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var contactMethod = $('#selContactMethod_'+quotationID).val();
	var textToShow = '';
	$.ajax({
		url: "index.php?p=mgmt.contactMethod.update",
		type: "POST",
		data: { quotationID : quotationID, contactMethod:contactMethod, caseID: <?= $caseAry['CaseID'] ?>  },
		dataType: "html",
		success: function(response){
			if(response=='success'){
				switch(contactMethod){
                case 'R':
                    textToShow = '<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['RegisteredLetter']?>';
                    break;
				case 'E':
					textToShow = '<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Email']?>';
					break;
				case 'P':
					textToShow = '<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone']?>';
					break;
				case 'F':
					textToShow = '<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax']?>';
					break;
				case 'I':
					textToShow = '<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['InPerson']?>';
					break;	
				}
			}else{
			}
			$('#ajax_edit_contact_method_'+quotationID).html(textToShow);
		}
	});
}

//Edit reply status
function editReplyStatus(quotationID){
	resetEditReplyStatus();
	$('#selReplyStatus_'+quotationID).show();
	$('#ajax_edit_reply_status_'+quotationID).hide();
	$('#edit_icon_reply_status_'+quotationID).hide();
}
function resetEditReplyStatus(){
	$('.selReplyStatus').hide();
	$('.ajax_edit_reply_status').css('display', 'inline-block');
	$('.edit_icon_reply_status').css('display', 'inline-block');
}
function saveReplyStatus(quotationID){
	$('#ajax_edit_reply_status_'+quotationID).html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var replyStatus = $('#selReplyStatus_'+quotationID).val();
    var lastReplyDate = $('#lastInputReplyDate_'+quotationID).val();
	var textToShow = '';
	$.ajax({
	  url: "index.php?p=mgmt.quoteReplyStatus.update",
	  type: "POST",
	  data: { quotationID : quotationID, replyStatus:replyStatus, inputReplyDate:lastReplyDate, caseID: <?= $caseAry['CaseID'] ?>  },
	  dataType: "html",
	  success: function(response){
			//location.reload();
		  resetEditReplyStatus();
			if(response!=''){
				switch(replyStatus){
				case '1':
					textToShow = '<span class="accept" value="1"><?=$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Accepted']?></span>';
					$('#icon_td_'+quotationID+' span').attr('class', 'icon_accept');
					refreshNotifiedUser(quotationID);
                    $('#edit_icon_reply_Date_'+quotationID).show();
                    resetInputReplyDate(quotationID);
					break;
				case '0':
					textToShow = '<span class="grey_text" value="0" ><?=$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Pending']?></span>';
					$('#icon_td_'+quotationID+' span').attr('class', 'icon_pending');
                    $('#ajax_replyDate_'+quotationID).html('<span class="grey_text">-</span>');
                    resetInputReplyDate(quotationID);
                    $('#edit_icon_reply_Date_'+quotationID).hide();
					break;
				case '-1':
					textToShow = '<span class="reject" $OnDatePickSelectedFunctionvalue="-1"><?=$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Rejected']?></span>';
					$('#icon_td_'+quotationID+' span').attr('class', 'icon_reject');
					refreshNotifiedUser(quotationID);
                    $('#edit_icon_reply_Date_'+quotationID).show();
                    resetInputReplyDate(quotationID);
					break;
				}
			}else{
				textToShow = 'failed';
			}
		  $('#ajax_edit_reply_status_'+quotationID).html(textToShow);
		  
	  }
	});
}

//Edit input reply date
function editInputReplyDate(quotationID){
    $('.selInputReplyDate').hide();
    $('#selInputReplyDate_'+quotationID+'_div').show();
    $('.ajax_replyDate').css('display', 'inline-block');
    $('#ajax_replyDate_'+quotationID).hide();
    // $('#edit_icon_reply_Date_'+quotationID).hide();
}
function resetInputReplyDate(quotationID){
    $('.selInputReplyDate').hide();
    $('.ajax_replyDate').css('display', 'inline-block');
    // $('#ajax_replyDate'+quotationID).show();
    $('#edit_icon_reply_Date_'+quotationID).show();


}
function saveInputReplyDate(quotationID){
    resetInputReplyDate(quotationID);
    $('#ajax_replyDate_'+quotationID).html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
    var inputReplyDate = $('#selInputReplyDate_'+quotationID).val();
    var textToShow='';
    $.ajax({
        url: "index.php?p=mgmt.quoteInputReplyDate.update",
        type: "POST",
        data: {quotationID: quotationID, inputReplyDate: inputReplyDate, caseID: <?= $caseAry['CaseID'] ?>  },
        dataType: "html",
        success: function (response) {
            if (response != '') {
                textToShow = inputReplyDate;
                $('#ajax_replyDate_' + quotationID).html(textToShow);
                $('#edit_icon_reply_Date_'+quotationID).show();
            } else {
                textToShow = '<span class="reject">failed</span>';
                $('#ajax_replyDate_' + quotationID).html(textToShow);
                $('#edit_icon_reply_Date_'+quotationID).hide();
            }
        }
    });
}
//Delete Quotes
function deleteQuotes(quotationID){
	if(confirm('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['DeleteQuotation']?>')){
		$.ajax({
			  url: "index.php?p=mgmt.quotation.delete",
			  type: "POST",
			  data: { quotationID : quotationID, caseID: <?= $caseAry['CaseID'] ?>  },
			  dataType: "html",
			  success: function(response){
				  location.reload();
			  }
			});
	}
}

//Send Invitation
function sendEmailInvitation(){
	$('.sendEmailInvitation').attr({'disabled':'disabled'}).val('<?php echo $Lang['ePCM']['Mgmt']['Case']['Sending'];?>');
	$('.warnMsgDiv').hide();
	$.ajax({
		'type':'POST',
		'url':'index.php?p=mgmt.sendEmailInvitation',
		'data':{
			'CaseID':'<?php echo $caseAry['CaseID']; ?>',
		},
		'success':function(data){
			var count_success=0;
			var array_quotation_id=data.split(',');
			
			for(var i=0;i<array_quotation_id.length;i++){
				var obj_select_quotation=$('#selContactMethod_'+array_quotation_id[i]);
				console.log(obj_select_quotation);
				if(obj_select_quotation.length<1){
				}else{
					obj_select_quotation.val('E');
					saveContactMethod(array_quotation_id[i]);
					count_success+=1;
				}
			}
			if(count_success==0 || array_quotation_id.length<1){
				$('#NoEmailWarnDiv').show();
			}else{
				$('#EmailSentWarnDiv').show();
				
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1;
				var yyyy = today.getFullYear();
				if(dd<10) {
				    dd='0'+dd
				}
				if(mm<10) {
				    mm='0'+mm
				}
				$('#latest_email_sent_date').html(yyyy+'-'+mm+'-'+dd);
				$('#div_latest_email_sent_date').show();
			}
			$('.sendEmailInvitation').removeAttr('disabled').val('<?php echo $Lang['ePCM']['Mgmt']['Case']['Re-send Invitation Email'];?>');
		}
	});
}
function previewEmail(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Email']['Preview']?>', 'onloadTBPreview();', inlineID='', defaultHeight=350, defaultWidth=800);
}
function onloadTBPreview(){
	$('div#TB_ajaxContent').load(
			"index.php?p=mgmt.previewEmail.form", 
			{ 
				caseID: <?= $caseAry['CaseID'] ?>
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}
//Notification
function resetNotification(){
	$('#Notification_div').css('display', 'inline-block');
	$('#NotificationEdit').show();
	$('#Notification_edit_div').hide();
}
function toggleEnableNotification(value){
// 	alert('toggleEnableNotification: '+value);
	if(value==1){
		$('#datePickerNotificationDate_div').css('display', 'inline-block');
	}else{
		$('#datePickerNotificationDate_div').hide();
	}
}
function editNotification(){
	if($('#QuotationEndDate_div').html()==''){
		alert('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['InputEndDateFirst']?>');
		return;
	}
	$('#Notification_div').hide();
	$('#NotificationEdit').hide();
	$('#Notification_edit_div').css('display', 'inline-block');
}
function sumbitNotiSchedule(){
// 	alert('sumbitNotiSchedule');
	var isEnabled = $('input[name="notification"]:checked').val();
	if(isEnabled==1){
		var scheduleDate = $('#datePickerNotificationDate').val();
		var hour = $('#timePickerNotificationDate_hour').val();
		var min = $('#timePickerNotificationDate_min').val();
		var sec = $('#timePickerNotificationDate_sec').val();
		var time = pad(hour,2)+':'+pad(min,2)+':'+pad(sec,2);
		var scheduleDateTime = scheduleDate+' '+time;
	}
	$.ajax({
		  url: "index.php?p=mgmt.notificationScheduleDate.save",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>, scheduleDateTime: scheduleDateTime, stageID: 2, isEnabled:isEnabled },
		  dataType: "html",
		  success: function(response){
			  var textToShow = '';
			  if(isEnabled==1){
				  
				  textToShow += scheduleDateTime;
				  refreshNotifiedUser();
			  }else{
				  textToShow += '<?=$Lang['ePCM']['General']['Disable']?>';
			  }
			  $('#Notification_div').html(textToShow);
			  
			  resetNotification();
			  refreshNotiUser();
		  }
		});
	
	
}
function refreshNotifiedUser(quotationID){
	$('#ajax_replyDate_'+quotationID).html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?p=ajax.quotaionReplyDate.refresh",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>,quotationID:quotationID},
		  dataType: "html",
		  success: function(response){
			  $('#ajax_replyDate_'+quotationID).html(response);
              $('#selInputReplyDate_'+quotationID).val(response);
              $('#lastInputReplyDate_'+quotationID).val(response);
		  }
		});
}
function refreshQuotationReplyDate(quotationID){
	$('#ajax_notified_user').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?p=ajax.notificationUser.refresh",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>},
		  dataType: "html",
		  success: function(response){
			  $('#ajax_notified_user').html(response);
		  }
		});
}
function editNotifedUser(){
	alert('editNotifedUser');
}
function refreshNotiUser(){
	$('#ajax_notified_user').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?p=ajax.notificationUser.refresh",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>},
		  dataType: "html",
		  success: function(response){
			  $('#ajax_notified_user').html(response);
		  }
		});
}

// refresh Expiry Date
function refreshExpiryDate(){
	$('#expiry_date').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var endDate = $('#QuotationEndDate_div').html();
	$.ajax({
		  url: "index.php?p=ajax.expiryDate.refresh",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>, endDate: endDate},
		  dataType: "html",
		  success: function(response){
			  $('#expiry_date').html(response);
		  }
		});
}
/**
*	End: Send Invitation / Tender
*/

/**
 * Start: Tender Opening
 */
function goTenderOpeningForm(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['TenderOpening'].' - '.$Lang['ePCM']['Mgmt']['Case']['Declaration']?>', 'onloadTenderOpeningForm()', inlineID='', defaultHeight=400, defaultWidth=800);
}
function onloadTenderOpeningForm() {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.tenderOpening.form", 
		{ 
			caseID: <?= $caseAry['CaseID'] ?>,
            ruleID: <?= $ruleID ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

//Edit Quotation
function editQuotation(quotationID){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['UploadDocument']?>', 'onloadTBEditQuotation('+quotationID+');', inlineID='', defaultHeight=300, defaultWidth=800);
}
function onloadTBEditQuotation(quotationID) {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.quotationDetailsUpdate.form", 
		{ 
			caseID: <?= $caseAry['CaseID'] ?>,
			quotationID: quotationID
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function submitQuotationDetails(){
	var quoteDate = $('#datePickerQuoteDate').val();

//  	var quoteTime = getTimeFromTimePicker('timeSelectQuoteDate');
//  	var quoteDateTime = quoteDate+' '+quoteTime;
// 	var quotationID = $('#quotationID').val();
	var ajaxFormData = $('form#ajax_form').serialize();
	if(quoteDate==''){
		alert('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['InputQuoteDate']?>');
	}else{
		$.post(
				"index.php?p=mgmt.quotationDetails.update", 
				ajaxFormData,
				function(ReturnData)
				{
					$('div#ajax_quotation_details').html(ReturnData);
					if(ReturnData=='Saved'){
						js_Hide_ThickBox();
						location.reload();
					}
				}
			);
	}
}
/**
 * End: Tender Opening
 */


/**
 * Start: Tender Approval
 */
function goTenderApprovalForm(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['TenderApproval'].' - '.$Lang['ePCM']['Mgmt']['Case']['Declaration']?>', 'onloadTenderApprovalForm()', inlineID='', defaultHeight=400, defaultWidth=800);
}
function onloadTenderApprovalForm() {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.tenderApproval.form", 
		{ 
			caseID: <?= $caseAry['CaseID'] ?>,
            ruleID: <?= $ruleID ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

//Input Result
function setResult(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['InputResult']?>', 'onloadTBResult();', inlineID='', defaultHeight=550, defaultWidth=800);
}
function onloadTBResult() {
	$('div#TB_ajaxContent').load(
		"index.php?p=mgmt.caseResult.form", 
		{ 
			caseID: <?= $caseAry['CaseID'] ?>,
            ruleID: <?= $ruleID ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
function submitResult(){

	var canSubmit = true;
	var isFocused = false;
// 	$('.requiredField').each( function() {
// 		var _elementId = $(this).attr('id');
// 		if (isObjectValueEmpty(_elementId)) {
// 			canSubmit = false;
			
// 			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
// 			if (!isFocused) {
// 				// focus the first element
// 				$('#' + _elementId).focus();
// 				isFocused = true;
// 			}
// 		}
// 	});
	if(!canSubmit){
		return;
	}
	var ajaxFormData = $('form#ajax_form').serialize();
	var supplierID = $('input[name=supplierRadio]:checked').val();
	var dealPrice = $('#dealPrice').val();
 	var dealDate = $('#datePickerDealDate').val();
 	if(!(supplierID>0)){
		alert("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['SelectSupplier']?>");
	}else if(!(dealPrice>0)){
		alert("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['PositiveDealPrice']?>");
	}else{
		$.post(
			"index.php?p=mgmt.result.set", 
			ajaxFormData,
			function(ReturnData)
			{
				$('div#ajax_result').html(ReturnData);
	 			if(ReturnData=='Saved'){
	 				js_Hide_ThickBox();
	 				location.reload();
	 			}
			}
		);
	}
}
function goPriceComparison(){
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison']?>', 'onloadTBPriceCompare();', inlineID='', defaultHeight=550, defaultWidth=800);
}
function onloadTBPriceCompare(){
	$('div#TB_ajaxContent').load(
			"index.php?p=mgmt.priceCompare.form", 
			{ 
				caseID: <?= $caseAry['CaseID'] ?>
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}
function submitPriceComparison(){
	var ajaxFormData = $('form#ajax_form').serialize();
	$.post(
			"index.php?p=mgmt.priceComparison.set", 
			ajaxFormData,
			function(ReturnData)
			{
				$('div#ajax_price_comparison').html(ReturnData);
	 			if(ReturnData=='Saved'){
	 				js_Hide_ThickBox();
                    localStorage.setItem(<?= $postVar['caseAry']['CaseID'] ?>+"_remarks_2", $('#remarks_2').val());
	 				location.reload();
	 			}
			}
		);
}
/**
 * End: Tender Approval
 */


 

/**
 * Start: Date Picker
 */
function resetDatePicker(pickerName){
	$('#datePicker'+pickerName+'_div').hide();
	$('#'+pickerName+'_div').css('display', 'inline-block');
	$('#'+pickerName+'Edit').css('display', 'inline-block');
}
function editDatePicker(pickerName){
	$('#datePicker'+pickerName+'_div').css('display', 'inline-block');
	$('#'+pickerName+'_div').hide();
	$('#'+pickerName+'Edit').hide();
}
function datePickerQuotationEndDateSelected(){
	$('#QuotationEndDate_div').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var date = $(datePickerQuotationEndDate).val();
	var time = getTimeFromTimePicker('timePickerQuotationEndDate');
	date = date + ' ' + time;
	$.ajax({
	  url: "index.php?p=mgmt.quotationEndDate.update",
	  type: "POST",
	  data: { caseID : <?= $caseAry['CaseID'] ?>, date:date },
	  dataType: "html",
	  success: function(response){
			//location.reload();
		  resetDatePicker('QuotationEndDate');
		  if(response==''){
			  response='failed';
		  }
		  $('#QuotationEndDate_div').html(response);
		  refreshExpiryDate();
		}
	});
}
function datePickerTenderOpeningStartDateSelected(){
	var originalDate = $('#TenderOpeningStartDate_div').html();
	$('#TenderOpeningStartDate_div').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var date = $(datePickerTenderOpeningStartDate).val();

	//Checking - no earlier than quotation end date
	var quotationEndDate = $('#QuotationEndDate_div').html();
	if((new Date(quotationEndDate).getTime() > new Date(date).getTime())){
		alert('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderOpeningStartDate']?>');
		$('#TenderOpeningStartDate_div').html(originalDate);	// get origanal value back
		return;
	}
	
	$.ajax({
	  url: "index.php?p=mgmt.tenderOpeningStartDate.update",
	  type: "POST",
	  data: { caseID : <?= $caseAry['CaseID'] ?>, date:date },
	  dataType: "html",
	  success: function(response){
		//location.reload();
		  resetDatePicker('TenderOpeningStartDate');
		  if(response==''){
			  response='failed';
		  }
		  $('#TenderOpeningStartDate_div').html(response);
	  }
	});
}
function datePickerTenderOpeningEndDateSelected(){
	var originalDate = $('#TenderOpeningEndDate_div').html();
	$('#TenderOpeningEndDate_div').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var date = $(datePickerTenderOpeningEndDate).val();

	//Checking - no earlier than quotation end date
	var tenderOpeningStartDate = $('#TenderOpeningStartDate_div').html();
	if((new Date(tenderOpeningStartDate).getTime() > new Date(date).getTime())){
		alert('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderOpeningEndDate']?>');
		$('#TenderOpeningEndDate_div').html(originalDate);	// get origanal value back
		return;
	}

	$.ajax({
	  url: "index.php?p=mgmt.tenderOpeningEndDate.update",
	  type: "POST",
	  data: { caseID : <?= $caseAry['CaseID'] ?>, date:date },
	  dataType: "html",
	  success: function(response){
		//location.reload();
		  resetDatePicker('TenderOpeningEndDate');
		  if(response==''){
			  response='failed';
		  }
		  $('#TenderOpeningEndDate_div').html(response);
	  }
	});
}
function datePickerTenderApprovalStartDateSelected(){
	var originalDate = $('#TenderApprovalStartDate_div').html();
	$('#TenderApprovalStartDate_div').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var date = $(datePickerTenderApprovalStartDate).val();

	//Checking - no earlier than quotation end date
	var tenderOpeningEndDate = $('#TenderOpeningEndDate_div').html();
	if((new Date(tenderOpeningEndDate).getTime() > new Date(date).getTime())){
		alert('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderApprovalStartDate']?>');
		$('#TenderApprovalStartDate_div').html(originalDate);	// get origanal value back
		return;
	}

	$.ajax({
	  url: "index.php?p=mgmt.tenderApprovalStartDate.update",
	  type: "POST",
	  data: { caseID : <?= $caseAry['CaseID'] ?>, date:date },
	  dataType: "html",
	  success: function(response){
		//location.reload();
		  resetDatePicker('TenderApprovalStartDate');
		  if(response==''){
			  response='failed';
		  }
		  $('#TenderApprovalStartDate_div').html(response);
	  }
	});
}
function datePickerTenderApprovalEndDateSelected(){
	var originalDate = $('#TenderApprovalEndDate_div').html();
	$('#TenderApprovalEndDate_div').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	var date = $(datePickerTenderApprovalEndDate).val();

	//Checking - no earlier than quotation end date
	var tenderApprovalStartDate = $('#TenderApprovalStartDate_div').html();
	if((new Date(tenderApprovalStartDate).getTime() > new Date(date).getTime())){
		alert('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderApprovalEndDate']?>');
		$('#TenderApprovalEndDate_div').html(originalDate);	// get origanal value back
		return;
	}
	
	$.ajax({
	  url: "index.php?p=mgmt.tenderApprovalEndDate.update",
	  type: "POST",
	  data: { caseID : <?= $caseAry['CaseID'] ?>, date:date },
	  dataType: "html",
	  success: function(response){
		//location.reload();
		  resetDatePicker('TenderApprovalEndDate');
		  if(response==''){
			  response='failed';
		  }
		  $('#TenderApprovalEndDate_div').html(response);
	  }
	});
}
/**
 * End: Date Picker
 */


function finishStage2(){
	var remarks = '';
	<?php if(count($caseAry['Quotation'])<$caseAry['MinQuotation']){ ?>
		//check remarks is entered?
		remarks = $('#remarks_2').val();
		if( remarks.trim() == ''){
			alert("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['InputReason']?>");
			return false;
		}
	<?php } ?>
	
	var isConfirm = confirm("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['Confrim2NextStage']?>");
	if(isConfirm){
		$.ajax({
		  url: "index.php?p=mgmt.finishStage2",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>, remarks:remarks },
		  dataType: "html",
		  success: function(response){
			location.reload();
		  }
		});
		
	}
}
//Date 2016-10-12 Villa fix reason cannot display problem
function finishStage3(){
	
	var remarks = '';
		<?php if( count($caseAry['Quotation'])<$caseAry['MinQuotation'] && $caseAry['QuotationType']=='V'){ ?>
			//check remarks is entered?
			remarks = $('#remarks_2').val();
			if( remarks.trim() == ''){
				alert("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['InputReason']?>");
				return false;
			}
		<?php } ?>
	var isConfirm = confirm("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['Confrim2NextStage']?>");
	if(isConfirm){
		$.ajax({
		  url: "index.php?p=mgmt.finishStage3",
		  type: "POST",
		  data: { caseID : <?= $caseAry['CaseID'] ?>, remarks:remarks  },
		  dataType: "html",
		  success: function(response){
			location.reload();
	  		}
		});
	}
}

function onChangeSupplierType(typeID){
	$('#ajax_supplier_table').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$('#ajax_supplier_table').load(
			"index.php?p=ajax.supplierDbTable", 
			{ 
				typeID: typeID,
				caseID: <?= $caseAry['CaseID'] ?>
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}
function onSearchSupplier(keyword){
	$('#typeID').val('');	//reset select dropdown
	$('#ajax_supplier_table').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$('#ajax_supplier_table').load(
			"index.php?p=ajax.supplierDbTable", 
			{ 
				keyword: keyword,
				caseID: <?= $caseAry['CaseID'] ?>
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}
function onchangeSupplierID(supplierID){
// 	var supplierSelectedOptionAry = $('#supplierSel option:selected');
// 	var numOfSupplierSelected = supplierSelectedOptionAry.length;
	if(false){
	$('#ajax_show_supplier_info').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');

	$('#ajax_show_supplier_info').load(
		"index.php?p=mgmt.supplierInfo", 
		{ 
			supplierID: supplierID
		},
		function(ReturnData) {
			$('#ajax_show_supplier_info').html(ReturnData);
		}
	);
	}
}

/*
 * Download document
 */
function downloadDocument(document){
	switch(document){
        case 'Summary':
            $('form#form1').attr('action', 'index.php?p=mgmt.document.summary');
            $('form#form1').submit();
            break;
        case 'mfsConfirmationForm':
            $('form#form1').attr('action', 'index.php?p=mgmt.document.summary.mfs_form');
            $('form#form1').submit();
            break;
    }
}



function getTimeFromTimePicker(id){
	var hour = $('#'+id+'_hour option:selected').val();
	var min = $('#'+id+'_min option:selected').val();
	var sec = $('#'+id+'_sec option:selected').val();
	return pad(hour,2)+':'+pad(min,2)+':'+pad(sec,2);
}
function pad(num, size) { //add zero
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function PrintApplcationFrom(){
	var myWindow = window.open("index.php?p=mgmt.caseDetail.printApplicationForm.<?=$caseAry['CaseID'] ?>");
}
function exportPurchase_by_OralQuotationForm(lang='en'){
    document.getElementById('EDB_export_lang').value = lang;
    document.getElementById('annex').value = 2;
    $('form#form1').attr('action', 'index.php?p=report.EDB_Word');
    $('form#form1').submit();
}
</script>
<?=$navigation ?>
<?= $htmlAry['caseDetails'] ?>
<?= $htmlAry['hiddenField'] ?>