<?php
// Editing by 
#################
#	Date: 2016-10-05 Omas
#		set $home_header_no_EmulateIE7 for IE not emulate IE7 anymore for ePCM 
###############
// for EJ php 5.4
$module_custom['isUTF8'] = true;
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.utf8.php");
include_once($PATH_WRT_ROOT."lang/ePCM_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_cfg.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_ui.php");

#### START db ####
intranet_auth();
intranet_opendb();

// 2016-10-05 -R105451 
$home_header_no_EmulateIE7 = true;


if(!$plugin['ePCM']){
	No_Access_Right_Pop_Up('','/');
	exit();
}
if(isset($junior_mck)){
	$g_encoding_unicode = true; // Change encoding from Big5 to UTF-8
}
//  debug_pr($eclass_filepath);
if($p){
	$_PAGE = array(); // page scope variable to store all data, controller instance, etc...
	$_PAGE['PATH_WRT_ROOT'] = $PATH_WRT_ROOT;
	$_PAGE['eclass_filepath'] = $eclass_filepath;
	$_PAGE['POST'] = $_POST;
	$_PAGE['GET'] = $_GET;
	$_PAGE['FILES'] = $_FILES;
	$_PAGE['libPCM'] = new libPCM();
// ui init after security control as ui will use $_SESSION 
// 	$_PAGE['libPCM_ui'] = new libPCM_ui();
	### handle all http post/get value by urldeocde, stripslashes, trim
	array_walk_recursive($_PAGE['POST'], 'handleFormPost');
	array_walk_recursive($_PAGE['GET'], 'handleFormPost');


	$path_params = explode('.',$p);
	if($path_params[0] != ''){
		
		### security control - common
		
		// get general setting
		$controller_dir = '/home/eAdmin/ResourcesMgmt/eProcurement/controllers';
		$controller_module_name = 'IndexController';
		$controller_module_php = $intranet_root . $controller_dir . '/' . $controller_module_name . '.php';
		include_once ($controller_module_php);
		$module_controller = new $controller_module_name();
		$_PAGE['Controller'] = $module_controller;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
		$setting = $db->getGeneralSettings();
		$_SESSION['ePCM']['Setting']['General'] = $setting;
		
		### END - security control - common
		
		### security control - network
		$ip_allowed = $_PAGE['libPCM']->checkCurrentIP($setting['IPList']);
		if(! $ip_allowed){
			$path_params[0] = 'index';
			$path_params[1] = 'networkipfail';
		}
		### END - security control - network
		
	    ### security control - login
		if($ip_allowed && $setting['AccessSetting'] == '1'){
			$ePCM_is_login = isset($_SESSION['ePCM']['session_password']);
			if(! $ePCM_is_login){
				$path_params[0] = 'index';
				$path_params[1] = 'login';
			}
		}else if($ip_allowed && $setting['AccessSetting'] != '1'){
			$_SESSION['ePCM']['session_password'] = 'ok';
		}
		### END - security control - login
		
		### init after security control as the $_SESSION not yet at the first time login
		$_PAGE['libPCM_ui'] = new libPCM_ui();
		
		$request_section = $path_params[0];
		$request_section = array_shift($path_params);
		$_PAGE['REQUEST_P'] = $p;
		$_PAGE['REQUEST_URI'] = implode('.',$path_params);
		// controller 
		$controller_dir = '/home/eAdmin/ResourcesMgmt/eProcurement/controllers';
		$controller_module_name = ucfirst($request_section).'Controller';
		$controller_module_php = $intranet_root.$controller_dir.'/'.$controller_module_name.'.php';
		
		if(!file_exists($controller_module_php)){ // no matching controller, use default IndexController
			$controller_module_name = 'IndexController';
			$controller_module_php = $intranet_root.$controller_dir.'/'.$controller_module_name.'.php';
		}
		else if(file_exists($controller_module_php)){
			include_once($controller_module_php);
			$module_controller = new $controller_module_name();
			$_PAGE['Controller'] = $module_controller;
			$request_mapping = $module_controller->findRequestMapping($_PAGE['REQUEST_URI']);
		}

		if(count($request_mapping)>0){
			$_PAGE['REQUEST_URI_PATTERN'] = $request_mapping['uri'];
			$_PAGE['PathVars'] = $request_mapping['pathVars'];
			$module_controller->requestValidation();
			$auth_method = 'authenticate';
			if(isset($request_mapping['auth'])){
				$auth_method = $request_mapping['auth'];
			}
			if($module_controller->$auth_method()){
				if(isset($request_mapping['pathVars']) && count($request_mapping['pathVars'])>0){
					call_user_func_array(array($module_controller,$request_mapping['function']), $request_mapping['pathVars']);
				}else{
					$module_controller->$request_mapping['function']();
				}
			}else{
// 			    debug_pr('no access right');
				echo 'No access right';
//                 No_Access_Right_Pop_Up('','index.php');
	           exit();
			}
		}else{
			$_PAGE['REQUEST_URI_PATTERN'] = '/';
			$module_controller->index();
		}
	}
}
else{
	header('Location: index.php?p=mgmt.view');
}

#### END db ####
intranet_closedb();
exit();

//debug_pr($_PAGE);
//debug_pr($_SESSION);
//debug_pr($content_type);


?>