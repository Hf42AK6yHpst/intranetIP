<?php
// Editing by  

abstract class BaseController
{
	const FLASH_ATTRIBUTES = 'FLASH_ATTRIBUTES';
	
	/*
	 * Usage of request mapping: 
	 * $this->RequestMapping['uri_regex_pattern'] = array('function'=>'controller method name','auth'=>'controller authentication method name','pathVars'=>array());
	 * uri_regex_pattern : absolute uri path that supports regular expression, where the first uri section is used to map to controller
	 * e.g. '/module1/section1/function1/record/(\d+)' module1 would be the expected controller, if that controller not exist, all requests would be delegated to IndexController. 
	 * the (\d+) would match the record id, and would pass as arguments to the controller method and temporary stored in 'pathVars'(no need to set it, router would auto map it).
	 * 'auth' is optional, if not set, would use default authenticate() method.  
	 */
	protected $RequestMapping = array(); // child classes need inherit this attribute
	/*
	 * ObjectRepository is used store class instances that avoid same classe created repeatedly. 
	 */
	protected $ObjectRepository = array(); 
	
	public function __construct()
	{
		global $_PAGE;
		if(isset($_SESSION[self::FLASH_ATTRIBUTES]) && count($_SESSION[self::FLASH_ATTRIBUTES])>0){
			foreach($_SESSION[self::FLASH_ATTRIBUTES] as $key => $val){
				$_PAGE[$key] = $val;
			}
		}
		unset($_SESSION[self::FLASH_ATTRIBUTES]);
	}
	
	public function __destruct()
	{
		global $_PAGE;
		unset($_PAGE);
	}
	
	/*
	 * Usage of flash attributes
	 * Flash attributes are used to pass temporary data across pages, e.g. pass error code from update page to submit page to indicate result.
	 * Flash attributes are copied from session to page scope variable $_PAGE automatically when controller is created.  
	 */
	public function setFlashAttribute($key, $value)
	{
		if(!isset($_SESSION[self::FLASH_ATTRIBUTES])){
			$_SESSION[self::FLASH_ATTRIBUTES] = array();
		}
		$_SESSION[self::FLASH_ATTRIBUTES][$key] = $value;
		//debug_pr($_SESSION);
	}
	
	public function getFlashAttribute($key)
	{
		global $_PAGE;
		if(isset($_PAGE[self::FLASH_ATTRIBUTES]) && isset($_PAGE[self::FLASH_ATTRIBUTES][$key])){
			return $_PAGE[self::FLASH_ATTRIBUTES][$key];
		}
		return '';
	}
	
	public function findRequestMapping($uri)
	{
		if(isset($this->RequestMapping[$uri])){
			$mapping = $this->RequestMapping[$uri];
			$mapping['uri'] = $uri;
			
			return $mapping;
		}else if(count($this->RequestMapping)>0){
			
			$uriParmArr = explode('.', $uri);
			$method = array_shift($uriParmArr);
			
			foreach($this->RequestMapping as $key => $val)
			{
				if($key == ''){
					continue;
				}
				
				$arr = explode('.',$uri);
				$arrSize = count($arr);
				
				// determine method and par position
				$methodKey = $arr[0].'.'.$arr[1];
				$parPos = 2;
				if(!isset($this->RequestMapping[$methodKey])){
					$methodKey = $arr[0];
					$parPos = 1;
				}
				if(isset($this->RequestMapping[$methodKey])){
					$mapping = $this->RequestMapping[$methodKey];
					$pathVarsArr = array();
					if($arrSize>$parPos){
						for($i = $parPos; $i < $arrSize;$i++){
							$pathVarsArr[] = $arr[$i];
						}
					}
					$mapping['uri'] = $methodKey;
					if(count($pathVarsArr) > 0){
						$mapping['pathVars'] = $pathVarsArr;
					}
					return $mapping;
				}

// 				if(preg_match('/^'.$key.'\.(.*)$/', $uri, $matches)){
// 					$mapping = $this->RequestMapping[$key];
// 					$mapping['uri'] = $key; 
// 					if(count($matches)>1){
// 						array_shift($matches);
// // 						$mapping['pathVars'] = $matches;
// 						$mapping['pathVars'] = explode('.', implode('', $matches));
// 					}
// // 					debug_pr('you have map to function: '.$mapping['function'].' if not correct please tell omas');
// // 					debug_pr('Changed the way of pass params by ?p=');
// 					return $mapping;
// 				}
			}
		}
		
		return array();
	}
	
	public function getRequestMapping()
	{
		return $this->RequestMapping;
	}
	
	public function getViewBaseDir()
	{
		return '/home/eAdmin/ResourcesMgmt/eProcurement/views';
	}
	
	// include a php page that inside the base view dir and output as is
	public function includeView($view_path)
	{
		global $intranet_root,$Lang,$_PAGE; 
		global $MODULE_OBJ, $TAGS_OBJ, $CurrentPageArr, $CurrentPage, $plugin, $sys_custom;
		global $views_variable,$ePCMcfg;
		
		if(file_exists($intranet_root.$this->getViewBaseDir().$view_path)){
			include($intranet_root.$this->getViewBaseDir().$view_path);
		}
	}
	
	// get the content from a php view page but do not output
	public function getViewContent($view_path, $replaceMap=array())
	{
		global $Lang,$_PAGE;
		
		ob_start();
		$this->includeView($view_path);
		$content = ob_get_clean();
		
		if(count($replaceMap)>0){
			foreach($replaceMap as $placeholder => $replace_content)
			{
				$content = str_replace($placeholder, $replace_content, $content);
			}
		}

		return $content;
	}
	
	// this is the overall layout with sidebar view optionally provided
	public function getLayout($mainViewContent, $otherContent='', $frameTemplate='', $mainTemplate='/main.php')
	{
		// $otherContent is not work now
		
		$replaceViews = array();

		if($frameTemplate != ''){
			$frame = $this->getViewContent($frameTemplate, array('<!--FRAME CONTENT-->' => $mainViewContent));
			$main = $this->getViewContent($mainTemplate, array('<!--MAIN-->' => $frame));
		}
		else{
			$main = $this->getViewContent($mainTemplate, array('<!--MAIN-->' => $mainViewContent));			
		}

		$replaceViews['<!--MAIN-->'] = $main;
		
		$view = $this->getViewContent('/template.php', $replaceViews);
		return $view;
	}
	
	// redirect to a page
	public function sendRedirect($url)
	{
		header("Location: ".$url);
		exit;
	}
	
	public function requestObject($className, $includePath, $params=array())
	{
		global $intranet_root, $PATH_WRT_ROOT, $eclass_filepath;
		
		$object_key = $className;
		if(count($params)>0){
			$object_key = $className.md5(serialize($params));
		}
		
		if(isset($this->ObjectRepository[$object_key])){
			return $this->ObjectRepository[$object_key];
		}else {
			$includePath = ltrim($includePath,'/');
			include_once($intranet_root.'/'.$includePath);
			
			if(count($params)>0){
				echo 'no ReflectionClass.';
				die();
				$clazz = new ReflectionClass($className);
				$this->ObjectRepository[$object_key] = $clazz->newInstanceArgs($params);
			}else{
				$this->ObjectRepository[$object_key] = new $className();
			}
			return $this->ObjectRepository[$object_key];
		}
	}
	
	public function requestValidation()
	{
		global $_PAGE;
		
		$request['POST'] = $_PAGE['POST'];
		$request['GET'] = $_PAGE['GET'];
		$request['PathVars'] = $_PAGE['PathVars'];

		$key = $_PAGE['REQUEST_URI_PATTERN'];
		$rules = $this->SecurityRegistry[$key];
		$requestHandler = $this->requestObject('libPCM_requestHandler', 'includes/ePCM/libPCM_requestHandler.php');
		if(!empty($rules)){
			$pass = $requestHandler->requestHandler($request, $rules);
		}
		return $pass;
	}
	
	public function authenticate()
	{
	    global $_PAGE;
	    
// 	    debug_pr($_PAGE['POST']);
// 	    debug_pr($_PAGE['PathVars']);
	    $key = $_PAGE['REQUEST_URI_PATTERN'];
	    $auth = $this->requestObject('libPCM_auth', 'includes/ePCM/libPCM_auth.php');
	    $authMapping = $this->AuthMapping[$key];
// 	    debug_pr($key);
// 	    debug_pr($authMapping);
	    $result = $auth->callAuthByMapping($authMapping);
	    if($result){
    		return true;
	    }
	    else{
            return false;	        
	    }
	}
	
// 	public function index()
// 	{
		
// 	}
}

?>