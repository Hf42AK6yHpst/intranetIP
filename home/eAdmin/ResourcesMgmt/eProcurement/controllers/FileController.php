<?php
// Editing by 
// ################ Change Log [Start] #####
//
// Date :
//
// ################# Change Log [End] ######
include_once ("BaseController.php");
class FileController extends BaseController {

	public function __construct(){
		global $_PAGE;
		parent::__construct();
		$this->RequestMapping['process'] = array('function' => 'process');
		$this->AuthMapping['process'] = array('function' => 'allUser', 'authValue' => '');
		// view file.bind
		$this->RequestMapping['download'] = array('function' => 'download');
		$this->AuthMapping['download'] = array('function' => 'checkViewFilePermission', 'authValue' => 'PathVars_0');
		// add & edit file related
		$this->RequestMapping['attach'] = array('function' => 'attach');
		$this->AuthMapping['attach'] = array('function' => 'allUser', 'authValue' => '');
		$this->RequestMapping['caseList'] = array('function' => 'getList');
		$this->AuthMapping['caseList'] = array('function' => 'allUser', 'authValue' => '');
		$this->RequestMapping['quoList'] = array('function' => 'getQuoList');
        $this->RequestMapping['ruleList'] = array('function' => 'getRuleList');
        $this->AuthMapping['ruleList'] = array('function' => 'allUser', 'authValue' => '');
		$this->AuthMapping['quoList'] = array('function' => 'allUser', 'authValue' => '');
		$this->RequestMapping['delete'] = array('function' => 'delete');
		$this->AuthMapping['delete'] = array('function' => 'allUser', 'authValue' => 'POST_AttachmentID');
		$this->RequestMapping['recover'] = array('function' => 'recoverTempDelete');
		$this->AuthMapping['recover'] = array('function' => 'allUser', 'authValue' => '');
		
		$this->RequestMapping['test'] = array('function' => 'test');
	}

	public function test($ID){
		$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
		$result = $db->getEmailAttachmentReady($ID);
	}
	
	public function download($AttachmentID){
		// need permission checking
		if($AttachmentID > 0){
			$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
			$db->getFileToBrowser($AttachmentID);
		}
	}
	
	public function process(){
		global $_PAGE;
		
		// $_GET
		$sessionToken = $_PAGE['GET']['session'];
		if($sessionToken != ''){
			$model = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
			$model->uploadToTempPath($sessionToken);
		}
	}

	public function attach($action){
		global $file_path, $UserID, $_PAGE;

		$ruleID   = $_PAGE['POST']['ruleID'];
        $stageID = $_PAGE['POST']['template_key'];
		
		$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
		$sessionToken = $_PAGE['POST']['session'];
		
		if($action == 'update'){
			$fileName = $_PAGE['POST']['upload_file_name'];
			$returnJs = $db->processTempToStorage($sessionToken, $fileName, $ruleID, $stageID);
		}else if($action == 'delete'){
			$AttachmentID = $_PAGE['POST']['AttachmentID'];
			// need add permission checking before delete?
			if($AttachmentID > 0){
				$returnJs = $db->removeTempAttachment($sessionToken, $AttachmentID, $ruleID, $stageID);
			}
		}
		echo $returnJs;
	}

	public function delete(){
		global $file_path, $UserID, $_PAGE;
		
		$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
		$sessionToken = $_PAGE['POST']['session'];
		$AttachmentID = $_PAGE['POST']['AttachmentID'];
		$template_key = $_PAGE['POST']['template_key'];
        $ruleID = $_PAGE['POST']['ruleID'];
		// need add permission checking before delete?
		if($AttachmentID > 0){
			$returnJs = $db->removeAttachmentStep1($sessionToken, $AttachmentID, $ruleID, $template_key);
		}
		echo $returnJs;
	}

	public function getQuoList(){
		global $_PAGE;
		
		$caseID = $_PAGE['POST']['caseID'];
		$quoID = $_PAGE['POST']['quoID'];
		$sessionToken = $_PAGE['POST']['session'];
		if($sessionToken != '' && $caseID != '' && $quoID != ''){

			$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
			$result = $db->updateFileSession($caseID, '3', $sessionToken, $quoID);
			
			$AttachList = $db->getUserSessionFileList($sessionToken);
			$js = $db->getRefreshFileListJs($AttachList);
			
			echo $js;
		}
	}

    public function getRuleList(){
        global $_PAGE;

        $caseID = $_PAGE['POST']['caseID'];
        $stageID = $_PAGE['POST']['stageID'];
        $ruleID = $_PAGE['POST']['ruleID'];
        $type = $_PAGE['POST']['type'];
        $sessionToken = $_PAGE['POST']['session'];
        if($sessionToken != '' && $ruleID  != '' && $stageID != ''){
            $db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
            $result = $db->updateFileSession($caseID, '', $sessionToken, '', $type, $ruleID);

            $AttachList = $db->getUserSessionFileList($sessionToken, $ruleID, $stageID);
            $js = $db->getRefreshFileListJs($AttachList);

            echo $js;
        }
    }

	public function getList(){
		global $_PAGE;
		
		$caseID = $_PAGE['POST']['caseID'];
		$stageID = $_PAGE['POST']['stageID'];
		$sessionToken = $_PAGE['POST']['session'];
		$type = $_PAGE['POST']['type'];
		if($sessionToken != '' && $caseID != '' && $stageID != ''){
			$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
			$result = $db->updateFileSession($caseID, $stageID, $sessionToken, '', $type);
			
			$AttachList = $db->getUserSessionFileList($sessionToken);
			$js = $db->getRefreshFileListJs($AttachList);
			
			echo $js;
		}
	}

	public function recoverTempDelete(){
		global $_PAGE;
		
		$db = $this->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
		$result = $db->recoverTempRemoveAttachment($_PAGE['POST']['session'], $_PAGE['POST']['WillDeletedList']);
	}
	
}

?>