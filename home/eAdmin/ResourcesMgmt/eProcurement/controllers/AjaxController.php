<?php
// Editing by 
/* 
 * Date: 2017-03-23 Villa
 * - modified getFundingSourceThickBox() - delete inactivate (record status 0)
 * Date: 2017-03-16 Villa
 * - checkingFundingBudgetValid() - add checking condition: the sum of funding budget cannot greater than budget
 * Date: 2017-03-07 Villa
 * - modified checkingFundingBudgetValid() - use the function to get the data in stead of calculating in this function
 * - add getFundingCaseThichkBox - printing Funding Case ThickBox
 * Date: 2017-03-06 Villa
 * - add checkingFundingBudgetValid() - ajax Checking for adding funding source in case
 * - add getFundingSourceSelectionBox - gen selection Box
 * 
 * Date: 2017-03-03 Villa
 * - add getFundingCategoryThickBox() - generate Funding Category ThickBox UI
 * - add getFundingSourceThickBox() - genertate Funding Record ThickBox UI
 * 
 * Date: 2017-03-01 Villa
 * - X100660 modified groupMemberSearchUser() allow search SMC User
 * - modified refreshUserSelection() allow select SMC User
 * 
 * Date: 2016-08-26 Omas
 * - modified getFinancialItemBudget() 
 * 
 * Date: 2016-07-06	Kenneth
 * - added returnEmailPreview(), added email preview function
 * 
 * Date: 2016-07-05	Kenneth
 * - modified supplierDbTable(), added supplier search function 
 *  
 * Date: 2016-07-04	Kenneth
 * - modified checkUnique(), added refreshCaseCode() and refreshCaseName()
 * 
 */
include_once("BaseController.php");

class AjaxController extends BaseController
{
	public function __construct()
	{
		global $_PAGE;
		parent::__construct();
		$this->RequestMapping[''] = array('function'=>'index');
		
		//Management
		$this->RequestMapping['caseApproval.update'] = array('function'=>'caseApprovalUpdate');
		$this->AuthMapping['caseApproval.update'] = array('function' => 'allUser');
		$this->RequestMapping['supplierSelectByType'] = array('function'=>'supplierSelectByType');
		$this->AuthMapping['supplierSelectByType'] = array('function' => 'allUser');
		$this->RequestMapping['supplierDbTable'] = array('function'=>'supplierDbTable');
		$this->AuthMapping['supplierDbTable'] = array('function' => 'allUser');
		$this->RequestMapping['supplierRadioList'] = array('function'=>'supplierRadioList');
		$this->AuthMapping['supplierRadioList'] = array('function' => 'allUser');
		$this->RequestMapping['financialItem'] = array('function'=>'getFinanialItem');
		$this->AuthMapping['financialItem'] = array('function' => 'allUser');
		$this->RequestMapping['financialItemBudget'] = array('function'=>'getFinancialItemBudget');
		$this->AuthMapping['financialItemBudget'] = array('function' => 'allUser');
		$this->RequestMapping['caseDates.get'] = array('function'=>'getCaseDates');
		$this->AuthMapping['caseDates.get'] = array('function' => 'allUser');
		$this->RequestMapping['checkExceedBudget'] = array('function'=>'returnExceedBudget');
		$this->AuthMapping['checkExceedBudget'] = array('function' => 'allUser');
		$this->RequestMapping['priceSummary.get'] = array('function'=>'returnPriceSummaryFromAllPriceItem');
		$this->AuthMapping['priceSummary.get'] = array('function' => 'allUser');
		$this->RequestMapping['emailpreview.get'] = array('function'=>'returnEmailPreview');
		$this->AuthMapping['emailpreview.get'] = array('function' => 'allUser');
		$this->RequestMapping['fundingSource.getSelectionBox'] = array('function'=>'getFundingSourceSelectionBox');
		$this->AuthMapping['fundingSource.getSelectionBox'] = array('function' => 'allUser');
		$this->RequestMapping['fundingSource.BudgetVaildChecking'] = array('function'=>'checkingFundingBudgetValid');
		$this->AuthMapping['fundingSource.BudgetVaildChecking'] = array('function' => 'allUser');
		
		## Refresh
		$this->RequestMapping['notificationUser.refresh'] = array('function'=>'refreshNotificationUser');
		$this->AuthMapping['notificationUser.refresh'] = array('function' => 'allUser');
		$this->RequestMapping['quotaionReplyDate.refresh'] = array('function'=>'refreshQuotationReplyDate');
		$this->AuthMapping['quotaionReplyDate.refresh'] = array('function' => 'allUser');
		$this->RequestMapping['expiryDate.refresh'] = array('function'=>'refreshExpiryDate');
		$this->AuthMapping['expiryDate.refresh'] = array('function' => 'allUser');
		$this->RequestMapping['caseCode.refresh'] = array('function'=>'refreshCaseCode');
		$this->AuthMapping['caseCode.refresh'] = array('function' => 'allUser');
		$this->RequestMapping['caseName.refresh'] = array('function'=>'refreshCaseName');
		$this->AuthMapping['caseName.refresh'] = array('function' => 'allUser');
		
		//Setting
// 		$this->RequestMapping['new.groupMember'] = array('function'=>'newGroupMemberStep1');
// 		$this->RequestMapping['new.groupMemberQuery'] = array('function'=>'newGroupMemberStep2');
		$this->RequestMapping['refreshUserList'] = array('function'=>'refreshUserSelection');
		$this->AuthMapping['refreshUserList'] = array('function' => 'allUser');
		$this->RequestMapping['searchUser'] = array('function'=>'groupMemberSearchUser');
		$this->AuthMapping['searchUser'] = array('function' => 'allUser');
		$this->RequestMapping['getSupplierTypeList'] = array('function'=>'getSupplierTypeList');
		$this->AuthMapping['getSupplierTypeList'] = array('function' => 'allUser');
		$this->RequestMapping['getFundingCategoryThickBox'] = array('function'=>'getFundingCategoryThickBox');
		$this->AuthMapping['getFundingCategoryThickBox'] = array('function' => 'allUser');
		$this->RequestMapping['getFundingSourceThickBox'] = array('function'=>'getFundingSourceThickBox');
		$this->AuthMapping['getFundingSourceThickBox'] = array('function' => 'allUser');
		$this->RequestMapping['getFundingCaseThichkBox'] = array('function'=>'getFundingCaseThichkBox');
		$this->AuthMapping['getFundingCaseThichkBox'] = array('function' => 'allUser');
// 		$this->RequestMapping['new.roleMember'] = array('function'=>'newRoleMemberStep1');
// 		$this->RequestMapping['new.roleMemberQuery'] = array('function'=>'newRoleMemberStep2');
		
		## check deuplicates
		$this->RequestMapping['checkUnique'] = array('function'=>'checkUnique');
		$this->AuthMapping['checkUnique'] = array('function' => 'allUser');
		$this->RequestMapping['groupBudget'] = array('function'=>'getGroupBudget');
		$this->AuthMapping['groupBudget'] = array('function' => 'allUser');
		
		
		### Import Load Reference
		$this->RequestMapping['import.load_reference'] = array('function'=>'loadReference');
		$this->AuthMapping['import.load_reference'] = array('function' => 'isAdmin');

		### Check Price range
        $this->RequestMapping['checkPriceRule'] = array('function'=>'checkPriceRule');
        $this->AuthMapping['checkPriceRule'] = array('function' => 'checkPriceRule');

        ### Check rule's Application form
        $this->RequestMapping['checkRuleApplicationForm'] = array('function'=>'checkRuleApplicationForm');
        $this->AuthMapping['checkRuleApplicationForm'] = array('function' => 'allUser');
	}
	
	public function index(){
		
		$main = $this->getViewContent('/index_tmpl.php');
		$view = $this->getLayout($main, $sidebar);

		echo $view;
	}

	public function supplierSelectByType(){
		global $_PAGE,$Lang;
		$postVar = $_PAGE['POST'];
		$typeID = $postVar['typeID'];
		$caseID = $postVar['caseID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		
		//Get existing quote
		$existingQuotes = $db->getCaseQuotation($caseID);
		$existingSupplierList = Get_Array_By_Key($existingQuotes, "SupplierID");
		
		## typeID=='' => show nothing
		if($typeID==''){
			echo '';
			return;
		}
		
		$supplierInfoAry = $db->getSupplierInfoTypeID($typeID);
		foreach($supplierInfoAry as $_supplier){
			if(in_array($_supplier['SupplierID'],$existingSupplierList)){
				continue;
			}
			$supplierAry[$_supplier['SupplierID']]=$_supplier['SupplierName'];
		}
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$sel = getSelectByAssoArray($supplierAry, $tags='id="supplierSel" name="supplierIDAry[]" multiple="multiple" size="7" onchange="onchangeSupplierID();"', $selected="", $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
		$selectAllBtn = $ui->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('supplierSel', 1);");
		echo $ui->Get_MultipleSelection_And_SelectAll_Div($sel, $selectAllBtn, $SpanID='');
	}
	public function supplierDbTable(){
		global $_PAGE,$Lang;
		$postVar = $_PAGE['POST'];
		$typeID = $postVar['typeID'];
		$keyword = $postVar['keyword'];
		$caseID = $postVar['caseID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		
		//Get existing quote
		$existingQuotes = $db->getCaseQuotation($caseID);
		$existingSupplierList = Get_Array_By_Key($existingQuotes, "SupplierID");
		## typeID=='' => show nothing
// 		if($typeID==''){
// 			echo '';
// 			return;
// 		}
		
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		
		$sql = $db->getSupplierSqlForDbTable($typeID,1,$keyword,$existingSupplierList);
		$headingAry = array();
		//$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Code'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'];
		//$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Type'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Description'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Quote Invitation'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Quote'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Bid Invitation'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Bid'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Deal'];
		$headingAry[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Last Deal Date'];
		$headingAry[] = "<input type=\"checkbox\" name=\"checkmaster\" onclick=\"(this.checked)?setChecked(1,this.form,'targetIdAry[]'):setChecked(0,this.form,'targetIdAry[]')\">";
		$contentAry = $db->returnResultSet($sql);
		
		//unset code and type
		foreach($contentAry as &$_supplier){
			unset($_supplier['Code']);
			unset($_supplier['Type']);
		}
		
		//$contentAry = array(array('a','b','c'),array('a','b','c'));
		$table = $ui->getTable($headingAry,$contentAry);
		
		echo $table;
	}
    public function supplierRadioList(){
        global $_PAGE,$Lang;
        $db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
        $linterface = $_PAGE['libPCM_ui'];

        $postVar = $_PAGE['POST'];
        $typeID = $postVar['typeID'];
        $selectedSupplierID = $postVar['selectedSupplierID'];
        $allSupplierInType = $db->getSupplierInfoTypeID("$typeID");
        $supplierListAry = array();
        foreach($allSupplierInType as $_supplier){
            $supplierListAry[] = array($_supplier['SupplierID'],$_supplier[Get_Lang_Selection('SupplierNameChi','SupplierName')]);
        }
        $counter = 1;
        $numToChangeRow = 1;

        foreach($supplierListAry as $_supplier){
            $isChecked = 0;
            if($selectedSupplierID==$_supplier[0]){
                $isChecked = 1;
            }
            if($_supplier[2]!=''){
                $displayPrice = ' ($'.$_supplier[2].')';
            }
            else{
                $displayPrice = '';
            }
            $Class = ($_supplier[3])?'notMin' : 'min';

            $supplierRadios .= $linterface->Get_Radio_Button('supplierRadio_'.$_supplier[0], 'supplierRadio', $_supplier[0], $isChecked, $Class, $_supplier[1].$displayPrice, $Onclick="onClickSuppiler(this.value);",$isDisabled=0);
            if($counter%$numToChangeRow==0){
                $supplierRadios .= '<br>';
            }
            $counter++;
        }
        echo $supplierRadios;
    }
	public function getFinancialItemBudget(){
		global $_PAGE,$Lang;
		$postVar = $_PAGE['POST'];
		$itemID = $postVar['itemID'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$budget = $mgmtObj->getFinancialItemBudget($itemID); 
		$usedAmount = $mgmtObj->getItemSpendingUsed($itemID);
		$budgetLeft = number_format($budget-$usedAmount);
		
		$textToShow = $Lang['ePCM']['Mgmt']['Case']['BudgetLeft'];
		$textToShow = str_replace('{-Budget-}',$budgetLeft,$textToShow);
		
		if($budgetLeft<0){
			$textToShow = '<span style="color:red">'.$textToShow.'</span>';
		}
		
		echo $textToShow;
	}
	public function getCaseDates(){
		echo 'return';
	}
	public function refreshUserSelection(){
		global $_PAGE, $intranet_root;
		$postVar = $_PAGE['POST'];
		$groupID = $postVar['groupID'];
		$roleID = $postVar['roleID'];
		$userIDAry = $postVar['userIDAry'];
		$groupCat = $postVar['groupCat'];
		$noMultiple = $postVar['noMultiple'];
		if($noMultiple){
			$multiple = false;
		}else{
			$multiple = true;
		}
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		if($groupCat[0] == 'I'){
            $teaching = ($groupCat == 'I1')? 1: 0;
    		$userIDAry = explode(',',$userIDAry);
    		$userSelection = $ui->Get_User_Selection($groupCat,$userIDAry,$multiple);
		}
		else if($groupCat[0] == 'G'){
            $intranetGroupID = substr($groupCat,1);
            $excludeUserCond = " UserID not in ('".implode("','", (array)$userIDAry)."')";
            include_once($intranet_root."/includes/libgroup.php");
            $libgroup = new libgroup($intranetGroupID);
            $groupUserArr = $libgroup->returnGroupUser('',$excludeUserCond);
            $userSelection = $ui->Get_User_Selection($groupCat,$userIDAry,$multiple);
            
		}else if($groupCat=='P'){
			$userIDAry = explode(',',$userIDAry);
			$userSelection = $ui->Get_User_Selection($groupCat,$userIDAry,$multiple);
		}elseif($groupCat=='SMC'){
			$userIDAry = explode(',',$userIDAry);
			$userSelection = $ui->Get_User_Selection($groupCat,$userIDAry,$multiple);
		}
		echo $userSelection;
	}
	public function groupMemberSearchUser(){
		global $_PAGE,$ePCMcfg,$junior_mck,$sys_custom;
		
		
		$type = $_PAGE['GET']['type'];
		$groupID = $_PAGE['GET']['groupID'];
		$roleID = $_PAGE['GET']['roleID'];
		$notAvalUserAry = explode(',',$_PAGE['GET']['notAvalUserAry']);
		$includeParent = $_PAGE['GET']['includeParent'];
// 		debug_pr($includeParent);
//  		debug_pr($roleID);
		
		# Get data
// 		$YearID = $_REQUEST['YearID'];
		//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
		$SelectedUserIDList = $_PAGE['GET']['SelectedUserIDList'];
		$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
		$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
		$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);
		
// 		debug_pr($notAvalUserAry);
		
		$notAvalUserAry = array_merge($SelectedUserIDListArr,$notAvalUserAry);
// 		debug_pr($notAvalUserAry);
		
		//Get existing user in Group
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
// 		debug_pr($groupID);
		if($type=='group'){
			$existingUser = $db->getGroupMemberInfo($groupID);
			$existingUserIDAry = Get_Array_By_Key($existingUser,'UserID');
		}else if($type=='role'){
// 			$existingUser = $db->getRoleUser($roleID,$academicYearID='');
// 			$existingUserIDAry = Get_Array_By_Key($existingUser,'UserID');
			$existingUserIDAry = array();
		}
		
		//debug_pr($existingUserIDAry);
		
		//debug_pr($notAvalUserAry);
		$notAvalUserAry = array_merge((array)$notAvalUserAry,(array)$existingUserIDAry);
		//debug_pr($notAvalUserAry);
		if(!empty($notAvalUserAry)){
			$usercond = " AND cfgiu.USERID NOT IN ('".implode("','",$notAvalUserAry)."') ";
		}
		
		$libdb = new libdb();
// 		$SearchValue = $libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q']))));
		$SearchValue = $libdb->Get_Safe_Sql_Like_Query(($_PAGE['GET']['q']));
		if(isset($junior_mck)){
			$name_field = getNameFieldByLang("cfgiu.",true);
		}else{
			$name_field = getNameFieldByLang("cfgiu.");
		}
		
		if($includeParent){
			$searchTypeCond = " AND (cfgiu.RecordType = 1 or cfgiu.RecordType = 3) ";
			if($sys_custom['ePCM']['SMC']){
				$searchTypeCond = " AND (cfgiu.RecordType = 1 or cfgiu.RecordType = 3 or cfgiu.RecordType = 5) ";
			}
		}else{
			$searchTypeCond = " AND cfgiu.RecordType = 1 ";
			if($sys_custom['ePCM']['SMC']){
				$searchTypeCond = " AND (cfgiu.RecordType = 1 or cfgiu.RecordType = 5)";
			}
		}
		
		$sql = "SELECT  
					cfgiu.UserID, 
					".$name_field." as Name, 
					cfgiu.UserLogin
				FROM 
					".$ePCMcfg['INTRANET_USER']." AS cfgiu INNER JOIN INTRANET_USER AS iu ON (cfgiu.UserID = iu.UserID)			
				WHERE 
					cfgiu.RecordStatus = 1
					$searchTypeCond
					$usercond
					AND ( cfgiu.ChineseName like '%".$SearchValue."%' 
							or cfgiu.EnglishName like '%".$SearchValue."%'
							OR cfgiu.UserLogin like '%".$SearchValue."%') 
				Order by cfgiu.EnglishName asc
				Limit 5";
		$result = $libdb->returnResultSet($sql);
		
		$returnString = '';
		foreach((array)$result as $_StudentInfoArr){
			$_UserID = $_StudentInfoArr['UserID'];
			$_Name = $_StudentInfoArr['Name'];
			$_ClassName = $_StudentInfoArr['ClassName'];
			$_ClassNumber = $_StudentInfoArr['ClassNumber'];
			$_thisUserNameWithClassAndClassNumber = $_Name;
		
			$returnString .= $_thisUserNameWithClassAndClassNumber.'|'.'|'.'U'.$_UserID."\n";
		}
		
		intranet_closedb();

		echo $returnString;
				
	}
	public function getFinanialItem(){
		global $_PAGE,$Lang;
		$groupID = $_PAGE['POST']['groupID'];
		$itemID = $_PAGE['POST']['itemID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		
		$settings = $db->getGeneralSettings();
		$academicYearID = isset($settings['defaultAcademicYearID'])? $settings['defaultAcademicYearID'] : Get_Current_Academic_Year_ID();
		$financialItemInfoAry = $db->getFinancialItemByGroup($groupID);
		$financialItemBudgetAssoc = BuildMultiKeyAssoc($db->getGroupFinancialItemBudgetByGroupId($groupID, $academicYearID,''), 'ItemID');
		
		$fiParData = array();
		$x = '';
		foreach($financialItemInfoAry as $num=>$fiAry){
			$fiParData[$num][1] = $fiAry[Get_Lang_Selection('ItemNameChi','ItemName')];
			if($_key%3==0&&$_key!=0){
				$x .= '<br>';
			}
			if($itemID==$fiAry['ItemID']){
				$checked = true;
			}
			
			# for default funding
			$defaultFunding = $financialItemBudgetAssoc[$fiAry['ItemID']]['FundingID'];
			$fundTag = $defaultFunding != ''? 'data-fund="'.$defaultFunding.'"' : 'data-fund="0"';
			
			$x .= $ui->Get_PCM_Radio_Button('item_'.$fiAry['ItemID'], 'itemID', $Value=$fiAry['ItemID'], $checked, $Class="financialItemRadio", $Display=$fiAry[Get_Lang_Selection('ItemNameChi','ItemName')], $Onclick="ajax_budget(this.value);",$isDisabled=0, $fundTag).'&nbsp;&nbsp;';
			$checked = false;
		}
		
		echo $x;
	}
	public function returnExceedBudget(){
		global $_PAGE;
		$budget = $_PAGE['POST']['budget'];
		$groupID = $_PAGE['POST']['groupID'];
		$itemID = $_PAGE['POST']['itemID'];
		
		//Get Budget of Group
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$groupInfo = $db->getGroupBudget($groupID);
		$groupBudget = $groupInfo[0]['Budget'];
		$usedAmount_group = $db->getGroupSpendingUsed($groupID);
		$groupBudgetLeft = $groupBudget-$usedAmount_group;

		//Get Finiancial Budget
		$financialBudget = $db->getFinancialItemBudget($itemID);
		$usedAmount_item = $db->getItemSpendingUsed($itemID);
		$itemBudgetLeft = $financialBudget-$usedAmount_item;
		
		//Get Setting -> exceed_budget_percentage
        $settings = $_SESSION['ePCM']['Setting']['General'];
		$exceed_budget_percentage = $settings['exceed_budget_percentage'];
		$exceed_budget_percentage = floatval($exceed_budget_percentage);
		
		
		if($groupBudgetLeft*(1+$exceed_budget_percentage) < $budget){
			$over_budget_group = true;
		}
		if($itemBudgetLeft*(1+$exceed_budget_percentage) < $budget){
			$over_budget_item = true;
		}
		
		if($over_budget_group&&$over_budget_item){
			echo 'GROUP:ITEM';
		}else if($over_budget_group){
			echo 'GROUP';
		}else if($over_budget_item){
			echo 'ITEM';
		}else{
			echo 'OK';
		}
		
	}
	public function returnPriceSummaryFromAllPriceItem(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$priceItemInfo = $db->getQuotationPriceItemInfo($caseID=$_PAGE['POST']['caseID'],$quotationID='',$supplierID=$_PAGE['POST']['supplierID']);
		
		$sum = 0;
		foreach ((array)$priceItemInfo as $_priceItem){
			$sum = $sum + $_priceItem['Price'];
		}
		echo $sum;
	}
	public function returnEmailPreview(){
		global $_PAGE,$Lang,$_SESSION,$junior_mck;
		$quotationID = $_PAGE['POST']['quotationID'];
		if($quotationID==''){
			echo '';
			die();
		}
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$quotations = $db->getCaseQuotation('',$quotationID);
		$supplierID = $quotations[0]['SupplierID'];
		$caseID = $quotations[0]['CaseID'];
		$array_supplier=$db->getSupplierInfo($supplierID);
		$rows_case=$db->getCaseInfo($caseID);
		$row_case=$rows_case[0];
		$rows_supplier=$array_supplier['MAIN'];
		$row_supplier=$rows_supplier[0];
		
		$case_name=$row_case['CaseName'];
		$case_number=$row_case['Code'];
		$supplier_name=$row_supplier['SupplierName'];
		
		$school_name=GET_SCHOOL_NAME();
		######EJ Only######
		if(isset($junior_mck)){
			$school_name = convert2unicode(trim($school_name), 1);
		}
		
		$pcm = $_PAGE['libPCM'];
		
		$sender_email = $pcm->getSenderEmail($_SESSION['UserID']);
		$sender_email = 'no-reply'.substr($sender_email,strpos($sender_email,'@'));
		$receiver_email = $row_supplier['Email'];
		$subject = $Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Subject'];
		$content = $Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Body'];
		
		$str_identifier1='<!--SUPPLIER_NAME-->';
		$str_identifier2='<!--CASE_NAME-->';
		$str_identifier3='<!--CASE_NUMBER-->';
		$str_identifier4='<!--SCHOOL_NAME-->';
		
		$subject=str_replace($str_identifier3,$case_number,$subject);
		$content=str_replace($str_identifier1,$supplier_name,$content);
		$content=str_replace($str_identifier2,$case_name,$content);
		$content=str_replace($str_identifier3,$case_number,$content);
		$content=str_replace($str_identifier4,$school_name,$content);
		
		//Sample api link and password
		$url_start_identifier = '<!--TOKEN_URL_START-->';
		$url_identifier = '<!--TOKEN_URL-->';
		$url_end_identifier = '<!--TOKEN_URL_END-->';
		$pw_identifier = '<!--API_PASSWORD-->';
		
		$sample_url = $_SESSION['SSV_HTTP_HOST'].'/api/acknowledge.php?t=sampleToken';
		$sample_pw = 'sample_password';
		$content=str_replace($url_start_identifier,'<a href="javascript:void(0);">',$content);
		$content=str_replace($url_identifier,$sample_url,$content);
		$content=str_replace($url_end_identifier,'</a>',$content);
		$content=str_replace($pw_identifier,$sample_pw,$content);
		
		$x = '';
		$x.= 'To: '.$receiver_email.'<br>';
		$x.= 'From: '.$sender_email.'<br>';
		$x.= 'Subject: '.$subject.'<br><hr>';
		$x.= $content;
		
		echo $x;
	}
	
	## Refresh
	public function refreshNotificationUser(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$returnSet = $db->getNotificationScheduleUser($_PAGE['POST']['caseID'],$stageID=2);
		$textToShow ='';
		foreach ($returnSet as $_notiUser){
			$textToShow.=$_notiUser['Name'].'<br>';
		}
		echo $textToShow;
	}
	public function refreshQuotationReplyDate(){
		global $_PAGE;
		$quotationID = $_PAGE['POST']['quotationID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$quotationInfo = $db->getCaseQuotation($caseID='',$quotationID);
		$ary = explode(' ',$quotationInfo[0]['ReplyDate']);
		echo $ary[0];
	}
	public function refreshExpiryDate(){
		global $_PAGE;
		$caseID = $_PAGE['POST']['caseID'];
		$endDate = $_PAGE['POST']['endDate'];
		
		$timeNow = date("Y-m-d H:i:s");
		$today = strtotime($timeNow);
		$dayLeft = (strtotime($endDate) - $today)/(60*60*24) ;
		
		echo floor($dayLeft);
	}
	public function refreshCaseCode(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		//Get CaseInfo
		$caseInfoAry =  $db->getCaseInfo($_PAGE['POST']['caseID'],'','','');
		echo $caseInfoAry[0]['Code'];
	}
	public function refreshCaseName(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		//Get CaseInfo
		$caseInfoAry =  $db->getCaseInfo($_PAGE['POST']['caseID'],'','','');
		echo $caseInfoAry[0]['CaseName'];
	}
	
	### check unique
	public function checkUnique(){
		global $_PAGE;
	
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
	
		$profile=$_PAGE['POST']['profile'];
		$language=$_PAGE['POST']['language'];
		$value=$_PAGE['POST']['value'];
		$excluded_value=$_PAGE['POST']['excluded_value'];
		
		$language = ($language==''?'en':'b5');
	
		switch($profile){
			case '1':
				$table='INTRANET_PCM_CATEGORY';
				$field='Code';
				$having_is_deleted=true;
				$allow_time_of_duplication=0;
				break;
			case '2':
				$table='INTRANET_PCM_GROUP';
				$field='Code';
				$having_is_deleted=true;
				$allow_time_of_duplication=0;
				break;
			case '3':
				$table='INTRANET_PCM_GROUP_FINANCIAL_ITEM';
				$field='Code';
				$having_is_deleted=true;
				$allow_time_of_duplication=0;
				break;
			case '4':
				$table='INTRANET_PCM_SUPPLY_TYPE';
				$field=$language=='en'?'TypeName':'TypeNameChi';
				$having_is_deleted=true;
				$allow_time_of_duplication=0;
				break;
			case '5':
				$table='INTRANET_PCM_CASE';
				$field='Code';
				$having_is_deleted=true;
				$allow_time_of_duplication=0;
				break;
		}
	
		$result=$db->checkDuplicate($table,$field,$value,$excluded_value,$having_is_deleted,$allow_time_of_duplication);
	
		echo $result?'UNIQUE':'DUPLICATED';
	}
	
	### get list of supplier type
	
	public function getSupplierTypeList(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		
		$list = $db->getSupplierType();
		
		$return_string='';
		foreach($list as $key=>$element){
			$return_string.=($return_string==''?'':'###').$key.'==='.$element;
		}
		
		echo $return_string;
	}
	
	public function getGroupBudget(){
		global $_PAGE;
		$_PAGE['POST']['GroupID'];
		$_PAGE['POST']['AcademicYear'];
		//print_r($_PAGE['POST']);
		
		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$rows_group_budget=$db->getGroupBudgetByGroupIdAndAcademicYearId($_PAGE['POST']['GroupID'],$_PAGE['POST']['AcademicYear']);
		$rows_group_financial_item=$db->getFinancialItemByGroup($rows_group_budget[0]['GroupID']);
		$rows_group_item_budget=$db->getGroupFinancialItemBudgetByGroupId($rows_group_budget[0]['GroupID'],$rows_group_budget[0]['AcademicYearID'],$rows_group_budget[0]['TermID']);
		
		$row_group_budget=$rows_group_budget[0];
		
		$item_budget_id_value=array();
		foreach((array)$rows_group_item_budget as $row_group_item_budget){
			$budget_value[$row_group_item_budget['ItemID']]=$row_group_item_budget['Budget'];
			$item_budget_id_value[$row_group_item_budget['ItemID']]=$row_group_item_budget['ItemBudgetID'];
		}
		
		$return=array(
			'GroupID'=>$_PAGE['POST']['GroupID'],
			'GroupBudgetID'=>$row_group_budget['GroupBudgetID'],
			'AcademicYear'=>$_PAGE['POST']['AcademicYear'],
			'GroupBudget'=>$row_group_budget['Budget'],
		);
		foreach((array)$rows_group_financial_item as $row_group_financial_item){
			$return['Budget_'.$row_group_financial_item['ItemID']]=$budget_value[$row_group_financial_item['ItemID']];
			$return['ItemBudgetID_'.$row_group_financial_item['ItemID']]=$item_budget_id_value[$row_group_financial_item['ItemID']];
		}
		
		echo implode(',',array_keys($return));
		echo ';';
		echo implode(',',$return);
	}
	public function loadReference(){
		global $_PAGE,$Lang;
		$flag = $_PAGE['POST']['flag'];
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		
		
		if($flag=='supplyType'){
			$titleArray = array($Lang['General']['Code'],$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'],$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName']);
			$supplyTypeInfoAry = $settingObj->getSupplyTypeInfo();
			$contentArray = array();
			foreach($supplyTypeInfoAry as $_supplierTypeInfo){
				$contentArray[] = array($_supplierTypeInfo['TypeID'],$_supplierTypeInfo['TypeNameChi'],$_supplierTypeInfo['TypeName']);
			}
		}
		
		
		$output = $ui->getReferenceThickBox($titleArray,$contentArray);
		echo $output;
		
	}
	public function getFundingCategoryThickBox(){
		global $_PAGE,$Lang;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$CategoryData = $db->getFundingCategoryRawData();
		$AjaxTable = "<table class='common_table_list ClassDragAndDrop' id='CategoryTable'>";
			$AjaxTable .= "<thead>";
				$AjaxTable .= "<th class='row_content'>";
					$AjaxTable .= $Lang['ePCM']['FundingSource']['ChineseName'];
				$AjaxTable .= "</th>";
				$AjaxTable .= "<th class='row_content'>";
					$AjaxTable .= $Lang['ePCM']['FundingSource']['EnglishName'];
				$AjaxTable .= "</th>";
				$AjaxTable .= "<th class='row_content'>";
					$AjaxTable .= "&nbsp;";	
				$AjaxTable .= "</th>";
			$AjaxTable .= "</thead>";
			$AjaxTable .= "<tbody>";
			###Category Start
			if($CategoryData){
				foreach ($CategoryData as $_Category){
						$AjaxTable .= "<tr>";
						$AjaxTable .= "<td>";
							$AjaxTable .= "<div class='CategoryThick_View_".$_Category['FundingCategoryID']."'>";
								$AjaxTable .= $_Category['FundingCategoryNameCHI'];
							$AjaxTable .= "</div>";
							$AjaxTable .= "<div class='CategoryThick_Edit_".$_Category['FundingCategoryID']."' style='display:none;'>";
								$AjaxTable .= "<input type='text' id = 'CategoryThick_Edit_CHIRow_".$_Category['FundingCategoryID']."' value='".$_Category['FundingCategoryNameCHI']."'>";
								$AjaxTable .= "<font color='red' id = 'CategoryThick_Edit_CHIAlert_".$_Category['FundingCategoryID']."' class = 'Alert' style='display: none;'>"."</br>".$Lang['ePCM']['FundingSource']['EditAlert']."</font>";
							$AjaxTable .= "</div>";
						$AjaxTable .= "</td>";
						$AjaxTable .= "<td>";
							$AjaxTable .= "<div class='CategoryThick_View_".$_Category['FundingCategoryID']."'>";
								$AjaxTable .= $_Category['FundingCategoryNameEN'];
							$AjaxTable .= "</div>";
							$AjaxTable .= "<div class='CategoryThick_Edit_".$_Category['FundingCategoryID']."' style='display:none;'>";
								$AjaxTable .= "<input type='text' id = 'CategoryThick_Edit_ENRow_".$_Category['FundingCategoryID']."' value='".$_Category['FundingCategoryNameEN']."'>";
								$AjaxTable .= "<font color='red' id = 'CategoryThick_Edit_ENAlert_".$_Category['FundingCategoryID']."' class = 'Alert' style='display: none;'>"."</br>".$Lang['ePCM']['FundingSource']['EditAlert']."</font>";
							$AjaxTable .= "</div>";
						$AjaxTable .= "</td>";
						$AjaxTable .= "<td>";
							$AjaxTable .= "<div class='table_row_tool row_content_tool CategoryThick_View_".$_Category['FundingCategoryID']."'>";
								$AjaxTable .= "<a href='#' onclick='js_Edit_FundingCategory_Row(".$_Category['FundingCategoryID'].");' class='edit_dim' title=''></a>";
								$AjaxTable .= "<a href='#' onclick='js_Delete_FundingCategory(".$_Category['FundingCategoryID'].");' class='delete_dim' title=''></a>";
							$AjaxTable .= "</div>";
							$AjaxTable .= "<div class='table_row_tool row_content_tool CategoryThick_Edit_".$_Category['FundingCategoryID']."' style='display:none;'>";
								$AjaxTable .= $_PAGE['libPCM_ui']->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "js_Edit_Category(".$_Category['FundingCategoryID'].");", "NewAcademicYearSubmitBtn");
								$AjaxTable .= $_PAGE['libPCM_ui']->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_CancelEdit_Category(".$_Category['FundingCategoryID'].");", "NewAcademicYearSubmitBtn");
							$AjaxTable .= "</div>";
						$AjaxTable .= "</td>";
						$AjaxTable .= "</tr>";
				}
			}else{ //No Category Record
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td colspan='3' align='center'>";
						$AjaxTable .= $Lang['General']['NoRecordAtThisMoment'];
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
			}
			###Category End
			
			###Add Row
			$AjaxTable .= "<tr id='AddCategoryRow'>";
				$AjaxTable .= "<td>";
					$AjaxTable .= "&nbsp;";
				$AjaxTablbe .= "</td>";
				$AjaxTable .= "<td>";
					$AjaxTable .= "&nbsp;";
				$AjaxTable .= "</td>";
				$AjaxTable .= "<td>";
					$AjaxTable .= "<div class='table_row_tool row_content_tool'>"."\n";
					//Remind to Add js_AddCategory Row in the page u use this ajax
					$AjaxTable .= "<a href='#' onclick='js_Add_FundingCategory_Row();' class='add_dim' title=''></a>";
					$AjaxTable .= "</div>"."\n";
				$AjaxTable .= "</td>";
			$AjaxTable .= "</tr>";
			$AjaxTable .= "</tbody>";
		$AjaxTable .= "</table>";
		###Table END
		###js Reload main view
		$AjaxTable .= "
			<script>
			$( document ).ready(function() {
				document.getElementById('TB_closeWindowButton').onclick=function(){
					location.reload();
				}
			});
			</script>";
		
		echo $AjaxTable;
	}
	function getFundingSourceThickBox(){
		global $_PAGE,$Lang;
		$CategoryID = $_POST['CategoryID'];
		$FundingID = $_POST['FundingID'];
		##Data Start
		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$CategoryData = $db->getFundingCategoryRawData($CategoryID);
		if($FundingID){
			$FundingData = $db->getFundingRawData($FundingID);
		}
		##Data End
		###Print Table Start
		$AjaxTable = "<form id='FundingThockBox'>";
			$AjaxTable .= "<table class='form_table_v30'>";
				//Category
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td width='25%' class='field_title'>";
						$AjaxTable .= $Lang['ePCM']['FundingSource']['Category'];
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td width='1%'>";
						$AjaxTable .= ":";
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td>";
						$AjaxTable .= Get_Lang_Selection($CategoryData[$CategoryID]['FundingCategoryNameCHI'], $CategoryData[$CategoryID]['FundingCategoryNameEN']);
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
				//Chinese
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td width='25%' class='field_title'>";
						$AjaxTable .= $Lang['ePCM']['FundingSource']['ChineseName'];
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td width='1%'>";
						$AjaxTable .= ":";
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td>";
						$AjaxTable .= "<input type='text' id='FundingThichBox_FundingChineseName' name='FundingThichBox_FundingChineseName' value='".$FundingData[$FundingID]['FundingNameCHI']."'>" ;
						$AjaxTable .= "<font color='red' id='FundingThichBox_FundingChineseName_Alert' Class='Alert' style='display:none;'>"."</br>".$Lang['ePCM']['FundingSource']['EditAlert']."</font>";
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
				//English
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td width='25%' class='field_title'>";
						$AjaxTable .= $Lang['ePCM']['FundingSource']['EnglishName'];
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td width='1%'>";
						$AjaxTable .= ":";
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td>";
						$AjaxTable .= "<input type='text' id='FundingThichBox_FundingEnglishName' name='FundingThichBox_FundingEnglishName' value='".$FundingData[$FundingID]['FundingNameEN']."'>" ;
						$AjaxTable .= "<font color='red' id='FundingThichBox_FundingEnglishName_Alert' Class='Alert' style='display:none;'>"."</br>".$Lang['ePCM']['FundingSource']['EditAlert']."</font>";
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
				//Budget
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td width='25%' class='field_title'>";
						$AjaxTable .= $Lang['ePCM']['FundingSource']['Budget'];
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td width='1%'>";
						$AjaxTable .= ":";
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td>";
						$AjaxTable .=  "<input type='text' id='FundingThichBox_FundingBudget' name='FundingThichBox_FundingBudget' value='".$FundingData[$FundingID]['Budget']."'>";
						$AjaxTable .= "<font color='red' id='FundingThichBox_FundingBudget_Alert' Class='Alert' style='display:none;'>"."</br>".$Lang['ePCM']['FundingSource']['EditAlert']."</font>";
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
				//RecordStatus
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td width='25%' class='field_title'>";
						$AjaxTable .= $Lang['ePCM']['FundingSource']['RecordStatus']['RecordStatus'];
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td width='1%'>";
						$AjaxTable .= ":";
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td>";
						if($FundingData[$FundingID]['RecordStatus']=='2'){
// 							$RecordStatus_Check_0 = "";
							$RecordStatus_Check_1 = "";
							$RecordStatus_Check_2 = "Checked";
						}elseif($FundingData[$FundingID]['RecordStatus']=='1'){
// 							$RecordStatus_Check_0 = "";
							$RecordStatus_Check_1 = "Checked";
							$RecordStatus_Check_2 = "";
						}else{
// 							$RecordStatus_Check_0 = "Checked";
// 							$RecordStatus_Check_1 = "";
// 							$RecordStatus_Check_2 = "";
							$RecordStatus_Check_1 = "Checked";
							$RecordStatus_Check_2 = "";
						}
// 						$AjaxTable .= "<input type='radio' value='0' id='FundingThichBox_FundingRecordStatus_0' name='FundingThichBox_FundingRecordStatus' $RecordStatus_Check_0><label for='FundingThichBox_FundingRecordStatus_0'>".$Lang['ePCM']['FundingSource']['RecordStatus'][0]."</label>";
						$AjaxTable .= "<input type='radio' value='1' id='FundingThichBox_FundingRecordStatus_1' name='FundingThichBox_FundingRecordStatus' $RecordStatus_Check_1><label for='FundingThichBox_FundingRecordStatus_1'>".$Lang['ePCM']['FundingSource']['RecordStatus'][1]."</label>";
						$AjaxTable .= "<input type='radio' value='2' id='FundingThichBox_FundingRecordStatus_2' name='FundingThichBox_FundingRecordStatus' $RecordStatus_Check_2><label for='FundingThichBox_FundingRecordStatus_2'>".$Lang['ePCM']['FundingSource']['RecordStatus'][2]."</label>";
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
				//DeadLine
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td width='25%' class='field_title'>";
						$AjaxTable .= $Lang['ePCM']['FundingSource']['DeadLine'];
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td width='1%'>";
						$AjaxTable .= ":";
					$AjaxTable .= "</td>";
					$AjaxTable .= "<td>";
						$AjaxTable .= $ui->GET_DATE_PICKER('FundingThichBox_FundingDeadline',$FundingData[$FundingID]['DeadLineDate']);
						$AjaxTable .= "<font color='red' id='FundingThichBox_FundingDeadline_Alert' Class='Alert' style='display:none;'>"."</br>".$Lang['ePCM']['FundingSource']['EditAlert']."</font>";
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
			$AjaxTable .= "</table>";
			$AjaxTable .= "</br>";
			# submit button & cancel button
			$AjaxTable .= "<div class='edit_bottom'>";
				$AjaxTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
				$AjaxTable .= "<tr><td><p class='spacer'></p></td></tr>";
				$AjaxTable .= "<tr>";
					$AjaxTable .= "<td align='center'>";
						$AjaxTable .= "<input id='NewTermSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Submit']."' onclick='js_updateFunding($FundingID);' >";
						$AjaxTable .= "<input id='NewTermCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >";
					$AjaxTable .= "</td>";
				$AjaxTable .= "</tr>";
				$AjaxTable .= "</table>";
			$AjaxTable .= "</div>";
			$AjaxTable .= "<input type='hidden' id='FundingThichBox_FundingID' name='FundingThichBox_FundingID' value='$FundingID'>";
			$AjaxTable .= "<input type='hidden' id='FundingThichBox_FundingCategoryID' name='FundingThichBox_FundingCategoryID' value='$CategoryID'>";
			$AjaxTable .= "<input type='hidden' id='FundingThichBox_DisplayOrder' name='FundingThichBox_DisplayOrder' value='".$FundingData[$FundingID]['DisplayOrder']."'>";
		$AjaxTable .= "</form>";
		###Print Table End
		echo $AjaxTable;
	}
	function getFundingSourceSelectionBox(){
		global $_PAGE,$Lang;
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$ajaxSelectionBox = $ui->getFundingByCategorySelectionBox($_POST['id']);
		echo $ajaxSelectionBox;
	}
	function checkingFundingBudgetValid(){
		global $_PAGE,$Lang, $intranet_root;
		include_once($intranet_root."/includes/json.php");
		## Data Start
		$FundingIDArr = $_PAGE['POST']['Funding_Selection_Box'];
		$BudgetUseArr = $_PAGE['POST']['Funding_Selection_Box_usingBudget'];
		$Budget =  $_PAGE['POST']['budget'];
// 		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
// 		$FundingRawData = $db->getFundingRawData();
// 		$FundingCaseRawData = $db->getFundingCaseRelationRawData();
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$FundingSourceArr = $db->getFundingCaseArr();

		$json = new JSON_obj();
		## Data END
		$TotalBudgetUser = 0;
		foreach ($FundingIDArr as $key=>$FundingID){
			if($FundingID){
				$TotalBudgetUser += $BudgetUseArr[$key];
			}
		}
		###	 Step 0 Check Budget cannot be null
		foreach ($FundingIDArr as $key=>$FundingID){
			if($FundingID){
				$BudgetUse = $BudgetUseArr[$key];
				if($BudgetUse){
					//pass Step 0
					###  Step 1 Check Budget use is decimal
					$BudgetUse_temp = explode('.',$BudgetUse);
					if(sizeof($BudgetUse_temp)>2){
						$error[$key] = $error[$key] = $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DecimalPoint'];
						//error
					}else{
						$check1 = is_numeric($BudgetUse_temp[0]); 
						if($BudgetUse_temp[1]){
							$check2 = is_numeric($BudgetUse_temp[1]);
						}else{
							$check2 = true;
						}
						$check3 = ($BudgetUse_temp[1]/100 > 1)?false:true;
						if($check1&&$check2&&$check3){
							//pass step1 
							$x= array_count_values($FundingIDArr);
							###  Step 2 Check Funding ID cannot be duplicate
							if($x[$FundingID]>1){
// 								$error[$key] = 'step2';
								$error[$key] = $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DuplicatedFunding'];
							}else{
								//pass step2
								###	 Step 3 Check Budget Remain must be greater than Budget use
								$BudgetRemain = $FundingSourceArr[$FundingID]['BugetRemain'];
// 								$BudgetRemain = 	$FundingRawData[$FundingID]['Budget'];
// 								foreach ((array)$FundingCaseRawData[$FundingID] as $_FundingCaseRawData){
// 									$BudgetRemain = $BudgetRemain - $_FundingCaseRawData['Amount'];
// 								}
								if($BudgetRemain-$BudgetUse>=0 && $BudgetUse>0){
									//pass Step 3
									if($TotalBudgetUser>$Budget){
										$error[$key] = $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['FundingOverBudget'];
									}
									
								}else{
// 									$error[$key] = 'step3';
									$error[$key] = $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['OverFundingSourceBudget'];
								}
							}
						}else{
// 							$error[$key] = 'step1';
							$error[$key] = $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DecimalPoint'];
						}
					}
				}else{
// 					$error[$key] = 'step0';
					$error[$key] = $Lang['ePCM']['FundingSource']['EditAlert'];
				}
			}
		}
		$error_msg = $json->encode($error);
		echo $error_msg;
	}
	function getFundingCaseThichkBox(){
		global $_PAGE,$Lang, $intranet_root;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$FundingID = $_PAGE['POST']['FundingID'];
		###Data Start 
		###Data End
		###UI Start
		$AjaxTable = '<table width="100%">';
			$AjaxTable .= '<tr>';	//
				$AjaxTable .= '<td>';
					$AjaxTable .= '<table width="100%">';
						$AjaxTable .= '<tr>';
							$AjaxTable .= '<td>';
								$AjaxTable .= $ui->getFundingCaseTable($FundingID,'0');
							$AjaxTable .= '</td>';
						$AjaxTable .= '</tr>';
						$AjaxTable .= '<tr>';
							$AjaxTable .= '<td>';
								$AjaxTable .= $ui->getFundingCaseTable($FundingID,'1');
							$AjaxTable .= '</td>';
						$AjaxTable .= '</tr>';
// 						$AjaxTable .= '<tr>';
// 							$AjaxTable .= '<td>';
// 								$AjaxTable .= $ui->getFundingCaseTable($FundingID,'2');
// 							$AjaxTable .= '</td>';
// 						$AjaxTable .= '</tr>';
					$AjaxTable .= '</table>';
				$AjaxTable .= '</td>';
			$AjaxTable .= '</tr>';
		$AjaxTable .= '</table>';
		###UI End
		echo $AjaxTable;
	}


    function checkRuleApplicationForm(){
        global $_PAGE, $ePCMcfg;
        $budget = $_PAGE['POST']['budget'];
        if($budget=='' || $budget==null){
            $budget=0;
        }
        $caseID = $_PAGE['POST']['caseID'];
        if($caseID=='' || $caseID==null){
            $caseID = -1;
        }
        $db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
        $sql = "Select 
                  RuleID 
                From 
                  INTRANET_PCM_RULE 
                WHERE 
                  IsTemplate = 1 
                  AND IsDeleted = '0'  
                  AND FloorPrice <=$budget 
                  AND (CeillingPrice>=$budget or CeillingPrice IS NULL)
                ";
        $ruleID = $db->returnResultSet($sql);
        $_ruleID = $ruleID[0]['RuleID'];
        $html = $ui->getTemplateFileUI($ePCMcfg['Template']['Application'], $_ruleID, $getAttachWithoutRuleIDAndRuleID = true, $caseID);
        echo $html;
        return $html;
	}


}

?>