<?php
// Editing by 
/*
 * 
 */
include_once("BaseController.php");

class IndexController extends BaseController
{
	public function __construct()
	{
		global $_PAGE;
		parent::__construct();
// 		$this->RequestMapping['/'] = array('function'=>'index');
		$this->RequestMapping['login'] = array('function'=>'login');
		$this->AuthMapping['login'] = array('function'=>'allUser');
		$this->RequestMapping['login.ajax'] = array('function'=>'ajaxLogin');
		$this->AuthMapping['login.ajax'] = array('function'=>'allUser');
		$this->RequestMapping['networkipfail'] = array('function'=>'redirectCheckIPFail');
		$this->AuthMapping['networkipfail'] = array('function'=>'allUser');
	}
	
// 	public function index(){
		
// 		$main = $this->getViewContent('/index_tmpl.php');
// 		$view = $this->getLayout($main, $sidebar);
// 		echo $view;
// 	}
	
	public function login(){
		global $_PAGE;
		
		$main = $this->getViewContent('/index/login.php');
		$view = $this->getLayout($main, '', '/template/form.php');
		echo $view;
	}
	
	public function ajaxLogin(){
		global $_PAGE;
		
		$password = stripslashes(trim($_PAGE['POST']['user_password']));

		if($_SESSION['eclass_session_password'] == $password){
			$_SESSION['ePCM']['session_password'] = $password;
		}
		
		echo 'RECEIVED';
	}
	
	public function redirectCheckIPFail(){
		global $_PAGE;
		
		$main = $this->getViewContent('/index/ip_fail.php');
		$view = $this->getLayout($main, '', '/template/empty.php');
		echo $view;
	}
}

?>