<?php
// Editing by Villa
/*
 * 2017-08-14 Simon
 * - modified ProcurementRecordSummary_pdf function with passing the order parameter
 * - modified ProcurementRecordSummary_csv function with passing the order parameter
 * 2017-03-01 Villa
 * - X100660 allow SMC see all the report
 * 2016-09-02 Omas
 * - Removed ajax load report directly
 * 2016-07-22 Henry HM
 * - Add field include_zero_expenses
 */
include_once("BaseController.php");

class ReportController extends BaseController
{
	
	public function __construct()
	{
		global $_PAGE, $sys_custom;
		parent::__construct();
		$this->RequestMapping[''] = array('function'=>'index');
//		no use 		
// 		$this->RequestMapping['pcmrs'] = array('function'=>'PCMRecordSummary');
// 		if($sys_custom['ePCM']['SMC']){
// 			$this->AuthMapping['pcmrs'] = array('function'=>'isAdminOrSMC');
// 		}else{
// 			$this->AuthMapping['pcmrs'] = array('function'=>'isAdmin');
// 		}
// 		$this->AuthMapping['pcmrs'] = array('function'=>'isAdmin');
		$this->RequestMapping['summary'] = array('function'=>'SummaryReportFilter');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['summary'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['summary'] = array('function'=>'isAdmin');
		}
// 		$this->AuthMapping['summary'] = array('function'=>'isAdmin');

// 		$this->RequestMapping['pcmrs_ajax'] = array('function'=>'ProcurementRecordSummary_ajax');
// 		$this->AuthMapping['pcmrs_ajax'] = array('function'=>'isAdmin');
		$this->RequestMapping['pcmrs_pdf'] = array('function'=>'ProcurementRecordSummary_pdf');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['pcmrs_pdf'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['pcmrs_pdf'] = array('function'=>'isAdmin');
		}
		$this->RequestMapping['pcmrs_csv'] = array('function'=>'ProcurementRecordSummary_csv');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['pcmrs_csv'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['pcmrs_csv'] = array('function'=>'isAdmin');
		}
// 		$this->AuthMapping['pcmrs_pdf'] = array('function'=>'isAdmin');

// 		$this->RequestMapping['fcs_ajax'] = array('function'=>'FileCodeSummary_ajax');
// 		$this->AuthMapping['fcs_ajax'] = array('function'=>'isAdmin');
		$this->RequestMapping['fcs_pdf'] = array('function'=>'FileCodeSummary_pdf');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['fcs_pdf'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['fcs_pdf'] = array('function'=>'isAdmin');
		}
		$this->RequestMapping['fcs_csv'] = array('function'=>'FileCodeSummary_csv');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['fcs_csv'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['fcs_csv'] = array('function'=>'isAdmin');
		}
// 		$this->AuthMapping['fcs_pdf'] = array('function'=>'isAdmin');
		
// 		$this->RequestMapping['ber_ajax'] = array('function'=>'BudgetExpensesReport_ajax');
// 		$this->AuthMapping['ber_ajax'] = array('function'=>'isAdmin');
		$this->RequestMapping['ber_pdf'] = array('function'=>'BudgetExpensesReport_pdf');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['ber_pdf'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['ber_pdf'] = array('function'=>'isAdmin');
		}
		$this->RequestMapping['ber_csv'] = array('function'=>'BudgetExpensesReport_csv');
		if($sys_custom['ePCM']['SMC']){
			$this->AuthMapping['ber_csv'] = array('function'=>'isAdminOrSMC');
		}else{
			$this->AuthMapping['ber_csv'] = array('function'=>'isAdmin');
		}
// 		$this->AuthMapping['ber_pdf'] = array('function'=>'isAdmin');
		
// 		$this->SecurityRegistry['r1Update']['POST'] = array(
// 				'nameEng' => 'a',
// 				'numberSel' => 'required|numeric|min:10|max:30'
// 		);
//  		$this->SecurityRegistry['r1']['PathVars'] = array(
//  				'0' => 'numeric'
//  		);
        $this->RequestMapping['EDB_Word'] = array('function'=>'generateEDBForm');
        $this->AuthMapping['EDB_Word'] = array('function'=>'allUser');
	}



	public function index(){
		$this->sendRedirect("index.php?p=report.fcs");
	}

	public function PCMRecordSummary(){

		global $_PAGE;

		$_PAGE['CurrentSection'] = 'Report';
		$_PAGE['CurrentPage'] = 'PCMRecordSummary';

		$main = $this->getViewContent('/index_tmpl.php');
		$view = $this->getLayout($main, $sidebar);

		echo $view;
	}

	public function SummaryReportFilter($type=''){
	
		global $_PAGE;
	
		switch($type){
			case 'fcs':
				$_PAGE['CurrentPage'] = 'FileCodeSummary';
				break;
			case 'pcmrs':
				$_PAGE['CurrentPage'] = 'PCMRecordSummary';
				break;
			case 'ber':
				$_PAGE['CurrentPage'] = 'BudgetExpensesReport';
				break;
			default:
				debug_pr('something wrong');
				die();
		}
		
		$_PAGE['CurrentSection'] = 'Report';
		
	
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
				
		$_PAGE['view_data']['type'] = $type;
		$_PAGE['select_group'] = $db->getUsersGroup();
		$_PAGE['select_rule'] = $db->getRuleTemplateInfo();
		$_PAGE['select_category'] = $db->getCategoryInfo(array());
		
		$main = $this->getViewContent('/report/report_filter.php');
		$view = $this->getLayout($main, $sidebar);
	
		echo $view;
	}
// 	public function ProcurementRecordSummary_ajax(){
// 		global $_PAGE;

// 		$_SESSION['ePCM']['Report']['ProcurementRecordSummary_post'] = $_PAGE['POST'];
		
// 		echo 'SUCCESS';
// 	}
	public function ProcurementRecordSummary_pdf(){
		global $_PAGE;
		ini_set('memory_limit','128M');
		
		
		
// 		$data = $_SESSION['ePCM']['Report']['ProcurementRecordSummary_post'];
// 		$array_category = explode(',',$data['categories']);
// 		$array_group = explode(',',$data['groups']);
// 		$array_rule = explode(',',$data['rules']);
// 		$array_status = explode(',',$data['status_list']);
// 		$academic_year = $data['academic_year'];
		$array_category = $_PAGE['POST']['select_category'];
		
		$array_group = $_PAGE['POST']['select_group'];
		$array_rule = $_PAGE['POST']['select_rule'];
		$array_status = $_PAGE['POST']['select_status'];
		$academic_year = $_PAGE['POST']['AcademicYearID'];
		
		$data['academic_year'] = $academic_year;
		$data['date_from'] = $_PAGE['POST']['DateFrom'];
		$data['date_to'] = $_PAGE['POST']['DateTo'];
		//applcation_type
		$data['application_type'] =  $_PAGE['POST']['applicationType'];
		//order
		$data['orders'] = $_PAGE['POST']['Orders'];
		if($data['orders'] == 'ApplyDate'){
			$data['orders'] = 'DateInput';
		}
		
		
		//read database
		$db  =  $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$_PAGE['rows_case'] = $db->getCaseInfoForReportPCMRS_v2($academic_year,$array_category,$array_group,$array_rule,$array_status,	$data['application_type'], $data['orders']);
		$_PAGE['data'] = $data;
		
		$main = $this->getViewContent('/report/procurement_record_summary_pdf.php');
		echo $main;
		
	}
	public function ProcurementRecordSummary_csv(){
		global $_PAGE, $PATH_WRT_ROOT, $Lang;
		
		$array_category = $_PAGE['POST']['select_category'];
		$array_group = $_PAGE['POST']['select_group'];
		$array_rule = $_PAGE['POST']['select_rule'];
		$array_status = $_PAGE['POST']['select_status'];
		$academic_year = $_PAGE['POST']['AcademicYearID'];
		
		$data['academic_year'] = $academic_year;
		$data['date_from'] = $_PAGE['POST']['DateFrom'];
		$data['date_to'] = $_PAGE['POST']['DateTo'];
		//applcation_type
		$data['application_type'] =  $_PAGE['POST']['applicationType'];
		//order
		$data['orders'] = $_PAGE['POST']['Orders'];
		if($data['orders'] == 'ApplyDate'){
			$data['orders'] = 'DateInput';
		}
		
		//read database
		$db  =  $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$_PAGE['reportType'] = 'pcmrs';
		$_PAGE['rows_item']= $db->getCaseInfoForReportPCMRS_v2($academic_year,$array_category,$array_group,$array_rule,$array_status, $data['application_type'], $data['orders']);
		
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$_PAGE['lexport'] = $lexport;
		
		$this->getViewContent('/report/export_csv.php');
		return true;
	}
	
// 	public function FileCodeSummary_ajax(){
// 		global $_PAGE;
		
// 		$_SESSION['ePCM']['Report']['FileCodeSummary_post'] = $_PAGE['POST'];
		
// 		//print_r($_SESSION['ePCM']['Report']['FileCodeSummary_post']);
// 		echo 'SUCCESS';
// 	}
	
	public function FileCodeSummary_pdf(){
		global $_PAGE;
		ini_set('memory_limit','128M');
		
// 		$data = $_SESSION['ePCM']['Report']['FileCodeSummary_post'];
// 		$array_category = explode(',',$data['categories']);
// 		$array_group = explode(',',$data['groups']);
// 		$array_rule = explode(',',$data['rules']);
// 		$array_status = explode(',',$data['status_list']);
// 		$academic_year = $data['academic_year'];
		$array_category = $_PAGE['POST']['select_category'];
		$array_group = $_PAGE['POST']['select_group'];
		$array_rule = $_PAGE['POST']['select_rule'];
		$array_status = $_PAGE['POST']['select_status'];
		$academic_year = $_PAGE['POST']['AcademicYearID'];
		
		$data['academic_year'] = $academic_year;
		$data['date_from'] = $_PAGE['POST']['DateFrom'];
		$data['date_to'] = $_PAGE['POST']['DateTo'];
		
		//read database
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$_PAGE['rows_case'] = $db->getCaseInfoForReportFCS($academic_year,$data['date_from'],$data['date_to'],$array_category,$array_group,$array_rule,$array_status);
		$_PAGE['data'] = $data;
		
		$main = $this->getViewContent('/report/file_code_summary_pdf.php');
		echo $main;
	}
	public function FileCodeSummary_csv(){
		global $_PAGE, $PATH_WRT_ROOT, $Lang;

		$array_category = $_PAGE['POST']['select_category'];
		$array_group = $_PAGE['POST']['select_group'];
		$array_rule = $_PAGE['POST']['select_rule'];
		$array_status = $_PAGE['POST']['select_status'];
		$academic_year = $_PAGE['POST']['AcademicYearID'];
		
		$data['academic_year'] = $academic_year;
		$data['date_from'] = $_PAGE['POST']['DateFrom'];
		$data['date_to'] = $_PAGE['POST']['DateTo'];
		
		//read database
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$_PAGE['rows_item'] = $db->getCaseInfoForReportFCS($academic_year,$data['date_from'],$data['date_to'],$array_category,$array_group,$array_rule,$array_status);
		$_PAGE['reportType'] = 'fcs';
		#patching CSV data
		$row = 0;
		$currentRange = '';
		#patching csv data end
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$_PAGE['lexport'] = $lexport;
		$this->getViewContent('/report/export_csv.php');
		return true;
	}
	
// 	public function BudgetExpensesReport_ajax(){
// 		global $_PAGE;
		
// 		$_SESSION['ePCM']['Report']['BudgetExpensesReport_post'] = $_PAGE['POST'];
		
// 		//print_r($_SESSION['ePCM']['Report']['FileCodeSummary_post']);
// 		echo 'SUCCESS';
// 	}
	
	
	public function BudgetExpensesReport_pdf(){
		global $_PAGE;
		ini_set('memory_limit','128M');
		
// 		$data = $_SESSION['ePCM']['Report']['BudgetExpensesReport_post'];
// 		$array_category = explode(',',$data['categories']);
// 		$array_group = explode(',',$data['groups']);
// 		$academic_year = $data['academic_year'];
// 		$is_include_zero_expenses = $data['include_zero_expenses'];
		$array_category = $_PAGE['POST']['select_category'];
		$array_group = $_PAGE['POST']['select_group'];
		$academic_year = $_PAGE['POST']['AcademicYearID'];
		$is_include_zero_expenses = $_PAGE['POST']['include_zero_expenses'];
		//applcation_type
		$data['application_type'] =  $_PAGE['POST']['applicationType'];

		
		$data['academic_year'] = $academic_year;
		$data['date_from'] = $_PAGE['POST']['DateFrom'];
		$data['date_to'] = $_PAGE['POST']['DateTo'];

		//read database
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$_PAGE['rows_item'] = $db->getCaseInfoForReportBER($academic_year,$array_category,$array_group,$is_include_zero_expenses,$data['application_type']);
		$_PAGE['data'] = $data;
// 		debug_pr($_PAGE['rows_item']);
		$main = $this->getViewContent('/report/budget_expenses_report_pdf.php');
		echo $main;
	}
	public function BudgetExpensesReport_csv(){
		global $_PAGE, $PATH_WRT_ROOT, $Lang;
		
		$array_category = $_PAGE['POST']['select_category'];
		$array_group = $_PAGE['POST']['select_group'];
		$academic_year = $_PAGE['POST']['AcademicYearID'];
		$is_include_zero_expenses = $_PAGE['POST']['include_zero_expenses'];
		//applcation_type
		$data['application_type'] =  $_PAGE['POST']['applicationType'];

		$data['academic_year'] = $academic_year;
		$data['date_from'] = $_PAGE['POST']['DateFrom'];
		$data['date_to'] = $_PAGE['POST']['DateTo'];
		
		//read database
		$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
		$_PAGE['rows_item'] = $db->getCaseInfoForReportBER($academic_year,$array_category,$array_group,$is_include_zero_expenses,$data['application_type']);
		$_PAGE['reportType'] = 'ber';
		
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$_PAGE['lexport'] = $lexport;
		
		$this->getViewContent('/report/export_csv.php');
		return true;
	}

	Public function generateEDBForm(){
        global $_PAGE;
        $db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
        $postVar= $_PAGE['POST'];
        $annex = $postVar['annex'];
        if($annex==2) {
            $main = $this->getViewContent('/report/export_EDB_Annex_II_word_Form.php');
        }
        echo $main;
    }
	
}

?>