<?php
// Editing by 

/* 
 * Date: 2019-06-03 Henry 
 * -add finishApprove() - support confirm completed case by principal
 * -add finishReject() - support undo confirm completed case by principal
 * 
 * Date: 2017-04-18 Villa #E114398 
 * -add caseEdit2() - editmode2 for skipping Budget
 * -add insertConfrimSkipBudget - updating case Record and skipping Budget
 * -modified newApplication - add parameter to skip budget
 * -modified updateFundingSource - support delete funding from case
 * 
 * Date: 2017-03-06 Villa
 * -modified caseInsertConfrim() - add funding source update
 * -modified caseUpdate() - add update funding source record status
 * -modified caseDetail() - add update funding source record status
 * -add updateFundingSource() - dateFundingSource Relation
 * 
 * Date: 2016-11-10	Villa
 * -modified RejectOther() - remove record in PCM_RESULT when rejecting
 * 
 * Date: 2016-11-09 Villa
 * -modified otherApprove() - add CaseApprovedDate when approve
 * -modified view() - add academic year filter
 * 
 * Date: 2016-11-03 Villa
 * -modified otherApprove() - Approve the application and count the budget for Misc Expenditure
 * -add RejectOther() - reject the application for Misc Expenditure
 * -modified caseUpdate() - other application -> for opening the setting
 * -modified caseInsertOtherConfirm() - skip insert the result to CaseResult tb in the db if the Approval setting is on
 * 
 * Date: 2016-10-12 Villa
 * -modified caseUpdate() - finalstage3 -> insert remarks 
 * -modified finishStage3() - add remarks
 * 
 * Date: 2016-10-04 Villa
 * -add otherApplicationDelete() - delete record of other application
 * -add caseInsertOtherConfirm() - insert/ edit(update) other application
 * -add finishedOtherView() - display record for other application
 * -add getCompletedOthersView() - display record for other application
 * -modified caseUpdate() - add case 'other' for other application 
 * -add newOtherApplication() - add new/edit other application
 * 
 * 
 * Date: 2016-09-28 Omas
 * - modified all eInventory operation genreal done in libPCM_db $linventory
 * 
 * Date: 2016-09-27 Villa
 * -modified caseInsertConfrim() - add GroupCode translation
 * 
 * Date: 2016-09-19 Omas
 * - modified getAddSupplierForm() - add group created supplier type => flag enable feature
 * 
 * Date: 2016-09-09 Villa
 * - modified setResult()- add funding source items
 * 
 * Date: 2016-08-26 Omas
 * - modified caseInsertConfrim(), caseUpdate(), showConfirm(), caseDetials() - for add approval mode 
 * - modified setResult() - for adding reason 
 * 
 * Date: 2016-08-24 Kenneth
 * - modified getAddSupplierForm(), tb_addSupplier -> supplier 出eng name only
 * 
 * Date: 2016-08-10 Kenneth
 * -modified setPriceComparison() access right
 * - added addSupplierByAjax()
 * - modified getCaseApprovalForm(), isApprove is false if type='N' and stage = 5
 * 
 * Date: 2016-07-06 Kenneth
 * - added getPreviewEmailForm(), add email preview function
 * - modified caseDetials(), retrive roles info for showing related users in caseDetails
 * 
 * Date: 2016-07-04	Kenneth
 * - added codeAndNameUpdate()
 * - modified view(), add search by quotation type
 * 
 * Date:	2016-06-29	Kenneth
 * 	- modified getCaseResultForm(), skip stage 4 for vebal quotation
 */
include_once("BaseController.php");

class MgmtController extends BaseController
{
	public function __construct()
	{
		global $_PAGE;
		parent::__construct();
		### View
		$this->RequestMapping[''] = array('function'=>'index');
		$this->RequestMapping['view'] = array('function'=>'view');
		$this->AuthMapping['view'] = array('function' => 'allUser');
		$this->RequestMapping['new'] = array('function'=>'newApplication');
		$this->AuthMapping['new'] = array('function' => 'allUser');
		$this->RequestMapping['case.saveDraft'] = array('function'=>'saveDraft');
		$this->AuthMapping['case.saveDraft'] = array('function' => 'allUser');
		$this->RequestMapping['case.edit'] = array('function'=>'caseEdit');
		$this->AuthMapping['case.edit'] = array('function' => 'caseOwner','authValue' => 'POST_caseID');
		#E114398
		$this->RequestMapping['case.edit2'] = array('function'=>'caseEdit2');
		$this->AuthMapping['case.edit2'] = array('function' => 'isAdmin');
		
		$this->RequestMapping['case.delete'] = array('function'=>'caseDelete');
		$this->AuthMapping['case.delete'] = array('function' => 'caseOwner','authValue' => 'POST_caseID');
		$this->RequestMapping['case.undoDelete'] = array('function'=>'caseUndoDelete');
		$this->AuthMapping['case.undoDelete'] = array('function' => 'caseOwner','authValue' => 'POST_caseID');
		$this->RequestMapping['case.perminantDelete'] = array('function'=>'casePerminantDelete');
		$this->AuthMapping['case.perminantDelete'] = array('function' => 'isAdmin');
		$this->RequestMapping['case.insertConfrim'] = array('function'=>'caseInsertConfrim');
		$this->AuthMapping['case.insertConfrim'] = array('function' => 'allUser');
		$this->RequestMapping['case.insertConfrimSkipBudget'] = array('function'=>'insertConfrimSkipBudget');
		$this->AuthMapping['case.insertConfrimSkipBudget'] = array('function' => 'isAdmin');
		
		$this->RequestMapping['case.showConfirm'] = array('function'=>'showConfirm');
		$this->AuthMapping['case.showConfirm'] = array('function' => 'checkCaseAccessRight', 'authValue' => 'PathVars_0');
		
// 		$this->RequestMapping['case.insertNew'] = array('function'=>'caseInsertNew');
		$this->RequestMapping['case.update'] = array('function'=>'caseUpdate');
		$this->AuthMapping['case.update'] = array('function' => 'checkCaseAccessRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['codeAndName.update'] = array('function'=>'codeAndNameUpdate');
		$this->AuthMapping['codeAndName.update'] = array('function' => 'caseOwner', 'authValue' => 'POST_caseID');
		
		$this->RequestMapping['case.sendNotification'] = array('function'=>'caseSendNotification');
		$this->AuthMapping['case.sendNotification'] = array('function' => 'allUser');
		$this->RequestMapping['caseDetials'] = array('function'=>'caseDetials');
		$this->AuthMapping['caseDetials'] = array('function' => 'checkCaseAccessRight', 'authValue' => 'PathVars_0');//checkCaseAccessRight($_PAGE['POST']['CaseID'])
		$this->RequestMapping['caseApproval.update'] = array('function'=>'caseApprovalUpdate');
		$this->AuthMapping['caseApproval.update'] = array('function' => 'checkCaseApprovalRight', 'authValue' => array('POST_caseID','POST_approverUserID'));
		$this->RequestMapping['custCaseApproval.update'] = array('function'=>'custCaseApprovalUpdate');
		$this->AuthMapping['custCaseApproval.update'] = array('function' => 'checkCustCaseApprovalRight', 'authValue' => array('POST_caseID','POST_approverUserID'));
		$this->RequestMapping['supplier.add'] = array('function'=>'supplierAdd');
		$this->AuthMapping['supplier.add'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['quoteReplyStatus.update'] = array('function'=>'updateQuoteReplyStatus');
		$this->AuthMapping['quoteReplyStatus.update'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
        $this->RequestMapping['quoteInputReplyDate.update'] = array('function'=>'updateQuoteInputReplyDate');
        $this->AuthMapping['quoteInputReplyDate.update'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');

		$this->RequestMapping['quotationEndDate.update'] = array('function'=>'updateQuotationEndDate');
		$this->AuthMapping['quotationEndDate.update'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderOpeningStartDate.update'] = array('function'=>'updateTenderOpeningStartDate');
		$this->AuthMapping['tenderOpeningStartDate.update'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['invitationDocument.update'] = array('function'=>'updateInvitationDocument');
		$this->AuthMapping['invitationDocument.update'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderOpeningEndDate.update'] = array('function'=>'updateTenderOpeningEndDate');
		$this->AuthMapping['tenderOpeningEndDate.update'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderApprovalStartDate.update'] = array('function'=>'updateTenderApprovalStartDate');
		$this->AuthMapping['tenderApprovalStartDate.update'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderApprovalEndDate.update'] = array('function'=>'updateTenderApprovalEndDate');
		$this->AuthMapping['tenderApprovalEndDate.update'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		
		$this->RequestMapping['finishStage2'] = array('function'=>'finishStage2');
		$this->AuthMapping['finishStage2'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['finishStage3'] = array('function'=>'finishStage3');
		$this->AuthMapping['finishStage3'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderOpeningDates.set'] = array('function'=>'setTenderOpeningDates');
		$this->AuthMapping['tenderOpeningDates.set'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderApprovalDates.set'] = array('function'=>'setTenderApprovalDates');
		$this->AuthMapping['tenderApprovalDates.set'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['quotationDetails.update'] = array('function'=>'quotationDetailsUpdate');
		$this->AuthMapping['quotationDetails.update'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['result.set'] = array('function'=>'setResult');
		$this->AuthMapping['result.set'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		
		$this->RequestMapping['priceComparison.set'] = array('function'=>'setPriceComparison');
		$this->AuthMapping['priceComparison.set'] = array('function' => 'allUser');
		
		
		$this->RequestMapping['contactMethod.update'] = array('function'=>'contactMethodUpdate');
		$this->AuthMapping['contactMethod.update'] =  array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['notificationScheduleDate.save'] = array('function'=>'saveNotiScheduleDate');
		$this->AuthMapping['notificationScheduleDate.save'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['quotation.delete'] = array('function'=>'deleteQuotation');
		$this->AuthMapping['quotation.delete'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		
		
		## TB Form
		$this->RequestMapping['caseApproval.form'] = array('function'=>'getCaseApprovalForm');
		$this->AuthMapping['caseApproval.form'] = array('function' => 'checkCaseApprovalRight', 'authValue' => array('POST_caseID','POST_approverUserID'));
		$this->RequestMapping['custCaseApproval.form'] = array('function'=>'getCustCaseApprovalForm');
		$this->AuthMapping['custCaseApproval.form'] = array('function' => 'checkCustCaseApprovalRight', 'authValue' => array('POST_caseID','POST_approverUserID'));
		$this->RequestMapping['addSupplier.form'] = array('function'=>'getAddSupplierForm');
		$this->AuthMapping['addSupplier.form'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['previewEmail.form'] = array('function'=>'getPreviewEmailForm');
		$this->AuthMapping['previewEmail.form'] = array('function' => 'checkSendQuotationInvitationRight','authValue' => 'POST_caseID');
		$this->RequestMapping['uploadInvitationDoc.form'] = array('function'=>'getUploadInviationDocForm');
		$this->AuthMapping['uploadInvitationDoc.form'] = array('function' => 'checkSendQuotationInvitationRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['uploadFinalDoc.form'] = array('function'=>'getUploadFinalDocForm');
		$this->AuthMapping['uploadFinalDoc.form'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderOpening.form'] = array('function'=>'getTenderOpeningForm');
		$this->AuthMapping['tenderOpening.form'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['priceCompare.form'] = array('function'=>'getPriceComparisonForm');
		$this->AuthMapping['priceCompare.form'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['tenderApproval.form'] = array('function'=>'getTenderApprovalForm');
		$this->AuthMapping['tenderApproval.form'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['caseResult.form'] = array('function'=>'getCaseResultForm');
		$this->AuthMapping['caseResult.form'] = array('function' => 'checkInputResultRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['supplierInfo'] = array('function'=>'getSupplierInfoHtml');
		$this->AuthMapping['supplierInfo'] = array('function' => 'allUser');
		$this->RequestMapping['quotationDetailsUpdate.form'] = array('function'=>'getQuotationDetailsUpdateForm');
		$this->AuthMapping['quotationDetailsUpdate.form'] = array('function' => 'checkInputQuotationResponseRight', 'authValue' => 'POST_caseID');
		$this->RequestMapping['termination.form'] = array('function'=>'getTerminationForm');
		$this->AuthMapping['termination.form'] = array('function' => 'caseOwner','authValue' => 'POST_caseID');
		
		
		
		$this->RequestMapping['declaration.save'] = array('function'=>'saveDeclaration');
 		$this->AuthMapping['declaration.save'] = array('function' => 'allUser');
		$this->RequestMapping['sendEmailInvitation'] = array('function'=>'sendEmailInvitation');
 		$this->AuthMapping['sendEmailInvitation'] = array('function' => 'allUser');
		
 		## Document Download
 		$this->RequestMapping['document.summary'] = array('function'=>'downloadDocumentSummary');
 		$this->AuthMapping['document.summary'] = array('function' => 'allUser');
 		
 		## Widget
 		$this->RequestMapping['case.chooseIndividual'] = array('function'=>'chooseIndividual');
 		$this->AuthMapping['case.chooseIndividual'] = array('function'=>'isAdmin');
 		
		### Finished View
		$this->RequestMapping['finished'] = array('function'=>'finishedView');
		$this->AuthMapping['finished'] = array('function' => 'allUser');
		
		$this->RequestMapping['ApproveFinish'] = array('function'=>'finishApprove');
		$this->AuthMapping['ApproveFinish'] = array('function' => 'allUser');
		
		$this->RequestMapping['RejectFinish'] = array('function'=>'finishReject');
		$this->AuthMapping['RejectFinish'] = array('function' => 'allUser');

		### OtherApplication
		$this->RequestMapping['newOther'] = array('function'=>'newOtherApplication'); 
		$this->AuthMapping['newOther'] = array('function' => 'allUser');
		$this->RequestMapping['case.insertOtherConfirm'] = array('function'=>'caseInsertOtherConfirm');
		$this->AuthMapping['case.insertOtherConfirm'] = array('function' => 'allUser');
		//delete reocord
		$this->RequestMapping['deleteOther'] = array('function'=>'otherApplicationDelete');
		$this->AuthMapping['deleteOther'] = array('function' => 'allUser');
		//display
		$this->RequestMapping['otherfinished'] = array('function'=>'finishedOtherView');
		$this->AuthMapping['otherfinished'] = array('function' => 'allUser');
		
		$this->RequestMapping['ApproveOther'] = array('function'=>'otherApprove');
		$this->AuthMapping['ApproveOther'] = array('function' => 'allUser');
		
		$this->RequestMapping['RejectOther'] = array('function'=>'RejectOther');
		$this->AuthMapping['RejectOther'] = array('function' => 'allUser');
		
		### Add settings in ajax
		$this->RequestMapping['supplier.addByAjax'] = array('function'=>'addSupplierByAjax');
		$this->AuthMapping['supplier.addByAjax'] = array('function' => 'allUser');
		
		$this->RequestMapping['reloadSupplierType'] = array('function'=>'reloadSupplierType');
		$this->AuthMapping['reloadSupplierType'] = array('function' => 'allUser');
		
		### Case.FundingSource
		$this->RequestMapping['case.updateFundingSource'] = array('function'=>'updateFundingSource');
		$this->AuthMapping['case.updateFundingSource'] = array('function' => 'allUser');
		
		### CaseDetail.printApplication
		$this->RequestMapping['caseDetail.printApplicationForm'] = array('function'=>'printApplicationForm');
		$this->AuthMapping['caseDetail.printApplicationForm'] = array('function' => 'checkCaseAccessRight', 'authValue' => 'PathVars_0');
	}
	
	public function index(){
		$this->sendRedirect("index.php?p=mgmt.view");
	}
	
	public function view($params=''){
		
		global $_PAGE;
		
		switch ($params){
			case 'AddSuccess' :
			case 'UpdateSuccess' :
			case 'AddUnsuccess' :
			case 'UpdateUnsuccess' :
			case 'DeleteSuccess' :
			case 'DeleteUnsuccess' :
				$_PAGE['returnMsgKey'] =  $params;
			break;
		}
		
		
		
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		$keyword = $_PAGE['POST']['keyword'];
		$currentStage = $_PAGE['POST']['currentStage'];
		$currentType = $_PAGE['POST']['currentType'];
        $currentStatus = $_PAGE['POST']['currentStatus'];
		$AcademicYearfilterValue = $_PAGE['POST']['AcademicYearfilterValue']?$_PAGE['POST']['AcademicYearfilterValue']:$_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
		if(!isset($currentStage))$currentStage='';
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$caseInfoAry = $db->getViewableCaseInfo($keyword,$currentStage,$currentType,$AcademicYearfilterValue,$currentStatus);
		$_PAGE['views_data']['caseInfoAry'] = $caseInfoAry;
		$_PAGE['views_data']['keyword'] = $keyword;
		$_PAGE['views_data']['currentStage'] = $currentStage;
		$_PAGE['views_data']['currentType'] = $currentType;
        $_PAGE['views_data']['currentStatus'] = $currentStatus;
		
// 		$db->db_show_debug_log_by_query_number(1);
		$main = $this->getViewContent('/management/view.php');
		$view = $this->getLayout($main);
		
		echo $view;
	}
	
	public function newApplication($skipBudget=false){
		
		global $_PAGE,$PATH_WRT_ROOT,$UserID;

		
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$caseID = $_PAGE['POST']['caseID'];
		if($caseID>0){
			$caseInfoAry = $db->getCaseInfo($caseID);
			$caseInfo = $caseInfoAry[0];
			$_PAGE['views_data']['caseInfo'] = $caseInfo;
			
			//Get Item select array by GroupID
			$itemArray = $db->getFinancialItemByGroup($caseInfo['ApplicantGroupID']);
			foreach($itemArray as $_item){
				$itemSelectArray[] = array($_item['ItemID'],$_item['ItemName']); 
			}
			$_PAGE['views_data']['itemSelectArray'] = $itemSelectArray;
		}
		
		$catInfo = $db->getCategoryInfo(array());
		$catParData = array();
		foreach($catInfo as $num => $catAry){
			$catParData[$num][0] = $catAry['CategoryID'];
			$catParData[$num][1] = $catAry['CategoryName'];
			$catParData[$num][2] = $catAry['CategoryNameChi'];
		}
		
		$groupInfo = $db->getUsersGroup();
		
		$groupParData = array();
		foreach($groupInfo as $num=>$groupAry){
			$groupParData[$num][0] = $groupAry['GroupID'];
			$groupParData[$num][1] = $groupAry[Get_Lang_Selection('GroupNameChi','GroupName')];
		}
		
		//Check Rule is Valid
		$settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$isRuleValid = $settingObj->isRuleValid();

		//get general settings -> exceed_budget_percentage
		$settings = $db->getGeneralSettings();
		$exceed_budget_percentage = $settings['exceed_budget_percentage'];
		
		//FundingSource
		$FundingSourceArr = $db->getFundingCaseArr();
		$_PAGE['views_data']['FundingSourceArr'] = $FundingSourceArr;
		
		$_PAGE['views_data']['catParData'] = $catParData;
		$_PAGE['views_data']['groupParData'] = $groupParData;
		$_PAGE['views_data']['currentStage'] = $_PAGE['POST']['currentStage'];
		$_PAGE['views_data']['isRuleValid'] = $isRuleValid;
		$_PAGE['views_data']['GeneralSetting']['exceed_budget_percentage'] = $exceed_budget_percentage;
		
		$_PAGE['views_data']['skipBudget'] = $skipBudget;
		
		$main = $this->getViewContent('/management/new.php');
		$view = $this->getLayout($main);
		
		echo $view;
	}
	
	public function newOtherApplication(){  
	
		global $_PAGE,$PATH_WRT_ROOT,$UserID;
	
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'OthersView';
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		$_PAGE['views_data']['other'] = '1';
		//edit mode -> get a not null CaseID
		$caseID = $_PAGE['POST']['targetIdAry'][0];
		
		//if the caseID not null(edit mode) -> get the caseInfo and display on the view
		if(isset($caseID) ){
			$result = $db->getCaseInfoForOtherApplication($caseID);
			$_PAGE['views_data']['EditRecord']['caseID'] = $caseID;
			$_PAGE['views_data']['EditRecord']['CaseName'] = $result[0]['CaseName'];
			$_PAGE['views_data']['EditRecord']['CategoryID'] = $result[0]['CategoryID'];
			$_PAGE['views_data']['EditRecord']['Description'] = $result[0]['Description'];
			$_PAGE['views_data']['EditRecord']['ApplicantGroupID'] = $result[0]['ApplicantGroupID'];
			$_PAGE['views_data']['EditRecord']['ItemID'] = $result[0]['ItemID'];
			$_PAGE['views_data']['EditRecord']['Budget'] = $result[0]['Budget'];
			$_PAGE['views_data']['EditRecord']['ApplicantType'] = $result[0]['ApplicantType'];
			$_PAGE['views_data']['EditRecord']['ApplicantUserID'] = $result[0]['ApplicantUserID'];
			$_PAGE['views_data']['EditRecord']['DateInput'] = $result[0]['DateInput'];
		}
		$catInfo = $db->getCategoryInfo(array());
		$catParData = array();
		foreach($catInfo as $num => $catAry){
			$catParData[$num][0] = $catAry['CategoryID'];
			$catParData[$num][1] = $catAry['CategoryName'];
			$catParData[$num][2] = $catAry['CategoryNameChi'];
		}
	
		$groupInfo = $db->getUsersGroup();
	
		$groupParData = array();
		foreach($groupInfo as $num=>$groupAry){
			$groupParData[$num][0] = $groupAry['GroupID'];
			$groupParData[$num][1] = $groupAry[Get_Lang_Selection('GroupNameChi','GroupName')];
		}
	
		//Check Rule is Valid
		$settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$isRuleValid = $settingObj->isRuleValidForOtherApplication();
	
		//get general settings -> exceed_budget_percentage
		$settings = $db->getGeneralSettings();
		$exceed_budget_percentage = $settings['exceed_budget_percentage'];
	
		$_PAGE['views_data']['catParData'] = $catParData;
		$_PAGE['views_data']['groupParData'] = $groupParData;
		$_PAGE['views_data']['currentStage'] = $_PAGE['POST']['currentStage'];
		$_PAGE['views_data']['isRuleValid'] = $isRuleValid;
		$_PAGE['views_data']['GeneralSetting']['exceed_budget_percentage'] = $exceed_budget_percentage;
	
		$main = $this->getViewContent('/management/other_new.php');
		$view = $this->getLayout($main);
	
		echo $view;
	}
	
	public function saveDraft(){
		global $_PAGE,$UserID;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$caseID = $_PAGE['POST']['caseID'];
		$caseName = $_PAGE['POST']['caseName'];
		$categoryID = $_PAGE['POST']['categoryID'];
		$description = $_PAGE['POST']['description'];
		$groupID = $_PAGE['POST']['groupID'];
		$itemID = $_PAGE['POST']['itemID'];
		$budget = $_PAGE['POST']['budget'];
		$applicantType = $_PAGE['POST']['applicantType'];
		$remark = $_PAGE['POST']['remark'];
		$FileIDArr = $_PAGE['POST']['FileID'];
		$WillDelFileArr = $_PAGE['POST']['WillDeletedList'];
		
		//$chooseIndividual = $_PAGE['POST']['chooseIndividual'];
		
		//Get current academic year
		$gsObj = $db->getGeneralSettings();
		$academicYearID = $gsObj['defaultAcademicYearID'];
		
		if($budget<=0){	//Failed
			$this->sendRedirect("index.php?p=mgmt.view.AddUnsuccess");
		}
		
		//Check rule
		$returnedTemplateRule = $db->getSuitableRuleTemplate($budget);
		//duplicate rule
		$newRuleID = $db->duplicateTemplateRule($returnedTemplateRule['RuleID']);

		$multiRowValueArr = array(
			array(
				$caseName,
				$categoryID,
				$description,
				$itemID,
				$academicYearID,
				$budget,
				$applicantType,
				$UserID,
				$groupID,
				0,
				$newRuleID
			)
		);
		
		if($caseID>0){
			//Update Case
			$result = $db->updateCase($multiRowValueArr,$caseID);
		}else{
			$result = $db->insertNewCase($multiRowValueArr);
			$caseID = ($result)? $db->db_insert_id():'';
		}

        //duplicate rule files
        $duplicatedRuleFiles = $db->duplicateRuleFile($returnedTemplateRule['RuleID'], $newRuleID, $caseID);

		if(!empty($FileIDArr)){
		    $stageID = 0;
		    $fileResult = $db->insertFileToCase($FileIDArr, $caseID, $stageID);
		}
		if(!empty($WillDelFileArr)){
		    $file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
		    foreach((array)$WillDelFileArr as $fileID){
    		    $file_db->removeAttachmentStep2($fileID);
		    }
		}
		
		$this->sendRedirect("index.php?p=mgmt.view");
	}
	public function caseEdit(){
		global $_PAGE;
		$this->newApplication();
	}
	public function caseEdit2(){
		global $_PAGE;
		$this->newApplication(true);
	}
	public function caseDelete(){
		global $_PAGE;
		$caseID = $_PAGE['POST']['caseID'];
		$reason = $_PAGE['POST']['delReason'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $mgmtObj->deleteCase($caseID,$reason);
		$mgmtObj->updateFundingCaseRelationByCase($caseID,'2'); // Chnage All the record with this CaseID to record status 2 => rejected
		if($success){
			echo 'Saved';
		}else{
			echo 'Failed';
		}
	}
	public function caseUndoDelete(){
		global $_PAGE;
		$caseID = $_PAGE['POST']['caseID'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $mgmtObj->undoDeleteCase($caseID);
		if($success){
			echo 'Saved';
		}else{
			echo 'Failed';
		}
	}
	
	public function casePerminantDelete(){
		global $_PAGE;
		$caseIDAry = $_PAGE['POST']['targetIdAry'];
		//TODO
	}
	public function caseInsertConfrim(){
		
		global $_PAGE, $sys_custom, $UserID;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		$caseID = $_PAGE['POST']['caseID'];
		//$code = 'RD'.rand(10,100);
		$caseName = $_PAGE['POST']['caseName'];
		$categoryID = $_PAGE['POST']['categoryID'];
		$description = $_PAGE['POST']['description'];
		$groupID = $_PAGE['POST']['groupID'];
		$itemID = $_PAGE['POST']['itemID'];
		$budget = $_PAGE['POST']['budget'];
		$applicantType = $_PAGE['POST']['applicantType'];
		$remark = $_PAGE['POST']['remark'];
		//$FileSizeArr = $_PAGE['POST']['FileSize'];
		$FileIDArr = $_PAGE['POST']['FileID'];
		$WillDelFileArr = $_PAGE['POST']['WillDeletedList'];
		//For admin only: Overriding applicantUser
 		$chooseIndividual = $_PAGE['POST']['chooseIndividual'];
 		//Funding Source Related
 		$FundingID = $_PAGE['POST']['Funding_Selection_Box'];
 		$FundingBudgetUse  = $_PAGE['POST']['Funding_Selection_Box_usingBudget'];
 		$RecordStatus = '0';
		/**
		 * Check if case are rejected by someone, if yes => start a new case
		 * 
		 */
 		
		$numOfRejected = $db->countOfRejectedApproval($caseID);
		if($numOfRejected>0){
			//$need to Startd
			$oldCaseID = $caseID;	//store case id as old case id => insert to history table in db later
			unset($caseID);	//unset case id to create new case
		}else{
			// do nth
		}

		//Get current academic year
		$gsObj = $db->getGeneralSettings();
		$academicYearID = $gsObj['defaultAcademicYearID'];
        $budget = preg_replace("/[^0-9\.]/", '', $budget); #extract number from money format
		// if proposed procurement budget <= 0
		if($budget<=0){	//Failed
			$this->sendRedirect("index.php?p=mgmt.view.AddUnsuccess");
		}

		//Check rule
		// normal find rule by budget
		$returnedTemplateRule = $db->getSuitableRuleTemplate($budget);
		
		// cust force tender
		if($sys_custom['ePCM']['enableForcingToTenderOption'] && $_PAGE['POST']['forceTender']){
			$rules = $db->getRuleTemplateInfo('','T');
			$returnedTemplateRule = $rules[Get_Array_Last_Key($rules)];
		}

		//duplicate rule
        $newRuleID = $db->duplicateTemplateRule($returnedTemplateRule['RuleID']);
		$RuleCode = $db->getRuleCode($newRuleID);
		
		if($oldCaseID>0){
			$code = $db->getCaseCode($oldCaseID);
			$multiRowValueArr = array(
					array(
							$code,
							$caseName,
							$categoryID,
							$description,
							$itemID,
							$academicYearID,
							$budget,
							$applicantType,
							$chooseIndividual,
							$groupID,
							0,
							$newRuleID
					)
			);
		}else{
			$multiRowValueArr = array(
					array(
							$caseName,
							$categoryID,
							$description,
							$itemID,
							$academicYearID,
							$budget,
							$applicantType,
							$chooseIndividual,
							$groupID,
							0,
							$newRuleID
					)
			);
		}

		//GET YEAR
        $AcademicYearID = $_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
        $sequenceBaseItemsAry=array('CategoryCode'=>$categoryID,'FinancialItemCode'=>$itemID,'RuleCode'=>$RuleCode,'GroupCode'=>$groupID,'AcademicYear'=>$AcademicYearID);
		if($caseID>0){
			//Update Case
			$result = $db->updateCase($multiRowValueArr,$caseID);
			$code = $db->getNextCaseNumber(array('CODE::Category'=>$db->getCategoryCode($categoryID),'CODE::GroupItem'=>$db->getFinancialItemCode($itemID),'CODE::Rule'=>$db->getRuleCode($newRuleID),'CODE::GroupCode'=>$db->getGroupCode($groupID)),$sequenceBaseItemsAry);
			$db->updateCaseCode($caseID,$code);
		}else{
			if($oldCaseID>0){
				$result = $db->insertNewCaseWithCode($multiRowValueArr);
				$caseID = ($result)? $db->db_insert_id():'';
			}else{
				$result = $db->insertNewCase($multiRowValueArr);
				$caseID = ($result)? $db->db_insert_id():'';
				$code = $db->getNextCaseNumber(array('CODE::Category'=>$db->getCategoryCode($categoryID),'CODE::GroupItem'=>$db->getFinancialItemCode($itemID),'CODE::Rule'=>$db->getRuleCode($newRuleID),'CODE::GroupCode'=>$db->getGroupCode($groupID)),$sequenceBaseItemsAry);
				$db->updateCaseCode($caseID,$code);
			}
		}
		//Funding Source 
		$this->updateFundingSource($caseID,$FundingID,$FundingBudgetUse,$RecordStatus);
		
		if(!empty($FileIDArr)){
		  $stageID = 0;
		  $fileResult = $db->insertFileToCase($FileIDArr, $caseID, $stageID);
		}
		if(!empty($WillDelFileArr)){
		    $file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
		    foreach((array)$WillDelFileArr as $fileID){
		        $file_db->removeAttachmentStep2($fileID);
		    }
		}

        //duplicate rule files
        $duplicatedRuleCaseFiles = $db->duplicateRuleFile($returnedTemplateRule['RuleID'], $newRuleID, $caseID);

		/**
		 * Mapping of oldCaseID to history table 
		 */
		if($oldCaseID==''){
			$oldCaseID = $caseID;
		}
		$db->writeCaseHistory($oldCaseID, $caseID);
		
		
		$_PAGE['POST']['caseID'] = $caseID;
		$_PAGE['POST']['action'] = 'sendCaseApproverInvitation';
		$_PAGE['POST']['data'] = array();
		
		$this->caseUpdate();
		$this->sendRedirect("index.php?p=mgmt.caseDetials.$caseID");

// 		$caseInfoAry =  $db->getCaseInfo($caseID);
// 		$caseInfoAry['CaseAttachment'] = $db->getCaseStageAttachment($caseID);
// 		$_PAGE['views_data']['caseInfoAry'] = $caseInfoAry;
		
// 		$main = $this->getViewContent('/management/insertConfrim.php');
// 		$view = $this->getLayout($main,  '', '/template/form.php');
// 		echo $view;
	}
	
	public function insertConfrimSkipBudget(){
		global $_PAGE,$UserID;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		$caseID = $_PAGE['POST']['caseID'];
		$caseName = $_PAGE['POST']['caseName'];
		$categoryID = $_PAGE['POST']['categoryID'];
		$description = $_PAGE['POST']['description'];
		$groupID = $_PAGE['POST']['groupID'];
		$itemID = $_PAGE['POST']['itemID'];
		$budget = $_PAGE['POST']['budget'];
		$applicantType = $_PAGE['POST']['applicantType'];
		$remark = $_PAGE['POST']['remark'];
		//$FileSizeArr = $_PAGE['POST']['FileSize'];
		$FileIDArr = $_PAGE['POST']['FileID'];
		$WillDelFileArr = $_PAGE['POST']['WillDeletedList'];
		//For admin only: Overriding applicantUser
		$chooseIndividual = $_PAGE['POST']['chooseIndividual'];
		//Funding Source Related
		$FundingID = $_PAGE['POST']['Funding_Selection_Box'];
		$FundingBudgetUse  = $_PAGE['POST']['Funding_Selection_Box_usingBudget'];
		$RecordStatus = '0';
		
		//Get current academic year
		$gsObj = $db->getGeneralSettings();
		$academicYearID = $gsObj['defaultAcademicYearID'];
		$multiRowValueArr = array(
					array(
							$caseName,
							$categoryID,
							$description,
							$itemID,
							$academicYearID,
							$applicantType,
							$chooseIndividual,
							$groupID
					)
			);

			//Update Case
		$result = $db->updateCaseSkipBudget($multiRowValueArr,$caseID);
		//Funding Source
		$this->updateFundingSource($caseID,$FundingID,$FundingBudgetUse,$RecordStatus);
		
		if(!empty($FileIDArr)){
			$stageID = 0;
			$fileResult = $db->insertFileToCase($FileIDArr, $caseID, $stageID);
		}
		if(!empty($WillDelFileArr)){
			$file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
			foreach((array)$WillDelFileArr as $fileID){
				$file_db->removeAttachmentStep2($fileID);
			}
		}
		$this->sendRedirect("index.php?p=mgmt.caseDetials.$caseID");
	
	}
	
	public function caseInsertOtherConfirm(){ 
	
		global $_PAGE,$UserID;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$caseID = $_PAGE['POST']['caseID'];
		$caseName = $db->Get_Safe_Sql_Query($_PAGE['POST']['caseName']);
		$categoryID = $_PAGE['POST']['categoryID'];
		$description = $db->Get_Safe_Sql_Query($_PAGE['POST']['description']);
		$groupID = $_PAGE['POST']['groupID'];
		$itemID = $_PAGE['POST']['itemID'];
		$budget = $_PAGE['POST']['budget'];
		$applicantType = $_PAGE['POST']['applicantType'];
// 		$remark = $db->Get_Safe_Sql_Query($_PAGE['POST']['remark']);
		//For admin only: Overriding applicantUser
		$chooseIndividual = $_PAGE['POST']['chooseIndividual'];
		//2016-10-04	Villa
		$RecordTime = $_PAGE['POST']['RecordTime'];
		$db_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
		$setting = $db_setting->getGeneralSettings();
	

		/**
		 * Check if case are rejected by someone, if yes => start a new case
		 *
		 */
	
		//Get current academic year
		$gsObj = $db->getGeneralSettings();
		$academicYearID = $gsObj['defaultAcademicYearID'];
		// if proposed procurement budget <= 0
		if($budget<=0){	//Failed
			$this->sendRedirect("index.php?p=mgmt.view.AddUnsuccess");
		}
		if( $caseID!='' ){
			
			$multiRowValueArr = array(
					array(
							$caseName,
							$categoryID,
							$description,
							$itemID,
							$academicYearID,
							$budget,
							$applicantType,
							$chooseIndividual,
							$groupID,
							0,
							$RecordTime
					)
			);
			
			$result = $db->updateCaseForOtherApp($multiRowValueArr,$caseID);
			if($setting['MiscExpenditureApprove']){
				//Do nothing~
			}else{
				$db->updateResultFromCaseID($caseID,'',$budget,$description,'','','','','');
			}
		
		}else{
			
			$newRuleID = $db->getLatestRuleIDForOtherApplication();
			$newRuleID = $newRuleID[0]['RuleID'];
	
			$multiRowValueArr = array(
					array(
								$caseName,
								$categoryID,
								$description,
								$itemID,
								$academicYearID,
								$budget,
								$applicantType,
								$chooseIndividual,
								$groupID,
								0,
								$newRuleID,
								$RecordTime
							
					)
				);	
		
			$result = $db->insertNewCaseWithTime($multiRowValueArr);
			$caseID = ($result)? $db->db_insert_id():'';
			if($setting['MiscExpenditureApprove']){
				//Do nothing~
			}else{
				$db->insertResult($caseID,'',$budget,$description,'','','','','');
			}
				
		}
	
	
		$_PAGE['POST']['caseID'] = $caseID;
		$_PAGE['POST']['action'] = 'other';
		$_PAGE['POST']['data'] = array();
	
		$this->caseUpdate();
		$this->sendRedirect("index.php?p=mgmt.otherfinished"); 
	
	}
	
	public function otherApplicationDelete(){
		global $_PAGE;
		
		$PostVar = $_PAGE['POST'];
		$caseIdAry = $PostVar['targetIdAry'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		for($i=0;$i<count($caseIdAry);$i++){
			$db->deleteResult($caseIdAry[$i]);
			$db->deleteCase($caseIdAry[$i],'');
		
		}
		
		$this->sendRedirect("index.php?p=mgmt.otherfinished");
		
	}
	
	public function finishApprove(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
		$setting = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		
		$isAdmin = false;
		if($auth->isAdmin()){
			$isAdmin = true;
		}
		
		$principalUserIDAry = $setting->getPrincipalUserIDAry();
		$isPrincipal = false;
		foreach($principalUserIDAry as $principalUserID){
			if($principalUserID['UserID'] == $_SESSION['UserID']){
				$isPrincipal = true;
				break;
			}
		}
		
		$PostVar = $_PAGE['POST'];
		$caseIdAry = $PostVar['targetIdAry'];
		if(!empty($caseIdAry)){
			foreach($caseIdAry as $value){
				$caseID = $value;
				$result = $db->getCaseInfoForFinishApplication($value);
//				$GroupID = $db->getGroupHeadByUserID($_SESSION['UserID']);
//				$check = false;
//				
//				foreach($GroupID as $GroupIDVal){
//					if($result[0]['ApplicantGroupID']== $GroupIDVal['GroupID']){
//						$check = true;
//					}
//				}
				
				if($isPrincipal||$isAdmin){
					if(!empty($result)){
						$Description = $result[0]['Description'];
						$Budget = $result[0]['Budget'];
						$CurrentStage = $result[0]['IsPrincipalApproved'];
						if($CurrentStage==1){
							// do nothing
						}else{
							$db->updateCasePrincipalApprove($caseID,'1');
						}
					}
				}
			}
		}
		$this->sendRedirect("index.php?p=mgmt.finished.completed");
	}
	
	public function finishReject(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
		$setting = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		
		$isAdmin = false;
		if($auth->isAdmin()){
			$isAdmin = true;
		}
		
		$principalUserIDAry = $setting->getPrincipalUserIDAry();
		$isPrincipal = false;
		foreach($principalUserIDAry as $principalUserID){
			if($principalUserID['UserID'] == $_SESSION['UserID']){
				$isPrincipal = true;
				break;
			}
		}
		
		$PostVar = $_PAGE['POST'];
		$caseIdAry = $PostVar['targetIdAry'];
		if(!empty($caseIdAry)){
			foreach($caseIdAry as $value){
				$caseID = $value;
				$result = $db->getCaseInfoForFinishApplication($value);
//				$GroupID = $db->getGroupHeadByUserID($_SESSION['UserID']);
//				$check = false;
//				
//				foreach($GroupID as $GroupIDVal){
//					if($result[0]['ApplicantGroupID']== $GroupIDVal['GroupID']){
//						$check = true;
//					}
//				}
				
				if($isPrincipal||$isAdmin){
					if(!empty($result)){
						$Description = $result[0]['Description'];
						$Budget = $result[0]['Budget'];
						$CurrentStage = $result[0]['IsPrincipalApproved'];
						if($CurrentStage==0){
							// do nothing
						}else{
							$db->updateCasePrincipalApprove($caseID,'0');
						}
					}
				}
			}
		}
		$this->sendRedirect("index.php?p=mgmt.finished.completed");
	}
	
	public function otherApprove(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
		$isAdmin = false;
		if($auth->isAdmin()){
			$isAdmin = true;
		}
		
		
		$PostVar = $_PAGE['POST'];
		$caseIdAry = $PostVar['targetIdAry'];
		if(!empty($caseIdAry)){
			foreach($caseIdAry as $value){
				$caseID = $value;
				$result = $db->getCaseInfoForOtherApplication($value);
				$GroupID = $db->getGroupHeadByUserID($_SESSION['UserID']);
				$check = false;
				
				foreach($GroupID as $GroupIDVal){
					if($result[0]['ApplicantGroupID']== $GroupIDVal['GroupID']){
						$check = true;
					}
				}
				
				if($check||$isAdmin){
					if(!empty($result)){
						$Description = $result[0]['Description'];
						$Budget = $result[0]['Budget'];
						$CurrentStage = $result[0]['CurrentStage'];
						if($CurrentStage==10){
							// do nothing
						}else{
							$db->deleteResult($caseID);
							$db->insertResult($caseID,'',$Budget,$Description,'','','','','');
							$updateStageID ='10';
							$db->updateCaseCurrentStage($caseID,$updateStageID);
							$db->updateCaseApprovalDate($caseID);
						}
					}
				}
			}
		}
		$this->sendRedirect("index.php?p=mgmt.otherfinished");
	}
	
	public function RejectOther(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$PostVar = $_PAGE['POST'];
		$caseIdAry = $PostVar['targetIdAry'];
		foreach($caseIdAry as $value){
			$caseID = $value;
			$updateStageID ='1';
			$db->deleteResult($caseID);
			$db->updateCaseCurrentStage($caseID,$updateStageID);
		}
		$this->sendRedirect("index.php?p=mgmt.otherfinished");
	}
	
	public function caseUpdate(){	//2016-09-29	Villa
		global $_PAGE,$plugin,$sys_custom;
		
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		
		$caseID = $_PAGE['POST']['caseID'];
		$action = $_PAGE['POST']['action'];
		$data = $_PAGE['POST']['data'];
// 		debug_pr($_PAGE);
		########################### Attention ######################
		#
		#	All other data should put into $_PAGE['POST']['data']
		#	- hidden field=> name = data['xyz']
		#
		###########################################################
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		$needUpdateStage = false;
		switch ($action){
			case 'sendCaseApproverInvitation':
				$db_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
				$setting = $db_setting->getGeneralSettings();
				// Omas 20160825
// 				$_PAGE['allow_send_app']=false;
// 				if(isset($plugin['eClassTeacherApp']) && $plugin['eClassTeacherApp']===true && $setting['eClassAppNotification']=='1'){
// 					$_PAGE['allow_send_app']=true;
// 				}
// 				$view = $db->sendCaseApproverInvitation($caseID);
// 				$main = $this->getViewContent('/management/insert_notify_approval_user.php');
// 				$view = $this->getLayout($main);

				$needUpdateStage = true;
				if($db->getCaseQuotationType($caseID)=='N'){
					$updateStageID = 4;
				}else{
					if($sys_custom['ePCM_skipCaseApproval']){
						$updateStageID = 2; //Skip Case Approval
					}else{
						$updateStageID = 1; //Case Approval
					}
				}
				
				if($sys_custom['ePCM_skipCaseApproval']){
					//Add quotation Start / End Date
					$db->setQuotationDates($caseID);
				}
			break;
			case 'caseApprovalUpdate':
				$json = $_PAGE['Controller']->requestObject('JSON_obj','includes/json.php');
				
				$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
				$thisRule = $db->getRuleInfo($caseInfoAry[0]['RuleID']);
				$thisApprovalMode = $thisRule[0]['ApprovalMode'];
				$ruleArr = $json->decode($thisRule[0]['CaseApprovalRule']);

				// check before update
				if($thisApprovalMode == 'S'){
					$caseApproval = $db->getCaseApproval($caseID);
// 					$beforeUpdateApprovalStage = $db->checkCurrentApprovalSequence($ruleArr, $caseApproval);
					$beforeUpdateSequenceNo = $db->checkCurrentApprovalSequence($ruleArr, $caseApproval);
					$maxSequenceNo = max(Get_Array_By_Key($caseApproval, 'ApprovalSequence'));
					unset($caseApproval);
				}
				
				//Update Case Approval
				$success = $db->updateCaseApproval($caseID,$data['approverUserID'],$data['recordStatus'],$data['feedback'],$data['approverRoleID']);
				$afterCaseApproval = $db->getCaseApproval($caseID);

				if($thisApprovalMode == 'T'){
                    $approvalSummaryAssoc = BuildMultiKeyAssoc($afterCaseApproval, array('ApprovalRoleID'),array('RecordStatus'),1,1);
                    $isRulePassedArr = array();
                    foreach($approvalSummaryAssoc as $_thisRoleId => $_roleApprovalStatusArr){
                        $_thisRoleRequirement = $ruleArr[$_thisRoleId];
                        if($_thisRoleRequirement == 0){
                            $isApprovalDone = (!in_array('-1', $_roleApprovalStatusArr)
                                            && !in_array('0', $_roleApprovalStatusArr) );
                        }else{
                            $_approvalCount = array_count_values($_roleApprovalStatusArr);
                            $isApprovalDone = false;
                            if(!isset($_approvalCount[-1]) && $_approvalCount[1] >= $_thisRoleRequirement){
                                // do not have reject and number of approved > approval requirement
                                $isApprovalDone = true;
                            }
                        }
                        $isRulePassedArr[] = $isApprovalDone;
                    }
                    $needChangeCaseStage = !in_array(false, $isRulePassedArr);
                }else if($thisApprovalMode == 'S'){
					$afterUpdateSequenceNo = $db->checkCurrentApprovalSequence($ruleArr, $afterCaseApproval);
					$needChangeCaseStage = $afterUpdateSequenceNo > $maxSequenceNo ? true : false;
				}
				 
				// check after update if need to send notification
				// change approval role notification
				if($thisApprovalMode == 'S' 
					&& !$upgradeStage
					&& $afterUpdateSequenceNo != $beforeUpdateSequenceNo
					&& $data['approverUserID'] == $_SESSION['UserID'] 
					){
					// check if sent already
					$logArr = $db->getLog('NotifyApproval-'.$afterUpdateSequenceNo, $caseID);
					
					if(count($logArr) == 0){
// 						// normally only send out 1 msg where count($resultMsgID) == 1
						if($_PAGE['libPCM']->canUseAppNotification()){
							$resultMsgID = $db->sendNotificationToApprovalRole($afterUpdateSequenceNo, $caseID, $caseInfoAry[0]['Code']);
							$db->addLog('NotifyApproval-'.$afterUpdateSequenceNo, $caseID, 'MessageID:'.implode(',',(array)$resultMsgID));
						}
					}
				}
				
				$currentStage = $caseInfoAry[0]['CurrentStage'];
				if(($db->getCaseQuotationType($caseID)=='N'||$sys_custom['ePCM_skipCaseApproval']) && $needChangeCaseStage){
					$needUpdateStage = true;
					$updateStageID = 10;	//End Case after endorsment
				}else if($needChangeCaseStage && $currentStage == 1){
					$needUpdateStage = true;
					$updateStageID = 2;
					
					if(!$sys_custom['ePCM_skipCaseApproval']){
						//Add quotation Start / End Date
						$db->setQuotationDates($caseID);
					}
					
					// Update Case Approval Date
					$db->updateCaseApprovalDate($caseID);
				}else{
					$updateStageID = 1;
				}
				
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'custCaseApprovalUpdate':
				$json = $_PAGE['Controller']->requestObject('JSON_obj','includes/json.php');
				
				$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
				$thisRule = $db->getRuleInfo($caseInfoAry[0]['RuleID']);
				$thisApprovalMode = $thisRule[0]['CustApprovalMode'];
				$ruleArr = $json->decode($thisRule[0]['CustCaseApprovalRule']);
				
				// check before update
				if($thisApprovalMode == 'S'){
					$caseApproval = $db->getCustCaseApproval($caseID);
					// 					$beforeUpdateApprovalStage = $db->checkCurrentApprovalSequence($ruleArr, $caseApproval);
					$beforeUpdateSequenceNo = $db->checkCurrentApprovalSequence($ruleArr, $caseApproval);
					$maxSequenceNo = max(Get_Array_By_Key($caseApproval, 'ApprovalSequence'));
					unset($caseApproval);
				}

				//Update Case Approval
				$success = $db->updateCustCaseApproval($caseID,$data['approverUserID'],$data['recordStatus'],$data['feedback'],$data['approverRoleID']);
				$afterCaseApproval = $db->getCustCaseApproval($caseID);
				
//				if($thisApprovalMode == 'T'){
//					$needChangeCaseStage = true;
//					foreach($afterCaseApproval as $_approvalInfo){
//						if($_approvalInfo['RecordStatus'] != '1'){
//							$needChangeCaseStage = false;
//						}
//					}
                if($thisApprovalMode == 'T'){
                    $approvalSummaryAssoc = BuildMultiKeyAssoc($afterCaseApproval, array('ApprovalRoleID'),array('RecordStatus'),1,1);
                    $isRulePassedArr = array();
                    foreach($approvalSummaryAssoc as $_thisRoleId => $_roleApprovalStatusArr){
                        $_thisRoleRequirement = $ruleArr[$_thisRoleId];
                        if($_thisRoleRequirement == 0){
                            $isApprovalDone = (!in_array('-1', $_roleApprovalStatusArr)
                                && !in_array('0', $_roleApprovalStatusArr) );
                        }else{
                            $_approvalCount = array_count_values($_roleApprovalStatusArr);
                            $isApprovalDone = false;
                            if(!isset($_approvalCount[-1]) && $_approvalCount[1] >= $_thisRoleRequirement){
                                // do not have reject and number of approved > approval requirement
                                $isApprovalDone = true;
                            }
                        }
                        $isRulePassedArr[] = $isApprovalDone;
                    }
                    $needChangeCaseStage = !in_array(false, $isRulePassedArr);
				}else if($thisApprovalMode == 'S'){
					$afterUpdateSequenceNo = $db->checkCurrentApprovalSequence($ruleArr, $afterCaseApproval);
					$needChangeCaseStage = $afterUpdateSequenceNo > $maxSequenceNo ? true : false;
				}

				$currentStage = $caseInfoAry[0]['CurrentStage'];
				if($currentStage == 6 && $needChangeCaseStage){
					$needUpdateStage = true;
					$updateStageID = 10;	//End Case after endorsment
				}
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'supplierAdd':
				//add supplier
				
				$success = $db->insertQuotation($caseID, $data['supplierIDAry']);
				$needUpdateStage = false;
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'updateQuotationEndDate':
				$date = $data['quotationEndDate'];
				$needUpdateStage = false;
				$success = $db->updateQuotationEndDate($caseID,$date);
				if($success){
					$view = $date;
				}
			break;
			case 'updateTenderOpeningStartDate':
				$date = $data['tenderOpeningStartDate'];
				$needUpdateStage = false;
				$success = $db->updateTenderOpeningStartDate($caseID,$date);
				if($success){
					$view = $date;
				}
			break;
			case 'updateTenderOpeningEndDate':
				$date = $data['tenderOpeningEndDate'];
				$needUpdateStage = false;
				$success = $db->updateTenderOpeningEndDate($caseID,$date);
				if($success){
					$view = $date;
				}
			break;
			case 'updateTenderApprovalStartDate':
				$date = $data['tenderApprovalStartDate'];
				$needUpdateStage = false;
				$success = $db->updateTenderApprovalStartDate($caseID,$date);
				if($success){
					$view = $date;
				}
			break;
			case 'updateTenderApprovalEndDate':
				$date = $data['tenderApprovalEndDate'];
				$needUpdateStage = false;
				$success = $db->updateTenderApprovalEndDate($caseID,$date);
				if($success){
					$view = $date;
				}
			break;
			case 'finishStage2':
				//Add remarks if remarks != ''
				$db->insertCaseRemarks($caseID,$stageID=2,$data['remarks']);
				$needUpdateStage = true;
				$updateStageID = 3;
			break;
			case 'finishStage3':
				$db->insertCaseRemarks($caseID,$stageID=3,$data['remarks']);
				$needUpdateStage = true;
				$updateStageID = 4;
			break;
			case 'setTenderOpeningDates':
				$needUpdateStage = false;
				$success = $db->updateTenderOpeningDates($caseID,$data['startDate'],$data['endDate']);
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'setTenderApprovalDates':
				$needUpdateStage = false;
				$success = $db->updateTenderApprovalDates($caseID,$data['startDate'],$data['endDate']);
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'updateQuotationDetails':
				$needUpdateStage = false;
				$success = $db->updateQuotationDate($data['quotationID'],$data['quoteDate'],$data['remarks']);
				if(!empty($data['FileIDArr'])){
				    $db->insertFileToQuotation($data['FileIDArr'], $data['caseID'], $data['quotationID']);
				}
				if(!empty($data['WillDelFileArr'])){
				    $file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
				    foreach((array)$data['WillDelFileArr'] as $fileID){
				        $file_db->removeAttachmentStep2($fileID);
				    }
				}
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'cancelQuotationDetails':
				$needUpdateStage = false;
				$success = $db->cancelQuotationDateAndFile($data['quotationID'],$data['remarks']);
				
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'setResult':
				$needUpdateStage = true;
				$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
				$quotationType = $caseInfoAry[0]['QuotationType'];
				
				if(isset($data['reason'])){
					$db->smartInsertCaseRemarks($caseID, $stageID=4, $data['reason']);
				}
				else{
					$db->smartDeleteCaseRemarks($caseID, $stageID=4);
				}
				
				if($quotationType=='N'){
					$updateStageID = 5;	// Endorsment
				}else{
					if($sys_custom['ePCM_skipCaseApproval']){
						$updateStageID = 5;		// Endorsment
					}else{
						$updateStageID = 10;	// End Case
					}
				}
				if($sys_custom['ePCM']['extraApprovalFlow'] && $quotationType != 'N'){
					$updateStageID = 6;
				}

				if($data['resultID']==''){
					//insertNew
					$success = $db->insertResult($caseID,$data['supplierID'],$data['dealPrice'],$data['description'],$data['dealDate'],$data['fundingId1'],$data['fundingId2'],$data['itemUnitPrice1'],$data['itemUnitPrice2']);
				}else{
					//update
					$success = $db->updateResult($data['resultID'],$caseID,$data['supplierID'],$data['dealPrice'],$data['description'],$data['dealDate'],$data['fundingId1'],$data['fundingId2'],$data['itemUnitPrice1'],$data['itemUnitPrice2']);
				}
				if(!empty($data['FileIDArr'])){
				    $db->insertFileToCase($data['FileIDArr'], $caseID, $FileStageID =5);
				}
				if(!empty($data['WillDelFileArr'])){
				    $file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
				    foreach((array)$data['WillDelFileArr'] as $fileID){
				        $file_db->removeAttachmentStep2($fileID);
				    }
				}
				if($success){
					$view = 'Saved';
				}else{
					$view = 'Failed';
				}
			break;
			case 'updateInvitationDoc':
				$needUpdateStage = false;
				$stageID = 2;
				if(!empty($data['FileIDArr'])){
					$db->insertFileToCase($data['FileIDArr'], $caseID, $stageID);
				}
				if(!empty($data['WillDelFileArr'])){
					$file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
					foreach((array)$data['WillDelFileArr'] as $fileID){
						$file_db->removeAttachmentStep2($fileID);
					}
				}
				$view = 'Saved';
			break;
			
			case 'other': //case Other application
				$needUpdateStage = true;
				$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
				$quotationType = 'O';
				if($_SESSION['ePCM']['Setting']['General']['MiscExpenditureApprove']){
					$updateStageID = 2; //pendingS
				}else{
				$updateStageID = 10;
				}
			break;
				
				
			default:
				debug_pr('something wrong');
				die();
			break;
		}
		
		// Update Current Stage
		if($needUpdateStage){
			$db->updateCaseCurrentStage($caseID,$updateStageID);
		}
		//Change Record Status to Approved when the case is finished
		if($updateStageID=='10'){
			$db->updateFundingCaseRelationByCase($caseID,'1');
		}
// 		//Check is Finished
// 		$checkIsCaseFinished = $db->checkIsCaseFinishedAllStage($caseID);
		
// 		//Update Result if case finished
// 		if($checkIsCaseFinished){
// 			//TODO
// 			debug_pr('all stage is done!!!');
// 		}
		
		if($action=='sendCaseApproverInvitation' ){
			$this->sendRedirect("index.php?p=mgmt.case.showConfirm.$caseID");
		}
		
		echo $view;
	}
	public function showConfirm($caseID=''){
		//@@@@@@@@@@@@@@@@@@@@@@@@@@New Case Direct Submit@@@@@@@@@@@@@@@@@@@@@@@@@@@ Origianal code: case 'sendCaseApproverInvitation':
		global $_PAGE,$plugin, $sys_custom;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		//##################### Send toApp
		$db_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
		$setting = $db_setting->getGeneralSettings();
		$_PAGE['allow_send_app']=false;
		//if(isset($plugin['eClassTeacherApp']) && $plugin['eClassTeacherApp']===true && $setting['eClassAppNotification']=='1'){
		if($_PAGE['libPCM']->canUseAppNotification() ){
			$_PAGE['allow_send_app']=true;
		}
		// Omas 20160825
		//$view = $db->sendCaseApproverInvitation($caseID);
		// views_data is inside sendCaseApproverInvitation
		$db->sendCaseApproverInvitation($caseID);
		
		if($sys_custom['ePCM']['extraApprovalFlow']){
			$db->sendCustCaseApproverInvitation($caseID);
		}
		

		if($sys_custom['ePCM_skipCaseApproval']){
			$this->sendRedirect("index.php?p=mgmt.caseDetials.$caseID");
		}
		
		$main = $this->getViewContent('/management/insert_notify_approval_user.php');
		$view = $this->getLayout($main);
		echo $view;
		//@ END @@@@@@@@@@@@@@@@@@@@@@@@@New Case Direct Submit@@@@@@@@@@@@@@@@@@@@@@@@@@@ Origianal code: case 'sendCaseApproverInvitation':
	}
	public function caseDetials($caseID=''){
		global $_PAGE, $Lang, $sys_custom;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'View';
		
		if(!$caseID>0){
			debug_pr("something wrong");
			die();
		}
		
		
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		//Get enable push noti / email
		$gs = $db->getGeneralSettings();
		$emailNotification = $gs['emailNotification'];
		$eClassAppNotification = $gs['eClassAppNotification'];
		
		//Get CaseInfo
		$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
		
		//FundingSource
		$FundingSourceArr = $db->getFundingCaseArr();
		$caseInfoAry[0]['FundingSourceArr'] = $FundingSourceArr;
		
		//Get RoleName
		$roleIDAry = array();
		if($caseInfoAry[0]['QuotationApprovalRoleID']!='0')$roleIDAry[] = $caseInfoAry[0]['QuotationApprovalRoleID'];
		if($caseInfoAry[0]['TenderAuditingRoleID']!='0')$roleIDAry[] = $caseInfoAry[0]['TenderAuditingRoleID'];
		if($caseInfoAry[0]['TenderApprovalRoleID']!='0')$roleIDAry[] = $caseInfoAry[0]['TenderApprovalRoleID'];
		
		$roleInfo = $db->getRoleInfo($roleIDAry);
		$roleInfoAssoAry = BuildMultiKeyAssoc($roleInfo, "RoleID");
		$roleNameKey = Get_Lang_Selection('RoleNameChi','RoleName');
		
		if($caseInfoAry[0]['QuotationApprovalRoleID'] > 0){
			$caseInfoAry[0]['QuotationApprovalRole'] = $roleInfoAssoAry[$caseInfoAry[0]['QuotationApprovalRoleID']][$roleNameKey];
		}else{
			if($caseInfoAry[0]['QuotationApprovalRoleID'] == -1){
				$caseInfoAry[0]['QuotationApprovalRole'] = $Lang['ePCM']['Setting']['Rule']['GroupHead'];
			}
			else if($caseInfoAry[0]['QuotationApprovalRoleID'] == -999){
				$caseInfoAry[0]['QuotationApprovalRole'] = $Lang['ePCM']['Mgmt']['Case']['Applicant'];
			}
		}
		$caseInfoAry[0]['TenderAuditingRole'] = $roleInfoAssoAry[$caseInfoAry[0]['TenderAuditingRoleID']][$roleNameKey];
		$caseInfoAry[0]['TenderApprovalRole'] = $roleInfoAssoAry[$caseInfoAry[0]['TenderApprovalRoleID']][$roleNameKey];
		
		//GetCaseApproval
		$approvalSorting = (($caseInfoAry[0]['ApprovalMode'] == 'T')? 'ApprovalRoleID asc' : 'ApprovalSequence asc').", ApproverName asc";
		$caseApproval = $db->getCaseApproval($caseID,'','',$approvalSorting);
		$isCaseRejected = $db->isCaseRejected('',$caseApproval);
		//Get CustCaseApproval
		if($sys_custom['ePCM']['extraApprovalFlow']){
			$custApprovalSorting = (($caseInfoAry[0]['CustApprovalMode'] == 'T')? 'ApprovalRoleID asc' : 'ApprovalSequence asc').", ApproverName asc";
			$custCaseApproval = $db->getCustCaseApproval($caseID,'','',$custApprovalSorting);
			$isCustCaseRejected = $db->isCaseRejected('',$custCaseApproval);
		}
		
		//Get Case Quotation 
		$quotations = $db->getCaseQuotation($caseID);
		$QuoIDArr = Get_Array_By_Key($quotations, 'QuotationID');
		
		//Get Case Remarks
		$remarks = $db->getCaseRemarks($caseID);
		
		//Get Case Result 
		$result = $db->getResult($caseID);
		
		//Get All Versions caseID
		$versions = $db->getAllHistoryCaseID($caseID);
		
		//Get Notification Schedule
		$pushNotiSchedule = $db->getPushNotificationSchedule($caseID);
		
		//Get Notification User
		$pushNotiUsers = $db->getNotificationScheduleUser($caseID);
// 		debug_pr($pushNotiUsers);
		
		//Get Price Comparison Info
		$priceComparisonInfo = $db->getQuotationPriceItemInfo($caseID=$caseID,$quotationID='',$supplierID='');
		$priceComparisonInfo = BuildMultiKeyAssoc($priceComparisonInfo,array('PriceItemID','QuotationID'));

		//Get log
		$log = $db->getLog('Email To Supplier',$caseID);
		
// 		debug_pr($caseInfoAry[0]);
// 		if($caseInfoAry[0]['IsDeleted'] || $caseInfoAry[0]['CurrentStage'] == 5){
		if($caseInfoAry[0]['IsDeleted'] || $caseInfoAry[0]['CurrentStage'] == 10){
			//if the case is deleted or finished -> set to finished view
			$_PAGE['CurrentPage'] = 'FinishedView';
			if(!$caseInfoAry[0]['IsDeleted']){
				$_PAGE['curTab'] = 'completed';
			}else{
				$_PAGE['curTab'] = 'terminated';
			}
		}
// 		$linventory = $_PAGE['Controller']->requestObject('libinventory','includes/libinventory.php');
// 		$fundingList_arr = $linventory->returnFundingSourceWithFundingType();
		$fundingList_arr = $db->returnFundingSourceWithFundingType_eInv();

		$caseInfoAry[0]['CaseApproval'] = $caseApproval;
		$caseInfoAry[0]['CustCaseApproval'] = $custCaseApproval;
		$caseInfoAry[0]['isCaseRejected'] = $isCaseRejected;
		$caseInfoAry[0]['isCustCaseRejected'] = $isCustCaseRejected;
		$caseInfoAry[0]['Quotation'] = $quotations;
		$caseInfoAry[0]['Remarks'] = $remarks;
		$caseInfoAry[0]['Result'] = $result;
// 		$caseInfoAry[0]['CaseAttachment'] = $db->getCaseStageAttachment($caseID, '0');
		$caseInfoAry[0]['CaseAttachment'] = $db->getCaseStageAttachment($caseID, '', true);
		$caseInfoAry[0]['DeclarationAttachment'] = $db->getCaseStageAttachment($caseID, '', true, '1');
		$caseInfoAry[0]['InvitationAttachment'] = $db->getCaseStageAttachment($caseID, '', true, '2');
		$caseInfoAry[0]['QuoAttachment'] = $db->getQuoAttachment($QuoIDArr,true);
		$caseInfoAry[0]['Version'] = $versions;
		$caseInfoAry[0]['NotiSchedule'] = $pushNotiSchedule; 
		$caseInfoAry[0]['NotiUsers'] = $pushNotiUsers;
		$caseInfoAry[0]['priceComparisonInfo'] = $priceComparisonInfo;
		$caseInfoAry[0]['Log'] = $log;
		$caseInfoAry[0]['fundingList_arr'] = $fundingList_arr;
		$caseInfoAry[0]['exceedBudget'] = $db->checkIfExceedBudget($caseInfoAry[0]['ApplicantGroupID'], $caseInfoAry[0]['ItemID'], $caseInfoAry[0]['Budget']);
		
		$thisRule = $db->getRuleInfo($caseInfoAry[0]['RuleID']);
		$thisRule = $thisRule[0];

// 		// if need principal || VP || group head may need to check ApprovalMode
// 		if( ( $thisRule['ApprovalRoleID'] == 1 || $thisRule['NeedGroupHeadApprove'] == 1 || $thisRule['NeedVicePrincipalApprove'] == 1 ) && $thisRule['ApprovalMode'] == 'S' ){
// 			$caseInfoAry[0]['ApprovalStage'] = $db->checkCurrentApprovalStage($caseApproval);
// 		}
// 		else{
// 			if( $thisRule['ApprovalRoleID'] > 0){
// 				$thisApprovalRoleInfo = $db->getRoleInfo($thisRule['ApprovalRoleID']);
// 				$caseInfoAry[0]['ApprovalRole']  = $thisApprovalRoleInfo[0][Get_Lang_Selection('RoleNameChi','RoleName')];
// 			}
// 			$caseInfoAry[0]['ApprovalStage'] = 'NA';
// 		}
		$json = $_PAGE['Controller']->requestObject('JSON_obj','includes/json.php');
		//$approvalRuleArr = $json->decode($_ruleRow['CaseApprovalRule']);
		$approvalRule = $json->decode($thisRule['CaseApprovalRule']);

		if($thisRule['ApprovalMode'] == 'S'){
			$caseInfoAry[0]['CurrentApprovalSequence'] = $db->checkCurrentApprovalSequence($approvalRule, $caseApproval);
		}else{
			$caseInfoAry[0]['CurrentApprovalSequence'] = 'NA';
		}

		if($sys_custom['ePCM']['extraApprovalFlow']){
			if(!empty($thisRule['CustCaseApprovalRule'])){
                $custApprovalRule = $json->decode($thisRule['CustCaseApprovalRule']);
            }else{
                $custApprovalRule = $approvalRule;
            }

			if($thisRule['CustApprovalMode'] == 'S'){
				$caseInfoAry[0]['CurrentCustApprovalSequence'] = $db->checkCurrentApprovalSequence($custApprovalRule, $custCaseApproval);
			}else{
				$caseInfoAry[0]['CurrentCustApprovalSequence'] = 'NA';
			}
		}
			
		$_PAGE['views_data']['caseAry'] = $caseInfoAry[0];
		$_PAGE['views_data']['generalSetting']['emailNotification'] = $emailNotification;
		$_PAGE['views_data']['generalSetting']['eClassAppNotification'] = $eClassAppNotification;
        $_PAGE['views_data']['ruleID'] = $caseInfoAry[0]['RuleID'];
		$main = $this->getViewContent('/management/caseDetails.php');
		$view = $this->getLayout($main);
		echo $view;
	}
	public function caseApprovalUpdate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'caseApprovalUpdate';
		$_PAGE['POST']['data']['approverUserID'] = $_PAGE['POST']['approverUserID'];
		$_PAGE['POST']['data']['approverRoleID'] = $_PAGE['POST']['approverRoleID'];
		$_PAGE['POST']['data']['recordStatus'] = $_PAGE['POST']['recordStatus'];
		$_PAGE['POST']['data']['feedback'] = $_PAGE['POST']['feedback'];
		
		unset($_PAGE['POST']['approverUserID']);
		unset($_PAGE['POST']['recordStatus']);
		unset($_PAGE['POST']['feedback']);
		
		$this->caseUpdate();
	}
	public function custCaseApprovalUpdate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'custCaseApprovalUpdate';
		$_PAGE['POST']['data']['approverUserID'] = $_PAGE['POST']['approverUserID'];
		$_PAGE['POST']['data']['approverRoleID'] = $_PAGE['POST']['approverRoleID'];
		$_PAGE['POST']['data']['recordStatus'] = $_PAGE['POST']['recordStatus'];
		$_PAGE['POST']['data']['feedback'] = $_PAGE['POST']['feedback'];
	
		unset($_PAGE['POST']['approverUserID']);
		unset($_PAGE['POST']['recordStatus']);
		unset($_PAGE['POST']['feedback']);
	
		$this->caseUpdate();
	}
	public function supplierAdd(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'supplierAdd';
		
		
		$_PAGE['POST']['data']['supplierIDAry']=explode(',',$_PAGE['POST']['supplierIDAry']);
		unset($_PAGE['POST']['supplierIDAry']);
		$this->caseUpdate();
	}
	public function updateQuoteReplyStatus(){
		global $_PAGE;
		$quotationID = $_PAGE['POST']['quotationID'];
		$replyStatus = $_PAGE['POST']['replyStatus'];
        $inputReplyDate = $_PAGE['POST']['inputReplyDate'];
        if(empty($inputReplyDate)||$inputReplyDate=''){
           $firstTimeInputReply=true;
        }
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $mgmtObj->updateQuoteReplyStatus($quotationID,$replyStatus,$firstTimeInputReply);
		if($success){
			echo 'success';
		}
	}
	public function updateQuoteInputReplyDate(){
        global $_PAGE;
        $quotationID = $_PAGE['POST']['quotationID'];
        $inputReplyDate = $_PAGE['POST']['inputReplyDate'];
        $mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
        $success = $mgmtObj->updateQuoteInputReplyDate($quotationID,$inputReplyDate);
        if($success){
            echo 'success';
        }
    }
	public function updateQuotationEndDate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'updateQuotationEndDate';
		$_PAGE['POST']['data']['quotationEndDate'] = $_PAGE['POST']['date'];
		unset($_PAGE['POST']['date']);
		$this->caseUpdate();
	}
	public function updateTenderOpeningStartDate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'updateTenderOpeningStartDate';
		$_PAGE['POST']['data']['tenderOpeningStartDate'] = $_PAGE['POST']['date'];
		unset($_PAGE['POST']['date']);
		$this->caseUpdate();
	}
	public function updateTenderOpeningEndDate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'updateTenderOpeningEndDate';
		$_PAGE['POST']['data']['tenderOpeningEndDate'] = $_PAGE['POST']['date'];
		unset($_PAGE['POST']['date']);
		$this->caseUpdate();
	}
	public function updateTenderApprovalStartDate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'updateTenderApprovalStartDate';
		$_PAGE['POST']['data']['tenderApprovalStartDate'] = $_PAGE['POST']['date'];
		unset($_PAGE['POST']['date']);
		$this->caseUpdate();
	}
	public function updateTenderApprovalEndDate(){
		global $_PAGE;
		$_PAGE['POST']['action'] = 'updateTenderApprovalEndDate';
		$_PAGE['POST']['data']['tenderApprovalEndDate'] = $_PAGE['POST']['date'];
		unset($_PAGE['POST']['date']);
		$this->caseUpdate();
	}
	public function saveNotiScheduleDate(){
		global $_PAGE,$Lang;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$scheduleDateTime = $postVar['scheduleDateTime'];
		$stageID = $postVar['stageID'];
		$isEnabled = $postVar['isEnabled'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		//if isEnabled = 1 => add row in INTRANET_PCM_NOTIFICATION_SCHEDULE
		//if isEnabled = 0 => delete row (physical delete)
		if($isEnabled==1){
			$success = $mgmtObj->saveNotificationSchedule($caseID,$stageID,$scheduleDateTime,$Lang['ePCM']['ScheduledPushNotification']['Deadline']['Title'],$Lang['ePCM']['ScheduledPushNotification']['Deadline']['Message']);
			//Add case owner to be notified User (temp)
			$userID = $mgmtObj->getCaseOwnerUserID($caseID);
			$mgmtObj->addNotificationScheduleUser($caseID,$stageID,$userID);
		}else{
			$success = $mgmtObj->deletePushNotificationSchedule($caseID,$stageID);
		}
	}
	public function finishStage2(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		
		$_PAGE['POST']['action'] = 'finishStage2';
		$_PAGE['POST']['data']['remarks'] = $postVar['remarks'];
		unset($_PAGE['POST']['remarks']);
		
		$this->caseUpdate();
	}
	public function finishStage3(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$_PAGE['POST']['action'] = 'finishStage3';
		$_PAGE['POST']['data']['remarks'] = $postVar['remarks'];
		unset($_PAGE['POST']['remarks']);
		$this->caseUpdate();
	}  
	public function setTenderOpeningDates(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		
		$_PAGE['POST']['action'] = 'setTenderOpeningDates';
		$_PAGE['POST']['data']['startDate'] = $postVar['startDate'];
		$_PAGE['POST']['data']['endDate'] = $postVar['endDate'];
		unset($postVar['startDate']);
		unset($postVar['endDate']);
		
		$this->caseUpdate();
	}
	public function setTenderApprovalDates(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		
		$_PAGE['POST']['action'] = 'setTenderApprovalDates';
		$_PAGE['POST']['data']['startDate'] = $postVar['startDate'];
		$_PAGE['POST']['data']['endDate'] = $postVar['endDate'];
		unset($postVar['startDate']);
		unset($postVar['endDate']);
		
		$this->caseUpdate();
	}
	public function quotationDetailsUpdate(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$quotationID = $postVar['quotationID'];
		$status = $postVar['status'];
		$remarks = $postVar['remarks'];
// 		$quoteDate = $postVar['QuoteDate'];
		$quoteDate = $postVar['datePickerQuoteDate'];
		$quoteTime = sprintf('%02d', $postVar['timeSelectQuoteDate_hour']).':'.sprintf('%02d', $postVar['timeSelectQuoteDate_min']).':'.sprintf('%02d', $postVar['timeSelectQuoteDate_sec']);
		$caseID = $postVar['caseID'];
        $FileIDArr = $postVar['FileID'];
        $WillDelFileArr = $postVar['WillDeletedList'];
//         $FileSizeArr = $postVar['FileSize'];
		
        if($status=='0'){
        	$_PAGE['POST']['action'] = 'cancelQuotationDetails';
        }else{
        	$_PAGE['POST']['action'] = 'updateQuotationDetails';
        }
		$_PAGE['POST']['data']['quotationID'] = $quotationID;
		$_PAGE['POST']['data']['quoteDate'] = $quoteDate.' '.$quoteTime;
		$_PAGE['POST']['data']['caseID'] = $caseID;
		$_PAGE['POST']['data']['remarks'] = $remarks;
		$_PAGE['POST']['data']['FileIDArr'] = $FileIDArr;
		$_PAGE['POST']['data']['WillDelFileArr'] = $WillDelFileArr;
// 		unset($postVar['quotationID']);
// 		unset($postVar['quoteDate']);
		unset($postVar);
		$this->caseUpdate();
	}
	public function contactMethodUpdate(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		
		$caseID = $postVar['caseID'];
		$quotationID = $postVar['quotationID'];
		$contactMethod = $postVar['contactMethod'];
		
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $mgmtObj->updateContactMethod($quotationID,$contactMethod);
		echo ($success)?'success':'failed';
	}
	public function setResult(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
//		// set funding source 2 price to 0 if funding source 2 is not select
//		if ($postVar['fundingId1']==0){
//			$postVar['itemUnitPrice1']=0;
//		}
//		if ($postVar['fundingId2']==0){
//			$postVar['itemUnitPrice2']=0;
//		}
        $caseID = $_PAGE['POST']['caseID'];
		$_PAGE['POST']['action'] = 'setResult';
		$_PAGE['POST']['data']['supplierID'] = $postVar['supplierRadio'];
		$_PAGE['POST']['data']['dealPrice'] = $postVar['dealPrice'];
		$_PAGE['POST']['data']['description'] = $postVar['description'];
		$_PAGE['POST']['data']['dealDate'] = $postVar['datePickerDealDate'];
		$_PAGE['POST']['data']['FileIDArr'] = $postVar['FileID'];
		$_PAGE['POST']['data']['resultID'] = $postVar['resultID'];
		$_PAGE['POST']['data']['WillDelFileArr'] = $postVar['WillDeletedList'];
//		$_PAGE['POST']['data']['fundingId1']= $postVar['fundingId1'];
//		$_PAGE['POST']['data']['fundingId2']= $postVar['fundingId2'];
//		$_PAGE['POST']['data']['itemUnitPrice1']=  $postVar['itemUnitPrice1'];
//		$_PAGE['POST']['data']['itemUnitPrice2']= $postVar['itemUnitPrice2'];
        //Funding Source Related
        $FundingID = $_PAGE['POST']['Funding_Selection_Box'];
        $FundingBudgetUse  = $_PAGE['POST']['Funding_Selection_Box_usingBudget'];
        $RecordStatus = '0';

		if(!empty($postVar['reason'])){
			$_PAGE['POST']['data']['reason'] = $postVar['reason'];
		}
// 		unset($postVar['supplierID']);
// 		unset($postVar['dealPrice']);
// 		unset($postVar['description']);
		unset($postVar);
		
		$this->caseUpdate();
        //Funding Source
        $this->updateFundingSource($caseID,$FundingID,$FundingBudgetUse,$RecordStatus);
	}
	function setPriceComparison(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];

		$caseID = $postVar['caseID'];
		$newItemNameAry = $postVar['newItemName'];
		$priceAry = $postVar['price'];
		$newPriceAry = $priceAry['new'];
		$existingPriceAry = $priceAry['existing'];
		$removePriceItemIDAry = $postVar['removePriceItemID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		if(!empty($newPriceAry)){
			//Step 1: Insert New Price Item - return ids 
			$returnIdAry = $db->insertPriceItem($caseID,$newItemNameAry);
	// 		debug_pr($returnIdAry);
	
			// Step 2: insert relationship between price item and quotation
			
			foreach($newPriceAry as $_quotationID => $_priceAry){
				foreach($_priceAry as $__key => $__price){
					$db->insertQuotationPriceItem($returnIdAry[$__key],$_quotationID,$__price);
				}
			}
		}
		
		
		if(!empty($existingPriceAry)){
			//Step 3: update existing price item
			foreach($existingPriceAry as $_quotationID => $_priceItem){
				foreach($_priceItem as $__priceItemID => $__price){
					$db->updateQuotationPriceItem($_quotationID,$__priceItemID,$__price);
				}
			}
		}
		foreach((array)$removePriceItemIDAry as $_priceItemID){
			$db->deletePriceItem($_priceItemID);
		}
		echo 'Saved';
	}
	public function updateInvitationDocument(){
// 		global $_PAGE;
// 		$postVar = $_PAGE['POST'];
		
// 		$_PAGE['POST']['action'] = 'updateInvitationDoc';
// 		$_PAGE['POST']['data']['FileIDArr'] = $postVar['FileID'];
// 		$_PAGE['POST']['data']['WillDelFileArr'] = $postVar['WillDeletedList'];
// 		unset($postVar);
		
// 		$this->caseUpdate();

		global $_PAGE;
		
		$caseID = $_PAGE['POST']['caseID'];
		$stageID = $_PAGE['POST']['stageID'];
		$FileIDArr = $_PAGE['POST']['FileID'];
		$WillDelFileArr = $_PAGE['POST']['WillDeletedList'];
		
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $db->insertFileToCase($FileIDArr, $caseID, $stageID, $type='2');
		if(!empty($WillDelFileArr)){
			$file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
			foreach((array)$WillDelFileArr as $fileID){
				$file_db->removeAttachmentStep2($fileID);
			}
		}
		if($success){
			echo 'Saved';
		}else{
			echo 'Failed';
		}
		
	}
	### Form
	public function getCaseApprovalForm(){
		global $_PAGE,$sys_custom;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$approverUserID = $postVar['approverUserID'];
		$approverRoleID = $postVar['approverRoleID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		
		$caseApprovalAry = $db->getCaseApproval($caseID,$approverUserID,$approverRoleID);
		$caseInfoAry =  $db->getCaseInfo($caseID);
		$currentStage =  $caseInfoAry[0]['CurrentStage'];
		//If current Stage > 1 => already approved => hide change status selector
		$quotationType = $caseInfoAry[0]['QuotationType'];
		if($quotationType=='N'){
			$isApproved = false;
		}else if($sys_custom['ePCM_skipCaseApproval']){
			if($currentStage==10){
				$isApproved = true;
			}else{
				$isApproved = false;
			}
		}
		else if($currentStage>1){
			$isApproved = true;
		}else{
			$isApproved = false;
		}
		
		$_PAGE['views_data']['isApproved'] = $isApproved;
		$_PAGE['views_data']['caseApprovalInfo'] = $caseApprovalAry[0];
		
//		$main = $ui->getCaseApprovalForm($caseApprovalAry[0],$isApproved);
		
		$main = $this->getViewContent('/management/tb_caseApprovalForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getCustCaseApprovalForm(){
		global $_PAGE,$sys_custom;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$approverUserID = $postVar['approverUserID'];
		$approverRoleID = $postVar['approverRoleID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		
		$caseApprovalAry = $db->getCustCaseApproval($caseID,$approverUserID,$approverRoleID);
		$caseInfoAry =  $db->getCaseInfo($caseID);
		$currentStage =  $caseInfoAry[0]['CurrentStage'];
		//If current Stage > 1 => already approved => hide change status selector
		$quotationType = $caseInfoAry[0]['QuotationType'];
		if($currentStage == 6){
			$isApproved = false;
		}else{
			$isApproved = true;
		}
		
		$_PAGE['views_data']['isApproved'] = $isApproved;
		$_PAGE['views_data']['caseApprovalInfo'] = $caseApprovalAry[0];
		$_PAGE['views_data']['custApproval'] = true;
		
		$main = $this->getViewContent('/management/tb_caseApprovalForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getAddSupplierForm(){
		global $_PAGE, $Lang;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		
		//Get existing quote
		$existingQuotes = $db->getCaseQuotation($caseID);
		$existingSupplierList = Get_Array_By_Key($existingQuotes, "SupplierID");
		
// 		$supplierTypeInfoAry = $db->getSupplyTypeInfo();
		$supplierTypeAry = $db->getSupplierTypeInfoAssoArrForDropDownList($excludeEmptySupplierType= true);
		$supplierTypeAryForAdd = $db->getSupplierTypeInfoAssoArrForDropDownList();

		//Remove existing supplier from supplierAry
		
		$_PAGE['views_data']['supplierTypeAry'] = $supplierTypeAry;
		$_PAGE['views_data']['supplierTypeAryForAdd'] = $supplierTypeAryForAdd;

//		$main = $ui->getAddSupplierFrom($supplierTypeAry);
		$main = $this->getViewContent('/management/tb_addSupplierForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getPreviewEmailForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$quotations = $db->getCaseQuotation($caseID);
		$_PAGE['views_data']['Quotations'] = $quotations;
		$_PAGE['views_data']['SupplierInfo']='Title';
		$_PAGE['views_data']['Subject']='Title';
		$main = $this->getViewContent('/management/tb_previewEmail.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getUploadInviationDocForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$_PAGE['view_data']['caseID'] = $caseID;
		$_PAGE['view_data']['stageID'] = 2;
		
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$main = $this->getViewContent('/management/tb_inviteSupplierUploadDocumentForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getUploadFinalDocForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$_PAGE['view_data']['caseID'] = $caseID;
		$_PAGE['view_data']['stageID'] = 6;
	
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$main = $this->getViewContent('/management/tb_inviteSupplierUploadDocumentForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	
	public function getTenderOpeningForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$_PAGE['view_data']['stageID'] = 4;
		
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$main = $this->getViewContent('/management/tb_declarationOfInterest.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getPriceComparisonForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		
		//Get Case Quotation
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$quotations = $db->getCaseQuotation($caseID);
		
		//Get Existing Price Item 
		$priceItem = $db->getQuotationPriceItemInfo($caseID);
		
		$_PAGE['view_data']['quotations'] = $quotations;
		$_PAGE['view_data']['priceItem'] = $priceItem;
		
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$main = $this->getViewContent('/management/tb_priceComparison.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function saveDeclaration(){
		global $_PAGE;
		
		$caseID = $_PAGE['POST']['caseID'];
		$stageID = $_PAGE['POST']['stageID'];
		$FileIDArr = $_PAGE['POST']['FileID'];
		$WillDelFileArr = $_PAGE['POST']['WillDeletedList'];

		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $db->insertFileToCase($FileIDArr, $caseID, $stageID, $type='1');
		if(!empty($WillDelFileArr)){
			$file_db = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
			foreach((array)$WillDelFileArr as $fileID){
				$file_db->removeAttachmentStep2($fileID);
			}
		}
		if($success){
			echo 'Saved';
		}else{
			echo 'Failed';
		}
	}
	public function getTenderApprovalForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$_PAGE['view_data']['stageID'] = 5;
		
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$main = $this->getViewContent('/management/tb_declarationOfInterest.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getCaseResultForm(){
		global $_PAGE, $Lang;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];

		//Get existing quote
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
// 		$linventory = $_PAGE['Controller']->requestObject('libinventory','includes/libinventory.php');
		
		$quotationType = $db->getCaseQuotationType($caseID);
		if($quotationType=='N'){
			$allSupplier = $db->getSupplierInfo('',0,1);
			$supplierListAry = array();
			foreach($allSupplier['MAIN'] as $_supplier){
				$supplierListAry[] = array($_supplier['SupplierID'],$_supplier[Get_Lang_Selection('SupplierNameChi','SupplierName')]);
			}
		}else{
			if($quotationType=='V'){
				//include non rsponsed supplier -> as non stage 4 of 'V'
				$existingQuotes = $db->getValidQuotation($caseID,true);
				$quotationType;
				
			}else{
				$existingQuotes = $db->getValidQuotation($caseID);
				
			}
			$quotataionIDAry = Get_Array_By_Key($existingQuotes, "QuotationID");
			$priceItemInfo = $db->getQuotationPriceItemInfo($caseID,$quotationID=$quotataionIDAry,$supplierID='');
			$priceItemInfoByQuotationID = BuildMultiKeyAssoc($priceItemInfo, "QuotationID",array(),0,1);
			$quotationPrice = array();
			foreach($priceItemInfoByQuotationID as $quotationID => $_priceInfo){
				$sum = 0;
				foreach($_priceInfo as $__priceInfo){
					$sum = $sum+$__priceInfo['Price'];
				}
				$quotationPrice[$quotationID] = $sum;
			}
			if(count($quotationPrice) > 0){
				$minPrice = min($quotationPrice);
			}
			$supplierListAry = array();
			foreach($existingQuotes as $_quote){
				$thisPrice = $quotationPrice[$_quote['QuotationID']];
				$notMinPrice = ( !isset($minPrice) || $minPrice > 0 && $thisPrice == $minPrice)? false : true;
				$supplierListAry[] = array($_quote['SupplierID'],$_quote[Get_Lang_Selection('SupplierNameChi','SupplierName')], $thisPrice, $notMinPrice );
			}
		}
		//get a funding list including funding id, funding name, funding type from returnFundingSourceWithFundingType
// 		$fundingList_arr = $linventory->returnFundingSourceWithFundingType();
		$fundingList_arr = $db->returnFundingSourceWithFundingType_eInv();
		$numOfFundingList_arr = count($fundingList_arr);
		$fundingListOpt[''] [] = $Lang['ePCM']['Mgmt']['FundingName']['NotApplicable'];
		//rebuild the $fundingList_arr structure
		for ($i = 1; ($i-1) < $numOfFundingList_arr; $i++){
			if($fundingList_arr[$i][2]==1){
				$fundingListOpt[ $Lang['ePCM']['Mgmt']['FundingName']['School'] ][$fundingList_arr[$i]['FundingSourceID']] = $fundingList_arr[$i][1];
			}
			
			if($fundingList_arr[$i][2]==2){
				$fundingListOpt[ $Lang['ePCM']['Mgmt']['FundingName']['Government'] ][ $fundingList_arr[$i]['FundingSourceID'] ]= $fundingList_arr[$i][1];
			}
			
			if($fundingList_arr[$i][2]==3){
				$fundingListOpt[ $Lang['ePCM']['Mgmt']['FundingName']['SponsoringBody'] ][$fundingList_arr[$i]['FundingSourceID']]= $fundingList_arr[$i][1];
			}			
		}

		//Get existing Result
		$result = $db->getResult($caseID);
		//debug_pr($result);
		// Get not choosing lowest price reason (if any)
		$reasonTemp = $db->getCaseRemarks($caseID, 4);
		$result[0]['Reason'] = $reasonTemp[0]['Remark'];

		//Get CaseInfo
		$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
        $FundingSourceArr = $db->getFundingCaseArr();
        $_PAGE['views_data']['FundingSourceArr'] = $FundingSourceArr;
		$_PAGE['views_data']['supplierListAry'] = $supplierListAry;
		$_PAGE['views_data']['caseID'] = $caseID;
		$_PAGE['views_data']['result'] = $result[0];
		$_PAGE['views_data']['caseInfoAry'] = $caseInfoAry[0];
		$_PAGE['views_data']['fundingListOpt'] = $fundingListOpt;
		$_PAGE['views_data']['quotationType'] = $quotationType;
		
// 		$supplierTypeInfoAry = $db->getSupplyTypeInfo();
        $supplierTypeInfoAry = $db->getSupplierTypeInfoAssoArrForDropDownList($excludeEmptySupplierType= true);
		$supplierTypeInfoAryForAdd = $db->getSupplierTypeInfoAssoArrForDropDownList();

		//BuildMultiKeyAssoc();
// 		foreach($supplierTypeInfoAry as $_suppliertype){
// 			$supplierTypeAry[$_suppliertype['TypeID']] = $_suppliertype['TypeName'];
// 		}
		//Remove existing supplier from supplierAry
		
		$_PAGE['views_data']['supplierTypeAry'] = $supplierTypeInfoAry;
        $_PAGE['views_data']['supplierTypeAryForAdd'] = $supplierTypeInfoAryForAdd;
// 		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
// 		$main = $ui->getResultForm($supplierListAry);
		$main = $this->getViewContent('/management/tb_resultForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getQuotationDetailsUpdateForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$quotationID = $postVar['quotationID'];
	
		//Get quotation details
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$quotesDetails = $db->getCaseQuotation('',$quotationID);
	
		$_PAGE['views_data']['quotesDetails'] = $quotesDetails[0];
//  		debug_pr($quotesDetails[0]);
		if($quotesDetails[0]['QuoteDate']!=''){
			$status = 1;
		}else{
			if($quotesDetails[0]['ReplyStatus']!='1'){
				$status = 0;
			}else{
				$status = 1;
			}
		}
		$_PAGE['views_data']['status'] = $status;
// 		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
// 		$main = $ui->getQuotationDetailsForm($quotesDetails[0]);
		$main = $this->getViewContent('/management/tb_quotationDetailsForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getTerminationForm(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		$caseID = $postVar['caseID'];
		$_PAGE['views_data']['caseID'] = $caseID;
		$main = $this->getViewContent('/management/tb_terminationForm.php');
		$view = $this->getLayout($main, '', '', '/tb.php');
		echo $view;
	}
	public function getSupplierInfoHtml(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$supplierInfoAry =$mgmtObj->getSupplierInfo($postVar['supplierID']);
		$supplierInfo = $supplierInfoAry['MAIN'][0];
		$supplierInfo['TypeID'] = $supplierInfoAry['TYPE_MAPPING'][0]['TypeID'];
		$typeInfo = $mgmtObj->getSupplyTypeInfo(0,$supplierInfo['TypeID']);
		$supplierInfo['TypeName'] = $typeInfo[0]['TypeName'];
		echo $ui->getSupplierInfo($supplierInfo);
	}
	
	### Finished
	public function finishedView($tab=''){
		global $_PAGE;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'FinishedView';
		
		if($tab != ''){
			$tabParmArr = explode('.',$tab);
			$_PAGE['curTab'] = $tabParmArr[0];
		}else{
			$_PAGE['curTab'] = 'completed';
		}
		
		switch ($_PAGE['curTab']){
			case 'completed':
				$view = $this->getCompletedView();
			break;
			case 'terminated':
				$view = $this->getTerminatedView();
			break;
		}
		echo $view;
	}
	
	public function finishedOtherView($tab=''){ //Villa	2016-09-29
		global $_PAGE;
		$_PAGE['CurrentSection'] = 'Management';
		$_PAGE['CurrentPage'] = 'OthersView';
		$view = $this->getCompletedOthersView();

		echo $view;
	}
	
	public function getCompletedView(){
		global $_PAGE,$UserID;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
		if($auth->isAdmin()){
			$sql = $db->getCompletedCaseSqlForDbTable();

		}else{
			$sql = $db->getCompletedCaseSqlForDbTableForNonAdmin();
			//$sql = $db->getCompletedCaseSqlForDbTable();
		}
		$_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
		$_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
		$_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
		$_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
		$_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
		$_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
		$_PAGE['views_data']['sql'] = $sql;
		$main = $this->getViewContent('/management/completedCase.php');
		$view = $this->getLayout($main);
		return $view;
	}
	
	public function getCompletedOthersView(){ //villa
		global $_PAGE,$UserID;
		
		//$other = true;
		
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
// 		if($auth->isAdmin()){
		if($auth->isAdmin()){
			$sql = $db->getCompletedCaseSqlForDbTable2();
		
		}else{
			$sql = $db->getCompletedCaseSqlForDbTableForNonAdminForOtherApplication();
			//$sql = $db->getCompletedCaseSqlForDbTable();
		}
		

		//$sql = 'Select * From INTRANET_PCM_CASE where  RuleID IN (Select RuleID From INTRANET_PCM_RULE WHERE QuotationType = "O")';
		
		$_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
		$_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
		$_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
		$_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
		$_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
		$_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
		$_PAGE['views_data']['sql'] = $sql;
		$main = $this->getViewContent('/management/completedOtherCase.php');
		$view = $this->getLayout($main);
		return $view;
	}
	
	public function getTerminatedView(){
		global $_PAGE;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');
		if($auth->isAdmin()){
			$sql = $db->getTerminatedCaseSqlForDbTable();
		}else{
			$sql = $db->getTerminatedCaseSqlForDbTableForNonAdmin();
		}
		
		
		$_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
		$_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
		$_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
		$_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
		$_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
		$_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
		$_PAGE['views_data']['sql'] = $sql;
		$main = $this->getViewContent('/management/terminatedCase.php');
		$view = $this->getLayout($main);
		return $view;
	}
	public function deleteQuotation(){
		global $_PAGE;
		$quotationID = $_PAGE['POST']['quotationID'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$success = $mgmtObj->deleteQuotation($quotationID);
		if($success){
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	
	
	public function caseSendNotification($method)
    {
        global $_PAGE, $Lang,$intranet_root;
        $pcm = $_PAGE['libPCM'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt', 'includes/ePCM/libPCM_db_mgmt.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        if ($_PAGE['POST']['caseID'] == '') {
            die();
        }
        $approverUserID = array_unique(Get_Array_By_Key($db->getCaseApproval($_PAGE['POST']['caseID']),
            'ApproverUserID'));
        $approverName = array_unique(Get_Array_By_Key($db->getCaseApproval($_PAGE['POST']['caseID']), 'ApproverName'));
        $ToArray = $approverUserID;

        $caseInfo = $db->getCaseInfo($_PAGE['POST']['caseID']);
        $caseID = $_PAGE['POST']['caseID'];
        $caseAry = $caseInfo[0];
        $caseName = $caseAry['CaseName'];
        $categoryName = $caseAry['CategoryName'];
        $categoryNameChi = $caseAry['CategoryNameChi'];
        $description = $caseAry['Description'];
        $groupName = $caseAry['GroupName'];
        $groupNameChi = $caseAry['GroupNameChi'];
        $financialItemName = $caseAry['ItemName'];
        $financialItemNameChi = $caseAry['ItemNameChi'];
        $budget = $caseAry['Budget'];
        $applicantType = $caseAry['ApplicantType'];
        $applicantUserName = $caseAry['ApplicantUserName'];
        $applicantGroupName = $caseAry['GroupName'];
        $applicantGroupNameChi = $caseAry['GroupNameChi'];
        $applicant = ($applicantType == 'I') ? $applicantUserName : Get_Lang_Selection($applicantGroupNameChi,
            $applicantGroupName);
        $dateInput = $caseAry['DateInput'];
        $FundingSourceArr = $db->getFundingCaseArr();

        $Subject = str_replace('<!--CASE_CODE-->', $_PAGE['POST']['code'],
                $Lang['ePCM']['Mgmt']['Case']['Email']['Title']) . ' ' . $caseName;
        $Message = str_replace('<!--CASE_CODE-->', $_PAGE['POST']['code'],
                $Lang['ePCM']['Mgmt']['Case']['Email']['Content']) . ' ' . $caseName;
        $x = '<table cellpadding="7" cellspacing="0" border="0" width="100%" class="info">';
        $x .= '<tbody valign="top">';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['Category'] . '</td>';
        $x .= '<td>:</td>';
        $x .= '<td>' . Get_Lang_Selection($categoryNameChi, $categoryName) . '</td>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . nl2br($Lang['ePCM']['Mgmt']['Case']['Purpose']) . '</td>';
        $x .= '<td>:</td>';
        $x .= '<td>' . Get_String_Display($description) . '</td>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['Group'] . '</td>';
        $x .= '<td>:</td>';
        $x .= '<td>' . Get_Lang_Selection($groupNameChi, $groupName) . '</td>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['FinancialItem'] . '</td>';
        $x .= '<td>:</td>';
        $x .= '<td>' . Get_Lang_Selection($financialItemNameChi, $financialItemName) . '</td>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['Budget'] . '</td>';
        $x .= '<td>:</td>';

        $x .= '<td>' . $ui->displayMoneyFormat($budget) . '</td>';
        $x .= '</tr>';
        if ($_PAGE['libPCM']->enableFundingSource()) {
            $x .= '<tr>';
            $x .= '<td>' . $Lang['ePCM']['FundingSource']['FundingSource'] . '</td>';
            $x .= '<td>' . ':' . '</td>';
            $x .= '<td>';
            $x .= '<table width="100%" style="float:left; ">';
            //Print the Funding Source
            $print_funding = true;
            foreach ((array)$FundingSourceArr as $_FundingSourceArr) { //loop every funding
                foreach ((array)$_FundingSourceArr['Case'] as $_Case) { //loop every case
                    if ($_Case['CaseID'] == $caseID) { //print only this case
                        $x .= '<tr>';
                        $x .= '<td width="30%">';
                        $x .= Get_Lang_Selection($_FundingSourceArr['FundingNameCHI'],
                            $_FundingSourceArr['FundingNameEN']);
                        $x .= "&nbsp;";
                        $x .= '(' . $Lang['ePCM']['FundingSource']['RemainingBudget'] . ':' . $_FundingSourceArr['BugetRemain'] . ')';
                        $x .= '</td>';
                        $x .= '<td style="float:left;">';
                        $x .= $Lang['ePCM']['FundingSource']['UsingBudget'];
                        $x .= $_Case['Amount'];
                        $x .= '</td>';
                        $x .= '</tr>';
                        $print_funding = false;
                    }
                }
            }
            if ($print_funding) {
                $x .= '<tr>';
                $x .= '<td>' . '-' . '</td>';
                $x .= '</tr>';
            }
            $x .= '</table>';
            $x .= '</td>';
            $x .= '</tr>';
        }
        $x .= '<tr>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['Applicant'] . '</td>';
        $x .= '<td>:</td>';
        $x .= '<td>' . $applicant . '</td>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td>' . $Lang['ePCM']['Mgmt']['Case']['ApplicationDate'] . '</td>';
        $x .= '<td>:</td>';
        $x .= '<td>' . $dateInput . '</td>';
        $x .= '</tr>';
        $x .= '</tbody>';
        $x .= '</table>';


        if ($method == 'email') {
            $Message .= $x;
            $failListStr = '';
            $result = true;
            $numSuccess = 0;
            foreach ((array)$ToArray as $key => $_approverUserID) {
                $mailResult = $pcm->sendModuleMail(array($_approverUserID), $Subject, $Message);
                if (!$mailResult) {
                    $result = false;
                    $name = $approverName[$key];
                    $failListStr .= '<br>' . $name;
                } else {
                    $numSuccess += 1;
                }
            }
            $resultStr = "<font color='#DD5555'>";
            $resultStr .= $Lang['ePCM']['Mgmt']['Case']['Finished'] . '!<br>';
            if ($numSuccess == count($ToArray) && $result) {
                $resultStr .= $Lang['ePCM']['Mgmt']['Case']['Sent successfully'];
            }
            echo $resultStr;
        }else {
                $individualTeacherMessageInfoAry = array();

                $Subject = str_replace('<!--CASE_CODE-->', $_PAGE['POST']['code'],
                    $Lang['ePCM']['Mgmt']['Case']['PushMessage']['Title']);
                $Message = str_replace('<!--CASE_NAME-->', $caseName,
                    str_replace('<!--CASE_CODE-->', $_PAGE['POST']['code'],
                        $Lang['ePCM']['Mgmt']['Case']['PushMessage']['Content']));

                $messageTitle = $Subject;
                $messageContent = $Message;

                $ToArrayTeacher = array_unique(Get_Array_By_Key($db->getCaseApproval($_PAGE['POST']['caseID'], '', '',
                    '', $userType = 1), 'ApproverUserID'));
                $ToArrayParent = array_unique(Get_Array_By_Key($db->getCaseApproval($_PAGE['POST']['caseID'], '', '',
                    '', $userType = 3), 'ApproverUserID'));

                $sql = "SELECT 
				ip.ParentID, ip.StudentID
		from 
				INTRANET_PARENTRELATION AS ip 
				Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
		WHERE 
				ip.ParentID IN ('" . implode("','", (array)$ToArrayParent) . "') 
				AND iu.RecordStatus = 1";
            $parentStudentAssoAry = BuildMultiKeyAssoc($db->returnResultSet($sql), 'ParentID', array('StudentID'),
                $SingleValue = 1, $BuildNumericArray = 1);

            foreach ($ToArrayTeacher as $to) {
                $info_array = array(
                    'relatedUserIdAssoAry' => array($to => $to)
                );
                array_push($individualTeacherMessageInfoAry, $info_array);
            }
            $resultT = $pcm->sendPushMessage($individualTeacherMessageInfoAry, $messageTitle, $messageContent,
                $isPublic = '', $recordStatus = 1, $appType = 'T');

            $messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
            $resultP = $pcm->sendPushMessage($messageInfoAry, $messageTitle, $messageContent, $isPublic = '',
                $recordStatus = 1, $appType = 'P');

            $result = array_merge((array)$resultP, (array)$resultT);

            include_once $intranet_root.'/includes/libmessagecenter.php';
            $messageCenter = new libmessagecenter();
            $resultAry = $messageCenter->getPushMessageUserSendSuccessStatus($result);

            $failListStr = '';
            $numSuccess = 0;
            foreach ($resultAry as $RecepientStatusAry) {
                if (!$RecepientStatusAry['MessageStatus']) {
                    $failListStr .= '<br>' . $RecepientStatusAry['name'];
                } else {
                    $numSuccess += 1;
                }
            }
            $resultStr = "<font color='#DD5555'>";
            $resultStr .= $Lang['ePCM']['Mgmt']['Case']['Finished'] . '!<br>';
            if ($numSuccess == count($ToArray) && count($ToArray) > 0) {
                $resultStr .= $Lang['ePCM']['Mgmt']['Case']['Sent successfully'];
            } else {
                $resultStr .= $Lang['ePCM']['Mgmt']['Case']['Failed to send pushMessage'] . $failListStr;
            }
            $resultStr .= "</font>";
            echo $resultStr;
        }
    }
	public function sendEmailInvitation(){
		global $_PAGE,$PATH_WRT_ROOT,$Lang,$junior_mck;
		//print_r($_PAGE['POST']);
		$str_identifier1='<!--SUPPLIER_NAME-->';
		$str_identifier2='<!--CASE_NAME-->';
		$str_identifier3='<!--CASE_NUMBER-->';
		$str_identifier4='<!--SCHOOL_NAME-->';
		
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$db_file = $_PAGE['Controller']->requestObject('libPCM_db_file','includes/ePCM/libPCM_db_file.php');
		$pcm = $_PAGE['libPCM'];
		
		$array_sent_quotation_id=array();
		
		$rows_quotation=$db->getCaseQuotation($_PAGE['POST']['CaseID']);
		foreach((array)$rows_quotation as $row_quotation){
			if($row_quotation['ReplyStatus']!='1'){
				$case_id=$row_quotation['CaseID'];
				$rows_case=$db->getCaseInfo($case_id);
				$row_case=$rows_case[0];
				
				$supplier_id=$row_quotation['SupplierID'];
				$array_supplier=$db->getSupplierInfo($supplier_id);
				$rows_supplier=$array_supplier['MAIN'];
				$row_supplier=$rows_supplier[0];
				
				$case_name=$row_case['CaseName'];
				$case_number=$row_case['Code'];
				$supplier_name=$row_supplier['SupplierName'];
				$school_name=GET_SCHOOL_NAME();
				
				$quotation_id=$row_quotation['QuotationID'];
				
				$sender_email=$pcm->getSenderEmail($_SESSION['UserID']);
				
				$from=$school_name.'<no-reply'.substr($sender_email,strpos($sender_email,'@')).'>';
				$cc='';
				$bcc='';
				
				//for email attachment
				//$attachment_folder=$db_file->getEmailAttachmentReady($case_id);
				//for api download attachment
				$api_attachment_folder = $db_file->getApiAttachmentReady($case_id);
				
				$subject=$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Subject'];
				$message=$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Body'];
				
				######EJ Only######
				if(isset($junior_mck)){
					$school_name = convert2unicode(trim($school_name), 1);
				}
				
				$subject=str_replace($str_identifier3,$case_number,$subject);
				$message=str_replace($str_identifier1,$supplier_name,$message);
				$message=str_replace($str_identifier2,$case_name,$message);
				$message=str_replace($str_identifier3,$case_number,$message);
				$message=str_replace($str_identifier4,$school_name,$message);
				
				$result=$pcm->sendEmailToSupplierByQuotation('SUPPLIER',$quotation_id,$token_id='',$subject,$message,$addition_to='',$cc,$bcc,$attachment_folder,$from);
				if($result=='SENT'){
					array_push($array_sent_quotation_id,$quotation_id);
				}
			}
		}
		$str_sent_quotation_id=implode(',',$array_sent_quotation_id);
		//save log
		if($array_sent_quotation_id==''){
			$db->addLog('Email To Supplier', $_PAGE['POST']['CaseID'], 'No emails are sent.');
		}else{
			$db->addLog('Email To Supplier', $_PAGE['POST']['CaseID'], 'Emails are sent successfully to the suppliers of the quotation IDs '.$str_sent_quotation_id.'.');
		}
		
		echo $str_sent_quotation_id;
	}
	
	public function chooseIndividual(){
		global $_PAGE;
		$postVar = $_PAGE['POST'];
		

		$main = '';
		$view = $this->getViewContent('/template/add_single_user.php');
		echo $view;
	}
	
	public function downloadDocumentSummary($parTask=""){
		global $_PAGE, $sys_custom, $intranet_root;
		$postVar = $_PAGE['POST'];

		$caseID = $postVar['caseID'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');
		
		//Get CaseInfo
		$caseInfoAry =  $mgmtObj->getCaseInfo($caseID,'','','');
		
		//Get Case Quotation
		$quotations = $mgmtObj->getCaseQuotation($caseID,'',true);
		
		//Get Case Result
		$result = $mgmtObj->getResult($caseID);
		
		//Get Price comparison info
        $priceItem = $mgmtObj->getQuotationPriceItemInfo($caseID);

        $caseApprovalInfoArr = $mgmtObj->getCaseApproval($caseID);
        $custCaseApprovalInfoArr = $mgmtObj->getCustCaseApproval($caseID);

		$_PAGE['views_data']['CaseInfoAry'] = $caseInfoAry[0];
		$_PAGE['views_data']['Quotations'] = $quotations;
		$_PAGE['views_data']['Result'] = $result[0];
		$_PAGE['views_data']['priceItem'] = $priceItem;
		$_PAGE['views_data']['Remarks'] = $mgmtObj->getCaseRemarks($caseID);
        $_PAGE['views_data']['caseApprovalInfo'] = $caseApprovalInfoArr;
        $_PAGE['views_data']['custCaseApprovalInfo'] = $custCaseApprovalInfoArr;

        $documentName = 'Summary_report_'.$caseInfoAry[0]['Code'].'_'.date("ymd").".doc";
        $docObj = $_PAGE['Controller']->requestObject('libPCM_doc','includes/ePCM/libPCM_doc.php');

        $basePath = $intranet_root.$this->getViewBaseDir();
        $custTemplatePath = '/template/custom/' . $sys_custom['ePCM']['SummaryDocCustTemplate'] . '/summary_template.php';
        $defaultTemplatePath = '/template/summary_approval_record_report_doc.php';

        if($parTask == '') {
            if (isset($sys_custom['ePCM']['SummaryDocCustTemplate'])
                && file_exists($basePath.$custTemplatePath)) {
                $html = $this->getViewContent($custTemplatePath);
            } else {
                $html = $this->getViewContent($defaultTemplatePath);
            }
        } else {
            $specialCustTemplatePath = '/template/custom/' . $sys_custom['ePCM']['SummaryDocCustTemplate'] . '/'.$parTask.'_template.php';
            if ($sys_custom['ePCM']['SummaryDocCustTemplate'] == 'mfs'
                && file_exists($basePath.$specialCustTemplatePath)) {
                $documentName = 'ProcumentConfirmationForm_'.$caseInfoAry[0]['Code'].'_'.date("ymd").".doc";
                $html = $this->getViewContent($specialCustTemplatePath);
            } else {
                $html = $this->getViewContent($defaultTemplatePath);
            }
        }
		$docObj->getWordDocument($html,$documentName);
	}

	public function codeAndNameUpdate(){
		global $_PAGE;
		$name = $_PAGE['POST']['name'];
		$code = $_PAGE['POST']['code'];
		$caseID = $_PAGE['POST']['caseID'];
		$mgmtObj = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');

		$result = $mgmtObj->updateCaseCodeAndName($caseID,$code,$name);
		
		if($result){
			echo 'SUCCESS';
		}else{
			echo 'FAILED';
		}
	}
	public function addSupplierByAjax(){
		global $_PAGE;
		$supplierNameChi = $_PAGE['POST']['supplierNameChi'];
		$supplierName = $_PAGE['POST']['supplierName'];
		$type = $_PAGE['POST']['type'];
		$description = $_PAGE['POST']['description'];
		
		$settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$settingObj->saveSupplier(
				array(
						'SupplierNameChi'=>$supplierNameChi,
						'SupplierName'=>$supplierName,
						'Type'=>$type,
						'IsActive'=>1,
						'Description'=>$description
				),
				array()
			);
		
		echo 'SUCCESS';
	}
	public function reloadSupplierType(){
		
		global $_PAGE, $Lang;
		$linterface = $_PAGE['libPCM_ui'];
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		
		$mode = $_PAGE['POST']['mode'];
		$needAddBtn = $_PAGE['POST']['addBtn'];
		
		$supplierTypeAry = $db->getSupplierTypeInfoAssoArrForDropDownList();
		
		if($mode =='1'){
			$supplierTypeSel = getSelectByAssoArray($supplierTypeAry, $tags='name="typeID_addNew" id="typeID_addNew"', $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
		}else if($mode == '2'){
			$supplierTypeSel = getSelectByAssoArray($supplierTypeAry, $tags='name="typeID" id="typeID" onchange="onChangeSupplierType(this.value);"', $selected="", $all=1, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
		}
		
		if($needAddBtn  && $_PAGE['libPCM']->enableGroupSupplierType() ){
// 			$newSupplierTypeBtn = '<a href="#" class="tablelink" onclick="showAddNewSupplierType();">[+]'.'</a>';
			$newSupplierTypeBtn = $linterface->GET_ACTION_BTN($Lang['ePCM']['Mgmt']['Case']['Supplier']['NewSupplierType'], 'button', $ParOnClick="showAddNewSupplierType();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
		}else{
			$newSupplierTypeBtn = '';
		}
		
		echo $supplierTypeSel. $newSupplierTypeBtn;
	}
	public function updateFundingSource($CaseID,$FundingIDArr,$FundingBudgetUseArr,$RecordStatus){
		global $_PAGE, $Lang;
		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		//Get Funding Relate to Case
		$rs = $db->getFundingCaseRelationRawData($CaseID);
		foreach((array)$rs as $FundingID => $_rs){
			$CaseFundingIDArr[] = $FundingID;
		}
		
		foreach ((array)$FundingIDArr as $FundingKey => $FundingID){
			$FundingBudgetUse = $FundingBudgetUseArr[$FundingKey];
			if($CaseID && $FundingID){
				$db->updateFundingCaseRelation($CaseID,$FundingID,$FundingBudgetUse,$RecordStatus);
			}elseif($CaseID && !$FundingID){ //N.A.
				//Get Funding need to be deleted
				$deleteFundingCaseRelationArr = array_diff((array)$CaseFundingIDArr,(array)$FundingIDArr);
				if(!empty($deleteFundingCaseRelationArr)){
					foreach((array)$deleteFundingCaseRelationArr as $deleteFundingCaseRelationID){
						$db->deleteFundingCaseRelation($CaseID,$deleteFundingCaseRelationID);
					}
				}
			}
		}
	}
	
	public function printApplicationForm($caseID){
		global $_PAGE,$Lang, $intranet_root,$sys_custom;

		$db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt','includes/ePCM/libPCM_db_mgmt.php');
		if($caseID != ''){
			$caseInfoAry =  $db->getCaseInfo($caseID,'','','');
			$Data = $caseInfoAry[0];
			
			$approvalSorting = ($Data[0]['ApprovalMode'] == 'T')? 'ApprovalRoleID asc' : 'ApprovalSequence asc';
			$caseApproval = $db->getCaseApproval($caseID,'','',$approvalSorting);
		}
		
		if(!$sys_custom['ePCM']['PrintArr']){
			$PrintArr= array('CaseName','CategoryName','Description','GroupName','ItemName','Budget','FundingSourceArr','PrincipalApproval','DateOfApproval');
		}else{
			$PrintArr= $sys_custom['ePCM']['PrintArr'];
		}
		$caseID = $Data['CaseID'];
		$groupName = $Data['GroupName'];
		$groupNameChi = $Data['GroupNameChi'];
		#RelatedUser
		$applicantType = $Data['ApplicantType'];
		$applicantUserID = $Data['ApplicantUserID'];
		$applicantUserName = $Data['ApplicantUserName'];
		$applicantGroupName = $Data['GroupName'];
		$applicantGroupNameChi = $Data['GroupNameChi'];
		$applicantGroupID = $Data['ApplicantGroupID'];
		$applicant = ($applicantType=='I')?$applicantUserName:Get_Lang_Selection($applicantGroupNameChi,$applicantGroupName);
		$dateInput = $Data['DateInput'];
		$caseApprovalAry = $caseApproval;
		$quotationApprovalRole = $Data['QuotationApprovalRole'];
		$tenderAuditingRole = $Data['TenderAuditingRole'];
		$tenderApprovalRole = $Data['TenderApprovalRole'];
		
		if($_PAGE['libPCM']->enableFundingSource()){
			$FundingSourceArr = $Data['FundingSourceArr'];
			$Data['FundingSourceArr']= '<table width="100%" style="float:left; ">';
			//Print the Funding Source
			$print_funding = true;
			foreach ((array)$FundingSourceArr as $_FundingSourceArr){ //loop every funding
				foreach ((array)$_FundingSourceArr['Case'] as $_Case){ //loop every case
					if($_Case['CaseID']==$caseID){ //print only this case
						$Data['FundingSourceArr'].= '<tr>';
						$Data['FundingSourceArr'].= '<td width="50%">';
						$Data['FundingSourceArr'].= Get_Lang_Selection($_FundingSourceArr['FundingNameCHI'], $_FundingSourceArr['FundingNameEN']);
						$Data['FundingSourceArr'].= "&nbsp;";
						$Data['FundingSourceArr'].= '('.$Lang['ePCM']['FundingSource']['RemainingBudget'] .':'.$_FundingSourceArr['BugetRemain'].')';
						$Data['FundingSourceArr'].= '</td>';
						$Data['FundingSourceArr'].= '<td style="float:left;">';
						$Data['FundingSourceArr'].= $Lang['ePCM']['FundingSource']['UsingBudget'];
						$Data['FundingSourceArr'].= $_Case['Amount'];
						$Data['FundingSourceArr'].= '</td>';
						$Data['FundingSourceArr'].= '</tr>';
						$print_funding = false;
					}
				}
			}
			if($print_funding){
				$Data['FundingSourceArr'].= '<tr>';
				$Data['FundingSourceArr'].= '<td>'.'-'.'</td>';
				$Data['FundingSourceArr'].= '</tr>';
			}
			$Data['FundingSourceArr'].= '</table>';
		}
		
		$approverNameAry = Get_Array_By_Key($caseApprovalAry, "ApproverName");
		$RelatedUser.= '<tr>';
		$RelatedUser.= '<td>'.$Lang['ePCM']['Mgmt']['Case']['RelatedUsers'].'</td>';
		$RelatedUser.= '<td>:</td>';
		$RelatedUser.= '<td>';
		$RelatedUser.= '<table>';
		$RelatedUser.= '<tr>';
		$RelatedUser.= '<td>'.$Lang['ePCM']['Report']['FCS']['Applicant'].'</td>';
		$RelatedUser.= '<td>:</td>';
		$RelatedUser.= '<td>'.$applicantUserName.'</td>';
		$RelatedUser.= '</tr>';
		if(!($quotationType=='N' || $sys_custom['ePCM_skipCaseApproval'])){
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Mgmt']['Case']['CaseApproval'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.implode(', ',(array)$approverNameAry).'</td>';
			$RelatedUser.= '</tr>';
		}
		if($quotationType=='V'){
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Setting']['Rule']['Verbal'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.$applicantUserName.'</td>';
			$RelatedUser.= '</tr>';
		}
		if($quotationType=='Q'){
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Quotation'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.$applicantUserName.'</td>';
			$RelatedUser.= '</tr>';
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Mgmt']['Case']['UploadQuoation'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.$applicantUserName.'</td>';
			$RelatedUser.= '</tr>';
		}
		if($quotationType=='T'){
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Setting']['Rule']['TenderAuditing'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.$tenderAuditingRole.'</td>';
			$RelatedUser.= '</tr>';
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Setting']['Rule']['TenderApproval'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.$tenderApprovalRole.'</td>';
			$RelatedUser.= '</tr>';
		}
		if($quotationType=='Q'||$quotationType=='N'||$quotationType=='V'){
			switch($quotationType){
				case 'Q':
				case 'V':
					$inputRole = $quotationApprovalRole;
					break;
				case 'N';
				$inputRole = $applicantUserName;
				break;
			}
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Mgmt']['Case']['InputResult'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.$inputRole.'</td>';
			$RelatedUser.= '</tr>';
		}
		if($quotationType=='N'||$sys_custom['ePCM_skipCaseApproval']){
			$RelatedUser.= '<tr>';
			$RelatedUser.= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Endorsement'].'</td>';
			$RelatedUser.= '<td>:</td>';
			$RelatedUser.= '<td>'.implode(', ',(array)$approverNameAry).'</td>';
			$RelatedUser.= '</tr>';
		}
		$RelatedUser.= '</table>';
		$RelatedUser.= '</td>';
		$RelatedUser.= '</tr>';
		$x ="<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> ";
		$x .= '<div align="center">'.$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['FormTitle'].'</div>';
		$x .= '<table width="100%" border="1" cellspacing="5" cellpadding="5">';
		$x .= '<tr>';
		$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['Group'].':'.'</td>';
		$x .= '<td>'.Get_Lang_Selection($groupNameChi,$groupName).'</td>';
		$x .= '<td>'.$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'].':'.'</td>';
		$x .= '<td>'.$dateInput.'</td>';
		$x .= '</tr>';
		foreach ($PrintArr as $Header){
			$x .= '<tr>';
			$x .= '<td colspan="2">'.$Lang['ePCM']['Mgmt']['Case']['ApplicationForm'][$Header].':'.'</td>';
			$x .= '<td colspan="2">'.Get_Lang_Selection($Data[$Header.'Chi'], $Data[$Header]).'</td>';
			$x .= '</tr>';
		}
		$x .= '<tr>';
		$x .= '<td colspan="4">'.'<table>'.$RelatedUser.'</table>'.'</td>';
		$x .= '</tr>';
		$x .= '</table>';
		$x .= '<script>window.print();</script>';
		echo $x;
	}
}

?>