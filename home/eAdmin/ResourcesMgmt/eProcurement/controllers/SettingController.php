<?php
// Editing by 
/* 
 * Date 2017-03-15 Villa
 * modified funding() - add recordStatus Filter backend mechanics
 * 
 * Date 2017-03-03 Villa
 * modified funding() - display funding Source page
 * add updateFundingCategory - update funding Category Record
 * add deleteFundingCategory - delete funding Category Record
 * add updateFunding - update funding Record
 * add deleteFunding - delete funding Record
 * 
 * Date	2016-11-23 Omas
 * modified exportSupplier() - fix EJ problem
 * 
 * Date 2016-09-19 Omas
 * modified supplierTypeUpdate() - add new param
 * modified supplierType.update - change permission
 * 
 * Date	2016-08-29 Omas
 * modified ruleUpdate() - added approval mode
 * modified groupBudget() - fixed academicYear Problem
 */
include_once("BaseController.php");

class SettingController extends BaseController
{
    public function __construct()
    {
        global $_PAGE;
        parent::__construct();
        $this->RequestMapping[''] = array('function' => 'index');
        $this->RequestMapping['general'] = array('function' => 'general');
        $this->AuthMapping['general'] = array('function' => 'isAdmin');
        $this->RequestMapping['general.update'] = array('function' => 'generalUpdate');
        $this->AuthMapping['general.update'] = array('function' => 'isAdmin');

        $this->RequestMapping['group'] = array('function' => 'group');
        $this->AuthMapping['group'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.new'] = array('function' => 'groupNew');
        $this->AuthMapping['group.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.update'] = array('function' => 'groupUpdate');
        $this->AuthMapping['group.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.delete'] = array('function' => 'groupDelete');
        $this->AuthMapping['group.delete'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.edit'] = array('function' => 'groupEdit');
        $this->AuthMapping['group.edit'] = array('function' => 'isAdmin');
        $this->RequestMapping['groupMember'] = array('function' => 'groupMember');
        $this->AuthMapping['groupMember'] = array('function' => 'isAdmin');
        $this->RequestMapping['groupMemberDelete'] = array('function' => 'groupMemberDelete');
        $this->AuthMapping['groupMemberDelete'] = array('function' => 'isAdmin');
        $this->RequestMapping['groupMemberAssignAdmin'] = array('function' => 'groupMemberAssignAdmin');
        $this->AuthMapping['groupMemberAssignAdmin'] = array('function' => 'isAdmin');
        $this->RequestMapping['groupAddMember'] = array('function' => 'groupAddMember');
        $this->AuthMapping['groupAddMember'] = array('function' => 'isAdmin');
        $this->RequestMapping['groupAddMember.update'] = array('function' => 'groupAddMemberUpdate');
        $this->AuthMapping['groupAddMember.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.item'] = array('function' => 'groupItem');
        $this->AuthMapping['group.item'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.budget'] = array('function' => 'groupBudget');
        $this->AuthMapping['group.budget'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.import_eInv'] = array('function' => 'importGroupFromEInventory');
        $this->AuthMapping['group.import_eInv'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.update_step2_import_group_eInv'] = array('function' => 'showGroupImportFrInvConfirmation');
        $this->AuthMapping['group.update_step2_import_group_eInv'] = array('function' => 'isAdmin');
        $this->RequestMapping['group.update_step3_import_group_eInv'] = array('function' => 'importInvGroup');
        $this->AuthMapping['group.update_step3_import_group_eInv'] = array('function' => 'isAdmin');

        $this->RequestMapping['supplier'] = array('function' => 'supplier');
        $this->AuthMapping['supplier'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.new'] = array('function' => 'supplierNew');
        $this->AuthMapping['supplier.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.update'] = array('function' => 'supplierUpdate');
        $this->AuthMapping['supplier.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.edit'] = array('function' => 'supplierEdit');
        $this->AuthMapping['supplier.edit'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.delete'] = array('function' => 'supplierDelete');
        $this->AuthMapping['supplier.delete'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.activate'] = array('function' => 'supplierActivate');
        $this->AuthMapping['supplier.activate'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.inactivate'] = array('function' => 'supplierInactivate');
        $this->AuthMapping['supplier.inactivate'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType'] = array('function' => 'supplierType');
        $this->AuthMapping['supplierType'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType.new'] = array('function' => 'supplierTypeNew');
        $this->AuthMapping['supplierType.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierTypeTb.new'] = array('function' => 'supplierTypeNewTb');
        $this->AuthMapping['supplierTypeTb.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType.update'] = array('function' => 'supplierTypeUpdate');
        // change permission becoz of $sys_custom['ePCM']['enableGroupSupplierType']
        if ($_PAGE['libPCM']->enableGroupSupplierType()) {
            $this->AuthMapping['supplierType.update'] = array('function' => 'allUser');
        } else {
            $this->AuthMapping['supplierType.update'] = array('function' => 'isAdmin');
        }
        $this->RequestMapping['supplierType.edit'] = array('function' => 'supplierTypeEdit');
        $this->AuthMapping['supplierType.edit'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType.delete'] = array('function' => 'supplierTypeDelete');
        $this->AuthMapping['supplierType.delete'] = array('function' => 'isAdmin');

        $this->RequestMapping['category'] = array('function' => 'category');
        $this->AuthMapping['category'] = array('function' => 'isAdmin');
        $this->RequestMapping['category.new'] = array('function' => 'categoryNew');
        $this->AuthMapping['category.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['category.update'] = array('function' => 'categoryUpdate');
        $this->AuthMapping['category.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['category.delete'] = array('function' => 'categoryDelete');
        $this->AuthMapping['category.delete'] = array('function' => 'isAdmin');
        $this->RequestMapping['category.edit'] = array('function' => 'categoryEdit');
        $this->AuthMapping['category.edit'] = array('function' => 'isAdmin');

        $this->RequestMapping['role'] = array('function' => 'role');
        $this->AuthMapping['role'] = array('function' => 'isAdmin');
        $this->RequestMapping['role.new'] = array('function' => 'roleNew');
        $this->AuthMapping['role.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['role.update'] = array('function' => 'roleUpdate');
        $this->AuthMapping['role.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['role.delete'] = array('function' => 'roleDelete');
        $this->AuthMapping['role.delete'] = array('function' => 'isAdmin');
        $this->RequestMapping['role.edit'] = array('function' => 'roleEdit');
        $this->AuthMapping['role.edit'] = array('function' => 'isAdmin');
        $this->RequestMapping['roleMember'] = array('function' => 'roleMember');
        $this->AuthMapping['roleMember'] = array('function' => 'isAdmin');
        $this->RequestMapping['roleMemberDelete'] = array('function' => 'roleMemberDelete');
        $this->AuthMapping['roleMemberDelete'] = array('function' => 'isAdmin');
        $this->RequestMapping['roleAddMember'] = array('function' => 'roleAddMember');
        $this->AuthMapping['roleAddMember'] = array('function' => 'isAdmin');
        $this->RequestMapping['roleAddMember.update'] = array('function' => 'roleAddMemberUpdate');
        $this->AuthMapping['roleAddMember.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['roleAddMember.copyFromOtherYear'] = array('function' => 'RoleCopyFromOtherYear');
        $this->AuthMapping['roleAddMember.copyFromOtherYear'] = array('function' => 'isAdmin');
        $this->RequestMapping['roleAddMember.RoleUpdateCopyFromOtherYear'] = array('function' => 'RoleUpdateCopyFromOtherYear');
        $this->AuthMapping['roleAddMember.RoleUpdateCopyFromOtherYear'] = array('function' => 'isAdmin');


        $this->RequestMapping['rule'] = array('function' => 'rule');
        $this->AuthMapping['rule'] = array('function' => 'isAdmin');
        $this->RequestMapping['rule.new'] = array('function' => 'ruleNew');
        $this->AuthMapping['rule.new'] = array('function' => 'isAdmin');
        $this->RequestMapping['rule.update'] = array('function' => 'ruleUpdate');
        $this->AuthMapping['rule.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['rule.delete'] = array('function' => 'ruleDelete');
        $this->AuthMapping['rule.delete'] = array('function' => 'isAdmin');
        $this->RequestMapping['rule.edit'] = array('function' => 'ruleEdit');
        $this->AuthMapping['rule.edit'] = array('function' => 'isAdmin');

        $this->RequestMapping['securityupdate'] = array('function' => 'securityUpdate');
        $this->AuthMapping['securityupdate'] = array('function' => 'isAdmin');
        $this->RequestMapping['security'] = array('function' => 'security');
        $this->AuthMapping['security'] = array('function' => 'isAdmin');

        $this->RequestMapping['alert'] = array('function' => 'notification');
        $this->AuthMapping['alert'] = array('function' => 'isAdmin');

        $this->RequestMapping['template'] = array('function' => 'template');
        $this->AuthMapping['template'] = array('function' => 'isAdmin');
        $this->RequestMapping['template.upload'] = array('function' => 'templateUpload');
        $this->AuthMapping['template.upload'] = array('function' => 'isAdmin');
        $this->RequestMapping['template.update'] = array('function' => 'templateUploadUpdate');
        $this->AuthMapping['template.update'] = array('function' => 'isAdmin');
        $this->RequestMapping['template.ruleTableUpdate'] = array('function' => 'ruleTemplateUploadTableUpdate');
        $this->AuthMapping['template.ruleTableUpdate'] = array('function' => 'isAdmin');

        #### Import
        $this->RequestMapping['supplier.import'] = array('function' => 'importSupplier');
        $this->AuthMapping['supplier.import'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.importUpdate'] = array('function' => 'importUpdateSupplier');
        $this->AuthMapping['supplier.importUpdate'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplier.importUpdateResult'] = array('function' => 'importUpdateResultSupplier');
        $this->AuthMapping['supplier.importUpdateResult'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType.import'] = array('function' => 'importSupplierType');
        $this->AuthMapping['supplierType.import'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType.importUpdate'] = array('function' => 'importUpdateSupplierType');
        $this->AuthMapping['supplierType.importUpdate'] = array('function' => 'isAdmin');
        $this->RequestMapping['supplierType.importUpdateResult'] = array('function' => 'importUpdateResultSupplierType');
        $this->AuthMapping['supplierType.importUpdateResult'] = array('function' => 'isAdmin');

        #### Export
        $this->RequestMapping['supplier.export'] = array('function' => 'exportSupplier');
        $this->AuthMapping['supplier.export'] = array('function' => 'isAdmin');

        ## fund mgmt prototype by Omas
        $this->RequestMapping['funding'] = array('function' => 'funding');
        $this->AuthMapping['funding'] = array('function' => 'isAdmin');
        $this->RequestMapping['funding.categoryUpdate'] = array('function' => 'updateFundingCategory');
        $this->AuthMapping['funding.categoryUpdate'] = array('function' => 'isAdmin');
        $this->RequestMapping['funding.categoryDelete'] = array('function' => 'deleteFundingCategory');
        $this->AuthMapping['funding.categoryDelete'] = array('function' => 'isAdmin');
        $this->RequestMapping['funding.fundingUpdate'] = array('function' => 'updateFunding');
        $this->AuthMapping['funding.fundingUpdate'] = array('function' => 'isAdmin');
        $this->RequestMapping['funding.fundingDelete'] = array('function' => 'deleteFunding');
        $this->AuthMapping['funding.fundingDelete'] = array('function' => 'isAdmin');

        ## import validate ##
        $this->RequestMapping['import.template'] = array('function' => 'importTemplate');
        $this->AuthMapping['import.template'] = array('function' => 'isAdmin');
        $this->RequestMapping['import.validate'] = array('function' => 'importValidate');
        $this->AuthMapping['import.validate'] = array('function' => 'isAdmin');
        $this->RequestMapping['import.loadref'] = array('function' => 'ajaxImportRef');
        $this->AuthMapping['import.loadref'] = array('function' => 'isAdmin');
        $this->RequestMapping['import.step1'] = array('function' => 'importStep1');
        $this->AuthMapping['import.step1'] = array('function' => 'isAdmin');
        $this->RequestMapping['import.step2'] = array('function' => 'importStep2');
        $this->AuthMapping['import.step2'] = array('function' => 'isAdmin');
        $this->RequestMapping['import.step3'] = array('function' => 'importStep3');
        $this->AuthMapping['import.step3'] = array('function' => 'isAdmin');
        $this->RequestMapping['export'] = array('function' => 'exportData');
        $this->AuthMapping['export'] = array('function' => 'isAdmin');
    }

    public function index()
    {
        $this->sendRedirect("index.php?p=setting.general");
    }

    public function groupAddMember()
    {
        global $_PAGE;
        $postVar = $_PAGE['POST'];
// 		$groupID = $postVar['groupID'];

        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');

        $_PAGE['POST']['type'] = "group";

        //$view = $ui->Get_Add_Group_Member_Form($groupID);
        $main = '';
        $view = $this->getViewContent('/template/add_user.php');
        echo $view;
    }

    public function roleAddMember()
    {
        global $_PAGE;
        $postVar = $_PAGE['POST'];

        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');

        $_PAGE['POST']['type'] = "role";

        $main = '';
        $view = $this->getViewContent('/template/add_user.php');
        echo $view;
    }

    public function roleAddMemberUpdate()
    {
        global $_PAGE;
        $postVar = $_PAGE['POST'];
        $userIDAry = $postVar['userIDAry'];
        $userIDAry = explode(',', $userIDAry);
        $roleID = $postVar['roleID'];
        $roleYearID = $postVar["academicYearId"];

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->insertRoleMember($userIDAry, $roleID, $roleYearID);
    }

    public function groupAddMemberUpdate()
    {
        global $_PAGE;
        $postVar = $_PAGE['POST'];
        $userIDAry = $postVar['userIDAry'];
        $userIDAry = explode(',', $userIDAry);
        $groupID = $postVar['groupID'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->insertGroupMember($userIDAry, $groupID);
    }

    public function general()
    {
        global $_PAGE, $intranet_root,$plugin;

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'General';

        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $generalSettings = $settingObj->getGeneralSettings();

        $_PAGE['views_data']['general_settings'] = $generalSettings;
// Omas 20160928
//     	include $intranet_root.'/includes/settings.php';
        $_PAGE['allow_send_app'] = false;
        if (isset($plugin['eClassTeacherApp']) && $plugin['eClassTeacherApp'] === true) {
            $_PAGE['allow_send_app'] = true;
        }

        $main = $this->getViewContent('/setting/general.php');
        $view = $this->getLayout($main, '', '/template/setting_form.php');
        echo $view;
    }

    public function generalUpdate()
    {
        global $_PAGE, $PATH_WRT_ROOT;
        //Get Post val
        $generalSettingsUpdateAry = $_PAGE['POST']['generalSettings'];
        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $settingObj->updateGeneralSettings($generalSettingsUpdateAry);

        //Back to General Settings
        $this->sendRedirect("index.php?p=setting.general");

    }


    ####################################################################
    ####	Group
    ####################################################################
    public function group($params = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';


        switch ($params) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params;
                break;
        }

        $keyword = $_PAGE['POST']['keyword'];

        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');


        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');


        $settings = $db->getGeneralSettings();
        $academicYearID = $settings['defaultAcademicYearID'];
        $acYAry = BuildMultiKeyAssoc(GetAllAcademicYearInfo(), "AcademicYearID");
        $academicYear = $acYAry[$academicYearID][Get_Lang_Selection('YearNameB5', 'YearNameEN')];

        $sql = $db->getGroupSqlForDbTable($keyword);

        $_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['sql'] = $sql;
        $_PAGE['views_data']['keyword'] = $keyword;
        $_PAGE['views_data']['academicYear'] = $academicYear;

        $main = $this->getViewContent('/setting/group.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    function groupNew($groupID = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';

        if ($groupID != '') {
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
            $returnInfoAry = $db->getGroupInfo($groupID);
            $groupInfo = $returnInfoAry[0];
        }

        $_PAGE['views_data']['groupInfo'] = $groupInfo;
        $main = $this->getViewContent('/setting/group_new.php');
        $view = $this->getLayout($main, '', '/template/form.php');

        echo $view;
    }

    function groupUpdate()
    {
        global $_PAGE;

        $PostVar = $_PAGE['POST'];
        $groupID = $PostVar['groupID'];
        $groupName = $PostVar['groupName'];
        $groupNameChi = $PostVar['groupNameChi'];
        $code = $PostVar['code'];


        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->saveGroup($groupID, $code, $groupName, $groupNameChi);

        if ($groupID == '') {
            $this->sendRedirect("index.php?p=setting.group.AddSuccess");
        } else {
            $this->sendRedirect("index.php?p=setting.group.UpdateSuccess");
        }

    }

    function groupDelete()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $groupIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $db->deleteGroup($groupIdAry);

        $this->sendRedirect("index.php?p=setting.group.DeleteSuccess");

    }

    function groupEdit()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $groupId = $PostVar['targetIdAry'][0];
        $this->groupNew($groupId);
    }

    function groupMember($params = '', $params2 = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';
        switch ($params2) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params2;
                break;
        }

        $groupID = $params; //temp

        if ($groupID == '') {
            echo 'Error';

        } else {
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
            $returnInfoAry = $db->getGroupInfo($groupID);
            $groupInfo = $returnInfoAry[0];
            $_PAGE['views_data']['groupInfo'] = $groupInfo;
        }

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $groupMemberInfo = $db->getGroupMemberInfo($groupID);
        $sql = $db->getGroupMemberSqlForDbTable($groupID);

        $_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['sql'] = $sql;
        $_PAGE['views_data']['groupMemberInfo'] = $groupMemberInfo;

        $main = $this->getViewContent('/setting/group_member.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    function groupMemberDelete($groupID = '')
    {
        global $_PAGE;
//		debug_pr($groupID);
        $PostVar = $_PAGE['POST'];
        $groupUserIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->deleteGroupMember($groupUserIdAry);

        $this->sendRedirect("index.php?p=setting.groupMember." . $groupID . ".DeleteSuccess");

    }

    function groupMemberAssignAdmin($groupID, $setAdmin)
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $groupUserIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        if ($setAdmin) {
            $db->GroupMemberAssignAdmin($groupUserIdAry, 1);
        } else {
            $db->GroupMemberAssignAdmin($groupUserIdAry, 0);
        }
        $this->sendRedirect("index.php?p=setting.groupMember." . $groupID . ".UpdateSuccess");

    }

    function groupItem($page = '', $action = '', $groupid = '')
    {

        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';

        switch ($action) {
            case 'AddSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateSuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $action;
                break;
        }

        if (is_numeric($page)) {
            $groupID = $page;
        } else {
            if (is_numeric($action)) {
                $groupID = $action;
            } else {
                $groupID = $groupid;
            }
        }
        if ($groupID == '' || $groupID == 0) {
            $groupID = $_PAGE['POST']['groupID'];
        }
        $_PAGE['groupID'] = $groupID;
        // need authentication here !!!!!!!!!!!!!!!!!!!!
        $db = $this->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $view_php = '';
        $frame_php = '';
        $header_location = '';
        if (is_numeric($page)) {
            $rows_group = $db->getGroupInfo(array($groupID));
            $_PAGE['views_data']['group_name'] = $rows_group[0]['GroupName'];
            $_PAGE['views_data']['group_name_chi'] = $rows_group[0]['GroupNameChi'];
            $_SESSION['ePCM']['Group']['current_group_name'] = $rows_group[0]['GroupName'];
            $_SESSION['ePCM']['Group']['current_group_name_chi'] = $rows_group[0]['GroupNameChi'];

            $sql = $db->getFinancialItemSQL($groupID);
            $_PAGE['views_data']['sql'] = $sql;
            $_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
            $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
            $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
            $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
            $_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
            $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];

            $view_php = '/setting/group_item.php';
        } else {
            if ($page == 'new') {
                $_PAGE['views_data']['group_name'] = $_SESSION['ePCM']['Group'][Get_Lang_selection('current_group_name_chi',
                    'current_group_name')];

                $view_php = '/setting/group_item_new.php';
                $frame_php = '/template/form.php';
            } else {
                if ($page == 'edit') {
                    $_PAGE['views_data']['group_name'] = $_SESSION['ePCM']['Group'][Get_Lang_selection('current_group_name_chi',
                        'current_group_name')];

                    $targetID = $_PAGE['POST']['targetIdAry'][0];
                    $_PAGE['view_data']['targetID'] = $targetID;
                    $_PAGE['view_data']['itemInfo'] = BuildMultiKeyAssoc($db->getFinancialItemByItemID($targetID),
                        'ItemID');

                    $view_php = '/setting/group_item_new.php';
                    $frame_php = '/template/form.php';
                } else {
                    if ($page == 'update') {
                        // both new update & edit update here
                        $saveToDbAry['itemName'] = $_PAGE['POST']['itemName'];
                        $saveToDbAry['itemNameChi'] = $_PAGE['POST']['itemNameChi'];
                        $saveToDbAry['code'] = $_PAGE['POST']['code'];
                        $saveToDbAry['groupID'] = $_PAGE['POST']['groupID'];
                        $saveToDbAry['targetID'] = $_PAGE['POST']['targetID'];

                        if ($saveToDbAry['targetID'] > 0) {
                            $result = $db->updateGroupFinancialItem($saveToDbAry);
                            $returnMsg = ($result == 1) ? 'UpdateSuccess' : 'UpdateUnsuccess';
                        } else {
                            $result = $db->insertGroupFinancialItem($saveToDbAry);
                            $returnMsg = ($result == 1) ? 'AddSuccess' : 'AddUnsuccess';
                        }
                        $header_location = "index.php?p=setting.group.item.$groupID.$returnMsg";
                    } else {
                        if ($page == 'delete') {
                            //$groupID
                            $saveToDbAry['TargetArr'] = $_PAGE['POST']['targetIdAry'];

                            $result = $db->deleteFinancialGroupItem($saveToDbAry);
                            $returnMsg = ($result == 1) ? 'DeleteSuccess' : 'DeleteUnsuccess';
                            $header_location = "index.php?p=setting.group.item.$groupID.$returnMsg";
                        } else {
                            debug_pr('get to here');
                            //$header_location = "index.php?p=setting.group";
                        }
                    }
                }
            }
        }

        if ($view_php != '') {
            $main = $this->getViewContent($view_php);
            $view = $this->getLayout($main, '', $frame_php);
            echo $view;
        } else {
            $this->sendRedirect($header_location);
        }
    }

    function groupBudget($page = '', $action = '', $groupid = '')
    {

        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';

        // need authentication here !!!!!!!!!!!!!!!!!!!!
        $view_php = '';
        $template_php = '';

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $settings = $db->getGeneralSettings();
        $academicYearID = $settings['defaultAcademicYearID'];
        $_PAGE['view_data']['academicYearID'] = $academicYearID;

        if (is_numeric($page)) {
            $_PAGE['rows_group_budget'] = $db->getGroupBudget(array($page), $academicYearID);
            $_PAGE['rows_group_financial_item'] = $db->getFinancialItemByGroup($page);
            $_PAGE['fundingSource'] = $db->getFundingSourceTableData();

            if (count($_PAGE['rows_group_budget']) > 0) {
                $_PAGE['rows_group_item_budget'] = $db->getGroupFinancialItemBudgetByGroupId($_PAGE['rows_group_budget'][0]['GroupID'],
                    $_PAGE['rows_group_budget'][0]['AcademicYearID'], $_PAGE['rows_group_budget'][0]['TermID']);
                $_PAGE['mode'] = 'edit';
            } else {
                $_PAGE['GroupID'] = $page;
                $_PAGE['mode'] = 'new';
            }
            $_PAGE['rows_group'] = $db->getGroupInfo($page);

            $view_php = '/setting/group_itembudget_new.php';
            $frame_php = '/template/form.php';
        } else {
            if ($page == 'new') {

                $rows_budget = array(
                    array(
                        'GroupID' => $_PAGE['POST']['GroupID'],
                        'AcademicYearID' => $_PAGE['POST']['generalSettings']['defaultAcademicYearID'],
                        'TermID' => '',
                        'Budget' => $_PAGE['POST']['gBudget'],
                    ),
                );
                $db->saveGroupBudget($rows_budget);

                $rows_item_budget = array();
                $budget_index_prefix = 'Budget_';
                $item_budget_id_index_prefix = 'ItemBudgetID_';
                foreach ((array)$_PAGE['POST'] as $key => $value) {
                    if (strpos($key, $budget_index_prefix) === 0) {
                        $item_id = str_replace($budget_index_prefix, '', $key);
                        array_push($rows_item_budget, array(
                            'ItemBudgetID' => $_PAGE['POST'][$item_budget_id_index_prefix . $item_id],
                            'ItemID' => $item_id,
                            'AcademicYearID' => $_PAGE['POST']['generalSettings']['defaultAcademicYearID'],
                            'TermID' => '',
                            'Budget' => $value,
                            'FundingID' => $_PAGE['POST']['funding'][$item_id]
                        ));
                    }
                }
                $db->saveGroupFinancialItemBudget($rows_item_budget);

                $header_location = 'index.php?p=setting.group.AddSuccess';
            } else {
                if ($page == 'edit') {
// 			$db->debugMode = false;

                    $rows_budget = array(
                        array(
                            'GroupBudgetID' => $_PAGE['POST']['GroupBudgetID'],
                            'GroupID' => $_PAGE['POST']['GroupID'],
                            'AcademicYearID' => $_PAGE['POST']['generalSettings']['defaultAcademicYearID'],
                            'TermID' => '',
                            'Budget' => $_PAGE['POST']['gBudget'],
                        ),
                    );
                    //print_r($rows_budget);
                    $db->saveGroupBudget($rows_budget);

                    $rows_item_budget = array();
                    $budget_index_prefix = 'Budget_';
                    $item_budget_id_index_prefix = 'ItemBudgetID_';
                    foreach ((array)$_PAGE['POST'] as $key => $value) {
                        if (strpos($key, $budget_index_prefix) === 0) {
                            $item_id = str_replace($budget_index_prefix, '', $key);
                            array_push($rows_item_budget, array(
                                'ItemBudgetID' => $_PAGE['POST'][$item_budget_id_index_prefix . $item_id],
                                'ItemID' => $item_id,
                                'AcademicYearID' => $_PAGE['POST']['generalSettings']['defaultAcademicYearID'],
                                'TermID' => '',
                                'Budget' => $value,
                                'FundingID' => $_PAGE['POST']['funding'][$item_id],
                            ));
                        }
                    }
                    //print_r($rows_item_budget);
                    $db->saveGroupFinancialItemBudget($rows_item_budget);

                    $header_location = 'index.php?p=setting.group.UpdateSuccess';
                } else {
                    if ($page == 'delete') {
                    }
                }
            }
        }
//		debug_pr('this page :'.$page);
//		debug_pr('this action:'.$action);

        if ($view_php != '') {
            $main = $this->getViewContent($view_php);
            $view = $this->getLayout($main, '', $frame_php);
            echo $view;
        } else {
            $this->sendRedirect($header_location);
        }

    }

    ####################################################################
    ####	Supplier
    ####################################################################
    /*
	 *
	function security($tab=''){
		global $_PAGE;
		$_PAGE['CurrentSection'] = 'Settings';
		$_PAGE['CurrentPage'] = 'Security';

		$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
		$ui = $_PAGE['Controller']->requestObject('libPCM_ui','includes/ePCM/libPCM_ui.php');

		$path='/setting/security_authentication.php';
		if($tab != ''){
			$tabParmArr = explode('.',$tab);
			$_PAGE['curTab'] = $tabParmArr[0];
			$_PAGE['curTab'] = $tab;
		}else{
			$_PAGE['curTab']='authentication';
		}

		if($_PAGE['curTab'] == 'authentication' ){
			//get general setting
			$obj_setting=$_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
			$result=$obj_setting->getGeneralSettings();
			$_PAGE['AccessSetting']=$result['AccessSetting'];
		}else if($_PAGE['curTab'] == 'network' ){
			$path='/setting/security_network.php';

			$obj_setting=$_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
			$result=$obj_setting->getGeneralSettings();
			$_PAGE['IPList']=$result['IPList'];
		}

		$main = $this->getViewContent($path);
		$view = $this->getLayout($main,'','/template/setting_form.php');
		echo $view;
	}
	 *
	 */
    public function supplier($params = '')
    {
        global $_PAGE, $Lang;
        error_reporting(0);
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';

        switch ($params) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params;
                break;
            case 'ActivateSuccess' :
            case 'InactivateSuccess' :
                $_PAGE['returnMsgKey'] = 'UpdateSuccess';
                break;
        }
        $_PAGE['curTab'] = 'supplier';

        $typeID = $_PAGE['POST']['typeID'];
        $isActive = $_PAGE['POST']['isActive'];
        $isActive = (!isset($_POST['isActive']) || $isActive == '') ? 1 : $isActive;
        $isActive = ($isActive == 'A' ? '' : $isActive);
        $keyword = $_PAGE['POST']['keyword'];

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        //$subjectGroupInfoAry = $settingObj->getSubjectGroupInfo();

        ### Supply Type Filter
// 		$_PAGE['supplyTypeInfoAry'] = $db->getSupplyTypeInfo();
        $supplyTypeInfoAry = $db->getSupplyTypeInfo();
        $supplierTypeAry = array();
        foreach ($supplyTypeInfoAry as $_suppliertype) {
            if ($_PAGE['libPCM']->enableGroupSupplierType()) {
                // flag enable feature: group created supplier type
                $relatedGroupID = $_suppliertype['GroupMappingID'];
                if ($relatedGroupID > 0) {
                    // group created type
                    $relatedGroupInfo = $db->getGroupInfo($relatedGroupID);
                    $relatedGroupInfo = $relatedGroupInfo[0];
                    $groupName = $Lang['ePCM']['Mgmt']['Case']['Group'] . ' (' . Get_Lang_Selection($relatedGroupInfo['GroupNameChi'],
                            $relatedGroupInfo['GroupName']) . ') ' . $Lang['ePCM']['Mgmt']['Case']['Supplier']['GroupType'];

                } else {
                    // school created type
                    $groupName = $Lang['ePCM']['Mgmt']['Case']['Supplier']['SchoolType'];
                }
                $supplierTypeAry[$groupName][$_suppliertype['TypeID']] = Get_Lang_Selection($_suppliertype['TypeNameChi'],
                    $_suppliertype['TypeName']);
            } else {
                // 		// normal case
                $supplierTypeAry[$_suppliertype['TypeID']] = Get_Lang_Selection($_suppliertype['TypeNameChi'],
                    $_suppliertype['TypeName']);
            }
        }
        $_PAGE['supplyTypeInfoAry'] = $supplierTypeAry;

        $sql = $db->getSupplierSqlForDbTable($typeID, $isActive, $keyword, $excludedSupplierAry = '', $returnSql = 1,
            $additionSelectStatement = "", $withLinks = true);

        $_PAGE['views_data']['typeID'] = $typeID;
        $_PAGE['views_data']['isActive'] = $isActive;
        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['sql'] = $sql;
        $_PAGE['views_data']['filter'] = $filter;
        $_PAGE['views_data']['keyword'] = $keyword;

        $main = $this->getViewContent('/setting/supplier.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    public function supplierNew($supplier_id = '')
    {
        global $_PAGE, $intranet_session_language;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';
        $_PAGE['curTab'] = 'supplier';

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $_PAGE['views_data']['supplier_type'] = $db->getSupplierType();

        if ($supplier_id != '') {
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
            $returnInfoAry = $db->getSupplierInfo($supplier_id);
            $supplierInfo = $returnInfoAry;
            $_PAGE['mode'] = 'edit';
        } else {
            $_PAGE['mode'] = 'new';
        }

        $_PAGE['views_data']['supplierInfo'] = $supplierInfo;
        $main = $this->getViewContent('/setting/supplier_new.php');
// 		$view = $this->getLayout($main, '', '/template/setting_form.php');
// 		should use form.php for this page
        $view = $this->getLayout($main, '', '/template/form.php');

        echo $view;
    }

    public function supplierUpdate()
    {
        global $_PAGE;
        $value_array = array(
            'SupplierID' => $_PAGE['POST']['SupplierID'],
            'SupplierName' => $_PAGE['POST']['SupplierName'],
            'SupplierNameChi' => $_PAGE['POST']['SupplierNameChi'],
            'Website' => $_PAGE['POST']['Website'],
            'Description' => $_PAGE['POST']['Description'],
            'Phone' => $_PAGE['POST']['Phone'],
            'Fax' => $_PAGE['POST']['Fax'],
            'Email' => $_PAGE['POST']['Email'],
            'Type' => $_PAGE['POST']['Type'],
            'IsActive' => $_PAGE['POST']['IsActive'],
        );

        $contact_array = array();
        if (count($_PAGE['POST']['cContactName']) > 0) {
            for ($i = 0; $i < count($_PAGE['POST']['cContactName']); $i++) {
                $contact_array[$i]['cContactID'] = $_PAGE['POST']['cContactID'][$i];
                $contact_array[$i]['cContactName'] = $_PAGE['POST']['cContactName'][$i];
                $contact_array[$i]['cPosition'] = $_PAGE['POST']['cPosition'][$i];
                $contact_array[$i]['cPhone'] = $_PAGE['POST']['cPhone'][$i];
                $contact_array[$i]['cEmail'] = $_PAGE['POST']['cEmail'][$i];
                $contact_array[$i]['cDescription'] = $_PAGE['POST']['cDescription'][$i];
            }
        }

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->saveSupplier($value_array, $contact_array);

        if ($_PAGE['POST']['SupplierID'] == '') {
            $this->sendRedirect("index.php?p=setting.supplier.AddSuccess");
        } else {
            $this->sendRedirect("index.php?p=setting.supplier.UpdateSuccess");
        }

    }

    function supplierEdit()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $supplierId = $PostVar['targetIdAry'][0];
        $this->supplierNew($supplierId);
    }

    function supplierDelete()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $supplierIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->deleteSupplier($supplierIdAry);

        $this->sendRedirect("index.php?p=setting.supplier.DeleteSuccess");

    }

    function supplierActivate()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $supplierIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->activateSupplier($supplierIdAry);

        $this->sendRedirect("index.php?p=setting.supplier.ActivateSuccess");
    }

    function supplierInactivate()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $supplierIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->inactivateSupplier($supplierIdAry);

        $this->sendRedirect("index.php?p=setting.supplier.InactivateSuccess");
    }

    public function supplierTypeNewTb($supplier_type_id = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';

        if ($supplier_type_id != '') {

        } else {
            $_PAGE['mode'] = 'new';
        }

        $_PAGE['views_data']['supplierInfo'] = $supplierInfo;
        $main = $this->getViewContent('/setting/tb_supplier_type_new.php');
        $view = $this->getLayout($main, '', '', '/tb.php');

        echo $view;

    }

    public function supplierType($params = '')
    {
        global $_PAGE;
        error_reporting(0);
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';

        switch ($params) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params;
                break;
            case 'ActivateSuccess' :
            case 'InactivateSuccess' :
                $_PAGE['returnMsgKey'] = 'UpdateSuccess';
                break;
        }
        $_PAGE['curTab'] = 'supplierType';

        $keyword = $_PAGE['POST']['keyword'];

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        //$subjectGroupInfoAry = $settingObj->getSubjectGroupInfo();

        $sql = $db->getSupplierTypeSqlForDbTable($keyword);

        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['sql'] = $sql;
        $_PAGE['views_data']['keyword'] = $keyword;

        $main = $this->getViewContent('/setting/supplier_type.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    public function supplierTypeNew($supplier_type_id = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';
        $_PAGE['curTab'] = 'supplierType';
        if ($supplier_type_id != '') {
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
            $returnInfoAry = $db->getSupplierTypeInfo($supplier_type_id);
            $supplierTypeInfo = $returnInfoAry;
            $_PAGE['mode'] = 'edit';
        } else {
            $_PAGE['mode'] = 'new';
        }

        $_PAGE['views_data']['supplierTypeInfo'] = $supplierTypeInfo;
        $main = $this->getViewContent('/setting/supplier_type_new.php');
        $view = $this->getLayout($main, '', '/template/form.php');

        echo $view;

    }

    public function supplierTypeUpdate($redirect = 1, $statusOnly = 0)
    {
        global $_PAGE;

        $value_array = array(
            'TypeID' => $_PAGE['POST']['TypeID'],
            'TypeName' => $_PAGE['POST']['TypeName'],
            'TypeNameChi' => $_PAGE['POST']['TypeNameChi'],
            'GroupMappingID' => $_PAGE['POST']['GroupMappingID'],
        );

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->saveSupplierType($value_array);

        if ($redirect) {
            if ($_PAGE['POST']['TypeID'] == '') {
                $this->sendRedirect("index.php?p=setting.supplierType.AddSuccess");
            } else {
                $this->sendRedirect("index.php?p=setting.supplierType.UpdateSuccess");
            }
        } else {
            if ($statusOnly) {
                echo 'SUCCESS';
            } else {
                echo 'Saved!=!' . $_PAGE['POST'][Get_Lang_Selection('TypeNameChi', 'TypeName')];
            }

        }
    }

    public function supplierTypeEdit()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $supplierTypeId = $PostVar['targetIdAry'][0];
        $this->supplierTypeNew($supplierTypeId);
    }

    public function supplierTypeDelete()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $supplierIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->deleteSupplierType($supplierIdAry);

        $this->sendRedirect("index.php?p=setting.supplierType.DeleteSuccess");
    }

    ####################################################################
    ####	Category
    ####################################################################
    public function category($params = '')
    {
        global $_PAGE;

        switch ($params) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params;
                break;
        }

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Category';

        $keyword = $_PAGE['POST']['keyword'];


        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
//		$settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');


        $sql = $db->getCategorySqlForDbTable($keyword);

        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['sql'] = $sql;

        $_PAGE['views_data']['keyword'] = $keyword;

        $main = $this->getViewContent('/setting/category.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    function categoryNew($categoryID = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Category';

        if ($categoryID != '') {
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
            $returnInfoAry = $db->getCategoryInfo($categoryID);
            $categoryInfo = $returnInfoAry[0];
        }

        $_PAGE['views_data']['categoryInfo'] = $categoryInfo;
        $main = $this->getViewContent('/setting/category_new.php');
        $view = $this->getLayout($main, '', '/template/form.php');

        echo $view;
    }

    function categoryUpdate()
    {
        global $_PAGE;
//		debug_pr($_PAGE['POST']);
        $PostVar = $_PAGE['POST'];


        $categoryID = $PostVar['categoryID'];
        $categoryName = $PostVar['categoryName'];
        $categoryNameChi = $PostVar['categoryNameChi'];
        $code = $PostVar['code'];


        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->saveCategory($categoryID, $code, $categoryName, $categoryNameChi);

        if ($categoryID == '') {
            $this->sendRedirect("index.php?p=setting.category.AddSuccess");
        } else {
            $this->sendRedirect("index.php?p=setting.category.UpdateSuccess");
        }

    }

    function categoryDelete()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $catIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->deleteCategory($catIdAry);

        $this->sendRedirect("index.php?p=setting.category.DeleteSuccess");

    }

    function categoryEdit()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $catId = $PostVar['targetIdAry'][0];
        $this->categoryNew($catId);
    }
    ####################################################################
    ####	Role
    ####################################################################
    function role($params = '')
    {
        global $_PAGE;

        switch ($params) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params;
                break;
        }

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Role';

        $keyword = $_PAGE['POST']['keyword'];


        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        $roleYearId = $_PAGE['GET']['academicYearId'];
        if (empty($roleYearId)) {
            $roleYearId = $_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
        }

        $sql = $db->getRoleSqlForDbTable($keyword, $roleYearId);


        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['sql'] = $sql;
        $_PAGE['views_data']['keyword'] = $keyword;

        $main = $this->getViewContent('/setting/role.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    function roleNew($roleId = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Role';

        if ($roleId != '') {
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
            $returnInfoAry = $db->getRoleInfo($roleId);
            $roleInfo = $returnInfoAry[0];
        }


        $_PAGE['views_data']['roleInfo'] = $roleInfo;
        $main = $this->getViewContent('/setting/role_new.php');
        $view = $this->getLayout($main, '', '/template/form.php');

        echo $view;
    }

    public function RoleCopyFromOtherYear()
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Role';
        $postVar = $_PAGE['POST'];
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        $main = $this->getViewContent('/setting/role_member_copy_from_other_year.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    public function RoleUpdateCopyFromOtherYear()
    {
        global $_PAGE;
        $postVar = $_PAGE['POST'];
        $roleYearIDFrom = $postVar["roleAcademicYearIdFrom"];
        $roleYearIDTo = $postVar["roleAcademicYearIdTo"];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->copyRoleMemberFromYear($roleYearIDFrom, $roleYearIDTo);
        $this->sendRedirect("index.php?p=setting.role.UpdateSuccess&&academicYearId=" . $roleYearIDTo);
    }

    function roleUpdate()
    {
        global $_PAGE;

        $PostVar = $_PAGE['POST'];
        $roleID = $PostVar['roleID'];
        $roleName = $PostVar['roleName'];
        $roleNameChi = $PostVar['roleNameChi'];
        $forCaseApproval = $PostVar['forCaseApproval'];
        $forQuotationApproval = $PostVar['forQuotationApproval'];
        $forTenderAuditing = $PostVar['forTenderAuditing'];
        $forTenderApproval = $PostVar['forTenderApproval'];

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->saveRole($roleID, $roleName, 0, $forCaseApproval, $forQuotationApproval, $forTenderAuditing,
            $forTenderApproval, $roleNameChi);

        if ($roleID == '') {
            $this->sendRedirect("index.php?p=setting.role.AddSuccess");
        } else {
            $this->sendRedirect("index.php?p=setting.role.UpdateSuccess");
        }
    }

    function roleDelete()
    {
        global $_PAGE;

        $PostVar = $_PAGE['POST'];
        $roleIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->deleteRole($roleIdAry);

        $this->sendRedirect("index.php?p=setting.role.DeleteSuccess");

    }

    function roleEdit()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $roleId = $PostVar['targetIdAry'][0];

        $this->roleNew($roleId);

    }

    function roleMember($roleID = '', $params2 = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Role';

//		debug_pr($groupID);
        switch ($params2) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params2;
                break;
        }
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        if ($roleID == '') {
            echo 'Error';
            die();
        } else {

            $returnInfoAry = $db->getRoleInfo($roleID);
            $roleInfo = $returnInfoAry[0];
            $_PAGE['views_data']['roleInfo'] = $roleInfo;
        }

        $roleYearId = $_GET["academicYearId"];
        if (empty($roleYearId)) {
            $roleYearId = $_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
        }

        $roleMemberInfo = $db->getRoleUser($roleID, $roleYearId);
        $sql = $db->getRoleUserSqlDbTable($roleID, $roleYearId);
        $_PAGE['views_data']['num_per_page'] = $_PAGE['POST']['num_per_page'];
        $_PAGE['views_data']['pageNo'] = $_PAGE['POST']['pageNo'];
        $_PAGE['views_data']['order'] = $_PAGE['POST']['order'];
        $_PAGE['views_data']['field'] = $_PAGE['POST']['field'];
        $_PAGE['views_data']['page_size_change'] = $_PAGE['POST']['page_size_change'];
        $_PAGE['views_data']['numPerPage'] = $_PAGE['POST']['numPerPage'];
        $_PAGE['views_data']['roleMemberInfo'] = $roleMemberInfo;
        $_PAGE['views_data']['sql'] = $sql;


        $main = $this->getViewContent('/setting/role_member.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    function roleMemberDelete($roleID = '', $roleYearId = '')
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $groupUserIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $roleYearId = $_PAGE['GET']['academicYearId'];
        if (empty($roleYearId)) {
            $roleYearId = $_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];
        }
        $db->deleteRoleMember($groupUserIdAry, $roleYearId);

        $this->sendRedirect("index.php?p=setting.roleMember." . $roleID . ".DeleteSuccess&academicYearId=" . $roleYearId);

    }
    ####################################################################
    ####	Rule
    ####################################################################
    function rule($params = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Rule';

        switch ($params) {
            case 'AddSuccess' :
            case 'UpdateSuccess' :
            case 'AddUnsuccess' :
            case 'UpdateUnsuccess' :
            case 'DeleteSuccess' :
            case 'DeleteUnsuccess' :
                $_PAGE['returnMsgKey'] = $params;
                break;
        }

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');

        $ruleInfo = $db->getRuleTemplateInfo();
        $_PAGE['views_data']['ruleInfo'] = $ruleInfo;

        //check rules is valid? return  invalid array
        $invalidAry = $db->getInvalidAryOfRule($ruleInfo);
        $_PAGE['views_data']['invalidAry'] = $invalidAry;

        $main = $this->getViewContent('/setting/rule.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    function approvalRuleTranslate($jsonString)
    {
        global $_PAGE;
        $json = $_PAGE['Controller']->requestObject('JSON_obj', 'includes/json.php');

        $translatedRuleArr = $json->decode($jsonString);
        return $translatedRuleArr;
    }

    function ruleNew($ruleId = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Rule';


        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        if ($ruleId != '') {
            $returnInfoAry = $db->getRuleInfo($ruleId);
            $ruleInfo = $returnInfoAry[0];
        }


        //get ForCaseApproval, ForQuotationApproval, ForTenderAudit, ForTenderApproval Group
        $forCaseApprovalGroup = $db->getRoleInfo($roleIDAry = '', $ForCaseApproval = '1', $ForQuotationApproval = '',
            $ForTenderAuditing = '', $ForTenderApproval = '', $isDeleted = '0');
        $forQuotationApprovalGroup = $db->getRoleInfo($roleIDAry = '', $ForCaseApproval = '',
            $ForQuotationApproval = '1', $ForTenderAuditing = '', $ForTenderApproval = '', $isDeleted = '0');
        $forTenderAuditingGroup = $db->getRoleInfo($roleIDAry = '', $ForCaseApproval = '', $ForQuotationApproval = '',
            $ForTenderAuditing = '1', $ForTenderApproval = '', $isDeleted = '0');
        $forTenderApprovalGroup = $db->getRoleInfo($roleIDAry = '', $ForCaseApproval = '', $ForQuotationApproval = '',
            $ForTenderAuditing = '', $ForTenderApproval = '1', $isDeleted = '0');

        //Get isPrincipal Roles
        $principalInfoAry = $db->getRoleInfo($roleIDAry = '', $ForCaseApproval = '', $ForQuotationApproval = '',
            $ForTenderAuditing = '', $ForTenderApproval = '', $isDeleted = '0', $isPrincipal = '1');
        $_PAGE['views_data']['ruleInfo'] = $ruleInfo;
        $_PAGE['views_data']['forCaseApprovalGroup'] = $forCaseApprovalGroup;
        $_PAGE['views_data']['forQuotationApprovalGroup'] = $forQuotationApprovalGroup;
        $_PAGE['views_data']['forTenderAuditingGroup'] = $forTenderAuditingGroup;
        $_PAGE['views_data']['forTenderApprovalGroup'] = $forTenderApprovalGroup;
        $_PAGE['views_data']['principalInfoAry'] = $principalInfoAry;

        $main = $this->getViewContent('/setting/rule_new.php');
        $view = $this->getLayout($main, '', '/template/form.php');

        echo $view;
    }

    function ruleUpdate()
    {
        global $_PAGE, $sys_custom;

        define(QUOTATION_TYPE_NO, 'N');
        define(QUOTATION_TYPE_QUOTATION, 'Q');
        define(QUOTATION_TYPE_TENDER, 'T');

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $PostVar = $_PAGE['POST'];

        $ruleID = $PostVar['ruleID'];
        $code = $PostVar['code'];
        $floorPrice = $PostVar['floorPrice'];
        $cellingPrice = $PostVar['cellingPrice'];
        $approvalRoleID = $PostVar['approvalRoleID'];
//		$needGroupHeadApproval = $PostVar['needGroupHeadApproval'];
        $quotationType = $PostVar['quotationType'];
        $minQuote = $PostVar['minQuote'];
        $quotationApprovalRoleID = $PostVar['quotationApprovalRoleID'];
        $tenderAuditingRoleID = $PostVar['tenderAuditingRoleID'];
        $tenderApprovalRoleID = $PostVar['tenderApprovalRoleID'];
        $isTemplate = 1;// isTemplate is used to identify rules is/not added as template only
        $approvalMode = ($PostVar['ApprovalMode'] == '') ? 'T' : $PostVar['ApprovalMode'];
        $sessionToken = $_PAGE['POST']['sessionToken'];
        $_PAGE['POST']['caseID'] ? $caseID = $_PAGE['POST']['caseID'] : $caseID = -1;

        //Checking of type => remove unnessary field
        switch ($quotationType) {
            case QUOTATION_TYPE_NO:
                $minQuote = '';
                $quotationApprovalRoleID = '';
                $tenderAuditingRoleID = '';
                $tenderApprovalRoleID = '';
                break;
            case QUOTATION_TYPE_QUOTATION:
                $tenderAuditingRoleID = '';
                $tenderApprovalRoleID = '';
                break;
            case QUOTATION_TYPE_TENDER:
                $quotationApprovalRoleID = '';
                break;
        }

        // new approval rule builder
        $approvalRule = array();
        foreach ($PostVar['selectedRoles'] as $_roleId) {
            $_approveNum = $PostVar['approveNum'][$_roleId];
            $approvalRule[$_roleId] = $_approveNum;
        }
        $json = $_PAGE['Controller']->requestObject('JSON_obj', 'includes/json.php');
        $approvalRuleJson = $json->encode($approvalRule);

        $custApprovalRuleJson = '';
        $custApprovalMode = '';
        if ($sys_custom['ePCM']['extraApprovalFlow'] && $quotationType != 'N') {
            $custApprovalRule = array();
            foreach ((array)$PostVar['cust_selectedRoles'] as $_roleId) {
                $_approveNum = $PostVar['cust_approveNum'][$_roleId];
                $custApprovalRule[$_roleId] = $_approveNum;
            }

            $custApprovalRuleJson = $json->encode($custApprovalRule);
            $custApprovalMode = ($PostVar['cust_ApprovalMode'] == '') ? 'T' : $PostVar['cust_ApprovalMode'];
        }
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->saveRule($ruleID, $code, $floorPrice, $cellingPrice, $approvalRoleID, $needGroupHeadApproval,
            $quotationType, $minQuote, $quotationApprovalRoleID, $tenderAuditingRoleID, $tenderApprovalRoleID,
            $isTemplate, $isDeleted = 0, $needVicePricipalApproval, $approvalMode, $approvalRuleJson, $custApprovalMode,
            $custApprovalRuleJson);
        $mgmt_db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt', 'includes/ePCM/libPCM_db_mgmt.php');
        $file_db = $_PAGE['Controller']->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
        $filelistArray = $db->getTempFileList($ruleID, $sessionToken, $AssocWithStage = true);
        if (!empty($filelistArray)) {
            if(!($ruleID >0)) {
                $newRuleIDResult = $db->getLatestRuleID();
                $ruleID = $newRuleIDResult[0]['RuleID'];
            }
            foreach ($filelistArray as $template_key => $_infoAry) {
                if (!empty($_infoAry)) {
                    foreach ($_infoAry as $__infoAry) {
                        $FileIDArr[] = $__infoAry['AttachmentID'];
                    }
                    $fileResult = $mgmt_db->insertFileToCase($FileIDArr, $caseID, $template_key, $type = '0', $ruleID);

                }
            }
        }

        $willdeletedlist = $db->getTempFileList($ruleID, $sessionToken, $AssocWithStage = true, $willdeleted = true);
        if (!empty($willdeletedlist)){
            foreach ($willdeletedlist as $willDelStageFileInfoAry) {
                foreach ($willDelStageFileInfoAry as $willDelFileInfoAry) {
                    $WillDelFileArr[] = $willDelFileInfoAry['AttachmentID'];
                }
            }
            foreach ((array)$WillDelFileArr as $fileID) {
                $file_db->removeAttachmentStep2($fileID, $keepAttachment=true);
            }
        }

        if ($ruleID == '') {
            $this->sendRedirect("index.php?p=setting.rule.AddSuccess");
        } else {
            $this->sendRedirect("index.php?p=setting.rule.UpdateSuccess");
        }

    }

    function ruleDelete()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $ruleIdAry = $PostVar['targetIdAry'];
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $db->deleteRule($ruleIdAry);

        $this->sendRedirect("index.php?p=setting.rule.DeleteSuccess");

    }

    function ruleEdit()
    {
        global $_PAGE;
        $PostVar = $_PAGE['POST'];
        $ruleId = $PostVar['targetIdAry'][0];

        $this->ruleNew($ruleId);
    }

    ####################################################################
    ####	Security
    ####################################################################
    function security($tab = '')
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Security';

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');

        $path = '/setting/security_authentication.php';
        if ($tab != '') {
            $tabParmArr = explode('.', $tab);
            $_PAGE['curTab'] = $tabParmArr[0];
            $_PAGE['curTab'] = $tab;
        } else {
            $_PAGE['curTab'] = 'authentication';
        }

        if ($_PAGE['curTab'] == 'authentication') {
            //get general setting
            $obj_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting',
                'includes/ePCM/libPCM_db_setting.php');
            $result = $obj_setting->getGeneralSettings();
            $_PAGE['AccessSetting'] = $result['AccessSetting'];
        } else {
            if ($_PAGE['curTab'] == 'network') {
                $path = '/setting/security_network.php';

                $obj_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting',
                    'includes/ePCM/libPCM_db_setting.php');
                $result = $obj_setting->getGeneralSettings();
                $_PAGE['IPList'] = $result['IPList'];
            }
        }

        $main = $this->getViewContent($path);
        $view = $this->getLayout($main, '', '/template/setting_form.php');
        echo $view;
    }

    function securityUpdate($tab = '')
    {
        global $_PAGE;

        if ($tab != '') {
// 			$tabParmArr = explode('.',$tab);
// 			$_PAGE['curTab'] = $tabParmArr[0];
            $_PAGE['curTab'] = $tab;
        } else {
            $_PAGE['curTab'] = 'authentication';
        }

        if ($_PAGE['curTab'] == 'authentication') {
            //save general setting
            $obj_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting',
                'includes/ePCM/libPCM_db_setting.php');
            $obj_setting->updateGeneralSettings(array('AccessSetting' => $_PAGE['POST']['AccessSetting']));

            //Back to Security Settings
            $this->sendRedirect("index.php?p=setting.security.authentication");
        } else {
            if ($_PAGE['curTab'] == 'network') {
                //save general setting
                $obj_setting = $_PAGE['Controller']->requestObject('libPCM_db_setting',
                    'includes/ePCM/libPCM_db_setting.php');
                $obj_setting->updateGeneralSettings(array('IPList' => $_PAGE['POST']['IPList']));

                //Back to Security Settings
                $this->sendRedirect("index.php?p=setting.security.network");

            }
        }

    }

    public function template()
    {
        global $_PAGE, $ePCMcfg;

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'SampleDocument';
        $db = $_PAGE['Controller']->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');

        $fileArr = $db->getCaseStageAttachment($CaseID = -1, $Stage = '', $AssocWithStage = true);
        $_PAGE['view_data']['fileArr'] = $fileArr;

        $main = $this->getViewContent('/setting/doc_template.php');
        $view = $this->getLayout($main);

        echo $view;
    }

    public function templateUpload($template_key)
    {
        global $_PAGE, $ePCMcfg;

        $postVar = $_PAGE['POST'];

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'SampleDocument';

        $_PAGE['view_data']['template_key'] = $template_key;
        $main = $this->getViewContent('/setting/doc_template_upload.php');
        if ($postVar['ruleID']) {
            $view = $this->getViewContent('/setting/rule_doc_template_upload.php');
        } else {
            $view = $this->getLayout($main);
        }

        echo $view;
    }

    public function ruleTemplateUploadTableUpdate()
    {
        global $_PAGE;

        $ruleID = $_PAGE['POST']['ruleID'];
        $session = $_PAGE['POST']['session'];
        $ui = $_PAGE['Controller']->requestObject('libPCM_ui', 'includes/ePCM/libPCM_ui.php');
        $html = $ui->getTempRuleTemplateFileUI($ruleID, $session);
        echo $html;
    }

    public function templateUploadUpdate()
    {
        global $_PAGE;

        $caseID = $_PAGE['POST']['caseID'];
        $template_key = $_PAGE['POST']['template_key'];
        $FileIDArr = $_PAGE['POST']['FileID'];
        $WillDelFileArr = $_PAGE['POST']['WillDeletedList'];
        $mgmt_db = $_PAGE['Controller']->requestObject('libPCM_db_mgmt', 'includes/ePCM/libPCM_db_mgmt.php');
        $file_db = $_PAGE['Controller']->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
        if (!empty($FileIDArr)) {
            $fileResult = $mgmt_db->insertFileToCase($FileIDArr, $caseID, $template_key);
        }
        if (!empty($WillDelFileArr)) {
            foreach ((array)$WillDelFileArr as $fileID) {
                $file_db->removeAttachmentStep2($fileID);
            }
        }
        $this->sendRedirect("index.php?p=setting.template");
    }

    public function notification()
    {
        global $_PAGE;

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Alert';

        $main = $this->getViewContent('/index_tmpl.php');
        $view = $this->getLayout($main);

        echo $view;
    }

    #### Import
    public function importGroupFromEInventory()
    {
        global $_PAGE, $plugin;

        if (!$plugin['Inventory']) {
            debug_pr('Only avaliable if eInventory is subscribed');
        }

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $eInvGroup = $db->getEInvAdminGroup();

        $_PAGE['views_data']['eInvGroup'] = $eInvGroup;

        $main = $this->getViewContent('/setting/import_group_eInv.php');
        $view = $this->getLayout($main, '', '/template/form.php');
        echo $view;

    }

    public function showGroupImportFrInvConfirmation()
    {
        global $_PAGE, $plugin;

        if (!$plugin['Inventory']) {
            debug_pr('Only avaliable if eInventory is subscribed');
        }

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Group';


        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        //Get Chi, EngName, Code of eInv Group
        $eInvAdminGroupIdAry = $_PAGE['POST']['groupIdAry'];
        $eInvGroupAry = $db->getEInvAdminGroup($eInvAdminGroupIdAry);

        // 		debug_pr($_PAGE['POST']);
        $needAddMember = $_PAGE['POST']['needAddMember'];
        $needSetGroupAdmin = $_PAGE['POST']['needSetGroupAdmin'];
        if ($needAddMember) {
            //Get eInv Group members' userIDs
            $eInvMemberAry = $db->getEInvAdminGroupMember($eInvAdminGroupIdAry);
            // 			debug_pr($eInvMemberAry);
            foreach ($eInvGroupAry as &$_group) {
                $_group['UserAry'] = $eInvMemberAry[$_group['AdminGroupID']];
            }
        }


        $_PAGE['views_data']['eInvGroupAry'] = $eInvGroupAry;
        $_PAGE['views_data']['groupIdAry'] = $eInvAdminGroupIdAry;
        $_PAGE['views_data']['needAddMember'] = $needAddMember;
        $_PAGE['views_data']['needSetGroupAdmin'] = $needSetGroupAdmin;


        $main = $this->getViewContent('/setting/import_group_eInv_step2.php');
        $view = $this->getLayout($main);
        echo $view;

    }

    public function importInvGroup()
    {
        global $_PAGE, $plugin;

        if (!$plugin['Inventory']) {
            debug_pr('Only avaliable if eInventory is subscribed');
        }
        // 		debug_pr($_PAGE['POST']);
        $eInvGroupAry = $_PAGE['POST']['eInvGroupAry'];
        $needAddMember = $_PAGE['POST']['needAddMember'];
        $needSetGroupAdmin = $_PAGE['POST']['needSetGroupAdmin'];

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $db->importInvGroup($eInvGroupAry, $needAddMember, $needSetGroupAdmin);

        $this->sendRedirect("index.php?p=setting.group.AddSuccess");

    }

    public function importSupplier()
    {
        global $_PAGE;
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';
        $_PAGE['curTab'] = 'supplier';

        $main = $this->getViewContent('/setting/supplier_import.php');
        $view = $this->getLayout($main, '', '/template/form.php');
        echo $view;
    }

    public function importUpdateSupplier()
    {
        global $_PAGE, $PATH_WRT_ROOT, $Lang;

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';
        $_PAGE['curTab'] = 'supplier';

        $userfile = $_PAGE['FILES']['userfile'];
        $heading_checking_array = array(
            'Supplier Name (Chi)',
            'Supplier Name (Eng)',
            'Website',
            'Description',
            'Phone',
            'Fax',
            'Email',
            'Type'
        );
        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $extChecking = $settingObj->importCsvFileExtChecking($userfile);
        $userEnteredHeading = $settingObj->getHeadingFromCSV($userfile);
        $headingCorrect = $settingObj->importCsvHeadingChecking($userEnteredHeading, $heading_checking_array);
        $data = $settingObj->getDataFromCSV($userfile);
        $error = $settingObj->importSupplier($data);

        if (!$extChecking) {
            $error[] = 'INCORRECT_EXT';
        }
        if (!$headingCorrect) {
            $error[] = 'INCORRECT_HEADING';
        }

        $_PAGE['view_data']['data'] = $data;
        $_PAGE['view_data']['error'] = $error;
        $_PAGE['view_data']['headings'] = $userEnteredHeading;
        $_PAGE['view_data']['heading_checking_array'] = $heading_checking_array;
        $_PAGE['view_data']['form_action'] = 'index.php?p=setting.supplier.importUpdateResult';
        $_PAGE['view_data']['back_location'] = 'index.php?p=setting.supplier.import';
        $navigationAry[] = array(
            $Lang['ePCM']['SettingsArr']['SupplierArr']['TabTitle'],
            'javascript: window.location=\'index.php?p=setting.supplier\';'
        );
        $navigationAry[] = array($Lang['Btn']['Import']);
        $_PAGE['view_data']['navigationAry'] = $navigationAry;

        $main = $this->getViewContent('/setting/import_step2.php');
        $view = $this->getLayout($main);
        echo $view;

    }

    public function importUpdateResultSupplier()
    {
        global $_PAGE;
        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $data = $settingObj->getTempTable_supplier();
        foreach ($data as $_data) {
            $settingObj->saveSupplier($_data, array());
        }

        $this->sendRedirect("index.php?p=setting.supplier.AddSuccess");
    }

    public function importSupplierType()
    {
        global $_PAGE;

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';
        $_PAGE['curTab'] = 'supplierType';

        $main = $this->getViewContent('/setting/supplier_type_import.php');
        $view = $this->getLayout($main, '', '/template/form.php');
        echo $view;
    }

    public function importUpdateSupplierType()
    {
        global $_PAGE, $PATH_WRT_ROOT, $Lang;

        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Supplier';
        $_PAGE['curTab'] = 'supplierType';

        $userfile = $_PAGE['FILES']['userfile'];
        $heading_checking_array = array('Type Name (Chi)', 'Type Name (Eng)');

        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

        $extChecking = $settingObj->importCsvFileExtChecking($userfile);
        $userEnteredHeading = $settingObj->getHeadingFromCSV($userfile);
        $headingCorrect = $settingObj->importCsvHeadingChecking($userEnteredHeading, $heading_checking_array);
        $data = $settingObj->getDataFromCSV($userfile);
        $error = $settingObj->importSupplierType($data);

        if (!$extChecking) {
            $error[] = 'INCORRECT_EXT';
        }
        if (!$headingCorrect) {
            $error[] = 'INCORRECT_HEADING';
        }

        $_PAGE['view_data']['data'] = $data;
        $_PAGE['view_data']['error'] = $error;
        $_PAGE['view_data']['headings'] = $userEnteredHeading;
        $_PAGE['view_data']['heading_checking_array'] = $heading_checking_array;
        $_PAGE['view_data']['form_action'] = 'index.php?p=setting.supplierType.importUpdateResult';
        $_PAGE['view_data']['back_location'] = 'index.php?p=setting.supplierType.import';
        $navigationAry[] = array(
            $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TabTitle'],
            'javascript: window.location=\'index.php?p=setting.supplierType\';'
        );
        $navigationAry[] = array($Lang['Btn']['Import']);
        $_PAGE['view_data']['navigationAry'] = $navigationAry;

        $main = $this->getViewContent('/setting/import_step2.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    public function importUpdateResultSupplierType()
    {
        global $_PAGE;

        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $data = $settingObj->getTempTable_supplierType();
        $multiRowValueArr = array();
        foreach ($data as $_key => $_value) {
            $multiRowValueArr[] = array($_value['TypeName'], $_value['TypeNameChi']);
        }
        $success = $settingObj->insertSupplierType($multiRowValueArr);

// 		if($success){
        $this->sendRedirect("index.php?p=setting.supplierType.AddSuccess");
// 		}else{
// 			$this->sendRedirect("index.php?p=setting.supplierType.AddUnsuccess");
// 		}
    }

    ### Export
    public function exportSupplier()
    {
        global $_PAGE, $PATH_WRT_ROOT;
        $keyword = $_PAGE['POST']['keyword'];
        $typeID = $_PAGE['POST']['typeID'];
        $isActive = $_PAGE['POST']['isActive'];

        $settingObj = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $additionSelectStatement = ",WebSite,SupplierNameChi,SupplierName as SupplierNameEng,Phone,Email,Fax";
        $data = $settingObj->getSupplierSqlForDbTable($typeID, $isActive, $keyword, $excludedSupplierAry = '', 0,
            $additionSelectStatement);
// 		debug_pr($data);
        include_once($PATH_WRT_ROOT . "includes/libexporttext.php");
        $lexport = new libexporttext();

        $exportColumn = array(
            'Supplier Name (Chi)',
            'Supplier Name (Eng)',
            'Website',
            'Description',
            'Phone',
            'Fax',
            'Email',
            'Type'
        );
        $ExportArr = array();

        foreach ($data as $_data) {
            $ExportArr[] = array(
                $_data['SupplierNameChi'],
                $_data['SupplierNameEng'],
                $_data['WebSite'],
                $_data['Description'],
                $_data['Phone'],
                $_data['Fax'],
                $_data['Email'],
                $_data['Type']
            );
        }

        $export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, $Delimiter = "", $LineBreak = "\r\n",
            $ColumnDefDelimiter = "", $DataSize = 0, $Quoted = "00", $includeLineBreak = 1);
        $filename = 'export_eProcurement_supplier.csv';
        //if(isset($junior_mck)){
        if ($_PAGE['libPCM']->isEJ()) {
            //EJ Only
            $lexport->EXPORT_FILE_UNICODE($filename, $export_content);
        } else {
            $lexport->EXPORT_FILE($filename, $export_content);
        }
    }

    ###Funding Source
    public function funding()
    {
        global $_PAGE;

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $_PAGE['CurrentSection'] = 'Settings';
        $_PAGE['CurrentPage'] = 'Funding';
        $FundingFilter = $_POST['FundingFilter'];
        $_PAGE['view_data']['TableData'] = $db->getFundingSourceTableData($FundingFilter);
        $_PAGE['view_data']['BtnAry'] = "";
        $main = $this->getViewContent('/setting/funding.php');
        $view = $this->getLayout($main);
        echo $view;
    }

    public function updateFundingCategory()
    {
        global $_PAGE;
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $CategoryID = $_PAGE['POST']['CategoryID'];
        $CategoryNameEN = $_PAGE['POST']['CategoryNameEN'];
        $CategoryNameCHI = $_PAGE['POST']['CategoryNameCHI'];
        $db->updateFundingCategory($CategoryID, $CategoryNameEN, $CategoryNameCHI);
    }

    public function deleteFundingCategory()
    {
        global $_PAGE;
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $CategoryID = $_PAGE['POST']['CategoryID'];
        if ($CategoryID) {
            $db->deleteFundingCategory($CategoryID);
        }
    }

    public function updateFunding()
    {
        global $_PAGE;
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $FundingID = $_PAGE['POST']['FundingThichBox_FundingID'];
        $FundingNameEN = $_PAGE['POST']['FundingThichBox_FundingEnglishName'];
        $FundingNameCHI = $_PAGE['POST']['FundingThichBox_FundingChineseName'];
        $Budget = $_PAGE['POST']['FundingThichBox_FundingBudget'];
        $RecordStatus = $_PAGE['POST']['FundingThichBox_FundingRecordStatus'];
        $DeadLineDate = $_PAGE['POST']['FundingThichBox_FundingDeadline'];
        $CategoryID = $_PAGE['POST']['FundingThichBox_FundingCategoryID'];
        $DisplayOrder = $_PAGE['POST']['FundingThichBox_DisplayOrder'];
        //Update Funding Record
        $FundingID = $db->updateFunding($FundingID, $FundingNameCHI, $FundingNameEN, $Budget, $RecordStatus,
            $DeadLineDate, $DisplayOrder, $CategoryID);
    }

    public function deleteFunding()
    {
        global $_PAGE;
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $FundingID = $_PAGE['POST']['FundingID'];
        if ($FundingID) {
            $db->deleteFunding($FundingID);
        }
    }

    ####################### Import Helper (Start) #######################
    private function isImportTypeValid($type)
    {
        $registry = array(
            'category',
            'group',
            'groupitem',
            'groupitembudget',
            'funding',
            'fundingcategory',
        );
        return $type != '' && in_array($type, $registry);
    }

    public function importTemplate($type)
    {
        if ($this->isImportTypeValid($type)) {
            global $_PAGE;
            $templateName = ucfirst(strtolower($type));
            $template = $_PAGE['Controller']->requestObject($templateName,
                'includes/ePCM/helper/importScheme/' . $templateName . '.php');
            $lexport = $_PAGE['Controller']->requestObject('libexporttext', 'includes/libexporttext.php');

            $templateHeaderArr = $template->getHeaderTemplate();
            $templateDataArr = $template->getContentTemplate();
            $propertyArr = $template->getHeaderProperty();
            $export_header = $lexport->GET_EXPORT_HEADER_COLUMN($templateHeaderArr, $propertyArr);
            $export_content = $lexport->GET_EXPORT_TXT($templateDataArr, $export_header);

            $filename = $templateName ? $type . "_" . "template.csv" : "template.csv";
            if ($_PAGE['libPCM']->isEJ()) {
                //EJ Only
                $lexport->EXPORT_FILE_UNICODE($filename, $export_content);
            } else {
                $lexport->EXPORT_FILE($filename, $export_content);
            }
        } else {
            echo 'Invalid request !';
            die();
        }
    }

    public function importValidate($type)
    {

        if (!$this->isImportTypeValid($type)) {
            echo 'Invalid request !';
            die();
        }

        global $_PAGE, $ePCMcfg, $intranet_root, $Lang;

        ### get import helper ###
        $templateName = ucfirst(strtolower($type));
        $importScheme = $_PAGE['Controller']->requestObject($templateName,
            'includes/ePCM/helper/importScheme/' . $templateName . '.php');
        $limport = $_PAGE['Controller']->requestObject('libimporttext', 'includes/libimporttext.php');
// 		$lfs = $_PAGE['Controller']->requestObject('libfilesystem','includes/libfilesystem.php');
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        require_once $intranet_root . '/includes/ePCM/helper/ValidationHelper.php';


        ### Read Tmp file
        $targetFilePath = $intranet_root . '/' . $ePCMcfg['INTERNATDATA'] . 'temp/import/' . $type . '/' . $_GET['tmpfile'];
        $headerArr = $importScheme->getHeaderTemplate();
        $propertyArr = $importScheme->getHeaderProperty();
        $csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $headerArr, $propertyArr);
        array_shift($csvData);
        $numOfData = count($csvData);

        $desc = $importScheme->getContentDescripiton();
        $widthArr = $importScheme->getContentWidth();
        $mapper = $importScheme->getContentMapping();
        $rules = $importScheme->getValidationRule();
        $numCol = count($mapper);

        $validationHelper = new ValidationHelper($csvData, $mapper, $rules, $db);
        $errors = $validationHelper->process();
        $processedData = $validationHelper->getProcessedData();
        $numOfError = count($errors);

        if (!empty($errors)) {
            # build $errorDataArr for display
            $errorDataArr = array();
            foreach ($errors as $_key => $_row) {
                // Get raw data
                $errorDataArr[$_key] = $csvData[$_key];
                foreach ($_row as $__colKey => $__colErrors) {
                    $__thisName = $desc[$mapper[$__colKey]];
                    // putting name into $__colErrors
                    foreach ($__colErrors as &$___error) {
                        $___error = str_replace('<!--name-->', $__thisName, $___error);
                    }
                    // insert error msg
                    $errorDataArr[$_key][$numCol][$__colKey] = implode('<br>', $__colErrors);
                }
            }
        }

        // preview table
        $html = '<table class="common_table_list_v30 view_table_list_v30">';
        $html .= '<thead>';
        $html .= '<th width="1">#</td>';
        foreach ($mapper as $_key => $_colKey) {
            if (isset($widthArr[$_key])) {
                $width = ' width="' . $widthArr[$_key] . '%"';
            } else {
                $width = '';
            }
            $html .= '<th' . $width . '>' . $desc[$_colKey] . '</th>';
        }
        $html .= '<th width="25%">' . $Lang['ePCM']['Import']['Remarks'] . '</td>';
        $html .= '</thead>';
        $html .= '<tbody>';


        //foreach($errorDataArr as $_key => $_rowData){
        foreach ($csvData as $_key => $_rowData) {
            $_isErrorRow = false;
            if (isset($errorDataArr[$_key])) {
                $_isErrorRow = true;
                $_rowData = $errorDataArr[$_key];
            }
            $_rowNum = $_key + 1;
            $html .= '<tr>';
            $html .= '<td class="tabletext">' . $_rowNum . '</td>';

            $_lastColKey = Get_Array_Last_Key($_rowData);
            foreach ($_rowData as $__colKey => $__col) {
                // processed data
                if (isset($processedData[$_key][$__colKey])) {
                    $__col = $processedData[$_key][$__colKey];
                }
                if ($__colKey == $_lastColKey && $_isErrorRow) {
                    // error description on import content
                    if (!empty($__col)) {
                        $html .= '<td class="red">' . implode('<br>', $__col) . '</td>';
                    } else {
                        $html .= '<td class="red"></td>';
                    }
                } else {
                    // import content
                    $__tdRedClass = ($_isErrorRow && $_rowData[$_lastColKey][$__colKey] != '') ? ' red' : '';
                    $html .= '<td class=" tabletext' . $__tdRedClass . '">' . $__col . '</td>';
                    if ($__colKey == $_lastColKey) {
                        $html .= '<td></td>';
                    }
                }
            }
            $html .= '</tr>';
        }
        $html .= '</tbody>';
        $html .= '</table>';

        echo '<script>';
        if ($numOfError > 0) {
            echo 'window.parent.document.getElementById("FailCountDiv").className = "red";';
            echo 'window.parent.checkForm = "";';
            echo 'window.parent.document.getElementById("submitBtn").disabled = true;';
            echo 'window.parent.document.getElementById("submitBtn").className += " formbutton_disable";';
            echo 'window.parent.document.getElementById("submitBtn").classList.remove("formbutton_v30");';
        }
        echo 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "' . ($numOfData - $numOfError) . '";';
        echo 'window.parent.document.getElementById("FailCountDiv").innerHTML = "' . $numOfError . '";';
        echo 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = "' . addslashes($html) . '";';
        echo 'window.parent.UnBlock_Document();';
        echo '</script>';
    }

    function ajaxImportRef($type)
    {
        global $intranet_root, $_PAGE, $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

        $refType = $_GET['refType'];

        $templateName = ucfirst(strtolower($type));

        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $modelName = $templateName . 'Model';
        require_once $intranet_root . '/includes/ePCM/helper/models/' . $modelName . '.php';
        $model = new $modelName($db);
        $importScheme = $_PAGE['Controller']->requestObject($templateName,
            'includes/ePCM/helper/importScheme/' . $templateName . '.php');
        require_once $intranet_root . '/includes/ePCM/helper/ReferenceHelper.php';
        $refHelper = new ReferenceHelper($model, $importScheme, $refType);

        $title = $refHelper->getReferenceHeaderHTML();
        $RemarkDetailsArr = $refHelper->getReferenceContentHTMLArr();

        // build thick box html content
        $RemarksLayer = '';
        $RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">' . "\r\n";
        $RemarksLayer .= '<tbody>' . "\r\n";
        $RemarksLayer .= '<tr>' . "\r\n";
        $RemarksLayer .= '<td align="right" style="border-bottom: medium none;">' . "\r\n";
        $RemarksLayer .= '<a href="javascript:hideRemarkLayer(\'type\');" ><img border="0" src="' . $PATH_WRT_ROOT . $image_path . '/' . $LAYOUT_SKIN . '/ecomm/btn_mini_off.gif" style="float:right"></a>' . "\r\n";
        $RemarksLayer .= '</td>' . "\r\n";
        $RemarksLayer .= '</tr>' . "\r\n";
        $RemarksLayer .= '<tr>' . "\r\n";
        $RemarksLayer .= '<td align="left" style="border-bottom: medium none;">' . "\r\n";
        $RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">' . "\r\n";
        $RemarksLayer .= '<thead>' . "\n";
        $RemarksLayer .= '<tr>' . "\n";
        $RemarksLayer .= $title . "\n";
        $RemarksLayer .= '</tr>' . "\n";
        $RemarksLayer .= '</thead>' . "\n";
        $RemarksLayer .= '<tbody>' . "\n";
        foreach ($RemarkDetailsArr as $RemarkDetail) {
            $RemarksLayer .= '<tr>' . "\n";
            $RemarksLayer .= $RemarkDetail . "\n";
            $RemarksLayer .= '</tr>' . "\n";
        }
        $RemarksLayer .= '</tbody>' . "\n";
        $RemarksLayer .= '</table>' . "\r\n";
        $RemarksLayer .= '</td>' . "\r\n";
        $RemarksLayer .= '</tr>' . "\r\n";
        $RemarksLayer .= '</tbody>' . "\r\n";
        $RemarksLayer .= '</table>' . "\r\n";

        //$importScheme->getRemarksThickBox($refType);
        echo $RemarksLayer;
    }

    function importStep1($type)
    {

        global $_PAGE, $Lang;
        if (!$this->isImportTypeValid($type)) {
            echo 'Invalid request !';
            die();
        }

        ### get import helper ###
        $templateName = ucfirst(strtolower($type));
        $importScheme = $_PAGE['Controller']->requestObject($templateName,
            'includes/ePCM/helper/importScheme/' . $templateName . '.php');

        ### Set this for module menu
        $importScheme->setCurrentSectionAndPage();

        ### passing data to view (start) ###
        $_PAGE['views_data'] = array();
        $_PAGE['views_data']['type'] = $type;
        # navigation
        $_PAGE['views_data']['navigationAry'] = $importScheme->getNavigationAry();
        # import description
        $_PAGE['views_data']['ColumnTitleArr'] = $importScheme->getColumnTitleArr();
        $_PAGE['views_data']['ColumnPropertyArr'] = $importScheme->getColumnPropertyArr();
        $_PAGE['views_data']['RemarksArr'] = $importScheme->getRemarksArr();
        $_PAGE['views_data']['sampleLink'] = '<a class="tablelink" href="?p=setting.import.template.' . $type . '">[' . $Lang['General']['ClickHereToDownloadSample'] . ']</a>';
        # for js
        $_PAGE['views_data']['js'] = array();
        $_PAGE['views_data']['js']['backLocation'] = 'index.php?p=' . $importScheme->getOriginPath();
        $_PAGE['views_data']['js']['submitLocation'] = 'index.php?p=setting.import.step2.' . $type;
        ### passing data to view (end) ###

        $main = $this->getViewContent('/template/import/step1.php');
        $view = $this->getLayout($main, '', '/template/form.php');
        echo $view;
    }

    function importStep2($type)
    {

        global $_PAGE, $ePCMcfg, $Lang, $intranet_root;
        if (!$this->isImportTypeValid($type)) {
            echo 'Invalid request !';
            die();
        }
        if ($_FILES['userfile']['name'] == '' || $_FILES['userfile']['size'] == 0) {
            echo 'No file is uploaded';
            die();
        }

        ### get import helper ###
        $limport = $_PAGE['Controller']->requestObject('libimporttext', 'includes/libimporttext.php');
        $templateName = ucfirst(strtolower($type));
        $importScheme = $_PAGE['Controller']->requestObject($templateName,
            'includes/ePCM/helper/importScheme/' . $templateName . '.php');

        ### Set this for module menu
        $importScheme->setCurrentSectionAndPage();

        ### Import Helper for transfer import file to temp (start) ###
        $lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
        $importHelper = $_PAGE['Controller']->requestObject('importHelper', 'includes/ePCM/helper/importHelper.php');
        $importHelper->importFile = $_FILES['userfile'];
        $importHelper->fileHandler = $lfs;
        $importHelper->tempPath = $importScheme->getTempFilePath();
        $importHelper->processToTemp();
        $result = $importHelper->processResult;
        unset($importHelper);
        ### Import Helper for transfer import file to temp (end) ###

        ### Check file header (start) ###
        $targetFilePath = $intranet_root . '/' . $ePCMcfg['INTERNATDATA'] . 'temp/import/' . $type . '/' . $result['filename'];
        $headerArr = $importScheme->getHeaderTemplate();
        $propertyArr = $importScheme->getHeaderProperty();
        $csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $headerArr, $propertyArr);
        $csvHeader = array_shift($csvData);
        $file_format = $headerArr['En'];

        $sizeOfCsvHeader = sizeof($csvHeader);
        $csvHeaderWrong = false;
        for ($i = 0; $i < $sizeOfCsvHeader; $i++) {
            if (trim($csvHeader[$i]) != trim($file_format[$i])) {
                $csvHeaderWrong = true;
                break;
            }
        }
        if ($csvHeaderWrong) {
            $result['error'] = 2;
        }
        ### Check file header (end) ###

        if ($result['error'] == 0 && $result['success'] == 1) {
            // success
            ### passing data to view (start) ###
            $_PAGE['views_data'] = array();
            # navigation
            $navigationAry[] = array(
                $Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'],
                'javascript: window.location=\'index.php?p=setting.category\';'
            );
            $navigationAry[] = array($Lang['Btn']['Import']);
            $_PAGE['views_data']['navigationAry'] = $navigationAry;
            $_PAGE['views_data']['tmpfile'] = $result['filename'];
            $_PAGE['views_data']['type'] = $type;
            # for js
            $_PAGE['views_data']['js'] = array();
            $_PAGE['views_data']['js']['backLocation'] = 'index.php?p=setting.import.step1.' . $type;
            $_PAGE['views_data']['js']['submitLocation'] = 'index.php?p=setting.import.step3.' . $type;
            ### passing data to view (end) ###

            $main = $this->getViewContent('/template/import/step2.php');
            $view = $this->getLayout($main, '', '/template/form.php');
            echo $view;
        } else {
            // fail
            $this->sendRedirect("index.php?p=setting.import.step1." . $type . '&error=' . $result['error']);
            die();
        }
    }

    function importStep3($type)
    {

        global $_PAGE, $intranet_root, $ePCMcfg;
        if (!$this->isImportTypeValid($type)) {
            echo 'Invalid request !';
            die();
        }

        ### get import helper & other library ###
        $templateName = ucfirst(strtolower($type));
        $importScheme = $_PAGE['Controller']->requestObject($templateName,
            'includes/ePCM/helper/importScheme/' . $templateName . '.php');
        $limport = $_PAGE['Controller']->requestObject('libimporttext', 'includes/libimporttext.php');
        $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');
        $lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');

        ### Set this for module menu
        $importScheme->setCurrentSectionAndPage();
        $_PAGE['views_data']['origin'] = $importScheme->getOriginPath();

        ### Read Tmp file
        $tempPath = $intranet_root . '/' . $ePCMcfg['INTERNATDATA'] . 'temp/import/' . $type . '/';
        $targetFilePath = $tempPath . $_POST['tmp'];
        if (file_exists($targetFilePath)) {

            $headerArr = $importScheme->getHeaderTemplate();
            $propertyArr = $importScheme->getHeaderProperty();
            $csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $headerArr, $propertyArr);
            array_shift($csvData);
            $numOfData = count($csvData);

            $mapper = $importScheme->getContentMapping();
            $rules = $importScheme->getValidationRule();
            $numCol = count($mapper);

            require_once $intranet_root . '/includes/ePCM/helper/ValidationHelper.php';
            $validationHelper = new ValidationHelper($csvData, $mapper, $rules, $db);
            $errors = $validationHelper->process();

            // if no error proceed
            if (empty($errors)) {
                $modelName = $templateName . 'Model';
                require_once $intranet_root . '/includes/ePCM/helper/models/' . $modelName . '.php';
                $model = new $modelName($db);
                $success = $model->insertToDb($csvData);
                $_PAGE['views_data']['success'] = $success;

                // rename tmp file to avoid double insert
                $lfs->lfs_move($targetFilePath, $tempPath . $_POST['tmp'] . '.done');
            }
            $status = ($success == $numOfData) ? 1 : -1;
        } else {
            // lossing tmp file
            $status = -2;
        }

        $_PAGE['views_data']['status'] = $status;
        $view = $this->getLayout($main, '', '/template/import/step3.php');
        echo $view;
    }

    public function exportData($type)
    {
        if ($this->isImportTypeValid($type)) {
            global $_PAGE, $intranet_root;
            $keyword = $_PAGE['POST']['keyword'];
            $templateName = ucfirst(strtolower($type));
            $template = $_PAGE['Controller']->requestObject($templateName,
                'includes/ePCM/helper/importScheme/' . $templateName . '.php');
            $lexport = $_PAGE['Controller']->requestObject('libexporttext', 'includes/libexporttext.php');
            $db = $_PAGE['Controller']->requestObject('libPCM_db_setting', 'includes/ePCM/libPCM_db_setting.php');

            $modelName = $templateName . 'Model';
            require_once $intranet_root . '/includes/ePCM/helper/models/' . $modelName . '.php';
            $model = new $modelName($db);

            $templateHeaderArr = $template->getExportHeader();
            $dataArr = $model->getExportData($keyword);

            $propertyArr = $template->getHeaderProperty();
            $export_header = $lexport->GET_EXPORT_HEADER_COLUMN($templateHeaderArr, $propertyArr);
            $export_content = $lexport->GET_EXPORT_TXT($dataArr, $export_header);

            $filename = $templateName ? $type . "_" . "template.csv" : "template.csv";
            if ($_PAGE['libPCM']->isEJ()) {
                //EJ Only
                $lexport->EXPORT_FILE_UNICODE($filename, $export_content);
            } else {
                $lexport->EXPORT_FILE($filename, $export_content);
            }
        } else {
            echo 'Invalid request !';
            die();
        }
    }
    ####################### Import Helper (End) #######################
}

?>