<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->allowViewBudgetReport)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinvoice_ui.php");



$linterface = new interface_html();
$linvoice_ui = new libinvoice_ui();

echo $linvoice_ui->Display_Budget_Detail($AcademicYearID, $GroupID, $CategoryID, $view=1);

intranet_closedb();
?>
<script language="javascript">
function goExportDetail() {
	//document.form1.action = "export.php";
	//document.form1.submit();
	self.location.href = "export.php?flag=detail&AcademicYearID=<?=$AcademicYearID?>&GroupID=<?=$GroupID?>&CategoryID=<?=$CategoryID?>";
}
</script>