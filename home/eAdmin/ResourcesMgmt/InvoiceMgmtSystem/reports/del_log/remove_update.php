<?php

//Modifying by: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();
$lg = new liblog();

$x = $lg->DELETE_LOG($linvoice->Module, $StartDate, $EndDate);

intranet_closedb();

header("location: index.php?StartDate=$StartDate&EndDate=$EndDate&msg=delete");
?>