<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

$InvoiceID = is_array($RecordID) ? $RecordID[0] : $RecordID;

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->isInvoiceCreator($InvoiceID, $UserID)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if(count($RecordID)>0) {
	$result = $linvoice->DeleteInvoice($RecordID);	
	
	if(in_array(FALSE, $result)) {
		$linvoice->RollBack_Trans();
		$msg = "DeleteUnsuccess";
	} else { 
		$linvoice->Commit_Trans();
		$msg = "DeleteSuccess";
	}
	
} else {
	$msg = "DeleteUnsuccess";
}

intranet_closedb();

header("Location: index.php?msg=$msg");
?>