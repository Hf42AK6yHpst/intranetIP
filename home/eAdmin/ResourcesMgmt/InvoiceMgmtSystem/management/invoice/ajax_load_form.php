<?
# using: yat

/*************************************************************************
 *  modification log
 *
 *	Date:	2011-06-13	YatWoon
 *			Fixed: connect database before declare linventory
 *
 *	Date:	2011-06-08	YatWoon
 *			Improved: Can back to step 1 from step 2 [Case#2011-0401-1247-08067]
 *
 * ************************************************************************/


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if($Action == "SubCategory")
{
	$arr_category2 = $linventory->getCategoryLevel2Name($CategoryID);
	$x = getSelectByArray($arr_category2, "name=targetCategory2 onChange='ajax_change_subcategory(1);'", $targetCategory2);
	$x .= '<span id="div_SubCategory_err_msg"></span>';
}

if($Action == "Purchase_Details")
{
	$sql = "SELECT ItemID, ".$linventory->getInventoryItemNameByLang()." FROM INVENTORY_ITEM WHERE ItemType = '$targetItemType' AND CategoryID = '$targetCategory' AND Category2ID = '$targetCategory2'";
	$arr_exist_item = $linventory->returnArray($sql,2);

	if(sizeof($arr_exist_item)>0)
	{
		$arr_purchase_type = array(array(1,$i_InventorySystem_Item_New),array(2,$i_InventorySystem_Item_Existing));
	}
	else if($targetCategory2)
	{
		$arr_purchase_type = array(array(1,$i_InventorySystem_Item_New));
	}
	else
	{
		$arr_purchase_type = array();
	}

	$x = getSelectByArray($arr_purchase_type, "name =\"purchaseType\" onChange='changePurchaseType(1);'",$purchaseType);

}

if($Action == "Retrieve_Existing_Item")
{
	$arr_exist_item = $linventory->returnExistingItems($targetItemType, $targetCategory, $targetCategory2);
	$x = getSelectByArray($arr_exist_item, "name =\"targetExistItem\" onChange='selectExistingItem();'",$targetExistItem);
}

if($Action == "Retrieve_Existing_Item_Info")
{
	$x1 = $linventory->returnItemInfo($ExistingItemID,'',$targetCategory,$targetCategory2);

	if(!empty($x1))
	{
		$x = "<script language='javascript'>";
		# Item Name (Chinese)
		$x .= "document.form1.item_chi_name.value = 'a". $x1[0]['NameChi'] ."';";
		$x .= "document.form1.item_chi_name.disabled=true;";
		# Item Name (English)
		$x .= "document.form1.item_eng_name.value = '". $x1[0]['NameEng'] ."';";
		$x .= "document.form1.item_eng_name.disabled=true;";

		if(!$sys_custom['eInventoryCustForSMC']) {
			# Description (Chinese)
			$x .= "document.form1.item_chi_discription.value = '". $x1[0]['DescriptionChi'] ."';";
			$x .= "document.form1.item_chi_discription.disabled=true;";
			# Description (English)
			$x .= "document.form1.item_eng_discription.value = '". $x1[0]['DescriptionEng'] ."';";
			$x .= "document.form1.item_eng_discription.disabled=true;";
		}

		# Ownership
		$x .= "document.form1.item_ownership[". ($x1[0]['Ownership']-1) ."].checked  = true;";
		$x .= "
			var t = document.form1.item_ownership.length;
			for(i=0;i<document.form1.item_ownership.length;i++)
			{
				document.form1.item_ownership[i].disabled=true;
			}
		";
		# Photo
		$x .= "document.form1.item_photo.disabled=true;";
		# Variance Manager
		$sql = "SELECT a.BulkItemAdmin FROM INVENTORY_ITEM_BULK_EXT AS a INNER JOIN INVENTORY_ADMIN_GROUP AS b ON (a.BulkItemAdmin = b.AdminGroupID) WHERE a.ItemID = '$ExistingItemID'";
		$BulkItemAdminID_temp = $linventory->returnArray($sql);
		$BulkItemAdminID = $BulkItemAdminID_temp[0][0];
		$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
		$arr_bulk_item_admin = $linventory->returnArray($sql);
		for($i=0;$i<sizeof($arr_bulk_item_admin);$i++)
		{
			if($arr_bulk_item_admin[$i][0] == $BulkItemAdminID)
				break;
		}
		$x .= "document.form1.bulk_item_admin.selectedIndex=". ($i+1) .";";
		$x .= "document.form1.bulk_item_admin.disabled=true;";
		$x .= "document.form1.exist_item_code.value='". $x1[0]['ItemCode'] ."';";
		$x .= "</script>";
	}
}

if($Action == "Reset_New_Item")
{
	$x = "<script language='javascript'>";
	# Item Name (Chinese)
	//$x .= "document.form1.item_chi_name.value='';";
	$x .= "document.form1.item_chi_name.disabled=false;";
	# Item Name (English)
	//$x .= "document.form1.item_eng_name.value='';";
	$x .= "document.form1.item_eng_name.disabled=false;";

	if(!$sys_custom['eInventoryCustForSMC']) {
		# Description (Chinese)
		//$x .= "document.form1.item_chi_discription.value = '';";
		$x .= "document.form1.item_chi_discription.disabled=false;";
		# Description (English)
		//$x .= "document.form1.item_eng_discription.value = '';";
		$x .= "document.form1.item_eng_discription.disabled=false;";
	}

	# Ownership
	$x .= "document.form1.item_ownership[0].checked  = true;";
	$x .= "
		var t = document.form1.item_ownership.length;
		for(i=0;i<document.form1.item_ownership.length;i++)
		{
			document.form1.item_ownership[i].disabled=false;
		}
	";
	# Photo
	$x .= "document.form1.item_photo.disabled=false;";
	# Variance Manager
	$x .= "document.form1.bulk_item_admin.selectedIndex=0;";
	$x .= "document.form1.bulk_item_admin.disabled=false;";


	$x .= "</script>";

}

if($Action == "Check_Resources_Mgt_Group")
{
	//targetLocation_index
	$sql = "select GroupInCharge, FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where ItemID='$targetItemID' and LocationID='$targetLocationID'";
	$result = $linventory->returnArray($sql);

	list($this_group, $this_funding) = $result[0];

	$x = "<script language='javascript'>";
	if(empty($result))
	{
		$x .= " document.form1.targetGroup_". $targetLocation_index.".disabled = false;";
		$x .= "for(i=0;i<document.form1.targetGroup_". $targetLocation_index.".length;i++) {";
		$x .= " if(document.form1.targetGroup_". $targetLocation_index."[i].value == ". $InvoiceGroupID .") {";
		$x .= " document.form1.targetGroup_". $targetLocation_index.".selectedIndex = i;";
		$x .= " }";
		$x .= "}";

		$x .= " document.form1.targetBulkFunding.disabled = false;";
		$x .= "for(i=0;i<document.form1.targetBulkFunding.length;i++) {";
		$x .= " if(document.form1.targetBulkFunding[i].value == ". $InvoiceFunding .") {";
		$x .= " document.form1.targetBulkFunding.selectedIndex = i;";
		$x .= " }";
		$x .= "}";
	}
	else
	{
		$x .= "for(i=0;i<document.form1.targetGroup_". $targetLocation_index.".length;i++) {";
		$x .= " if(document.form1.targetGroup_". $targetLocation_index."[i].value == ". $this_group .") {";
		$x .= " document.form1.targetGroup_". $targetLocation_index.".selectedIndex = i;";
		$x .= " document.form1.targetGroup_". $targetLocation_index.".disabled = true;";
		$x .= " }";
		$x .= "}";

		$x .= "for(i=0;i<document.form1.targetBulkFunding.length;i++) {";
		$x .= " if(document.form1.targetBulkFunding[i].value == ". $this_funding .") {";
		$x .= " document.form1.targetBulkFunding.selectedIndex = i;";
		$x .= " document.form1.targetBulkFunding.disabled = true;";
		$x .= " }";
		$x .= "}";
		//targetBulkFunding
	}
	$x .= "</script>";



}




intranet_closedb();

echo $x;
?>