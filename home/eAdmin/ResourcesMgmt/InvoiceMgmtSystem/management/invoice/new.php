<?php
// modifying : 
/**
 * Change Log:
 * 2017-07-13 Pun [ip.2.5.8.7.1]
 *  - Fixed can submit form many times by click the submit button many times
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linvoice		= new libinvoice();

$admin_group_leader_arr = $linvoice->returnAdminGroup($UserID, $leader=1);

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && count($admin_group_leader_arr)==0) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Management_InvoiceList";
$linterface 	= new interface_html();


$PAGE_NAVIGATION[] = array($Lang['Invoice']['Invoice'],"index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New']);

$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

# Academic Year Filter
$YearID = $YearID ? $YearID : Get_Current_Academic_Year_ID();
if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'])
{
	//$YearSelection = getSelectAcademicYear("YearID","onChange='document.form1.submit();'",1,0,$YearID);
	$YearSelection = getSelectAcademicYear("YearID","",1,0,$YearID);
}
else
{
	$YearSelection = getAcademicYearByAcademicYearID($YearID,$intranet_session_language) . "<input type='hidden' name='YearID' value='$YearID'>";
}

# PIC selection
// $PICSel = "<select name='pic[]' id='pic[]' size='6' multiple='multiple'></select>";
# default user
$namefield = getNameFieldWithClassNumberByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID=$UserID";
$array_pic = $linvoice->returnArray($sql,2);
$PICSel = $linterface->GET_SELECTION_BOX($array_pic, "name='pic[]' ID='pic[]' class='select_studentlist' size='6' multiple='multiple'", "");

$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");

# Funding Source
$arr_funding_source = $linvoice->returnFundingSource();
$funding_selection = getSelectByArray($arr_funding_source,"name=item_funding", $item_funding);

?>

<script language="javascript">
<!--
var no_of_upload_file = <?=$no_file ==""?1:$no_file;?>;

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	if (document.all)
	{
		var cell = row.insertCell(0);
		//x= '<input class="file" type="file" name="attachment_'+no_of_upload_file+'" size="40">';
		//x+='<input type="hidden" name="hidden_attachment_'+no_of_upload_file+'">';

		x= '<input class="file" type="file" name="att[]" size="40">';
		
		cell.innerHTML = x;
		no_of_upload_file++;
		
		document.form1.attachment_size.value = no_of_upload_file;
	}
}


function reset_innerHtml()
{
 	document.getElementById('div_company_err_msg').innerHTML = "";
 	document.getElementById('div_pic_err_msg').innerHTML = "";
 	document.getElementById('div_discount_err_msg').innerHTML = "";
 	document.getElementById('div_totalamt_err_msg').innerHTML = "";
 	document.getElementById('div_InvoiceDate_err_msg').innerHTML = "";
 	document.getElementById('div_AccountDate_err_msg').innerHTML = "";
 	document.getElementById('div_description_err_msg').innerHTML = "";
}

function check_form(new_item)
{
	//// Reset div innerHtml
	reset_innerHtml();
	
	var obj = document.form1;
	var error_no = 0;
	var focus_field = "";
	 
	// Invoice Date 
	if (!check_date_30(obj.InvoiceDate,"<?php echo $i_invalid_date; ?>", "div_InvoiceDate_err_msg")) 
	 {
		 error_no++;
		if(focus_field=="")	focus_field = "InvoiceDate";
	 }
	 
	 if (compareDate('<?=date('Y-m-d')?>', obj.InvoiceDate.value) < 0)
	{
		document.getElementById('div_InvoiceDate_err_msg').innerHTML = '<font color="red"><?=$Lang['Invoice']['InvoiceDateError1']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "InvoiceDate";
	}
	
	// Company
	if(!check_text_30(obj.InvoiceCompany, "<?php echo $i_alert_pleasefillin.$Lang['Invoice']['InvoiceCompany']; ?>.", "div_company_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "InvoiceCompany";
	}
	
	// Description of use
	if(!check_text_30(obj.InvoiceDescription, "<?php echo $i_alert_pleasefillin.$Lang['Invoice']['Description']; ?>.", "div_description_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "InvoiceDescription";
	}
	
	// Discount Amt
	if(obj.DiscountAmount.value=='')
	{
		obj.DiscountAmount.value=0;
	}
	// Check Discount Amt  > 0 
	if(!check_positive_float_30(obj.DiscountAmount, "<?=$Lang['General']['JS_warning']['InputPositiveValue']?>","div_discount_err_msg"))
	{ 
		error_no++;
		if(focus_field=="")	focus_field = "DiscountAmount";
	}
	
	
	// Total Amt, empty => set 0
	if(obj.TotalAmount.value=='')
	{
		obj.TotalAmount.value=0;
	}
	// Check Total Amt  > 0 
	if( !check_positive_float_30(obj.TotalAmount, "<?=$Lang['General']['JS_warning']['InputPositiveValue']?>","div_totalamt_err_msg"))
	{ 
		error_no++;
		if(focus_field=="")	focus_field = "TotalAmount";
	}
	if( obj.TotalAmount.value==0 )
	{
		document.getElementById('div_totalamt_err_msg').innerHTML = '<font color="red"><?=$Lang['General']['JS_warning']['InputPositiveValue']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "TotalAmount";
	}
	 	
	// Account Date 
	if (!check_date_allow_null_30(obj.AccountDate,"<?php echo $i_invalid_date; ?>", "div_AccountDate_err_msg")) 
	 {
		 error_no++;
		if(focus_field=="")	focus_field = "AccountDate";
	 }
	 
	// PIC
	checkOptionAll(obj.elements["pic[]"]);
	if(obj.elements["pic[]"].length==0)
	{ 
		document.getElementById('div_pic_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$Lang['Invoice']['PIC']?></font>';
		error_no++;
	}
	if(obj.elements["pic[]"].length>1)
	{ 
		document.getElementById('div_pic_err_msg').innerHTML = '<font color="red"><?=$Lang['Invoice']['JS_warning']['PIC_1only']?></font>';
		error_no++;
	}
	
	if(error_no>0)
	{
		if(focus_field!="")
		{
			eval("obj." + focus_field +".focus();");
		}
		return false;
	}
	else
	{
		obj.add_invoice_item.value=new_item;
		checkOptionAll(obj.elements["pic[]"]);
		obj.submit();
		obj.submit_btn.disabled = true;
// 		Big5FileUploadHandler();
	}
}

function back() {
	self.location.href = "index.php";
}

//-->
</script>
<form name="form1" action="new_update.php" method="post" enctype="multipart/form-data">
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<div class="this_main_content">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
	<td><?=$YearSelection?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['InvoiceDate']?></td>
	<td><?=$linterface->GET_DATE_PICKER("InvoiceDate",$InvoiceDate);?> <span id="div_InvoiceDate_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['InvoiceCompany']?></td>
	<td><input name="InvoiceCompany" type="text" class="textboxtext"/><span id="div_company_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['InvoiceNo']?></td>
	<td><input name="InvoiceNo" type="text" class="textboxnum"/> <span class="tabletextremark"><?=$Lang['Invoice']['InvoiceNo_Remark']?></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['Description']?></td>
	<td><?=$linterface->GET_TEXTAREA("InvoiceDescription", $InvoiceDescription);?><br><span id="div_description_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['DiscountAmount']?></td>
	<td>$ <input name="DiscountAmount" type="text" class="textboxnum" value="0"/> <span id="div_discount_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['TotalAmount']?></td>
	<td>$ <input name="TotalAmount" type="text" class="textboxnum" value="0"/> <span id="div_totalamt_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['FundingSource']?></td>
	<td><?=$funding_selection?></td>
</tr> 

<tr>
	<td class="field_title"><?=$Lang['Invoice']['AccountDate']?></td>
	<td><?=$linterface->GET_DATE_PICKER("AccountDate","","","","","","","","","",1);?> <span id="div_AccountDate_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['PIC']?></td>
	<td>
		<table border="0" cellpadding="0" cellspacing="0" class="inside_form_table">
		<tr>
			<td><?=$PICSel?></td>
			<td valign="bottom">
			&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=pic[]&permitted_type=1', 9)"," name='select_pic'")?><br />
			&nbsp;<?=$button_remove_html?>
			</td>
		</tr>
		</table>
		<span id="div_pic_err_msg"></span>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem_Item_Attachment?></td>
	<td>
		<table id="upload_file_list" class="inside_form_table" cellpadding="0" cellspacing="0" >
			<script language="javascript">
			for(i=0;i<no_of_upload_file;i++)
			{
				//document.writeln('<tr><td><input class="file" type="file" name="attachment_'+i+'" size="40">');
				document.writeln('<tr><td><input class="file" type="file" name="att[]" size="40">');
			    document.writeln('<input type="hidden" name="hidden_attachment_'+i+'"></td></tr>');
			}
			</script>
		</table>
		<input type=button value=" + " onClick="add_field()">
	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['General']['Remark']?></td>
	<td><?=$linterface->GET_TEXTAREA("Remarks", $Remarks);?></td>
</tr>
		
</table>


<div class="edit_bottom_v30">
	<?//=$linterface->GET_ACTION_BTN($Lang['Invoice']['CreateInvoiceOnly'], "button", "javascript:check_form(0);") ?>
	<?=$linterface->GET_ACTION_BTN($Lang['Invoice']['CreateInvoiceWithItems'], "button", "javascript:check_form(1);", 'submit_btn') ?>
	<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:reset_form();") ?>
	<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:back();") ?>
	<p class="spacer"></p>
</div>	


</div>

<input type="hidden" name="flag" value="">
<input type="hidden" name="add_invoice_item" value="0">
<input type="hidden" name="attachment_size" value="<? echo $no_file==""?1:$no_file;?>">
</form>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>




