<?php
// modifying : yat

#################
#
#	Date:	2011-10-11	YatWoon
#			add "print payment voucher"
#
#################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


intranet_auth();
intranet_opendb();

$lf = new libfilesystem();
$linvoice		= new libinvoice();

$data = $linvoice->RetrieveInvoiceAllInfo($RecordID);

$templates = "./payment_voucher.html";
$x = $lf->file_read($templates);

# replace data
// $description = $data['InvoiceDescription'] ? chopString($data['InvoiceDescription'],68,"") : $Lang['General']['EmptySymbol'];
$description_data = $data['InvoiceDescription'] ? $data['InvoiceDescription'] : $Lang['General']['EmptySymbol'];
// $description = "<div style='height: 37px; display:block; overflow:hidden;'>". $data['InvoiceDescription'] ."</div>";
$description = "<iframe src='desc.php?RecordID=$RecordID' width='100%' height='40' marginheight=0 marginwidth=0 frameborder=0 scrolling=no></iframe>";
$group = chopString($linvoice->getAllAdminGroupOfInvoice($RecordID), 34, "");

$sql = "SELECT ChineseName as name FROM INTRANET_USER WHERE UserID=". $data['InputBy'];
$DataEnteredBy = $linvoice->returnVector($sql);	
	
$sql = "SELECT ChineseName as name FROM INTRANET_USER WHERE UserID=". $data['PIC'];
$InvoiceSubmittedBy = $linvoice->returnVector($sql);	
		
$x = str_replace("<!--InvoiceNo--//>",			$data['InvoiceNo'],	$x); 
$x = str_replace("<!--DataEnteredBy--//>",		$DataEnteredBy[0],	$x);
$x = str_replace("<!--InvoiceDate--//>",		$data['InvoiceDate'],$x);
$x = str_replace("<!--Amount--//>",				number_format($data['TotalAmount'],2),$x);
$x = str_replace("<!--Group--//>",				$group,				$x);
$x = str_replace("<!--Description--//>", 		$description,		$x);
$x = str_replace("<!--InvoiceSubmittedBy--//>",	$InvoiceSubmittedBy[0],$x);

intranet_closedb();

echo $x;
?>
