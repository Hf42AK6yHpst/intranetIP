<?php
// modifying : yat

#################
#
#	Date:	2011-10-11	YatWoon
#			add "print payment voucher"
#
#################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


intranet_auth();
intranet_opendb();

$linvoice		= new libinvoice();

$data = $linvoice->RetrieveInvoiceAllInfo($RecordID);
$description_data = $data['InvoiceDescription'] ? $data['InvoiceDescription'] : $Lang['General']['EmptySymbol'];

echo $description_data;

intranet_closedb();
?>
