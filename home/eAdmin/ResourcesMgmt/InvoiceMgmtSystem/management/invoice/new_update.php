<?php
// modifying : 

############ Change Log Start ###############
#
#	Date	:	2020-05-07 Tommy
#				modified Attachment checking, since empty(attachment) result will not be true if there is no attachment
#               change to checking attachment size and error
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linvoice		= new libinvoice();

# retrieve data 
if(empty($InvoiceNo))
{
	$InvoiceNo = $linvoice->GenerateInvoiceNumber($InvoiceDate);
}
$DiscountAmount = $DiscountAmount ? $DiscountAmount : 0;
$item_funding = $item_funding ? $item_funding : 0;
$pic_str = implode(",",$pic);
$pic_str = str_replace("U","",$pic_str);

$sql = "insert into INVOICEMGMT_INVOICE
		(AcademicYearID, InvoiceDate, Company, InvoiceNo, InvoiceDescription, 
		DiscountAmount, TotalAmount, AccountDate, FundingSource, Remarks,
		PIC, DateInput, InputBy)
		values
		($YearID, '$InvoiceDate', '$InvoiceCompany', '$InvoiceNo', '$InvoiceDescription',
		$DiscountAmount, $TotalAmount, '$AccountDate', $item_funding, '$Remarks',
		'$pic_str',	now(), $UserID)
		";
		
$linvoice->db_db_query($sql);
$RecordID = $linvoice->db_insert_id();

### Attachment
$attach = $_FILES['att'];

// if(!empty($attach))
// {
// 	# check dir
// 	$path = "$file_path/file/InvoiceMgmtSysAtt/".$RecordID;
// 	$lf = new libfilesystem();
	
// 	$att_size = sizeof($attach['name']);
	
// 	for($i=0;$i<$att_size;$i++)
// 	{
// 		$filename = stripslashes($attach['name'][$i]);
// 		if(!empty($filename))
// 		{
// 			if (!is_dir($path))
// 			{
// 				$lf->folder_new($path);
// 			}
	
// 			$target = "$path/".$filename;		
// 			$tmp_file = $attach['tmp_name'][$i];
// 			$lf->lfs_copy($tmp_file, $target);
// 		}
// 	}
// }

### Attachment
$attach = $_FILES['att'];
# check dir
$path = "$file_path/file/InvoiceMgmtSysAtt/".$RecordID;
$lf = new libfilesystem();

$att_size = sizeof($attach['name']);

for($i=0;$i<$att_size;$i++)
{
    if($attach["error"][$i] == 0 && $attach['size'][$i] > 0)
    {
        $filename = stripslashes($attach['name'][$i]);
        if(!empty($filename))
        {
            if (!is_dir($path))
            {
                $lf->folder_new($path);
            }
            
            $target = "$path/".$filename;
            $tmp_file = $attach['tmp_name'][$i];
            $lf->lfs_copy($tmp_file, $target);
        }
    }
}

intranet_closedb();

if($add_invoice_item)
{
	header("location: new_item.php?RecordID=$RecordID");	
}
else
{
	header("location: index.php?msg=AddSuccess");	
}


?>

