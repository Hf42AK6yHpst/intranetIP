<?php
// modifying : 

#################
#
#
#################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linvoice		= new libinvoice();

$RecordID = is_array($RecordID) ? $RecordID[0] : $RecordID;

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->isInvoiceCreator($RecordID, $UserID)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}






$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Management_InvoiceList";
$linterface 	= new interface_html();

$lf = new libfilesystem();

#### Retrieve invoice data
$Invoice_Info = $linvoice->RetrieveInvoiceInfo($RecordID);
$this_Invoice = $Invoice_Info[0];
// debug_pr($this_Invoice);
 
# Academic Year Filter
$YearID = $this_Invoice['AcademicYearID'];
$YearSelection = getSelectAcademicYear("YearID","onChange='document.form1.submit();'",1,0,$YearID);

# PIC selection
$namefield = getNameFieldWithClassNumberByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID IN (". $this_Invoice['PIC'] .") ORDER BY EnglishName";
$array_pic = $linvoice->returnArray($sql,2);
// $PICSel = "<select name='pic[]' id='pic[]' size='6' multiple='multiple'></select>";
$PICSel = $linterface->GET_SELECTION_BOX($array_pic, "name='pic[]' ID='pic[]' class='select_studentlist' size='6' multiple='multiple'", "");
$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");

# Funding Source
$arr_funding_source = $linvoice->returnFundingSource();
$item_funding = $this_Invoice['FundingSource'];
$funding_selection = getSelectByArray($arr_funding_source,"name=item_funding", $item_funding);

$InvoiceDate = $this_Invoice['InvoiceDate'];
$InvoiceCompany = $this_Invoice['Company'];
$InvoiceNo = $this_Invoice['InvoiceNo'];
$InvoiceDescription = $this_Invoice['InvoiceDescription'];
$DiscountAmount = $this_Invoice['DiscountAmount'];
$TotalAmount = $this_Invoice['TotalAmount'];
$AccountDate = $this_Invoice['AccountDate']=="0000-00-00" ? "" : $this_Invoice['AccountDate'];
$Remarks = $this_Invoice['Remarks'];

## attachment
$file_list = $linvoice->GetAttachmentListByRecordID($RecordID);
if(!empty($file_list))
{
	$table_content = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\">";
	for($i=0; $i<sizeof($file_list); $i++)
	{
		$last_pos = strrpos($file_list[$i], "/");
		$this_filename = substr($file_list[$i], $last_pos+1);
		$table_content .= "<tr><td class=\"tabletext\"><a href=". $file_list[$i] ." target='_blank'>$this_filename</a></td><td class=\"tabletext\"> [ <a class=\"tablelink\" href=\"remove_invoice_attachment.php?RecordID=$RecordID&filename=$this_filename\">$button_remove</a> ]</td></tr>";
	}
	$table_content .= "</table>";

}

$PAGE_NAVIGATION[] = array($Lang['Invoice']['Invoice'],"index.php");
$PAGE_NAVIGATION[] = array($InvoiceNo,"view.php?RecordID=$RecordID");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);


?>

<script language="javascript">
<!--
var no_of_upload_file = <?=$no_file ==""?1:$no_file;?>;

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	if (document.all)
	{
		var cell = row.insertCell(0);
		//x= '<input class="file" type="file" name="attachment_'+no_of_upload_file+'" size="40">';
		//x+='<input type="hidden" name="hidden_attachment_'+no_of_upload_file+'">';

		x= '<input class="file" type="file" name="att[]" size="40">';
		
		cell.innerHTML = x;
		no_of_upload_file++;
		
		document.form1.attachment_size.value = no_of_upload_file;
	}
}


function reset_innerHtml()
{
 	document.getElementById('div_company_err_msg').innerHTML = "";
 	document.getElementById('div_pic_err_msg').innerHTML = "";
 	document.getElementById('div_discount_err_msg').innerHTML = "";
 	document.getElementById('div_totalamt_err_msg').innerHTML = "";
 	document.getElementById('div_InvoiceDate_err_msg').innerHTML = "";
 	document.getElementById('div_AccountDate_err_msg').innerHTML = "";
}

function check_form()
{
	//// Reset div innerHtml
	reset_innerHtml();
	
	var obj = document.form1;
	var error_no = 0;
	var focus_field = "";
	 
	// Invoice Date 
	if (!check_date_30(obj.InvoiceDate,"<?php echo $i_invalid_date; ?>", "div_InvoiceDate_err_msg")) 
	 {
		 error_no++;
		if(focus_field=="")	focus_field = "InvoiceDate";
	 }
	 
	 if (compareDate('<?=date('Y-m-d')?>', obj.InvoiceDate.value) < 0)
	{
		document.getElementById('div_InvoiceDate_err_msg').innerHTML = '<font color="red"><?=$Lang['Invoice']['InvoiceDateError1']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "InvoiceDate";
	}
	 
	// Company
	if(!check_text_30(obj.InvoiceCompany, "<?php echo $i_alert_pleasefillin.$Lang['Invoice']['InvoiceCompany']; ?>.", "div_company_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "InvoiceCompany";
	}
	
	// Discount Amt
	if(obj.DiscountAmount.value=='')
	{
		obj.DiscountAmount.value=0;
	}
	// Check Discount Amt  > 0 
	if(!check_positive_float_30(obj.DiscountAmount, "<?=$Lang['General']['JS_warning']['InputPositiveValue']?>","div_discount_err_msg"))
	{ 
		error_no++;
		if(focus_field=="")	focus_field = "DiscountAmount";
	}
	
	
	// Total Amt, empty => set 0
	if(obj.TotalAmount.value=='')
	{
		obj.TotalAmount.value=0;
	}
	// Check Total Amt  > 0 
	if( !check_positive_float_30(obj.TotalAmount, "<?=$Lang['General']['JS_warning']['InputPositiveValue']?>","div_totalamt_err_msg"))
	{ 
		error_no++;
		if(focus_field=="")	focus_field = "TotalAmount";
	}
	if( obj.TotalAmount.value==0 )
	{
		document.getElementById('div_totalamt_err_msg').innerHTML = '<font color="red"><?=$Lang['General']['JS_warning']['InputPositiveValue']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "TotalAmount";
	}
	 	
	// Account Date 
	if (!check_date_allow_null_30(obj.AccountDate,"<?php echo $i_invalid_date; ?>", "div_AccountDate_err_msg")) 
	 {
		 error_no++;
		if(focus_field=="")	focus_field = "AccountDate";
	 }
	 
	// PIC
	<? if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {?>
	checkOptionAll(obj.elements["pic[]"]);
	if(obj.elements["pic[]"].length==0)
	{ 
		document.getElementById('div_pic_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$Lang['Invoice']['PIC']?></font>';
		error_no++;
	}
	if(obj.elements["pic[]"].length>1)
	{ 
		document.getElementById('div_pic_err_msg').innerHTML = '<font color="red"><?=$Lang['Invoice']['JS_warning']['PIC_1only']?></font>';
		error_no++;
	}
	<? } ?>
	
	if(error_no>0)
	{
		if(focus_field!="")
		{
			eval("obj." + focus_field +".focus();");
		}
		return false;
	}
	else
	{
		<? if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {?>
		checkOptionAll(obj.elements["pic[]"]);
		<? } ?>
		obj.submit();
// 		Big5FileUploadHandler();
	}
}

function back() {
	self.location.href = "view.php?RecordID=<?=$RecordID?>";
}

//-->
</script>
<form name="form1" action="invoice_edit_update.php" method="post" enctype="multipart/form-data">
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<div class="this_main_content">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
	<td><?=$YearSelection?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['InvoiceDate']?></td>
	<td><?=$linterface->GET_DATE_PICKER("InvoiceDate",$InvoiceDate);?> <span id="div_InvoiceDate_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['InvoiceCompany']?></td>
	<td><input name="InvoiceCompany" type="text" value="<?=$InvoiceCompany?>" class="textboxtext"/><span id="div_company_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['InvoiceNo']?></td>
	<td><input name="InvoiceNo" type="text" value="<?=$InvoiceNo?>" class="textboxnum"/> <span class="tabletextremark"><?=$Lang['Invoice']['InvoiceNo_Remark']?></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['Description']?></td>
	<td><?=$linterface->GET_TEXTAREA("InvoiceDescription", $InvoiceDescription);?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['DiscountAmount']?></td>
	<td>$ <input name="DiscountAmount" type="text" value="<?=$DiscountAmount?>" class="textboxnum" value="0"/> <span id="div_discount_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['TotalAmount']?></td>
	<td>$ <input name="TotalAmount" type="text" value="<?=$TotalAmount?>" class="textboxnum" value="0"/> <span id="div_totalamt_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['FundingSource']?></td>
	<td><?=$funding_selection?></td>
</tr> 

<tr>
	<td class="field_title"><?=$Lang['Invoice']['AccountDate']?></td>
	<td><?=$linterface->GET_DATE_PICKER("AccountDate",$AccountDate,"","","","","","","","",1);?> <span id="div_AccountDate_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['PIC']?></td>
	<td>
		<? if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {?>
			<table border="0" cellpadding="0" cellspacing="0" class="inside_form_table">
			<tr>
				<td><?=$PICSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=pic[]&permitted_type=1', 9)"," name='select_pic'")?><br />
				&nbsp;<?=$button_remove_html?>
				</td>
			</tr>
			</table>
		<? } else {?>
			<?=$array_pic[0][1];?>
		<? } ?>
		<span id="div_pic_err_msg"></span>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem_Item_Attachment?></td>
	<td>
		<?=$table_content?>
		<table id="upload_file_list" class="inside_form_table" cellpadding="0" cellspacing="0" >
			<script language="javascript">
			for(i=0;i<no_of_upload_file;i++)
			{
				document.writeln('<tr><td><input class="file" type="file" name="att[]" size="40">');
			    document.writeln('<input type="hidden" name="hidden_attachment_'+i+'"></td></tr>');
			}
			</script>
		</table>
		<input type=button value=" + " onClick="add_field()">
	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['General']['Remark']?></td>
	<td><?=$linterface->GET_TEXTAREA("Remarks", $Remarks);?></td>
</tr>
		
</table>


<div class="edit_bottom_v30">
	<?=$linterface->GET_ACTION_BTN($button_submit, "button", "javascript:check_form();") ?>
	<?=$linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:back();") ?>
	<p class="spacer"></p>
</div>	


</div>

<input type="hidden" name="RecordID" value="<?=$RecordID?>">
<input type="hidden" name="flag" value="">
<input type="hidden" name="attachment_size" value="<? echo $no_file==""?1:$no_file;?>">
</form>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>




