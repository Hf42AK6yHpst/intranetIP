<?php
# using: yat


/*************************************************************************
 *  modification log
 * 
 * 
 * ************************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

// debug_pr("isAsset: " . $isAsset);
// debug_pr("add_next: ". $add_next);

$item_chi_name = $item_chi_name1;
$item_eng_name = $item_eng_name1;
$item_total_qty = $item_total_qty1;

if(!$isAsset)		### No need input to eInventory
{	
	### insert data to INVOICEMGMT_ITEM, INVOICEMGMT_GROUP_BUDGET_USED
	$dataAry = array();
	$dataAry['InvoiceRecordID'] = $RecordID; 
	$dataAry['NameChi'] = $item_chi_name; 
	$dataAry['NameEng'] = $item_eng_name; 
	$dataAry['Price'] = $item_price; 
	$dataAry['Quantity'] = $item_total_qty; 
	$dataAry['CategoryID'] = $ItemCategory; 
	$dataAry['ResourceMgmtGroup'] = $GroupID; 
	$dataAry['IsAssetItem'] = $isAsset; 
	
	$ItemID = $linvoice->INSERT_ITEM_RECORD($dataAry);
	
	### Add other item or not
	if($add_next)
	{
		header("location: new_item.php?RecordID=$RecordID&msg=AddSuccess&GroupID=$GroupID&ItemCategory=$ItemCategory");
	}
	else
	{
		# found out all groups in this invoice and check any group need send email notification
		$data = $linvoice->RetrieveInvoiceBasicInfo($RecordID);
		$GroupIDAry = $linvoice->getAllAdminGroupIDOfInvoice($RecordID);
		$linvoice->CheckOverBudget($data[0]['AcademicYearID'], $GroupIDAry);
		
		header("location: invoice_budget_summary.php?RecordID=$RecordID");
	}
}
else			### eInventory data
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libinventory.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	$linventory = new libinventory();
	$CurrentPageArr['InvoiceMgmtSystem'] = 1;
	$CurrentPage	= "Management_InvoiceList";
	$linterface		= new interface_html();
	
	$STEPS_OBJ[] = array($Lang['Invoice']['EnterItemInfo'], 0);
	$STEPS_OBJ[] = array($Lang['Invoice']['EnterInventoryItemInfo1'],1);
	$STEPS_OBJ[] = array($Lang['Invoice']['EnterInventoryItemInfo2'],0);
	$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
	$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START();
	
	### Retrieve Invoice details
	$Invoice_Info = $linvoice->RetrieveInvoiceBasicInfo($RecordID);
	$item_purchase_date = $Invoice_Info[0]['InvoiceDate'];
	$item_supplier = $Invoice_Info[0]['Company'];
	$item_invoice = $Invoice_Info[0]['InvoiceNo'];
	$item_funding = $Invoice_Info[0]['FundingSource'];
	
	### Invoice item details
	$item_purchase_price = $item_price;
	
	### Type
	$arr_item_type = array(array(1,$i_InventorySystem_ItemType_Single),array(2,$i_InventorySystem_ItemType_Bulk));
	$item_type_selection = getSelectByArray($arr_item_type,"name=targetItemType onChange=\"ajax_change_type();\"",$targetItemType,0,1);

	### Category 
	$arr_category = $linventory->getCategoryName("",0);
	$category_selection = getSelectByArray($arr_category, " name=\"targetCategory\" onChange=\"ajax_change_category(1);\" ",$targetCategory);

	### Sub-category 
	$subcategory_selection = "<select name='targetCategory2'><option>-- $button_select --</option></select>";	

	### Funding Source
	$arr_funding_source = $linventory->returnFundingSource();
	$funding_selection = getSelectByArray($arr_funding_source,"name=item_funding", $item_funding);

	### Purchase Details 
	$purchase_details_selection = "<select name='purchaseType'><option>-- $button_select --</option></select>";	

	### Existing ITem
	$existing_item_selection = "<select name='targetExistItem'><option>-- $button_select --</option></select>";	
	
	### Variance Manager
	$arr_bulk_item_admin = $linventory->returnAdminGroup();
	$bulk_item_admin_selection = getSelectByArray($arr_bulk_item_admin, "name=\"bulk_item_admin\"", $bulk_item_admin, 0, 0);


?>	
	<script language="javascript">
	<!--
	var no_of_upload_file = <?=$no_file ==""?5:$no_file;?>;
	
	function ajax_change_type()
	{
		var typeid = document.form1.targetItemType.value;
		
		reset_innerHtml();
		
		///// Purchase Details
		if(typeid==1)	// Single item
		{
			document.form1.purchaseType.selectedIndex = 0;			
			document.getElementById('tr_purchase_details').style.display='none';
			document.getElementById('tr_funding').style.display='';
			document.form1.bulk_item_admin.selectedIndex = 0;
			document.getElementById('tr_variance_manager').style.display='none';
			<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
			document.getElementById('tr_brand').style.display='';
			<? } ?>
			
			document.form1.targetExistItem.selectedIndex = 0;		
			document.getElementById('tr_existing_item').style.display='none';
			
			<? if(!$targetCategory) {?>
				selectNewItem();
			<? } ?>
		}
		else
		if(typeid==2)	// Bulk item
		{
			document.getElementById('tr_purchase_details').style.display='';
			document.getElementById('tr_funding').style.display='none';
			document.getElementById('tr_variance_manager').style.display='';
			<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
			document.getElementById('tr_brand').style.display='none';
			<? } ?>
		}
	}
	
	function ajax_change_category(fs)
	{
		var typeid = document.form1.targetItemType.value;
		var categoryid = document.form1.targetCategory.value;
		
		var categoryid2 = document.form1.targetCategory2.value;
		var categoryid2_pass = '<?=$targetCategory2?>';
		if(categoryid2=='' && categoryid2_pass!='')
				categoryid2 = categoryid2_pass;
		
		var purchaseTypeid = document.form1.purchaseType.value;
		var purchaseTypeid_pass = '<?=$purchase_type?>';
		if(purchaseTypeid=='' && purchaseTypeid_pass!='')
			purchaseTypeid = purchaseTypeid_pass;
		if(fs==1)	purchaseTypeid='';
		
		$('#div_subcategory').load
		(
			'ajax_load_form.php', 
			{	
				Action: "SubCategory",
				CategoryID: categoryid,
				targetCategory2: categoryid2
			}, 
			function (data) { }
		); 
		
		$('#div_purchase_type').load
		(
			'ajax_load_form.php', 
			{	
				Action: "Purchase_Details",
				targetItemType: typeid,
				targetCategory: categoryid,
				targetCategory2: categoryid2,
				purchaseType: purchaseTypeid
			}, 
			function (data) { }
		); 
		
		if(categoryid2=='')
		{
			document.form1.purchaseType.selectedIndex = 0;
			document.form1.targetExistItem.selectedIndex = 0;		
			document.getElementById('tr_existing_item').style.display='none';
			
			var purchaseTypeid = document.form1.purchaseType.value;
			var purchaseTypeid_pass = '<?=$purchase_type?>';
			if(purchaseTypeid=='' && purchaseTypeid_pass!='')
				purchaseTypeid = purchaseTypeid_pass;
			
			if(purchaseTypeid!=2)
			{
				selectNewItem();
			}
		}
	}
	
	function ajax_change_subcategory(fs)
	{
		var typeid = document.form1.targetItemType.value;
		var categoryid = document.form1.targetCategory.value;
		var subcategoryid = document.form1.targetCategory2.value;
		var subcategoryid_pass = '<?=$targetCategory2?>';
		if(subcategoryid=='' && subcategoryid_pass!='')
			subcategoryid = subcategoryid_pass;
		
		var purchaseTypeid = document.form1.purchaseType.value;
		var purchaseTypeid_pass = '<?=$purchase_type?>';
		if(purchaseTypeid=='' && purchaseTypeid_pass!='')
			purchaseTypeid = purchaseTypeid_pass;
		
		if(fs==1)
			purchaseTypeid='';
			
		$('#div_purchase_type').load
		(
			'ajax_load_form.php', 
			{	
				Action: "Purchase_Details",
				targetItemType: typeid,
				targetCategory: categoryid,
				targetCategory2: subcategoryid,
				purchaseType: purchaseTypeid
			}, 
			function (data) { }
		); 
		document.form1.targetExistItem.selectedIndex = 0;		
		document.getElementById('tr_existing_item').style.display='none';
		
		if(purchaseTypeid==2)
		{
			selectNewItem(); 
		}
	}
	
	var imgObj=0;
	var callback = {
	        success: function ( o )
	        {
	                writeToLayer('ToolMenu2',o.responseText);
	                showMenu2("img_"+imgObj,"ToolMenu2");
	        }
	}
	function hideMenu2(menuName){
			objMenu = document.getElementById(menuName);
			if(objMenu!=null)
				objMenu.style.visibility='hidden';
			setDivVisible(false, menuName, "lyrShim2");
	}
	function showMenu2(objName,menuName){
			  hideMenu2('ToolMenu2');
	           objIMG = document.getElementById(objName);
				offsetX = (objIMG==null)?0:objIMG.width;
				offsetY =0;             	 	
	            var pos_left = getPostion(objIMG,"offsetLeft");
				var pos_top  = getPostion(objIMG,"offsetTop");
				
				objDiv = document.getElementById(menuName);
				
				if(objDiv!=null){
					objDiv.style.visibility='visible';
					objDiv.style.top = pos_top+offsetY+"px";
					objDiv.style.left = pos_left+offsetX+"px";
					setDivVisible(true, menuName, "lyrShim2");
				}
	}
	function showInfo(val)
	{
	        obj = document.form1;
	
	        var myElement = document.getElementById("ToolMenu2");
	
	        writeToLayer('ToolMenu2','');
	        imgObj = val;
	        
			showMenu2("img_"+val,"ToolMenu2");
	        YAHOO.util.Connect.setForm(obj);
	
	        var path = "getHelpInfo2.php?Val=" + val;
	        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
	}
	
	function add_field()
	{
		var table = document.getElementById("upload_file_list");
		var row = table.insertRow(no_of_upload_file);
		if (document.all)
		{
			var cell = row.insertCell(0);
			x= '<input class="file" type="file" name="item_attachment_'+no_of_upload_file+'" size="40">';
			x+='<input type="hidden" name="hidden_item_attachment_'+no_of_upload_file+'">';
	
			cell.innerHTML = x;
			no_of_upload_file++;
			
			document.form1.attachment_size.value = no_of_upload_file;
		}
	}
	
	function changePurchaseType(fs) 
	{
		var purchaseTypeid = document.form1.purchaseType.value;
		var purchaseTypeid_pass = '<?=$purchase_type?>';
		if(purchaseTypeid=='' && purchaseTypeid_pass!='')
			purchaseTypeid = purchaseTypeid_pass;
		
		var targetExistItem = document.form1.targetExistItem.value;
		var targetExistItem_pass = '<?=$targetExistItem?>';
		if(targetExistItem=='' && targetExistItem_pass!='')
			targetExistItem = targetExistItem_pass;
	
		if(fs==1)		
			targetExistItem = '';
		if(purchaseTypeid==2)	// Existing
		{
			var typeid = document.form1.targetItemType.value;
			var categoryid = document.form1.targetCategory.value;
			
			var subcategoryid = document.form1.targetCategory2.value;
			var subcategoryid_pass = '<?=$targetCategory2?>';
			if(subcategoryid=='' && subcategoryid_pass!='')
				subcategoryid = subcategoryid_pass;
			
			document.getElementById('tr_existing_item').style.display='';
			
			$('#div_existing_item').load
			(
				'ajax_load_form.php', 
				{	
					Action: "Retrieve_Existing_Item",
					targetItemType: typeid,
					targetCategory: categoryid,
					targetCategory2: subcategoryid,
					targetExistItem: targetExistItem
				}, 
				function (data) { }
			); 
			
			if(targetExistItem!='' && fs!=1)
				selectExistingItem();
		}	
		else					
		{
			document.form1.targetExistItem.selectedIndex = 0;		
			document.getElementById('tr_existing_item').style.display='none';
			
			var purchaseTypeid = document.form1.purchaseType.value;
			var purchaseTypeid_pass = '<?=$purchase_type?>';
			if(purchaseTypeid=='' && purchaseTypeid_pass!='')
				purchaseTypeid = purchaseTypeid_pass;
			
			if(purchaseTypeid!=2 && fs==1)
			{
				selectNewItem();
			}
		}
	}
	
	function selectExistingItem()
	{
		var categoryid = document.form1.targetCategory.value;
		var subcategoryid = document.form1.targetCategory2.value;
		var subcategoryid_pass = '<?=$targetCategory2?>';
		if(subcategoryid=='' && subcategoryid_pass!='')
			subcategoryid = subcategoryid_pass;
				
		var ExistingItemID = document.form1.targetExistItem.value;
		var ExistingItemID_pass = '<?=$targetExistItem?>';
		if(ExistingItemID=='' && ExistingItemID_pass!='')
			ExistingItemID = ExistingItemID_pass;
		
		if(ExistingItemID > "")
		{
			$('#div_js').load
			(
				'ajax_load_form.php', 
				{	
					Action: "Retrieve_Existing_Item_Info",
					targetCategory: categoryid,
					targetCategory2: subcategoryid,
					ExistingItemID: ExistingItemID
				}, 
				function (data) { 
					
					}
			); 
		}
		else
		{
			var purchaseTypeid = document.form1.purchaseType.value;
			var purchaseTypeid_pass = '<?=$purchase_type?>';
			if(purchaseTypeid=='' && purchaseTypeid_pass!='')
				purchaseTypeid = purchaseTypeid_pass;
			
			if(purchaseTypeid!=2)
				selectNewItem();
		}
	}
	
	function selectNewItem()
	{
		$('#div_js').load
			(
				'ajax_load_form.php', 
				{	
					Action: "Reset_New_Item"
				}, 
				function (data) { 
					
					}
			); 
	}
	
	function AutoFillIn(TargetObj, Obj) {
		TargetObj.value = Obj.value;
	}
	
	function checkForm()
	{
		var obj=document.form1;
		var error_no = 0;
		var focus_field = "";
		
		//// Reset div innerHtml
		reset_innerHtml();
		
		var passed = 1;
		var ItemUnitPrice = obj.item_unit_price;
		var ItemFunding = obj.item_funding;
		var item_type = obj.targetItemType.value;
		
		// Category
		if(!check_select_30(obj.targetCategory, "<?php echo $i_alert_pleaseselect.$i_InventorySystem['Category']; ?>.","","div_Category_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "targetCategory";  
		}
		
		// SubCategory
		if(!check_select_30(obj.targetCategory2, "<?php echo $i_alert_pleaseselect.$i_InventorySystem['SubCategory']; ?>.","","div_SubCategory_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "targetCategory2";  
		}
		
		// Purchase Details
		if(item_type==2)
		{
			if(!check_select_30(obj.purchaseType, "<?php echo $i_alert_pleaseselect.$i_InventorySystem_PurchaseDetails; ?>.","","div_purchase_details_err_msg"))
			{
				error_no++;
				if(focus_field=="")	focus_field = "purchaseType";  
			}
			
			if(obj.purchaseType.value==2)
			{
				if(!check_select_30(obj.targetExistItem, "<?php echo $i_alert_pleaseselect.$i_InventorySystem_Item_Existing; ?>.","","div_existing_item_err_msg"))
				{
					error_no++;
					if(focus_field=="")	focus_field = "targetExistItem";  
				}
			}
		}
		
		// Item Name (Chinese)
		if(!check_text_30(obj.item_chi_name, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_Item_ChineseName; ?>.", "div_ChiName_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "item_chi_name";
		}
		
		// Item Name (English)
		if(!check_text_30(obj.item_eng_name, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_Item_EnglishName; ?>.", "div_EngName_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "item_eng_name";
		}
		
		// Funding
		if(item_type==1)
		{
			if(!check_select_30(obj.item_funding, "<?php echo $i_alert_pleaseselect.$i_InventorySystem_Item_Funding; ?>.","","div_Funding_err_msg"))
			{
				error_no++;
				if(focus_field=="")	focus_field = "item_funding";  
			}
		}
		
		<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
		// Purchase Price
		if(obj.item_purchase_price.value!='' && !checkUnitPrice(obj.item_purchase_price,obj.item_purchase_price.value))
		{
			document.getElementById('div_PurchasePrice_err_msg').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidPurchasePrice']?></font>";
			error_no++;
			if(focus_field=="")	focus_field = "item_purchase_price";
		}
		<? } ?>
		
		// Unit Price
		if(ItemUnitPrice.value!='' && !checkUnitPrice(ItemUnitPrice,ItemUnitPrice.value))
		{
			document.getElementById('div_UnitPrice_err_msg').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidUnitPrice']?></font>";
			error_no++;
			if(focus_field=="")	focus_field = "item_unit_price";
		}
		
		// Quantity
		if(!check_text_30(obj.item_total_qty, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_NumOfItemAdd; ?>.", "div_Qty_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "item_total_qty";
		}
		else
		{
			if(!isInteger(obj.item_total_qty.value))
			{
				document.getElementById('div_Qty_err_msg').innerHTML = "<font color=red><?=$i_InventorySystem_StockCheck_ValidQuantityWarning;?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "item_total_qty";
			}
		}
		
		if(error_no>0)
		{
			eval("obj." + focus_field +".focus();");
			return false;
		}
		else
		{
			
			obj.category_id.value = obj.targetCategory.value;
			obj.category2_id.value = obj.targetCategory2.value;
			obj.purchase_type.value = obj.purchaseType.value;
			
			Big5FileUploadHandler();
			document.form1.action = "new_item3.php";
			return true;
		}
		
	}
	
	function Big5FileUploadHandler() 
	{
		for(j=0;j<=no_of_upload_file;j++)
		{
			objFile = eval('document.form1.item_attachment_'+j);
			objHiddenFile = eval('document.form1.hidden_item_attachment_'+j);
			if(objFile!=null && objFile.value!='' && objHiddenFile!=null)
			{
				var Ary = objFile.value.split('\\');
				objHiddenFile.value = Ary[Ary.length-1];
			}
		}
		
		return true;
	}
	
	function reset_innerHtml()
	{
	 	document.getElementById('div_EngName_err_msg').innerHTML = "";
	  	document.getElementById('div_ChiName_err_msg').innerHTML = "";
	 	document.getElementById('div_Funding_err_msg').innerHTML = "";
	 	document.getElementById('div_Category_err_msg').innerHTML = "";
	 	document.getElementById('div_SubCategory_err_msg').innerHTML = "";
	 	document.getElementById('div_Qty_err_msg').innerHTML = "";
	 	document.getElementById('div_UnitPrice_err_msg').innerHTML = "";
	 	
	 	<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
	 	document.getElementById('div_PurchasePrice_err_msg').innerHTML = "";
	 	<? } ?>
	 	document.getElementById('div_purchase_details_err_msg').innerHTML = "";
	 	document.getElementById('div_existing_item_err_msg').innerHTML = "";
	 	
	}
	
	function checkUnitPrice(fieldName, fieldValue) {
		decallowed = 2;  // how many decimals are allowed?
		
		if (isNaN(fieldValue) || fieldValue == "") {
			return false;
		}
		else {
			if (fieldValue.indexOf('.') == -1) fieldValue += ".";
			dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);
	
			if (dectext.length > decallowed)
			{
				return false;
	      	}
			else {
				return true;
	      	}
	   	}
	}
	
	function synContentTo(targetObj, srcContent)
	{
		if (targetObj.value=="")
		{
			targetObj.value = srcContent;
		}
	}
	
	//-->
	</script>
	<div id="div_js"><!-- javascript //--></div>

	<?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>

	<form name="form1" enctype="multipart/form-data" method="post" action="" onSubmit="return checkForm()">

	<div class="this_table">
	<table class="form_table_v30">
	
	<tr>
		<td class="field_title"><?=$i_general_Type?></td>
		<td><?=$item_type_selection?></td>
	</tr>
	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem['Category']?></td>
		<td><?=$category_selection?>
		<span id="div_Category_err_msg"></span></td>
	</tr>
	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem['SubCategory']?></td>
		<td><div id="div_subcategory"><?=$subcategory_selection?> <span id="div_SubCategory_err_msg"></span></div></td>
	</tr>
	
	<tr id="tr_purchase_details" style="display:none">
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_PurchaseDetails?></td>
		<td><span id="div_purchase_type"><?=$purchase_details_selection?></span> <a href="javascript:showInfo(3)"><img id='img_3' src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif" border=0></a> 
		<span id="div_purchase_details_err_msg"></span></td>
	</tr>
	
	<tr id="tr_existing_item" style="display:none">
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_Existing?></td>
		<td><div id="div_existing_item"><?=$existing_item_selection?></div> <span id="div_existing_item_err_msg"></span></td>
	</tr>
	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_ChineseName?></td>
		<td><input name="item_chi_name" type="text" value="<?=$item_chi_name?>" class="textboxtext" onBlur="synContentTo(this.form.item_eng_name, this.value)" />
		<span id='div_ChiName_err_msg'></span>
		</td>
	</tr>
	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_EnglishName?></td>
		<td><input name="item_eng_name" type="text" value="<?=$item_eng_name?>" class="textboxtext" onBlur="synContentTo(this.form.item_chi_name, this.value)" />
		<span id='div_EngName_err_msg'></span>
		</td>
	</tr>
	<? if(!$sys_custom['eInventoryCustForSMC']) {?>
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_ChineseDescription?></td>
		<td><?=$linterface->GET_TEXTAREA("item_chi_discription", $item_chi_discription);?></td>
	</tr>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_EnglishDescription?></td>
		<td><?=$linterface->GET_TEXTAREA("item_eng_discription", $item_eng_discription);?></td>
	</tr>
	<? } ?>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Ownership?> </td>
		<td>
			<input name="item_ownership" id="ownership1" type="radio" value=1 <?=(($item_ownership == "") OR ($item_ownership == 1)) ? "checked":""?> <?=$disable?>> <label for="ownership1"><?=$i_InventorySystem_Ownership_School?></label> 
			<input name="item_ownership" id="ownership2" type="radio" value=2 <?=($item_ownership == 2) ? "checked":""?> <?=$disable?>> <label for="ownership2"><?=$i_InventorySystem_Ownership_Government?></label>
			<input name="item_ownership" id="ownership3" type="radio" value=3 <?=($item_ownership == 3) ? "checked":""?> <?=$disable?>> <label for="ownership3"><?=$i_InventorySystem_Ownership_Donor?></label>
		</td>
	</tr>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Photo?></td>
		<td>
			<input type="file" name="item_photo" class="file" <?=$disable?>>
			<input type="hidden" name="hidden_item_photo"><br/>
			<span class="tabletextremark"><?=$i_InventorySystem_PhotoGuide?></span>
		</td>
	</tr>
	
	<tr id="tr_funding" style="display:table-row"">
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_Funding?></td>
		<td colspan="3" class="tabletext" valign="top"><?=$funding_selection?><span id='div_Funding_err_msg'></span></td>
	</tr>
	
	<tr id="tr_variance_manager" style="display:none">
		<td class="field_title"><?=$i_InventorySystem_Settings_BulkItemAdmin?></td>
		<td colspan="3" class="tabletext" valign="top"><?=$bulk_item_admin_selection?></td>
	</tr>
				
	<? if(!$sys_custom['eInventoryCustForSMC']) {?>				
	<tr id="tr_brand" style="display:table-row"">
		<td class="field_title"><?=$i_InventorySystem_Item_Brand_Name?></td>
		<td><input name="item_brand" type="text" value="<?=$item_brand?>" class="textboxtext"></td>
	</tr>
								
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Supplier_Name?></td>
		<td><input name="item_supplier" type="text" value="<?=$item_supplier?>" class="textboxtext"></td>
	</tr>							
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Supplier_Contact?></td>
		<td><input name="item_supplier_contact" type="text" value="<?=$item_supplier_contact?>" class="textboxtext"></td>
	</tr>	
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Supplier_Description?></td>
		<td><input name="item_supplier_description" type="text" value="<?=$item_supplier_description?>" class="textboxtext"></td>
	</tr>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Quot_Num?></td>
		<td><input name="item_quotation" type="text" value="<?=$item_quotation?>" class="textboxtext"></td>
	</tr>	
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Tender_Num?></td>
		<td><input name="item_tender" type="text" value="<?=$item_tender?>" class="textboxtext"></td>
	</tr>	
	<? } ?>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Invoice_Num?></td>
		<td><input name="item_invoice" type="text" value="<?=$item_invoice?>" class="textboxtext"></td>
	</tr>	
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Purchase_Date?></td>
		<td><?=$linterface->GET_DATE_PICKER("item_purchase_date",$item_purchase_date);?></td>
	</tr>	
		
	<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_Price?> <a href="javascript:showInfo(1)"><img id='img_1' src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif" border=0></a></td>
		<td>$ <input name="item_purchase_price" type="text" value="<?=$item_purchase_price ? $item_purchase_price : 0?>" class="textboxnum"> <span id="div_PurchasePrice_err_msg"></span></td>
	</tr>	
	<? } ?>
	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Unit_Price?> <a href="javascript:showInfo(2)"><img id='img_2' src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif" border=0></a></td>
		<td>$ <input name="item_unit_price" type="text" value="<?=$item_unit_price ? $item_unit_price : 0?>" class="textboxnum"> <span id="div_UnitPrice_err_msg"></span></td>
	</tr>	
	
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_NumOfItemAdd?></td>
		<td><input name="item_total_qty" type="text" value="<?=$item_total_qty?>" class="textboxnum"> <span id="div_Qty_err_msg"></span></td>
	</tr>	
	
	<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
	<tr>
		<td class="field_title"><?=$i_InventorySystemItemMaintainInfo?></td>
		<td><?=$linterface->GET_TEXTAREA("item_maintain_info", $item_maintain_info);?></td>
	</tr>
	<? } ?>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Remark?></td>
		<td><?=$linterface->GET_TEXTAREA("item_remark", $item_remark);?></td>
	</tr>
	
	<tr>
		<td class="field_title"><?=$i_InventorySystem_Item_Attachment?></td>
		<td>
			<table id="upload_file_list" class="inside_form_table" cellpadding="0" cellspacing="0" >
				<script language="javascript">
				for(i=0;i<no_of_upload_file;i++)
				{
					document.writeln('<tr><td><input class="file" type="file" name="item_attachment_'+i+'" size="40">');
				    document.writeln('<input type="hidden" name="hidden_item_attachment_'+i+'"></td></tr>');
				}
				</script>
			</table>
			<input type=button value=" + " onClick="add_field()">
		</td>
	</tr>
	
	
	
	
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$linterface->GET_ACTION_BTN($button_next, "submit");?> 
	<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript: reset_innerHtml(); this.form.reset();","reset2");?> 
	<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='items_full_list.php'","cancelbtn");?>
	<p class="spacer"></p>
	</div>
	 
	</div>
	
	<input type="hidden" name="exist_item_code" value="">
	<input type="hidden" name="purchase_type" value="">
	<input type="hidden" name="category_id" value="">
	<input type="hidden" name="category2_id" value="">
	<input type="hidden" name="attachment_size" value="<? echo $no_file==""?5:$no_file;?>">
	
	<input type="hidden" name="RecordID" value="<?=$RecordID?>">
	<input type="hidden" name="item_chi_name1" value="<?=$item_chi_name1?>">
	<input type="hidden" name="item_eng_name1" value="<?=$item_eng_name1?>">
	<input type="hidden" name="item_price" value="<?=$item_price?>">
	<input type="hidden" name="item_total_qty1" value="<?=$item_total_qty1?>">
	<input type="hidden" name="ItemCategory" value="<?=$ItemCategory?>">
	<input type="hidden" name="GroupID" value="<?=$GroupID?>">
	</form>
	
	
	<script language="javascript">
	<!--
	<? if($targetCategory) {?>
		ajax_change_type();
		ajax_change_category(0);
		ajax_change_subcategory(0);
		changePurchaseType(0);
		
		<? if($targetExistItem) {?>
			selectExistingItem();
		<? } ?>
	<? } ?>
	
	//-->
	</script>

	 
	
	
	
	
	
	
	
	
<?	
	$linterface->LAYOUT_STOP();
}


intranet_closedb();

?>