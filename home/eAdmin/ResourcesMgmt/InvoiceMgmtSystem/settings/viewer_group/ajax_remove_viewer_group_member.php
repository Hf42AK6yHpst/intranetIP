<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");



intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

$result = $linvoice->removeViewerGroupMember($userID);

intranet_closedb();

if(sizeof($result) && in_array(false, $result)) 
	$msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
else 
	$msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];

echo $msg;
?>