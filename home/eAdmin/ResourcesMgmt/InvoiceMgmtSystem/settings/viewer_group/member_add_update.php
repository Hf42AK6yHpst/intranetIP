<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");


$tempUserAry = array();
for($i=0, $i_max=sizeof($student); $i<$i_max; $i++) {
	$tempUserAry[] = (is_numeric($student[$i])) ? $student[$i] : substr($student[$i],1);
}


$linvoice = new libinvoice();

$result = $linvoice->addViewerGroupMember($tempUserAry);

intranet_closedb();

if(sizeof($result) && in_array(false, $result)) 
	$msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
else 
	$msg = $Lang['General']['ReturnMessage']['AddSuccess'];

header("Location:index.php?msg=$msg");
?>