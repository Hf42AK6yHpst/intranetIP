<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linvoice		= new libinvoice();

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['Category']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

# table action button
$BtnArr = array();
$BtnArr[] = array("edit", "javascript:checkEdit(document.form1,'CategoryID[]','edit.php')", $button_edit);
$BtnArr[] = array("delete", "javascript:checkRemove(document.form1,'CategoryID[]','remove_update.php')", $button_delete);
		
		
if ($page_size_change == 1)
{
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
		
$li = new libdbtable2007($field, $order, $pageNo);
	
//$name_field = getNamefieldByLang("USR.");	
$sql = "
	SELECT 
		".get_Lang_Selection("NameChi", "NameEng")." as Name,
		IF(DateModified IS NULL,'-', DateModified) as DateModified,
		CONCAT('<input type=\'checkbox\' name=\'CategoryID[]\' id=\'CategoryID[]\' value=', CategoryID ,'>')
	FROM
		INVOICEMGMT_ITEM_CATEGORY	
	WHERE
		RecordStatus=".RECORDSTATUS_ACTIVE;	
	
$li->sql = $sql;
$li->field_array = array("Name", "DateModified");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='70%' >".$li->column_IP25($pos++, $Lang['Invoice']['CategoryName'])."</th>\n";
$li->column_list .= "<th width='30%' >".$li->column_IP25($pos++, $Lang['General']['LastModified'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("CategoryID[]")."</th>\n";		

//$linvoiceUI = new libinvoice_ui();

$actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);
?>

<script language="javascript">


</script>
<form name="form1" method="POST" action="">

<div class="Conntent_tool">
	<a href="new.php" class="new"><?=$button_new?></a>
</div>
<br style="clear:both">

<?=$actionBtn?>

<?=$li->display()?>

<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />

</form>


<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>
