<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linvoice		= new libinvoice();

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['Category']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$catID = $CategoryID[0];

$data = $linvoice->getCategoryNameById($catID);

?>
<script language="javascript">
<!--
function checkForm() {
	reset_innerHtml();
	
	var obj = document.form1;
	var error_no = 0
	
	if(Trim(obj.TitleEng.value)=="") {
		document.getElementById('div_eng_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleasefillin.$Lang['Invoice']['CategoryTitle']." (".$Lang['Invoice']['Eng'].")"?></font>';
		error_no++;
	}
	if(Trim(obj.TitleChi.value)=="") {
		document.getElementById('div_chi_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleasefillin.$Lang['Invoice']['CategoryTitle']." (".$Lang['Invoice']['Chi'].")"?></font>';
		error_no++;
	}
	
	if(error_no>0)
	{
		return false;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_eng_err_msg').innerHTML = "";
 	document.getElementById('div_chi_err_msg').innerHTML = "";
}

function goBack() {
	self.location.href = "index.php";
}

<? 
if($msg!="") {
	echo "Get_Return_Message(\"".$msg."\");\n";	
}
?>
//-->
</script>
<form name="form1" method="POST" action="edit_update.php" onSubmit="return checkForm()">

<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation_v30 navigation_2">
				<a href="javascript:goBack()"><?=$Lang['Invoice']['CategoryList']?></a>
				<span><?=$Lang['Invoice']['EditCategory']?> </span>
			</div>
			<p class="spacer"></p>
			<div class="table_board">
				<table class="form_table_v30 ">
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['CategoryTitle']." (".$Lang['Invoice']['Eng'].")"?></td>
						<td>
							<input name="TitleEng" id="TitleEng" type="text" class="textboxtext" value="<?=$data['NameEng']?>" />
							<div id="div_eng_err_msg"></div>
						</td>
					</tr>
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['CategoryTitle']." (".$Lang['Invoice']['Chi'].")"?></td>
						<td>
							<input name="TitleChi" id="TitleChi" type="text" class="textboxtext" value="<?=$data['NameChi']?>" />
							<div id="div_chi_err_msg"></div>
						</td>
					</tr>
				</table>
				<br style="clear:both">
				<div class="edit_bottom">
					<?
						echo $linterface->GET_ACTION_BTN($button_submit, "submit", "");
						echo "&nbsp;";
						echo $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()");
					?>
				</div>
			</div>
		</td>
	</tr>
</table>

<input type="hidden" name="CategoryID" id="CategoryID" value="<?=$catID?>">
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>