<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice		= new libinvoice();

$category_ary = $linvoice->returnGroupCategoriesBudget($YearID, $AdminGroupID);
if(sizeof($category_ary) > 0)
{
	for($i=0; $i<sizeof($category_ary); $i++)
	{
		list($caetgory_id, $category_name, $this_budget) = $category_ary[$i];
		$this_category_budget = ${"Category_".$caetgory_id};
		
		# check insert or update
		$sql = "select RecordID from INVOICEMGMT_GROUP_BUDGET where AcademicYearID=$YearID and AdminGroupID=$AdminGroupID and CategoryID=$caetgory_id";
		$result = $linvoice->returnVector($sql);
		$RecordID = $result[0];
		if(!empty($RecordID))		# update
		{
			$sql = "update INVOICEMGMT_GROUP_BUDGET set Budget=$this_category_budget, DateModified=now(), ModifyBy=$UserID where RecordID=$RecordID";
		}
		else						# insert
		{
			$sql = "insert into INVOICEMGMT_GROUP_BUDGET 
					(AcademicYearID, AdminGroupID, CategoryID, Budget, DateInput, InputBy)
					values
					($YearID, $AdminGroupID, $caetgory_id, $this_category_budget, now(), $UserID)
					";
		}
		$linvoice->db_db_query($sql) or die(mysql_error());
	}
}

intranet_closedb();

header("location: edit.php?YearID=$YearID&AdminGroupID=$AdminGroupID&msg=UpdateSuccess");

?>
