<?php
// editing by 
/******************************** Modification Log *******************************************
 * 2018-01-04 (Carlos): Enforce password checking.
 * 2012-06-08 (Carlos): added password remark
 *********************************************************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsharedmailbox.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"] || !$plugin['imail_gamma']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lsharedmailbox = new libsharedmailbox();

$CurrentPageArr['SharedMailBox'] = 1;
$CurrentPage = "Mgmt_SharedMailBox";

if(!isset($MailBoxID[0]) || $MailBoxID[0]==""){
	intranet_closedb();
	header("Location: index.php");
	exit;
}

$MailBox = $lsharedmailbox->Get_Shared_MailBox($MailBoxID[0]);
if(sizeof($MailBox)==0){
	intranet_closedb();
	header("Location: index.php");
	exit;
}

$MailBoxQuota = $lsharedmailbox->Get_MailBox_Total_Quota($MailBox[0]['MailBoxName']);

$MailBoxMembers = $lsharedmailbox->Get_MailBox_UserList($MailBoxID[0]);
$member_options = '';
for($i=0;$i<sizeof($MailBoxMembers);$i++){
	$member_options .= '<option value="'.$MailBoxMembers[$i]['UserID'].'">'.htmlspecialchars($MailBoxMembers[$i]['UserName']).'</option>';
}
$member_options .= '<option value=""></option>';

$TAGS_OBJ[] = array($Lang['SharedMailBox']['SharedMailBox']);
$MODULE_OBJ['title'] = $Lang['SharedMailBox']['SharedMailBox'];

$MODULE_OBJ = $lsharedmailbox->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$PAGE_NAVIGATION[] = array($Lang['SharedMailBox']['EditSharedMailBox'], "");
?>
<script type="text/javascript" language="javascript">
function goSubmit()
{
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>
	var password1 = $('#MailBoxPassword1');
	var password2 = $('#MailBoxPassword2');
	var quota = $('input#MailBoxQuota');
	
	if(Trim(password1.val())!='' || Trim(password2.val())!='')
	{
		if(Trim(password1.val())=='' || Trim(password2.val())==''){
			alert('<?=$Lang['SharedMailBox']['Warning']['PasswordCannotBlank']?>');
			password1.focus();
			return false;
		}
		if(password1.val() != password2.val()){
			alert('<?=$Lang['SharedMailBox']['Warning']['PasswordsDonotMatch']?>');
			password1.focus();
			return false;
		}
		var check_password_result = CheckPasswordCriteria(password1.val(),'<?=substr($MailBox[0]['MailBoxName'],0,strpos($MailBox[0]['MailBoxName'],'@'))?>',8);
		if(check_password_result.indexOf(1) == -1){
			var password_warning_msg = '';
			for(var i=0;i<check_password_result.length;i++){
				password_warning_msg += password_warnings[check_password_result[i]] + "\n";
			}
			alert(password_warning_msg);
			password1.focus();
			return false;
		}
	}
	if($.trim(quota.val())!='' && !Check_Positive_Int(quota.val())){
		$('div#QuotaWarning').css('display','');
		quota.focus();
		return;
	}else{
		$('div#QuotaWarning').css('display','none');
	}
	
	var members = $('#MemberList option');
	members.each(
		function(index){
			var t = $(this);
			if(t.val()!="")
				t.attr('selected',true);
			else
				t.attr('selected',false);
		}
	);
	document.form1.submit();
}

function removeMember()
{
	var members = $('#MemberList option:selected');
	members.each(
		function(index){
			var t = $(this);
			if(t.val()!="") t.remove();
		}
	);
}

function Check_Positive_Int(n)
{
	var value = parseInt(n);
	var result = /^\d+$/.test(n);
	if (isNaN(value) || value < 0 || result==false)
		return false;
	else
		return true;
}
</script>
<form name="form1" method="post" action="edit_update.php">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$xmsg?></td>
	</tr>
	<tr>
	<td class="board_menu_closed">
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
        		<td class="main_content">
        			<div class="table_board">
        				<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
				            <tr>
				                <td width="20%" class="formfieldtitle"><?=$Lang['SharedMailBox']['MailBox']?></td>
								<td><?=htmlspecialchars($MailBox[0]['MailBoxName'])?>
								</td>
				            </tr>
				            <tr>
				            	<td rowspan="3" width="20%" class="formfieldtitle"><?=$Lang['SharedMailBox']['Password']?></td>
				            	<td><input id="MailBoxPassword1" name="MailBoxPassword1" type="password" class="textboxnum" maxlength="30" value="<?=$MailBox[0]['MailBoxPassword']?>" /></td>
				            </tr>
				            <tr>
				            	<td><input id="MailBoxPassword2" name="MailBoxPassword2" type="password" class="textboxnum" maxlength="30" value="<?=$MailBox[0]['MailBoxPassword']?>" /> (<?=$Lang['AccountMgmt']['Retype']?>)</td>
				            </tr>
				            <tr>
								<td class="tabletextremark"><?=str_replace("<!--PASSWORD_LENGTH-->",8,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements']))?></td>
							</tr>
				            <tr>
				            	<td width="20%" class="formfieldtitle"><?=$Lang['Gamma']['Quota']?></td>
				            	<td><input id="MailBoxQuota" name="MailBoxQuota" type="text" class="textboxnum" maxlength="10" value="<?=$MailBoxQuota?>" />(Mb)(<?=$Lang['SharedMailBox']['QuotaDescription']?>)<div id="QuotaWarning" style="color:red;display:none;"><?=$Lang['SharedMailBox']['Warning']['QuotaRequestPositiveInt']?></div></td>
				            </tr>
				            <tr>
				            	<td width="20%" class="formfieldtitle"><?=$Lang['SharedMailBox']['SharedBy']?></td>
				            	<td>
				            		<select id="MemberList" name="MemberList[]" multiple="true" size="10">
				            			<?=$member_options?>
				            		</select>
				            		<a href="javascript:newWindow('select_member.php?fieldname=MemberList',9);" class="iMailsubject" title="<?=$Lang['SharedMailBox']['AddSharers']?>"><img src="<?=$image_path."/".$LAYOUT_SKIN?>/iMail/icon_address_book.gif" width="20" height="20" border="0" align="absmiddle" /></a>
				            		<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'],"button","removeMember()")?>
				            	</td>
				            </tr>
				        </table>
						<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
                        <p class="spacer"></p>
                        <?= $linterface->GET_ACTION_BTN($button_save, "button", "goSubmit('edit_update.php')")?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location.href='./index.php'")?>
						<p class="spacer"></p>
                    </div>
				</td>
			</tr>
        </table>
	</td>
</tr>
</table>
<input type="hidden" id="MailBoxID" name="MailBoxID" value="<?=$MailBox[0]['MailBoxID']?>" />
<input type="hidden" id="MailBoxName" name="MailBoxName" value="<?=htmlspecialchars($MailBox[0]['MailBoxName'])?>" />
<input type="hidden" id="OldMailBoxQuota" name="OldMailBoxQuota" value="<?=$MailBoxQuota?>" />
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>