<?php
/* 
 * Editing by  
 * 
 * Note: page called for using jquery.autocomplete.js only 
 * 
 * @Param	q : 	search string
 * 			field:	field name for which to retireve
 * 

 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");



include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
intranet_auth();
intranet_opendb();

$ldb = new libdb();


########################################################################################################


## Get Data
$q = (isset($q) && $q != '') ? $q : '';

$field = (isset($field) && $field != '') ? $field : '';
$school_id = (isset($school_id) && $school_id!= '') ? $school_id: '';
$programme_title= (isset($programme_title) && $programme_title!= '') ? $programme_title: '';


$x = '';
// debug_pr($_GET);

## Maing
if (($q != '') && ($field != '')) {
	$limit = 100;
	$validStr = false;
	$extraFilter = '';
	# search with input keyword
	switch ($field) {
		case 'UniversityProgramme':
			$table = 'JUPAS_PROGRAMME';
			$Field1 = 'ProgrammeFullName';
// 			$selectField = Get_Lang_Selection($Field1,$Field2);
			$orderBy = $field;
			$validStr = true;
			$extraFilter = " AND Institution = '$school_id'";

			break;	
			
		case 'JUPASCode':
		    $table = 'JUPAS_PROGRAMME';
		    $Field1 = 'JUPASCode';
		    $orderBy = $field;
		    $validStr = true;
		    $extraFilter = " AND Institution = '$school_id' AND ProgrammeFullName ='$programme_title'";
	}
	
	if ($validStr) {
	    $filter = " where ".$Field1." like '%".$q."%'".$extraFilter;
	    $sql = "select ".$Field1." from ".$table.$filter;
	    $result = $ldb->returnArray($sql,null,2);	// numeric array only
		if(!empty($result)){
			foreach ($result as $row){
				$res = intranet_undo_htmlspecialchars($row[0]);
				$x .= $res."|".$res."\n";
			}
		}
	}
}

echo $x;