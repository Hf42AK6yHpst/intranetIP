<?php

##########################################
#   Date:   2019-01-23 Anna
#           set larger time limit [#152375]
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2011-09-28	YatWoon
#			Add $YearOfLeft parameter to archiveStudentsInfo()
#
##########################################
set_time_limit(600);

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$lsp = new libstudentpromotion();
$lu = new libuser();

# Perform Archival of Data
# Set Academic Year

$CurrentYear = getCurrentAcademicYear();
$lsp->setAcademicYear($CurrentYear);

/* all unassigned students can assign to Alumni Group, commented by Henry Chow on 20100604 */
/*
# Get List of students in status Left
$students = $lu->returnLeftStudentIDList();
if (sizeof($students)==0)
{
    header("Location: index.php?step=3&failed=1");
    exit();
}
$list = implode(",",$students);
*/

$list = $userList;

# Archive Student Information
$lsp->archiveStudentsInfo($list, $YearOfLeft);
# Archive Process of remove users
$lu->prepareUserRemoval($list);

# If alumni auto , change record type and add to alumni
# else remove from INTRANET_USER
//if ($special_feature['alumni'] && $alumni_system_type==1)
if ($special_feature['alumni'])
{
    $lu->setToAlumni($list,$YearOfLeft);
}
else
{
    $lu->removeUsers($list);
}


# GSuite
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
if($ssoservice["Google"]["Valid"]){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	$libGoogleSSO = new libGoogleSSO();
	$libGoogleSSO->removeUserFromEClassToGoogle(explode(',', $list));
}	
$successAry['synUserDataToModules'] = $lu->synUserDataToModules(explode(',', $list));

header ("Location: notInClass.php?msg=1");


?>
