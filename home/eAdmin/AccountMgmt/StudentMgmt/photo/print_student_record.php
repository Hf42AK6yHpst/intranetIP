<?php
//using: 
/*
* *******************************************
*   2018-01-28 Isaac 
*   - aligned access right as photo list page
*	2017-11-28 Isaac
*   - added sorrting by student name for student without class
*   - remove css attribute 'nowrap' from td elements
*
*   2017-11-22 Simon
*	- add handling the print archive student
*  	2017-11-20 Simon [K130888 - 保良局香港道教聯合會圓玄小學]
* 	- print student photo
*
* *******************************************
*/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
include_once($PATH_WRT_ROOT.'includes/lib.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php"); 
include_once($PATH_WRT_ROOT."includes/libstudentphoto.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");

intranet_auth();
intranet_opendb();

### Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$laccount = new libaccountmgmt();
// $libfs = new libfilesystem();
// $fcm = new form_class_manage();
// $liuser = new libuser();
$lprofile = new libprofile();

$YearClassID = IntegerSafe($_GET['YearClassID']);
$subjectGroupIdAry = $_POST['subjectGroupIdAry'];



$canAccess = false;
$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();
if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || in_array($UserID,$officalPhotoMgmtMember))) {
    $canAccess = true;
}

if(!$canAccess) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$html = $lprofile->Print_Student_photo($YearClassID, $subjectGroupIdAry);
# add handling the print archive student

echo $html;
?>
