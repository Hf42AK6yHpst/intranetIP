<?php
//using: Rita

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
		
intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$fcm = new form_class_manage();
$laccount = new libaccountmgmt();

# Initialize Export Variables
$exportColumn = array();
$ExportArr = array();
//$thisRow = array();

# Get Form Data
$AcademicYearID = Get_Current_Academic_Year_ID();
$formList = $fcm->Get_Form_List();
$numOfForms = count($formList);

# Export Columns
$exportColumn[] = "Class Name";
$exportColumn[] = "Class Number";
$exportColumn[] = "Student Name";
$exportColumn[] = "Student Login";


# Export Row(s)
if($numOfForms==0){
	$ExportArr = $Lang['General']['NoRecordFound'];	
}

# Loop Each Form
for($i=0;$i<$numOfForms;$i++){
	$classList = $fcm->Get_Class_List($AcademicYearID,$formList[$i]['YearID']);
	$numOfClassList = count($classList);

	for ($j=0; $j<$numOfClassList; $j++) {	
		$thisClassID = $classList[$j]['YearClassID'];
		$thisClassNameEn = $classList[$j]['ClassTitleEN'];
		$thisClassNameB5 = $classList[$j]['ClassTitleB5'];
	
		$thisClassName = Get_Lang_Selection($thisClassNameB5, $thisClassNameEn);
	
		# Get Student Info
		$userInfoList = $laccount->getStudentUserInfoListByClassID($thisClassID);
		$userInfoListAssoc = BuildMultiKeyAssoc($userInfoList, 'UserLogin');
		
		# Get Student Without Photo
		$StudentListWithoutPhoto = $laccount->getStudentListWithoutPhoto($thisClassID);
		
		foreach($StudentListWithoutPhoto as $eachRecord=>$__studentUserLogin){
			$thisRow = array();
			$__classNumber = $userInfoListAssoc[$__studentUserLogin]['ClassNumber'];
			$__studentNameEn = $userInfoListAssoc[$__studentUserLogin]['EnglishName'];
			$__studentNameB5 = $userInfoListAssoc[$__studentUserLogin]['ChineseName'];
			$__studentName = Get_Lang_Selection($__studentNameB5, $__studentNameEn);
			
			# Prepare Export Row
			$thisRow[]= $thisClassName;
			$thisRow[]= $__classNumber;
			$thisRow[]= $__studentName;
			$thisRow[]= $__studentUserLogin;
							
			$ExportArr[]= $thisRow;
		}
					 
	}

}


$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0);

intranet_closedb();
      
$filename = "student_without_photo_list.csv";
$lexport->EXPORT_FILE($filename, $export_content);	

?>
