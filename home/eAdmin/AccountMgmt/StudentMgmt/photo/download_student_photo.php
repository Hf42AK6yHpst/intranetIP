<?php
// using: 
/*
 * if need to upload this file to client site, please also upload libaccountmgmt.php    [before IPv11.1]
 */

/*
* *******************************************
* 2019-09-12 Tommy [2019-0911-0906-53066]
* - change memory-limit to 500M for large zip file
* - add time limit for timeout
* 
*  2018-10-05 Bill  [2018-1005-1149-13073]
*   - Use output2browserByPath(), to prevent blank page when export large zip file
*   - Use UserID to get Official Photo
*  
*  2018-06-27 Carlos
*   - Use $file_path instead of $intranet_root in order to cater KIS file path.
* 
*  2017-12-20 Simon
*   - Modify the get photo method by using $liuser->GET_OFFICIAL_PHOTO()
*  
*  2017-11-14 Isaac
*   - Changed file name to download_student_photo.php	
*  
*  2017-11-14 Simon [K130888 - 保良局香港道教聯合會圓玄小學]
* 	- bulk download or one class zip student photo
*
* *******************************************
*/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
		
intranet_auth();
intranet_opendb();

### Temp assign memory of this page
ini_set("memory_limit", "500M");
@SET_TIME_LIMIT(216000);

$laccount = new libaccountmgmt();
$liuser = new libuser();
$libfs = new libfilesystem();

$thisClassID = $_GET['YearClassID'];
if($thisClassID == "ALL")
{
	### Create folder to contain bulk photo folder
	$_TempFolder = $file_path."/file/temp/class_photo";
	$_TempBulkPhotoFolder = $_TempFolder.'/bulk';
	if (!file_exists($_TempBulkPhotoFolder)) {
		$SuccessArr['CreateTempBulkFolder'] = $libfs->folder_new($_TempBulkPhotoFolder);
	}
	
	$AcademicYearID = Get_Current_Academic_Year_ID();
	
	// Form list
	// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$fcm = new form_class_manage();
	$FormList = $fcm->Get_Form_List();
	for ($i=0; $i < sizeof($FormList); $i++)
	{
	    // Class list
		$ClassList = $fcm->Get_Class_List($AcademicYearID, $FormList[$i]['YearID']);
		for ($j=0; $j < sizeof($ClassList); $j++)
		{
			$_thisClassID = $ClassList[$j]['YearClassID'];
			$_StudentPhotoInfoAry = $laccount->getStudentListWithPhoto($_thisClassID);
			if(!empty($_StudentPhotoInfoAry) && $_StudentPhotoInfoAry != null)
			{
			    ### Prepare temp class folder
			    $_thisClassName = $_StudentPhotoInfoAry[0]['ClassTitleEN'];
				$_TempPhotoFolder = $_TempFolder.'/'.$_thisClassName;
				
				// Student list
				foreach($_StudentPhotoInfoAry as $_StudentPhoto)
				{
				    ### Get offical photo
					//$webSamsRegNo = $_StudentPhoto['WebSAMSRegNo'];
					//$_StudentPhoto = $liuser->GET_OFFICIAL_PHOTO($webSamsRegNo);
				    $_studentUserID = $_StudentPhoto['UserID'];
				    $_StudentPhoto = $liuser->GET_OFFICIAL_PHOTO_BY_USER_ID($_studentUserID);
					
					### Create temp class folder
					if (!file_exists($_TempPhotoFolder) && !empty($_StudentPhoto[2])) {
						$SuccessArr['CreateTempClassFolder'] = $libfs->folder_new($_TempPhotoFolder);
					}
			        
					if(!empty($_StudentPhoto[2])) {
						$_thisFilePath = $_StudentPhoto[2];
					}
                    
					### Copy offical photo to temp class folder
					$_onlyfilename = end(explode("/", $_thisFilePath));
					if(!empty($_onlyfilename)) {
						$SuccessArr['CopyPhotoFiles'] = $libfs->file_copy($file_path.$_thisFilePath, $_TempPhotoFolder.'/'.$_onlyfilename);
					}
				}
				
				### Copy temp class folder to bulk photo folder
				$SuccessArr['CopyPhotoFolder'] = $libfs->folder_copy($_TempPhotoFolder, $_TempBulkPhotoFolder);
                
				### Delete temp class folder
				$SuccessArr['DeleteTempClassFolder'] = $libfs->folder_remove_recursive($_TempPhotoFolder);
			}
		}
	}
	
	### Download path
	$fileName = 'bulk';
	$downloadZipFile = $fileName. '.zip';
	
	### Delete old zip file first
    $SuccessArr['DeleteOldZipFiles'] = $libfs->file_remove($_TempFolder.'/'.$downloadZipFile);
    
	### Generated zip file
	# 1st param: download file name
	# 2nd param: download zip file path
	# 3rd param: download folder location
	$SuccessArr['CreateZipFiles'] = $libfs->file_zip($fileName, $downloadZipFile, $_TempFolder);
	
	### Remove bulk photo folder
	$SuccessArr['DeleteTempBulkFolder'] = $libfs->folder_remove_recursive($_TempBulkPhotoFolder);
	
	### Output zip file
	//output2browser(get_file_content($_TempBulkPhotoFolder.'.zip'), $downloadZipFile);
	session_write_close();
	output2browserByPath($_TempFolder.'/'.$downloadZipFile, $downloadZipFile);
	
	die();
}
else if($thisClassID == "")
{
    ### Prepare temp folder
    $_TempFolder = $file_path."/file/temp/class_photo";
	$_TempBulkPhotoFolder = $_TempFolder.'/archive';
	
	### Get student list
	$_StudentPhotoInfoAry = $laccount->getArchiveStudentListWithPhoto();
	if(!empty($_StudentPhotoInfoAry)) 
	{
	    // Student list
		foreach($_StudentPhotoInfoAry as $_StudentPhoto)
		{
			### Get offical photo
			//$webSamsRegNo = $_StudentPhoto['WebSAMSRegNo'];
			//$_StudentPhoto = $liuser->GET_OFFICIAL_PHOTO($webSamsRegNo);
			$_studentUserID = $_StudentPhoto['UserID'];
			$_StudentPhoto = $liuser->GET_OFFICIAL_PHOTO_BY_USER_ID($_studentUserID);
			
			### Create temp folder
			if (!empty($_StudentPhoto[2])) {
				$SuccessArr['CreateTempFolder'] = $libfs->folder_new($_TempBulkPhotoFolder);
			}
			
			if(!empty($_StudentPhoto[2]) && $_StudentPhoto[2] != null) {
				$_thisFilePath = $_StudentPhoto[2];
			}
			
			### Copy offical photo to temp class folder
			$_onlyfilename = end(explode("/", $_thisFilePath));
			if(!empty($_onlyfilename)) {
				if(file_exists($file_path.$_thisFilePath)) {
					$SuccessArr['CopyPhotoFiles'] = $libfs->file_copy($file_path.$_thisFilePath, $_TempBulkPhotoFolder.'/'.$_onlyfilename);
				}
			}
		}
	}
	
	### Download path
	$fileName = 'archive';
	$downloadZipFile = $fileName. '.zip';
	
	### Remove the last generated zip file
	$SuccessArr['DeleteOldZipFiles'] = $libfs->file_remove($_TempFolder.'/'.$downloadZipFile);
	
	### Generated zip file
	# 1st param: download file name
	# 2nd param: download zip file path
	# 3rd param: download folder location
	$SuccessArr['CreateZipFiles'] = $libfs->file_zip($fileName, $downloadZipFile, $_TempFolder);
	
	### Remove bulk folder
	$SuccessArr['DeleteTempFolder'] = $libfs->folder_remove_recursive($_TempBulkPhotoFolder);
	
	### Output zip file
	//output2browser(get_file_content($_TempBulkPhotoFolder . '.zip'), $downloadZipFile);
	session_write_close();
	output2browserByPath($_TempFolder.'/'.$downloadZipFile, $downloadZipFile);
	
	die();
}
else
{
    ### Get student list
	$StudentPhotoInfoAry = $laccount->getStudentListWithPhoto($thisClassID);
	if(empty($StudentPhotoInfoAry)) {
		echo "<script type='text/javascript'>alert('No such information. Please select the class again.');";
		echo "window.location='photo_list.php?YearClassID=".$thisClassID."';";
		echo "</script>";
	}
    
	### Prepare temp class folder
	$thisClassName = $StudentPhotoInfoAry[0]['ClassTitleEN'];
	$TempFolder = $file_path."/file/temp/class_photo";
	$TempPhotoFolder = $TempFolder.'/'.$thisClassName;
	
	### Create temp class folder
	if (!file_exists($TempPhotoFolder)) {
		$SuccessArr['CreateTempClassFolder'] = $libfs->folder_new($TempPhotoFolder);
	}
	
	// Student list
	foreach($StudentPhotoInfoAry as $StdPhoto)
	{
	    ### Get offical photo
		//$webSamsRegNo = $StdPhoto['WebSAMSRegNo'];
		//$StudentPhoto = $liuser->GET_OFFICIAL_PHOTO($webSamsRegNo);
	    $_studentUserID = $StdPhoto['UserID'];
	    $StudentPhoto = $liuser->GET_OFFICIAL_PHOTO_BY_USER_ID($_studentUserID);
		if(!empty($StudentPhoto[2])){
			$thisFilePath = $StudentPhoto[2];
		}
		
		### Copy offical photo to temp class folder
		$onlyfilename = end(explode("/", $thisFilePath));
		$SuccessArr['CopyPhotoFiles'] = $libfs->file_copy($file_path.$thisFilePath, $TempPhotoFolder.'/'.$onlyfilename);
	}
	
	### Download path
	$downloadFileName = $thisClassName;
	$ZipTempPhotoFolder = $thisClassName.'.zip';
	$downloadZipPath = $TempFolder.'/'.$ZipTempPhotoFolder;
	
	### Remove the last generated zip file
	$SuccessArr['DeleteOldZipFiles'] = $libfs->file_remove($TempFolder.'/'.$ZipTempPhotoFolder);
	
	### Generated zip file
	# 1st param: download file name
	# 2nd param: download zip file path
	# 3rd param: download folder location
	$SuccessArr['CreateZipFiles'] = $libfs->file_zip($downloadFileName, $downloadZipPath, $TempFolder);
	
	### Remove the folder
	$SuccessArr['DeleteTempClassFolder'] = $libfs->folder_remove_recursive($TempPhotoFolder);
	
	### Output zip file
	//output2browser(get_file_content($TempFolder.'/'.$ZipTempPhotoFolder), $ZipTempPhotoFolder);
	session_write_close();
	output2browserByPath($TempFolder.'/'.$ZipTempPhotoFolder, $ZipTempPhotoFolder);
	
	die();
}
?>