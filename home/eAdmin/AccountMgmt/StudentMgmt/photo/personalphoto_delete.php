<?php
# using: 

#####################################
#
#	Date:	2019-02-22  Bill	[2019-0221-0952-39207]
#			fixed: not update modified user in INTRANET_USER
#
#	Date:	2015-10-13	Yuen	[2015-0922-1008-00071]
#			fixed: syn user information (ie. photo) to eLib Plue database
#
#	Date:	2013-04-03	YatWoon
#			add $return_path
#
#####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libuser.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include($PATH_WRT_ROOT."includes/libuserphoto.php");

intranet_auth();
intranet_opendb();
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$li = new libuser($UserID);
$UserID = $_GET['UserID'];
$lf = new libfilesystem();
$lphoto = new libuserphoto();
	$path = $intranet_root.$li->PersonalPhotoLink;
	if (is_file($path) && strtoupper($lf->file_ext($li->PersonalPhotoLink))!="")
	{
	    $lf->file_remove($path);
	}
	
	$fieldname = '';
	$fieldname .= "PersonalPhotoLink = '', ";
	$fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
	
	$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
	
	if ($lphoto->isUserPersonalPhotoLinkedToOfficialPhoto($UserID)) {
	    $lphoto->deletePersonalPhotoTempFile($UserID);
	}

$return_path = $return_path ? $return_path : "../edit.php";
$li->db_db_query($sql);
$li->synUserDataToModules($UserID);
$UserID = $_GET['UserID'];
// header("Location: ". $return_path ."?msg=photo_delete&uid=$UserID");
?>
<form id="form1" name="form1" method="POST" action="<?=$return_path?>">
	<input type="hidden" name="uid" value='<?=$UserID?>' />
</form>
<script type="text/javascript">
	window.onload = function(){
		document.form1.submit();
	}
</script>