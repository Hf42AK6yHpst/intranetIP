<?php
// Editing by 
/*
 * Modification: 
 * 
 * Date: 2019-05-09 (Bill)      [2019-0506-1813-48170]
 *  - trim() userlogin to rebuild filename before storage > prevent cannot display official photo due to spaces in filename
 *  - added handleImageOrientation() > fix image orientation problems
 * 
 * Date: 2018-06-27 (Carlos)
 *  - Use $file_path instead of $intranet_root in order to cater KIS file path.
 * 
 * Date: 2017-07-14 (Carlos)
 *  - Do scale opration to photo.
 * 
 * Date: 2014-04-02 (Carlos)
 *  - Do not rename [userlogin].jpg to tmp file name, only zip file should be copy to a renamed tmp file
 * 
 * Date: 2013-12-10 (YatWoon)
 *	- fixed: genearte temp filename for the uploaded file, prevent Chinese filename coding problem [Case#2013-1209-1044-01066]
 *
 * Date: 2012-08-01 (Ivan)
 * 	- added logic to syn user info to library system
 */

ini_set("memory_limit", "1024M");
set_time_limit(2*3600);

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/SimpleImage.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || in_array($UserID,$officalPhotoMgmtMember))) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

function handleImageOrientation($image_path)
{
    # Create image object
    $image_obj = imagecreatefromjpeg($image_path);
    
    # Rotate image if orientation is incorrect
    if (function_exists('exif_read_data'))
    {
        $exif = exif_read_data($image_path);
        if($exif && isset($exif['Orientation']))
        {
            $orientation = $exif['Orientation'];
            if($orientation != 1)
            {
                $deg = 0;
                switch ($orientation) {
                    case 3:
                        $deg = 180;
                        break;
                    case 6:
                        $deg = 270;
                        break;
                    case 8:
                        $deg = 90;
                        break;
                }
                if ($deg) {
                    $image_obj = imagerotate($image_obj, $deg, 0);
                }
            }
        }
    }
    
    # Create jpeg file
    imagejpeg($image_obj, $image_path, 100);
    
    return $image_obj;
}

$luser = new libuser();
$lfs = new libfilesystem();
$simple_image = new SimpleImage();
$linterface = new interface_html();

$best_width = 412; // px
$best_height = 459; // px

# upload file handling start
$filename = $_FILES["UploadFile"]["name"];
$tmpfilepath = $_FILES["UploadFile"]["tmp_name"];
$fileext = $lfs->file_ext($filename);

//$user_photo_path = $intranet_root."/file/user_photo";
$user_photo_path = $file_path."/file/user_photo";
$base_img_path = "/file/user_photo";
$tmpfolder = $user_photo_path."/tmp";

### generate tmp filename (prevent Chinease filename coding problem)
$tmpfilename = "TMP_".$UserID.$fileext;
//$tmpfile = $tmpfolder."/$filename";
$tmpfile = $tmpfolder."/$tmpfilename";
# create a tmp file
$lfs->folder_new($tmpfolder);

if(strtolower($fileext)==".zip")
{
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
	if($lfs->file_unzip($tmpfile,$tmpfolder))
	{
		$lfs->lfs_remove($tmpfile);
		$tmpfilelist = $lfs->return_folderlist($tmpfolder);
	}
}
else if(strtolower($fileext)==".jpg")
{
	# copy to tmp folder first
	//$lfs->lfs_copy($tmpfilepath, $tmpfile);
	$tmpfile = $tmpfolder."/$filename";
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
	$tmpfilelist = $lfs->return_folderlist($tmpfolder);
}

$userIdAry = array();
if(count($tmpfilelist) > 0)
{
	$NoOfUploadSuccess = 0;
	for($i=0; $i< count($tmpfilelist); $i++)
	{
		$UploadSuccess = false;
		$thisUserInfo = '';
		if(strtolower($lfs->file_ext($tmpfilelist[$i])) == ".jpg")
		{
			$thisrawext = substr($tmpfilelist[$i], strrpos($tmpfilelist[$i],".")); // no strtolower 
			if($thisrawext != ".jpg") // ext not lower case
			{
				$lowercasefilepath = str_replace($thisrawext,".jpg",$tmpfilelist[$i]);
				$lfs->file_rename($tmpfilelist[$i], $lowercasefilepath);
				$tmpfilelist[$i] = $lowercasefilepath;
			}
			
			$thisUserLogin = basename($tmpfilelist[$i],".jpg");
			$thisUserInfo = $laccount->getUserInfoByUserLogin($thisUserLogin);
			if(!empty($thisUserInfo))
			{
			    // [2019-0506-1813-48170] trim() userlogin for correct filename
			    $target_photo_name = basename($tmpfilelist[$i]);
			    $target_photo_userlogin = trim($thisUserLogin);
			    if($target_photo_name != '' && $target_photo_userlogin != $thisUserLogin) {
			        if($target_photo_userlogin != '' && isset($thisUserInfo[$target_photo_userlogin])) {
			            $thisUserLogin = $target_photo_userlogin;
			            $target_photo_name = $thisUserLogin.".jpg";
			        }
			    }
			    // $target_photo_path = $user_photo_path."/".basename($tmpfilelist[$i]);
			    $target_photo_path = $user_photo_path."/".$target_photo_name;
			    
				if($lfs->lfs_copy($tmpfilelist[$i], $target_photo_path))
				{
				    // [2019-0506-1813-48170] fix image orientation problem
					// $image = imagecreatefromjpeg($target_photo_path);
					// imagejpeg($image,$target_photo_path, 100);
				    $image = handleImageOrientation($target_photo_path);
				    
					//$simple_photo_path = $base_img_path."/".basename($tmpfilelist[$i]);
                    $simple_photo_path = $base_img_path."/".$target_photo_name;
					$laccount->updatePhotoLink($thisUserLogin,$simple_photo_path);
					if($PhotoOperation == '2'){ // scale to fit
						$simple_image->load($target_photo_path);
						$image_width = $simple_image->getWidth();
						$image_height = $simple_image->getHeight();
						$width_ratio = $image_width / (float)$best_width;
						$height_ratio = $image_height / (float)$best_height;
						if($width_ratio != 1.0 && $height_ratio != 1.0 && $image_width > 0 && $image_height > 0)
						{
							$target_ratio = 1.0 / max($width_ratio, $height_ratio);
							$target_width = (int)($target_ratio * $image_width);
							$target_height = (int)($target_ratio * $image_height);
							$simple_image->resize($target_width,$target_height);
							$simple_image->save($target_photo_path);
						}
					}
					$UploadSuccess = true;
					$NoOfUploadSuccess++;
				}
				$userIdAry[] = $thisUserInfo[$thisUserLogin]['UserID'];
			}
		}
		
		$red_css = !$UploadSuccess?" red ":"";

		$rowcss = " class='".($key++%2==0? "tablebluerow2":"tablebluerow1")." $red_css' ";

		# gen table row
		$tr.= '<tr '.$rowcss.'>'."\n";
			$tr.= '<td>'.basename($tmpfilelist[$i]).'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["ClassName"]:"-").'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["ClassNumber"]:"-").'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["Name"]:"-").'</td>'."\n";
		$tr.= '</tr>'."\n";
	}
	
	$display.= '<table cellpadding=5 cellspacing=0 border=0 width="100%">'."\n";
		$display.= '<tr class="tablebluetop tabletopnolink">'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['File'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['Class'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['ClassNo'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['StudentName'].'</td>'."\n";
		$display.= '</tr>'."\n";		
		$display.= $tr;
	$display.= '</table>'."\n";
	
}

$UploadStat.= '<table cellpadding="5" cellspacing="0" border="0" width="100%">'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['PhotoAttempt'].'</td><td class="tabletext">'.count($tmpfilelist).'</td></tr>'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadSuccess'].'</td><td class="tabletext">'.$NoOfUploadSuccess.'</td></tr>'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadFail'].'</td><td class="tabletext">'.(count($tmpfilelist)-$NoOfUploadSuccess).'</td></tr>'."\n";
$UploadStat.= '</table>'."\n";

//$lfs->folder_remove_recursive($tmpfolder);
shell_exec("rm -rf '$tmpfolder'");
# upload file handling end

# Gen button 
$uploadBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['UploadMorePhoto'], "button", "window.location.href='upload.php'", "uploadBtn");
$backBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['BackToPhotoMgmt'], "button", "window.location.href='index.php'", "backBtn");

# Page info
$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 1);
$StepObj = $linterface->GET_STEPS($STEPS_OBJ);

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ManagePhoto'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$TAGS_OBJ[] = array($Lang['AccountMgmt']['Photo']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$successAry['synUserDataToModules'] = $luser->synUserDataToModules($userIdAry);
?>

<br>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
			</table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$UploadStat?>
					</td>
					<td align="right"></td>
				</tr>
			</table>
			<br>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$uploadBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
		</td>
	</tr>
</table>
<br><br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>