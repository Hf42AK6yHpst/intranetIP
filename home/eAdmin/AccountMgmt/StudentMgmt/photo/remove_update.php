<?php
// Using: 

/**
 * 
 * Date: 2018-09-10 (Bill)  [2018-0907-1131-40073]
 * - added update log when delete photo
 * 
 * Date: 2018-06-27 (Carlos)
 * - Use $file_path instead of $intranet_root in order to cater KIS file path.
 * 
 * Date: 2012-08-01 (Ivan)
 * - added logic to syn user info to library system
 * 
 * Date: 2010-06-08 (Sandy)
 * - update db after deleteing physical file.
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$lfs = new libfilesystem();
$li = new libdb();
$luser = new libuser();

foreach((array) $UserLogin as $thisUserLogin)
{
	$img_path = $file_path."/file/user_photo/$thisUserLogin.jpg";
	## Unlink file
	$is_succ = $lfs->file_remove($img_path);
	$result[] = $is_succ;
	
	## update DB intranet photo
	if($is_succ == true || $is_succ == 1) {
		$sql = "UPDATE INTRANET_USER SET PhotoLink = NULL, DateModified = NOW(), ModifyBy = '".$UserID."' WHERE UserLogin = '".$thisUserLogin."' Limit 1";
		$li->db_db_query($sql);
	}
}

$sql = "Select UserID From INTRANET_USER where UserLogin In ('".implode("','", (array)$UserLogin)."')";
$userIdAry = $li->returnVector($sql);
$successAry['synUserDataToModules'] = $luser->synUserDataToModules($userIdAry);

$msg = in_array(0,$result)?"delete_failed":"photo_delete";
header("location: photo_list.php?YearClassID=$YearClassID&msg=$msg");
?>