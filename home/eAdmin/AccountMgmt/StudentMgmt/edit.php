<?php
# using:  Philips
###									
### 		!!!!! Note: Required to deploy with libuser.php if upload this file to client BEFORE ip.2.5.9.1.1	!!!!!	
### 		!!!!! Note: Please also upload templates/edit.tmpl.php when update this file since 2015-12-17 		!!!!! 
###

############# Change Log
#	Date:	2019-11-22 Philips	[2019-1022-1549-02073]
#			Added Personal Photo delete link
#
#   Date:   2019-10-25 Cameron
#           Fixed: strip blackslash when it contains apostrophe in address field
#
#   Date:   2019-08-08 Paul
#           handle Google SSO quota checking by try / catch
#
#   Date:   2019-07-04 Pun
#           Added QuitTime for MSSCH Printing cust
#
#   Date:   2019-04-30 Cameron
#           fix cross site scripting by applying cleanCrossSiteScriptingCode() or cleanHtmlJavascript() or IntegerSafe() to variables
#
#   Date:   2018-12-31 Isaac
#           Added flag $sys_custom['AccountMgmt']['StudentDOBRequired'] to force DOB field compulsory
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-09-19 Anna
#           Added tungwah sdas cust - PrimarySchoolCode
#
#   Date:   2018-09-03 (Bill)   [2017-1207-0956-53277]
#           added Graduation Date
#
#   Date:   2018-05-15 Isaac
#           Added Last Modified username and time.
#
#	Date:	2018-01-13 Pun [ip.2.5.9.3.1]
#           Added NCS cust
#
#	Date:	2018-01-03 Carlos - Improved password checking.
#
#	Date:	2017-12-22 Cameron
#			configure for Oaks refer to Amway
#
#	Date:	2017-10-19 Ivan [ip.2.5.9.1.1]
#			Changed default personal photo path to call function by libuser.php
#
#	Date:   2017-08-24 Simon
#			Remove the photo height
#
#	Date: 	2017-06-30 Paul
#			Add NCS special fields and QR code generator
#
#	Date: 	2017-01-16 HenryHM
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date: 	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added new text field HouseholdRegister.
#
#	Date:   2016-06-10 Cara
#			Add creator's info($nameDisplay, $InputBy, $DateInput)
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-12-17 Cameron
#			move layout to template folder to customize layout [Amway #P89445]
#
#	Date:	2015-03-17 Cameron
#			use fax field for recording date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true  
# 									
#	Date:	2014-12-10	Bill
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] to display CardID textbox for eEnrolment
#
#	Date:	2013-12-12	Fai
#			Add a cust attribute "stayOverNight" with $plugin['medical']
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3
#	2013-08-13 (Carlos): hide WEBSAMS number and JUPAS Application number for KIS
#
#	Date:   2012-10-29 	Rita	
#			add Parent List
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date :	2012-08-03 Bill Mak
#			Add Fields Nationality, PlaceOfBirth, AdmissionDate
#
#	2012-06-08 (Carlos): added password remark
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date :	2011-12-19	(Yuen)
#			Display student photos
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	Date:	2010-11-19 (Henry Chow)
#			revised "Group"/"Role" display from function getGroupInfoByUserID() & getRoleInfoByUserID()
#
#	Date:	2010-11-05	Marcus
#			Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($eclass_filepath."/src/includes/php/lib-portfolio.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//if(sizeof($_POST)==0) 
if(empty($uid))
{
	header("Location: index.php");	
	exit;
}

$uid = IntegerSafe($uid);

$isKIS = $_SESSION["platform"]=="KIS";

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lo = new libgrouping();
$lrole = new librole();

$iportfolio_activated = false;

if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();

		for($i=0;$i<sizeof($data);$i++){
			if($uid==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}

}
//$iportfolio_activated = false;

$userInfo = $laccount->getUserInfoByID($uid);
$userPersonalSetting = $laccount->getUserPersonalSettingByID($uid);
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
	$userNCSSetting = $laccount->getUserNCSSettingById($uid);
}

# 1) eRC Rubrics Project (Shek Wu)
# 2) Student Mgmt additional info [2012-0201-1440-47098 : Bliss and Wisdom Charitable Foundation]
if($sys_custom['UserExtraInformation']) {
	$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
	$ExtraInfoOthersArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);
	if(count($ExtraInfoOthersArr)>0) {
		foreach($ExtraInfoOthersArr as $item_id=>$item_code) {
			if(${'ExtraOthers_'.$item_id}!='') 
				$ExtraOthers[$item_id] = ${'ExtraOthers_'.$item_id};
		}
	}
			
	if($sys_custom['StudentMgmt']['BlissWisdom']) {
		if($HowToKnowBlissWisdom==1) {
			$HowToKnowBlissWisdom .= "::".$FriendName;	
		} else if($HowToKnowBlissWisdom==6) {
			$HowToKnowBlissWisdom.= "::".$HowToKnowOthersField;	
		}
		
		$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToKnowBlissWisdom);
	}
			
	$UserExtraInfoOption = $laccount->Get_User_Extra_Info_UI($uid,$ExtraInfo,$ExtraOthers,$FieldAry);
}
	
if($fromParent!=1) {
	if($userlogin=="") $userlogin = $userInfo['UserLogin'];
	//if($pwd=="") $pwd = $userInfo['UserPassword'];
	if($email=="") $email = isset($UserEmail)? $UserEmail : $userInfo['UserEmail'];
	if($status=="") $status = $userInfo['RecordStatus'];
	if($smartcardid=="") $smartcardid = $userInfo['CardID'];
	if($sys_custom['SupplementarySmartCard']){
		if($smartcardid2=="") $smartcardid2 = $userInfo['CardID2'];
		if($smartcardid3=="") $smartcardid3 = $userInfo['CardID3'];
		if($smartcardid4=="") $smartcardid4 = $userInfo['CardID4'];
	}
	if($strn=="") $strn = $userInfo['STRN'];
	if($WebSAMSRegNo=="") $WebSAMSRegNo = $userInfo['WebSAMSRegNo'];
	if($HKJApplNo=="") $HKJApplNo = $userInfo['HKJApplNo'];
	if($barcode=="") $barcode = $userInfo['Barcode'];
	if($engname=="") $engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
	if($chiname=="") $chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
	if($nickname=="") $nickname = $userInfo['NickName'];
	if($dob=="") $dob = substr($userInfo['DateOfBirth'],0,10);
	if($dob=="0000-00-00") $dob = "";
	if($gender=="") $gender = $userInfo['Gender'];
	if($hkid=="") $hkid = $userInfo['HKID'];
	if($address=="") {
        $address = $userInfo['Address'];
    }
	else {
        $address = standardizeFormPostValue($address);
    }
	if($country=="") $country = $userInfo['Country'];
	if($homePhone=="") $homePhone = $userInfo['HomeTelNo'];
	if($mobilePhone=="") $mobilePhone = $userInfo['MobileTelNo'];
	if($faxPhone=="") $faxPhone = $userInfo['FaxNo'];
	$original_pass = $userInfo['UserPassword'];
	if($remark=="") $remark = $userInfo['Remark'];
	if($sys_custom['StudentAccountAdditionalFields']){
		$NonChineseSpeaking = $userInfo['NonChineseSpeaking'];
		$SpecialEducationNeeds = $userInfo['SpecialEducationNeeds'];
		$PrimarySchool = $userInfo['PrimarySchool'];
		$University = $userInfo['University'];
		$Programme = $userInfo['Programme'];
	}
	
	
	if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
	    $PrimarySchoolCode= $userInfo['PrimarySchoolCode'];	     
	}
	
	if($Nationality=="") $Nationality = $userPersonalSetting['Nationality'];
	if($PlaceOfBirth=="") $PlaceOfBirth = $userPersonalSetting['PlaceOfBirth'];
	if($AdmissionDate=="") $AdmissionDate = $userPersonalSetting['AdmissionDate'];
	if($AdmissionDate=="0000-00-00") $AdmissionDate = "";
	if($GraduationDate=="") $GraduationDate = $userPersonalSetting['GraduationDate'];
	if($GraduationDate=="0000-00-00") $GraduationDate = "";
	if($plugin['medical']){
		if($stayOverNight=="") $stayOverNight = $userPersonalSetting['stayOverNight'];
		$stayOverNightIsChecked = ' checked ';
		if($stayOverNight == '' || $stayOverNight == 0){
			$stayOverNightIsChecked = '';
		}
	}
	if($sys_custom['BIBA_AccountMgmtCust']){
		if($HouseholdRegister == "") $HouseholdRegister = $userInfo['HouseholdRegister'];
	}
} else {
	$userlogin = $userInfo['UserLogin'];
	//$pwd = $userInfo['UserPassword'];
	$email = $userInfo['UserEmail'];
	$status = $userInfo['RecordStatus'];
	$smartcardid = $userInfo['CardID'];
	if($sys_custom['SupplementarySmartCard']){
		$smartcardid2 = $userInfo['CardID2'];
		$smartcardid3 = $userInfo['CardID3'];
		$smartcardid4 = $userInfo['CardID4'];
	}
	$strn = $userInfo['STRN'];
	$WebSAMSRegNo = $userInfo['WebSAMSRegNo'];
	$HKJApplNo = $userInfo['HKJApplNo'];
	$barcode = $userInfo['Barcode'];
	$engname = $userInfo['EnglishName'];
	$chiname = $userInfo['ChineseName'];
	$nickname = $userInfo['NickName'];
	$dob = substr($userInfo['DateOfBirth'],0,10);
	if($dob=="0000-00-00") $dob = "";
	$gender = $userInfo['Gender'];
	$hkid = $userInfo['HKID'];
	$address = $userInfo['Address'];
	$country = $userInfo['Country'];
	$homePhone = $userInfo['HomeTelNo'];
	$mobilePhone = $userInfo['MobileTelNo'];
	$faxPhone = $userInfo['FaxNo'];
	$original_pass = $userInfo['UserPassword'];
	$remark = $userInfo['Remark'];
	if($sys_custom['StudentAccountAdditionalFields']){
		$NonChineseSpeaking = $userInfo['NonChineseSpeaking'];
		$SpecialEducationNeeds = $userInfo['SpecialEducationNeeds'];
		$PrimarySchool = $userInfo['PrimarySchool'];
		$University = $userInfo['University'];
		$Programme = $userInfo['Programme'];
	}
	
	if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
	    $PrimarySchoolCode= $userInfo['PrimarySchoolCode'];
	}
	$Nationality = $userPersonalSetting['Nationality'];
	$PlaceOfBirth = $userPersonalSetting['PlaceOfBirth'];
	$AdmissionDate = $userPersonalSetting['AdmissionDate'];
	if($AdmissionDate=="0000-00-00") $AdmissionDate = "";
	$GraduationDate = $userPersonalSetting['GraduationDate'];
	if($GraduationDate=="0000-00-00") $GraduationDate = "";
	if($plugin['medical']){
		if($stayOverNight=="") $stayOverNight = $userPersonalSetting['stayOverNight'];
		$stayOverNightIsChecked = ' checked ';
		if($stayOverNight == '' || $stayOverNight == 0){
			$stayOverNightIsChecked = '';
		}
	}
	if($sys_custom['BIBA_AccountMgmtCust']){
		$HouseholdRegister = $userInfo['HouseholdRegister'];
	}
}
if($plugin['mssch_printing_module']){
    if($QuitTime=="") $QuitTime = substr($userPersonalSetting['QuitTime'], 0, 16);
    if($QuitTime=="0000-00-00 00:00") $QuitTime = "";
}
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
	$staffInCharge = $userNCSSetting['StaffInCharge'];
	$hkDuration = $userNCSSetting['DurationInHK'];
	$re = '/([^,]+):([^,]+)/';
	preg_match_all($re, $userNCSSetting['LanguagePriority'], $priorityOrder, PREG_SET_ORDER, 0);
	foreach($priorityOrder as $order){
	    $LangPriority[ trim($order[1]) ] = trim($order[2]);
	}
	$isNCS = ($userNCSSetting['IsNCS'] == '1')?'checked':'';
	$NGO = $userNCSSetting['NGO'];
}

//IF the $HKJApplNo is 0 , set it to empty
$HKJApplNo = ($HKJApplNo == 0)?'':$HKJApplNo;

# imail gamma
if($plugin['imail_gamma'])
{
	if(!$IMapEmail = $laccount->getIMapUserEmail($uid))
	{
		$userInfo = $laccount->getUserInfoByID($uid);
		$IMapEmail = $userInfo['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$disabled = "checked";
	}	
	else
	{
		$enabled = "checked";
	}
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_STUDENT);
	$EmailQuota = $laccount->getUserImailGammaQuota($uid);
	$EmailQuota = $EmailQuota?$EmailQuota:$DefaultQuota[1];
}

if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	
	$google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
	$sso_db = new libSSO_db();
	$lib_google_sso = new libGoogleSSO();
	foreach((array)$google_apis as $google_api){
		$config_index = $google_api->getConfigIndex();
		$gmail_variable = 'gmail_'.$config_index;
		$user = $sso_db->getSSOIdentifier($uid, $userInfo['UserLogin'].'@'.$google_api->getDomain());
		$$gmail_variable = count($user) > 0 && $user[0]['SSOAccountStatus']!='n' && $user[0]['SSOAccountStatus']!='x';
		try{
			$remaining_quota[$config_index] = $lib_google_sso->getRemainingQuota($google_api);
		}catch(Exception $e){
			$remaining_quota[$config_index] = false;
		}
		$total_quota[$config_index] = $ssoservice["Google"]['api'][$config_index]['quota'];
	}
}

# - class & group info -
# class
$classInfo = $laccount->getClassNameClassNoByUserID($uid);
$classNameNo = ($classInfo['ClassName']) ? ($classInfo['ClassName'].($classInfo['ClassNumber']?"-".$classInfo['ClassNumber']:"")) : "---";
# group
/*
$groupInfo = $laccount->getGroupNameByStudentID($uid, Get_Current_Academic_Year_ID());
$groupList = implode(', ', $groupInfo);
if($groupList=="") $groupList = "---";
*/
$gp = $lo->getGroupInfoByUserID($uid, " AND u.GroupID!=2 AND g.AcademicYearID='".Get_Current_Academic_Year_ID()."'");

if(sizeof($gp)>0) {
	$delim = "";
	for($i=0; $i<sizeof($gp); $i++) {
		$groupList .= $delim."(".$gp[$i][0].") ".$gp[$i][1]."";
		$delim = ", ";
	}	
} else {
	$groupList = "---";
}


# role
/*
$roleInfo = $laccount->getGroupRoleByStudentID($uid, Get_Current_Academic_Year_ID());
if($roleInfo=="") $roleInfo = "---";
*/
$role = $lrole->getRoleInfoByUserID($uid);
if(sizeof($role)>0) {
	$delim = "";
	for($i=0; $i<sizeof($role); $i++) {
		$roleInfo .= $delim.$role[$i][0];
		$delim = ", ";
	}
} else {
	$roleInfo = "---";
}




/*
$countrySelection = "<select name='country' id='country'><option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
for($i=0; $i<sizeof($Lang['Country']); $i++) {
	$countrySelection .= "<option value=\"".$Lang['Country'][$i]."\" ".(($Lang['Country'][$i]==$country)?"selected" : "").">".$Lang['Country'][$i]."</option>";	
}
$countrySelection .= "</select>";
*/
$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

if(!isset($comeFrom) || $comeFrom=="")
	$comeFrom = "index.php";

$comeFrom = cleanCrossSiteScriptingCode($comeFrom);
	
if($comeFrom=="notInClass.php") {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"notInClass.php?clearCoo=1",0);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",1);
} else {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",1);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
}
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('$comeFrom')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputStudentDetails'], 1);
if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputGuardianDetail'], 0);
}
if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();


# parent
//$parentList = implode(', ', $parentInfo);
//if($parentList=="") $parentList = "---";
$parentList = $laccount->getParentListByStudentID($uid);

$li = new libuser($uid);
$inputBy = $li->InputBy;
$DateInput = $li->DateInput;
$ModifyBy = $li->ModifyBy;
$DateModified = $li->DateModified;

$userObj = new libuser($inputBy);
if(empty($userObj->UserID)){
    $CreatorUserinfoAry= $userObj->ReturnArchiveStudentInfo($inputBy);
    $CreatorEnglishName = $CreatorUserinfoAry['EnglishName'];
    $CreatorChineseName = $CreatorUserinfoAry['ChineseName'];
}else{
    $CreatorEnglishName = $userObj->EnglishName;
    $CreatorChineseName = $userObj->ChineseName;
}
if($CreatorChineseName){
    $nameDisplay = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
} else{
    $nameDisplay=$CreatorEnglishName;
}

$ModifyUserObj = new libuser($ModifyBy);
if(empty($ModifyUserObj->UserID)){
    $ModifyUserinfoAry= $ModifyUserObj->ReturnArchiveStudentInfo($ModifyBy);
    $ModifierEnglishName = $ModifyUserinfoAry['EnglishName'];
    $ModifierChineseName = $ModifyUserinfoAry['ChineseName'];
}else{
    $ModifierEnglishName = $ModifyUserObj->EnglishName;
    $ModifierChineseName = $ModifyUserObj->ChineseName;
}
if($ModifierChineseName){
    $ModifiernameDisplay = Get_Lang_Selection($ModifierChineseName, $ModifierEnglishName);
} else{
    $ModifiernameDisplay=$ModifierEnglishName;
}


$thisUserType = $li->RecordType;
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;
$SettingArr = $li->RetrieveUserInfoSettingInArrary($SettingNameArr);
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6) $PasswordLength = 6;

########## Official Photo
//$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
$photo_link = $userObj->returnDefaultOfficialPhotoPath();
if ($li->PhotoLink !="")
{
    if (is_file($intranet_root.$li->PhotoLink))
    {
        $photo_link = $li->PhotoLink;
    }
}


########## Personal Photo
//$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
$photo_personal_link = $userObj->returnDefaultPersonalPhotoPath();
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
        if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")) // 2019-11-22 Philips Added Photo delete link
        	$personal_photo_delete = "<br/><a style='display: block; width: 100px;' href=\"javascript:deletePersonalPhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeletePersonalPhoto'] ." </a>";
   }
}


if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    include_once($PATH_WRT_ROOT."includes/cees/libcees.php");  
    $lcees = new libcees();

    $SchoolInfoAry= $lcees->getAllSchoolInfoFromCentralServer('P');
    $i = 0;
    $SchoolNameAry[0][] = '000000';
    $SchoolNameAry[0][] = $Lang['General']['NotApplicable'] ;
    foreach((array)$SchoolInfoAry as $SchoolInfo){
        $i++;
        $SchoolNameAry[$i][] = $SchoolInfo['SchoolCode'];
        $SchoolNameAry[$i][] = $SchoolInfo['NameEn'];
        
    } 
}


	
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function goSubmit(urlLink) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>		
	var obj = document.form1;	
	//if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin." ".$i_UserPassword?>")) return false;
	//if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value!="" && (obj.pwd.value != obj.pwd2.value))
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword']){ ?> 	
	if (obj.pwd.value!=""){
		var check_password_result = CheckPasswordCriteria(obj.pwd.value,'<?=$li->UserLogin?>',<?=$PasswordLength?>);
		if(check_password_result.indexOf(1) == -1){
			var password_warning_msg = '';
			for(var i=0;i<check_password_result.length;i++){
				password_warning_msg += password_warnings[check_password_result[i]] + "\n";
			}
			alert(password_warning_msg);
			obj.pwd.focus();
			return false;
		}
	}
<?php } ?>	
	// check email
	if(!check_text(obj.UserEmail, "<?php echo $i_alert_pleasefillin.$Lang['Gamma']['UserEmail']; ?>."))  return false;
	if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
	 
	if(obj.status[0].checked==false && obj.status[1].checked==false && obj.status[2].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	<?php if($sys_custom['SupplementarySmartCard']){ ?>
	if(obj.smartcardid && obj.smartcardid2 && obj.smartcardid3 && obj.smartcardid4){
		var smartcardid_ary = [];
		var smartcardid_word = [];
		smartcardid_ary.push(obj.smartcardid.value.Trim());
		smartcardid_ary.push(obj.smartcardid2.value.Trim());
		smartcardid_ary.push(obj.smartcardid3.value.Trim());
		smartcardid_ary.push(obj.smartcardid4.value.Trim());
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID']?>');
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID2']?>');
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID3']?>');
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID4']?>');
		
		for(var i=0;i<smartcardid_ary.length;i++){
			for(var j=0;j<smartcardid_ary.length;j++){
				if(i != j){
					if(smartcardid_ary[i]!='' && smartcardid_ary[j]!='' && smartcardid_ary[i] == smartcardid_ary[j]){
						alert(smartcardid_word[j] + "<?=$Lang['AccountMgmt']['IsDuplicatedInput']?>");
						return false;
					}
				}
			}
		}
	}
	<?php } ?>
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
<?php if (!$sys_custom['project']['CourseTraining']['IsEnable']): ?>	
	if(obj.dob.value!="") {
		if(!check_date(obj.dob,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
	if(obj.AdmissionDate.value!="") {
		if(!check_date(obj.AdmissionDate,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
<?php endif; ?>	
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
	if(obj.faxPhone.value!="") {
		if(!check_date(obj.faxPhone,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
<?php
	}
?>	
<?php
if($sys_custom['AccountMgmt']['StudentDOBRequired']){
    ?>
	if(obj.dob.value=="" || obj.dob.value==null) {
		alert("<?=$i_alert_pleasefillin." ".$i_UserDateOfBirth?>");	
		return false;
	}
    <?php
}
?>
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink, id) {
	if(id!=undefined) {
		document.form1.uid.value = id;
		document.form1.fromParent.value = 1;
	}
	document.form1.action = urlLink;
	document.form1.submit();
}
function deletePersonalPhoto() {
	if(confirm("<?=$Lang['Personal']['DeletePersonalPhoto']?>?")){
		window.location = "photo/personalphoto_delete.php?&UserID=<?=$uid?>"
	}
}

//function goURL(urlLink) {
//	document.form1.action = urlLink;
//	document.form1.submit();
//}
</script>
<?php
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){?>
<script src="/templates/polyu_ncs/js/jquery.qrcode.min.js"></script>
<?php 
}
?>
<form name="form1" method="post" action="">
<?php
    if ($sys_custom['project']['CourseTraining']['IsEnable']) {
		include("templates/edit_amway.tmpl.php");
	}
	else {
		include("templates/edit.tmpl.php");
	}
?>

<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="fromParent" id="fromParent" value="">
<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
<input type="hidden" name="skipStep2" id="skipStep2" value="">
<input type="hidden" name="targetClass" id="targetClass" value="<?=cleanHtmlJavascript($targetClass)?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=cleanHtmlJavascript($recordstatus)?>">
<input type="hidden" name="original_pass" id="original_pass" value="<?=cleanHtmlJavascript($original_pass)?>">
<input type="hidden" name="keyword" id="keyword" value="<?=cleanHtmlJavascript($keyword)?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>