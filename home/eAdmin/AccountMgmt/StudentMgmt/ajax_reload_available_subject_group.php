<?php
/*
 * Change Log:
 * Date 2017-04-11 Villa
 * 		-Remove Unknown &nbsp; for the option value
 * Date 2017-03-27 Villa #S115131 
 * 		-Restructure the $row data to subject-Index data 
 * 		-Rewrite the Selection Box Structure
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();

$sql = "SELECT ".Get_Lang_Selection("CH_DES","EN_DES")." as Subject, ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." as SubjectGroupName, stc.SubjectGroupID, st.SubjectID FROM SUBJECT_TERM_CLASS stc LEFT OUTER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stc.SubjectGroupID) LEFT OUTER JOIN ASSESSMENT_SUBJECT sub ON (sub.RecordID=st.SubjectID) WHERE stc.SubjectGroupID IN ($avaSbjGroupStr) ORDER BY DisplayOrder,".Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
$row = $ldb->returnResultSet($sql);
foreach ((array)$row as $_row){
	$index = count($SubjectGroup[$_row['SubjectID']]);
	$SubjectGroup[$_row['SubjectID']][$index]['SubjectID'] = $_row['SubjectID'];
	$SubjectGroup[$_row['SubjectID']][$index]['SubjectName'] = $_row['Subject'];
	$SubjectGroup[$_row['SubjectID']][$index]['SubjectGroupName'] = $_row['SubjectGroupName'];
	$SubjectGroup[$_row['SubjectID']][$index]['SubjectGroupID'] = $_row['SubjectGroupID'];
	
}
// debug_pr($SubjectGroup);
//$avaSubjectGroup = explode(',',$avaSbjGroupStr);
//$selectedSubjectGroup = explode(',',$selectedSbjGroupStr);


$groupAry = array();

$x .= "<select name=AvailableSbjGpID[] id=AvailableSbjGpID size=10 multiple>\n";

// for($i=0; $i<sizeof($row); $i++)
// {
// 	if(!in_array($row[$i][0], $groupAry)) {
// 		$x .= "<optgroup label='".$row[$i][0]."'></optgroup>";
// 		array_push($groupAry, $row[$i][0]);
// 	}			
// 	$gpID = $row[$i][2];
// 	$gpName = $row[$i][1];
// 	$x .= "<option value=$gpID>$gpName</option>\n";
// }
foreach ((array)$SubjectGroup as $Subject){
	$x .= "<optgroup label='".$Subject[0]['SubjectName']."'>";
	foreach ((array)$Subject as $_SubjectGroup){
		$x .= "<option value='".$_SubjectGroup['SubjectGroupID']."'>".$_SubjectGroup['SubjectGroupName']."</option>\n";
	}
	$x .= "</optgroup>";
}

$x .= "</select>\n";
 
echo $x;
?>