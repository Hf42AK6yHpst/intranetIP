<?php
# modifying : 

############ Change Log Start ###############
#	Date:	2014-03-11	Carlos
#			Added [Import Forwarding Emails] & [Export Forwarding Emails] buttons for Lassel College
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
#	Date: 	2012-10-29 	Rita
#			add parent column
#
#	Date:	2011-09-27 YatWoon
#			remove the js alert for "delete" button
#	
#	Date:	2011-01-27 Yatwoon
#			can also search with SmartCardID
# 
#	Date:	2011-01-10	YatWoon
#			- IP25 UI
#			- Add "Active", "Suspend" tool options
#			- set cookies
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_parent_targetClass", "targetClass");
$arrCookies[] = array("ck_parent_recordstatus", "recordstatus");
$arrCookies[] = array("ck_parent_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}


intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();
if(sizeof($officalPhotoMgmtMember) && in_array($UserID, $officalPhotoMgmtMember))
	$canAccessOfficalPhotoSetting = 1;

if(!$sys_custom['SISUserManagement']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] ||$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && $canAccessOfficalPhotoSetting) {
	header("Location: photo/index.php");
}


$lclass = new libclass();
$linterface = new interface_html();

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index_sis.php?clearCoo=1",1);
//$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass_sis.php?clearCoo=1",0);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# online help button
$onlineHelpBtn = gen_online_help_btn_and_layer('user','student');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

//$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0);
//$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0);
//$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
//$toolbar .= $linterface->GET_LNK_EMAIL("javascript:checkPost(document.form1,'email.php')",$button_email,"","","",0);
//if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['BatchSetForwardingEmail']){
//	$toolbar .= $linterface->GET_LNK_IMPORT("../import/import_forwarding_email.php?TabID=".USERTYPE_STUDENT,$Lang['AccountMgmt']['ImportForwardingEmails'],"","","",0);
//	$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_STUDENT,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
//}

# student selection
$studentTypeMenu = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm()\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, "");

# Record Status 
$recordStatusMenu = "<select name=\"recordstatus\" id=\"recordstatus\" onChange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($recordstatus) || $recordstatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($recordstatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($recordstatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "<option value='3'".(($recordstatus=='3') ? " selected" : "").">{$Lang['Status']['Left']}</option>";
$recordStatusMenu .= "</select>";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND USR.RecordStatus=".$recordstatus;

if($targetClass!="") {
	if($targetClass=='0') {					# all classes
		# do nothing	
	} else if(is_numeric($targetClass)) {	# All students in specific form
		$conds .= " AND y.YearID=$targetClass";
	} else {								# All students in specific class
		$conds .= " AND yc.YearClassID=".substr($targetClass, 2);
	}
}

if($keyword != "")	
	$conds .= " AND (UserLogin like '%$keyword%' OR
				UserEmail like '%$keyword%' OR
				EnglishName like '%$keyword%' OR
				ChineseName like '%$keyword%' OR
				ClassName like '%$keyword%' OR
				WebSamsRegNo like '%$keyword%' OR
				CardID like '%$keyword%'
				)";
	
$conds .= " AND yc.AcademicYearID=".get_Current_Academic_Year_ID();	

$sql = "SELECT
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
			ycu.ClassNumber as clsNo,
			EnglishName,
			IFNULL(IF(ChineseName='', '---', ChineseName),'---') as ChineseName,
			USR.UserID as StudentID,
			UserLogin,
			IFNULL(LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				WHEN 3 THEN '". $Lang['Status']['Left'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox,
			USR.UserID
		FROM 
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
		WHERE
			RecordType = ".TYPE_STUDENT."
			$conds ";
	

$li->sql = $sql;
//$li->field_array = array("clsName", "concat(TRIM(SUBSTRING_INDEX(clsNo, '-', 1)), IF(INSTR(clsNo, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(clsNo, '-', -1)), 10, '0')))", "EnglishName", "ChineseName", "UserLogin", "LastUsed", "recordStatus");
$li->field_array = array("clsName", "clsNo", "EnglishName", "ChineseName", "StudentID", "UserLogin", "LastUsed", "recordStatus");
$li->no_col = sizeof($li->field_array)+2;
if($field!=1)	
	$li->fieldorder2 = ", USR.ClassNumber ASC";
$li->IsColOff = "UserMgmtStudentAccount";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_general_class)."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $i_ClassNumber)."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['Identity']['Parent'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";


# student license
if (isset($account_license_student) && $account_license_student>0)
{
	$license_student_used = $li->getLicenseUsed();
	$license_student_left = $account_license_student - $license_student_used;
	$student_license_text = str_replace("<!--license_total-->", $account_license_student, $i_license_sys_msg);
	$student_license_text = str_replace("<!--license_used-->", $license_student_used, $student_license_text);
	$student_license_text = str_replace("<!--license_left-->", $license_student_left, $student_license_text);
	$student_license_text .= "<br />";
}

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "index_sis.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit_sis.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function goURL(thisURL, uid) {
	document.form1.action = thisURL;
	document.form1.uid.value = uid;
	document.form1.submit();
}

</script>
<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2"><?=$student_license_text?></td>
</tr>
<tr>
	<td valign="bottom">
		<?=$studentTypeMenu?> <?=$recordStatusMenu?>
	</td>
</tr>
</table>


<?= $li->display()?>


	
<input type="hidden" name="TabID" id="TabID" value="" />
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/StudentMgmt/index_sis.php">
<input type="hidden" name="clearCoo" id="clearCoo" value="" />
</form>
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>