<?php
// Editing by 
/*
 * Modification:
 * Date: 2020-10-21 Tommy
 *  - fix update sql, added modifyby user
 * Date: 2019-04-30 Cameron
 *  - fix cross site scripting by applying cleanCrossSiteScriptingCode() to variables
 * Date: 2016-09-21 Carlos
 *  - suspend Webmail / iMail plus account and disable auto forward and auto reply functions.  
 * Date: 2013-02-08	YatWoon
 *  - use "user_id[]" instead of userID[] 
 * Date: 2012-08-01 (Ivan)
 * 	- added logic to syn user info to library system
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$user_id = IntegerSafe($user_id);
if(sizeof($user_id)==0) {
	header("Location : index.php");	
	exit;
}

$laccount = new libaccountmgmt();
$luser = new libuser();
$successAry = array();

$thisYear = date('Y');
$sql = "UPDATE INTRANET_USER SET RecordStatus = '3', YearOfLeft=IF(YearOfLeft='' OR YearOfLeft IS NULL,'". $thisYear ."',YearOfLeft), DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' WHERE UserID IN (".implode(",", $user_id).")";
$successAry['updateIntranetUserDb'] = $laccount->db_db_query($sql);
if($sys_custom['project']['NCS']){
	include_once($intranet_root."/includes/libcurdlog.php");
	$log = new libcurdlog();
	$userSql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ('".implode("','", $user_id)."')";
	$stdList = $log->returnVector($userSql);
	$log->addlog("User Settings", "UPDATE", "INTRANET_USER", "UserID", implode(",", $user_id), "", "LEFT: ".implode(",",$stdList), $sql);
	unset($log);
}

$successAry['synUserDataToModules'] = $luser->synUserDataToModules($user_id);

foreach($user_id as $uid)
{
	$successAry['suspendiMail_'.$uid] = $laccount->suspendEmailFunctions($uid);
}

intranet_closedb();

if(!in_array(false, $successAry))
	$xmsg = "UpdateSuccess";
else 
	$xmsg = "UpdateUnsuccess";

$comeFrom = cleanCrossSiteScriptingCode($comeFrom);
header("Location: $comeFrom?&xmsg=$xmsg");

?>