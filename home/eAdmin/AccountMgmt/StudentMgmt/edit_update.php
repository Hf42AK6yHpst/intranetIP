<?php
# using: 

############# Change Log
#	Date:	2020-09-21 Philips
#			Added KISClassType for $plugin['SDAS_module']['KISMode']
#
#	Date:	2020-03-16 Philips
#			Added syncToStudentRegistry for cdsj_5_macau (customization)
#
#   Date:   2019-11-19 Paul
#           Fixed: updated Google SSO not to sync data on is_readonly case
#
#   Date:   2019-10-25 Cameron
#           Fixed: strip blackslash when it contains apostrophe in address field
#
#   Date:   2019-08-15 Pun [166383] [ip.2.5.10.10.1]
#           Fixed cannot save if mssch_printing_module is on and quit time is empty
#
#   Date:   2019-08-08 Chris
#           Updated Google SSO Implementation
#
#   Date:   2019-07-04 Pun
#           Added QuitTime for MSSCH Printing cust
#
#	Date:	2019-06-24 Carlos
#			Empty UserPassword field.
#
#   Date:   2019-04-30 Cameron
#           fix cross site scripting by applying cleanHtmlJavascript() to hidden variables
#
#   Date:   2018-09-19 Anna
#           Added PrimarySchoolCode
#
#   Date:   2018-09-03 (Bill)   [2017-1207-0956-53277]
#           added Graduation Date
#
#   Date:   2018-08-30 (Pun) [ip.2.5.9.10.1]
#           Modified syncUserFromEClassToGoogle(), added $chiname
#
#	Date:	2018-01-13 Pun [ip.2.5.9.3.1]
#           Added NCS cust
#
#	Date:	2018-01-03 Carlos - Improved password checking.
#
#	Date:	2017-01-16 HenryHM
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added new text field HouseholdRegister.
#
#	**** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
#	Date:	2016-08-03 Ivan [ip.2.5.7.9.1]
#			added CompanyName for macau pui ching (but treat as general improvement)
#	**** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-10-22	Pun
#			Added checkbox "Add new Students from Subject Group to eClass" for subject group
#
#	Date:	2015-08-14 (Omas)
#			Add Edit Log
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions.
#
#	Date:	2015-03-23 (Bill)
#			fixed - cannot add student to usergroup if usergroup without default group role
# 												
#	Date:	2014-12-10 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4
#
#	Date:	2014-09-22 (Bill)
#			Add RoleID to INTRANET_USERGROUP to display default role in Group
#
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	2014-05-07 (Carlos): Added suspend/unsuspend iMail plus account api call
#
#	Date:	2013-12-12	Fai
#			Add a cust attribute "stayOverNight"
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3
#
#	Date:	2013-03-15	Carlos
#			remove shared group calendars for user and add shared group calendars to user
#
#	Date:	2012-10-03	Carlos
#			fix failed to enable iMail plus account when password method is HASHED
# 
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date: 	2012-08-3 Bill Mak
#			update fields in personal settings via updateUserPersonalSetting();
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2012-05-15 	Kelvin
#			added stripslashes to english name and chinese name
#
#	Date:	2012-04-02 	Jason
#			fix the problem of the failure of updating user_email from ip to eclass
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date:	2012-02-07	YatWoon
#			fixed: failed to update hashed password due to mysql syntax error
#
#	Date:	2011-09-30  Carlos
#			update two-way hashed/encrypted password if password is modified
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	Date:	2011-03-28	YatWoon
#			change email notification subject & content data 
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	Date: 	2010-12-30 (Jason)
#			change the update eclass info function by eClass40UserUpdateInfo()
#
#	Date:	2010-11-30	(Henry Chow)
#			when modify subject group list, also update the member list in "Classroom" of eClass
#
#	Date:	2010-11-19	(Henry Chow)
#			add selection of "Group" & "Role"
#
#	Date:	2010-11-05	Marcus
#			Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");


intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if(sizeof($_POST)==0) {
	header("Location: index.php");	
	exit;
}

$magic_quotes_enabled = function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc();

### start handle sql injection and cross site scripting
$uid = IntegerSafe($uid);
$status = IntegerSafe($status);
$comeFrom = cleanCrossSiteScriptingCode($comeFrom);

$targetClass = cleanHtmlJavascript($targetClass);
$recordstatus = cleanHtmlJavascript($recordstatus);
$keyword = cleanHtmlJavascript($keyword);
$userlogin = cleanHtmlJavascript($userlogin);
$pwd = cleanHtmlJavascript($pwd);
$smartcardid=cleanHtmlJavascript($smartcardid);
if($sys_custom['SupplementarySmartCard']){
    $smartcardid2 = cleanHtmlJavascript($smartcardid2);
    $smartcardid3 = cleanHtmlJavascript($smartcardid3);
    $smartcardid4 = cleanHtmlJavascript($smartcardid4);
}
if($plugin['medical']){	
    $stayOverNight = IntegerSafe($stayOverNight);
}
if($plugin['SDAS_module']['KISMode']){
	$KISClassType = IntegerSafe($KISClassType);
}

$strn = cleanHtmlJavascript($strn);
$WebSAMSRegNo = cleanHtmlJavascript($WebSAMSRegNo);
$HKJApplNo = cleanHtmlJavascript($HKJApplNo);
$barcode = cleanHtmlJavascript($barcode);
$engname = cleanHtmlJavascript($engname);
$chiname = cleanHtmlJavascript($chiname);
$nickname = cleanHtmlJavascript($nickname);
$dob = cleanHtmlJavascript($dob);
$gender = cleanHtmlJavascript($gender);
$hkid = cleanHtmlJavascript($hkid);
$address = cleanHtmlJavascript(standardizeFormPostValue($address));
$country = cleanHtmlJavascript($country);
$homePhone = cleanHtmlJavascript($homePhone);
$mobilePhone = cleanHtmlJavascript($mobilePhone);
$faxPhone = cleanHtmlJavascript($faxPhone);
$open_webmail = cleanHtmlJavascript($open_webmail);
$open_file = cleanHtmlJavascript($open_file);
$remark = cleanHtmlJavascript($remark);

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
	$PrimarySchoolCode = cleanHtmlJavascript($PrimarySchoolCode);     
}

if($sys_custom['StudentAccountAdditionalFields']){
	$NonChineseSpeaking = cleanHtmlJavascript($NonChineseSpeaking);
	$SpecialEducationNeeds = cleanHtmlJavascript($SpecialEducationNeeds);
    $PrimarySchool = cleanHtmlJavascript($PrimarySchool);
    $University = cleanHtmlJavascript($University);
	$Programme = cleanHtmlJavascript($Programme);
}
if($sys_custom['BIBA_AccountMgmtCust']){
    $HouseholdRegister = cleanHtmlJavascript($HouseholdRegister);
}
$Nationality = cleanHtmlJavascript($Nationality);
$PlaceOfBirth = cleanHtmlJavascript($PlaceOfBirth);
$AdmissionDate = cleanHtmlJavascript($AdmissionDate);
$GraduationDate = cleanHtmlJavascript($GraduationDate);
$QuitTime = cleanHtmlJavascript($QuitTime);
if($plugin['radius_server']){
    $enable_wifi_access = IntegerSafe($enable_wifi_access);
}
$errorList = cleanHtmlJavascript($errorList);
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
    $staffInCharge = cleanHtmlJavascript($staffInCharge);
	$hkDuration = cleanHtmlJavascript($hkDuration);
	$b5Priority = cleanHtmlJavascript($b5Priority);
	$enPriority = cleanHtmlJavascript($enPriority);
	$hinduPriority = cleanHtmlJavascript($hinduPriority);
	$isNCS = cleanHtmlJavascript($isNCS);
	$NGO = cleanHtmlJavascript($NGO);
}
### end handle sql injection and cross site scripting

$lauth = new libauth();
$laccount = new libaccountmgmt();
$li = new libregistry();
$lu = new libuser($uid);
$lwebmail = new libwebmail();
$le = new libeclass();
$lcalendar = new icalendar();

# Log
if($sys_custom['AccountMgmt']['LogEditImportUser']){
	include_once($PATH_WRT_ROOT."includes/libupdatelog.php");
	$liblog = new libupdatelog();
	$liblog->INSERT_UPDATE_LOG('AccountMgmt', 'StudentMgmt', 'target='.$uid);
	unset($liblog);
}

$userInfo = $laccount->getUserInfoByID($uid);

if($sys_custom['UseStrongPassword']){
	if($pwd != ''){
		$thisUserType = $lu->RecordType;
		$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
		$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
		$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;
		$SettingArr = $lu->RetrieveUserInfoSettingInArrary($SettingNameArr);
		$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
		if ($PasswordLength<6) $PasswordLength = 6;
		$check_password_result = $lauth->CheckPasswordCriteria($pwd,$lu->UserLogin,$PasswordLength);
		if(!in_array(1,$check_password_result)){
			$error_msg[] = $pwd;
		}
	}
}

# check email
$UserEmail = intranet_htmlspecialchars(trim($UserEmail));
if ($UserEmail != "" && $userInfo['UserEmail'] != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail) || $le->isEmailExistInSystem($UserEmail))
    {
        header("Location: edit.php?uid=$uid&xmsg=EmailUsed");
        exit;
    }
}

// # retrieve original email
// $sql = "select UserEmail from INTRANET_USER where UserID=$uid";
// $result = $lu->returnVector($sql);
// $ori_UserEmail = $result[0];
$ori_UserEmail = $lu->UserEmail;
$new_UserEmail = $UserEmail;

$iportfolio_activated = false;

if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();

		for($i=0;$i<sizeof($data);$i++){
			if($uid==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}

}
//$iportfolio_activated = false;

## check Smart Card ID duplication ###
if($smartcardid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE (CardID='$smartcardid'".($sys_custom['SupplementarySmartCard']?" OR CardID2='$smartcardid' OR CardID3='$smartcardid' OR CardID4='$smartcardid'":"").") AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid2!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE (CardID='$smartcardid2' OR CardID2='$smartcardid2' OR CardID3='$smartcardid2' OR CardID4='$smartcardid2') AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid2;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid3!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE (CardID='$smartcardid3' OR CardID2='$smartcardid3' OR CardID3='$smartcardid3' OR CardID4='$smartcardid3') AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid3;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid4!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE (CardID='$smartcardid4' OR CardID2='$smartcardid4' OR CardID3='$smartcardid4' OR CardID4='$smartcardid4') AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid4;
	}
}

## check STRN duplication ###
if($strn!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE STRN='$strn' AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $strn;
	}
}

## check WebSAMSRegNo duplication ###
if(!$iportfolio_activated) {
	if($WebSAMSRegNo!=""){
		if(substr(trim($WebSAMSRegNo),0,1)!="#")
			$WebSAMSRegNo = "#".$WebSAMSRegNo;
			
		$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo='$WebSAMSRegNo' AND RecordType=".TYPE_STUDENT." AND UserID!='$uid'";
		$temp = $laccount->returnVector($sql);
		if(sizeof($temp)!=0){
			$error_msg[] = $WebSAMSRegNo;
		}
	}
}

## check JUPAS APPLICATION NO duplication ###
if($HKJApplNo!="" && $HKJApplNo != 0){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKJApplNo='$HKJApplNo' AND RecordType=".TYPE_STUDENT." AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $HKJApplNo;
	}
}

## check HKID duplication ###
if($hkid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKID='$hkid' AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $hkid;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode' AND UserID!='$uid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

## check QuitTime format ##
if ($plugin['mssch_printing_module'] && $QuitTime) {
    $QuitTimeArr = explode(' ', $QuitTime);
    if(!checkDateIsValid($QuitTimeArr[0]) || !checkTimeIsValid("{$QuitTimeArr[1]}:59")) {
        $error_msg[] = $QuitTime;
    }
}

if(sizeof($error_msg)>0) {
	$action = "edit.php";
	$errorList = implode(',', $error_msg);	
}


if(sizeof($error_msg)>0) { 


?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="edit.php">
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
		<input type="hidden" name="uid" id="uid" value="<?=$uid?>">	
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
	<?php if($sys_custom['SupplementarySmartCard']){ ?>	
		<input type="hidden" name="smartcardid2" id="smartcardid2" value="<?=$smartcardid2?>">
		<input type="hidden" name="smartcardid3" id="smartcardid3" value="<?=$smartcardid3?>">
		<input type="hidden" name="smartcardid4" id="smartcardid4" value="<?=$smartcardid4?>">
	<?php } ?>

		<?php if($plugin['medical']){ ?>	
				<input type="hidden" name="stayOverNight" id="stayOverNight" value="<?=$stayOverNight?>">
		<?php } ?>

		<input type="hidden" name="strn" id="strn" value="<?=$strn?>">
		<input type="hidden" name="WebSAMSRegNo" id="WebSAMSRegNo" value="<?=$WebSAMSRegNo?>">
		<input type="hidden" name="HKJApplNo" id="HKJApplNo" value="<?=$HKJApplNo?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="engname" id="engname" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="dob" id="dob" value="<?=$dob?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
		
		<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){ ?>
	    <input type="hidden" name="PrimarySchoolCode" id="PrimarySchoolCode" value="<?=$PrimarySchoolCode?>" />     
		<? }?>
	
		<?php if($sys_custom['StudentAccountAdditionalFields']){ 
    		if($magic_quotes_enabled){
    			$PrimarySchool = stripslashes($PrimarySchool);
    			$University = stripslashes($University);
    			$Programme = stripslashes($Programme);
    		}
	    ?>	
		<input type="hidden" name="NonChineseSpeaking" id="NonChineseSpeaking" value="<?=$NonChineseSpeaking?>" />
		<input type="hidden" name="SpecialEducationNeeds" id="SpecialEducationNeeds" value="<?=$SpecialEducationNeeds?>" />
		<input type="hidden" name="PrimarySchool" id="PrimarySchool" value="<?=intranet_htmlspecialchars($PrimarySchool)?>" />
		<input type="hidden" name="University" id="University" value="<?=intranet_htmlspecialchars($University)?>" />
		<input type="hidden" name="Programme" id="Programme" value="<?=intranet_htmlspecialchars($Programme)?>" />
    	<?php } ?>
    	<?php if($sys_custom['BIBA_AccountMgmtCust']){ ?>
		<input type="hidden" name="HouseholdRegister" id="HouseholdRegister" value="<?=intranet_htmlspecialchars($HouseholdRegister)?>" />
		<?php } ?>
		<input type="hidden" name="Nationality" id="Nationality" value="<?=$Nationality?>">
		<input type="hidden" name="PlaceOfBirth" id="PlaceOfBirth" value="<?=$PlaceOfBirth?>">
		<input type="hidden" name="AdmissionDate" id="AdmissionDate" value="<?=$AdmissionDate?>">
		<input type="hidden" name="GraduationDate" id="GraduationDate" value="<?=$GraduationDate?>">
        <?php if($plugin['mssch_printing_module']){ ?>
		<input type="hidden" name="QuitTime" id="QuitTime" value="<?=$QuitTime?>" />
		<?php } ?>
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
<?php if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){?>
		<input type="hidden" name="staffInCharge" id="staffInCharge" value="<?=$staffInCharge?>" />
		<input type="hidden" name="hkDuration" id="hkDuration" value="<?=$hkDuration?>" />
		<input type="hidden" name="b5Priority" id="b5Priority" value="<?=$b5Priority?>" />
		<input type="hidden" name="enPriority" id="enPriority" value="<?=$enPriority?>" />
		<input type="hidden" name="hinduPriority" id="hinduPriority" value="<?=$hinduPriority?>" />
		<input type="hidden" name="isNCS?" id="isNCS?" value="<?=$isNCS?>" />
		<input type="hidden" name="NGO" id="NGO" value="<?=$NGO?>" />
<?php }?>
<?php if($plugin['SDAS_module']['KISMode']){?>
		<input type="hidden" name="KISClassType" id="KISClassType" value=<?=$KISClassType?> />
<?php }?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit();  // form submit and exit the program	
	} else { 	
		
	
	/*
	debug_pr($lu); 
	$userlogin = $lu->UserLogin;
	$email = $lu->UserEmail;
	$original_pass = $lu->UserPassword;
	*/
	$userlogin = $userInfo['UserLogin'];
// 	$email = $userInfo['UserEmail'];
	
	if ($intranet_authentication_method=="HASH" && $pwd!='') {
		$modifyPassword = 1;
	}
	else {
		$modifyPassword = ($pwd!="" && $pwd!=$original_pass) ? 1 : 0;
	}
		
	
	# create & update info in INTRANET_USER
	$UserPassword = trim($pwd);
	$CardID = trim($smartcardid);
	if($sys_custom['SupplementarySmartCard']){
		$CardID2 = trim($smartcardid2);
		$CardID3 = trim($smartcardid3);
		$CardID4 = trim($smartcardid4);
	}
	if($plugin['medical']){
		$stayOverNight = trim($stayOverNight);
	}
	if($plugin['SDAS_module']['KISMode']){
		$KISClassType = trim($KISClassType);
	}
	$STRN = trim($strn);
	if(!$iportfolio_activated) {
		$WebSAMSRegNo = trim($WebSAMSRegNo);
	}
	$NickName = intranet_htmlspecialchars(trim($nickname));
	$DateOfBirth = trim($dob);
	$Gender = trim($gender);
	$HKID = trim($hkid);
//	$Address = intranet_htmlspecialchars(trim($address));
    $Address = standardizeFormPostValue($address);
	$Country = trim($country);
	$HomeTelNo = intranet_htmlspecialchars(trim($homePhone));
	$MobileTelNo = intranet_htmlspecialchars(trim($mobilePhone));
	$FaxNo = intranet_htmlspecialchars(trim($faxPhone));
	$Remark = intranet_htmlspecialchars(trim($remark));
	$Nationality_ = intranet_htmlspecialchars(trim($Nationality));
	$Place_OfBirth = intranet_htmlspecialchars(trim($PlaceOfBirth));
	$Admission_Date = intranet_htmlspecialchars(trim($AdmissionDate));
	$Graduation_Date = intranet_htmlspecialchars(trim($GraduationDate));
    $QuitTime = intranet_htmlspecialchars(trim($QuitTime));

	$dataAry = array();
	
	if($modifyPassword) {
		if ($intranet_authentication_method=="HASH") {
// 			$dataAry['UserPassword'] = "HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
			$dataAry['UserPassword'] = "";
			$dataAry['HashedPass'] = "md5('$userlogin$UserPassword$intranet_password_salt')";
		} else {
			$dataAry['UserPassword'] = "";
		}
	}
	
	if(!$modifyPassword){
		if($intranet_authentication_method=="HASH"){
			$UserPassword = $lauth->GetUserDecryptedPassword($uid);
		}else{
			$UserPassword = $userInfo['UserPassword'];
		}
	}
		
	if($status==3) {
		$dataAry['YearOfLeft'] = ($userInfo['YearOfLeft']=="") ? date('Y') : $userInfo['YearOfLeft'];
	} else {
		$dataAry['YearOfLeft'] = '';
	}	
	$dataAry['EnglishName'] = intranet_htmlspecialchars(trim($engname));
	$dataAry['ChineseName'] = intranet_htmlspecialchars(trim($chiname));
	
	# added on 2012-05-15
	$dataAry['EnglishName'] = stripslashes($dataAry['EnglishName']);
	$dataAry['ChineseName'] = stripslashes($dataAry['ChineseName']);
	###############################################################
	
	$dataAry['RecordStatus'] = $status;
	$dataAry['CardID'] = $CardID;
	if($sys_custom['SupplementarySmartCard']){
		$dataAry['CardID2'] = $CardID2;
		$dataAry['CardID3'] = $CardID3;
		$dataAry['CardID4'] = $CardID4;
	}
	if(!$iportfolio_activated)
		$dataAry['WebSAMSRegNo'] = $WebSAMSRegNo;
	$dataAry['HKJApplNo'] = $HKJApplNo;
	$dataAry['Barcode'] = $barcode;
	$dataAry['NickName'] = $NickName;
	$dataAry['DateOfBirth'] = $DateOfBirth;
	$dataAry['Gender'] = $Gender;
	$dataAry['Address'] = $Address;
	$dataAry['Country'] = $Country;
	$dataAry['HomeTelNo'] = $HomeTelNo;
	$dataAry['MobileTelNo'] = $MobileTelNo;
	$dataAry['FaxNo'] = $FaxNo;
	if($special_feature['ava_strn']) 
		$dataAry['STRN'] = $STRN;
	if($special_feature['ava_hkid']) 
		$dataAry['HKID'] = $HKID;
	$dataAry['Remark'] = $Remark;
	$dataAry['UserEmail'] = intranet_htmlspecialchars(trim($UserEmail));
	
	if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
// 	    include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
// 	    $libpf_exam = new libpf_exam();
	    // primary school code 
	    $dataAry['PrimarySchoolCode'] = $PrimarySchoolCode;
	    
	    // JUPAS RESULT 
// 	    $JupasCode = $libpf_exam->getJUPASCodeBySchoolNameProgram($UniversityCode,$UniversityProgramme);
// 	    $_offerJupasCode_db = $JupasCode[0];

// 	    $JUPASDataAry[] = "( '$academicYearID', '$uid', '$HKJApplNo',
// 	    '$JupasCode', '$UniversityCode', '',
// 	    '', '', '',
// 	    now(),'".$_SESSION['UserID']."', now(),'".$_SESSION['UserID']."' )";
// 	    $libpf_exam->DeleteStudentJUPASRecord($uid);
// 	    $success_insert = $libpf_exam->insertJupasStudentData($JUPASDataAry);

	    // EXIT TO choice

// 	    $ExitToDataAry['FinalChoice']=$FinalChoice;
// 	    if($FinalChoice == '2'){
// 	        $Remark = $Remark1;
// 	    }
// 	    else if($FinalChoice == '3'){
// 	        $Remark = $Remark2;
// 	    }
// 	    $ExitToDataAry['Remark']=$Remark;
	    
// 	    $ExistedExitInfo = $libpf_exam->Get_User_ExitTo_Info($uid);
// 	    if(empty($ExistedExitInfo)){
	        
// 	        $GraduationYear = getAcademicYearInfoAndTermInfoByDate($Graduation_Date);
// 	        $GraduationAcademicYearID = $GraduationYear[0];
	 
	         
// 	        $ExitToDataAry['AcademicYearID'] = $GraduationAcademicYearID;
// 	        $ExitToDataAry['StudentID'] = $uid;
// 	        $ExitToDataAry['JupasAppNo'] = $HKJApplNo;
// 	        $ExitToDataAry['JupasCode'] = '';
	        
// 	        $ExitToSuccess = $libpf_exam->Insert_User_ExitTo_Info($ExitToDataAry);        
// 	    }else{
// 	        $ExitToSuccess = $libpf_exam->Update_User_ExitTo_Info($uid, $ExitToDataAry);
	        
// 	    }
	}
	
	
	
	if($sys_custom['StudentAccountAdditionalFields']){
		$PrimarySchool = trim($PrimarySchool);
		if(!$magic_quotes_enabled){
			$PrimarySchool = addslashes($PrimarySchool);
		}
		$University = trim($University);
		if(!$magic_quotes_enabled){
			$University = addslashes($University);
		}
		$Programme = trim($Programme);
		if(!$magic_quotes_enabled){
			$Programme = addslashes($Programme);
		}
		
		$dataAry['NonChineseSpeaking']=$NonChineseSpeaking;
		$dataAry['SpecialEducationNeeds']=$SpecialEducationNeeds;
		$dataAry['PrimarySchool']=$PrimarySchool;
		$dataAry['University']=$University;
		$dataAry['Programme']=$Programme;
	}
	
	
	if($sys_custom['BIBA_AccountMgmtCust']){
		$HouseholdRegister = trim($HouseholdRegister);
		if(!$magic_quotes_enabled){
				$HouseholdRegister = addslashes($HouseholdRegister);
		}
		$dataAry['HouseholdRegister'] = $HouseholdRegister;
	}
	
	$success = $laccount->updateUserInfo($uid, $dataAry);
	
	$dataAry_PersonalSetting = array();

	$dataAry_PersonalSetting['Nationality'] = $Nationality_;
	$dataAry_PersonalSetting['PlaceOfBirth'] = $Place_OfBirth;
	$dataAry_PersonalSetting['AdmissionDate'] = $Admission_Date;
	$dataAry_PersonalSetting['GraduationDate'] = $Graduation_Date;
    if ($plugin['mssch_printing_module']) {
        $dataAry_PersonalSetting['QuitTime'] = $QuitTime;
    }
    if($plugin['medical']){
		$dataAry_PersonalSetting['stayOverNight'] = $stayOverNight;
    }
    if($plugin['SDAS_module']['KISMode']){
    	$dataAry_PersonalSetting['KISClassType'] = $KISClassType;
    }
	$success_ps = $laccount->updateUserPersonalSetting($uid, $dataAry_PersonalSetting);

	if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
		$dataAry_NCSSettings = array();
		$dataAry_NCSSettings['StaffInCharge'] = $staffInCharge;
		$dataAry_NCSSettings['DurationInHK'] = $hkDuration;
		$dataAry_NCSSettings['IsNCS'] = $isNCS;
		$dataAry_NCSSettings['NGO'] = $NGO;
		
		$dataAry_NCSSettings['LanguagePriority']=implode(", ",(array)$LangPriority);
		if($OthersPriority){
		    $dataAry_NCSSettings['LanguagePriority'] .= ', Others: ';
		    $dataAry_NCSSettings['LanguagePriority'] .= trim($OthersPriority_details);
		    $dataAry_NCSSettings['LanguagePriority'] = trim($dataAry_NCSSettings['LanguagePriority'],', ');
		}
		
		$success_ncs = $laccount->updateUserNCSSetting($uid, $dataAry_NCSSettings);
	}
	
	if($modifyPassword && $success && $success_ps){
		$lauth->UpdateEncryptedPassword($uid, $UserPassword);
	}
	
	$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='$uid'";
	$existing_groups = $lu->returnVector($sql);
				
	if($plugin["imail_gamma"])
	{
		$IMap = new imap_gamma();
		$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		if($EmailStatus=="enable")
		{
			if($IMap->is_user_exist($IMapEmail))
			{
				$account_exist = true;
				if($modifyPassword)
					$IMap->change_password($IMapEmail, $UserPassword);
			}
			else
			{
				$igmma_password = $UserPassword;
				
				$account_exist = $IMap->open_account($IMapEmail, $igmma_password);
				// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
				$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
				if($internal_only[USERTYPE_STUDENT-1]){ // shift index by one
					$IMap->addGroupBlockExternal(array($IMapEmail));
					$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
			    	$li->db_db_query($sql);
				}
			}
			if($account_exist)
			{
				$laccount->setIMapUserEmail($uid,$IMapEmail);
				
				$result["setQuota"] = $IMap->SetTotalQuota($IMapEmail,$Quota, $uid);
				
				$IMap->setUnsuspendUser($IMapEmail,"iMail",$UserPassword); // resume iMail plus account
			}
		}
		else
		{
			# disable gamma mail
			$laccount->setIMapUserEmail($uid,"");
			
			$IMap->setSuspendUser($IMapEmail,"iMail"); // suspend iMail plus account
		}
	}
	
	if(!$plugin["imail_gamma"])
	{
		# update webmail password
		if ($modifyPassword && $lwebmail->has_webmail)
			$lwebmail->change_password($userlogin, $UserPassword, "iMail");
	}
	
	# FTP management
	if ($plugin['personalfile'])
	{
		$lftp = new libftp();
		if ($modifyPassword && $lftp->isFTP)
		{
			$lftp->changePassword($userlogin, $UserPassword, "iFolder");
		}
	}
        
	if ($intranet_authentication_method=='LDAP')
	{
		$lldap = new libldap();
		if ($modifyPassword && $lldap->isPasswordChangeNeeded())
		{
			$lldap->connect();
			$lldap->changePassword($userlogin, $UserPassword);
		}
	}
	
    # SchoolNet
	if ($plugin['SchoolNet']) {
		include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
		$lschoolnet = new libschoolnet();
		
		$sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '$uid'";
		$data = $li->returnArray($sql,12);
		if($modifyPassword)
			$data[0][1] = $UserPassword;
		$lschoolnet->addStudentUser($data);
	}
	
	# send email
//		$le = new libsendmail();
//		$le->send_email ($email, reset_password_title(), reset_password_body($lu->Title, $lu->EnglishName) );
	if($modifyPassword)
	{
// 		$lwebmail->sendModuleMail((array)$uid,reset_password_title(),reset_password_body($lu->Title, $lu->EnglishName) );
		list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData("", "", "", stripslashes($engname), stripslashes($chiname), 1);
		//$lwebmail->sendModuleMail((array)$uid,$mailSubject,$mailBody);
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if(!$sys_custom['DisableEmailAfterChangedAccountPassword'] &&  $UserEmail != "" && intranet_validateEmail($UserEmail,true)){
			$lsendmail->SendMail($UserEmail, $mailSubject, $mailBody,"$headers");
		}
	}
	 // end password changed
	
		if($sys_custom['UserExtraInformation'] )
		{
			if(!isset($ExtraInfo2)) {
				$ExtraInfoCatArr = $laccount->Get_User_Extra_Info_Category("", " AND eic.RecordType=2");
				$noOfSingleSelectItem = count($ExtraInfoCatArr);
				if($noOfSingleSelectItem>0) {
					for($i=0; $i<$noOfSingleSelectItem; $i++) {
						if(${'ExtraInfo2_'.$ExtraInfoCatArr[$i]['CategoryID']}[0] != 0) {
							$ExtraInfo2[] .= ${'ExtraInfo2_'.$ExtraInfoCatArr[$i]['CategoryID']}[0];
						}
					}
				}
			}
			
			if(!isset($ExtraInfo))
				$ExtraInfo = array_merge((array)$ExtraInfo1,(array)$ExtraInfo2);
			
			$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
			$ExtraInfoOthersArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);
			if(count($ExtraInfoOthersArr)>0) {
				foreach($ExtraInfoOthersArr as $item_id=>$item_code) {
					if(${'ExtraOthers_'.$item_id}!='') 
						$ExtraOthers[$item_id] = ${'ExtraOthers_'.$item_id};
				}
			}
			
			if(count($ExtraInfo)>0)
				$laccount->Update_User_Extra_Info_Mapping($uid,$ExtraInfo, $ExtraOthers);
			
		}
	
	if($sys_custom['StudentMgmt']['BlissWisdom']) {
		include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
		$libps = new libuserpersonalsettings();
			
		if($skipStep2) {
			if($HowToKnowBlissWisdom==1) {
				$HowToKnowBlissWisdom .= "::".$FriendName;	
			} else if($HowToKnowBlissWisdom==6) {
				$HowToKnowBlissWisdom.= "::".$HowToKnowOthersField;	
			}
		}
		
		$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToKnowBlissWisdom);
		
		$libps->Save_Setting($uid, $FieldAry);
		
	}

	# GSuite
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
		//prepare password
		include_once($PATH_WRT_ROOT."includes/libpwm.php");
		$libpwm = new libpwm();
		
		$password_for_google_account_creation='';
		$rows_password = $libpwm->getData(array($uid));
		foreach((array)$rows_password as $key=>$row_password){
			$password_for_google_account_creation = $row_password;
		}
		
		//google api
		include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
		include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
		include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
		$libGoogleSSO = new libGoogleSSO();
				
		$gmail_input_array = array();
		$array_config_index = $libGoogleSSO->getAllGoogleAPIConfigIndex();
		foreach((array)$array_config_index as $config_index){
			$gmail_array_index = 'gmail_' . $config_index;
			$gmail_input_array[$gmail_array_index] = $$gmail_array_index;
		}
		
		$google_have_error=false;
		if ($ssoservice["Google"]['service_directory']['user']['is_readonly'] == false) {
		  $array_error_message = $libGoogleSSO->syncUserFromEClassToGoogle($uid, $userlogin, $status, $engname, $password_for_google_account_creation, $gmail_input_array, $chiname);
    		foreach((array)$array_error_message as $config_index => $error_message){
    			if($error_message==''){
    				if($google_account_exists_during_creation){
    					$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "ACCOUNT_CREATED";
    				}else{
    					$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "NO_MESSAGE";
    				}
    			}else{
    				$google_have_error = true;
    				$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = $error_message;
    			}
    		}    		
		}
		$user_groups = $libGoogleSSO->enabledUserForGoogle($laccount, array($uid));
		if (count($user_groups['activeUsers'])>0) {
		    $year_id = Get_Current_Academic_Year_ID();
		    $sql_query_one = "SELECT GroupID FROM INTRANET_GROUP WHERE (AcademicYearID = ".$year_id." OR AcademicYearID is NULL)";
		    $all_group_ids = $laccount->returnVector($sql_query_one);
		    if (count($all_group_ids)>0) {
		        $sql_query = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='".$uid."' AND GroupID IN (".implode(',', $all_group_ids).")";
		        $group_ids = $laccount->returnVector($sql_query);
		        if ($ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
		          $libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $group_ids);
		        }
		    }
		}
	}
	
	if($skipStep2!=1 || (isset($newids) && $newids=="")) {
	
		# modify Group
		# Hide ECA Groups if eEnrolment is updated to use Year-based Term-based Club
		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
		$libenroll = new libclubsenrol();
		$cond_ECA_GroupID = "";
		if ($plugin['eEnrollment'] && $libenroll->isUsingYearTermBased)
		{
			$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = '5'";
			$ECA_GroupIDArr = $li->returnVector($sql);
			
			$cond_ECA_GroupID = '';
			if (is_array($ECA_GroupIDArr) && count($ECA_GroupIDArr) > 0)
			{
				$ECA_GroupIDList = implode(',', $ECA_GroupIDArr);
				$cond_ECA_GroupID = " AND GroupID NOT IN (".$ECA_GroupIDList.") ";
			}
		}
	
		$gpID = 2;
		$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID!=$gpID AND GroupID NOT IN (1,2,3,4)";
		$sql .= $cond_ECA_GroupID;
		$laccount->db_db_query($sql);
		
		if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
			$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID!=$gpID AND GroupID NOT IN (1,2,3,4)";
			$rows_group_original = $laccount->returnArray($sql);
			
			$array_original_group_id = array();
			foreach((array)$rows_group_original as $row_group_original){
				array_push($array_original_group_id,$row_group_original['GroupID']);
			}
		}
	
		# add group member
		for($i=0; $i<sizeof($GroupID); $i++){
			if($GroupID[$i] != "") {
				$lgroup = new libgroup($GroupID[$i]);
				$defaultRoleID = $lgroup->returnGroupDefaultRole($GroupID[$i]);
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('".$GroupID[$i]."', '$uid', '$defaultRoleID', now(), now())";
				$laccount->db_db_query($sql);
			}
		}
		
		
		# Assign to Role
		# remove existing role
		$sql = "DELETE FROM ROLE_MEMBER WHERE UserID=$uid";
		$laccount->db_db_query($sql);
	
		# add role
		$identityType = "Student";
		for($i=0; $i<sizeof($RoleID); $i++){
			if($RoleID[$i] != "" && $RoleID[$i]!=0) {
				$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
				$laccount->db_db_query($sql);
			}
		}
			
		# Assign to Subject Group
		//if(sizeof($sbjGpID)>0) {
			$sql = "SELECT st.SubjectGroupID FROM SUBJECT_TERM st LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) WHERE st.YearTermID=".getCurrentSemesterID()." AND stcu.UserID='$uid'";
			$oldSbjGpID = $laccount->returnVector($sql);
			if(sizeof($oldSbjGpID)>0) {
				$sql = "DELETE FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID IN (".implode(',',$oldSbjGpID).") AND UserID='$uid'";
				$laccount->db_db_query($sql);
			}
			if(sizeof($sbjGpID)>0) {
				foreach($sbjGpID as $sbjID) {
					$sql = "INSERT INTO SUBJECT_TERM_CLASS_USER (SubjectGroupID, UserID, DateInput, InputBy, DateModified, ModifiedBy) VALUES ('$sbjID','$uid',NOW(),'$UserID',NOW(),'$UserID')";
					$laccount->db_db_query($sql);
				}
			}
			
		//}
	
		if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student'] && $ssoservice["Google"]['mode']['user_and_group']){
			include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
			include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
			include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
			$libGoogleSSO = new libGoogleSSO();
			
			$libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $GroupID, $array_original_group_id);
		}

		
		# remove guardians
		$sql = "DELETE FROM $eclass_db.GUARDIAN_STUDENT WHERE UserID='$uid'";
		$laccount->db_db_query($sql);
		$sql = "DELETE FROM $eclass_db.GUARDIAN_STUDENT_EXT_1 WHERE UserID='$uid'";
		$laccount->db_db_query($sql);
		
		if(isset($newids) && $newids!="") {
			# Create guardians
			$ids = explode(",",$newids);
			for($i=0;$i<sizeof($ids);$i++){
				$id = $ids[$i];
				$ch_name = ${'chname_new_'.$id};
				$en_name = ${'enname_new_'.$id};
				$phone   = ${'phone_new_'.$id};
				$emphone = ${'emphone_new_'.$id};
				
				$add_area = '';
				$add_road = '';
				$add_address = '';
				$address = ${'address_new_'.$id};
				
				$relation= ${'relation_new_'.$id};
				
				$new_str ="new_$id";
				
				$IsMain = ($mainGuardianFlag==$id) ? 1 : 0;
				$IsSMS = ($smsFlag==$id) ? 1 : 0;
				$IsEmergencyContact = ($emergencyContactFlag==$id) ? 1 : 0;
				
				
				
				$occupation = ${'occupation_new_'.$id};
				$companyName = ${'companyName_new_'.$id};
				
				$IsLiveTogether = (int)${'liveTogether_new_'.$id};
	
				
				
				$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT (
						UserID,ChName,EnName,
						Phone,EmPhone,Address,Relation,
						IsMain,IsSMS,InputDate,ModifiedDate
					) VALUES (
						'$uid','$ch_name','$en_name',
						'$phone','$emphone','$address','$relation',
						'$IsMain', '$IsSMS',NOW(), NOW()
					) ";
				$li->db_db_query($sql);
				$record_id = mysql_insert_id();
				
				//insert the extend record
				$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT_EXT_1 (
						UserID,RecordID, IsLiveTogether, IsEmergencyContact, Occupation, CompanyName,
						AreaCode, Road, Address,
						InputDate,ModifiedDate
					) VALUES (
						'$uid','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation', '$companyName',
						'$add_area','$add_road', '$add_address', 
						NOW(), NOW()
					)";
				$li->db_db_query($sql);
				
				# 2020-03-16 (Philips) - Sync to Student Registry
				if($sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry'] && $IsMain){
					$guardianAry = array();
					$guardianAry['StudentID'] = $uid;
					$guardianAry['PG_TYPE'] = 'G';
					$guardianAry['GUARD'] = 'O';
					$guardianAry['G_GENDER'] = 'M';
					$guardianAry['NAME_C'] = $ch_name;
					$guardianAry['NAME_E'] = $en_name;
					$guardianAry['G_RELATION'] = $ec_guardian[$relation];
					$guardianAry['TEL'] = $phone;
					$guardianAry['MOBILE'] = $emphone;
					$guardianAry['PROF'] = $occupation;
					$guardianAry['LIVE_SAME'] = $IsLiveTogether;
					$nl = strpos($address, "\n");
					if(is_int($nl)){
						$road = substr($address,0, $nl);
						$gAddress = substr($address, $nl);
					} else {
						$road = $address;
						$gAddress = '';
					}
					$guardianAry['G_ROAD'] = $road;
					$guardianAry['G_ADDRESS'] = $gAddress;
					$laccount->syncToStudentRegistry_GuardianInfo($uid, $guardianAry);
				}
			}
		
		}
	}
	
	#### Special handling
	//$lireg = new libregistry();
	$laccount->UpdateRole_UserGroup();
	
	# icalendar
		$sql = "select GroupID from INTRANET_USERGROUP where UserID = '$uid' ";
		$all_groups = $lu->returnVector($sql);
		
		$sql = "delete from CALENDAR_CALENDAR_VIEWER where 
			GroupID not in ('".implode("','",$all_groups)."') and GroupType = 'E' and UserID = '$uid'";
		$lu->db_db_query($sql);
		
		$sql = "select GroupID from CALENDAR_CALENDAR_VIEWER where GroupType = 'E' and UserID = '$uid'";
		$existing = $lu->returnVector($sql);
		
		$existing = empty($existing)? array():$existing;	
	
		//$newGroup = array_diff($GroupID,$existing);
		//if (!empty($existing)){
		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = '$uid'
				where g.GroupID not in ('".implode("','",$existing)."')";
		$lu->db_db_query($sql);
		
		# remove shared group calendars 
		$cal_remove_groups = array_values(array_diff($existing_groups,$all_groups));
		for($i=0;$i<sizeof($cal_remove_groups);$i++) {
			$lcalendar->removeCalendarViewerFromGroup($cal_remove_groups[$i],$uid);
		}
		# insert calendar viewer to calendars that have shared to the user's groups
		for($i=0;$i<sizeof($all_groups);$i++) {
			$lcalendar->addCalendarViewerToGroup($all_groups[$i],$uid);
		}
}

# no longer use this function
//$le->addTitleToCourseUser($lu->UserEmail,$dataAry['TitleEnglish'],$dataAry['TitleChinese'],$dataAry['EnglishName'],$dataAry['ChineseName'],$dataAry['DateOfBirth'],$dataAry['HomeTelNo'],$dataAry['FaxNo'],$dataAry['Address']);

$dataAry['SubjectGroupID'] = $sbjGpID;
$dataAry['addToEclass'] = $addToEclass;
$le->eClass40UserUpdateInfo($ori_UserEmail, $dataAry, $new_UserEmail);
// $le->eClass40UserUpdateInfo($ori_UserEmail,$dataAry);

$successAry['synUserDataToModules'] = $lu->synUserDataToModules($uid);

$laccount->suspendEmailFunctions($uid, $status == 1);

if($plugin['radius_server']){
	$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
}

if(sizeof($sbjGpID)>0) {
	foreach($sbjGpID as $subjectGroupID) {
		$sql = "SELECT course_id FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID='$subjectGroupID'";
		$result = $laccount->returnVector($sql);
		$course_id = $result[0];

		if($course_id!="" && $course_id>0) {
			$eclassClassNumber = $userInfo['ClassName'].' - '.$userInfo['ClassNumber'];
	
			$lo = new libeclass($course_id); 
			
			$lo->eClassUserAddFullInfo($UserEmail, $userInfo['Title'], $userInfo['FirstName'],$userInfo['LastName'], $dataAry['EnglishName'], $dataAry['ChineseName'], $NickName, 'S', $UserPassword, $eclassClassNumber,$Gender, $userInfo['ICQNo'],$HomeTelNo,$FaxNo,$DateOfBirth,$address,$userInfo['Country'],$userInfo['URL'],$userInfo['Info'], "", $uid,$dataAry['TitleEnglish'],$dataAry['TitleChinese']);
		}
	}
}

intranet_closedb();

$flag = ($success) ? "UpdateSuccess" : "UpdateUnsuccess";



if(substr($comeFrom,0,2)=="./")
	$comeFrom = ".".$comeFrom;
$comeFrom = $comeFrom ? $comeFrom : "index.php";
if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
	if($google_have_error){
		header("Location: edit.php?uid=$uid");
		exit();
	}
}
header("Location: $comeFrom?xmsg=$flag");

?>