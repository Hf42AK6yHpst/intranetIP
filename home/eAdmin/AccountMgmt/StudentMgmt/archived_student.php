<?php
// Editing by 
/*
 * 2018-12-17 (Isaac ): added HomeTelNo, FaxNo and MobileTelNo to the keyword search        
 * 2018-04-09 (Danny ): $sys_custom['StudentMgnt']['ExportForAlumni'] Added for Export Student Details for Alumni
 * 2017-09-11 (Carlos): $sys_custom['AccountMgmt']['DeleteArchivedStudent'] Added [Delete] button.
 * 2016-11-24 (Carlos): Added import and export function.
 * 2016-06-28 (Carlos): Added. 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if(isset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_RESULT'])){
	$xmsg = $_SESSION['FLASH_VAR_ARCHIVED_STUDENT_RESULT'];
	unset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_RESULT']);
}

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_page_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"",1);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

$keyword = trim(stripslashes(rawurldecode($keyword)));

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$archiver_name_field = getNameFieldByLang2("u2.");
$sql = "SELECT
			CONCAT('<span style=\"display:none\">',u.EnglishName,'</span><a href=\"javascript:goEdit(',u.UserID,');\">',u.EnglishName,'</a>') as EnglishName,
			IFNULL(IF(u.ChineseName='', '---', u.ChineseName),'---') as ChineseName,
			u.UserLogin,
			u.ClassName,
			u.ClassNumber,
			u.HKID,
			u.WebSAMSRegNo,
			u.STRN,
			u.HKJApplNo,
			IF(u.DateOfBirth='0000-00-00 00:00:00' OR u.DateOfBirth IS NULL,'---',DATE_FORMAT(u.DateOfBirth,'%Y-%m-%d')) as DateOfBirth,
			u.YearOfLeft,
			u.DateArchive,
			$archiver_name_field as ArchiveByUser,
			CONCAT('<input type=\"checkbox\" name=\"user_id[]\" id=\"user_id[',u.UserID,']\" value=\"', u.UserID ,'\">') as checkbox
		FROM 
			INTRANET_ARCHIVE_USER as u 
			LEFT JOIN INTRANET_USER as u2 ON u2.UserID=u.ArchivedBy 
		WHERE 
			u.RecordType = ".TYPE_STUDENT." "; 
if($keyword != '')
{
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$sql .= " AND (u.EnglishName LIKE '%$safe_keyword%' OR u.ChineseName LIKE '%$safe_keyword%' OR u.UserLogin LIKE '%$safe_keyword%'
				OR u.ClassName LIKE '%$safe_keyword%' OR
				u.HomeTelNo like '%$safe_keyword%' OR
				u.FaxNo like '%$safe_keyword%' OR
				u.MobileTelNo like '%$safe_keyword%') ";
}
$li->sql = $sql;
$li->field_array = array("EnglishName","ChineseName","u.UserLogin","u.ClassName","u.ClassNumber","u.HKID","u.WebSAMSRegNo","u.STRN","u.HKJApplNo","u.DateOfBirth","u.YearOfLeft","u.DateArchive","ArchiveByUser");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "UserMgmtStudentNoClass";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
$li->column_list .= "<th width='7%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $i_ClassName)."</th>\n"; 
$li->column_list .= "<th width='5%' >".$li->column($pos++, $i_ClassNumber)."</th>\n"; 
$li->column_list .= "<th width='5%' >".$li->column($pos++, $i_HKID)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_WebSAMS_Registration_No)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_STRN)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['AccountMgmt']['HKJApplNo'])."</th>\n";
$li->column_list .= "<th width='7%' >".$li->column($pos++, $Lang['StudentRegistry']['BirthDate'])."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['AccountMgmt']['YearOfLeft'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['AccountMgmt']['DateOfArchive'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['AccountMgmt']['ArchivedBy'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";

$toolbar = $linterface->GET_LNK_IMPORT("import_archived_student.php",$Lang['Btn']['Import'],"","","",0);
$toolbar.= $linterface->GET_LNK_EXPORT("export_archived_student.php",$Lang['Btn']['Export'],"","","",0);

if($sys_custom['StudentMgnt']['ExportForAlumni']){
//	$toolbar.= $linterface->GET_LNK_EXPORT("export_archived_student_for_alumni.php", $Lang['Btn']['Export'] . " For Alumni","","","",0);
	$toolbar.= $linterface->GET_LNK_EXPORT("export_for_alumni.php", $Lang['StudentMgmt']['ExportForAlumni'] ,"","","",0);
}

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script type="text/javascript" language="javascript">
function goEdit(uid) 
{
	document.form1.action = "edit_archived_student.php";
	document.getElementById('user_id['+uid+']').checked = true;
	document.form1.submit();
}
</script>
<form name="form1" method="post" action="">

<div class="content_top_tool">
	<div class="Conntent_tool">
	<?=$toolbar?>
	</div>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom"></td>
	<td valign="bottom">
	<div class="common_table_tool">
        <a href="javascript:checkEdit(document.form1,'user_id[]','edit_archived_student.php')" class="tool_edit"><?=$Lang['Btn']['Edit']?></a>
    <?php if($sys_custom['AccountMgmt']['DeleteArchivedStudent']){ ?>    
        <a href="javascript:checkRemove(document.form1,'user_id[]','delete_archived_student.php')" class="tool_delete"><?=$Lang['Btn']['Delete']?></a>
    <?php } ?>
	</div>
	</td>
</tr>
</table>
</div>
<?= $li->display()?>
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
</body>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>