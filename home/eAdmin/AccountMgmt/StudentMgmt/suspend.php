<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($userID)==0) {
	header("Location : index.php");	
	exit;
}

$laccount = new libaccountmgmt();

$sql = "UPDATE INTRANET_USER SET RecordStatus = '0', DateModified = now() WHERE UserID IN (".implode(",", $userID).")";
$result = $laccount->db_db_query($sql);

intranet_closedb();

if($result)
	$xmsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
else 
	$xmsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	
header("Location: index.php?targetClass=$targetClass&recordstatus=$recordstatus&keyword=$keyword&returnMsg=$xmsg");
?>