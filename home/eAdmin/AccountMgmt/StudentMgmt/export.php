<?php

# modifying :


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if(sizeof($_POST)==0) {
	header("Location : index.php");	
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lexport = new libexporttext();

# record status
if(isset($recordstatus) && $recordstatus!="")
	$conds .= " AND RecordStatus=$recordstatus";

if($targetClass!="") {
	if($targetClass=='0') {					# all classes
		# do nothing	
	} else if(is_numeric($targetClass)) {	# All students in specific form
		$conds .= " AND y.YearID=$targetClass";
	} else {								# All students in specific class
		$conds .= " AND yc.YearClassID=".substr($targetClass, 2);
	}
}
		
		
$sql = "SELECT
			USR.ClassName,
			USR.ClassNumber,
			IFNULL(IF(USR.EnglishName='','---',USR.EnglishName),'---') as EnglishName,
			IFNULL(IF(USR.ChineseName='','---',USR.ChineseName),'---') as ChineseName,
			USR.UserEmail,
			USR.NickName,
			USR.UserID,
			USR.UserLogin,
			IFNULL(USR.LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '$i_status_suspended'
				WHEN 1 THEN '$i_status_approved'
				ELSE '---' END as recordStatus
		FROM 
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR=UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
			
		WHERE
			USR.RecordType = ".TYPE_STUDENT."
			$conds 
		ORDER BY
			USR.EnglishName
			";

$result = $laccount->returnArray($sql,8);

for($i=0; $i<sizeof($result); $i++)
{
	$ExportArr[$i][0] = $result[$i]['EnglishName'];		//DateInput
	$ExportArr[$i][1] = $result[$i]['ChineseName'];		//name
	$ExportArr[$i][2] = $result[$i]['title'];			//Location
	$ExportArr[$i][3] = $result[$i]['teaching'];		//Location Detail
	//$ExportArr[$i][4] = $result[$i]['UserID'];			//title
	if($result[$i]['UserID'] != "") {
		$temp = $laccount->getTeachingClassList($result[$i]['UserID']);
		$ExportArr[$i][4] = ($temp!="") ? $temp : "---";	
	}
	$ExportArr[$i][5] = $result[$i]['UserLogin'];		//Content
	$ExportArr[$i][6] = $result[$i]['LastUsed'];		//category name
	$ExportArr[$i][7] = $result[$i]['recordStatus'];	//Remark
}

intranet_closedb();

$exportColumn = array($i_UserEnglishName,$i_UserChineseName,$i_UserTitle,$i_eClass_Identity,$i_general_class,$i_UserLogin,$i_frontpage_eclass_lastlogin,$i_UserRecordStatus);
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

$filename = 'staff_account.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>