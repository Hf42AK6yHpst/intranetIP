<?php

###  Using by

/***
 * Change Log:
 * 
 * 2018-04-09 (Danny ): Added this page for export information for alumni
 * 
 */

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
# Required for this page
ini_set('zend.ze1_compatibility_mode', '0');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

/** PHPExcel_IOFactory */
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");

intranet_auth();
intranet_opendb();

# Filter By YearOfLeft
$yearOfLeft_cond = '';
if (isset($_POST['Fields']) && is_array($_POST['Fields'])) {

	# Handle Null YearOfLeft Value
	$null_yearOfLeft_cond = "";
	if (in_array("null_year", $_POST['Fields'])) {
		for($i = 0; $i < count($_POST['Fields']); $i++) {
			if ($_POST['Fields'][$i] == "null_year") {
				$null_yearOfLeft_cond = " OR YEAROFLEFT is null OR TRIM(YEAROFLEFT) = '' ";
				unset($_POST['Fields'][$i]);
				break;
			}
		}
	}

	# Handle rest of YearOfLeft Years	
	$yearOfLeft_cond = " AND (YEAROFLEFT IN ('" . implode("','", $_POST['Fields']) . "') $null_yearOfLeft_cond  )";
}

ob_end_clean();

$li = new libdb();

# A : Alumni No.
$xlsHeaderArr = array("Alumni No.*");
$fields = 'UserLogin';

# B : Email
$xlsHeaderArr[] = "Email*";
$fields .= ",UserEmail";

# C : Role
$xlsHeaderArr[] = "Role";
$fields .= ",''";

# D : Date of Birth
$xlsHeaderArr[] = "Date Of Birth";
$fields .= ",IF(DateOfBirth='0000-00-00 00:00:00' OR DateOfBirth IS NULL,'',DATE_FORMAT(DateOfBirth,'%Y-%m-%d')) as DOB";

# E : First Name (English)
$xlsHeaderArr[] = "First Name (English)*";
$fields .= ",EnglishName";

# F : Last Name (English)
$xlsHeaderArr[] = "Last Name (English)*";
$fields .= ",EnglishName";

# G : First Name (Chinese)
$xlsHeaderArr[] = "First Name (Chinese)*";
$fields .= ",ChineseName";

# H : Last Name (Chinese)
$xlsHeaderArr[] = "Last Name (Chinese)*";
$fields .= ",ChineseName";

# I : Graduation Year
$xlsHeaderArr[] = "Graduation Year*";
$fields .= ",YearOfLeft";

# J : Gender
$xlsHeaderArr[] = "Gender*";
$fields .= ",Gender";

# K : Mobile
$xlsHeaderArr[] = "Mobile*";
$fields .= ",MobileTelNo";

# L : Address
$xlsHeaderArr[] = "Address";
$fields .= ",Address";

# M : Expired At
$xlsHeaderArr[] = "Expired At";
$fields .= ",''";

# N : Membership Recorded Years
$xlsHeaderArr[] = "Membership Recorded Years";
$fields .= ",''";

# O : Membership Recorded Months
$xlsHeaderArr[] = "Membership Recorded Months";
$fields .= ",''";

# P : Membership Recorded Charges
$xlsHeaderArr[] = "Membership Recorded Charges";
$fields .= ",''";

$sql = "SELECT 
			$fields 
		FROM 
			INTRANET_ARCHIVE_USER 
		WHERE 
			RecordType='".TYPE_STUDENT."'
			$yearOfLeft_cond
		ORDER BY 
			YEAROFLEFT DESC, UserLogin";

$rows = $li->returnArray($sql);

$objPHPExcel = new PHPExcel();
// Set Excel properties
$objPHPExcel->getProperties()
			->setCreator("eClass")
			->setLastModifiedBy("eClass")
			->setTitle("eClass Archived Student records")
			->setSubject("eClass Archived Student records")
			->setDescription("to eAlumni")
			->setKeywords("eClass Archived Student records")
			->setCategory("eClass");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$ActiveSheet = $objPHPExcel->getActiveSheet();

$ActiveSheet->setTitle("Columns Header");


// Build xls header
for ($j=0; $j<count($xlsHeaderArr); $j++) {
 	$ActiveSheet->setCellValue(chr(65+$j).'1', $xlsHeaderArr[$j]);
}

// Set whole xls format From Cell A2 to end of sheet
$range = 'A2:'.chr(65+count($xlsHeaderArr)-1).(count($rows)+1);

$objPHPExcel->getActiveSheet()
			->getStyle($range)
			->getNumberFormat()
			->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			
// Build xls content
for ($i=0; $i<count($rows); $i++) {
	for ($j=0; $j<count($rows[$i]); $j++) {
		$ActiveSheet->setCellValueExplicit(chr(65+$j).($i+2), $rows[$i][$j], PHPExcel_Cell_DataType::TYPE_STRING);
	}
}


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="eclass_archived_student_for_ealumni.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1201 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


intranet_closedb();
?>