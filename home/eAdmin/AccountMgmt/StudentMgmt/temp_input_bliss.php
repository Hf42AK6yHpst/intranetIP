<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");


intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

# Category

$sql = "INSERT INTO INTRANET_USER_EXTRA_INFO_CATEGORY (CategoryCode, CategoryNameCh, CategoryNameEn, DisplayOrder, RecordType, RecordStatus) VALUES ";
$sql .= "('WorkingIndustry', '職業界別', '職業界別', '1', '2', 1), ";			
$sql .= "('Role', '工作角色', '工作角色', '2', '2', 1), ";						
$sql .= "('Skill', '專長', '專長', '3', '1', 1), ";								
$sql .= "('Lang_Listening', '語言(聽)', '語言(聽)', '4', '1', 1), ";				
$sql .= "('Lang_Spoken', '語言(講)', '語言(講)', '5', '1', 1), ";				
$sql .= "('ContactPeriod', '方便聯絡時間', '方便聯絡時間', '6', '1', 1), ";		
$sql .= "('District', '居住地區', '居住地區', '7', '2', 1),";						
$sql .= "('PassportType', '護照種類', '護照種類', '8', '2', 1)";					

$laccount->db_db_query($sql);
//echo $sql.'<br>';


# Item

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='Lang_Listening'");
$thisCategoryID = $Result[0][0];

$sql = "INSERT INTO INTRANET_USER_EXTRA_INFO_ITEM (CategoryID, ItemCode, ItemNameCh, ItemNameEn, DisplayOrder, RecordStatus) VALUES ";

$sql .= "('$thisCategoryID', 'L1_1', '粵語', '粵語', '1', '1'), ";
$sql .= "('$thisCategoryID', 'L1_2', '普通話', '普通話', '2', '1'), ";
$sql .= "('$thisCategoryID', 'L1_3', '英語', '英語', '3', '1'), ";
$sql .= "('$thisCategoryID', 'L1_O', '其他', '其他', '4', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='Lang_Spoken'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'L2_1', '粵語', '粵語', '1', '1'), ";
$sql .= "('$thisCategoryID', 'L2_2', '普通話', '普通話', '2', '1'), ";
$sql .= "('$thisCategoryID', 'L2_3', '英語', '英語', '3', '1'), ";
$sql .= "('$thisCategoryID', 'L2_O', '其他', '其他', '4', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='ContactPeriod'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'Time1', '上午9時﹣中午12時', '上午9時﹣中午12時', '1', '1'), ";
$sql .= "('$thisCategoryID', 'Time2', '中午12時﹣下午2時', '中午12時﹣下午2時', '2', '1'), ";
$sql .= "('$thisCategoryID', 'Time3', '下午2時﹣下午6時', '下午2時﹣下午6時', '3', '1'), ";
$sql .= "('$thisCategoryID', 'Time4', '下午6時﹣晚上9時', '下午6時﹣晚上9時', '4', '1'), ";
$sql .= "('$thisCategoryID', 'Time5', '晚上9時﹣晚上11時', '晚上9時﹣晚上11時', '5', '1'), ";
$sql .= "('$thisCategoryID', 'Time6', '任何時段', '任何時段', '6', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='WorkingIndustry'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'DM1', '教育（校長/教授/講師/教師）', '教育（校長/教授/講師/教師）', '1', '1'), ";
$sql .= "('$thisCategoryID', 'DM2', '醫療護理（中西醫/護士/針灸推拿跌打/物理治療）', '醫療護理（中西醫/護士/針灸推拿跌打/物理治療）', '2', '1'), ";
$sql .= "('$thisCategoryID', 'DM3', '建築及工程（建築師/工程師/測量師）', '建築及工程（建築師/工程師/測量師）', '3', '1'), ";
$sql .= "('$thisCategoryID', 'DM4', '公共行政（政府/公共機構）', '公共行政（政府/公共機構）', '4', '1'), ";
$sql .= "('$thisCategoryID', 'DM5', '法律及議會（法官/律師/議員）', '法律及議會（法官/律師/議員）', '5', '1'), ";
$sql .= "('$thisCategoryID', 'DM6', '社會福利（社工、非牟利組織）', '社會福利（社工、非牟利組織）', '6', '1'), ";
$sql .= "('$thisCategoryID', 'DM7', '大眾傳播（報章雜誌/電台電視）', '大眾傳播（報章雜誌/電台電視）', '7', '1'), ";
$sql .= "('$thisCategoryID', 'DM8', '商業（進出口）', '商業（進出口）', '8', '1'), ";
$sql .= "('$thisCategoryID', 'DM9', '商業（批發零售）', '商業（批發零售）', '9', '1'), ";
$sql .= "('$thisCategoryID', 'DM10', '製造業（食品製造）', '製造業（食品製造）', '10', '1'), ";
$sql .= "('$thisCategoryID', 'DM11', '製造業（紡織成衣）', '製造業（紡織成衣）', '11', '1'), ";
$sql .= "('$thisCategoryID', 'DM12', '製造業（其他）', '製造業（其他）', '12', '1'), ";
$sql .= "('$thisCategoryID', 'DM13', '餐飲酒店', '餐飲酒店', '13', '1'), ";
$sql .= "('$thisCategoryID', 'DM14', '建造業（各類工程）', '建造業（各類工程）', '14', '1'), ";
$sql .= "('$thisCategoryID', 'DM15', '保安、物業管理', '保安、物業管理', '15', '1'), ";
$sql .= "('$thisCategoryID', 'DM16', '金融保險', '金融保險', '16', '1'), ";
$sql .= "('$thisCategoryID', 'DM17', '地產', '地產', '17', '1'), ";
$sql .= "('$thisCategoryID', 'DM18', '電訊及資訊科技', '電訊及資訊科技', '18', '1'), ";
$sql .= "('$thisCategoryID', 'DM19', '運輸物流', '運輸物流', '19', '1'), ";
$sql .= "('$thisCategoryID', 'DM20', '推廣銷售', '推廣銷售', '20', '1'), ";
$sql .= "('$thisCategoryID', 'DM21', '傳媒公關', '傳媒公關', '21', '1'), ";
$sql .= "('$thisCategoryID', 'DM22', '藝術文娛', '藝術文娛', '22', '1'), ";
$sql .= "('$thisCategoryID', 'DM23', '廣告設計印刷', '廣告設計印刷', '23', '1'), ";
$sql .= "('$thisCategoryID', 'DM24', '個人護理（美容理髮、家務助理）', '個人護理（美容理髮、家務助理）', '24', '1'), ";
$sql .= "('$thisCategoryID', 'DM25', '漁農礦業', '漁農礦業', '25', '1'), ";
$sql .= "('$thisCategoryID', 'DM26', '家庭主婦', '家庭主婦', '26', '1'), ";
$sql .= "('$thisCategoryID', 'DM27', '學生', '學生', '27', '1'), ";
$sql .= "('$thisCategoryID', 'DM28', '退休人士', '退休人士', '28', '1'), ";
$sql .= "('$thisCategoryID', 'DM_O', '其他', '其他', '29', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='Skill'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'Sk1', '英語', '英語', '1', '1'), ";
$sql .= "('$thisCategoryID', 'Sk2', '普通話', '普通話', '2', '1'), ";
$sql .= "('$thisCategoryID', 'Sk3', '廣普翻譯', '廣普翻譯', '3', '1'), ";
$sql .= "('$thisCategoryID', 'Sk4', '中英翻譯', '中英翻譯', '4', '1'), ";
$sql .= "('$thisCategoryID', 'Sk5', '寫作、採訪、編輯', '寫作、採訪、編輯', '5', '1'), ";
$sql .= "('$thisCategoryID', 'Sk6', '電腦硬件維修', '電腦硬件維修', '6', '1'), ";
$sql .= "('$thisCategoryID', 'Sk7', '電腦軟件應用', '電腦軟件應用', '7', '1'), ";
$sql .= "('$thisCategoryID', 'Sk8', '電腦軟件設計', '電腦軟件設計', '8', '1'), ";
$sql .= "('$thisCategoryID', 'Sk9', '攝影', '攝影', '9', '1'), ";
$sql .= "('$thisCategoryID', 'Sk10', '影片拍攝', '影片拍攝', '10', '1'), ";
$sql .= "('$thisCategoryID', 'Sk11', '平面設計', '平面設計', '11', '1'), ";
$sql .= "('$thisCategoryID', 'Sk12', '影音剪接', '影音剪接', '12', '1'), ";
$sql .= "('$thisCategoryID', 'Sk13', '網頁製作', '網頁製作', '13', '1'), ";
$sql .= "('$thisCategoryID', 'Sk14', '舞台管理', '舞台管理', '14', '1'), ";
$sql .= "('$thisCategoryID', 'Sk15', '舞台燈光', '舞台燈光', '15', '1'), ";
$sql .= "('$thisCategoryID', 'Sk16', '場地音響', '場地音響', '16', '1'), ";
$sql .= "('$thisCategoryID', 'Sk17', '裝修工程', '裝修工程', '17', '1'), ";
$sql .= "('$thisCategoryID', 'Sk18', '水電工程', '水電工程', '18', '1'), ";
$sql .= "('$thisCategoryID', 'Sk19', '駕駛（私家車）', '駕駛（私家車）', '19', '1'), ";
$sql .= "('$thisCategoryID', 'Sk20', '駕駛（巴士）', '駕駛（巴士）', '20', '1'), ";
$sql .= "('$thisCategoryID', 'Sk21', '駕駛（貨車）', '駕駛（貨車）', '21', '1'), ";
$sql .= "('$thisCategoryID', 'Sk22', '中西醫學', '中西醫學', '22', '1'), ";
$sql .= "('$thisCategoryID', 'Sk23', '營養養生（營養學、自然療法）', '營養養生（營養學、自然療法）', '23', '1'), ";
$sql .= "('$thisCategoryID', 'Sk24', '運動保健（瑜珈、太極、推拿跌打）', '運動保健（瑜珈、太極、推拿跌打）', '24', '1'), ";
$sql .= "('$thisCategoryID', 'Sk25', '急救證書', '急救證書', '25', '1'), ";
$sql .= "('$thisCategoryID', 'Sk26', '護理', '護理', '26', '1'), ";
$sql .= "('$thisCategoryID', 'Sk27', '育兒托管', '育兒托管', '27', '1'), ";
$sql .= "('$thisCategoryID', 'Sk28', '表演藝術（戲劇、舞蹈）', '表演藝術（戲劇、舞蹈）', '28', '1'), ";
$sql .= "('$thisCategoryID', 'Sk29', '中國戲曲', '中國戲曲', '29', '1'), ";
$sql .= "('$thisCategoryID', 'Sk30', '樂器演奏', '樂器演奏', '30', '1'), ";
$sql .= "('$thisCategoryID', 'Sk31', '歌曲演唱', '歌曲演唱', '31', '1'), ";
$sql .= "('$thisCategoryID', 'Sk32', '繪畫', '繪畫', '32', '1'), ";
$sql .= "('$thisCategoryID', 'Sk33', '書法', '書法', '33', '1'), ";
$sql .= "('$thisCategoryID', 'Sk34', '手工藝', '手工藝', '34', '1'), ";
$sql .= "('$thisCategoryID', 'Sk35', '壁報美工', '壁報美工', '35', '1'), ";
$sql .= "('$thisCategoryID', 'Sk36', '廚藝', '廚藝', '36', '1'), ";
$sql .= "('$thisCategoryID', 'Sk37', '中文打字', '中文打字', '37', '1'), ";
$sql .= "('$thisCategoryID', 'Sk38', '財務簿記', '財務簿記', '38', '1'), ";
$sql .= "('$thisCategoryID', 'Sk39', '倉務管理', '倉務管理', '39', '1'), ";
$sql .= "('$thisCategoryID', 'Sk_O', '其他', '其他', '40', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='PassportType'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'PpType1', '香港特區護照', '香港特區護照', '1', '1'), ";
$sql .= "('$thisCategoryID', 'PpType2', 'BNO', 'BNO', '2', '1'), ";
$sql .= "('$thisCategoryID', 'PpType3', '中國護照', '中國護照', '3', '1'), ";
$sql .= "('$thisCategoryID', 'PpType4', '台灣護照', '台灣護照', '4', '1'), ";
$sql .= "('$thisCategoryID', 'PpType_O', '其他', '其他', '5', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='District'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'District1', '中西區', '中西區', '1', '1'), ";
$sql .= "('$thisCategoryID', 'District2', '灣仔區', '灣仔區', '2', '1'), ";
$sql .= "('$thisCategoryID', 'District3', '東區', '東區', '3', '1'), ";
$sql .= "('$thisCategoryID', 'District4', '南區', '南區', '4', '1'), ";
$sql .= "('$thisCategoryID', 'District5', '油尖旺區', '油尖旺區', '5', '1'), ";
$sql .= "('$thisCategoryID', 'District6', '深水埗區', '深水埗區', '6', '1'), ";
$sql .= "('$thisCategoryID', 'District7', '九龍城區', '九龍城區', '7', '1'), ";
$sql .= "('$thisCategoryID', 'District8', '黃大仙區', '黃大仙區', '8', '1'), ";
$sql .= "('$thisCategoryID', 'District9', '觀塘區', '觀塘區', '9', '1'), ";
$sql .= "('$thisCategoryID', 'District10', '荃灣區', '荃灣區', '10', '1'), ";
$sql .= "('$thisCategoryID', 'District11', '屯門區', '屯門區', '11', '1'), ";
$sql .= "('$thisCategoryID', 'District12', '元朗區', '元朗區', '12', '1'), ";
$sql .= "('$thisCategoryID', 'District13', '北區', '北區', '13', '1'), ";
$sql .= "('$thisCategoryID', 'District14', '大埔區', '大埔區', '14', '1'), ";
$sql .= "('$thisCategoryID', 'District15', '西貢區', '西貢區', '15', '1'), ";
$sql .= "('$thisCategoryID', 'District16', '沙田區', '沙田區', '16', '1'), ";
$sql .= "('$thisCategoryID', 'District17', '葵青區', '葵青區', '17', '1'), ";
$sql .= "('$thisCategoryID', 'District18', '離島區', '離島區', '18', '1'), ";
$sql .= "('$thisCategoryID', 'District19', '內地', '內地', '19', '1'), ";
$sql .= "('$thisCategoryID', 'District20', '澳門', '澳門', '20', '1'), ";
$sql .= "('$thisCategoryID', 'District_O', '其他', '其他', '21', '1')";

$sql .= ", ";

$Result = $laccount->Get_User_Extra_Info_Category('', " AND CategoryCode='Role'");
$thisCategoryID = $Result[0][0];

$sql .= "('$thisCategoryID', 'Role1', '自僱', '自僱', '1', '1'), ";
$sql .= "('$thisCategoryID', 'Role2', '受僱（行政管理）', '受僱（行政管理）', '2', '1'), ";
$sql .= "('$thisCategoryID', 'Role3', '受僱（一般職員）', '受僱（一般職員）', '3', '1'), ";
$sql .= "('$thisCategoryID', 'Role4', '學生', '學生', '4', '1'), ";
$sql .= "('$thisCategoryID', 'Role5', '退休／家庭工作', '退休／家庭工作', '5', '1'), ";
$sql .= "('$thisCategoryID', 'Role6', '待業', '待業', '6', '1')";


$laccount->db_db_query($sql);
echo $sql.'<br>';

intranet_closedb();
?>