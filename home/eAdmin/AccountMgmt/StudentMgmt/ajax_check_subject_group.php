<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();

if(substr($selectedSbjGroupStr, -1)==",")
	$selectedSbjGroupStr = substr($selectedSbjGroupStr,0,-1);


$sql = "SELECT COUNT(sub.RecordID) FROM SUBJECT_TERM_CLASS stc LEFT OUTER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stc.SubjectGroupID) LEFT OUTER JOIN ASSESSMENT_SUBJECT sub ON (sub.RecordID=st.SubjectID) WHERE stc.SubjectGroupID IN ($selectedSbjGroupStr) Group By sub.RecordID";
$row = $ldb->returnVector($sql);

$error = "";
foreach($row as $count) {
	if($count>1) {
		$error = $Lang['AccountMgmt']['OnlyCanAssignTo1SubjectGroup'];
		break;
	}		
}

echo $error;
?>