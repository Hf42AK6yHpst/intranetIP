<?php
// Edit by anna
/*
 * 2016-06-28 (Carlos): Added. 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($eclass_filepath."/src/includes/php/lib-portfolio.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!isset($user_id))
{
	intranet_closedb();
	header("Location: archived_student.php");	
	exit;
}

$user_id = IntegerSafe($user_id);

$laccount = new libaccountmgmt();
$errors = array();
## check STRN duplication ###
if($strn!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE STRN='$strn' AND UserID!='$user_id'";
	$temp = $laccount->returnVector($sql);
	
	$sql = "SELECT UserID FROM INTRANET_ARCHIVE_USER WHERE STRN='$strn' AND UserID!='$user_id'";
	$temp2 = $laccount->returnVector($sql);
	if(sizeof($temp)!=0 || sizeof($temp2)!=0){
		$errors[] = $strn;
	}
}

## check WebSAMSRegNo duplication ###
if($WebSAMSRegNo!=""){
	if(substr(trim($WebSAMSRegNo),0,1)!="#")
		$WebSAMSRegNo = "#".$WebSAMSRegNo;
		
	$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo='$WebSAMSRegNo' AND RecordType=".TYPE_STUDENT." AND UserID!='$user_id'";
	$temp = $laccount->returnVector($sql);
	
	$sql = "SELECT UserID FROM INTRANET_ARCHIVE_USER WHERE WebSAMSRegNo='$WebSAMSRegNo' AND RecordType=".TYPE_STUDENT." AND UserID!='$user_id'";
	$temp2 = $laccount->returnVector($sql);
	if(sizeof($temp)!=0  || sizeof($temp2)!=0){
		$errors[] = $WebSAMSRegNo;
	}
}

## check JUPAS APPLICATION NO duplication ###
if($HKJApplNo!="" && $HKJApplNo != 0){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKJApplNo='$HKJApplNo' AND RecordType=".TYPE_STUDENT." AND UserID!='$user_id'";
	$temp = $laccount->returnVector($sql);
	
	$sql = "SELECT UserID FROM INTRANET_ARCHIVE_USER WHERE HKJApplNo='$HKJApplNo' AND RecordType=".TYPE_STUDENT." AND UserID!='$user_id'";
	$temp2 = $laccount->returnVector($sql);
	
	if(sizeof($temp)!=0 || sizeof($temp2)!=0){
		$errors[] = $HKJApplNo;
	}
}

## check HKID duplication ###
if($hkid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKID='$hkid' AND UserID!='$user_id'";
	$temp = $laccount->returnVector($sql);
	
	$sql = "SELECT UserID FROM INTRANET_ARCHIVE_USER WHERE HKID='$hkid' AND UserID!='$user_id'";
	$temp2 = $laccount->returnVector($sql);
	if(sizeof($temp)!=0 || sizeof($temp2)!=0){
		$errors[] = $hkid;
	}
}

if($dob != ''){
	$now = date("Y-m-d");
	$dob = convertDateTimeToStandardFormat($dob, false);
	$dob_parts = explode("-",$dob);
	if($dob > $now || !checkdate(intval($dob_parts[1]),intval($dob_parts[2]),intval($dob_parts[0])))
	{
		$errors[] = $dob;
	}
}


if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
    $libpf_exam = new libpf_exam();
 
    // JUPAS Programme
    $sql = "SELECT JupasCode FROM JUPAS_PROGRAMME 
            WHERE 
            JupasCode = '".$JUPASCode."' 
            AND Institution = '".$UniversityCode."'
            AND ProgrammeFullName = '".$UniversityProgramme."'";
    $JUPASProgramme = $libpf_exam->returnVector($sql);
    
    if(empty($JUPASProgramme)){
        $ProgrammeType = $libpf_exam->getJupasProgTypeByName($UniversityProgramme);
        $insertProgramArr[] = " ( '$JUPASCode', '".$libpf_exam->Get_Safe_Sql_Query($UniversityProgramme)."', '$UniversityCode', '$ProgrammeType' ) ";
        $insert_prog = $libpf_exam->insertJupasProgrammeData($insertProgramArr);
    }
    //     $UniversityProgramme $JUPASCode $UniversityCode
    
    
    if($HKJApplNo == '' || $HKJApplNo == '0'){
        $errors[] = $HKJApplNo;
    }else{
        
        $SelectedYearJUPASRecords = $libpf_exam->getStudentJupasOfferByYear($academicYearID);

        $StudentJUPASRecordsAry = Get_Array_By_Key($SelectedYearJUPASRecords, 'StudentID');    
        if(in_array($user_id,$StudentJUPASRecordsAry)){
            $updateDataArr['JupasCode'] = $JUPASCode;
            $updateDataArr['Institution'] = $UniversityCode;
//             $updateDataArr['AcademicYearID'] = $academicYearID;
//             $updateDataArr['AcademicYearID'] = $academicYearID;

            $libpf_exam->UpdateStudentJUPASRecord($user_id,$academicYearID,$updateDataArr);
        }else{
            
            $JUPASDataAry[] = "( '$academicYearID', '$user_id', '$HKJApplNo',
            '$JUPASCode', '$UniversityCode', '',
            '', '', '',
            now(),'".$_SESSION['UserID']."', now(),'".$_SESSION['UserID']."' )";
            $success_insert = $libpf_exam->insertJupasStudentData($JUPASDataAry);     
        }
     
        
        
        
//         $libpf_exam->DeleteStudentJUPASRecord($user_id);
//         $success_insert = $libpf_exam->insertJupasStudentData($JUPASDataAry);     
    }

    // EXIT TO choice   
    $ExitToDataAry['FinalChoice']=$FinalChoice;
    if($FinalChoice == '2'){
        $Remark = $Remark1;
    }
    else if($FinalChoice == '3'){
        $Remark = $Remark2;
    }
    $ExitToDataAry['Remark']=$Remark;
    
    $ExistedExitInfo = $libpf_exam->Get_User_ExitTo_Info($user_id);
    if(empty($ExistedExitInfo)){
        
        $GraduationYear = getAcademicYearInfoAndTermInfoByDate($Graduation_Date);
        $GraduationAcademicYearID = $GraduationYear[0];
        
        
        $ExitToDataAry['AcademicYearID'] = $GraduationAcademicYearID;
        $ExitToDataAry['StudentID'] = $user_id;
        $ExitToDataAry['JupasAppNo'] = $HKJApplNo;
        $ExitToDataAry['JupasCode'] = '';
        
        $ExitToSuccess = $libpf_exam->Insert_User_ExitTo_Info($ExitToDataAry);
    }else{
        $ExitToSuccess = $libpf_exam->Update_User_ExitTo_Info($user_id, $ExitToDataAry);        
    }
}


if(count($errors)>0){
	intranet_closedb();
	$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_ERROR'] = $errors;
	//header("Location: edit_archived_student.php?user_id=".$user_id);	
	$x = '<html>
			<body onload="document.form1.submit();">
			<form name="form1" method="post" action="edit_archived_student.php">';
	foreach($_POST as $key => $val)
	{
		$x .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />';
	}
	$x .= '</form>
			</body>
			</html>';
	echo $x;	
	exit;
}



$sql = "UPDATE INTRANET_ARCHIVE_USER SET 
			WebSAMSRegNo='".$laccount->Get_Safe_Sql_Query($WebSAMSRegNo)."', STRN='".$laccount->Get_Safe_Sql_Query($strn)."', HKJApplNo=".(trim($HKJApplNo)==''?"NULL":"'".$laccount->Get_Safe_Sql_Query($HKJApplNo)."'").",
			HKID='".$laccount->Get_Safe_Sql_Query($hkid)."', DateOfBirth=".($dob==''?"NULL":"'".$laccount->Get_Safe_Sql_Query($dob)."'")."
			 WHERE UserID='$user_id'";
$success = $laccount->db_db_query($sql);

$xmsg = ($success) ? "UpdateSuccess" : "UpdateUnsuccess";

$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_RESULT'] = $xmsg;

intranet_closedb();
header("Location: archived_student.php");
exit;
?>