<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lexport = new libexporttext();

$CurrentPageArr['StudentMgmt'] = 1;
//$CurrentPage = "User List";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ExportUser'], "");

$linterface->LAYOUT_START();

?>
<script language="javascript">
function goURL(urlLink) {
	document.form1.action = urlLink;
	document.form1.submit();
}

function goSubmit() {
	document.form1.action = "export_update.php";
	document.form1.submit();	
}

function checkform(obj){
	if(countOption(obj.elements["Fields[]"])==0) {
		alert(globalAlertMsg18); return false; 
	}
	return false;
}

</script>
<br>
<form name="form1" action="export_update.php" method="post" onSubmit="return checkform(this);">
<table width="98%" border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan="2" height="40" valign="top"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
<tr><td>
<table width="95%" cellpadding="4" cellspacing="0" border="0" align="center">
<tr><td width="20%" valign="top" class="formfieldtitle"><?=$i_export_msg2?></td><td>
<?
if($sys_custom['UserExpoertWithCheckbox']) { ?>
	<input type="checkbox" name="Fields[]" value="UserLogin" id="UserLogin" checked> <label for="UserLogin"><?= $i_UserLogin; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="UserEmail" id="UserEmail" checked> <label for="UserEmail"><?= $i_UserEmail; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="EnglishName" id="EnglishName" checked> <label for="EnglishName"><?= $i_UserEnglishName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="FirstName" id="FirstName"> <label for="FirstName"><?= $i_UserFirstName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="LastName" id="LastName"> <label for="LastName"><?= $i_UserLastName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="ChineseName" id="ChineseName" checked> <label for="ChineseName"><?= $i_UserChineseName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="IF(Title IS NULL OR TRIM(Title) = '', '', CASE Title WHEN 0 THEN 'Mr.' WHEN 1 THEN 'Miss' WHEN 2 THEN 'Mrs.' WHEN 3 THEN 'Ms.' WHEN 4 THEN 'Dr.' WHEN 5 THEN 'Prof.' ELSE '' END) AS Title " id="Title"> <label for="Title"><?= $i_UserTitle; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="Gender" id="Gender"> <label for="Gender"><?= $i_UserGender; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="MobileTelNo" id="MobileTelNo"> <label for="MobileTelNo"><?= $i_UserMobileTelNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="ClassNumber" id="ClassNumber"> <label for="ClassNumber"><?= $i_UserClassNumber; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="IF(DateOfBirth IS NULL OR date_format(DateOfBirth,'%Y-%m-%d') = '0000-00-00', '', DateOfBirth) AS DateOfBirth " id="DateOfBirth"> <label for="DateOfBirth"><?= $i_UserDateOfBirth; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="HomeTelNo" id="HomeTelNo"> <label for="HomeTelNo"><?= $i_UserHomeTelNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="OfficeTelNo" id="OfficeTelNo"> <label for="OfficeTelNo"><?= $i_UserOfficeTelNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="FaxNo" id="FaxNo"> <label for="FaxNo"><?= $i_UserFaxNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="ICQNo" id="ICQNo"> <label for="ICQNo"><?= $i_UserICQNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="Address" id="Address"> <label for="Address"><?= $i_UserAddress; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="Country" id="Country"> <label for="Country"><?= $i_UserCountry; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="CardID" id="CardID"> <label for="CardID"><?= $i_SmartCard_CardID; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="WebSamsRegNo" id="WebSamsRegNo"> <label for="WebSamsRegNo"><?= $i_WebSAMS_Registration_No; ?></label> <br>
	
	<? if($special_feature['ava_hkid']) {?>
		<input type="checkbox" name="Fields[]" value="HKID" id="HKID"> <label for="HKID"><?= $i_HKID; ?></label> <br>
	<? } 
	
	if($special_feature['ava_strn']) {?>
		<input type="checkbox" name="Fields[]" value="STRN" id="STRN"> <label for="STRN"><?= $i_STRN; ?></label> <br>
	<? } ?>
	
	<? if($sys_custom['LaiShanStudentExport'] && $TabID==2) {?>
		<input type="checkbox" name="Fields[]" value="Parent" id="Parent"> <label for="Parent"><?= $Lang['Identity']['Parent']; ?></label> <br>
		<input type="checkbox" name="Fields[]" value="Guardian" id="Guardian"> <label for="Guardian"><?= $Lang['Identity']['Guardian']; ?></label> <br>
	<? }
	}  else { ?>
		<select name=Fields[] size=10 multiple>
		<option value=UserLogin SELECTED><?= $i_UserLogin; ?></option>
		<option value=UserEmail SELECTED><?= $i_UserEmail; ?></option>
		<option value=EnglishName SELECTED><?= $i_UserEnglishName; ?></option>
		<option value=FirstName ><?= $i_UserFirstName; ?></option>
		<option value=LastName ><?= $i_UserLastName; ?></option>
		<option value=ChineseName SELECTED><?= $i_UserChineseName; ?></option>
		<option value="IF(Title IS NULL OR TRIM(Title) = '', '', CASE Title WHEN 0 THEN 'Mr.' WHEN 1 THEN 'Miss' WHEN 2 THEN 'Mrs.' WHEN 3 THEN 'Ms.' WHEN 4 THEN 'Dr.' WHEN 5 THEN 'Prof.' ELSE '' END) AS Title "><?= $i_UserTitle; ?></option>
		<option value=Gender><?= $i_UserGender; ?></option>
		<option value=MobileTelNo><?= $i_UserMobileTelNo; ?></option>
		<option value=ClassNumber><?= $i_UserClassNumber; ?></option>
		<option value=ClassName><?= $i_UserClassName; ?></option>
		<option value="IF(DateOfBirth IS NULL OR date_format(DateOfBirth,'%Y-%m-%d') = '0000-00-00', '', DateOfBirth) AS DateOfBirth "><?= $i_UserDateOfBirth; ?></option>
		<option value=HomeTelNo><?= $i_UserHomeTelNo; ?></option>
		<option value=OfficeTelNo><?= $i_UserOfficeTelNo; ?></option>
		<option value=FaxNo><?= $i_UserFaxNo; ?></option>
		<option value=ICQNo><?= $i_UserICQNo; ?></option>
		<option value=Address><?= $i_UserAddress; ?></option>
		<option value=Country><?= $i_UserCountry; ?></option>
		<option value=CardID><?= $i_SmartCard_CardID; ?></option>
		<option value=WebSamsRegNo><?= $i_WebSAMS_Registration_No; ?></option>
		<? if($special_feature['ava_hkid']) {?>
		<option value=HKID><?= $i_HKID; ?></option>
		<? } ?>
		<? if($special_feature['ava_strn']) {?>
		<option value='STRN'><?= $i_STRN; ?></option>
		<? } ?>
		
		<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option>
		</select>
	<? } ?>
</td></tr>

<tr><td>&nbsp;</td>
<td>
<?= $linterface->GET_ACTION_BTN($button_export, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('index.php')")?>
</td>
</tr>
</table>

</td></tr>
</table>
	<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
	<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
	<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();


?>