<?php
// Editing by 
/*
 * 2017-09-11 (Carlos): Created. Would save the deleted records to /file/archived_user_delete_log
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($eclass_filepath."/src/includes/php/lib-portfolio.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!isset($user_id) || count($user_id)==0)
{
	intranet_closedb();
	$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_RESULT'] = "DeleteUnsuccess";
	header("Location: archived_student.php");	
	exit;
}

$user_id = IntegerSafe($user_id);

$laccount = new libaccountmgmt();

$sql = "SELECT * FROM INTRANET_ARCHIVE_USER WHERE UserID IN (".implode(",",(array)$user_id).")";
$records = $laccount->returnResultSet($sql);
$record_size = count($records);
$header_row = implode("\t",array_keys($records[0]))."\n";
$log_content = '';
for($i=0;$i<$record_size;$i++){
	$log_content .= implode("\t",$records[$i])."\n";
}

$log_file_path = $file_path.'/file/archived_user_delete_log';
if(!file_exists($log_file_path)){
	$log_content = $header_row.$log_content;
}
$log_success = file_put_contents($log_file_path,$log_content,FILE_APPEND);

if($log_success !== false){
	$sql = "DELETE FROM INTRANET_ARCHIVE_USER WHERE UserID IN (".implode(",",(array)$user_id).")";
	$success = $laccount->db_db_query($sql);
}else{
	$success = false;
}

$xmsg = ($success) ? "DeleteSuccess" : "DeleteUnsuccess";

$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_RESULT'] = $xmsg;

intranet_closedb();
header("Location: archived_student.php");
exit;
?>