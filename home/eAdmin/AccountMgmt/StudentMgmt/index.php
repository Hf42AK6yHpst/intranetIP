<?php
# modifying :  

############ Change Log Start ###############
#
#   Date:   2019-05-02 Cameron
#           show export button for oak as it required to batch change password for users [case #M159968]
#
#	Date:	2019-03-12 Carlos
#			$sys_custom['iMail']['ExportForwardingEmail'] - Added export forwarding emails for Webmail.
#
#   Date:   2018-12-17 Isaac
#           added HomeTelNo, FaxNo and MobileTelNo to the keyword search
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#	Date:	2018-01-10 Carlos - Fixed search keyword with special symbols issue. 
#
#	Date:	2017-12-22 Cameron
#			configure buttons for Oaks (same as Amway: show only new and import buttons)
#
#	Date:	2017-12-06 Pun
#			don't show [Parent] tab for NCS
#
#	Date:	2017-10-27 Cameron
#			don't show [Archived Student] tab for Amway
#
#	Date:	2017-10-26 Cameron
#			release Import function for Amway
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - display House group.
#
#	Date:	2016-06-28 Carlos - Added [Archived Student] tab.
#
#	Date:	2016-03-02 Kenneth
#			added house filter
#			deploy: ip2.5.7.3.1
#
#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added table tool buttons for enable/disable Wi-Fi access and export Wi-Fi access status list.
#
#	Date:	2015-12-15 Cameron
#			hide all button except new for Amway
#
#	Date:	2015-02-13	Omas
#			For the part of YEAR, YEAR_CLASS, YEAR_CLASS_USER Change to inner join
#
#	Date:	2014-11-11	Omas
#			Modified SQL Can sort by Parent Name and improve performance
#
#	Date:	2014-03-11	Carlos
#			Added [Import Forwarding Emails] & [Export Forwarding Emails] buttons for Lassel College
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
#	Date: 	2012-10-29 	Rita
#			add parent column
#
#	Date:	2011-09-27 YatWoon
#			remove the js alert for "delete" button
#	
#	Date:	2011-01-27 Yatwoon
#			can also search with SmartCardID
# 
#	Date:	2011-01-10	YatWoon
#			- IP25 UI
#			- Add "Active", "Suspend" tool options
#			- set cookies
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_student_targetClass", "targetClass");
$arrCookies[] = array("ck_student_recordstatus", "recordstatus");
$arrCookies[] = array("ck_student_house", "houseID");
$arrCookies[] = array("ck_student_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}


intranet_auth();
intranet_opendb();

$AcademicYearID = get_Current_Academic_Year_ID();

$laccount = new libaccountmgmt();
$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();
if(sizeof($officalPhotoMgmtMember) && in_array($UserID, $officalPhotoMgmtMember))
	$canAccessOfficalPhotoSetting = 1;

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] ||$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && $canAccessOfficalPhotoSetting) {
	header("Location: photo/index.php");
}


$lclass = new libclass();
$linterface = new interface_html();

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",1);

$showAdvanceClassOption = true;
if ($sys_custom["isCorporateClient"] && $sys_custom["disallowWithoutClass"]) {
	$showAdvanceClassOption = false;
} 
if ($showAdvanceClassOption) {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
	if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
		$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php?clearCoo=1",0);
	}
}

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('user','student');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0);

## show export button for oak as it required to batch change password for users [case #M159968]
if ($sys_custom['project']['CourseTraining']['Oaks']) {
    $toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
}

// hide all button except new and import for Amway
if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
	$toolbar .= $linterface->GET_LNK_EMAIL("javascript:checkPost(document.form1,'email.php')",$button_email,"","","",0);
	if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['BatchSetForwardingEmail']){
		$toolbar .= $linterface->GET_LNK_IMPORT("../import/import_forwarding_email.php?TabID=".USERTYPE_STUDENT,$Lang['AccountMgmt']['ImportForwardingEmails'],"","","",0);
		$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_STUDENT,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
	}
	if($plugin['webmail'] && $sys_custom['iMail']['ExportForwardingEmail']){
		$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_STUDENT,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
	}
	if($plugin['radius_server']){
		$toolbar .= $linterface->GET_LNK_EXPORT("../export_wifi_status.php?TabID=".USERTYPE_STUDENT,$Lang['AccountMgmt']['ExportWiFiAccessStatus'],"","","",0);
	}
}

// button for NCS only
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:goPrintQRcode()",$Lang['AccountMgmt']['PrintQRCode'] ,"","","",0);
}

# student selection
$studentTypeMenu = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm()\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, "");

# student selection for qr code printing
/*
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
	$classArr = $lclass->getSelectClassWithoutWholeForm();
	$studentQRTypeMenu = "<select name=\"printQRcodeClass\" id=\"classToPrint\">";
	foreach($classArr as $ca){
		$studentQRTypeMenu .= "<option value='".$ca['YearClassID']."'>{$ca['ClassTitleB5']}</option>";
	}
	$studentQRTypeMenu .= "</select>";
}*/

# Record Status 
$recordStatusMenu = "<select name=\"recordstatus\" id=\"recordstatus\" onChange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($recordstatus) || $recordstatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($recordstatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($recordstatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "<option value='3'".(($recordstatus=='3') ? " selected" : "").">{$Lang['Status']['Left']}</option>";
$recordStatusMenu .= "</select>";

# House selection 
$libgroup = new libgroup();
$houseInfoArr[0][0]=-1;	//Select All Student
$houseInfoArr[0][1]=$Lang['AccountMgmt']['StudentAccount']['HouseFilter']['SelectHouse'];
$houseInfoArr[1][0]=-2;	//Select All House
$houseInfoArr[1][1]=$Lang['AccountMgmt']['StudentAccount']['HouseFilter']['AllHouse'];
$numOfDefaultInfoArr = count($houseInfoArr);
$houseArr = $libgroup->returnAllGroupIDAndName("","",4,$AcademicYearID);
$numOfHouse = count($houseArr);
for($i=0;$i<$numOfHouse;$i++){
	$houseInfoArr[$i+$numOfDefaultInfoArr][]=$houseArr[$i]['GroupID'];
	$houseInfoArr[$i+$numOfDefaultInfoArr][] = '&nbsp&nbsp&nbsp'.(Get_Lang_Selection($houseArr[$i]['TitleChinese'], $houseArr[$i]['Title']));
}
$numOfHouseInfoArr = count($houseInfoArr);
$houseInfoArr[$numOfHouseInfoArr][0]=-3;	//Select not in any house
$houseInfoArr[$numOfHouseInfoArr][1]=$Lang['AccountMgmt']['StudentAccount']['HouseFilter']['WithoutHouse'];
if($sys_custom['project']['NCS']){
    $display = 'style="display:none;"';
}
$houseSelection = $linterface->GET_SELECTION_BOX($houseInfoArr, "name=\"houseID\" id=\"houseID\" onChange=\"reloadForm()\" {$display}", '', $ParSelected=$houseID, $CheckType=false);
# End of House selection 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

# house filter
if(isset($houseID)){
	if($houseID>0){
		$groupObj= new libgroup($houseID);
		$studentUserInfoInHouse = $groupObj->Get_Group_User(USERTYPE_STUDENT);
		$studentUserIDInHouse = Get_Array_By_Key($studentUserInfoInHouse, 'UserID');
		$conds.=" AND USR.UserID IN ('".implode("','",(array)$studentUserIDInHouse)."')";
	}
	else if ($houseID==-1) {
		// "select house" option => include with and without house students
		//Do nothing
	}
	if($houseID==-2||$houseID==-3){
		// $houseID==-2 => "all house" option => with house students only
		// $houseID==-3 => "without house" option => without house students only
		// Get All House ID in current ac yr
		$houseGroupIDArr = Get_Array_By_Key($houseArr, 'GroupID');
		$studentUserIDInHouse=array();
		for($i=0;$i<$numOfHouse;$i++){
			//Create new obj for each house
			$groupObj= new libgroup($houseGroupIDArr[$i]);
			$studentUserInfoInHouse = $groupObj->Get_Group_User(USERTYPE_STUDENT);
			$tempStudentUserIDInHouse = Get_Array_By_Key($studentUserInfoInHouse, 'UserID');
			$studentUserIDInHouse=array_merge($studentUserIDInHouse,$tempStudentUserIDInHouse);
		}
		if($houseID==-2){
			$conds.=" AND USR.UserID IN ('".implode("','",(array)$studentUserIDInHouse)."')";
		}else if($houseID==-3){
			$conds.=" AND USR.UserID NOT IN ('".implode("','",(array)$studentUserIDInHouse)."')";
		}
	}
}
# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND USR.RecordStatus=".$recordstatus;

if($targetClass!="") {
	if($targetClass=='0') {					# all classes
		# do nothing	
	} else if(is_numeric($targetClass)) {	# All students in specific form
		$conds .= " AND y.YearID=$targetClass";
	} else {								# All students in specific class
		$conds .= " AND yc.YearClassID=".substr($targetClass, 2);
	}
}

if(isset($keyword) && $li->isMagicQuotesOn()){
	$keyword = stripslashes($keyword);
}
if($keyword != "") {
	$safe_keyword = htmlentities($li->Get_Safe_Sql_Like_Query($keyword),ENT_QUOTES);
	$conds .= " AND (USR.UserLogin like '%$safe_keyword%' OR
				USR.UserEmail like '%$safe_keyword%' OR
				USR.EnglishName like '%$safe_keyword%' OR
				USR.ChineseName like '%$safe_keyword%' OR 
				USR.ChineseName like '%".$li->Get_Safe_Sql_Like_Query($keyword)."%' OR 
				USR.ClassName like '%$safe_keyword%' OR
				USR.WebSamsRegNo like '%$safe_keyword%' OR
				USR.CardID like '%$safe_keyword%' OR
				USR.HomeTelNo like '%$safe_keyword%' OR
                USR.FaxNo like '%$safe_keyword%' OR
				USR.MobileTelNo like '%$safe_keyword%'
				)";
}

$showWithClass = true;
if ($sys_custom["isCorporateClient"] && $sys_custom["disallowWithoutClass"]) {
	$showWithClass = false;
}
if ($showWithClass) {
	$conds .= " AND yc.AcademicYearID='".get_Current_Academic_Year_ID()."'";
}

//$sql = "SELECT
//			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
//			ycu.ClassNumber as clsNo,
//			EnglishName,
//			IFNULL(IF(ChineseName='', '---', ChineseName),'---') as ChineseName,
//			USR.UserID as StudentID,
//			UserLogin,
//			IFNULL(LastUsed, '---') as LastUsed,
//			CASE USR.RecordStatus
//				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
//				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
//				WHEN 3 THEN '". $Lang['Status']['Left'] ."'
//				ELSE '$i_status_suspended' END as recordStatus,
//			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox,
//			USR.UserID
//		FROM 
//			INTRANET_USER USR LEFT OUTER JOIN
//			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
//			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
//			YEAR y ON (y.YearID=yc.YearID)
//		WHERE
//			RecordType = ".TYPE_STUDENT."
//			$conds ";
$classNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$parentNameField = Get_Lang_Selection('IF(par.ChineseName=\'\' OR par.ChineseName is null, par.EnglishName, par.ChineseName)', 'IF(par.EnglishName=\'\' OR par.EnglishName is null, par.ChineseName, par.EnglishName)');

$houseGroupNameField = Get_Lang_Selection('g.TitleChinese','g.Title');

$sql = "SELECT ";
if ($showWithClass) {
	$sql .= "".$classNameField." as clsName,
				ycu.ClassNumber as clsNo,";
}
$sql .= " USR.EnglishName,
			IFNULL(IF(USR.ChineseName='', '---', USR.ChineseName),'---') as ChineseName,";
if ($showWithClass && !$sys_custom['project']['NCS']) {
			/*start of link*/
	$sql .= "Group_Concat(
				Distinct Concat(
					'<a href=\"javascript:goURL(\'../ParentMgmt/edit.php\',', par.UserID, ');\">', $parentNameField, '</a>'
				) 
				Separator '<br>' 
			) as parentNameLink,";
}
			/*end of link
			USR.UserID as StudentID,*/
$sql .= " USR.UserLogin,";
if ($showWithClass) {
	if($sys_custom['BIBA_AccountMgmtCust']){
		$sql .= " GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', ') as House, ";
	}
}
$sql .=	" IFNULL(USR.LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				WHEN 3 THEN '". $Lang['Status']['Left'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox,
			USR.UserID,
			Group_concat(distinct par.EnglishName SEPARATOR '<br>') as parentName
		FROM 
			INTRANET_USER USR";
if ($showWithClass) {
	$sql .=	" LEFT OUTER JOIN INTRANET_PARENTRELATION pr ON (pr.StudentID = USR.UserID) 
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) 
			INNER JOIN	YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
			INNER JOIN	YEAR y ON (y.YearID=yc.YearID)
			LEFT OUTER JOIN INTRANET_USER par ON (pr.ParentID = par.UserID) ";
} else {
	$sql .=	" LEFT JOIN INTRANET_PARENTRELATION pr ON (pr.StudentID = USR.UserID)
			LEFT JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
			LEFT JOIN	YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			LEFT JOIN	YEAR y ON (y.YearID=yc.YearID)
			LEFT JOIN INTRANET_USER par ON (pr.ParentID = par.UserID) ";
}
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " LEFT JOIN INTRANET_USERGROUP as ug ON ug.UserID=USR.UserID 
			  LEFT JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND g.RecordType=4 AND g.AcademicYearID='$AcademicYearID' ";
}
$sql .=" WHERE
			USR.RecordType = ".TYPE_STUDENT."
			$conds 
		Group by
			USR.UserID";	

$li->sql = $sql;
// echo $sql;
//$li->field_array = array("clsName", "concat(TRIM(SUBSTRING_INDEX(clsNo, '-', 1)), IF(INSTR(clsNo, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(clsNo, '-', -1)), 10, '0')))", "EnglishName", "ChineseName", "UserLogin", "LastUsed", "recordStatus");
if($sys_custom['project']['NCS']){
	$li->field_array = array("clsName", "clsNo", "EnglishName", "ChineseName", "UserLogin");
}else if ($showWithClass) {
	$li->field_array = array("clsName", "clsNo", "EnglishName", "ChineseName", "parentName", "UserLogin");
} else {
	$li->field_array = array("EnglishName", "ChineseName", "UserLogin");
}
if ($showWithClass) {
	if($sys_custom['BIBA_AccountMgmtCust']){
		$li->field_array = array_merge($li->field_array, array("House"));
	}
}
$li->field_array = array_merge($li->field_array, array( "LastUsed", "recordStatus"));
$li->no_col = sizeof($li->field_array)+2;
if ($showWithClass) {
	if($field!=1)	
		$li->fieldorder2 = ", USR.ClassNumber ASC";
}
$li->IsColOff = "UserMgmtStudentAccount";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
if ($showWithClass) {
	$li->column_list .= "<th width='12%' >".$li->column($pos++, $i_general_class)."</th>\n";
	$li->column_list .= "<th width='5%' >".$li->column($pos++, $i_ClassNumber)."</th>\n";
	$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
	if($sys_custom['project']['NCS']){
	    $li->column_list .= "<th width='29%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
	}else{
	    $li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
	}
} else {
	$li->column_list .= "<th width='25%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
	$li->column_list .= "<th width='25%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
}

if ($showWithClass && !$sys_custom['project']['NCS']) {
	$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['Identity']['Parent'])."</th>\n";
}
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
if ($showWithClass) {
	if($sys_custom['BIBA_AccountMgmtCust']){
		$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['AccountMgmt']['House'])."</th>\n";
	}
}
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";


# student license
if (isset($account_license_student) && $account_license_student>0)
{
	$license_student_used = $li->getLicenseUsed();
	$license_student_left = $account_license_student - $license_student_used;
	$student_license_text = str_replace("<!--license_total-->", $account_license_student, $i_license_sys_msg);
	$student_license_text = str_replace("<!--license_used-->", $license_student_used, $student_license_text);
	$student_license_text = str_replace("<!--license_left-->", $license_student_left, $student_license_text);
	$student_license_text .= "<br />";
}

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "index.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function newUser() {
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() {
	document.form1.TabID.value = <?=TYPE_STUDENT?>;
	document.form1.action = "../export.php?TabID=<?=TYPE_STUDENT?>";
	document.form1.submit();
}

function goImport() {
	document.form1.TabID.value = <?=TYPE_STUDENT?>;
	document.form1.action ="../import/import.php?TabID=<?=TYPE_STUDENT?>";
	document.form1.submit();
}

function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

function checkSuspend(obj,element,url) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm("<?=$Lang['AccountMgmt']['AlertMsg_SuspendConfirm']?>")) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

function checkLeft(obj,element,url) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm("<?=$Lang['AccountMgmt']['AlertMsg_LeftConfirm']?>")) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

/*
function checkDelete(obj,element,url) {
	if(countChecked(obj,element)==0)
	{
		alert("<?=$Lang['AccountMgmt']['JSWarning']['PleaseSelectUserAccount']?>");
	}
	else 
	{
			document.form1.action = url;
			document.form1.submit();
	}
}
*/

function goURL(thisURL, uid) {
	document.form1.action = thisURL;
	document.form1.uid.value = uid;
	document.form1.submit();
}

function goPrintQRcode(){
	/*
	$.blockUI({ 
        title:    'This is your title', 
        message:  $('#PrintQrCodeMenu')
    });
	 
	$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI); 
	*/
	load_dyn_size_thickbox_ip('<?=$Lang['AccountMgmt']['PrintQRCode']?>', 'onloadThickBox()', inlineID='', defaultHeight=200, defaultWidth=400);
}

function onloadThickBox(){
	$('div#TB_ajaxContent').load(
		"get_student_qr_code_thickbox_content.php", 
		{ 
			academicYearId: 1,
			yearTermId: 2
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function goPrint(){
	window.open('print_students_qr_code.php?YearClassID='+$('#classToPrint').val(), "_blank", "width=800,height=600");
	//$.unblockUI();
	js_Hide_ThickBox();
}
/*
function goCancelPrint(){
	$.unblockUI();
}*/

</script>
<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2"><?=$student_license_text?></td>
</tr>
<tr>
	<td valign="bottom">
		<?php
		if ($showWithClass) {
			echo $studentTypeMenu;
		}
			echo $recordStatusMenu;
		if ($showWithClass) {
			echo $houseSelection;
		}
		?>
	</td>
</tr>
<tr>

	<td valign="bottom">
		<div class="common_table_tool">
		<a href="javascript:changeFormAction(document.form1,'user_id[]','../email_password.php')" class="tool_email"><?=$Lang['AccountMgmt']['SendResetPasswordEmail']?></a>
        <? if($plugin['radius_server']){ ?>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../enable_wifi_access.php')" class="tool_approve"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></a>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../disable_wifi_access.php')" class="tool_reject"><?=$Lang['AccountMgmt']['DisableWifiAccess']?></a>
        <? } ?>
		<? if($recordstatus!=1) {?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<? } ?>
		<? if($recordstatus=="" || $recordstatus!=0) {?>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<? } ?>
		<? if($recordstatus!=3) {?>
		<a href="javascript:checkLeft(document.form1,'user_id[]','left.php')" class="tool_other"><?=$Lang['Status']['Left']?></a>
		<? } ?>
        <a href="javascript:checkRemove(document.form1,'user_id[]','remove.php', '<?=$Lang['AccountMgmt']['DeleteUserWarning']?>')" class="tool_delete"><?=$button_delete?></a> 
		</div>
	</td>
</tr>
</table>


<?= $li->display()?>

<input type="hidden" name="TabID" id="TabID" value="" />
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/StudentMgmt/index.php">
<input type="hidden" name="clearCoo" id="clearCoo" value="" />
</form>
<div id="PrintQrCodeMenu" style="display:none">
	<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
	<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen">
</div>
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>