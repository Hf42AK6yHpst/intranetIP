<?php
# using: 

############# Change Log
#	Date:	2020-09-21 Philips
#			Added KISClassType for $plugin['SDAS_module']['KISMode']
#
#   Date:   2019-04-30 Cameron
#           fix cross site scripting by applying cleanCrossSiteScriptingCode() and cleanHtmlJavascript() to variables
#
#   Date:   2018-11-12 Paul 
#           Handled NCS Lang Priority error. Bypassing the inputfield creation if no Lang chosen.
#
#   Date:   2018-09-18 Anna 
#           Added $plugin['StudentDataAnalysisSystem_Style'] == "tungwah"
#
#	Date:	2018-01-03 Carlos - Improved password checking.
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added new text field HouseholdRegister.
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date: 	2015-12-30 Carlos
#			$plugin['radius_server'] - added Wi-Fi access option.
# 									
#	Date:	2014-12-10	Bill
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4
#
#	Date:	2013-12-12	Fai
#			Add a cust attribute "stayOverNight"
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#
#	Date:	2010-11-19	(Henry Chow)
#			add selection of "Group" & "Role"
#
#	Date:	2010-11-05	Marcus
#			Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}

$magic_quotes_enabled = function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc();

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lo = new libgrouping();
$lrole = new librole();

$lauth = new libauth();
$lu = new libuser();
$thisUserType = USERTYPE_STUDENT;
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;
$SettingArr = $lu->RetrieveUserInfoSettingInArrary($SettingNameArr);
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6) $PasswordLength = 6;

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

### start handle sql injection and cross site scripting
$status = IntegerSafe($status);
$comeFrom = cleanCrossSiteScriptingCode($comeFrom);

$targetClass = cleanHtmlJavascript($targetClass);
$recordstatus = cleanHtmlJavascript($recordstatus);
$keyword = cleanHtmlJavascript($keyword);
$userlogin = cleanHtmlJavascript($userlogin);
$pwd = cleanHtmlJavascript($pwd);
$email = cleanHtmlJavascript($email);           // not available in edit
$smartcardid=cleanHtmlJavascript($smartcardid);
if($sys_custom['SupplementarySmartCard']){
    $smartcardid2 = cleanHtmlJavascript($smartcardid2);
    $smartcardid3 = cleanHtmlJavascript($smartcardid3);
    $smartcardid4 = cleanHtmlJavascript($smartcardid4);
}
if($plugin['medical']){
    $stayOverNight = IntegerSafe($stayOverNight);
}

$strn = cleanHtmlJavascript($strn);
$WebSAMSRegNo = cleanHtmlJavascript($WebSAMSRegNo);
$HKJApplNo = cleanHtmlJavascript($HKJApplNo);
$barcode = cleanHtmlJavascript($barcode);
$engname = cleanHtmlJavascript($engname);
$chiname = cleanHtmlJavascript($chiname);
$nickname = cleanHtmlJavascript($nickname);
$dob = cleanHtmlJavascript($dob);
$gender = cleanHtmlJavascript($gender);
$hkid = cleanHtmlJavascript($hkid);
$address = cleanHtmlJavascript($address);
$country = cleanHtmlJavascript($country);
$homePhone = cleanHtmlJavascript($homePhone);
$mobilePhone = cleanHtmlJavascript($mobilePhone);
$faxPhone = cleanHtmlJavascript($faxPhone);
$open_webmail = cleanHtmlJavascript($open_webmail);
$open_file = cleanHtmlJavascript($open_file);
$remark = cleanHtmlJavascript($remark);
$errorList = cleanHtmlJavascript($errorList);

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    $PrimarySchoolCode = cleanHtmlJavascript($PrimarySchoolCode);
}

if($sys_custom['StudentAccountAdditionalFields']){
    $NonChineseSpeaking = cleanHtmlJavascript($NonChineseSpeaking);
    $SpecialEducationNeeds = cleanHtmlJavascript($SpecialEducationNeeds);
    $PrimarySchool = cleanHtmlJavascript($PrimarySchool);
    $University = cleanHtmlJavascript($University);
    $Programme = cleanHtmlJavascript($Programme);
}
if($sys_custom['BIBA_AccountMgmtCust']){
    $HouseholdRegister = cleanHtmlJavascript($HouseholdRegister);
}
$Nationality = cleanHtmlJavascript($Nationality);
$PlaceOfBirth = cleanHtmlJavascript($PlaceOfBirth);
$AdmissionDate = cleanHtmlJavascript($AdmissionDate);
//$GraduationDate = cleanHtmlJavascript($GraduationDate);
$Quota = cleanHtmlJavascript($Quota);
$EmailStatus = cleanHtmlJavascript($EmailStatus);

if($sys_custom['StudentMgmt']['BlissWisdom']) {
    $Occupation = cleanHtmlJavascript($Occupation);
    $Company = cleanHtmlJavascript($Company);
    $PassportNo = cleanHtmlJavascript($PassportNo);
    $Passport_ValidDate = cleanHtmlJavascript($Passport_ValidDate);
    $HowToKnowBlissWisdom = cleanHtmlJavascript($HowToKnowBlissWisdom);
}
if($plugin['radius_server']){
    $enable_wifi_access = IntegerSafe($enable_wifi_access);
}

if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
    $staffInCharge = cleanHtmlJavascript($staffInCharge);
//     $hkDuration = cleanHtmlJavascript($hkDuration);
//     $b5Priority = cleanHtmlJavascript($b5Priority);
//     $enPriority = cleanHtmlJavascript($enPriority);
//     $hinduPriority = cleanHtmlJavascript($hinduPriority);
    $OthersPriority = cleanHtmlJavascript($OthersPriority);                 // not availabe in edit
    $OthersPriority_details=cleanHtmlJavascript($OthersPriority_details);   // not availabe in edit
    $isNCS = cleanHtmlJavascript($isNCS);
    $NGO = cleanHtmlJavascript($NGO);
}
### end handle sql injection and cross site scripting

if($comeFrom=="notInClass.php") {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"notInClass.php",0);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php",1);
} else {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",1);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php",0);
}
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputStudentDetails'], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputGuardianDetail'], 1);

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check Smart Card ID duplication ###
if($smartcardid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid'";
	if($sys_custom['SupplementarySmartCard']){
		$sql.= " OR CardID2='$smartcardid' OR CardID3='$smartcardid' OR CardID4='$smartcardid'";
	}
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid2!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid2' OR CardID2='$smartcardid2' OR CardID3='$smartcardid2' OR CardID4='$smartcardid2'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid2;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid3!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid3' OR CardID2='$smartcardid3' OR CardID3='$smartcardid3' OR CardID4='$smartcardid3'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid3;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid4!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid4' OR CardID2='$smartcardid4' OR CardID3='$smartcardid4' OR CardID4='$smartcardid4'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid4;
	}
}

## check STRN duplication ###
if($strn!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE STRN='$strn'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $strn;
	}
}

## check WebSAMSRegNo duplication ###
if($WebSAMSRegNo!=""){
	if(substr(trim($WebSAMSRegNo),0,1)!="#")
		$WebSAMSRegNo = "#".$WebSAMSRegNo;
		
	$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo='$WebSAMSRegNo' AND RecordType=".TYPE_STUDENT;
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $WebSAMSRegNo;
	}
}

## check JUPAS APPLICATION NO duplication ###
if($HKJApplNo!="" && $HKJApplNo != 0){
		
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKJApplNo='$HKJApplNo' AND RecordType=".TYPE_STUDENT;
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $HKJApplNo;
	}
}

## check HKID duplication ###
if($hkid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKID='$hkid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $hkid;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}

# country selection
//$countrySelection = "<select name='country' id='country'>";
$countrySelection .= "<option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
for($i=0; $i<sizeof($Lang['Country']); $i++) {
	$countrySelection .= '<option value=\''.$Lang['Country'][$i].'\' '.(($Lang['Country'][$i]==$country)?'selected' : '').'>'.$Lang['Country'][$i].'</option>';	
}
//$countrySelection .= "</select>";

# guardian table
//$table_content = "<div id='guardians'></div>";
$table_content="<table id='guardians' width=100% border=0 cellpadding=2 cellspacing=0>";
$table_content.="<tr>";
$table_content.="<th class='tableTitle' width=1>&nbsp;</td>";
$table_content.="<th class='tableTitle' width=70%>$i_Discipline_Details</td>";
$table_content.="<th class='tableTitle' width=18%>$i_Discipline_Statistics_Options</td>";	
$table_content.="<th class='tableTitle'>&nbsp;</td></tr>";
$table_content.="</table>";

# end guardian table

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();


if($sys_custom['UserExtraInformation'])
{
	if(sizeof($ExtraInfo1)>0) {
		foreach($ExtraInfo1 as $thisInfo)
			$ExtraInfoInput .= '<input type="hidden" name="ExtraInfo[]" value="'.$thisInfo.'">';
	}
	
	$ExtraInfoCatArr = $laccount->Get_User_Extra_Info_Category("", " AND eic.RecordType=2");
	$noOfSingleSelectItem = count($ExtraInfoCatArr);
	if($noOfSingleSelectItem>0) {
		for($i=0; $i<$noOfSingleSelectItem; $i++) {
			if(${'ExtraInfo2_'.$ExtraInfoCatArr[$i]['CategoryID']}[0] != 0) {
				$ExtraInfoInput .= '<input type="hidden" name="ExtraInfo[]" value="'.${'ExtraInfo2_'.$ExtraInfoCatArr[$i]['CategoryID']}[0].'">';
			}
		}
	}
	
	$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
	$ExtraInfoOthersArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);
	
	if(count($ExtraInfoOthersArr)>0) {
		foreach($ExtraInfoOthersArr as $item_id=>$item_code) {
			if(${'ExtraOthers_'.$item_id}!='') 
				$ExtraInfoInput .= '<input type="hidden" name="ExtraOthers_'.$item_id.'" value="'.${'ExtraOthers_'.$item_id}.'">';
		}
	}

}


if($sys_custom['StudentMgmt']['BlissWisdom']) {
	if($HowToKnowBlissWisdom==1) {
		$HowToKnowBlissWisdom .= "::".$FriendName;	
	} else if($HowToKnowBlissWisdom==6) {
		$HowToKnowBlissWisdom.= "::".$HowToKnowOthersField;	
	}
}

if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="new.php">
		<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
	<?php if($sys_custom['SupplementarySmartCard']){ ?>
		<input type="hidden" name="smartcardid2" id="smartcardid2" value="<?=$smartcardid2?>">
		<input type="hidden" name="smartcardid3" id="smartcardid3" value="<?=$smartcardid3?>">
		<input type="hidden" name="smartcardid4" id="smartcardid4" value="<?=$smartcardid4?>">
	<?php } ?>
	<?php if($plugin['medical']){ ?>	
				<input type="hidden" name="stayOverNight" id="stayOverNight" value="<?=$stayOverNight?>">
	<?php } ?>
		<input type="hidden" name="strn" id="strn" value="<?=$strn?>">
		<input type="hidden" name="WebSAMSRegNo" id="WebSAMSRegNo" value="<?=$WebSAMSRegNo?>">
		<input type="hidden" name="HKJApplNo" id="HKJApplNo" value="<?=$HKJApplNo?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="engname" id="engname" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="dob" id="dob" value="<?=$dob?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
	<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){ ?>
	    <input type="hidden" name="PrimarySchoolCode" id="PrimarySchoolCode" value="<?=$PrimarySchoolCode?>" />  	  
	<? }?>
	
	<?php if($sys_custom['StudentAccountAdditionalFields']){ 
		if($magic_quotes_enabled){
			$PrimarySchool = stripslashes($PrimarySchool);
			$University = stripslashes($University);
			$Programme = stripslashes($Programme);
		}
	?>	
		<input type="hidden" name="NonChineseSpeaking" id="NonChineseSpeaking" value="<?=$NonChineseSpeaking?>" />
		<input type="hidden" name="SpecialEducationNeeds" id="SpecialEducationNeeds" value="<?=$SpecialEducationNeeds?>" />
		<input type="hidden" name="PrimarySchool" id="PrimarySchool" value="<?=intranet_htmlspecialchars($PrimarySchool)?>" />
		<input type="hidden" name="University" id="University" value="<?=intranet_htmlspecialchars($University)?>" />
		<input type="hidden" name="Programme" id="Programme" value="<?=intranet_htmlspecialchars($Programme)?>" />
	<?php } ?>
	

	
	
	<?php if($sys_custom['BIBA_AccountMgmtCust']){ ?>
		<input type="hidden" name="HouseholdRegister" id="HouseholdRegister" value="<?=intranet_htmlspecialchars($HouseholdRegister)?>" />
	<?php } ?>
		<input type="hidden" name="Nationality" id="Nationality" value="<?=$Nationality?>">
		<input type="hidden" name="PlaceOfBirth" id="PlaceOfBirth" value="<?=$PlaceOfBirth?>">
		<input type="hidden" name="AdmissionDate" id="AdmissionDate" value="<?=$AdmissionDate?>">
		<? if($sys_custom['StudentMgmt']['BlissWisdom']) {?>
		<input type="hidden" name="Occupation" id="Occupation" value="<?=$Occupation?>">
		<input type="hidden" name="Company" id="Company" value="<?=$Company?>">
		<input type="hidden" name="PassportNo" id="PassportNo" value="<?=$PassportNo?>">
		<input type="hidden" name="Passport_ValidDate" id="Passport_ValidDate" value="<?=$Passport_ValidDate?>">
		<input type="hidden" name="HowToKnowBlissWisdom" id="HowToKnowBlissWisdom" value="<?=$HowToKnowBlissWisdom?>">
		<?}?>
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
		
		<?php 
		if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){ 
			if(!empty($LangPriority)){
			    foreach($LangPriority as $priority){
			        echo '<input type="hidden" name="LangPriority[]" value="'.$priority.'">';
			    }
			}
	        echo '<input type="hidden" name="OthersPriority" value="'.$OthersPriority.'">';
	        echo '<input type="hidden" name="OthersPriority_details" value="'.$OthersPriority_details.'">';
	        echo '<input type="hidden" name="isNCS" value="'.$isNCS.'">';
	        echo '<input type="hidden" name="NGO" value="'.$NGO.'">';
		} 
		?>
		<?=$ExtraInfoInput?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;

	} else { ?>

	<script language="javascript">
	function goURL(urlLink) {
		document.form1.action = urlLink;
		document.form1.submit();
	}
	
	function goSubmit() {
		var obj = document.form1;
		
		if(obj.newids.value!="") {
			var tmp = obj.newids.value.split(',');
			for(i=0; i<tmp.length; i++) {
				/*
				if(eval("obj.enname_new_"+tmp[i]+".value")=="")	{
					alert("<?=$i_alert_pleasefillin." ".$i_UserEnglishName?>");
					eval("obj.enname_new_"+tmp[i]).focus();
					return false;
				}
				if(eval("obj.chname_new_"+tmp[i]+".value")=="")	{
					alert("<?=$i_alert_pleasefillin." ".$i_UserChineseName?>");
					eval("obj.chname_new_"+tmp[i]).focus();
					return false;
				}
				*/
				if(eval("obj.enname_new_"+tmp[i]+".value")=="" && eval("obj.chname_new_"+tmp[i]+".value")=="")	{
					alert("<?=$i_alert_pleasefillin." ".$i_UserEnglishName." $i_general_or ".$i_UserChineseName?>");
					eval("obj.enname_new_"+tmp[i]).focus();
					return false;
				}
				if(eval("obj.relation_new_"+tmp[i]+".value")=="") {
					alert("<?=$i_alert_pleaseselect." ".$ec_iPortfolio['relation']?>");
					eval("obj.relation_new_"+tmp[i]).focus();
					return false;
				}
				if(eval("obj.emphone_new_"+tmp[i]+".value")=="") {
					alert("<?=$i_alert_pleasefillin." ".$i_StudentGuardian_EMPhone?>");
					eval("obj.emphone_new_"+tmp[i]).focus();
					return false;
				}
				if(obj.mainGuardianFlag.value=="") {
					alert("<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['MainGuardian']?>");	
					return false;
				}
				<? if($plugin['sms']){ ?>
				if(obj.smsFlag.value=="") {
					alert("<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['ReceiveSMS']?>");	
					return false;
				}
				<? } ?>
				if(obj.emergencyContactFlag.value=="") {
					alert("<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['EmergencyContact']?>");	
					return false;
				}
			}
		}
		var groupLength = document.form1.elements['GroupID'].length;
		var roleLength = document.form1.elements['RoleID'].length;
		
		for(i=0; i<groupLength; i++) {
			document.form1.elements['GroupID'][i].selected = true;	
		}
		for(i=0; i<roleLength; i++) {
			document.form1.elements['RoleID'][i].selected = true;	
		}
		
		//obj.submit();	
	}
	
	// add new guardian
	function addGuardian() {
		objTable = document.getElementById("guardians");
		tableRows = objTable.rows;
		
		rowStyle = 'tableContent';
		//rowStyle ='';
		row = objTable.insertRow(-1);
		row.id = parseInt(document.form1.tsize.value,10)+1;
		
		document.form1.tsize.value = row.id;
		document.form1.newids.value+=document.form1.newids.value!=""?","+row.id:row.id;
		document.form1.totalRow.value = parseInt(document.form1.totalRow.value)+1;
		
		cell0 = "<table class='form_table inside_form_table'>";
		cell0 += "	<tr><td class='field_title' rowspan='2'><span class='tabletextrequire'>*</span><?=$i_general_name?></td><td><span class='sub_row_content'>(<?=$ip20_lang_eng?>)</span> <input name='enname_new_"+row.id+"' type='text' id='enname_new_"+row.id+"' class='textbox_name'/></td></tr><tr><td><span class='sub_row_content'>(<?=$Lang['AccountMgmt']['Chi']?>)</span> <input name='chname_new_"+row.id+"' type='text' id='chname_new_"+row.id+"' class='textbox_name'/> </td></tr>";
		cell0 += "	<tr><td class='formfieldtitle'><span class='tabletextrequire'>*</span><?=$ec_iPortfolio['relation']?></td><td><select name='relation_new_"+row.id+"' id='relation_new_"+row.id+"'><option selected>- <?=$i_alert_pleaseselect.' '.$i_general_Type?> -</option><option value='01'><?=$ec_guardian['01']?></option><option value ='02'><?=$ec_guardian['02']?></option><option value ='03'><?=$ec_guardian['03']?></option><option value ='04'><?=$ec_guardian['04']?></option><option value ='05'><?=$ec_guardian['05']?></option><option value ='06'><?=$ec_guardian['06']?></option><option value ='07'><?=$ec_guardian['07']?></option><option value ='08'><?=$ec_guardian['08']?></option></select><br /></td></tr>";
		cell0 += "	<tr><td class='formfieldtitle'><?=$Lang['AccountMgmt']['Tel']?></td><td><input name='phone_new_"+row.id+"' type='text' id='phone_new_"+row.id+"' class='textboxnum'/></td></tr>";
		cell0 += "	<tr><td class='formfieldtitle'><span class='tabletextrequire'>*</span><?=$i_StudentGuardian_EMPhone?></td><td><input name='emphone_new_"+row.id+"' type='text' id='emphone_new_"+row.id+"' class='textboxnum'/><br /></td></tr>";
		cell0 += "	<tr><td class='formfieldtitle'><?=$i_StudentGuardian_Occupation?> </td><td><input name='occupation_new_"+row.id+"' type='text' id='occupation_new_"+row.id+"' class='textbox_name'/><br /></td></tr>";
		cell0 += "	<tr><td class='formfieldtitle'><?=$i_StudentGuardian_LiveTogether?> </td><td ><input name='liveTogether_new_"+row.id+"' type='radio' id='liveTogether_new_"+row.id+"_1' value='1' checked='checked' /><?=$i_general_yes?><input type='radio' name='liveTogether_new_"+row.id+"' id='liveTogether_new_"+row.id+"_0' value='0' /><?=$i_general_no?></td></tr>";
		cell0 += "	<tr><td class='formfieldtitle'><?=$i_UserAddress?></td><td>";
		cell0 += "	<textarea cols='60' rows='5' name='address_new_"+row.id+"' id='address_new_"+row.id+"'></textarea>";
		//cell0 += "<br><select name='country_new_"+row.id+"' id='country_new_"+row.id+"'><?=$countrySelection?></select>";
		cell0 += "</td></tr>";
			
		<? if($plugin['StudentRegistry']){
			/*
			$js_select_area ="<SELECT name='add_area_new_\"+row.id+\"'>";
			foreach($i_StudentRegistry_AreaCode as $v=>$d)
			        $js_select_area .="<OPTION value='$v'/>$d";
			$js_select_area.="</SELECT>";
			*/
		} else{ ?>
			//cell0+="<tr><td><?=$i_UserAddress?> :&nbsp;</td><td><textarea cols=30 rows=3 name='address_new_"+row.id+"'></textarea></td></tr>";
		<? } ?>
		cell0+="</table>";
		/*
		cell1="<input type=radio name='main[]' value='new_"+row.id+"'>";
	
		cell12 = "<input type=radio name='ec[]' value='new_"+row.id+"'>";
		*/
		cell1 = "<div id='spanOptions"+row.id+"' class='guardian_option'> <a href=javascript:changeOption('mainGuardian','mainGuardian_"+row.id+"',"+row.id+") class='main_guardian' id='mainGuardian_"+row.id+"' title='<?=$Lang['AccountMgmt']['MainGuardian']?>'>&nbsp;</a>";
		cell12 = "";
		<? if($plugin['sms']){ ?>
				//cell2 = "<input type=radio name='sms[]' value='"+row.id+"'>";
				cell1 += "<a href=javascript:changeOption('sms','sms_"+row.id+"',"+row.id+") class='send_sms_to' id='sms_"+row.id+"' title='<?=$Lang['AccountMgmt']['ReceiveSMS']?>'>&nbsp;</a>";
				cell2 = "";
				cell3 = "<div class='table_row_tool row_content_tool'><a href=\"javascript:removeGuardian('"+row.id+"')\" class='delete_dim' title='<?=$button_delete?>'></a></div>";
		<? } else{ ?>
				cell2 = "<div class='table_row_tool row_content_tool'><a href=\"javascript:removeGuardian('"+row.id+"')\" class='delete_dim' title='<?=$button_delete?>'></a></div>";
	
		<? } ?>
		cell1 += " <a href=javascript:changeOption('emergencyContact','emergencyContact_"+row.id+"',"+row.id+") class='emergency_contact' id='emergencyContact_"+row.id+"' title='<?=$Lang['AccountMgmt']['EmergencyContact']?>'>&nbsp;</a></div>";
		
		cell = row.insertCell(0);
		cell.className = rowStyle;
		cell.innerHTML = row.id;
		
		cell = row.insertCell(1);
		cell.className = rowStyle;
		cell.innerHTML = cell0;
		
		cell = row.insertCell(2);
		cell.className = rowStyle;
		cell.innerHTML = cell1;
	
		//cell = row.insertCell(3);
		cell.className = rowStyle;
		cell.innerHTML += cell12;
		
		//cell = row.insertCell(4);
		cell.className = rowStyle;
		cell.innerHTML += cell2;
	
		<? if($plugin['sms']){?>
			cell = row.insertCell(3);
			cell.className = rowStyle;
			cell.innerHTML = cell3;
		<? } ?>
		if(row.id==1) {
			changeOption('mainGuardian','mainGuardian_'+row.id,row.id);
			changeOption('sms','sms_'+row.id,row.id);
			changeOption('emergencyContact','emergencyContact_'+row.id,row.id);
		}
	}
	
	// to remove new guardian
	function removeGuardian(rid){
		var obj = document.form1;
		if(obj.mainGuardianFlag.value==rid) {
			alert("<?=$i_StudentGuardian_warning_Delete_Main_Guardian?>");	
		}
		<? if($plugin['sms']){ ?>
		else if(obj.smsFlag.value==rid) {
			alert("<?=$i_StudentGuardian_warning_Delete_SMS?>");	
		}
		<? } ?>
		else if(obj.emergencyContactFlag.value==rid) {
			alert("<?=$Lang['AccountMgmt']['AlertDeleteEmergencyContact']?>");	
		}
		else {
	
			objTable = document.getElementById("guardians");
			tableRows = objTable.rows;
			for(i=0;i<tableRows.length;i++)
				if(tableRows[i].id==rid){
					objTable.deleteRow(i);
					if(rid.indexOf("r_")>-1){
						//document.form1.deletedids.value+=document.form1.deletedids.value!=""?","+rid.substring(2):rid.substring(2);
					}else{
		
						newids = document.form1.newids.value;
						if(newids==rid) newids="";
						newids = newids.replace(","+rid,"");
						newids = newids.replace(rid+",","");
						document.form1.newids.value = newids;
					}
					break;
				}
				
			document.form1.totalRow.value = parseInt(document.form1.totalRow.value)-1;;	
		}
	}
	
	function changeOption(emt, emtRef, rid) {
		if(emt=='mainGuardian') {
			for(i=1; i<=document.form1.tsize.value; i++) {
				if(document.getElementById("mainGuardian_"+i) != undefined)
					document.getElementById("mainGuardian_"+i).className = "main_guardian";
			}
			document.getElementById(emtRef).className = (document.getElementById('mainGuardianFlag').value==rid) ? "main_guardian" : "main_guardian_selected";
			document.getElementById('mainGuardianFlag').value = (document.getElementById('mainGuardianFlag').value==rid) ? "" : rid;
		}
		if(emt=='sms') {
			for(i=1; i<=document.form1.tsize.value; i++) {
				if(document.getElementById("sms_"+i) != undefined)
					document.getElementById("sms_"+i).className = "send_sms_to";
			}
			document.getElementById(emtRef).className = (document.getElementById('smsFlag').value==rid) ? "send_sms_to" : "send_sms_to_selected";
			document.getElementById('smsFlag').value = (document.getElementById('smsFlag').value==rid) ? "" : rid;
		}
		if(emt=='emergencyContact') {
			for(i=1; i<=document.form1.tsize.value; i++) {
				if(document.getElementById("emergencyContact_"+i) != undefined)
					document.getElementById("emergencyContact_"+i).className = "emergency_contact";
			}
			document.getElementById(emtRef).className = (document.getElementById('emergencyContactFlag').value==rid) ? "emergency_contact" : "emergency_contact_selected";
			document.getElementById('emergencyContactFlag').value = (document.getElementById('emergencyContactFlag').value==rid) ? "" : rid;
		}

	}

	function KISClassTimeSlotOnChange(val){
		if(val=='AM'){
			$('#KISClassType_AM').removeAttr('disabled').show();
			$('#KISClassType_PM').attr('disabled', true).hide();
		} else if(val=='PM'){
			$('#KISClassType_PM').removeAttr('disabled').show();
			$('#KISClassType_AM').attr('disabled', true).hide();
		}
	}
		
	</script>
	<form name="form1" method="post" action="new_update.php" onSubmit="return goSubmit()">
	<table width="100%" border="0" cellpadding="4" cellspacing="0">
	    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
	    <tr>
		    <td>
	                      <div class="table_board">
							<table class="form_table">
							  <tr>
							    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['ClassGroupInfo']?> -</em></td>
							  </tr>
							  <tr>
							    <td class="formfieldtitle" width="20%"><?=$i_adminmenu_group?></td>
							    <td >
								    <table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><?=$lo->displayUserGroups(); ?></td>
										</tr>
								    </table>
							    </td>
							  </tr>
							  <tr>
							    <td class="formfieldtitle" width="20%"><?=$Lang['Header']['Menu']['Role']?></td>
							    <td >
								    <table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><?= $lrole->displayRoleSelection(); ?></td>
										</tr>
								    </table>
							    </td>
							  </tr>
						      <?php if($plugin['SDAS_module']['KISMode']){
						      	include_once($PATH_WRT_ROOT.'/home/cees_kis/lang/'.$intranet_session_language.'.php');
						      	?>
						      <tr>
						      	<td class="formfieldtitle" width="20%"><?=$Lang['CEES']['MonthlyReport']['JSLang']['KISClassType']?></td>
							    <td>
								    <select name="KISCLassType" id="KISClassTimeSlot" onChange="KISClassTimeSlotOnChange(this.value)">
								    	<? foreach($Lang['CEES']['MonthlyReport']['JSLang']['TimeSlot'] as $k => $v){?><option value='<?=$k?>'><?=$v?></option><?}?>
								    </select>
								    <select name="KISCLassType" id="KISClassType_AM">
								    	<? for($i=1;$i<=4;$i++){?><option value='<?=$i?>'><?=$Lang['CEES']['MonthlyReport']['JSLang']['KISClassTypeAry'][$i]?></option><?}?>
								    	<option value='<?=9?>'><?=$Lang['CEES']['MonthlyReport']['JSLang']['KISClassTypeAry'][9]?></option>
								    </select>
								    <select name="KISCLassType" id="KISClassType_PM" style='display:none' disabled>
								    	<? for($i=5;$i<=5;$i++){?><option value='<?=$i?>'><?=$Lang['CEES']['MonthlyReport']['JSLang']['KISClassTypeAry'][$i]?></option><?}?>
								    	<option value='<?=9?>'><?=$Lang['CEES']['MonthlyReport']['JSLang']['KISClassTypeAry'][9]?></option>
								    </select>
							    </td>
						      </tr>
							  <?php }?>
							</table>
							<p class="spacer"></p>
	                      </div>
		    
		    </td>
	    </tr>
	    
	    <tr><td height="40"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['GuardianInfo']?> -</em></td></tr>
	</table>
	
		<table class="common_table_list common_table_list_form" width="90%">
		<col  class="num_check" />
		<tr>
			<td colspan="4">
				<?=$table_content?>
			</td>
		</tr>
		<tr>
			<td class="num_check"><div class="table_row_tool row_content_tool"><a href="javascript:addGuardian()" class="add_dim" title="Add Guardian"></a></div></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</thead>
	</table>
				<input type="hidden" name="tsize" id="tsize" value="0">
				<input type="hidden" name="newids" id="newids" value="">
				<input type="hidden" name="totalRow" id="totalRow" value="0">
				<input type="hidden" name="mainGuardianFlag" id="mainGuardianFlag" value="">
				<input type="hidden" name="smsFlag" id="smsFlag" value="">
				<input type="hidden" name="emergencyContactFlag" id="emergencyContactFlag" value="">
	<p class="spacer"></p>
    <div class="edit_bottom">									
        <?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "goURL('new.php')")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom')")?>
    </div>			
	<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
	<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
	<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
	<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
	<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
	<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
	<input type="hidden" name="email" id="email" value="<?=$email?>">
	<input type="hidden" name="status" id="status" value="<?=$status?>">
	<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
	<?php if($sys_custom['SupplementarySmartCard']){ ?>
	<input type="hidden" name="smartcardid2" id="smartcardid2" value="<?=$smartcardid2?>">
	<input type="hidden" name="smartcardid3" id="smartcardid3" value="<?=$smartcardid3?>">
	<input type="hidden" name="smartcardid4" id="smartcardid4" value="<?=$smartcardid4?>">
	<?php } ?>
	<?php if($plugin['medical']){ ?>	
	<input type="hidden" name="stayOverNight" id="stayOverNight" value="<?=$stayOverNight?>">
	<?php } ?>
	<input type="hidden" name="strn" id="strn" value="<?=$strn?>">
	<input type="hidden" name="WebSAMSRegNo" id="WebSAMSRegNo" value="<?=$WebSAMSRegNo?>">
	<input type="hidden" name="HKJApplNo" id="HKJApplNo" value="<?=$HKJApplNo?>">
	<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
	<input type="hidden" name="engname" id="engname" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>">
	<input type="hidden" name="chiname" id="chiname" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>">
	<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
	<input type="hidden" name="dob" id="dob" value="<?=$dob?>">
	<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
	<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
	<input type="hidden" name="address" id="address" value="<?=$address?>">
	<input type="hidden" name="country" id="country" value="<?=$country?>">
	<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
	<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
	<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
	<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
	<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
	<input type="hidden" name="error" id="error" value="<?=$errorList?>">
	<input type="hidden" name="Nationality" id="Nationality" value="<?=$Nationality?>">
	<input type="hidden" name="PlaceOfBirth" id="PlaceOfBirth" value="<?=$PlaceOfBirth?>">
	<input type="hidden" name="AdmissionDate" id="AdmissionDate" value="<?=$AdmissionDate?>">
	<input type="hidden" name="Quota" id="Quota" value="<?=$Quota?>">
	<input type="hidden" name="EmailStatus" id="EmailStatus" value="<?=$EmailStatus?>">
	<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
	
	<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){ ?>
	    <input type="hidden" name="PrimarySchoolCode" id="PrimarySchoolCode" value="<?=$PrimarySchoolCode?>" />  
	<? }?>
	
	<?php if($sys_custom['StudentAccountAdditionalFields']){  
		if($magic_quotes_enabled){
			$PrimarySchool = stripslashes($PrimarySchool);
			$University = stripslashes($University);
			$Programme = stripslashes($Programme);
		}
	?>
		<input type="hidden" name="NonChineseSpeaking" id="NonChineseSpeaking" value="<?=$NonChineseSpeaking?>" />
		<input type="hidden" name="SpecialEducationNeeds" id="SpecialEducationNeeds" value="<?=$SpecialEducationNeeds?>" />
		<input type="hidden" name="PrimarySchool" id="PrimarySchool" value="<?=intranet_htmlspecialchars($PrimarySchool)?>" />
		<input type="hidden" name="University" id="University" value="<?=intranet_htmlspecialchars($University)?>" />
		<input type="hidden" name="Programme" id="Programme" value="<?=intranet_htmlspecialchars($Programme)?>" />
	<?php } ?>
	<?php if($sys_custom['BIBA_AccountMgmtCust']){ ?>
	<input type="hidden" name="HouseholdRegister" id="HouseholdRegister" value="<?=intranet_htmlspecialchars($HouseholdRegister)?>" />
	<?php } ?>
	<? if($sys_custom['StudentMgmt']['BlissWisdom']) {?>
	<input type="hidden" name="Occupation" id="Occupation" value="<?=$Occupation?>">
	<input type="hidden" name="Company" id="Company" value="<?=$Company?>">
	<input type="hidden" name="PassportNo" id="PassportNo" value="<?=$PassportNo?>">
	<input type="hidden" name="Passport_ValidDate" id="Passport_ValidDate" value="<?=$Passport_ValidDate?>">
	<input type="hidden" name="HowToKnowBlissWisdom" id="HowToKnowBlissWisdom" value="<?=$HowToKnowBlissWisdom?>">
	<?}?>
	<?php if($plugin['radius_server']){ ?>
	<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
	<?php } ?>
	<?=$ExtraInfoInput?>
		
	<?php 
	if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){ 
		if(!empty($LangPriority)){
		    foreach($LangPriority as $priority){
		        echo '<input type="hidden" name="LangPriority[]" value="'.$priority.'">';
		    }
		}
        echo '<input type="hidden" name="OthersPriority" value="'.$OthersPriority.'">';
        echo '<input type="hidden" name="OthersPriority_details" value="'.$OthersPriority_details.'">';
        echo '<input type="hidden" name="isNCS" value="'.$isNCS.'">';
        echo '<input type="hidden" name="NGO" value="'.$NGO.'">';
	} 
	?>
	</form>
	<script language="javascript">
		//addGuardian();
	</script>
<?
	intranet_closedb();
	$linterface->LAYOUT_STOP();
} 
?>