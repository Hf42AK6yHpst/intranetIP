<?php
// Modifng by : 

###############################################
#	Date:	2019-05-13 	Henry
#			Security fix: SQL without quote
#
#	Date:	2014-09-05 	Carlos
#			Fix the condition of eLibrary unreturned books 
#
#	Date:	2014-03-31	Carlos
#			Added pre-archive checking of eLibrary overdue payments
#			Also check on unreturned books
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
#	Date:	2011-11-01	YatWoon
#			Improved: Display black in balance=0, otherwise in red
#
#	Date:	2011-09-27	YatWoon
#			- updated to IP25 layout standard
#			- add "year of left" field for user input
#
###############################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
// include_once($PATH_WRT_ROOT."includes/libwebmail.php");
// include_once($PATH_WRT_ROOT."includes/libform.php");
// include_once($PATH_WRT_ROOT."includes/libftp.php");
// include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
	exit;
}

intranet_auth();
intranet_opendb();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$linterface = new interface_html();
$laccount = new libaccountmgmt();

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";
//$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList']);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",1);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php",0);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goBack()");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['DeleteUser'], "");

$linterface->LAYOUT_START();
	

$list = implode(",", $user_id);
$lu = new libuser();

?>

<script language="javascript">
<!--
function goBack() {
	document.form1.action="index.php";
	document.form1.submit();	
}

function goDelete()
{
	var obj = document.form1;
	var error_no = 0;
	var focus_field = "";
	
	// check year of left
	if(!check_positive_int_30(obj.YearOfLeft, "<?=$Lang['AccountMgmt']['YearOfLeft_Error1']?>", "","","div_YearOfLeft"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "YearOfLeft";
	}
	
	if(obj.YearOfLeft.value<1900 || obj.YearOfLeft.value> <?=date("Y")+1?>)
	{
		document.getElementById('div_YearOfLeft').innerHTML = '<font color="red"><?=$Lang['AccountMgmt']['YearOfLeft_Error2a']?><?=date("Y")+1?><?=$Lang['AccountMgmt']['YearOfLeft_Error2b']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "YearOfLeft";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		if(confirm("<?=$Lang['General']['JS_warning']['ConfirmDelete']?>")) 
		{
			document.form1.action="remove_update.php";
			document.form1.submit();	
		}
		
	}
	

}
-->
</script>
	
<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" method="post">
<?
#### Check any account has non-zero balance
$sql="SELECT COUNT(*) FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.StudentID = b.StudentID) WHERE a.StudentID IN ('".implode("','", $user_id)."') AND (a.Balance >0 OR b.RecordStatus=0)";
$temp = $lu->returnVector($sql);

if($plugin['library_management_system']){
	include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
	include_once($PATH_WRT_ROOT."includes/libpayment.php");
	
	$lpayment = new libpayment();
	$liblms = new liblms();
	
	$sql = "SELECT 
				lu.ClassName, 
				lu.ClassNumber, 
				lu.EnglishName, 
				lu.ChineseName,
				(lu.balance*-1) as Fine,
				lu.UserID,
				lu.UserLogin,
				ol.OverDueLogID,
				if(ol.PaymentReceived is null, ol.Payment, ol.Payment-ol.PaymentReceived) as BookFine, 
				if(ol.DaysCount is NULL, 'LOST', ol.DaysCount) As LostOrDays,
				b.BookTitle,
				ifnull(bu.ACNO, bu.ACNO_BAK) AS ACNO,
				IF(ol.OverDueLogID IS NULL,bl.DueDate,ol.DateCreated) as FineDate 
			FROM LIBMS_BORROW_LOG as bl 
			LEFT JOIN LIBMS_OVERDUE_LOG as ol ON bl.BorrowLogID=ol.BorrowLogID 
			INNER JOIN LIBMS_BOOK as b ON b.BookID=bl.BookID 
			INNER JOIN LIBMS_USER as lu ON lu.UserID=bl.UserID 
			LEFT JOIN LIBMS_BOOK_UNIQUE as bu ON bu.UniqueID=bl.UniqueID 
			WHERE bl.UserID IN ('".implode("','",$user_id)."') 
				 AND ((bl.RecordStatus='BORROWED') OR (ol.RecordStatus='OUTSTANDING' AND ol.IsWaived=0)) 	
			ORDER BY lu.ClassName,lu.ClassNumber+0";
	
	$elibOverdueRecords = $liblms->returnResultSet($sql);
	
	$elibOverdueRecordCount = count($elibOverdueRecords);
	if($elibOverdueRecordCount > 0)
	{
		$elibOverdueContent = '<fieldset class="instruction_warning_box_v30">
									<legend>'.$Lang['General']['Warning'].'</legend>
									'.$Lang['AccountMgmt']['UnsettledLibraryOverduePaymentsWarning'].'
								</fieldset>';
		
		$elibOverdueContent .= '<table class="common_table_list">
								<thead>
									<tr>
										<th style="width:10%;">'.$Lang['libms']['bookmanagement']['ClassName'].'</th>
										<th style="width:5%;">'.$Lang['libms']['bookmanagement']['ClassNumber'].'</th>
										<th style="width:10%;">'.$Lang["libms"]["book"]["user_name"].'</th>
										<th style="width:10%;">'.$i_UserLogin.'</th>
										<th style="width:30%;">'.$Lang['libms']['bookmanagement']['bookTitle'].'</th>
										<th style="width:10%;">'.$Lang['libms']['CirculationManagement']['overdue_type'] .'</th>
										<th style="width:10%;">'.$Lang["libms"]["CirculationManagement"]["payment"].'</th>
										<th style="width:15%;">'.$Lang["libms"]["CirculationManagement"]["FineDate"].'</th>
									</tr>
								</thead>
								<tbody>';
		$elibOverdueRecordRowSpan = array();
		for($i=0;$i<$elibOverdueRecordCount;$i++) {
			$elibOverdueRecordRowSpan[$elibOverdueRecords[$i]['UserID']] += 1;
		}
				
		for($i=0;$i<$elibOverdueRecordCount;$i++) {
	
			$display_name = Get_Lang_Selection($elibOverdueRecords[$i]['ChineseName'],$elibOverdueRecords[$i]['EnglishName']);
			if($elibOverdueRecords[$i]['OverDueLogID'] != ''){
				$overdue_type = $elibOverdueRecords[$i]['LostOrDays'] == 'LOST'? $Lang["libms"]["book_status"]["LOST"] : str_replace('!','',$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"]).'&nbsp;'.$elibOverdueRecords[$i]['LostOrDays'].'&nbsp;'.$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"];
				$fine = $lpayment->getWebDisplayAmountFormat($elibOverdueRecords[$i]['BookFine']);
			}else{
				$overdue_type = $Lang['AccountMgmt']['BookUnreturned'];
				$fine = $Lang['General']['EmptySymbol'];
			}
			$ACNO = $elibOverdueRecords[$i]['ACNO'];
			
			$elibOverdueContent .= '<tr>';
			if($elibOverdueRecords[$i]['UserID'] != $elibOverdueRecords[$i-1]['UserID']){
				$elibOverdueContent.='<td rowspan="'.$elibOverdueRecordRowSpan[$elibOverdueRecords[$i]['UserID']].'">'.Get_String_Display($elibOverdueRecords[$i]['ClassName']).'</td>';
				$elibOverdueContent.='<td rowspan="'.$elibOverdueRecordRowSpan[$elibOverdueRecords[$i]['UserID']].'">'.Get_String_Display($elibOverdueRecords[$i]['ClassNumber']).'</td>';
				$elibOverdueContent.='<td rowspan="'.$elibOverdueRecordRowSpan[$elibOverdueRecords[$i]['UserID']].'">'.$display_name.'</td>';
				$elibOverdueContent.='<td rowspan="'.$elibOverdueRecordRowSpan[$elibOverdueRecords[$i]['UserID']].'">'.$elibOverdueRecords[$i]['UserLogin'].'</td>';
			}
				$elibOverdueContent.='<td>'.$elibOverdueRecords[$i]['BookTitle'].($ACNO!=''?' ['.$ACNO.']':'').'</td>';
				$elibOverdueContent.='<td>'.$overdue_type.'</td>';
				$elibOverdueContent.='<td>'.$fine.'</td>';
				$elibOverdueContent.='<td>'.$elibOverdueRecords[$i]['FineDate'].'</td>';
			$elibOverdueContent .= '</tr>'."\n";
		}
		
		$elibOverdueContent .= '</tbody>';
		$elibOverdueContent .= '</table>'."\n";		
	}
}

if (!$isKIS && ($temp[0]>0 || $elibOverdueRecordCount>0))
{
	if($temp[0]>0)
	{
	    $langfield = getNameFieldByLang("b.");
	    $sql = "SELECT 
	    			b.UserLogin, 
	    			$langfield, 
	    			b.ClassName, 
	    			b.ClassNumber, 
	    			IF(a.Balance=0,CONCAT('$', FORMAT(a.Balance, 2)),CONCAT('<font color=red>$', FORMAT(a.Balance, 2),'</font>')) AS Amount,
	    			c.PaymentID,
	    			d.Name,
	    			CONCAT('$',FORMAT(c.Amount,2)), 
					c.RecordStatus as paid
				FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID LEFT OUTER JOIN 
					PAYMENT_PAYMENT_ITEMSTUDENT as c ON (a.StudentID = c.StudentID) LEFT OUTER JOIN 
					PAYMENT_PAYMENT_ITEM as d ON (c.ItemID = d.ItemID)
				WHERE 
					a.StudentID IN ('".implode("','", $user_id)."') AND 
					(a.Balance >0 OR c.RecordStatus=0) 
				ORDER BY 
					a.StudentID";
	    $result = $lu->returnArray($sql,8);
	    
	    $m = 0;
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance, $payment_id, $item_name, $unpaid_amount, $paid) = $result[$i];
	         if($pre_student!=$t_userLogin) $m = 0;
		     $result2[$t_userLogin]['name'] = $t_name;
		     $result2[$t_userLogin]['class'] = $t_className;
		     $result2[$t_userLogin]['classnumber'] = $t_classNum;
		     $result2[$t_userLogin]['balance'] = $t_balance;
		     $delim = "";
		     if($payment_id != ""){
		     	if($paid==0 && $paid!="") {
		     		$result2[$t_userLogin]['items'][$m] = $delim."<font color=red>- ".$item_name." (".$unpaid_amount.")</font>";
		     		$m++;
		     	} 
		     	$delim = "<br>";
		     }
		     $pre_student = $t_userLogin;
	    }
	    $content = "";
	
	    $j=0;
	    foreach($result2 as $user_login => $values){
	    	$items = "";
		    $name = $values['name'];
		    $class = $values['class'];
		    $classnumber = $values['classnumber'];
		    $balance = $values['balance'];
		    
		    if(is_array($values['items'])){
			    $items = "<table border=0 class='inside_form_table' cellpadding='0' cellspacing='0'>";
			  	for($x=0; $x<sizeof($values['items']); $x++){
		    		$items .= "<tr><td>".$values['items'][$x]."</td></tr>";
		    	}
		    	$items .= "</table>";
			} else {
				$items = "---";	
			}
			if($items=="") $items = "---";
		    
		    $j++;
	// 	    $css =$css = ($j%2 ? "1":"2");
	//         $content .= "<tr class=tablerow$css><td>$user_login</td><td>$name</td><td>$class&nbsp;</td><td>$classnumber</td><td>$balance</td><td>$items</td></tr>\n";
	        
			$content .= "
						<tr>
							<td>$class&nbsp;</td>
							<td>$classnumber</td>
							<td>$name</td>
							<td>$user_login</td>
							<td>$balance</td>
							<td>$items</td>
						</tr>\n";
		}
	}
    ?>
    
    <?php if($temp[0]>0){ ?>
	<fieldset class="instruction_warning_box_v30">
	<legend><?=$Lang['General']['Warning']?></legend>
	<?=$i_UserRemoveStop_PaymentBalanceNonZero?>
	</fieldset>
 
	<div class="table_board">
		<table class="common_table_list">
		<thead>
			<tr>
				<th><?=$i_UserClassName?></th>
				<th><?=$i_UserClassNumber?></th>
				<th><?=$i_UserName?></th>
				<th><?=$i_UserLogin?></th>
				<th><?=$i_Payment_Field_Balance?></th>
				<th><?=$Lang['ePayment']['UnpaidItem'] ." ($i_Payment_Field_Amount)"?></th> 
			</tr>
		</thead>
		<?=$content?>
		</table>
	</div>
	
	<? } ?>
	
	<?php 
	if($elibOverdueRecordCount>0){
		echo $elibOverdueContent;
	}
	?>
	
 	<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()")?>
	</div>
    
	<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
	<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
	<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
	
    <?
}
else
{
?>
	<br>
	<?=$linterface->GET_WARNING_TABLE('', '', '', $Lang['AccountMgmt']['DeleteStudentWarning'])?>
	<fieldset class="instruction_box_v30">
	<legend><?=$Lang['General']['Remark']?></legend>
	<?=$Lang['AccountMgmt']['ClickContinueToProceed']?>
	<? if($special_feature['alumni']) { ?>
		 <br><br><?=$Lang['AccountMgmt']['ToBeAlumni']?>
	<? } ?>
	</fieldset>


	<?if ($special_feature['alumni'])
	{
	    if ($alumni_GroupID != "")
	    {
	        $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE GroupID = '".$alumni_GroupID."'";
	        $temp = $laccount->returnVector($sql);
	        if ($temp[0]=="")
	        {
	            $alumni_warning = $i_StudentPromotion_AutoAlumni_NoGroupExist;
	        }
	    }
	    else
	    {
	        $alumni_warning = $i_StudentPromotion_AutoAlumni_NoGroupSet;
	    }
	} ?>
	
	<? if($alumni_warning) {?>
		<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Warning']?></legend>
			<?=$alumni_warning?>
		</fieldset>
	<? } ?>	

<div class="div_form_table">
	<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['AccountMgmt']['NoOfSelectedStudents']?></td>
		<td><?=sizeof($user_id)?></td>
	</tr>
	<tr>
		<td class="field_title"><span class='tabletextrequire'>*</span> <?=$Lang['AccountMgmt']['YearOfLeft']?></td>
		<td><input name="YearOfLeft" type="text" id="YearOfLeft" class="textboxnum" value="<?=date("Y");?>" maxlength="4"/> <span id="div_YearOfLeft"></span></td>
	</tr>
	</table>
	
	<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "goDelete()")?> 
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()")?>
	<p class="spacer"></p>
	</div>
</div>
<input type="hidden" name="list" value="<?=$list?>">
<? } ?>

</form>
<? 
intranet_closedb();
$linterface->LAYOUT_STOP();
    
?>