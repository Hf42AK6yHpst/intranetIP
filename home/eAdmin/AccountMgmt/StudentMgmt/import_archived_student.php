<?php
// Editing by 
/*
 * 2016-11-30 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(isset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'])){
	$xmsg = $_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'];
	unset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT']);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();



$csv_format = '';
/*
$Lang['AccountMgmt']['ArchivedStudentImportFields'] = array();
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('UserLogin','User Login ID','<span style="color:red">*</span>','');
if($special_feature['ava_strn']){
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('STRN','STRN Number','','');
}
if($_SESSION["platform"]!="KIS"){
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('WebSAMSRegNo','WebSAMS Registration Number','','<span class="tabletextremark"> (Please add "#" before WebSAMSRegNo)</span>');
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('HKJApplNo','HK JUPAS Application Number','','');
}
if($special_feature['ava_hkid']){
	$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('HKID','Hong Kong Identity Card Number','','');
}
$Lang['AccountMgmt']['ArchivedStudentImportFields'][] = array('DOB','Date of Birth','','<span class="tabletextremark"> (Date format is YYYY-MM-DD or DD/MM/YYYY.)</span>');
*/
for($i=0;$i<count($Lang['AccountMgmt']['ArchivedStudentImportFields']);$i++){
	$csv_format .= $Lang['General']['ImportArr']['Column']." ".($i+1)." : ".$Lang['AccountMgmt']['ArchivedStudentImportFields'][$i][2].$Lang['AccountMgmt']['ArchivedStudentImportFields'][$i][1].$Lang['AccountMgmt']['ArchivedStudentImportFields'][$i][3];
	$csv_format .= '<br />';
}


$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],"");

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php",1);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($xmsg);
?>
<form method="POST" name="form1" id="form1" action="import_archived_student_confirm.php" enctype="multipart/form-data" onsubmit="return true;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><a class="tablelink" href="get_archived_student_import_sample.php"><?=$Lang['General']['ClickHereToDownloadSample']?></a></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['ImportArr']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>&nbsp;
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='archived_student.php'","back_btn"," class='formbutton' ")?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>
</form>
<br><br>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>