<?php
$PATH_WRT_ROOT = "../../../../";
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libemail.php");
include($PATH_WRT_ROOT."includes/libsendmail.php");
include($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."lang/email.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$lwebmail = new libwebmail();

$mailSubject = stripslashes($subject);
$mailBody = stripslashes($message).email_footer();
/*
$UserEmail = array_unique($UserEmail);
$UserEmail = array_values($UserEmail);

for($i=0;$i<sizeof($UserEmail);$i++)
	$mailTo .= ($i==0) ? $UserEmail[$i] : ",".$UserEmail[$i];
*/
$receiver = $UserEmail;
	
$lu = new libsendmail();
//$lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);

$exmail_success = $lwebmail->sendModuleMail($receiver, $mailSubject, $mailBody, 1);

intranet_closedb();


header("Location: $comeFrom?xmsg=EmailSent");
?>