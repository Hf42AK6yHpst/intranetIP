<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$linterface 	= new interface_html();
$lclass = new libclass();
$limport = new libimporttext();
$lo = new libfilesystem();

### CSV Checking
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: student_import.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$csv_header = array_shift($data);

$file_format = array("Class Name","Class Number","Event Code");
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($csv_header[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	intranet_closedb();
	header("location:  student_import.php?xmsg=import_header_failed");
	exit();
}

if(empty($data))
{
	intranet_closedb();
	header("location: student_import.php?xmsg=import_failed");
	exit();
	
}
$StudentIDAry=Array();
if($importTarget == "form")
{
	foreach($FormSelection as $YearID)
	{
		$ClassInfoAry = $lclass->returnClassListByLevel($YearID);
		foreach ($ClassInfoAry as $ClassInfo)
		{
			$ClassNameAry[$ClassInfo["YearClassID"]] = $ClassInfo["ClassTitleEN"];
		}
	}	
}
else
{
	foreach($ClassSelection as $ClassID)
	{
		$ClassNameAry[$ClassID] = $lclass->getClassName($ClassID);
	}
}

$ErrLog = Array(); // To record all err of each record
$EnrolList = Array(); //To record student enrolment (EventCode)
$EnrolCount = Array(); //To count overall number of enrolments of each student 
foreach ($data as $key => $rec)
{
	list($ClassName,$ClassNumber,$EventCode)=$rec;
	$ClassID = $lclass->getClassID($ClassName);
	
	if(!in_array($ClassName,$ClassNameAry))
	{
		$ErrLog[$key][] = $Lang['eSports']['NotInTargetClasses'];
	}	
	
	$StudentID = $lsports->returnStudentIDByClassNameAndClassNumber($ClassName,$ClassNumber);
	if(empty($StudentID))
	{
		$ErrLog[$key][] = $Lang['eSports']['StudentNotExist'];			
	}
	else
	{
		$EventGroupInfo = $lsports->returnEventGroupInfoByEventCode($EventCode);
		list($EventID,$GroupID,$EventGroupID,$CountQuota) = $EventGroupInfo;
		
		$AgeGroupInfo = $lsports->returnStudentAgeGroup($StudentID);
		
		if(empty($EventGroupInfo))
			$ErrLog[$key][]=$Lang['eSports']['NoSuchEvent'];
		else
		if(!($GroupID == $AgeGroupInfo['AgeGroupID'] || $GroupID=='B' || $GroupID == $AgeGroupInfo['Gender']))
		{
			####################
			#	Case GroupID
			#	M : Boys Open
			#	F : Girls Open
			#	B : Mixed Open
			#####################
			$ErrLog[$key][]=$Lang["eSports"]["WrongAgeGroup"];
		}
		else
		{
			$EnrolMax[1] = $AgeGroupInfo['EnrolMaxTrack'];
			$EnrolMax[2] = $AgeGroupInfo['EnrolMaxField'];
			$EnrolMax["total"] = $AgeGroupInfo['EnrolMaxTotal'];
			
			$EventInfo = $lsports->returnEventInfo($EventID);
			$EventType = $EventInfo['EventType'];
			
			if($EnrolCount[$StudentID]["total"]==$EnrolMax["total"]&&$CountQuota) // if exceed maximun total enrolment quota
			{
				$ErrLog[$key][] = $Lang['eSports']['ExceedTotalQuota'];
			}
			else
			{
				if($EnrolCount[$StudentID][$EventType]==$EnrolMax[$EventType]&&$CountQuota) // if exceed maximun EventType enrolment quota
				{
					$ErrLog[$key][] = $Lang['eSports']['ExceedQuota'][$EventType];
				}
				else
				{
					if(!$EnrolList[$StudentID]||!in_array($EventGroupID,$EnrolList[$StudentID]))
					{
						$EnrolList[$StudentID][]=$EventGroupID;
						$ImportData[$key]=Array($StudentID,$EventGroupID);
						if($CountQuota)
						{
							$EnrolCount[$StudentID][$EventType]++;
							$EnrolCount[$StudentID]["total"]++;
						}
					}
					else
						$ErrLog[$key][] = $Lang['eSports']['DuplicateRecord'];
					
				}					
			}
						
		}			
	}

	#Build Table Content
	$rowcss = " class='".($key%2==0? "tablebluerow2":"tablebluerow1")."' ";
	
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext'>$ClassName</td>";
	$Confirmtable .= "		<td class='tabletext'>$ClassNumber</td>";
	$Confirmtable .= "		<td class='tabletext'>$EventCode</td>";
	
	if($ErrLog[$key])
	{
		$ErrMsg = count($ErrLog[$key])>1?implode("<br>",$ErrLog[$key]):$ErrLog[$key][0];
		$Confirmtable .= "		<td class='red'>".$Lang['eSports']['Error'].":<br>$ErrMsg</td>"; 
	}
	else
	{
		$Confirmtable .= "		<td>".$Lang['eSports']['NoError']."</td>";
	}
	$Confirmtable .= "	</tr>";	
		
}

## Post Data 
foreach($data as $key => $eachdata)
{
	$postValue .= "<input type='hidden' name='ImportRawData[$key]' value='".implode(":_Separator_:",(array)$eachdata)."'>\n";
	$postValue .= "<input type='hidden' name='ImportData[$key]' value='".implode(":_Separator_:",(array)$ImportData[$key])."'>\n";	
}
$postValue .= "<input type='hidden' name='ImportTargetClasses' value='".implode(":_Separator_:",(array)$ClassNameAry)."'>\n ";

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn = $BackBtn."&nbsp;";
$Btn .= empty($ErrLog)?$NextBtn:"";

### Error Msg
$xmsg = (!empty($ErrLog))?$Lang['eSports']['warnNoRecordInserted']:"";

### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Arrangement_EnrolmentUpdate ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$CurrentPage	= "PageArrangement_EnrolmentUpdate";

$TAGS_OBJ[] = array($TitleTitle, "", 0);    
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<form name="frm1" method="POST" action="import.php" onsubmit="return confirm('<?=$Lang['eSports']['warnDeleteEnrolRecord']?>')">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="5">
				<table width="30%">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($ErrLog)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($ErrLog)?"red":""?>'><?=count($ErrLog)?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<?if(!empty($ErrLog)){?>
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%">Row#</td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['ClassTitle']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['ClassNumber']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['EventCode']?></td>
				<td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
<?=$postValue?>
</form>

<?
$linterface->LAYOUT_STOP();
?>
