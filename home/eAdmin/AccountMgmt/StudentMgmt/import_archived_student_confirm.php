<?php
// Editing by 
/* 2018-12-31 Isaac: Added $sys_custom['AccountMgmt']['StudentDOBRequired'] check.
 * 2017-03-23 Villa: #A113222 Eliminate the unwanted data (ClassName,ClassNumber,YearOfLeft)
 * 2016-11-30 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(isset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'])){
	$xmsg = $_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'];
	unset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT']);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

$lf = new libfilesystem();
$laccount = new libaccountmgmt();
$linterface = new interface_html();
$limport = new libimporttext();

### CSV Checking
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lf->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	header("location: import_archived_student.php");
	exit();
}
$data = $limport->GET_IMPORT_TXT($_FILES['userfile']['tmp_name']);
$csv_header = array_shift($data);

###
$count = 0;
$eliminate_array = array('ClassName','ClassNumber','YearOfLeft');
foreach ($csv_header as $col=>$_csv_header){
	if(in_array($_csv_header, $eliminate_array)){
		//Do nothing
		$eliminate_col[] = $col;
	}else{
		$header_temp_arr[$count] =  $_csv_header;
		$count++;
	}
}
foreach ($data as $NoOfData => $_data){
	$count = 0;
	foreach ($_data as $row => $__data){
		if(in_array($row, $eliminate_col)){
			//Do Nothing
		}else{
			$data_temp_arr[$NoOfData][$count] = $__data;
			$count++;
		}
	}
}
$csv_header = $header_temp_arr;
$data = $data_temp_arr;

$data_size = count($data);

$header = array("UserLogin");
if($special_feature['ava_strn'])
{
	$header = array_merge($header, array("STRN"));
}
if(!$isKIS) {
	$header = array_merge($header,array("WebSAMSRegNo","HKJApplNo"));
}
$header = array_merge($header, array("DOB"));
if($special_feature['ava_hkid'])
{
	$header = array_merge($header, array("HKID"));
}

# check csv header
$format_wrong = false;
for($i=0; $i<sizeof($header); $i++)
{
	if ($csv_header[$i]!=$header[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	intranet_closedb();
	$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['WrongCSVHeader'];
	header("location:  import_archived_student.php");
	exit();
}
if(empty($data))
{
	intranet_closedb();
	$_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['CSVFileNoData'];
	header("location: import_archived_student.php");
	exit();
}
//debug_pr($data);
$sql = "CREATE TABLE IF NOT EXISTS INTRANET_ARCHIVE_USER_TEMP_IMPORT (
			UserLogin varchar(20) NOT NULL,
			STRN varchar(9),
			WebSAMSRegNo varchar(100),
			HKJApplNo int(10),
			DOB date,
			HKID varchar(10),
			ImportSession varchar(300),
			ImportTime datetime,
			INDEX IdxUserLogin(UserLogin),
			INDEX IdxSession(ImportSession),
			INDEX IdxImportTime(ImportTime) 
		)Engine=InnoDB DEFAULT CHARSET=UTF8";
$laccount->db_db_query($sql);

$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");

$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
$sql = "DELETE FROM INTRANET_ARCHIVE_USER_TEMP_IMPORT WHERE ImportSession='$cur_sessionid' OR ImportTime<'$threshold_date'";
$laccount->db_db_query($sql);
//debug_pr($sql);
$sql = "SELECT UserLogin FROM INTRANET_ARCHIVE_USER WHERE RecordType='".USERTYPE_STUDENT."'";
$existing_users = $laccount->returnVector($sql);

$error_msg = array();
$addedAry = array();
$insert_sql = "INSERT INTO INTRANET_ARCHIVE_USER_TEMP_IMPORT (".implode(",",$header).",ImportSession,ImportTime) VALUES ";
$values = array();
for($i=0;$i<$data_size;$i++)
{
	$insert_val = '(';
	
	$col = 0;
	$t_userlogin = trim($data[$i][$col]);
	if($t_userlogin == '' || !in_array($t_userlogin,$existing_users)){
		if(!isset($error_msg[$i+2])){
			$error_msg[$i+2] = array($t_userlogin, array());
		}
		$error_msg[$i+2][1][] = $Lang['AccountMgmt']['ErrorMsg']['CannotFindStudent'];
	}
	$insert_val .= "'".$laccount->Get_Safe_Sql_Query($t_userlogin)."'";
	if($special_feature['ava_strn']){
		$col += 1;
		$t_strn = trim($data[$i][$col]);
		$insert_val .= ",'".$laccount->Get_Safe_Sql_Query($t_strn)."'";
		if($t_userlogin !='' && $t_strn != ''){
			$sql = "SELECT UserLogin FROM INTRANET_USER WHERE STRN='$t_strn' AND UserLogin!='$t_userlogin'";
			$temp = $laccount->returnVector($sql);
			
			$sql = "SELECT UserLogin FROM INTRANET_ARCHIVE_USER WHERE STRN='$t_strn' AND UserLogin!='$t_userlogin'";
			$temp2 = $laccount->returnVector($sql);
			if(sizeof($temp)!=0 || sizeof($temp2)!=0){
				if(!isset($error_msg[$i+2])){
					$error_msg[$i+2] = array($t_userlogin, array());
				}
				$error_msg[$i+2][1][] = 'STRN'.$Lang['AccountMgmt']['UsedByOtherUser'];
			}
		}
	}
	if(!$isKIS) {
		$col += 1;
		$t_websams_number = trim($data[$i][$col]);
		$insert_val .= ",'".$laccount->Get_Safe_Sql_Query($t_websams_number)."'";
		if($t_websams_number !='' && substr(trim($t_websams_number),0,1)!="#"){
			$t_websams_number = "#".$t_websams_number;
		}
		if($t_userlogin !='' && $t_websams_number != '')
		{
			$sql = "SELECT UserLogin FROM INTRANET_USER WHERE WebSAMSRegNo='$t_websams_number' AND RecordType=".TYPE_STUDENT." AND UserLogin!='$t_userlogin'";
			$temp = $laccount->returnVector($sql);
			
			$sql = "SELECT UserLogin FROM INTRANET_ARCHIVE_USER WHERE WebSAMSRegNo='$t_websams_number' AND RecordType=".TYPE_STUDENT." AND UserLogin!='$t_userlogin'";
			$temp2 = $laccount->returnVector($sql);
			if(sizeof($temp)!=0 || sizeof($temp2)!=0){
				if(!isset($error_msg[$i+2])){
					$error_msg[$i+2] = array($t_userlogin, array());
				}
				$error_msg[$i+2][1][] = 'WebSAMSRegNo'.$Lang['AccountMgmt']['UsedByOtherUser'];
			}
		}
		
		$col += 1;
		$t_jupas_number = trim($data[$i][$col]);
		$insert_val .= ",'".$laccount->Get_Safe_Sql_Query($t_jupas_number)."'";
		if($t_userlogin !='' && $t_jupas_number!="" && $t_jupas_number != 0){
			$sql = "SELECT UserLogin FROM INTRANET_USER WHERE HKJApplNo='$t_jupas_number' AND RecordType=".TYPE_STUDENT." AND UserLogin!='$t_userlogin'";
			$temp = $laccount->returnVector($sql);
			
			$sql = "SELECT UserLogin FROM INTRANET_ARCHIVE_USER WHERE HKJApplNo='$t_jupas_number' AND RecordType=".TYPE_STUDENT." AND UserLogin!='$t_userlogin'";
			$temp2 = $laccount->returnVector($sql);
			
			if(sizeof($temp)!=0 || sizeof($temp2)!=0){
				if(!isset($error_msg[$i+2])){
					$error_msg[$i+2] = array($t_userlogin, array());
				}
				$error_msg[$i+2][1][] = 'HK JUPAS Application Number'.$Lang['AccountMgmt']['UsedByOtherUser'];
			}
		}
	}
	$col += 1;
	$t_dob = trim($data[$i][$col]);
	if($sys_custom['AccountMgmt']['StudentDOBRequired']){
	    if($t_dob == ''|| empty($t_dob)){
	        if(!isset($error_msg[$i+2])){
	            $error_msg[$i+2] = array($t_userlogin, array());
	        }
	        $error_msg[$i+2][1][] = $Lang['AccountMgmt']['ErrorMsg']['MissedDOB'];
	    }
	}
	if (($t_dob != '') && ($_dob != '0000-00-00')) {
    	if (!checkDateIsValidFor2Format($t_dob)) {
    		if(!isset($error_msg[$i+2])){
				$error_msg[$i+2] = array($t_userlogin, array());
			}
			$error_msg[$i+2][1][] = $Lang['AccountMgmt']['ErrorMsg']['InvalidDateFormat'];
    	}
	}
	$insert_val .= ",'".$laccount->Get_Safe_Sql_Query(getDefaultDateFormat($t_dob))."'";
	if($special_feature['ava_hkid']){
		$col += 1;
		$t_hkid = trim($data[$i][$col]);
		$insert_val .= ",'".$laccount->Get_Safe_Sql_Query($t_hkid)."'";
		
		if($t_userlogin !='' && $t_hkid!=""){
			$sql = "SELECT UserLogin FROM INTRANET_USER WHERE HKID='$t_hkid' AND UserLogin!='$t_userlogin'";
			$temp = $laccount->returnVector($sql);
			
			$sql = "SELECT UserLogin FROM INTRANET_ARCHIVE_USER WHERE HKID='$t_hkid' AND UserLogin!='$t_userlogin'";
			$temp2 = $laccount->returnVector($sql);
			if(sizeof($temp)!=0 || sizeof($temp2)!=0){
				if(!isset($error_msg[$i+2])){
					$error_msg[$i+2] = array($t_userlogin, array());
				}
				$error_msg[$i+2][1][] = 'HKID'.$Lang['AccountMgmt']['UsedByOtherUser'];
			}
		}
	}
	$insert_val .= ",'$cur_sessionid','$cur_time')";
	
	if(in_array($t_userlogin, $addedAry)){
		if(!isset($error_msg[$i+2])){
			$error_msg[$i+2] = array($t_userlogin, array());
		}
		$error_msg[$i+2][1][] = $t_userlogin.$Lang['AccountMgmt']['IsDuplicatedInput'];
	}else{
		$values[] = $insert_val;
	}
	$addedAry[] = $t_userlogin;
	if($i % 10 == 0){
		usleep(1);
	}
}

$chunks = array_chunk($values,100);
for($i=0;$i<count($chunks);$i++){
	$chunk = $chunks[$i];
	$sql = $insert_sql.implode(",",$chunk);
	//debug_pr($sql);
	$laccount->db_db_query($sql);
	usleep(10);
}

if(count($error_msg)>0)
{
	foreach((array)$error_msg as $key => $errorInfo)
	{
		#Build Table Content
		$rowcss = " class=\"".($ctr++%2==0? "tablebluerow2":"tablebluerow1")."\" ";
		
		$Confirmtable .= "	<tr $rowcss>";
		$Confirmtable .= "		<td class=\"tabletext\" valign=\"top\">".($key)."</td>";
		$Confirmtable .= "		<td class=\"tabletext\" valign=\"top\">".($errorInfo[0])."</td>";
		$Confirmtable .= "		<td class=\"tabletext\">";
		foreach($errorInfo[1] as $thisReason){
			$Confirmtable .= $thisReason."<br>";
		}
		$Confirmtable .= "		</td>";
		$Confirmtable .= "	</tr>";
	}
}

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],"");

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php",1);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<form method="POST" name="form1" id="form1" action="import_archived_student_update.php" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="formfieldtitle" width="30%"><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class="tabletext" width="70%"><?=count($data)-count($error_msg)?></td>
			</tr>
			<tr>
				<td class="formfieldtitle" width="30%"><?=$Lang['General']['FailureRecord']?></td>
				<td class="tabletext <?=count($error_msg)?"red":""?>" width="70%"><?=count($error_msg)?></td>
			</tr>
		</table>
		<?if(!empty($error_msg)){?>
        <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="tablebluetop tabletopnolink" width="1%"><?=$Lang['General']['ImportArr']['Row']?></td>
				<td class="tablebluetop tabletopnolink"><?=$i_UserLogin?></td>
				<td class="tablebluetop tabletopnolink" width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
		</table>
		<?}?>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?php if($data_size > 0 && count($error_msg)==0){ ?>
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
					&nbsp;
				<?php } ?>
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_archived_student.php'","back_btn"," class='formbutton' ")?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>
</form>
<br><br>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>