<?php
// Editing by 
/*
 * 2014-03-11 (Carlos): Created for Lassel College
 */
@SET_TIME_LIMIT(3600);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

if(!$plugin['imail_gamma'] || !$sys_custom['iMailPlus']['BatchSetForwardingEmail'] || !isset($TabID) || !in_array($TabID,array(USERTYPE_STAFF,USERTYPE_STUDENT,USERTYPE_PARENT,USERTYPE_ALUMNI))){
	No_Access_Right_Pop_Up();
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || 
	($TabID==USERTYPE_STAFF && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"]) || 
	($TabID==USERTYPE_STUDENT && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"]) || 
	($TabID==USERTYPE_PARENT &&  $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"]) || 
	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) 
{
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$laccount = new libaccountmgmt();
$IMap = new imap_gamma(true);

switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ImportStaffForwardingEmails'] ;
        	$CurrentPageArr['StaffMgmt'] = 1;
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
        	$CurrentPage = "Mgmt_Account";
        	$back_url = "../StaffMgmt/index.php";
        	break;
        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ImportStudentForwardingEmails'] ;
        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	//$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
        	$back_url = "../StudentMgmt/index.php"; 
        	break;
        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ImportParentForwardingEmails'];
        	$CurrentPageArr['ParentMgmt'] = 1; 
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];
        	$back_url = "../ParentMgmt/index.php";
        	$CurrentPage = "Mgmt_Account";
        	break;
        case TYPE_ALUMNI: 
        	$Title = $Lang['AccountMgmt']['ImportAlumniForwardingEmails'];
        	$CurrentPageArr['AlumniMgmt'] = 1;
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
        	$back_url = "../AlumniMgmt/index.php"; 
			break;
        default:  
        	break;
}

$import_success = 0;

$sql = "SELECT Email, ForwardingEmails, KeepCopy FROM TEMP_IMPORT_MAIL_FORWARD_EMAIL";
$records = $laccount->returnResultSet($sql);
$record_count = count($records);

$resultAry = array();

for($i=0;$i<$record_count;$i++){
	$t_email = $records[$i]['Email'];
	$t_forward_emails = trim($records[$i]['ForwardingEmails']);
	$t_keepcopy = $records[$i]['KeepCopy'];
	
	if($t_forward_emails == ''){
		$api_update_success = $IMap->removeMailForward(array($t_email));
		$sql = "UPDATE MAIL_PREFERENCE SET ForwardedEmail=NULL, ForwardKeepCopy=NULL WHERE MailBoxName='$t_email'";
		$db_update_success = $laccount->db_db_query($sql);
	}else{
		$api_update_success = $IMap->removeMailForward(array($t_email));
		$t_forward_email_list = explode(",",$t_forward_emails);
		//debug_r($t_forward_email_list);
		for($j=0;$j<count($t_forward_email_list);$j++){
			$api_update_success = $IMap->setMailForward($t_forward_email_list[$j], $t_keepcopy, array($t_email)) && $api_update_success;
		}
		$sql = "UPDATE MAIL_PREFERENCE SET ForwardedEmail='$t_forward_emails', ForwardKeepCopy='$t_keepcopy' WHERE MailBoxName='$t_email'";
		$db_update_success = $laccount->db_db_query($sql);
		if($laccount->db_affected_rows() == 0){
			$sql = "INSERT INTO MAIL_PREFERENCE (MailBoxName,ForwardedEmail,ForwardKeepCopy,DateInput,DateModified)
                 VALUES ('$t_email','$t_forward_emails','$t_keepcopy', NOW(), NOW())";
            $db_update_success = $laccount->db_db_query($sql);
		}
	}
	$resultAry[] = $api_update_success && $db_update_success;
	$import_success += ($api_update_success && $db_update_success ? 1 : 0);
}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$back_url'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");


### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserEmailForwarding'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class='tabletext' align='center'><?=$import_success?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$BackBtn ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
?>