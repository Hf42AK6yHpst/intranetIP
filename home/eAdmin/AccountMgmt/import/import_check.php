<?php
# using:

###########################################
#
#   Date:   2020-02-26 Bill     [2020-0220-1623-28098]
#           added fields Graduation Date
#
#   Date:   2018-11-09 Anna
#           Added $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" cust
#
#   Date:   2018-08-21 Cameron [ip.2.5.9.10.1]
#           revised import columns for HKPF
#
#   Date:   2018-08-14 Cameron [ip.2.5.9.10.1] 
#           generate random password for HKPF if it's empty
#
#   Date:   2018-08-09 Cameron
#           add Grade and Duty field for HKPF
#
#	Date:	2017-01-18 HenryHM
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added [HouseholdRegister] and [House] for student. 
#												- added [House] for staff.
#
#	Date:	2016-05-09 Carlos
#			$sys_custom['ImportStudentAccountActionType'] - added action_type for student user type.
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-03-19	Cameron
#			use fax field for recording date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true
#				(applys to student and teacher only)
# 									
#	Date:	2014-12-10	Bill
#			$sys_custom['SupplementarySmartCard'] - added CardID4 for student user type
#
#	Date:	2014-08-20	Bill
#			Add Home Tel to format fields
#
#	Date: 	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] to check if header 'CardID' exist in import file for eEnrolment  
#
#	Date:	2013-12-13	Fai
#			Add a cust attribute "stayOverNight" with $plugin['medical']
#
#	Date:	2013-12-12	Carlos
#			iMail plus - $sys_custom['iMailPlus']['EmailAliasName'] Added field [EmailUserLogin] for staff user to input its own iMail plus account name
#
#	Date:	2013-11-13	Carlos
#			$sys_custom['SupplementarySmartCard'] - added CardID2 and CardID3 for student user type
#
#	Date:	2013-09-20	Ivan
#			Fixed: hidden field "ImportData" changed from intranet_htmlspecialchars to urlencode
#
#	Date:	2013-08-13	Carlos
#			KIS - hide staff code, WEBSAMS number and JUPAS application number for staff and student type
#
#	Date:	2013-08-08	Carlos
#			added field [Address] for student user type
#
#	Date:	2012-09-27	YatWoon
#			add "Barcode"
#
#	Date:	2012-08-03	Bill Mak
#			added fields Nationality, PlaceOfBirth, AdmissionDate
#
#	Date:	2011-11-02	YatWoon
#			add alumni 
#
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

//debug_pr($_POST);

$laccount = new libaccountmgmt();
//$lsports->authSportsSystem();

$linterface = new interface_html();
//$lclass = new libclass();
$limport = new libimporttext();
$lo = new libfilesystem();
$li = new libimport();

### CSV Checking
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed&TabID=$TabID");
	exit();
}

$data = $limport->GET_IMPORT_TXT($userfile);
$csv_header = array_shift($data);

$isKIS = $_SESSION["platform"] == "KIS";

# Smartcard
$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

if (($TabID == TYPE_STUDENT) || ($TabID == TYPE_TEACHER)) {
	$faxCol = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? "DateOfBaptism" : "Fax";	
}

# Prepare array to check csv header
if ($TabID == TYPE_TEACHER)     # Teacher / Staff
{	
    if ($sys_custom['project']['HKPF']) {
        $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","Remarks","Grade","Duty");
    }
    else {
	    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","$faxCol","Barcode","Remarks","TitleEnglish","TitleChinese");
    
    	if(!$isKIS) {
    		$file_format = array_merge($file_format, array("WebSAMSStaffCode"));
    	}
    }
	if($hasTeacherSmartCard){
		$file_format = array_merge($file_format, array("CardID"));
	}
}
else if ($TabID == TYPE_STUDENT)	# Student
{
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Home Tel","Mobile","$faxCol","Barcode","Remarks","DOB");
    if(!$isKIS) {
		$file_format = array_merge($file_format, array("WebSAMSRegNo","HKJApplNo"));
	}
	$file_format = array_merge($file_format, array("Address"));
}
else if ($TabID == TYPE_PARENT)	    # Parent
{
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Barcode","Remarks");
}
/*    
else if ($TabID == TYPE_ALUMNI)	# alumni
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","YearOfLeft");
*/

if($TabID != TYPE_ALUMNI)
{
	if($TabID == TYPE_STUDENT && $hasSmartCard)
	{
		$file_format = array_merge($file_format, array("CardID"));
		if($sys_custom['SupplementarySmartCard']){
			$file_format = array_merge($file_format, array("CardID2","CardID3","CardID4"));
		}
	}

	if($special_feature['ava_hkid'] && !$sys_custom['project']['HKPF'])
	{
		$file_format = array_merge($file_format, array("HKID"));
		if($TabID == TYPE_PARENT) {
			$file_format = array_merge($file_format, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
        }
	}

	if($sys_custom['BIBA_AccountMgmtCust'] && $TabID == TYPE_TEACHER)
	{
		$file_format = array_merge($file_format, array("House"));
	}
	
	if($special_feature['ava_strn'] && ($TabID == TYPE_STUDENT || $TabID == TYPE_ALUMNI))
    {
		$file_format = array_merge($file_format, array("STRN"));
    }
	
	if(($TabID == TYPE_TEACHER && $ssoservice["Google"]["Valid"]) || ($TabID == TYPE_STUDENT && $ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']))
	{
		$file_format = array_merge($file_format, array("EnableGSuite","GoogleUserLogin"));
	}
	
	if($sys_custom['BIBA_AccountMgmtCust'] && $TabID == TYPE_STUDENT)
	{
		$file_format = array_merge($file_format, array("HouseholdRegister", "House"));
	}
	
	if($TabID == TYPE_STUDENT && $sys_custom['StudentAccountAdditionalFields'])
	{
		$file_format = array_merge($file_format, array("Non-Chinese Speaking","Special Education Needs","Primary School","University/Institution","Programme"));
	}
}

# Additional Personal Settings Column
if($TabID == TYPE_STUDENT)
{
	if($plugin['medical'])
	{
		array_push($file_format, "StayOverNight");
	}

	// [2020-0220-1623-28098]
	//array_push($file_format, "Nationality","PlaceOfBirth","AdmissionDate");
    array_push($file_format, "Nationality","PlaceOfBirth","AdmissionDate","GraduationDate");
    
    if($plugin['SDAS_module']['KISMode']){
    	array_push($file_format, "KISClassType");
    }

	if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah")
	{
	    array_push($file_format, "PrimarySchoolCode");
	}
}

if ($TabID == TYPE_TEACHER && $plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'])	    # Teacher / Staff
{	
	$file_format = array_merge($file_format, array("EmailUserLogin"));
}

# Check csv header
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($csv_header[$i] != $file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	intranet_closedb();

	header("location:  import.php?xmsg=import_header_failed&TabID=$TabID");
	exit();
}

if(empty($data))
{
	intranet_closedb();

	header("location: import.php?xmsg=import_failed&TabID=$TabID");
	exit();
}

# Pass header checking / start import
if($TabID != TYPE_ALUMNI)
{
    if (is_array($data))
    {
        if ($sys_custom['project']['HKPF'])
        {
            foreach($data as $_row => $_colAry) {
                if (empty($data[$_row][1])) {
                    $data[$_row][1] = intranet_random_passwd($length=16, $enableUpperCase=true);
                }
            }
        }
        
    	$format = 1; # 1 means CSV
        $li->open_webmail = ($open_webmail==1);
        $li->open_file_account= ($open_file==1);
        $li->teaching = $teaching;
        
        if(/*$sys_custom['ImportStudentAccountActionType'] && */ $TabID == USERTYPE_STUDENT){
        	if(!in_array($action_type,array(1,2))) $action_type == 1;
        	$li->action_type = $action_type;
		}
        $li->set_format($format);
        $li->check_data($data);

        # SchoolNet integration
        if ($plugin['SchoolNet'])
        {
            include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
            $lschoolnet = new libschoolnet();

            if (sizeof($li->new_account_userids) != 0)
            {
                $userlist = implode(",",$li->new_account_userids);
                if ($TabID == 1)
                {
                    $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '$UserID'";
                    $tmpdata = $li->returnArray($sql,11);
                    $lschoolnet->addStaffUser($tmpdata);
                }
                else if ($TabID == 2)
                {
                     $sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '$UserID'";
                     $tmpdata = $li->returnArray($sql,12);
                     $lschoolnet->addStudentUser($tmpdata);
                }
                else if ($TabID == 3)
                {
                    // nothing
                }
            }
        }
         
        $error_msg = $li->getErrorMsgArr();
    }
}
else    // Alumni
{
	if (is_array($data))
    {
    	$format = 1; # 1 means CSV
        $li->open_webmail = ($open_webmail==1);
        // $li->open_file_account= ($open_file==1);
        // $li->teaching = $teaching;
        $li->set_format($format);
        $li->check_data($data);

        $error_msg = $li->getErrorMsgArr();
    }
}

// build error table
foreach((array)$error_msg as $key => $errorInfo)
{
	# Build Table Content
	$rowcss = " class='".($ctr++%2==0? "tablebluerow2":"tablebluerow1")."' ";
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[0]."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[1]."</td>";
	$Confirmtable .= "		<td class='tabletext'>";
	foreach($errorInfo[2] as $thisReason) {
		$Confirmtable .= $thisReason."<br>";
    }
	$Confirmtable .= "		</td>";
	$Confirmtable .= "	</tr>";
}
/*
## Post Data 
foreach($data as $key => $eachdata)
{
	$postValue .= "<input type='hidden' name='ImportRawData[$key]' value='".implode(":_Separator_:",(array)$eachdata)."'>\n";
	$postValue .= "<input type='hidden' name='ImportData[$key]' value='".implode(":_Separator_:",(array)$ImportData[$key])."'>\n";	
}
$postValue .= "<input type='hidden' name='ImportTargetClasses' value='".implode(":_Separator_:",(array)$ClassNameAry)."'>\n ";
*/

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

$Btn = empty($error_msg) ? $NextBtn."&nbsp;" : "";
$Btn .= $BackBtn;

### Title ###
switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ImportTeacherAccount'];

        	$CurrentPageArr['StaffMgmt'] = 1;
            $CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
        	break;

        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ImportStudentAccount'];

        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
        	break;

        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ImportParentAccount'];

        	$CurrentPageArr['ParentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount']; 
        	break;

        case TYPE_ALUMNI: 
        	$Title = $Lang['AccountMgmt']['ImportAlumniAccount'];

        	$CurrentPageArr['StudentMgmt'] = 1;
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
        	break;
}

$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);
      
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();
?>

<script language="javascript">
function goBack() {
	document.frm1.action = "import.php";
	document.frm1.submit();
}
</script>

<form name="frm1" method="POST" action="import_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="30%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td colspan="5">
				<table width="30%">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($error_msg)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($error_msg)?"red":""?>'><?=count($error_msg)?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<?if(!empty($error_msg)){?>
                <tr>
                    <td class='tablebluetop tabletopnolink' width="1%">Row#</td>
                    <td class='tablebluetop tabletopnolink'><?=$i_UserLogin?></td>
                    <td class='tablebluetop tabletopnolink'><?=$i_UserEmail?></td>
                    <td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
                </tr>
                <?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>

<input type="hidden" value='<?=urlencode(serialize($data))?>' name="ImportData">
<input type="hidden" value="<?=$open_file?>" name="open_file">
<input type="hidden" value="<?=$open_webmail?>" name="open_webmail">
<input type="hidden" value="<?=$open_webmail?>" name="open_webmail">
<input type="hidden" value="<?=$teaching?>" name="teaching">
<input type="hidden" value="<?=$TabID?>" name="TabID">
<?php if(/*$sys_custom['ImportStudentAccountActionType'] && */ $TabID == USERTYPE_STUDENT){ ?>
<input type="hidden" name="action_type" value="<?=$action_type?>" />
<?php } ?>
</form>

<?
$linterface->LAYOUT_STOP();
?>