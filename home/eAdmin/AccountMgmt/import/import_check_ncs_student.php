<?php
# using: Paul

###########################################
#
#
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
//$lsports->authSportsSystem();

$linterface 	= new interface_html();
//$lclass = new libclass();
$limport = new libimporttext();
$lo = new libfilesystem();
$li = new libimport();

### CSV Checking
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed&TabID=$TabID");
	exit();
}
$data = $limport->GET_IMPORT_TXT($userfile);
/*$format_data = array();
foreach($data as $idx=>$d){
	$format_data[$idx] = $li->comma_parse($d[0]);
}
$data = $format_data;*/


$csv_header = array_shift($data);

# smartcard
//$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox']);
//$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

$hasSmartCard = $plugin['eEnrollment'];

# prepare array to check csv header 
/*
$file_format = array ("UserLogin","UserPassword","UserEmail","EnglishName","ChineseName","NickName","Gender","HomeTelNo","MobileTelNo","FaxNo","Barcode","Remark","DateOfBirth","WebSAMSRegNo","HKJApplNo","Address");

if($TabID != TYPE_ALUMNI)
{
	if($TabID == TYPE_STUDENT && $hasSmartCard)
		$file_format = array_merge($file_format, array("CardID"));
	if($special_feature['ava_hkid']) {
		$file_format = array_merge($file_format, array("HKID"));
		if($TabID==TYPE_PARENT)
			$file_format = array_merge($file_format, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
	}
	
	if($special_feature['ava_strn'] && ($TabID==TYPE_STUDENT || $TabID==TYPE_ALUMNI))
		$file_format = array_merge($file_format, array("STRN"));
}

# Additional Personal Settings Column
if($TabID == TYPE_STUDENT){
	array_push($file_format, "Nationality","PlaceOfBirth","AdmissionDate");
}

# Extra Info fields
$ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
$totalCategory = count($ExtraInfoCatInfo);

for($a=0; $a<$totalCategory; $a++) {
	$file_format[] = $ExtraInfoCatInfo[$a]['CategoryCode'];
}

$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
for($a=0; $a<count($BlissFieldAry); $a++) {
	$file_format[] = $BlissFieldAry[$a];
}*/

$file_format = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Home Tel","Mobile","Fax","Barcode", "Remarks", "DOB", "WebSAMSRegNo", "HKJApplNo", "Address", "CardID", "HKID", "STRN", "Nationality", "PlaceOfBirth", "AdmissionDate", "StaffInCharge", "DurationInHK", "Language", "IsNCS", "NGO");	
	
//debug_pr(count($file_format));

# check csv header
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{

	if (trim($csv_header[$i])!=trim($file_format[$i]))
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	intranet_closedb();
	header("location:  import_ncs_student.php?xmsg=import_header_failed&TabID=$TabID");
	exit();
}
if(empty($data))
{
	intranet_closedb();
	header("location: import_ncs_student.php?xmsg=import_failed&TabID=$TabID");
	exit();
	
}

# pass header checking / start import


    if (is_array($data))
    {
    	$format = 1; # 1 means CSV
        $li->open_webmail = ($open_webmail==1);
        $li->open_file_account= ($open_file==1);
        $li->teaching = $teaching;
        $li->set_format($format);
        $li->check_data($data);
        
        $error_msg = $li->getErrorMsgArr();
    }


// build error table
foreach((array)$error_msg as $key => $errorInfo)
{
	#Build Table Content
	$rowcss = " class='".($ctr++%2==0? "tablebluerow2":"tablebluerow1")."' ";
	
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[0]."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[1]."</td>";
	$Confirmtable .= "		<td class='tabletext'>";
	foreach($errorInfo[2] as $thisReason)
		$Confirmtable .= $thisReason."<br>";
	$Confirmtable .= "		</td>";
	$Confirmtable .= "	</tr>";
}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

$Btn = empty($error_msg)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;

### Title ###

        	$Title = $Lang['AccountMgmt']['ImportStudentAccount'];
        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
        	

$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);
      
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();
?>
<script language="javascript">
function goBack() {
	document.frm1.action = "import_ncs_student.php";
	document.frm1.submit();
}
</script>
<form name="frm1" method="POST" action="import_update_ncs_student.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="5">
				<table width="30%">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($error_msg)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($error_msg)?"red":""?>'><?=count($error_msg)?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<?if(!empty($error_msg)){?>
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%">Row#</td>
				<td class='tablebluetop tabletopnolink'><?=$i_UserLogin?></td>
				<td class='tablebluetop tabletopnolink'><?=$i_UserEmail?></td>
				<td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
<input type="hidden" value='<?=intranet_htmlspecialchars(serialize($data))?>' name="ImportData">
<input type="hidden" value="<?=$open_file?>" name="open_file">
<input type="hidden" value="<?=$open_webmail?>" name="open_webmail">
<input type="hidden" value="<?=$open_webmail?>" name="open_webmail">
<input type="hidden" value="<?=$teaching?>" name="teaching">
<input type="hidden" value="<?=$TabID?>" name="TabID">
<input type="hidden" value="<?=count($file_format)?>" name="NoOfFields">

</form>

<?

$linterface->LAYOUT_STOP();
?>
