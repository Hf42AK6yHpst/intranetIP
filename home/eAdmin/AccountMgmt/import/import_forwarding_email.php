<?php
// Editing by 
/*
 * 2014-03-11 (Carlos): Created for Lassel College
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

if(!$plugin['imail_gamma'] || !$sys_custom['iMailPlus']['BatchSetForwardingEmail'] || !isset($TabID) || !in_array($TabID,array(USERTYPE_STAFF,USERTYPE_STUDENT,USERTYPE_PARENT,USERTYPE_ALUMNI))){
	No_Access_Right_Pop_Up();
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || 
	($TabID==USERTYPE_STAFF && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"]) || 
	($TabID==USERTYPE_STUDENT && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"]) || 
	($TabID==USERTYPE_PARENT &&  $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"]) || 
	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) 
{
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$laccount = new libaccountmgmt();

switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ImportStaffForwardingEmails'];
        	$CurrentPageArr['StaffMgmt'] = 1;
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
        	$CurrentPage = "Mgmt_Account";
        	$back_url = "../StaffMgmt/index.php";
        	break;
        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ImportStudentForwardingEmails'];
        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	//$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
        	$back_url = "../StudentMgmt/index.php"; 
        	break;
        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ImportParentForwardingEmails'];
        	$CurrentPageArr['ParentMgmt'] = 1; 
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];
        	$back_url = "../ParentMgmt/index.php";
        	$CurrentPage = "Mgmt_Account";
        	break;
        case TYPE_ALUMNI: 
        	$Title = $Lang['AccountMgmt']['ImportAlumniForwardingEmails'] ;
        	$CurrentPageArr['AlumniMgmt'] = 1;
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
        	$back_url = "../AlumniMgmt/index.php"; 
			break;
        default:  
        	break;
}

$sample_file = GET_CSV("eclass_forwarding_email.csv"); 

$csv_format = '';
for($i=0;$i<count($Lang['AccountMgmt']['EmailForwardingCSVFields']);$i++){
	$csv_format .= $Lang['SysMgr']['Homework']['Column'].' '.($i+1).' : '. $Lang['AccountMgmt']['EmailForwardingCSVFields'][$i];
}
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$back_url'","back_btn"," class='formbutton' "); 

//$csv_format.= $Lang['SysMgr']['Homework']['Column'].' 1 : <font color=red>*</font>Email <span class="tabletextremark">(iMail plus Account)</span><br />';
//$csv_format.= $Lang['SysMgr']['Homework']['Column'].' 2 : <font color=red>^</font>English Name<br />';
//$csv_format.= $Lang['SysMgr']['Homework']['Column'].' 3 : <font color=red>^</font>Chinese Name<br />';
//$csv_format.= $Lang['SysMgr']['Homework']['Column'].' 4 : <font color=red></font>Forwarding Emails <span class="tabletextremark">(Use comma as email separator. Left blank to unset.)</span><br />';
//$csv_format.= $Lang['SysMgr']['Homework']['Column'].' 5 : <font color=red>*</font>Keep Copy <span class="tabletextremark">(1 to keep copy. 0 do not keep copy.)</span><br />';

### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserEmailForwarding'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<form method="POST" name="form1" id="form1" action="import_forwarding_email_check.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><a class="tablelink" href="<?=$sample_file?>"><?=$Lang['General']['ClickHereToDownloadSample']?></a></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<br />
					<div class="tabletextremark">
						<?=$Lang['General']['RequiredField']?><br />
						<?=$Lang['General']['ReferenceField']?>
					</div>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $BackBtn ?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>  
<input id="TabID" name="TabID" value="<?=$TabID?>" type="hidden">
</form>
<br><br>
<script type="text/javascript" language="JavaScript">
function CheckForm()
{
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	return true;	
}
</script>
<?
 $linterface->LAYOUT_STOP();
?>