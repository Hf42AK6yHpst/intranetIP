<?php
// using : Henry

## Please use UTF-8 to edit because there is a customizated field 電子郵件帳號
###########################################
#
#   Date:   2020-02-26 Bill     [2020-0220-1623-28098]
#           Added Display field reminder for Graduation Date
#
#   Date:   2018-12-28 Isaac
#           Added $sys_custom['AccountMgmt']['StudentDOBRequired'] cust to set DOB field compulsory field.
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-11-09 Anna
#           added $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" cust
#
#   Date:   2018-11-01 Bill     [2018-1029-1009-38066]
#           added remarks for Gender column
#
#   Date:   2018-08-21 Cameron
#           revised field for HKPF
#   
#   Date:   2018-08-09 Cameron
#           Add $Lang['AccountMgmt']['AddnStaffFields'] for HKPF
#
#	Date:	2018-04-03 Carlos
#			Fixed KIS import student description columns skip WebSAMSRegNo and HKJApplNo index shifted issue. 
#
#	Date:	2018-01-03 Carlos
#			added password criteria remarks.
#
#	Date:	2017-12-22 Cameron
#			configure for Oaks refer to Amway 
#
#	Date:	2017-10-26 Cameron
#			redirect to import_amway for importing amway distributor
#
#	Date:	2017-07-19 Paul
#			redirect to import_ncs_student for NCS project
#
#	Date:	2017-01-18 HenryHM
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2016-05-09 Carlos
#			$sys_custom['ImportStudentAccountActionType'] - added action_type for student user type.
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-03-19 	Cameron
#			Add $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'], show hints for DateOfBaptism if it's set			
#
#	Date:	2014-09-12 	Carlos
#			Remove * for Parent's Gender field as Gender is not mandatory for parent type user only.
#
#	Date:	2014-08-21	Bill
#			Add Home Tel in Data Column and Display field reminder for Admission Date
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] for eEnrolment
# 
#	Date:	2013-12-12  Carlos 
#			iMail plus - $sys_custom['iMailPlus']['EmailAliasName'] Added field [EmailUserLogin] for staff user to input its own iMail plus account name
#
#	Date:	2013-09-25	Ivan
#			Fixed show Gender as compulsory field for TYPE_ALUMNI and TYPE_PARENT
#
#	Date:	2013-08-13  Carlos
#			KIS - hide staff code, WEBSAMS number and JUPAS application number for staff and student 
#
#	Date:	2013-08-08	Carlos
#			Changed to use get_csv_sample.php?TabID=[1 | 2 | 3 | 4] to output sample csv file
#
#	Date:	2013-03-19	Rita
#			Fix remark display issue
#
#	Date:	2012-09-27	YatWoon
#			Add "Barcode"
#
#	Date:	2012-08-03	Bill Mak
#			Support import fields for personal setting such as nationality, PlaceOfBirth, etc
#			field selected by $Lang['AccountMgmt']['AddnPersonalSettings'] in language file
#
#	Date:	2011-11-02	YatWoon
#			add alumni 
#
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!isset($TabID) || $TabID=="") {
	header("location: /");
	exit();
}

if($sys_custom['StudentMgmt']['BlissWisdom'] && $TabID==USERTYPE_STUDENT) {
	header("Location: import_bliss_wisdom.php?TabID=$TabID");	
}
if((isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] !="") && $TabID==USERTYPE_STUDENT) {
	header("Location: import_ncs_student.php?TabID=$TabID");
}
if($sys_custom['project']['CourseTraining']['IsEnable'] && $TabID==USERTYPE_STUDENT) {
	header("Location: import_amway.php?TabID=$TabID");	
}

$linterface = new interface_html();
// $CurrentPage	= "PageArrangement_EnrolmentUpdate";

$laccount = new libaccountmgmt();

$lu = new libuser();
$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$TabID));

$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$TabID];
if ($PasswordLength < 6) {
    $PasswordLength = 6;
}

$isKIS = $_SESSION["platform"] == "KIS";

### Smartcard
$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

### Download csv / page title
$other_fields = "";
if($TabID==TYPE_STUDENT && $hasSmartCard) {
	$other_fields .= "_card";
}
if($special_feature['ava_hkid']) {
	$other_fields .= "_hkid";
}
if($special_feature['ava_strn'] && ($TabID == TYPE_STUDENT || $TabID == TYPE_ALUMNI)) {
	$other_fields .= "_strn";
}

$csv_format = "";
$delim = "";
$addnReferenceArray = array();

switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ImportTeacherAccount'];

        	$CurrentPageArr['StaffMgmt'] = 1;
            $CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

        	$back_url = "../StaffMgmt/index.php";
        	$sample_file = $hasTeacherSmartCard?GET_CSV("eclass_staff_card$other_fields.csv"):GET_CSV("eclass_staff$other_fields.csv");

        	$referenceAry = $Lang['AccountMgmt']['StaffImportFields'];
        	if ($sys_custom['project']['HKPF']) {
        	    $compulsoryField = array(1,0,0,1,0,0,0,0);
        	}
        	else {
        	   $compulsoryField = array(1,1,0,1,0,0,1,0,0,0,0,0,0,0,0);
        	}
        	if($ssoservice["Google"]["Valid"]){
        		foreach((array)$Lang['AccountMgmt']['GoogleImportFields'] as $google_field){
        			array_push($referenceAry, $google_field);
        			array_push($compulsoryField, 0);
        		}
        	}

			if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
				$referField = array(8 => $Lang['AccountMgmt']['DOBFieldReminder']);
			}
			else {
				$referField = array();
			}
			$referField[6] = $Lang['AccountMgmt']['GenderFieldReminder'];
        	break;

        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ImportStudentAccount'];

        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

        	$back_url = "../StudentMgmt/index.php"; 
        	$sample_file = GET_CSV("eclass$other_fields.csv");

        	$referenceAry = $Lang['AccountMgmt']['StudentImportFields']; 
        	$compulsoryField = array(1,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0);
        	if($sys_custom['AccountMgmt']['StudentDOBRequired']){
        	    $compulsoryField[12] = 1;
        	}
        	if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
        		foreach((array)$Lang['AccountMgmt']['GoogleImportFields'] as $google_field){
        			array_push($referenceAry, $google_field);
        			array_push($compulsoryField, 0);
        		}
        	}
        	$referField = array(6 => $Lang['AccountMgmt']['GenderFieldReminder'], 12 => $Lang['AccountMgmt']['DOBFieldReminder'], 13 => $Lang['AccountMgmt']['WebSAMSFieldReminder']);
			if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
				$referField[9] = $Lang['AccountMgmt']['DOBFieldReminder'];
			}        	
        	$addnReferenceArray = $Lang['AccountMgmt']['AddnPersonalSettings'];
        	break;

        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ImportParentAccount'];

        	$CurrentPageArr['ParentMgmt'] = 1;
            $CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];

        	$back_url = "../ParentMgmt/index.php";
        	$sample_file = GET_CSV("eclass_parent$other_fields.csv");

        	$referenceAry = $Lang['AccountMgmt']['ParentImportFields']; 
        	$compulsoryField = array(1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
        	break;

        case TYPE_ALUMNI:
        	$Title = $Lang['AccountMgmt']['ImportAlumniAccount'];

        	$CurrentPageArr['AlumniMgmt'] = 1;
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];

        	$back_url = "../AlumniMgmt/index.php"; 
			$sample_file = GET_CSV("eclass_alumni.csv");

			$referenceAry = $Lang['AccountMgmt']['AlumniImportFields']; 
			$compulsoryField = array(1,1,0,1,0,0,1,0,0,0,0,0,0,0);
			break;

        default: 
        	$sample_file = GET_CSV("eclass$other_fields.csv");
        	break;
}

$sample_file = "get_csv_sample.php?TabID=".$TabID;

//debug_pr($referenceAry);
$col_i = 0;
for($i=0; $i<sizeof($referenceAry); $i++)
{
	if($isKIS && $TabID == TYPE_TEACHER && $i == 13) { // KIS - skip WEBSAMS staff code for staff
		continue;
	}
	if($isKIS && $TabID == TYPE_STUDENT && ($i == 13 || $i == 14)) {
		continue;
	}
	
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($col_i+1)." : ";
	$csv_format .= ($compulsoryField[$i]) ? "<font color=red>*</font>" : "";
	$csv_format .= $referenceAry[$i];

	if($i == 2 && !$sys_custom['project']['HKPF']) {
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['EmailFieldReminder']."</span>";
	}
	else if(($TabID == TYPE_STUDENT || $TabID == TYPE_TEACHER) && array_key_exists($i, $referField)) {
		$csv_format .= " <span class=\"tabletextremark\">".$referField[$i]."</span>";
	}
	
	if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'] && $TabID == TYPE_TEACHER && ($referenceAry[$i] == "EmailUserLogin" || $referenceAry[$i] == "電子郵件帳號")){
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['EmailUserLoginFieldReminder']."</span>";
	}	
	
	if($sys_custom['StudentAccountAdditionalFields'] && 
		($referenceAry[$i] == "University/Institution" || $referenceAry[$i] == "Programme" || $referenceAry[$i] == "大學/專上院校" || $referenceAry[$i] == "專業課程")){
		$csv_format .= " <span class=\"tabletextremark\">(".$Lang['AccountMgmt']['UniversityProgrammeRemark'].")</span>";
	}

	$delim = "<br>";
	$col_i++;
}

# Additional Personal Settings Column
for($p=0; $p<sizeof($addnReferenceArray); $i++, $p++)
{
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($col_i+1)." : ";
	$csv_format .= $Lang['AccountMgmt'][$addnReferenceArray[$p]];

	// [2020-0220-1623-28098]
    //if($addnReferenceArray[$p] === 'AdmissionDate') {
	if($addnReferenceArray[$p] === 'AdmissionDate' || $addnReferenceArray[$p] === 'GraduationDate') {
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['DOBFieldReminder']."</span>";
	}

	$delim = "<br>";
	$col_i++;
}
if($plugin['SDAS_module']['KISMode'] && $TabID == TYPE_STUDENT){
	include_once($PATH_WRT_ROOT.'/home/cees_kis/lang/'.$intranet_session_language.'.php');
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($col_i+1)." : ";
	$csv_format .= $Lang['CEES']['MonthlyReport']['JSLang']['KISClassType'];
	$csv_format .= '<a id="remark2Btn" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer2()">['.$Lang['AccountMgmt']['Remarks'].']</a>';
	## Remarks Layer
	$RemarksLayer2 = '';
	$RemarksLayer2 .= '<div id="remark2Div" class="selectbox_layer" style="width:400px;">'."\r\n";
	$RemarksLayer2 .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
	$RemarksLayer2 .= '<thead>'."\r\n";
	$RemarksLayer2 .= '<tr>'."\r\n";
	$RemarksLayer2 .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
	$RemarksLayer2 .= '<a href="javascript:hideRemarkLayer2();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
	$RemarksLayer2 .= '</td>'."\r\n";
	$RemarksLayer2 .= '</tr>'."\r\n";
	$RemarksLayer2 .= '<tr>'."\r\n";
	$RemarksLayer2 .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
	$RemarksLayer2 .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$RemarksLayer2 .= '<thead>'."\n";
	$RemarksLayer2 .= '<tr>'."\n";
	$RemarksLayer2 .= '<th width="80%">'.$Lang['CEES']['MonthlyReport']['JSLang']['KISClassType'].'</th>'."\n";
	$RemarksLayer2 .= '<th width="20%">'.$Lang['General']['Code'].'</th>'."\n";
	$RemarksLayer2 .= '</tr>'."\n";
	$RemarksLayer2 .= '</thead>'."\n";
	$RemarksLayer2 .= '<tbody>'."\n";
	foreach($Lang['CEES']['MonthlyReport']['JSLang']['KISClassTypeAry'] as $k => $v){
		$RemarksLayer2 .= "<tr>";
		$RemarksLayer2 .= '<td>'.$v.'</td>'."\n";
		$RemarksLayer2 .= '<td>'.$k.'</td>'."\n";
		$RemarksLayer2 .= "</tr>";
	}
	$RemarksLayer2 .= '</tbody>'."\n";
	$RemarksLayer2 .= '</table>'."\r\n";
	$RemarksLayer2 .= '</td>'."\r\n";
	$RemarksLayer2 .= '</tr>'."\r\n";
	$RemarksLayer2 .= '</tbody>'."\r\n";
	$RemarksLayer2 .= '</table>'."\r\n";
	$RemarksLayer2 .= '</div>'."\r\n";
	$col_i++;
}

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && $TabID == TYPE_STUDENT)
{
    $csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($col_i+1)." : ";
    $csv_format .= $Lang['AccountMgmt']['PrimarySchoolCode'];
    $csv_format .= '

<span class="tabletextremark">('.$Lang['AccountMgmt']['ShowPrimarySchoolCode'].')</span>
<a id="remarkBtn" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer()">['.$Lang['AccountMgmt']['Remarks'].']</a>';
 
    ## get primary school code from central
    include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
    $lcees = new libcees();
    $SchoolInfoAry = $lcees->getAllSchoolInfoFromCentralServer('P');
    $i = 0;
    $SchoolNameAry[0][] = '000000';
    $SchoolNameAry[0][] = $Lang['General']['NotApplicable'] ;
   
    ## Remarks Layer
    $RemarksLayer = '';
    $RemarksLayer .= '<div id="remarkDiv" class="selectbox_layer" style="width:400px;">'."\r\n";
        $RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
            $RemarksLayer .= '<thead>'."\r\n";
                $RemarksLayer .= '<tr>'."\r\n";
                    $RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
                        $RemarksLayer .= '<a href="javascript:hideRemarkLayer();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
                    $RemarksLayer .= '</td>'."\r\n";
                $RemarksLayer .= '</tr>'."\r\n";
                $RemarksLayer .= '<tr>'."\r\n";
                    $RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
                    $RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
                    $RemarksLayer .= '<thead>'."\n";
                    $RemarksLayer .= '<tr>'."\n";
                    $RemarksLayer .= '<th>'.$Lang['AccountMgmt']['PrimarySchoolEnglishName'].'</th>'."\n";
                    $RemarksLayer .= '<th>'.$Lang['AccountMgmt']['PrimarySchoolCode'].'</th>'."\n";
                $RemarksLayer .= '</tr>'."\n";
                $RemarksLayer .= '</thead>'."\n";
                $RemarksLayer .= '<tbody>'."\n";
                
                foreach((array)$SchoolInfoAry as $SchoolInfo){
                    $RemarksLayer .= '<tr>'."\n";
                    $RemarksLayer .= '<td>'.$SchoolInfo['NameEn'].'</td>'."\n";
                    $RemarksLayer .= '<td>'.$SchoolInfo['SchoolCode'].'</td>'."\n";
                    $RemarksLayer .= '</tr>'."\n";             
                }

                $RemarksLayer .= '</tbody>'."\n";
                $RemarksLayer .= '</table>'."\r\n";
                $RemarksLayer .= '</td>'."\r\n";
                $RemarksLayer .= '</tr>'."\r\n";
            $RemarksLayer .= '</tbody>'."\r\n";
        $RemarksLayer .= '</table>'."\r\n";
    $RemarksLayer .= '</div>'."\r\n";
}

$csvFile = "<a class='tablelink' href='". $sample_file ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$back_url'","back_btn"," class='formbutton' "); 

### is teaching staff radio btn
if($TabID == TYPE_TEACHER)
{
	$teaching_row .= '<tr>'."\n";
		$teaching_row .= '<td class="formfieldtitle" width="30%">'."\n";
			$teaching_row .= $Lang['AccountMgmt']['Importing']."\n";	
		$teaching_row .= '</td>'."\n";
		$teaching_row .= '<td class="tabletext">'."\n";
				$teaching_row .= '<input id="isTeaching" type=radio name=teaching value=1 checked><label for="isTeaching">'.$i_teachingStaff."</label>\n";
				$teaching_row .= '<input id="notTeaching" type=radio name=teaching value=0><label for="notTeaching">'.$i_nonteachingStaff."</label>\n";
				if ($plugin['SLRS']) {
					$teaching_row .= '<input id="supplyTeacher" type=radio name=teaching value="S"><label for="supplyTeacher">'.$Lang['Identity']['SupplyTeacher']."</label>\n";
				}
				$teaching_row .= "<br><span class=tabletextremark>$i_teachingDifference</span>\n";
		$teaching_row .= '</td>'."\n";
	$teaching_row .= '</tr>'."\n"; 
}

### webmail setting
$account_creation = false;
if (($TabID != TYPE_ALUMNI && isset($plugin['webmail']) && $plugin['webmail'] && in_array($TabID, $webmail_identity_allowed)) || ($plugin['imail_gamma']))
{
	if ($webmail_type == 2) {
        $mail_title = $i_Mail_LinkWebmail;
	}
	else if ($webmail_type == 3) {
        $mail_title = $i_Mail_AllowSendReceiveExternalMail;
	}
	else {
    	$mail_title = $i_Mail_OpenWebmailAccount;
	}
	
    $account_creation = true;
	$webmail_checkbox = '<input id="mailopts" type=checkbox name=open_webmail value=1 checked><label for="mailopts">'.$Lang['AccountMgmt']['CreateImailAccounts']."</label>\n";
}

### ifile setting
if ($TabID != TYPE_ALUMNI && isset($plugin['personalfile']) && $plugin['personalfile'] && in_array($TabID, $personalfile_identity_allowed))
{
    if ($personalfile_type == 'FTP')
    {
        include_once($PATH_WRT_ROOT."includes/libftp.php");
        $lftp = new libftp();
        if ($lftp->isAccountManageable) {
            $file_title = $i_Files_UseIFolder;
        }
        else {
            $file_title = $i_Files_LinkFTPAccount;
        }
    }
    else if ($personalfile_type == 'AERO')
    {
         $file_title = $i_Files_LinkToAero;
    }
    
    $account_creation = true;
    $ifile_checkbox = '<input id="ifileopts" type=checkbox name=open_file value=1 checked><label for="ifileopts">'.$Lang['AccountMgmt']['CreateIfolderAccounts']."</label>\n";
}

if($account_creation)
{
	if($ifile_checkbox && $webmail_checkbox) {
		$br = "<br>";
	}
	
	$ifile_mail_row = '<tr>'."\n";
		$ifile_mail_row .= '<td width=30% class="formfieldtitle">'.$Lang['AccountMgmt']['Options'].'</td>'."\n";
		$ifile_mail_row .= '<td class="tabletext">';
			$ifile_mail_row .= $webmail_checkbox.$br.$ifile_checkbox."<br>";
			if($TabID != 3) {
				//$ifile_mail_row .= '<span class=tabletextremark>'.$Lang['AccountMgmt']['FileMailRemarks'].'</span>';
				$ifile_mail_row .= '<span class=tabletextremark>'.$Lang['AccountMgmt']['ImportStudentRemark'].'</span>';
			}
		$ifile_mail_row .= '</td>'."\n";
	$ifile_mail_row .= '</tr>'."\n";	
}

### Import Remarks
$ImportRemarkArr = $Lang['AccountMgmt']['ImportRemarks'];
if($TabID == TYPE_STUDENT) {
	$ImportRemarkArr[] = $Lang['AccountMgmt']['ImportRemarks2'];
}

$ImportRemark = '<table cellpadding=0 cellspacing=0 border=0>';	
for($i=0; $i < count($ImportRemarkArr); $i++) {
	$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.($i+1).'. </td><td>'.$ImportRemarkArr[$i].'</td></tr>';
}

if($sys_custom['UseStrongPassword']) {
	for($i=0; $i<count($Lang['AccountMgmt']['PasswordRequirements']); $i++) {
		$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.(count($ImportRemarkArr)+$i+1).'. </td><td>'.str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,$Lang['AccountMgmt']['PasswordRequirements'][$i]).'</td></tr>';
	}
}
$ImportRemark .= '</table>';

### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<form method="POST" name="frm1" id="frm1" action="import_check.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
						</tr>
						
					<?php if(/*$sys_custom['ImportStudentAccountActionType'] && */ $TabID == USERTYPE_STUDENT) { ?>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['ActionType']?></td>
							<td class="tabletext">
								<?=$linterface->Get_Radio_Button("action_type1", "action_type", "1", 1, "", $Lang['AccountMgmt']['ActionType.Add']).'&nbsp;'?>
								<?=$linterface->Get_Radio_Button("action_type2", "action_type", "2", 0, "", $Lang['AccountMgmt']['ActionType.Update'])?>
							</td>
						</tr>
					<?php } ?>
					
						<?=$teaching_row?>
						<?=$ifile_mail_row?>
						<?=$remarks_row?>

						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['Remarks']?> </td>
							<td class="tabletext"><?=$ImportRemark?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $BackBtn ?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>

<input name="TabID" value="<?=$TabID?>" type="hidden">
</form>

<?=$RemarksLayer?>
<?=$RemarksLayer2?>
<br><br>

<script>
function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn' ;
	var remarkDivId = 'remarkDiv';
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}
function showRemarkLayer2(remarkType) {
	var remarkBtnId = 'remark2Btn' ;
	var remarkDivId = 'remark2Div';
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = 'remarkDiv';
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideRemarkLayer2(remarkType) {
	var remarkDivId = 'remark2Div';
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}

function CheckForm() {
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext != ".csv" && fileext != ".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	return true;	
}
</script>

<?
$linterface->LAYOUT_STOP();
?>