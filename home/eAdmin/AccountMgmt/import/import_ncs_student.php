<?php
// using : 

###########################################
#
#
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

if(!isset($TabID) || $TabID=="") {
	header("location: /");
	exit();
}


$linterface 	= new interface_html();
// $CurrentPage	= "PageArrangement_EnrolmentUpdate";

$laccount = new libaccountmgmt();

### smartcard
//$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox']);
//$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );


### download csv / page title
$other_fields = "";
if($TabID==TYPE_STUDENT && $hasSmartCard)
	$other_fields .= "_card";
if($special_feature['ava_hkid'])
	$other_fields .= "_hkid";
if($special_feature['ava_strn'] && ($TabID==TYPE_STUDENT || $TabID==TYPE_ALUMNI))
	$other_fields .= "_strn";

$other_fields .= "_ncs";

$csv_format = "";
$delim = "";

$Title = $Lang['AccountMgmt']['ImportStudentAccount'];
$CurrentPageArr['StudentMgmt'] = 1; 
$CurrentPage = "Mgmt_Account";
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$back_url = "../StudentMgmt/index.php"; 
$sample_file = GET_CSV("eclass$other_fields.csv");
$referenceAry = $Lang['AccountMgmt']['StudentImportFields']; 
$addnReferenceArray = $Lang['AccountMgmt']['AddnPersonalSettings'];
$compulsoryField = array(1,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0);
$referField = array(12=>$Lang['AccountMgmt']['DOBFieldReminder'], 13=>$Lang['AccountMgmt']['WebSAMSFieldReminder']);
        	
for($i=0; $i<sizeof($referenceAry); $i++){
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ";
	$csv_format .= ($compulsoryField[$i]) ? "<font color=red>*</font>" : "";
	$csv_format .= $referenceAry[$i];
	if($i==2)
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['EmailFieldReminder']."</span>";
	else if($TabID==TYPE_STUDENT && array_key_exists($i, $referField))
		$csv_format .= " <span class=\"tabletextremark\">".$referField[$i]."</span>";
	$delim = "<br>";
}

# Additional Personal Settings Column
for($p=0; $p<sizeof($addnReferenceArray); $p++){
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ";
	$csv_format .= $Lang['AccountMgmt'][$addnReferenceArray[$p]];
	if($addnReferenceArray[$p]==='AdmissionDate'){
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['DOBFieldReminder']."</span>";
	}
	$i++;
}

# Extra Info fields
$extraAry = $Lang['AccountMgmt']['NCSImportFields'];
$totalCategory = count($extraAry);
for($a=0; $a<$totalCategory; $a++) {
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ";
	$csv_format .= $extraAry[$a];
	if($a==2){
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['LanguagePriorityRemark']."</span>";
	}
	$i++;	
}


$csvFile = "<a class='tablelink' href='". $sample_file ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$back_url'","back_btn"," class='formbutton' "); 

### is teaching staff radio btn
if($TabID == TYPE_TEACHER)
{
	
	$teaching_row .= '<tr>'."\n";
		$teaching_row .= '<td class="formfieldtitle" width="30%">'."\n";
			$teaching_row .= $Lang['AccountMgmt']['Importing']."\n";	
		$teaching_row .= '</td>'."\n";
		$teaching_row .= '<td class="tabletext">'."\n";
				$teaching_row .= '<input id="isTeaching" type=radio name=teaching value=1 checked><label for="isTeaching">'.$i_teachingStaff."</label>\n";
				$teaching_row .= '<input id="notTeaching" type=radio name=teaching value=0><label for="notTeaching">'.$i_nonteachingStaff."</label>\n";
				$teaching_row .= "<br><span class=tabletextremark>$i_teachingDifference</span>\n";
		$teaching_row .= '</td>'."\n";
	$teaching_row .= '</tr>'."\n"; 
}

### webmail setting
$account_creation = false;
if (($TabID != TYPE_ALUMNI && isset($plugin['webmail']) && $plugin['webmail'] && in_array($TabID, $webmail_identity_allowed)) || ($plugin['imail_gamma']))
{
	if ($webmail_type == 2)
        $mail_title = $i_Mail_LinkWebmail;
    else if ($webmail_type == 3)
        $mail_title = $i_Mail_AllowSendReceiveExternalMail;
    else 
    	$mail_title = $i_Mail_OpenWebmailAccount;
		
    $account_creation = true;
	$webmail_checkbox = '<input id="mailopts" type=checkbox name=open_webmail value=1 checked><label for="mailopts">'.$Lang['AccountMgmt']['CreateImailAccounts']."</label>\n";
}

### ifile setting
if ($TabID != TYPE_ALUMNI && isset($plugin['personalfile']) && $plugin['personalfile'] && in_array($TabID,$personalfile_identity_allowed))
{
    if ($personalfile_type=='FTP')
    {
        include_once($PATH_WRT_ROOT."includes/libftp.php");
        $lftp = new libftp();
        if ($lftp->isAccountManageable)
            $file_title = $i_Files_UseIFolder;
        else
            $file_title = $i_Files_LinkFTPAccount;
    }
    else if ($personalfile_type=='AERO')
         $file_title = $i_Files_LinkToAero;

    $account_creation = true;
    $ifile_checkbox = '<input id="ifileopts" type=checkbox name=open_file value=1 checked><label for="ifileopts">'.$Lang['AccountMgmt']['CreateIfolderAccounts']."</label>\n";
}

if($account_creation)
{
	if($ifile_checkbox&&$webmail_checkbox)
		$br = "<br>";
	
	$ifile_mail_row = '<tr>'."\n";
		$ifile_mail_row .= '<td width=30% class="formfieldtitle">'.$Lang['AccountMgmt']['Options'].'</td>'."\n";
		$ifile_mail_row .= '<td class="tabletext">';
			$ifile_mail_row .= $webmail_checkbox.$br.$ifile_checkbox."<br>";
			if($TabID!=3)
				//$ifile_mail_row .= '<span class=tabletextremark>'.$Lang['AccountMgmt']['FileMailRemarks'].'</span>';
				$ifile_mail_row .= '<span class=tabletextremark>'.$Lang['AccountMgmt']['ImportStudentRemark'].'</span>';
		$ifile_mail_row .= '</td>'."\n";
	$ifile_mail_row .= '</tr>'."\n";	
}

### Import Remarks
$ImportRemarkArr = $Lang['AccountMgmt']['ImportRemarks'];
if($TabID==TYPE_STUDENT) 
	$ImportRemarkArr[] = $Lang['AccountMgmt']['ImportRemarks2'];
	
$ImportRemark = '<table cellpadding=0 cellspacing=0 border=0>';	
for($i=0; $i < count($ImportRemarkArr); $i++)
	$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.($i+1).'. </td><td>'.$ImportRemarkArr[$i].'</td></tr>';	 
$ImportRemark .= '</table>';

### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<form method="POST" name="frm1" id="frm1" action="import_check_ncs_student.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
						</tr>
						<?=$teaching_row?>
						<?=$ifile_mail_row?>
						<?=$remarks_row?>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['Remarks']?> </td>
							<td class="tabletext"><?=$ImportRemark?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $BackBtn ?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>  
<input name="TabID" value="<?=$TabID?>" type="hidden">
</form>
<br><br>
<script>
function CheckForm()
{
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	return true;	
}
</script>
<?
 $linterface->LAYOUT_STOP();
?>

