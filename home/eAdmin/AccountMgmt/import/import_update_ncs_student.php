<?php

//using : 

###########################################
#
#
#
###########################################

@SET_TIME_LIMIT(600);

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcurdlog.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$li = new libimport();
$lu = new libuser();
$log = new libcurdlog();

$linterface 	= new interface_html();

$data = unserialize(stripslashes(intranet_undo_htmlspecialchars($ImportData)));

//$startExtraInfo = $NoOfFields-15;
// Total field related to Bliss is 14
$startExtraInfo = $NoOfFields-14;

if (is_array($data))
{
	$format = 1; # 1 means CSV

    $li->open_webmail = ($open_webmail==1);
    $li->open_file_account= ($open_file==1);
    $li->teaching = $teaching;
    $li->set_format($format);
    $li->process_data2($data);
	     
    $error_msg = $li->getErrorMsgArr();
	
    $log->addlog("User Settings", "IMPORT", "INTRANET_USER", "UserID", implode(",",$li->new_account_userids), "", "", "");
	
}

$successAry['synUserDataToModules'] = $lu->synUserDataToModules($li->new_account_userids);

for($i=0;$i<count($data);$i++)
{
	if(empty($li->reason[$i]))
		$import_success++;
}

## gen title / back_btn_url 
$Title = $Lang['AccountMgmt']['ImportStudentAccount'];
$CurrentPageArr['StudentMgmt'] = 1; 
$CurrentPage = "Mgmt_Account";
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$url = "../StudentMgmt/index.php";


$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$url'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class='tabletext' align='center'><?=$import_success?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$BackBtn ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
?>