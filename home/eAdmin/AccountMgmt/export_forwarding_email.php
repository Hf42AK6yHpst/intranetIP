<?php
// Editing by 
/*
 * 2019-03-12 (Carlos): $sys_custom['iMail']['ExportForwardingEmail'] - Export forwarding emails for Webmail.
 * 2014-03-11 (Carlos): Created for Lassel College
 */
@SET_TIME_LIMIT(3600);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

if(!( ($plugin['imail_gamma'] && $sys_custom['iMailPlus']['BatchSetForwardingEmail']) || ($plugin['webmail'] && $sys_custom['iMail']['ExportForwardingEmail']) ) || !isset($TabID) || !in_array($TabID,array(USERTYPE_STAFF,USERTYPE_STUDENT,USERTYPE_PARENT,USERTYPE_ALUMNI))){
	No_Access_Right_Pop_Up();
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || 
	($TabID==USERTYPE_STAFF && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"]) || 
	($TabID==USERTYPE_STUDENT && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"]) || 
	($TabID==USERTYPE_PARENT &&  $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"]) || 
	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) 
{
	No_Access_Right_Pop_Up();
}

$lexport = new libexporttext();
$li = new libdb();

if($TabID==USERTYPE_STAFF){
	$eng_name_field = "u.EnglishName";
	$chi_name_field = "u.ChineseName";
}else if($TabID==USERTYPE_STUDENT){
	$eng_name_field = "CONCAT(u.EnglishName,IF(TRIM(u.ClassName)<>'',CONCAT('(',u.ClassName,'#',u.ClassNumber,')'),'')) as EnglishName";
	$chi_name_field = "CONCAT(u.ChineseName,IF(TRIM(u.ClassName)<>'',CONCAT('(',u.ClassName,'#',u.ClassNumber,')'),'')) as ChineseName";
}else if($TabID==USERTYPE_PARENT){
	$eng_name_field = "CONCAT(u.EnglishName,IF(u2.UserID IS NOT NULL,CONCAT('(',u2.EnglishName,' - ',u2.ClassName,'#',u2.ClassNumber,')'),'')) as EnglishName";
	$chi_name_field = "CONCAT(u.ChineseName,IF(u2.UserID IS NOT NULL,CONCAT('(',u2.ChineseName,' - ',u2.ClassName,'#',u2.ClassNumber,')'),'')) as ChineseName";
	$student_user_table = "LEFT JOIN INTRANET_PARENTRELATION as r ON r.ParentID=u.UserID 
							LEFT JOIN INTRANET_USER as u2 ON u2.UserID=r.StudentID ";
}else if($TabID==USERTYPE_ALUMNI){
	$eng_name_field = "CONCAT(u.EnglishName,IF(TRIM(u.YearOfLeft)<>'',CONCAT('(',u.YearOfLeft,')'),'')) as EnglishName";
	$chi_name_field = "CONCAT(u.ChineseName,IF(TRIM(u.YearOfLeft)<>'',CONCAT('(',u.YearOfLeft,')'),'')) as ChineseName";
}

if($plugin['imail_gamma'])
{
	$sql = "SELECT 
				u.ImapUserEmail as Email, 
				$eng_name_field,
				$chi_name_field,
				p.ForwardedEmail,
				p.ForwardKeepCopy 
			FROM INTRANET_USER as u 
			LEFT JOIN MAIL_PREFERENCE as p ON p.MailBoxName=u.ImapUserEmail $student_user_table 
			WHERE u.RecordStatus='1' AND u.RecordType='$TabID' AND u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> '' 
			ORDER BY u.ImapUserEmail";
}else if($plugin['webmail']){
	$sql = "SELECT 
				CONCAT(u.UserLogin,'@".$webmail_info['mailhost']."') as Email,
				$eng_name_field,
				$chi_name_field,
				p.ForwardedEmail,
				p.ForwardKeepCopy 
			FROM INTRANET_USER as u 
			LEFT JOIN INTRANET_IMAIL_PREFERENCE as p ON p.UserID=u.UserID
			WHERE u.RecordStatus='1' AND u.RecordType='$TabID'
			ORDER BY u.UserLogin ";
}

$records = $li->returnResultSet($sql);
$record_count = count($records);

$header_row = array("Email", "English Name", "Chinese Name", "Forwarding Emails", "Keep Copy");
$rows = array();
for($i=0;$i<$record_count;$i++) {
	$forward_email = str_replace("\n",", ",trim($records[$i]['ForwardedEmail']));
	$keep_copy = $records[$i]['ForwardKeepCopy'] == 1? "1" : "0";
	$rows[] = array($records[$i]['Email'],$records[$i]['EnglishName'],$records[$i]['ChineseName'],$forward_email,$keep_copy);
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $header_row);	
$filename = "forwarding_emails.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>