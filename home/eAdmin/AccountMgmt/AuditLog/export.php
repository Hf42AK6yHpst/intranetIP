<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libcurdlog.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_opendb();
$linterface = new interface_html();
$log = new libcurdlog();
$lexport = new libexporttext();

if($module == "") $module = "user";
switch($module){
	case 'login':	
		$username_field = getNameFieldWithLoginClassNumberByLang("b.");
		if($identity)
		{
			$identity_sql = " and b.RecordType=$identity ";	
		}
		if($keyword)
		{
			$keyword_sql = " and $username_field LIKE '%".$keyword."%' ";
		}
		
		$sql  = "SELECT
		   $username_field, b.ClassName, a.StartTime, a.DateModified
		   ,a.ClientHost
		   ,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM INTRANET_LOGIN_SESSION as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
		where a.StartTime>='$start 00:00:00' and a.DateModified<='$end 23:59:59' $identity_sql $keyword_sql
		order by a.StartTime
		";
		         
		$lib = new libdb();
		$result = $lib->returnArray($sql);
		
		$exportColumn = array("$i_UserName ($i_UserLogin)",$i_general_class,$i_UsageStartTime,$i_UsageEndTime,$i_UsageHost,$i_UsageDuration);
		
		intranet_closedb();      
		$filename = "UserLoginRecord".date('ymdhis').".csv";
		
		break;
	case 'user':
	case 'group':
		$sql = $log->getLogSql($module);
		$lib = new libdb();
		$result = $lib->returnArray($sql);
		
		$exportColumn = array("Module","ActionType","Description", "input");
		
		intranet_closedb();
		$filename = "ManagementRecord".date('ymdhis')."-".$module.".csv";
		
		break;
	default:		
		break;
}
$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn, "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

?>