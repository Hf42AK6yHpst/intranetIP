<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libcurdlog.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
intranet_opendb();
$linterface = new interface_html();
$log = new libcurdlog();

$CurrentPageArr['AuditLog'] = 1;
$CurrentPage = "Mgmt_Account";

// Search Parameters
$field = isset($_POST["field"])?$_POST["field"]:3;
$order = isset($_POST["order"])?$_POST["order"]:0;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

// other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$classId = isset($_POST["YearClassID"])?$_POST["YearClassID"]:'all';
$status = isset($_POST["status"])?$_POST["status"]:'all';
if($module == "") $module = "user";

$TAGS_OBJ[] = array($Lang['AuditLog']['UserMgmt'],"/home/eAdmin/AccountMgmt/AuditLog/?module=user",$module=="user");
$TAGS_OBJ[] = array($Lang['AuditLog']['GroupMgmt'],"/home/eAdmin/AccountMgmt/AuditLog/?module=group",$module=="group");
$TAGS_OBJ[] = array($Lang['AuditLog']['LoginRecord'],"/home/eAdmin/AccountMgmt/AuditLog/login_record.php",$module=="login");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AuditLog'];

# online help button
$onlineHelpBtn = gen_online_help_btn_and_layer('user','student');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$li = new libdbtable2007($field, $order, $pageNo);

$sql = $log->getLogSql($module);
$li->field_array = array("Module","ActionType", "Description", "input");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = 2;
$li->page_size = $numPerPage;
		
// TABLE COLUMN
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='20%' nowrap='nowrap'>".$li->column(0,"Module")."</td>\n";
$li->column_list .= "<th width='15%' nowrap='nowrap'>".$li->column(1,"Action")."</td>\n";
$li->column_list .= "<th width='45%' nowrap='nowrap'>".$li->column(2,"Description")."</td>\n";
$li->column_list .= "<th width='20%' nowrap='nowrap'>".$li->column(3,"Input")."</td>\n";
$li->column_array = array(0,0,0,0);

$toolbar ="<div class='Conntent_tool'><a href='javascript:goExport()' class='export'>".$button_export."</a></div>";
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<script type="text/javascript">
function goExport(){
	document.form1.action = "export.php?module=<?=$module?>";
	document.form1.submit();
	document.form1.action = "";
}
</script>
<form name="form1" method="post">
	<div class="content_top_tool">
		<?=$toolbar?>
		<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
	<br style="clear:both" />
	</div>
	<div class="table_board">
		<?= $li->display()?>				
	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="RecordType" value=<?=$RecordType?> />
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
	<input type="hidden" name="page_size_change" />
	<input type="hidden" name="miscParameter" />
	<input type="hidden" name="ReportItemID" id="ReportItemID" value="">
</form>
</body>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>