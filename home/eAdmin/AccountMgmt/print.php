<?php
# using: 

########## Change Log [Start] #############
#	Date:	2017-07-14	Icarus
#			fixed the print function of parent, school staff and student
#
#	Date:	2017-06-08 Icarus
#			added house filtering
#			remove the "double quotation mark" of data
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-03-20	Cameron
#			change fax field to date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true for teacher and student
#
#	Date:	2014-11-17	Bill
#			Add Guardian Info
#
#	Date:	2014-08-21	Bill
#			Add Home Tel
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] to print data with column CardID - eEnrolment
#
#	Date: 	2012-08-03 Bill Mak
#			add Nationality, PlaceOfBirth, AdmissionDate Group, Role for Export
#
#	Date:	2010-09-03 Henry Chow
#			Export data according to the filters
#
#	Date:	2010-04-19 YatWoon
#			for "LaiShan" project, add parent/guardian name to student export csv
#
#	Date:	2010-04-16 YatWoon
#			Export staff csv for import: missing check CardID should be include or not
#
########## Change Log [End] #############


$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_opendb();

$lexport = new libexporttext();

if (!isset($default) || $default != 1)
{
     $li = new libdbdump();
     
     # check if there parent / guardian column is selected
     # set prefix if yes
     if(in_array("Parent", $Fields) || in_array("Guardian", $Fields) || in_array("GuardianInfo", $Fields) || in_array("StudentInfo", $Fields) || in_array("Group", $Fields) || in_array("Role", $Fields) )
     {
	     # handle special column
	     foreach($Fields as $k=>$d)
	     {

		  	if($d == "Parent")		{	$WithParent = 1;		unset($Fields[$k]);	}
		  	if($d == "Guardian")	{	$WithGuardian = 1;		unset($Fields[$k]);	}
		  	if($d == "GuardianInfo"){	$WithGuardianInfo = 1;	unset($Fields[$k]);	}
		  	if($d == "StudentInfo")	{	$WithStudentInfo = 1;	unset($Fields[$k]);	}
		  	if($d == "Group")		{	$WithGroup = 1;			unset($Fields[$k]);	}
		  	if($d == "Role")		{	$WithRole = 1;			unset($Fields[$k]);	}
	     }
     }
     
	$li->setFieldArray($Fields);
	$li->setTable("INTRANET_USER");
     	
     if ($TabID == "")
     {
         $conds = "";
     }
     else
     {
         $conds = "RecordType = $TabID AND ";
     }
	
	# class filtering
	if($targetClass!="0" && $targetClass!='') {
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$studentAry = $lclass->storeStudent(0,$targetClass);
		
		if(sizeof($studentAry)>0) {
			/*
			if($TabID==3 && $parentLink!=""){
				$studentConds .= " b.StudentID IN (".implode(',', $studentAry).") AND ";
			} else {
				$conds .= " UserID IN (".implode(',', $studentAry).") AND ";
			}
			*/
			if($TabID==3)
				$studentConds .= " b.StudentID IN (".implode(',', $studentAry).") AND ";
			else 
				$conds .= " UserID IN (".implode(',', $studentAry).") AND ";
		} else {
			if($TabID==3)
				$studentConds .= " b.StudentID IN (-1) AND ";
			else 
				$conds .= " UserID IN (-1) AND ";
		}
	}
	
	# house filtering
	$libgroup = new libgroup();
	$houseArr = $libgroup->returnAllGroupIDAndName("","",4,Get_Current_Academic_Year_ID());
	if (isset($houseID)) {				
		if ($houseID > 0){
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$houseID."'";
			$houseStudentIdAry = Get_Array_By_Key($lexport->returnResultSet($sql), 'UserID');
		
			if($TabID==3)
				$studentConds .= " b.StudentID IN (".implode(',', $houseStudentIdAry).") AND ";
			else 
				$conds .= " UserID IN (".implode(',', $houseStudentIdAry).") AND ";			
		} else if ($houseID == -1) {
			// do nothing 
		} else if($houseID==-2||$houseID==-3){
			// $houseID==-2 => "all house" option => with house students only
			// $houseID==-3 => "without house" option => without house students only
			// Get All House ID in current ac yr
			$houseGroupIDArr = Get_Array_By_Key($houseArr, 'GroupID');
			$houseStudentIdAry=array();
			for($i=0;$i<count($houseArr);$i++){
				//Create new obj for each house
				$groupObj= new libgroup($houseGroupIDArr[$i]);
				$houseStudentIdAry = array_values(array_unique(array_merge($houseStudentIdAry,Get_Array_By_Key($groupObj->Get_Group_User(USERTYPE_STUDENT), 'UserID'))));
			}
			if($houseID==-2){
				$conds.="UserID IN (".implode(',',$houseStudentIdAry).") AND ";
			}else if($houseID==-3){
				$conds.="UserID NOT IN (".implode(',',$houseStudentIdAry).") AND ";
			}
		}		
	}
	

     # parent
     if($TabID==3){
     	if(!isset($notInList) || $notInList=="") {	# only export User List
	     $li2 = new libdb();
	     $sqlParents = "SELECT DISTINCT UserID FROM INTRANET_USER as a, INTRANET_PARENTRELATION as b WHERE $studentConds b.ParentID IS NOT NULL AND b.StudentID IS NOT NULL AND a.UserID=b.ParentID";
	     $r = $li2->returnVector($sqlParents,1);
	   
    	 $parentIDs = implode(",",$r);
	     
//		 if(sizeof($r)>0)
//		 	$conds .= " UserID IN (".implode(',', $r).") AND ";
		 	$conds .= " UserID IN (".(sizeof($r)>0?implode(',', $r):'-1').") AND ";
     	}
     	else {
     		# parent not in list
     		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=3 AND UserID NOT IN (SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION)";
     		$result = $li->returnVector($sql);

			# class filter
     		include_once($PATH_WRT_ROOT."includes/libclass.php");
     		$lclass = new libclass();
     		
     		if(isset($targetClass) && $targetClass!="0") {
	     		$studentIDAry = $lclass->storeStudent('0',$targetClass,Get_Current_Academic_Year_ID());
	     		$addCond = " WHERE StudentID IN (".implode(',',$studentIDAry).")";
     		}
     		
     		//if(sizeof($studentIDAry)>0) {
     			$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION $addCond";
     			$temp = $li->returnVector($sql);

     			if(sizeof($temp)>0)
     				$result = array_merge($result,$temp);
     		//}

     		$conds .= " UserID IN (".implode(',',$result).") AND ";
     	}
		 
	 }

	 # student
	 if($TabID==2) {
	 	/*
	 	if(!isset($notInList) || $notInList=="") {	# only export User List
		 	$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=".Get_Current_Academic_Year_ID();
		 	$temp = $li->returnVector($sql);
		 	if(sizeof($temp)>0) $conds .= " UserID IN (".implode(',',$temp).") AND ";
		}	
	 	*/
	 	if($userList && $notInList) {	// all students
			// no addition condition	
		} else {
			
			$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=".Get_Current_Academic_Year_ID();
		 		$temp = $li->returnVector($sql);
		 		
			if($userList) {		// only export user list (with classes)
		 		if(sizeof($temp)>0) $conds .= " UserID IN (".implode(',',$temp).") AND ";
			} 
			else if($notInList) {	// only export user list (without classes)
				if(sizeof($temp)>0) $conds .= " UserID NOT IN (".implode(',',$temp).") AND ";
			} else {
				// export nothing	
			}
		}
	 }
	 
	 if($recordstatus!="")
	 	$conds .= " RecordStatus='$recordstatus' AND ";
	 	
	 if($TeachingType!="")
	 	$conds.= ($TeachingType==1 || $TeachingType=='S')? " Teaching='$TeachingType' AND " : " (Teaching='0' OR Teaching IS NULL) AND ";
	 
     $li->setFilterSQL("WHERE $conds (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR ChineseName like '%$keyword%' OR EnglishName like '%$keyword%' OR ClassName like '%$keyword%' OR WebSamsRegNo like '%$keyword%')");
	
     $url = "/file/export/eclass-user-".session_id()."-".time().".csv";
	 $export_content = $li->getDumpDataWithSeparator_utf($WithParent, $WithGuardian, 1, $WithStudentInfo, $TabID,$WithGroup, $WithRole, $WithGuardianInfo);
	
	
	# change "StaffCode" to "WebSAMSStaffCode"
	$count = count($export_content[0]);
	for($a=0; $a<$count; $a++) {
		if($export_content[0][$a]=="StaffCode") {
			$export_content[0][$a] = "WebSAMSStaffCode";
			break;	
		}	
	}
	
	# teacher & student use fax field for date of baptism
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] && (($TabID==1) || ($TabID==2))) {
		for($a=0; $a<$count; $a++) {
			if($export_content[0][$a]=="FaxNo") {
				$export_content[0][$a] = "DateOfBaptism";
				break;	
			}
		}
	}	
	
?>
	<table border="01" cellspacing="0" cellpadding="0">
	
	<?
	
	foreach($export_content as $k=>$d) { ?>
	<tr>
		<? foreach($d as $k1=>$d1) { 
			$d1 = str_replace("\"\"", "<!--doubleQuote-->", $d1);
			$d1 = str_replace("\"", "", $d1);
			$d1 = str_replace("<!--doubleQuote-->", "\"", $d1);
			
			if ($d1 == '"') {
				$d1 = '';
			}
			?>
		<td><?=$d1?$d1:"&nbsp;"?></td>
		<? } ?>
	</tr>
	<? }
	 ?>

	</table>
<?
	 
}

else # Export default format
{
     if ($TabID == "")
     {
         $tab_conds = "";
     }
     else
     {
         $tab_conds = "RecordType = $TabID AND ";
     }
     $conds = "WHERE $tab_conds RecordStatus = '$filter' AND (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR ChineseName like '%$keyword%' OR EnglishName like '%$keyword%')";
     if ($TabID == 1)
     {
         $col_count = 13;
         if($special_feature['ava_hkid'])	$col_count++;
         if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) $col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, FirstName, LastName, ChineseName,
             IF(Title IS NULL OR TRIM(Title) = '','',
             CASE Title
                         WHEN 0 THEN 'Mr.'
                         WHEN 1 THEN 'Miss'
                         WHEN 2 THEN 'Mrs.'
                         WHEN 3 THEN 'Ms.'
                         WHEN 4 THEN 'Dr.'
                         WHEN 5 THEN 'Prof.'
                         ELSE '' END),
             Gender, MobileTelNo, TitleEnglish, TitleChinese";
        if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) $sql .= ", CardID ";
		if($special_feature['ava_hkid'])	$sql .= ", HKID ";
		$sql .= " FROM INTRANET_USER $conds ORDER BY EnglishName";
     }
     else if ($TabID == 2)
     {
         $col_count = 15;
         if($special_feature['ava_hkid'])	$col_count++;
         if($special_feature['ava_strn'])	$col_count++;
         if($sys_custom['StudentAccountAdditionalFields']) {
         	$col_count += 5;	
         }
         if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])|| (isset($plugin['eEnrollment'])&& $plugin['eEnrollment'])) $col_count++;
         
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, FirstName, LastName, ChineseName,
             IF(Title IS NULL OR TRIM(Title) = '','',
             CASE Title
                         WHEN 0 THEN 'Mr.'
                         WHEN 1 THEN 'Miss'
                         WHEN 2 THEN 'Mrs.'
                         WHEN 3 THEN 'Ms.'
                         WHEN 4 THEN 'Dr.'
                         WHEN 5 THEN 'Prof.'
                         ELSE '' END),
             Gender, HomeTelNo, MobileTelNo, ";
             if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])|| (isset($plugin['eEnrollment'])&& $plugin['eEnrollment']))
             $sql .= "CardID, ";
             $sql .= "DateOfBirth,
             IF(TRIM(WebSAMSRegNo)!='' AND LEFT(TRIM(WebSAMSRegNo),1)!='#',CONCAT('#',TRIM(WebSAMSRegNo)),TRIM(WebSAMSRegNo)) ";
         if($special_feature['ava_hkid'])	$sql .= ", HKID ";
         if($special_feature['ava_strn'])	$sql .= ", STRN ";
         if($sys_custom['StudentAccountAdditionalFields']) {
         	$sql .= ",NonChineseSpeaking, SpecialEducationNeeds, PrimarySchool, University, Programme ";
         }
         $sql .=" FROM INTRANET_USER $conds ORDER BY ClassName, ClassNumber, EnglishName";
     }
     else
     {
         $col_count = 12;
         if($special_feature['ava_hkid'])	$col_count++;
         $sql = "SELECT UserID, UserLogin, '', UserEmail, EnglishName, FirstName, LastName, ChineseName,
             IF(Title IS NULL OR TRIM(Title) = '','',
             CASE Title
                         WHEN 0 THEN 'Mr.'
                         WHEN 1 THEN 'Miss'
                         WHEN 2 THEN 'Mrs.'
                         WHEN 3 THEN 'Ms.'
                         WHEN 4 THEN 'Dr.'
                         WHEN 5 THEN 'Prof.'
                         ELSE '' END),
             Gender, MobileTelNo, YearOfLeft ";
		if($special_feature['ava_hkid'])	$sql .= ", HKID ";
		$sql .= " FROM INTRANET_USER $conds ORDER BY EnglishName";
     }
     $li = new libdb();
     $row = $li->returnArray($sql);
     
     if ($TabID == 3)
     {
         # Try to find children relation
         $sql = "SELECT b.ParentID, c.UserLogin, c.EnglishName FROM INTRANET_PARENTRELATION as b
                        LEFT OUTER JOIN INTRANET_USER as a ON b.ParentID = a.UserID
                        LEFT OUTER JOIN INTRANET_USER as c ON b.StudentID = c.UserID AND c.RecordType = 2
                        WHERE a.UserID IS NOT NULL AND c.UserID IS NOT NULL
                        ORDER BY a.UserID";
         $raw_relations = $li->returnArray($sql,3);
         $relations = array();
         $i = 0;
         $prev_id = 0;
         #print_r($raw_relations);
         $max_child = 0;
         while ($i < sizeof($raw_relations))
         {
                $temp = array();
                list($id,$login,$eng) = $raw_relations[$i];
                $prev_id = $id;
                while ($prev_id == $id)
                {
                       $temp[] = array($login,$eng);
                       $i++;
                       list($id,$login,$eng) = $raw_relations[$i];
                }
                $relations[$prev_id] = $temp;
         }
         #print_r($relations);
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,StudentLogin1,StudentEngName1,StudentLogin2,StudentEngName2,StudentLogin3,StudentEngName3";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     else if ($TabID == 1)
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,TitleEnglish,TitleChinese";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","TitleEnglish","TitleChinese");
         if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment']))
		{
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		}
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     else if ($TabID == 2)
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Home Tel,Mobile";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Home Tel","Mobile");
         
         if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])|| (isset($plugin['eEnrollment'])&& $plugin['eEnrollment']))
		{
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		}
		$x .= ",DOB,WebSAMSRegNo";
		$exportColumn = array_merge($exportColumn,array("DOB","WebSAMSRegNo"));
		
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			$x .= ",STRN";
			$exportColumn = array_merge($exportColumn, array("STRN"));
         }
         
         if($sys_custom['StudentAccountAdditionalFields']){
         	$x .= ",Non-Chinese Speaking,Special Education Needs,Primary School,University/Institution,Programme ";
         	$exportColumn = array_merge($exportColumn, array("Non-Chinese Speaking"));
         	$exportColumn = array_merge($exportColumn, array("Special Education Needs"));
         	$exportColumn = array_merge($exportColumn, array("Primary School"));
         	$exportColumn = array_merge($exportColumn, array("University/Institution"));
         	$exportColumn = array_merge($exportColumn, array("Programme"));
         }
     }
     else
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,FirstName,LastName,ChineseName,Title,Gender,Mobile,YearOfLeft";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","YearOfLeft");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
     }
     $x .= "\n";
     
     for($i=0; $i<sizeof($row); $i++)
     {
         $delim = "";
         $row_length = ($TabID==3? 11: $col_count);
         $child_data = $relations[$row[$i][0]];
         if($TabID!=3){
	         	for($j=1; $j<$row_length; $j++){
		         	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
		             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
		             $x .= $delim.$row[$i][$j];		             
		             $delim = ",";
	      		}
				#$x .= implode(",", $row[$i]);
				$utf_rows[] = $utf_row;
				unset($utf_row);
				$x .="\n";
	     }
	     # parent
		 else{
			 	$child_data = $relations[$row[$i][0]];
			 	
			 	# Linked
			 	if($parentLink==1){
				 	if(sizeof($child_data)>0){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}
// 	            		$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
// 	            		$x .="\n";	 	
					}
				}
				# No Linked
			 	else if($parentLink==2){
				 	if(sizeof($child_data)<=0){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
// 	      				$x .="\n";
// 	      				$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
					}
				}
			 	# ALL 
			 	else if($parentLink==""){
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}	 
// 	            		$utf_rows[] = $utf_row;
// 	            		unset($utf_row);	
// 	            		$x .="\n";
				}
				
				if($special_feature['ava_hkid'])
	         {
		         # add skip empty cell
		         for($k=sizeof($child_data); $k<3;$k++)	
		         {
			         $x .= ",\" \",\" \"";
		         	$utf_row[] = " ";
		         	$utf_row[] = " ";
		         }
		         
		         $x .= ",\"".intranet_undo_htmlspecialchars($row[$i]['HKID'])."\"";
		         $utf_row[] = intranet_undo_htmlspecialchars($row[$i]['HKID']);
	         }
	         $utf_rows[] = $utf_row;
	         unset($utf_row);	
		     $x .="\n";
	     }
     }
     
     # print with Table format
?>
	<table border="01" cellspacing="0" cellpadding="0">
	
	<tr>
	<? foreach($exportColumn as $k=>$d) { ?>
		<td><?=$d?></td>
	<? } ?>
	</tr>
	
	<? foreach($utf_rows as $k=>$d) { ?>
	<tr>
		<? foreach($d as $k1=>$d1) { 
			$d1 = str_replace("\"\"", "<!--doubleQuote-->", $d1);
			$d1 = str_replace("\"", "", $d1);
			$d1 = str_replace("<!--doubleQuote-->", "\"", $d1);
			
			if ($d1 == '"') {
				$d1 = '';
			}
		?>
		<td><?=$d1?$d1:"&nbsp;"?></td>
		<? } ?>
	</tr>
	<? } ?>

</table>
<?     
}


intranet_closedb();
?>
