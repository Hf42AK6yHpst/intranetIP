<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

# set cookies
$arrCookies[] = array("ck_usermgmt_page_size", "numPerPage");
$arrCookies[] = array("ck_usermgmt_page_no", "pageNo");
$arrCookies[] = array("ck_usermgmt_page_order", "order");
$arrCookies[] = array("ck_usermgmt_page_field", "field");

$arrCookies[] = array("ck_usermgmt_recordstatus", "Recordstatus");
$arrCookies[] = array("ck_usermgmt_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$libdhl = new libdhl();

if ($page_size_change == 1)
{
    setcookie("ck_usermgmt_page_size", $numPerPage, 0, "", "", 0);
    $ck_usermgmt_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_usermgmt_page_size) && $ck_usermgmt_page_size != "") $page_size = $ck_usermgmt_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$filterMap = array('GetDBQuery'=>1,'libdbtable'=>$li);
if(isset($RecordStatus) && $RecordStatus != ''){
	$filterMap['RecordStatus'] = $RecordStatus;
}
if(isset($Keyword) && trim($Keyword)!=''){
	$filterMap['Keyword'] = $Keyword;
}
$db_query_info = $libdhl->getUserRecords($filterMap);

$li->field_array = $db_query_info[0];
$li->sql = $db_query_info[1];
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = $db_query_info[3];
$li->wrap_array = $db_query_info[4];
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 0;
}

foreach($db_query_info[2] as $column_def)
{
	$li->column_list .= $column_def;
}

$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);

# Record Status 
$recordStatusMenu = "<select name=\"RecordStatus\" id=\"RecordStatus\" onchange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($RecordStatus) || $RecordStatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($RecordStatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($RecordStatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "</select>";


$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

//$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
if(isset($_SESSION['DHL_USERMGMT_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['DHL_USERMGMT_RETURN_MSG'];
	unset($_SESSION['DHL_USERMGMT_RETURN_MSG']);
}
if($xmsg != '' &&  isset($Lang['General']['ReturnMessage'][$xmsg]))
{
	$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
}
$linterface->LAYOUT_START($ReturnMsg);
?>
<br />
<form name="form1" method="post" action="">
<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("Keyword", $Keyword)?>
	<br style="clear:both" />
</div>
<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<?=$recordStatusMenu?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		<a href="javascript:changeFormAction(document.form1,'user_id[]','../email_password.php')" class="tool_email"><?=$Lang['AccountMgmt']['SendResetPasswordEmail']?></a>
		<? if($recordstatus!=1) {?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<? } ?>
		<? if($recordstatus=="" || $recordstatus==1) {?>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<? } ?>		
		<a href="javascript:checkRemove(document.form1,'user_id[]','remove.php','<?=$Lang['AccountMgmt']['DeleteUserWarning']?>')" class="tool_delete"><?=$button_delete?></a>
		</div>
	</td>
</tr>
</table>
<?= $li->display()?>
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/StaffMgmt/index.php">
</form>
<script language="javascript">
function reloadForm() 
{
	document.form1.action = "index.php";
	document.form1.submit();
}

function goEdit(id) 
{
	document.form1.action = "edit.php";
	var objs = document.getElementsByName('user_id[]');
	for(var i=0;i<objs.length;i++){
		if(objs[i].value == id){
			objs[i].checked = true;
		}else{
			objs[i].checked = false;
		}
	}
	document.form1.submit();
}

function newUser() 
{
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() 
{
	document.form1.action = "dhl_user_export.php";
	document.form1.submit();
	document.form1.action = "index.php";
}

function goImport() {
	window.location.href ="dhl_user_import.php";
}

function changeFormAction(obj,element,url,type) 
{
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>