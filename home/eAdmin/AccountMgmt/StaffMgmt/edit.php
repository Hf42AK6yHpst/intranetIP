<?php
# using: Henry

###
### 		!!!!! Note: Required to deploy with libuser.php if upload this file to client BEFORE ip.2.5.9.1.1	!!!!!	
### 		!!!!! Note: Please also upload templates/edit.tmpl.php when update this file since 2015-12-17 		!!!!! 
###

############# Change Log
#
#   Date:   2019-08-08 Paul
#           handle Google SSO quota checking by try / catch
#
#   Date:   2019-04-30 Cameron
#           apply IntegerSafe() to $uid
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-08-22 (Cameron) [ip.2.5.9.10.1]: apply stripslashes to Grade and Duty for HKPF, apply stripslashes to Remark
#
#   Date:   2018-08-21 (Cameron) [ip.2.5.9.10.1]: by pass gender checking for HKPF
#
#   Date:   2018-08-15 (Henry)
#           fixed XSS
#
#   Date:   2018-08-07 (Cameron) [ip.2.5.9.10.1]: add Grade and Duty for HKPF
#
#   Date:   2018-05-15 Isaac
#           Added Last Modified username and time.
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#
#	Date:	2017-12-22 Cameron
#			configure for Oaks refer to Amway
#
#	Date:	2017-10-19 Ivan [ip.2.5.9.1.1]
#			Changed default personal photo path to call function by libuser.php
#
#	2016-11-21 (HenryHM) [ip.2.5.8.1.1] 
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	2016-10-04 (Carlos) [ip.2.5.8.1.1] 
#			$sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
#
#	2016-09-28 (Ivan) [ip.2.5.7.10.1]
#			Added teaching = 'S' supply teacher logic
#
#	2016-06-10 (Cara)
#			Add creator info($nameDisplay, $InputBy, $DateInput)
#
#	2015-12-17 (Cameron)
#			move layout to template folder to customize layout [Amway #P89445]
#
#	2015-03-19 Cameron
#			use fax field for recording date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true
#  
#	2014-05-23 (Carlos): Fix $sys_custom['iMailPlus']['EmailAliasName'] js checking
#	2014-02-20 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - add specific error msg for each reason
#	2013-12-12 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - user can input its own iMail plus user name
#
#	2013-08-13	(Carlos): hide WEBSAMS staff code for KIS
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	2012-06-08 (Carlos): added password remark
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date :	2011-12-20	(Yuen)
#			Display personal  photos
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

if($sys_custom['DHL']){
	include("./dhl_user.php");
	exit;
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$uid = IntegerSafe($uid);
//if(sizeof($_POST)==0) 
if(empty($uid))
{
	header("Location: index.php");	
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

if(isset($error) && $error!="")
	$errorAry = explode(',', $error);
	
$laccount = new libaccountmgmt();
$linterface = new interface_html();

$userInfo = $laccount->getUserInfoByID($uid);
//if($pwd=="") $pwd = $userInfo['UserPassword'];
if($userlogin=="") $userlogin = $userInfo['UserLogin']; 
if($email=="") $email = isset($UserEmail)? $UserEmail : $userInfo['UserEmail'];
if($teaching=="") $teaching = $userInfo['Teaching'];
if($status=="") $status = $userInfo['RecordStatus'];
if($smartcardid=="") $smartcardid = $userInfo['CardID'];
if($staffCode=="") $staffCode = $userInfo['StaffCode'];
if($barcode=="") $barcode = $userInfo['Barcode'];
if($grade=="") $grade = stripslashes($userInfo['Grade']);
if($duty=="") $duty = stripslashes($userInfo['Duty']);
if($engname=="") $engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
if($chiname=="") $chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
if($nickname=="") $nickname = $userInfo['NickName'];
if($gender=="") $gender = $userInfo['Gender'];
if($chiTitle=="") $chiTitle = $userInfo['TitleChinese'];
if($engTitle=="") $engTitle = $userInfo['TitleEnglish'];
if($address=="") $address = $userInfo['Address'];
if($country=="") $country = $userInfo['Country'];
if($homePhone=="") $homePhone = $userInfo['HomeTelNo'];
if($officePhone=="") $officePhone = $userInfo['OfficeTelNo'];
if($mobilePhone=="") $mobilePhone = $userInfo['MobileTelNo'];
if($faxPhone=="") $faxPhone = $userInfo['FaxNo'];
if($remark=="") $remark = stripslashes($userInfo['Remark']);
if($sys_custom['iMail']['UserDisplayOrder']){
	if($DisplayOrder == "") $DisplayOrder = $userInfo['DisplayOrder'];
}
if(!$ssoservice["Google"]["Valid"]){
	if($googleUserLogin == "") $googleUserLogin = $userInfo['GoogleUserLogin'];
}
if ($intranet_session_language!="gb") {
	$chi_title_array = array($i_general_Principal,$i_general_VPrincipal,$i_general_Director);
}
else {
	$chi_title_array = array();
}
$sql = "SELECT DISTINCT TitleChinese FROM INTRANET_USER WHERE TitleChinese IS NOT NULL AND TitleChinese !='' ORDER BY TitleChinese";
$temp = $laccount->returnVector($sql);
if (sizeof($temp)!=0) {
	$chi_title_array = array_merge($chi_title_array, $temp);
	$chi_title_array = array_unique($chi_title_array);
	$chi_title_array = array_values($chi_title_array);
}
$sql = "SELECT DISTINCT TitleEnglish FROM INTRANET_USER WHERE TitleEnglish IS NOT NULL AND TitleEnglish !='' ORDER BY TitleEnglish";
$eng_title_array = $laccount->returnVector($sql);
if (sizeof($eng_title_array)!=0) {
	$select_eng_title = getSelectByValue($eng_title_array, "onChange=this.form.engTitle.value=this.value");
} else
{
	$select_eng_title = "";
}
$select_chi_title = getSelectByValue($chi_title_array, "onChange=this.form.chiTitle.value=this.value");

$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

# imail gamma
if($plugin['imail_gamma'])
{
	if(!$IMapEmail = $laccount->getIMapUserEmail($uid))
	{
		$userInfo = $laccount->getUserInfoByID($uid);
		$IMapEmail = $userInfo['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$disabled = "checked";
	}	
	else
	{
		$enabled = "checked";
	}
	
	if($sys_custom['iMailPlus']['EmailAliasName']){
		$sql = "SELECT ImapUserLogin FROM INTRANET_USER WHERE UserID='$uid'";
		$record = $laccount->returnVector($sql);
		$CurrentImapUserLogin = $record[0];
	}
	
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_TEACHER);
	$EmailQuota = $laccount->getUserImailGammaQuota($uid);
	$EmailQuota = $EmailQuota?$EmailQuota:$DefaultQuota[1];
}

if($ssoservice["Google"]["Valid"]){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	
	$google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
	$sso_db = new libSSO_db();
	$lib_google_sso = new libGoogleSSO();
	foreach((array)$google_apis as $google_api){
		$config_index = $google_api->getConfigIndex();
		$gmail_variable = 'gmail_'.$config_index;
		$user = $sso_db->getSSOIdentifier($uid, $userInfo['UserLogin'].'@'.$google_api->getDomain());
		$$gmail_variable = count($user) > 0 && $user[0]['SSOAccountStatus']!='n' && $user[0]['SSOAccountStatus']!='x';
		try{
			$remaining_quota[$config_index] = $lib_google_sso->getRemainingQuota($google_api);
		}catch(Exception $e){
			$remaining_quota[$config_index] = false;
		}
		$total_quota[$config_index] = $ssoservice["Google"]['api'][$config_index]['quota'];
		try{
			$account_exists[$config_index] = ($google_api->getGoogleAccountByEmail($userInfo['UserLogin'].'@'.$google_api->getDomain()) == null ? false : true);
		}catch(Exception $e){
			$account_exists[$config_index] = false;
		}
	}
}

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goBack()");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");



$li = new libuser($uid);
$inputBy = $li->InputBy;
$DateInput = $li->DateInput;
$ModifyBy = $li->ModifyBy;
$DateModified = $li->DateModified;

$SettingArr = $li->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$li->RecordType));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$li->RecordType];
if ($PasswordLength<6) $PasswordLength = 6;

$userObj = new libuser($inputBy);
if(empty($userObj->UserID)){
    $CreatorUserinfoAry= $userObj->ReturnArchiveStudentInfo($inputBy);
    $CreatorEnglishName = $CreatorUserinfoAry['EnglishName'];
    $CreatorChineseName = $CreatorUserinfoAry['ChineseName'];
}else{
$CreatorEnglishName = $userObj->EnglishName;
$CreatorChineseName = $userObj->ChineseName;
}
if($CreatorChineseName){
    $nameDisplay = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
} else{
    $nameDisplay=$CreatorEnglishName;
}

$ModifyUserObj = new libuser($ModifyBy);
if(empty($ModifyUserObj->UserID)){
    $ModifyUserinfoAry= $ModifyUserObj->ReturnArchiveStudentInfo($ModifyBy);
    $ModifierEnglishName = $ModifyUserinfoAry['EnglishName'];
    $ModifierChineseName = $ModifyUserinfoAry['ChineseName'];
}else{
$ModifierEnglishName = $ModifyUserObj->EnglishName;
$ModifierChineseName = $ModifyUserObj->ChineseName;
}
if($ModifierChineseName){
$ModifiernameDisplay = Get_Lang_Selection($ModifierChineseName, $ModifierEnglishName);
} else{
    $ModifiernameDisplay=$ModifierEnglishName;
}
########## Personal Photo
//$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
$photo_personal_link = $userObj->returnDefaultPersonalPhotoPath();
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

########## KIS CEES ADDITIONAL INFO
if($plugin['SDAS_module']['KISMode']){
	$sql = "SELECT InfoField, InfoValue FROM INTRANET_USER_ADDITIONAL_INFO WHERE UserID = '$uid'";
	$additionalInfo = $li->returnArray($sql);
	$additionalInfoAry = array();
	foreach($additionalInfo as $aInfo){ $additionalInfoAry[$aInfo['InfoField']] = $aInfo['InfoValue'];}
	$position = $additionalInfoAry['Position'];
	$contractType = $additionalInfoAry['ContractType'];
	$contractStartDate = $additionalInfoAry['ContractStartDate'];
	$contractEndDate = $additionalInfoAry['ContractEndDate'];
}


$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function goSubmit(thisUrl) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>	
	var obj = document.form1;
	//if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin," ".$i_UserPassword?>")) return false;
	//if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value!="" && (obj.pwd.value != obj.pwd2.value))
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword']){ ?> 	
	if(obj.pwd.value != ''){
		var check_password_result = CheckPasswordCriteria(obj.pwd.value,'<?=$li->UserLogin?>',<?=$PasswordLength?>);
		if(check_password_result.indexOf(1) == -1){
			var password_warning_msg = '';
			for(var i=0;i<check_password_result.length;i++){
				password_warning_msg += password_warnings[check_password_result[i]] + "\n";
			}
			alert(password_warning_msg);
			obj.pwd.focus();
			return false;
		}
	}
<?php } ?>	
<?php if (!$sys_custom['project']['CourseTraining']['IsEnable']): ?>	
	var identityValid = true;	
	if(obj.teaching[0].checked==false && obj.teaching[1].checked==false) {
		<?php if ($plugin['SLRS']) { ?>
	        if(obj.teaching[2].checked==false) {
	        	identityValid = false;
	        }
        <?php } else { ?>
			identityValid = false;
		<?php } ?>
	}
	
	if (identityValid == false) {
		alert("<?=$i_alert_pleaseselect." ".$i_identity?>");
		return false;
	}
<?php endif; ?>
	
<?php if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){ ?>	
	if(obj.UseImapUserLogin.value == 1) {
		if($.trim(obj.ImapUserLogin.value) == ""){
			obj.ImapUserLogin.focus();
			alert("<?=$i_alert_pleasefillin." ".$Lang['Gamma']['Email']?>");
			return false;
		}
	}
<?php } ?>
	
	// check email
	if(!check_text(obj.UserEmail, "<?php echo $i_alert_pleasefillin.$Lang['Gamma']['UserEmail']; ?>."))  return false;
	if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
	
	if(obj.status[0].checked==false && obj.status[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
<?php if (!$sys_custom['project']['HKPF']):?>	
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
<?php endif;?>	
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
	if(obj.faxPhone.value!="") {
		if(!check_date(obj.faxPhone,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
<?php
	}
?>	
<?php if($ssoservice["Google"]["Valid"]){ ?>
	if($('#div_gmail .is_display_options').is(':checked')){
		var obj_google_radio_buttons = $('div#div_gmail input[type="radio"]');
								
		var is_checked = false;
		obj_google_radio_buttons.each(function(){
			if($(this).is(':checked')){
				is_checked = true;
			}
		});
		
		if(!is_checked){
			alert('<?php echo Get_Lang_Selection('請選擇 G Suite 帳戶', 'Please choose a G Suite account'); ?>')
			return false;
		}
	}
<?php } ?>
	obj.action = thisUrl;
	obj.submit();
}

function goBack() {
	document.form1.action = "index.php";
	document.form1.submit();
}

function ToggleImapUserLogin(onOff)
{
	if(onOff) {
		var old_imap_email = $("input#OldImapUserEmail").val();
		var email_parts = old_imap_email.split('@');
		$("span#IMapUserEmail").hide();
		if(email_parts.length>0){
			$("#ImapUserLogin").val(email_parts[0]);
		}
		$("#ImapUserLogin").show();
		$("#ImapUserLoginDomain").show();
		$("#BtnCancelImapUserLogin").show();
		$("#BtnEditImapUserLogin").hide();
		$("input#UseImapUserLogin").val(1);
	}else{
		$("span#IMapUserEmail").show();
		$("#ImapUserLogin").hide();
		$("#ImapUserLoginDomain").hide();
		$("#BtnCancelImapUserLogin").hide();
		$("#BtnEditImapUserLogin").show();
		$("input#UseImapUserLogin").val(0);
	}
}
<?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>
function restrictInteger(obj)
{
	var val = $.trim(obj.value);
	var num = parseInt(val);
	if(isNaN(num) || val == ''){
		num = 0;
	}
	obj.value = num;
}
<?php } ?>
</script>


<form name="form1" method="post" action="">
<?php
    if ($sys_custom['project']['CourseTraining']['IsEnable']) {
		include("templates/edit_amway.tmpl.php");
	}
	else {
		include("templates/edit.tmpl.php");
	}
?>
<input type="hidden" name="skipStep2" id="skipStep2" value="">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
<input type="hidden" name="TeachingType" id="TeachingType" value="<?=cleanHtmlJavascript($TeachingType)?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=cleanHtmlJavascript($recordstatus)?>">
<input type="hidden" name="keyword" id="keyword" value="<?=cleanHtmlJavascript($keyword)?>">
<input type="hidden" name="original_pass" id="original_pass" value="<?=$userInfo['UserPassword']?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>