<?php
// Editing by 
/*
 * 2020-01-17 (Bill): pass $type to upsertUserRecord() for import fields handling [2020-0117-1616-21226]
 * 2017-07-20 (Carlos): Added warning message "No new record for importing." there are no new joiner for new join import type.
 * 2017-06-22 (Carlos): For [New joiner] import type, just ignore existing users and let the import to pass.
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
//include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//$laccount = new libaccountmgmt();
$linterface = new interface_html();
//$lo = new libgrouping();
//$lrole = new librole();
$libdhl = new libdhl();
$limport = new libimporttext();
$lfs = new libfilesystem();

if(!isset($step) && $step == '' && !in_array($step,array(0,1,2))){
	$step = 0;
}

if(!isset($type) || $type == '' || !in_array($type,array('NewJoiner','Movement','Leaver'))){
	$type = 'NewJoiner';
}

// Step 1 - Upload csv file
if($step == 0)
{
	/*
	### Import Remarks
	$ImportRemarkArr = $Lang['AccountMgmt']['ImportRemarks'];
	if($TabID==TYPE_STUDENT) 
		$ImportRemarkArr[] = $Lang['AccountMgmt']['ImportRemarks2'];
		
	$ImportRemark = '<table cellpadding=0 cellspacing=0 border=0>';	
	for($i=0; $i < count($ImportRemarkArr); $i++)
		$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.($i+1).'. </td><td>'.$ImportRemarkArr[$i].'</td></tr>';	 
	$ImportRemark .= '</table>';
	*/
	$type_ary = array('NewJoiner','Movement','Leaver');
	$csvFile = array();
	$csv_format = array();
	
	foreach($type_ary as $type_key)
	{
		$csvFile[$type_key] = "<a class='tablelink' href='dhl_import_user_sample.php?type=".$type_key."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
		$csv_format[$type_key] = '';
		for($i=0; $i<count($Lang['DHL']['ImportAccountColumnDefinitions_'.$type_key]); $i++)
		{
			$csv_format[$type_key] .= $Lang['General']['ImportArr']['Column'].' '.($i+1).' : '.($Lang['DHL']['ImportAccountColumnDefinitions_'.$type_key][$i][2]?'<span class="red">*</span>':''). $Lang['DHL']['ImportAccountColumnDefinitions_'.$type_key][$i][0].' - '.$Lang['DHL']['ImportAccountColumnDefinitions_'.$type_key][$i][1];
			if($Lang['DHL']['ImportAccountColumnDefinitions_'.$type_key][$i][3] != ''){
				$csv_format[$type_key] .= $Lang['DHL']['ImportAccountColumnDefinitions_'.$type_key][$i][3];
			}
			$csv_format[$type_key] .= '<br />'."\n";
		}
	}
}

// Step 2 - Check data and confirm
if($step == 1)
{
	$file_format = $libdhl->getImportUserHeaderColumns($type);
	
	### CSV Checking
	$name = $_FILES['userfile']['name'];
	$ext = strtoupper($lfs->file_ext($name));
	
	if(!($ext == ".CSV" || $ext == ".TXT"))
	{
		$_SESSION['DHL_USERMGMT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['WrongFileFormat'];
		intranet_closedb();
		header("Location: ".$_SERVER['SCRIPT_NAME']);
		exit();
	}

	$data = $limport->GET_IMPORT_TXT($userfile);
	$csv_header = array_shift($data);
	$data_size = count($data);
	
	# check csv header
	$format_wrong = false;
	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($csv_header[$i] != $file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	if($format_wrong)
	{
		$_SESSION['DHL_USERMGMT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['WrongCSVHeader'];
		intranet_closedb();

		header("Location: ".$_SERVER['SCRIPT_NAME']);
		exit();
	}

	if(empty($data))
	{
		$_SESSION['DHL_USERMGMT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['CSVFileNoData'];
		intranet_closedb();

		header("Location: ".$_SERVER['SCRIPT_NAME']);
		exit();
	}
	
	$data_ary = array(); // valid data rows
	$remark_ary = array(); // line number to remark
	$error_ary = array(); // line number to error msg
	$added_ary = array(); // added userlogin array
	
	$users = $libdhl->getUserRecords(array());
	$user_size = count($users);
	$userloginToUser = array();
	$emailToUser = array();
	for($i=0; $i<$user_size; $i++){
		$userloginToUser[$users[$i]['UserLogin']] = $users[$i];
		if($users[$i]['UserEmail'] != ''){
			$emailToUser[$users[$i]['UserEmail']] = $users[$i];
		}
	}
	
	$organizations = $libdhl->getDepartmentRecords(array('OrderByDivisionFirst'=>1,'DoNotGroup'=>1));
	$organization_size = count($organizations);
	$companyCodeToRecord = array();
	$divisionCodeToRecord = array();
	$departmentCodeToRecord = array();
	$multiCodeToRecord = array();
	for($i=0; $i<$organization_size; $i++)
	{
		$company_code = $organizations[$i]['CompanyCode'];
		$division_code = $organizations[$i]['DivisionCode'];
		$department_code = $organizations[$i]['DepartmentCode'];
		
		$companyCodeToRecord[$company_code] = $organizations[$i];
		$divisionCodeToRecord[$division_code] = $organizations[$i];
		$departmentCodeToRecord[$department_code] = $organizations[$i];
		
		if(!isset($multiCodeToRecord[$company_code])){
			$multiCodeToRecord[$company_code] = array();
		}
		if(!isset($multiCodeToRecord[$company_code][$division_code])){
			$multiCodeToRecord[$company_code][$division_code] = array();
		}
		if(!isset($multiCodeToRecord[$company_code][$division_code][$department_code])){
			$multiCodeToRecord[$company_code][$division_code][$department_code] = $organizations[$i];
		}
	}
	
	$x = '';
	
	//debug_pr($data);
	//debug_pr($userloginToUser);
	//debug_pr($multiCodeToRecord);
	
	for($i=0; $i<$data_size; $i++)
	{
		if($type == 'NewJoiner')
		{
			list($userlogin, $password, $user_email, $english_name, $company_code, $division_code, $department_code, $join_date, $terminated_date, $status) = $data[$i];
		}
		else if($type == 'Movement')
		{
			list($userlogin, $password, $user_email, $english_name, $company_code, $division_code, $department_code, $effective_date, $status) = $data[$i];
		}
		else if($type == 'Leaver')
		{
			list($userlogin, $password, $user_email, $english_name, $company_code, $division_code, $department_code, $join_date, $terminated_date, $status) = $data[$i];
		}
		$line = $i+2;
		
		$error = array();
		
		if($userlogin == ''){
			$error[] = str_replace('<!--FIELDNAME-->','UserLogin',$Lang['DHL']['ImportErrors']['BlankValue']);
		}else if(in_array($userlogin, $added_ary)){
			$error[] = str_replace('<!--FIELDNAME-->','UserLogin',$Lang['DHL']['ImportErrors']['DuplicatedImportRecord']);
		}else{
			if($type != 'NewJoiner' && !isset($userloginToUser[$userlogin])){
				$error[] = str_replace('<!--FIELDVALUE-->',$userlogin,$Lang['DHL']['ImportErrors']['UserNotFound']);
			}
			/*else if($type == 'NewJoiner' && isset($userloginToUser[$userlogin]))
			{
				$error[] = str_replace('<!--FIELDVALUE-->',$userlogin,$Lang['DHL']['ImportErrors']['FieldUsed']);
			}
			*/
		}
		$is_new = isset($userloginToUser[$userlogin])? false : true;
		
		$remark_ary[$line] = $is_new? $Lang['DHL']['ImportCreateNewUser'] : $Lang['DHL']['ImportUpdateExistingUser'];
		
		if($is_new && $password == ''){
			$error[] = str_replace('<!--FIELDNAME-->','Password',$Lang['DHL']['ImportErrors']['NewUserMustInputPassword']);
		}
		
		if($user_email == ''){
			//$error[] = str_replace('<!--FIELDNAME-->','UserEmail',$Lang['DHL']['ImportErrors']['BlankValue']);
		}else if(!intranet_validateEmail($user_email)){
			$error[] = 'UserEmail: invalid email.';
		}else if($is_new && isset($emailToUser[$user_email])){
			$error[] = 'UserEmail: email is being used by another user.';
		}
		
		if($english_name == ''){
			$error[] = str_replace('<!--FIELDNAME-->','EnglishName',$Lang['DHL']['ImportErrors']['BlankValue']);
		}
		
		if($company_code == ''){
			$error[] = str_replace('<!--FIELDNAME-->','Company',$Lang['DHL']['ImportErrors']['BlankValue']);
		}else if(!isset($companyCodeToRecord[$company_code])){
			$error[] = str_replace('<!--FIELDNAME-->','Company',$Lang['DHL']['ImportErrors']['CodeNotFound']);
		}
		
		if($division_code == ''){
			$error[] = str_replace('<!--FIELDNAME-->','Division',$Lang['DHL']['ImportErrors']['BlankValue']);
		}else if(!isset($divisionCodeToRecord[$division_code])){
			$error[] = str_replace('<!--FIELDNAME-->','Division',$Lang['DHL']['ImportErrors']['CodeNotFound']);
		}
		
		if($department_code == ''){
			$error[] = str_replace('<!--FIELDNAME-->','Department',$Lang['DHL']['ImportErrors']['BlankValue']);
		}else if(!isset($departmentCodeToRecord[$department_code])){
			$error[] = str_replace('<!--FIELDNAME-->','Department',$Lang['DHL']['ImportErrors']['CodeNotFound']);
		}
		
		if($company_code != '' && $division_code != '' && $department_code != ''){
			if(!isset($multiCodeToRecord[$company_code][$division_code][$department_code])){
				$error[] = str_replace('<!--FIELDNAME-->','Department',$Lang['DHL']['ImportErrors']['InvalidDepartment']);
			}
		}
		
		if(in_array($type,array('NewJoiner','Leaver')))
		{
			if($join_date == ''){
				$error[] = str_replace('<!--FIELDNAME-->','JoinDate',$Lang['DHL']['ImportErrors']['BlankValue']);
			}else if(!checkDateIsValidFor2Format($join_date)){
				$error[] = str_replace('<!--FIELDNAME-->','JoinDate', $Lang['DHL']['ImportErrors']['InvalidDateFormat']);
			}else{
				$join_date = getDefaultDateFormat($join_date);
			}
		}
		
		if($type == 'Movement'){
			if($effective_date == ''){
				$error[] = str_replace('<!--FIELDNAME-->','EffectiveDate',$Lang['DHL']['ImportErrors']['BlankValue']);
			}else if(!checkDateIsValidFor2Format($effective_date)){
				$error[] = str_replace('<!--FIELDNAME-->','EffectiveDate', $Lang['DHL']['ImportErrors']['InvalidDateFormat']);
			}else{
				$effective_date = getDefaultDateFormat($effective_date);
			}
		}
		
		if(in_array($type,array('NewJoiner','Leaver')))
		{
			if($type == 'Leaver' && $terminated_date == '')
			{
				$error[] = str_replace('<!--FIELDNAME-->','TerminatedDate',$Lang['DHL']['ImportErrors']['BlankValue']);
			}
			else
            {
				if($terminated_date != ''){
					if(!checkDateIsValidFor2Format($terminated_date)){
						$error[] = str_replace('<!--FIELDNAME-->','TerminatedDate', $Lang['DHL']['ImportErrors']['InvalidDateFormat']);
					}else{
						$terminated_date = getDefaultDateFormat($terminated_date);
					}
				}
			}
		}
		
		if($status == ''){
			$error[] = str_replace('<!--FIELDNAME-->','Status',$Lang['DHL']['ImportErrors']['BlankValue']);
		}else if(!in_array($status,array('1','0'))){
			$error[] = str_replace('<!--FIELDNAME-->','Status',$Lang['DHL']['ImportErrors']['InvalidStatus']);
		}
		
		if(count($error)>0){
			$error_ary[$line] = $error;
		}else{
			$added_ary[] = $userlogin;
			$department_field = $type == 'Movement' ? 'MoveToDepartmentID' : 'DepartmentID';
			if($type == 'NewJoiner' && isset($userloginToUser[$userlogin]))
			{
				// skip updating existing users for New joiner type
			}
			else
            {
				$data_ary[] = array('user_id'=>$is_new?'':$userloginToUser[$userlogin]['UserID'],
									'UserLogin'=>$userlogin,
									'UserEmail'=>$user_email,
									'UserPassword'=>$password,
									'EnglishName'=>$english_name,
									'JoinDate'=>$join_date,
									'EffectiveDate'=>$effective_date,
									'TerminatedDate'=>$terminated_date,
									'RecordStatus'=>$status,
									$department_field => $multiCodeToRecord[$company_code][$division_code][$department_code]['DepartmentID']
                                );
			}
		}
		
		$rowcss = " class=\"".($line %2 == 0? "tablebluerow2":"tablebluerow1")."\" ";
		$x .= "	<tr $rowcss>";
		$x .= "		<td class=\"tabletext\" valign=\"top\">".($line)."</td>";
		$x .= "		<td class=\"tabletext\" valign=\"top\">".($userlogin)."</td>";
		$x .= "		<td class=\"tabletext\" valign=\"top\">".($remark_ary[$line])."</td>";
		$x .= "		<td class=\"tabletext red\">";
		$x .= isset($error_ary[$line]) ? implode("<br />",$error_ary[$line]) : "&nbsp;";
		$x .= "		</td>";
		$x .= "	</tr>";
	}	
	
	$serialized_data = serialize($data_ary);
	$base64encoded_data = base64_encode($serialized_data);
	$hidden_fields = '<input type="hidden" name="data" id="data" value="'.$base64encoded_data.'" />';
}

// Step 3 - Do update
if($step == 2)
{
	$lauth = new libauth();
	$lu = new libuser();
	
	$data_ary = unserialize(base64_decode($data));
	//debug_pr($data_ary);
	$data_size = count($data_ary);
	
	$successAry = array();
	$failAry = array();

	for($i=0; $i<$data_size; $i++)
	{
	    // [2020-0117-1616-21226]
        // $uid = $libdhl->upsertUserRecord($data_ary[$i]);
		$uid = $libdhl->upsertUserRecord($data_ary[$i], $type);

		if($uid != '' && $uid > 0)
		{
			if($type != 'Movement')
			{
				$department_id = trim($data_ary[$i]['DepartmentID']);
				
				$libdhl->deleteDepartmentUserRecord('', $uid);
				$libdhl->insertDepartmentUserRecord($department_id, array($uid));
			}

			# Log down two-way hashed/encrypted password
			if(trim($data_ary[$i]['UserPassword'])!='')
			{
				$lauth->UpdateEncryptedPassword($uid, trim($data_ary[$i]['UserPassword']));
			}
			
			$lu->synUserDataToModules($uid);
			
			$successAry[] = $data_ary[$i]['UserLogin'];
		}
		else
        {
			$failAry[] = $data_ary[$i]['UserLogin'];
		}
		
		if($i % 30 == 0){
			usleep(10);
		}
	}

	$libdhl->auotActivateSuspendUserAccounts();
}

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['DHL']['ImportStaffAccounts']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['DHL']['ImportStaffAccounts'], "");

$STEPS_OBJ[] = array($i_general_select_csv_file, $step==0 || $step==''? 1 : 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], $step==1?1: 0);
$STEPS_OBJ[] = array($i_general_imported_result, $step==2?1:0);

// $MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
if(isset($_SESSION['DHL_USERMGMT_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['DHL_USERMGMT_RETURN_MSG'];
	unset($_SESSION['DHL_USERMGMT_RETURN_MSG']);
}
if($xmsg != '' &&  isset($Lang['General']['ReturnMessage'][$xmsg]))
{
	$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
}
$linterface->LAYOUT_START($ReturnMsg);
?>

<br />
<form id="form1" name="form1" method="post" action="" onsubmit="return false;" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    	<td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
    </tr>
    <tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
<?php if($step == 0){ ?>
	<tr>
		<td colspan="2">
	        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	            <tr> 
	            	<td><br />
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="formfieldtitle" align="left"><?=$Lang['DHL']['ImportType']?></td>
								<td class="tabletext">
									<?=$linterface->Get_Radio_Button("type_NewJoiner", "type", "NewJoiner", $___isChecked=($type == 'NewJoiner'), $____Class="", $____Display=$Lang['DHL']['ImportTypeNewJoiner'], $____Onclick="$('.Type').hide();$('.NewJoiner').show();", $___isDisabled=0)?>&nbsp;
									<?=$linterface->Get_Radio_Button("type_Movement", "type", "Movement", $___isChecked=($type == 'Movement'), $____Class="", $____Display=$Lang['DHL']['ImportTypeMovement'], $____Onclick="$('.Type').hide();$('.Movement').show();", $___isDisabled=0)?>&nbsp;
									<?=$linterface->Get_Radio_Button("type_Leaver", "type", "Leaver", $___isChecked=($type == 'Leaver'), $____Class="", $____Display=$Lang['DHL']['ImportTypeLeaver'], $____Onclick="$('.Type').hide();$('.Leaver').show();", $___isDisabled=0)?>&nbsp;
								</td>
							</tr>
							<tr>
								<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
								<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
							</tr>
							<tr>
								<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
								<td class="tabletext">
									<div class="Type NewJoiner"<?=$type=='NewJoiner'?'':' style="display:none" '?>><?=$csvFile['NewJoiner']?></div>
									<div class="Type Movement"<?=$type=='Movement'?'':' style="display:none" '?>><?=$csvFile['Movement']?></div>
									<div class="Type Leaver"<?=$type=='Leaver'?'':' style="display:none" '?>><?=$csvFile['Leaver']?></div>
								</td>
							</tr>
							<tr>
								<td class="formfieldtitle" align="left"><?=$Lang['General']['ImportArr']['DataColumn']?></td>
								<td class="tabletext">
									<div class="Type NewJoiner"<?=$type=='NewJoiner'?'':' style="display:none" '?>><?=$csv_format['NewJoiner']?></div>
									<div class="Type Movement"<?=$type=='Movement'?'':' style="display:none" '?>><?=$csv_format['Movement']?></div>
									<div class="Type Leaver"<?=$type=='Leaver'?'':' style="display:none" '?>><?=$csv_format['Leaver']?></div>
								</td>
							</tr>
						</table>
						<span class="tabletextremark"><?=$i_general_required_field?></span>
					</td>
		        </tr>
		    </table>
		</td>
	</tr>
    <tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
	                <td class="dotline"><img src="<?=$image_path."/".$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
	            </tr>
	            <tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "CheckForm();","submit_btn"," class=\"formbutton\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						&nbsp;
						<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href='index.php'","back_btn"," class='formbutton' "); ?>				
					</td>
				</tr>
	        </table>                                
		</td>
	</tr>
<?php } ?>

<?php if($step == 1){ ?>
	<tr>
		<td colspan="2">
			<input type="hidden" name="type" id="type" value="<?=$type?>" />
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['SuccessfulRecord']?></td>
					<td class="tabletext" width="70%"><?=count($data)-count($error_ary)?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['FailureRecord']?></td>
					<td class="tabletext <?=count($error_ary)?"red":""?>" width="70%"><?=count($error_ary)?></td>
				</tr>
				<?php
				if($type == 'NewJoiner' && count($data_ary) == 0){
					echo '<tr><td colspan="2">'.$linterface->Get_Warning_Message_Box('', $Lang['DHL']['NoNewRecordForImporting'], "").'</td></tr>';
				}
				?>
			</table>
			<?if(!empty($error_ary)){?>
	        <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="tablebluetop tabletopnolink" width="1%"><?=$Lang['General']['ImportArr']['Row']?></td>
					<td class="tablebluetop tabletopnolink"><?=$i_UserLogin?></td>
					<td class="tablebluetop tabletopnolink" width="10%"><?=$Lang['General']['Remark']?></td>
					<td class="tablebluetop tabletopnolink" width="50%"><?=$Lang['General']['Error']?></td>
				</tr>
				<?=$x?>
			</table>
			<?}?>
		</td>
	</tr>
	<tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	            </tr>
	            <tr>
					<td align="center">
					<?php if(count($data_ary) > 0 /* && count($error_ary)==0 */){ ?>
						<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "ConfirmSubmit();","submit_btn"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
						&nbsp;
					<?php } ?>
						<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "CancelConfirm();","back_btn"," class='formbutton' ")?>				
					</td>
				</tr>
	        </table>                                
		</td>
	</tr>
<?php } ?>

<?php if($step == 2){ ?>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['SuccessfulRecord']?></td>
					<td class="tabletext" width="70%"><?=count($successAry)?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['FailureRecord']?></td>
					<td class="tabletext <?=count($failAry)?"red":""?>" width="70%"><?=count($failAry)?></td>
				</tr>
				<?php 
				if(count($failAry)>0){
					$x = '<tr>';
						$x.= '<td class="formfieldtitle" width="30%">&nbsp;</td>';
						$x.= '<td class="tabletext red" width="70%">';
							$x .= implode("<br />",$failAry);
						$x .= '</td>';
					$x.= '</tr>';
					echo $x;
				}
				?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	            </tr>
	            <tr>
					<td align="center">
						<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href='index.php'","back_btn"," class='formbutton' ")?>				
					</td>
				</tr>
	        </table>                                
		</td>
	</tr>
<?php } ?>
</table>

<input type="hidden" name="step" id="step" value="<?=$step?>" />
<?=$hidden_fields?>
</form>

<div id="InfoLayer" class="selectbox_layer selectbox_layer_show_info" style="visibility:visible;display:none;">
	<div style="float:right">
		<a href="javascript:void(0);" onclick="$('#InfoLayer').toggle();">
			<img border="0" src="/images/<?=$LAYOUT_SKIN?>/ecomm/btn_mini_off.gif">
		</a>
	</div>
	<p class="spacer"></p>
	<div class="content" style="min-width:200px;max-height:512px;overflow-y:auto;"></div>
</div>

<script type="text/javascript" language="javascript">
var last_task = '';
function displayInfoLayer(btnElement,layerId,task)
{
	var btnObj = $(btnElement);
	var layerObj = $('#'+layerId);
	var posX = btnObj.offset().left;
	var posY = btnObj.offset().top + btnObj.height();
	
	layerObj.hide('fast');
	if(layerObj.is(':visible') && task == last_task){
		last_task = task;
		return;
	}
	last_task = task;

	$.get(
		'dhl_user_task.php?task='+task,
		{
			
		},
		function(data){
			$('#'+layerId+' .content').html(data);
			layerObj.css({'position':'absolute','top':posY+'px','left':posX+'px'});
			layerObj.show('fast');
		}
	);
}

function CheckForm()
{
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}

	document.getElementById('step').value='1';
	document.form1.submit();
}

function ConfirmSubmit()
{
	document.getElementById('step').value='2';
	document.form1.submit();
}

function CancelConfirm()
{
	document.getElementById('step').value='0';
	document.form1.submit();
}
</script>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>