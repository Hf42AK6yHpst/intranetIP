<?php

# using: 
##################################### Change Log [Start] #####################################################
#
# 	2017-10-31 (Simon) [M129084]	
#	Add personal photo list
#
###################################### Change Log [End] ######################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || in_array($UserID,$officalPhotoMgmtMember))) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

# upload btn
$toolbar .= $linterface->GET_LNK_UPLOAD("upload_personal_photo.php");

#delete btn
$deleteBtn = '<a href="javascript:checkAlert(document.form1,\'userIdArr[]\',\'remove_personal_photo_update.php\',\''.$Lang['AccountMgmt']['ConfirmMsg']['RemoveSelectedPhoto'].'\')" class="tabletool"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_delete.gif" width="12" height="12" border="0" align="absmiddle">'.$button_delete.'</a>';

# start to generate table format
$field = Get_Lang_Selection("if(iu.ChineseName != '', iu.ChineseName, iu.EnglishName) as name" ,"iu.EnglishName as name");

$ayId = Get_Current_Academic_Year_ID();

$sql = " select $field, iu.UserLogin, iu.PersonalPhotoLink, iu.UserID 
		from INTRANET_USER as iu
		where iu.RecordType = 1
		order by name
		 ";

$teacherInfo = $laccount->returnResultSet($sql);

$x .= '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
$x .= '<tr class="tabletop">';
$x .= '<td class="tabletopnolink" width="35%">'.$Lang['AccountMgmt']['StaffMgmt']['TeacherName'].'</td>';
$x .= '<td class="tabletopnolink" width="25%">'.$Lang['AccountMgmt']['StaffMgmt']['LoginID'].'</td>';
$x .= '<td class="tabletopnolink" width="25%">'.$Lang['AccountMgmt']['StaffMgmt']['Photo'].'</td>';
$x .= '<td class="tabletopnolink" width="10px" align="right"><input id="CheckAllPhoto" type="checkbox" onclick="CheckAll(\'userIdArr[]\')"></td>';
$x .= '</tr>';

foreach((array) $teacherInfo as $teacher)
{
	
	if($teacher['PersonalPhotoLink'])
	{
		$img = '<img src="'.$teacher['PersonalPhotoLink'].'" width="100px">';
		$checkBox = '<input type="checkbox" name="userIdArr[]" value="'.$teacher['UserID'].'" onclick="check_click(this)">';
	}
	else
	{
		$img ="&nbsp;";
		$checkBox ="&nbsp;";
	}
	
	$css = (++$ctr%2==1)?"retablerow1":"retablerow2";
	$x .= '<tr class="'.$css.'">';
	$x .= '<td class="tabletext " valign="top">'.$teacher['name'].'</td>';
	$x .= '<td class="tabletext " valign="top">'.$teacher['UserLogin'].'</td>';
	$x .= '<td class="tabletext " valign="top">'.$img.'</td>';
	$x .= '<td class="tabletext " valign="top" align="right">'.$checkBox.'</td>';
	$x .= '</tr>';
}
$x .= '</table>';

# end to generate table format

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

$PAGE_NAVIGATION[] = array($Lang['Personal']['PersonalPhoto'], "personal_photo_list.php");

$TAGS_OBJ[] = array($Lang['AccountMgmt']['StaffMgmt']['PersonalPhotoList']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
					<tr>
						<td>
							<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
							<br><br>
							<?=$toolbar?>				
						</td>
						<td align="right"></td>
					</tr>
				</table>
			</form>
			<form name="form1" method="get" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="28" align="right" valign="bottom">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" valign="bottom">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
											<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
												<table border="0" cellspacing="0" cellpadding="2">
													<tr>
														<td nowrap><?=$deleteBtn?></td>
													</tr>
												</table>
											</td>
											<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$x?>
					</td>
				</tr>
			</table><br>
			</form>
		</td>
	</tr>
</table>
<script>
function check_click(obj)
{
	if(obj.checked)
	{
		if($("input[name='userIdArr[]']:not(:checked)").length==0)
			$("#CheckAllPhoto").attr("checked","checked");
	}
	else
	{
		$("#CheckAllPhoto").attr("checked","");
	}
}

function CheckAll(objname)
{
	if($("#CheckAllPhoto").attr("checked"))
	{
		$("[name='"+objname+"']").attr("checked","checked")
	}
	else
	{
		$("[name='"+objname+"']").attr("checked","")
	}
}
</script>
<?		
		
$linterface->LAYOUT_STOP();		
?>