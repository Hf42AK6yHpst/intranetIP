<?php
# using: yat

#####################################
#
#	Date:	2011-02-22	YatWoon
#			command resize command
#
#####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/SimpleImage.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$lfs = new libfilesystem();
$linterface = new interface_html();
$SimpleImage = new SimpleImage();

# upload file handling start
$filename = $_FILES["UploadFile"]["name"];
$tmpfilepath = $_FILES["UploadFile"]["tmp_name"];
$fileext = $lfs->file_ext($filename);

$user_photo_path = $PATH_WRT_ROOT."file/signature";
$base_img_path = "/file/signature";
$tmpfolder = $user_photo_path."/tmp";
$tmpfile = $tmpfolder."/$filename";

# create a tmp file
$lfs->folder_new($tmpfolder);

if(strtolower($fileext)==".zip")
{
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
	
	if($lfs->file_unzip($tmpfile,$tmpfolder))
	{
		$lfs->lfs_remove($tmpfile);
		$tmpfilelist = $lfs->return_folderlist($tmpfolder);
	}
}
else if(strtolower($fileext)==".jpg")
{
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
	$tmpfilelist = $lfs->return_folderlist($tmpfolder);
}

if(count($tmpfilelist)>0)
{	
	$NoOfUploadSuccess = 0;
	$UserIDSignatureMapping = array();
	for($i=0; $i< count($tmpfilelist); $i++)
	{
		$UploadSuccess = false;
		$thisUserInfo = '';
		if(strtolower($lfs->file_ext($tmpfilelist[$i])) == ".jpg")
		{
			$thisrawext = substr($tmpfilelist[$i], strrpos($tmpfilelist[$i],".")); // no strtolower
			if($thisrawext != ".jpg") // ext not lower case
			{
				$lowercasefilepath = str_replace($thisrawext,".jpg",$tmpfilelist[$i]);
				$lfs->file_rename($tmpfilelist[$i], $lowercasefilepath);
				$tmpfilelist[$i] = $lowercasefilepath;
			}
			
			//$thisUserLogin = get_file_basename($tmpfilelist[$i],".jpg");
			$thisUserLogin = get_file_basename($tmpfilelist[$i],".jpg");
			$thisUserLogin = str_replace('.jpg', '', $thisUserLogin);
			$thisUserInfo = $laccount->getUserInfoByUserLogin($thisUserLogin);
			
			if(empty($thisUserInfo))
			{
				$err_msg = $Lang['General']['ImportWarningArr']['WrongUserLogin'];
			}
			else if($thisUserInfo[$thisUserLogin]['RecordType']!=1)
			{
				$err_msg = $Lang['AccountMgmt']['SignatureUpload']['WrongStaffLogin'];
			}
			else
			{
				$err_msg = '';
				$target_photo_path = $user_photo_path."/".get_file_basename($tmpfilelist[$i]);
				if($lfs->lfs_copy($tmpfilelist[$i],$target_photo_path))
				{
					$SimpleImage->load($target_photo_path);
// 					$SimpleImage->resize(120,55);
					$SimpleImage->save($target_photo_path);
					$simple_photo_path = $base_img_path."/".get_file_basename($tmpfilelist[$i]);
					$UserIDSignatureMapping[$thisUserLogin] = $simple_photo_path;
					$UploadSuccess = true;
					$NoOfUploadSuccess++;
				}
			}
		} 
		
		$red_css = !$UploadSuccess?" red ":"";

		$rowcss = " class='".($key++%2==0? "tablebluerow2":"tablebluerow1")." $red_css' ";

		# gen table row
		$tr.= '<tr '.$rowcss.'>'."\n";
			$tr.= '<td>'.get_file_basename($tmpfilelist[$i]).'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["Name"]:"-").'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["UserLogin"]:"-").'</td>'."\n";
			$tr.= '<td width="120">'.(file_exists($target_photo_path)?'<img src="'.$target_photo_path.'" width="120" height="55">':'-').'</td>'."\n";
			$tr.= '<td>'.$err_msg.'</td>'."\n";
		$tr.= '</tr>'."\n";
	}
	
	$laccount->updateSignatureLink($UserIDSignatureMapping);
	
	$display.= '<table cellpadding=5 cellspacing=0 border=0 width="100%">'."\n";
		$display.= '<tr class="tablebluetop tabletopnolink">'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['File'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['UserName'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['LoginID'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['Signature'].'</td>'."\n";
			$display.= '<td>'.$Lang['General']['Remark'].'</td>'."\n";
		$display.= '</tr>'."\n";		
		$display.= $tr;
	$display.= '</table>'."\n";
	
}

$UploadStat.= '<table cellpadding="5" cellspacing="0" border="0" width="100%">'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['PhotoAttempt'].'</td><td class="tabletext">'.count($tmpfilelist).'</td></tr>'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadSuccess'].'</td><td class="tabletext">'.$NoOfUploadSuccess.'</td></tr>'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadFail'].'</td><td class="tabletext">'.(count($tmpfilelist)-$NoOfUploadSuccess).'</td></tr>'."\n";
$UploadStat.= '</table>'."\n";

$lfs->folder_remove_recursive($tmpfolder);
# upload file handling end

# gen button 
$uploadBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['UploadMorePhoto'], "button", "window.location.href='upload.php'", "uploadBtn");
$backBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['BackToPhotoMgmt'], "button", "window.location.href='photo_list.php'", "backBtn");

# page info
$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Signature_Upload";

$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 1);
$StepObj = $linterface->GET_STEPS($STEPS_OBJ);

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UploadSignature'], "photo_list.php");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UploadSignature']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
			</table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<?=$UploadStat?>
					</td>
					<td align="right"></td>
				</tr>
			</table>
			<br>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$uploadBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
		</td>
	</tr>
</table>
<br><br>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>