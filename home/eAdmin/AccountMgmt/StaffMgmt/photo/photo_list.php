<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$arrCookies = array();
$arrCookies[] = array("ck_account_mgmt_teacher_signature_page_size", "numPerPage");
$arrCookies[] = array("ck_account_mgmt_teacher_signature_field", "field");	
$arrCookies[] = array("ck_account_mgmt_teacher_signature_order", "order");
$arrCookies[] = array("ck_account_mgmt_teacher_signature_pageNo", "pageNo");
$arrCookies[] = array("ck_account_mgmt_teacher_signature_Keyword", "Keyword");
$arrCookies[] = array("ck_account_mgmt_teacher_signature_Filter", "Filter");			

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

# Search box
$searchbar = $linterface->Get_Search_Box_Div("Keyword",$Keyword);

# filter

$optionArr[] = array(1,$Lang['AccountMgmt']['Uploaded']);
$optionArr[] = array(2,$Lang['AccountMgmt']['NotYetUploaded']);
$filter = getSelectByArray($optionArr," name='Filter' onchange='document.form1.submit();' ",$Filter,1);

# upload btn
$toolbar .= "<a href='upload.php' class='upload'> $button_upload</a>";
// $toolbar .= $linterface->GET_LNK_UPLOAD("upload.php");

#delete btn
$deleteBtn = '<a href="javascript:checkAlert(document.form1,\'UserLogin[]\',\'remove_update.php\',\''.$Lang['AccountMgmt']['ConfirmMsg']['RemoveSelectedPhoto'].'\')" class="tool_delete">'.$button_delete.'</a>';

# Get Student Photo Display Table 
$StudentPhotoListTable = $laccount->getTeacherSignatureDBTable($field,$order,$pageNo,$Keyword,$Filter);

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Signature_Upload";

// $PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UploadSignature'], "");

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UploadSignature']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

?>

<form name="form1" method="get">
			
<div class="content_top_tool">
	<div class="Conntent_tool">
       <?=$toolbar?>
	</div>
       <?=$searchbar?>
<br style="clear:both" />
</div>


<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="table-action-bar">
		<td valign="bottom"><?=$filter?></td>
		<td valign="bottom"><div class="common_table_tool"><?=$deleteBtn?></div></td>
	</tr>
	</table>
</div>

<?=$StudentPhotoListTable?>

<?		
		
$linterface->LAYOUT_STOP();		
?>