<?php
# using: 

#####################################
#
#	Date:	2017-10-31	(Simon) [M129084]
#			handle upload personal photo
#
#####################################


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

# Remarks list
for($i = 1; $i <= sizeof($Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList']); $i++){
	$remarks .= $i.'. '.$Lang['AccountMgmt']['PersonalPhotoUpload']['RemarksList'][$i-1]."<br>";
}

# submit/back Btn
$url = "personal_photo_list.php";

$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='$url'", "backBtn");

#step
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 1);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 0);
$StepObj = $linterface->GET_STEPS($STEPS_OBJ);

# navigation
$PAGE_NAVIGATION[] = array($Lang['Personal']['PersonalPhoto'], $url);
$PAGE_NAVIGATION[] = array($button_upload, "");
$PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['StaffMgmt']['PersonalPhotoUpload']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$PageNavigation ?></td>
							</tr>
						</table>
					</td>
					<td align="right"></td>
				</tr>
				</table>
				<br>
			<form name="form1" method="post" action="upload_personal_photo_update.php" onsubmit="return checkform()"  enctype="multipart/form-data">
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
			</table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['AccountMgmt']['File']?></td>
					<td class="tabletext">
						<input id="personal_photo" type="file" name="personal_photo">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Remarks']?></td>
					<td class="tabletext"><?=$remarks?></td>
				</tr>

			</table>
			
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$submitBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
			<br><br>
		</td>
	</tr>
</table>
</form>
<script>
function checkform()
{
	var filename = $("#personal_photo").val();
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase();
	if(fileext!=".jpg" && fileext!=".zip")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectJPGorZIP']?>");
		return false;
	}
	return true;
}
</script>
<?		
		
$linterface->LAYOUT_STOP();		
?>