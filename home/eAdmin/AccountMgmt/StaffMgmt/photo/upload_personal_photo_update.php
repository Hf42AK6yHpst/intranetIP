<?php
# Using: 

#####################################
#
#	Date:	2019-08-13	(Bill)   [2019-0812-1522-19073]
#			fixed cannot upload photos in KIS
#
#	Date:	2017-10-31	(Simon)  [M129084]
#			handle personal photo update
#
#####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$lfs = new libfilesystem();
$linterface = new interface_html();
$SimpleImage = new SimpleImage();
$luser = new libuser();

################################################
# Start to Handle Personal Photo Upload
################################################

$best_width = 412; // px
$best_height = 459; // px

$filename = $_FILES["personal_photo"]["name"];
$tmpfilepath = $_FILES["personal_photo"]["tmp_name"];
$fileext = $lfs->file_ext($filename);

// [2019-0812-1522-19073]
//$user_photo_path = $PATH_WRT_ROOT."file/photo/personal";
$user_photo_path = $file_path."/file/photo/personal";
$base_img_path = "/file/photo/personal";

$tmpfolder = $user_photo_path."/tmp";
$tmpfile = $tmpfolder."/$filename";
$lfs->folder_new($tmpfolder);

if(strtolower($fileext) == ".zip"){
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
    
	if($lfs->file_unzip($tmpfile,$tmpfolder)) {
		$lfs->lfs_remove($tmpfile);
		$tmpfilelist = $lfs->return_folderlist($tmpfolder);
	}
	
	# remove the path without jpg extension 
	foreach($tmpfilelist as $k => $filepath){
		if(!preg_match ("/\.(jpg|jpeg)(?:[\?\#].*)?$/i", $tmpfilelist[$k])){
			array_splice($tmpfilelist, $k, 1);
		}
	}
}else if(strtolower($fileext) == ".jpg"){
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
	$tmpfilelist = $lfs->return_folderlist($tmpfolder);
}

if(count($tmpfilelist) > 0){
	//debug_pr($tmpfilelist);
	$NoOfUploadSuccess = 0;
	$UserIDSignatureMapping = array();
	for($i = 0; $i < count($tmpfilelist); $i++){
		$UploadSuccess = false;
		$thisUserInfo = '';
		
		if(strtolower($lfs->file_ext($tmpfilelist[$i])) == ".jpg"){
			$thisrawext = substr($tmpfilelist[$i], strrpos($tmpfilelist[$i],".")); // no strtolower
			if($thisrawext != ".jpg") {
				// ext not lower case
				$lowercasefilepath = str_replace($thisrawext,".jpg",$tmpfilelist[$i]);
				$lfs->file_rename($tmpfilelist[$i], $lowercasefilepath);
				$tmpfilelist[$i] = $lowercasefilepath;
			}
			
			$thisUserLogin = get_file_basename($tmpfilelist[$i],".jpg");
			$thisUserLogin = get_file_basename($tmpfilelist[$i],".jpg");
			$thisUserLogin = str_replace('.jpg', '', $thisUserLogin);
			$thisUserInfo = $laccount->getUserInfoByUserLogin($thisUserLogin);
			
			if(empty($thisUserInfo)){
				$err_msg = $Lang['General']['ImportWarningArr']['WrongUserLogin'];
			}else if($thisUserInfo[$thisUserLogin]['RecordType'] != 1){
				$err_msg = $Lang['AccountMgmt']['StaffMgmt']['NotATeacher'];
			}else{
				$err_msg = '';
				$userId = $thisUserInfo[$thisUserLogin]['UserID'];
				$file_extension = strtolower($lfs->file_ext($tmpfilelist[$i]));
				
				$target_photo_path = $user_photo_path. '/p'. $userId. $file_extension;
				if($lfs->lfs_copy($tmpfilelist[$i],$target_photo_path)){
					## resize photo on proportion
					$SimpleImage->load($target_photo_path);
					$image_width = $SimpleImage->getWidth();
					$image_height = $SimpleImage->getHeight();
					$width_ratio = $image_width / (float)$best_width;
					$height_ratio = $image_height / (float)$best_height;
					if($width_ratio != 1.0 && $height_ratio != 1.0 && $image_width > 0 && $image_height > 0)
					{
						$target_ratio = 1.0 / max($width_ratio, $height_ratio);
						$target_width = (int)($target_ratio * $image_width);
						$target_height = (int)($target_ratio * $image_height);
						$SimpleImage->resize($target_width,$target_height);
						$SimpleImage->save($target_photo_path);
					}
					$simple_photo_path = $base_img_path."/".get_file_basename($tmpfilelist[$i]);
					$UserIDSignatureMapping[$thisUserLogin] = $simple_photo_path;
					$UploadSuccess = true;
					$NoOfUploadSuccess++;
					
					# To do update the INTRANET_USER by sql
					$update_file_path = $base_img_path. '/p' . $userId. $file_extension;
					$fieldname = "DateModified = now() , PersonalPhotoLink = '$update_file_path' ";
					$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$userId' ";
					//debug_pr($sql);
					$userIdAry[] = $userId;
					
					$result = $laccount->db_db_query($sql);
				}
			}
		}
		
		$red_css = !$UploadSuccess?" red ":"";
		$rowcss = " class='".($key++%2==0? "tablebluerow2":"tablebluerow1")." $red_css' ";
		
		# gen table row
		$tr.= '<tr '.$rowcss.'>'."\n";
			$tr.= '<td>'.get_file_basename($tmpfilelist[$i]).'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["Name"]:"-").'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$thisUserLogin]["UserLogin"]:"-").'</td>'."\n";
			$tr.= '<td width="120">'.(file_exists($target_photo_path)?'<img src="'.$update_file_path.'" width="100" height="130">':'-').'</td>'."\n";
			$tr.= '<td>'.$err_msg.'</td>'."\n";
		$tr.= '</tr>'."\n";
	}
	
	# synUserDataToModules
	$successAry['synUserDataToModules'] = $luser->synUserDataToModules($userIdAry);
	
	$display.= '<table cellpadding=5 cellspacing=0 border=0 width="100%">'."\n";
		$display.= '<tr class="tablebluetop tabletopnolink">'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['File'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['UserName'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['LoginID'].'</td>'."\n";
			$display.= '<td>'.$Lang['AccountMgmt']['Photo'].'</td>'."\n";
			$display.= '<td>'.$Lang['General']['Remark'].'</td>'."\n";
		$display.= '</tr>'."\n";
		$display.= $tr;
	$display.= '</table>'."\n";
}

$UploadStat.= '<table cellpadding="5" cellspacing="0" border="0" width="100%">'."\n";
$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['PhotoAttempt'].'</td><td class="tabletext">'.count($tmpfilelist).'</td></tr>'."\n";
$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadSuccess'].'</td><td class="tabletext">'.$NoOfUploadSuccess.'</td></tr>'."\n";
$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadFail'].'</td><td class="tabletext">'.(count($tmpfilelist)-$NoOfUploadSuccess).'</td></tr>'."\n";
$UploadStat.= '</table>'."\n";

$lfs->folder_remove_recursive($tmpfolder);

# gen button 
$uploadBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['UploadMorePhoto'], "button", "window.location.href='upload_personal_photo.php'", "uploadBtn");
$backBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['BackToPhotoMgmt'], "button", "window.location.href='personal_photo_list.php'", "backBtn");

# page info
$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 1);
$StepObj = $linterface->GET_STEPS($STEPS_OBJ);

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['StaffMgmt']['PersonalPhotoUpload'], "personal_photo_list.php");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$TAGS_OBJ[] = array($Lang['AccountMgmt']['StaffMgmt']['PersonalPhotoUpload']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
			</table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<?=$UploadStat?>
					</td>
					<td align="right"></td>
				</tr>
			</table>
			<br>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$uploadBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
		</td>
	</tr>
</table>
<br><br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>