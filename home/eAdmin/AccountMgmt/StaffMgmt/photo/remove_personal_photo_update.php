<?php
//
/**
 * Log
 * - 2019-08-13	(Bill)   [2019-0812-1522-19073]
 *   fixed cannot delete photos in KIS
 *
 * - 2017-11-01 (Simon)
 *   update db after deleteing physical file.
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$lfs = new libfilesystem();
$li = new libdb();
$luser = new libuser();

foreach((array)$_POST['userIdArr'] as $thisUserID)
{
	$sql = "select PersonalPhotoLink from INTRANET_USER where UserID = '$thisUserID' ";
	$img_path = $laccount->returnVector($sql);

    // [2019-0812-1522-19073]
	# ltrim remove first slash character in /file/photo/personal_photo/p207088.jpg
	// $new_img_path = ltrim($img_path[0], '/');
	// $new_img_path = $PATH_WRT_ROOT . $new_img_path;
    $new_img_path = $file_path.$img_path[0];
	
	## Unlink file
	if(!empty($new_img_path)){
		$is_succ = $lfs->file_remove($new_img_path);
		if($is_succ == true || $is_succ == 1) {
            $SuccUserIDArr[] = $thisUserID;
        }
	}
}

# set PersonalPhotoLink to NULL when delete photo
$ParUserIDSql = "'".implode("','",(array)$SuccUserIDArr)."'";
$sql = " UPDATE INTRANET_USER SET PersonalPhotoLink = NULL WHERE UserID IN ($ParUserIDSql); ";
$result = $laccount->db_db_query($sql) ? 1 : 0;

$successAry['synUserDataToModules'] = $luser->synUserDataToModules($SuccUserIDArr);

$msg = $result?"DeleteSuccess":"DeleteUnSuccess";
header("location: personal_photo_list.php?msg=$msg");
?>