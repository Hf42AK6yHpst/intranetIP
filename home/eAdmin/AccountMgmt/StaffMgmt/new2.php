<?php
# using:  

############# Change Log
#	Date:	2020-07-14 Philips
#			Added position, contractType, contractStartDate, contractEndDate for KIS CEES
#
#   2019-04-30 Cameron
#           fix cross site scripting by applying cleanHtmlJavascript() to variables
#   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#   2018-08-07 (Cameron) [ip.2.5.9.10.1]: add Grade and Duty for HKPF
#	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#   2016-11-29 (HenryHM) [ip.2.5.8.1.1]: $ssoservice["Google"]["Valid"] - add GSuite Logic
#	2016-10-04 (Carlos) [ip.2.5.8.1.1]: $sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
#	2015-12-30 (Carlos)
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	2015-12-17 (Cameron)
#			hide Group selection for Amway
#
#	2013-12-12 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - user can input its own iMail plus user name
#
#	2012-08-30 (YatWoon): add barcode
#
#	Date:	2011-11-01 Henry chow
#			Add "Staff Code" field
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_auth();
intranet_opendb();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lo = new libgrouping();
$lrole = new librole();

$lauth = new libauth();
$lu = new libuser();
$thisUserType = USERTYPE_STAFF;
$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$thisUserType));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6) $PasswordLength = 6;

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

### start handle sql injection and cross site scripting
$TeachingType = cleanHtmlJavascript($TeachingType);
$recordstatus = cleanHtmlJavascript($recordstatus);
$keyword = cleanHtmlJavascript($keyword);
$userlogin = cleanHtmlJavascript($userlogin);
$pwd = cleanHtmlJavascript($pwd);
$teaching = cleanHtmlJavascript($teaching);
$email = cleanHtmlJavascript($email);
$status = IntegerSafe($status);
$smartcardid = cleanHtmlJavascript($smartcardid);
$staffCode = cleanHtmlJavascript($staffCode);
$barcode = cleanHtmlJavascript($barcode);
$grade = cleanHtmlJavascript($grade);
$duty = cleanHtmlJavascript($duty);
$engname = cleanHtmlJavascript($engname);
$chiname = cleanHtmlJavascript($chiname);
$nickname = cleanHtmlJavascript($nickname);
$gender = cleanHtmlJavascript($gender);
$engTitle = cleanHtmlJavascript($engTitle);
$chiTitle = cleanHtmlJavascript($chiTitle);
$address = cleanHtmlJavascript($address);
$country = cleanHtmlJavascript($country);
$homePhone = cleanHtmlJavascript($homePhone);
$officePhone = cleanHtmlJavascript($officePhone);
$mobilePhone = cleanHtmlJavascript($mobilePhone);
$faxPhone = cleanHtmlJavascript($faxPhone);
$open_webmail = cleanHtmlJavascript($open_webmail);
$open_file = cleanHtmlJavascript($open_file);
$Quota = cleanHtmlJavascript($Quota);
$EmailStatus = cleanHtmlJavascript($EmailStatus);
$errorList = cleanHtmlJavascript($errorList);
$remark = cleanHtmlJavascript($remark);
$position = cleanHtmlJavascript($position);
$contractType = cleanHtmlJavascript($contractType);
$contractStartDate = cleanHtmlJavascript($contractStartDate);
$contractEndDate = cleanHtmlJavascript($contractEndDate);
if($sys_custom['iMailPlus']['EmailAliasName']){
	$ImapUserLogin = cleanHtmlJavascript($ImapUserLogin);
	$UseImapUserLogin = cleanHtmlJavascript($UseImapUserLogin);
}
if($plugin['radius_server']){
	$enable_wifi_access = IntegerSafe($enable_wifi_access);
}
if($sys_custom['iMail']['UserDisplayOrder']){
    $DisplayOrder = cleanHtmlJavascript($DisplayOrder);
}
### end handle sql injection and cross site scripting


$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputStaffDetails'], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['SelectGroupAndRole'], 1);

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check Email duplication ###
if($email != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail='$email'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $email;
	}
}

## check Smart Card ID duplication ###
if($smartcardid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid;
	}
}

## check Staff Code duplication ###
if($staffCode!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE StaffCode='$staffCode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $staffCode;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

## check iMail plus type in UserEmail ##
if($sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1 && $ImapUserLogin!='' && $ImapUserLogin != $userlogin){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$ImapUserLogin'";
	$temp = $laccount->returnVector($sql);
	$IMap = new imap_gamma(1);
	if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']) ||  sizeof($temp)!=0 || in_array($ImapUserLogin, (array)$system_reserved_account) || $IMap->is_user_exist($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
		$error_msg[] = $ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}
}

if($ssoservice["Google"]["Valid"]){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	$google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();


if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="new.php">
		<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="teaching" id="teaching" value="<?=$teaching?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
		<input type="hidden" name="staffCode" id="staffCode" value="<?=$staffCode?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="grade" id="grade" value="<?=$grade?>">
		<input type="hidden" name="duty" id="duty" value="<?=$duty?>">
		<input type="hidden" name="engname" id="engname" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="engTitle" id="engTitle" value="<?=$engTitle?>">
		<input type="hidden" name="chiTitle" id="chiTitle" value="<?=$chiTitle?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
		<input type="hidden" name="position" id="position" value="<?=$position?>" />
		<input type="hidden" name="contractType" id="contractType" value="<?=$contractType?>" />
		<input type="hidden" name="contractStartDate" id="contractStartDate" value="<?=$contractStartDate?>" />
		<input type="hidden" name="contractEndDate" id="contractEndDate" value="<?=$contractEndDate?>" />
	<?php if($sys_custom['iMailPlus']['EmailAliasName']){ ?>	
		<input type="hidden" name="ImapUserLogin" id="ImapUserLogin" value="<?=$ImapUserLogin?>" />
		<input type="hidden" name="UseImapUserLogin" id="UseImapUserLogin" value="<?=$UseImapUserLogin?>" />
	<?php } ?>
	<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
	<?php } ?>
	<?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>
		<input type="hidden" name="DisplayOrder" id="DisplayOrder" value="<?=$DisplayOrder?>" />
	<?php } ?>
	<?php if($ssoservice["Google"]["Valid"]){ ?>
    	<?php foreach((array)$google_apis as $google_api){ ?>
    		<?php $config_index = $google_api->getConfigIndex(); ?>
    		<?php $gmail_variable = 'gmail_'.$config_index; ?>
			<input type="hidden" name="gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>" value="<?php echo $$gmail_variable; ?>" />
		<?php } ?>
	<?php } ?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;
	
	} else { ?>

	<script language="javascript">
	function goURL(urlLink) {
		document.form1.action = urlLink;
		document.form1.submit();
	}
	
	function goSubmit() {
<?php if (!$sys_custom['project']['CourseTraining']['IsEnable']): ?>		
		var groupLength = document.form1.elements['GroupID'].length;
		for(i=0; i<groupLength; i++) {
			document.form1.elements['GroupID'][i].selected = true;	
		}
<?php endif; ?>		
		var roleLength = document.form1.elements['RoleID'].length;
		for(i=0; i<roleLength; i++) {
			document.form1.elements['RoleID'][i].selected = true;	
		}
		document.form1.submit();	
	}
	
	</script>
	<form name="form1" method="post" action="new_update.php">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
	    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
	    <tr>
	    	<td class="board_menu_closed">
			  
				<table width="99%" border="0" cellspacing="0" cellpadding="0">
	                <tr> 
	                  <td class="main_content">
	                      <div class="table_board">
							<table class="form_table">
							  <tr>
							    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['ClassGroupInfo']?> -</em></td>
							  </tr>
<?php if (!$sys_custom['project']['CourseTraining']['IsEnable']): ?>							  
							  <tr>
							    <td class="formfieldtitle" width="20%"><?=$i_adminmenu_group?></td>
							    <td >
								    <table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><?php echo $lo->displayUserGroups(); ?></td>
										</tr>
								    </table>
							    </td>
							  </tr>
<?php endif; ?>							  
							  <tr>
							    <td class="formfieldtitle" width="20%"><?=$Lang['Header']['Menu']['Role']?></td>
							    <td >
								    <table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><?= $lrole->displayRoleSelection(); ?></td>
										</tr>
								    </table>
							    </td>
							  </tr>
							</table>
							<p class="spacer"></p>
	                      </div>
	                      <div class="edit_bottom">
	                          <p class="spacer"></p>
	                          <?= $linterface->GET_ACTION_BTN($button_save, "button", "goSubmit()")?>
	                          <?= $linterface->GET_ACTION_BTN($button_back, "button", "goURL('new.php')")?>
	                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('index.php')")?>
								<p class="spacer"></p>
	                    </div></td>
	                </tr>
	              </table>
	    	</td>
	    </tr>
	</table>
	<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
	<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
	<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
	<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
	<input type="hidden" name="teaching" id="teaching" value="<?=$teaching?>">
	<input type="hidden" name="email" id="email" value="<?=$email?>"> 
	<input type="hidden" name="status" id="status" value="<?=$status?>">
	<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
	<input type="hidden" name="staffCode" id="staffCode" value="<?=$staffCode?>">
	<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
	<input type="hidden" name="grade" id="grade" value="<?=$grade?>">
	<input type="hidden" name="duty" id="duty" value="<?=$duty?>">
	<input type="hidden" name="engname" id="engname" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>">
	<input type="hidden" name="chiname" id="chiname" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>">
	<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
	<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
	<input type="hidden" name="engTitle" id="engTitle" value="<?=$engTitle?>">
	<input type="hidden" name="chiTitle" id="chiTitle" value="<?=$chiTitle?>">
	<input type="hidden" name="address" id="address" value="<?=$address?>">
	<input type="hidden" name="country" id="country" value="<?=$country?>">
	<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
	<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
	<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
	<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
	<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
	<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
	<input type="hidden" name="Quota" id="Quota" value="<?=$Quota?>">
	<input type="hidden" name="EmailStatus" id="EmailStatus" value="<?=$EmailStatus?>">		
	<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
<?php if($sys_custom['iMailPlus']['EmailAliasName']){ ?>	
	<input type="hidden" name="ImapUserLogin" id="ImapUserLogin" value="<?=$ImapUserLogin?>" />
	<input type="hidden" name="UseImapUserLogin" id="UseImapUserLogin" value="<?=$UseImapUserLogin?>" />
<?php } ?>
<?php if($plugin['radius_server']){ ?>
	<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
<?php } ?>
	<?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>
		<input type="hidden" name="DisplayOrder" id="DisplayOrder" value="<?=$DisplayOrder?>" />
	<?php } ?>
	<?php if($ssoservice["Google"]["Valid"]){ ?>
    	<?php foreach((array)$google_apis as $google_api){ ?>
    		<?php $config_index = $google_api->getConfigIndex(); ?>
    		<?php $gmail_variable = 'gmail_'.$config_index; ?>
			<input type="hidden" name="gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>" value="<?php echo $$gmail_variable; ?>" />
		<?php } ?>
	<?php } ?>
	</form>
<?
	intranet_closedb();
	$linterface->LAYOUT_STOP();
} 
?>