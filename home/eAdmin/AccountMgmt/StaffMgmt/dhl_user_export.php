<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libauth.php");
//include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/librole.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	intranet_closedb();
	header("Location:index.php");
	exit;
}

$lexport = new libexporttext();
$libdhl = new libdhl();

$params = $_POST;
$params['GetOrganizationInfo'] = 1;
$records = $libdhl->getUserRecords($params);
$record_size = count($records);
//debug_pr($records);

$header = $libdhl->getImportUserHeaderColumns();
$fieldMap = array("UserLogin"=>"UserLogin","Password"=>"","UserEmail"=>"UserEmail","EnglishName"=>"EnglishName","Company"=>"CompanyCode","Division"=>"DivisionCode","Department"=>"DepartmentCode","JoinDate"=>"JoinDate","EffectiveDate"=>"EffectiveDate","TerminatedDate"=>"TerminatedDate","Status"=>"RecordStatus");
$rows = array();

for($i=0;$i<$record_size;$i++){
	
	$row = array();
	foreach($fieldMap as $header_key => $field_key)
	{
		if($field_key != ''){
			$row[] = $records[$i][$field_key];
		}else{
			$row[] = '';
		}
	}
	$rows[] = $row;
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $header);	

intranet_closedb();

$filename = "DHL_users.csv";
$lexport->EXPORT_FILE($filename, $export_content);
exit;
?>