<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location : index.php");	
	exit;
}

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lexport = new libexporttext();

# Teaching type
if($TeachingType==1)
	$conds = " AND Teaching=$TeachingType";
else if(isset($TeachingType) && $TeachingType!='')
	$conds = " AND (Teaching!=".TEACHING." OR Teaching IS NULL)";

# record status
if(isset($recordstatus) && $recordstatus!="")
	$conds .= " AND RecordStatus=$recordstatus";
	
$sql = "SELECT
			IFNULL(IF(EnglishName='','---',EnglishName),'---') as EnglishName,
			IFNULL(IF(ChineseName='','---',ChineseName),'---') as ChineseName,
			CASE Title
				WHEN 0 THEN '$i_title_mr '
				WHEN 1 THEN '$i_title_miss '
				WHEN 2 THEN '$i_title_mrs '
				WHEN 3 THEN '$i_title_ms '
				WHEN 4 THEN '$i_title_dr '
				WHEN 5 THEN '$i_title_prof '
				ELSE '' END as title,
			CASE Teaching
				WHEN 1 THEN '".$Lang['Identity']['TeachingStaff']."'
				WHEN 0 THEN '".$Lang['Identity']['NonTeachingStaff']."'
				ELSE '---' END as teaching,
			UserID,
			UserLogin,
			IFNULL(LastUsed, '---') as LastUsed,
			CASE RecordStatus
				WHEN 0 THEN '$i_status_suspended'
				WHEN 1 THEN '$i_status_approved'
				WHEN 2 THEN '$i_status_pending'
				ELSE '---' END as recordStatus
		FROM 
			INTRANET_USER
		WHERE
			RecordType = ".TYPE_TEACHER."
			$conds 
		ORDER BY
			EnglishName
			";

$result = $laccount->returnArray($sql,8);

for($i=0; $i<sizeof($result); $i++)
{
	$ExportArr[$i][0] = $result[$i]['EnglishName'];		//DateInput
	$ExportArr[$i][1] = $result[$i]['ChineseName'];		//name
	$ExportArr[$i][2] = $result[$i]['title'];			//Location
	$ExportArr[$i][3] = $result[$i]['teaching'];		//Location Detail
	//$ExportArr[$i][4] = $result[$i]['UserID'];			//title
	if($result[$i]['UserID'] != "") {
		$temp = $laccount->getTeachingClassList($result[$i]['UserID']);
		$ExportArr[$i][4] = ($temp!="") ? $temp : "---";	
	}
	$ExportArr[$i][5] = $result[$i]['UserLogin'];		//Content
	$ExportArr[$i][6] = $result[$i]['LastUsed'];		//category name
	$ExportArr[$i][7] = $result[$i]['recordStatus'];	//Remark
}

intranet_closedb();

$exportColumn = array($i_UserEnglishName,$i_UserChineseName,$i_UserTitle,$i_eClass_Identity,$i_general_class,$i_UserLogin,$i_frontpage_eclass_lastlogin,$i_UserRecordStatus);
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

$filename = 'staff_account.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>