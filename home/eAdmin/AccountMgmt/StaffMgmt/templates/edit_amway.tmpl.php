<?php
	// Using:
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
                      <div class="table_board">

						<table class="form_table" border="0">
						  <tr>
						    <td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
						    </tr>
						 
						  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td width="75%"><?=$userInfo['UserLogin']?></td>
						    <td rowspan="7"><img src="<?=$photo_personal_link?>" width="100" height="130" title="<?=$Lang['Personal']['PersonalPhoto']?>" /></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
						    <td><input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/></td>
						  </tr>
						  <tr>
						    <td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> 
						      (<?=$Lang['AccountMgmt']['Retype']?>)        </td>
						  </tr>
						  <tr>
							<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
						  </tr>
  							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_TEACHER) )
									{	
										if($sys_custom['iMailPlus']['EmailAliasName'])
										{
											$btnsImapUserLogin = '<input name="ImapUserLogin" type="text" id="ImapUserLogin" class="textbox_name" value="'.(isset($ImapUserLogin)?$ImapUserLogin:$CurrentImapUserLogin).'" maxlength="100" '.($UseImapUserLogin==1?'':'style="display:none;"').' /><span id="ImapUserLoginDomain" '.($UseImapUserLogin==1?'':'style="display:none;"').'>@'.$SYS_CONFIG['Mail']['UserNameSubfix'].'</span>';
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", 'ToggleImapUserLogin(true);', "BtnEditImapUserLogin", ($UseImapUserLogin==1?'style="display:none;"':''), "", "");
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", 'ToggleImapUserLogin(false);', "BtnCancelImapUserLogin", ($UseImapUserLogin==1?'':'style="display:none;"'), "", "");
											$btnsImapUserLogin.= '<input type="hidden" id="UseImapUserLogin" name="UseImapUserLogin" value="'.($UseImapUserLogin==1?'1':'0').'" />';
											$btnsImapUserLogin.= '<input type="hidden" id="OldImapUserEmail" name="OldImapUserEmail" value="'.(isset($OldImapUserEmail)?$OldImapUserEmail:$IMapEmail).'" />';
											if(sizeof($errorAry)>0 && in_array($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'], $errorAry)){
												if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else if(in_array($ImapUserLogin, (array)$system_reserved_account)){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['EmailUsed'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else{
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>";
												}
											}
										}
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" <?=$disabled?>/><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" <?=$enabled?>/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label> - <span id="IMapUserEmail" <?=$sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1?'style="display:none"':''?>><?=$IMapEmail?></span><?=$sys_custom['iMailPlus']['EmailAliasName']?$btnsImapUserLogin:""?><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>: <input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['Gamma']['UserEmail']?></td>
						    <td><input class="textbox_name" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$userInfo['UserEmail']?>">
								<br /><span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0" || $status=="") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>

						  <tr>
						    <td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
						    </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$i_general_name?></td>
						    <td colspan="2"><span class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)</span>
								<input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>" />
						    </td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
								<input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>" />
						    </td>
						  </tr>
						  <tr>
						    <td><span class="tabletextrequire">*</span><?=$i_UserGender?></td>
						    <td colspan="2"><input type="radio" name="gender" id="genderM" value="M" <? if($gender=="M") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F"  <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserAddress?></td>
						    <td colspan="2"><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="4"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>"/></td>
						  </tr>
						  <!-- Additional Info //-->
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
								
								
						  <!-- Internet Usage //-->
						  <tr>
						    <td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  </tr>
						  <?
						  	$sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$uid'";
						  	$result = $laccount->returnVector($sql);
							$can_access_iMail = substr($result[0],0,1);
							$can_access_iFolder = substr($result[0],1,1);
						  ?>
						  <?if(!$plugin['imail_gamma']) {?>
						  	<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(TYPE_TEACHER, $webmail_identity_allowed)) { ?>
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
							    <td colspan="2"><input type="checkbox" value="1" name="open_webmail" disabled <? echo ($can_access_iMail)? "checked" : "" ?> />
							      <?=$i_Mail_AllowSendReceiveExternalMail?></td>
							  </tr>
						  	<?}?>
						  <?}?>
						  <? if(isset($plugin['personalfile']) && $plugin['personalfile'] && in_array(TYPE_TEACHER,$personalfile_identity_allowed)) { ?>
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iFolder']?></td>
							    <td colspan="2"><input type="checkbox" value="1" name="open_file" disabled <? echo ($can_access_iFolder)? "checked" : "" ?> />
							      <?=$i_Files_OpenAccount?></td>
							  </tr>
						  <? } ?>
						</table>
						<p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                          <p class="spacer"></p>
                          <?= $linterface->GET_ACTION_BTN($button_continue, "button", "goSubmit('edit2.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>
