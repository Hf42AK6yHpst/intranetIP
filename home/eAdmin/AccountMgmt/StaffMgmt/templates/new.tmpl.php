<?php
// Using: 
/*
 * 2020-11-04 (Philips) default $contractStartDate empty
 * 2020-07-13 (Philips) Added position, contractType, contractStartDate,contractEndDate for CEES KIS
 * 2018-08-21 (Cameron) [ip.2.5.9.10.1]: hide unnecessary fields for HKPF
 * 2018-08-14 (Cameron) [ip.2.5.9.10.1]: password field is not mandatory for HKPF 
 * 2018-08-07 (Cameron) [ip.2.5.9.10.1]: add Grade and Duty for HKPF
 * 2018-01-03 (Carlos): [ip.2.5.9.1.1]: Apply new password criteria remark.
 * 2016-11-29 (HenryHM) [ip.2.5.8.1.1]: $ssoservice["Google"]["Valid"] - add GSuite Logic
 * 2016-10-04 (Carlos) [ip.2.5.8.1.1]: $sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
 * 2016-09-28 (Ivan) [ip.2.5.7.10.1]: Added teaching = 'S' supply teacher logic
 * 2015-12-30 (Carlos): $plugin['radius_server'] - added Wi-Fi access option.
 */
?>	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
                      <div class="table_board">

						<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
						  <tr>
						    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
						    </tr>
						 
						  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td ><input name="userlogin" type="text" id="userlogin" class="textboxnum" value="<?=$userlogin?>" maxlength="20" onKeyUp="updateIMapEmail(this);"/>
						    		<?/* if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo "<font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; */?>
						    		<span id="spanChecking"><? if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo " <font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?><input type="hidden" name="available" id="available" value="1"></span>
						    		<span id="spanCheckingImage" style="display:none;"><?=$linterface->Get_Ajax_Loading_Image()?></span>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="3"><?php if (!$sys_custom['project']['HKPF']):?><span class="tabletextrequire">*</span><?php endif;?><?=$i_UserPassword?></td>
						    <td>
						    	<input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>" maxlength="20"/>
						    	<? if(sizeof($errorAry)>0 && in_array($pwd, $errorAry)) echo "<font color='#FF0000'>".$Lang["Login"]["password_err"]."</font>"; ?>
						    </td>
						  </tr>
						  <tr>
						    <td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>" maxlength="20"/> 
						      (<?=$Lang['AccountMgmt']['Retype']?>)        </td>
						  </tr>
						  <tr>
							<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_identity?></td>
						    <td ><input type="radio" value="1" name="teaching" id="teaching1"  <? if($teaching==1 || !isset($teaching)) echo "checked"; ?>/>
						      <label for="teaching1"><?=$Lang['Identity']['TeachingStaff']?></label>
						        <input type="radio" value="0" name="teaching" id="teaching0" <? if($teaching=='0') echo "checked"; ?>/>
						        <label for="teaching0"><?=$Lang['Identity']['NonTeachingStaff']?></label>
						        <?php if ($plugin['SLRS']) { ?>
							        <input type="radio" value="S" name="teaching" id="teachingS" <? if($teaching=='S') echo "checked"; ?>/>
							        <label for="teachingS"><?=$Lang['Identity']['SupplyTeacher']?></label>
						        <?php } ?>
						        <br />
						        
						        <?=$i_teachingDifference?></td>
						  </tr>
  							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_TEACHER) )
									{	
										if($sys_custom['iMailPlus']['EmailAliasName'])
										{
											$btnsImapUserLogin = '<input name="ImapUserLogin" type="text" id="ImapUserLogin" class="textbox_name" value="'.$ImapUserLogin.'" maxlength="100" '.($UseImapUserLogin==1?'':'style="display:none;"').' /><span id="ImapUserLoginDomain" '.($UseImapUserLogin==1?'':'style="display:none;"').'>@'.$SYS_CONFIG['Mail']['UserNameSubfix'].'</span>';
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", 'ToggleImapUserLogin(true);', "BtnEditImapUserLogin", ($UseImapUserLogin==1?'style="display:none;"':''), "", "");
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", 'ToggleImapUserLogin(false);', "BtnCancelImapUserLogin", ($UseImapUserLogin==1?'':'style="display:none;"'), "", "");
											$btnsImapUserLogin.= '<input type="hidden" id="UseImapUserLogin" name="UseImapUserLogin" value="'.($UseImapUserLogin==1?'1':'0').'" />';
											if(sizeof($errorAry)>0 && in_array($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'], $errorAry)){
												if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else if(in_array($ImapUserLogin, (array)$system_reserved_account)){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['EmailUsed'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else{
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>";
												}
											}
										}
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" /><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" CHECKED/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label>,&nbsp;<span id="IMapUserEmail" <?=$sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1?'style="display:none"':''?>>@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?></span><?=$sys_custom['iMailPlus']['EmailAliasName']?$btnsImapUserLogin:''?><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>:<input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
								<!-- imail -->
						  <tr>
						    <td class="formfieldtitle"><!--<span class="tabletextrequire">*</span>--><?=$Lang['Gamma']['UserEmail']?></td>
						    <td>
						    	<input name="email" type="text" id="email" class="textbox_name" value="<?=$email?>" maxlength="100"/>
						    	<? if(sizeof($errorAry)>0 && in_array($email, $errorAry)) echo "<font color='#FF0000'>".$i_UserEmail.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
<?php if (!$sys_custom['project']['HKPF']):?>						    	
						    	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
<?php endif;?>						    	
						    </td>
						  </tr>
								
							<?php if(!$ssoservice["Google"]["Valid"]){ ?>
							  <tr>
							    <td class="formfieldtitle"><?php echo $Lang['Gamma']['UserGmail']; ?></td>
							    <td>
							    	<div id="div_gmail">
							    	<input type="checkbox" class="is_display_options" />
							    	<div class="options" style="display:none;">
							    	<?php foreach((array)$google_apis as $google_api){ ?>
    									<?php $config_index = $google_api->getConfigIndex(); ?>
    									<?php $gmail_variable = 'gmail_'.$config_index; ?>
    									<input type="radio" name="r_gmail" id="r_gmail_<?php echo $config_index; ?>" <?php echo ($$gmail_variable == '1')? "checked" : "" ?> <?php echo ($total_quota[$config_index]!=='NO_QUOTA' && $remaining_quota[$config_index] <= 0)? "disabled" : "" ?> />
						    			<input type="checkbox" value="1" name="gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>" <?php echo ($$gmail_variable == '1')? "checked" : "" ?> style="display:none;" />
						    			<label for="r_gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>_label"><span class="GSuiteUserLogin"></span>@<?php echo $google_api->getDomain();?> <?php if($total_quota[$config_index]!=='NO_QUOTA'){?>(<?php echo $total_quota[$config_index] - $remaining_quota[$config_index]; ?>/<?php echo $total_quota[$config_index]; ?>)<?php } ?><span id="account_exists_<?php echo $config_index; ?>" class="account_exists" style="display:none;"> <span style="color:rgb(0,97,0);background-color:rgb(198,239,206);">(G Suite <?php echo Get_Lang_Selection('帳戶已經存在', 'account exists'); ?>)</span></span></label>
						    			<?php
						    			$message = '';
					    				switch($_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]){
					    					case "ACCOUNT_CREATED":
					    						$message = $Lang['Gamma']['UserGmailAccountExists'];
					    						$_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]='NO_MESSAGE';
					    						break;
					    					case "NO_MESSAGE":
					    						break;
					    					default:
					    						$message = $_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()];
					    						$_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]='NO_MESSAGE';
					    						break;
					    				}
						    			?>
						    			<span style="background-color:rgb(255,199,206);color:rgb(156,0,6);"><?php echo ($message==''?'':'&nbsp;&nbsp;&nbsp;').$message.($message==''?'':'&nbsp;&nbsp;&nbsp;'); ?></span>
						    			<br/>
						    		<?php } ?>
						    		</div>
						    		</div>
								</td>
							  </tr>
							  <tr id="tr_googleUserLogin" style="display:none;">
							    <td class="formfieldtitle"><?=$Lang['General']['GoogleUserLogin']?></td>
							    <td>
							    	<input name="googleUserLogin" type="text" id="googleUserLogin" class="textbox_name" value="<?=$googleUserLogin?>" maxlength="100"/>
							    </td>
							  </tr>
							  <script>
							  	var obj_is_display_options = $('div#div_gmail .is_display_options');
								var obj_checkboxes = $('div#div_gmail input[type="checkbox"]').not(obj_is_display_options);
								var obj_google_radio_buttons = $('div#div_gmail input[type="radio"]');
								
								var google_radio_button_initially_disabled = [];
								obj_google_radio_buttons.each(function(){
									var google_radio_button_id = $(this).attr('id');
									google_radio_button_initially_disabled[google_radio_button_id] = $(this).is(':disabled');
								});
								
							  	obj_is_display_options.bind('change',function(){
									var is_checked = $(this).is(":checked");
									obj_checkboxes.attr('checked',false);
									obj_google_radio_buttons.attr('checked',false);
									if(is_checked){
										$('div#div_gmail .options').css({'display':'block'});
										$('#tr_googleUserLogin').show();
									}else{
										$('div#div_gmail .options').css({'display':'none'});
										$('#tr_googleUserLogin').hide();
									}
							  	});
								if(obj_checkboxes.length == 1){
									obj_is_display_options.css({'display':'none'});
									$('div#div_gmail .options').css({'display':'block'});
									$('#tr_googleUserLogin').show();
									
									obj_checkboxes.each(function(){
										var id = $(this).attr('id');
										$(this).css({'display':'inline'});
										obj_google_radio_buttons.css('display','none');
										$('#'+id+'_label').css({'display':($(this).is(":checked") == true?'inline':'none')});
									});
									obj_checkboxes.bind('click',function(){
										var id = $(this).attr('id');
										$('#'+id+'_label').css({'display':($(this).is(":checked") == true?'inline':'none')});
									});
								}
								obj_checkboxes.bind('change',function(){
									var is_checked = $(this).is(":checked");
									if(is_checked){
										obj_checkboxes.each(function(){
											$(this).attr('checked',false);
										});
										$(this).attr('checked',true);
									}
								});
								obj_google_radio_buttons.bind('change',function(){
									var id = $(this).attr('id');
									var checkbox_id = id.substring(2);
									$('div#div_gmail #'+checkbox_id).attr('checked',true);
									$('div#div_gmail #'+checkbox_id).trigger('change');
								});
								
								var obj_userlogin = $('input#userlogin');
								obj_userlogin.bind('change',function(){
									
									//reset controls
									obj_google_radio_buttons.attr('checked',false);
									obj_google_radio_buttons.attr('disabled','disabled');
									obj_checkboxes.attr('checked',false);
									$('.account_exists').css({'display':'none'});
									
									//do ajax
									$.ajax({
										'url':'new_ajax.php',
										'type':'POST',
										'data':{
											userLogin:obj_userlogin.val()
										},
										'success':function(data){
											var obj_data = JSON.parse(data);
											var accounts = obj_data['accounts'];
											for(var config_index in accounts){
												
												var account_exists = accounts[config_index];
												if(account_exists){
													$('#r_gmail_'+config_index).removeAttr('disabled');
													$('#account_exists_'+config_index).css({'display':'inline'});
												}else{
													if(google_radio_button_initially_disabled['r_gmail_'+config_index]){
														$('#r_gmail_'+config_index).attr('disabled','disabled');
													}else{
														$('#r_gmail_'+config_index).removeAttr('disabled');
													}
													$('#account_exists_'+config_index).css({'display':'none'});
												}
											}
										}
									});
								});
							  </script>
							<?php }	?>
							
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="" || $status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						  <? if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) { ?>
						  <tr>
						    <td class="formfieldtitle"><?=$i_SmartCard_CardID?></td>
						    <td>
						    	<input name="smartcardid" type="text" id="smartcardid" class="textboxnum" value="<?=$smartcardid?>" maxlength="200"/>
						    	<? if(sizeof($errorAry)>0 && in_array($smartcardid, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
						    </td>
						  </tr>
						  <? } ?>
						  
<?php if (!$sys_custom['project']['HKPF']):?>						  
						  <tr<?=$isKIS?' style="display:none;"':''?>>
						    <td class="formfieldtitle"><?=$Lang['AccountMgmt']['StaffCode']?></td>
						    <td>
						    	<input name="staffCode" type="text" id="staffCode" class="textboxnum" value="<?=$staffCode?>" maxlength="200"/>
						    	<? if(sizeof($errorAry)>0 && in_array($staffCode, $errorAry)) echo "<font color='#FF0000'>".$Lang['AccountMgmt']['StaffCode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
						    </td>
						  </tr>
						  
						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
								<td>
									<input name="barcode" type="text" id="barcode" class="textboxnum" value="<?=$barcode?>" maxlength="30"/>
									<? if(sizeof($errorAry)>0 && in_array($barcode, $errorAry)) echo "<font color='#FF0000'>".$Lang['AccountMgmt']['Barcode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
								</td>
							</tr>
<?php endif;?>

<?php if ($sys_custom['project']['HKPF']):?>
						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Grade']?></td>
								<td>
									<input name="grade" type="text" id="grade" class="textbox_name" value="<?=$grade?>"/>
								</td>
						  </tr>

						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Duty']?></td>
								<td>
									<input name="duty" type="text" id="duty" class="textbox_name" value="<?=$duty?>" />
								</td>
						  </tr>
<?php endif;?>							
						  <tr>
						    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
						    </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$i_general_name?></td>
						    <td class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)
						      <input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>" maxlength="250"/>
						    </td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>" maxlength="50"/>
						   </td>
						  </tr>
						  
<?php if (!$sys_custom['project']['HKPF']):?>						  
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserNickName?></td>
						    <td><input name="nickname" type="text" id="nickname" class="textbox_name" value="<?=$nickname?>" maxlength="100"/></td>
						  </tr>
						  <tr>
						    <td><span class="tabletextrequire">*</span><?=$i_UserGender?></td>
						    <td><input type="radio" name="gender" id="genderM" value="M"  <? if($gender=="M" || $gender=="") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F" <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$Lang['AccountMgmt']['UserDisplayTitle']?></td>
						    <td><span class="sub_row_content">(<?=$ip20_lang_eng?>)</span>
						      <input name="engTitle" type="text" id="engTitle" class="textbox_name" value="<?=$engTitle?>" maxlength="100"/> <?=$select_eng_title?>
						       </td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiTitle" type="text" id="chiTitle" class="textbox_name" value="<?=$chiTitle?>" maxlength="50"/> <?=$select_chi_title?>
						       </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserAddress?></td>
						    <td><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="<?=$sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']?'3':'4'?>"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>" maxlength="20" /><? if (isset($plugin['sms']) && $plugin['sms']){ echo $i_UserMobileSMSNotes; }?></td>
						  </tr>
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
						<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Fax']?></td>
							<td><input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="10"/>(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)</td>
						</tr>
<?php
	}
	else {
?>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="20" /></td>
						  </tr>
<?php		
	}	
?>								
						  <?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>	
								<tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['DisplayPriority']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTBOX_NUMBER("DisplayOrder", "DisplayOrder", 0, '', array('onchange'=>'restrictInteger(this);'));?><br /><span class="tabletextremark">(<?=$Lang['AccountMgmt']['DisplayPriorityRemark']?>)</span>
									</td>
								</tr>
						  <?php } ?>
<?php endif; // not HKPF?>						  
						  
						  <!-- Additional Info //-->
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td>
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
<?php if($plugin['SDAS_module']['KISMode']):?>
<?php include($PATH_WRT_ROOT."/home/cees_kis/lang/".$intranet_session_language.".php");?>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['Position']?></td>
									<td>
										<?=getSelectByAssoArray($Lang['CEES']['MonthlyReport']['JSLang']['PositionName'], " name='position' id='form_position' ", $position);?>
									</td>
								</tr>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['ContractType']?></td>
									<td>
										<?=getSelectByAssoArray(array('1' => $Lang['CEES']['MonthlyReport']['JSLang']['Permanent'], '4' => $Lang['CEES']['MonthlyReport']['JSLang']['Contract']), " name='contractType' id='form_contractType' ", $contractType);?>
									</td>
								</tr>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['EffectiveDate']?></td>
									<td>
										<?=$linterface->GET_DATE_PICKER("contractStartDate", $contractStartDate,"","yy-mm-dd","","","","","",0, $CanEmptyField=1);?>
									</td>
								</tr>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['TerminationDate']?></td>
									<td>
										<?=$linterface->GET_DATE_PICKER("contractEndDate", $contractEndDate,"","yy-mm-dd","","","","","",0, $CanEmptyField=1);?>
									</td>
								</tr>
<?php endif;?>
<?php if (!$sys_custom['project']['HKPF']):?>								
						  <tr>
						    <td colspan="2"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  </tr>
<?php endif;?>						  
						  <?if(!$plugin['imail_gamma']) {?>
						    	<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(TYPE_TEACHER, $webmail_identity_allowed)) { ?>
								  <tr>
								    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
								    <td><input type="checkbox" value="1" name="open_webmail" id="open_webmail" <?if(!isset($open_webmail) || $open_webmail==1) echo "checked";?>/>
								      <label for="open_webmail"><?=$i_Mail_AllowSendReceiveExternalMail?></label></td>
								  </tr>
						  		<? } ?>
						  <? } ?>
						  <? if(isset($plugin['personalfile']) && $plugin['personalfile'] && in_array(TYPE_TEACHER,$personalfile_identity_allowed)) { ?>
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iFolder']?></td>
							    <td><input type="checkbox" value="1" name="open_file" id="open_file" <?if(!isset($open_file) || $open_file==1) echo "checked";?>/>
							      <label for="open_file"><?=$i_Files_OpenAccount?></label></td>
							  </tr>
						  <? } ?>
						  
						  <?php if($plugin['radius_server']){ ?>
						  <tr>
						  	<td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['WifiUsage']?> -</em></td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></td>
						  	<td><input type="checkbox" name="enable_wifi_access" value="1" checked="checked" /></td>
						  </tr>
						  <?php } ?>
						</table>
						<p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                          <p class="spacer"></p>
<?php if (!$sys_custom['project']['HKPF']):?>                          
                          <?= $linterface->GET_ACTION_BTN($button_continue, "button", "goSubmit('new2.php')")?>
<?php endif;?>                          
                          <?= $linterface->GET_ACTION_BTN($button_finish, "button", "goSubmit('new_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('index.php')")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>