<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
//include_once($PATH_WRT_ROOT."includes/libdb.php");

//intranet_auth();
//intranet_opendb();

if($ssoservice["Google"]["Valid"]){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	
	$google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
	foreach((array)$google_apis as $google_api){
		$config_index = $google_api->getConfigIndex();
		$accounts[$config_index] = false;
		try{
			$accounts[$config_index] = ($google_api->getGoogleAccountByEmail($_POST['userLogin'].'@'.$google_api->getDomain()) == null ? false : true);
		}catch(Exception $e){
			$accounts[$config_index] = false;
		}
	}
	
	echo json_encode(array(
		'accounts'=>$accounts
	));
}

//intranet_closedb();
?>