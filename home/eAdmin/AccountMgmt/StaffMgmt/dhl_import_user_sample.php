<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	intranet_closedb();
	header("Location:index.php");
	exit;
}

$lexport = new libexporttext();
$libdhl = new libdhl();

$export_column = $libdhl->getImportUserHeaderColumns($type);
$rows = array();

$samples = array();
$samples[] = array("UserLogin"=>"00001","Password"=>"abc123","UserEmail"=>"00001@email.com","EnglishName"=>"Chan Tai Man","Company"=>"HKCO","Division"=>"CS","Department"=>"CCC","JoinDate"=>"2017-05-01","EffectiveDate"=>"2017-05-01","TerminatedDate"=>"2018-04-30","Status"=>"1");
$samples[] = array("UserLogin"=>"00002","Password"=>"abc123","UserEmail"=>"00002@email.com","EnglishName"=>"John Lee","Company"=>"CAH","Division"=>"CAH","Department"=>"HEG","JoinDate"=>"2017-06-01","EffectiveDate"=>"2017-06-05","TerminatedDate"=>"","Status"=>"0");
$samples[] = array("UserLogin"=>"00011","Password"=>"abc123","UserEmail"=>"00011@email.com","EnglishName"=>"Amy Kwok","Company"=>"HKCO","Division"=>"FA","Department"=>"ACC","JoinDate"=>"2017-04-01","EffectiveDate"=>"2017-04-03","TerminatedDate"=>"","Status"=>"1");

foreach($samples as $sample)
{
	$row = array();
	foreach($sample as $key => $val)
	{
		if(in_array($key,$export_column)){
			$row[] = $val;
		}
	}
	$rows[] = $row;
}

/*
$rows[] = array("00001","abc123","00001@email.com","Chan Tai Man","HKCO","CS","CCC","2017-05-01","2017-05-01","2018-04-30","1");
$rows[] = array("00002","abc123","00002@email.com","John Lee","CAH","CAH","HEG","2017-06-01","2017-06-05","","0");
$rows[] = array("00011","abc123","00011@email.com","Amy Kwok","HKCO","FA","ACC","2017-04-01","2017-04-03","","1");
*/

$export_content = $lexport->GET_EXPORT_TXT($rows, $export_column);	
$filename = "import_users_".strtolower($type)."_sample.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
exit;
?>