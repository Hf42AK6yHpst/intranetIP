<?php
# using:   

###
### 		!!!!! Note: Please also upload templates/new.tmpl.php when update this file since 2015-12-17 		!!!!! 
###

############# Change Log
#   2019-08-08 Paul
#   		handle Google SSO quota checking by try / catch
#   2019-04-30 Cameron
#           fix cross site scripting by applying cleanHtmlJavascript() to variables
#   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#   2018-08-21 (Cameron) [ip.2.5.9.10.1]: by pass gender checking for HKPF
#   2018-08-14 (Cameron) [ip.2.5.9.10.1]: password field is not mandatory for HKPF
#	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#	2017-12-22 Cameron
#			configure for Oaks refer to Amway
#
#   2016-11-29 (HenryHM) [ip.2.5.8.1.1]: $ssoservice["Google"]["Valid"] - add GSuite Logic
#	2016-10-04 (Carlos) [ip.2.5.8.1.1]: $sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
#	2016-09-28 (Ivan) [ip.2.5.7.10.1]
#			Added teaching = 'S' supply teacher logic
#	2015-12-17 (Cameron)
#			move layout to template folder to customize layout [Amway #P89445]
#	2015-03-19 (Cameron)
#			use fax field for recording date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true  
#	2015-01-12 (Omas):	 new js file for delay checking loginID templates/2009a/js/account_mgmt_checking.js
#	2014-05-23 (Carlos): Fix $sys_custom['iMailPlus']['EmailAliasName'] js checking
#	2014-02-20 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - add specific error msg for each reason
#	2013-12-12 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - user can input its own iMail plus user name
#	2013-08-13 (Carlos): hide WEBSAMS staff code for KIS
#	2012-08-30 (YatWoon): add barcode
#	2012-06-08 (Carlos): added password remark
#
#	Date:	2011-11-01 Henry chow
#			Add "Staff Code" field
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if($sys_custom['DHL']){
	include("./dhl_user.php");
	exit;
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$lu = new libuser();
$thisUserType = USERTYPE_STAFF;
$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$thisUserType));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6) $PasswordLength = 6;

$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

if ($intranet_session_language!="gb") {
	$chi_title_array = array($i_general_Principal,$i_general_VPrincipal,$i_general_Director);
}
else {
	$chi_title_array = array();
}

$sql = "SELECT DISTINCT TitleChinese FROM INTRANET_USER WHERE TitleChinese IS NOT NULL AND TitleChinese !='' ORDER BY TitleChinese";
$temp = $laccount->returnVector($sql);
if (sizeof($temp)!=0) {
	$chi_title_array = array_merge($chi_title_array, $temp);
	$chi_title_array = array_unique($chi_title_array);
	$chi_title_array = array_values($chi_title_array);
}
$sql = "SELECT DISTINCT TitleEnglish FROM INTRANET_USER WHERE TitleEnglish IS NOT NULL AND TitleEnglish !='' ORDER BY TitleEnglish";
$eng_title_array = $laccount->returnVector($sql);
if (sizeof($eng_title_array)!=0) {
	$select_eng_title = getSelectByValue($eng_title_array, "onChange=this.form.engTitle.value=this.value");
} else
{
	$select_eng_title = "";
}
$select_chi_title = getSelectByValue($chi_title_array, "onChange=this.form.chiTitle.value=this.value");

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

# imail gamma
if($plugin['imail_gamma'])
{
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_TEACHER);
	$EmailQuota = $DefaultQuota[1];
}

if($ssoservice["Google"]["Valid"]){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	
	$google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
	$lib_google_sso = new libGoogleSSO();
	foreach((array)$google_apis as $google_api){
		$config_index = $google_api->getConfigIndex();
		try{
			$remaining_quota[$config_index] = $lib_google_sso->getRemainingQuota($google_api);
		}catch(Exception $e){
			$remaining_quota[$config_index] = false;
		}
		$total_quota[$config_index] = $ssoservice["Google"]['api'][$config_index]['quota'];
	}
}
    
$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputStaffDetails'], 1);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['SelectGroupAndRole'], 0);

if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script src="<?=$PATH_WRT_ROOT.'/templates/2009a/js/account_mgmt_checking.js'?>" type="text/javascript"></script>
<script language="javascript">
function goSubmit(urlLink) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>		
	var obj = document.form1;
	show_checking('userlogin','spanChecking');
	
	if(obj.userlogin.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$i_UserLogin?>");	
		return false;
	}

<?php if (!$sys_custom['project']['HKPF']):?>	
	if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin," ".$i_UserPassword?>")) return false;
	if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
<?php endif;?>	
	if (obj.pwd.value != obj.pwd2.value)
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword'] && !$sys_custom['project']['HKPF']){ ?>	
	var check_password_result = CheckPasswordCriteria(obj.pwd.value,obj.userlogin.value,<?=$PasswordLength?>);
	if(check_password_result.indexOf(1) == -1){
		var password_warning_msg = '';
		for(var i=0;i<check_password_result.length;i++){
			password_warning_msg += password_warnings[check_password_result[i]] + "\n";
		}
		alert(password_warning_msg);
		obj.pwd.focus();
		return false;
	}
<?php } ?>	
<?php if (!$sys_custom['project']['CourseTraining']['IsEnable']): ?>
	var identityValid = true;	
	if(obj.teaching[0].checked==false && obj.teaching[1].checked==false) {
		<?php if ($plugin['SLRS']) { ?>
	        if(obj.teaching[2].checked==false) {
	        	identityValid = false;
	        }
        <?php } else { ?>
			identityValid = false;
		<?php } ?>
	}
	
	if (identityValid == false) {
		alert("<?=$i_alert_pleaseselect." ".$i_identity?>");
		return false;
	}
<?php endif; ?>	
	
	if (obj.email) {
		if( obj.email.value != "")
		{
			if(!validateEmail(obj.email, "<?=$i_invalid_email?>")) return false;
		}
	//	else
	//	{
	//		alert("<?=$i_alert_pleasefillin." ".$Lang['Gamma']['UserEmail']?>");
	//		return false;
	//	}	
	}
	
<?php if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){ ?>	
	if(obj.UseImapUserLogin.value == 1) {
		if($.trim(obj.ImapUserLogin.value) == ""){
			obj.ImapUserLogin.focus();
			alert("<?=$i_alert_pleasefillin." ".$Lang['Gamma']['Email']?>");
			return false;
		}
	}
<?php } ?>
	
	if(obj.status[0].checked==false && obj.status[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
<?php if (!$sys_custom['project']['HKPF']):?>	
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
<?php endif;?>	
	if(obj.available.value==0) {
		//alert("<?=$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']?>");
		alert("<?=$i_UserLogin.$Lang['StudentRegistry']['IsInvalid']?>");
		return false;	
	}
	if(obj.available.value==2) {
		alert("<?=$i_UserLogin.$Lang['AccountMgmt']['StartedWithA2Z']?>");
		return false;	
	}	
	/*
	if(obj.address.value.length<=0) {
		alert("<?=$i_alert_pleasefillin." ".$i_UserAddress?>");	
		return false;			
	}
	*/
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
	if(obj.faxPhone.value!="") {
		if(!check_date(obj.faxPhone,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
<?php
	}
?>	
<?php if($ssoservice["Google"]["Valid"]){ ?>
	if($('#div_gmail .is_display_options').is(':checked')){
		var obj_google_radio_buttons = $('div#div_gmail input[type="radio"]');
								
		var is_checked = false;
		obj_google_radio_buttons.each(function(){
			if($(this).is(':checked')){
				is_checked = true;
			}
		});
		
		if(!is_checked){
			alert('<?php echo Get_Lang_Selection('請選擇 G Suite 帳戶', 'Please choose a G Suite account'); ?>')
			return false;
		}
	}
<?php } ?>
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink) {
	document.form1.action = urlLink;
	document.form1.submit();
}

function updateIMapEmail(obj)
{
	$("span#IMapUserEmail").html(obj.value.Trim()+"@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?>");
}

function ToggleImapUserLogin(onOff)
{
	if(onOff) {
		$("span#IMapUserEmail").hide();
		$("#ImapUserLogin").val($.trim($("input#userlogin").val()));
		$("#ImapUserLogin").show();
		$("#ImapUserLoginDomain").show();
		$("#BtnCancelImapUserLogin").show();
		$("#BtnEditImapUserLogin").hide();
		$("input#UseImapUserLogin").val(1);
	}else{
		$("span#IMapUserEmail").show();
		$("#ImapUserLogin").hide();
		$("#ImapUserLoginDomain").hide();
		$("#BtnCancelImapUserLogin").hide();
		$("#BtnEditImapUserLogin").show();
		$("input#UseImapUserLogin").val(0);
	}
}
</script>
<script language="JavaScript">
var ClickID = '';
var callback_checking = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('spanChecking').innerHTML = tmp_str;
	    DisplayPosition();
	    $('#spanCheckingImage').hide();
	}
}


function show_checking(type, click)
{
	$('#spanCheckingImage').show();
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "../ajax_checking.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_checking);
}


function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('spanChecking').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('spanChecking').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('spanChecking').style.visibility='visible';
}
<?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>
function restrictInteger(obj)
{
	var val = $.trim(obj.value);
	var num = parseInt(val);
	if(isNaN(num) || val == ''){
		num = 0;
	}
	obj.value = num;
}
<?php } ?>
</script>
<form name="form1" method="post" action="">
<?php
    if ($sys_custom['project']['CourseTraining']['IsEnable']) {
		include("templates/new_amway.tmpl.php");
	}
	else {
		include("templates/new.tmpl.php");
	}
?>

<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="ClickID" id="ClickID" value="">
<input type="hidden" name="uid" id="uid" value="<?=IntegerSafe($uid)?>">
<input type="hidden" name="TeachingType" id="TeachingType" value="<?=cleanHtmlJavascript($TeachingType)?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=cleanHtmlJavascript($recordstatus)?>">
<input type="hidden" name="keyword" id="keyword" value="<?=cleanHtmlJavascript($keyword)?>">
</form>
<script language="javascript">
	show_checking('userlogin','spanChecking');
</script>
<?php if($ssoservice["Google"]["Valid"]){ ?>
	<script>
		$('#userlogin').bind('change',function(){
			$('.GSuiteUserLogin').html($(this).val());
		});
	</script>
<?php } ?>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>