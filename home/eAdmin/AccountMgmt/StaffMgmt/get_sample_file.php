<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();
$lrepairsystem = new librepairsystem();


if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$_GET["file"]) {
	header ("Location: /");
	exit();
}

$filename = basename($_GET["file"]);

$ref = @$HTTP_REFERER;
$url_format = parse_url($ref);
$path = $intranet_root.str_replace(basename($url_format["path"]), "", $url_format["path"]).$filename;

$parts = explode(".", $filename);
if (is_array($parts) && count($parts) > 1)
	$extension = strtoupper(end($parts));

# Only allow plain text files on the server to be read
$allow_ext = array("CSV", "TXT", "LOG");

$handle = @fopen($path,"r");

if ($handle && in_array($extension, $allow_ext)) {
	$data = fread($handle, filesize($path));
	
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	header('Content-type: application/octet-stream');
	header('Content-Length: '.strlen($data));

	header('Content-Disposition: attachment; filename="'.$filename.'";');

	print $data;
}
else {
	header ("Location: /");
	exit();
}
			
?>