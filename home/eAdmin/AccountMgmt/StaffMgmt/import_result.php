<?
// Modifying by: henry

$PATH_WRT_ROOT="../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0)
	header("Location: import.php");

$limport = new libimporttext();
$lo = new libfilesystem();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

if(is_array($data))
{
	$col_name = array_shift($data);
}

$file_format = array('User Login','Password','Identity','Email','Smart Card ID','English Name','Chinese Name','NickName','Gender','Display English Title','Display Chinese Title','Address','Country','Home Telephone','Office Telephone','Mobile Telephone','iMail','iFolder');
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}
if(sizeof($data)==0)
{
	header("location: import.php?xmsg=import_no_record");
	exit();
}


$laccount = new libaccountmgmt();
$linterface = new interface_html();

# menu highlight setting
$CurrentPageArr['StaffMgmt'] = 1;
//$CurrentPage = "User List";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();


### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserLogin</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserPassword</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_identity."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserEmail."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_SmartCard_CardID."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserEnglishName</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserChineseName."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserNickName."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserGender."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserTitle."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserDisplayTitle_English."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserDisplayTitle_Chinese."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserAddress."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserCountry."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserHomeTelNo."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserOfficeTelNo."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserMobileTelNo."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_StaffAttendance_Group_Title."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['AccountMgmt']['RoleID']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Mail_AllowSendReceiveExternalMail."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Files_OpenAccount."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$x .= "</tr>";
/*
$sql = "drop table temp_staff_account_import";
$laccount->db_db_query($sql);
*/
$sql = "CREATE TABLE IF NOT EXISTS temp_staff_account_import
	(
		UserLogin varchar(20) UNIQUE,
		UserPassword varchar(20),
		Teaching char(2),
		UserEmail varchar(100) UNIQUE,
		CardID varchar(255),
		EnglishName varchar(255),
		ChineseName varchar(100),
		NickName varchar(100),
		Gender char(2),
		EnglishTitle varchar(100),
		ChineseTitle varchar(100),
		Address varchar(255),
		Country varchar(50),
		HomeTelNo varchar(20),
		OfficeTelNo varchar(20),
		MobileTelNo varchar(20),
		iMail char(2),
		iFolder char(2),
		DateInput datetime,
		InputBy int(8),
		RecordType char(2),
		RecordStatus char(2)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
$laccount->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_staff_account_import');
$columns = mysql_num_fields($fields);
for ($i = 0; $i < $columns; $i++) {$field_array[] = mysql_field_name($fields, $i);}

# delete the temp data in temp table 
$sql = "DELETE FROM temp_staff_account_import WHERE InputBy=$UserID";
$laccount->db_db_query($sql) or die(mysql_error());

$error_occured = 0;

for($i=0;$i<sizeof($data);$i++)
{
	$error = array();
	$PicIDArr = array();
	
	### get data
	//list($UserLogin, $UserPassword, $Identity, $UserEmail, $CardID, $EnglishName, $ChineseName, $NickName, $Gender, $Title, $EnglishTitle, $ChineseTitle, $Address, $Country, $HomeTelNo, $OfficeTelNo, $MobileTelNo, $GroupID, $RoleID, $iMail, $iFolder) = $data[$i];
	list($UserLogin, $UserPassword, $Identity, $UserEmail, $CardID, $EnglishName, $ChineseName, $NickName, $Gender, $EnglishTitle, $ChineseTitle, $Address, $Country, $HomeTelNo, $OfficeTelNo, $MobileTelNo, $iMail, $iFolder) = $data[$i];
	
	$UserLogin = trim($UserLogin);
	$UserPassword = trim($UserPassword);
	$Identity = trim($Identity);
	$UserEmail = trim($UserEmail);
	$CardID = trim($CardID);
	$EnglishName = intranet_htmlspecialchars(trim($EnglishName));
	$ChineseName = intranet_htmlspecialchars(trim($ChineseName));
	$NickName = intranet_htmlspecialchars(trim($NickName));
	//$Title = intranet_htmlspecialchars(trim($Title));
	$EnglishTitle = intranet_htmlspecialchars(trim($EnglishTitle));
	$ChineseTitle = intranet_htmlspecialchars(trim($ChineseTitle));
	$Address = intranet_htmlspecialchars(trim($Address));
	$Country = intranet_htmlspecialchars(trim($Country));
			
	# User Login
	if(trim($UserLogin)=="") {
		$error['UserLogin'] = $ec_html_editor['missing']." ".$i_UserLogin;
	} else {
		# check duplication
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$UserLogin'";
		$result = $laccount->returnVector($sql);
		
		if(sizeof($result)!=0) {
			$error['UserLogin'] = $i_UserLogin." ".$Lang['AccountMgmt']['UsedByOtherUser'];					
		}
	}

	# User Password
	if(trim($UserPassword)=="") {
		$error['UserPassword'] = $ec_html_editor['missing']." ".$i_UserPassword;
	}
	
	# Identity
	if(trim($Identity)=="") {
		$error['Identity'] = $ec_html_editor['missing']." ".$i_identity;
	} else {
		$identityAry = array("NT"=>"Non-Teaching", "T"=>"Teaching");	
		if(!array_key_exists($Identity, $identityAry))
			$error['Identity'] = $i_identity." ".$ec_html_editor['incorrect'];
		else {
			$identityID = ($Identity=="T") ? 1 : 0;	
		}
	}
	
	# UserEmail
	if(trim($UserEmail)=="") {
		$error['UserEmail'] = $ec_html_editor['missing']." ".$i_UserEmail;
	} else {
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail='$UserEmail'";	
		$result = $laccount->returnVector($sql);
		if(sizeof($result)!=0) {
			$error['UserEmail'] = $i_UserEmail.$Lang['AccountMgmt']['UsedByOtherUser'];
		}
	}
		
	# Smart Card ID
	if(trim($CardID)=="") {
		$CardID = "---";
	} else {
		$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$CardID'";	
		$result = $laccount->returnVector($sql);
		if(sizeof($result)!=0) {
			$error['CardID'] = $i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser'];
		}
	}
	
	if(trim($EnglishName)=="" && trim($ChineseName)=="") {
		$error['Name'] = $ec_html_editor['missing']." ".$i_general_name;
	} else {
		# English Name
		if(trim($EnglishName)=='') {
			$EnglishName = "---";	
		}
		
		# Chinese Name
		if(trim($ChineseName)=='') {
			$ChineseName = "---";	
		}
	}
	
	# NickName
	if(trim($NickName)=='') {
		$NickName = "---";	
	}

	# Gender
	if(trim($Gender)=='') {
		$error['Gender'] = $ec_html_editor['missing']." ".$i_UserGender;
	}
	/*
	# Title
	if(trim($Title)=='') {
		$Title = "---";	
	} else {
		$titleAry = array("Mr.","Miss","Mrs.","Ms.","Dr.","Prof.");	
		if(!in_array(trim($Title), $titleAry))
			$error['Title'] = $i_UserTitle." ".$ec_html_editor['incorrect'];
	}
	*/
	# English Title
	if(trim($EnglishTitle)=='') {
		$EnglishTitle = "---";	
	}

	# Chinese Title
	if(trim($ChineseTitle)=='') {
		$ChineseTitle = "---";	
	}
	/*
	# Address
	if(trim($Address)=='') {
		$error['Gender'] = $ec_html_editor['missing']." ".$i_UserAddress;
	}
	
	# Group
	if(trim($GroupID)=='') {
		$GroupID = "---";	
	} else {
		$gp = explode(',', $GroupID);
		$gpName = "";
		for($j=0; $j<sizeof($gp); $j++) {
			$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID=".$gp[$j];
			$gpTitle = $laccount->returnVector($sql);
			if($gpTitle[0])
				//$gpName .= ($gpName!="") ? ", ".$gpTitle[0] : $gpTitle[0];
				$gpName .= "<li>".$gpTitle[0]."</li>";
			else 
				$error['GroupID'] = $Lang['AccountMgmt']['SomeGroupsDoNotExist'];
		}
	}

	# Role
	if(trim($RoleID)=='') {
		$RoleID = "---";	
	} else {
		$rID = explode(',', $RoleID);
		$rName = "";
		for($j=0; $j<sizeof($rID); $j++) {
			$sql = "SELECT RoleName FROM ROLE WHERE RoleID=".$rID[$j];
			$rolename = $laccount->returnVector($sql);
			if($rolename[0])
				$rName .= "<li>".$rolename[0]."</li>";
			else 
				$error['RoleID'] = $Lang['AccountMgmt']['SomeRolesDoNotExist'];
		}
	}
	*/
	# iMail
	if(trim($iMail)!="Y" && trim($iMail)!="N" && trim($iMail)!="") {
		$error['iMail']	= $ip20TopMenu['iMail']." ".$ec_html_editor['incorrect'];
	} else if(trim($iMail)=='N') {
		$iMail = "";
	}
		
	# iFolder
	if(trim($iFolder)!="Y" && trim($iFolder)!="N" && trim($iFolder)!="") {
		$error['iFolder']	= $ip20TopMenu['iFolder']." ".$ec_html_editor['incorrect'];
	} else if(trim($iFolder)=='N') {
		$iFolder = "";
	}
				

		
	$css = (sizeof($error)==0) ? "tabletext":"red";

	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">".$UserLogin."</td>";
	$x .= "<td class=\"$css\">".$UserPassword."</td>";
	$x .= "<td class=\"$css\">".$identityAry[$Identity]."</td>";
	$x .= "<td class=\"$css\">".$UserEmail."</td>";
	$x .= "<td class=\"$css\">".$CardID."</td>";
	$x .= "<td class=\"$css\">".$EnglishName."</td>";
	$x .= "<td class=\"$css\">".$ChineseName."</td>";
	$x .= "<td class=\"$css\">".$NickName."</td>";
	$x .= "<td class=\"$css\">".$Gender."</td>";
	//$x .= "<td class=\"$css\">".$Title."</td>";
	$x .= "<td class=\"$css\">".$EnglishTitle."</td>";
	$x .= "<td class=\"$css\">".$ChineseTitle."</td>";
	$x .= "<td class=\"$css\">".$Address."</td>";
	$x .= "<td class=\"$css\">".$Country."</td>";
	$x .= "<td class=\"$css\">".$HomeTelNo."</td>";
	$x .= "<td class=\"$css\">".$OfficeTelNo."</td>";
	$x .= "<td class=\"$css\">".$MobileTelNo."</td>";
	//$x .= "<td class=\"$css\">".$gpName."</td>";
	//$x .= "<td class=\"$css\">".$rName."</td>";
	$x .= "<td class=\"$css\">".(($iMail=="")?"N":$iMail)."</td>";
	$x .= "<td class=\"$css\">".(($iFolder=="")?"N":$iFolder)."</td>";
	$x .= "<td class=\"$css\">";
	
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .= "<li>".$Value.'</li>';
		}	
	} else {
		$sql = "INSERT INTO temp_staff_account_import VALUES (
				'".$UserLogin."',
				'".$UserPassword."',
				'".$identityID."',
				'".$UserEmail."',
				'".$CardID."',
				'".$EnglishName."',
				'".$ChineseName."',
				'".$NickName."',
				'".$Gender."',
				'".$EnglishTitle."',
				'".$ChineseTitle."',
				'".$Address."',
				'".$Country."',
				'".$HomeTelNo."',
				'".$OfficeTelNo."',
				'".$MobileTelNo."',
				'".$iMail."',
				'".$iFolder."',
				NOW(),
				'$UserID',
				'".TYPE_TEACHER."',
				'".STATUS_APPROVED."'
				)";
		$laccount->db_db_query($sql);
	}
	
	$x.="</td>";
	$x .= "</tr>";


	if($error)
	{
		$error_occured++;
	}	
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");	
}

?>

<br />
<form name="form1" method="post" action="import_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>