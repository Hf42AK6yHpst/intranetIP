<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//$laccount = new libaccountmgmt();
$linterface = new interface_html();
//$lo = new libgrouping();
//$lrole = new librole();
$libdhl = new libdhl();

$is_edit = false;
if(isset($_POST['user_id'])){
	$user_id = is_array($_REQUEST['user_id'])? IntegerSafe($_REQUEST['user_id'][0]) : IntegerSafe($_REQUEST['user_id']);
	$userRecord = $libdhl->getUserRecords(array('user_id'=>$user_id));
	if($userRecord == 0){
		intranet_closedb();
		header("Location:index.php");
		exit;
	}
	$userInfo = $userRecord[0];
	$departmentUserRecord = $libdhl->getDepartmentUserRecords(array('DepartmentUserID'=>$userInfo['UserID']));
	$DepartmentID = count($departmentUserRecord)>0 ? $departmentUserRecord[0]['DepartmentID'] : '';
	$is_edit = true;
	$hidden_userid_field = '<input type="hidden" name="user_id" id="user_id" value="'.$user_id.'" />';
}else{
	$userInfo = array();
}


$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($is_edit? $Lang['AccountMgmt']['EditUser'] : $Lang['AccountMgmt']['NewUser']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($is_edit? $Lang['AccountMgmt']['EditUser'] : $Lang['AccountMgmt']['NewUser'], "");

//$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
if(isset($_SESSION['DHL_USERMGMT_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['DHL_USERMGMT_RETURN_MSG'];
	unset($_SESSION['DHL_USERMGMT_RETURN_MSG']);
}
if($xmsg != '' &&  isset($Lang['General']['ReturnMessage'][$xmsg]))
{
	$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
}
$linterface->LAYOUT_START($ReturnMsg);
?>
<br />
<form id="form1" name="form1" method="post" action="" onsubmit="return false;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td class="board_menu_closed">
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                      <div class="table_board">
                      	<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
                      	  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td>
						    	<?php if($is_edit){ ?>
						    	<?=$userInfo['UserLogin']?>
						    	<input name="UserLogin" type="hidden" id="UserLogin" class="textboxnum" value="<?=$userInfo['UserLogin']?>" />
						    	<?php }else{ ?>
						    	<input name="UserLogin" type="text" id="UserLogin" class="textboxnum required" value="<?=$userInfo['UserLogin']?>" maxlength="20" />
						    	<span id="spanChecking"></span>
						    	<span id="spanCheckingImage" style="display:none;"><?=$linterface->Get_Ajax_Loading_Image()?></span>
						    	<?=$linterface->Get_Thickbox_Warning_Msg_Div("UserLogin_Error",'', "WarnMsg")?>
								<?php } ?>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
						    <td>
						    	<input name="UserPassword" type="password" id="UserPassword" class="textboxnum<?=$is_edit?"":" required"?>" value="" maxlength="20"/>
						    	<?=$linterface->Get_Thickbox_Warning_Msg_Div("UserPassword_Error",'', "WarnMsg")?>
						    </td>
						  </tr>
						  <tr>
						    <td>
						    	<input name="UserPassword2" type="password" id="UserPassword2" class="textboxnum<?=$is_edit?"":" required"?>" value="" maxlength="20"/> (<?=$Lang['AccountMgmt']['Retype']?>)
						    	<?=$linterface->Get_Thickbox_Warning_Msg_Div("UserPassword2_Error",'', "WarnMsg")?>
						     </td>
						  </tr>
						  <!--
						  <tr>
							<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
						  -->
						  <tr>
						    <td class="formfieldtitle"><?=$Lang['Gamma']['UserEmail']?></td>
						    <td>
						    	<input name="UserEmail" type="text" id="UserEmail" class="textbox_name" value="<?=$userInfo['UserEmail']?>" maxlength="100" />
						    	<?=$linterface->Get_Thickbox_Warning_Msg_Div("UserEmail_Error",'', "WarnMsg")?>
						    	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_name?></td>
						    <td>
						      <input name="EnglishName" type="text" id="EnglishName" class="textbox_name required" value="<?=$userInfo['EnglishName']?>" maxlength="250" />
						      <?=$linterface->Get_Thickbox_Warning_Msg_Div("EnglishName_Error",'', "WarnMsg")?>
						    </td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['DHL']['JoinDate']?></td>
						  	<td>
						  		<?=$linterface->GET_DATE_PICKER("JoinDate",$userInfo['JoinDate'],$___OtherMember="",$___DateFormat="yy-mm-dd",$___MaskFunction="",$___ExtWarningLayerID="",$___ExtWarningLayerContainer="",$___OnDatePickSelectedFunction="",$___ID="JoinDate",$___SkipIncludeJS=0, $___CanEmptyField=1, $___Disable=false, $___cssClass="textboxnum required")?>
						  		<?=$linterface->Get_Thickbox_Warning_Msg_Div("JoinDate_Error",'', "WarnMsg")?>
						  	</td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['DHL']['EffectiveDate']?></td>
						  	<td>
						  		<?=$linterface->GET_DATE_PICKER("EffectiveDate",$userInfo['EffectiveDate'],$___OtherMember="",$___DateFormat="yy-mm-dd",$___MaskFunction="",$___ExtWarningLayerID="",$___ExtWarningLayerContainer="",$___OnDatePickSelectedFunction="",$___ID="EffectiveDate",$___SkipIncludeJS=0, $___CanEmptyField=1, $___Disable=false, $___cssClass="textboxnum")?>
						  		<?=$linterface->Get_Thickbox_Warning_Msg_Div("EffectiveDate_Error",'', "WarnMsg")?>
						  	</td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['DHL']['TerminatedDate']?></td>
						  	<td>
						  		<?=$linterface->GET_DATE_PICKER("TerminatedDate",$userInfo['TerminatedDate'],$___OtherMember="",$___DateFormat="yy-mm-dd",$___MaskFunction="",$___ExtWarningLayerID="",$___ExtWarningLayerContainer="",$___OnDatePickSelectedFunction="",$___ID="TerminatedDate",$___SkipIncludeJS=0, $___CanEmptyField=1, $___Disable=false, $___cssClass="textboxnum")?>
						  		<?=$linterface->Get_Thickbox_Warning_Msg_Div("TerminatedDate_Error",'', "WarnMsg")?>
						  	</td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['DHL']['Department']?></td>
						  	<td>
						  		<?=$libdhl->getDepartmentSelection("DepartmentID", "DepartmentID", $DepartmentID, $___isMultiple=false, $___hasFirst=true, $___firstText='', $___tags=' class="required" ');?>
						  		<?=$linterface->Get_Thickbox_Warning_Msg_Div("DepartmentID_Error",'', "WarnMsg")?>
						  	</td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="RecordStatus" id="RecordStatus1" value="1" <? if($userInfo['RecordStatus']=="" || $userInfo['RecordStatus']=="1") echo "checked";?>><label for="RecordStatus1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="RecordStatus" id="RecordStatus0" value="0" <? if($userInfo['RecordStatus']=="0") echo "checked";?>><label for="RecordStatus0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						  <?php
						  /*
						  <tr>
						    <td class="formfieldtitle" width="20%"><?=$i_adminmenu_group?></td>
						    <td>
							    <table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td><?=$lo->displayUserGroups(isset($userInfo['UserID'])?$userInfo['UserID']:0); ?></td>
									</tr>
							    </table>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" width="20%"><?=$Lang['Header']['Menu']['Role']?></td>
						    <td>
							    <table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td><?=$lrole->displayRoleSelection(isset($userInfo['UserID'])? $userInfo['UserID']:0); ?></td>
									</tr>
							    </table>
						    </td>
						  </tr>
						  */
						  ?>
                      	</table>
                      	<p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                          <p class="spacer"></p>
                          <?= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit(document.form1)","submit_btn")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('index.php')")?>
						  <p class="spacer"></p>
                      </div>
                  </td>
                </tr>
          </table>
    	</td>
    </tr>
</table>
<?=$hidden_userid_field?>
</form>
<script type="text/javascript" language="javascript">
function goSubmit(formObj)
{
	var is_valid = true;
	var fields = document.getElementsByClassName('required');
	$('.WarnMsg').hide();
	$('#submit_btn').attr('disabled',true);
	
	for(var i=0;i<fields.length;i++){
		if($.trim(fields[i].value) == ''){
			$('#'+fields[i].id+'_Error').html('<?=$Lang['General']['JS_warning']['CannotBeBlank']?>').show();
			is_valid = false;
		}
	}
	
	var pass1 = $.trim($('#UserPassword').val());
	var pass2 = $.trim($('#UserPassword2').val());
	if(pass1 != '' || pass2 != ''){
		if(pass1 != pass2){
			$('#UserPassword_Error').html('<?=$i_frontpage_myinfo_password_mismatch?>').show();
			is_valid = false;
		}
	}
	
	if(!is_valid){
		$('#submit_btn').attr('disabled',false);
		return false;
	}
	
	if(is_valid){
		var submit_check_func = function(){
			var form_data = $(formObj).serialize();
			form_data += '&task=CheckUser';
			$.post(
				'dhl_user_task.php',
				form_data,
				function(returnJson)
				{
					var error_ary = [];
					if(JSON && JSON.parse){
						error_ary = JSON.parse(returnJson);
					}else{
						eval('error_ary='+returnJson+';');
					}
					
					if(error_ary.length > 0){
						valid = false;
						for(var i=0;i<error_ary.length;i++){
							var error_id = error_ary[i][0];
							var error_msg = error_ary[i][1];
							if($('#'+error_id).length>0)
								$('#'+error_id).html(error_msg).show();
						}
						$('#submit_btn').attr('disabled',false);
					}else{
						goFinish();
					}
				}
			);
		}
<?php if($is_edit){ ?>
		submit_check_func();
<?php }else{ ?>
		$('#spanCheckingImage').show();
		$.post(
			'../ajax_checking.php',
			{
				'task':'userlogin',
				'userlogin':$.trim($('#UserLogin').val())
			},
			function(returnHtml){
				$('#spanCheckingImage').hide();
				$('#spanChecking').html(returnHtml);
				if($('#available').val() == '1'){
					submit_check_func();
				}else{
					$('#submit_btn').attr('disabled',false);
				}
			}
		);
<?php } ?>
	}
}

function goFinish() 
{
	document.form1.action = "dhl_user_update.php";
	document.form1.submit();	
}

function goURL(urlLink) 
{
	document.form1.action = urlLink;
	document.form1.submit();
}
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>