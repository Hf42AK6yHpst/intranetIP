<?php
// Modifying by: 

/*
 * 2019-05-13 (Henry) Security fix: SQL without quote
 */

$PATH_WRT_ROOT="../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");


if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_opendb();

$laccount = new libaccountmgmt();
$li = new libregistry();

$sql = "SELECT * FROM temp_staff_account_import";
$recordArr = $laccount->returnArray($sql);

if(sizeof($recordArr)==0)
	header("Location: import.php");

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserLogin</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserPassword</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_identity."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserEmail."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_SmartCard_CardID."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserEnglishName</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserChineseName."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserNickName."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserGender."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserTitle."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserDisplayTitle_English."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserDisplayTitle_Chinese."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserAddress."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserCountry."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserHomeTelNo."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserOfficeTelNo."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserMobileTelNo."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_StaffAttendance_Group_Title."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['AccountMgmt']['RoleID']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Mail_AllowSendReceiveExternalMail."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Files_OpenAccount."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$x .= "</tr>";

$identityAry = array("Non-Teaching", "Teaching");

for($a=0;$a<sizeof($recordArr);$a++)
{
	//list($UserLogin, $UserPassword, $Identity, $UserEmail, $CardID, $EnglishName, $ChineseName, $NickName, $Gender, $Title, $EnglishTitle, $ChineseTitle, $Address, $Country, $HomeTelNo, $OfficeTelNo, $MobileTelNo, $GroupID, $RoleID, $open_file, $open_webmail) = $recordArr[$a];
	list($UserLogin, $UserPassword, $Identity, $UserEmail, $CardID, $EnglishName, $ChineseName, $NickName, $Gender, $EnglishTitle, $ChineseTitle, $Address, $Country, $HomeTelNo, $OfficeTelNo, $MobileTelNo, $open_file, $open_webmail) = $recordArr[$a];
	/*
	# Group
	if(trim($GroupID)=='') {
		$GroupID = "---";	
	} else {
		$gp = explode(',', $GroupID);
		$gpName = "";
		for($j=0; $j<sizeof($gp); $j++) {
			$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID=".$gp[$j];
			$gpTitle = $laccount->returnVector($sql);
			if($gpTitle[0])
				$gpName .= "<li>".$gpTitle[0]."</li>";
		}
	}

	# Role
	if(trim($RoleID)=='') {
		$RoleID = "---";	
	} else {
		$rID = explode(',', $RoleID);
		$rName = "";
		for($j=0; $j<sizeof($rID); $j++) {
			$sql = "SELECT RoleName FROM ROLE WHERE RoleID=".$rID[$j];
			$rolename = $laccount->returnVector($sql);
			if($rolename[0])
				$rName .= "<li>".$rolename[0]."</li>";
		}
	}	
	*/
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">".$UserLogin."</td>";
	$x .= "<td class=\"$css\">".$UserPassword."</td>";
	$x .= "<td class=\"$css\">".$identityAry[$Identity]."</td>";
	$x .= "<td class=\"$css\">".$UserEmail."</td>";
	$x .= "<td class=\"$css\">".$CardID."</td>";
	$x .= "<td class=\"$css\">".$EnglishName."</td>";
	$x .= "<td class=\"$css\">".$ChineseName."</td>";
	$x .= "<td class=\"$css\">".$NickName."</td>";
	$x .= "<td class=\"$css\">".$Gender."</td>";
	//$x .= "<td class=\"$css\">".$Title."</td>";
	$x .= "<td class=\"$css\">".$EnglishTitle."</td>";
	$x .= "<td class=\"$css\">".$ChineseTitle."</td>";
	$x .= "<td class=\"$css\">".$Address."</td>";
	$x .= "<td class=\"$css\">".$Country."</td>";
	$x .= "<td class=\"$css\">".$HomeTelNo."</td>";
	$x .= "<td class=\"$css\">".$OfficeTelNo."</td>";
	$x .= "<td class=\"$css\">".$MobileTelNo."</td>";
	//$x .= "<td class=\"$css\">".$gpName."</td>";
	//$x .= "<td class=\"$css\">".$rName."</td>";
	$x .= "<td class=\"$css\">".(($open_file=="")?"N":$open_file)."</td>";
	$x .= "<td class=\"$css\">".(($open_webmail=="")?"N":$open_webmail)."</td>";
	$x .= "</tr>";
	
	########################################
	########################################
	
	if($li->UserNewAdd($UserLogin, $UserEmail, intranet_htmlspecialchars(trim($EnglishName)), intranet_htmlspecialchars(trim($ChineseName)), STATUS_APPROVED)==1) {
		
		# create & update info in INTRANET_USER
		
		$uid = $li->db_insert_id();
		//$UserPassword = $pwd;
		if ($intranet_authentication_method=="HASH") {
		    $fieldname = "HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		} else {
		    $fieldname = "UserPassword = '$UserPassword', ";
		}
		$fieldname .= "NickName = '".intranet_htmlspecialchars(trim($NickName))."', ";
		//$fieldname .= "Title = '".intranet_htmlspecialchars(trim($Title))."', ";
		$fieldname .= "Gender = '$gender', ";
		$fieldname .= "HomeTelNo = '".intranet_htmlspecialchars(trim($HomeTelNo))."', ";
		$fieldname .= "OfficeTelNo = '".intranet_htmlspecialchars(trim($OfficeTelNo))."', ";
		$fieldname .= "MobileTelNo = '".intranet_htmlspecialchars(trim($MobileTelNo))."', ";
		$fieldname .= "Address = '".intranet_htmlspecialchars(trim($Address))."', ";
		$fieldname .= "Country = '".intranet_htmlspecialchars(trim($Country))."', ";
		$fieldname .= "RecordType = '".TYPE_TEACHER."'";
	
		if ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) {
			$fieldname .= ",CardID = '$CardID'";
		}
		$TitleEnglish = intranet_htmlspecialchars(trim($EnglishTitle));
		$TitleChinese = intranet_htmlspecialchars(trim($ChineseTitle));
		$fieldname .= ",TitleEnglish = '$TitleEnglish'";
		$fieldname .= ",TitleChinese = '$TitleChinese'";
		
		if ($Identity == 1) {   # Teaching staff -> Teacher group
		    $target_idgroup = 1;
		    $fieldname .= ",Teaching = ".TEACHING;
		} else {                   # Non-teaching staff -> Admin staff group
		    $target_idgroup = 3;
		    $fieldname .= ",Teaching = ".NONTEACHING;
		}
		$fieldname .= ", DateModified = now()";
		$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = $uid";
		$li->db_db_query($sql);
		
		# insert into INTRANET_USERGROUP (assign to teacher group / admin staff group)
		$gpID = ($Identity==1) ? 1 : 3;
		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($gpID, $uid, now(), now())";
		$li->db_db_query($sql);
		/*
		# Assign to Group
		$GroupID = explode(',', $GroupID);
		for($i=0; $i<sizeof($GroupID); $i++){
			if($GroupID[$i] != "") {
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (".$GroupID[$i].", $uid, now(), now())";
				$li->db_db_query($sql);
			}
		}
		
		# Assign to Role
		$RoleID = explode(',', $RoleID);
		$identityType = ($Identity==0) ? "NonTeaching" : "Teaching";
		for($i=0; $i<sizeof($RoleID); $i++){
			if($RoleID[$i] != "") {
				$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
				$li->db_db_query($sql);
			}
		}
		*/
		
		/*
		$msg = 1;
		# Call sendmail function (Email notification of registration)
		$mailSubject = registration_title();
		$mailBody = registration_body_new($UserEmail, $UserLogin, $UserPassword, stripslashes($EnglishName), stripslashes($ChineseName));
		$mailTo = $email;
		$lu = new libsendmail();
		$lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);
		
		#### Special handling
		$li->UpdateRole_UserGroup();
		*/
		$acl_field = 0;
		
		# iFolder
		if ($open_file=="Y" && in_array(TYPE_TEACHER, $personalfile_identity_allowed) )
		{
		    if ($personalfile_type=='FTP')
		    {
		        $lftp = new libftp();
		        if ($lftp->isAccountManageable)
		        {
		            $file_failed = ($lftp->open_account($UserLogin,$UserPassword)? 0:1);
		            $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
		            if ($file_content == "")
		            {
		                $userquota = array(10,10,10);
		            }
		            else
		            {
		                $userquota = explode("\n", $file_content);
		            }
		            $target_quota = $userquota[TYPE_TEACHER-1];
		            $lftp->setTotalQuota($UserLogin, $target_quota, "iFolder");
		        }
		    }
		    $acl_field += 2;
		}
		
		# Webmail
		if ($open_webmail=="Y" && in_array(TYPE_TEACHER, $webmail_identity_allowed))
		{
			$lwebmail = new libwebmail();
			if ($lwebmail->has_webmail) {
				$lwebmail->open_account($UserLogin, $UserPassword);
				$acl_field += 1;
				$file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
				if ($file_content == "") {
					$userquota = array(10,10,10);
				}
				else {
					$userquota = explode("\n", $file_content);
				}
				$target_quota = $userquota[TYPE_TEACHER-1];
				$lwebmail->setTotalQuota($UserLogin, $target_quota, "iMail");
			}
		}
		
		$sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, $acl_field)";
		$li->db_db_query($sql);
		
		# LDAP
		if ($intranet_authentication_method=='LDAP')
		{
		    $lldap = new libldap();
		    if ($lldap->isAccountControlNeeded()) {
		        $lldap->connect();
		        $lldap->openAccount($UserLogin, $UserPassword);
		    }
		}
		
		# Payment
		if($plugin['payment']) {
		    $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$uid',0)";
		    $li->db_db_query($sql);
		}
		
		# iMail Quota
		if ($special_feature['imail'])
		{
		    # Get Default Quota
		    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
		    if ($file_content == "")
		    {
		        $quota = 10;
		    }
		    else
		    {
		        $userquota = explode("\n", $file_content);
	            $quotaline = $userquota[TYPE_TEACHER-1];
		        $temp = explode(":",$quotaline);
		        $quota = $temp[1];
		    }
		    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$uid', '$quota')";
		    $li->db_db_query($sql);
		    
		    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$uid', 0)";
		    $li->db_db_query($sql);
		}
		
		
		# SchoolNet integration
		if ($plugin['SchoolNet'])
		{
		    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
		    $lschoolnet = new libschoolnet();
	        # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
	        $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '".$uid."'";
	        $data = $li->returnArray($sql, 11);
	        $lschoolnet->addStaffUser($data);
		}		
	}
	########################################
	########################################
	
}
$x .= "</table>";

$xmsg2 = "<font color=green>".sizeof($recordArr)." ".$iDiscipline['Reord_Import_Successfully']."</font>";


$sql = "DELETE FROM temp_staff_account_import WHERE InputBy=$UserID";
$laccount->db_db_query($sql);

$linterface = new interface_html();

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

# menu highlight setting
$CurrentPageArr['StaffMgmt'] = 1;
//$CurrentPage = "User List";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>