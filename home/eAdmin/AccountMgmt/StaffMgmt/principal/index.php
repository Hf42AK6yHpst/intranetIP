<?php 
# using: Paul

################# Change Log [Start] ############
/*
 * Date: 2019-02-11 Paul
 * 	- Create this file
 */
################# Change Log [End] ############


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$laccount = new libaccountmgmt();
$lgeneralsettings = new libgeneralsettings();
$lui = new form_class_manage_ui();

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Principal";

# Left menu 
$TAGS_OBJ[] = array($Lang['AccountMgmt']['PrincipalMgmt']);
// $MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($xmsg);

$formNameAry = array();
$yearAry = array();

if(!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onChange="Get_Special_User_List()"', $AcademicYearID);

$specialUserList = $laccount->getSpecialIdentityMapping($AcademicYearID);
$thisUserType = USERTYPE_STAFF;

// Identity
// 	1: Principal
//	2: Vice-principal
//	3: Chancellor
$principal = array();
$vicePrincipals = array();
$chancellor = array();
if(!empty($specialUserList)){
	$principal = $specialUserList[1];
	$vicePrincipals = $specialUserList[2];
	$chancellor = $specialUserList[3];
}
?>
<script language="javascript">
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
	$('select[name="vice_principal[]"]').scrollTop($('select[name="vice_principal[]"] option:selected').offset().top-270);
}		

function ResetInfo()
{	
	$('select[name="vice_principal[]"]').scrollTop($('select[name="vice_principal[]"] option:selected').offset().top-270);
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
	
	$('.Edit_Show_EnablePasswordPolicy').attr('style', 'display: none');
}

function check_form()
{
	document.form1.submit();
}
function Get_Special_User_List()
{	
	Block_Element("ClassListLayer");
	$('#form1').attr('action','index.php');
	$('#form1').submit();
}
</script>
		
<form id="form1" name="form1" method="post" action="update.php">

<div class="table_board">
	<div class="table_row_tool ">		
		<?php echo $Lang['StudentRegistry']['AcademicYear']?> : <?php echo $yearSelectionMenu; ?>
	</div>
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['AccountMgmt']['SpecialRole']?> </span>-</em>
		<p class="spacer"></p> 
	</div>
	<table class="form_table_v30">
	
	<!-- Principal //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['SpecialRolePrincipal']?></td>
		<td> 
		<span class="Edit_Hide">
		<?php
			foreach((array)$principal as $p){
				echo $p['name'].'<br>';
			}
		?>
		</span>
		<span class="Edit_Show" style="display:none">
		<? /* ?>
		<input type="checkbox" name="dob" value="1" id="dob" <?=($data['CanUpdateDOB_'.$thisUserType]?"checked":"")?>> <label for="dob"><?=$i_UserDateOfBirth?></label> <br>
		<? */ ?>
		<?php
			echo $lui->Get_Staff_Selection('principal',1,$principal[0]['id']);
		?>
		<br>
		</span></td>
	</tr>
	
	<!-- Vice Principal //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['SpecialRoleVicePrincipal']?></td>
		<td> 
		<span class="Edit_Hide">
		<?php
			foreach((array)$vicePrincipals as $vp){
				echo $vp['name'].'<br>';
			}
		?>
		</span>
		<span class="Edit_Show" style="display:none">
		<?php
			$selectedVP = array();
			foreach((array)$vicePrincipals as $vp){
				$selectedVP[] = $vp['id'];
			}
			echo $lui->Get_Staff_Selection('vice_principal[]',1,$selectedVP,"",1,1);
		?>
		<br>
		</span></td>
	</tr>
	
	<!-- Councellor //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['SpecialRoleChancellor']?></td>
		<td> 
		<span class="Edit_Hide">
		<?php
			foreach((array)$chancellor as $c){
				echo $c['name'].'<br>';
			}
		?>
		</span>
		<span class="Edit_Show" style="display:none">
		<?php
			echo $lui->Get_Staff_Selection('chancellor',0,$chancellor[0]['id']);
		?>
		</span>
		</td>
	</tr>
			
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "button","check_form();","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
