<?php
# using: Paul

################# Change Log [Start] ############
/*
 * Date: 2019-02-11 Paul
 * 	- Create this file
 */
################# Change Log [End] ############


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$lgeneralsettings = new libgeneralsettings();

$result = array();
$thisUserType = USERTYPE_STAFF;

$specialUserList = $laccount->getSpecialIdentityMapping($AcademicYearID,true);

// Identity
// 	1: Principal
//	2: Vice-principal
//	3: Chancellor
$oldList = array();
if(!empty($specialUserList)){
	$oldPrincipal = $specialUserList[1][0];
	$oldVC = $specialUserList[2];
	$oldChancellor = $specialUserList[3][0];
	$oldList = array($oldPrincipal,$oldChancellor);
	if(!empty($oldVC)){
		foreach($oldVC as $ovc){
			$oldList[] = $ovc;
		}
	}
}
$newList = array($principal,$chancellor);
if(!empty($vice_principal)){
	foreach($vice_principal as $vc){
		$newList[] = $vc;
	}
}
$toDeleteRecords = array_diff($oldList,$newList);

$laccount->Start_Trans();

$result[] = $laccount->upsertSpecialIdentityMapping($AcademicYearID, 1, $principal);
if(!empty($vice_principal)){
	foreach($vice_principal as $vc){
		$result[] = $laccount->upsertSpecialIdentityMapping($AcademicYearID, 2, $vc);
	}
}
$result[] = $laccount->upsertSpecialIdentityMapping($AcademicYearID, 3, $chancellor);

foreach($toDeleteRecords as $delete){
	$result[] = $laccount->deleteSpecialIdentityMapping($AcademicYearID,$delete);
}

if(!in_array(false,$result) && !in_array(0,$result)){
	$laccount->Commit_Trans();
	header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
}else{
	$laccount->RollBack_Trans();
	header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateFailed']);
}
intranet_closedb();