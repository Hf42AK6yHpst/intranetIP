<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$add_update_key = isset($_POST['user_id']) && $_POST['user_id']>0 ? 'Update' : 'Add';

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	intranet_closedb();
	header('HTTP/1.0 401 Unauthorized', true, 401);
	exit;
}

$libdhl = new libdhl();
$JSON = new JSON_obj();

$task = $_REQUEST['task'];

switch($task)
{
	case 'CheckUser': 
		$user_login = trim($_POST['UserLogin']);
		$data = array('CheckUserLogin'=>1,'UserLogin'=>$user_login);
		if(isset($_POST['user_id'])){
			$data['ExcludeUserID'] = $_POST['user_id'];
		}
		$check_userlogin_records = $libdhl->getUserRecords($data);
		
		$json_ary = array();
		if(count($check_userlogin_records)>0){
			$json_ary[] = array('UserLogin_Error', $i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']);
		}
		if(trim($_POST['UserEmail']) != ''){
			$user_email = trim($_POST['UserEmail']);
			$data = array('CheckUserEmail'=>1,'UserEmail'=>$user_email);
			if(isset($_POST['user_id'])){
				$data['ExcludeUserID'] = $_POST['user_id'];
			}
			$check_useremail_records = $libdhl->getUserRecords($data);
			
			$email_error_ary = array();
			if(!intranet_validateEmail($user_email)){
				$email_error_ary[] = $Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'];
			}
			if(in_array($user_email, (array)$system_reserved_account)){
				$email_error_ary[] = $Lang['AccountMgmt']['ErrorMsg']['EmailUsed'];
			}
			if(count($check_useremail_records)>0){
				$email_error_ary[] = $Lang['Gamma']['UserEmail'].$Lang['AccountMgmt']['UsedByOtherUser'];
			}
			if(count($email_error_ary)>0){
				$json_ary[] = array('UserEmail_Error', implode("<br />\n",$email_error_ary));
			}
		}
		
		header("Content-Type: application/json;charset=utf-8");
		echo $JSON->encode($json_ary);
	break;
	
	case 'GetCompanyList':
		
		$records = $libdhl->getCompanyRecords();
		$record_size = count($records);
		$x .= '<table class="common_table_list_v30">'."\n";
		$x .= '<tr class="tabletop"><th>'.$Lang['DHL']['Code'].'</th><th>'.$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['English'].')'.'</th><th>'.$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['Chinese'].')'.'</th></tr>'."\n";
		if($record_size == 0){
			$x .= '<tr><td colspan="3" align="center">'.$i_no_record_exists_msg.'</td></tr>'."\n";
		}
		for($i=0;$i<$record_size;$i++){
			$code = $records[$i]['CompanyCode'];
			$eng_name = $records[$i]['CompanyNameEng'];
			$chi_name = $records[$i]['CompanyNameChi'];
			$x .= '<tr><td>'.$code.'</td><td>'.Get_String_Display($eng_name).'</td><td>'.Get_String_Display($chi_name).'</td></tr>'."\n";
		}
		$x .= '</table>'."\n";
		echo $x;
		
	break;
	
	case 'GetDivisionList':
		
		$records = $libdhl->getDivisionRecords(array('OrderByCompanyFirst'=>1));
		$record_size = count($records);
		$x .= '<table class="common_table_list_v30">'."\n";
		$x .= '<tr class="tabletop">
				<th>'.$Lang['DHL']['Code'].'('.$Lang['DHL']['Company'].')'.'</th><th>'.$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['English'].')'.'</th><th>'.$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['Chinese'].')'.'</th>
				<th>'.$Lang['DHL']['Code'].'('.$Lang['DHL']['Division'].')'.'</th><th>'.$Lang['DHL']['DivisionName'].'('.$Lang['DHL']['English'].')'.'</th><th>'.$Lang['DHL']['DivisionName'].'('.$Lang['DHL']['Chinese'].')'.'</th>
			   </tr>'."\n";
		if($record_size == 0){
			$x .= '<tr><td colspan="6" align="center">'.$i_no_record_exists_msg.'</td></tr>'."\n";
		}
		for($i=0;$i<$record_size;$i++){
			$company_code = $records[$i]['CompanyCode'];
			$eng_name = $records[$i]['CompanyNameEng'];
			$chi_name = $records[$i]['CompanyNameChi'];
			$division_code = $records[$i]['DivisionCode'];
			$division_eng_name = $records[$i]['DivisionNameEng'];
			$division_chi_name = $records[$i]['DivisionNameChi'];
			$x .= '<tr><td>'.$company_code.'</td><td>'.Get_String_Display($eng_name).'</td><td>'.Get_String_Display($chi_name).'</td><td>'.$division_code.'</td><td>'.Get_String_Display($division_eng_name).'</td><td>'.Get_String_Display($division_chi_name).'</td></tr>'."\n";
		}
		$x .= '</table>'."\n";
		echo $x;
		
	break;
	
	case 'GetDepartmentList':
		
		$records = $libdhl->getDepartmentRecords(array('OrderByDivisionFirst'=>1,'DoNotGroup'=>1));
		$record_size = count($records);
		$x .= '<table class="common_table_list_v30">'."\n";
		$x .= '<tr class="tabletop">
				<th>'.$Lang['DHL']['Code'].'('.$Lang['DHL']['Company'].')'.'</th><th>'.$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['English'].')'.'</th><th>'.$Lang['DHL']['CompanyName'].'('.$Lang['DHL']['Chinese'].')'.'</th>
				<th>'.$Lang['DHL']['Code'].'('.$Lang['DHL']['Division'].')'.'</th><th>'.$Lang['DHL']['DivisionName'].'('.$Lang['DHL']['English'].')'.'</th><th>'.$Lang['DHL']['DivisionName'].'('.$Lang['DHL']['Chinese'].')'.'</th>
				<th>'.$Lang['DHL']['Code'].'('.$Lang['DHL']['Department'].')'.'</th><th>'.$Lang['DHL']['DepartmentName'].'('.$Lang['DHL']['English'].')'.'</th><th>'.$Lang['DHL']['DepartmentName'].'('.$Lang['DHL']['Chinese'].')'.'</th>
			   </tr>'."\n";
		if($record_size == 0){
			$x .= '<tr><td colspan="9" align="center">'.$i_no_record_exists_msg.'</td></tr>'."\n";
		}
		for($i=0;$i<$record_size;$i++){
			$company_code = $records[$i]['CompanyCode'];
			$eng_name = $records[$i]['CompanyNameEng'];
			$chi_name = $records[$i]['CompanyNameChi'];
			$division_code = $records[$i]['DivisionCode'];
			$division_eng_name = $records[$i]['DivisionNameEng'];
			$division_chi_name = $records[$i]['DivisionNameChi'];
			$department_code = $records[$i]['DepartmentCode'];
			$department_eng_name = $records[$i]['DepartmentNameEng'];
			$department_chi_name = $records[$i]['DepartmentNameChi'];
			$x .= '<tr>
					<td>'.$company_code.'</td><td>'.Get_String_Display($eng_name).'</td><td>'.Get_String_Display($chi_name).'</td>
					<td>'.$division_code.'</td><td>'.Get_String_Display($division_eng_name).'</td><td>'.Get_String_Display($division_chi_name).'</td>
					<td>'.$department_code.'</td><td>'.Get_String_Display($department_eng_name).'</td><td>'.Get_String_Display($department_chi_name).'</td>
				   </tr>'."\n";
		}
		$x .= '</table>'."\n";
		echo $x;
		
	break;
}

intranet_closedb();
?>