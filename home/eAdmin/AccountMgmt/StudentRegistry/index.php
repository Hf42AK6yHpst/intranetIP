<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && sizeof($SESSION_ACCESS_RIGHT['STUDENTREGISTRY'])==0)) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else {
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($SESSION_ACCESS_RIGHT['STUDENTREGISTRY']['MGMT']))
		header("Location: management/");
	else if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($SESSION_ACCESS_RIGHT['STUDENTREGISTRY']['SETTINGS']))
		header("Location: settings/access_right");
	else if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($SESSION_ACCESS_RIGHT['STUDENTREGISTRY']['STATS']['STUDENT']))
		header("Location: statistic/student");
	else if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($SESSION_ACCESS_RIGHT['STUDENTREGISTRY']['STATS']['TEACHER']))
		header("Location: statistic/teacher");
	else if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($SESSION_ACCESS_RIGHT['STUDENTREGISTRY']['REPORTS']['REGISTRYINFORMATION']))
		header("Location: report/information");	
	else if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($SESSION_ACCESS_RIGHT['STUDENTREGISTRY']['REPORTS']['REGISTRYSTATUS']))
		header("Location: report/status");	
}

?>