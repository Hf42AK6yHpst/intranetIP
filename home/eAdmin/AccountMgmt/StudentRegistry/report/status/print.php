<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryStatus-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lsr = new libstudentregistry();

$print = 1;
$dateAry = array("startDate"=>$startDate, "endDate"=>$endDate);
$tableContent = $lsr->getRegistryStatusReport($AcademicYearID, $Type, $list, $statistics, $print, $dateSelect, $dateAry);

?>

<table border="0" width="100%">
	<tr class='print_hide'>
		<td align="right">
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>	
	<tr>
		<td>
			<?=$tableContent?>
		</td>
	</tr>
</table>
<p></p>
<?

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>