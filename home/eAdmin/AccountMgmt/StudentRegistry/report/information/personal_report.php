<?php
#using:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$linterface = new interface_html();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryInformation-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$StudBasic 	= isset($StudBasic)? $StudBasic : 0;
$StudAdv 	= isset($StudAdv)? $StudAdv : 0;
$FatherInfo = isset($FatherInfo)? $FatherInfo : 0;
$MotherInfo = isset($MotherInfo)? $MotherInfo : 0;
$GuardInfo	= isset($GuardInfo)? $GuardInfo : 0;

$result = array();
$Father = array();
$Mother = array();
$Guard	= array();

if($report_type==2)
{	
	for($i=0;$i<sizeof($StudentList);$i++)
	{
		$result[] = $lsr -> RETRIEVE_STUDENT_INFO_ADV_MAL($StudentList[$i]);
		$Father[] = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentList[$i], 'F');
		$Mother[] = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentList[$i], 'M');
		$Guard[]  = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentList[$i], 'G');
	}
	$default_char = '--';
}
else
	$default_char = '&nbsp;';

//debug_pr($Guard);

$no_of_student = ($report_type == 2)? sizeof($StudentList) : 1;

for($i=0;$i<$no_of_student;$i++)
{
	$form_title = "<div id=\"container\"".($i!=$no_of_student-1 ? "style=\"page-break-after: always;" : "")."\">
						<div class=\"page_title_print\">
							".GET_SCHOOL_NAME()."<br>".$Lang['StudentRegistry']['StudentRegistryForm']."
						</div>";
	
	#decoding
	$gender_ary  	  = $lsr->GENDER_ARY();
	$stud_gender 	  = $result[$i][0]['Gender']? $lsr->RETRIEVE_DATA_ARY($gender_ary, $result[$i][0]['Gender']) : $default_char;
	$g_gender		  = $Guard[$i][0]['G_GENDER']? $lsr->RETRIEVE_DATA_ARY($gender_ary, $Guard[$i][0]['G_GENDER']) : $default_char;   
		
	$nation_ary	 	  = $lsr->MAL_NATION_DATA_ARY();
	$stud_nation 	  = $result[$i][0]['NATION']? $lsr->RETRIEVE_DATA_ARY($nation_ary, $result[$i][0]['NATION']) : $default_char;
	
	$b_place_ary 	  = $lsr->MAL_B_PLACE_DATA_ARY();
	$stud_b_place	  = $result[$i][0]['B_PLACE']? $lsr->RETRIEVE_DATA_ARY($b_place_ary, $result[$i][0]['B_PLACE']) : $default_char;
	
	$race_ary		  = $lsr->MAL_RACE_DATA_ARY();
	$stud_race		  = $result[$i][0]['RACE']? $lsr->RETRIEVE_DATA_ARY($race_ary, $result[$i][0]['RACE']) : $default_char;
	
	$religion_ary	  = $lsr->MAL_RELIGION_DATA_ARY();
	$stud_religion	  = $result[$i][0]['RELIGION']? $lsr->RETRIEVE_DATA_ARY($religion_ary, $result[$i][0]['RELIGION']) : $default_char; 

	$family_lang_ary  = $lsr->MAL_FAMILY_LANG_ARY();
	$stud_family_lang = $result[$i][0]['FAMILY_LANG']? $lsr->RETRIEVE_DATA_ARY($family_lang_ary, $result[$i][0]['FAMILY_LANG']) : $default_char; 

	$pay_type_ary	  = $lsr->MAL_PAYMENT_TYPE_ARY();
	$stud_pay_type	  = $result[$i][0]['PAYMENT_TYPE']? $lsr->RETRIEVE_DATA_ARY($pay_type_ary, $result[$i][0]['PAYMENT_TYPE']) : $default_char; 
	
	$lodging_ary	  = $lsr->MAL_LODGING_ARY();
	$stud_lodging	  = $result[$i][0]['LODGING']? $lsr->RETRIEVE_DATA_ARY($lodging_ary, $result[$i][0]['LODGING']) : $default_char;  
	
	$job_nature_ary	  = $lsr->MAL_JOB_NATURE_DATA_ARY();
	$f_job_nature	  = $Father[$i][0]['JOB_NATURE']? $lsr->RETRIEVE_DATA_ARY($job_nature_ary, $Father[$i][0]['JOB_NATURE']) : $default_char;   
	$m_job_nature	  = $Mother[$i][0]['JOB_NATURE']? $lsr->RETRIEVE_DATA_ARY($job_nature_ary, $Mother[$i][0]['JOB_NATURE']) : $default_char;   
	$g_job_nature	  = $Guard[$i][0]['JOB_NATURE']? $lsr->RETRIEVE_DATA_ARY($job_nature_ary, $Guard[$i][0]['JOB_NATURE']) : $default_char;   
	
	$job_title_ary	  = $lsr->MAL_JOB_TITLE_DATA_ARY();
	$f_job_title	  = $Father[$i][0]['JOB_TITLE']? $lsr->RETRIEVE_DATA_ARY($job_title_ary, $Father[$i][0]['JOB_TITLE']) : $default_char;   
	$m_job_title	  = $Mother[$i][0]['JOB_TITLE']? $lsr->RETRIEVE_DATA_ARY($job_title_ary, $Mother[$i][0]['JOB_TITLE']) : $default_char;   
	$g_job_title	  = $Guard[$i][0]['JOB_TITLE']? $lsr->RETRIEVE_DATA_ARY($job_title_ary, $Guard[$i][0]['JOB_TITLE']) : $default_char;   
	
	$marital_ary	  = $lsr->MAL_MARITAL_STATUS_DATA_ARY();
	$f_marital		  = $Father[$i][0]['MARITAL_STATUS']? $lsr->RETRIEVE_DATA_ARY($marital_ary, $Father[$i][0]['MARITAL_STATUS']) : $default_char;   
	$m_marital		  = $Mother[$i][0]['MARITAL_STATUS']? $lsr->RETRIEVE_DATA_ARY($marital_ary, $Mother[$i][0]['MARITAL_STATUS']) : $default_char;     
	
	$education_ary	  = $lsr->MAL_EDU_LEVEL_DATA_ARY();
	$f_education	  = $Father[$i][0]['EDU_LEVEL']? $lsr->RETRIEVE_DATA_ARY($education_ary, $Father[$i][0]['EDU_LEVEL']) : $default_char;   
	$m_education	  = $Mother[$i][0]['EDU_LEVEL']? $lsr->RETRIEVE_DATA_ARY($education_ary, $Mother[$i][0]['EDU_LEVEL']) : $default_char;     	
	$g_education	  = $Guard[$i][0]['EDU_LEVEL']? $lsr->RETRIEVE_DATA_ARY($education_ary, $Guard[$i][0]['EDU_LEVEL']) : $default_char;     	
	
	$relation_ary	  = $lsr->MAL_GUARD_DATA_ARY();
	$g_relation		  = $Guard[$i][0]['GUARD']? $lsr->RETRIEVE_DATA_ARY($relation_ary, $Guard[$i][0]['GUARD']) : $default_char;     	
	
	$live_same_ary	  = $lsr->MAL_LIVE_SAME_ARY();
	$g_live_same	  = $Guard[$i][0]['LIVE_SAME']? $lsr->RETRIEVE_DATA_ARY($live_same_ary, $Guard[$i][0]['LIVE_SAME']) : $default_char;     	
	
	#decoding - Elder Brothers / Sister in School
	if($result[$i][0]['BRO_SIS_IN_SCHOOL'])
	{
		$Siblings_UserID_ary = explode(",", $result[$i][0]['BRO_SIS_IN_SCHOOL']);
		$Siblings_html = "";
		$Siblings_option_html = "";
		for($j=0;$j<sizeof($Siblings_UserID_ary);$j++)
		{
			$Siblings_Info = $lsr->getStudentInfo_by_UserID($Siblings_UserID_ary[$j], $AcademicYearID);
			if(!$Siblings_Info)
				continue;
			else
				$Siblings_html .= "<tr><td><em>".($j+1).".</em>&nbsp;".$Siblings_Info['ClassName']."-".$Siblings_Info['ClassNumber']." ".$Siblings_Info['StudentName']."</td></tr>";
		}
		if($Siblings_html == "")
			$Siblings_html = "<tr><td>$default_char</td></tr>";
	}
	else
		$Siblings_html = "<tr><td>$default_char</td></tr>";
	
	$form_stud_basic = "<div class=\"form_sub_title_v30\">
							<em>- ".$Lang['StudentRegistry']['StudInfo']." -</em>
							<p class=\"spacer\"></p>
						</div>
						<table class=\"form_table_v30\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<tbody>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Mal_StudentNo']."</td>
									<td colspan=\"5\">".($result[$i][0]['STUD_ID']? $result[$i][0]['STUD_ID'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ChineseName']."</td>
									<td>".($result[$i][0]['ChineseName']? $result[$i][0]['ChineseName'] : '&nbsp;')."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EnglishName']."</td>
									<td colspan=\"3\">".($result[$i][0]['EnglishName']? $result[$i][0]['EnglishName'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Gender']."</td>
									<td>$stud_gender</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Nationality']."</td>
									<td colspan=\"3\">$stud_nation</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HomePhone']."</td>
									<td colspan=\"5\">".($result[$i][0]['HomeTelNo']? $result[$i][0]['HomeTelNo'] : $default_char)."</td>
								</tr>
							</tbody>
							<col class=\"field_title_short\">
							<col class=\"field_c\">
						</table>";
						
	$form_stud_adv 	 = "<div class=\"form_sub_title_v30\">
							<em>- ".$Lang['StudentRegistry']['StudInfo']." -</em>
							<p class=\"spacer\"></p>
						</div>
						<table class=\"form_table_v30\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<tbody>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Mal_StudentNo']."</td>
									<td colspan=\"5\">".($result[$i][0]['STUD_ID']? $result[$i][0]['STUD_ID'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ChineseName']."</td>
									<td>".($result[$i][0]['ChineseName']? $result[$i][0]['ChineseName'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EnglishName']."</td>
									<td colspan=\"3\">".($result[$i][0]['EnglishName']? $result[$i][0]['EnglishName'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Gender']."</td>
									<td>$stud_gender</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['BirthDate']."</td>
									<td>".($result[$i][0]['DateOfBirth']? date("Y-m-d", strtotime($result[$i][0]['DateOfBirth'])) : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['BirthPlace']."</td>
									<td>$stud_b_place</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Nationality']."</td>
									<td>$stud_nation</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['AncestalHome']."</td>
									<td>".($result[$i][0]['ANCESTRAL_HOME']? $result[$i][0]['ANCESTRAL_HOME'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Race']."</td>
									<td>$stud_race</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['IdCard'].$Lang['StudentRegistry']['CardNumber']."</td>
									<td>".($result[$i][0]['ID_NO']? $result[$i][0]['ID_NO'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['BirthCertNo']."</td>
									<td colspan=\"3\">".($result[$i][0]['BIRTH_CERT_NO']? $result[$i][0]['BIRTH_CERT_NO'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Religion']."</td>
									<td>$stud_religion</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['FamilyLang']."</td>
									<td>$stud_family_lang</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['PayType']."</td>
									<td>$stud_pay_type</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HomePhone']."</td>
									<td>".($result[$i][0]['HomeTelNo']? $result[$i][0]['HomeTelNo'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Email']."</td>
									<td colspan=\"3\">".($result[$i][0]['Email']? $result[$i][0]['Email'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HousingState']."</td>
									<td colspan=\"5\">$stud_lodging</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HomeAddress']."</td>
									<td colspan=\"5\">
										<div class=\"form_field_sub_content\">
											<table width=\"99%\">
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Door'].")</em>&nbsp;
													".($result[$i][0]['ADDRESS1']? $result[$i][0]['ADDRESS1'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Road'].")</em>&nbsp;
													".($result[$i][0]['ADDRESS2']? $result[$i][0]['ADDRESS2'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Garden'].")</em>&nbsp;
													".($result[$i][0]['ADDRESS3']? $result[$i][0]['ADDRESS3'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Town'].")</em>&nbsp;
													".($result[$i][0]['ADDRESS4']? $result[$i][0]['ADDRESS4'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['State'].")</em>&nbsp;
													".($result[$i][0]['ADDRESS5']? $result[$i][0]['ADDRESS5'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Postal'].")</em>&nbsp;
													".($result[$i][0]['ADDRESS6']? $result[$i][0]['ADDRESS6'] : $default_char)."</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EntryDate']."</td>
									<td>".($result[$i][0]['ADMISSION_DATE']? date("Y-m-d", strtotime($result[$i][0]['ADMISSION_DATE'])) : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Siblings']."</td>
									<td>
										<table class=\"form_field_sub_content\">
											$Siblings_html
										</table>
									</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['PastPrimarySch']."</td>
									<td>".($result[$i][0]['PRI_SCHOOL']? $result[$i][0]['PRI_SCHOOL'] : $default_char)."</td>
								</tr>
							</tbody>
							<col class=\"field_title_short\">
							<col class=\"field_c\">
						</table>";
	
	$form_father_info = "<div class=\"form_sub_title_v30\">
							<em>- ".$Lang['StudentRegistry']['FatherInfo']." -</em>
							<p class=\"spacer\"></p>
						</div>
						<table class=\"form_table_v30\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<tbody>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ChineseName']."</td>
									<td>".($Father[$i][0]['NAME_C']? $Father[$i][0]['NAME_C'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EnglishName']."</td>
									<td colspan=\"3\">".($Father[$i][0]['NAME_E']? $Father[$i][0]['NAME_E'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Job']."</td>
									<td>".($Father[$i][0]['PROF']? $Father[$i][0]['PROF'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['JobNature']."</td>
									<td>$f_job_nature</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['JobTitle']."</td>
									<td>$f_job_title</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['MaritalStatus']."</td>
									<td>$f_marital</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EducationLevel']."</td>
									<td colspan=\"3\">$f_education</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HomePhone']."</td>
									<td>".($Father[$i][0]['TEL']? $Father[$i][0]['TEL'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['CellPhone']."</td>
									<td>".($Father[$i][0]['MOBILE']? $Father[$i][0]['MOBILE'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['OfficeNumber']."</td>
									<td>".($Father[$i][0]['OFFICE_TEL']? $Father[$i][0]['OFFICE_TEL'] : $default_char)."</td>	
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ContactAddress']."</td>
									<td colspan=\"5\">
										<div class=\"form_field_sub_content\">
											<table width=\"99%\">
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Door'].")</em>&nbsp;
													".($Father[$i][0]['POSTAL_ADDR1']? $Father[$i][0]['POSTAL_ADDR1'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Road'].")</em>&nbsp;
													".($Father[$i][0]['POSTAL_ADDR2']? $Father[$i][0]['POSTAL_ADDR2'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Garden'].")</em>&nbsp;
													".($Father[$i][0]['POSTAL_ADDR3']? $Father[$i][0]['POSTAL_ADDR3'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Town'].")</em>&nbsp;
													".($Father[$i][0]['POSTAL_ADDR4']? $Father[$i][0]['POSTAL_ADDR4'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['State'].")</em>&nbsp;
													".($Father[$i][0]['POSTAL_ADDR5']? $Father[$i][0]['POSTAL_ADDR5'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Postal'].")</em>&nbsp;
													".($Father[$i][0]['POSTAL_ADDR6']? $Father[$i][0]['POSTAL_ADDR6'] : $default_char)."</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Postal']."</td>
									<td>".($Father[$i][0]['POST_CODE']? $Father[$i][0]['POST_CODE'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Email']."</td>
									<td colspan=\"3\">".($Father[$i][0]['EMAIL']? $Father[$i][0]['EMAIL'] : $default_char)."</td>
								</tr>
							</tbody>
							<col class=\"field_title_short\">
							<col class=\"field_c\">
						</table>";
	
	$form_mother_info = "<div class=\"form_sub_title_v30\">
							<em>- ".$Lang['StudentRegistry']['MotherInfo']." -</em>
							<p class=\"spacer\"></p>
						</div>
						<table class=\"form_table_v30\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<tbody>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ChineseName']."</td>
									<td>".($Mother[$i][0]['NAME_C']? $Mother[$i][0]['NAME_C'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EnglishName']."</td>
									<td colspan=\"3\">".($Mother[$i][0]['NAME_E']? $Mother[$i][0]['NAME_E'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Job']."</td>
									<td>".($Mother[$i][0]['PROF']? $Mother[$i][0]['PROF'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['JobNature']."</td>
									<td>$m_job_nature</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['JobTitle']."</td>
									<td>$m_job_title</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['MaritalStatus']."</td>
									<td>$m_marital</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EducationLevel']."</td>
									<td colspan=\"3\">$m_education</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HomePhone']."</td>
									<td>".($Mother[$i][0]['TEL']? $Mother[$i][0]['TEL'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['CellPhone']."</td>
									<td>".($Mother[$i][0]['MOBILE']? $Mother[$i][0]['MOBILE'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['OfficeNumber']."</td>
									<td>".($Mother[$i][0]['OFFICE_TEL']? $Mother[$i][0]['OFFICE_TEL'] : $default_char)."</td>	
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ContactAddress']."</td>
									<td colspan=\"5\">
										<div class=\"form_field_sub_content\">
											<table width=\"99%\">
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Door'].")</em>&nbsp;
													".($Mother[$i][0]['POSTAL_ADDR1']? $Mother[$i][0]['POSTAL_ADDR1'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Road'].")</em>&nbsp;
													".($Mother[$i][0]['POSTAL_ADDR2']? $Mother[$i][0]['POSTAL_ADDR2'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Garden'].")</em>&nbsp;
													".($Mother[$i][0]['POSTAL_ADDR3']? $Mother[$i][0]['POSTAL_ADDR3'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Town'].")</em>&nbsp;
													".($Mother[$i][0]['POSTAL_ADDR4']? $Mother[$i][0]['POSTAL_ADDR4'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['State'].")</em>&nbsp;
													".($Mother[$i][0]['POSTAL_ADDR5']? $Mother[$i][0]['POSTAL_ADDR5'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Postal'].")</em>&nbsp;
													".($Mother[$i][0]['POSTAL_ADDR6']? $Mother[$i][0]['POSTAL_ADDR6'] : $default_char)."</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Postal']."</td>
									<td>".($Mother[$i][0]['POST_CODE']? $Mother[$i][0]['POST_CODE'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Email']."</td>
									<td colspan=\"3\">".($Mother[$i][0]['EMAIL']? $Mother[$i][0]['EMAIL'] : $default_char)."</td>
								</tr>
							</tbody>
							<col class=\"field_title_short\">
							<col class=\"field_c\">
						</table>";
						
	$form_guard_info = "<div class=\"form_sub_title_v30\">
							<em>- ".$Lang['StudentRegistry']['GuardianInfo']." -</em>
							<p class=\"spacer\"></p>
						</div>
						<table class=\"form_table_v30\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<col class=\"field_title_short\">
							<col class=\"field_content_short\">
							<tbody>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Relationship']."</td>
									<td colspan=\"5\">$g_relation</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ChineseName']."</td>
									<td>".($Guard[$i][0]['NAME_C']? $Guard[$i][0]['NAME_C'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EnglishName']."</td>
									<td colspan=\"3\">".($Guard[$i][0]['NAME_E']? $Guard[$i][0]['NAME_E'] : $default_char)."</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Gender']."</td>
									<td colspan=\"5\">$g_gender</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['RelationOther']."</td>
									<td>".($Guard[$i][0]['G_RELATION']? $Guard[$i][0]['G_RELATION'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['StayTogether']."</td>
									<td colspan=\"3\">$g_live_same</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Job']."</td>
									<td>".($Guard[$i][0]['PROF']? $Guard[$i][0]['PROF'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['JobNature']."</td>
									<td>$g_job_nature</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['JobTitle']."</td>
									<td>$g_job_title</td>
								</tr>
								<tr>	
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['EducationLevel']."</td>
									<td colspan=\"5\">$g_education</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['HomePhone']."</td>
									<td>".($Guard[$i][0]['TEL']? $Guard[$i][0]['TEL'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['CellPhone']."</td>
									<td>".($Guard[$i][0]['MOBILE']? $Guard[$i][0]['MOBILE'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['OfficeNumber']."</td>
									<td>".($Guard[$i][0]['OFFICE_TEL']? $Guard[$i][0]['OFFICE_TEL'] : $default_char)."</td>	
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['ContactAddress']."</td>
									<td colspan=\"5\">
										<div class=\"form_field_sub_content\">
											<table width=\"99%\">
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Door'].")</em>&nbsp;
													".($Guard[$i][0]['POSTAL_ADDR1']? $Guard[$i][0]['POSTAL_ADDR1'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Road'].")</em>&nbsp;
													".($Guard[$i][0]['POSTAL_ADDR2']? $Guard[$i][0]['POSTAL_ADDR2'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Garden'].")</em>&nbsp;
													".($Guard[$i][0]['POSTAL_ADDR3']? $Guard[$i][0]['POSTAL_ADDR3'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Town'].")</em>&nbsp;
													".($Guard[$i][0]['POSTAL_ADDR4']? $Guard[$i][0]['POSTAL_ADDR4'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['State'].")</em>&nbsp;
													".($Guard[$i][0]['POSTAL_ADDR5']? $Guard[$i][0]['POSTAL_ADDR5'] : $default_char)."</td>
												</tr>
												<tr>
													<td><em>(".$Lang['StudentRegistry']['Postal'].")</em>&nbsp;
													".($Guard[$i][0]['POSTAL_ADDR6']? $Guard[$i][0]['POSTAL_ADDR6'] : $default_char)."</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Postal']."</td>
									<td>".($Guard[$i][0]['POST_CODE']? $Guard[$i][0]['POST_CODE'] : $default_char)."</td>
									<td class=\"field_title_short\">".$Lang['StudentRegistry']['Email']."</td>
									<td colspan=\"3\">".($Guard[$i][0]['EMAIL']? $Guard[$i][0]['EMAIL'] : $default_char)."</td>
								</tr>
							</tbody>
							<col class=\"field_title_short\">
							<col class=\"field_c\">
						</table>";

	$form_body  = "<div class=\"table_board\">
				  		<p class=\"spacer\"></p>";

	if($StudBasic)
		$form_body .= $form_stud_basic;
	if($StudAdv)
		$form_body .= $form_stud_adv;
	if($FatherInfo)
		$form_body .= $form_father_info;
	if($MotherInfo)
		$form_body .= $form_mother_info;
	if($GuardInfo)
		$form_body .= $form_guard_info;

	$form_body .= "			<br><br>
							<p class=\"spacer\"></p>
				   		</div>
				   		<br><br>
				   </div>";
				   
	$form .= $form_title.$form_body;
}
?>

<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">
	<tr>
		<td colspan="2" align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
</table>
<?=$form?>

<?php
intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>