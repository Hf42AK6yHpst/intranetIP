<?php
# using: Thomas
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryInformation-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();
$lexport = new libexporttext();

$filename = "student_registry_report.csv";
$ExportArr = array();

# Get Selected Student Info
$StudBasic = isset($StudBasic)? $StudBasic : 0;
$StudAdv = isset($StudAdv)? $StudAdv : 0;
$S_Info = (!$StudBasic && !$StudAdv)? 0 : (($StudBasic)? 1 : 2);

if($S_Info)
{
	if($S_Info==1)
	{
		for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][0]);$i++)
		{
			$title_ary[0][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][0][$i];
			$title_ary[1][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][1][$i];
		}
	}
	else
	{
		for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][0]);$i++)
		{
			$title_ary[0][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][0][$i];
			$title_ary[1][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][1][$i];
		}
	}
}
if($FatherInfo)
{
		for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][0]);$i++)
		{
			$title_ary[0][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][0][$i];
			$title_ary[1][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][1][$i];
		}
}
if($MotherInfo)
{
		for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][0]);$i++)
		{
			$title_ary[0][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][0][$i];
			$title_ary[1][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][1][$i];
		}
}
if($GuardInfo)
{
		for($i=0;$i<sizeof($Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][0]);$i++)
		{
			$title_ary[0][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][0][$i];
			$title_ary[1][] = $Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][1][$i];
		}
}
			
for($i=0;$i<sizeof($StudentList);$i++)
{
	$result[] = $lsr -> RETRIEVE_STUDENT_INFO_ADV_MAL($StudentList[$i]);
	$Father[] = $lsr  -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentList[$i], 'F');
	$Mother[] = $lsr  -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentList[$i], 'M');
	$Guard[]  = $lsr  -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($StudentList[$i], 'G');
}

for($i=0;$i<sizeof($StudentList);$i++)
{
	#decoding
	$gender_ary  	  = $lsr->GENDER_ARY();
	$stud_gender 	  = $result[$i][0]['Gender']? $lsr->RETRIEVE_DATA_ARY($gender_ary, $result[$i][0]['Gender']) : "--";
	$g_gender		  = $Guard[$i][0]['G_GENDER']? $lsr->RETRIEVE_DATA_ARY($gender_ary, $Guard[$i][0]['G_GENDER']) : "--";   
			
	$nation_ary	 	  = $lsr->MAL_NATION_DATA_ARY();
	$stud_nation 	  = $result[$i][0]['NATION']? $lsr->RETRIEVE_DATA_ARY($nation_ary, $result[$i][0]['NATION']) : "--";
		
	$b_place_ary 	  = $lsr->MAL_B_PLACE_DATA_ARY();
	$stud_b_place	  = $result[$i][0]['B_PLACE']? $lsr->RETRIEVE_DATA_ARY($b_place_ary, $result[$i][0]['B_PLACE']) : "--";
		
	$race_ary		  = $lsr->MAL_RACE_DATA_ARY();
	$stud_race		  = $result[$i][0]['RACE']? $lsr->RETRIEVE_DATA_ARY($race_ary, $result[$i][0]['RACE']) : "--";
		
	$religion_ary	  = $lsr->MAL_RELIGION_DATA_ARY();
	$stud_religion	  = $result[$i][0]['RELIGION']? $lsr->RETRIEVE_DATA_ARY($religion_ary, $result[$i][0]['RELIGION']) : "--"; 
	
	$family_lang_ary  = $lsr->MAL_FAMILY_LANG_ARY();
	$stud_family_lang = $result[$i][0]['FAMILY_LANG']? $lsr->RETRIEVE_DATA_ARY($family_lang_ary, $result[$i][0]['FAMILY_LANG']) : "--"; 

	$pay_type_ary	  = $lsr->MAL_PAYMENT_TYPE_ARY();
	$stud_pay_type	  = $result[$i][0]['PAYMENT_TYPE']? $lsr->RETRIEVE_DATA_ARY($pay_type_ary, $result[$i][0]['PAYMENT_TYPE']) : "--"; 
	
	$lodging_ary	  = $lsr->MAL_LODGING_ARY();
	$stud_lodging	  = $result[$i][0]['LODGING']? $lsr->RETRIEVE_DATA_ARY($lodging_ary, $result[$i][0]['LODGING']) : "--";  
	
	$job_nature_ary	  = $lsr->MAL_JOB_NATURE_DATA_ARY();
	$f_job_nature	  = $Father[$i][0]['JOB_NATURE']? $lsr->RETRIEVE_DATA_ARY($job_nature_ary, $Father[$i][0]['JOB_NATURE']) : "--";   
	$m_job_nature	  = $Mother[$i][0]['JOB_NATURE']? $lsr->RETRIEVE_DATA_ARY($job_nature_ary, $Mother[$i][0]['JOB_NATURE']) : "--";   
	$g_job_nature	  = $Guard[$i][0]['JOB_NATURE']? $lsr->RETRIEVE_DATA_ARY($job_nature_ary, $Guard[$i][0]['JOB_NATURE']) : "--";   
	
	$job_title_ary	  = $lsr->MAL_JOB_TITLE_DATA_ARY();
	$f_job_title	  = $Father[$i][0]['JOB_TITLE']? $lsr->RETRIEVE_DATA_ARY($job_title_ary, $Father[$i][0]['JOB_TITLE']) : "--";   
	$m_job_title	  = $Mother[$i][0]['JOB_TITLE']? $lsr->RETRIEVE_DATA_ARY($job_title_ary, $Mother[$i][0]['JOB_TITLE']) : "--";   
	$g_job_title	  = $Guard[$i][0]['JOB_TITLE']? $lsr->RETRIEVE_DATA_ARY($job_title_ary, $Guard[$i][0]['JOB_TITLE']) : "--";   
	
	$marital_ary	  = $lsr->MAL_MARITAL_STATUS_DATA_ARY();
	$f_marital		  = $Father[$i][0]['MARITAL_STATUS']? $lsr->RETRIEVE_DATA_ARY($marital_ary, $Father[$i][0]['MARITAL_STATUS']) : "--";   
	$m_marital		  = $Mother[$i][0]['MARITAL_STATUS']? $lsr->RETRIEVE_DATA_ARY($marital_ary, $Mother[$i][0]['MARITAL_STATUS']) : "--";     
	
	$education_ary	  = $lsr->MAL_EDU_LEVEL_DATA_ARY();
	$f_education	  = $Father[$i][0]['EDU_LEVEL']? $lsr->RETRIEVE_DATA_ARY($education_ary, $Father[$i][0]['EDU_LEVEL']) : "--";   
	$m_education	  = $Mother[$i][0]['EDU_LEVEL']? $lsr->RETRIEVE_DATA_ARY($education_ary, $Mother[$i][0]['EDU_LEVEL']) : "--";     	
	$g_education	  = $Guard[$i][0]['EDU_LEVEL']? $lsr->RETRIEVE_DATA_ARY($education_ary, $Guard[$i][0]['EDU_LEVEL']) : "--";     	
	
	$relation_ary	  = $lsr->MAL_GUARD_DATA_ARY();
	$g_relation		  = $Guard[$i][0]['GUARD']? $lsr->RETRIEVE_DATA_ARY($relation_ary, $Guard[$i][0]['GUARD']) : "--";     	
	
	$live_same_ary	  = $lsr->MAL_LIVE_SAME_ARY();
	$g_live_same	  = $Guard[$i][0]['LIVE_SAME']? $lsr->RETRIEVE_DATA_ARY($live_same_ary, $Guard[$i][0]['LIVE_SAME']) : "--";     	
	
	# Display Siblings Info
	if($result[$i][0]['BRO_SIS_IN_SCHOOL'])
	{
		$Siblings_UserID_ary = explode(",", $result[$i][0]['BRO_SIS_IN_SCHOOL']);
		$Siblings_val = "";
		for($j=0;$j<sizeof($Siblings_UserID_ary);$j++)
		{
			$Siblings_Info = $lsr->getStudentInfo_by_UserID($Siblings_UserID_ary[$j], $AcademicYearID);
			if(!$Siblings_Info)
				continue;
			else
			{
				if($Siblings_val!="")
					$Siblings_val .= ", ";
				$Siblings_val .= $Siblings_Info['ClassName']."-".$Siblings_Info['ClassNumber']." ".$Siblings_Info['StudentName'];
			}
		}
		if($Siblings_val == "")
			$Siblings_val = "--";
	}
	else
		$Siblings_val = "--";
				
	if($S_Info==1)
	{
		$ExportArr[$i][] = $result[$i][0]['STUD_ID']? $result[$i][0]['STUD_ID'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ChineseName']? $result[$i][0]['ChineseName'] : "--";
		$ExportArr[$i][] = $result[$i][0]['EnglishName']? $result[$i][0]['EnglishName'] : "--";
		$ExportArr[$i][] = $stud_gender;
		$ExportArr[$i][] = $stud_nation;
		$ExportArr[$i][] = $result[$i][0]['HomeTelNo']? $result[$i][0]['HomeTelNo'] : "--";
	}
	else if($S_Info==2)
	{
		$ExportArr[$i][] = $result[$i][0]['STUD_ID']? $result[$i][0]['STUD_ID'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ChineseName']? $result[$i][0]['ChineseName'] : "--";
		$ExportArr[$i][] = $result[$i][0]['EnglishName']? $result[$i][0]['EnglishName'] : "--";
		$ExportArr[$i][] = $stud_gender;
		$ExportArr[$i][] = $result[$i][0]['DateOfBirth']? date("Y-m-d", strtotime($result[$i][0]['DateOfBirth'])) : "--";
		$ExportArr[$i][] = $stud_b_place;
		$ExportArr[$i][] = $stud_nation;
		$ExportArr[$i][] = $result[$i][0]['ANCESTRAL_HOME']? $result[$i][0]['ANCESTRAL_HOME'] : "--";
		$ExportArr[$i][] = $stud_race;
		$ExportArr[$i][] = $result[$i][0]['ID_NO']? $result[$i][0]['ID_NO'] : "--";
		$ExportArr[$i][] = $result[$i][0]['BIRTH_CERT_NO']? $result[$i][0]['BIRTH_CERT_NO'] : "--";
		$ExportArr[$i][] = $stud_religion;
		$ExportArr[$i][] = $stud_family_lang;
		$ExportArr[$i][] = $stud_pay_type;
		$ExportArr[$i][] = $result[$i][0]['HomeTelNo']? $result[$i][0]['HomeTelNo'] : "--";
		$ExportArr[$i][] = $result[$i][0]['Email']? $result[$i][0]['Email'] : "--";
		$ExportArr[$i][] = $stud_lodging;
		$ExportArr[$i][] = $result[$i][0]['ADDRESS1']? $result[$i][0]['ADDRESS1'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ADDRESS2']? $result[$i][0]['ADDRESS2'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ADDRESS3']? $result[$i][0]['ADDRESS3'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ADDRESS4']? $result[$i][0]['ADDRESS4'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ADDRESS5']? $result[$i][0]['ADDRESS5'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ADDRESS6']? $result[$i][0]['ADDRESS6'] : "--";
		$ExportArr[$i][] = $result[$i][0]['ADMISSION_DATE']? date("Y-m-d", strtotime($result[$i][0]['ADMISSION_DATE'])) : "--";
		$ExportArr[$i][] = $Siblings_val;
		$ExportArr[$i][] = $result[$i][0]['PRI_SCHOOL']? $result[$i][0]['PRI_SCHOOL'] : "--";
	}
	if($FatherInfo)
	{
		$ExportArr[$i][] = $Father[$i][0]['NAME_C']? $Father[$i][0]['NAME_C'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['NAME_E']? $Father[$i][0]['NAME_E'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['PROF']? $Father[$i][0]['PROF'] : "--";
		$ExportArr[$i][] = $f_job_nature;
		$ExportArr[$i][] = $f_job_title;
		$ExportArr[$i][] = $f_marital;
		$ExportArr[$i][] = $f_education;
		$ExportArr[$i][] = $Father[$i][0]['TEL']? $Father[$i][0]['TEL'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['MOBILE']? $Father[$i][0]['MOBILE'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['OFFICE_TEL']? $Father[$i][0]['OFFICE_TEL'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POSTAL_ADDR1']? $Father[$i][0]['POSTAL_ADDR1'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POSTAL_ADDR2']? $Father[$i][0]['POSTAL_ADDR2'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POSTAL_ADDR3']? $Father[$i][0]['POSTAL_ADDR3'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POSTAL_ADDR4']? $Father[$i][0]['POSTAL_ADDR4'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POSTAL_ADDR5']? $Father[$i][0]['POSTAL_ADDR5'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POSTAL_ADDR6']? $Father[$i][0]['POSTAL_ADDR6'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['POST_CODE']? $Father[$i][0]['POST_CODE'] : "--";
		$ExportArr[$i][] = $Father[$i][0]['EMAIL']? $Father[$i][0]['EMAIL'] : "--";
	}
	if($MotherInfo)
	{
		$ExportArr[$i][] = $Mother[$i][0]['NAME_C']? $Mother[$i][0]['NAME_C'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['NAME_E']? $Mother[$i][0]['NAME_E'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['PROF']? $Mother[$i][0]['PROF'] : "--";
		$ExportArr[$i][] = $m_job_nature;
		$ExportArr[$i][] = $m_job_title;
		$ExportArr[$i][] = $m_marital;
		$ExportArr[$i][] = $m_education;
		$ExportArr[$i][] = $Mother[$i][0]['TEL']? $Mother[$i][0]['TEL'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['MOBILE']? $Mother[$i][0]['MOBILE'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['OFFICE_TEL']? $Mother[$i][0]['OFFICE_TEL'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POSTAL_ADDR1']? $Mother[$i][0]['POSTAL_ADDR1'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POSTAL_ADDR2']? $Mother[$i][0]['POSTAL_ADDR2'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POSTAL_ADDR3']? $Mother[$i][0]['POSTAL_ADDR3'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POSTAL_ADDR4']? $Mother[$i][0]['POSTAL_ADDR4'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POSTAL_ADDR5']? $Mother[$i][0]['POSTAL_ADDR5'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POSTAL_ADDR6']? $Mother[$i][0]['POSTAL_ADDR6'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['POST_CODE']? $Mother[$i][0]['POST_CODE'] : "--";
		$ExportArr[$i][] = $Mother[$i][0]['EMAIL']? $Mother[$i][0]['EMAIL'] : "--";
	}
	if($GuardInfo)
	{
		$ExportArr[$i][] = $g_relation;
		$ExportArr[$i][] = $Guard[$i][0]['NAME_C']? $Guard[$i][0]['NAME_C'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['NAME_C']? $Guard[$i][0]['NAME_C'] : "--";
		$ExportArr[$i][] = $g_gender;
		$ExportArr[$i][] = $Guard[$i][0]['G_RELATION']? $Guard[$i][0]['G_RELATION'] : "--";
		$ExportArr[$i][] = $g_live_same;
		$ExportArr[$i][] = $Guard[$i][0]['PROF']? $Guard[$i][0]['PROF'] : "--";
		$ExportArr[$i][] = $g_job_nature;
		$ExportArr[$i][] = $g_job_title;
		$ExportArr[$i][] = $g_education;
		$ExportArr[$i][] = $Guard[$i][0]['TEL']? $Guard[$i][0]['TEL'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['MOBILE']? $Guard[$i][0]['MOBILE'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['OFFICE_TEL']? $Guard[$i][0]['OFFICE_TEL'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POSTAL_ADDR1']? $Guard[$i][0]['POSTAL_ADDR1'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POSTAL_ADDR2']? $Guard[$i][0]['POSTAL_ADDR2'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POSTAL_ADDR3']? $Guard[$i][0]['POSTAL_ADDR3'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POSTAL_ADDR4']? $Guard[$i][0]['POSTAL_ADDR4'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POSTAL_ADDR5']? $Guard[$i][0]['POSTAL_ADDR5'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POSTAL_ADDR6']? $Guard[$i][0]['POSTAL_ADDR6'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['POST_CODE']? $Guard[$i][0]['POST_CODE'] : "--";
		$ExportArr[$i][] = $Guard[$i][0]['EMAIL']? $Guard[$i][0]['EMAIL'] : "--";
	}
}

# define column title
$exportColumn = array();
$exportColumn[0] = $title_ary[0];
$exportColumn[1] = $title_ary[1];

//debug_pr($ExportArr);

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>