<?php
#using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-STATS-Student-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();


# Get Submit_flag
$submit_flag = isset($submit_flag)? $submit_flag : 0;

# Get Academic Year Drop Down List
$AcademicYearID = isset($AcademicYearID)? $AcademicYearID : Get_Current_Academic_Year_ID();
$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onchange="javascript:ChangeReportOption(this.form, 3)"', $AcademicYearID, 0, 1);

$typeAry = array();
$i = 0;
foreach($Lang['StudentRegistry']['StudentStatistics_Type_Malaysia'] as $key=>$val) {
	$typeAry[$i][] = $key;
	$typeAry[$i][] = $val;
	$i++;
}
$typeSelection = getSelectByArray($typeAry, 'name="type" id="type"', $type, 0, 1, "");

#Class List Array to String
$ClassListStr = "";
for($i=0;$i<sizeof($ClassList);$i++)
{
	if($ClassListStr!="")
		$ClassListStr .= ",";
	$ClassListStr .= strval($ClassList[$i]);
}

#Student List Array to String
$StudentListStr = "";
for($i=0;$i<sizeof($StudentList);$i++)
{
	if($StudentListStr!="")
		$StudentListStr .= ",";
	$StudentListStr .= strval($StudentList[$i]);
}

# group 1 selection
$group1Ary[0][] = "NA";
$group1Ary[0][] = $Lang['StudentRegistry']['NA'];
$group1Ary[1][] = "Status";
$group1Ary[1][] = $Lang['StudentRegistry']['RegistryStatus'];
$group1Selection = getSelectByArray($group1Ary, 'name="group1" id="group1"', $group1, 0, 1, "");

# group 2 selection
$group2Ary[0][] = "NA";
$group2Ary[0][] = $Lang['StudentRegistry']['NA'];
$group2Ary[1][] = "Form";
$group2Ary[1][] = $Lang['AccountMgmt']['Form'];
$group2Ary[2][] = "Class";
$group2Ary[2][] = $Lang['AccountMgmt']['Class'];
$group2Ary[3][] = "Gender";
$group2Ary[3][] = $Lang['StudentRegistry']['Gender'];
$group2Selection = getSelectByArray($group2Ary, 'name="group2" id="group2"', $group2, 0, 1, "");

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Stat_Student";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['Student'], "index.php",1);
if($plugin['StudentRegistry_Malaysia']) {
	$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['Student']." (".$Lang['StudentRegistry']['WithTransferredStudents'].")", "index2.php",0);
}

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

if($submit_flag==1) 
	$tableContent = $lsr->getStudentRegistryStatistics($type, $AcademicYearID, $StudentList, $group1, $group2, $subtotal, $total);


?>

<script language="javascript">
function hideOption()
{
	$('#spanHideOption').attr('style', 'display:none');
	$('#spanShowOption').attr('style', '');
	$('#report_form').attr('style', 'display:none');
	$('#td_option').addClass('report_show_option');
}

function showOption()
{
	$('#spanHideOption').attr('style', '');
	$('#spanShowOption').attr('style', 'display:none');
	$('#report_form').attr('style', '');
	$('#td_option').removeClass();
}

function hide_year_class_stud()
{
	$('.blank_hide').attr('style', 'display:none');
}

function show_year_class_stud()
{
	$('.blank_hide').attr('style', '');	
}

function ChangeReportOption(obj, mode)
{
	if(mode == 4)
	{
		var ClassListLength = obj.elements['ClassList'].length;
		var ClassListStr = "";
		for(i=0;i<ClassListLength;i++)
		{
			if(obj.elements['ClassList'][i].selected)
			{
				if(ClassListStr!="")
					ClassListStr = ClassListStr + ",";
				ClassListStr = ClassListStr + obj.elements['ClassList'][i].value;
			}
		}
		
		var StudentListLength = obj.elements['StudentList'].length;
		var StudentListStr = "";
		for(i=0;i<StudentListLength;i++)
		{
			if(obj.elements['StudentList'][i].selected)
			{
				if(StudentListStr!="")
					StudentListStr = StudentListStr + ",";
				StudentListStr = StudentListStr + obj.elements['StudentList'][i].value;
			}
		}
	}
	else if(mode == 2)
	{
		ClassListStr = '<?=$ClassListStr?>';
		StudentListStr = '<?=$StudentListStr?>';
	}
		
	
	var AcademicYearID = $('#AcademicYearID').val();

	$.post(
			'ajax_change_report_options.php',
		   	{
				AcademicYearID 	: AcademicYearID,
				ClassListStr	: ClassListStr,
				StudentListStr	: StudentListStr,
				mode		   	: mode
		 	},
		 	function(data){
		 		var DataArr = data.split("|=|")
		 		if(mode!=4)
		 		{
		 			if(DataArr[0]=="")
			 		{
		 				$('#ClassList').html('<option value="">No Class</option>');
	 					$('#ClassList').attr('disabled', 'disabled');
	 					$('#ClassCtrlRemind').attr('style', 'display:none');
						$('#ClassSelectAll').attr('style', 'display:none');
			 		}
			 		else
			 		{
		 				$('#ClassList').html(DataArr[0]);
		 				$('#ClassList').attr('disabled', '');
		 				$('#ClassCtrlRemind').attr('style', '');
			 			$('#ClassSelectAll').attr('style', '');
			 		}
		 		}
		 		if(DataArr[1]=="")
		 		{
		 			$('#StudentList').html('<option value="">No Student</option>');
		 			$('#StudentList').attr('disabled', 'disabled');
		 			$('#StudentCtrlRemind').attr('style', 'display:none');
		 			$('#StudentSelectAll').attr('style', 'display:none');
		 		}
		 		else
		 		{
		 			$('#StudentList').html(DataArr[1]);
		 			$('#StudentList').attr('disabled', '');
		 			$('#StudentCtrlRemind').attr('style', '');
		 			$('#StudentSelectAll').attr('style', '');
		 		}
		 	});
}

function checksubmit(obj, mode)
{
	if(obj.type.value=="")
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['ReportType']?>');
		return false;
	}
	if(obj.AcademicYearID.value == '')
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['AcademicYear']?>');
		return false;
	}
	if(obj.ClassList.value == '')
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['Class']?>');
		return false;
	}
	if(obj.StudentList.value == '')
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['Student']?>');
		return false;
	}
	/*
	if(!obj.subtotal.checked && !obj.total.checked) {
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['SubTotal']." ".$i_general_or." ".$Lang['StudentRegistry']['Total']?>');
		return false;		
	} 
		*/
	obj.submit_flag.value = 1;
	
	if(mode==0) //Go to generate report
	{
		obj.target = "_self";
		obj.action = "index.php";
	}
	else if(mode==1) //Go to Print
	{
		obj.target = "_blank";
		obj.action = "index_print.php"
	}
	else if(mode==2) //Go to export csv
	{
		obj.target = "_blank"
		obj.action = "index_export.php";
	}
	
	//alert($('[name="form1"]').serialize());
	obj.submit();
}

$(document).ready(function(){
	if(<?=$submit_flag?>)
		ChangeReportOption(document.form1, 2);
	else
		ChangeReportOption(document.form1, 1);
});
</script>
<form name="form1" method="POST" action="">
	<table cellspacing="0" cellpadding="3" border="0" align="center" width="99%">
		<tr class="report_show_option"<?=$_POST["submit_flag"] == 1? "" : "style=\"display:none\""?>>
			<td id="td_option" class="report_show_option">
				<span id="spanHideOption" <?=$_POST["submit_flag"] == 1? "style=\"display:none\"" : ""?>>
					<a href="javascript:hideOption();" class="contenttool">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$Lang['StudentRegistry']['HideReportOption']?>
					</a>
				</span>
				<span id="spanShowOption">
					<a href="javascript:showOption();" class="contenttool">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$Lang['StudentRegistry']['ShowReportOption']?>
					</a>
				</span>
			</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" width="99%">
		<tr>
			<td class="main_content">
				<span id="report_form" <?=$_POST["submit_flag"] == 1? "style=\"display:none\"" : ""?>>
					<div class="report_option report_option_title" <?=$_POST["submit_flag"] == 1? "style=\"display:none\"" : ""?>>
						- <?= $Lang['StudentRegistry']['Options'] ?> -
					</div>
					<div class="table_board">
						<table class="form_table_v30">
							<tbody>
								<tr>
									<td class="field_title"><?= $Lang['StudentRegistry']['ReportType'] ?></td>
									<td>
										<?=$typeSelection?>
									</td>
								</tr>
							</tbody>
							<col class="field_title">
        					<col class="field_c">
							<tbody>
								<tr class="blank_hide" <?= $report_type == 3? "style=\"display:none\"" : ""?>>
									<td class="field_title"><?=$Lang['StudentRegistry']['AcademicYear']?></td>
									<td><?=$yearSelectionMenu?></td>
								</tr>
								<tr class="blank_hide" <?= $report_type == 3? "style=\"display:none\"" : ""?>>
									<td class="field_title"><?=$Lang['StudentRegistry']['Class']?></td>
									<td>
										<select size="5" id="ClassList" name="ClassList[]" onchange="javascript:ChangeReportOption(this.form, 4)" multiple>
											<?=$ClassOptionHTML?>
										</select>
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:checkOptionAll(this.form.ClassList); ChangeReportOption(this.form, 4);", "ClassSelectAll")?>
										<br>
										<span id="ClassCtrlRemind" class="tabletextremark">(<?=$Lang['StudentRegistry']['CtrlSelectAll']?>)</span>
									</td>
								</tr>
								<tr class="blank_hide" <?= $report_type == 3? "style=\"display:none\"" : ""?>>
									<td class="field_title"><?=$Lang['StudentRegistry']['Student']?></td>
									<td>
										<select size="5" id="StudentList" name="StudentList[]" multiple>
											<?=$StudentOptionHTML?>
										</select>
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:checkOptionAll(this.form.StudentList)", "StudentSelectAll")?>
										<br>
										<span id="StudentCtrlRemind" class="tabletextremark">(<?=$Lang['StudentRegistry']['CtrlSelectAll']?>)</span>
									</td>
								</tr>
								<? if($plugin['StudentRegistry_Malaysia']) {?>
								<tr>
                            <td class="field_title"><?=$Lang['StudentRegistry']['StudentGrouping1']?></td>
                            <td>                                                                                                                                         </select>
                            	<?=$group1Selection?>
                        	</td>
                          </tr>
                          <tr>
                            <td class="field_title"><?=$Lang['StudentRegistry']['StudentGrouping2']?></td>
                          
                            <td><?=$group2Selection?></td>
                          </tr>
                          <? } else if($plugin['StudentRegistry_Macau']) {?>
                          <tr>
                            <td class="field_title"><?=$Lang['StudentRegistry']['StudentGrouping']?></td>
                            <td><?=$group2Selection?><input type="hidden" name="group1" id="group1" value="NA"></td>
                          </tr>       
                          <? } ?>                   
                          <tr>
                            <td class="field_title"><?=$Lang['AccountMgmt']['display']?></td>
                            <td><input type="checkbox" name="subtotal" id="subtotal" value="1" <? if($subtotal) echo "checked";?>/><label for="subtotal"><?=$Lang['StudentRegistry']['SubTotal']?></label>
                                <input type="checkbox" name="total" id="total" value="1" <? if($total) echo "checked";?>/><label for="total"><?=$Lang['StudentRegistry']['Total']?></label>
                            </td>
                          </tr>
							</tbody>
						</table>
						<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
						<p class="spacer"></p>
						<?=$linterface->GET_ACTION_BTN($Lang['StudentRegistry']['GenerateReport'], "button", "checksubmit(this.form, 0)", "GenerateBtn")?>
						<p class="spacer"></p>
					</div>
				</span>
				<?=$tableContent?>	
			</td>
		</tr>
	</table>
	<input type="hidden" id="submit_flag" name="submit_flag" value="">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>