<?php
#using:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$linterface = new interface_html();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-STATS-Parent-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

//$AcademicYearName = $AcademicYearID == ""? $Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'] : $lsr->getAcademicYearNameByYearID($AcademicYearID);
$AcademicYearName = $lsr->getAcademicYearNameByYearID($AcademicYearID);
$profession = isset($profession)? $profession : 0;
$title = isset($title)? $title : 0;
$father = isset($father)? $father : 0;
$mother = isset($mother)? $mother : 0;
$guardian = isset($guardian)? $guardian : 0;
$year_cond  = $AcademicYearID == ""? "WHERE yc.AcademicYearID IS NOT NULL" : "WHERE yc.AcademicYearID = $AcademicYearID";
$parent_cond .= $father? ($parent_cond? " OR srpg.PG_TYPE = 'F'" : "srpg.PG_TYPE = 'F'") : "";
$parent_cond .= $mother? ($parent_cond? " OR srpg.PG_TYPE = 'M'" : "srpg.PG_TYPE = 'M'") : "";
$parent_cond .= $guardian? ($parent_cond? " OR (srpg.PG_TYPE = 'G' AND srpg.GUARD = 'O')" : "srpg.PG_TYPE = 'G'") : "";

$table_name  = "";
$table_name .= $profession? $Lang['StudentRegistry']['PrintReport']['ParentProfession'] : "";
$table_name .= $profession && $title? $Lang['StudentRegistry']['PrintReport']['And'] : "";
$table_name .= $title? $Lang['StudentRegistry']['PrintReport']['ParentJobTitle'] : "";
$table_name .= $Lang['StudentRegistry']['PrintReport']['Statistics'];

$ParentType  = "";
if($father)
{
	$ParentType .= $Lang['StudentRegistry']['Father'];
	if($mother)
	{
		if($guardian)
			$ParentType .= ", ".$Lang['StudentRegistry']['Mother'].$Lang['StudentRegistry']['PrintReport']['And'].$Lang['StudentRegistry']['Guardian'];
		else
			$ParentType .= $Lang['StudentRegistry']['PrintReport']['And'].$Lang['StudentRegistry']['Mother'];
	}
	else
	{
		if($guardian)
			$ParentType .= $Lang['StudentRegistry']['PrintReport']['And'].$Lang['StudentRegistry']['Guardian'];
	}
}
else
{
	if($mother)
	{
		$ParentType .= $Lang['StudentRegistry']['Mother'];
		if($guardian)
			$ParentType .= $Lang['StudentRegistry']['PrintReport']['And'].$Lang['StudentRegistry']['Guardian'];
	}
	else
		$ParentType .= $Lang['StudentRegistry']['Guardian'];
}

$generate_date = getdate();
$generate_year = $generate_date['year'];
$generate_month= $generate_date['mon'] < 10? "0".$generate_date['mon'] : $generate_date['mon'];
$generate_day  = $generate_date['mday'] < 10? "0".$generate_date['mday'] : $generate_date['mday'];

$table_head = "<tr style=\"font-size: 16px; font-weight: bold\">
					<td colspan=\"2\" style=\"text-align:center\">".
						GET_SCHOOL_NAME()."
					</td>
			   </tr>
			   <tr style=\"font-size: 16px; font-weight: bold\">
					<td colspan=\"2\" style=\"text-align:center\">
						$AcademicYearName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$table_name
					</td>
			   </tr>
			   <tr style=\"font-size: 16px; font-weight: bold\">
					<td colspan=\"2\" style=\"text-align:center\">
						($ParentType)
					</td>
			   </tr>
			   <tr style=\"font-size: 13px; font-weight: bold\">
					<td colspan=\"2\" class='tabletext'>".
						$eDiscipline['ReportGeneratedDate']." ".$generate_year."-".$generate_month."-".$generate_day."
					</td>
			   </tr>";

//$cond = $AcademicYearID == ""? "WHERE yc.AcademicYearID IS NOT NULL" : "WHERE yc.AcademicYearID = $AcademicYearID";
$cond = "WHERE yc.AcademicYearID = $AcademicYearID";

$table_body = "<tr valign=\"top\">";

if($profession)
{
	$sql = "SELECT
				IF(a.JOB_NATURE = '', '--', a.JOB_NATURE) AS profession,
				COUNT(*) AS total,
				ROUND(100*(COUNT(*)/b.total), 2) AS percent
			FROM
				(SELECT
					srpg.StudentID,
					IF(srpg.JOB_NATURE IS NULL, '', srpg.JOB_NATURE) AS JOB_NATURE
				FROM
					STUDENT_REGISTRY_PG AS srpg".
				($parent_cond? " WHERE $parent_cond" : "").") AS a LEFT JOIN
				YEAR_CLASS_USER AS ycu ON a.StudentID = ycu.UserID LEFT JOIN
				YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID, 
				(SELECT
					COUNT(*) AS total
				 FROM
					STUDENT_REGISTRY_PG AS srpg LEFT JOIN
					YEAR_CLASS_USER AS ycu ON srpg.StudentID = ycu.UserID LEFT JOIN
					YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
				 $year_cond".($parent_cond? " AND ($parent_cond)" : "").") AS b
			$year_cond
			GROUP BY
				a.JOB_NATURE
			";
	$result = $lsr->returnArray($sql);
	
	$table_body .= "<td ".($profession&&$title? "style=\"width: 50%\"" : "colspan=\"2\"").">
								<table cellspacing=\"0\" cellpadding=\"3\" border=\"0\" width=\"100%\" class=\"tableborder_print\">
									<tr class=\"tabletop_print tabletext\" style=\"text-align:center\">
										<td colspan=\"3\">".
											$Lang['StudentRegistry']['PrintReport']['ParentProfession'].$Lang['StudentRegistry']['PrintReport']['Statistics']."
										</td>
									</tr>
									<tr class=\"tabletop_print tabletext\">
										<td>".$Lang['StudentRegistry']['JobNatureGroup']."</td>
										<td>".$Lang['StudentRegistry']['NoOfParent']."</td>
										<td>%</td>									
									</tr>";
								
	$no_of_parent = 0;
	$JOB_NATURE_ary = $lsr -> MAL_JOB_NATURE_DATA_ARY();
	for($i=0;$i<sizeof($result);$i++)
	{
		$JOB_NATURE_val = $lsr -> RETRIEVE_DATA_ARY($JOB_NATURE_ary, $result[$i]['profession']);
		$JOB_NATURE_val	= $JOB_NATURE_val == ''? '--' : $JOB_NATURE_val;
		$table_body .= "<tr class=\"row_print\">
							<td class=\"table_text row_print\">".$JOB_NATURE_val."</td>
							<td class=\"table_text row_print\">".$result[$i]['total']."</td>
							<td class=\"table_text row_print\">".$result[$i]['percent']."</td>
						</tr>";
		$no_of_parent += $result[$i]['total'];
	}
								
	$table_body .= "<tr class=\"row_print\">
						<td class=\"table_text row_print\">".$Lang['StudentRegistry']['Total']."</td>
						<td class=\"table_text row_print\">$no_of_parent</td>
						<td class=\"table_text row_print\">&nbsp;</td>
				    </tr>
					</table>
					</td>";
}

if($title)
{
	$sql = "SELECT
				IF(a.JOB_TITLE = '', '--', a.JOB_TITLE) AS title,
				COUNT(*) AS total,
				ROUND(100*(COUNT(*)/b.total), 2) AS percent
			FROM
				(SELECT
					srpg.StudentID,
					IF(srpg.JOB_TITLE IS NULL, '', srpg.JOB_TITLE) AS JOB_TITLE
				FROM
					STUDENT_REGISTRY_PG AS srpg".
				($parent_cond? " WHERE $parent_cond" : "").") AS a LEFT JOIN
				YEAR_CLASS_USER AS ycu ON a.StudentID = ycu.UserID LEFT JOIN
				YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID, 
				(SELECT
					COUNT(*) AS total
				 FROM
					STUDENT_REGISTRY_PG AS srpg LEFT JOIN
					YEAR_CLASS_USER AS ycu ON srpg.StudentID = ycu.UserID LEFT JOIN
					YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
				 $year_cond".($parent_cond? " AND ($parent_cond)" : "").") AS b
			$year_cond
			GROUP BY
				a.JOB_TITLE
			";
	$result = $lsr->returnArray($sql);
	
	$table_body .= "<td ".($profession&&$title? "style=\"width: 50%\"" : "colspan=\"2\"").">
								<table cellspacing=\"0\" cellpadding=\"3\" border=\"0\" width=\"100%\" class=\"tableborder_print\">
									<tr class=\"tabletop_print tabletext\" style=\"text-align:center\">
										<td colspan=\"3\">".
											$Lang['StudentRegistry']['PrintReport']['ParentJobTitle'].$Lang['StudentRegistry']['PrintReport']['Statistics']."
										</td>
									</tr>
									<tr class=\"tabletop_print tabletext\">
										<td>".$Lang['StudentRegistry']['JobTitleGroup']."</td>
										<td>".$Lang['StudentRegistry']['NoOfParent']."</td>
										<td>%</td>									
									</tr>";
									
	$no_of_parent = 0;
	$JOB_TITLE_ary = $lsr -> MAL_JOB_TITLE_DATA_ARY();
	for($i=0;$i<sizeof($result);$i++)
	{
		$JOB_TITLE_val 	   = $lsr -> RETRIEVE_DATA_ARY($JOB_TITLE_ary, $result[$i]['title']);
		$JOB_TITLE_val	   = $JOB_TITLE_val == ''? '--' : $JOB_TITLE_val;
		$table_body .= "<tr class=\"row_print\">
							<td class=\"table_text row_print\">".$JOB_TITLE_val."</td>
							<td class=\"table_text row_print\">".$result[$i]['total']."</td>
							<td class=\"table_text row_print\">".$result[$i]['percent']."</td>
						</tr>";
		$no_of_parent += $result[$i]['total'];
	}
								
	$table_body .= "<tr class=\"row_print\">
						<td class=\"table_text row_print\">".$Lang['StudentRegistry']['Total']."</td>
						<td class=\"table_text row_print\">$no_of_parent</td>
						<td class=\"table_text row_print\">&nbsp;</td>
				    </tr>
					</table>
					</td>";
}

	$table_body .= "
						</td>
				   </tr>";
?>

<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">
	<tr>
		<td colspan="2" align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
	<?=$table_head?>
	<?=$table_body?>
	<tr>
		<td style="width:50%; visibility: hidden">&nbsp;</td>
		<td style="width:50%; visibility: hidden">&nbsp;</td>
	</tr>
</table>
	
<?
intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>