<?
// Modifying by: Thomas

############# Change Log [Start] ################
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_opendb();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$li = new libdb();

$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_macau.php?xmsg=import_failed&AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass);
	exit();
}
$file_format = $Lang['StudentRegistry']['Macau_CSV'][0];
// $file_format = array("Class Name", "Class Number", "Login Name", "Student ID", "DSEJ Code", "Chinese Name", "Foreign Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Origin", "School Code", "Date of Entry", "ID Card Type", "ID Card No.", "ID Issue Place", "ID Issue Date", "ID Valid Date", "Staying Permit Issue Date", "Staying Permit Valid Date", "Phone No. (Home)", "Email", "Address – (Street)", "Address – (Door, Bldg, Floor, Block)", "Father Chinese Name", "Father Foreign Name", "Father Job Occupation", "Father Marital Status", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Email", "Mother Chinese Name", "Mother Foreign Name", "Mother Job Occupation", "Mother Marital Status", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Email", "Guardian Chinese Name", "Guardian Foreign Name", "Guardian Gender", "Guardian Relationship with Student", "Guardian Relationship with Student (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Address – (Street)", "Guardian Address – (Door, Bldg, Floor, Block)", "Guardian Email", "Emergency Contact Person Chinese Name", "Emergency Contact Person Foreign Name", "Emergency Contact Person Relationship with Student", "Emergency Contact Person Phone No.(Home)", "Emergency Contact Person Phone No.(Mobile)", "Emergency Contact Person Phone No.(Office)", "Emergency Address (Street)", "Emergency Address (Door, Bldg, Floor, Block)");
// $flagAry = array(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
$flagAry = array();
foreach($file_format as $f) array_push($flagAry, 1);
### Custom Column
$custCols = $lsr->getCustomColumn();
foreach($custCols as $ccol){
    array_push($file_format, $ccol['Code']);
    array_push($flagAry, 1);
}

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);
$col_name = array_shift($data);

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass");

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		echo $col_name[$i].'/'.$file_format[$i];
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_macau.php?xmsg=".$returnMsg."&AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass);
	exit();
}

# Title / Menu
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_STUDENT_REGISTRY_MACAU_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		StudentID int(11),
		CLASS varchar(255),
		C_NO int(8),
		UserLogin varchar(20),
		STUD_ID varchar(20),
		CODE varchar(9),
		NAME_C varchar(100),
		NAME_E varchar(100),
		SEX char(2),
		B_DATE date,
		B_PLACE int(8),
		NATION varchar(2),
		ORIGIN varchar(20),
		RELIGION int(8),
		S_CODE varchar(3),
		ENTRY_DATE date,
		ID_TYPE varchar(5),
		ID_NO varchar(20),
		I_PLACE int(8),
		I_DATE date,
		V_DATE date,
		S6_TYPE tinyint(1),
		S6_IDATE date,
		S6_VDATE date,
		TEL varchar(20),
		EMAIL varchar(255),	
		R_AREA char(1),
		RA_DESC varchar(255),
		AREA char(1),
		ROAD varchar(255),
		ADDRESS varchar(255),
		FATHER_C varchar(255),
		FATHER_E varchar(255),
		F_PROF varchar(255),
		F_MARITAL int(8),
		F_TEL varchar(20),
		F_MOBILE varchar(20),
		F_OFFICE varchar(20),
		F_EMAIL varchar(255),
		MOTHER_C varchar(255),
		MOTHER_E varchar(255),
		M_PROF varchar(255),
		M_MARITAL int(8),
		M_TEL varchar(20),
		M_MOBILE varchar(20),
		M_OFFICE varchar(20),
		M_EMAIL varchar(255),
		GUARDIAN_C varchar(255),
		GUARDIAN_E varchar(255),
		G_SEX char(2),
		GUARD char(1),
		G_REL_OTHERS varchar(255),
		LIVE_SAME tinyint(1),
		G_PROF varchar(255),
		G_TEL varchar(40),
		G_MOBILE varchar(40),
		G_OFFICE varchar(40),
		G_AREA char(1),
		G_ROAD varchar(255),
		G_ADDRESS varchar(255),
		G_EMAIL varchar(255),
		EC_NAME_C varchar(255),
		EC_NAME_E varchar(255),
		EC_REL varchar(255),
		EC_TEL varchar(40),
		EC_MOBILE varchar(40),
		EC_OFFICE varchar(40),
		EC_AREA char(1),
		EC_ROAD varchar(255),
		EC_ADDRESS varchar(255),
		DateInput datetime,
		InputBy int(11),
		PRIMARY KEY (TempID),
		KEY InputBy (InputBy)

	) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$lsr->db_db_query($sql) or die(mysql_error());

if(sizeof($custCols) > 0){
    
    $sql = "CREATE TABLE IF NOT EXISTS TEMP_STUDENT_REGISTRY_MACAU_IMPORT_CUST
    (
     RecordID int(11) NOT NULL,
     ";
    foreach($custCols as $ccol)	$sql .= $ccol[Code] . " varchar(200) default NULL, ";
    $sql .= "
     InputBy int(8) default NULL,
     DateInput datetime default NULL,
     PRIMARY KEY (RecordID)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
    $lsr->db_db_query($sql) or die(mysql_error());
    
    $sql = "alter table TEMP_STUDENT_REGISTRY_MACAU_IMPORT_CUST ";
    foreach($custCols as $ccol){
        $exsql = $sql . "ADD COLUMN $ccol[Code] varchar(200) default NULL";
        $lsr->db_db_query($exsql);
    }
}

# delete the temp data in temp table 
$sql = "delete from TEMP_STUDENT_REGISTRY_MACAU_IMPORT where InputBy=$UserID";

$lsr->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=0;


foreach($data as $key => $data)
{
	$i++;
	$thisAry = array();
	$error_msg = array();
	$a = 0;
	$thisAry['CLASS'] 		= trim($data[$a]); $a++;
	$thisAry['C_NO'] 		= trim($data[$a]); $a++;
	$thisAry['UserLogin'] 	= trim($data[$a]); $a++;
	$thisAry['STUD_ID'] 	= trim($data[$a]); $a++;
	$thisAry['CODE'] 		= trim($data[$a]); $a++;
	$thisAry['NAME_C'] 		= trim($data[$a]); $a++;
	$thisAry['NAME_E'] 		= trim($data[$a]); $a++;
	$thisAry['SEX'] 		= trim($data[$a]); $a++;
	$thisAry['B_DATE'] 		= trim($data[$a]); $a++;
	$thisAry['B_PLACE'] 	= trim($data[$a]); $a++;
	$thisAry['NATION'] 		= trim($data[$a]); $a++;
	$thisAry['ORIGIN'] 		= trim($data[$a]); $a++;
	//$thisAry['RELIGION'] 	= trim($data[$a]); $a++;
	$thisAry['S_CODE'] 		= trim($data[$a]); $a++;
	$thisAry['ENTRY_DATE'] 	= trim($data[$a]); $a++;
	$thisAry['ID_TYPE'] 	= trim($data[$a]); $a++;
	$thisAry['ID_NO'] 		= trim($data[$a]); $a++;
	$thisAry['I_PLACE'] 	= trim($data[$a]); $a++;
	$thisAry['I_DATE'] 		= trim($data[$a]); $a++;
	$thisAry['V_DATE'] 		= trim($data[$a]); $a++;
	//$thisAry['S6_TYPE'] 	= trim($data[$a]); $a++;
	$thisAry['S6_IDATE'] 	= trim($data[$a]); $a++;
	$thisAry['S6_VDATE'] 	= trim($data[$a]); $a++;
	$thisAry['TEL'] 		= trim($data[$a]); $a++;
	$thisAry['EMAIL'] 		= trim($data[$a]); $a++;
	//$thisAry['R_AREA'] 		= trim($data[$a]); $a++;
	//$thisAry['RA_DESC'] 	= trim($data[$a]); $a++;
	//$thisAry['AREA'] 		= trim($data[$a]); $a++;
	$thisAry['ROAD'] 		= trim($data[$a]); $a++;
	$thisAry['ADDRESS'] 	= trim($data[$a]); $a++;
	$thisAry['FATHER_C'] 	= trim($data[$a]); $a++;
	$thisAry['FATHER_E'] 	= trim($data[$a]); $a++;
	$thisAry['F_PROF'] 		= trim($data[$a]); $a++;
	$thisAry['F_MARITAL'] 	= trim($data[$a]); $a++;
	$thisAry['F_TEL'] 		= trim($data[$a]); $a++;
	$thisAry['F_MOBILE'] 	= trim($data[$a]); $a++;
	$thisAry['F_OFFICE'] 	= trim($data[$a]); $a++;
	$thisAry['F_EMAIL'] 	= trim($data[$a]); $a++;
	$thisAry['MOTHER_C'] 	= trim($data[$a]); $a++;
	$thisAry['MOTHER_E'] 	= trim($data[$a]); $a++;
	$thisAry['M_PROF'] 		= trim($data[$a]); $a++;
	$thisAry['M_MARITAL'] 	= trim($data[$a]); $a++;
	$thisAry['M_TEL'] 		= trim($data[$a]); $a++;
	$thisAry['M_MOBILE'] 	= trim($data[$a]); $a++;
	$thisAry['M_OFFICE'] 	= trim($data[$a]); $a++;
	$thisAry['M_EMAIL'] 	= trim($data[$a]); $a++;
	$thisAry['GUARDIAN_C'] 	= trim($data[$a]); $a++;
	$thisAry['GUARDIAN_E'] 	= trim($data[$a]); $a++;
	$thisAry['G_SEX'] 		= trim($data[$a]); $a++;
	$thisAry['GUARD'] 		= trim($data[$a]); $a++;
	$thisAry['G_REL_OTHERS']= trim($data[$a]); $a++;
	$thisAry['LIVE_SAME'] 	= trim($data[$a]); $a++;
	$thisAry['G_PROF'] 		= trim($data[$a]); $a++;
	$thisAry['G_TEL'] 		= trim($data[$a]); $a++;
	$thisAry['G_MOBILE']	= trim($data[$a]); $a++;
	$thisAry['G_OFFICE'] 	= trim($data[$a]); $a++;
	//$thisAry['G_AREA'] 		= trim($data[$a]); $a++;
	$thisAry['G_ROAD'] 		= trim($data[$a]); $a++;
	$thisAry['G_ADDRESS'] 	= trim($data[$a]); $a++;
	$thisAry['G_EMAIL'] 	= trim($data[$a]); $a++;
	$thisAry['EC_NAME_C'] 	= trim($data[$a]); $a++;
	$thisAry['EC_NAME_E'] 	= trim($data[$a]); $a++;
	$thisAry['EC_REL'] 		= trim($data[$a]); $a++;
	$thisAry['EC_TEL'] 		= trim($data[$a]); $a++;
	$thisAry['EC_MOBILE'] 	= trim($data[$a]); $a++;
	$thisAry['EC_OFFICE'] 	= trim($data[$a]); $a++;
	//$thisAry['EC_AREA'] 	= trim($data[$a]); $a++;
	$thisAry['EC_ROAD'] 	= trim($data[$a]); $a++;
	$thisAry['EC_ADDRESS'] 	= trim($data[$a]); $a++;
	
	### store csv data to temp data
	$error_msg = $lsr->checkData_Macau('import', $thisAry);
		
	$s_id = $lsr->retrieveStudentIDByClassOrLogin($thisAry['CLASS'], $thisAry['C_NO'], $thisAry['UserLogin']);
	
	if(empty($error_msg))
	{
		$successCount++;
		
		$sql = "INSERT INTO TEMP_STUDENT_REGISTRY_MACAU_IMPORT 
			   (
				StudentID,
				CLASS,
				C_NO,
				UserLogin,
				STUD_ID,
				CODE,
				NAME_C,
				NAME_E,
				SEX,
				B_DATE,
				B_PLACE,
				NATION,
				ORIGIN,
				RELIGION,
				S_CODE,
				ENTRY_DATE,
				ID_TYPE,
				ID_NO,
				I_PLACE,
				I_DATE,
				V_DATE,
				S6_TYPE,
				S6_IDATE,
				S6_VDATE,
				TEL,
				EMAIL,	
				R_AREA,
				RA_DESC,
				AREA,
				ROAD,
				ADDRESS,
				FATHER_C,
				FATHER_E,
				F_PROF,
				F_MARITAL,
				F_TEL,
				F_MOBILE,
				F_OFFICE,
				F_EMAIL,
				MOTHER_C,
				MOTHER_E,
				M_PROF,
				M_MARITAL,
				M_TEL,
				M_MOBILE,
				M_OFFICE,
				M_EMAIL,
				GUARDIAN_C,
				GUARDIAN_E,
				G_SEX,
				GUARD,
				G_REL_OTHERS,
				LIVE_SAME,
				G_PROF,
				G_TEL,
				G_MOBILE,
				G_OFFICE,
				G_AREA,
				G_ROAD,
				G_ADDRESS,
				G_EMAIL,
				EC_NAME_C,
				EC_NAME_E,
				EC_REL,
				EC_TEL,
				EC_MOBILE,
				EC_OFFICE,
				EC_AREA,
				EC_ROAD,
				EC_ADDRESS,
				DateInput,
				InputBy
 			   ) 
			   VALUES
			   ($s_id,
				'".intranet_htmlspecialchars($thisAry['CLASS'])."',
				'".intranet_htmlspecialchars($thisAry['C_NO'])."',
				'".$thisAry['UserLogin']."',
				'".$thisAry['STUD_ID']."',
				'".$thisAry['CODE']."',
				'".intranet_htmlspecialchars($thisAry['NAME_C'])."',
				'".intranet_htmlspecialchars($thisAry['NAME_E'])."',
				'".$thisAry['SEX']."',
				'".$thisAry['B_DATE']."',
				'".intranet_htmlspecialchars($thisAry['B_PLACE'])."',
				'".intranet_htmlspecialchars($thisAry['NATION'])."',
				'".intranet_htmlspecialchars($thisAry['ORIGIN'])."',
				'".intranet_htmlspecialchars($thisAry['RELIGION'])."',
				'".$thisAry['S_CODE']."',
				'".$thisAry['ENTRY_DATE']."',
				'".$thisAry['ID_TYPE']."',
				'".$thisAry['ID_NO']."',
				'".intranet_htmlspecialchars($thisAry['I_PLACE'])."',
				'".$thisAry['I_DATE']."',
				'".$thisAry['V_DATE']."',
				'".$thisAry['S6_TYPE']."',
				'".$thisAry['S6_IDATE']."',
				'".$thisAry['S6_VDATE']."',
				'".$thisAry['TEL']."',
				'".$thisAry['EMAIL']."',
				'".intranet_htmlspecialchars($thisAry['R_AREA'])."',
				'".intranet_htmlspecialchars($thisAry['RA_DESC'])."',
				'".intranet_htmlspecialchars($thisAry['AREA'])."',
				'".intranet_htmlspecialchars($thisAry['ROAD'])."',
				'".intranet_htmlspecialchars($thisAry['ADDRESS'])."',
				'".intranet_htmlspecialchars($thisAry['FATHER_C'])."',
				'".intranet_htmlspecialchars($thisAry['FATHER_E'])."',
				'".intranet_htmlspecialchars($thisAry['F_PROF'])."',
				'".$thisAry['F_MARITAL']."',
				'".$thisAry['F_TEL']."',
				'".$thisAry['F_MOBILE']."',
				'".$thisAry['F_OFFICE']."',
				'".$thisAry['F_EMAIL']."',
				'".intranet_htmlspecialchars($thisAry['MOTHER_C'])."',
				'".intranet_htmlspecialchars($thisAry['MOTHER_E'])."',
				'".intranet_htmlspecialchars($thisAry['M_PROF'])."',
				'".$thisAry['M_MARITAL']."',
				'".$thisAry['M_TEL']."',
				'".$thisAry['M_MOBILE']."',
				'".$thisAry['M_OFFICE']."',
				'".$thisAry['M_EMAIL']."',
				'".intranet_htmlspecialchars($thisAry['GUARDIAN_C'])."',
				'".intranet_htmlspecialchars($thisAry['GUARDIAN_E'])."',
				'".$thisAry['G_SEX']."',
				'".$thisAry['GUARD']."',
				'".intranet_htmlspecialchars($thisAry['G_REL_OTHERS'])."',
				'".$thisAry['LIVE_SAME']."',
				'".intranet_htmlspecialchars($thisAry['G_PROF'])."',
				'".$thisAry['G_TEL']."',
				'".$thisAry['G_MOBILE']."',
				'".$thisAry['G_OFFICE']."',
				'".intranet_htmlspecialchars($thisAry['G_AREA'])."',
				'".intranet_htmlspecialchars($thisAry['G_ROAD'])."',
				'".intranet_htmlspecialchars($thisAry['G_ADDRESS'])."',
				'".$thisAry['G_EMAIL']."',
				'".intranet_htmlspecialchars($thisAry['EC_NAME_C'])."',
				'".intranet_htmlspecialchars($thisAry['EC_NAME_E'])."',
				'".intranet_htmlspecialchars($thisAry['EC_REL'])."',
				'".$thisAry['EC_TEL']."',
				'".$thisAry['EC_MOBILE']."',
				'".$thisAry['EC_OFFICE']."',
				'".intranet_htmlspecialchars($thisAry['EC_AREA'])."',
				'".intranet_htmlspecialchars($thisAry['EC_ROAD'])."',
				'".intranet_htmlspecialchars($thisAry['EC_ADDRESS'])."',
				NOW(),
				$UserID
			   )";
		$lsr->db_db_query($sql) or die(mysql_error());
		
		$rID = $lsr->db_insert_id();
		
		# save to temp table
		$sql = "insert into TEMP_STUDENT_REGISTRY_MACAU_IMPORT_CUST (RecordID,";
		foreach($custCols as $ccol)	$sql .= $ccol[Code] . ",";
		$sql .= "InputBy, DateInput) values ('$rID',";
		//for($i=0;$i<sizeof($d);$i++)		$sql .= "'". addslashes($d[$i]) . "',";
		for($i=0;$i<sizeof($custCols);$i++)		$sql .= "'". addslashes(intranet_htmlspecialchars(trim($data[$a + $i]))) . "',";
		$sql .= $UserID . ", now())";
		$lsr->db_db_query($sql) or die(mysql_error());
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
		
		$displayContent = "<ul>";
		foreach($error_msg as $err) {
			$displayContent .= "<li>".$err."</li>";
		}
		$displayContent .= "</ul>";
		
		$stdInfo = $lsr->getUserInfoByID($s_id);
		$name = ((!isset($error_msg['UserInfo'])) ? Get_Lang_Selection($stdInfo['ChineseName'], $stdInfo['EnglishName']) : ("<font color='red'>[".(($thisAry['CLASS']!="" && $thisAry['C_NO']!="")? ($thisAry['CLASS']."-".$thisAry['C_NO']) : $thisAry['UserLogin'])."]</font>"));
																					
		$msg .= "<tr style='vertical-align:top'>";
		$msg .= "<td class=\"tablebluerow$i\" width=\"10\">". $i ."</td>";
		$msg .= "<td class=\"tablebluerow$i\">". $name ."</td>";
		$msg .= "<td class=\"tablebluerow$i\"><font color='red'>". $displayContent ."</font></td>";
		$msg .= "</tr>";
		
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
/*
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". GetCurrentAcademicYear() ."</td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if($error_result)
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $i_UserStudentName ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	$x .= $msg;
	$x .= "</table>";
}


if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_macau.php?AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass."'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit");
	$import_button .= " &nbsp;";
	$import_button .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_macau.php?AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass."'");
}
?>

<br />
<form name="form1" method="post" action="import_macau_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
