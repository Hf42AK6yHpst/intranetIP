<?
// Modifying by:

############# Change Log [Start] ################
#
# Date: 2020-03-16 (Philips) [2019-1115-1452-49066]
#		- add sync guardian info to student account
#
# Date:	2010-07-07 (Henry Chow)
#		- add GET_IMPORT_TXT_WITH_REFERENCE(), Apply new standard of import
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$li = new libdb();
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";


$fail = 0;
$success = 0;

# delete the temp data in temp table 
$sql = "SELECT * FROM TEMP_STUDENT_REGISTRY_MACAU_IMPORT WHERE InputBy=$UserID";
$data = $lsr->returnArray($sql);

if(sizeof($data)==0) {
	intranet_closedb();
	header("Location: import_macau.php?xmsg=import_no_record&AcademicYearID=$AcademicYearID&targetClass=$targetClass");	
	exit();
}

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass");

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

//debug_pr($data);
$linterface->LAYOUT_START(); 

for($i=0; $i<sizeof($data); $i++)
{
	$result = array();
	
	list($rec_id, $StudentID, $CLASS, $C_NO, $UserLogin, $STUD_ID, $CODE, $NAME_C, $NAME_E, $SEX, $B_DATE, $B_PLACE, $NATION, $ORIGIN, $RELIGION, $S_CODE, $ENTRY_DATE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $TEL, $EMAIL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $FATHER_C, $FATHER_E, $F_PROF, $F_MARITAL, $F_TEL, $F_MOBILE, $F_OFFICE, $F_EMAIL, $MOTHER_C, $MOTHER_E, $M_PROF, $M_MARITAL, $M_TEL, $M_MOBILE, $M_OFFICE, $M_EMAIL, $GUARDIAN_C, $GUARDIAN_E, $G_SEX, $GUARD, $G_REL_OTHERS, $LIVE_SAME, $G_PROF, $G_TEL, $G_MOBILE, $G_OFFICE, $G_AREA, $G_ROAD, $G_ADDRESS, $G_EMAIL, $EC_NAME_C, $EC_NAME_E, $EC_REL, $EC_TEL, $EC_MOBILE, $EC_OFFICE, $EC_AREA, $EC_ROAD, $EC_ADDRESS) = $data[$i];
	//list($rec_id, $StudentID, $CLASS, $C_NO, $UserLogin, $STUD_ID, $CODE, $NAME_C, $NAME_E, $SEX, $B_DATE, $B_PLACE, $NATION, $ORIGIN, $S_CODE, $ENTRY_DATE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_IDATE, $S6_VDATE, $TEL, $EMAIL, $ROAD, $ADDRESS, $FATHER_C, $FATHER_E, $F_PROF, $F_MARITAL, $F_TEL, $F_MOBILE, $F_OFFICE, $F_EMAIL, $MOTHER_C, $MOTHER_E, $M_PROF, $M_MARITAL, $M_TEL, $M_MOBILE, $M_OFFICE, $M_EMAIL, $GUARDIAN_C, $GUARDIAN_E, $G_SEX, $GUARD, $G_REL_OTHERS, $LIVE_SAME, $G_PROF, $G_TEL, $G_MOBILE, $G_OFFICE, $G_ROAD, $G_ADDRESS, $G_EMAIL, $EC_NAME_C, $EC_NAME_E, $EC_REL, $EC_TEL, $EC_MOBILE, $EC_OFFICE, $EC_ROAD, $EC_ADDRESS) = $data[$i];
	
	# update INTRANET_USER
	$studentAry = array(
						"ChineseName"	=>$NAME_C,
						"EnglishName"	=>$NAME_E,
						"Gender"		=>$SEX,
						"DateOfBirth"	=>$B_DATE,
						"HomeTelNo"		=>$TEL
				);
	$result[] = $lsr->updateStudentInfo($studentAry, $StudentID);

	# insert into STUDENT_REGISTRY_STUDENT
	$dataAry = array(
					"STUD_ID"		=>$STUD_ID,
					"CODE"			=>$CODE,
					"ADMISSION_DATE"=>$ENTRY_DATE,
					"S_CODE"		=>$S_CODE,
					"B_PLACE"		=>$B_PLACE,
					"ID_TYPE"		=>$ID_TYPE,
					"ID_NO"			=>$ID_NO,
					"I_PLACE"		=>$I_PLACE,
					"I_DATE"		=>$I_DATE,
					"V_DATE"		=>$V_DATE,
					"S6_TYPE"		=>$S6_TYPE,
					"S6_IDATE"		=>$S6_IDATE,
					"S6_VDATE"		=>$S6_VDATE,
					"NATION"		=>$NATION,
					"ORIGIN"		=>$ORIGIN,
					"RELIGION"		=>$RELIGION,
					"R_AREA"		=>$R_AREA,
					"RA_DESC"		=>$RA_DESC,
					"AREA"			=>$AREA,
					"ROAD"			=>$ROAD,
					"ADDRESS"		=>$ADDRESS,
					"EMAIL"			=>$EMAIL
				);
				
	$result[] = $lsr->updateStudentRegistry_Simple_Macau($dataAry, $StudentID);
	
	# insert into STUDENT_REGISTRY_PG - FATHER
	$fatherAry = array(
					"NAME_C"		=>$FATHER_C,
					"NAME_E"		=>$FATHER_E,
					"PROF"			=>$F_PROF,
					"MARITAL_STATUS"=>$F_MARITAL,
					"TEL"			=>$F_TEL,
					"MOBILE"		=>$F_MOBILE,
					"OFFICE_TEL"	=>$F_OFFICE,
					"EMAIL"			=>$F_EMAIL					
				);
	//$lsr->removeStudentRegistry_PG($StudentID, "F");			
	$result[] = $lsr->updateStudentRegistry_PG($fatherAry, "F", $StudentID);
	
	# insert into STUDENT_REGISTRY_PG - MOTHER
	$motherAry = array(
					"NAME_C"		=>$MOTHER_C,
					"NAME_E"		=>$MOTHER_E,
					"PROF"			=>$M_PROF,
					"MARITAL_STATUS"=>$M_MARITAL,
					"TEL"			=>$M_TEL,
					"MOBILE"		=>$M_MOBILE,
					"OFFICE_TEL"	=>$M_OFFICE,
					"EMAIL"			=>$M_EMAIL					
				);
	//$lsr->removeStudentRegistry_PG($StudentID, "M");			
	$result[] = $lsr->updateStudentRegistry_PG($motherAry, "M", $StudentID);
	
	# insert into STUDENT_REGISTRY_PG - GUARDIAN
	$guardianAry = array(
						"NAME_C"	=>$GUARDIAN_C,
						"NAME_E"	=>$GUARDIAN_E,
						"G_GENDER"	=>$G_SEX,
						"GUARD"		=>$GUARD,
						"G_RELATION"=>$G_REL_OTHERS,
						"LIVE_SAME" =>$LIVE_SAME,
						"PROF"		=>$G_PROF,
						"TEL"		=>$G_TEL,
						"MOBILE"	=>$G_MOBILE,
						"OFFICE_TEL"=>$G_OFFICE,
						"G_AREA"	=>$G_AREA,
						"G_ROAD"	=>$G_ROAD,
						"G_ADDRESS"	=>$G_ADDRESS,
						"EMAIL"		=>$M_EMAIL					
					);
	//$lsr->removeStudentRegistry_PG($StudentID, "G");
	$result[] = $lsr->updateStudentRegistry_PG($guardianAry, "G", $StudentID);
	# 2020-03-16 (Philips) Sync Guardian to Student Account
	if($sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry']){
		$lsr->syncToAccountMainGuardian($StudentID, $guardianAry);
	}
	
	# insert into STUDENT_REGISTRY_PG - EMERGENCY CONTACT
	$ecAry = array(
						"NAME_C"=>$EC_NAME_C,
						"NAME_E"=>$EC_NAME_E,
						"GUARD"=>$EC_REL,
						"TEL"=>$EC_TEL,
						"MOBILE"=>$EC_MOBILE,
						"OFFICE_TEL"=>$EC_OFFICE,
						"G_AREA"=>$EC_AREA,
						"G_ROAD"=>$EC_ROAD,
						"G_ADDRESS"=>$EC_ADDRESS
					);
	//$lsr->removeStudentRegistry_PG($userID, "E");
	$result[] = $lsr->updateStudentRegistry_PG($ecAry, "E", $StudentID);
	
	$custCols = $lsr->getCustomColumn();
	$sql = "SELECT * FROM TEMP_STUDENT_REGISTRY_MACAU_IMPORT_CUST WHERE RecordID = '$rec_id'";
	$custRecord = $lsr->returnArray($sql);
	if(sizeof($custRecord)>0){
	    $custRecord = $custRecord[0];
	    $custValues = array();
	    foreach($custCols as $col){
	        $custValues[] = array(
	            "ColumnID" => $col['ColumnID'],
	            "value" => $custRecord[$col['Code']]
	        );
	    }
	    //     	debug_pr($custValues);die();
	    $result[] = $lsr->updateCustomValue($StudentID, $custValues);
	}
	
	if(in_array(false, $result)) {
		$fail++;	
	} else {
		$sql = "DELETE FROM TEMP_STUDENT_REGISTRY_MACAU_IMPORT WHERE StudentID=$StudentID AND InputBy=$UserID";
		$sql = "DELETE FROM TEMP_STUDENT_REGISTRY_MACAU_IMPORT_CUST WHERE RecordID='$rec_id' AND InputBy=$UserID";
		$lsr->db_db_query($sql);
		$success++;	
	}
	//debug_pr($result);
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
/*
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". GetCurrentAcademicYear() ."</td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $success ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($fail ? "<font color='red'>":"") . $fail . ($fail ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";


$import_button = $linterface->GET_ACTION_BTN($button_finish, "button", "window.location='class.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_macau.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass'");

?>

<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
