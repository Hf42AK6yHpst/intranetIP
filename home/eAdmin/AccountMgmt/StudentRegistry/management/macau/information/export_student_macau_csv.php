<?php
# using: Thomas
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();
$lexport = new libexporttext();

$ExportArr = array();
$custCols = $lsr->getCustomColumn();
//$AcademicYearID = Get_Current_Academic_Year_ID();

# condition
$conds = "";

if($searchText != "")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

if($targetClass!="") {
	$studentAry = $lsr->storeStudent('0', $targetClass);
	$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus=$registryStatus";
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";


$filename = "student_registry_class.csv";	


# SQL Statement
$sql = "SELECT
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as CLASS,
			ycu.ClassNumber,
			USR.UserLogin,
			srs.STUD_ID,
			srs.CODE,
			USR.ChineseName,
			USR.EnglishName,
			USR.Gender,
			IF(USR.DateOfBirth='0000-00-00 00:00:00','',LEFT(USR.DateOfBirth,10)),
			srs.B_PLACE,
			srs.NATION,
			srs.ORIGIN,
			srs.RELIGION,
			srs.S_CODE,
			IF(srs.ADMISSION_DATE='0000-00-00 00:00:00','',LEFT(srs.ADMISSION_DATE,10)),
			srs.ID_TYPE,
			srs.ID_NO,
			srs.I_PLACE,
			IF(srs.I_DATE='0000-00-00 00:00:00','',LEFT(srs.I_DATE,10)),
			IF(srs.V_DATE='0000-00-00 00:00:00','',LEFT(srs.V_DATE,10)),
			srs.S6_TYPE,
			IF(srs.S6_IDATE='0000-00-00 00:00:00','',LEFT(srs.S6_IDATE,10)),
			IF(srs.S6_VDATE='0000-00-00 00:00:00','',LEFT(srs.S6_VDATE,10)),
			USR.HomeTelNo,
			srs.EMAIL,
			srs.R_AREA,
			srs.RA_DESC,
			srs.AREA,
			srs.ROAD,
			srs.ADDRESS,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			yc.Sequence, ycu.ClassNumber
		";	
		
$row = $lsr->returnArray($sql,28);



# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	//list($STUD_ID, $CODE, $NAME_C, $NAME_P, $SEX, $B_DATE, $B_PLACE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $NATION, $ORIGIN, $TEL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $s_CODE, $GRADE, $CLASS, $C_NO, $userid) = $row[$i];
	list($CLASS, $C_NO, $UserLogin, $STUD_ID, $CODE, $NAME_C, $NAME_E, $SEX, $B_DATE, $B_PLACE, $NATION, $ORIGIN, $RELIGION, $S_CODE, $ENTRY_DATE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $TEL, $EMAIL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $userid) = $row[$i];
	
	$a = 0;
	$ExportArr[$i][$a]  = $CLASS; $a++;
	$ExportArr[$i][$a]  = $C_NO; $a++;
	$ExportArr[$i][$a]  = $UserLogin; $a++;
	$ExportArr[$i][$a]  = $STUD_ID; $a++;
	$ExportArr[$i][$a]  = $CODE; $a++;
	$ExportArr[$i][$a]  = $NAME_C; $a++;
	$ExportArr[$i][$a]  = $NAME_E; $a++;
	$ExportArr[$i][$a]  = $SEX; $a++;
	$ExportArr[$i][$a]  = $B_DATE; $a++;
	$ExportArr[$i][$a]  = $B_PLACE; $a++;
	$ExportArr[$i][$a] = $NATION; $a++;
	$ExportArr[$i][$a] = $ORIGIN; $a++;
	//$ExportArr[$i][$a] = $RELIGION; $a++;
	$ExportArr[$i][$a] = $S_CODE; $a++;
	$ExportArr[$i][$a] = $ENTRY_DATE; $a++;
	$ExportArr[$i][$a] = $ID_TYPE; $a++;
	$ExportArr[$i][$a] = $ID_NO; $a++;
	$ExportArr[$i][$a] = $I_PLACE; $a++;
	$ExportArr[$i][$a] = $I_DATE; $a++;
	$ExportArr[$i][$a] = $V_DATE; $a++;
	//$ExportArr[$i][$a] = $S6_TYPE; $a++;
	$ExportArr[$i][$a] = $S6_IDATE; $a++;
	$ExportArr[$i][$a] = $S6_VDATE; $a++;
	$ExportArr[$i][$a] = $TEL; $a++;
	$ExportArr[$i][$a] = $EMAIL; $a++;
	//$ExportArr[$i][$a] = $R_AREA; $a++;
	//$ExportArr[$i][$a] = $RA_DESC; $a++;
	//$ExportArr[$i][$a] = $AREA; $a++;
	$ExportArr[$i][$a] = $ROAD; $a++;
	$ExportArr[$i][$a] = $ADDRESS; $a++;
	
	$father = $lsr->getParentGuardianInfo($userid, 'F');
	
	$ExportArr[$i][$a] = $father['NAME_C']; $a++;
	$ExportArr[$i][$a] = $father['NAME_E']; $a++;
	$ExportArr[$i][$a] = $father['PROF']; $a++;
	$ExportArr[$i][$a] = $father['MARITAL_STATUS']; $a++;
	$ExportArr[$i][$a] = $father['TEL']; $a++;
	$ExportArr[$i][$a] = $father['MOBILE']; $a++;
	$ExportArr[$i][$a] = $father['OFFICE_TEL']; $a++;
	$ExportArr[$i][$a] = $father['EMAIL']; $a++;
	
	$mother = $lsr->getParentGuardianInfo($userid, 'M');
	
	$ExportArr[$i][$a] = $mother['NAME_C']; $a++;
	$ExportArr[$i][$a] = $mother['NAME_E']; $a++;
	$ExportArr[$i][$a] = $mother['PROF']; $a++;
	$ExportArr[$i][$a] = $mother['MARITAL_STATUS']; $a++;
	$ExportArr[$i][$a] = $mother['TEL']; $a++;
	$ExportArr[$i][$a] = $mother['MOBILE']; $a++;
	$ExportArr[$i][$a] = $mother['OFFICE_TEL']; $a++;
	$ExportArr[$i][$a] = $mother['EMAIL']; $a++;
	
	$guardian = $lsr->getParentGuardianInfo($userid, 'G');
	
	$ExportArr[$i][$a] = $guardian['NAME_C']; $a++;
	$ExportArr[$i][$a] = $guardian['NAME_E']; $a++;
	$ExportArr[$i][$a] = $guardian['G_GENDER']; $a++;
	$ExportArr[$i][$a] = $guardian['GUARD']; $a++;
	$ExportArr[$i][$a] = $guardian['G_RELATION']; $a++;
	$ExportArr[$i][$a] = $guardian['LIVE_SAME']; $a++;
	$ExportArr[$i][$a] = $guardian['PROF']; $a++;
	$ExportArr[$i][$a] = $guardian['TEL']; $a++;
	$ExportArr[$i][$a] = $guardian['MOBILE']; $a++;
	$ExportArr[$i][$a] = $guardian['OFFICE_TEL']; $a++;
	//$ExportArr[$i][$a] = $guardian['AREA']; $a++;
	$ExportArr[$i][$a] = $guardian['ROAD']; $a++;
	$ExportArr[$i][$a] = $guardian['ADDRESS']; $a++;
	$ExportArr[$i][$a] = $guardian['EMAIL']; $a++;
	
	$emergency = $lsr->getParentGuardianInfo($userid, 'E');
	
	$ExportArr[$i][$a] = $emergency['NAME_C']; $a++;
	$ExportArr[$i][$a] = $emergency['NAME_E']; $a++;
	$ExportArr[$i][$a] = $emergency['GUARD']; $a++;
	$ExportArr[$i][$a] = $emergency['TEL']; $a++;
	$ExportArr[$i][$a] = $emergency['MOBILE']; $a++;
	$ExportArr[$i][$a] = $emergency['OFFICE_TEL']; $a++;
	//$ExportArr[$i][$a] = $emergency['AREA']; $a++;
	$ExportArr[$i][$a] = $emergency['ROAD']; $a++;
	$ExportArr[$i][$a] = $emergency['ADDRESS']; $a++;
	
	// Custom Column
	
	$custValues = $lsr->getCustomColumnValueByUserID($userid);
	$tempValues = array();
	foreach($custCols as $ccol){
	    $tempValues[$ccol['Code']] = '';
	}
	foreach($custValues as $cval){
	    $tempValues[$cval['Code']] = $cval['Value'];
	}
	foreach($tempValues as $tval){
	    array_push($ExportArr[$i], $tval);
	}
}

# define column title
$exportColumn = array();
$exportColumn[0] = $Lang['StudentRegistry']['Macau_CSV'][0];
$exportColumn[1] = $Lang['StudentRegistry']['Macau_CSV'][1];
foreach($custCols as $ccol){
    array_push($exportColumn[0], $ccol['Code']);
    array_push($exportColumn[1], $ccol['DisplayText_ch']);
}
	
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
