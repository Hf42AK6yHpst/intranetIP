<?php
# using: Thomas
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();

$ExportArr = array();

//$AcademicYearID = Get_Current_Academic_Year_ID();

# condition
$conds = "";

if($searchText != "")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

if($targetClass!="") {
	$studentAry = $lsr->storeStudent('0', $targetClass);
	$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus=$registryStatus";
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";


$filename = "student_registry_class.xml";	


# SQL Statement
$sql = "SELECT
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as CLASS,
			ycu.ClassNumber,
			USR.UserLogin,
			srs.STUD_ID,
			srs.CODE,
			USR.ChineseName,
			USR.EnglishName,
			USR.Gender,
			IF(USR.DateOfBirth='0000-00-00 00:00:00','',LEFT(USR.DateOfBirth,10)),
			srs.B_PLACE,
			srs.NATION,
			srs.ORIGIN,
			srs.RELIGION,
			srs.S_CODE,
			IF(srs.ADMISSION_DATE='0000-00-00 00:00:00','',LEFT(srs.ADMISSION_DATE,10)),
			srs.ID_TYPE,
			srs.ID_NO,
			srs.I_PLACE,
			IF(srs.I_DATE='0000-00-00 00:00:00','',LEFT(srs.I_DATE,10)),
			IF(srs.V_DATE='0000-00-00 00:00:00','',LEFT(srs.V_DATE,10)),
			srs.S6_TYPE,
			IF(srs.S6_IDATE='0000-00-00 00:00:00','',LEFT(srs.S6_IDATE,10)),
			IF(srs.S6_VDATE='0000-00-00 00:00:00','',LEFT(srs.S6_VDATE,10)),
			USR.HomeTelNo,
			srs.EMAIL,
			srs.R_AREA,
			srs.RA_DESC,
			srs.AREA,
			srs.ROAD,
			srs.ADDRESS,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			yc.Sequence, ycu.ClassNumber
		";	
		
$row = $lsr->returnArray($sql,28);

$data = "<?xml version=\"1.0\" encoding=\"BIG5\"?>\r\n";
$data .= "<!DOCTYPE SRA SYSTEM \"http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd\">\r\n";
$data .= "\r\n<SRA>\r\n";

# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	list($CLASS, $C_NO, $UserLogin, $STUD_ID, $CODE, $NAME_C, $NAME_E, $SEX, $B_DATE, $B_PLACE, $NATION, $ORIGIN, $RELIGION, $S_CODE, $ENTRY_DATE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $TEL, $EMAIL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $userid) = $row[$i];
	
	$father 	= $lsr->getParentGuardianInfo($userid, 'F');
	$mother 	= $lsr->getParentGuardianInfo($userid, 'M');
	$guardian 	= $lsr->getParentGuardianInfo($userid, 'G');
	$emergency 	= $lsr->getParentGuardianInfo($userid, 'E');
			
	$data .= "<STUDENT>\r\n";
	
	$data .= "<CLASS>".iconv("UTF-8","big5",$CLASS)."</CLASS>\r\n";
	$data .= "<C_NO>".iconv("UTF-8","big5",$C_NO)."</C_NO>\r\n";
	$data .= "<UserLogin>".iconv("UTF-8","big5",$UserLogin)."</UserLogin>\r\n";	
	$data .= "<STUD_ID>".iconv("UTF-8","big5",$STUD_ID)."</STUD_ID>\r\n";		//student id
	$data .= "<CODE>".iconv("UTF-8","big5",$CODE)."</CODE>\r\n";			//code
	$data .= "<NAME_C>".iconv("UTF-8","big5",$NAME_C)."</NAME_C>\r\n";  		//name in chinese
	$data .= "<NAME_E>".iconv("UTF-8","big5",$NAME_E)."</NAME_E>\r\n";  		//name in p
	$data .= "<SEX>".iconv("UTF-8","big5",$SEX)."</SEX>\r\n";  			//sex
	$data .= "<B_DATE>".iconv("UTF-8","big5",$B_DATE)."</B_DATE>\r\n";  		//date of birth
	$data .= "<B_PLACE>".iconv("UTF-8","big5",$B_PLACE)."</B_PLACE>\r\n";  		//place of birth
	$data .= "<NATION>".iconv("UTF-8","big5",$NATION)."</NATION>\r\n";  		//nation
	$data .= "<ORIGIN>".iconv("UTF-8","big5",$ORIGIN)."</ORIGIN>\r\n";  		//origin
	$data .= "<RELIGION>".iconv("UTF-8","big5",$RELIGION)."</RELIGION>\r\n";
	$data .= "<S_CODE>".iconv("UTF-8","big5",$S_CODE)."</S_CODE>\r\n";
	$data .= "<ENTRY_DATE>".iconv("UTF-8","big5",$ENTRY_DATE)."</ENTRY_DATE>\r\n";
	$data .= "<ID_TYPE>".iconv("UTF-8","big5",$ID_TYPE)."</ID_TYPE>\r\n";  		//id type
	$data .= "<ID_NO>".iconv("UTF-8","big5",$ID_NO)."</ID_NO>\r\n";  		//id number
	$data .= "<I_PLACE>".iconv("UTF-8","big5",$I_PLACE)."</I_PLACE>\r\n";  		//issue place
	$data .= "<I_DATE>".iconv("UTF-8","big5",$I_DATE)."</I_DATE>\r\n";  		//issue date
	$data .= "<V_DATE>".iconv("UTF-8","big5",$V_DATE)."</V_DATE>\r\n";  		//valid date
	$data .= "<S6_TYPE>".iconv("UTF-8","big5",$S6_TYPE)."</S6_TYPE>\r\n";  		
	$data .= "<S6_IDATE>".iconv("UTF-8","big5",$S6_IDATE)."</S6_IDATE>\r\n";  		
	$data .= "<S6_VDATE>".iconv("UTF-8","big5",$S6_VDATE)."</S6_VDATE>\r\n";  		
	$data .= "<TEL>".iconv("UTF-8","big5",$TEL)."</TEL>\r\n";  		//telephone
	$data .= "<EMAIL>".iconv("UTF-8","big5",$EMAIL)."</EMAIL>\r\n"; 
	$data .= "<R_AREA>".iconv("UTF-8","big5",$R_AREA)."</R_AREA>\r\n";  		
	$data .= "<RA_DESC>".iconv("UTF-8","big5",$RA_DESC)."</RA_DESC>\r\n";  		
	$data .= "<AREA>".iconv("UTF-8","big5",$AREA)."</AREA>\r\n";  		//area
	$data .= "<ROAD>".iconv("UTF-8","big5",$ROAD)."</ROAD>\r\n";  		//road
	$data .= "<ADDRESS>".iconv("UTF-8","big5",$ADDRESS)."</ADDRESS>\r\n";  	//address
	
	$date .= "<FATHER_C>".iconv("UTF-8","big5",$father['NAME_C'])."</FATHER_C>\r\n";
	$date .= "<FATHER_E>".iconv("UTF-8","big5",$father['NAME_E'])."</FATHER_E>\r\n";	
	$data .= "<F_PROF>".iconv("UTF-8","big5",$father['PROF'])."</F_PROF>\r\n";																// father professional
	$data .= "<F_MARITAL>".iconv("UTF-8","big5",$father['MARITAL_STATUS'])."</F_MARITAL>\r\n";
	$data .= "<F_TEL>".iconv("UTF-8","big5",$father['TEL'])."</F_TEL>\r\n";
	$data .= "<F_MOBILE>".iconv("UTF-8","big5",$father['MOBILE'])."</F_MOBILE>\r\n";
	$data .= "<F_OFFICE>".iconv("UTF-8","big5",$father['OFFICE_TEL'])."</F_OFFICE>\r\n";
	$data .= "<F_EMAIL>".iconv("UTF-8","big5",$father['EMAIL'])."</F_EMAIL>\r\n";

	$date .= "<MOTHER_C>".iconv("UTF-8","big5",$mother['NAME_C'])."</MOTHER_C>\r\n";
	$date .= "<MOTHER_E>".iconv("UTF-8","big5",$mother['NAME_E'])."</MOTHER_E>\r\n";	
	$data .= "<M_PROF>".iconv("UTF-8","big5",$mother['PROF'])."</M_PROF>\r\n";																// mother professional
	$data .= "<M_MARITAL>".iconv("UTF-8","big5",$mother['MARITAL_STATUS'])."</M_MARITAL>\r\n";
	$data .= "<M_TEL>".iconv("UTF-8","big5",$mother['TEL'])."</M_TEL>\r\n";
	$data .= "<M_MOBILE>".iconv("UTF-8","big5",$mother['MOBILE'])."</M_MOBILE>\r\n";
	$data .= "<M_OFFICE>".iconv("UTF-8","big5",$mother['OFFICE_TEL'])."</M_OFFICE>\r\n";
	$data .= "<M_EMAIL>".iconv("UTF-8","big5",$mother['EMAIL'])."</M_EMAIL>\r\n";

	$date .= "<GUARDIAN_C>".iconv("UTF-8","big5",$guardian['NAME_C'])."</GUARDIAN_C>\r\n";
	$date .= "<GUARDIAN_E>".iconv("UTF-8","big5",$guardian['NAME_E'])."</GUARDIAN_E>\r\n";
	$date .= "<G_SEX>".iconv("UTF-8","big5",$guardian['G_GENDER'])."</G_SEX>\r\n";
	$date .= "<GUARD>".iconv("UTF-8","big5",$guardian['GUARD'])."</GUARD>\r\n";
	$date .= "<G_REL_OTHERS>".iconv("UTF-8","big5",$guardian['G_RELATION'])."</G_REL_OTHERS>\r\n";	
	$date .= "<LIVE_SAME>".iconv("UTF-8","big5",$guardian['LIVE_SAME'])."</LIVE_SAME>\r\n";
	$date .= "<G_PROF>".iconv("UTF-8","big5",$guardian['PROF'])."</G_PROF>\r\n";
	$date .= "<G_TEL>".iconv("UTF-8","big5",$guardian['TEL'])."</G_TEL>\r\n";
	$date .= "<G_MOBILE>".iconv("UTF-8","big5",$guardian['MOBILE'])."</G_MOBILE>\r\n";
	$date .= "<G_OFFICE>".iconv("UTF-8","big5",$guardian['OFFICE_TEL'])."</G_OFFICE>\r\n";
	$date .= "<G_AREA>".iconv("UTF-8","big5",$guardian['AREA'])."</G_AREA>\r\n";
	$date .= "<G_ROAD>".iconv("UTF-8","big5",$guardian['ROAD'])."</G_ROAD>\r\n";
	$date .= "<G_ADDRESS>".iconv("UTF-8","big5",$guardian['ADDRESS'])."</G_ADDRESS>\r\n";
	$date .= "<G_EMAIL>".iconv("UTF-8","big5",$guardian['EMAIL'])."</G_EMAIL>\r\n";

	$date .= "<EC_NAME_C>".iconv("UTF-8","big5",$guardian['NAME_C'])."</EC_NAME_C>\r\n";
	$date .= "<EC_NAME_E>".iconv("UTF-8","big5",$guardian['NAME_E'])."</EC_NAME_E>\r\n";
	$date .= "<EC_REL".iconv("UTF-8","big5",$guardian['GUARD'])."</EC_REL>\r\n";
	$date .= "<EC_TEL>".iconv("UTF-8","big5",$guardian['TEL'])."</EC_TEL>\r\n";
	$date .= "<EC_MOBILE>".iconv("UTF-8","big5",$guardian['MOBILE'])."</EC_MOBILE>\r\n";
	$date .= "<EC_OFFICE>".iconv("UTF-8","big5",$guardian['OFFICE_TEL'])."</EC_OFFICE>\r\n";
	$data .= "<EC_AREA>".iconv("UTF-8","big5",$emergency['G_AREA'])."</EC_AREA>\r\n";  														//emergency area
	$data .= "<EC_ROAD>".iconv("UTF-8","big5",$emergency['G_ROAD'])."</EC_ROAD>\r\n";  														//emergency road
	$data .= "<EC_ADDRESS>".iconv("UTF-8","big5",$emergency['G_ADDRESS'])."</EC_ADDRESS>\r\n"; 											 			//emergency address
	
	$data .= "</STUDENT>\r\n";
}
$data .= "</SRA>";


intranet_closedb();


header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Content-type: application/octet-stream');
header('Content-Length: '.strlen($data));

header('Content-Disposition: attachment; filename="'.$filename.'";');	
header("ContentType:text/xml; pragma:no-cache; Cache-Control:no-cache; charset=BIG5; ");
?>


<?=$data?>