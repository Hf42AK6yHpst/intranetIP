<?php
# using:
/*
 *  2020-09-01 Cameron
 *      - change xml encoding from ISO-8859-1 to BIG5
 *
 *  2020-08-28 Cameron
 *      - pass argument $charset to Array_To_XML() so that it can handle big5
 *
 *  2020-08-24 Cameron
 *      - revised to apply getGradeAndClass() instead of lookupSraGrade()
 *
 *  2020-08-21 Cameron
 *      - NAME_P should retrieve EnglishName before comma
 *
 *  2020-08-17 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libxml.php");


intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

# Temp Assign memory of this page
ini_set("memory_limit", "1G");

$lsr = new libstudentregistry();
$xml = new LibXML();

$studentArr = array();
$student = array();

# condition
$conds = "";

if($searchText != "")
    $conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

if($targetClass!="") {
    $studentAry = $lsr->storeStudent('0', $targetClass);
    $conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
    $conds .= " AND USR.RecordStatus='$registryStatus'";
else
    $conds .= " AND USR.RecordStatus IN (0,1,2)";


# SQL Statement
$sql = "SELECT
			srs.STUD_ID,
			srs.CODE,
			USR.ChineseName,
			USR.EnglishName,
			USR.Gender,
			IF(USR.DateOfBirth='0000-00-00 00:00:00','',LEFT(USR.DateOfBirth,10)) as B_DATE,
			srs.B_PLACE,
			srs.ID_TYPE,
			srs.ID_NO,
			srs.I_PLACE,
			IF(srs.I_DATE='0000-00-00 00:00:00','',LEFT(srs.I_DATE,10)) as I_DATE,
			IF(srs.V_DATE='0000-00-00 00:00:00','',LEFT(srs.V_DATE,10)) as V_DATE,
			srs.S6_TYPE,
			IF(srs.S6_IDATE='0000-00-00 00:00:00','',LEFT(srs.S6_IDATE,10)) as S6_IDATE,
			IF(srs.S6_VDATE='0000-00-00 00:00:00','',LEFT(srs.S6_VDATE,10)) as S6_VDATE,
			srs.NATION,
			srs.ORIGIN,
			USR.HomeTelNo,
			USR.MobileTelNo,
			srs.R_AREA,
			srs.RA_DESC,
			srs.AREA,
			srs.ROAD,
			srs.ADDRESS,
            ".Get_Lang_Selection('fa.NAME_C','fa.NAME_E')." as FATHER,
			".Get_Lang_Selection('mo.NAME_C','mo.NAME_E')." as MOTHER,
			fa.PROF as F_PROF,
			mo.PROF as M_PROF,
			g.GUARD,
		    g.LIVE_SAME,
/*			
			".Get_Lang_Selection('e.NAME_C','e.NAME_E')." as EC_NAME,
			e.TEL as EC_TEL,
			e.G_AREA,
			e.POST_CODE as EC_POSTAL_CODE,
			e.G_ROAD,
			e.G_ADDRESS,
*/						
			srs.S_CODE,
            yc.ClassTitleEN, 
			ycu.ClassNumber,
			".Get_Lang_Selection('g.NAME_C','g.NAME_E')." as G_NAME,
			g.G_RELATION,
			g.PROF as G_PROFESSION,
			g.G_AREA,
			g.POST_CODE as G_POSTAL_CODE,
			g.G_ROAD,
			g.G_ADDRESS,
			g.TEL as G_TEL,
			g.MOBILE as GUARDMOBILE,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN 
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN 
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN 
			STUDENT_REGISTRY_PG fa ON (fa.StudentID=srs.UserID AND fa.PG_TYPE='F') LEFT OUTER JOIN 
			STUDENT_REGISTRY_PG mo ON (mo.StudentID=srs.UserID AND mo.PG_TYPE='M') LEFT OUTER JOIN 
			STUDENT_REGISTRY_PG g ON (g.StudentID=srs.UserID AND g.PG_TYPE='G')
/*			STUDENT_REGISTRY_PG e ON (e.StudentID=srs.UserID AND g.PG_TYPE='E')*/
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			yc.Sequence, ycu.ClassNumber
		";
//debug_pr($sql);
//exit;
$rs = $lsr->returnResultSet($sql);
if (count($rs)) {
    foreach($rs as $_rs) {
        $student['STUD_ID'] = $lsr->convert2Big5($_rs['STUD_ID']);
        $student['CODE'] = $lsr->convert2Big5($_rs['CODE']);
        $student['NAME_C'] = $lsr->convert2Big5($_rs['ChineseName']);
        list($NAME_P, $nickName) = explode(',', $_rs['EnglishName']);
        $student['NAME_P'] = $lsr->convert2Big5($NAME_P);
        $student['SEX'] = $_rs['Gender'];
        $student['B_DATE'] = $_rs['B_DATE'];
        $student['B_PLACE'] = $lsr->convert2Big5($_rs['B_PLACE']);
        $student['ID_TYPE'] = $lsr->convert2Big5($_rs['ID_TYPE']);
        $student['ID_NO'] = $lsr->convert2Big5($_rs['ID_NO']);
        $student['I_PLACE'] = $lsr->convert2Big5($_rs['I_PLACE']);
        $student['I_DATE'] = $_rs['I_DATE'];
        $student['V_DATE'] = $_rs['V_DATE'];
        $S6_TYPE = $_rs['S6_TYPE'];
        if ($S6_TYPE) {
            $student['S6_TYPE'] = $S6_TYPE;
        }
        else {
            $student['S6_TYPE'] = '';
        }
        $student['S6_IDATE'] = $_rs['S6_IDATE'];
        $student['S6_VDATE'] = $_rs['S6_VDATE'];
        $student['NATION'] = $lsr->convert2Big5($_rs['NATION']);
        $student['ORIGIN'] = $lsr->convert2Big5($_rs['ORIGIN']);
        $student['TEL'] = $lsr->convert2Big5($_rs['HomeTelNo']);
        $student['MOBILE'] = $lsr->convert2Big5($_rs['MobileTelNo']);
        $student['R_AREA'] = $lsr->convert2Big5($_rs['R_AREA']);
        $student['RA_DESC'] = $lsr->convert2Big5($_rs['RA_DESC']);
        $student['AREA'] = $lsr->convert2Big5($_rs['AREA']);
        $student['POSTAL_CODE'] = '';
        $student['ROAD'] = $lsr->convert2Big5($_rs['ROAD']);
        $student['ADDRESS'] = $lsr->convert2Big5($_rs['ADDRESS']);
        $student['FATHER'] = $lsr->convert2Big5($_rs['FATHER']);
        $student['MOTHER'] = $lsr->convert2Big5($_rs['MOTHER']);
        $student['F_PROF'] = $lsr->convert2Big5($_rs['F_PROF']);
        $student['M_PROF'] = $lsr->convert2Big5($_rs['M_PROF']);
        $student['GUARD'] = $lsr->convert2Big5($_rs['GUARD']);
        $student['LIVE_SAME'] = $lsr->convert2Big5($_rs['LIVE_SAME']);
//        if ($_rs['EC_NAME'] && $_rs['EC_REL'] && $_rs['EC_TEL'] && $_rs['EC_AREA'] && $_rs['EC_ROAD'] && $_rs['EC_ADDRESS']) {
//            $student['EC_NAME'] = $lsr->convert2Big5($_rs['EC_NAME']);
//            $student['EC_REL'] = $lsr->convert2Big5($_rs['EC_REL']);
//            $student['EC_TEL'] = $lsr->convert2Big5($_rs['EC_TEL']);
//            $student['EC_AREA'] = $lsr->convert2Big5($_rs['EC_AREA']);
//            $student['EC_POSTAL_CODE'] = '';
//            $student['EC_ROAD'] = $lsr->convert2Big5($_rs['EC_ROAD']);
//            $student['EC_ADDRESS'] = $lsr->convert2Big5($_rs['EC_ADDRESS']);
//        }
//        else {
            $student['EC_NAME'] = '';
            $student['EC_REL'] = '';
            $student['EC_TEL'] = '';
            $student['EC_AREA'] = '';
            $student['EC_POSTAL_CODE'] = '';
            $student['EC_ROAD'] = '';
            $student['EC_ADDRESS'] = '';
//        }
        $student['S_CODE'] = $lsr->convert2Big5($_rs['S_CODE']);
        $gradeAndClass = $lsr->getGradeAndClass($_rs['ClassTitleEN']);
        $student['GRADE'] = $gradeAndClass['Grade'];
        $student['CLASS'] = $gradeAndClass['Class'];
        $student['C_NO'] = $_rs['ClassNumber'];
        $student['G_NAME'] = $lsr->convert2Big5($_rs['G_NAME']);
        $student['G_RELATION'] = $lsr->convert2Big5($_rs['G_RELATION']);
        $student['G_PROFESSION'] = $lsr->convert2Big5($_rs['G_PROFESSION']);
        $student['G_AREA'] = $lsr->convert2Big5($_rs['G_AREA']);
        $student['G_POSTAL_CODE'] = $lsr->convert2Big5($_rs['G_POSTAL_CODE']);
        $student['G_ROAD'] = $lsr->convert2Big5($_rs['G_ROAD']);
        $student['G_ADDRESS'] = $lsr->convert2Big5($_rs['G_ADDRESS']);
        $student['G_TEL'] = $lsr->convert2Big5($_rs['G_TEL']);
        $student['GUARDMOBILE'] = $lsr->convert2Big5($_rs['GUARDMOBILE']);

        $studentArr[] = $student;
        unset($student);
    }
    $s_code = $rs[0]['S_CODE'];
}

$batch_no = date("Ymd");
$filename = "stud3_".$s_code."_".$batch_no.".xml";

$sraArr = array();
$sraArr['SRA'] = array('STUDENT'=>$studentArr);
$content = '<?xml version="1.0" encoding="BIG5"?>'."\r\n";
$content .= '<!DOCTYPE SRA SYSTEM "http://appl.dsej.gov.mo/sra/edu/sra/sra3.dtd">'."\r\n";
$content .= $xml->Array_To_XML($sraArr, $omitHeader=true, $lineBreak=true, $charset="big5");

intranet_closedb();

# Output the file to user browser
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Content-type: application/octet-stream');
header('Content-Length: '.strlen($content));

header('Content-Disposition: attachment; filename="'.$filename.'";');
header("ContentType:text/xml; pragma:no-cache; Cache-Control:no-cache; charset=BIG5; ");
echo $content;

?>
