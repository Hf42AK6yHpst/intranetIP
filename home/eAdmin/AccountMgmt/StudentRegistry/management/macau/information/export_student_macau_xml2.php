<?php
# using:
/*
 *  2020-09-01 Cameron
 *      - change xml encoding from ISO-8859-1 to BIG5
 *
 *  2020-08-28 Cameron
 *      - pass argument $charset to Array_To_XML() so that it can handle big5
 *
 *  2020-08-24 Cameron
 *      - revised to apply getGradeAndClass() instead of lookupSraGrade()
 *
 *  2020-08-21 Cameron
 *      - NAME_P should retrieve EnglishName before comma
 *
 *  2020-08-17 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libxml.php");


intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

# Temp Assign memory of this page
ini_set("memory_limit", "1G");

$lsr = new libstudentregistry();
$xml = new LibXML();

$studentArr = array();
$student = array();

# condition
$conds = "";

if($searchText != "")
    $conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

if($targetClass!="") {
    $studentAry = $lsr->storeStudent('0', $targetClass);
    $conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
    $conds .= " AND USR.RecordStatus='$registryStatus'";
else
    $conds .= " AND USR.RecordStatus IN (0,1,2)";


# SQL Statement
$sql = "SELECT
            srs.CODE,
            srs.ID_NO,
            USR.EnglishName,
            srs.S_CODE,
            yc.ClassTitleEN, 
			ycu.ClassNumber,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			yc.Sequence, ycu.ClassNumber
		";

$rs = $lsr->returnResultSet($sql);
if (count($rs)) {
    foreach($rs as $_rs) {
        $student['CODE'] = $lsr->convert2Big5($_rs['CODE']);
        $student['ID_NO'] = $lsr->convert2Big5($_rs['ID_NO']);
        list($NAME_P, $nickName) = explode(',', $_rs['EnglishName']);
        $student['NAME_P'] = $lsr->convert2Big5($NAME_P);
        $student['S_CODE'] = $lsr->convert2Big5($_rs['S_CODE']);
        $gradeAndClass = $lsr->getGradeAndClass($_rs['ClassTitleEN']);
        $student['GRADE'] = $gradeAndClass['Grade'];
        $student['CLASS'] = $gradeAndClass['Class'];
        $student['C_NO'] = $_rs['ClassNumber'];
        $studentArr[] = $student;
        unset($student);
    }
    $s_code = $rs[0]['S_CODE'];
}

$batch_no = date("Ymd");
$filename = "stud2_".$s_code."_".$batch_no.".xml";

$sraArr = array();
$sraArr['SRA'] = array('STUDENT'=>$studentArr);
$content = '<?xml version="1.0" encoding="BIG5"?>'."\r\n";
$content .= '<!DOCTYPE SRA SYSTEM "http://appl.dsej.gov.mo/sra/edu/sra/sra2.dtd">'."\r\n";
$content .= $xml->Array_To_XML($sraArr, $omitHeader=true, $lineBreak=true, $charset="big5");

intranet_closedb();

# Output the file to user browser
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Content-type: application/octet-stream');
header('Content-Length: '.strlen($content));

header('Content-Disposition: attachment; filename="'.$filename.'";');
header("ContentType:text/xml; pragma:no-cache; Cache-Control:no-cache; charset=BIG5; ");
echo $content;

?>
