<?php
#using :

/*
 *  2020-04-16 Cameron
 *      - must declare var for confirm in checksubmit(),otherwise it'll encounter error when it gets error returned from ajax 
 *      
 *  2020-04-06 Cameron
 *      - ask admin to synchronize student info or not when save changes [case #Y173401]
 *      
 *  2019-08-23 Cameron
 *      - hide Submit button [case #Y166047]
 *      - set these fields be mandatory: Home Address (Chinese), Name in Chinese (Father & Mother), Email Address (Father & Mother), all Emergency Contact Person Information
 *
 *  2019-07-08 Cameron
 *      - show $student_district_data in English [case #Y164476]
 *
 *  2019-06-24 Cameron
 *      - pass $engOnly to getDistrictSelection() to always show district in English
 *      - add function isValidPhonePattern()
 *      
 *  2019-05-09 Cameron
 *      - update statement and declaration
 *      - hide left menu if there's no menu item for parent
 *      
 *  2019-03-11 Cameron
 *      - create this file
 * 
 */

// $PATH_WRT_ROOT is defined in index.php, other libraries are also included in this file
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$_SESSION['ParentFillOnlineReg'])) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$studentID = IntegerSafe($studentID);
$lsr = new libstudentregistry_kv();
if($_SESSION['ParentFillOnlineReg']) {
    $isMyChild = $lsr->verifyParentChild($studentID, $_SESSION['UserID']);
    if (!$isMyChild) {
        $laccessright->NO_ACCESS_RIGHT_REDIRECT();
        exit;
    }
    $linterface = new libstudentregistry_ui("popup.html");
}
else {
    $linterface = new libstudentregistry_ui();
}

$result = $lsr->getStudentInfoBasic($studentID);

//debug_pr($result);        
$li = new libuser($studentID);

# English Name
// if (($result['EnglishName'] == '') && ($result['FirstName'] != '') && ($result['LastName'] != '')) {
//     $englishName = $result['LastName']. ' ' . $result['FirstName'];
// }
// else {
//     $englishName = $result['EnglishName'];
// }

if($_SESSION['ParentFillOnlineReg'])
{
    $english_name_data = $englishName;
    
    $filePath = "$file_path/file/student_registry/";
    $statementFile = "KentvillePersonalInformationCollectionStatementAndDeclaration.pdf";
    
    $statementFileLink = '<a class="tabletool" href="/home/download_attachment.php?target_e=' . getEncryptedText($filePath. $statementFile) . '&filename_e=' . getEncryptedText($statementFile) . '">'.$Lang['StudentRegistry']['Here'].'</a>';
}
else
{
    $english_name_data = "<input type='text' name='english_name' value='". $englishName."'>";
}


#Gender related info
$gender_ary      = $lsr->getGenderAry();
$gender_val      = $lsr->getGenderDisplay($result['Gender']);
$gender_edit_ary = $gender_ary;
$gender_RBL_html = $lsr->GENERATE_RADIO_BTN_LIST($gender_edit_ary, "gender", $gender_val);

# PlaceOfBirth
$student_placeofbirth_data = $lsr->returnPresetCodeName("PLACEOFBIRTH", $result['B_PLACE_CODE']);
if ($result['B_PLACE_CODE'] == '999') {
    $student_placeofbirth_data = $result['B_PLACE_OTHERS'];
}
$student_placeofbirth_options = $lsr->displayPresetCodeSelection("PLACEOFBIRTH", "place_of_birth", $result['B_PLACE_CODE'], $noFirst=0, "place_of_birth");

# Ethnicity
$Ethnicity_data = $lsr->returnPresetCodeName("ETHNICITY", $result['ETHNICITY_CODE']);
if ($result['ETHNICITY_CODE'] == '999') {
    $Ethnicity_data = $result['RACE_OTHERS'];
}
$Ethnicity_data_options = $lsr->displayPresetCodeSelection("ETHNICITY", "ethnicity", $result['ETHNICITY_CODE'], $noFirst=0, "ethnicity");

# Language Spoken at Home
$HomeLang_data = $lsr->returnPresetCodeName("FAMILY_LANG", $result['FAMILY_LANG']);
if ($result['FAMILY_LANG'] == '999') {
    $HomeLang_data = $result['FAMILY_LANG_OTHERS'];
}
$HomeLang_data_options = $lsr->displayPresetCodeSelection("FAMILY_LANG", "family_lang", $result['FAMILY_LANG'], $noFirst=0, "family_lang");

# Nationality
$student_nationality_data = $lsr->returnPresetCodeName("NATIONALITY", $result['NATION_CODE']);
if ($result['NATION_CODE'] == '999') {
    $student_nationality_data = $result['NATION_OTHERS'];
}
$student_nationality_options = $lsr->displayPresetCodeSelection("NATIONALITY", "nation_code", $result['NATION_CODE'], $noFirst=0, "nation_code");

# District
$student_district_data = $lsr->getPresetCodeEnglishName("SUBDISTRICT", $result['DISTRICT_CODE']);
if ($result['DISTRICT_CODE'] == '999') {
    $student_district_data = $result['ADDRESS_DISTRICT_EN'];
}
$student_district_options = $lsr->getDistrictSelection("student_district_code", $result['DISTRICT_CODE'], $engOnly=true);

# Address Area
for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
{
    $AddressAreaOptions .= "<input type='radio' name='address_area' class='area' value='". ($i+1) ."' id='AddressArea". ($i+1) ."' ". ($result['ADDRESS_AREA']==($i+1) ? "checked": "")."> <label for='AddressArea". ($i+1) ."'>". $Lang['StudentRegistry']['AreaOptions'][$i] ."</label> ";
}

$siblingTables = $linterface->getSiblingsTable($studentID);


# Guardian
for($g=1;$g<=2;$g++) 
{
    $g_result = $lsr->getGuardianInfo($studentID, $g);        // $g = 1 <- father, g = 2 <- mother
    
    $gEmail[$g] = $g_result['EMAIL'];
    $guardian_email[$g] = "<input type='text' name='guardian_email". $g."' value='". $gEmail[$g] ."'/>";

    $gEnglishFirstName[$g] = $g_result['FIRST_NAME_E'];
    $guardian_english_first_name[$g] = "<input type='text' name='guardian_english_first_name". $g."' value='". $gEnglishFirstName[$g] ."'/>";

    $gEnglishLastName[$g] = $g_result['LAST_NAME_E'];
    $guardian_english_last_name[$g] = "<input type='text' name='guardian_english_last_name". $g."' value='". $gEnglishLastName[$g] ."'/>";
    
    $gEnglishName[$g] = $g_result['NAME_E'];
    $guardian_english_name[$g] = "<input type='text' name='guardian_english_name". $g."' value='". $gEnglishName[$g] ."'/>";
    
    $gChineseName[$g] = $g_result['NAME_C'];
    $guardian_chinese_name[$g] = "<input type='text' name='guardian_chinese_name". $g."' value='". $gChineseName[$g] ."'/>";
    
    $gBusinessName[$g] = $g_result['BUSINESS_NAME'];
    $guardian_business_name[$g] = "<input type='text' name='guardian_business_name". $g."' value='". $gBusinessName[$g] ."'/>";

    if ($g_result['IS_ALUMNI']) {
        $gIsAlumni[$g] = $Lang['General']['Yes'];
        $_checkIsAlumniYes = 'checked';
        $_checkIsAlumniNo = '';
    }
    else {
        $gIsAlumni[$g] = $Lang['General']['No'];
        $_checkIsAlumniYes = '';
        $_checkIsAlumniNo = 'checked';
    }
    $guardian_isAlumni[$g] = "<label><input type='radio' name='isAlumni_".$g."' id='isAlumniYes_".$g."' value='1' ".$_checkIsAlumniYes.">".$Lang['General']['Yes']."</label>";
    $guardian_isAlumni[$g] .= "<label><input type='radio' name='isAlumni_".$g."' id='isAlumniNo_".$g."' value='0' ".$_checkIsAlumniNo.">".$Lang['General']['No']."</label>";
    $graduateYear = $g_result['GRADUATE_YEAR'];
    $gGraduateYear[$g] = $graduateYear ? $graduateYear : '--';
    $guardian_graduateYear[$g]= $linterface->getGuardianGraduateYearOptions("graduateYear_".$g, "graduateYear_".$g, $g_result['GRADUATE_YEAR']);
    
    $gJobTitle[$g] = $g_result['JOB_TITLE'];
    $guardian_job_title[$g] = "<input type='text' name='guardian_job_title". $g."' value='". $g_result['JOB_TITLE'] ."'/>";

    $gOccupation[$g] = $g_result['JOB_NATURE'];
    $guardian_occupation[$g] = "<input type='text' name='guardian_occupation". $g."' value='". $g_result['JOB_NATURE'] ."'/>";
    
    $gTel[$g] = $g_result['TEL'];
    $guardian_tel[$g] = "<input type='text' name='guardian_tel". $g."' value='". $g_result['TEL'] ."'/>";
    
    $gMobile[$g] = $g_result['MOBILE'];
    $guardian_mobile[$g] = "<input type='text' name='guardian_mobile". $g."' id='guardian_mobile". $g."' value='". $g_result['MOBILE'] ."'/>";
    
    $gAddressRoomEn[$g] = $g_result['ADDRESS_ROOM_EN'];
    $gAddressFloorEn[$g] = $g_result['ADDRESS_FLOOR_EN'];
    $gAddressBlkEn[$g] = $g_result['ADDRESS_BLK_EN'];
    $gAddressBldEn[$g] = $g_result['ADDRESS_BLD_EN'];
    $gAddressEstEn[$g] = $g_result['ADDRESS_EST_EN'];
    $gAddressStrEn[$g] = $g_result['ADDRESS_STR_EN'];
    
    $gAddressDistrict[$g] = $lsr->returnPresetCodeName("SUBDISTRICT", $g_result['DISTRICT_CODE']);
    if ($g_result['DISTRICT_CODE'] == '999') {
        $gAddressDistrict[$g] = $g_result['ADDRESS_DISTRICT_EN'];
    }
    $gAddressDistrictOptions[$g] = $lsr->getDistrictSelection("guardian_district".$g, $g_result['DISTRICT_CODE']);
    $gAddressDistrictOthers[$g] = $g_result['ADDRESS_DISTRICT_EN'];
    $gAddressArea[$g] = $Lang['StudentRegistry']['AreaOptions'][$g_result['ADDRESS_AREA']-1];
    for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
    {
        $guardian_address_area[$g] .= "<input type='radio' name='guardian_address_area". $g."' class='area' value='". ($i+1) ."' id='guardian_address_area".$g."_". ($i+1) ."' ". ($g_result['ADDRESS_AREA']==($i+1)?"checked":"") ."> <label for='guardian_address_area".$g."_". ($i+1) ."'>". $Lang['StudentRegistry']['AreaOptions'][$i] ."</label> ";
    }
    
}



# Contact Persion in case of emergency
$contact_result = $lsr->getEmergencyContactInfo($studentID);
$nrContactResult = count($contact_result);

$maxContact = 2;
for($i=0; $i<$maxContact; $i++) {
    if ($i<$nrContactResult) {
        $_contact_result = $contact_result[$i];
        $cEnglishFirstName[$i] = $_contact_result['Contact3_EglishFirstName'];
        $cEnglishLastName[$i] = $_contact_result['Contact3_EglishLastName'];
        $cChineseName[$i] = $_contact_result['Contact3_ChineseName'];
        $cRelationshipCode[$i] = $_contact_result['Contact3_RelationshipCode'];
        $cRelationshipOthers[$i] = $_contact_result['Contact3_Relationship'];
        $cMobile[$i] = $_contact_result['Contact3_Mobile'];
    }
    else {
        $cEnglishFirstName[$i] = '';
        $cEnglishLastName[$i] = '';
        $cChineseName[$i] = '';
        $cRelationshipCode[$i] = '';
        $cRelationshipOthers[$i] = '';
        $cMobile[$i] = '';
    }
    
    $j = $i + 1;
    $contact_relationship[$i] = $lsr->returnPresetCodeName("RELATIONSHIP",  $cRelationshipCode[$i]);
    if ($cRelationshipCode[$i] == '999') {
        $contact_relationship[$i] = $cRelationshipOthers[$i];
    }

    if ($_SESSION['intranet_session_language'] == 'en') {
        $contact_relationship_options[$i] = $lsr->getPresetCodeEnglishSelection("RELATIONSHIP", "contact_relationship".$j, $cRelationshipCode[$i], $noFirst=0, "contact_relationship".$j, $groupByName=true);
    }
    else {
        $contact_relationship_options[$i] = $lsr->displayPresetCodeSelection("RELATIONSHIP", "contact_relationship".$j, $cRelationshipCode[$i], $noFirst=0, "contact_relationship".$j);
    }
    
    $contact_english_first_name[$i] = "<input type='text' name='contact_english_first_name". $j."' value='". $cEnglishFirstName[$i] ."'/>";
    $contact_english_last_name[$i] = "<input type='text' name='contact_english_last_name". $j."' value='". $cEnglishLastName[$i] ."'/>";
    $contact_chinese_name[$i] = "<input type='text' name='contact_chinese_name". $j."' value='". $cChineseName[$i] ."'/>";
//    $contact_relationship[$i] = "<input type='text' name='contact_relationship". $j."' value='". $cRelationship[$i] ."'/>";
    $contact_mobile[$i] = "<input type='text' name='contact_mobile". $j."' value='". $cMobile[$i] ."'/>";
}


    #Modified By
    $ModifiedBy = $result['ModifyBy']? $lsr->RETRIEVE_MODIFIED_BY_INFO($result['ModifyBy']) : "";
    
    $CurrentPageArr['StudentRegistry'] = 1;
    $CurrentPage = "Mgmt_RegistryInfo";
    $TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
    $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
    
    $MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();
    
    if($_SESSION['ParentFillOnlineReg']) {          // parent has no menu
        if (sizeof($MODULE_OBJ['menu'] == 0)) {
            unset($MODULE_OBJ);
            $MODULE_OBJ['title_css'] = "menu_closed";
        }
    }
    
    if(!$_SESSION['ParentFillOnlineReg'])
    {
        # navigation bar
        $PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
        $PAGE_NAVIGATION[] = array(($result['ClassName'] ? $result['ClassName'] : '')." ".$Lang['StudentRegistry']['StudentList'], ($result['YearClassID']) ? "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result['YearClassID'] : '');
        $PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result['EnglishName'] : $result['ChineseName']), "");
    }
    
    #Save and Cancel Button
//    $SaveBtn   = $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checksubmit(this.form)", "SaveBtn", "style=\"display: none\"");
    $SaveSubmitBtn   = $linterface->GET_ACTION_BTN($Lang['Button']['SaveSubmit'], "button", "javascript:document.form1.confirm_flag.value=1; checksubmit(this.form)", "SaveSubmitBtn", "style=\"display: none\"");
    $CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:ResetInfo()", "CancelBtn", "style=\"display: none\"");
    
    ########## Official Photo
    $photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
    if ($li->PhotoLink !="")
    {
        if (is_file($intranet_root.$li->PhotoLink))
        {
            $photo_link = $li->PhotoLink;
        }
    }
    
    $imgPath = $image_path."/".$LAYOUT_SKIN;
    if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]) {
        $exportCsvBtn = '<a style="float:left" href="export_kentville_csv.php?StudentID='.$studentID.'" class="contenttool"><img src="'.$imgPath.'/icon_export.gif" hspace="4" border="0" align="absmiddle">'.$Lang['StudentRegistry']['BtnExportCsv'].'</a>';
    }
    else {
        $exportCsvBtn = '';
    }
    $exportPdfBtn = '<a style="float:left" href="export_kentville_pdf.php?studentID='.$studentID.'" class="contenttool"><img src="'.$imgPath.'/icon_export.gif" hspace="4" border="0" align="absmiddle">'.$Lang['StudentRegistry']['BtnExportPdf'].'</a>';
    $editBtn = '<a style="float:left" href="#" onClick="EditInfo();" id="EditBtn" class="contenttool"><img src="'.$imgPath.'/icon_edit.gif" hspace="4" border="0" align="absmiddle">'.$Lang['Btn']['Edit'].'</a>';
    
    if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
    $linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
    ?>

<script language="javascript">
<!--
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#btnTools').attr('style', 'visibility: hidden');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#SaveSubmitBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');

		$('#place_of_birth').change();
		$('#nation_code').change();
		$('#ethnicity').change();
		$('#family_lang').change();
		$('select[name^="contact_relationship"]').change();
		$("input:radio[name^='isAlumni_']:checked").change();
		$("input:radio[name^='BS_IsSchoolStudentOrAlumni_']:checked").change();
		$('select[name^="BS_CurLevel_"]').change();
		$('.district').change();
		
		enable_BSTable();
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', 'text-align:left');
		$('#btnTools').attr('style', '');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#SaveSubmitBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');

		$('#spanBPlace').css('display','none');
		$('#spanNation').css('display','none');
		$('#spanRace').css('display','none');				
		$('#spanFamilyLang').css('display','none');		
		
		$('div[id^="spanContactRelationship"]').each(function(){
			$(this).css('display','none');
		});
		
		$('div[id^="spanDistrictOthers"]').each(function(){
			$(this).css('display','none');
		});
		
		$('#ErrorLog').html('');
	}

	function checksubmit(obj)
	{
<?php if(!$_SESSION['ParentFillOnlineReg']): ?>
		// English name, Chinese Name
//		if(!check_text(obj.english_name,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EnglishName']?>"))	return false;
		if(!check_text(obj.chinese_name,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ChineseName']?>"))	return false;
<?php endif; ?>
		
<?php 
	# for parent fill-in checking only 
	if($_SESSION['ParentFillOnlineReg']): ?>
		// compulsory field: English last name, first name
		if(!check_text(obj.last_name,"<?=$i_alert_pleasefillin." ".$i_UserLastName?>"))	return false;		
		if(!check_text(obj.first_name,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['GivenNames']?>"))	return false;

		// compulsory field: Chinese Name
		if(!check_text(obj.chinese_name,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ChineseName']?>"))	return false;

		// compulsory field: DOB 
		if(!check_text(obj.date_of_birth,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['BirthDate']?>"))	return false;

		// compulsory field: place of birth 
		if(!check_text(obj.place_of_birth,"<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['BirthPlace']?>"))	return false;

		// compulsory field: Identification Document No. 
		if(!check_text(obj.id_no,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ID_DocumentNo']?>"))	return false;
		
		// compulsory field: gender
		if(!countChecked(obj, "gender")) {
			alert("<?=$i_alert_pleaseselect?> <?=$Lang['StudentRegistry']['Gender']?>");
			obj.gender[0].focus();
			return false;
		}
		
		// compulsory field: Nationality 
		if(!check_text(obj.nation_code,"<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['Nationality']?>"))	return false;
		
		// compulsory field: Ethnicity 
		if(!check_text(obj.ethnicity,"<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['Race']?>"))	return false;

		// compulsory field: Family Spoken Lang 
		if(!check_text(obj.family_lang,"<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['HomeLang']?>"))	return false;
		
		// compulsory field: Home Phone No.
		if(!check_text(obj.home_tel_no,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['HomePhoneNo']?>"))	return false;
		if (isValidPhonePattern(obj.home_tel_no.value) == false) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['InvalidHomeTel'];?>");
			obj.home_tel_no.focus();
			return false;
		}
		
		// compulsory field: English Address
		if ((Trim(obj.address_room_en.value) == '') && (Trim(obj.address_floor_en.value) == '') && (Trim(obj.address_blk_en.value) == '') && (Trim(obj.address_bld_en.value) == '') && (Trim(obj.address_est_en.value) == '') && (Trim(obj.address_str_en.value) == '')) {
			alert("<?php echo $i_alert_pleasefillin." ".$Lang['StudentRegistry']['Address'];?>");
			obj.address_room_en.value = '';
			obj.address_room_en.focus();
			return false;
		}

        // compulsory field: Chinese Address
        if(!check_text(obj.chi_address,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ChiAddress']?>"))	return false;

		// compulsory fiels: father info
		if(!check_text(obj.guardian_english_last_name1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['FatherLastName']?>"))	return false;
		if(!check_text(obj.guardian_english_first_name1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['FatherFirstName']?>"))	return false;
		if(!check_text(obj.guardian_chinese_name1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['FatherChineseName']?>"))	return false;
		if(!check_text(obj.guardian_email1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['FatherEmail']?>"))	return false;
		if(!check_text(obj.guardian_mobile1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['FatherMobile']?>"))	return false;
		if (obj.guardian_tel1.value != '' && !isValidPhonePattern(obj.guardian_tel1.value)) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['InvalidTel'];?>");
			obj.guardian_tel1.focus();
			return false;
		}
		if (obj.guardian_mobile1.value != '' && !isValidPhonePattern(obj.guardian_mobile1.value)) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['InvalidTel'];?>");
			obj.guardian_mobile1.focus();
			return false;
		}
		
		// compulsory fiels: mother info
		if(!check_text(obj.guardian_english_last_name2,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['MotherLastName']?>"))	return false;
		if(!check_text(obj.guardian_english_first_name2,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['MotherFirstName']?>"))	return false;
		if(!check_text(obj.guardian_chinese_name2,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['MotherChineseName']?>"))	return false;
		if(!check_text(obj.guardian_email2,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['MotherEmail']?>"))	return false;
		if(!check_text(obj.guardian_mobile2,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['MotherMobile']?>"))	return false;
		if (obj.guardian_tel2.value != '' && !isValidPhonePattern(obj.guardian_tel2.value)) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['InvalidTel'];?>");
			obj.guardian_tel2.focus();
			return false;
		}
		if (obj.guardian_mobile2.value != '' && !isValidPhonePattern(obj.guardian_mobile2.value)) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['InvalidTel'];?>");
			obj.guardian_mobile2.focus();
			return false;
		}
		
    <?php for ($i=1;$i<=2;$i++):?>
    		if(!check_text(obj.contact_english_last_name<?php echo $i;?>,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EmergencyContactLastName'].$i?>"))	return false;
			if(!check_text(obj.contact_english_first_name<?php echo $i;?>,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EmergencyContactFirstName'].$i?>"))	return false;
			if(!check_text(obj.contact_chinese_name<?php echo $i;?>,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EmergencyContactChineseName'].$i?>"))	return false;
			if(!check_text(obj.contact_relationship<?php echo $i;?>,"<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['EmergencyContactRelationship'].$i?>"))	return false;
			if(!check_text(obj.contact_mobile<?php echo $i;?>,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EmergencyContactPhone'].$i?>"))	return false;
			
	<?php endfor;?>	

	<?php for ($i=1;$i<=2;$i++):?>
		if (obj.contact_mobile<?php echo $i;?>.value != '' && !isValidPhonePattern(obj.contact_mobile<?php echo $i;?>.value)) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['InvalidTel'];?>");
			obj.contact_mobile<?php echo $i;?>.focus();
			return false;
		}
	<?php endfor;?>
	
<?php endif; ?>
		
		// Date of Birth
		if(Trim(obj.date_of_birth.value)) {	
			if(!check_date(obj.date_of_birth,"<?=$Lang['General']['InvalidDateFormat']?>"))	return false;
		}

		for(var i=0;i<3;i++) {
			var val = Trim(eval('obj.BS_DateOfBirth_' + i +'.value'));
			var dateObj = eval('obj.BS_DateOfBirth_' + i);
//console.log(val);
			 
			if(val) {	
				if(!check_date(dateObj,"<?=$Lang['General']['InvalidDateFormat']?>"))	return false;
			}
		}
		
		for(var i=1;i<=2;i++) {
			if (eval("obj.guardian_email"+ i+'.value') != '') {
				if(!validateEmail(eval("obj.guardian_email"+ i), "<?php echo $i_invalid_email; ?>.")) return false;
			}
		}
		
// 		if(obj.photo && obj.photo.value!="" && getFileExtension(obj.photo).toLowerCase()!=".jpg"){
//			alert("<?=$i_UserPhotoGuide?>");
// 			return false;
// 		}
		
<?php if($_SESSION['ParentFillOnlineReg']): ?>		
		if (!$('#acceptDeclaration').attr('checked')) {
			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['AcceptDeclarations'];?>");
			return false;
		}
<?php else:?>
		var confirm = window.confirm('<?php echo $Lang['StudentRegistry']['SynchronizationConfirm'];?>');
		if (confirm) {
			$('#IsSynchronize').val('1');
		}
		else {
			$('#IsSynchronize').val('0');
		}
<?php endif;?>

		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax.php',
			data : {
				action: 'checkFormBeforeSave',
				studentID 	: obj.studentID.value,
				id_no: obj.id_no.value,
				guardian_email1: obj.guardian_email1.value,
				guardian_email2: obj.guardian_email2.value
 			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					if (ajaxReturn.html != '') {
						$('#ErrorLog').html(ajaxReturn.html);
					}
					else {
						obj.submit();
					}					
				}
			},
			error: show_ajax_error
		});
		
	}

// 	function add_sibling()
// 	{
// 		$('#BSTable').css('display', '');		// show table
// 		var rowCount = parseInt($('#bs_row_count').val());
// 		if ($('#BSTable tr').length > 3) {
//			alert("<?php echo $Lang['StudentRegistry']['JS_warning']['MaxSiblings'];?>");
// 		}
// 		else {
//     		$.ajax({
//     			dataType: "json",
//     			type: "POST",
//     			url: 'ajax.php',
//     			data : {
//     				action: 'addBSRow',
//     				rowNr: rowCount 
//     			},		  
//     			success: function(ajaxReturn){
//     				if (ajaxReturn != null && ajaxReturn.success){
//     					$('#BSTable').append(ajaxReturn.html);
//     					$('#bs_row_count').val(rowCount+1);
//     				}
//     			},
//     			error: show_ajax_error
//     		});
// 		}
// 	}

	function isValidPhonePattern(phoneNo)
	{
		var pattern = /^[+]{0,1}[0-9]*$/;
		return pattern.test(phoneNo);
	}
	
	function show_ajax_error() 
	{
		alert('<?=$Lang['General']['AjaxError']?>');
	}

// 	function removeBSRow(rowNr)
// 	{
// 		$('tr #bs_row_' + rowNr).remove();
// 	}
	
<? if($_SESSION['ParentFillOnlineReg'] && $xmsg) {?>
if(window.opener!=null)
	window.opener.location.reload();
<? } ?>	

function enable_BSTable()
{
	$('#BSTable').attr('style', '');
}

$(document).ready(function(){
	$('.district').change(function(){
		var districtCode = $(this).val();
		if (districtCode == '999') {
			$(this).parent().parent().find("div[id^=spanDistrictOthers]").css('display','inline');
		}
		else {
			$(this).parent().parent().find("div[id^=spanDistrictOthers]").css('display','none');
		}
		
		districtCode = districtCode.substring(0,1);
		var tbody = $(this).parent().parent().parent().parent();
		
		if (districtCode>='A' && districtCode<='D') {
			tbody.find('.area[value=1]').attr('checked',true);
		}
		else if (districtCode>='E' && districtCode<='J') {
			tbody.find('.area[value=2]').attr('checked',true);
		} 
		else if (districtCode>='K' && districtCode<='T') {
			tbody.find('.area[value=3]').attr('checked',true);
		} 
	});

	$('#place_of_birth').change(function(){
		if ($(this).val() == '999') {
			$('#spanBPlace').css('display','inline');
		}
		else {
			$('#spanBPlace').css('display','none');
		}
	});

	$('#ethnicity').change(function(){
		if ($(this).val() == '999') {
			$('#spanRace').css('display','inline');
		}
		else {
			$('#spanRace').css('display','none');
		}
	});

	$('#nation_code').change(function(){
		if ($(this).val() == '999') {
			$('#spanNation').css('display','inline');
		}
		else {
			$('#spanNation').css('display','none');
		}
	});
	
	$('#family_lang').change(function(){
		if ($(this).val() == '999') {
			$('#spanFamilyLang').css('display','inline');
		}
		else {
			$('#spanFamilyLang').css('display','none');
		}
	});

	$("input:radio[name^='isAlumni_']").change(function() {
		var id = $(this).attr('id');
		var idx = id.substring(id.length-1,id.length);
		
		if ($(this).val() == '0') {
			$('#graduateYear_'+idx).css('display','none');
			$('#graduateYear_'+idx).val('');
		}
		else {
			$('#graduateYear_'+idx).css('display','');
		}
	});
	
	$('select[name^="contact_relationship"]').change(function(){
		var id = $(this).attr('id');
		var idx = id.substring(id.length-1,id.length);
		if ($(this).val() == '999') {
			$('#spanContactRelationship' + idx).css('display','inline');
		}
		else {
			$('#spanContactRelationship' + idx).css('display','none');
		}
	});

	$('select[name^="BS_CurLevel_"]').change(function(){
		var id = $(this).attr('id');
		var idx = id.substring(12,id.length);
// console.log(id);		
// console.log(idx);
		if ($(this).val() == '999') {
			$('#spanCurLevelOthers' + idx).css('display','inline');
		}
		else {
			$('#spanCurLevelOthers' + idx).css('display','none');
		}
	});

	$("input:radio[name^='BS_IsSchoolStudentOrAlumni_']").change(function() {
		var id = $(this).attr('id');
		var idx = id.substring(id.length-1,id.length);
		
		if ($(this).val() == '0') {
			$('#trSchoolUserOrAlumni_'+idx).css('display','none');
		}
		else {
			$('#trSchoolUserOrAlumni_'+idx).css('display','');
		}
	});

	<?php if (IntegerSafe($_POST['EditRecord'])):?>
		$('#EditBtn').click();
	<?php endif;?>
});
//-->
</script>

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<form name="form1" method="POST" action="view_kentville_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<div class="table_board">
			
            	<div class="content_top_tool">
            	<?php if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $_SESSION['ParentFillOnlineReg']):?>
            		<div id="btnTools"><?=$exportCsvBtn. $exportPdfBtn. $editBtn?></div><br>
            	<?php endif;?>
            		<br style="clear:both" />
            	</div>

                <div>
                	<table class="form_table_v30">
                		<tr>
                			<td class="field_title_short"><?php echo $Lang['StudentRegistry']['StudentName'];?></td>
                			<td><?php echo Get_Lang_Selection($result['ChineseName'],$result['EnglishName']);?></td>
                			<td class="field_title_short"><?php echo $Lang['StudentRegistry']['StudentIDNo'];?></td>
                			<td><?php echo $result['UserLogin']?></td>
                			<td class="field_title_short"><?php echo $Lang['StudentRegistry']['Date'];?></td>
                			<td><?php echo date("Y-m-d")?></td>
                		</tr>
                	</table>
                </div>

				<div>&nbsp;</div>
                <div>
                	<span style="font-weight: bold;"><?php echo $Lang['StudentRegistry']['StudentDataRecord'].' ('.getCurrentAcademicYear('en').')'; ?></span>
                </div>
                <div>&nbsp;</div>
                
                
                <? ########################################################### ?>
				<? #  Student Information                                    # ?>
				<? ########################################################### ?>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
                <table class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td colspan="3">
                    			<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
                 		       			<tr>
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['LastName']?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $result['LastName'] ? $result['LastName'] : "--"?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="last_name" value="<?= $result['LastName']?>"/></span>
                     		       			</td>
                     		       			
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['GivenNames']?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $result['FirstName'] ? $result['FirstName'] : "--"?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="first_name" value="<?= $result['FirstName']?>"/></span>
                     		       			</td>
                        	    		</tr>
                     		       		<tr>
                     		       			<td colspan="4"><em>(<?= $Lang['StudentRegistry']['AsAppearsOn']?>)</em></td>
<!--                      		       			
                     		       			<td>
                     		       				<span class="Edit_Hide"><?=$result['EnglishName']?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$english_name_data?></span>
                     		       			</td>
 -->                     		       			
                 		       			</tr>
                        	    	</table>
								</div>
							</td>
                        	
							<td rowspan="10">
                            	<?=$Lang['StudentRegistry']['OfficialPhoto']?><br /><img src="<?=$photo_link?>" width="100" height="130"><br>
                        	</td>
                        </tr>
                        
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
							<td>
     		       				<span class="Edit_Hide"><?=$result['ChineseName']?></span>
     		       				<span class="Edit_Show" style="display:none">
     		       					<input type="text" name="chinese_name" value="<?php echo $result['ChineseName']; ?>">
     		       				</span>
							</td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Class'] ?></td>
							<td>
								<?php echo $result['ClassName']; ?>
							</td>							
                        </tr>
                        
                        <tr>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['BirthDate'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result['DOB']=="0000-00-00" ? "" : $result['DOB'] ?></span>
                            	<span class="Edit_Show" style="display:none">
                            	<!--  <input type="text" name="date_of_birth" value="<?= $result['DOB']=="0000-00-00" ? "" : $result['DOB']  ?>"/> <span class="tabletextremark">(YYYY-MM-DD)</span>-->
                            	<?php echo $linterface->GET_DATE_PICKER("date_of_birth", $result['DOB']=="0000-00-00" ? "" : $result['DOB']); ?>
                            	</span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['BirthPlace'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $student_placeofbirth_data?></span>
     		       				<span class="Edit_Show" style="display:none"><?=$student_placeofbirth_options?></span>
								<div id="spanBPlace" style='display:none' class='form_field_sub_content'>
									<table>
										<tr>
											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
											<td><input type="text" name="bplace_others" id="bplace_others" value="<?=$result['B_PLACE_OTHERS']?>"></td>
										</tr>
									</table>
								</div>                            	
                            </td>
                            
                        </tr>

                        <tr>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ID_DocumentNo'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result['ID_NO']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="id_no" value="<?= $result['ID_NO']?>"/></span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gender_val ?></span>
                            	<span class="Edit_Show" style="display:none"><?= $gender_RBL_html ?></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['Nationality']?></td>
                            <td>
                            	<span class="Edit_Hide"><?php echo $student_nationality_data;?></span>
                            	<span class="Edit_Show" style="display:none"><?php echo $student_nationality_options;?></span>
								<div id="spanNation" style='display:none' class='form_field_sub_content'>
									<table>
										<tr>
											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
											<td><input type="text" name="nation_others" id="nation_others" value="<?=$result['NATION_OTHERS']?>"></td>
										</tr>
									</table>
								</div>                            	
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['Race'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?php echo $Ethnicity_data;?></span>
                            	<span class="Edit_Show" style="display:none"><?php echo $Ethnicity_data_options;?></span>
								<div id="spanRace" style='display:none' class='form_field_sub_content'>
									<table>
										<tr>
											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
											<td><input type="text" name="race_others" id="race_others" value="<?=$result['RACE_OTHERS']?>"></td>
										</tr>
									</table>
								</div>                            	
                            </td>
                        
                        <tr>
							<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['HomeLang'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?php echo $HomeLang_data;?></span>
                            	<span class="Edit_Show" style="display:none"><?php echo $HomeLang_data_options;?></span>
								<div id="spanFamilyLang" style='display:none' class='form_field_sub_content'>
									<table>
										<tr>
											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
											<td><input type="text" name="family_lang_others" id="family_lang_others" value="<?=$result['FAMILY_LANG_OTHERS']?>"></td>
										</tr>
									</table>
								</div>                            	
                            </td>
                            
							<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result['HomeTelNo']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="home_tel_no" value="<?= $result['HomeTelNo']?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['Address'] ?><br> (<?php echo $Lang['StudentRegistry']['inEnglish'];?>)</td>
                            <td colspan="3">
                            			<div class="form_field_sub_content">
		                     		       	<table class="inside_form_table">
		                     		       		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
		                     		       			<td>
		                     		       				<span class="Edit_Hide"><?= $result['ADDRESS_ROOM_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_room_en" maxlength="5" value="<?= $result['ADDRESS_ROOM_EN']?>"/></span>
		                     		       			</td>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
		                     		       			<td>
		                     		       				<span class="Edit_Hide"><?= $result['ADDRESS_FLOOR_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_floor_en" maxlength="5" value="<?= $result['ADDRESS_FLOOR_EN']?>"/></span>
		                     		       			</td>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
		                        	    			<td>
		                        	    				<span class="Edit_Hide"><?= $result['ADDRESS_BLK_EN']?></span>
		                        	    				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_blk_en" maxlength="5" value="<?= $result['ADDRESS_BLK_EN']?>"/></span>
		                        	    			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result['ADDRESS_BLD_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_bld_en" value="<?= $result['ADDRESS_BLD_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result['ADDRESS_EST_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_est_en" value="<?= $result['ADDRESS_EST_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result['ADDRESS_STR_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_str_en" value="<?= $result['ADDRESS_STR_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $student_district_data?></span>
		                     		       				<span class="Edit_Show" style="display:none"><?=$student_district_options?></span>
                        								<div id="spanDistrictOthers" style='display:none' class='form_field_sub_content'>
                        									<table>
                        										<tr>
                        											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
                        											<td><input type="text" name="district_others" id="district_others" value="<?=$result['ADDRESS_DISTRICT_EN']?>"></td>
                        										</tr>
                        									</table>
                        								</div>                            	
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $Lang['StudentRegistry']['AreaOptions'][$result['ADDRESS_AREA']-1]?></span>
		                     		       				<span class="Edit_Show" style="display:none"><?=$AddressAreaOptions?></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    	</table>
										</div>
										</td>
                        </tr>
                       
                        <tr>
							<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['Address'] ?><br> (<?php echo $Lang['StudentRegistry']['inChinese'];?>)</td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $result['ADDRESS_STR_CH']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="chi_address" value="<?= $result['ADDRESS_STR_CH']?>"/></span>
                            </td>
						</tr>
						                       
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['SchoolBus'] ?></td>
                            <td>
                            	<?= $result['SCHOOL_BUS']?>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['NannyBus'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result['NANNY_BUS'] ?></span> 
                            	<span class="Edit_Show" style="display:none"><input type="text" name="nanny_bus" value="<?= $result['NANNY_BUS']?>" /></span>
                            </td>
                        </tr>

					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				
				
				
				<? ########################################################### ?>
				<? #  Guardian                                               # ?>
				<? ########################################################### ?>
				<? for($g=1;$g<=2;$g++)  { ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= ($g==1) ?$Lang['StudentRegistry']['FatherInfo']:$Lang['StudentRegistry']['MotherInfo']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
 		       			<tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td colspan="3">
                    			<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
<!--
                      		       		<tr>
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['DisplayName']?>)</em></td>
                     		       			<td>
                                           		<span class="Edit_Hide"><?= $gEnglishName[$g] ?></span>
                                            	<span class="Edit_Show" style="display:none"><?=$guardian_english_name[$g]?></span>
                     		       			</td>
                 		       			</tr>
 -->                 		       			
                 		       			<tr>
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['LastName']?>)</em></td>
                     		       			<td>
                                           		<span class="Edit_Hide"><?= $gEnglishLastName[$g] ?></span>
                                            	<span class="Edit_Show" style="display:none"><?=$guardian_english_last_name[$g]?></span>
                     		       			</td>
                     		       			
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['GivenNames']?>)</em></td>
                     		       			<td>
                                           		<span class="Edit_Hide"><?= $gEnglishFirstName[$g] ?></span>
                                            	<span class="Edit_Show" style="display:none"><?=$guardian_english_first_name[$g]?></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
								</div>
							</td>
        	    		</tr>
					
						<tr>
                         	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gChineseName[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_chinese_name[$g]?></span>
                            </td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
					
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['BusinessName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?=$gBusinessName[$g]?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_business_name[$g]?></span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ParentEMailAddress']?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gEmail[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_email[$g]?></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['BusinessAddress'] ?></td>
                            <td colspan="3">
                    			<div class="form_field_sub_content" id="gAddressDiv<?=$g?>">
                     		       	<table class="inside_form_table">
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $gAddressRoomEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_room_en<?=$g?>" maxlength="5" value="<?= $gAddressRoomEn[$g] ?>"/></span>
                     		       			</td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $gAddressFloorEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_floor_en<?=$g?>" maxlength="5" value="<?= $gAddressFloorEn[$g] ?>"/></span>
                     		       			</td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
                        	    			<td>
                        	    				<span class="Edit_Hide"><?= $gAddressBlkEn[$g] ?></span>
                        	    				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_blk_en<?=$g?>" maxlength="5" value="<?= $gAddressBlkEn[$g] ?>"/></span>
                        	    			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressBldEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_bld_en<?=$g?>" value="<?= $gAddressBldEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressEstEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_est_en<?=$g?>" value="<?= $gAddressEstEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressStrEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_str_en<?=$g?>" value="<?= $gAddressStrEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressDistrict[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$gAddressDistrictOptions[$g]?></span>
                								<div id="spanDistrictOthers<?php echo $g;?>" style='display:none' class='form_field_sub_content'>
                									<table>
                										<tr>
                											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
                											<td><input type="text" name="district_others<?php echo $g;?>" id="district_others<?php echo $g;?>" value="<?=$gAddressDistrictOthers[$g]?>"></td>
                										</tr>
                									</table>
                								</div>                            	
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressArea[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$guardian_address_area[$g]?></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
								</div>
							</td>
                        </tr>
                        
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['JobTitle'] ?></td>
                            <td >
                            	<span class="Edit_Hide"><?= $gJobTitle[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_job_title[$g]?></span>	
                           	</td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Job'] ?></td>
                            <td >
                            	<span class="Edit_Hide"><?= $gOccupation[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_occupation[$g]?></span>	
                           	</td>
                        </tr>

                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Alumni']?></td>
                            <td >
                            	<span class="Edit_Hide"><?= $gIsAlumni[$g] ?><?php if ($gIsAlumni[$g]) echo '/'. $gGraduateYear[$g]; ?></span>
                            	<span class="Edit_Show" style="display:none"><?php echo $guardian_isAlumni[$g] . $guardian_graduateYear[$g]?></span>	
                           	</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></span></td>
                        	<td>
                            	<span class="Edit_Hide"><?= $gTel[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_tel[$g]?></span>	
                           	</td>
                        	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><span class="field_title"><?= $Lang['StudentRegistry']['MobilePhoneNo']?></span></td>
                         	<td>
                            	<span class="Edit_Hide"><?= $gMobile[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_mobile[$g]?></span>	
                           	</td>
						</tr>
						
					</tbody>
				</table>
				<? } ?>
				
				
				<? ########################################################### ?>
				<? #  Last School 	                                         # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['LastSchool']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['LastSchoolName']?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result['LAST_SCHOOL_EN']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="last_school_en" value="<?= $result['LAST_SCHOOL_EN']?>"/></span>
                            	</td>
                        </tr>
				</tbody>
				</table>
				
				
				<? ########################################################### ?>
				<? #  Brothers & Sistes currently studying                   # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['BrotherSisterInfo']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td>
                            	<span class="Edit_Hide"><?=$siblingTables['viewTable']?></span>
                            	<span class="Edit_Show" style="display:none;">
								
								<?=$siblingTables['editTable']?>
								</span>
                            </td>
                        </tr>
				</tbody>
				</table>

				
				<? ########################################################### ?>
				<? #  Contact Persion in case of emergency                   # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['EmergencyContactPersonInfo']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
					
				<?php for($i=0; $i<$maxContact; $i++):?>					
					<?php $j = $i+1; ?>
						<tr>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['EmergencyContact'] ?> (<?php echo ($i+1);?>) <br>
                            	(<?php echo $Lang['StudentRegistry']['NonParent'];?>)
                            </td>
                            <td colspan="3">
                    			<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
                 		       			<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['LastName']?>)</em></td>
                     		       			<td>
                                           		<span class="Edit_Hide"><?= $cEnglishLastName[$i] ?></span>
                                            	<span class="Edit_Show" style="display:none"><?=$contact_english_last_name[$i]?></span>
                     		       			</td>
                     		       			
                     		       			<td><em>(<?= $Lang['StudentRegistry']['GivenNames']?>)</em></td>
                     		       			<td>
                                           		<span class="Edit_Hide"><?= $cEnglishFirstName[$i] ?></span>
                                            	<span class="Edit_Show" style="display:none"><?=$contact_english_first_name[$i]?></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
                        	    </div>
                        	</td>
                        </tr>
                        
						<tr>
                         	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $cChineseName[$i] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$contact_chinese_name[$i]?></span>
                            </td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        
						<tr>
                         	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['GuardianStudentRelationship']?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $contact_relationship[$i] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$contact_relationship_options[$i]?></span>
								<div id="spanContactRelationship<?php echo $j;?>" style='display:none' class='form_field_sub_content'>
									<table>
										<tr>
											<td><em><?=$Lang['General']['PlsSpecify']?></em></td>
											<td><input type="text" name="contact_relationship_others<?php echo $j;?>" id="contact_relationship_others<?php echo $j;?>" value="<?=$cRelationshipOthers[$i]?>"></td>
										</tr>
									</table>
								</div>                            	
                            	
                            </td>
                         	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ContactPhone']?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $cMobile[$i] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$contact_mobile[$i]?></span>
                            </td>
                        </tr>
                <?php endfor;?>
                        
                  	</tbody>
				</table>
				
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				
				<div id="ErrorLog"></div>
				
			<?php if($_SESSION['ParentFillOnlineReg']):?>
				<div class="Edit_Show" style="display:none">
    				<table class="form_table_v30">
    					<tr>
    						<td><?php echo sprintf($Lang['StudentRegistry']['PersonalInformationCollectionStatement'],$statementFileLink)?></td>
    					</tr>
    					
    					<tr>
    						<td><label style="font-weight: bold"><input type="checkbox" name="acceptDeclaration" id="acceptDeclaration" value="1"><?php echo $Lang['StudentRegistry']['ReadAndUnderstandDeclarations'];?></label></td>
    					</tr>
					</table>
				</div>				
			<?php endif;?>
			
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark" style="text-align:left">
					<?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result['DateModified']? $result['DateModified']." (".$ModifiedBy.")" : "--"?><br>
					<?= $Lang['StudentRegistry']['LastSubmitted'] ?> : <?= $result['DATA_CONFIRM_DATE']? $result['DATA_CONFIRM_DATE']." (".$result['SubmitByName'].")" : "--"?>
					</span>
					
					<? if($_SESSION['ParentFillOnlineReg']) {?>
					<br><br><br><span class="row_content tabletextremark" style="text-align:left"><?=$Lang['StudentRegistry']['PrivayRemark']?></span>
					<br><br>
					<? } ?>
                    <p class="spacer"></p>
<!--                    --><?//= $SaveBtn ?>
                    <?= $SaveSubmitBtn ?>
                    <?= $CancelBtn ?>
                </div>
			</div>
		</td>
	</tr>
	
</table>
<br>

<input type="hidden" name="studentID" id="studentID" value="<?=$studentID?>">
<input type="hidden" name="confirm_flag" id="confirm_flag" value="0">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="IsSynchronize" id="IsSynchronize" value="0">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>