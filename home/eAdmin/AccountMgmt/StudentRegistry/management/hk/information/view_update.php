<?php
#using:

#########################
#
#   Date:   2019-08-15 Cameron
#           add/update confirm data to STUDENT_REGISTRY_SUBMITTED so that the confirm status is by academic year [case #Y164458]
#
#   Date:   2019-07-18 Cameron
#           fix: don't call updateCustomValue if there's no custom column [case #B165321]
#
#   Date:   2019-04-29  Philips
#           add Custom Column
#
#	Date:	2014-05-05	YatWoon
#			add BSName
#
#########################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$lps = new libuserpersonalsettings();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lfcm = new form_class_manage();

# current date/time
$cur_datetime = Date("Y-m-d H:i:s");

$studentID = IntegerSafe($_POST['studentID']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$academicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$resultAry = array();

# retrieve original email
$sql = "select UserEmail from INTRANET_USER where UserID='$studentID'";
$result = $lsr->returnVector($sql);
$ori_UserEmail = $result[0];

# INTRANET_USER
$userAry = array();
if(!$_SESSION['ParentFillOnlineReg'])
{
	$userAry['EnglishName'] = $english_name;
	$userAry['ChineseName'] = $chinese_name;
}
$userAry['DateOfBirth'] = $date_of_birth;
$userAry['Gender'] = $gender;
$userAry['HKID'] = $passport_type=="04" ? $id_no : "";
$userAry['HomeTelNo'] = $home_tel_no;
$userAry['MobileTelNo'] = $mobile_no;
$userAry['UserEmail'] = $student_email;
$userAry['FirstName'] = $first_name;
$userAry['LastName'] = $last_name;
$userAry['CFirstName'] = $cfirst_name;
$userAry['CLastName'] = $clast_name;
$lsr->updateStudentInfo($userAry, $studentID);

# INTRANET_USER_PERSONAL_SETTINGS
//$psAry['Nationality'] = $nationality;
$psAry['Nationality'] = $nation_code ? $lsr->returnPresetCodeName("NATIONALITY", $nation_code) : $nationality;
$psAry['PassportNo'] = $passport_type=="02" ? $id_no : "";
$lps->Save_Setting($studentID, $psAry);

# STUDENT_REGISTRY_STUDENT
$stuAry['CHN_COMMERCIAL_CODE1'] = $chn_commercial_code1;
$stuAry['CHN_COMMERCIAL_CODE2'] = $chn_commercial_code2;
$stuAry['CHN_COMMERCIAL_CODE3'] = $chn_commercial_code3;
$stuAry['CHN_COMMERCIAL_CODE4'] = $chn_commercial_code4;
$stuAry['ORIGIN'] = $origin;
$stuAry['ID_TYPE'] = $passport_type;
$stuAry['ID_NO'] = $id_no;
$stuAry['ETHNICITY_CODE'] = $ethnicity;
// $stuAry['ETHNICITY_OTHERS'] = $ethnicity==2 ? $ethnicity_others : "";
$stuAry['FAMILY_LANG'] = $family_lang;
$stuAry['FAMILY_LANG_OTHERS'] = $family_lang==0 ? $family_lang_others : "";
$stuAry['RELIGION'] = $religion;
$stuAry['RELIGION_OTHERS'] = $religion==0 ? $religion_others : "";
$stuAry['CHURCH'] = $church;
$stuAry['ADDRESS_ROOM_EN'] = $address_room_en;
$stuAry['ADDRESS_FLOOR_EN'] = $address_floor_en;
$stuAry['ADDRESS_BLK_EN'] = $address_blk_en;
$stuAry['ADDRESS_BLD_EN'] = $address_bld_en;
$stuAry['ADDRESS_EST_EN'] = $address_est_en;
$stuAry['ADDRESS_STR_EN'] = $address_str_en;
//$stuAry['ADDRESS_DISTRICT_EN'] = $address_district_en;
$stuAry['DISTRICT_CODE'] = $student_district_code;
$stuAry['ADDRESS_AREA'] = $address_area;
$stuAry['LAST_SCHOOL_EN'] = $last_school_en;
$stuAry['LAST_SCHOOL_CH'] = $last_school_ch;
$stuAry['NATION_CODE'] = $nation_code;
if($confirm_flag==1)
{
	$stuAry['DATA_CONFIRM_DATE'] = $cur_datetime;       // void after 2019-08-15
	$stuAry['DATA_CONFIRM_BY'] = $UserID;               // void after 2019-08-15
}
$lsr->updateStudentRegistry_HK($stuAry, $studentID);

if($confirm_flag==1) {
    $confirmDataAry = array();
    $condAry = array();
    if ($lsr->isSubmitted($studentID, $academicYearID)) {
        $confirmDataAry['DATA_CONFIRM_DATE'] = "now()";
        $confirmDataAry['DATA_CONFIRM_BY'] = $_SESSION['UserID'];

        $condAry['UserID'] = $studentID;
        $condAry['AcademicYearID'] = $academicYearID;
        $resultAry['UpdateConfirmRegistry'] = $lsr->update2Table('STUDENT_REGISTRY_SUBMITTED', $confirmDataAry, $condAry, $run=true, $updateDateModified=false);
    }
    else {
        $confirmDataAry['UserID'] = $studentID;
        $confirmDataAry['AcademicYearID'] = $academicYearID;
        $confirmDataAry['DATA_CONFIRM_DATE'] = "now()";
        $confirmDataAry['DATA_CONFIRM_BY'] = $_SESSION['UserID'];
        $resultAry['InsertConfirmRegistry'] = $lsr->insert2Table('STUDENT_REGISTRY_SUBMITTED', $confirmDataAry, $condAry, $run=true, $insertIgnore = false, $updateDateModified=false);
    }
    unset($confirmDataAry);
    unset($condAry);
}

/*
# LAST ACADEMIC YEAR INFO
$lfcm = new form_class_manage();
$academicAry = array();
$last_academic_year = $lfcm->Get_Academic_Year_List('', $OrderBySequence, $excludeYearIDArr="", $noPastYear="", $pastAndCurrentYearOnly=1, $excludeCurrentYear=1);
for($i=0;$i<sizeof($last_academic_year);$i++)
{
	$thisAcademicYearID = $last_academic_year[$i]['AcademicYearID'];
	if($thisAcademicYearID=="")	break;
	
	$this_ClassName = ${"ClassName_".$thisAcademicYearID};
	$this_ClassNumber = ${"ClassNumber_".$thisAcademicYearID};
	if($this_ClassName && $this_ClassNumber)
	{
		$academicAry[$thisAcademicYearID]['ClassName'] = $this_ClassName;
		$academicAry[$thisAcademicYearID]['ClassNumber'] = $this_ClassNumber;
	}
}
$lsr->updateStudentLastClassInfo_HK($studentID,$academicAry);
*/

# STUDENT_REGISTRY_BROSIS
$bsAry = array();
for($i=0;$i<3;$i++)
{
	if(${"BS_UserLogin_".$i})
	{
		$bsAry[$i]['BS_UserLogin'] = ${"BS_UserLogin_".$i};
		$bsAry[$i]['BS_Name'] = ${"BS_Name_".$i};
		$bsAry[$i]['Relationship'] = ${"BS_Relationship_".$i};
	}
}
$lsr->updateStudentBroSisInfo_HK($studentID,$bsAry);

# STUDENT_REGISTRY_PG
for($g=1;$g<=2;$g++)
{
	$gAry['SEQUENCE'] = $g;
	$gAry['PG_TYPE'] = ${"guardian_type".$g};
	$gAry['PG_TYPE_OTHERS'] = ${"guardian_type".$g}==0 ? ${"guardian_type_others".$g} : "";
	$gAry['EMAIL'] = ${"guardian_email".$g};
	$gAry['NAME_E'] = ${"guardian_english_name".$g};
	$gAry['NAME_C'] = ${"guardian_chinese_name".$g};
// 	$gAry['HKID'] = ${"guardian_hkid".$g};
	$gAry['JOB_TITLE_OTHERS'] = ${"guardian_occupation".$g};
	$gAry['TEL'] = ${"guardian_tel".$g};
	$gAry['MOBILE'] = ${"guardian_mobile".$g};
	$gAry['RELIGION'] = ${"guardian_religion".$g};
// 	$gAry['RELIGION_OTHERS'] = ${"guardian_religion".$g}==0 ? ${"guardian_religion".$g."_others"} : "";
	$gAry['CHURCH'] = ${"guardian_church".$g};
	
	//debug_pr(${"gSameAsStudent".$g});
	$gAry['ADDRESS_ROOM_EN'] = ${"gSameAsStudent".$g} ? "" : ${"address_room_en".$g};
	$gAry['ADDRESS_FLOOR_EN'] = ${"gSameAsStudent".$g} ? "" : ${"address_floor_en".$g};
	$gAry['ADDRESS_BLK_EN'] = ${"gSameAsStudent".$g} ? "" : ${"address_blk_en".$g};
	$gAry['ADDRESS_BLD_EN'] = ${"gSameAsStudent".$g} ? "" : ${"address_bld_en".$g};
	$gAry['ADDRESS_EST_EN'] = ${"gSameAsStudent".$g} ? "" : ${"address_est_en".$g};
	$gAry['ADDRESS_STR_EN'] = ${"gSameAsStudent".$g} ? "" : ${"address_str_en".$g};
	//$gAry['ADDRESS_DISTRICT_EN'] = ${"address_district_en".$g};
	$gAry['DISTRICT_CODE'] = ${"guardian_district".$g};
	$gAry['ADDRESS_AREA'] = ${"guardian_address_area".$g};
	$lsr->updateStudentRegistry_HK(array(), $studentID, $gAry);
}

# STUDENT_REGISTRY_EMERGENCY_CONTACT
/*
$eContactAry['Contact1'] = $emergency_contact1;
$eContactAry['Contact1_Others'] = $emergency_contact1==0 ? $emergency_contact1_others : "";
$eContactAry['Contact2'] = $emergency_contact2;
$eContactAry['Contact2_Others'] = $emergency_contact2==0 ? $emergency_contact2_others : "";
*/
$eContactAry['Contact3_Title'] = $emergency_contact3_title;
$eContactAry['Contact3_EnglishName'] = $emergency_contact3_english_name;
$eContactAry['Contact3_ChineseName'] = $emergency_contact3_chinese_name;
$eContactAry['Contact3_Relationship'] = $emergency_contact3_relationship;
$eContactAry['Contact3_Phone'] = $emergency_contact3_phone;
$eContactAry['Contact3_Mobile'] = $emergency_contact3_mobile;
$lsr->updateStudentRegistry_HK(array(), $studentID, array(), $eContactAry);


# update eClass40 data
$dataAry = array();
if(!$_SESSION['ParentFillOnlineReg'])
{
	$dataAry['EnglishName'] = $english_name;
	$dataAry['ChineseName'] = $chinese_name;
}
$dataAry['Gender'] = $gender;
$dataAry['DateOfBirth'] = $date_of_birth;
$dataAry['HomeTelNo'] = $home_tel_no;
$le = new libeclass();
$le->eClass40UserUpdateInfo($ori_UserEmail, $dataAry, $student_email);

# update custom column values 
$custCol = $_POST['custCol'];
if (count($custCol)) {
    $custValues = array();
    foreach ((array)$custCol as $code => $colId) {
        $custValues[] = array(
            "ColumnID" => $colId,
            "value" => $_POST[$code]
        );
    }
    $lsr->updateCustomValue($studentID, $custValues);
}

intranet_closedb();
header("Location: view.php?xmsg=UpdateSuccess&AcademicYearID=$academicYearID&studentID=$studentID");
?>