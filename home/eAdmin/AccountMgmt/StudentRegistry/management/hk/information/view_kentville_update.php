<?php
/*
 *  Using:
 *
 *  2020-04-16 Cameron
 *      - don't update EnglishName & HKID, left them to be done in synchronization 
 *      
 *  2020-04-06 Cameron
 *      - synchronize student info to current account [case #Y173401]
 *      
 *  2019-08-15 Cameron
 *      - add/update confirm data to STUDENT_REGISTRY_SUBMITTED so that the confirm status is by academic year [case #Y164458]
 * 
 *  2019-03-20 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
//include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
//$lps = new libuserpersonalsettings();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$_SESSION['ParentFillOnlineReg'])) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry_kv();
//$lfcm = new form_class_manage();

# current date/time
$cur_datetime = Date("Y-m-d H:i:s");

$studentID = IntegerSafe($_POST['studentID']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$academicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$isSynchronize = IntegerSafe($_POST['IsSynchronize']);

# retrieve original email
$result = $lsr->getStudentKeyInfo($studentID);
$ori_UserEmail = $result['UserEmail'];
$userLogin = $result['UserLogin'];

$resultAry = array();

# INTRANET_USER
$userAry = array();
// if(!$_SESSION['ParentFillOnlineReg'])
// {
//     $userAry['EnglishName'] = standardizeFormPostValue($_POST['english_name']);
// }

$userAry['ChineseName'] = standardizeFormPostValue($_POST['chinese_name']);
$userAry['DateOfBirth'] = standardizeFormPostValue($_POST['date_of_birth']);

$place_of_birth_code = standardizeFormPostValue($_POST['place_of_birth']);
if (!empty($place_of_birth_code)) {
    $placeOfBirth = $lsr->getPresetCodeEnglishName('PLACEOFBIRTH',$place_of_birth_code);
    $userAry['PlaceOfBirth'] = $placeOfBirth;
    if ($place_of_birth_code == '999') {
        $bplace_others = standardizeFormPostValue($_POST['bplace_others']);
        $userAry['PlaceOfBirth'] = $bplace_others;
    }
    else {
        $bplace_others = '';
    }
}
else {
    $bplace_others = '';
}

$userAry['Gender'] = standardizeFormPostValue($_POST['gender']);
//$userAry['HKID'] = standardizeFormPostValue($_POST['id_no']);                     // left this for sync action
$userAry['HomeTelNo'] = standardizeFormPostValue($_POST['home_tel_no']);
$userAry['FirstName'] = standardizeFormPostValue($_POST['first_name']);
$userAry['LastName'] = standardizeFormPostValue($_POST['last_name']);
//$userAry['EnglishName'] = $userAry['LastName'] . ' ' . $userAry['FirstName'];     // left this for sync action
$userAry['ModifyBy'] = $_SESSION['UserID'];

$condAry = array();
$condAry['UserID'] = $studentID;
$resultAry['UpdateIntranet'] = $lsr->update2Table('INTRANET_USER', $userAry, $condAry);


# INTRANET_USER_PERSONAL_SETTINGS
// $psAry['Nationality'] = $nation_code ? $lsr->returnPresetCodeName("NATIONALITY", $nation_code) : $nationality;
// $lps->Save_Setting($studentID, $psAry);

# STUDENT_REGISTRY_STUDENT
$stuAry = array();
$stuAry['ID_NO'] = standardizeFormPostValue($_POST['id_no']);
$stuAry['B_PLACE_CODE'] = $place_of_birth_code;
$stuAry['B_PLACE_OTHERS'] = $bplace_others;
$nation_code = standardizeFormPostValue($_POST['nation_code']);
$stuAry['NATION_CODE'] = $nation_code;
$stuAry['NATION_OTHERS'] = $nation_code == '999' ? standardizeFormPostValue($_POST['nation_others']) : '';
$ethnicity_code = standardizeFormPostValue($_POST['ethnicity']);
$stuAry['ETHNICITY_CODE'] = $ethnicity_code;
$stuAry['RACE_OTHERS'] = $ethnicity_code == '999' ? standardizeFormPostValue($_POST['race_others']) : '';
$family_lang = standardizeFormPostValue($_POST['family_lang']);
$stuAry['FAMILY_LANG'] = $family_lang;
$stuAry['FAMILY_LANG_OTHERS'] = $family_lang == '999' ? standardizeFormPostValue($_POST['family_lang_others']) : '';
$stuAry['ADDRESS_ROOM_EN'] = standardizeFormPostValue($_POST['address_room_en']);
$stuAry['ADDRESS_FLOOR_EN'] = standardizeFormPostValue($_POST['address_floor_en']);
$stuAry['ADDRESS_BLK_EN'] = standardizeFormPostValue($_POST['address_blk_en']);
$stuAry['ADDRESS_BLD_EN'] = standardizeFormPostValue($_POST['address_bld_en']);
$stuAry['ADDRESS_EST_EN'] = standardizeFormPostValue($_POST['address_est_en']);
$stuAry['ADDRESS_STR_EN'] = standardizeFormPostValue($_POST['address_str_en']);
$district_code = standardizeFormPostValue($_POST['student_district_code']);
$stuAry['DISTRICT_CODE'] = $district_code;
$stuAry['ADDRESS_DISTRICT_EN'] = $district_code == '999' ? standardizeFormPostValue($_POST['district_others']) : '';
$stuAry['ADDRESS_AREA'] = standardizeFormPostValue($_POST['address_area']);
$stuAry['ADDRESS_STR_CH'] = standardizeFormPostValue($_POST['chi_address']);        // one field for Chinese address
$stuAry['NANNY_BUS'] = standardizeFormPostValue($_POST['nanny_bus']);
$stuAry['LAST_SCHOOL_EN'] = standardizeFormPostValue($_POST['last_school_en']);
$stuAry['ModifyBy'] = $_SESSION['UserID'];

if($confirm_flag==1)
{
	$stuAry['DATA_CONFIRM_DATE'] = $cur_datetime; 
	$stuAry['DATA_CONFIRM_BY'] =  $_SESSION['UserID']; 
}

$isStudentRegistryExist = $lsr->isStudentRegistryExist($studentID);
if ($isStudentRegistryExist) {
    $condAry = array();
    $condAry['UserID'] = $studentID;
    $resultAry['UpdateStudentRegistry'] = $lsr->update2Table('STUDENT_REGISTRY_STUDENT', $stuAry, $condAry);
}
else {
    $stuAry['UserID'] = $studentID;
    $stuAry['InputBy'] = $_SESSION['UserID'];
    $stuAry['DateInput'] = 'now()';
    $resultAry['InsertStudentRegistry'] = $lsr->insert2Table('STUDENT_REGISTRY_STUDENT', $stuAry);
}

if($confirm_flag==1) {
    $confirmDataAry = array();
    $condAry = array();
    if ($lsr->isSubmitted($studentID, $academicYearID)) {
        $confirmDataAry['DATA_CONFIRM_DATE'] = "now()";
        $confirmDataAry['DATA_CONFIRM_BY'] = $_SESSION['UserID'];

        $condAry['UserID'] = $studentID;
        $condAry['AcademicYearID'] = $academicYearID;
        $resultAry['UpdateConfirmRegistry'] = $lsr->update2Table('STUDENT_REGISTRY_SUBMITTED', $confirmDataAry, $condAry, $run=true, $updateDateModified=false);
    }
    else {
        $confirmDataAry['UserID'] = $studentID;
        $confirmDataAry['AcademicYearID'] = $academicYearID;
        $confirmDataAry['DATA_CONFIRM_DATE'] = "now()";
        $confirmDataAry['DATA_CONFIRM_BY'] = $_SESSION['UserID'];
        $resultAry['InsertConfirmRegistry'] = $lsr->insert2Table('STUDENT_REGISTRY_SUBMITTED', $confirmDataAry, $condAry, $run=true, $insertIgnore = false, $updateDateModified=false);
    }
    unset($confirmDataAry);
    unset($condAry);
}


# STUDENT_REGISTRY_BROSIS
//$bs_row_count = IntegerSafe($_POST['bs_row_count']);
$maxSiblings = 3;
$resultAry['RemoveSiblings'] = $lsr->removeSiblings($studentID);

for($i=0;$i<$maxSiblings;$i++)
{
    $bsAry = array();
    $_bsRelationship = standardizeFormPostValue($_POST["BS_Relationship_$i"]);
    $_bsDateOfBirth = standardizeFormPostValue($_POST["BS_DateOfBirth_$i"]);
    $_bsName = standardizeFormPostValue($_POST["BS_Name_$i"]);
    $_bsIsSchoolStudentOrAlumni= standardizeFormPostValue($_POST["BS_IsSchoolStudentOrAlumni_$i"]);
    $_bsSchoolClass = standardizeFormPostValue($_POST["BS_SchoolClass_$i"]);
    $_bsGraduateYear = standardizeFormPostValue($_POST["BS_GraduateYear_$i"]);
    $_bsCurSchool = standardizeFormPostValue($_POST["BS_CurSchool_$i"]);
    $_bsCurLevel = standardizeFormPostValue($_POST["BS_CurLevel_$i"]);
    $_bsCurLevelOthers = standardizeFormPostValue($_POST["BS_CurLevelOthers_$i"]);
    
    if ($_bsRelationship || $_bsName || $_bsDateOfBirth || $_bsSchoolClass || $_bsGraduateYear || $_bsCurSchool || $_bsCurLevel) {
        $bsAry['StudentID'] = $studentID;
        $bsAry['Relationship'] = $_bsRelationship;
        $bsAry['DateOfBirth'] = $_bsDateOfBirth;
        $bsAry['StudentName'] = $_bsName;
        $bsAry['IsSchoolStudentOrAlumni'] = $_bsIsSchoolStudentOrAlumni;
        if ($_bsIsSchoolStudentOrAlumni) {
            $bsAry['SchoolClass'] = $_bsSchoolClass;
            $bsAry['GraduateYear'] = $_bsGraduateYear;
        }
        else {
            $bsAry['SchoolClass'] = '';
            $bsAry['GraduateYear'] = '';
        }
        $bsAry['CurSchool'] = $_bsCurSchool;
        $bsAry['CurLevel'] = $_bsCurLevel;
        $bsAry['CurLevelOthers'] = $_bsCurLevel == '999' ? $_bsCurLevelOthers : '';
// debug_pr($bsAry);
// exit;
        $resultAry['InsertBrotherNSister_'.$i] = $lsr->insert2Table('STUDENT_REGISTRY_BROSIS', $bsAry, $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = false);
    }
}
// debug_pr($bsAry);
// exit;


# STUDENT_REGISTRY_PG
for($g=1;$g<=2;$g++)
{
    $gAry = array();
	$gAry['SEQUENCE'] = $g;
	$gAry['PG_TYPE'] = ($g==1) ? 'F' : 'M';
	$gAry['FIRST_NAME_E'] = standardizeFormPostValue($_POST["guardian_english_first_name$g"]);
	$gAry['LAST_NAME_E'] = standardizeFormPostValue($_POST["guardian_english_last_name$g"]);
	$gAry['NAME_E'] = $gAry['LAST_NAME_E'] . ' ' . $gAry['FIRST_NAME_E'];
	$gAry['NAME_C'] = standardizeFormPostValue($_POST["guardian_chinese_name$g"]);
	$gAry['BUSINESS_NAME'] = standardizeFormPostValue($_POST["guardian_business_name$g"]);
	$gAry['EMAIL'] = standardizeFormPostValue($_POST["guardian_email$g"]);
 	$gAry['JOB_TITLE'] = standardizeFormPostValue($_POST["guardian_job_title$g"]);
	$gAry['JOB_NATURE'] = standardizeFormPostValue($_POST["guardian_occupation$g"]);
	$gAry['IS_ALUMNI'] = standardizeFormPostValue($_POST["isAlumni_$g"]);
	$gAry['GRADUATE_YEAR'] = standardizeFormPostValue($_POST["graduateYear_$g"]);
	$gAry['TEL'] = standardizeFormPostValue($_POST["guardian_tel$g"]);
	$gAry['MOBILE'] = standardizeFormPostValue($_POST["guardian_mobile$g"]);
	$gAry['ADDRESS_ROOM_EN'] = standardizeFormPostValue($_POST["address_room_en$g"]);
	$gAry['ADDRESS_FLOOR_EN'] = standardizeFormPostValue($_POST["address_floor_en$g"]);
	$gAry['ADDRESS_BLK_EN'] = standardizeFormPostValue($_POST["address_blk_en$g"]);
	$gAry['ADDRESS_BLD_EN'] = standardizeFormPostValue($_POST["address_bld_en$g"]);
	$gAry['ADDRESS_EST_EN'] = standardizeFormPostValue($_POST["address_est_en$g"]);
	$gAry['ADDRESS_STR_EN'] = standardizeFormPostValue($_POST["address_str_en$g"]);
	
	$district_code = standardizeFormPostValue($_POST["guardian_district$g"]);
	$gAry['DISTRICT_CODE'] = $district_code;
	$gAry['ADDRESS_DISTRICT_EN'] = $district_code == '999' ? standardizeFormPostValue($_POST["district_others$g"]) : '';
	$gAry['ADDRESS_AREA'] = standardizeFormPostValue($_POST["guardian_address_area$g"]);
	$gAry['ModifyBy'] = $_SESSION['UserID'];
	
	$isParentGuardianExist = $lsr->isParentGuardianExist($studentID, $g);
	if ($isParentGuardianExist) {
	    $condAry = array();
	    $condAry['StudentID'] = $studentID;
	    $condAry['SEQUENCE'] = $g;
	    $resultAry['UpdateParentGuardian_'.$g] = $lsr->update2Table('STUDENT_REGISTRY_PG', $gAry, $condAry);
	}
	else {
	    $gAry['StudentID'] = $studentID;
	    $gAry['InputBy'] = $_SESSION['UserID'];
	    $gAry['DateInput'] = 'now()';
	    $resultAry['InsertParentGuardian_'.$g] = $lsr->insert2Table('STUDENT_REGISTRY_PG', $gAry);
	}
}

# STUDENT_REGISTRY_EMERGENCY_CONTACT
for($c=1;$c<=2;$c++)
{
    $eContactAry= array();
    $eContactAry['Sequence'] = $c;
    $eContactAry['Contact3_EglishFirstName'] = standardizeFormPostValue($_POST["contact_english_first_name$c"]);
    $eContactAry['Contact3_EglishLastName'] = standardizeFormPostValue($_POST["contact_english_last_name$c"]);
    $eContactAry['Contact3_EnglishName'] = $eContactAry['Contact3_EglishLastName'] . ' ' . $eContactAry['Contact3_EglishFirstName'];
    $eContactAry['Contact3_ChineseName'] = standardizeFormPostValue($_POST["contact_chinese_name$c"]);
    $contact_relationship_code = standardizeFormPostValue($_POST["contact_relationship$c"]);
    $eContactAry['Contact3_RelationshipCode'] = $contact_relationship_code;
    $eContactAry['Contact3_Relationship'] = $contact_relationship_code == '999' ? standardizeFormPostValue($_POST["contact_relationship_others$c"]) : '';
    $eContactAry['Contact3_Mobile'] = standardizeFormPostValue($_POST["contact_mobile$c"]);
    $eContactAry['ModifyBy'] = $_SESSION['UserID'];
    
    $isEmergencyContactExist = $lsr->isEmergencyContactExist($studentID, $c, $setSequence=true);
    if ($isEmergencyContactExist) {
        $condAry = array();
        $condAry['StudentID'] = $studentID;
        $condAry['Sequence'] = $c;
        $resultAry['UpdateEmergencyContact_'.$c] = $lsr->update2Table('STUDENT_REGISTRY_EMERGENCY_CONTACT', $eContactAry, $condAry);
    }
    else {
        $eContactAry['StudentID'] = $studentID;
        $eContactAry['InputBy'] = $_SESSION['UserID'];
        $eContactAry['DateInput'] = 'now()';
        $resultAry['InsertEmergencyContact_'.$c] = $lsr->insert2Table('STUDENT_REGISTRY_EMERGENCY_CONTACT', $eContactAry);
    }
}


# update eClass40 data
$dataAry = array();
if(!$_SESSION['ParentFillOnlineReg'])
{
	$dataAry['EnglishName'] = $english_name;
	$dataAry['ChineseName'] = $chinese_name;
}
$dataAry['Gender'] = $gender;
$dataAry['DateOfBirth'] = $date_of_birth;
$dataAry['HomeTelNo'] = $home_tel_no;
$student_email = $ori_UserEmail;
$le = new libeclass();
$le->eClass40UserUpdateInfo($ori_UserEmail, $dataAry, $student_email);


# synchronize student info to current account
if(!$_SESSION['ParentFillOnlineReg'] && $isSynchronize) {
    $resultAry['SynchronizeAccountInfo'] = $lsr->syncStudentRegistryToAccount(array($studentID));
}

# Update photo
################################################
# Official Photo
################################################
// $re_path = "/file/user_photo";
// $path = "$intranet_root$re_path";
// $target = "";
// if($photo == "none" || $photo == "") {
//     // do nothing
// }
// else
// {
//     $lf = new libfilesystem();
//     if (!is_dir($path))
//     {
//         $lf->folder_new($path);
//     }
    
//     $ext = strtolower($lf->file_ext($photo_name));
//     if ($ext == ".jpg")
//     {
//         $target = $path ."/". $userLogin . $ext;
//         $re_path .= "/". $userLogin. $ext;
//         $lf->lfs_copy($photo, $target);
        
//         # Check need to resize or not
//         include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
//         $im = new SimpleImage();
//         if($im->GDLib_ava)
//         {
//             $im->load($target);
//             $w = $im->getWidth();
//             $h = $im->getHeight();
//             $default_photo_width = $default_photo_width ? $default_photo_width : 100;
//             $default_photo_height = $default_photo_height ? $default_photo_height : 130;
//             if($w > $default_photo_width || $h > $default_photo_height)
//             {
//                 $im->resize($default_photo_width,$default_photo_height);
//                 $im->save($target);
//             }
//         }
//     }
//}
################################################
# Official Photo [End]
################################################


intranet_closedb();
header("Location: view.php?xmsg=UpdateSuccess&AcademicYearID=$academicYearID&studentID=$studentID");
?>