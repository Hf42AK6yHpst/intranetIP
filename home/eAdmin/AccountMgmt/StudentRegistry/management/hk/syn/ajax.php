<?
/*
 * Log
 *
 * Description: output json format data
 *
 *  2019-10-29 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");


$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] ) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}


intranet_auth();
intranet_opendb();

$characterset = 'utf-8';

header('Content-Type: text/html; charset=' . $characterset);

$lsr = new libstudentregistry_kv();

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = ($junior_mck) ? false : true; // whether to remove new line, carriage return, tab and back slash

switch ($action) {
    case 'getStudentNameByClass':
        $ClassName = $_POST['ClassName'];
        if (!empty($ClassName)) {
            $data = $lsr->getStudentNameListWClassNumberByClassName($ClassName);
        }
        else {
            $data = array();
        }
        $x = getSelectByArray($data, "name='StudentID' id='StudentID'");
        $json['success'] = true;
        break;
}

if ($remove_dummy_chars) {
    $x = remove_dummy_chars_for_json($x);
}

$json['html'] = $x;

echo $ljson->encode($json);

intranet_closedb();
?>