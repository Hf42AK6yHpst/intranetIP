<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();
$linterface = new interface_html();

$templateArr = $lsr->TemplateInfoByCategoryID($newstatus);

if(is_array($templateArr))
{
	$a = 0;
	foreach($templateArr as $Key=>$Value)
	{
		$catArr[$a] = array($Key,$Value);
		$a++;
	}
}

$templateArr = array_merge(array(array("0",$i_alert_pleaseselect)), $templateArr);

$templateSelection = getSelectByArray($templateArr, ' name="TemplateID" id="TemplateID"', $TemplateID, "", 0, "-- ".$i_Discipline_System_Discipline_Template_Name." --");

intranet_closedb();

$data = $templateSelection."<br>".$i_Discipline_Additional_Info." : ".$linterface->GET_TEXTAREA('TextAdditionalInfo', "");

echo $data;
?>