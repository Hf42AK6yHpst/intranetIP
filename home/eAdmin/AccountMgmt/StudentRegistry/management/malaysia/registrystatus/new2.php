<?php
#using : henry chow

############# Change Log [Start]
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!is_array($userID) || $userID[0]=="") {
	header("Location: new1.php");
	exit;	
}

$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryStatus";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['StudentRegistryStatus'],);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$stdInfo = $lsr->RETRIEVE_STUDENT_INFO_ADV_MAL($userID[0]);
//debug_pr($_POST);
# Gender
$gender_ary      = $lsr -> GENDER_ARY();
# Nationality
$nation_ary      = $lsr -> MAL_NATION_DATA_ARY();

#Past Enrollment Information
$Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($userID[0]);
if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}

$guardian = $lsr->getParentGuardianInfo($userID[0], "G");
switch($guardian['GUARD']) {
	case "F" : $guardian_gender = $ec_guardian['01']; break;
	case "M" : $guardian_gender = $ec_guardian['02']; break;
	case "O" : $guardian_gender = $guardian['G_RELATION']; break;
}

# step information
switch($newstatus) {
	case 1 : $applyFor = $Lang['StudentRegistry']['StatusLeft']; $navigation = $button_new." ".$Lang['StudentRegistry']['StatusLeft']; $step2 = $Lang['StudentRegistry']['InputLeftInfo']; break;
	case 2 : $applyFor = $Lang['StudentRegistry']['StatusSuspended']; $navigation = $button_new." ".$Lang['StudentRegistry']['StatusSuspended']; $step2 = $Lang['StudentRegistry']['InputSuspendInfo']; break;
	case 3 : $applyFor = $Lang['StudentRegistry']['StatusResume']; $navigation = $button_new." ".$Lang['StudentRegistry']['StatusResume']; $step2 = $Lang['StudentRegistry']['InputResumeInfo']; break;
	default : $newstatus=STUDENT_REGISTRY_RECORDSTATUS_LEFT; $navigation = $button_new." ".$Lang['StudentRegistry']['StatusLeft']; $step2 = $Lang['StudentRegistry']['InputLeftInfo']; break;
}

$history = $lsr->getRegistryStatusHistory($userID[0]);

$i = 0;
foreach($Lang['StudentRegistry']['ReasonAry'] as $id=>$val) {
	$reasonAry[$i][] = $id;
	$reasonAry[$i][] = $val;
	$i++;
}
$reasonSelect = getSelectByArray($reasonAry, ' name="reason" id="reason" onChange="displayOtherReason(this.value);"', $reason, 0, 0);

$STEPS_OBJ[] = array($Lang['StudentRegistry']['SelectStudent'], 0);
$STEPS_OBJ[] = array($step2, 1);
$STEPS_OBJ[] = array($Lang['StudentRegistry']['PrintNotice'], 0);

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($navigation, "");

$linterface->LAYOUT_START();
echo $lsrUI->Include_JS_CSS();

?>
<script language="javascript">
<!--
function checkForm() {
	if(document.getElementById('applyDate').value=="") {
		alert("<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ApplyDate']?>");
		document.getElementById('applyDate').focus();
		return false;
	}
	if(!check_date(document.getElementById('applyDate'),'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) {
		document.getElementById('applyDate').focus();
		return false;
	}
	<? if($newstatus!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) { ?>
	if(document.getElementById('reason').value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['Reason']?>");
		document.getElementById('reason').focus();
		return false;		
	}
	if(document.getElementById('reason').value==0 && document.getElementById('otherReason').value=="") {
		alert("<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['Reason']?>");
		document.getElementById('otherReason').focus();
		return false;		
	}
	<? } ?>
	if(document.getElementById('printNotice').checked==true && document.getElementById('TemplateID').value=="0") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['eNoticeTemplate']?>");
		document.getElementById('TemplateID').focus();
		return false;
	}
	goCheckDate("document.form1");

}

	function goCheckDate(obj)
	{
		obj = eval(obj);
		$.post(
				'ajax_check_date.php',
				{
					userID : obj.userID.value,
					applyDate 	: obj.applyDate.value
				},
				function(data){
					if(data=="")
					{
						$('#ErrorLog').html('');
						obj.action = "new_update.php";
						obj.submit();
					}
					else
					{
						$('#ErrorLog').html(data);
					}
				});
	}
	
function displayOtherReason(val) {
	if(val==0) {
		document.getElementById('spanOtherReason').style.display = "inline";
		document.getElementById('otherReason').focus();
	} else {
		document.getElementById('spanOtherReason').style.display = "none";
	}
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}

function checkDisplayTemplate(checkFlag, layername) {
	if(checkFlag==true) {
		//Get_Template_Menu();
		show_template_list(layername);
	} else {
		document.getElementById(layername).style.display = "none";	
		document.getElementById('ref_list').innerHTML = "";
	}
}

var ClickID = '';
var callback_show_template_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_template_list(click)
{
	ClickID = click;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_get_template_menu.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_template_list);
}

function getPosition(obj, direction)
{

	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top = getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility = 'visible';
}

function goBack() {
	document.form1.action = "new1.php";
	document.form1.submit();
	
}
-->
</script>
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<div class="navigation">
    			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</div>
			<br style="clear:both" />
			<div class="table_board">
				<div>      
                        <?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>
                        <br style="clear:both" />
                </div>

			</div>
              <div class="table_board">
        			  <div class="form_sub_title_v30"><em>- <span class="field_title"><?=$Lang['StudentRegistry']['StudInfo']?></span> -</em>
                          <p class="spacer"></p>
      			    </div>
        			  <table class="form_table_v30">
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['StudentNo']?></td>
                          <td ><?=$stdInfo[0]['STUD_ID']?></td>
                          <td >&nbsp;</td>
                          <td >&nbsp;</td>
                          <td >&nbsp;</td>
                          <td >&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$i_UserChineseName?></td>
                          <td ><?=$stdInfo[0]['ChineseName']?></td>
                          <td class="field_title_short"><?=$i_UserEnglishName?></td>
                          <td colspan="3" ><?=$stdInfo[0]['EnglishName']?></td>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['Gender']?></td>
                          <td ><?=($stdInfo[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $stdInfo[0]['Gender']) : "--")?></td>
                          <td class="field_title_short"><span class="field_title"><?=$Lang['StudentRegistry']['Nationality']?></span></td>
                          <td ><?= ($stdInfo[0]['NATION']? $lsr -> RETRIEVE_DATA_ARY($nation_ary, $stdInfo[0]['NATION']) : "--")?></td>
                          <td >&nbsp;</td>
                          <td >&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['HomePhone']?></td>
                          <td colspan="5" ><?=$stdInfo[0]['HomeTelNo']?></td>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['Class']?></td>
                          <td ><?=$stdInfo[0]['ClassName']?></td>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['ClassNumber']?></td>
                          <td ><?=$stdInfo[0]['ClassNumber']?></td>
                          <td >&nbsp;</td>
                          <td >&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['PastEnroll']?></td>
                          <td colspan="5" >
                          	<div class="form_field_sub_content">
                          		<table> 
                          			<?=$Past_Enroll_html?>
                          		</table> 
                          	</div>
                          </td>
                        </tr>
                        <col class="field_title_short" />
                        <col  class="field_c" />
                      </table>
					<div class="form_sub_title_v30">
					<em>- <span class="field_title"><?=$Lang['StudentRegistry']['ContactPersonInfo']?> </span>-</em>
                          <p class="spacer"></p>
      			    </div>
        			  <table class="form_table_v30">
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <tr>
                          <td class="field_title_short"><?=$i_UserChineseName?></td>
                          <td ><?= ($guardian['NAME_C']!=""?$guardian['NAME_C']:"---")?></td>
                          <td class="field_title_short"><?=$i_UserEnglishName?></td>
                          <td colspan="3" ><?=($guardian['NAME_E']!=""?$guardian['NAME_E']:"---")?></td>
                        </tr>
                        <tr>
                          <td class="field_title_short"><span class="field_title"><?=$Lang['StudentRegistry']['Mobile']?></span></td>
                          <td ><?=($guardian['TEL']!=""?$guardian['TEL']:"---")?></td>
                          <td class="field_title_short"><span class="field_title"><?=$Lang['StudentRegistry']['RelationshipWithStudent']?></span></td>
                          <td ><?=($guardian_gender!=""?$guardian_gender:"---")?></td>
                          <td >&nbsp;</td>
                          <td >&nbsp;</td>
                        </tr>
                        <col class="field_title_short" />
                        <col  class="field_c" />
                      </table>
        			  <div class="form_sub_title_v30"><em>- <?=$Lang['StudentRegistry']['ChangeOfRegistryStatus']?> -</em>
                        <p class="spacer"></p>
      			    </div>
        			  <table class="form_table_v30">      			    
      			    <?
      			    
      			    for($i=0; $i<sizeof($history); $i++) { 
      			    	if($history[$i]['NoticeID']!="") 
      			    		$displayNotice = '<a href="javascript:newWindow(\'/home/eService/notice/sign.php?NoticeID='.$history[$i]['NoticeID'].'&StudentID='.$userID[0].'\',1)">['.$Lang['StudentRegistry']['View'].']</a>';
      			    	else 	
      			    		$displayNotice = "---";	
      			    ?>

                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['ApplyFor']?></td>
                          <td ><?=$history[$i]['status']?></td>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['ApplyDate']?></td>
                          <td ><?=$history[$i]['applyDate']?></td>
                          <? if($history[$i]['status']!=$Lang['StudentRegistry']['StatusResume']) { ?>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['Reason']?></td>
                          <td ><?=$history[$i]['reason']?></td>
                          <? }?>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['PrintNoticeToGuardian']?></td>
                          <td colspan="5" ><?=$displayNotice?></td>
                        </tr>
                        <col class="field_title_short" />
                        <col  class="field_c" />
        			  <? } ?>
                      </table>
        			  <br />
        			  <table class="form_table_v30">
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <col class="field_title_short" />
                        <col class="field_content_short" />
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['ApplyFor']?></td>
                          <td ><?=$applyFor?></td>
                          <td class="field_title_short"><span class="tabletextrequire">*</span><?=$Lang['StudentRegistry']['ApplyDate']?></td>
                          <td ><span class="row_content_v30">
                            <?=$linterface->GET_DATE_PICKER("applyDate",$applyDate)?><div id="ErrorLog"></div>
                            </span>
                          </td>
                          <? if($newstatus!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) {?>
                          <td class="field_title_short"><span class="tabletextrequire">*</span><?=$Lang['StudentRegistry']['Reason']?></td>
                          <td >
                          	<?=$reasonSelect?>
                          	<div id="spanOtherReason" style="display:none"><input type="text" name="otherReason" id="otherReason" value="" onBlur="this.value=trim(this.value)"></div>
                          </td>
                          <? } ?>
                        </tr>
                        <tr>
                          <td class="field_title_short"><?=$Lang['StudentRegistry']['PrintNoticeToGuardian']?></td>
                          <td colspan="5" >
                          	<input type="checkbox" name="printNotice" id="printNotice" value="1" onClick="checkDisplayTemplate(this.checked,'TemplateLayer')"/> <label for="printNotice"><?=$i_general_yes?>  </label>
                          	<span id="TemplateLayer" style="display:hidden"></span>
                          	<span id="ref_list"></span>
                          </td>
                        </tr>

                        <col class="field_title_short" />
                        <col  class="field_c" />
                      </table>
        			  <span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span>
					<p class="spacer"></p>
      			  </div>
        			<div class="edit_bottom">
        			  <p class="spacer"></p>
        			    <?=$linterface->GET_ACTION_BTN($button_back, "submit", "goBack()")?>
                        <?=$linterface->GET_ACTION_BTN($button_continue, "button", "document.getElementById('skipPrintNotice').value=0;checkForm()")?>
                        <?=$linterface->GET_ACTION_BTN($button_finish, "button", "document.getElementById('skipPrintNotice').value=1;checkForm()")?>
                        <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")?>
				</div>
				<p>&nbsp;</p>
		</td>
	</tr>
</table>
<input type="hidden" name="skipPrintNotice" id="skipPrintNotice" value ="0">
<input type="hidden" name="ClickID" id="ClickID" value ="0">
<input type="hidden" name="userID" id="userID" value ="<?=$userID[0]?>">
<input type="hidden" name="newstatus" id="newstatus" value ="<?=$newstatus?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value ="<?=$AcademicYearID?>">
<input type="hidden" name="studentno" id="studentno" value ="<?=$studentno?>">
<input type="hidden" name="studentname" id="studentname" value ="<?=$studentname?>">
<? if($newstatus!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) { ?>
<script language="javascript">
	document.getElementById('reason').selectedIndex = 0;
</script>
<? } ?>
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>