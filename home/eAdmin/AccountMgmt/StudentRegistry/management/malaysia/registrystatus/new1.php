<?php
#using : henry chow

############# Change Log [Start]
#
#	Date:   2010-07-27 Thomas
#			update the layout of Edit Button to IP25 standard
#			Add javascript function - 'checkEdit' to the Edit button
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();
$linterface = new interface_html();

$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearEN, $yearB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearB5, $yearEN);
}
$yearSelection = getSelectByArray($yearAry, ' name="AcademicYearID" id="AcademicYearID"', $AcademicYearID);


$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryStatus";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['StudentRegistryStatus'],);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# step information
switch($newstatus) {
	case 1 : $navigation = $button_new." ".$Lang['StudentRegistry']['StatusLeft']; $step2 = $Lang['StudentRegistry']['InputLeftInfo']; break;
	case 2 : $navigation = $button_new." ".$Lang['StudentRegistry']['StatusSuspended']; $step2 = $Lang['StudentRegistry']['InputSuspendInfo']; break;
	case 3 : $navigation = $button_new." ".$Lang['StudentRegistry']['StatusResume']; $step2 = $Lang['StudentRegistry']['InputResumeInfo']; break;
	default : $newstatus=STUDENT_REGISTRY_RECORDSTATUS_LEFT; $navigation = $button_new." ".$Lang['StudentRegistry']['StatusLeft']; $step2 = $Lang['StudentRegistry']['InputLeftInfo']; break;
}
$STEPS_OBJ[] = array($Lang['StudentRegistry']['SelectStudent'], 1);
$STEPS_OBJ[] = array($step2, 0);
$STEPS_OBJ[] = array($Lang['StudentRegistry']['PrintNotice'], 0);

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($navigation, "");

$linterface->LAYOUT_START();
echo $lsrUI->Include_JS_CSS();

?>

<script language="javascript">
function displaySpan(layerName) {
	if(document.getElementById('exportFlag').value==0) {
		document.getElementById(layerName).style.visibility = 'visible';
		document.getElementById('exportFlag').value = 1;
	} else {
		document.getElementById(layerName).style.visibility = 'hidden';
		document.getElementById('exportFlag').value = 0;
	}
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function displayExport() {
	displaySpan('export_option');	
}

function goExport(flag) {
	var str = "AcademicYearID=<?=$AcademicYearID?>&searchText="+document.getElementById('searchText').value+"&targetClass="+document.getElementById('targetClass').value+"&registryStatus="+document.getElementById('registryStatus').value;
	if(flag=="CSV") {
		self.location.href = "export_student_macau_csv.php?"+str;	
	} else if(flag=="XML") {
		self.location.href = "export_student_macau_xml.php?"+str;	
	}
}	

function goImport() {
	self.location.href = "import_macau.php?AcademicYearID=<?=$AcademicYearID?>";	
}

function changeYear() {
	document.getElementById('searchText').value = '';
	document.getElementById('targetClass').selectedIndex = 0;
	document.getElementById('registryStatus').selectedIndex = 0;
	document.form1.submit();
}

function displayResult () {
	var PostVar = {
			yearid: encodeURIComponent($('#AcademicYearID').val()),
			stdno: encodeURIComponent($('Input#studentno').val()),
			stdname: encodeURIComponent($('Input#studentname').val()),
			newstatus: encodeURIComponent($('Input#newstatus').val())
			}

	Block_Element("StudentListLayer");
	$.post('ajax_display_student_list.php',PostVar,
					function(data){
						//alert(data);
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#StudentListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("StudentListLayer");
						}
					});
}					

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

function submitSearch() {
	if(document.getElementById('AcademicYearID').value=="" && document.getElementById('studentno').value=="" && document.getElementById('studentname').value=="") {
		alert("<?= $i_alert_pleaseselect.' '.$Lang['StudentRegistry']['AcademicYear'].', '.$Lang['StudentRegistry']['StudentNo'].' '.$i_general_or.' '.$i_general_name?>");
		return false;
	} 
	else { 
		displayResult();
		return false;
	}
}

function checkForm() {
	checkEdit(document.form1, 'userID[]', 'new2.php');
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<div class="navigation">
    			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</div>
			 <br style="clear:both" />
			<div class="table_board">
<form name="form1" method="post" onSubmit="return submitSearch()">			
				<div>      
                        <?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>
                        <br style="clear:both" />
                </div>
                <br>
                <div class="table_filter"><?=$Lang['StudentRegistry']['SelectStudent']?> :[<?=$Lang['StudentRegistry']['AcademicYear']?>]
	                <?=$yearSelection?>
					[<?=$Lang['StudentRegistry']['StudentNo']?>]
					<input name="studentno" id="studentno" type="text" value="<?=$studentno?>"/>
					[<?=$i_general_name?>]
					<input name="studentname" id="studentname" type="text" value="<?=$studentname?>"/>
					<?= $linterface->GET_BTN($button_search, "submit") ?>
                </div>
                <p>&nbsp;</p>
              
				<div id="StudentListLayer">
        			 <div class="no_record_find_v30"> <?=$Lang['StudentRegistry']['NoStudentRecord']?></div>
				</div>

			</div>
			<p>&nbsp;</p>
		</td>
	</tr>
</table>
<input type="hidden" name="newstatus" id="newstatus" value="<?=$newstatus?>">
</form>
<? if($new!=1) {?>
<script language="javascript">
displayResult();
</script>
<? } ?>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>