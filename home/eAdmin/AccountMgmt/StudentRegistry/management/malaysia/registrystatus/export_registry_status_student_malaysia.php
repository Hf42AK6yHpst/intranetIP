<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();
$lexport = new libexporttext();

$ExportArr = array();

$filename = "student_registry_status_student_malaysia.csv";	

# condition
$conds = "";

if($targetClass!="") {
	if(sizeof($studentAry)>0)
		$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
	else {
		if(is_numeric($targetClass)) 
			$conds .= " AND y.YearID=".$targetClass;	
		else
			$conds .= " AND yc.YearClassID=".str_replace("::","",$targetClass);	  
	}		
}

if($recordstatus!="" && $recordstatus!=0)
	$conds .= " AND st.RecordStatus='$recordstatus'";
else if($recordstatus!="" && $recordstatus==0)
	$conds .= " AND st.UserID IS NULL";

if($searchText!="")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR srs.STUD_ID LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

$name_field = getNameFieldByLang("USR.");
	
$sql = "SELECT 
			IFNULL(srs.STUD_ID,'---') as stud_id,".
			Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
			ycu.ClassNumber as clsNo,
			IFNULL(USR.EnglishName,'---') as engname,
			IFNULL(USR.ChineseName,'---') as chiname,
			IFNULL(st.RecordStatus,'".$Lang['StudentRegistry']['RegistryStatus_Approved']."') as status,
			IF(st.Reason IS NULL,'---',IF(st.Reason!=0, st.Reason, st.ReasonOthers)) as reason,
			IFNULL(ApplyDate,'---') as applyDate,
			IFNULL(LEFT(st.DateModified,10),'---') as lastUpdated,
			st.ModifyBy			
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN 
			STUDENT_REGISTRY_STATUS st ON (st.UserID=USR.UserID AND st.IsCurrent=1)
		WHERE
			USR.RecordType=2 AND
			yc.AcademicYearID='$AcademicYearID'
			$conds
		ORDER BY
			y.Sequence, yc.Sequence, ycu.ClassNumber
		";
	
$row = $lsr->returnArray($sql,4);


# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	list($stud_id, $className, $classNo, $engname, $chiname, $status, $reason, $applyDate, $lastUpdated, $modifyBy) = $row[$i];
	if(is_numeric($status)) {
		switch($status) {
			case 1 : $displayStatus = $Lang['StudentRegistry']['RegistryStatus_Left']; break;
			case 2 : $displayStatus = $Lang['StudentRegistry']['RegistryStatus_Suspend']; break;
			case 3 : $displayStatus = $Lang['StudentRegistry']['RegistryStatus_Resume']; break;
			default : $displayStatus = "---"; break;
		}
	} else 
		$displayStatus = $status;
	
	if(is_numeric($reason)) {
		$displayReason = $Lang['StudentRegistry']['ReasonAry'][$reason];
	} else 
		$displayReason = $reason;
	
	$ExportArr[$i][0] = $stud_id;		//student id
	$ExportArr[$i][1] = $className;			//code
	$ExportArr[$i][2] = $classNo;  		//name in chinese
	$ExportArr[$i][3] = $engname;		// English name
	$ExportArr[$i][4] = $chiname;		// Chinese Name
	$ExportArr[$i][5] = $displayStatus;
	$ExportArr[$i][6] = $displayReason;
	$ExportArr[$i][7] = $applyDate;
	$ExportArr[$i][8] = $lastUpdated;
	if($modifyBy!="") {
		$userinfo = $lsr->getUserInfoByID($modifyBy);
		$ExportArr[$i][8] .= " (".Get_Lang_Selection($userinfo['ChineseName'], $userinfo['EnglishName']).")";
	}
		
	
}


for($m=0; $m<2; $m++) {
	for($k=0; $k<sizeof($Lang['StudentRegistry']['Malaysia_Student_CSV_field'][$m]); $k++) {
		$exportColumn[$m][$k] = $Lang['StudentRegistry']['Malaysia_Student_CSV_field'][$m][$k];
	}
}
	
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
