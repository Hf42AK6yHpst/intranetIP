<?php
#using : henry chow

############# Change Log [Start]
#
#	Date:   2010-07-27 Thomas
#			update the layout of Class List($tableContent)
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();
$linterface = new interface_html();

$searchText = stripslashes(htmlspecialchars($searchText));

# table content
$totalStudent = 0;
$totalwaitApproval = 0;
$formNameAry = array();
$yearAry = array();

if(!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onChange="Get_Class_List()"', $AcademicYearID, 0, 1);


$currentYear = getCurrentAcademicYear();
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryStatus";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['StudentRegistryStatus'], "index.php");

//$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($xmsg);
echo $lsrUI->Include_JS_CSS();

?>
<script language="javascript">
function displaySpan(layerName) {
	if(document.getElementById('exportFlag').value==0) {
		document.getElementById(layerName).style.visibility = 'visible';
		document.getElementById('exportFlag').value = 1;
	} else {
		document.getElementById(layerName).style.visibility = 'hidden';
		document.getElementById('exportFlag').value = 0;
	}
}

function goExport() {
	self.location.href = "export_registry_status_class_malaysia.php?AcademicYearID="+document.getElementById('AcademicYearID').value+"&text="+document.getElementById('searchText').value;
}

function reloadForm() {

	//document.form1.pageNo.value = 1;
	document.form1.submit();	
}

{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Class_List();
	else
		return false;
}
}

{
function Get_Class_List()
{
	var PostVar = {
			searchText: encodeURIComponent($('Input#searchText').val()),
			AcademicYearID: encodeURIComponent($('#AcademicYearID').val())
			}

	Block_Element("ClassListLayer");
	$.post('ajax_get_class_list.php',PostVar,
					function(data){
						//alert(data);
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ClassListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ClassListLayer");
						}
					});
}
	
}

function exportCSV() {
	self.location.href = 'export_csv.php?searchText='+document.getElementById('searchText').value;
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

function goAdd(status) {
	document.getElementById('newstatus').value = status;
	document.form1.action = "new1.php";
	document.form1.submit();
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="content_top_tool">
				<div class="Conntent_tool">
					<? if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage")) {?>
					<div class="btn_option">
						<a href="javascript:;" class="new" id="btn_new" onclick="MM_showHideLayers('new_option','','show');document.getElementById('btn_new').className = 'new parent_btn';"> <?=$button_new?></a>
						<br style="clear:both" />
						<div class="btn_option_layer" id="new_option" onclick="MM_showHideLayers('new_option','','hide');document.getElementById('btn_new').className = 'new';">
						<a href="javascript:goAdd(<?=STUDENT_REGISTRY_RECORDSTATUS_LEFT?>)" class="sub_btn"><?=$Lang['StudentRegistry']['StatusLeft']?></a>
						<a href="javascript:goAdd(<?=STUDENT_REGISTRY_RECORDSTATUS_SUSPEND?>)" class="sub_btn"><?=$Lang['StudentRegistry']['StatusSuspended']?></a>
						<a href="javascript:goAdd(<?=STUDENT_REGISTRY_RECORDSTATUS_RESUME?>)" class="sub_btn"><?=$Lang['StudentRegistry']['StatusResume']?></a>
						</div>	  
					</div>
					<? } ?>
					<a href="javascript:goExport()" class="export"> <?=$button_export?></a>
				</div>
				<div class="Conntent_search">
					<input name="searchText" id="searchText" type="text" value="<?=$searchText?>" onkeyup="Check_Go_Search(event);"/>
				</div>
				<br style="clear:both" />
			</div>			

<form name="form1" method="post">			
			<div class="table_board">
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="bottom">
						<div class="table_filter">
						<?=$Lang['StudentRegistry']['AcademicYear']?> : <?=$yearSelectionMenu?>
						</div>
						<br  style="clear:both"/>
					</td>
					<td valign="bottom">&nbsp;</td>
				</tr>
			</table>
			
			<div id="ClassListLayer">
			<!--
			<table class="common_table_list">
				<thead>
					<tr>
						<th><?=$Lang['AccountMgmt']['Form']?></th>
						<th><?=$Lang['StudentRegistry']['Class']?></th>
						<th><?=$Lang['StudentRegistry']['AmountOfApprove']?></th>
						<th><?=$Lang['StudentRegistry']['AmountOfLeft']?></th>
						<th><?=$Lang['StudentRegistry']['AmountOfSuspend']?></th>
						<th><?=$Lang['StudentRegistry']['AmountOfResume']?></th>
						<th class="total_col"><a href="#"><?=$Lang['StudentRegistry']['TotalAmountOfStudents']?></a></th>
					</tr>
					<?=$tableContent?>
					<tr  class="total_row">
						<th>&nbsp;</th>
						<th><?=$Lang['StudentRegistry']['Total']?></th>
						<td><?=$totalStudent?></td>
						<td><?=$totalWaitApproval?></td>
					</tr>
				</thead>
			</table>
			-->
			</div>
<input type="hidden" name="newstatus" id="newstatus" value="0">
<input type="hidden" name="new" id="new" value="1">
</form>
	<input type="hidden" name="exportFlag" id="exportFlag" value="0">

		</td>
	</tr>
</table>

</body>
<script language="javascript">
Get_Class_List();
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>