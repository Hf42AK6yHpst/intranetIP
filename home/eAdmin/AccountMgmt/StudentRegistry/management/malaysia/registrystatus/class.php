<?php
#using : 

############# Change Log [Start]
#
#	Date:   2010-07-27 Thomas
#			update the layout of Edit Button to IP25 standard
#			Add javascript function - 'checkEdit' to the Edit button
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lsr = new libstudentregistry();
$linterface = new interface_html();

$searchText = stripslashes(htmlspecialchars($searchText));


# class menu
$classMenu = $lsr->getSelectClassWithWholeForm("name=\"targetClass\" id=\"targetClass\" onChange=\"document.form1.submit()\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select, $AcademicYearID);
$studentAry = $lsr->storeStudent('0', $targetClass, $AcademicYearID);

# Button
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'userID[]','view.php')", "", "", "", "", 0);

# registry status 
$registryStatusMenu = $lsr->getRecordStatusMenu_Malaysia($recordstatus, ' onChange="document.form1.submit()"');

# record status
//$recordStatusMenu = $lsr->getRecordStatusMenu($recordstatus, ' onChange="document.form1.submit()"');


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if($order == "") $order = 1;
if($field == "") $field = 1;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onChange="changeYear()"', $AcademicYearID, 0, 1);


# conditions
$conds = "";

if($targetClass!="") {
	if(sizeof($studentAry)>0)
		$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
	else {
		if(is_numeric($targetClass)) 
			$conds .= " AND y.YearID=".$targetClass;	
		else
			$conds .= " AND yc.YearClassID=".str_replace("::","",$targetClass);	  
	}		
}

if($recordstatus!="" && $recordstatus!=0)
	$conds .= " AND st.RecordStatus='$recordstatus'";
else if($recordstatus!="" && $recordstatus==0)
	$conds .= " AND st.UserID IS NULL";

if($searchText!="")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR srs.STUD_ID LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

$name_field = getNameFieldByLang("USR.");
	
$sql = "SELECT 
			IFNULL(srs.STUD_ID,'---') as stud_id,".
			Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
			ycu.ClassNumber as clsNo,
			IFNULL(USR.EnglishName,'---') as engname,
			IFNULL(USR.ChineseName,'---') as chiname,
			IFNULL(st.RecordStatus,'".$Lang['StudentRegistry']['RegistryStatus_Approved']."') as status,
			IF(st.Reason IS NULL,'---',IF(st.Reason!=0, st.Reason, st.ReasonOthers)) as reason,
			IFNULL(ApplyDate,'---') as applyDate,
			IF(st.NoticeID!='',CONCAT('<a href=javascript:; onClick=newWindow(\'/home/eService/notice/sign.php?NoticeID=',st.NoticeID,'&StudentID=',st.UserID,'\',1)>".$Lang['StudentRegistry']['View']."</a>'),'---') as noticeID,
			IFNULL(LEFT(st.DateModified,10),'---') as lastUpdated,
			CONCAT('<input type=\'checkbox\' name=\'userID[]\' id=\'userID[]\' value=\'',USR.UserID,'\'>') as checkbox,
			CASE(st.RecordStatus)
				WHEN 1 THEN 'row_resign'
				WHEN 2 THEN 'row_temp_resign'
				WHEN 3 THEN 'row_signback'
				ELSE '' END as trCustClass,
			st.ModifyBy,
			yc.AcademicYearID,
			USR.UserID
			
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN 
			STUDENT_REGISTRY_STATUS st ON (st.UserID=USR.UserID AND st.IsCurrent=1)
		WHERE
			USR.RecordType=2 AND
			yc.AcademicYearID='$AcademicYearID'
			$conds
		";

$li->sql = $sql;
$li->field_array = array("stud_id", "clsName", "clsNo", "engname", "chiname", "status", "reason", "applyDate", "noticeID", "lastUpdated");
$li->no_col = sizeof($li->field_array)+2;
$li->fieldorder2 = ", y.Sequence, yc.Sequence, ycu.ClassNumber";
$li->IsColOff = "Malaysia_RegistryStatus";

$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['StudentNo'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['Class'])."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ClassNumber'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['EnglishName'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ChineseName'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['RegistryStatus'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['Reason'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ApplyDate'])."</th>\n";
$li->column_list .= "<th width='10%' >".$Lang['StudentRegistry']['Notice']."</th>\n"; $pos++;
$li->column_list .= "<th width='15%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['LastUpdated'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("userID[]")."</td>\n";

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryStatus";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['StudentRegistryStatus'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "");

$linterface->LAYOUT_START();

$noticeAry = $lsr->getNoticeAry();
$js = "var noticeID_Ary = new Array(".sizeof($noticeAry).");\n";
for($i=0; $i<sizeof($noticeAry); $i++) {
	list($u, $n) = $noticeAry[$i];
	$js .= "noticeID_Ary[$u] = '$n';\n";
}
?>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/form_class_management.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">
<?=$js?>
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function displayExport() {
	displaySpan('export_option');	
}

function goExport() {
	var str = "AcademicYearID=<?=$AcademicYearID?>&searchText="+document.getElementById('searchText').value+"&targetClass="+document.getElementById('targetClass').value+"&recordstatus="+document.getElementById('recordstatus').value;
	self.location.href = "export_registry_status_student_malaysia.php?"+str;	
}	

function goImport() {
	self.location.href = "import_macau.php?AcademicYearID=<?=$AcademicYearID?>";	
}

function changeYear() {
	document.getElementById('searchText').value = '';
	document.getElementById('targetClass').selectedIndex = 0;
	document.getElementById('recordstatus').selectedIndex = 0;
	document.form1.submit();
}

function changeStatus(status) {
	document.getElementById('newstatus').value = status;
	checkEdit(document.form1, 'userID[]', 'new2.php');
}

function printNotice() {
	document.form1.noticeID.value = "";
	if(countChecked(document.form1, 'userID[]')!=1) {
		alert(globalAlertMsg1);
	} else {
		var userLength = document.form1.elements.length;
		
		for(var i=0; i<userLength; i++) {
			if(document.form1.elements[i].name=="userID[]" && document.form1.elements[i].checked==true) {
				if(typeof(noticeID_Ary[document.form1.elements[i].value])!='undefined') {
					document.form1.noticeID.value = noticeID_Ary[document.form1.elements[i].value];
					break;					
				} else {
					alert("<?=$Lang['StudentRegistry']['NoNoticeForThisStudent']?>");
				}
			}				
		}
		if(document.form1.noticeID.value!="")
			newWindow("notice_print_preview.php?NoticeID="+document.form1.noticeID.value,1);
	}
}

function goAdd(status) {
	document.getElementById('newstatus').value = status;
	document.form1.action = "new1.php";
	document.form1.submit();
}

</script>
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<div class="navigation">
    			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</div>
			<div class="Conntent_tool">
			<? if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage")) {?>
				<div class="btn_option">
					<a href="javascript:;" class="new" id="btn_new" onclick="MM_showHideLayers('new_option','','show');document.getElementById('btn_new').className = 'new parent_btn';"> <?=$button_new?></a>
					<br style="clear:both" />
					<div class="btn_option_layer" id="new_option" onclick="MM_showHideLayers('new_option','','hide');document.getElementById('btn_new').className = 'new';">
					<a href="javascript:goAdd(<?=STUDENT_REGISTRY_RECORDSTATUS_LEFT?>)" class="sub_btn"><?=$Lang['StudentRegistry']['StatusLeft']?></a>
					<a href="javascript:goAdd(<?=STUDENT_REGISTRY_RECORDSTATUS_SUSPEND?>)" class="sub_btn"><?=$Lang['StudentRegistry']['StatusSuspended']?></a>
					<a href="javascript:goAdd(<?=STUDENT_REGISTRY_RECORDSTATUS_RESUME?>)" class="sub_btn"><?=$Lang['StudentRegistry']['StatusResume']?></a>
					</div>	  
				</div>
			<? } ?>	


				<div class="btn_option">
					<a href="javascript:goExport()" class="export" id="btn_export"><?=$button_export?></a>
				</div>
				<!--<a href="javascript:;" class="print"><?=$button_print?></a>-->
				</div>
				<div class="Conntent_search">
					<input name="searchText" id="searchText" type="text" value="<?=$searchText?>" onKeyUp="Check_Go_Search(event)"/>
				</div>
				<br style="clear:both" />
			</div>
			<p class="spacer"></p>
			<div class="table_board">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr class="table-action-bar">
						<td valign="bottom">
							<div class="table_filter"><?=$yearSelectionMenu?> &nbsp;</div>
							<div class="table_filter">
								<?=$classMenu?>
								<?=$registryStatusMenu?>
								<?=$recordStatusMenu?>
							</div>
							<br  style="clear:both"/>
						</td>
						<td valign="bottom">
							<div class="common_table_tool">
								<a href="javascript:printNotice()" class="tool_set"><?=$Lang['eDiscipline']['PrintNotice']?></a>
								<? if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage")) {?>
								<a href="javascript:changeStatus(<?=STUDENT_REGISTRY_RECORDSTATUS_LEFT?>);" class="tool_assign_to_student"><?=$Lang['StudentRegistry']['StatusLeft']?></a>
								<a href="javascript:changeStatus(<?=STUDENT_REGISTRY_RECORDSTATUS_SUSPEND?>);" class="tool_assign_to_student"><?=$Lang['StudentRegistry']['StatusSuspended']?></a>
								<a href="javascript:changeStatus(<?=STUDENT_REGISTRY_RECORDSTATUS_RESUME?>);" class="tool_assign_to_student"><?=$Lang['StudentRegistry']['StatusResume']?></a>
								<? } ?>  
							</div>
						</td>
					</tr>
				</table>
				<div id="ClassListLayer">
				<?=$li->display()?>
				</div>
			</div>
			<p>&nbsp;</p>
		</td>
	</tr>
</table>

<input type="hidden" name="newstatus" id="newstatus" value="0">
<input type="hidden" name="new" id="new" value="1">
<input type="hidden" name="noticeID" id="noticeID" value="">
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>