<?php
#using : 

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

//$lsr = new libstudentregistry();
$lnotice = new libnotice();
$linterface = new interface_html();

$lnotice->returnRecord($NoticeID);

switch($newstatus) {
	case 1 : $step2 = $Lang['StudentRegistry']['InputLeftInfo']; $title = $Lang['StudentRegistry']['LeftNotice']; break;
	case 2 : $step2 = $Lang['StudentRegistry']['InputSuspendInfo']; $title = $Lang['StudentRegistry']['SuspendNotice']; break;
	case 3 : $step2 = $Lang['StudentRegistry']['InputResumeInfo']; $title = $Lang['StudentRegistry']['ResumeNotice']; break;
	default : $newstatus=STUDENT_REGISTRY_RECORDSTATUS_LEFT; $step2 = $Lang['StudentRegistry']['InputLeftInfo']; $title = $Lang['StudentRegistry']['LeftNotice']; break;
}

?>
<div class="report_template_paper"> 
                          
                          <!-- Header -->
                          <div class="template_part" >
                            <p class="spacer"></p> 
                             <div class="template_part_content">
                         	 	<center><h2><?=$title?></h2></center>
                             </div>
                          </div>
                          <br />
                          <!-- Content -->
                          <div class="template_part" >
                            <p class="spacer"></p> 
                       	    <div class="template_part_content">
                         	    <div  class="added_content">
                         	    <?=$lnotice->Description?>
                         	    <p align="center"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/print_scissors.gif"></p>
                         	    <?=$lnotice->ReplySlipContent?>
                         	    </div>
                         	  </div>
                         	  <p class="spacer"></p>
                         	  <p align="center">
                         	  	<?=$linterface->GET_ACTION_BTN($button_print, "button", "window.print()")?>&nbsp;
                         	  	<?=$linterface->GET_ACTION_BTN($button_close, "button", "self.close()")?>
                         	  </p>
                        </div>
<?
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
?>

