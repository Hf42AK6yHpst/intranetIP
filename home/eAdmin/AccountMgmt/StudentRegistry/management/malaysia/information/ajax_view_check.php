<?
#using: henry chow

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$thisAry = array();

$thisAry['NAME_C'] 	= intranet_htmlspecialchars(stripslashes(trim($ChineseName)));
$thisAry['NAME_E'] 	= intranet_htmlspecialchars(stripslashes(trim($EnglishName)));
$thisAry['SEX'] 	= intranet_htmlspecialchars(trim($Gender));
$thisAry['B_DATE'] 	= intranet_htmlspecialchars(trim($BirthDate));
$thisAry['TEL'] 	= intranet_htmlspecialchars(trim($HomePhone));

$thisAry['UserID'] 		  = $userID;
$thisAry['STUD_ID'] 	  = intranet_htmlspecialchars(trim($StudentID));
$thisAry['B_PLACE'] 	  = intranet_htmlspecialchars(trim($BirthPlace));
$thisAry['NATION'] 		  = intranet_htmlspecialchars(trim($Nationality));
$thisAry['ANCESTRAL_HOME'] = intranet_htmlspecialchars(stripslashes(trim($AncestralHome)));
$thisAry['RACE'] 		  = intranet_htmlspecialchars(stripslashes(trim($Race)));
$thisAry['ID_NO'] 		  = intranet_htmlspecialchars(trim($IdCardNumber));
$thisAry['BIRTH_CERT_NO'] = intranet_htmlspecialchars(trim($BirthCertNo));
$thisAry['RELIGION'] 	  = intranet_htmlspecialchars(trim($Religion));
$thisAry['FAMILY_LANG']   = intranet_htmlspecialchars(trim($FamilyLang));
$thisAry['PAYMENT_TYPE']  = intranet_htmlspecialchars(trim($PayType));
$thisAry['EMAIL'] 		  = intranet_htmlspecialchars(stripslashes(trim($Email)));
$thisAry['LODGING'] 	  = intranet_htmlspecialchars(trim($HouseState));
$thisAry['ADDRESS1'] 	  = intranet_htmlspecialchars(stripslashes(trim($Address1)));
$thisAry['ADDRESS2'] 	  = intranet_htmlspecialchars(stripslashes(trim($Address2)));
$thisAry['ADDRESS3']  	  = intranet_htmlspecialchars(stripslashes(trim($Address3)));
$thisAry['ADDRESS4']  	  = intranet_htmlspecialchars(stripslashes(trim($Address4)));
$thisAry['ADDRESS5']  	  = intranet_htmlspecialchars(stripslashes(trim($Address5)));
$thisAry['ADDRESS6'] 	  = intranet_htmlspecialchars(stripslashes(trim($Address6)));
$thisAry['ENTRY_DATE'] 	  = intranet_htmlspecialchars(trim($EntryDate));
$thisAry['SIBLINGS']	  = intranet_htmlspecialchars(trim($SiblingsStr));
$thisAry['PRI_SCHOOL']    = intranet_htmlspecialchars(stripslashes(trim($PriSchool)));

$thisAry['FATHER_C'] 	   = intranet_htmlspecialchars(stripslashes(trim($FatherChiName)));
$thisAry['FATHER_E'] 	   = intranet_htmlspecialchars(stripslashes(trim($FatherEngName)));
$thisAry['F_PROF'] 	 	   = intranet_htmlspecialchars(stripslashes(trim($FatherJobName)));
$thisAry['F_JOB_NATURE']   = intranet_htmlspecialchars(stripslashes(trim($FatherJobNature)));
$thisAry['F_JOB_TITLE']    = intranet_htmlspecialchars(stripslashes(trim($FatherJobTitle)));
$thisAry['F_MARITAL'] 	   = intranet_htmlspecialchars(trim($FatherMarry));
$thisAry['F_EDU_LEVEL']    = intranet_htmlspecialchars(stripslashes(trim($FatherEduLevel)));
$thisAry['F_TEL'] 		   = intranet_htmlspecialchars(trim($FatherHomePhone));
$thisAry['F_MOBILE']	   = intranet_htmlspecialchars(trim($FatherCellPhone));
$thisAry['F_OFFICE'] 	   = intranet_htmlspecialchars(trim($FatherOfficePhone));
$thisAry['F_POSTAL_ADDR1'] = intranet_htmlspecialchars(stripslashes(trim($FatherAddr1)));
$thisAry['F_POSTAL_ADDR2'] = intranet_htmlspecialchars(stripslashes(trim($FatherAddr2)));
$thisAry['F_POSTAL_ADDR3'] = intranet_htmlspecialchars(stripslashes(trim($FatherAddr3)));
$thisAry['F_POSTAL_ADDR4'] = intranet_htmlspecialchars(stripslashes(trim($FatherAddr4)));
$thisAry['F_POSTAL_ADDR5'] = intranet_htmlspecialchars(stripslashes(trim($FatherAddr5)));
$thisAry['F_POSTAL_ADDR6'] = intranet_htmlspecialchars(stripslashes(trim($FatherAddr6)));
$thisAry['F_POST_CODE']    = intranet_htmlspecialchars(stripslashes(trim($FatherPostCode)));
$thisAry['F_EMAIL'] 	   = intranet_htmlspecialchars(trim($FatherEmail));

$thisAry['MOTHER_C'] 	   = intranet_htmlspecialchars(stripslashes(trim($MotherChiName)));
$thisAry['MOTHER_E'] 	   = intranet_htmlspecialchars(stripslashes(trim($MotherEngName)));
$thisAry['M_PROF'] 	 	   = intranet_htmlspecialchars(stripslashes(trim($MotherJobName)));
$thisAry['M_JOB_NATURE']   = intranet_htmlspecialchars(stripslashes(trim($MotherJobNature)));
$thisAry['M_JOB_TITLE']    = intranet_htmlspecialchars(stripslashes(trim($MotherJobTitle)));
$thisAry['M_MARITAL'] 	   = intranet_htmlspecialchars(trim($MotherMarry));
$thisAry['M_EDU_LEVEL']    = intranet_htmlspecialchars(stripslashes(trim($MotherEduLevel)));
$thisAry['M_TEL'] 		   = intranet_htmlspecialchars(trim($MotherHomePhone));
$thisAry['M_MOBILE']	   = intranet_htmlspecialchars(trim($MotherCellPhone));
$thisAry['M_OFFICE'] 	   = intranet_htmlspecialchars(trim($MotherOfficePhone));
$thisAry['M_POSTAL_ADDR1'] = intranet_htmlspecialchars(stripslashes(trim($MotherAddr1)));
$thisAry['M_POSTAL_ADDR2'] = intranet_htmlspecialchars(stripslashes(trim($MotherAddr2)));
$thisAry['M_POSTAL_ADDR3'] = intranet_htmlspecialchars(stripslashes(trim($MotherAddr3)));
$thisAry['M_POSTAL_ADDR4'] = intranet_htmlspecialchars(stripslashes(trim($MotherAddr4)));
$thisAry['M_POSTAL_ADDR5'] = intranet_htmlspecialchars(stripslashes(trim($MotherAddr5)));
$thisAry['M_POSTAL_ADDR6'] = intranet_htmlspecialchars(stripslashes(trim($MotherAddr6)));
$thisAry['M_POST_CODE']    = intranet_htmlspecialchars(stripslashes(trim($MotherPostCode)));
$thisAry['M_EMAIL'] 	   = intranet_htmlspecialchars(trim($MotherEmail));

if($GuardRelation=="F") { 			# copy from father
	$thisAry['GUARD'] 		   = "F";
	$thisAry['GUARDIAN_C'] 	   = $thisAry['FATHER_C'];
	$thisAry['GUARDIAN_E'] 	   = $thisAry['FATHER_E'];
	$thisAry['G_SEX'] 		   = "M";
	$thisAry['G_REL_OTHERS']   = "";
	$thisAry['LIVE_SAME'] 	   = "1";
	$thisAry['G_PROF'] 		   = $thisAry['F_PROF'];
	$thisAry['G_JOB_NATURE']   = $thisAry['F_JOB_NATURE'];
	$thisAry['G_JOB_TITLE']    = $thisAry['F_JOB_TITLE'];
	$thisAry['G_MARITAL']	   = $thisAry['F_MARITAL'];
	$thisAry['G_EDU_LEVEL']	   = $thisAry['F_EDU_LEVEL'];
	$thisAry['G_TEL']	   	   = $thisAry['F_TEL'];
	$thisAry['G_MOBILE'] 	   = $thisAry['F_MOBILE'];
	$thisAry['G_OFFICE'] 	   = $thisAry['F_OFFICE'];
	$thisAry['G_POSTAL_ADDR1'] = $thisAry['F_POSTAL_ADDR1'];
	$thisAry['G_POSTAL_ADDR2'] = $thisAry['F_POSTAL_ADDR2'];
	$thisAry['G_POSTAL_ADDR3'] = $thisAry['F_POSTAL_ADDR3'];
	$thisAry['G_POSTAL_ADDR4'] = $thisAry['F_POSTAL_ADDR4'];
	$thisAry['G_POSTAL_ADDR5'] = $thisAry['F_POSTAL_ADDR5'];
	$thisAry['G_POSTAL_ADDR6'] = $thisAry['F_POSTAL_ADDR6'];
	$thisAry['G_POST_CODE']    = $thisAry['F_POST_CODE'];
	$thisAry['G_EMAIL'] 	   = $thisAry['F_EMAIL'];
} 
else if($GuardRelation=="M") {	# copy from father
	$thisAry['GUARD'] 		   = "M";
	$thisAry['GUARDIAN_C'] 	   = $thisAry['MOTHER_C'];
	$thisAry['GUARDIAN_E'] 	   = $thisAry['MOTHER_E'];
	$thisAry['G_SEX'] 		   = "F";
	$thisAry['G_REL_OTHERS']   = "";
	$thisAry['LIVE_SAME'] 	   = "1";
	$thisAry['G_PROF'] 		   = $thisAry['M_PROF'];
	$thisAry['G_JOB_NATURE']   = $thisAry['M_JOB_NATURE'];
	$thisAry['G_JOB_TITLE']    = $thisAry['M_JOB_TITLE'];
	$thisAry['G_MARITAL']	   = $thisAry['M_MARITAL'];
	$thisAry['G_EDU_LEVEL']	   = $thisAry['M_EDU_LEVEL'];
	$thisAry['G_TEL']	   	   = $thisAry['M_TEL'];
	$thisAry['G_MOBILE'] 	   = $thisAry['M_MOBILE'];
	$thisAry['G_OFFICE'] 	   = $thisAry['M_OFFICE'];
	$thisAry['G_POSTAL_ADDR1'] = $thisAry['M_POSTAL_ADDR1'];
	$thisAry['G_POSTAL_ADDR2'] = $thisAry['M_POSTAL_ADDR2'];
	$thisAry['G_POSTAL_ADDR3'] = $thisAry['M_POSTAL_ADDR3'];
	$thisAry['G_POSTAL_ADDR4'] = $thisAry['M_POSTAL_ADDR4'];
	$thisAry['G_POSTAL_ADDR5'] = $thisAry['M_POSTAL_ADDR5'];
	$thisAry['G_POSTAL_ADDR6'] = $thisAry['M_POSTAL_ADDR6'];
	$thisAry['G_POST_CODE']    = $thisAry['M_POST_CODE'];
	$thisAry['G_EMAIL'] 	   = $thisAry['M_EMAIL'];
}
else {							# self input info
	$thisAry['GUARD']		   = intranet_htmlspecialchars(trim($GuardRelation));
	$thisAry['GUARDIAN_C']	   = intranet_htmlspecialchars(trim($GuardChiName));
	$thisAry['GUARDIAN_E']	   = intranet_htmlspecialchars(trim($GuardEngName));
	$thisAry['G_SEX']		   = intranet_htmlspecialchars(trim($GuardGender));
	$thisAry['G_REL_OTHERS']   = intranet_htmlspecialchars(trim($GuardRelationOther));
	$thisAry['LIVE_SAME']	   = intranet_htmlspecialchars(trim($GuardStay));	
	$thisAry['G_PROF']		   = intranet_htmlspecialchars(trim($GuardJobName));
	$thisAry['G_JOB_NATURE']   = intranet_htmlspecialchars(trim($GuardJobNature));
	$thisAry['G_JOB_TITLE']	   = intranet_htmlspecialchars(trim($GuardJobTitle));
	$thisAry['G_MARITAL']	   = intranet_htmlspecialchars(trim($GuardMarry));
	$thisAry['G_EDU_LEVEL']	   = intranet_htmlspecialchars(trim($GuardEduLevel));
	$thisAry['G_TEL']		   = intranet_htmlspecialchars(trim($GuardHomePhone));
	$thisAry['G_MOBILE']	   = intranet_htmlspecialchars(trim($GuardCellPhone));
	$thisAry['G_OFFICE']	   = intranet_htmlspecialchars(trim($GuardOfficePhone));
	$thisAry['G_POSTAL_ADDR1'] = intranet_htmlspecialchars(trim($GuardAddr1));
	$thisAry['G_POSTAL_ADDR2'] = intranet_htmlspecialchars(trim($GuardAddr2));
	$thisAry['G_POSTAL_ADDR3'] = intranet_htmlspecialchars(trim($GuardAddr3));
	$thisAry['G_POSTAL_ADDR4'] = intranet_htmlspecialchars(trim($GuardAddr4));
	$thisAry['G_POSTAL_ADDR5'] = intranet_htmlspecialchars(trim($GuardAddr5));
	$thisAry['G_POSTAL_ADDR6'] = intranet_htmlspecialchars(trim($GuardAddr6));
	$thisAry['G_POST_CODE']    = intranet_htmlspecialchars(stripslashes(trim($GuardPostCode)));		
	$thisAry['G_EMAIL'] 	   = intranet_htmlspecialchars(trim($GuardEmail));
}

$error = array();
$error = $lsr->checkData_Mal('edit', $thisAry, $Mode);
$err_msg = "";

/*
if(sizeof($error['StudentInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['StudentInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['FatherInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['FatherInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['FatherInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['MotherInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['MotherInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['MotherInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['GuardianInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['GuardianInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['GuardianInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}
*/

if(sizeof($error)>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error as $key=>$val) {
		$err_msg .= "<li>$val</li>";
	}
   	$err_msg .= "</ul></font>";
}

echo $err_msg;

intranet_closedb();
?>