<?
#using: Thomas

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && (!$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv")) && ($laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$siblings = $lsr->SEARCH_SIBLINGS($StudentNo, $AcademicYearID);

$result = "";

for($i=0;$i<sizeof($siblings);$i++)
{
	$result .= "<option value=\"".$siblings[$i]['UserID']."\">".$siblings[$i]['ClassName']."-".$siblings[$i]['ClassNumber']." ".$siblings[$i]['StudentName']."</option>\n";
}

echo $result;

intranet_closedb();
?>