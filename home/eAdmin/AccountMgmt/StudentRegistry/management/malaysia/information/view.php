<?php
#using: henry chow
##### Change Log [Start] #####
#
#	Date	:	2010-10-13 Henry Chow
#				update Guardian info in basic mode
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$userID = (is_array($userID)? $userID[0]:$userID);

$laccessright = new libaccessright();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && (!$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic")) && ($laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv"))) {
	header("Location: view_adv.php?AcademicYearID=$AcademicYearID&userID=$userID");
	exit;
}
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lsr = new libstudentregistry();
$linterface = new interface_html();

$result = $lsr -> RETRIEVE_STUDENT_INFO_BASIC_MAL($userID);

#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "Gender", $gender_val);

#Nationality related info
$nation_ary      = $lsr -> MAL_NATION_DATA_ARY();
$nation_val      = $result[0]['NATION']? $lsr -> RETRIEVE_DATA_ARY($nation_ary, $result[0]['NATION']) : "--";
$nation_edit_ary = $lsr -> RETRIEVE_DATA_ARY($nation_ary);
$nation_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($nation_edit_ary, "Nationality", $nation_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanNation\'); else hideSpan(\'spanNation\');"');
if($result[0]['NATION']=="-9" && $result[0]['NATION_OTHERS']!="") $nation_val .= " (".$result[0]['NATION_OTHERS'].")";

# Registry Status
$registry_status = $lsr -> RETRIEVE_STUDENT_REGISTRY_STATUS($userID);
switch($registry_status) {
	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Left'];
			break;
	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Suspend'];
			break;
	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Resume'];
			break;
	default: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Approved'];	
}

#Past Enrollment Information
$Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($userID);

if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}
else
{
	$Past_Enroll_html = "<tr><td>---</td></tr>";
}

$guardianInfo = $lsr->getParentGuardianInfo($userID, "G");
$guardian_CH_Name = intranet_htmlspecialchars($guardianInfo['NAME_C']);
$guardian_EN_Name = intranet_htmlspecialchars($guardianInfo['NAME_E']);
$CH_Name_HTML = "<input type=\"text\" name=\"G_NAME_C\" id=\"G_NAME_C\" VALUE=\"$guardian_CH_Name\" readonly=\"readonly\" style=\"border: 0px\">";
$EN_Name_HTML = "<input type=\"text\" name=\"G_NAME_E\" id=\"G_NAME_E\" VALUE=\"$guardian_EN_Name\" readonly=\"readonly\" style=\"border: 0px\">";  
$G_HOME_TEL = "<input type=\"text\" name=\"G_HOME_TEL\" id=\"G_HOME_TEL\" VALUE=\"".$guardianInfo['TEL']."\" readonly=\"readonly\" style=\"border: 0px\">";
$G_MOBILE_TEL = "<input type=\"text\" name=\"G_MOBILE_TEL\" id=\"G_MOBILE_TEL\" VALUE=\"".$guardianInfo['MOBILE']."\" readonly=\"readonly\" style=\"border: 0px\">";

#Modified By
$ModifiedBy = $result[0]['ModifyBy']? $lsr -> RETRIEVE_MODIFIED_BY_INFO($result[0]['ModifyBy']) : "";

//debug_pr($result[0]['Gender']);
//debug_pr($gender_edit_ary);

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";


$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
# need to modified here!
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($result[0]['ClassName']." ".$Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[0]['YearClassID']);
$PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result[0]['EnglishName'] : $result[0]['ChineseName']), "");

#Content Top Tool Button
if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv"))
	$Content_Top_BTN[] = array($Lang['StudentRegistry']['AdvInfo'], "view_adv.php?AcademicYearID=$AcademicYearID&userID=$userID");
$Content_Top_BTN[] = array($Lang['StudentRegistry']['BasicInfo'], "view.php?AcademicYearID=$AcademicYearID&userID=$userID", "", true);

#Save and Cancel Button
$SaveBtn   = $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checksubmit(this.form)", "SaveBtn", "style=\"display: none\"");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:ResetInfo()", "CancelBtn", "style=\"display: none\"");

//$linterface->LAYOUT_START();
if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
$linterface->LAYOUT_START(urldecode($msg));

?>
<script language="javascript">
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#EditBtn').attr('style', 'visibility: hidden');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		<? if($result[0]['NATION']=="-9") {?>
		$('#spanNation').attr('style', '');
		<?}?>
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');
		$(':text[value="--"]').val('');
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', '');
		$('#EditBtn').attr('style', '');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		$('#spanNation').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');
		$(':text[value=""]').val('--');
		
		$('#ErrorLog').html('');
	}

/*	
	function show_arg()
	{
		if(!checkform()) return false;
		//var abc = $('[name="form1"]').serialize();
		//alert(abc);
		document.form1.submit();
	}
	
	function checkform()
	{
		var obj = document.form1;
		
		if(!check_text(obj.StudentID, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['StudentID']?>")) return false;
		if(!check_text(obj.ChineseName, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['ChineseName']?>")) return false;
		if(!check_text(obj.EnglishName, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['EnglishName']?>")) return false;
		if(obj.Gender[0].checked == false && obj.Gender[1].checked == false){
			alert('<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['Gneder']?>');
			return false;
		}
		if(!check_text(obj.HomePhone, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['HomePhone']?>")) return false;
		
		return true;
	}
*/
	
	function checksubmit(obj)
	{
		var Gender = obj.Gender[0].checked? "M":"F";
		$.post(
				'ajax_view_check.php',
				{
					Mode 		: 0,
					StudentID 	: obj.StudentID.value,
					ChineseName : obj.ChineseName.value,
					EnglishName : obj.EnglishName.value,
					Gender 		: Gender,
					Nationality : obj.Nationality.value,
					HomePhone 	: obj.HomePhone.value,
					GuardChiName	: obj.G_NAME_C.value,
					GuardEngName	: obj.G_NAME_E.value,
					GuardHomePhone	: obj.G_HOME_TEL.value,
					GuardCellPhone: obj.G_MOBILE_TEL.value
					
				},
				function(data){
					
					if(data=="")
					{
						obj.submit();
					}
					else
					{
						$('#ErrorLog').html(data);
					}
				});
	}
	
	$(document).ready(function()
	{
		$(':text[value=""]').val('--');
	});
	
function showSpan(layerName) {
	document.getElementById(layerName).style.display = 'inline';
	//$('#spanNation').attr('style', '');
}

function hideSpan(layerName) {
	document.getElementById(layerName).style.display = 'none';	
	//$('#spanNation').attr('style', 'none');
}

</script>
<form name="form1" method="POST" action="view_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<p class="spacer"></p>
			<div class="content_top_tool">
				<?= $linterface->GET_CONTENT_TOP_BTN($Content_Top_BTN)?>
			</div>
			<div class="table_board">
        		<div class="table_row_tool row_content_tool">
                	<?=$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]? $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Edit'], "button", "javascript:EditInfo();", "EditBtn") : "&nbsp;" ?>
                </div>
                <p class="spacer"></p>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
                <table class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Mal_StudentNo'] ?></td>
                        	<td colspan="5"><input type="text" name="StudentID" value="<?= $result[0]['STUD_ID']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input type="text" name="ChineseName" value="<?= $result[0]['ChineseName']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td><input type="text" name="EnglishName" value="<?= $result[0]['EnglishName']?>" readonly="readonly" style="border: 0px"/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td><span class="Edit_Hide"><?= $gender_val ?></span><?= $gender_RBL_html ?></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Nationality']?></td>
                            <td colspan="3"><span class="Edit_Hide"><?= $nation_val ?></span><?= $nation_DDL_html ?><div id="spanNation" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="nation_others" id="nation_others" value="<?=$result[0]['NATION_OTHERS']?>"></td></tr></table></div></td>  
                        </tr>
						<tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                            <td colspan="5"><input type="text" name="HomePhone" value="<?= $result[0]['HomeTelNo']?>" readonly="readonly" style="border: 0px"/></td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['StudyAt'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Grade'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['YearName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Class'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['ClassName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['InClassNumber'] ?>)</em></td>
                        	    			<td><span class="row_content"><?= $result[0]['ClassNumber']?></span></td>
                        	    		</tr>
                        	    	</table>
								</div>
                            </td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['PastEnroll'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table> 
                        				<?= $Past_Enroll_html ?>
                        			</table>
                        		</div>
                        	</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['RegistryStatus'] ?></td>
                            <td colspan="5"><?=$ownRegistryStatus?></td>
                        </tr>
                        <tr>
                        	<td colspan="6">&nbsp;</td>
                        </tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['ContactPersonInfo'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
        		<table class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                        	<td><?=$CH_Name_HTML?></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                        	<td><?=$EN_Name_HTML?></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></span></td>
                        	<td><?=$G_HOME_TEL?></td>
                        	<td class="field_title_short"><span class="field_title"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></span></td>
                         	<td><?=$G_MOBILE_TEL?></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                        <tr>
                        	<td colspan="6">&nbsp;</td>
                        </tr>				
                  	</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				<div id="ErrorLog"></div>
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark"><?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result[0]['DateModified']? $result[0]['DateModified']."(".$ModifiedBy.")" : "--"?></span>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $CancelBtn ?>
                </div>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<script language="javascript">
	document.getElementById('SaveBtn').style.visibility = "hidden";
	document.getElementById('CancelBtn').style.visibility = "hidden";
	<? if($EditRecord) {?>
	$('#EditBtn').click();	
	<? }?>
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>