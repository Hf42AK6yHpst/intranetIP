<?php
#using: 
##### Change Log [Start] #####
#
#	Date	:	2010-10-13 Henry Chow
#				update Guardian info in basic mode
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$dataAry = array();
$pgAry = array();

$dataAry['STUD_ID'] = $StudentID;
$dataAry['NATION']  = $Nationality;
$dataAry['NATION_OTHERS']  = $nation_others;

$pgAry['NAME_C'] = $G_NAME_C;
$pgAry['NAME_E'] = $G_NAME_E;
$pgAry['TEL'] = $G_HOME_TEL;
$pgAry['MOBILE'] = $G_MOBILE_TEL;

$lsr->updateStudentRegistry_Simple_Mal($dataAry, $userID, $pgAry);

$userAry['ChineseName'] = $ChineseName;
$userAry['EnglishName'] = $EnglishName;
$userAry['Gender'] = $Gender;
$userAry['HomeTelNo'] = $HomePhone;
$lsr->updateStudentInfo($userAry, $userID);

# update eClass
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
$lu = new libuser($userID);
$le = new libeclass();
$le->eClass40UserUpdateInfo($lu->UserEmail, $userAry);


intranet_closedb();

header("Location: view.php?xmsg=1&AcademicYearID=$AcademicYearID&userID=$userID");
?>