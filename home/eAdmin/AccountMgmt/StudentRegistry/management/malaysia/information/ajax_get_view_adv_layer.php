<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lsr = new libstudentregistry();

#Student's Info
$result = $lsr -> RETRIEVE_STUDENT_INFO_ADV_MAL($userID);
$Father = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID, 'F');
$Mother = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID, 'M');
$Guard 	= $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID, 'G');
$GuardRelation_ary = $lsr -> MAL_GUARD_DATA_ARY();
$GuardRelation_val = $Guard[0]['GUARD']? $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary, $Guard[0]['GUARD']) : $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary, 'O');

#Past Enrollment Information
$Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($userID);

if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}
else
{
	$Past_Enroll_html = "<tr><td>---</td></tr>";
}

#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "Gender", $gender_val);

#Nationality related info
$nation_ary      = $lsr -> MAL_NATION_DATA_ARY();
$nation_val      = $result[0]['NATION']? $lsr -> RETRIEVE_DATA_ARY($nation_ary, $result[0]['NATION']) : "--";
$nation_edit_ary = $lsr -> RETRIEVE_DATA_ARY($nation_ary);
$nation_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($nation_edit_ary, "Nationality", $nation_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanNation\'); else hideSpan(\'spanNation\');"');
if($result[0]['NATION']=="-9" && $result[0]['NATION_OTHERS']!="") $nation_val .= " (".$result[0]['NATION_OTHERS'].")";

#Race related info
$race_ary 	   = $lsr -> MAL_RACE_DATA_ARY();
$race_val      = $result[0]['RACE']? $lsr -> RETRIEVE_DATA_ARY($race_ary, $result[0]['RACE']) : "--";
$race_edit_ary = $lsr -> RETRIEVE_DATA_ARY($race_ary);
$race_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($race_edit_ary, "Race", $race_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanRace\'); else hideSpan(\'spanRace\');"');
if($result[0]['RACE']=="-9" && $result[0]['RACE_OTHERS']!="") $race_val .= " (".$result[0]['RACE_OTHERS'].")";

#religion related info
$religion_ary      = $lsr -> MAL_RELIGION_DATA_ARY();
$religion_val      = $result[0]['RELIGION']? $lsr -> RETRIEVE_DATA_ARY($religion_ary, $result[0]['RELIGION']) : "--";
$religion_edit_ary = $lsr -> RETRIEVE_DATA_ARY($religion_ary);
$religion_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($religion_edit_ary, "Religion", $religion_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanReligion\'); else hideSpan(\'spanReligion\');"');
if($result[0]['RELIGION']=="-9" && $result[0]['RELIGION_OTHERS']!="") $religion_val .= " (".$result[0]['RELIGION_OTHERS'].")";

#Family Language related info
$familyLang_ary      = $lsr -> MAL_FAMILY_LANG_ARY();
$familyLang_val      = $result[0]['FAMILY_LANG']? $lsr -> RETRIEVE_DATA_ARY($familyLang_ary, $result[0]['FAMILY_LANG']) : "--";
$familyLang_edit_ary = $lsr -> RETRIEVE_DATA_ARY($familyLang_ary);
$familyLang_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($familyLang_edit_ary, "FamilyLang", $familyLang_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanFamilyLang\'); else hideSpan(\'spanFamilyLang\');"');
if($result[0]['FAMILY_LANG']=="-9" && $result[0]['FAMILY_LANG_OTHERS']!="") $familyLang_val .= " (".$result[0]['FAMILY_LANG_OTHERS'].")";

#Family Language related info
$payType_ary      = $lsr -> MAL_PAYMENT_TYPE_ARY();
$payType_val      = $result[0]['PAYMENT_TYPE']? $lsr -> RETRIEVE_DATA_ARY($payType_ary, $result[0]['PAYMENT_TYPE']) : "--";
$payType_edit_ary = $lsr -> RETRIEVE_DATA_ARY($payType_ary);
$payType_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($payType_edit_ary, "PayType", $payType_val);

#BirthPlace related info
$BPlace_ary      = $lsr -> MAL_B_PLACE_DATA_ARY();
$BPlace_val      = $result[0]['B_PLACE']? $lsr -> RETRIEVE_DATA_ARY($BPlace_ary, $result[0]['B_PLACE']) : "--";
$BPlace_edit_ary = $lsr -> RETRIEVE_DATA_ARY($BPlace_ary);
$BPlace_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($BPlace_edit_ary, "BirthPlace", $BPlace_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanBPlace\'); else hideSpan(\'spanBPlace\');"');
if($result[0]['B_PLACE']=="-9" && $result[0]['B_PLACE_OTHERS']!="") $BPlace_val .= " (".$result[0]['B_PLACE_OTHERS'].")";

#Housing State related info
$HouseState_ary      = $lsr -> MAL_LODGING_ARY();
$HouseState_val      = $result[0]['LODGING']? $lsr -> RETRIEVE_DATA_ARY($HouseState_ary, $result[0]['LODGING']) : "--";
$HouseState_edit_ary = $lsr -> RETRIEVE_DATA_ARY($HouseState_ary);
if($result[0]['LODGING']=="-9" && $result[0]['LODGING_OTHERS']!="") $HouseState_val .= " (".$result[0]['LODGING_OTHERS'].")";

#Common Array (For Father, Mother, Guardian)
	$Marry_ary 			= $lsr -> MAL_MARITAL_STATUS_DATA_ARY();
	$Marry_edit_ary 	= $lsr -> RETRIEVE_DATA_ARY($Marry_ary);
	$JobNature_ary 		= $lsr -> MAL_JOB_NATURE_DATA_ARY();
	$JobTitle_ary 		= $lsr -> MAL_JOB_TITLE_DATA_ARY();
	$EduLevel_ary		= $lsr -> MAL_EDU_LEVEL_DATA_ARY();
	
#Father's Info
	//$Father = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'F');
	#Father's Marital Status Together info
	$FatherMarry_val 	  = $Father[0]['MARITAL_STATUS'] !== null? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Father[0]['MARITAL_STATUS']) : "--";
	
	#Father's Job Nature info
	$FatherJobNature_val = $Father[0]['JOB_NATURE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobNature_ary, $Father[0]['JOB_NATURE']) : "--";
	if($Father[0]['JOB_NATURE']=="-9" && $Father[0]['JOB_NATURE_OTHERS']!="") $FatherJobNature_val .= " (".$Father[0]['JOB_NATURE_OTHERS'].")";
	
	#Father's Job Title info
	$FatherJobTitle_val = $Father[0]['JOB_TITLE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary, $Father[0]['JOB_TITLE']) : "--";
	if($Father[0]['JOB_TITLE']=="-9" && $Father[0]['JOB_TITLE_OTHERS']!="") $FatherJobTitle_val .= " (".$Father[0]['JOB_TITLE_OTHERS'].")";
	
	#Father's Education Level info
	$FatherEduLevel_val = $Father[0]['EDU_LEVEL'] !== null? $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary, $Father[0]['EDU_LEVEL']) : "--";	
	if($Father[0]['EDU_LEVEL']=="-9" && $Father[0]['EDU_LEVEL_OTHERS']!="") $FatherEduLevel_val .= " (".$Father[0]['EDU_LEVEL_OTHERS'].")";
	
#Mother's Info
	//$Mother = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'M');
	#Mother's Marital Status info
	$MotherMarry_val 	  = $Mother[0]['MARITAL_STATUS'] !== null? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Mother[0]['MARITAL_STATUS']) : "--";
	
	#Mother's Job Nature info
	$MotherJobNature_val = $Mother[0]['JOB_NATURE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobNature_ary, $Mother[0]['JOB_NATURE']) : "--";
	if($Mother[0]['JOB_NATURE']=="-9" && $Mother[0]['JOB_NATURE_OTHERS']!="") $MotherJobNature_val .= " (".$Mother[0]['JOB_NATURE_OTHERS'].")";
	
	#Mother's Job Title info
	$MotherJobTitle_val = $Mother[0]['JOB_TITLE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary, $Mother[0]['JOB_TITLE']) : "--";
	if($Mother[0]['JOB_TITLE']=="-9" && $Mother[0]['JOB_TITLE_OTHERS']!="") $MotherJobTitle_val .= " (".$Mother[0]['JOB_TITLE_OTHERS'].")";
	
	#Mother's Education Level info
	$MotherEduLevel_val = $Mother[0]['EDU_LEVEL'] !== null? $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary, $Mother[0]['EDU_LEVEL']) : "--";	
	if($Mother[0]['EDU_LEVEL']=="-9" && $Mother[0]['EDU_LEVEL_OTHERS']!="") $MotherEduLevel_val .= " (".$Mother[0]['EDU_LEVEL_OTHERS'].")";
	
#Guardian's Info
	//$Guard = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'G');
	#Guardian's Gender info
	$GuardGender_val	  = ($Guard[0]['GUARD'] == 'O' && $Guard[0]['G_GENDER'])? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $Guard[0]['G_GENDER']) : "--";

	#Guardian's Job Nature info
	$GuardJobNature_val = $Guard[0]['JOB_NATURE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobNature_ary, $Guard[0]['JOB_NATURE']) : "--";
	if($Guard[0]['JOB_NATURE']=="-9" && $Guard[0]['JOB_NATURE_OTHERS']!="") $GuardJobNature_val .= " (".$Guard[0]['JOB_NATURE_OTHERS'].")";
	
	#Guardian's Job Title info
	$GuardJobTitle_val = $Guard[0]['JOB_TITLE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary, $Guard[0]['JOB_TITLE']) : "--";
	if($Guard[0]['JOB_TITLE']=="-9" && $Guard[0]['JOB_TITLE_OTHERS']!="") $GuardJobTitle_val .= " (".$Guard[0]['JOB_TITLE_OTHERS'].")";
	
	#Guardian's Education Level info
	$GuardEduLevel_val = $Guard[0]['EDU_LEVEL'] !== null? $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary, $Guard[0]['EDU_LEVEL']) : "--";	
	if($Guard[0]['EDU_LEVEL']=="-9" && $Guard[0]['EDU_LEVEL_OTHERS']!="") $GuardEduLevel_val .= " (".$Guard[0]['EDU_LEVEL_OTHERS'].")";
	
	#Guardian's Relation with Student info
	$GuardRelation_ary 		= $lsr -> MAL_GUARD_DATA_ARY();
	$GuardRelation_val		= $Guard[0]['GUARD']? $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary, $Guard[0]['GUARD']) : $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary, 'O');

	#Guardian's Stay with Student info
	$GuardStay_ary 		= $lsr -> MAL_LIVE_SAME_ARY();
	$GuardStay_val 		= ($Guard[0]['GUARD'] == 'O' && $Guard[0]['LIVE_SAME']!="")? $lsr -> RETRIEVE_DATA_ARY($GuardStay_ary, $Guard[0]['LIVE_SAME']) : "--";

$registry_status = $lsr -> RETRIEVE_STUDENT_REGISTRY_STATUS($userID);
switch($registry_status) {
	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Left'];
			break;
	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Suspend'];
			break;
	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Resume'];
			break;
	default: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Approved'];	
}

#Elder Brothers / Sister in School
if($result[0]['BRO_SIS_IN_SCHOOL'])
{
	$Siblings_UserID_ary = explode(",", $result[0]['BRO_SIS_IN_SCHOOL']);
	$Siblings_html = "";
	$Siblings_option_html = "";
	for($i=0;$i<sizeof($Siblings_UserID_ary);$i++)
	{
		$Siblings_Info = $lsr->getStudentInfo_by_UserID($Siblings_UserID_ary[$i], $AcademicYearID);
		
		if(!$Siblings_Info)
			continue;
		else
		{
			$text = (($Siblings_Info['YearOfLeft']!="") ? "(".$Siblings_Info['YearOfLeft'].")" : $Siblings_Info['ClassName']."-".$Siblings_Info['ClassNumber'])." ".$Siblings_Info['StudentName'];
			$Siblings_html .= "<tr><td><em>".($i+1).".</em>$text</td></tr>";
		}
	}
	if($Siblings_html == "")
		$Siblings_html = "<tr><td>---</td></tr>";
}
else
	$Siblings_html = "<tr><td>---</td></tr>";

$tableContent = '
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title">'.$Lang['StudentRegistry']['StudInfo'].'</span> -</em>
                    <p class="spacer"></p>
                </div>
                <table id="StudentInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short">'.$Lang['StudentRegistry']['Mal_StudentNo'].'</td>
                        	<td colspan="5">'.$result[0]['STUD_ID'].'</td>
						</tr>
                        <tr>
                            <td class="field_title_short">'.$Lang['StudentRegistry']['ChineseName'].'</td>
                            <td>'.$result[0]['ChineseName'].'</td>
                            <td class="field_title_short">'.$Lang['StudentRegistry']['EnglishName'].'</td>
                            <td>'.$result[0]['EnglishName'].'</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
						<tr>
                            <td class="field_title_short">'.$Lang['StudentRegistry']['Gender'].'</td>
                            <td>'.$gender_val.'</td>
                            <td class="field_title_short">'.$Lang['StudentRegistry']['BirthDate'].'</td>
                        	<td>'.($result[0]['DateOfBirth']? date("Y-m-d", strtotime($result[0]['DateOfBirth'])) : "").'</td>
                            <td class="field_title_short">'.$Lang['StudentRegistry']['BirthPlace'].'</td>
                            <td>'.$BPlace_val.'</td>
                        </tr>

                        <tr>
                            <td class="field_title_short">'.$Lang['StudentRegistry']['Nationality'].'</td>
                            <td>'. $nation_val .'</td>                      
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['AncestalHome'].'</td>
                        	<td>'. $result[0]['ANCESTRAL_HOME'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['Race'].'</td>
                        	<td>'. $race_val .'</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['IdCard'].$Lang['StudentRegistry']['CardNumber'].'</td>
                        	<td>'. $result[0]['ID_NO'].'</td>                 
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['BirthCertNo'].'</td>
                        	<td colspan="3">'. $result[0]['BIRTH_CERT_NO'].'</td>
                        </tr>
                        <tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['Religion'].'</td>
                            <td>'. $religion_val .'</td>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['FamilyLang'].'</td>
                            <td>'. $familyLang_val .'</td>                       
                            <td class="field_title_short">'. $Lang['StudentRegistry']['PayType'].'</td>
                            <td>'. $payType_val .'</td>
                        </tr>
                        <tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['HomePhone'] .'</td>
                            <td>'. $result[0]['HomeTelNo'].'</td>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['Email'] .'</td>
                            <td>'. $result[0]['EMAIL'].'</td>
                        	<td colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['HousingState'].'</td>
                            <td colspan="5">'. $HouseState_val .'</td>
                        </tr>
                        <tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['HomeAddress'] .'</td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Door'] .')</em></td>
                  		      				<td>'. $result[0]['ADDRESS1'].'</td>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Road'] .')</em></td>
                  		      				<td>'. $result[0]['ADDRESS2'].'</td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Garden'] .')</em></td>
                        	    			<td>'. $result[0]['ADDRESS3'].'</td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>('. $Lang['StudentRegistry']['Town'] .')</em></td>
                        	    			<td>'. $result[0]['ADDRESS4'].'</td>
                        	    			<td><em>('. $Lang['StudentRegistry']['State'] .')</em></td>
                        	    			<td>'. $result[0]['ADDRESS5'].'</td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Postal'] .')</em></td>
                        	    			<td>'. $result[0]['ADDRESS6'].'</td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['StudyAt'] .'</td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Grade'] .')</em></td>
                     		       			<td><span class="row_content">'. $result[0]['YearName'].'</span></td>
                     		       			<td><em>('. $Lang['StudentRegistry']['Class'] .')</em></td>
                     		       			<td><span class="row_content">'. $result[0]['ClassName'].'</span></td>
                     		       			<td><em>('. $Lang['StudentRegistry']['InClassNumber'] .')</em></td>
                        	    			<td><span class="row_content">'. $result[0]['ClassNumber'].'</span></td>
                        	    		</tr>
                        	    	</table>
                        	    	<table>
                        	    			<td><em>('. $Lang['StudentRegistry']['EntryDate'] .')</em></td>
                  		      				<td colspan="7"><span class="row_content">'.($result[0]['ADMISSION_DATE']? date("Y-m-d", strtotime($result[0]['ADMISSION_DATE'])) : "").'</span></td>
                        	    	</table>
								</div>
                            </td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['PastEnroll'] .'</td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table> 
										'. $Past_Enroll_html .'
                        			</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['PastPrimarySch'] .'</td>
                            <td colspan="5">'. $result[0]['PRI_SCHOOL'].'</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'.$Lang['StudentRegistry']['Siblings'].'</td>
                        	<td colspan="5">
                        		<table class="Edit_Hide form_field_sub_content">
                        			'.$Siblings_html.'
                        		</table>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['RegistryStatus'] .'</td>
                        	<td colspan="5">
                        		'.$ownRegistryStatus.'
                        	</td>
                        </tr>
                        <tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title_v30">
    				<em>- <span class="field_title">'. $Lang['StudentRegistry']['FatherInfo'] .'</span> -</em>
					<p class="spacer"></p>
				</div>
        		<table id="FatherInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['ChineseName'] .'</td>
                            <td>'. $Father[0]['NAME_C'].'</td>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['EnglishName'] .'</td>
                            <td>'. $Father[0]['NAME_E'].'</td>
							<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['Job'] .'</td>
                        	<td>'. $Father[0]['PROF'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['JobNature'] .'</td>
                        	<td>'. $FatherJobNature_val .'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['JobTitle'] .'</td>
                        	<td>'. $FatherJobTitle_val .'</td>
						</tr>
						<tr>
						    <td class="field_title_short">'. $Lang['StudentRegistry']['MaritalStatus'] .'</td>
                         	<td>'. $FatherMarry_val .'</td>
                         	<td class="field_title_short">'. $Lang['StudentRegistry']['EducationLevel'] .'</td>
                        	<td>'. $FatherEduLevel_val .'</td>
							<td colspan="2">&nbsp;</td>
						</tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['HomePhone'] .'</td>
                        	<td>'. $Father[0]['TEL'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['CellPhone'] .'</td>
                         	<td>'. $Father[0]['MOBILE'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['OfficeNumber'] .'</td>
                         	<td>'. $Father[0]['OFFICE_TEL'].'</td>						
						</tr>
						<tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['ContactAddress'] .'</td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Door'] .')</em></td>
                  		      				<td><span class="row_content">'. $Father[0]['POSTAL_ADDR1'].'</span></td>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Road'] .')</em></td>
                  		      				<td><span class="row_content">'. $Father[0]['POSTAL_ADDR2'].'</span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Garden'] .')</em></td>
                        	    			<td><span class="row_content">'. $Father[0]['POSTAL_ADDR3'].'</span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>('. $Lang['StudentRegistry']['Town'] .')</em></td>
                        	    			<td><span class="row_content">'. $Father[0]['POSTAL_ADDR4'].'</span></td>
                        	    			<td><em>('. $Lang['StudentRegistry']['State'] .')</em></td>
                        	    			<td><span class="row_content">'. $Father[0]['POSTAL_ADDR5'].'</span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Postal'] .')</em></td>
                        	    			<td><span class="row_content">'. $Father[0]['POSTAL_ADDR6'].'</span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
						<tr>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Postal'] .'</td>
							<td>'. $Father[0]['POST_CODE'].'</td>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Email'] .'</td>
							<td>'. $Father[0]['EMAIL'].'</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title_v30">
    				<em>- <span class="field_title">'. $Lang['StudentRegistry']['MotherInfo'] .'</span> -</em>
					<p class="spacer"></p>
				</div>
        		<table id="MotherInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['ChineseName'] .'</td>
                            <td>'. $Mother[0]['NAME_C'].'</td>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['EnglishName'] .'</td>
                            <td>'. $Mother[0]['NAME_E'].'</td>
							<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['Job'] .'</td>
                        	<td>'. $Mother[0]['PROF'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['JobNature'] .'</td>
                        	<td>'. $MotherJobNature_val .'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['JobTitle'] .'</td>
                        	<td>'. $MotherJobTitle_val .'</td>
						</tr>
						<tr>
						    <td class="field_title_short">'. $Lang['StudentRegistry']['MaritalStatus'] .'</td>
                         	<td>'. $MotherMarry_val .'</td>
                         	<td class="field_title_short">'. $Lang['StudentRegistry']['EducationLevel'] .'</td>
                        	<td>'. $MotherEduLevel_val .'</td>
							<td colspan="2">&nbsp;</td>
						</tr>
                        <tr>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['HomePhone'] .'</td>
                        	<td>'. $Mother[0]['TEL'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['CellPhone'] .'</td>
                         	<td>'. $Mother[0]['MOBILE'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['OfficeNumber'] .'</td>
                         	<td>'. $Mother[0]['OFFICE_TEL'].'</td>						
						</tr>
						<tr>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['ContactAddress'] .'</td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Door'] .')</em></td>
                  		      				<td><span class="row_content">'. $Mother[0]['POSTAL_ADDR1'].'</span></td>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Road'] .')</em></td>
                  		      				<td><span class="row_content">'. $Mother[0]['POSTAL_ADDR2'].'</span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Garden'] .')</em></td>
                        	    			<td><span class="row_content">'. $Mother[0]['POSTAL_ADDR3'].'</span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>('. $Lang['StudentRegistry']['Town'] .')</em></td>
                        	    			<td><span class="row_content">'. $Mother[0]['POSTAL_ADDR4'].'</span></td>
                        	    			<td><em>('. $Lang['StudentRegistry']['State'] .')</em></td>
                        	    			<td><span class="row_content">'. $Mother[0]['POSTAL_ADDR5'].'</span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Postal'] .')</em></td>
                        	    			<td><span class="row_content">'. $Mother[0]['POSTAL_ADDR6'].'</span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
						<tr>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Postal'] .'</td>
							<td>'. $Mother[0]['POST_CODE'].'</td>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Email'] .'</td>
							<td>'. $Mother[0]['EMAIL'].'</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title">'. $Lang['StudentRegistry']['GuardianInfo'] .'</span> -</em>
					<p class="spacer"></p>
				</div>
        		<table id="GuardianInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr id="GuardNoInfo" '. ($Guard? "style=\"Display:none\"" : "").'>
							<td colspan="6">'.$Lang['StudentRegistry']['NoInformation'].'</td>
						</tr>
						<tr class="GuardRelationInfo" '. ($Guard? "" : "style=\"Display:none\"").'>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Relationship'] .'</td>
							<td>'. $GuardRelation_val .'</td>
							<td class="GuardInfo field_title_short" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>'. $Lang['StudentRegistry']['RelationOther'] .'</td>
                        	<td class="GuardInfo" colspan="3">'.$Guard[0]['G_RELATION'].'</td>
						</tr>


						<tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['ChineseName'] .'</td>
                            <td>'.$Guard[0]['NAME_C'].'</td>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['EnglishName'] .'</td>
                            <td>'.$Guard[0]['NAME_E'].'</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['Gender'] .'</td>
                        	<td>'. $GuardGender_val .'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['StayTogether'] .'</td>
                         	<td colspan="3">'. $GuardStay_val .'</td>
                        </tr>
                        <tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['Job'] .'</td>
                        	<td>'.$Guard[0]['PROF'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['JobNature'] .'</td>
                        	<td>'. $GuardJobNature_val .'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['JobTitle'] .'</td>
                        	<td>'. $GuardJobTitle_val .'</td>
 						</tr>
 						<tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
                         	<td class="field_title_short">'. $Lang['StudentRegistry']['EducationLevel'] .'</td>
                        	<td>'. $GuardEduLevel_val .'</td>
							<td colspan="4">&nbsp;</td>
 						</tr>
                        <tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['HomePhone'] .'</td>
                        	<td>'.$Guard[0]['TEL'].'</td>
                        	<td class="field_title_short">'. $Lang['StudentRegistry']['CellPhone'] .'</td>
                         	<td>'.$Guard[0]['MOBILE'].'</td>
						    <td class="field_title_short">'. $Lang['StudentRegistry']['OfficeNumber'] .'</td>
                         	<td>'.$Guard[0]['OFFICE_TEL'].'</td>
						</tr>
						<tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
                            <td class="field_title_short">'. $Lang['StudentRegistry']['ContactAddress'] .'</td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Door'] .')</em></td>
                  		      				<td><span class="row_content">'. $Guard[0]['POSTAL_ADDR1'].'</span></td>
                  	  	    				<td><em>('. $Lang['StudentRegistry']['Road'] .')</em></td>
                  		      				<td><span class="row_content">'. $Guard[0]['POSTAL_ADDR2'].'</span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Garden'] .')</em></td>
                        	    			<td><span class="row_content">'. $Guard[0]['POSTAL_ADDR3'].'</span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>('. $Lang['StudentRegistry']['Town'] .')</em></td>
                        	    			<td><span class="row_content">'. $Guard[0]['POSTAL_ADDR4'].'</span></td>
                        	    			<td><em>('. $Lang['StudentRegistry']['State'] .')</em></td>
                        	    			<td><span class="row_content">'. $Guard[0]['POSTAL_ADDR5'].'</span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>('. $Lang['StudentRegistry']['Postal'] .')</em></td>
                        	    			<td><span class="row_content">'. $Guard[0]['POSTAL_ADDR6'].'</span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
						<tr class="GuardInfo" '. ($Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\"").'>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Postal'] .'</td>
							<td>'. $Guard[0]['POST_CODE'].'</td>
							<td class="field_title_short">'. $Lang['StudentRegistry']['Email'] .'</td>
							<td>'.$Guard[0]['EMAIL'].'</td>
							<td colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>';
				
echo $tableContent;

?>