<?
// Modifying by: Thomas

############# Change Log [Start] ################
#
# Date:	2010-07-07 (Henry Chow)
#		- add GET_IMPORT_TXT_WITH_REFERENCE(), Apply new standard of import
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$li = new libdb();
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";


$fail = 0;
$success = 0;

# delete the temp data in temp table 
$sql = "SELECT * FROM TEMP_STUDENT_REGISTRY_MALAYSIA_IMPORT WHERE InputBy=$UserID";
$data = $lsr->returnArray($sql);

if(sizeof($data)==0) {
	intranet_closedb();
	header("Location: import_malaysia.php?xmsg=import_no_record");	
	exit();
}

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass");

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

//debug_pr($data);
$linterface->LAYOUT_START(); 

for($i=0; $i<sizeof($data); $i++)
{
	$result = array();
	
	list($rec_id, $StudentID, $CLASS, $C_NO, $UserLogin, $STUD_ID, $NAME_C, $NAME_E, $SEX, $B_DATE, $B_PLACE, $NATION, $ANCESTRAL_HOME, $RACE, $ID_NO, $BIRTH_CERT_NO, $RELIGION, $FAMILY_LANG, $PAYMENT_TYPE, $TEL, $EMAIL, $LODGING, $ADDRESS1, $ADDRESS2, $ADDRESS3, $ADDRESS4, $ADDRESS5, $ADDRESS6, $ENTRY_DATE, $SIBLINGS, $PRI_SCHOOL, $FATHER_C, $FATHER_E, $F_PROF, $F_JOB_NATURE, $F_JOB_TITLE, $F_MARITAL, $F_EDU_LEVEL, $F_TEL, $F_MOBILE, $F_OFFICE, $F_POSTAL_ADDR1, $F_POSTAL_ADDR2, $F_POSTAL_ADDR3, $F_POSTAL_ADDR4, $F_POSTAL_ADDR5, $F_POSTAL_ADDR6, $F_POSTAL_CODE, $F_EMAIL, $MOTHER_C, $MOTHER_E, $M_PROF, $M_JOB_NATURE, $M_JOB_TITLE, $M_MARITAL, $M_EDU_LEVEL, $M_TEL, $M_MOBILE, $M_OFFICE, $M_POSTAL_ADDR1, $M_POSTAL_ADDR2, $M_POSTAL_ADDR3, $M_POSTAL_ADDR4, $M_POSTAL_ADDR5, $M_POSTAL_ADDR6, $M_POSTAL_CODE, $M_EMAIL, $GUARDIAN_C, $GUARDIAN_E, $G_SEX, $GUARD, $G_REL_OTHERS, $LIVE_SAME, $G_PROF, $G_JOB_NATURE, $G_JOB_TITLE, $G_EDU_LEVEL, $G_TEL, $G_MOBILE, $G_OFFICE, $G_POSTAL_ADDR1, $G_POSTAL_ADDR2, $G_POSTAL_ADDR3, $G_POSTAL_ADDR4, $G_POSTAL_ADDR5, $G_POSTAL_ADDR6, $G_POSTAL_CODE, $G_EMAIL) = $data[$i];

	# update INTRANET_USER
	$studentAry = array(
						"ChineseName"	=>$NAME_C,
						"EnglishName"	=>$NAME_E,
						"Gender"		=>$SEX,
						"DateOfBirth"	=>$B_DATE,
						"HomeTelNo"		=>$TEL
						);
	$result[] = $lsr->updateStudentInfo($studentAry, $StudentID);
	
	# insert into STUDENT_REGISTRY_STUDENT
	$dataAry = array(
					"STUD_ID"			=>$STUD_ID,
					"B_PLACE"			=>$B_PLACE,
					"NATION"			=>$NATION,
					"ANCESTRAL_HOME"	=>$ANCESTRAL_HOME,
					"RACE"				=>$RACE,
					"ID_NO"				=>$ID_NO,
					"BIRTH_CERT_NO"		=>$BIRTH_CERT_NO,
					"RELIGION"			=>$RELIGION,
					"FAMILY_LANG"		=>$FAMILY_LANG,
					"PAYMENT_TYPE"		=>$PAYMENT_TYPE,
					"EMAIL"				=>$EMAIL,
					"LODGING"			=>$LODGING,
					"ADDRESS1"			=>$ADDRESS1,
					"ADDRESS2"			=>$ADDRESS2,
					"ADDRESS3"			=>$ADDRESS3,
					"ADDRESS4"			=>$ADDRESS4,
					"ADDRESS5"			=>$ADDRESS5,
					"ADDRESS6"			=>$ADDRESS6,
					"ADMISSION_DATE"	=>$ENTRY_DATE,
					"BRO_SIS_IN_SCHOOL"	=>$SIBLINGS,
					"PRI_SCHOOL"		=>$PRI_SCHOOL
					);
	$result[] = $lsr->updateStudentRegistry_Simple_Mal($dataAry, $StudentID);
	
	# insert into STUDENT_REGISTRY_PG - FATHER
	$fatherAry = array(
						"GUARD"			 =>"F",
						"NAME_C"		 =>$FATHER_C,
						"NAME_E"		 =>$FATHER_E,
						"G_GENDER"		 =>"M",
						"PROF"			 =>$F_PROF,
						"JOB_NATURE"	 =>$F_JOB_NATURE,
						"JOB_TITLE"		 =>$F_JOB_TITLE,
						"MARITAL_STATUS" =>$F_MARITAL,
						"EDU_LEVEL"		 =>$F_EDU_LEVEL,
						"TEL"			 =>$F_TEL,
						"MOBILE"		 =>$F_MOBILE,
						"OFFICE_TEL"	 =>$F_OFFICE,
						"POSTAL_ADDR1"	 =>$F_POSTAL_ADDR1,
						"POSTAL_ADDR2"	 =>$F_POSTAL_ADDR2,
						"POSTAL_ADDR3"	 =>$F_POSTAL_ADDR3,
						"POSTAL_ADDR4"	 =>$F_POSTAL_ADDR4,
						"POSTAL_ADDR5"	 =>$F_POSTAL_ADDR5,
						"POSTAL_ADDR6"	 =>$F_POSTAL_ADDR6,
						"POST_CODE"		 =>$F_POSTAL_CODE,
						"EMAIL"			 =>$F_EMAIL				
				);
	//$lsr->removeStudentRegistry_PG($StudentID, "F");			
	$result[] = $lsr->updateStudentRegistry_PG($fatherAry, "F", $StudentID);
	
	# insert into STUDENT_REGISTRY_PG - MOTHER
	$motherAry = array(
					"GUARD"			 =>"M",
					"NAME_C"		 =>$MOTHER_C,
					"NAME_E"		 =>$MOTHER_E,
					"G_GENDER"		 =>"F",
					"PROF"			 =>$M_PROF,
					"JOB_NATURE"	 =>$M_JOB_NATURE,
					"JOB_TITLE"		 =>$M_JOB_TITLE,
					"MARITAL_STATUS" =>$M_MARITAL,
					"EDU_LEVEL"		 =>$M_EDU_LEVEL,
					"TEL"			 =>$M_TEL,
					"MOBILE"		 =>$M_MOBILE,
					"OFFICE_TEL"	 =>$M_OFFICE,
					"POSTAL_ADDR1"	 =>$M_POSTAL_ADDR1,
					"POSTAL_ADDR2"	 =>$M_POSTAL_ADDR2,
					"POSTAL_ADDR3"	 =>$M_POSTAL_ADDR3,
					"POSTAL_ADDR4"	 =>$M_POSTAL_ADDR4,
					"POSTAL_ADDR5"	 =>$M_POSTAL_ADDR5,
					"POSTAL_ADDR6"	 =>$M_POSTAL_ADDR6,
					"POST_CODE"		 =>$M_POSTAL_CODE,
					"EMAIL"			 =>$M_EMAIL					
				);
	//$lsr->removeStudentRegistry_PG($StudentID, "M");			
	$result[] = $lsr->updateStudentRegistry_PG($motherAry, "M", $StudentID);
	
	# insert into STUDENT_REGISTRY_PG - GUARDIAN
	$guardianAry = array(
					"GUARD"			 =>$GUARD,
					"NAME_C"		 =>$GUARDIAN_C,
					"NAME_E"		 =>$GUARDIAN_E,
					"G_GENDER"		 =>$G_SEX,
					"G_RELATION"	 =>$G_REL_OTHERS,
					"LIVE_SAME"		 =>$LIVE_SAME,
					"PROF"			 =>$G_PROF,
					"JOB_NATURE"	 =>$G_JOB_NATURE,
					"JOB_TITLE"		 =>$G_JOB_TITLE,
					"MARITAL_STATUS" =>"",
					"EDU_LEVEL"		 =>$G_EDU_LEVEL,
					"TEL"			 =>$G_TEL,
					"MOBILE"		 =>$G_MOBILE,
					"OFFICE_TEL"	 =>$G_OFFICE,
					"POSTAL_ADDR1"	 =>$G_POSTAL_ADDR1,
					"POSTAL_ADDR2"	 =>$G_POSTAL_ADDR2,
					"POSTAL_ADDR3"	 =>$G_POSTAL_ADDR3,
					"POSTAL_ADDR4"	 =>$G_POSTAL_ADDR4,
					"POSTAL_ADDR5"	 =>$G_POSTAL_ADDR5,
					"POSTAL_ADDR6"	 =>$G_POSTAL_ADDR6,
					"POST_CODE"		 =>$G_POSTAL_CODE,
					"EMAIL"			 =>$G_EMAIL					
				);
	//$lsr->removeStudentRegistry_PG($StudentID, "G");			
	$result[] = $lsr->updateStudentRegistry_PG($guardianAry, "G", $StudentID);

	if(in_array(false, $result)) {
		$fail++;	
	} else {
		$sql = "DELETE FROM TEMP_STUDENT_REGISTRY_MALAYSIA_IMPORT WHERE StudentID=$StudentID AND InputBy=$UserID";
		$lsr->db_db_query($sql);
		$success++;	
	}
	//debug_pr($result);
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
/*
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". GetCurrentAcademicYear() ."</td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $success ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($fail ? "<font color='red'>":"") . $fail . ($fail ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";


$import_button = $linterface->GET_ACTION_BTN($button_finish, "button", "window.location='class.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_malaysia.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass'");

?>

<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
