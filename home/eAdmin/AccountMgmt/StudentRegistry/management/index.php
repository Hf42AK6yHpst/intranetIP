<?php
# using: yat

#####################################
#
#	Date:	2013-03-25	YatWoon
#			Add HK version (requested by UCCKE)
#
#####################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if($plugin['StudentRegistry_Macau'])
	header("Location: macau/information/");
else if($plugin['StudentRegistry_Malaysia'])
	header("Location: malaysia/information/");
else if($plugin['StudentRegistry_HongKong'])
	header("Location: hk/information/");	
else 
	header("Location: /");
?>