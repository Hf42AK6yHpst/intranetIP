<?php
#using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$userID = (is_array($userID)? $userID[0]:$userID);

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();

$result = $lsr -> RETRIEVE_STUDENT_INFO_BASIC_MACAU($userID);

#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "Gender", $gender_val);

#School code related info
$schoolcode_ary  	 = $lsr -> MACAU_S_CODE_DATA_ARY();
$schoolcode_val	 	 = $result[0]['S_CODE']? $lsr -> RETRIEVE_DATA_ARY($schoolcode_ary, $result[0]['S_CODE']) : "--";
$schoolcode_edit_ary = $lsr -> RETRIEVE_DATA_ARY($schoolcode_ary);
$schoolcode_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($schoolcode_edit_ary, "SchoolCode", $schoolcode_val);

#Past Enrollment Information
$Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($userID);

# Registry Status
switch($result[0]['RecordStatus']) {
	case 0: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
			break;
	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['StatusApproved'];
			break;
	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
			break;
	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['StatusLeft'];
			break;
	default: $ownRegistryStatus = "---";	
}

if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}

#Modified By
$ModifiedBy = $result[0]['ModifyBy']? $lsr -> RETRIEVE_MODIFIED_BY_INFO($result[0]['ModifyBy']) : "";

//debug_pr($result[0]['Gender']);
//debug_pr($gender_edit_ary);

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";


$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
# need to modified here!
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($result[0]['ClassName']." ".$Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[0]['YearClassID']);
$PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result[0]['EnglishName'] : $result[0]['ChineseName']), "");

#Content Top Tool Button
$Content_Top_BTN[] = array($Lang['StudentRegistry']['AdvInfo'], "view_adv.php?AcademicYearID=$AcademicYearID&userID=$userID");
$Content_Top_BTN[] = array($Lang['StudentRegistry']['BasicInfo'], "view.php?AcademicYearID=$AcademicYearID&userID=$userID", "", true);

#Save and Cancel Button
$SaveBtn   = $linterface->GET_ACTION_BTN($button_save, "button", "javascript:show_arg()", "SaveBtn", "style=\"display: none\"");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:ResetInfo()", "CancelBtn", "style=\"display: none\"");

//$linterface->LAYOUT_START();
if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
$linterface->LAYOUT_START(urldecode($msg));
?>
<script language="javascript">
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#EditBtn').attr('style', 'visibility: hidden');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');
		$(':text[value="--"]').val('');
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', '');
		$('#EditBtn').attr('style', '');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');
		$(':text[value=""]').val('--');
	}
	
	function show_arg()
	{
		if(!checkform()) return false;
		//var abc = $('[name="form1"]').serialize();
		//alert(abc);
		document.form1.submit();
	}
	
	function checkform()
	{
		var obj = document.form1;
		
		if(!check_text(obj.StudentID, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['StudentID']?>")) return false;
		if(!check_text(obj.DSEJ_Number, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['DSEJ_Number']?>")) return false;
		if(!check_text(obj.ChineseName, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['ChineseName']?>")) return false;
		if(obj.Gender[0].checked == false && obj.Gender[1].checked == false){
			alert('<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['Gneder']?>');
			return false;
		}
		if(!check_text(obj.HomePhone, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['HomePhone']?>")) return false;
		if(!check_text(obj.SchoolCode, "<?= $Lang['StudentRegistry']['PleaseEnter']." ".$Lang['StudentRegistry']['SchoolCode']?>")) return false;
		
		return true;
	}
	
	$(document).ready(function()
	{
		$(':text[value=""]').val('--');
	});
</script>
<form name="form1" method="POST" action="view_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<p class="spacer"></p>
			<div class="content_top_tool">
				<?= $linterface->GET_CONTENT_TOP_BTN($Content_Top_BTN)?>
			</div>
			<div class="table_board">
        		<div class="table_row_tool row_content_tool">
                	<input id="EditBtn" type="button" name="submit4" class="formsmallbutton" onclick="javascript:EditInfo()" value="<?= $Lang['StudentRegistry']['Edit'] ?>">
                </div>
                <p class="spacer"></p>
                <div class="form_sub_title">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
                <table class="form_table">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['StudentID'] ?></td>
                        	<td><input type="text" name="StudentID" value="<?= $result[0]['STUD_ID']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['DSEJ_Number'] ?></td>
                        	<td><input type="text" name="DSEJ_Number" value="<?= $result[0]['CODE']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input type="text" name="ChineseName" value="<?= $result[0]['ChineseName']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ForeignName'] ?></td>
                            <td><input type="text" name="EnglishName" value="<?= $result[0]['EnglishName']?>" readonly="readonly" style="border: 0px"/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td colspan="5"><span class="Edit_Hide"><?= $gender_val ?></span><?= $gender_RBL_html ?></td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                            <td colspan="5"><input type="text" name="HomePhone" value="<?= $result[0]['HomeTelNo']?>" readonly="readonly" style="border: 0px"/></td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['StudyAt'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['SchoolCode'] ?>)</em></td>
                  		      				<td><span class="row_content"><span class="Edit_Hide"><?= $schoolcode_val ?></span><?= $schoolcode_DDL_html ?></span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Grade'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['YearName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Class'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['ClassName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['InClassNumber'] ?>)</em></td>
                        	    			<td><span class="row_content"><?= $result[0]['ClassNumber']?></span></td>
                        	    		</tr>
                        	    	</table>
								</div>
                            </td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['PastEnroll'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table> 
                        				<?= $Past_Enroll_html ?>
                        			</table>
                        		</div>
                        	</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['RegistryStatus'] ?></td>
                            <td colspan="5"><?=$ownRegistryStatus?></td>
                        </tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title" style="display: none">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['ContactPersonInfo'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
        		<table class="form_table" style="display: none">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                        	<td>陳大明</td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                        	<td>Chan Tai Ming</td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['PortugueseName']?></td>
                        	<td>Chan Tai Ming</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['HomePhone'] ?></span></td>
                        	<td>23456789</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['CellPhone'] ?></span></td>
                         	<td>98765432</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
						<tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['Relationship'] ?></span></td>
                        	<td>父子</td>
                        	<td>&nbsp;</td>
                         	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark"><?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result[0]['DateModified']? $result[0]['DateModified']."(".$ModifiedBy.")" : "--"?></span>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $CancelBtn ?>
                </div>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<script language="javascript">
	document.getElementById('SaveBtn').style.visibility = "hidden";
	document.getElementById('CancelBtn').style.visibility = "hidden";

</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>