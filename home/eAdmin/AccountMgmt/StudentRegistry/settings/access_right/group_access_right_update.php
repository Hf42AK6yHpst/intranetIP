<?php
// editing by : henry chow
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$GroupTitle = trim(urldecode(stripslashes($_REQUEST['GroupTitle'])));
$GroupID = $_REQUEST['GroupID'];
$GroupDescription = trim(urldecode(stripslashes($_REQUEST['GroupDescription'])));
$Right = (is_array($_REQUEST['Right']))? $_REQUEST['Right']:array();

if(trim($GroupTitle) == "")
{
	header("Location: index.php");
	intranet_closedb();
	exit();
}
//debug_pr($_POST); exit;
$lsr->Start_Trans();
$GroupID = $lsr->Insert_Group_Access_Right($GroupID, $GroupTitle, $GroupDescription, $Right);

if($GroupID != false) {
	$lsr->Commit_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$lsr->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}
//echo $GroupID.'/'.$Msg; exit;
header("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>