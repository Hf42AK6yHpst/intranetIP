<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();


$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$GroupID = $_REQUEST['GroupID'];

$lsr->Start_Trans();
if ($lsr->Delete_Group($GroupID)) {
	$lsr->Commit_Trans();
	echo $Lang['StaffAttendance']['DeleteGroupSuccess'];
}
else {
	$lsr->RollBack_Trans();
	echo $Lang['StaffAttendance']['DeleteGroupFail'];
}

intranet_closedb();
?>