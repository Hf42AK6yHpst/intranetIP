<?php 

# using: Philips

################# Change Log [Start] ############
#
#   2019-04-24 [Philips]
#       - create file
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lrs = new libstudentregistry();

$form_code = array();
# Student Information
array_push($form_code, "ClassName", "ClassNumber", "WebSAMSRegNo", "STU_EngName", "STU_EngFirstName", "STU_EngLastName", "STU_ChiName", "STU_ChiFirstName",
    "STU_ChiLastName", "STU_ChiCommercialCode1", "STU_ChiCommercialCode2", "STU_ChiCommercialCode3", "STU_ChiCommercialCode4",
    "STU_Gender", "STU_DOB", "STU_NationalityCode", "STU_EthnicityCode", "STU_IDTypeCode", "STU_IDNo", "STU_HomeLangCode",
    "STU_ReligionCode", "STU_Church", "STU_HomePhone", "STU_Mobile", "STU_Email",
    "STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
    "STU_AddressStreet", "STU_AddressDistrictCode", "STU_AddressAreaCode");
# Last School
array_push($form_code, "STU_LastSchoolEngName", "STU_LastSchoolChiName");

# Brothers & sisters currently studying in school
array_push($form_code, "STU_BroSisLoginID1", "STU_BroSisName1", "STU_BroSisRelationship1", "STU_BroSisLoginID2", "STU_BroSisName2", "STU_BroSisRelationship2", "STU_BroSisLoginID3", "STU_BroSisName3", "STU_BroSisRelationship3");

# Guardian
for($i=1;$i<=2;$i++)
{
    array_push($form_code, "GRD_TypeCode".$i, "GRD_TypeOther".$i, "GRD_Email".$i, "GRD_EngName".$i, "GRD_ChiName".$i, "GRD_Occupation".$i,
        "GRD_DayPhone".$i, "GRD_Mobile".$i, "GRD_ReligionCode".$i, "GRD_Church".$i,
        "GRD_AddressFlat".$i, "GRD_AddressFloor".$i, "GRD_AddressBlock".$i, "GRD_AddressBuilding".$i, "GRD_AddressEstate".$i,
        "GRD_AddressStreet".$i, "GRD_AddressDistrictCode".$i, "GRD_AddressAreaCode".$i);
}

# Other Contact Person
array_push($form_code, "OtherConact_TitleCode", "OtherConact_Relationship", "OtherConact_EngName", "OtherConact_ChiName",
    "OtherConact_DayPhone", "OtherConact_Mobile");

$columnID = $_GET['ColumnID'];
$code = $_GET['Code'];

if(in_array($code, $form_code)){
    echo false;
} else {
    $result = $lrs->checkCustomColumnCode($code, $columnID);
    echo $result;
}

intranet_closedb();
?>