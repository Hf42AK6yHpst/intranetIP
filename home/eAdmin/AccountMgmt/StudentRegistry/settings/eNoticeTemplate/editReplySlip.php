<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lnotice = new libnotice();
$lsr = new libstudentregistry();

if (!$lnotice->disabled)
{
    $MODULE_OBJ['title'] = $i_Notice_ReplyContent;
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
	
	if($TemplateID)
	{
		$tempDetails = $lsr->retrieveTemplateDetails($TemplateID);
		list($Module,$CategoryID,$Title,$Subject,$Content,$ReplySlip,$RecordType,$RecordStatus,$ReplySlipContent) = $tempDetails[0];
	}
		
	if($cStr)
	{
		$cStr = stripslashes($cStr);
		$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);	
		$ReplySlipContent = $cStr;
	}
	
?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js"></script>

<script type="text/javascript">
var callback_changeGenVariables = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('genVariableDiv').innerHTML = tmp_str;
	}
}

function changeGenVariables()
{
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_genvariable.php";
	
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_changeGenVariables);
}

function copyback()
{
	var field = FCKeditorAPI.GetInstance('Content');
	var content_var = field.GetHTML(true);
	if(content_var=="")
	{
		alert('<?=$i_Form_pls_fill_in?>');
	}
	else
	{
 		window.opener.document.form1.cStr.value = content_var;
 		self.close();
	}
}
</script>

<br /> 
 
<form name="ansForm" action="">
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
<tr valign="top">
	<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eNotice['ReplySlipType']?></td>
	<td class="tabletext" colspan="2"><?=$eNotice['ContentBase']?></td>
</tr>

<tr valign="top">
	<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Content']?></td>
	<td><div id="genVariableDiv"><?=$select?></div></td>
</tr>

<tr valign="top">
	<td align="center" colspan="3">
		<?
		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		$objHtmlEditor = new FCKeditor ( 'Content' , "100%", "250", "","", $ReplySlipContent);
		$objHtmlEditor->Create();
		?>
	</td>
	
</tr>
	<script language="javascript">
	function FillIn()
	{
		var field = FCKeditorAPI.GetInstance('Content');
		field.InsertHtml(document.ansForm.genVariable.value);
	}
	</script>
<tr>
	<td class="dotline" colspan="3"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center" colspan="3">
		<?= $linterface->GET_ACTION_BTN($button_save, "submit", "copyback(); return false;","submit2") ?>
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
	</td>
</tr>
</table>                   

</form>
<script>changeGenVariables()</script>
<?php
}
else
{
    header ("Location: /");
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>