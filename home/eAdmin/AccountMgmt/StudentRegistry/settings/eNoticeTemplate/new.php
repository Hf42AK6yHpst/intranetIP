<?php
# using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lsr = new libstudentregistry();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$catTmpArr = $lsr->TemplateCategory();
if($CategoryID=='')
{
	$CategoryID	= $catTmpArr[0][0];
}

if(is_array($catTmpArr))
{
	foreach($catTmpArr as $Key=>$Value)
	{
		$catArr[] = array($Key,$Value);
	}
}
$catArr = array_merge(array(array("",$Lang['SysMgr']['Homework']['PleaseSelect'])), $catArr);
$catSelection = getSelectByArray($catTmpArr, ' name="CategoryID" id="CategoryID" onChange="changeGenVariables(this.value)"', $CategoryID, "", 1);

$editReplySlipBtn = $linterface->GET_BTN($button_edit, 'button', "newWindow('select_reply_slip_type.php',1)", $ParName, $ParOtherAttribute);
$previewBtn = $linterface->GET_BTN($button_preview, "button","newWindow('preview.php',10)");

# menu highlight setting
$CurrentPage = "Setting_RegistryNoticeTemplate";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['eNoticeTemplate']);

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['eNoticeTemplate'], "index.php");
$PAGE_NAVIGATION[] = array($button_new_template);

$CurrentPageArr['StudentRegistry'] = 1;

# Left menu 
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script type="text/javascript">
var callback_changeGenVariables = {
	success: function ( o )
    {
	    if(document.form1.temp_form_display_alert.value==1)
	    {
		    alert("<?=$Lang['eDiscipline']['ChangeReasonNotice']?>");
	    }
	    document.form1.temp_form_display_alert.value = 1;
	    var tmp_str = o.responseText;
	    document.getElementById('genVariableDiv').innerHTML = tmp_str;
	}
}

function changeGenVariables()
{
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_genvariable.php";
	
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_changeGenVariables);
}

function checkForm(obj) 
{
	if(!check_text(obj.Title,'<?=$i_Discipline_System_Discipline_Template_Title_JS_warning?>'))
	{
		return false
	}
	if(!check_text(obj.Subject,'<?=$i_Discipline_System_Discipline_Template_Subject_JS_warning?>'))
	{
		return false
	}
	/*
	if((obj.Content.value==""))
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	*/
	var field = FCKeditorAPI.GetInstance('Content');
	var content_var = field.GetHTML(true);
	if(content_var=="")
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	
	return true
}
						
function FillIn()
{
	var field = FCKeditorAPI.GetInstance('Content');
	field.InsertHtml(document.form1.genVariable.value);
}
</script>
						
<form name="form1" method="post" action="new_update.php" onSubmit="return checkForm(form1)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Template_Title?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT maxLength="80" name="Title" class="textboxtext">
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Reason_For_Issue?>
								</td>
								<td class="tabletext"><?=$catSelection?>
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Reply_Slip?>
								</td>
								<td class="tabletext"><INPUT type="checkbox" name="ReplySlip" id="ReplySlip"><label for="ReplySlip"><?=$i_general_yes?></label><?=$editReplySlipBtn?><?=$previewBtn?>
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Subject']?> <span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT type="text" maxLength="80" name="Subject" id="Subject" class="textboxtext">
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Status?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext">
								<input type='radio' id='RecordStatus0' name='RecordStatus' value=0 checked><label for="RecordStatus0"><?=$i_Discipline_System_Discipline_Template_Draft?></label>
								<input type='radio' id='RecordStatus1' name='RecordStatus' value=1><label for="RecordStatus1"><?=$i_Discipline_System_Discipline_Template_Published?></label>
								</td>
							</tr>
						</table>
						<br><br>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Content']?> <span class="tabletextrequire">* </span></td>
								<td>
									<div id="genVariableDiv"></div>
								</td>
							</tr>
						</table>
						
						
						
						<?
						include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
						$objHtmlEditor = new FCKeditor ( 'Content' , "100%", "320");
						$objHtmlEditor->Create();

						?>
						
						
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="merit" value="<?= $merit?>">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">
<input type="hidden" name="cStr" value="">
<input type="hidden" name="temp_form_display_alert" value="0">
<script>changeGenVariables()</script>

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


