<?php
# using: 

########## Change Log [Start] #############
#
#   Date:   2019-05-13 Cameron
#           - fix potential sql injection problem by enclosing var with apostrophe
#
#	Date:	2014-08-21	Bill
#			Modify query to get required fields from db
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] for export file with CardID - eEnrolment
#
########## Change Log [End] #############

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();
//$li = new libdbdump();
$laccount = new libaccountmgmt();
$libps = new libuserpersonalsettings();

$filename = "eclass_user.csv";

     
     if (!isset($default) || $default != 1) {
		$FieldArray = $Fields;
		$FieldNameAry = $Fields;
		$joins = "";
	} else {
		$FieldArray = array("U.UserLogin", "U.UserPassword", "U.UserEmail", "U.EnglishName", "U.ChineseName", "U.NickName", "U.Gender", "U.HomeTelNo", "U.MobileTelNo", "U.FaxNo", "U.Barcode", "U.Remark", "U.DateOfBirth", "U.WebSAMSRegNo", "U.HKJApplNo", "U.Address");
		$FieldNameAry = array("UserLogin", "UserPassword", "UserEmail", "EnglishName", "ChineseName", "NickName", "Gender", "HomeTelNo", "MobileTelNo", "FaxNo", "Barcode", "Remark", "DateOfBirth", "WebSAMSRegNo", "HKJApplNo", "Address");
		$joins = "U.";
		
		if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])|| (isset($plugin['eEnrollment'])&& $plugin['eEnrollment']))
		{
			$FieldArray = array_merge($FieldArray, array("U.CardID"));
			$FieldNameAry = array_merge($FieldNameAry, array("CardID"));
		}
		
	
	
         if($special_feature['ava_hkid'])
         {
			$FieldArray = array_merge($FieldArray, array("U.HKID"));
			$FieldNameAry = array_merge($FieldNameAry, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			$FieldArray = array_merge($FieldArray, array("U.STRN"));
			$FieldNameAry = array_merge($FieldNameAry, array("STRN"));
         }
	     
	     if($plugin['medical']){
	     	$FieldArray = array_merge($FieldArray, array("P.stayOverNight"));
			$FieldNameAry = array_merge($FieldNameAry, array("StayOverNight"));
	     }
	     $FieldArray = array_merge($FieldArray, array("P.Nationality","P.PlaceOfBirth","P.AdmissionDate"));
		 $FieldNameAry = array_merge($FieldNameAry, array("Nationality","PlaceOfBirth","AdmissionDate"));
        
//        }
        $ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
		$totalCategory = count($ExtraInfoCatInfo);
	
		for($i=0; $i<$totalCategory; $i++) {
			$catname = Get_Lang_Selection($ExtraInfoCatInfo[$i]['CategoryNameCh'], $ExtraInfoCatInfo[$i]['CategoryNameEn']);
			$FieldArray[] = $ExtraInfoCatInfo[$i]['CategoryCode'];	
			$FieldNameAry[] = $ExtraInfoCatInfo[$i]['CategoryCode'];	
		}
		
		$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
		for($a=0; $a<count($BlissFieldAry); $a++) {
			$FieldArray[] = $BlissFieldAry[$a];
			$FieldNameAry[] = $BlissFieldAry[$a];	
		}	
	
     }
	
    $conds = " ".$joins."RecordType = ".USERTYPE_STUDENT." AND ";
    
	if($targetClass!="0") {
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$studentAry = $lclass->storeStudent(0,$targetClass);
		
		if(sizeof($studentAry)>0) {
			$conds .= " ".$joins."UserID IN (".implode(',', $studentAry).") AND ";
		}
	}		

     

	 # student
	 
	 	if($userList && $notInList) {	// all students
			// no addition condition	
		} else {
			
			$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'";
		 		$temp = $laccount->returnVector($sql);
		 		
			if($userList) {		// only export user list (with classes)
		 		if(sizeof($temp)>0) $conds .= " ".$joins."UserID IN (".implode(',',$temp).") AND ";
			} 
			else if($notInList) {	// only export user list (without classes)
				if(sizeof($temp)>0) $conds .= " ".$joins."UserID NOT IN (".implode(',',$temp).") AND ";
			} else {
				// export nothing	
			}
		}
	 
	 
	 if($recordstatus!="")
	 	$conds .= " ".$joins."RecordStatus='$recordstatus' AND ";
	 

	
	 
     $finalConds = "WHERE $conds ( ".$joins."UserLogin like '%$keyword%' OR ".$joins."UserEmail like '%$keyword%' OR ".$joins."ChineseName like '%$keyword%' OR ".$joins."EnglishName like '%$keyword%' OR ".$joins."ClassName like '%$keyword%' OR ".$joins."WebSamsRegNo like '%$keyword%')";

     $url = "/file/export/eclass-user-".session_id()."-".time().".csv";
	 
	$ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
	//debug_pr($ExtraInfoCatInfo);
	$totalCategory = count($ExtraInfoCatInfo);
	for($a=0; $a<$totalCategory; $a++) {
		$catCode[] = $ExtraInfoCatInfo[$a]['CategoryCode'];	
		$thisCatName = Get_Lang_Selection($ExtraInfoCatInfo[$a]['CategoryNameCh'], $ExtraInfoCatInfo[$a]['CategoryNameEn']);
		//echo $thisCatName.'/';
		$catInfo[$ExtraInfoCatInfo[$a]['CategoryCode']] = $ExtraInfoCatInfo[$a]['CategoryID'];
	}
	
	$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
	
	
	 $delim = "";
	 
	 foreach($FieldArray as $_field) {
	 	if(!in_array($_field, $catCode) && !in_array($_field, $BlissFieldAry)) {
				$fieldStr .= $delim.$_field;
				$delim = ", ";
	 	}
	 }
	 
	if (!isset($default) || $default != 1) {
		$sql = "SELECT $fieldStr, UserID FROM INTRANET_USER $finalConds ORDER BY ClassName, ClassNumber";
	} else {
	 $sql = "SELECT $fieldStr, U.UserID FROM INTRANET_USER U LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS P on U.UserID = P.UserID $finalConds ORDER BY U.ClassName, U.ClassNumber";
	}
	 $export_content = $laccount->returnArray($sql);

	 //debug_pr($export_content);
	 
	for($i=0; $i<count($FieldNameAry); $i++) {
		$exportContent .= $FieldNameAry[$i]."\t"; 	
	}
	$exportContent .= "\n";
	
	$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
	
	# table content
	for($i=0; $i<count($export_content); $i++) {
		$colNo = (count($export_content[$i])/2)-1;
		
		for($j=0; $j<$colNo; $j++) {
			$exportContent .= $export_content[$i][$j]."\t";	
		}
		
		 foreach($FieldArray as $_field) {
		 	
			if(in_array($_field, $catCode)) {
				$itemAry = $laccount->Get_User_Extra_Info_Item('', $catInfo[$_field]);
				
				$getItemName = Get_Lang_Selection("i.ItemNameCh", "i.ItemNameEn");
				$sql = "SELECT CONCAT($getItemName, IF((m.OtherInfo IS NOT NULL AND m.OtherInfo!=''),CONCAT('::',m.OtherInfo),'')) as ItemName FROM INTRANET_USER_EXTRA_INFO_MAPPING m INNER JOIN INTRANET_USER_EXTRA_INFO_ITEM i ON (i.ItemID=m.ItemID) WHERE m.UserID='".$export_content[$i]['UserID']."' AND i.CategoryID='".$catInfo[$_field]."'";
				$itemAry = $laccount->returnVector($sql);
				$exportContent .= ((count($itemAry)>0) ? implode(',',$itemAry) : "")."\t";
				
			}
			
			if(in_array($_field, $BlissFieldAry)) {
				$ResultAry = $libps->Get_Setting($export_content[$i]['UserID'], $BlissFieldAry);
				
				if($_field=="HowToKnowBlissWisdom") {
					$HowToAry = explode('::', $ResultAry['HowToKnowBlissWisdom']);
					$HowToID = $HowToAry[0];
					$HowToOthers = $HowToAry[1] ? '::'.$HowToAry[1] : "";
					
					$exportContent .= (isset($Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'][$HowToID]) ? $Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'][$HowToID].$HowToOthers : "")."\t";	
				} else {
					$exportContent .= (($ResultAry[$_field]!="0000-00-00" && $ResultAry[$_field]!="") ? $ResultAry[$_field] : "")."\t";
				}	
			} 
		 }
		 
		$exportContent .= "\n";
	}
	
	//$export_content .= stripslashes($exportContent);	
	
	$lexport->EXPORT_FILE($filename, $exportContent);   

intranet_closedb();
?>