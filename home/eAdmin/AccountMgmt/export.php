<?php
# modifying by :   

########## Change Log [Start] #############
#
#   Date:   2019-05-06 Cameron
#           customize export fields for Oaks
#
#   Date:   2018-11-20 Anna
#           Added tungwah sdas cust - PrimarySchoolCode
#
#   Date:   2018-08-21 (Cameron)
#           revised available columns for HKPF 
#
#   Date:   2018-08-15 (Henry)
#           fixed XSS
#
#   Date:   2018-08-10 Cameron
#           add Grade and Duty for HKPF
#
#	Date:	2017-06-08 Icarus
#			added hidden input for houseID
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added [HouseholdRegister] for student. [House] field is not included in specific column mode as it is not a data field of INTRANET_USER. 
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
# 									
#	Date:	2014-12-10  Bill
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4 for student user type
#
#	Date:	2014-11-17	Bill
#			Add Guardian Information
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] for eEnrolment
#
#	Date:	2013-12-13 (Fai) 
#			Add a cust attribute "stayOverNight" with $plugin['medical']
#
#	Date:	2013-12-12 (Carlos) 
#			iMail plus - $sys_custom['iMailPlus']['EmailAliasName'] Added field [EmailUserLogin] for staff user to input its own iMail plus account name
#
#	Date:	2013-11-13 (Carlos)
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3 for student user type
#
#	Date:	2013-08-13 (Carlos)
#			hide Staff code, WEBSAMS number and JUPAS application number for KIS
#
#	Date:	2102-09-27	YatWoon
#			Add Barcode
#
#	Date:	2012-08-03 Bill Mak
#			$sys_custom['Students_Export_RPG'] to allow export Group, Role, Parent
#
#	Date:	2011-11-02 YatWoon
#			Add almuni 
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	Date:	2010-09-03 Henry Chow
#			Export data according to the filters
#
########## Change Log [End] #############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$keyword = cleanHtmlJavascript($keyword);
$recordstatus = cleanHtmlJavascript($recordstatus);
$TeachingType = cleanHtmlJavascript($TeachingType);

if($sys_custom['StudentMgmt']['BlissWisdom'] && $TabID==USERTYPE_STUDENT) {
	header("Location: export_bliss_wisdon.php?TabID=$TabID&targetClass=$targetClass&recordstatus=$recordstatus&keyword=$keyword");
	exit;	
}

$linterface 	= new interface_html();

$laccount = new libaccountmgmt();

$isKIS = $_SESSION["platform"]=="KIS";

### smartcard
$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

$showWithClass = true;
if ($sys_custom["isCorporateClient"] && $sys_custom["disallowWithoutClass"]) {
	$showWithClass = false;
}

### Title ###
switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ExportTeacherAccount'];
        	$CurrentPageArr['StaffMgmt'] = 1;
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
        	$CurrentPageArr['StaffMgmt'] = 1;
        	$back_url = "./StaffMgmt/index.php";
        	 break;
        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ExportStudentAccount'];
        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
        	$back_url = "./StudentMgmt/index.php";
        	if ($showWithClass) {
        		$exportOption = "<input type='checkbox' name='userList' id='userList' value='1' checked><label for='userList'>".$Lang['AccountMgmt']['UserList']."</label>";
				if(!isset($targetClass) || $targetClass=="0") $exportOption .= "<input type='checkbox' name='notInList' id='notInList' value='1'><label for='notInList'>".$Lang['AccountMgmt']['StudentNotInClass']."</label>";
        	} else {
        		$exportOption = "<input type='hidden' name='userList' id='userList' value='1'>";
        		if(!isset($targetClass) || $targetClass=="0") $exportOption .= "<input type='hidden' name='notInList' id='notInList' value='1'>";
        	}
        	 break; 
        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ExportParentAccount'];
        	$CurrentPageArr['ParentMgmt'] = 1; 
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];
        	$back_url = "./ParentMgmt/index.php";
        	$CurrentPage = "Mgmt_Account";
        	$exportOption = "<input type='checkbox' name='userList' id='userList' value='1' checked DISABLED><label for='userList'>".$Lang['AccountMgmt']['UserList']."</label>";
			if(!isset($targetClass) || $targetClass=="0") $exportOption .= "<input type='checkbox' name='notInList' id='notInList' value='1'><label for='notInList'>".$Lang['AccountMgmt']['ParentWithoutChildInClass']."</label>";
        	 break;
        case TYPE_ALUMNI: 
        	$Title = $Lang['AccountMgmt']['ExportAlumniAccount'];
        	$CurrentPageArr['AlumniMgmt'] = 1;
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
        	$back_url = "./AlumniMgmt/index.php"; 
        default: $sample_file = GET_CSV("eclass$other_fields.csv"); break;
}

# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$goBackURL = $back_url;

### ycc custom
if ($sys_custom['yyc_pda_question'])
{
	$ycc_cust.= '<tr>';
		$ycc_cust.= '<td class="formfieldtitle" align="left" width="30%">'.$ec_yyc['yyc_export_usage'].' (school_info)</td>';
		$ycc_cust.= '<td class="tabletext">'.$linterface->GET_ACTION_BTN($button_export, "button", "jEXPORT(1)","export_yyc1"," class=\'formbutton\'").'</td>';
	$ycc_cust.= '</tr>';
	$ycc_cust.= '<tr>';
		$ycc_cust.= '<td class="formfieldtitle" align="left"  width="30%">'.$ec_yyc['yyc_export_usage'].' (account_info)</td>';
		$ycc_cust.= '<td class="tabletext">'.$linterface->GET_ACTION_BTN($button_export, "button", "jEXPORT(2)","export_yyc2"," class=\'formbutton\'").'</td>';
	$ycc_cust.= '</tr>';
}

### prepare option array
$option= array();
if($TabID==TYPE_STUDENT || $TabID==TYPE_ALUMNI) {
	if ($showWithClass) {
		$option["ClassName"] = array("ClassName",$i_ClassName);
		$option["ClassNumber"] = array("ClassNumber",$i_ClassNumber);
	}
}
$option["UserLogin"] = array("UserLogin",$i_UserLogin);
$option["UserEmail"] = array("UserEmail",$i_UserEmail);
$option["EnglishName"] = array("EnglishName",$i_UserEnglishName);

if ($sys_custom['project']['HKPF'] && $TabID==TYPE_TEACHER) {
    $option["ChineseName"] = array("ChineseName",$i_UserChineseName);
    $option["Grade"] = array("Grade",$Lang['AccountMgmt']['Grade']);
    $option["Duty"] = array("Duty",$Lang['AccountMgmt']['Duty']);
}
else if ($sys_custom['project']['CourseTraining']['Oaks'] && $TabID==TYPE_STUDENT) {
    $option["ChineseName"] = array("ChineseName",$i_UserChineseName);
    $option["Gender"] = array("Gender",$i_UserGender);
    $option["HomeTelNo"] = array("HomeTelNo",$i_UserHomeTelNo);
    $option["MobileTelNo"] = array("MobileTelNo",$i_UserMobileTelNo);
    $option["FaxNo"] = array("FaxNo",$i_UserFaxNo);
    $option["Remark"] = array("Remark",$Lang['General']['Remark']);
    $option["Address"] = array("Address",$i_UserAddress);
    $option["CardID"] = array("CardID",$Lang['iAccount']['ADACardNo']);
}
else {
    $option["FirstName"] = array("FirstName",$i_UserFirstName);
    $option["LastName"] = array("LastName",$i_UserLastName);
    $option["ChineseName"] = array("ChineseName",$i_UserChineseName);
    //$option["Title"] = array("IF(Title IS NULL OR TRIM(Title) = '', '', CASE Title WHEN 0 THEN 'Mr.' WHEN 1 THEN 'Miss' WHEN 2 THEN 'Mrs.' WHEN 3 THEN 'Ms.' WHEN 4 THEN 'Dr.' WHEN 5 THEN 'Prof.' ELSE '' END) AS Title ",$i_UserTitle);
    $option["Gender"] = array("Gender",$i_UserGender);
    $option["MobileTelNo"] = array("MobileTelNo",$i_UserMobileTelNo);
    // $option["ClassNumber"] = array("ClassNumber",$i_UserClassNumber);
    $option["DateOfBirth"] = array("IF(DateOfBirth IS NULL OR date_format(DateOfBirth,'%Y-%m-%d') = '0000-00-00', '', DateOfBirth) AS DateOfBirth ",$i_UserDateOfBirth);
    $option["HomeTelNo"] = array("HomeTelNo",$i_UserHomeTelNo);
    $option["OfficeTelNo"] = array("OfficeTelNo",$i_UserOfficeTelNo);
    $option["FaxNo"] = array("FaxNo",$i_UserFaxNo);
    $option["Barcode"] = array("Barcode",$Lang['AccountMgmt']['Barcode']);
    $option["ICQNo"] = array("ICQNo",$i_UserICQNo);
    $option["Address"] = array("Address",$i_UserAddress);
    if($ssoservice["Google"]["Valid"]){
    	foreach((array)$Lang['AccountMgmt']['GoogleImportFields'] as $idx=>$google_field){
    		$option["GSuite".$idx] = array("GSuite".$idx,$google_field);
    	}
    }
    $option["Country"] = array("Country",$i_UserCountry);
    $option["CardID"] = array("CardID",$i_SmartCard_CardID);

    if($sys_custom['SupplementarySmartCard']){
    	$option["CardID2"] = array("CardID2",$Lang['AccountMgmt']['SmartCardID2']);
    	$option["CardID3"] = array("CardID3",$Lang['AccountMgmt']['SmartCardID3']);
    	$option["CardID4"] = array("CardID4",$Lang['AccountMgmt']['SmartCardID4']);
    }
    if(!$isKIS) {
    	$option["WebSamsRegNo"] = array("WebSamsRegNo",$i_WebSAMS_Registration_No);
    	$option["HKJApplNo"] = array("HKJApplNo",$Lang['AccountMgmt']['HKJApplNo']);
    	$option["WebSAMSStaffCode"] = array("StaffCode",$Lang['AccountMgmt']['StaffCode']);
    }
    if($special_feature['ava_hkid']) 
    {
    	$option["HKID"] = array("HKID",$i_HKID);
    } 
}


if($special_feature['ava_strn'] && $TabID==TYPE_STUDENT && !$sys_custom['project']['CourseTraining']['Oaks']) 
{
	$option["STRN"] = array("STRN",$i_STRN);
}

if($sys_custom['BIBA_AccountMgmtCust'] && $TabID==TYPE_STUDENT){
	$option["HouseholdRegister"] = array("HouseholdRegister",$Lang['AccountMgmt']['HouseholdRegister']);
	//$option["House"] = array("House", $Lang['AccountMgmt']['House']);
}

// House is not data field of INTRANET_USER 
//if($sys_custom['BIBA_AccountMgmtCust'] && $TabID==TYPE_TEACHER){
	//$option["House"] = array("House", $Lang['AccountMgmt']['House']);
//}
if($sys_custom['StudentAccountAdditionalFields'] && $TabID==TYPE_STUDENT){
	$option["NonChineseSpeaking"] = array("NonChineseSpeaking",$Lang['AccountMgmt']['NonChineseSpeaking']);
	$option["SpecialEducationNeeds"] = array("SpecialEducationNeeds", $Lang['AccountMgmt']['SpecialEducationNeeds']);
	$option["PrimarySchool"] = array("PrimarySchool",$Lang['AccountMgmt']['PrimarySchool']);
	$option["University/Institution"] = array("University", $Lang['AccountMgmt']['UniversityInstitution']);
	$option["Programme"] = array("Programme", $Lang['AccountMgmt']['Programme']);
}

if($sys_custom['LaiShanStudentExport'] && $TabID==TYPE_STUDENT) 
{
	if ($showWithClass) {
		$option["Parent"] = array("Parent",$Lang['Identity']['Parent']);
		$option["Guardian"] = array("Guardian",$Lang['Identity']['Guardian']);
	}
}

if($sys_custom['Students_Export_RPG'] && $TabID==TYPE_STUDENT)
{
	$option["Group"] = array("Group",$Lang['AccountMgmt']['Group']);
	$option["Role"] = array("Role",$Lang['AccountMgmt']['Role']);
	$option["Parent"] = array("Parent",$Lang['Identity']['Parent']);
}

if($TabID==TYPE_STUDENT) {
    if (!$sys_custom['project']['CourseTraining']['Oaks']) {
    	foreach($Lang['AccountMgmt']['AddnPersonalSettings'] as $val){
    		$option[$val] = array($val,$Lang['AccountMgmt'][$val]);
    	}
    }
	//if($plugin['medical'])
	//{
	//	$option[$stayOverNight] = array('stayOverNight',$Lang["medical"]["stayOverNight"]);
	//}
	if ($showWithClass && !$sys_custom['project']['CourseTraining']['Oaks']) {
		# Add Guardian Information
		$option['GuardianInfo'] = array('GuardianInfo', $Lang['AccountMgmt']['GuardianInfo']);
	}
	
	if($plugin['SDAS_module']['KISMode']){
		include_once($PATH_WRT_ROOT.'/home/cees_kis/lang/'.$intranet_session_language.'.php');
		$option['KISClassType'] = array('KISClassType', $Lang['CEES']['MonthlyReport']['JSLang']['KISClassType']);
	}
	
	if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
	    $option['PrimarySchoolCode'] = array('PrimarySchoolCode',$Lang['AccountMgmt']['PrimarySchoolCode']);
	}
}



if($TabID==TYPE_PARENT) 
{
	$option["StudentInfo"] = array("StudentInfo",$Lang['AccountMgmt']['ExportStudentInfo']);
}

if($TabID==USERTYPE_ALUMNI) 
{
	$option["YearOfleft"] = array("YearOfleft",$Lang['AccountMgmt']['YearOfLeft']);
}

if($TabID==TYPE_TEACHER && $plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){
	$option['EmailUserLogin'] = array("ImapUserLogin",$Lang['Gamma']['EmailAccount']);
}

# get selectbox / checkbox
$default_selected = array("ClassName","ClassNumber","UserLogin","UserEmail","EnglishName","ChineseName");
if($sys_custom['UserExpoertWithCheckbox']) 
{
	foreach($option as $id => $thisoption)
	{
		list($val,$label) = $thisoption;
		$checked = in_array($val,$default_selected)?"checked":""; 
		$selectoption .= '<input type="checkbox" name="Fields[]" value="'.$val.'" id="'.$id.'" '.$checked.'> <label for="'.$id.'">'.$label.'</label> <br>';
	}
}
else
	$selectoption = getSelectByArray(array_values($option)," name='Fields[] ' id='Fields' multiple size=10 style='width:400px;'",$default_selected ,0,1);



$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<form method="POST" name="form1" id="frm1" action="export_update.php" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">		
						<?=$ycc_cust?>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?=$Lang['AccountMgmt']['ExportFormat']?></td>
							<td class="tabletext">
								<input type="radio" value="0" name="ExportFormat" checked id="SpecificColumn" onclick="disableSelection()"><label for="SpecificColumn"><?=$Lang['AccountMgmt']['SpecificColumns']?> </label>
								<input type="radio" value="1" name="ExportFormat" id="DefaultFormat" onclick="disableSelection()"><label for="DefaultFormat"><?=$i_UserDefaultExport?></label>
							</td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?php echo $i_export_msg2; ?></td>
							<td class="tabletext">
								<?php echo $selectoption; ?>
							</td>
						</tr>
						<? if($TabID==TYPE_STUDENT || $TabID==TYPE_PARENT) {
								if ($showWithClass) {?>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?php echo $i_Discipline_List_View; ?> </td>
							<td class="tabletext">
								<?=$exportOption?>
							</td>
						</tr>
						<?php
								} else {
									echo "<tr><td colspan='2'>" . $exportOption . "</td></tr>";
								}
							}
						?>
					</table>
				</td>
	        </tr>

			<tr>
				<td colspan="2" class="dotline">
					<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br>
					<span id="printBtn">
						<?=$linterface->GET_ACTION_BTN($button_print, "button", "click_print()","print"," class=\'formbutton\'")?>
						<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					</span>
					<?=$linterface->GET_ACTION_BTN($button_export, "button", "checksubmit()","export2"," class=\'formbutton\'")?>
					<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "document.getElementById('Fields').disabled=false;","resetbtn"," class=\'formbutton\'")?>
					<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					<?=$BackBtn?>
				</td>
			</tr>	        
	    </table>
	</td>
</tr>
</table>  
<input name="TabID" value="<?=$TabID?>" type="hidden">
<input name="keyword" value="<?=$keyword?>" type="hidden">
<input name="targetClass" value="<?=$targetClass?>" type="hidden">
<input name="recordstatus" value="<?=$recordstatus?>" type="hidden">
<input name="houseID" value="<?=$houseID?>" type="hidden">
<input name="TeachingType" value="<?=$TeachingType?>" type="hidden">
<input type=hidden name=default id=default value=''>
<input type=hidden name=filter value=1>
</form>
<br><br>
<script>
function jEXPORT(jNum)
{
	if (jNum == 2)
	{
		window.location="export_update_yyc_account.php";
	}
	else 
	{
		window.location="export_update_yyc.php";
	}
}

function click_print()
{
	var pass = 1;
	<?
	if($TabID==TYPE_STUDENT || $TabID==TYPE_PARENT) {
		echo "pass = checkExportCheckboxOption();\n";
	}	
	?>
	if(pass == 1) {
		
	    if($("select>option:selected").length>0 || $("input:checkbox:checked").length>0)
	     {
	     	$("#default").val(0);
			document.form1.action='print.php';
			document.form1.target='blank';
			document.form1.submit();
			document.form1.action='export_update.php';
			document.form1.target='_self';
		}
		else {
			alert(globalAlertMsg18);
		}
	} else {
		alert(globalAlertMsg18);	
	}
	
}

function checkExportCheckboxOption() {
<?php if ($showWithClass) { ?>
	var a = $('form #userList').is(':checked');
	var b = $('form #notInList').is(':checked');
<?php } else {?>
	var a = true;
	var b = true;
<?php } ?>
	return (a==false && b==false) ? false : true; 	
}

function checksubmit(){ 
	var pass = 1;
	<?
	if($TabID==TYPE_STUDENT || $TabID==TYPE_PARENT) {
		echo "pass = checkExportCheckboxOption();\n";
	}	
	?>
	if(pass) {
		var printdefault = $("input[name='ExportFormat']:checked").val()==1;
		
		if(printdefault)
		{
			$("#default").val(1);

			<?php 
			if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
			    echo "$('#frm1').attr('action', 'export_update_ncs.php')";
			}
			?>			
			document.form1.submit()
		}
		else
		{
			
			if($("select>option:selected").length>0 || $("input:checkbox:checked").length>0)
			{
				$("#default").val(0);
				$('#frm1').attr('action', 'export_update.php')
		    	document.form1.submit()
			}
			else
				alert(globalAlertMsg18);
	
		}
	} else {
		alert(globalAlertMsg18);
	}
}

function goBack() {
	document.form1.action = "<?=$goBackURL?>";
	document.form1.submit();	
}

function disableSelection()
{
	var val = $("input[name='ExportFormat']:checked").val();
	if(val==1)
	{
		$("select#Fields").attr("disabled","disabled");
		$("span#printBtn").hide();
	}
	else
	{
		$("span#printBtn").show();
		$("select#Fields").attr("disabled","");
	}	
}

$(function(){
	disableSelection();	
});
</script>
<?
 $linterface->LAYOUT_STOP();
?>

