<?php
// editing: 
##### Change Log [Start] #####
#	Date:	2019-06-24 Carlos
#			Empty UserPassword field.
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#	Date:	2016-05-18 (Carlos)
#			$sys_custom['DisableEmailAfterCreatedNewAccount'] - disable sending notification email after created new user account. 
#
#	Date:	2015-12-30 (Carlos) - $plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions.
#
#	Date:	2015-05-05 (Omas)
# 			improved - insert MyCalendar
#
#	Date:	2015-03-23 (Bill)
#			fixed - cannot add parent to usergroup if usergroup without default group role
#
#	Date:	2014-09-22 (Bill)
#			Add RoleID to INTRANET_USERGROUP to display default role in Group
#
#	Date:	2014-09-03 (Carlos) - iMail plus: Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
#
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	Date:	2013-03-15	Carlos
#			add shared group calendars to user
#
#	Date:	2012-08-30 YatWoon
#			add barcode
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2012-04-03 (Henry Chow)
#			allow to input HKID if flag is ON
#
#	Date:	2011-09-30  Carlos
#			store two-hashed/encrypted password
#
#	Date:	2011-04-04	YatWoon
#			change email notification subject & content data 
#
#	Date:	2010-11-19	(Henry Chow)
#			add selection of "Group" & "Role"
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}

$lauth = new libauth();
$laccount = new libaccountmgmt();
$li = new libregistry();
$lwebmail = new libwebmail();
$luser = new libuser();
$lcalendar = new icalendar();

$SettingArr = $luser->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_PARENT));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_PARENT];
if ($PasswordLength<6) $PasswordLength = 6;

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check Email duplication ###
if($email != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail='$email'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $email;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

## check HKID duplication ###
if($hkid != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKID='$hkid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $hkid;
	}
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}


if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="new.php">
		<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;
	
	} else { 
	
	$domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
	$target_email = ($email=="" ? $userlogin . "@".$domain_name : $email);
	
	if($li->UserNewAdd($userlogin, $target_email, trim($engname), trim($chiname), $status)==1) {

		
		# create & update info in INTRANET_USER
		
		$uid = $li->db_insert_id();
		$UserPassword = $pwd;
		$Gender = trim($gender);
		$Address = intranet_htmlspecialchars(trim($address));
		$Country = trim($country);
		$HomeTelNo = intranet_htmlspecialchars(trim($homePhone));
		$OfficeTelNo = intranet_htmlspecialchars(trim($officePhone));
		$MobileTelNo = intranet_htmlspecialchars(trim($mobilePhone));
		$FaxNo = intranet_htmlspecialchars(trim($faxPhone));
		$HKID = intranet_htmlspecialchars(trim($hkid));
		$Barcode = intranet_htmlspecialchars(trim($barcode));
		
		
		if ($intranet_authentication_method=="HASH") {
		    $fieldname .= "UserPassword = NULL, HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		} else {
		    $fieldname .= "UserPassword = NULL, ";
		}
        $fieldname .= "Gender = '$Gender', ";
        $fieldname .= "HomeTelNo = '$HomeTelNo', ";
        $fieldname .= "OfficeTelNo = '$OfficeTelNo', ";
        $fieldname .= "MobileTelNo = '$MobileTelNo', ";
        $fieldname .= "FaxNo = '$FaxNo', ";
        $fieldname .= "Address = '$Address', ";
        $fieldname .= "Country = '$Country', ";
        $fieldname .= "Remark = '$remark', ";
        $fieldname .= "HKID = '$HKID', ";
        $fieldname .= "Barcode = '$Barcode', ";
		$fieldname .= "RecordType = '".TYPE_PARENT."', ";
		$fieldname .= "DateModified = now()";
		
		$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = $uid";
		$li->db_db_query($sql);
		
		# Log down two-way hashed/encrypted password
		$lauth->UpdateEncryptedPassword($uid, $UserPassword);
		
		# insert into INTRANET_USERGROUP (student = 2)
		$gpID = 4;
		$lgroup = new libgroup($gpID);
		$defaultRoleID = $lgroup->returnGroupDefaultRole($gpID);
		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('$gpID', '$uid', '$defaultRoleID', now(), now())";
		$li->db_db_query($sql);
		

		# add group member
		$gpID = 4;
		for($i=0; $i<sizeof($GroupID); $i++){
			if($GroupID[$i] != "") {
				$lgroup = new libgroup($GroupID[$i]);
				$defaultRoleID = $lgroup->returnGroupDefaultRole($GroupID[$i]);
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('".$GroupID[$i]."', '$uid', '$defaultRoleID', now(), now())";
				$laccount->db_db_query($sql);
			}
		}

		# Assign to Role
		$identityType = "Parent";
		for($i=0; $i<sizeof($RoleID); $i++){
			if($RoleID[$i] != "" && $RoleID[$i]!=0) {
				$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
				$li->db_db_query($sql);
			}
		}
		
		#### Special handling
		$li->UpdateRole_UserGroup();
		
		$acl_field = 0;
		
		# insert MyCalendar
		$lcalendar->insertMyCalendar($uid);
		
		# insert group calendar
		$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = $uid
			"; //echo $cal_sql.'<br><br>';
			$li->db_db_query($cal_sql);
		
		# insert calendar viewer to calendars that have shared to the user's groups
		$cal_group_sql = "SELECT g.GroupID FROM INTRANET_GROUP as g 
							INNER JOIN INTRANET_USERGROUP as u ON u.GroupID=g.GroupID 
							WHERE u.UserID='$uid'";
		$cal_group_ary = $li->returnVector($cal_group_sql);
		for($i=0;$i<sizeof($cal_group_ary);$i++) {
			$lcalendar->addCalendarViewerToGroup($cal_group_ary[$i],$uid);
		}
		
		# gamma mail
		if($plugin['imail_gamma'])
		{
			if($EmailStatus=='enable')
			{
				$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
				$IMap = new imap_gamma(1);
				if($IMap->open_account($IMapEmail,$pwd))
				{
					$laccount->setIMapUserEmail($uid,$IMapEmail);
					$IMap->SetTotalQuota($IMapEmail,$Quota,$uid);
					
					// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
					$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
					if($internal_only[USERTYPE_PARENT-1]){ // shift index by one
						$IMap->addGroupBlockExternal(array($IMapEmail));
						//$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
				    	//$li->db_db_query($sql);
				    	$acl_field -= 1; // offset the following $acl_field += 1
					}
				}
			}
			$acl_field += 1;
		}
		else
		{
		# Webmail
			if ($open_webmail && in_array(TYPE_PARENT, $webmail_identity_allowed))
			{
				
				if ($lwebmail->has_webmail) {
					$lwebmail->open_account($userlogin, $UserPassword);
					$acl_field += 1;
					$file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
					if ($file_content == "") {
						$userquota = array(10,10,10);
					}
					else {
						$userquota = explode("\n", $file_content);
					}
					$target_quota = $userquota[TYPE_PARENT-1];
					$lwebmail->setTotalQuota($userlogin, $target_quota, "iMail");
				}
			}
						
			# iMail Quota
			if ($special_feature['imail'] && !$plugin["imail_gamma"])
			{
			    # Get Default Quota
			    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
			    if ($file_content == "")
			    {
			        $quota = 10;
			    }
			    else
			    {
			        $userquota = explode("\n", $file_content);
		            $quotaline = $userquota[3];
			        $temp = explode(":",$quotaline);
			        $quota = $temp[1];
			    }
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$uid', '$quota')";
			    $li->db_db_query($sql);
			    
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$uid', 0)";
			    $li->db_db_query($sql);
			}
		}
		
		$sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, $acl_field)";
		$li->db_db_query($sql);
		
		$laccount->suspendEmailFunctions($uid, $status == 1);
				
		# LDAP
		if ($intranet_authentication_method=='LDAP')
		{
		    $lldap = new libldap();
		    if ($lldap->isAccountControlNeeded()) {
		        $lldap->connect();
		        $lldap->openAccount($userlogin, $UserPassword);
		    }
		}
		
		# Create Parent / Guardian relationship
		$ids = explode(",",$newids);
		
		for($i=0;$i<sizeof($ids);$i++){
			$id = $ids[$i];
			//$sid = ${'uid_'.$id};	// student id 
			$ch_name = trim($chiname);
			$en_name = trim($engname);
			$studentid = ${'uid_'.$id};
			$guardianCheck = ${'guardianCheck_'.$id};
			$relation = ${'relation_new_'.$id};
			$emphone = ${'emphone_new_'.$id};
			$occupation = ${'occupation_new_'.$id};
			
			$linkedGuardianID = explode(',', ${'linkedGuardianID_'.$id});
			
			# create parent relationship
			$sql = "INSERT INTO INTRANET_PARENTRELATION VALUES ($uid, $studentid)";
			$laccount->db_db_query($sql);
			
			
			# change Guardian #
			# modify guardians
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsMain=0 WHERE UserID=$studentid";
			$laccount->db_db_query($sql);	
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsSMS=0 WHERE UserID=$studentid";	
			$laccount->db_db_query($sql);
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET IsEmergencyContact=0 WHERE UserID=$studentid";	
			$laccount->db_db_query($sql);
				
			if($guardianCheck=="on") {
				$IsMain = (${'mainGuardianFlag_'.$id}=='0') ? 1 : 0;
				$IsSMS = (${'smsFlag_'.$id}=='0') ? 1 : 0;
				$IsEmergencyContact = (${'emergencyContactFlag_'.$id}=='0') ? 1 : 0;
				
				$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT (
						UserID,ChName,EnName,
						EmPhone,Relation,
						IsMain,IsSMS,InputDate,ModifiedDate
					) VALUES (
						'$studentid','$ch_name','$en_name',
						'$emphone','$relation',
						'$IsMain', '$IsSMS',NOW(), NOW()
					) ";
				$li->db_db_query($sql);
				$record_id = mysql_insert_id();
				
				//insert the extend record
				$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT_EXT_1 (
						UserID,RecordID, IsEmergencyContact, Occupation, IsLiveTogether, 
						InputDate,ModifiedDate
					) VALUES (
						'$studentid','$record_id', '$IsEmergencyContact', '$occupation', '0', 
						NOW(), NOW()
					)";
				$li->db_db_query($sql);
			}
			# update Main Guardian, SMS Receiver, Emergency Contact
			if($IsMain!=1) {
				$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsMain=1 WHERE RecordID=".$linkedGuardianID[${'mainGuardianFlag_'.$id}-1];
				$laccount->db_db_query($sql);
				//echo $sql.'<br>';
			}
			if($IsSMS!=1) {
				$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsSMS=1 WHERE RecordID=".$linkedGuardianID[${'smsFlag_'.$id}-1];
				$laccount->db_db_query($sql);
				//echo $sql.'<br>';
			}
			if($IsEmergencyContact!=1) {
				$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET IsEmergencyContact=1 WHERE RecordID=".$linkedGuardianID[${'emergencyContactFlag_'.$id}-1];
				$laccount->db_db_query($sql);
				//echo $sql.'<br>';
			}
		
		}
		
		# Call sendmail function (Email notification of registration)
		list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData($email, $userlogin, $UserPassword, stripslashes($engname), stripslashes($chiname));
// 		$mailTo = $uid;
// 		$lwebmail->sendModuleMail((array)$mailTo,$mailSubject,$mailBody);
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if(!$sys_custom['DisableEmailAfterCreatedNewAccount'] && $email != "" && intranet_validateEmail($email,true)){
			$lsendmail->SendMail($email, $mailSubject, $mailBody,"$headers");
		}
		$successAry['synUserDataToModules'] = $luser->synUserDataToModules($uid);
		
		
		if($plugin['radius_server']){
			$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
		}
		
		$flag = "AddSuccess";
	} else {
		$flag = "AddUnsuccess";	
	}

	intranet_closedb();
	
	header("Location: $comeFrom?xmsg=$flag");
}
?>