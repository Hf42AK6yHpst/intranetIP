<?php
# using: yat

#################### Change Log ###
#	2012-08-30 (YatWoon): added barcode
#	2012-06-08 (Carlos): added password remark
#
#	Date:	2012-04-03 (Henry Chow)
#			allow to input HKID if flag is ON
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date :	2011-12-20	(Yuen)
#			Display personal  photos
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	2011-01-31	YatWoon
#		Gender field not a MUST for parent (requested by UCCKE with case #2011-0131-1621-41073)
#
####################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


intranet_auth();
intranet_opendb();
if(!$sys_custom['SISUserManagement']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($comeFrom=="")
	$comeFrom = "index.php";


//if(sizeof($_POST)==0) 
if(empty($uid))
{
	header("Location: $comeFrom");
	exit;
}
/*
$_SERVER['HTTP_COOKIE'];
debug_pr($HTTP_COOKIE_VARS['ck_accountmgmt_teachingType']);
*/
if($uid=="") {
	header("Location: index.php?targetClass=$targetClass&keyword=$keyword");
	exit;	
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$userInfo = $laccount->getUserInfoByID($uid);

if($userlogin=="") $userlogin = $userInfo['UserLogin'];
//if($pwd=="") $pwd = $userInfo['UserPassword'];
if($email=="") $email = $userInfo['UserEmail'];
if($status=="") $status = $userInfo['RecordStatus'];
if($engname=="") $engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
if($chiname=="") $chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
if($gender=="") $gender = $userInfo['Gender'];
if($address=="") $address = $userInfo['Address'];
if($country=="") $country = $userInfo['Country'];
if($homePhone=="") $homePhone = $userInfo['HomeTelNo'];
if($officePhone=="") $officePhone = $userInfo['OfficeTelNo'];
if($mobilePhone=="") $mobilePhone = $userInfo['MobileTelNo'];
if($faxPhone=="") $faxPhone = $userInfo['FaxNo'];
if($remark=="") $remark = $userInfo['Remark'];
$original_pass = $userInfo['UserPassword'];
if($hkid=="") $hkid = $userInfo['HKID'];
if($barcode=="") $barcode = $userInfo['Barcode'];

# imail gamma
if($plugin['imail_gamma'])
{
	if(!$IMapEmail = $laccount->getIMapUserEmail($uid))
	{
		$userInfo = $laccount->getUserInfoByID($uid);
		$IMapEmail = $userInfo['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$disabled = "checked";
	}	
	else
	{
		$enabled = "checked";
	}
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_PARENT);
	$EmailQuota = $laccount->getUserImailGammaQuota($uid);
	$EmailQuota = $EmailQuota?$EmailQuota:$DefaultQuota[1];
}
/*
$countrySelection = "<select name='country' id='country'><option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
for($i=0; $i<sizeof($Lang['Country']); $i++) {
	$countrySelection .= "<option value=\"".$Lang['Country'][$i]."\" ".(($Lang['Country'][$i]==$country)?"selected" : "").">".$Lang['Country'][$i]."</option>";	
}
$countrySelection .= "</select>";
*/
$countrySelection = "<input type='text' name='country' id='country' value='$country'>";
    
$CurrentPageArr['ParentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index_sis.php",0);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('$comeFrom','')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputParentDetails'], 1);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['SelectCorrespondingStudents'], 0);

if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$linkedStudentList = $laccount->getStudentListByParentID($uid);


$li = new libuser($uid);

########## Personal Photo
$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

//$linterface->LAYOUT_START();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function goSubmit(urlLink) {
	var obj = document.form1;
			
	//if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin," ".$i_UserPassword?>")) return false;
	//if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value!="" && (obj.pwd.value != obj.pwd2.value))
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
	
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink, id) {
	if(id!=undefined) {
		document.form1.uid.value = id;
		document.form1.fromParent.value = 1;
	}
	document.form1.action = urlLink;
	document.form1.submit();
}
</script>

<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td class="board_menu_closed">
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
						<div class="table_board">
						<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
							<tr>
								<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
							</tr>
							<tr>
								<td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
								<td width="75%"><?=$userlogin?></td>
								<td rowspan="5"><img src="<?=$photo_personal_link?>" width="100" height="130" title="<?=$Lang['Personal']['PersonalPhoto']?>" /></td>
							</tr>
							<tr>
								<td rowspan="3" class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
								<td><input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/></td>
							</tr>
							<tr>
								<td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> (<?=$Lang['AccountMgmt']['Retype']?>) </td>
							</tr>
							<tr>
								<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
							</tr>
							<tr>
								<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
							</tr>
							<tr>
								<td rowspan="2" class="formfieldtitle"><?=$i_general_name?></td>
								<td colspan="2" ><span class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)</span>
								<input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>"/></td>
							</tr>
							<tr>
								<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span> 
								<input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>"/>
								</td>
							</tr>
              		</table>	
			<div class="edit_bottom">
                  <p class="spacer"></p>
                  <?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update_sis.php')")?>
                  <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom','')")?>
				<p class="spacer"></p>
            </div></td>
          </table>
</table>
		
<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
<input type="hidden" name="fromParent" id="fromParent" value="">
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="ClickID" id="ClickID" value="">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="original_pass" id="original_pass" value="<?=$original_pass?>">
<input type="hidden" name="skipStep2" id="skipStep2" value="">
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>