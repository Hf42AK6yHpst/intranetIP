<?php
# using: 

############ Change Log Start ###############
#   Date:   2019-09-26 Tommy [2019-0916-0930-52073]
#           fix: status wrong sorting
#
#   Date:   2018-12-17 Isaac
#           added $save_keyword to prevent sql injection
#           added FaxNo, HomeTelNo, OfficeTelNo and MobileTelNo to the keyword search
#
#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added table tool buttons for enable/disable Wi-Fi access.
#
#	Date:	2015-08-10	Bill	[2015-0513-1146-45207]
#			added filter to select parent without child and parent without child in current year class
#
#	Date:	2014-11-11	Omas
#			Modified SQL Can sort by Student Name and improve performance 
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
#	Date:	2011-01-10	YatWoon
#			- set cookies
#
#	Date:	2011-01-06	YatWoon
#			- IP25 UI
#			- Add "Active", "Suspend" tool options
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_parent_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$CurrentPageArr['ParentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ParentWithoutChildInClass'],"noChildInSchool.php?clearCoo=1",1);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

// [2015-0513-1146-45207] Target Type drop down list
$withoutChildTypeMenu = "<select name=\"withoutChildType\" id=\"withoutChildType\" onChange=\"reloadForm()\">";
$withoutChildTypeMenu .= "<option value='0'".(($withoutChildType=='' || $withoutChildType=='0') ? " selected" : "").">{$Lang['AccountMgmt']['ParentAll']}</option>";
$withoutChildTypeMenu .= "<option value='1'".(($withoutChildType=='1') ? " selected" : "").">{$Lang['AccountMgmt']['ParentWithoutChildInCurrentYearClass']}</option>";
$withoutChildTypeMenu .= "<option value='2'".(($withoutChildType=='2') ? " selected" : "").">{$Lang['AccountMgmt']['ParentWithoutChild']}</option>";
$withoutChildTypeMenu .= "</select>";

$li = new libdbtable2007($field, $order, $pageNo);

$AcademicYearID = Get_Current_Academic_Year_ID();

if($keyword != "")	{
    $safe_keyword = htmlentities($li->Get_Safe_Sql_Like_Query($keyword),ENT_QUOTES);
    $conds .= " AND (USR.UserLogin like '%$safe_keyword%' OR
                USR.UserEmail like '%$safe_keyword%' OR
                USR.EnglishName like '%$safe_keyword%' OR
                USR.ChineseName like '%$safe_keyword%' OR
                USR.ClassName like '%$safe_keyword%' OR
                USR.WebSamsRegNo like '%$safe_keyword%' OR
				USR.HomeTelNo like '%$safe_keyword%' OR
				USR.OfficeTelNo like '%$safe_keyword%' OR
                USR.FaxNo like '%$safe_keyword%' OR
				USR.MobileTelNo like '%$safe_keyword%'
				)";
}

// sql for Parent with child
$sql = "SELECT
			USR.UserID
		FROM 
			INTRANET_USER USR INNER JOIN
			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID)
		WHERE
			USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)		
		GROUP BY 
			USR.UserID
			";				
$parentWithChild = $ldiscipline->returnVector($sql);

// [2015-0513-1146-45207] Target Type - All Parent / Parent without child in current year class
if($withoutChildType!='2'){
	// sql for Parent with child in current year class
	$sql = "SELECT
				USR.UserID
			FROM 
				INTRANET_USER USR INNER JOIN
				INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) INNER JOIN
				YEAR_CLASS_USER ycu ON (pr.StudentID=ycu.UserID) INNER JOIN 
				YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID='$AcademicYearID')
			WHERE
				USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)		
			GROUP BY 
				USR.UserID
				";				
	$parentWithChildInClass = $ldiscipline->returnVector($sql);
}

//if(sizeof($parentWithChild)>0)
//	$conds .= " AND USR.UserID NOT IN (".implode(',', $parentWithChild).")";
	
// [2015-0513-1146-45207] sql condition for different target type of Parent
$parentChildCond = "";
if(sizeof($parentWithChild)>0){
	// Target Type - All Parent / Parent without child
	if($withoutChildType!='1') {
		$parentChildCond .= "USR.UserID NOT IN (".implode(',', (array)$parentWithChild).")";
	}
	// Target Type - All Parent / Parent without child in current year class
	if(sizeof($parentWithChildInClass)>0 && ($withoutChildType!='2')) {
		if($withoutChildType=='' || $withoutChildType=='0'){
			$parentChildCond .= " OR ";
		}
		$parentChildCond .= "USR.UserID IN (".implode(',', (array)$parentWithChild).") AND USR.UserID NOT IN (".implode(',', (array)$parentWithChildInClass).")";
	}
	$conds .= " AND ($parentChildCond)";
}

//$sql = "SELECT
//			CONCAT('<a href=javascript:; title=\'',IFNULL(IF(USR.EnglishName='','---',USR.EnglishName),'---'),'\' onclick=goEdit(',USR.UserID,')>',IFNULL(IF(USR.EnglishName='','---',USR.EnglishName),'---'),'</a>') as EnglishName,
//			IFNULL(IF(USR.ChineseName='', '---', USR.ChineseName),'---') as ChineseName,
//			USR.UserID as studentName,
//			USR.UserID as phone,
//			USR.UserLogin,
//			IFNULL(USR.LastUsed, '---') as LastUsed,
//			CASE USR.RecordStatus
//				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
//				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
//				ELSE '$i_status_suspended' END as recordStatus,
//			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox
//		FROM 
//			INTRANET_USER USR LEFT OUTER JOIN
//			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) LEFT OUTER JOIN
//			YEAR_CLASS_USER ycu ON (ycu.UserID=pr.StudentID) LEFT OUTER JOIN
//			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$AcademicYearID) LEFT OUTER JOIN
//			YEAR y ON (y.YearID=yc.YearID)
//		WHERE
//			USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)		
//			
//			$conds 
//		GROUP BY
//			USR.UserID
//			";
//$sql = "SELECT
//			CONCAT('<a href=javascript:; title=\'', IFNULL(IF(USR.EnglishName='','---',USR.EnglishName),'---'),'\' onclick=goEdit(',USR.UserID,')>',IFNULL(IF(USR.EnglishName='','---',USR.EnglishName),'---'),'</a>') as EnglishName,
//			IFNULL(IF(USR.ChineseName='', '---', USR.ChineseName),'---') as ChineseName,
//			'',			
//			CONCAT_WS(' / ', IF(USR.HomeTelNo='', null, USR.HomeTelNo), IF(USR.OfficeTelNo='', null, USR.OfficeTelNo), IF(USR.MobileTelNo='', null, USR.MobileTelNo)) as phone,
//			USR.UserLogin,
//			IFNULL(USR.LastUsed, '---') as LastUsed,
//			CASE USR.RecordStatus
//				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
//				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
//				ELSE '$i_status_suspended' END as recordStatus,
//			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox
//		FROM 
//			INTRANET_USER USR LEFT OUTER JOIN
//			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) LEFT OUTER JOIN
//			YEAR_CLASS_USER ycu ON (ycu.UserID=pr.StudentID) LEFT OUTER JOIN
//			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$AcademicYearID) LEFT OUTER JOIN
//			YEAR y ON (y.YearID=yc.YearID)
//			LEFT OUTER JOIN INTRANET_USER stu ON (pr.StudentID = stu.UserID)
//		WHERE
//			USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)		
//			
//			$conds 
//		GROUP BY
//			USR.UserID
//			";
			
// [2015-0513-1146-45207] field for studentNameLink
$studentNameField = Get_Lang_Selection("IF(stu.ChineseName='' OR stu.ChineseName is null, stu.EnglishName, stu.ChineseName)", "IF(stu.EnglishName='' OR stu.EnglishName is null, stu.ChineseName, stu.EnglishName)");
if($sys_custom['SISUserManagement']){
	$editPage = 'edit_sis.php';	
}
else {
	$editPage = 'edit.php';
}

$sql = "SELECT
			IFNULL(IF(USR.EnglishName='', '---', USR.EnglishName),'---') as EnglishName,
			IFNULL(IF(USR.ChineseName='', '---', USR.ChineseName),'---') as ChineseName,
			/* start of student link */
			Group_Concat(
				Distinct CONCAT('<a href=\"javascript:goURL(\'../StudentMgmt/".$editPage."\',' , stu.UserID , ');\">', $studentNameField, '</a>')
				Separator '<br>' 
			) as studentNameLink,
			/* end of student link */
			CONCAT_WS(' / ', IF(USR.HomeTelNo='', null, USR.HomeTelNo), IF(USR.OfficeTelNo='', null, USR.OfficeTelNo), IF(USR.MobileTelNo='', null, USR.MobileTelNo)) as phone,
			USR.UserLogin,
			IFNULL(USR.LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox,
			USR.UserID,
			group_concat(distinct stu.EnglishName SEPARATOR '<br/>') as studentName
		FROM 
			INTRANET_USER USR LEFT OUTER JOIN
			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=pr.StudentID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$AcademicYearID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
			LEFT OUTER JOIN INTRANET_USER stu ON (pr.StudentID = stu.UserID)
		WHERE
			USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)
			$conds 
		GROUP BY
			USR.UserID
			";

$li->sql = $sql;
$li->field_array = array("EnglishName", "ChineseName", "studentName", "UserLogin", "LastUsed", "recordStatus");
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "UserMgmtParentAccount";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
//$li->column_list .= "<th width='25%' >".$i_identity_student."</th>\n";$pos++;
$li->column_list .= "<th width='25%' >".$li->column($pos++, $i_identity_student)."</th>\n";
// $li->column_list .= "<th width='10%' >".$Lang['AccountMgmt']['Tel']."</th>\n";$pos++;
$li->column_list .= "<th width='10%' >".$Lang['AccountMgmt']['Tel']."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "noChildInSchool.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function newUser() {
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() {
	document.form1.TabID.value = <?=TYPE_PARENT?>;
	document.form1.action = "../export.php";
	document.form1.submit();
}

function goImport() {
	document.form1.TabID.value = <?=TYPE_PARENT?>;
	document.form1.action = "../import/import.php";
	document.form1.submit();
}

function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

function goURL(thisURL, uid) {
	document.form1.action = thisURL;
	document.form1.uid.value = uid;
	document.form1.submit();
}

function confirmRemove(obj, thisURL) {
	if(confirm("<?=$Lang['AccountMgmt']['ConfirmDeleteAllParent']?>")) {
		obj.action = thisURL;
		obj.all.value = 1;
		obj.submit();
	}
		
}

</script>
<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<?=$withoutChildTypeMenu?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		<? if($plugin['radius_server']){ ?>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../enable_wifi_access.php')" class="tool_approve"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></a>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../disable_wifi_access.php')" class="tool_reject"><?=$Lang['AccountMgmt']['DisableWifiAccess']?></a>
        <? } ?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<a href="javascript:confirmRemove(document.form1,'remove.php')" class="tool_delete"><?=$Lang['AccountMgmt']['DeleteAll']?></a> 
		<a href="javascript:checkRemove(document.form1,'user_id[]','remove.php')" class="tool_delete"><?=$Lang['AccountMgmt']['DeleteSelected']?></a> 
		</div>
	</td>
</tr>
</table>

<?= $li->display()?>

</div>




<input type="hidden" name="all" id="all" value="" />
<input type="hidden" name="TabID" id="TabID" value="" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/ParentMgmt/noChildInSchool.php">
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="clearCoo" id="clearCoo" value="" />

</form>
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>