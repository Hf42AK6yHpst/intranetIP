<?php
# using: 

#################### Change Log ####################
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#	Date:	2015-12-30 (Carlos) - $plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-01-12	Omas 	
#			new js file for delay checking loginID templates/2009a/js/account_mgmt_checking.js
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
# 2012-06-08 (Carlos): added password remark
#
####################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$lu = new libuser();
$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_ALUMNI));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_ALUMNI];
if ($PasswordLength<6) $PasswordLength = 6;

$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

if ($intranet_session_language!="gb") {
	$chi_title_array = array($i_general_Principal,$i_general_VPrincipal,$i_general_Director);
}
else {
	$chi_title_array = array();
}

$sql = "SELECT DISTINCT TitleChinese FROM INTRANET_USER WHERE TitleChinese IS NOT NULL AND TitleChinese !='' ORDER BY TitleChinese";
$temp = $laccount->returnVector($sql);
if (sizeof($temp)!=0) {
	$chi_title_array = array_merge($chi_title_array, $temp);
	$chi_title_array = array_unique($chi_title_array);
	$chi_title_array = array_values($chi_title_array);
}
$sql = "SELECT DISTINCT TitleEnglish FROM INTRANET_USER WHERE TitleEnglish IS NOT NULL AND TitleEnglish !='' ORDER BY TitleEnglish";
$eng_title_array = $laccount->returnVector($sql);
if (sizeof($eng_title_array)!=0) {
	$select_eng_title = getSelectByValue($eng_title_array, "onChange=this.form.engTitle.value=this.value");
} else
{
	$select_eng_title = "";
}
$select_chi_title = getSelectByValue($chi_title_array, "onChange=this.form.chiTitle.value=this.value");

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

# imail gamma
if($plugin['imail_gamma'])
{
	$DefaultQuota = $laccount->getEmailDefaultQuota(USERTYPE_ALUMNI);
	$EmailQuota = $DefaultQuota[1];
}
    
$CurrentPageArr['AlumniMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
// $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script src="<?=$PATH_WRT_ROOT.'/templates/2009a/js/account_mgmt_checking.js'?>" type="text/javascript"></script>
<script language="javascript">
function goSubmit(urlLink) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>	
	var obj = document.form1;
	show_checking('userlogin','spanChecking');
	
	if(obj.userlogin.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$i_UserLogin?>");	
		return false;
	}
	
	if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin," ".$i_UserPassword?>")) return false;
	if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value != obj.pwd2.value)
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword']){ ?>   	
	var check_password_result = CheckPasswordCriteria(obj.pwd.value,obj.userlogin.value,<?=$PasswordLength?>);
	if(check_password_result.indexOf(1) == -1){
		var password_warning_msg = '';
		for(var i=0;i<check_password_result.length;i++){
			password_warning_msg += password_warnings[check_password_result[i]] + "\n";
		}
		alert(password_warning_msg);
		obj.pwd.focus();
		return false;
	}
<?php } ?>	
	if (obj.email) {
		if( obj.email.value != "")
		{
			if(!validateEmail(obj.email, "<?=$i_invalid_email?>")) return false;
		}	
	}
	
	if(obj.status[0].checked==false && obj.status[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
	if(obj.available.value==0) {
		alert("<?=$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']?>");
		return false;	
	}
	if(obj.available.value==2) {
		alert("<?=$i_UserLogin.$Lang['AccountMgmt']['StartedWithA2Z']?>");
		return false;	
	}	
	
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink) {
	document.form1.action = urlLink;
	document.form1.submit();
}

function updateIMapEmail(obj)
{
	$("span#IMapUserEmail").html(obj.value.Trim()+"@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?>");
}
</script>
<script language="JavaScript">
var ClickID = '';
var callback_checking = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('spanChecking').innerHTML = tmp_str;
	    DisplayPosition();
	    $('#spanCheckingImage').hide();
	}
}


function show_checking(type, click)
{
	$('#spanCheckingImage').show();
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "../ajax_checking.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_checking);
}


function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('spanChecking').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('spanChecking').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('spanChecking').style.visibility='visible';
}
</script>
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
                      <div class="table_board">

						<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
						  <tr>
						    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
						    </tr>
						 
						  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td ><input name="userlogin" type="text" id="userlogin" class="textboxnum" value="<?=$userlogin?>" maxlength="20" onKeyUp="updateIMapEmail(this);"/>
						    		<?/* if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo "<font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; */?>
						    		<span id="spanChecking"><? if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo " <font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?><input type="hidden" name="available" id="available" value="1"></span>
						    		<span id="spanCheckingImage" style="display:none;"><?=$linterface->Get_Ajax_Loading_Image()?></span>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
						    <td><input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>" maxlength="20"/> 
						      (<?=$Lang['AccountMgmt']['Retype']?>)        </td>
						  </tr>
						  <tr>
						  	<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
  							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(USERTYPE_ALUMNI) )
									{	
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" /><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" CHECKED/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label>,<span id="IMapUserEmail">@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?></span><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>:<input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
								<!-- imail -->
						  <tr>
						    <td class="formfieldtitle"><!--<span class="tabletextrequire">*</span>--><?=$Lang['Gamma']['UserEmail']?></td>
						    <td>
						    	<input name="email" type="text" id="email" class="textbox_name" value="<?=$email?>" maxlength="100"/>
						    	<? if(sizeof($errorAry)>0 && in_array($email, $errorAry)) echo "<font color='#FF0000'>".$i_UserEmail.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
						    	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="" || $status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						 
						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
								<td colspan="2">
									<input name="barcode" type="text" id="barcode" class="textboxnum" value="<?=$barcode?>" maxlength="30"/>
									<? if(sizeof($errorAry)>0 && in_array($barcode, $errorAry)) echo "<font color='#FF0000'>".$Lang['AccountMgmt']['Barcode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
								</td>
							</tr>
							
						  <tr>
						    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
						    </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$i_general_name?></td>
						    <td class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)
						      <input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>" maxlength="250"/>
						    </td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>" maxlength="50"/>
						   </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserNickName?></td>
						    <td><input name="nickname" type="text" id="nickname" class="textbox_name" value="<?=$nickname?>" maxlength="100"/></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserGender?></td>
						    <td><input type="radio" name="gender" id="genderM" value="M"  <? if($gender=="M" || $gender=="") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F" <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$Lang['AccountMgmt']['UserDisplayTitle']?></td>
						    <td><span class="sub_row_content">(<?=$ip20_lang_eng?>)</span>
						      <input name="engTitle" type="text" id="engTitle" class="textbox_name" value="<?=$engTitle?>" maxlength="100"/> <?=$select_eng_title?>
						       </td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiTitle" type="text" id="chiTitle" class="textbox_name" value="<?=$chiTitle?>" maxlength="50"/> <?=$select_chi_title?>
						       </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserAddress?></td>
						    <td><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="4"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>" maxlength="20" /><? if (isset($plugin['sms']) && $plugin['sms']){ echo $i_UserMobileSMSNotes; }?></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="20" /></td>
						  </tr>
						  
						  <!-- Additional Info //-->
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td>
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
								
								<tr>
									<td class="formfieldtitle"><?=$i_ClassName?></td>
									<td><input name="classname" type="text" id="classname" class="textboxnum" value="<?=$classname?>"/></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$i_ClassNumber?></td>
									<td><input name="classnumber" type="text" id="classnumber" class="textboxnum" value="<?=$classnumber?>"/></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['YearOfLeft']?></td>
									<td><input name="yearofleft" type="text" id="yearofleft" class="textboxnum" value="<?=$yearofleft?>"/></td>
								</tr>
							
								
						<?if(!$plugin['imail_gamma']) {?>
						  <tr>
						    <td colspan="2"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  </tr>
						    	<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(USERTYPE_ALUMNI, $webmail_identity_allowed)) { ?>
								  <tr>
								    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
								    <td><input type="checkbox" value="1" name="open_webmail" id="open_webmail" />
								      <label for="open_webmail"><?=$i_Mail_AllowSendReceiveExternalMail?></label></td>
								  </tr>
						  		<? } ?>
						  <? } ?>
						  
						  <?php if($plugin['radius_server']){ ?>
						  <tr>
						  	<td colspan="2"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['WifiUsage']?> -</em></td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></td>
						  	<td><input type="checkbox" name="enable_wifi_access" value="1" checked="checked" /></td>
						  </tr>
						  <?php } ?>
						  
						</table>
						<p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                          <p class="spacer"></p>
                          <?= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit('new_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('index.php')")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="ClickID" id="ClickID" value="">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
</form>
<script language="javascript">
	show_checking('userlogin','spanChecking');
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>