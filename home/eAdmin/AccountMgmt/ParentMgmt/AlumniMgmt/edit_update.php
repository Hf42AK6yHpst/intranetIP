<?php
# using: 

############# Change Log
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#
#	Date:	2015-12-30 (Carlos)
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions.
#
#	Date:	2014-09-03 (Carlos) - iMail plus: Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
#	
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	2014-05-07 (Carlos): Added suspend/unsuspend iMail plus account api call
#
#	Date:	2013-03-15	Carlos
#			remove shared group calendars for user and add shared group calendars to user
#
#	Date:	2012-11-21	YatWoon
#			fix: missing to add the Encoded password to personal information
#
#	Date:	2012-10-03	Carlos
#			fix failed to enable iMail plus account when password method is HASHED
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date:	2012-02-07	YatWoon
#			add: add hashed password 
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lauth = new libauth();
$laccount = new libaccountmgmt();
$lu = new libuser($uid);
$le = new libeclass();
$lcalendar = new icalendar();



$dataAry = array();

$userInfo = $laccount->getUserInfoByID($uid);

if($sys_custom['UseStrongPassword']){
	if($pwd != ''){
		$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_ALUMNI));
		$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_ALUMNI];
		if ($PasswordLength<6) $PasswordLength = 6;
		$check_password_result = $lauth->CheckPasswordCriteria($pwd,$lu->UserLogin,$PasswordLength);
		if(!in_array(1,$check_password_result)){
			intranet_closedb();
			$_SESSION['AlumniAccountUpdatePasswordError'] = $Lang["Login"]["password_err"];
	        header("Location: edit.php?uid=$uid");
	        exit;
		}
	}
}

# check email
$UserEmail = intranet_htmlspecialchars(trim($UserEmail));
if ($UserEmail != "" && $userInfo['UserEmail'] != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail))
    {
        header("Location: edit.php?uid=$uid&xmsg=EmailUsed");
        exit;
    }
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode' AND UserID!=$uid";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		header("Location: edit.php?uid=$uid");
        exit;
	}
}

if ($intranet_authentication_method=="HASH" && $pwd!='') {
	$modifyPassword = 1;
}
else {
	$modifyPassword = ($pwd!="" && $pwd!=$original_password) ? 1 : 0;
}	

if($modifyPassword) {
	if ($intranet_authentication_method=="HASH") {
		//$dataAry['UserPassword'] = "HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		$dataAry['UserPassword'] = "";
		$dataAry['HashedPass'] = "md5('$userlogin$UserPassword$intranet_password_salt')";
	} else {
		$dataAry['UserPassword'] = trim($pwd);
	}
}

if(!$modifyPassword){
	if($intranet_authentication_method=="HASH"){
		$UserPassword = $lauth->GetUserDecryptedPassword($uid);
	}else{
		$UserPassword = $userInfo['UserPassword'];
	}
}

$dataAry['RecordStatus'] = $status;
$dataAry['EnglishName'] = intranet_htmlspecialchars(stripslashes(trim($engname)));
$dataAry['ChineseName'] = intranet_htmlspecialchars(stripslashes(trim($chiname)));
$dataAry['NickName'] = intranet_htmlspecialchars(trim($nickname));
$dataAry['Gender'] = $gender;
$dataAry['TitleEnglish'] = intranet_htmlspecialchars(trim($engTitle));
$dataAry['TitleChinese'] = intranet_htmlspecialchars(trim($chiTitle));
$dataAry['Address'] = intranet_htmlspecialchars(trim($address));
$dataAry['Country'] = $country;
$dataAry['HomeTelNo'] = intranet_htmlspecialchars(trim($homePhone));
$dataAry['OfficeTelNo'] = intranet_htmlspecialchars(trim($officePhone));
$dataAry['MobileTelNo'] = intranet_htmlspecialchars(trim($mobilePhone));
$dataAry['FaxNo'] = intranet_htmlspecialchars(trim($faxPhone));
$dataAry['Remark'] = intranet_htmlspecialchars(trim($remark));
$dataAry['ClassName'] = intranet_htmlspecialchars(trim($classname));
$dataAry['ClassNumber'] = intranet_htmlspecialchars(trim($classnumber));
$dataAry['YearOfLeft'] = intranet_htmlspecialchars(trim($yearofleft));
$dataAry['UserEmail'] = intranet_htmlspecialchars(trim($UserEmail));
$dataAry['Barcode'] = intranet_htmlspecialchars(trim($barcode));

$success = $laccount->updateUserInfo($uid, $dataAry);

if($modifyPassword && $success){
		$lauth->UpdateEncryptedPassword($uid, $UserPassword);
	}

$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID='$alumni_GroupID'";
$count = $lu->returnVector($sql);

if($count[0]==0) {
	
	$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($alumni_GroupID, $uid, now(), now())";
	$lu->db_db_query($sql);
}


# add to corresponding alumni group according to YearOfLeft
$lu->AddOrUpdateAlumniYearGroup($uid, trim($yearofleft));


$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='$uid'";
$existing_groups = $lu->returnVector($sql);

$userlogin = $lu->UserLogin;

if($plugin["imail_gamma"])
{
	$IMap = new imap_gamma(1);
	$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	
	if($EmailStatus=="enable")
	{
		if($IMap->is_user_exist($IMapEmail))
		{
			$account_exist = true;
			if($modifyPassword)
				$IMap->change_password($IMapEmail, $UserPassword);
		}
		else
		{
			$igmma_password = $UserPassword;
			
			$account_exist = $IMap->open_account($IMapEmail, $igmma_password);
			// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
			$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
			if($internal_only[USERTYPE_ALUMNI-1]){ // shift index by one
				$IMap->addGroupBlockExternal(array($IMapEmail));
				$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
		    	$lu->db_db_query($sql);
			}
			
		}
		if($account_exist)
		{
			$laccount->setIMapUserEmail($uid,$IMapEmail);
			$result["setQuota"] = $IMap->SetTotalQuota($IMapEmail,$Quota, $uid);
			$IMap->setUnsuspendUser($IMapEmail,"iMail",$UserPassword); // resume iMail plus account
		}
	}
	else
	{
		# disable gamma mail
		$laccount->setIMapUserEmail($uid,"");
		
		$IMap->setSuspendUser($IMapEmail,"iMail"); // suspend iMail plus account
	}
}
	
$lwebmail = new libwebmail();
if(!$plugin["imail_gamma"])
{
	# update webmail password
	if ($modifyPassword && $lwebmail->has_webmail)
		$lwebmail->change_password($userlogin, $UserPassword, "iMail");
}

/*	
# FTP management
if ($plugin['personalfile'])
{
	$lftp = new libftp();
	if ($modifyPassword && $lftp->isFTP)
	{
		$lftp->changePassword($userlogin, $dataAry['UserPassword'], "iFolder");
	}
}
    */
    
if ($intranet_authentication_method=='LDAP')
{
	$lldap = new libldap();
	if ($modifyPassword && $lldap->isPasswordChangeNeeded())
	{
		$lldap->connect();
		$lldap->changePassword($userlogin, $UserPassword);
	}
}

# send email
if($modifyPassword)
{
	list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData("", "", "", stripslashes($engname), stripslashes($chiname), 1);
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if($UserEmail != "" && intranet_validateEmail($UserEmail,true)){
			$lsendmail->SendMail($UserEmail, $mailSubject, $mailBody,"$headers");
		}
} 

/*
# SchoolNet
if ($plugin['SchoolNet']) {
	include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
	$lschoolnet = new libschoolnet();
	
	$sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = $uid";
	$data = $li->returnArray($sql,12);
	if($modifyPassword)
		$data[0][1] = $dataAry['UserPassword'];
	$lschoolnet->addStudentUser($data);
}
*/

/*
if($skipStep2 != 1) {
	# Assign to Group
	# remove existing group
	$gpID = ($teaching==1) ? 1 : 3;
	$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID=$uid AND GroupID!=$gpID";
	$laccount->db_db_query($sql);
	
	# add group member
	for($i=0; $i<sizeof($GroupID); $i++){
		if($GroupID[$i] != "") {
			$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (".$GroupID[$i].", $uid, now(), now())";
			$laccount->db_db_query($sql);
		}
	}
	
	# Assign to Role
	# remove existing role
	$sql = "DELETE FROM ROLE_MEMBER WHERE UserID=$uid";
	$laccount->db_db_query($sql);

	# add role
	$identityType = ($teaching==0) ? "NonTeaching" : "Teaching";
	for($i=0; $i<sizeof($RoleID); $i++){
		if($RoleID[$i] != "" && $RoleID[$i]!=0) {
			$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
			$laccount->db_db_query($sql);
		}
	}
}
*/

# icalendar
	//$GroupID = empty($GroupID)?Array():$GroupID;
	
	$sql = "select GroupID from INTRANET_USERGROUP where UserID = '$uid' ";
	$all_groups = $lu->returnVector($sql);
	
	$sql = "delete from CALENDAR_CALENDAR_VIEWER where 
		GroupID not in ('".implode("','",$all_groups)."') and GroupType = 'E' and UserID = $uid";
	$lu->db_db_query($sql);
	
	$sql = "select GroupID from CALENDAR_CALENDAR_VIEWER where GroupType = 'E' and UserID = $uid";
	$existing = $lu->returnVector($sql);
	$existing = empty($existing)? array():$existing;
	
	
	//$newGroup = array_diff($GroupID,$existing);
	//if (!empty($existing)){
		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = $uid
				where g.GroupID not in ('".implode("','",$existing)."')";
		$lu->db_db_query($sql);
	//}
	
	# remove shared group calendars 
	$cal_remove_groups = array_values(array_diff($existing_groups,$all_groups));
	for($i=0;$i<sizeof($cal_remove_groups);$i++) {
		$lcalendar->removeCalendarViewerFromGroup($cal_remove_groups[$i],$uid);
	}
	# insert calendar viewer to calendars that have shared to the user's groups
	for($i=0;$i<sizeof($all_groups);$i++) {
		$lcalendar->addCalendarViewerToGroup($all_groups[$i],$uid);
	}

$successAry['synUserDataToModules'] = $lu->synUserDataToModules($uid);

$laccount->suspendEmailFunctions($uid, $status == 1);

if($plugin['radius_server']){
	$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
}

# no longer use this function
// $le->eClass40UserUpdateInfo($UserEmail,$dataAry);

intranet_closedb();

if($success) 
	$xmsg = "UpdateSuccess";
else 
	$xmsg = "UpdateUnsuccess";
	
// header("Location: index.php?targetClass=$targetClass&recordstatus=$recordstatus&keyword=$keyword&xmsg=$flag");	
header("Location: index.php?xmsg=$xmsg");	
?>

