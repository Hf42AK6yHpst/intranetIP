<?php
// Editing by 
/*
 * Modification: 
 *  2018.9.18   Chris
 *  added in GoogleSSO
 * 2015-12-14 (Carlos) [ip2.5.7.1.1] : Moved the approve users flow into libaccountmgmt.php#approveUsers().
 * 2015-05-26 (Carlos): Resume mail server auto forward and auto reply functions for resumed users.
 * Date: 2013-02-08	YatWoon
 *  - use "user_id[]" instead of userID[]
 * Date: 2012-08-01 (Ivan)
 * 	- added logic to syn user info to library system
 */
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();
$laccount = new libaccountmgmt();
###GoogleSSO

if($ssoservice["Google"]["Valid"]){
    include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
    include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
    include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $user_list = $libGoogleSSO->enabledUserForGoogle($laccount, $user_id);
    if (isset($user_list['suspendedUsers'])){
        $libGoogleSSO->activateUserFromEClassToGoogle($laccount, $user_list['suspendedUsers']);
    }
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($user_id)==0) {
	header("Location : index.php");	
	exit;
}

$successAry = $laccount->approveUsers($user_id);

/*
$luser = new libuser();
$successAry = array();

$dbModifiedByUserId = ($_SESSION['UserID']=='')? 'null' : $_SESSION['UserID'];
$sql = "UPDATE INTRANET_USER SET RecordStatus = '1', DateModified = now(), ModifyBy = $dbModifiedByUserId WHERE UserID IN (".implode(",", $user_id).")";
$successAry['updateIntranetUserDb'] = $laccount->db_db_query($sql);

$successAry['synUserDataToModules'] = $luser->synUserDataToModules($user_id);

// Resume mail server email forwarding function and auto reply function
foreach($user_id as $single_user_id)
{
	$laccount->suspendEmailFunctions($single_user_id, true);
}
*/

intranet_closedb();

if(!in_array(false, $successAry))
	$xmsg = "UpdateSuccess";
else 
	$xmsg = "UpdateUnsuccess";
	
$comeFrom = $comeFrom ? $comeFrom : "index.php";	

header("Location: $comeFrom?&xmsg=$xmsg");
?>