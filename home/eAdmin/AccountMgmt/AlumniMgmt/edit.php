<?php
# using: 

### 		!!!!! Note: Required to deploy with libuser.php if upload this file to client BEFORE ip.2.5.9.1.1	!!!!!	

############# Change Log
#	Date:	2019-05-13 (Henry) Security fix: SQL without quote
#
#   Date:   2018-05-16 Isaac
#           Added Creator name and date
#           Added Last Modified username and time.
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#
#	Date:	2017-10-19 Ivan [ip.2.5.9.1.1]
#			Changed default personal photo path to call function by libuser.php
#
#   Date:	2016-06-10 Anna
#   add creator's information below the webpage

#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date:	2012-06-08	Carlos
#			added password remark
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date :	2011-12-20	(Yuen)
#			Display personal  photos
#
#################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// if(sizeof($_POST)==0) 
if(empty($uid))
{
	header("Location: index.php");	
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$userInfo = $laccount->getUserInfoByID($uid);

//if($pwd=="") $pwd = $userInfo['UserPassword'];
if($email=="") $email = $userInfo['UserEmail'];
if($status=="") $status = $userInfo['RecordStatus'];
if($engname=="") $engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
if($chiname=="") $chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
if($nickname=="") $nickname = $userInfo['NickName'];
if($gender=="") $gender = $userInfo['Gender'];
if($chiTitle=="") $chiTitle = $userInfo['TitleChinese'];
if($engTitle=="") $engTitle = $userInfo['TitleEnglish'];
if($address=="") $address = $userInfo['Address'];
if($country=="") $country = $userInfo['Country'];
if($homePhone=="") $homePhone = $userInfo['HomeTelNo'];
if($officePhone=="") $officePhone = $userInfo['OfficeTelNo'];
if($mobilePhone=="") $mobilePhone = $userInfo['MobileTelNo'];
if($faxPhone=="") $faxPhone = $userInfo['FaxNo'];
if($remark=="") $remark = $userInfo['Remark'];
if ($intranet_session_language!="gb") {
	$chi_title_array = array($i_general_Principal,$i_general_VPrincipal,$i_general_Director);
}
else {
	$chi_title_array = array();
}
if($classname=="") $classname = $userInfo['ClassName'];
if($classnumber=="") $classnumber = $userInfo['ClassNumber'];
if($yearofleft=="") $yearofleft = $userInfo['YearOfLeft'];
if($barcode=="") $barcode = $userInfo['Barcode'];

$sql = "SELECT DISTINCT TitleChinese FROM INTRANET_USER WHERE TitleChinese IS NOT NULL AND TitleChinese !='' ORDER BY TitleChinese";
$temp = $laccount->returnVector($sql);
if (sizeof($temp)!=0) {
	$chi_title_array = array_merge($chi_title_array, $temp);
	$chi_title_array = array_unique($chi_title_array);
	$chi_title_array = array_values($chi_title_array);
}
$sql = "SELECT DISTINCT TitleEnglish FROM INTRANET_USER WHERE TitleEnglish IS NOT NULL AND TitleEnglish !='' ORDER BY TitleEnglish";
$eng_title_array = $laccount->returnVector($sql);
if (sizeof($eng_title_array)!=0) {
	$select_eng_title = getSelectByValue($eng_title_array, "onChange=this.form.engTitle.value=this.value");
} else
{
	$select_eng_title = "";
}
$select_chi_title = getSelectByValue($chi_title_array, "onChange=this.form.chiTitle.value=this.value");

$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

# imail gamma
if($plugin['imail_gamma'])
{
	if(!$IMapEmail = $laccount->getIMapUserEmail($uid))
	{
		$userInfo = $laccount->getUserInfoByID($uid);
		$IMapEmail = $userInfo['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$disabled = "checked";
	}	
	else
	{
		$enabled = "checked";
	}
	
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_TEACHER);
	$EmailQuota = $laccount->getUserImailGammaQuota($uid);
	$EmailQuota = $EmailQuota?$EmailQuota:$DefaultQuota[1];
}

$CurrentPageArr['AlumniMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goBack()");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();






$li = new libuser($uid);
$ModifyBy = $li->ModifyBy;
$DateModified = $li->DateModified;
$ArchiverUserinfoAry = $li->ReturnArchiveUserCreateModifyInfo($uid);
$ArchivedBy=$ArchiverUserinfoAry['ArchivedBy'];
$DateArchive = $ArchiverUserinfoAry['DateArchive'];

$SettingArr = $li->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_ALUMNI));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_ALUMNI];
if ($PasswordLength<6) $PasswordLength = 6;


$userObj = new libuser($ArchivedBy);
$DateArchive? $DateInput=$DateArchive:'';

if(empty($userObj->UserID)){
    $CreatorUserinfoAry= $userObj->ReturnArchiveStudentInfo($inputBy);
    $CreatorEnglishName = $CreatorUserinfoAry['EnglishName'];
    $CreatorChineseName = $CreatorUserinfoAry['ChineseName'];
}else{
    $CreatorEnglishName = $userObj->EnglishName;
    $CreatorChineseName = $userObj->ChineseName;
}
if($CreatorChineseName){
    $nameDisplay = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
} else{
    $nameDisplay=$CreatorEnglishName;
}

$ModifyUserObj = new libuser($ModifyBy);
if(empty($ModifyUserObj->UserID)){
    $ModifyUserinfoAry= $ModifyUserObj->ReturnArchiveStudentInfo($ModifyBy);
    $ModifierEnglishName = $ModifyUserinfoAry['EnglishName'];
    $ModifierChineseName = $ModifyUserinfoAry['ChineseName'];
}else{
    $ModifierEnglishName = $ModifyUserObj->EnglishName;
    $ModifierChineseName = $ModifyUserObj->ChineseName;
}
if($ModifierChineseName){
    $ModifiernameDisplay = Get_Lang_Selection($ModifierChineseName, $ModifierEnglishName);
} else{
    $ModifiernameDisplay=$ModifierEnglishName;
}




//debug_pr('$inputBy = '.$inputBy);
//debug_pr('$englishName = '.$englishName);


########## Personal Photo
//$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
$photo_personal_link = $userObj->returnDefaultPersonalPhotoPath();
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

// $linterface->LAYOUT_START();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function goSubmit(thisUrl) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>	
	var obj = document.form1;
	//if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin," ".$i_UserPassword?>")) return false;
	//if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value!="" && (obj.pwd.value != obj.pwd2.value))
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword']){ ?>  	
	if(obj.pwd.value != ''){
		var check_password_result = CheckPasswordCriteria(obj.pwd.value,'<?=$li->UserLogin?>',<?=$PasswordLength?>);
		if(check_password_result.indexOf(1) == -1){
			var password_warning_msg = '';
			for(var i=0;i<check_password_result.length;i++){
				password_warning_msg += password_warnings[check_password_result[i]] + "\n";
			}
			alert(password_warning_msg);
			obj.pwd.focus();
			return false;
		}
	}
<?php } ?>	
	// check email
	if(!check_text(obj.UserEmail, "<?php echo $i_alert_pleasefillin.$Lang['Gamma']['UserEmail']; ?>."))  return false;
	if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
	
	if(obj.status[0].checked==false && obj.status[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
	obj.action = thisUrl;
	obj.submit();
}

function goBack() {
	document.form1.action = "index.php";
	document.form1.submit();
}
</script>
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
                      <div class="table_board">

						<table class="form_table" border="0">
						  <tr>
						    <td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
						    </tr>
						 
						  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td width="75%"><?=$userInfo['UserLogin']?></td>
						    <td rowspan="5"><img src="<?=$photo_personal_link?>" width="100" height="130" title="<?=$Lang['Personal']['PersonalPhoto']?>" /></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
						    <td>
						    	<input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/>
						    	<?php
								if(isset($_SESSION['AlumniAccountUpdatePasswordError'])){
									echo "<font color='#FF0000'>".$_SESSION['AlumniAccountUpdatePasswordError']."</font>";
									unset($_SESSION['AlumniAccountUpdatePasswordError']);
								}
								?>
						    </td>
						  </tr>
						  <tr>
						    <td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> 
						      (<?=$Lang['AccountMgmt']['Retype']?>)        </td>
						  </tr>
						  <tr>
						  	<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
						  
  							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_TEACHER) )
									{	
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" <?=$disabled?>/><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" <?=$enabled?>/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label> - <span id="IMapUserEmail"><?=$IMapEmail?></span><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>: <input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['Gamma']['UserEmail']?></td>
						    <td><input class="textbox_name" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$userInfo['UserEmail']?>">
						    <br /><span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0" || $status=="") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						  
						   <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
								<td colspan="2">
									<input name="barcode" type="text" id="barcode" class="textboxnum" value="<?=$barcode?>" maxlength="30"/>
									<? if(sizeof($errorAry)>0 && in_array($barcode, $errorAry)) echo "<font color='#FF0000'>".$Lang['AccountMgmt']['Barcode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
								</td>
							</tr>
						  
						  <tr>
						    <td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
						    </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$i_general_name?></td>
						    <td colspan="2" ><span class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)</span>
						      <input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>"/>
						    </td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>"/>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserNickName?></td>
						    <td colspan="2"><input name="nickname" type="text" id="nickname" class="textbox_name" value="<?=$nickname?>"/></td>
						  </tr>
						  <tr>
						    <td><span class="tabletextrequire">*</span><?=$i_UserGender?></td>
						    <td colspan="2"><input type="radio" name="gender" id="genderM" value="M" <? if($gender=="M") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F"  <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$Lang['AccountMgmt']['UserDisplayTitle']?></td>
						    <td colspan="2"><span class="sub_row_content">(<?=$ip20_lang_eng?>)</span>
						      <input name="engTitle" type="text" id="engTitle" class="textbox_name" value="<?=$engTitle?>"/> <?=$select_eng_title?>
						       </td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiTitle" type="text" id="chiTitle" class="textbox_name" value="<?=$chiTitle?>"/> <?=$select_chi_title?>
						       </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserAddress?></td>
						    <td colspan="2"><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="4"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>"/></td>
						  </tr>
						  
						  <!-- Additional Info //-->
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
								
						<tr>
							<td class="formfieldtitle"><?=$i_ClassName?></td>
							<td colspan="2"><input name="classname" type="text" id="classname" class="textboxnum" value="<?=$classname?>"/></td>
						</tr>
						<tr>
							<td class="formfieldtitle"><?=$i_ClassNumber?></td>
							<td colspan="2"><input name="classnumber" type="text" id="classnumber" class="textboxnum" value="<?=$classnumber?>"/></td>
						</tr>
						<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['YearOfLeft']?></td>
							<td colspan="2"><input name="yearofleft" type="text" id="yearofleft" class="textboxnum" value="<?=$yearofleft?>"/></td>
						</tr>
						
						 

					
						
						
								
						<?if(!$plugin['imail_gamma']) {?>
							
						  <? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(TYPE_TEACHER, $webmail_identity_allowed)) { ?>
						  <!-- Internet Usage //-->
						  <tr>
						    <td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  </tr>
						  <?
						  	$sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '".$uid."'";
						  	$result = $laccount->returnVector($sql);
							$can_access_iMail = substr($result[0],0,1);
							$can_access_iFolder = substr($result[0],1,1);
						  ?>
						  
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
							    <td colspan="2"><input type="checkbox" value="1" name="open_webmail" disabled <? echo ($can_access_iMail)? "checked" : "" ?> />
							      <?=$i_Mail_AllowSendReceiveExternalMail?></td>
							  </tr>
							  
							  
							  
						  	<?}?>
						  <?}?>
						  
						  
						
						 <?php if($plugin['radius_server']){ 
						  	$enable_wifi_access = !$laccount->isUserDisabledInRadiusServer($userInfo['UserLogin']);
						  ?>
						  <tr>
						  	<td colspan="2"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['WifiUsage']?> -</em></td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></td>
						  	<td><input type="checkbox" name="enable_wifi_access" value="1" <?=$enable_wifi_access?'checked="checked"':''?> /></td>
						  </tr>
						  <?php } ?>
						  
						  
						  
    					    <tr>
    							<td colspan="3"><em class="form_sep_title"> -  <?=$Lang['AccountMgmt']['CreatedAndModifiedRecords']?> -</em></td>
    						</tr>
    						<?php  if($nameDisplay){?>
    							<tr>
    								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['CreateUserName']?></td>
    								<td colspan="2"><?=$nameDisplay?></td>
    							</tr>
    							<tr>
    								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['CreateUserDate']?></td>
    								<td colspan="2"><?=$DateInput?></td>
    							</tr>
    						<? } ?>
    						<tr>
    							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['LastEditUserName']?></td>
    							<td colspan="2"><?=$ModifiernameDisplay?></td>
    						</tr>
    						<tr>
    							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['LastEditUserDate']?></td>
    							<td colspan="2"><?=$DateModified?></td>
    						</tr>
						  
						  
						  
						  
						 
						</table>
						<p class="spacer"></p>
                      </div>
                      	 	

                      
                      <div class="edit_bottom">
                          <p class="spacer"></p>
                          <?= $linterface->GET_ACTION_BTN($button_submit, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>

<input type="hidden" name="skipStep2" id="skipStep2" value="">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
<input type="hidden" name="original_pass" id="original_pass" value="<?=$userInfo['UserPassword']?>">



</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>