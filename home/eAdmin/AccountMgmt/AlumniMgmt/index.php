<?php
// modifying : 

/*
 *  Date:	2015-12-30 Carlos 
 *  		$plugin['radius_server'] - added table tool buttons for enable/disable Wi-Fi access and export Wi-Fi access status list.
 * 
 *  Date:	2014-09-12	YatWoon
 *  		use "user_id[]" instead of userID[] (Case#L67733)
 * 
 *	Date:	2014-03-11	Carlos
 *			Added [Import Forwarding Emails] & [Export Forwarding Emails] buttons for Lassel College
 */


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_alumni_recordstatus", "recordstatus");
$arrCookies[] = array("ck_alumni_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$CurrentPageArr['AlumniMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
// $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# online help button
// $onlineHelpBtn = gen_online_help_btn_and_layer('user','staff');
// $TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);


$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
$toolbar .= $linterface->GET_LNK_EMAIL("javascript:checkPost(document.form1,'email.php')",$button_email,"","","",0);
if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['BatchSetForwardingEmail']){
	$toolbar .= $linterface->GET_LNK_IMPORT("../import/import_forwarding_email.php?TabID=".USERTYPE_ALUMNI,$Lang['AccountMgmt']['ImportForwardingEmails'],"","","",0);
	$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_ALUMNI,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
}
if($plugin['radius_server']){
	$toolbar .= $linterface->GET_LNK_EXPORT("../export_wifi_status.php?TabID=".USERTYPE_ALUMNI,$Lang['AccountMgmt']['ExportWiFiAccessStatus'],"","","",0);
}

# Record Status 
$recordStatusMenu = "<select name=\"recordstatus\" id=\"recordstatus\" onChange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($recordstatus) || $recordstatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($recordstatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($recordstatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "</select>";

# YearOfLeft filter
$sql = "select distinct(YearOfLeft) from INTRANET_USER where RecordType=4 and YearOfLeft!='' order by YearOfLeft";
$result = $laccount->returnVector($sql);
$YearOfLeft_Filter = getSelectByValue($result, " name='YearOfLeft' onChange='reloadForm()' ", $YearOfLeft, 1);			

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND RecordStatus=".STATUS_SUSPENDED;
if($keyword != "")	
	$conds .= " AND (UserLogin like '%$keyword%' OR
				UserEmail like '%$keyword%' OR
				EnglishName like '%$keyword%' OR
				ChineseName like '%$keyword%' OR
				ClassName like '%$keyword%'
				)";
if($YearOfLeft)
{
	$conds .= " and YearOfLeft=$YearOfLeft";
}				
				/*
$sql = "SELECT
			concat('<a href=\"javascript:goEdit(', a.UserID ,')\">', IFNULL(IF(a.EnglishName='', '---', a.EnglishName),'---') ,'</a>') as EnglishName,
			IFNULL(IF(a.ChineseName='', '---', a.ChineseName),'---') as ChineseName,
			if(b.YearOfLeft, b.YearOfLeft, a.YearOfLeft),
			if(b.ClassName, b.ClassName, a.ClassName),
			if(b.ClassNumber, b.ClassNumber, a.ClassNumber),
			a.UserLogin,
			IFNULL(a.LastUsed, '---') as LastUsed,
			CASE a.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended']."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'userID[]\' id=\'userID[]\' value=', a.UserID ,'>') as checkbox
		FROM 
			INTRANET_USER as a
			left join INTRANET_ARCHIVE_USER as b on (b.UserID=a.UserID)
		WHERE
			a.RecordType = ".TYPE_ALUMNI."
			$conds ";
*/	
$sql = "SELECT
			concat('<a href=\"javascript:goEdit(', UserID ,')\">', IFNULL(IF(EnglishName='', '---', EnglishName),'---') ,'</a>') as EnglishName,
			IFNULL(IF(ChineseName='', '---', ChineseName),'---') as ChineseName,
			YearOfLeft,
			ClassName,
			if(ClassNumber=0, '', ClassNumber),
			UserLogin,
			IFNULL(LastUsed, '---') as LastUsed,
			CASE RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended']."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', UserID ,'>') as checkbox
		FROM 
			INTRANET_USER
		WHERE
			RecordType = ".TYPE_ALUMNI."
			$conds ";		
$result = $laccount->returnArray($sql,9);			
$li->sql = $sql;
$li->field_array = array("EnglishName", "ChineseName", "YearOfLeft", "ClassName", "ClassNumber","UserLogin", "LastUsed", "recordStatus");
$li->no_col = sizeof($li->field_array)+2;
//$li->IsColOff = "UserMgmtAlumniAccount";
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_Profile_DataLeftYear)."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $i_general_class)."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $i_ClassNumber)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "index.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function newUser() {
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() {
	document.form1.action = "../export.php?TabID=<?=TYPE_ALUMNI?>";
	document.form1.submit();
}

function goImport() {
	window.location.href ="../import/import.php?TabID=<?=TYPE_ALUMNI?>";
}

function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}
</script>
<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<?=$YearOfLeft_Filter?> <?=$recordStatusMenu?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		<a href="javascript:changeFormAction(document.form1,'user_id[]','../email_password.php')" class="tool_email"><?=$Lang['AccountMgmt']['SendResetPasswordEmail']?></a>
		<? if($plugin['radius_server']){ ?>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../enable_wifi_access.php')" class="tool_approve"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></a>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../disable_wifi_access.php')" class="tool_reject"><?=$Lang['AccountMgmt']['DisableWifiAccess']?></a>
        <? } ?>
		<? if($recordstatus!=1) {?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<? } ?>
		<? if($recordstatus=="" || $recordstatus==1) {?>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<? } ?>		
		<a href="javascript:checkRemove(document.form1,'user_id[]','remove.php','<?=$Lang['AccountMgmt']['DeleteUserWarning']?>')" class="tool_delete"><?=$button_delete?></a>
		</div>
	</td>
</tr>
</table>

<?= $li->display()?>
	
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/AlumniMgmt/index.php">
</form>
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>