<?php
# modifying by : 

/******************************************
 *  modification log:
 *	Date	:	2015-12-11	Omas
 *	Detail	:	1) replace ereg() by preg_match() for PHP5.4
 *				2) standardizeFormPostValue() to output remove extra slash
 *
 *	Date	:	20100902 Henry Chow
 *	Detail	:	only allow lowercase letter & number in "user login" field
 *
******************************************/


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();
/*
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/

$laccount = new libaccountmgmt();

if($task=='userlogin')
{
	//$result = $laccount->validateLogin($userlogin);

	$alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','_');
	$numCharacters = array('1','2','3','4','5','6','7','8','9','0');
	
    if ($special_option['login_uppercase'])
    {
        $additional_chars = "A-Z";
        $alphabet = array_merge($alphabet, array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'));
    }
    else
    {
        $additional_chars = "";
    }
    if ($special_option['login_hypen'] && $special_option['login_first_num'])
    {
        $validCharacters="^[_a-z".$additional_chars."0-9-]{0,30}$";
    }
    else if ($special_option['login_hypen'])
    {
        $validCharacters="^[a-z".$additional_chars."]([_a-z".$additional_chars."0-9-]{0,30})$";
    }
    else if ($special_option['login_first_num'])
    {
        $validCharacters="^[_a-z".$additional_chars."0-9]{0,30}$";
    }
    else
    {
        $validCharacters="^[a-z".$additional_chars."]([_a-z".$additional_chars."0-9]{0,30})$";
    }

	if($userlogin!="") {
		if (in_array($userlogin, (array)$system_reserved_account)) {
			$output = "<font color='#FF0000'>\"$userlogin\" ".$Lang['StudentRegistry']['IsInvalid']."</font>";	
			$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"0\">";
		}
		else if(!$special_option['login_hypen'] && !$special_option['login_first_num'] && (is_numeric(substr($userlogin,0,1)) || !in_array(substr($userlogin,0,1), $alphabet))) {	
			$output = "<font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['StartedWithA2Z']."</font>";	
			$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"2\">";
		}
		else {
			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
			$result = $laccount->returnVector($sql);
			
			if($result[0]!=0) {
				$output = "<font color='#FF0000'>\"$userlogin\" ".$Lang['AccountMgmt']['IsNotAvailable']."</font>";	
				$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"0\">";
			} else {
				
//				$success = ereg($validCharacters, $userlogin);
				$success = preg_match('/'.$validCharacters.'/', $userlogin);
				if($success) {
					$output = "<font color='#008F37'>\"$userlogin\" ".$Lang['AccountMgmt']['IsAvailable']."</font>";
					$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"1\">";	
				} else {
					$output = "<font color='#FF0000'>\"$userlogin\" ".$Lang['StudentRegistry']['IsInvalid']."</font>";	
					$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"0\">";			
				}
			}			
		}
	}
	
}


echo standardizeFormPostValue($output);
?>
