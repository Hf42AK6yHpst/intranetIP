<?php
// Using : 
/*
 * Change Log:
 *  Date: 2019-10-11 (Bill)
 *          - added folder for archive report
 *  Date: 2018-12-11 (Philips + Bill)
 *          - allow parent access
 *  Date: 2018-06-14 (Bill)
 *          - check access right
 *          - default page based on user role 
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/reportcardkindergarten_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/reportcard_kindergarten/config.inc.php");
include_once($PATH_WRT_ROOT."includes/reportcard_kindergarten/libreportcardkindergarten.php");
include_once($PATH_WRT_ROOT."includes/reportcard_kindergarten/libreportcardkindergarten_ui.php");


# Initial objects
$indexVar['libreportcard'] = new libreportcardkindergarten();
$indexVar['libreportcard_ui'] = new libreportcardkindergarten_ui();
$indexVar['libfilesystem'] = new libfilesystem();
$indexVar['JSON'] = new JSON_obj();


# Get Basic Data
$indexVar['curUserId'] = $_SESSION['UserID'];
$indexVar['thisBasePath'] = $PATH_WRT_ROOT;
$indexVar['thisDbName'] = $indexVar['libreportcard']->DBName;
$indexVar['thisYearId'] = $indexVar['libreportcard']->AcademicYearID;
// $indexVar['thisImage'] = $PATH_WRT_ROOT."file/reportcard_kindergarten/images/";
$thisSchoolYearName = getAYNameByAyId($indexVar['thisYearId']);
$indexVar['thisImage'] = $PATH_WRT_ROOT."file/reportcard_kindergarten/".$thisSchoolYearName."/images/";
$indexVar['thisArchiveReport'] = $PATH_WRT_ROOT."file/reportcard_kindergarten/".$thisSchoolYearName."/reports/";
$indexVar['emptySymbol'] = '---';


# Handle all POST / GET values by urldecode(), stripslashes(), trim()
array_walk_recursive($_POST, 'handleFormPost');
array_walk_recursive($_GET, 'handleFormPost'); 


# Get System Message
$returnMsgKey = $_GET['returnMsgKey'];
$indexVar['returnMsgLang'] = $Lang['General']['ReturnMessage'][$returnMsgKey];


# Set Page Tab
$indexVar['task'] = $_POST['task']? $_POST['task'] : $_GET['task'];
if ($indexVar['task'] == '') {
	// Go to module index page if not defined
	//$indexVar['task'] = 'mgmt'.$ercKindergartenConfig['taskSeparator'].'topic_timetable'.$ercKindergartenConfig['taskSeparator'].'list';
    $indexVar['task'] = $indexVar['libreportcard']->Get_User_Default_Page();
}
$CurrentPage = $indexVar['task'];
$CurrentPageArr['eReportCardKindergarten_eAdmin'] = 1;

# Special handling for Parent Access
if($indexVar['libreportcard']->IS_KG_PARENT_ACCESS()) {
    unset($CurrentPageArr['eReportCardKindergarten_eAdmin']);
    $CurrentPageArr['eServiceeReportCardKindergarten'] = 1;
}


# Check Access Right
$indexVar['libreportcard']->Check_Access_Right($indexVar['task']);


# Check Tablet Page
$taskStructureAry = explode($ercKindergartenConfig['taskSeparator'], $indexVar['task']);
$isDeviceView = $taskStructureAry[0]=="lesson";


# Load Controller
$indexVar['controllerScript'] = 'controllers/'.str_replace($ercKindergartenConfig['taskSeparator'], '/', $indexVar['task']).'.php';
if (file_exists($indexVar['controllerScript']))
{
	// Include Controller Script
	include_once($indexVar['controllerScript']);
	
	# Load Template
	$indexVar['viewScript'] = 'views/'.str_replace($ercKindergartenConfig['taskSeparator'], '/', $indexVar['task']).'.tmpl.php';
	if (file_exists($indexVar['viewScript']))
	{
		// Include Template Script
		include_once($indexVar['viewScript']);

		# Page Footer
		if( strpos($indexVar['task'], 'print') !== false ||
            strpos($indexVar['task'], 'thickbox') !== false ||
            strpos($indexVar['task'], 'mgmt.generate_reports.archive') !== false ||
            strpos($indexVar['task'], 'settings.admin_group.tb_member') !== false
        ) {
			// Print Page and Thickbox => no footer
		}
		else if ($isDeviceView) {
			$indexVar['libreportcard_ui']->Echo_Device_Layout_Stop();
		}
		else {
			$indexVar['libreportcard_ui']->Echo_Module_Layout_Stop();
		}
	}
	else {
		// Update or Ajax Script => without template script
	}
}
else {
	No_Access_Right_Pop_Up();
}

intranet_closedb();
?>