<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.award_list.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['Code']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxnum requiredField" name="Code" id="Code" value="<?=$awardCode?>">
				<div style="display:none;" class="warnMsgDiv" id="Code_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameB5']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="CH_Name" id="CH_Name" value="<?=$awardNameCH?>">
				<div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameEN']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="EN_Name" id="EN_Name" value="<?=$awardNameEn?>">
				<div style="display:none;" class="warnMsgDiv" id="EN_Name_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
	
	<input type='hidden' id='AwardID' name='AwardID' value=<?=$awardID?> >
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$button_submit?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$button_back?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
function checkForm()
{
	$('#Code_Warn').hide();
	$('#CH_Name_Warn').hide();
	$('#EN_Name_Warn').hide();
	
	var check = true;
	if(!$('#Code').val()){
		$('#Code_Warn').show();
		$('#Code').focus();
		check = false;
	}
	if(!$('#CH_Name').val()){
		$('#CH_Name_Warn').show();
		$('#CH_Name').focus();
		check = false;
	}
	if(!$('#EN_Name').val()){
		$('#EN_Name_Warn').show();
		$('#EN_Name').focus();
		check = false;
	}
	return check;
}

function goSubmit()
{
	if(checkForm())
	{
    	$.post(
    		"?task=settings.award_list.ajax_task", 
    		$('form').serialize(),
    		function(data_str) {
    			if (data_str == '') {
					form1.submit();
    			}
    			else {
        			data = data_str.split(',');
        			$.each(data, function(key, value) {
            			console.log(value);
        				$('#' + value + '_Warn').show();
        				$('#' + value).focus();
    				});
    			}
    		}
    	);
	}
}

function goBack(){
	window.location.href = "index.php?task=settings.award_list.list";
}

function goDelete(){
	$('#goDelete').val(1);
}

function goURL(){
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>award_list<?=$ercKindergartenConfig['taskSeparator']?>list';
}

<?php if ($_GET['isEdit']) { ?>
    $(document).ready(function(){
        $('#NeedConfirmMsg').show();
    });
<?php } ?>
</script>