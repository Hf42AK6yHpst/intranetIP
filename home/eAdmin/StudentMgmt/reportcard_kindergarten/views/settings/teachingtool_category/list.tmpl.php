<meta Http-Equiv="Cache-Control" Content="no-cache">
<script type="text/javascript">
$( document ).ready(function() {
	$('.checkbox').click(function(){
		if($(this).attr('checked')){
			// do nothing
		}
		else{
			$('#checkmaster').removeAttr('checked');
		}
	});
});

function CheckAll(){
	if($('#checkmaster').attr('checked')){
		$('.checkbox').attr('checked','checked');
	}
	else{
		$('.checkbox').removeAttr('checked');
	}
}

function goNew() {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>teachingtool_category<?=$ercKindergartenConfig['taskSeparator']?>edit';
}

function goEdit(catID) {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>teachingtool_category<?=$ercKindergartenConfig['taskSeparator']?>edit&isEdit=1&catID='+catID;
}

function Edit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push ($(this).val());
	});
	
	if(EditID.length==1){
		goEdit(EditID[0]);
	}
	else if(EditID.length>1){
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}

function Delete(){
	var DeleteID = [];
	$('.checkbox:checked').each(function(){
		DeleteID.push ($(this).val());
	});
	
	if(DeleteID.length){
		$('#form1').attr('action', 'index.php?task=settings.teachingtool_category.delete');
		$('#form1').submit();
	}
	else{
		alert("<?=$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete']?>");
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<!--
		<div class="table_filter">
			<?=$YearSelection?>
		</div>
		<p class="spacer"></p>
		-->
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?= $table?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<?=$htmlAry['dndTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>