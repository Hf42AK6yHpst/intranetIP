<script type="text/javascript">
$(document).ready( function() {
	$('input#codeTb').focus();

	<?php if($_GET["isEdit"]) { ?>
	    $('#NeedConfirmMsg').show();
	<?php } ?>
});

function goBack() {
	window.location = 'index.php?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>langbehavior_item<?=$ercKindergartenConfig["taskSeparator"]?>list';
}

function goSubmit() {
	var canSubmit = true;
	var isFocused = false;
	var emptyCount = 0;
	
	$('div.warnMsgDiv').hide();
	
	// disable action button to prevent double submission
	$('input.actionBtn').attr('disabled', 'disabled');
	
	// check required fields
	$('.requiredField').each( function()
	{
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId))
		{
			console.log(_elementId);
			if(_elementId == 'Code') {
				canSubmit = false;
				$('div#' + _elementId + 'EmptyWarnDiv').show();
				if (!isFocused) {
					// focus the first element
					$('#' + _elementId).focus();
					isFocused = true;
				}
			}
			else {
				emptyCount++;
			}
		}
	});
	$('select').each( function()
	{
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId))
		{
			canSubmit = false;
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	
	// display warning if both topic lang empty
	if(emptyCount > 0)
	{
		canSubmit = false;
		$('.requiredField').each( function()
		{
			var _elementId = $(this).attr('id');
			if (isObjectValueEmpty(_elementId))
			{
				if(_elementId != 'codeTb') {
					$('div#' + _elementId + 'EmptyWarnDiv').show();
					if (!isFocused) {
						// focus the first element
						$('#' + _elementId).focus();
						isFocused = true;
					}
				}
			}
		});
	}
	
	if (canSubmit) {
//		$.post(
//			"ajax_task.php", 
//			{ 
//				task: 'validateCode',
//				formId: $('input#formId').val(),
//				subjectId: $('input#subjectId').val(),
//				code: $('input#codeTb').val(),
//				subjectTopicId: $('input#subjectTopicId').val()
//			},
//			function(ReturnData) {
//				if (ReturnData == '1') {
//					$('form#form1').attr('action', 'edit_update.php').submit();
//				}
//				else {
//					$('div#codeTbCodeInUseWarnDiv').show();
//					$('input.actionBtn').attr('disabled', '');
//				}
//			}
//		);
		$('form#form1').attr('action', '?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>langbehavior_item<?=$ercKindergartenConfig["taskSeparator"]?>edit_update').submit();
	}
	else {
		// enable the action buttons if not allowed to submit
		$('input.actionBtn').attr('disabled', '');
	}
}
</script>

<?=$htmlAry["navigation"]?>
<br> 
<br>

<form name="form1" id="form1" method="POST">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eReportCardKG"]["Setting"]["Code"]?></td>
				<td>
					<input type="text" maxlength="255" class="textboxnum requiredField" name="Code" id="Code" value="<?=$Code?>">
					<div style="display:none;" class="warnMsgDiv" id="CodeEmptyWarnDiv">
						<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["Code"]?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eReportCardKG"]["Setting"]["NameB5"]?></td>
				<td>
					<input type="text" maxlength="255" class="textboxtext requiredField" name="CH_Name" id="CH_Name" value="<?=$Name_CH?>">
					<div style="display:none;" class="warnMsgDiv" id="CH_NameEmptyWarnDiv">
						<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["NameB5"]?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eReportCardKG"]["Setting"]["NameEN"]?></td>
				<td>
					<input type="text" maxlength="255" class="textboxtext requiredField" name="EN_Name" id="EN_Name" value="<?=$Name_EN?>">
					<div style="display:none;" class="warnMsgDiv" id="EN_NameEmptyWarnDiv">
						<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["NameEN"]?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eReportCardKG"]["Setting"]["Type"]?></td>
				<td>
					<?=$htmlAry["topicCategorySelection"]?>
					<div style="display:none;" class="warnMsgDiv" id="TopicCatIDEmptyWarnDiv">
						<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["Type"]?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eReportCardKG"]["Setting"]["ApplyForm"]?></td>
				<td>
					<?=$htmlAry["yearSelection"]?>
					<div style="display:none;" class="warnMsgDiv" id="YearIDEmptyWarnDiv">
						<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["ApplyForm"]?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["General"]["Term"]?></td>
				<td>
					<?=$htmlAry["termSelection"]?>
					<div style="display:none;" class="warnMsgDiv" id="TermIDEmptyWarnDiv">
						<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Term"]?></span>
					</div>
				</td>
			</tr>
		</table>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />

	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
		<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
		<p class="spacer"></p>
	</div>
	
	<input type='hidden' id='topicId' name='topicId' value=<?=$topicIdAry[0]?> >
	<input type='hidden' id='goDelete' name='goDelete' value='0' >
</form>