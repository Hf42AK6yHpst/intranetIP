<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.form_topic.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['Code']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxnum requiredField" name="Code" id="Code" value="<?=$topicCode?>">
				<div style="display:none;" class="warnMsgDiv" id="Code_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameB5']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="CH_Name" id="CH_Name" value="<?=$topicCodeCH?>">
				<div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameEN']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="EN_Name" id="EN_Name" value="<?=$topicCodeEN?>">
				<div style="display:none;" class="warnMsgDiv" id="EN_Name_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['ApplyForm']?></td>
			<td>
				<?=$YearSelection?>
				<div style="display:none;" class="warnMsgDiv" id="Form_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseSelectForm']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Remarks'] ?></td>
			<td>
				<textarea wrap="virtual" onfocus="this.rows=5;" rows="2" cols="70" id="Remarks" name="Remarks" class="tabletext requiredField"><?=$remarks?></textarea>
			</td>
		</tr>
		
		<!--
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['LearningZone']['Title']?></td>
			<td>
				<span class="table_row_tool row_content_tool"><a onclick="goDelete('badge');" title="<?=$button_delete?>" class="delete" href="javascript:void(0);"></a></span>
				<div id='cataList'><?=$catagory?></div>
				<br style="clear:both;">
				<br style="clear:both;">
				<span class="table_row_tool row_content_tool">
					<?=$AddCata?>
				</span>
			</td>
		</tr>
		-->
		</tbody>
	</table>
	
	<input type='hidden' id='TopicID' name='TopicID' value=<?=$topicID?> >
</form>

<?=$thickBox?>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$button_submit?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$button_back?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
function checkForm(){
	$('#EN_Name_Warn').hide();
	$('#B5_Name_Warn').hide();
	$('#Code_Warn').hide();
	$('#Form_Warn').hide();

	var check = true;
	if(!$('#YearID').val()){
		$('#YearID').focus();
		check = false;
	}
	if(!$('#EN_Name').val()){
		$('#EN_Name_Warn').show();
		$('#EN_Name').focus();
		check = false;
	}
	if(!$('#CH_Name').val()){
		$('#CH_Name_Warn').show();
		$('#CH_Name').focus();
		check = false;
	}
	if(!$('#Code').val()){
		$('#Code_Warn').show();
		$('#Code').focus();
		check = false;
	}
	if(!$('#YearID').val()){
		$('#Form_Warn').show();
		$('#YearID').focus();
		check = false;
	}
	return check;
}

function goSubmit(){
	if(checkForm()){
		form1.submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=settings.form_topic.list";
}

function goDelete(){
	$('#goDelete').val(1);
}

function goURL(){
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>form_topic<?=$ercKindergartenConfig['taskSeparator']?>list';
}

function ThickBoxGoSubmit(){

	//Transit data to Form
	var CataID = [];
	if( $('#CataGoryID').val() ){
		CataID.push( $('#CataGoryID').val() );
	}
	cata = $('#cata').val();
	NumOfCata = cata.length;
	for(i=0;i<NumOfCata;i++){
		CataID.push( cata[i] );
	}
	$('#TB_window').fadeOut();
	tb_remove();
	numOfCataID = CataID.val().length;
	document.getElementById("cataList").innerHTML = CataID+"<br>"+CataID;
// 	$('#cataList').html(CataID);
// 	$('#cata').each(function(){
// 		CataID.push( $(this).val() );
// 		console.log( $(this).val() );
// 	});
// 	CataID.push( $('#cata').val() );
}

function ThickBoxClose(){
	 $('#TB_window').fadeOut();
	 tb_remove();
}

<?php if ($_GET['isEdit']) { ?>
    $(document).ready(function(){
        $('#NeedConfirmMsg').show();
    });
<?php } ?>
</script>