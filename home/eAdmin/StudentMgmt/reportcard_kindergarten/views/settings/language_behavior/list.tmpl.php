<meta Http-Equiv="Cache-Control" Content="no-cache">

<script type="text/javascript">
$(document).ready( function() {
	loadDataTable();
});

function initDndTable() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "DataTable")
			{
				Block_Element(table.id);
				
				// Get the order string
				var rowArr = table.tBodies[0].rows;
				var numOfRow = rowArr.length;
				var recordIDArr = new Array();
				for (var i=0; i<numOfRow; i++) {
					var tempId = parseInt(rowArr[i].id.replace('tr_', ''));
					if (tempId != "" && !isNaN(tempId)) {
						recordIDArr[recordIDArr.length] = tempId;
					}
				}
				var recordIDStr = recordIDArr.join(',');
				
				// Update DB
				$.post(
					"?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior<?=$ercKindergartenConfig["taskSeparator"]?>ajax_task",
					{
						dataType: "reorderTopicCat" ,
						displayOrderString: recordIDStr,
						catId: $('select#CatID').val()
					},
					function(ReturnData)
					{
						// Reset the ranking display
						var jsRowCounter = 0;
						$('span.rowNumSpan').each( function () {
							$(this).html(++jsRowCounter);
						})
						
						// Get system message
						if (ReturnData == '1') {
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						}
						else {
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function loadDataTable() {
	$('div#dataTableDiv').html('<?=$indexVar["libreportcard_ui"]->Get_Ajax_Loading_Image()?>').load(
		"?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior<?=$ercKindergartenConfig["taskSeparator"]?>ajax_task", 
		{
			dataType: 'getDataTable',
			catId: $('select#CatID').val() 
		},
		function(ReturnData) {
			initDndTable();
		}
	);
}

function goNew() {
	$('form#form1').attr('action', '?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior<?=$ercKindergartenConfig["taskSeparator"]?>edit').submit();
}

function goEdit() {
	$('form#form1').attr('action', '?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior<?=$ercKindergartenConfig["taskSeparator"]?>edit&isEdit=1').submit();
}

function goDelete() {
	var DeleteID = [];
	$('.ScaleChk:checked').each(function(){
		DeleteID.push ($(this).val());
	});
	if(DeleteID.length){
		if(confirm('<?= $Lang['General']['JS_warning']['ConfirmDelete'] ?>')){
			$('form#form1').attr('action', '?task=settings<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior<?=$ercKindergartenConfig["taskSeparator"]?>delete').submit();
		}
	}else{ //no click check box
		alert("<?=$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete']?>");
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry["contentTool"]?>
			<?=$htmlAry["searchBox"]?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry["categorySelection"]?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry["dbTableActionBtn"]?>
		<div id="dataTableDiv"><!--load by ajax--></div>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry["hiddenField"]?>
</form>