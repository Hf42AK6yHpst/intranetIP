<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.ability_grade_remark.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name']?></td>
			<td><?=$indexSelection//$IndexCat?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Grading']?></td>
        	<?php if($isEdit) { ?>
    			<td><?=$IndexGrade?></td>
			<?php } else { ?>
    			<td><?=$gradeSelection?></td>
			<?php } ?>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['UpperLimit']?></td>
			<td><?=$IndexLowerLimit?> ~ <?=$IndexUpperLimit?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Comment']?></td>
			<td>
				<textarea maxlength="255" class="textboxtext requiredField" name="IndexOriRemark" id="IndexOriRemark"><?=$IndexRemarks?></textarea>
				<div style="display:none;" class="warnMsgDiv" id="IndexOriName_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['OriComment']?></td>
			<td>
				<textarea maxlength="255" class="textboxtext requiredField" name="IndexRemark" id="IndexRemark" ><?=$IndexRemarkAll?></textarea>
				<div style="display:none;" class="warnMsgDiv" id="IndexName_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
	</tbody>
	</table>
	
	<?php if($isEdit) { ?>
    	<input type='hidden' id='IndexID' name='IndexID' value='<?=$IndexID?>'>
    	<input type='hidden' id='IndexGradingRange' name='IndexGradingRange' value='<?=$IndexGradingRange?>'>
    	<input type='hidden' id='isResetRemark' name='isResetRemark' value='0'>
	<?php } ?>
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$button_submit?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$button_back?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<?php if($isEdit) { ?>
		<input type="button" value="<?=$Lang['eReportCardKG']['Management']['AbilityRemarks']['Reset']?>" class="formbutton_v30 print_hide " onclick="goReset()" id="resetBtn" name="resetBtn">
	<?php } ?>
	<p class="spacer"></p>
</div>

<script>
function checkForm() {
	$('#IndexName_Warn').hide();
	
	var check = true;
	if(!$('#IndexRemark').val()) {
		$('#IndexName_Warn').show();
		$('#IndexRemark').focus();
		check = false;
	}
	if(!$('#IndexOriRemark').val()) {
		$('#IndexOriName_Warn').show();
		$('#IndexOriRemark').focus();
		check = false;
	}
	
	return check;
}

function goSubmit() {
	if(checkForm()) {
		form1.submit();
	}
}

function goBack() {
	window.location.href = "index.php?task=settings.ability_grade_remark.list";
}

<?php if($isEdit) { ?>
    function goReset() {
    	if(confirm('<?=$Lang['eReportCardKG']['Management']['AbilityRemarks']['ConfirmResetRemark']?>')) {
    		$('#IndexRemark').val('');
    		$('#isResetRemark').val(1);
    		form1.submit();
    	}
    }
    
    function changeRemarkAbility() {
    	if(confirm('<?=$Lang['eReportCardKG']['Management']['AbilityRemarks']['ChangeAbilityResetAll']?>')) {
    		form1.action = 'index.php?task=settings.ability_grade_remark.change_ability_cat';
    		form1.submit();
    	}
    }
<?php } else { ?>
	function changeRemarkAbility() {
		// do nothing
	}

	function refreshAvailableIndex() {
		form1.action = 'index.php?task=settings.ability_grade_remark.edit';
		form1.submit();
	}
<?php } ?>
    
function goURL() {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>ability_grade_remark<?=$ercKindergartenConfig['taskSeparator']?>list';
}

<?php if($isEdit) { ?>
    $(document).ready(function(){
        $('#NeedConfirmMsg').show();
    });
<?php } ?>
</script>