<meta Http-Equiv="Cache-Control" Content="no-cache">
<script type="text/javascript">

$(function() {
	  var loadedElements = 0;

	  $('.lazy').lazy({
		  effect: 'fadeIn'
// 		  beforeLoad: function(element){
// 			  console.log('image "' + (element.data('src')) + '" is about to be loaded');
// 		  },
// 		  afterLoad: function(element) {
// 			  loadedElements++;
// 			  console.log('image "' + (element.data('src')) + '" was loaded successfully');
// 		  },
// 		  onError: function(element) {
// 			  loadedElements++;
// 			  console.log('image "' + (element.data('src')) + '" could not be loaded');
// 		  },
// 		  onFinishedAll: function() {
// 			  console.log('finished loading ' + loadedElements + ' elements');
// 			  console.log('lazy instance is about to be destroyed')
// 		  }
	  });
});

$(document).ready(function() {
	$('.checkbox').click(function(){
		if($(this).attr('checked')){
			// do nothing
		}
		else{
			$('#checkmaster').removeAttr('checked');
		}
	});
});

function CheckAll(){
	if($('#checkmaster').attr('checked')){
		$('.checkbox').attr('checked','checked');
	}
	else{
		$('.checkbox').removeAttr('checked');
	}
}
function goNew() {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>teaching_tool<?=$ercKindergartenConfig['taskSeparator']?>edit';
}

function goEdit(codeID) {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>teaching_tool<?=$ercKindergartenConfig['taskSeparator']?>edit&isEdit=1&codeID='+codeID;
}

function Edit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push ($(this).val());
	});
	
	if(EditID.length==1){
		goEdit(EditID[0]);
	}
	else if(EditID.length>1){
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}

function Delete(){
	var DeleteID = [];
	$('.checkbox:checked').each(function(){
		DeleteID.push ($(this).val());
	});
	
	if(DeleteID.length){
		if(confirm('<?= $Lang['General']['JS_warning']['ConfirmDelete'] ?>')){
		//if(checkRemove(document.form1,'checkbox[]','delete.php')) {
			$('#form1').attr('action', 'index.php?task=settings.teaching_tool.delete');
			$('#form1').submit();
		}
	}else{ //no click check box
		alert("<?=$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete']?>");
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$YearSelection?>
			<?=$CatSelection?>
			<?=$ZoneSearchSelection?>
			<?=$ChapterSelection?>
		</div>
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?= $table?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<?=$htmlAry['dndTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>