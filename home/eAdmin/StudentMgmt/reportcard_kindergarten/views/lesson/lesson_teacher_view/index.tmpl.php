<script type="text/javascript">

$(document).ready( function() {
	// reset hidden input field when back
	$('input#class_input').val("<?=$defaultSelectedClass["YearClassID"]?>");
	$('input#topic_input').val("<?=$defaultAvailableTopic["TopicID"]?>");
});

function updateSelection(target_id, target_type, obj) {
	if(target_id==0 || target_type=="") return;
	
	// update selection display and id
	$('span#'+target_type+'title')[0].textContent = obj.textContent;
	$('input#'+target_type+'input').val(target_id);
}

function updateTopicSelection() {
	var ClassID = $('input#class_input').val();
	if(ClassID=="") return;
	
	// clear current topic id
	$('input#topic_input').val("");
	
	$.ajax({
		type: "POST",
		url : "index.php?task=lesson.lesson_teacher_view.get_ajax",
		data:{
			"Type": "TopicSelection",
			"ClassID": ClassID
		},
		success: function(topicSel){
			// update topic selection and set first topic as selected topic
			$('div#TopicSelectionID').html(topicSel);
			$("div#TopicSelectionID li:first-child a:first-child")[0].click()
		}
	});
}

function checkForm() {
	if($('input#class_input').val()=="") {
		alert('<?=$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyClasses']?>');
		return false;
	}
	else if($('input#topic_input').val()=="") {
		alert('<?=$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyTopics']?>');
		return false;
	}
	
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="index.php?task=lesson.lesson_teacher_view.topic">

<div class="col-md-1 col-lg-2"></div>
<div class="center-area col-sm-12 col-md-10 col-lg-8">
	
	<div class="content-area">
		<div class="login-area">
			<div class="bg"><img src="<?=$ercKindergartenConfig['imageFilePath']?>/teacher_login_bg.png"/></div>
			
			<div class="selection-area">
				<div id="ClassSelectionID"><?=$htmlAry['ClassSelection']?></div>
				<div id="TopicSelectionID"><?=$htmlAry['TopicSelection']?></div>
			</div>
		
			<div class="btn-area">
				<?=$htmlAry['submitBtn']?>
			</div>
		</div>
		
		<div class="btn-area">
			<?=$htmlAry['backBtn']?>
		</div>
	</div>
	
</div>
<div class="col-md-1 col-lg-2"></div>

<?=$htmlAry['hiddenInputField']?>

</form>