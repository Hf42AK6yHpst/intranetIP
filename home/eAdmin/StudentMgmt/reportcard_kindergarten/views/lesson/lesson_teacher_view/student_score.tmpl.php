<script type="text/javascript">

$(document).ready( function() {
	$("li.star-obj").on("click",function() {
		// Get parent and adjacent
		var li_parent = this.parentNode;
		var li_parent_id = li_parent.id;
		var li_all_adjacent = li_parent.children;

		// loop adjacent
		var this_score = 0;
		var is_enabled = false;
		for (var adj_count=0; adj_count<li_all_adjacent.length; adj_count++) {
			// clear star display
			var this_adjacent = li_all_adjacent[adj_count];
			this_adjacent.className = 'star-obj';

			// get teaching tools score
			if(this == this_adjacent) {
				this_score = 5 - adj_count;
                is_enabled = true;
			}

			if(is_enabled) {
                $(this_adjacent).find('div.star-w').hide();
                $(this_adjacent).find('div.star-y').show();
            } else {
                $(this_adjacent).find('div.star-w').show();
                $(this_adjacent).find('div.star-y').hide();
            }
		}

		// update student tool score
		this.className = 'star-obj selected';
		$('input#'+li_parent_id+'_val').val(this_score);
	});
});

function updateStudentScore() {
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_teacher_view.student_score_update';
	$('form#form1').submit();
}

function backToZone() {
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_teacher_view.zone';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="index.php?task=lesson.lesson_teacher_view.student_score_update">

<div class="col-md-1 col-lg-2"></div>
<div class="center-area col-sm-12 col-md-10 col-lg-8">
	
	<div class="content-area">
		<div class="title-area">
			<?=$htmlAry['TitleInfo']?>
		</div>
		
		<div class="marking-area">
			<ul class="photo-area teacher marking">
				<?=$htmlAry['studentDisplay']?>
			</ul><ul class="tool-area"><!-- NO SPACE PLEASE --><?=$htmlAry['studentScoreDisplay']?>
			</ul>
		</div>
		
		<div class="btn-area">
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
		</div>
	</div>
	
</div>
<div class="col-md-1 col-lg-2"></div>

<?=$htmlAry['hiddenInputField']?>

</form>