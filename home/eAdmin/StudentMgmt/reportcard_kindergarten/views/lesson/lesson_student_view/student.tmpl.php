<script type="text/javascript">
var isSelected = 0;
function selectStudent(sid,zid) {
	if(isSelected != 1 && sid != '' && zid != '') {
		isSelected = 1;
		
		var data = {
			Type: 'checkInAnotherZone',
			ZoneID: zid,
			StudentID: sid
		};
    	$.ajax({
    		method: 'post',
    		url: 'index.php?task=lesson.lesson_student_view.get_ajax',
    		data: data,
    		success: function(zoneName) {
    			if(zoneName != '') {
    				alert("<?php echo $Lang['eReportCardKG']['Management']['Device']['PleaseLeaveFirst1']?>" + zoneName +  "<?php echo $Lang['eReportCardKG']['Management']['Device']['PleaseLeaveFirst2']?>");
    			} else {
    				enterZone(sid);
    			}
    		}
    	});

       	 setTimeout(function() {
       		isSelected = 0;
       	 }, 800);
	}
}

function enterZone(sid)
{
	$('input#student_input').val(sid);
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_student_view.enter';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="index.php?task=lesson.lesson_student_view.enter">
    <nav class="row navbar navbar-default navbar-fixed-bottom" style="background: #a5e3f8;">
    	<div class="col-md-1 col-lg-2"></div>
    	<div class="center-area col-sm-12 col-md-10 col-lg-8">
    		<div class="content-area">
        		<div class="btn-area">
        			<!--<?=$htmlAry['backBtn']?>-->
        		</div>
    		</div>
    	</div>
    	<div class="col-md-1 col-lg-2"></div>
    </nav>
    
    <div class="row" style="padding-bottom: 150px;">
    	<div class="col-md-1 col-lg-2"></div>
    	<div class="center-area col-sm-12 col-md-10 col-lg-8">
    		<div class="content-area">
    			<div class="title-area">
    				<?=$htmlAry['TitleInfo']?>
    			</div>
    			<ul class="photo-area">
    				<?=$htmlAry['studentDisplay']?>
    			</ul>
    		</div>
    	</div>
    	<div class="col-md-1 col-lg-2"></div>
    </div>
    
    <?=$htmlAry['hiddenInputField']?>
</form>