<script type="text/javascript">

function selectTools() {
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_student_view.student_tools';
	$('form#form1').submit();
}

function updateEnterStatus() {
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_student_view.student_tools_update';
	$('form#form1').submit();
}

function backToStudentList() {
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_student_view.student';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="index.php?task=lesson.lesson_student_view.student_tools">

<div class="col-md-1 col-lg-2"></div>
<div class="center-area col-sm-12 col-md-10 col-lg-8">
	
	<div class="content-area">
		<div class="title-area">
			<?=$htmlAry['TitleInfo']?>
		</div>
		
		<ul class="photo-area single">
			<?=$htmlAry['studentDisplay']?>
		</ul>
		
		<div class="btn-area">
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['exitBtn']?>
			<?=$htmlAry['changeBtn']?>
		</div>
	</div>
	
</div>
<div class="col-md-1 col-lg-2"></div>

<?=$htmlAry['hiddenInputField']?>

</form>