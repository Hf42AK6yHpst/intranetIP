<script type="text/javascript">

$(document).ready( function() {
	$(".check-btn").on("mouseup", function(e) {
		// toogle clicked teaching tools
		if($(this).hasClass("checked")) {
			$(this).removeClass("checked");
		}
		else {
			$(this).addClass("checked");
			handleToolsCheckbox(this.id);
		}
		
		// update teaching tools
		var selectedToolIdAry = [];
		$('.checked').each(function() {
			var thisToolId = this.id;
			thisToolId = thisToolId.replace("teaching_tool_", "");
			selectedToolIdAry.push(thisToolId);
		});
		var selectedToolId = selectedToolIdAry.join();
		$('input#tool_input').val(selectedToolId);
	});
	
	// reset hidden input field when back
	$('input#tool_input').val("");
	$('input#submit_flag').val("");
});

function handleToolsCheckbox(targetId)
{
	// INCOMPLETE > Unset all selected tools
    if(targetId == 'teaching_tool_0')
    {
    	$('.checked').each(function() {
    		if(this.id != 'teaching_tool_0') {
    			$(this).removeClass("checked");
    		}
    	});
    }
    // Normal > Unset incomplete
    else
    {
    	if($('#teaching_tool_0') && $('#teaching_tool_0').hasClass("checked")) {
    		$('#teaching_tool_0').removeClass("checked");
    	}
    }
}

function updateTools() {
	if($('input#tool_input').val()=="") {
		alert("<?=$Lang['eReportCardKG']['Management']['Device']['PleaseSelectTool']?>")
		return false;
	}
		
	$('input#submit_flag').val(1);
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_student_view.student_tools_update';
	$('form#form1').submit();
}

function formSubmit() {	
	if($('input#submit_flag').val() == 1) {
		return true;
	}
	else {
		return false;
	}
}
</script>

<form name="form1" id="form1" method="POST" onsubmit="return formSubmit()">

<div class="container" role="main">
	<div class="col-md-1 col-lg-2"></div>
	<div class="center-area col-sm-12 col-md-10 col-lg-8">
		
		<div class="content-area">
			<div class="title-area">
				<?=$htmlAry['TitleInfo']?>
			</div>
			
			<ul class="photo-area single">
				<?=$htmlAry['teachingToolDisplay']?>
			</ul>
			
			<div class="btn-area">
				<?=$htmlAry['confirmBtn']?>
			</div>
		</div>
		
	</div>
	<div class="col-md-1 col-lg-2"></div>

<?=$htmlAry['hiddenInputField']?>

</form>