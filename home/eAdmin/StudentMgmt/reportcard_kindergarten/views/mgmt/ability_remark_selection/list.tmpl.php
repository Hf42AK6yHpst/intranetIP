<style type="text/css">
.remarkSelectionTable, .remarkSelectionTable td, remarkSelectionTable.th{
/*     text-align: center; */
/*     line-height: 2; */
}
</style>

<script type="text/javascript">
$(document).ready(function(){
    if($('#YearID').val()=='') {
        $('#YearID option:nth-child(2)').attr('selected', true);
        document.form1.submit();
    }
    if($('#TermID').val()=='') {
        $('#TermID option:nth-child(2)').attr('selected',true);
        document.form1.submit();
    }
    $('#NeedConfirmMsg').show();
});
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div style="display:none;" id="NeedConfirmMsg">
			<?=$WarningBox?>
		</div>
		<div class="content_top_tool">
			<br style="clear:both;">
			<?=$htmlAry['typeSelection']?>
			<br style="clear:both;">
		</div>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>
<br style="clear:both;">
<p class="spacer"></p>
		
<form name="form2" id="form2" method="POST" action="?task=mgmt.ability_remark_selection.update">
	<?=$htmlAry['dataTable']?>
	<?=$htmlAry['form2Hidden']?>
	<br/>
	
	<?=$htmlAry['lastModifiedDiv']?>
	<div style="text-align:center">
		<?=$htmlAry['form2Submit']?>
	</div>
</form>