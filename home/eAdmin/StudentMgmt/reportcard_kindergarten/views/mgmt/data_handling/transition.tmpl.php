<script type="text/javascript">
function toggleDisplay(type) {
	if(type) {
		$("#WarningMsgRow").hide();
		$("#WarningBtnRow").hide();
		
		$("#FormContentRow1").show();
		$("#FormContentRow2").show();
		$("#FormContentRow3").show();
		$("#FormBtnRow").show();
	}
	else {
		$("#FormContentRow1").hide();
		$("#FormContentRow2").hide();
		$("#FormContentRow3").hide();
		$("#FormBtnRow").hide();
		
		$("#WarningMsgRow").show();
		$("#WarningBtnRow").show();
	}
}

function toggleAction(type) {
	var otherYearSelect = document.getElementById("OtherYear");
	if (type == "other") {
		otherYearSelect.disabled = false;
	}
	else {
		otherYearSelect.disabled = true;
	}
}

function confirmSubmit() {
	var canSubmit = false;
	if ($('input#transitionAction1').attr('checked')) {
		if(confirm("<?=$htmlAry["YearWarning"]?>")) {
			canSubmit = true;
		}
	}
	else if ($('input#transitionAction2').attr('checked')) {
		if(confirm("<?=$Lang['eReportCardKG']['Management']['DataTransition']['ConfirmTransition']?>")) {
			canSubmit = true;
		}
	}
	if(canSubmit) {
		$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>data_handling<?=$ercKindergartenConfig['taskSeparator']?>transition_update';
		$('form#form1').submit();
	}
}
</script>

<form name="form1" id="form1" method="POST" action="transition_update.php" onsubmit="return confirmSubmit()">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td>
		<?=$htmlAry["TransitionTable"]?>
	</td></tr>
	</table>
</form>