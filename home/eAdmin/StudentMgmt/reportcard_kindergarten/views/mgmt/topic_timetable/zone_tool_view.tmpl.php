<script type="text/javascript">
var toolData = []; 			// saving tool data
var zoneQuotaArr = [];		// saving zone quota
var myJSON = '';

$(document).ready(function(){
	// Teaching Tools
	<?php foreach((array)$Data as $ZoneID => $_Data){?>
		var ToolIDArr = []; // store checked id
		<?php foreach((array)$_Data['LearningTool'] as $_ToolID){?>
			ToolIDArr.push('<?=$_ToolID?>');
		<?php } ?>
		toolData['<?=$ZoneID?>'] = ToolIDArr;
	<?php } ?>
	
	var key;
	var toolDataObj = {};
	for(key in toolData){
		if(key!='in_array'){
			toolDataObj[key] = toolData[key];
		}
	}
	myJSON = JSON.stringify(toolDataObj);
	$('#toolData').val(myJSON);
	
	// Zone Quota
	<?php foreach((array)$zone_quota as $ZoneID => $ToolZoneQuota) { ?>
		zoneQuotaArr['<?=$ZoneID?>'] = [];
		zoneQuotaArr['<?=$ZoneID?>'][0] = '<?=$ToolZoneQuota?>';
		<?php foreach((array)$class_zone_quota[$ZoneID] as $thisClassID => $ClassZoneQuota) { ?>
    		zoneQuotaArr['<?=$ZoneID?>'][<?=$thisClassID?>] = '<?=$ClassZoneQuota?>';
    	<?php } ?>
	<?php } ?>
	
	var key;
	var zoneQuotaObj = {};
	for(key in zoneQuotaArr)
	{
		if(key != 'in_array') {
			zoneQuotaObj[key] = {};
			for(key2 in zoneQuotaArr[key])
			{
				if(key2 != 'in_array') {
					zoneQuotaObj[key][key2] = zoneQuotaArr[key][key2];
				}
			}
		}
	}
	myJSON = JSON.stringify(zoneQuotaObj);
	$('#zoneQuotaData').val(myJSON);
});

function GoEdit(ZoneID)
{
	load_dyn_size_thickbox_ip('<?=$Lang['eReportCardKG']['Setting']['TeachingTool']['Title'] ?>', 'onloadThickBox('+ZoneID+');');
}

function onloadThickBox(ZoneID)
{
	var YearID = $('#YearID').val();
	var toolDataID = $('#toolData').val();
	var zoneQuotaData = $('#zoneQuotaData').val();
	
	$.ajax({
		type: "POST",
		url: "index.php?task=mgmt.topic_timetable.ajax_get_learning_tool", 
		data:{ 
			"ZoneID": ZoneID,
			"YearID": '<?=$YearID?>',
			"toolData" : toolDataID,
			"zoneQuotaData" : zoneQuotaData
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}

function onSelectCatInThickBox(ZoneID, ToolCatID)
{
	var YearID = $('#YearID').val();
	var toolDataID = $('#toolData').val();
	var zoneQuotaData = $('#zoneQuotaData').val();
	
	$.ajax({
		type: "POST",
		url: "index.php?task=mgmt.topic_timetable.ajax_get_learning_tool", 
		data:{ 
			"ToolCatID": ToolCatID,
			"ZoneID": ZoneID,
			"YearID": '<?=$YearID?>',
			"toolData" : toolDataID,
			"zoneQuotaData" : zoneQuotaData
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}

function SaveTools()
{
	var timeTableID = '<?=$timetableID?>';
	var TopicID = '<?=$TopicID?>';
	
	$('#form_learningTool').attr('action', 'index.php?task=mgmt.topic_timetable.zone_tool_view_update&TimeTableID='+timeTableID+'&TopicID='+TopicID);
	$('#form_learningTool').submit();
}

function GoDelete(LearningToolID, ZoneID)
{
	$('#form1').attr('action', 'index.php?task=mgmt.topic_timetable.zone_tool_view_update&isDelete=1&LearningToolID='+LearningToolID+'&ZoneID='+ZoneID);
	$('#form1').submit();
}

function CloseThickBox()
{
	$('#TB_window').fadeOut();
	tb_remove();
}
</script>

<?=$NivagationBar?>
<br> 
<br>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$table?>
	</div>
	<input type="hidden" id="toolData" name="toolData">
	<input type="hidden" id="zoneQuotaData" name="zoneQuotaData">
	<input type="hidden" id="TimeTableID" name="TimeTableID" value="<?=$timetableID?>" >
</form>