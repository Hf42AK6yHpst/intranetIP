<script type="text/javascript">
$(document).ready( function() {
	reloadOtherInfoTableContent();
	$('#Content_tab a').click(function(e) {
		e.preventDefault();
		if(this.attributes != undefined && this.attributes[0] != undefined) {
			if(this.attributes[0].value != 'null') {
				setUploadType(this.attributes[0].value);
			}
			// for IE usage
			else if(this.nameProp != undefined && this.nameProp != 'null') {
				setUploadType(this.nameProp);
			}
		}
		return false;
	});
});

function reloadOtherInfoTableContent()
{
	var UploadType = $("input#UploadType").val();
	var YearID = $("select#YearID").val();
	var TermID = $("select#TermID").val();
	
	Block_Element("other_info_div");
	
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.other_info.ajax_task",
		data:
		{
			Task : "Reload_Other_Info_Table",
			UploadType : UploadType,
			YearID : YearID,
			TermID : TermID
		},
		success: function(returnData)
		{
			$("div#other_info_div").html(returnData);
			UnBlock_Element("other_info_div");
		}
	});
}

function goEditOtherInfo(e, ClassID)
{
	if(e.preventDefault) {
		e.preventDefault();
	} else {
		e.returnValue = false;
	}
	$('input#ClassID').val(ClassID);
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>edit';
	$('form#form1').submit();
}

function goImport()
{
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>import';
	$('form#form1').submit();
}

function goExport()
{
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>export';
	$('form#form1').submit();
}

function setUploadType(type)
{
	$("input#UploadType").val(type);
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>index';
 	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry['YearSelection']?>
			<?=$htmlAry['TermSelection']?>
		</div>
		<br style="clear:both;">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div id='other_info_div'></div>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry['hiddenInputField']?>
</form>