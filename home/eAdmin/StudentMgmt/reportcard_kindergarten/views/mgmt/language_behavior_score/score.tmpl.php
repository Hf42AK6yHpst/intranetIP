<style type="text/css">
.score_table_outter { width: 1800px; overflow-x: scroll; }
/* .score_table { table-layout: fixed; } */
.score_table th, .score_table td { white-space: nowrap; min-width: 100px; }
.score_table .static_display_col {
    position:absolute; 
    left:auto;
    top:auto;
    padding: 0;
    vertical-align: middle;
}
</style>

<script type="text/javascript">
// for fixed first column
$(document).ready(handleFirstColDisplay);
$(window).resize(handleFirstColDisplay);

function handleFirstColDisplay() {
	// get display area width
	var window_width = $(window).width();
	var left_menu_width = $('#leftmenu').width();
	var left_menu_left = parseInt($('#leftmenu').css("left"));
	if (left_menu_left < 0) {
		left_menu_width = left_menu_width + left_menu_left;
	}
	window_width = (window_width - left_menu_width) * 0.99;
	window_width = window_width + 'px';
	$('div.score_table_outter').width(window_width);
	
	$('table.score_table tr').each(function(index) {
		if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
			var col_height = $(this).find('td.static_based_col')[0].clientHeight - 1;
			var col_width = $(this).find('td.static_based_col').width();
		}
		else {
    		var col_height = $(this).find('td.static_based_col')[0].getBoundingClientRect().height - 1;
    		var col_width = $(this).find('td.static_based_col').width();
		}
		
		$(this).find('.static_display_col').height(col_height).width(col_width).css('line-height', col_height+'px');
	});
	$('#NeedConfirmMsg').show();
}

function goBack() {
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior_score<?=$ercKindergartenConfig["taskSeparator"]?>list';
	$('form#form1').submit();
}

function goExport() {
    $('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior_score<?=$ercKindergartenConfig["taskSeparator"]?>export';
    $('form#form1').submit();
}

function formSubmit() {
	if(formChecking()) {
		$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior_score<?=$ercKindergartenConfig["taskSeparator"]?>score_update';
		$('form#form1').submit();
	}
}

function formChecking() {
	// Full Mark
	var jFullMark = document.getElementById("FullMark").value;
	
	// loop Input Fields
	var inputList = $("input[name^='mark']");
	for(var i=0; i<inputList.length; i++)
	{
		var thisObj = inputList[i];
		var thisObjValid = thisObj.disabled != true;
		var thisObjScore = Trim(thisObj.value);
		
		// Score Checking
		if(thisObj && thisObjValid && thisObjScore != "") {
			if(thisObjScore < 0) {
				alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["InputMarkCannotBeNegative"]?>");
				thisObj.focus();
				return false;
			}
			if(!IsNumeric(thisObjScore)) {
				alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["InputMarkInvalid"]?>");
				thisObj.focus();
				return false;
			}
			if(parseFloat(thisObjScore) > parseFloat(jFullMark)) {
				alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["InputMarkCannotBeLargerThanFullMark"]?>");
				thisObj.focus();
				return false;
			}
		}
	}
	return true;
}
</script>

<?=$htmlAry["Navigation"]?>

<div class="content_top_tool">
    <br style="clear:both;">
    <?=$htmlAry['contentTool']?>
</div>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<p class="spacer"></p>
		<div style="display:none;" id="NeedConfirmMsg">
			<?=$WarningBox?>
		</div>
      	<table class="form_table_v30">
      		<tr>
				<td class="field_title"><?=$Lang["General"]["Term"]?></td>
				<td><?=$htmlAry["termNameDisplay"]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang["eReportCardKG"]["Management"]["LanguageBehavior"]["Title"]?></td>
				<td><?=$htmlAry["topicCatNameDisplay"]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang["General"]["Class"]?></td>
				<td><?=$htmlAry["classNameDisplay"]?></td>
			</tr>
      	</table>
      	<br style="clear:both;" />
		
		<?=$htmlAry["scoreTableContent"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["Button"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>