<form id="form1" name="form1" action="?task=mgmt.view_edit_period_setting.edit_update" method="post">
	<?php echo $htmlAry['Navigation']?>
	<br/>
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<div class="form_table">
		<table class="form_table_v30">
			<?php echo $htmlAry['formBody']?>
		</table>
	</div>
	<?php echo $htmlAry['hiddenField']?>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?php echo $htmlAry['btn_submit']?>
		<?php echo $htmlAry['btn_back']?>
		<p class="spacer"></p>
	</div>
</form>

<script type="text/javascript">
// 	$(document).ready(function(){
// 		$('#inputStart_date').datepick();
// 		$('#inputEnd_date').datepick();
// 		$('#viewStart_date').datepick();
// 		$('#viewEnd_date').datepick();
// 	});
	$('#NeedConfirmMsg').show();
	$('#form1').submit(function(){
		/*let inputStartTime = $('#inputStart_date').val() + ' ' + $('#inputStart_hour').val() + ':' + $('#inputStart_minute').val() + ':' + $('#inputStart_second').val();
		let inputStartDate = new Date(inputStartTime);
		
		let inputEndTime = $('#inputEnd_date').val() + ' ' + $('#inputEnd_hour').val() + ':' + $('#inputEnd_minute').val() + ':' + $('#inputEnd_second').val();
		let inputEndDate = new Date(inputEndTime);

		let viewStartTime = $('#viewStart_date').val() + ' ' +$('#viewStart_hour').val() + ':' + $('#viewStart_minute').val() + ':' + $('#viewStart_second').val();
		let viewStartDate = new Date(viewStartTime);

		let viewEndTime = $('#viewEnd_date').val() + ' ' + $('#viewEnd_hour').val() + ':' + $('#viewEnd_minute').val() + ':' + $('#viewEnd_second').val();
		let viewEndDate = new Date(viewEndTime);
		
		let inputCheck = inputStartDate <= inputEndDate;
		let viewCheck = viewStartDate <= viewEndDate;
		//compareTime(date_s, hr_s, min_s, date_e, hr_e, min_e, sec_s, sec_e)
		let inputCheck = compareTime(
				$('#inputStart_date')[0], 
				$('#inputStart_hour')[0], 
				$('#inputStart_minute')[0], 
				$('#inputEnd_date')[0], 
				$('#inputEnd_hour')[0], 
				$('#inputEnd_minute')[0], 
				$('#inputStart_second')[0], 
				$('#inputEnd_second')[0]
		);
		let viewCheck = compareTime(
				$('#viewStart_date')[0], 
				$('#viewStart_hour')[0], 
				$('#viewStart_minute')[0], 
				$('#viewEnd_date')[0], 
				$('#viewEnd_hour')[0], 
				$('#viewEnd_minute')[0], 
				$('#viewStart_second')[0], 
				$('#viewEnd_second')[0]
		);
		*/

		let inputCheck = compareTimeByObjId('inputStart', 'inputStart_hour', 'inputStart_min', 'inputStart_sec', 'inputEnd', 'inputEnd_hour', 'inputEnd_min', 'inputEnd_sec');
		let viewCheck = compareTimeByObjId('viewStart', 'viewStart_hour', 'viewStart_min', 'viewStart_sec', 'viewEnd', 'viewEnd_hour', 'viewEnd_min', 'viewEnd_sec');
		if(inputCheck && viewCheck) {
			return true;
		}
		else {
			if(!inputCheck) {
				$('#input_Warn').show();
			}
			if(!viewCheck) {
				$('#view_Warn').show();
			}
		 	return false;
		}
	});
</script>