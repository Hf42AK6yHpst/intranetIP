<script type="text/javascript">
function js_Reload_Student_Selection()
{
	var classIdList = $("select#ClassID").val();
	if(classIdList == "") {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$.post(
        "index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>export_performance<?=$ercKindergartenConfig["taskSeparator"]?>get_ajax",
		{
			Type: 'StudentByClass',
			SelectionID: 'StudentID',
			SelectionName: 'StudentID[]',
			ClassID: classIdList,
			noFirst: 1,
			isMultiple: 1,
			isAll: 1,
			withSelectAll: 1
		},
		function(ReturnData)
		{
			$("div#studentSelDiv").html(ReturnData);
			js_Select_All('StudentID', 1);
		}
	)
}

function goSubmit()
{
	var termIdList = $('select#TermID').val();
	var classIdList = $('select#ClassID').val();
	var stuIdList = $('select#StudentID').val();
	$('div.warnMsgDiv').hide();
	
	var canSubmit = true;
	if(termIdList == null || termIdList == '') {
		canSubmit = false;
		$('div#Term_Warn').show();
	}
	if(classIdList == null || classIdList == '') {
		canSubmit = false;
		$('div#Class_Warn').show();
	}
	if(stuIdList == null || stuIdList == '') {
		canSubmit = false;
		$('div#Student_Warn').show();
	}
	
	if(canSubmit) {
		$('form#form1').submit();
	}
}
</script>

<form id="form1" method="POST" target="_blank" action="index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>export_performance<?=$ercKindergartenConfig["taskSeparator"]?>export">
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><?=$Lang["General"]["Term"]?></td>
			<td>
				<?=$htmlAry["SemesterSelection"]?>
				<div style="display:none;" class="warnMsgDiv" id="Term_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Term"]?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang["General"]["Class"]?></td>
			<td>
				<?=$htmlAry["ClassSelection"]?>
				<div style="display:none;" class="warnMsgDiv" id="Class_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Class"]?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang["Identity"]["Student"]?></td>
			<td>
				<div id="studentSelDiv"></div>
				<div style="display:none;" class="warnMsgDiv" id="Student_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["Identity"]["Student"]?></span>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</form>

<div class="edit_bottom_v30">
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
</div>