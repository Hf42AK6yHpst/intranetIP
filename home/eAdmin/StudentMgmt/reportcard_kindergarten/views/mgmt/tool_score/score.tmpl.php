<script type="text/javascript">
var studentIdAry = [];

$(document).ready(function() {
	$('input[name="StudentID[]"]').each( function() {
		studentIdAry.push(this.value);
	});
	$('#NeedConfirmMsg').show();
});

function goEdit(ClassID, ZoneID) {
	$('form#form1')[0].action = 'index?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>tool_score<?=$ercKindergartenConfig["taskSeparator"]?>score';
	$('form#form1').submit();
}

function goBack(target) {
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>tool_score<?=$ercKindergartenConfig["taskSeparator"]?>zone';
	$('form#form1').submit();
}

function addInputScore(targetStudentID, rowSize)
{
	if(targetStudentID <= 0 || rowSize <= 0) {
		return false;
	}
	var studentRowPrefix = "Tr_score_" + targetStudentID + "_";
	
	// Get Student existing rows
	var studentScoreRows = $("tr[id^='" + studentRowPrefix + "']");
	var studentScoreRowCount = 0;
	if(studentScoreRows) {
		studentScoreRowCount = studentScoreRows.length;
	}
	
	// Build new row content
	var addedScoreRow = "";
	var newRowCount = 0;
	var currentRowNum = studentScoreRowCount;
	for(var i = 0; i < rowSize; i++)
	{
		// for student without any Input Score
		if(studentScoreRowCount == 2 && i == 0)
		{
			// Update first row display
			var emptyRowScoreCol = $("tr#" + studentRowPrefix + "row1 td.row1_score_col'")[0];
			var emptyRowActionCol = $("tr#" + studentRowPrefix + "row1 td.row1_delete_col'")[0];
			if(emptyRowScoreCol && emptyRowActionCol && emptyRowScoreCol.innerHTML == "<?=$Lang["eReportCardKG"]["General"]["NoRecord"]?>")
			{
				emptyRowScoreCol.innerHTML = "<input type='text' class='textbox' name='InputScore[" + targetStudentID + "][row1]' id='mark[temp0][0]' value=''>";
				emptyRowActionCol.innerHTML = "<span class='table_row_tool row_content_tool'><a onclick='deleteInputScore(this, \"" + targetStudentID + "\", \"row1\")' title='delete' class='delete' href='javascript:void(0);'></a></span>";
				
				//newRowCount++;
				continue;
			}
		}
		
		// for new Input Score rows
		currentRowNum = studentScoreRowCount + newRowCount;
		addedScoreRow += "<tr id='" + studentRowPrefix + "row" + currentRowNum + "' class='dataRow'>";
			addedScoreRow += "<td>";
				addedScoreRow += "<input type='text' class='textbox' name='InputScore[" + targetStudentID + "][row" + currentRowNum + "]' id='mark[temp" + newRowCount + "][0]' value=''>";
			addedScoreRow += "</td>";
			addedScoreRow += "<td>";
				addedScoreRow += "<span class='table_row_tool row_content_tool'>";
				addedScoreRow += "<a onclick='deleteInputScore(this, \"" + targetStudentID + "\", \"row" + currentRowNum + "\")' title='delete' class='delete' href='javascript:void(0);'></a>";
			addedScoreRow += "</span>";
		addedScoreRow += "</td>";
		
		newRowCount++;
	}
	
	// Update rowspan for Student Columns
	currentRowNum = studentScoreRowCount + newRowCount;
	$("tr[id^='" + studentRowPrefix + "'] td.student_col").each( function() {
		$(this).attr('rowspan', currentRowNum);
	});
	
	// Append new row Content
	if(addedScoreRow != "") {
		$("tr#" + studentRowPrefix + "Overall").before(addedScoreRow);
	}
	
	if(rowSize == 1)
	{
		// Update all input fields > for copy and paste
		rebuildInputScore();
		
		// Refresh all input fields > for press "Enter"
		$.getScript("<?=$indexVar["thisBasePath"]?>templates/2009a/js/copypaste_2016.js", function() {
			// do nothing
		});
	}
}

function deleteInputScore(obj, targetStudentID, targetScoreID)
{
	if(targetStudentID <= 0 || targetScoreID == "") {
		return false;
	}
	var targetStudentInput = "tr#Tr_score_" + targetStudentID + "_" + targetScoreID + " input";
	
	// Check target row type
	var isTempAddedRow = targetScoreID.indexOf("row");
	isTempAddedRow = isTempAddedRow != -1;
	
	// JS Handle only for temp added row
	if(isTempAddedRow)
	{
		// Display delete message
		obj.parentElement.innerHTML = '<span class="tabletextrequire"><?=$i_con_gen_msg_delete?></span>';
		
		// Disable input field
		var inputScoreDisplay = $(targetStudentInput)[0];
		if(inputScoreDisplay) {
			inputScoreDisplay.disabled = true;
		}
		
		// Update all input fields > for copy and paste
		rebuildInputScore();
	}
	// AJAX Handle for existing score in DB
	else
	{
		$.ajax({
			type: "POST",
			url: "index.php?task=mgmt.tool_score.ajax_score", 
			data:{ 
				"targetScoreID": targetScoreID
			},
			success: function(result){
				if(result)
				{
					// Display delete message
					obj.parentElement.innerHTML = '<span class="tabletextrequire"><?=$i_con_gen_msg_delete?></span>';
					
					// Disable input field
					var inputScoreDisplay = $(targetStudentInput)[0];
					inputScoreDisplay.disabled = true;
					
					// Update all input fields > for copy and paste
					rebuildInputScore();
				}
			}
		});
	}
}

function rebuildInputScore()
{
	var inputScoreNum = 0;
	$("input[name^='InputScore[']").each( function() {
		// Disabled Input Fields > Clear ID
		if($(this)[0].disabled) {
			$(this)[0].id = '';
		}
		// Reorder Input Fields ID
		else {
			$(this)[0].id = "mark[" + inputScoreNum + "][0]";
			inputScoreNum++;
		}
	});
	
	if(inputScoreNum == 0) {
		$('#submitBtn').hide();
	}
	else {
		$('#submitBtn').show();
	}
}

function addMultipleRows()
{
	// Checking values
	var add_count = parseInt($('#new_row_num').val());
	if(add_count > 20) {
		alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["MaxAddNewInputScoreRow"]?>");
		this.value = 20;
		return false;
	}
	else if (this.value <= 0) {
		alert("<?=$Lang["General"]["JS_warning"]["InputPositiveValue"]?>");
		this.value = 1;
		return false;
	}
	
	// loop students > Add new row Content
	var student_count = studentIdAry.length;
	if(student_count > 0 && add_count > 0)
	{
		for(var i = 0; i < student_count; i++) {
			var thisStudentId = studentIdAry[i];
			if(thisStudentId > 0) {
				addInputScore(thisStudentId, add_count)
			}
		}
	}
	
	// Update all input fields > for copy and paste
	rebuildInputScore();
	
	// Refresh all input fields > for press "Enter"
	$.getScript("<?=$indexVar["thisBasePath"]?>templates/2009a/js/copypaste_2016.js", function() {
		// do nothing
	});
}

function formSubmit()
{
	if(formChecking())
	{
		$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>tool_score<?=$ercKindergartenConfig["taskSeparator"]?>score_update';
		$('form#form1').submit();
	}
}

function formChecking()
{
	// Full Mark
	var jFullMark = document.getElementById("FullMark").value;
	
	// loop Input Fields
	var inputList = $("input[name^='InputScore']");
	for(var i=0; i<inputList.length; i++)
	{
		var thisObj = inputList[i];
		var thisObjValid = thisObj.disabled != true;
		var thisObjScore = Trim(thisObj.value);
		
		// Input Checking
		if(thisObj && thisObjValid)
		{
			if(thisObjScore < 0) {
				alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["InputMarkCannotBeNegative"]?>");
				thisObj.focus();
				return false;
			}
			if(!IsNumeric(thisObjScore) || thisObjScore == "") {
				alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["InputMarkInvalid"]?>");
				thisObj.focus();
				return false;
			}
			if(parseFloat(thisObjScore) > parseFloat(jFullMark)) {
				alert("<?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["InputMarkCannotBeLargerThanFullMark"]?>");
				thisObj.focus();
				return false;
			}
		}
	}
	return true;
}
</script>

<?=$htmlAry["navigation"]?>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		<div style="display:none;" id="NeedConfirmMsg">
			<?=$WarningBox?>
		</div>
      	<table class="form_table_v30">
      		<tr>
				<td class="field_title"><?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["TimeTable"]?></td>
				<td><?=$htmlAry["timeTableNameDisplay"]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["Title"]?></td>
				<td><?=$htmlAry["topicNameDisplay"]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang["eReportCardKG"]["Management"]["Device"]["LearningZone"]?></td>
				<td><?=$htmlAry["zoneNameDisplay"]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["TeachingTools"]?></td>
				<td><?=$htmlAry["toolNameDisplay"]?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang["eReportCardKG"]["Management"]["ToolScore"]["ClassName"]?></td>
				<td><?=$htmlAry["classTeacherNameDisplay"]?></td>
			</tr>
      	</table>
      	<br style="clear:both;" />
      	
		<?=$htmlAry["addTableRowInput"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<?=$htmlAry["timeTableContent"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["button"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>