<script type="text/javascript">
$( document ).ready(function() {
	$('input#ToolID').val('');
});

function goEdit(ToolID) {
	$('input#ToolID').val(ToolID);
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>tool_score<?=$ercKindergartenConfig['taskSeparator']?>score';
	$('form#form1').submit();
}

function goBack() {
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>tool_score<?=$ercKindergartenConfig['taskSeparator']?>list';
	$('form#form1').submit();
}
</script>
<?=$htmlAry['Navigation']?>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		
      	<table class="form_table_v30">
      		<tr>
				<td class="field_title"><?=$Lang['eReportCardKG']['Management']['ToolScore']['TimeTable']?></td>
				<td><?=$thisTimeTableName?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['eReportCardKG']['Management']['ToolScore']['Title']?></td>
				<td><?=$thisTopicName?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['eReportCardKG']['Management']['Device']['LearningZone']?></td>
				<td><?=$thisZoneName?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['eReportCardKG']['Management']['ToolScore']['ClassName']?></td>
				<td><?=$thisTeachingClassName?></td>
			</tr>
      	</table>
      	<br style="clear:both;" />
		
		<?=$htmlAry['ToolTableContent']?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry['hiddenInputField']?>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry['Button']?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>