<script type="text/javascript">
$(document).ready(function() {
	$('input#ClassID').val('');
	$('input#SubjectID').val('');
	$('input#CodeID').val('');
});

function goEdit(ClassID, SubjectID, CodeID) {
	$('input#ClassID').val(ClassID);
	$('input#SubjectID').val(SubjectID);
	$('input#CodeID').val(CodeID);
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>subject_topic_score<?=$ercKindergartenConfig["taskSeparator"]?>score';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry["yearSelection"]?>
			<?=$htmlAry["subjectSelection"]?>
			<?=$htmlAry["termSelection"]?>
			<?=$htmlAry["timeTableSelection"]?>
		</div>
		<br style="clear:both;">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry["dbTableContent"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>