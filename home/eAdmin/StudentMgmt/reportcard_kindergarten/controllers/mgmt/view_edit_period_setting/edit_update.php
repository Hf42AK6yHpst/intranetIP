<?php
// editing by
/*
 *  Date: 2019-02-14 Philips
 *          - Create file
 */

### Cookies Setting
// $arrCookies = array();
// if(isset($clearCoo) && $clearCoo == 1) {
//     clearCookies($arrCookies);
// }
// else {
//     updateGetCookies($arrCookies);
// }

function getDateTimeFromPOST($str, $post){
    return array(
        "Date" => $post[$str],
        "Hour" => ($post[$str.'_hour'] < 10? '0'.$post[$str.'_hour'] : $post[$str.'_hour']),
        "Minute" => ($post[$str.'_min'] < 10? '0'.$post[$str.'_min'] : $post[$str.'_min']),
        "Second" => ($post[$str.'_sec'] < 10? '0'.$post[$str.'_sec'] : $post[$str.'_sec'])
    );
}

function getDateTimeString($dtAry){
    return $dtAry['Date'].' '.$dtAry['Hour'].':'.$dtAry['Minute'].':'.$dtAry['Second'];
}

$YearTermID = $_POST['YearTermID'];

$inputStart = getDateTimeFromPOST('inputStart', $_POST);
$inputStart = getDateTimeString($inputStart);

$inputEnd = getDateTimeFromPOST('inputEnd', $_POST);
$inputEnd = getDateTimeString($inputEnd);

$viewStart = getDateTimeFromPOST('viewStart', $_POST);
$viewStart = getDateTimeString($viewStart);

$viewEnd = getDateTimeFromPOST('viewEnd', $_POST);
$viewEnd = getDateTimeString($viewEnd);

if(!$indexVar['libreportcard']->find_INPUT_SCORE_PERIOD_SETTING($YearTermID)) {
    $inputResult = $indexVar['libreportcard']->insert_INPUT_SCORE_PERIOD_SETTING($YearTermID, $inputStart, $inputEnd);
}
else {
    $inputResult = $indexVar['libreportcard']->edit_INPUT_SCORE_PERIOD_SETTING($YearTermID, $inputStart, $inputEnd);
}

if(!$indexVar['libreportcard']->find_VIEW_REPORT_PERIOD_SETTING($YearTermID)) {
    $inputResult = $indexVar['libreportcard']->insert_VIEW_REPORT_PERIOD_SETTING($YearTermID, $viewStart, $viewEnd);
}
else {
    $viewResult = $indexVar['libreportcard']->edit_VIEW_REPORT_PERIOD_SETTING($YearTermID, $viewStart, $viewEnd);
}

//debug_pr($inputResult);
//debug_pr($viewResult);
//die();
header('location: ?task=mgmt.view_edit_period_setting.list');

?>