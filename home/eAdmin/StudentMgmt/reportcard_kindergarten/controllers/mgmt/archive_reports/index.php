<?
// Using :
/*
 * 	Date: 2019-10-11 (Bill)
 * 			Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["PrintArchiveReport"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# DB Year Selection
//$activeYearID = $indexVar['libreportcard']->Get_Active_AcademicYearID();
$htmlAry["DBYearSelection"] = $indexVar["libreportcard_ui"]->Get_DB_Year_Selection($activeYearID, $onChange="js_Reload_Term_Class_Selection()", $noFirstTitle=false);
?>