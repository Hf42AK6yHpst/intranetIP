<?php
// Using : 
/*
 *  Date: 2019-10-11 (Bill)
 *          - added handling for archive mode
 *  Date: 2018-12-11 (Philips + Bill)
 *          - support parent access
 */

### Handle POST data
// Students
$studentIdAry = $_POST["StudentID"];
$htmlAry["StudentID"] = implode("', '", (array)$studentIdAry);

// Term
$htmlAry["TermID"] = $_POST["TermID"];

// Class
if(!$indexVar['libreportcard']->IS_KG_PARENT_ACCESS())
{
    $htmlAry["ClassID"] = $_POST["ClassID"][0];

    ## Special Handling for archive report
    if($_POST['isArchiveReport'] == 1)
    {
        # Get Class Student
        $classStudentAry = array();
        if($htmlAry["ClassID"]) {
            $classStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
        }
        $studentIdAry = Get_Array_By_Key($classStudentAry, 'UserID');

        $htmlAry["StudentID"] = implode("', '", (array)$studentIdAry);
        $_POST["StudentID"] = $studentIdAry;
    }
}
else
{
    # Get available KG classes
    $classes = $indexVar["libreportcard"]->GetAllClassesInTimeTable();
    $classAry = array();
    foreach($classes as $c){
        $classAry[] = $c['YearClassID'];
    }
    
    # Get related KG classes
    //$students = $indexVar['libclass']->getStudentByClassId($classAry);
    $students = $indexVar["libreportcard"]->Get_Student_By_Class($classAry, $_POST['StudentID']);
    $classStudents = array();
    $classAry = array();
    foreach($students as $student){
        if(in_array($student['UserID'], $_POST['StudentID'])){
            $classStudents[$student['YearClassID']][] = $student['UserID'];
            $classAry[] = $student['YearClassID'];
        }
    }
    //array_unique($classAry);
    //$htmlAry['ClassID'] = implode("','", $classAry);
    $htmlAry['ClassID'] = $classAry[0];
}
// $htmlAry["topicScoreArr"] = "";

$htmlAry["targetPage"] = !$indexVar['libreportcard']->IS_KG_PARENT_ACCESS() && $_POST['isArchiveReport']? 'archive' : 'print';

# Get Macau Category
$topicMOType = $indexVar["libreportcard"]->Get_Macau_Category($returnAssocArr=true);
$topicMOType = array_values($topicMOType);
if(empty($topicMOType)) {
    $topicMOType = array('健康與體育', '語言', '個人、社交與人文', '數學與科學', '藝術');
}

# Get Student Topic Score Summary
if(!$indexVar['libreportcard']->IS_KG_PARENT_ACCESS())
{
    $studentInfoAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"], $studentIdAry, 0, 0, 0, 1);
    $topicScoreSummary = $indexVar["libreportcard"]->GetStudentScoreSummary($_POST["StudentID"], $_POST["ClassID"][0], $_POST["TermID"]);
    $topicScoreSummary = $topicScoreSummary[0];
}
else
{
    $studentInfoAry = array();
    $topicScoreSummary = array();
    foreach($classStudents as $class => $students)
    {
        $stdInfo = $indexVar["libreportcard"]->Get_Student_By_Class(array($class), $students, 0, 0, 0, 1);
        $studentInfoAry = $studentInfoAry + $stdInfo;
        $tss = $indexVar['libreportcard']->GetStudentScoreSummary($students, $class, $_POST['TermID']);
        if(!empty($tss)) {
            $topicScoreSummary = $topicScoreSummary + $tss[0];
        }
    }
}

$htmlAry["highchartsAry"] = array();
$htmlAry["highchartsDiv"] = "";
$htmlAry["highchartsScript"] = "";
for($i=0; $i<sizeof($studentIdAry); $i++)
{
	// Student Info for Highcharts
	$thisStudentId = $studentIdAry[$i];
	$thisStudentName = $studentInfoAry[$thisStudentId]["StudentNameCh"];
	$studentCatAScore = $topicScoreSummary[$thisStudentId]["A"];
	$studentCatAScore = $studentCatAScore? $studentCatAScore : "0";
	$studentCatBScore = $topicScoreSummary[$thisStudentId]["B"];
	$studentCatBScore = $studentCatBScore? $studentCatBScore : "0";
	$studentCatCScore = $topicScoreSummary[$thisStudentId]["C"];
	$studentCatCScore = $studentCatCScore? $studentCatCScore : "0";
	$studentCatDScore = $topicScoreSummary[$thisStudentId]["D"];
	$studentCatDScore = $studentCatDScore? $studentCatDScore : "0";
	$studentCatEScore = $topicScoreSummary[$thisStudentId]["E"];
	$studentCatEScore = $studentCatEScore? $studentCatEScore : "0";
	
	$chartName = "chart".($i + 1);
	$containerName = "container_".$thisStudentId;
	
	$htmlAry["highchartsAry"][] = $chartName;
	$htmlAry["highchartsDiv"] .= "<div class='container' id='".$containerName."'></div>";
	$htmlAry["highchartsScript"] .= "
				var ".$chartName." = Highcharts.chart('".$containerName."', {
								chart: {
									backgroundColor: '#FCF4D4',
									marginTop: 20,
									spacingBottom: 0
								},
								credits: {
							    	enabled: false
							  	},
								
							    title: {
							        text: ''
							    },
							    subtitle: {
							        text: ''
							    },
								
							    xAxis: {
							        labels: {
							    		style: {
							    			fontSize: '12px'
							    		}
							    	},
							        categories: ['".implode("', '", $topicMOType)."']
							    },
							    yAxis: {
							        title: {
							            text: ''
							        },
							        max: 5,
							        min: 0,
							        tickInterval: 0.5,
							        gridLineColor: '#E1C093',
							        gridLineWidth: 3,
							        zIndex: 0,
							        labels: {
							        	style: {
							        		fontSize: '16px'
							        	}
							        },
							        plotBands: {
							        	color: '#3F9032',
							        	borderColor: '#3F9032',
							        	borderWidth: 3,
							        	thickness: 5,
							        	from: 3.5,
							        	to: 3.5,
							        	zIndex: 1
							        }
							    },
								legend: {
									itemStyle:{
										color: 'skyblue'
									},
									itemHiddenStyle:{
										color: '#3F9032'
									}
								},
								
							    series: [{
							        		name: '基礎能力分數',
							        		data: [3.5],
							        		marker: {
							            		radius: 0,
							            		visible: false
							        		},
							        		dataLabels: {enabled: false},
							        		label: {enabled: false},
							        		visible: false
							        	},
								        {
								        	name: '".$thisStudentName."',
								        	data: [$studentCatAScore, $studentCatBScore, $studentCatCScore, $studentCatDScore, $studentCatEScore],
								        	marker: {
												radius: 4,
												symbol: 'circle'
								        	},
											dataLabels: {enabled: false},
											label: {enabled: false},
											color: 'skyblue'
								    	}
							    ]
							});\n";
}

$htmlAry["highchartsAry"] = implode(", ", $htmlAry["highchartsAry"]);
?>