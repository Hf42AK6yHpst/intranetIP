<?
// Using : 
/*
 *  Date: 2010-02-13 (Philips)
 *          Added View Report Checking with View Report Period Checking
 * 
 *  Date: 2018-12-11 (Philips + Bill)
 *          - support parent access
 * 	Date: 2018-01-11 (Bill)
 * 			Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["GenerateReports"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start();

# Student Related Selection
if(!$indexVar['libreportcard']->IS_KG_PARENT_ACCESS())
{
    # Semester Selection
    $htmlAry["SemesterSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection("", " ");
    
    # Class Selection
    $htmlAry["ClassSelection"] = $indexVar["libreportcard_ui"]->Get_Class_Selection("", " js_Reload_Student_Selection() ", true);
}
else
{
    # Semester
    $htmlAry['SemesterSelection'] = "";
    $relatedSemesters = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
    foreach($relatedSemesters as $thisSemesterID => $thisSemesterName)
    {
        $isViewEnabled = $indexVar['libreportcard']->isViewReportPeriod($thisSemesterID);
        if($isViewEnabled) {
            $htmlAry["SemesterSelection"] .= "<option value='$thisSemesterID'>$thisSemesterName</option>";
        }
        //$htmlAry["SemesterSelection"] = $thisSemesterName;
        //$htmlAry["SemesterSelection"] .= "<input type='hidden' name='TermID' id='TermID' value='$thisSemesterID' />";
    }
    $disabled = $htmlAry["SemesterSelection"] != '' ? '' : 'disabled style="display:none"';
    
    if($htmlAry["SemesterSelection"] != '') {
        $htmlAry['SemesterSelection'] = '<select name="TermID" id="TermID">' . $htmlAry['SemesterSelection'] . '</select>';
    } else {
        $htmlAry['SemesterSelection'] = '--';
    }
    
    # Get Children
    include_once ($indexVar['thisBasePath']."includes/libuser.php");
    $lu = new libuser($indexVar['curUserId']);
    $ChildrenList = $lu->getChildrenList();
    $ChildrenIDAry = Get_Array_By_Key($ChildrenList, 'StudentID');
    
    # Get available KG classes
    $classes = $indexVar["libreportcard"]->GetAllClassesInTimeTable();
    $classAry = array();
    foreach($classes as $c){
        $classAry[] = $c['YearClassID'];
    }
    
    # Get Students in KG classes
    //$students = $indexVar['libclass']->getStudentByClassId($classAry);
    $students = $indexVar["libreportcard"]->Get_Student_By_Class($classAry, $ChildrenIDAry);
    $studentAry = array();
    foreach($students as $student){
        $studentAry[] = $student['UserID'];
    }
    $studentList = array();
    foreach($ChildrenList as $children){
        if(in_array($children['StudentID'], $studentAry)){
            $studentList[] = $children;
        }
    }
    
    # Children Selection
    //$htmlAry["ChildSelection"] = getSelectByArray($studentList, " name='StudentID[]' id='StudentID' multiple", "", "", 1);
    //$htmlAry["ChildSelection"] .= $indexVar["libreportcard_ui"]->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('StudentID', 1);", "selectAllBtn");
    //$htmlAry["ChildSelection"] .= $indexVar["libreportcard_ui"]->MultiSelectionRemark();
    $htmlAry["ChildSelection"] = getSelectByArray($studentList, " name='StudentID[]' id='StudentID' ", "", 0, 1);
}
?>