<?php
// Using:
/*
 *  Date: 2019-10-31 Bill
 *          - Create page
 */

if($_POST["ClassID"] && $_POST["YearTermID"] && $_POST["TopicCatID"])
{
    include_once($indexVar['thisBasePath']."includes/libexporttext.php");
    $lexport = new libexporttext();

    # Get Class Info
    $allClassAry = $indexVar["libreportcard"]->Get_All_KG_Class();
    $allClassAry = BuildMultiKeyAssoc((array)$allClassAry, "YearClassID");
    $thisClassAry = $allClassAry[$_POST["ClassID"]];
    $thisLevelID = $thisClassAry["YearID"];
    $thisClassName = Get_Lang_Selection($thisClassAry["ClassTitleB5"], $thisClassAry["ClassTitleEN"]);

    # Get Semester
    $yearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
    $thisTermName = $yearTermAry[$_POST["YearTermID"]];

    # Get Topics
    $topicsAry = $indexVar["libreportcard"]->getTopics("", $_POST["TopicCatID"], $thisLevelID, false, $_POST["YearTermID"], false, true);
    $numOfTopics = count($topicsAry);
    $thisTopicCatName = $topicsAry[0]["TopicCatTypeName"];

    # Get Class Student
    $thisClassStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
    $numOfClassStudent = count($thisClassStudentAry);

    # Get Student Topic Score
    $topicIDAry = Get_Array_By_Key((array)$topicsAry, "TopicID");
    $studentIDAry = Get_Array_By_Key((array)$thisClassStudentAry, "UserID");
    $studentScoreAry = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($studentIDAry, $thisLevelID, $topicIDAry, "", $_POST["YearTermID"]);

    ## Header
    $headerArr = array();
    $headerArr[] = '班別';
    $headerArr[] = '學號';
    $headerArr[] = '學生名稱';
    foreach($topicsAry as $thisTopicData) {
        $headerArr[] = $thisTopicData["NameCh"];
    }
    $headerArr[] = '總分';

    ## Student rows
    $exportArr = array();
    foreach($thisClassStudentAry as $thisClassStudent)
    {
        $dataArr = array();
        $dataArr[] = $thisClassStudent["ClassName"];
        $dataArr[] = $thisClassStudent["ClassNumber"];
        $dataArr[] = $thisClassStudent["StudentName"];

        // loop Ability Index
        $topic_score_total = 0;
        $topic_score_count = 0;
        foreach($topicsAry as $thisTopicData)
        {
            $topic_score = $studentScoreAry[$thisClassStudent["UserID"]][$thisTopicData["TopicID"]];
            if(isset($topic_score)) {
                $topic_score = $topic_score["Score"];
                $topic_score = $topic_score ? $topic_score : "";
                if($topic_score != "") {
                    $topic_score_total += $topic_score;
                }
            } else {
                $topic_score = "";
            }
            $dataArr[] = $topic_score;

            $topic_score_count++;
        }

        // Calculate total average
        if($topic_score_count > 0) {
            $topic_score_total = $topic_score_total / $topic_score_count;
            $topic_score_total = my_round($topic_score_total, 2);
        }
        $dataArr[] = $topic_score_total;

        $exportArr[] = $dataArr;
    }

    $export_content .= $lexport->GET_EXPORT_TXT($exportArr, $headerArr, "\t", "\r\n", "\t", 0, "11");

    intranet_closedb();

    # Output the file to user browser
    $filename = 'summary.csv';
    $lexport->EXPORT_FILE($filename, $export_content);
}
?>