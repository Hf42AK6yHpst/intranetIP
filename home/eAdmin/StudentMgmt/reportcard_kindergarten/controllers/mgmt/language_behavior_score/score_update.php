<?
// Editing by 
/*
 * 	Date: 2017-12-18 (Bill)
 * 			Create file
 */

if($_POST["ClassID"] && $_POST["ClassLevelID"] && $_POST["YearTermID"] && $_POST["TopicCatID"] && !empty($_POST["mark"]))
{
	$studentIDAry = array_keys((array)$_POST["mark"]);
	$studentScoreAry = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($studentIDAry, $_POST["ClassLevelID"], "", $_POST["TopicCatID"], $_POST["YearTermID"]);
	
	$insertDataAry = array();
	$updateDataAry = array();
	foreach($_POST["mark"] as $_studentId => $_studentScore) {
		foreach($_studentScore as $_topicId  => $_topicScore)
		{
			$dataAry = array();
			if(isset($studentScoreAry[$_studentId][$_topicId]))
			{
				$dataAry["TopicScoreID"] = $studentScoreAry[$_studentId][$_topicId]["TopicScoreID"];
				$dataAry["Score"] = $_topicScore;
				$dataAry["ModifiedBy"] = $UserID;
				$updateDataAry[] = $dataAry;
			}
			else
			{
				$dataAry["StudentID"] = $_studentId;
				$dataAry["ClassLevelID"] = $_POST["ClassLevelID"];
				$dataAry["TopicID"] = $_topicId;
				$dataAry["TermID"] = $_POST["YearTermID"];
				$dataAry["Score"] = $_topicScore;
				$insertDataAry[] = $dataAry;
			}
		}
	}
	
	// Insert Topic Score
	$success_insert = true;
	if(count($insertDataAry) > 0) {
		$success_insert = $indexVar["libreportcard"]->INSERT_STUDENT_TOPIC_SCORE($insertDataAry);
	}
	
	// Update Topic Score
	$success_update = true;
	if(count($updateDataAry) > 0) {
		$success_update = $indexVar["libreportcard"]->UPDATE_STUDENT_TOPIC_SCORE($updateDataAry);
	}
}

$success = $success_insert && $success_update ? "1" : "0";
if($indexVar["libreportcard"]->IS_KG_ADMIN_USER() || $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()){
    header("Location:index.php?task=mgmt.language_behavior_score.list&YearID=".$_POST["ClassLevelID"]."&TermID=".$_POST["YearTermID"]."&success=".$success);
}
else{
    header("Location:index.php?task=mgmt.language_behavior_score.list&success=".$success);
}
?>