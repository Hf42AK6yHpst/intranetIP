<?
// Editing by 
/*
 *  Date: 2019-10-31 Bill
 *          - Show all topics
 *          - Add export function
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 	Date: 2019-05-28 (Bill)
 * 			always display indicators column
 *  Date: 2019-02-08 (Philips)
 *          Added Input Lock with checking Input Score Period
 * 	Date: 2017-12-20 (Bill)
 * 			Get Topics related to Current Term / Whole Year only
 * 	Date: 2017-12-18 (Bill)
 * 			Create file
 */

## Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["LanguageBehavior"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

# Navigation
$NavigationAry[] = array($Lang["eReportCardKG"]["Management"]["LanguageBehavior"]["Title"], "javascript:goBack();");
$NavigationAry[] = array($Lang["eReportCardKG"]["Management"]["LanguageBehavior"]["InputScore"]);
$htmlAry["Navigation"] = $indexVar["libreportcard_ui"]->GET_NAVIGATION_IP25($NavigationAry);

$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmScoreUpdate']);

// Valid Data
if($_POST["ClassID"] && $_POST["YearTermID"] && $_POST["TopicCatID"])
{
	# Get Class Info
	$allClassAry = $indexVar["libreportcard"]->Get_All_KG_Class();
	$allClassAry = BuildMultiKeyAssoc((array)$allClassAry, "YearClassID");
	$thisClassAry = $allClassAry[$_POST["ClassID"]];
	$thisLevelID = $thisClassAry["YearID"];
	$thisClassName = Get_Lang_Selection($thisClassAry["ClassTitleB5"], $thisClassAry["ClassTitleEN"]);
	$htmlAry["classNameDisplay"] = $thisClassName;
	
	# Get Semester
	$yearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
	$thisTermName = $yearTermAry[$_POST["YearTermID"]];
	$htmlAry["termNameDisplay"] = $thisTermName;
	
	# Get Topics
	$topicsAry = $indexVar["libreportcard"]->getTopics("", $_POST["TopicCatID"], $thisLevelID, false, $_POST["YearTermID"], false, true);

	$numOfTopics = count($topicsAry);
	$thisTopicCatName = $topicsAry[0]["TopicCatTypeName"];
	$htmlAry["topicCatNameDisplay"] = $thisTopicCatName;
	
	# Get Class Student
	$thisClassStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
	$numOfClassStudent = count($thisClassStudentAry);
	
	# Get Student Topic Score
	$topicIDAry = Get_Array_By_Key((array)$topicsAry, "TopicID");
	$studentIDAry = Get_Array_By_Key((array)$thisClassStudentAry, "UserID");
	$studentScoreAry = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($studentIDAry, $thisLevelID, $topicIDAry, "", $_POST["YearTermID"]);
}
//$isInputLocked = true;
$isInputLocked = $indexVar['libreportcard']->isInputScorePeriod($_POST["YearTermID"])? false : true;
$disabled = $isInputLocked? " disabled " : "";

# Topic Score Table
$tableContent = "";
$tableContent .= "<div class='score_table_outter'>";
$tableContent .= "<table class='score_table' width='100%' cellpadding='4' cellspacing='0'>";
	$tableContent .= "<tr>";
if(empty($thisClassStudentAry))
{
		$tableContent .= "<td width='100%' align='center' class='tablegreentop tabletopnolink'>&nbsp;</td>";
}
else
{
	// Table Column Style
	$_column_count = $numOfClassStudent ? $numOfClassStudent : 1;
	$_td_width = 75 / $_column_count;
	$_td_width = (int)$_td_width."%";
	
	$tableContent .= "<td align='center' class='tablegreentop tabletopnolink static_display_col'>&nbsp;</td>";
	$tableContent .= "<td align='center' class='tablegreentop tabletopnolink static_based_col'>&nbsp;</td>";
	foreach($thisClassStudentAry as $thisClassStudent)
	{
		$thisStudentName = $thisClassStudent["StudentName"]." (".$thisClassStudent["ClassName"]." - ".$thisClassStudent["ClassNumber"].")";
		$tableContent .= "<td align='center' class='tablegreentop tabletopnolink'>".$thisStudentName."</td>";
	}
}
	$tableContent .= "</tr>";

if(empty($topicsAry))
{
	// No Record
	$tableContent .= "<tr>";
		$tableContent .= "<td align='center' colspan='".($numOfClassStudent + 1)."'>";
			$tableContent .= $Lang["eReportCardKG"]["General"]["NoRecord"] ;
		$tableContent .= "</td>";
	$tableContent .= "</tr>";
}
else
{
	$tr_count = 0;
	foreach($topicsAry as $thisTopicData)
	{
		$_tr_css = ($tr_count % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
		$_td_css = ($tr_count % 2 == 1) ? "attendanceouting" : "attendancepresent";
		$_topicName = Get_Lang_Selection($thisTopicData["NameCh"], $thisTopicData["NameEn"]);
		
		$tableContent .= "<tr class='".$_tr_css."'>";
		$tableContent .= "<td align='left' class='".$_td_css." tabletext static_display_col'>".$_topicName."</td>";
		$tableContent .= "<td align='left' class='".$_td_css." tabletext static_based_col'>".$_topicName."</td>";
		
		$td_count = 0;
		foreach($thisClassStudentAry as $thisClassStudent)
		{
		    $mark_value = $studentScoreAry[$thisClassStudent["UserID"]][$thisTopicData["TopicID"]];
		    if(isset($mark_value)) {
		    	$mark_value = $mark_value["Score"];
		    	$mark_value = $mark_value ? $mark_value : "";
		    }
		    else {
		    	$mark_value = "";
		    }
		    $mark_id = "mark[$tr_count][$td_count]";
		    $mark_name = "mark[".$thisClassStudent["UserID"]."][".$thisTopicData["TopicID"]."]";
		    $mark_tag = " id='".$mark_id."' name='".$mark_name."' ".$disabled ." class='tabletexttoptext' value='".$mark_value."' ";
		    
			$InputMethod = "<input type='number' class='textboxnum' ".$mark_tag." />";
	  		$tableContent .= "<td align='center' class='".$_td_css." tabletext'>".$InputMethod."</td>";
	  		
	  		$td_count++;
		}
		$tableContent .= "</tr>";
		
		$tr_count++;
	}
}
$tableContent .= "</table>";
$tableContent .= "</div>";
$htmlAry["scoreTableContent"] = $tableContent;

### content tool
$btnAry = array();
$btnAry[] = array('export', 'javascript: goExport();', $Lang['Btn']['Export']);
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# Button
$htmlAry["Button"] = "";
if(!$isInputLocked && $numOfTopics > 0 && $numOfClassStudent > 0)
{
	$htmlAry["Button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_submit, "button", "formSubmit()", "backBtn");
	$htmlAry["Button"] .= "&nbsp;&nbsp;";
}
$htmlAry["Button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value='".$_POST["ClassID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='YearTermID' id='YearTermID' value='".$_POST["YearTermID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='TopicCatID' id='TopicCatID' value='".$_POST["TopicCatID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassLevelID' id='ClassLevelID' value='".$thisLevelID."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='FullMark' id='FullMark' value='5'>\r\n";

### Required JS CSS - Paste Score from Excel
echo $indexVar["libreportcard_ui"]->Include_CopyPaste_JS_CSS("2016");
echo $indexVar["libreportcard_ui"]->Include_Excel_JS_CSS();
?>