<?php 
// Using:
/*
 * 	Date: 2019-09-24 (Philips)
 * 			Seperated from ajax_get_learning_tool.php to handle teaching tool display filtered by Chapter Selection
 *
 */
$ZoneData = $indexVar["libreportcard"]->Get_Learning_Zone_Record($ZoneID);

$ToolData = $indexVar["libreportcard"]->Get_Equipment_Record($YearID, '', '', '', $ZoneID, '' , $chapter);

$toolData_Timetable = str_replace("\\", "", $toolData); 	// records in Timetable
$toolData_Timetable = $indexVar["JSON"]->decode($toolData_Timetable);

$i = 0;
$count = sizeof($ToolData);
$numberInRow = 3;
if(!$YearID)
{
	$CheckBoxArr = $Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["PleaseEnterTopic"];
	echo $CheckBoxArr;
	return false;
}

$CheckBoxArr = "<table class='form_table_v30 inside_form_table inside_form_table_v30'>";
if(!empty($ToolData))
{
	// Select All button
	$CheckBoxArr .= "<tr>";
		$CheckBoxArr .= "<td width='100%' style='text-align: right' colspan='".$numberInRow."'>";
			$CheckBoxArr .= $indexVar["libreportcard_ui"]->GET_SMALL_BTN($button_select_all, "button", "setChecked(1, this.form, 'ToolID[]'); return false;");
		$CheckBoxArr .= "</td>";
	$CheckBoxArr .= "</tr>";
	foreach ((array)$ToolData as $_ToolData)
	{
		if(($i % $numberInRow) === 0) {
			$CheckBoxArr .= "<tr>";
		}
		
		$checked = "";
		if(in_array($_ToolData["CodeID"], (array)$toolData_Timetable[$ZoneID])) {
			$checked = "checked";
		}
		
		$CheckBoxArr .= "<td>";
			$CheckBoxArr .= "<table>";
				$CheckBoxArr .= "<tr><td style='width:300px; height:225px;'>";
					$CheckBoxArr .= "<label for='Tool_".$_ToolData["CodeID"]."'>";
					if($_ToolData["PhotoPath"]) {
						$CheckBoxArr .= "<img src='".$indexVar["thisImage"].$_ToolData["PhotoPath"]."?t=".time()."' style='width:300px;'>";
					}
					else {
						$CheckBoxArr .= "";
					}
					$CheckBoxArr .= "</label>";
				$CheckBoxArr .= "</td></tr>";
				$CheckBoxArr .= "<tr><td>";
					$CheckBoxArr .= "<input type='checkbox' id='Tool_".$_ToolData["CodeID"]."' name='ToolID[]' value='".$_ToolData["CodeID"]."' $checked>";
					$CheckBoxArr .= "&nbsp";
					$CheckBoxArr .= "<label for='Tool_".$_ToolData["CodeID"]."'>";
						$CheckBoxArr .= Get_Lang_Selection($_ToolData["CH_Name"], $_ToolData["EN_Name"])." (".$_ToolData["Code"].")";
					$CheckBoxArr .= "</label>";
				$CheckBoxArr .= "</td></tr>";
			$CheckBoxArr .= "</table>";
		$CheckBoxArr .= "</td>";
		
		if((($i % $numberInRow) === 0 && $count === 1) || ($i + 1) == $count) {
			$CheckBoxArr .= "</tr>";
		}
		$i++;
	}
}
else
{
	$CheckBoxArr .= "<tr><td>";
		$CheckBoxArr .= $Lang["General"]["NoRecordAtThisMoment"] ;
	$CheckBoxArr .= "</td></tr>";
}
$CheckBoxArr .= "</table>";

echo $CheckBoxArr;
?>