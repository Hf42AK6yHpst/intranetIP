<?php 
/*
 * Change Log:
 * Date 2017-02-03 Villa:	Open the File
 */
foreach ((array)$timeTable_checkbox as $timeTableID){
	if($timeTableID){
		$indexVar['libreportcard'] ->DeleteTimeTable($timeTableID);
		$indexVar['libreportcard'] ->DeleteTimeTopicMapping($timeTableID);
		$indexVar['libreportcard'] ->DeleteTimeZoneMapping($timeTableID);
		$indexVar['libreportcard'] ->DeleteTimeToolMapping($timeTableID);
	}
}
header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.topic_timetable.list&success=1");
?>