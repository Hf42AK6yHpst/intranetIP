<?php 
/* Using: Villa
 * Change Log:
 * Date: 2017-11-17	Bill	Update Quota Settings
 * Date: 2017-2-2 	Villa	Open the File
 */

$toolData = str_replace("\\", "", $toolData);
$toolData_Timetable = $indexVar['JSON']->decode($toolData);

$ZoneQuotaData = str_replace("\\", "", $zoneQuotaData);
$ZoneQuotaData = $indexVar['JSON']->decode($ZoneQuotaData);

$DateStart = $DateStart.$DateStart_hour.":".$DateStart_min.":".$DateStart_sec;
$StartDateTime = strtotime($DateStart);
$DateStart = date("Y-m-d H:i:s", $StartDateTime);

$DateEnd = $DateEnd.$DateEnd_hour.":".$DateEnd_min.":".$DateEnd_sec;
$EndDateTime = strtotime($DateEnd);
$DateEnd = date("Y-m-d H:i:s", $EndDateTime);

### Update Start ###
$temp = $indexVar['libreportcard']->UpdateTimeTable($TimeTableID,$Code, $CH_Name, $EN_Name, $YearID, $DateStart, $DateEnd);
$TimeTableID = $temp[0]['TopicTimeTableID']; // Get TimeTable ID
if($topic && $TimeTableID)
{
	$indexVar['libreportcard']->UpdateTimeTopicMapping($TimeTableID, $topic);
	if($ZoneID){
		$indexVar['libreportcard']->UpdateTimeZoneMapping($TimeTableID, $topic, $ZoneID, $ZoneQuotaData);
		if($toolData_Timetable){
			$indexVar['libreportcard']->UpdateTimeToolMapping($TimeTableID, $topic, $ZoneID, $toolData_Timetable);
		}
	}
	else{ // No Zone is selected
		$indexVar['libreportcard']->DeleteTimeZoneMapping($TimeTableID);
		$indexVar['libreportcard']->DeleteTimeToolMapping($TimeTableID);
	}
}
header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.topic_timetable.list&success=1");
?>