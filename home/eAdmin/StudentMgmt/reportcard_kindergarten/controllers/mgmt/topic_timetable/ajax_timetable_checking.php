<?php

# Get POST data
$dataAry = array();
parse_str($_POST["DataList"], $dataAry);
		
# Get data after parse_str()
$TimeTableID = $dataAry["TimeTableID"];
$Code = trim($dataAry["Code"]);
$CH_Name = trim($dataAry["CH_Name"]);
$EN_Name = trim($dataAry["EN_Name"]);
$YearID = $dataAry["YearID"];
$DateStart = $dataAry["DateStart"];
$DateStart_hour = $dataAry["DateStart_hour"];
$DateStart_min = $dataAry["DateStart_min"];
$DateStart_sec = $dataAry["DateStart_sec"];
$DateEnd = $dataAry["DateEnd"];
$DateEnd_hour = $dataAry["DateEnd_hour"];
$DateEnd_min = $dataAry["DateEnd_min"];
$DateEnd_sec = $dataAry["DateEnd_sec"];

# Format Date
$DateStart = $DateStart.$DateStart_hour.":".$DateStart_min.":".$DateStart_sec;
$DateStart = strtotime($DateStart);
$DateStart = date("Y-m-d H:i:s", $DateStart);
$DateEnd = $DateEnd.$DateEnd_hour.":".$DateEnd_min.":".$DateEnd_sec;
$DateEnd = strtotime($DateEnd);
$DateEnd = date("Y-m-d H:i:s", $DateEnd);

# Condition
$DateRangeCond = "";
$DateRangeCond .= " (StartDate <= '$DateStart' AND EndDate >= '$DateEnd') OR ";
$DateRangeCond .= " (StartDate >= '$DateStart' AND EndDate <= '$DateEnd') OR ";
$DateRangeCond .= " (StartDate <= '$DateStart' AND EndDate >= '$DateStart') OR ";
$DateRangeCond .= " (StartDate <= '$DateEnd' AND EndDate >= '$DateEnd') ";
$TimeTableCond = "";
if($TimeTableID) {
	$TimeTableCond = " AND TopicTimeTableID != '$TimeTableID'";
}

# Perform Checking
$sql = "SELECT COUNT(TopicTimeTableID) FROM ".$indexVar['thisDbName'].".RC_TOPICTIMETABLE WHERE TimeTableCode = '$Code' AND IsDeleted = '0' $TimeTableCond";
$ExistingCode = $indexVar['libreportcard']->returnVector($sql);
$ExistingCode = $ExistingCode[0] > 0;
$sql = "SELECT COUNT(TopicTimeTableID) FROM ".$indexVar['thisDbName'].".RC_TOPICTIMETABLE WHERE CH_Name = '$CH_Name' AND IsDeleted = '0' $TimeTableCond";
$ExistingCH_Name = $indexVar['libreportcard']->returnVector($sql);
$ExistingCH_Name = $ExistingCH_Name[0] > 0;
$sql = "SELECT COUNT(TopicTimeTableID) FROM ".$indexVar['thisDbName'].".RC_TOPICTIMETABLE WHERE EN_Name = '$EN_Name' AND IsDeleted = '0' $TimeTableCond";
$ExistingEN_Name = $indexVar['libreportcard']->returnVector($sql);
$ExistingEN_Name = $ExistingEN_Name[0] > 0;
$sql = "SELECT COUNT(TopicTimeTableID) FROM ".$indexVar['thisDbName'].".RC_TOPICTIMETABLE WHERE ($DateRangeCond) AND YearID = '$YearID' AND IsDeleted = '0' $TimeTableCond";
$ExistingDateRange = $indexVar['libreportcard']->returnVector($sql);
$ExistingDateRange = $ExistingDateRange[0] > 0;

# Return Result
$errorType = "";
if($ExistingCode) {
	$errorType = "Code";
}
else if ($ExistingCH_Name) {
	$errorType = "CH_Name";
}
else if ($ExistingEN_Name) {
	$errorType = "EN_Name";
}
else if($DateStart > $DateEnd) {
	$errorType = "Date";
}
else if ($ExistingDateRange) {
	$errorType = "Date2";
}
echo $errorType;

?>