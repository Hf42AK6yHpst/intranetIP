<?
// Editing by 
/*
 *  Date: 2019-10-31 (Bill)
 *          improved: class teacher > view own classes only
 *  Date: 2019-02-12 (Bill)
 *          fixed: php error due to $indexVar["libreportcard"]->Get_All_KG_Form()[0]['YearID']
 *  Date: 2018-04-09 (Bill)
 *          fixed: admin cannot access all KG classes if is class teacher
 * 	Date: 2017-12-18 (Bill)
 * 			Default show active TimeTable
 * 	Date: 2017-10-17 (Bill)
 * 			Support Admin to access all classes
 */

# Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["ToolScore"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

$AllYearIDAry = $indexVar["libreportcard"]->Get_All_KG_Form();

$filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();
if($filterByClassTeacher) {
    $AllYearIDAry = $indexVar["libreportcard"]->Get_Teaching_Level($UserID);
}

# Year Selection
$YearID = isset($_POST["YearID"])? $_POST["YearID"] : $_GET["YearID"];
if(!$YearID && !empty($AllYearIDAry)) {
    $YearID = $AllYearIDAry[0]['YearID'];
}
$htmlAry["YearSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($YearID, $onChange="document.form1.submit()", $allKGOptions=false, $noFirstTitle=true);

// for selected Class Level
if($YearID > 0)
{
	# Get TimeTable Info
	$TimeTableAry = $indexVar["libreportcard"]->GetTimeTable($YearID, "", false, "", $filterByClassTeacher);
	
	# Build TimeTable Mapping
	$TimeTableMapping = array();
	foreach ($TimeTableAry as $thisAryData)
	{
		$thisTimeTableID = $thisAryData["TopicTimeTableID"];
		$TimeTableMapping[$thisTimeTableID][] = $thisAryData;
	}
	if(!empty($TimeTableMapping))
	{
		$_timeTableIdAry = array_keys((array)$TimeTableMapping);
		if(!in_array($TimeTableID, $_timeTableIdAry)) {
			unset($TimeTableID);
		}
	}
	
	// loop Mapping
	$TimeTableData = array();
	$TimeTableSelected = false;
	foreach($TimeTableMapping as $thisTimeTableID => $thisTimeTableInfo)
	{
		$thisTimeTableName = Get_Lang_Selection($thisTimeTableInfo[0]["CH_Name"], $thisTimeTableInfo[0]["EN_Name"]);
		$thisTimeTableDateRange = $thisTimeTableInfo[0]["StartDate"]." - ".$thisTimeTableInfo[0]["EndDate"];
		$thisTimeTableName .= " (".$thisTimeTableDateRange.")"; 
		$TimeTableData[] = array($thisTimeTableID, $thisTimeTableName, $thisTimeTableDateRange);
		
		$getDefaultTimeTable = false;
		if(!$TimeTableID) {
			$StartDateTS = strtotime($thisTimeTableInfo[0]["StartDate"]);
			$EndDateTS = strtotime($thisTimeTableInfo[0]["EndDate"]);
			$currentDateTS = time();
			$getDefaultTimeTable = ($currentDateTS >= $StartDateTS) && ($currentDateTS <= $EndDateTS);
		}
		
		// for selected TimeTable
		if(!$TimeTableSelected || $getDefaultTimeTable || $TimeTableID == $thisTimeTableID) {
			$thisTopicName = $Lang["eReportCardKG"]["Management"]["ToolScore"]["TitleName"]." :&nbsp;&nbsp;".Get_Lang_Selection($thisTimeTableInfo[0]["TopicNameB5"], $thisTimeTableInfo[0]["TopicNameEN"]);
			$TimeTableSelected = $thisTimeTableID;
		}
	}
	
	# TimeTable Selection
	if(!empty($TimeTableData))
	{
		sortByColumn2($TimeTableData, "2");
		$htmlAry['TimeTableSelection'] = getSelectByArray($TimeTableData," id= \"TimeTableID\" name=\"TimeTableID\" onChange=\"document.form1.submit()\"", $TimeTableSelected, 0, 1);
		
		# Get TimeTable and related Info
		if($TimeTableSelected && $TimeTableSelected > 0)
		{
			// Teaching Classes
			$UserTeachingClasses = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, $YearID);

			# Admin can access all classes - Get Target Class Info
			//if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($UserTeachingClasses))
			if($indexVar['libreportcard']->IS_KG_ADMIN_USER())
			{
				$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
				$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearID", "YearClassID"));
				$UserTeachingClasses = $AccessibleClasses[$YearID];
			}
			
			// Zone in selected TimeTable
			$selectedTimeTable = $TimeTableMapping[$TimeTableSelected];
			$TimeTableZoneAry = BuildMultiKeyAssoc($selectedTimeTable, array("ZoneID", "ToolCodeID"));
		}
	}
}

# Table Header
$Table_Content = "";
$Table_Content .= "<table class='common_table_list_v30' id='ContentTable'>";
$Table_Content .= "<thead>";
	$Table_Content .= "<tr>";
		$Table_Content .= "<th style='width:3%;'>#</th>";
	
		$Table_Content .= "<th style='width:10%;'>";
			$Table_Content .= $Lang['eReportCardKG']['Management']['ToolScore']['ClassName'];
		$Table_Content .= "</th>";
		
		$Table_Content .= "<th style='width:10%;'>";
			$Table_Content .= $Lang['eReportCardKG']['Management']['Device']['LearningZone'];
		$Table_Content .= "</th>";
		
		$Table_Content .= "<th style='width:3%;'>&nbsp;</th>";
	$Table_Content .= "</tr>";
$Table_Content .= "</thead>";
$Table_Content .= "<tbody>";

# Table Content
if(empty($UserTeachingClasses) || empty($TimeTableZoneAry)){
	// No Record
	$Table_Content .= "<tr>";
		$Table_Content .= "<td align='center' colspan='4'>";
			$Table_Content .= $Lang['eReportCardKG']['General']['NoRecord'] ;
		$Table_Content .= "</td>";
	$Table_Content .= "</tr>";
}
else{
	$i = 1;
	// loop Teaching Classes
	foreach ($UserTeachingClasses as $thisTeachingClass)
	{
		// loop TimteTable Zone
		foreach ($TimeTableZoneAry as $thisTimeTableZoneID => $thisTimeTableInfo){
			if(empty($thisTimeTableInfo) || !is_array($thisTimeTableInfo))
				continue;
			
			// Get Zone Details
			$thisZoneInfo = array_shift(array_slice($thisTimeTableInfo, 0, 1));
			$thisZoneCount = count($thisTimeTableInfo);
			if($thisZoneCount == 1) {
				$thisTools = array_values((array)$thisTimeTableInfo);
				$thisZoneCount = $thisTools[0]["ToolCodeID"]? $thisZoneCount : 0;
			}
			
			// Content row
			$Table_Content .= "<tr>";
				$Table_Content .= "<td>".$i."</td>";
				$Table_Content .= "<td>".$thisTeachingClass['ClassName']."</td>";
				$Table_Content .= "<td>".Get_Lang_Selection($thisZoneInfo['ZoneNameB5'], $thisZoneInfo['ZoneNameEN'])."</td>";
				$Table_Content .= "<td>";
				if($thisZoneCount > 0) {
					$Table_Content .= "<a href='javascript:goEdit(\"".$thisTeachingClass["ClassID"]."\", \"".$thisTimeTableZoneID."\")'>";
						$Table_Content .= $thisZoneCount;
					$Table_Content .= "</a>";
				}
				else {
					$Table_Content .= $thisZoneCount;
				}
//				$Table_Content .= "<img src='/images/2009a/icon_edit_b.gif' width='20' height='20' border='0' title='Edit'>";
				$Table_Content .= "</td>";
			$Table_Content .= "<tr>";
			$i++;
		}
	}
}
$Table_Content .= "</tbody>";
$Table_Content .= "</table>";
$htmlAry['TimeTableContent'] = $Table_Content;

# Hidden Input Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='ClassID' id='ClassID' value=''>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='ZoneID' id='ZoneID' value=''>\r\n";

?>