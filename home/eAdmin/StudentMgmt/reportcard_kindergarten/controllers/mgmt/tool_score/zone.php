<?
// editing by 
/*
 * 	Date: 2017-10-17 (Bill)
 * 			Support Admin to access all classes
 */

# Get Data
$YearID = $_POST["YearID"]? $_POST["YearID"] : $_GET["YearID"];
$TimeTableID = $_POST["TimeTableID"]? $_POST["TimeTableID"] : $_GET["TimeTableID"];
$ClassID = $_POST["ClassID"]? $_POST["ClassID"] : $_GET["ClassID"];
$ZoneID = $_POST["ZoneID"]? $_POST["ZoneID"] : $_GET["ZoneID"];

# Return Message
if($success=="update"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success=="update_fail"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
else{
	$returnMsg = "";
}

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['ToolScore']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

# Navigation
$NavigationAry[] = array($Lang['eReportCardKG']['Management']['ToolScore']['Title'], "javascript:goBack();");
$NavigationAry[] = array($Lang['eReportCardKG']['Management']['ToolScore']['ToolList']);
$htmlAry['Navigation'] = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($NavigationAry); 

// Valid Data
if($YearID && $TimeTableID && $ClassID && $ZoneID)
{
	# Get TimeTable Info
	$TimeTableAry = $indexVar['libreportcard']->GetTimeTable($YearID, $TimeTableID);
	$TimeTableInfo = BuildMultiKeyAssoc($TimeTableAry, array("ZoneID"));
	$TimeTableInfo = $TimeTableInfo[$ZoneID];
	$thisTimeTableName = Get_Lang_Selection($TimeTableInfo["CH_Name"], $TimeTableInfo["EN_Name"]);
	$thisTimeTableRange = $TimeTableInfo["StartDate"]." - ".$TimeTableInfo["EndDate"];
	$thisTimeTableName .= " (".$thisTimeTableRange.")";
	$thisTopicName = Get_Lang_Selection($TimeTableInfo["TopicNameB5"], $TimeTableInfo["TopicNameEN"]);
	$thisZoneName = Get_Lang_Selection($TimeTableInfo["ZoneNameB5"], $TimeTableInfo["ZoneNameEN"]);

	# Get Class Info
	$thisTeachingClass = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, $YearID, "", $ClassID);
	$thisTeachingClassName = $thisTeachingClass[0]["ClassName"];
	
	# Admin can access all classes - Get Target Class Info
	if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisTeachingClass))
	{
		$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
		$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
		$thisTeachingClass = $AccessibleClasses[$ClassID];
		$thisTeachingClassName = $thisTeachingClass["ClassName"];
	}
	
	# Get Teaching Tools
	$TeachingToolsList = array();
	//$TeachingToolIDAry = Get_Array_By_Key($TimeTableAry, "ToolCodeID");
	$TeachingToolIDAry = BuildMultiKeyAssoc((array)$TimeTableAry, array("ZoneID", "ToolCodeID"));
	$TeachingToolIDAry = $TeachingToolIDAry[$ZoneID];
	foreach((array)$TeachingToolIDAry as $thisToolCodeID => $thisToolInfo) {
		$currentTeachingTool = $indexVar['libreportcard']->Get_Equipment_Record($YearID, $thisToolCodeID);
		$TeachingToolsList[] = $currentTeachingTool[0];
	}
}

# Table Header
$Table_Content = "";
$Table_Content .= "<table class='common_table_list_v30' id='ContentTable'>";
$Table_Content .= "<thead>";
	$Table_Content .= "<tr>";
		$Table_Content .= "<th style='width:1%;'>#</th>";
	
		$Table_Content .= "<th style='width:20%;'>";
			$Table_Content .= $Lang['eReportCardKG']['Management']['ToolScore']['TeachingTools'];
		$Table_Content .= "</th>";
		
		$Table_Content .= "<th style='width:10%;'>&nbsp;</th>";
	$Table_Content .= "</tr>";
$Table_Content .= "</thead>";
$Table_Content .= "<tbody>";

# Table Content
if(empty($TeachingToolIDAry)){
	// No Record
	$Table_Content .= "<tr>";
		$Table_Content .= "<td align='center' colspan='3'>";
			$Table_Content .= $Lang['eReportCardKG']['General']['NoRecord'] ;
		$Table_Content .= "</td>";
	$Table_Content .= "</tr>";
}
else{
	$i = 1;
	// loop Teaching Tools
	foreach ($TeachingToolsList as $thisTeachingTool)
	{
		if(empty($thisTeachingTool) || !is_array($thisTeachingTool))
			continue;
		
		// Content row
		$Table_Content .= "<tr>";
			$Table_Content .= "<td>".$i."</td>";
			$Table_Content .= "<td>".$thisTeachingTool['CH_Name']." ".$thisTeachingTool['EN_Name']."</td>";
			$Table_Content .= "<td>";
				$Table_Content .= "<a href='javascript:goEdit(\"".$thisTeachingTool['CodeID']."\")'>";
					$Table_Content .= "<img src='/images/2009a/icon_edit_b.gif' width='20' height='20' border='0' title='Edit'>";
				$Table_Content .= "</a>";
			$Table_Content .= "</td>";
		$Table_Content .= "<tr>";
		$i++;
	}
}
$Table_Content .= "</tbody>";
$Table_Content .= "</table>";
$htmlAry['ToolTableContent'] = $Table_Content;

# Button
$htmlAry['Button'] = $indexVar['libreportcard_ui']->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");

# Hidden Input Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='YearID' id='YearID' value='".$YearID."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='TimeTableID' id='TimeTableID' value='".$TimeTableID."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='ClassID' id='ClassID' value='".$ClassID."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='ZoneID' id='ZoneID' value='".$ZoneID."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='ToolID' id='ToolID' value=''>\r\n";

?>