<?
// editing by 
/*
 * 	Date: 2017-12-28 (Bill)
 * 			Support Insert new Input Score
 */

if($_POST["YearID"] && $_POST["TimeTableID"] && $_POST["ClassID"] && $_POST["ZoneID"] && $_POST["ToolID"])
{
	// loop Student
	$insertScoreAry = array();
	$updateScoreAry = array();
	foreach($_POST["InputScore"] as $thisStudentID => $thisStudentInputScore)
	{
		// loop Input Score
		foreach($thisStudentInputScore as $thisInputScoreID => $thisInputScoreValue)
		{
			// Insert new Input Score
			if(strpos($thisInputScoreID, "row") !== false)
			{
				$thisScoreAry = array();
				$thisScoreAry["StudentID"] = $thisStudentID;
				$thisScoreAry["ClassLevelID"] = $_POST["YearID"];
				$thisScoreAry["TimeTableID"] = $_POST["TimeTableID"];
				$thisScoreAry["TopicID"] = $_POST["TopicID"];
				$thisScoreAry["ZoneID"] = $_POST["ZoneID"];
				$thisScoreAry["ToolID"] = $_POST["ToolID"];
				$thisScoreAry["Score"] = $thisInputScoreValue;
				$thisScoreAry["isFromMgmt"] = 1;
				$thisScoreAry["ModifiedBy"] = $UserID;
				$insertScoreAry[] = $thisScoreAry;
			}
			// Update existing Input Score
			else
			{
				$thisScoreAry = array();
				$thisScoreAry["InputScoreID"] = $thisInputScoreID;
				$thisScoreAry["Score"] = $thisInputScoreValue;
				$thisScoreAry["ModifiedBy"] = $UserID;
				$updateScoreAry[] = $thisScoreAry;
			}
		}
	}
	
	// Update DB records
	$result = array();
	if(count($insertScoreAry) > 0) {
		$result["insert_success"] = $indexVar["libreportcard"]->INSERT_STUDENT_SCORE($insertScoreAry);
	}
	if(count($updateScoreAry) > 0) {
		$result["update_success"] = $indexVar["libreportcard"]->UPDATE_STUDENT_SCORE($updateScoreAry);
	}
}

$success = !empty($result) && !in_array(false, $result) ? "update" : "update_fail";
header("Location:index.php?task=mgmt.tool_score.zone&YearID=".$_POST["YearID"]."&TimeTableID=".$_POST["TimeTableID"]."&ClassID=".$_POST["ClassID"]."&ZoneID=".$_POST["ZoneID"]."&success=".$success);

?>