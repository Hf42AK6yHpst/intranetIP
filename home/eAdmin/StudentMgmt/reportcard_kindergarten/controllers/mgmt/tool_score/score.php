<?
// editing by 
/*
 * 	Date: 2019-10-11 Philips
 * 			Added general warning box
 * 
 *  Date: 2019-02-08 (Philips)
 *          Added Input Lock with checking Input Score Period
 * 
 * 	Date: 2017-12-27 (Bill)
 * 			Support Paste Input Score from Excel
 * 			Support Add Input Score row
 * 	Date: 2017-12-18 (Bill)
 * 			Hide Submit Button if no existing input
 * 	Date: 2017-10-17 (Bill)
 * 			Support Admin to access all classes
 */

# Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["ToolScore"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

# Navigation
$NavigationAry[] = array($Lang["eReportCardKG"]["Management"]["ToolScore"]["Title"], "javascript:goBack('list');");
$NavigationAry[] = array($Lang["eReportCardKG"]["Management"]["ToolScore"]["ToolList"], "javascript:goBack('zone');");
$NavigationAry[] = array($Lang["eReportCardKG"]["Management"]["ToolScore"]["InputScore"]);
$htmlAry["navigation"] = $indexVar["libreportcard_ui"]->GET_NAVIGATION_IP25($NavigationAry); 

$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmScoreUpdate']);

// Valid Data
$currentTermIDArr = array();
if($_POST["YearID"] && $_POST["TimeTableID"] && $_POST["ClassID"] && $_POST["ZoneID"] && $_POST["ToolID"])
{
	# Get TimeTable Info
	$TimeTableAry = $indexVar["libreportcard"]->GetTimeTable($_POST["YearID"], $_POST["TimeTableID"]);
	$TimeTableInfo = BuildMultiKeyAssoc($TimeTableAry, array("ZoneID"));
	$TimeTableInfo = $TimeTableInfo[$_POST["ZoneID"]];
	
	// TimeTable Display Info
	$thisTimeTableName = "";
	$thisTimeTableName .= Get_Lang_Selection($TimeTableInfo["CH_Name"], $TimeTableInfo["EN_Name"]);
	$thisTimeTableName .= " (".$TimeTableInfo["StartDate"]." - ".$TimeTableInfo["EndDate"].")";
	$htmlAry["timeTableNameDisplay"] = $thisTimeTableName;
	$htmlAry["topicNameDisplay"] = Get_Lang_Selection($TimeTableInfo["TopicNameB5"], $TimeTableInfo["TopicNameEN"]);
	$htmlAry["zoneNameDisplay"] = Get_Lang_Selection($TimeTableInfo["ZoneNameB5"], $TimeTableInfo["ZoneNameEN"]);
	
	# Get Teaching Tool Info
	$thisTeachingTool = $indexVar["libreportcard"]->Get_Equipment_Record($_POST["YearID"], $_POST["ToolID"]);
	$thisTeachingToolName = Get_Lang_Selection($thisTeachingTool[0]["CH_Name"], $thisTeachingTool["EN_Name"]);
	$htmlAry["toolNameDisplay"] = $thisTeachingToolName;

	# Get Class Info
	$thisTeachingClass = $indexVar["libreportcard"]->Return_Class_Teacher_Class($UserID, $_POST["YearID"], "", $_POST["ClassID"]);
	$thisTeachingClassName = $thisTeachingClass[0]["ClassName"];
	$htmlAry["classTeacherNameDisplay"] = $thisTeachingClassName;
	
	# Admin can access all classes - Get Target Class Info
	if($indexVar["libreportcard"]->IS_KG_ADMIN_USER() && empty($thisTeachingClass))
	{
		$AccessibleClasses = $indexVar["libreportcard"]->GetAllClassesInTimeTable();
		$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
		$thisTeachingClass = $AccessibleClasses[$_POST["ClassID"]];
		$thisTeachingClassName = $thisTeachingClass["ClassName"];
		$htmlAry["classTeacherNameDisplay"] = $thisTeachingClassName;
	}
	
	# Get Class Student
	$ClassStudentList = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
	$ClassStudentIDAry = Get_Array_By_Key($ClassStudentList, "UserID");
	
	# Get Student Score
	$thisStudentToolsScore = $indexVar["libreportcard"]->GET_STUDENT_SCORE($ClassStudentIDAry, $_POST["YearID"], $_POST["TimeTableID"], $TimeTableInfo["TopicID"], $_POST["ZoneID"], $_POST["ToolID"]);
	
	# Get related Academic Term
	$startYearTermInfo = getAcademicYearAndYearTermByDate($TimeTableInfo['StartDate']);
	$endYearTermInfo = getAcademicYearAndYearTermByDate($TimeTableInfo['EndDate']);
    if($indexVar["libreportcard"]->AcademicYearID == $startYearTermInfo['AcademicYearID']) {
        $currentTermIDArr[] = $startYearTermInfo['YearTermID'];
    }
    if($indexVar["libreportcard"]->AcademicYearID == $endYearTermInfo['AcademicYearID']) {
        $currentTermIDArr[] = $endYearTermInfo['YearTermID'];
    }
    $currentTermIDArr = array_values(array_filter(array_unique($currentTermIDArr)));
}
//$isInputLocked = true;
$isInputLocked = $indexVar['libreportcard']->isInputScorePeriod($currentTermIDArr) ? false : true;
$disabled = $isInputLocked? " disabled " : "";

# Table Header
$Table_Content = "";
$Table_Content .= "<table class='common_table_list_v30' id='ContentTable'>";
$Table_Content .= "<thead>";
	$Table_Content .= "<tr>";
		$Table_Content .= "<th>#</th>";
		$Table_Content .= "<th>";
			$Table_Content .= $Lang["eReportCardKG"]["Management"]["ToolScore"]["ClassNumber"];
		$Table_Content .= "</th>";
		$Table_Content .= "<th style='width:25%;'>";
			$Table_Content .= $Lang["eReportCardKG"]["Management"]["ToolScore"]["StudentName"];
		$Table_Content .= "</th>";
		$Table_Content .= "<th style='width:55%;'>&nbsp;</th>";
		$Table_Content .= "<th>&nbsp;</th>";
	$Table_Content .= "</tr>";
$Table_Content .= "</thead>";
$Table_Content .= "<tbody>";

# Table Content
if(empty($ClassStudentList))
{
	// No Record
	$Table_Content .= "<tr>";
		$Table_Content .= "<td align='center' colspan='5'>";
			$Table_Content .= $Lang["eReportCardKG"]["General"]["NoRecord"] ;
		$Table_Content .= "</td>";
	$Table_Content .= "</tr>";
}
else
{
	$i = 1;
	$tr_count = 0;
	
	// loop Teaching Tools
	foreach ($ClassStudentList as $thisClassStudent)
	{
		// Student Score
		$thisStudentID = $thisClassStudent["UserID"];
		$thisStudentScore = $thisStudentToolsScore[$thisStudentID];
		if(empty($thisClassStudent) || !is_array($thisClassStudent))
		{
			continue;
		}
		
		// Add row Button
		$addButton = $indexVar["libreportcard_ui"]->GET_ACTION_LNK("javascript:void(0);", "", $ParOnClick="addInputScore('".$thisStudentID."', '1')", $ParClass="add");
		if($isInputLocked) {
			$addButton = "&nbsp;";
		}
		
		// Student with Input Score
		$thisStudentScoreSize = count((array)$thisStudentScore);
		if($thisStudentScoreSize > 0)
		{
			// loop Input Score
			$isStudentFirstRow = true;
			$thisStudentTotalScore = 0;
			foreach($thisStudentScore as $thisStudentInputScore)
			{
				// Input Score Info
				$thisStudentInputScoreID = $thisStudentInputScore["InputScoreID"];
				$thisStudentInputScoreValue = $thisStudentInputScore["Score"];
				$thisStudentInputScoreValue = $thisStudentInputScoreValue ? $thisStudentInputScoreValue : 0;
				$thisStudentTotalScore += $thisStudentInputScoreValue;
				
				// Input Score row
				$Table_Content .= "<tr id='Tr_score_".$thisStudentID."_".$thisStudentInputScoreID."' class='dataRow'>";
				
				// Student Info Column
				if($isStudentFirstRow)
				{
					$firstRowSpan = $thisStudentScoreSize + 1;
					$Table_Content .= "<td class='student_col' rowspan='".$firstRowSpan."'>".$i."</td>";
					$Table_Content .= "<td class='student_col' rowspan='".$firstRowSpan."'>".$thisClassStudent["ClassTitleEn"]."-".$thisClassStudent["ClassNumber"]."</td>";
					$Table_Content .= "<td class='student_col' rowspan='".$firstRowSpan."'>".$thisClassStudent["StudentNameEn"]." (".$thisClassStudent["StudentNameCh"].")</td>";
				}
					
					$Table_Content .= "<td>";
						$Table_Content .= "<input type='text' class='textbox' name='InputScore[".$thisStudentID."][".$thisStudentInputScoreID."]' id='mark[".$tr_count."][0]' value='".$thisStudentInputScoreValue."' ".$disabled.">";
					$Table_Content .= "</td>";
					
					$Table_Content .= "<td>";
						if($isInputLocked) {
							$Table_Content .= "&nbsp;";
						} else {
							$Table_Content .= "<span class='table_row_tool row_content_tool'>";
								$Table_Content .= "<a onclick='deleteInputScore(this, \"".$thisStudentID."\", \"".$thisStudentInputScoreID."\")' title='delete' class='delete' href='javascript:void(0);'></a>";
							$Table_Content .= "</span>";
						}
					$Table_Content .= "</td>";
				$Table_Content .= "</tr>";
				
				$isStudentFirstRow = false;
				$tr_count++;
			}
			
			// Calculate Average Score
			$thisAverageScore = "&nbsp;";
			if($thisStudentScoreSize > 1)
			{
				$thisAverageScore = $thisStudentTotalScore / $thisStudentScoreSize;
				$thisAverageScore = my_round($thisAverageScore, 2);
				$thisAverageScore = $indexVar["libreportcard"]->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisAverageScore, 2);
			}
				
			// Input Score Summary row
			$Table_Content .= "<tr id='Tr_score_".$thisStudentID."_Overall'>";
				$Table_Content .= "<td style='vertical-align: middle;'>".$thisAverageScore."</td>";
				$Table_Content .= "<td>".$addButton."</td>";
				$Table_Content .= "<input type='hidden' name='StudentID[]' id='StudentID_".$thisStudentID."' value='".$thisStudentID."'>\r\n";
			$Table_Content .= "</tr>";
		}
		else
		{
			// Empty row
			$Table_Content .= "<tr id='Tr_score_".$thisStudentID."_row1' class='dataRow'>";
				$Table_Content .= "<td class='student_col' rowspan='2'>".$i."</td>";
				$Table_Content .= "<td class='student_col' rowspan='2'>".$thisClassStudent["ClassTitleEn"]."-".$thisClassStudent["ClassNumber"]."</td>";
				$Table_Content .= "<td class='student_col' rowspan='2'>".$thisClassStudent["StudentNameEn"]." (".$thisClassStudent["StudentNameCh"].")</td>";
				$Table_Content .= "<td class='row1_score_col'>".$Lang["eReportCardKG"]["General"]["NoRecord"]."</td>";
				$Table_Content .= "<td class='row1_delete_col'>&nbsp;</td>";
			$Table_Content .= "</tr>";
			
			$Table_Content .= "<tr id='Tr_score_".$thisStudentID."_Overall'>";
				$Table_Content .= "<td style='vertical-align: middle;'>&nbsp;</td>";
				$Table_Content .= "<td>".$addButton."</td>";
				$Table_Content .= "<input type='hidden' name='StudentID[]' id='StudentID_".$thisStudentID."' value='".$thisStudentID."'>\r\n";
			$Table_Content .= "</tr>";
		}
		
		$i++;
	}
}
$Table_Content .= "</tbody>";
$Table_Content .= "</table>";
$htmlAry["timeTableContent"] = $Table_Content;
	    
# Add Subject Topic JS Checking
$jsNumChecking = "";
$jsNumChecking .= " if(this.value > 20) { alert(\"".$Lang["eReportCardKG"]["Management"]["ToolScore"]["MaxAddNewInputScoreRow"]."\"); this.value = 20; return false; } ";
$jsNumChecking .= " else if (this.value <= 0) { alert(\"".$Lang["General"]["JS_warning"]["InputPositiveValue"] ."\"); this.value = 1; return false; } ";

# Add Table row fields
$TableRowInput = "";
if(!$isInputLocked)
{
    $TableRowInput .= $Lang["eReportCardKG"]["Management"]["ToolScore"]["AddNewInputScoreRow"].": ";
    $TableRowInput .= "<input type='number' id='new_row_num' name='new_row_num' min='1' max='20' onKeyUp='".$jsNumChecking."' style='width: 100px;'>&nbsp;";
    $TableRowInput .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_confirm, "button", $ParOnClick="addMultipleRows()", $ParName="new_row_submit", $ParOtherAttribute="", $ParDisabled=0, $ParClass=" ");
    $htmlAry["addTableRowInput"] = $TableRowInput;
}

# Button
if(!$isInputLocked)
{
    $htmlAry["button"] = "";
    $htmlAry["button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_submit, "button", "formSubmit()", "submitBtn", (empty($thisStudentToolsScore) ? "style='display:none'" : ""));
    $htmlAry["button"] .= "&nbsp;&nbsp;";
}
$htmlAry["button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='YearID' id='YearID' value='".$_POST["YearID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='TimeTableID' id='TimeTableID' value='".$_POST["TimeTableID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='TopicID' id='TopicID' value='".$TimeTableInfo["TopicID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value='".$_POST["ClassID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ZoneID' id='ZoneID' value='".$_POST["ZoneID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ToolID' id='ToolID' value='".$_POST["ToolID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='FullMark' id='FullMark' value='5'>\r\n";

### Required JS CSS - Paste Score from Excel
echo $indexVar["libreportcard_ui"]->Include_CopyPaste_JS_CSS("2016");
echo $indexVar["libreportcard_ui"]->Include_Excel_JS_CSS();
?>