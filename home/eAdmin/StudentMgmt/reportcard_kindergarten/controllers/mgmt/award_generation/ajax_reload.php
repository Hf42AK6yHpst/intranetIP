<?php
// Using: 
/*
 *  Date: 2019-10-31 (Bill)
 *          copy action: StudentByClass from mgmt.get_ajax > access group member checking checking in IS_KG_ADMIN_USER()
 *          class teacher > view own class students only
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

$action = ($_POST["Action"])? $_POST["Action"] : $_GET["Action"];
if ($action == "Edit_Award_Student_Table")
{
    $ClassID = $_POST["ClassID"]? $_POST["ClassID"] : $_GET["ClassID"];
    $ClassID = IntegerSafe($ClassID);
    $AwardID = $_POST["AwardID"]? $_POST["AwardID"] : $_GET["AwardID"];
    $AwardID = IntegerSafe($AwardID);
    
    $viewMode = $_POST["viewMode"]? $_POST["viewMode"] : $_GET["viewMode"];
    $targetID = $viewMode == 'CLASS'? $ClassID : $AwardID;
    $filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();
    echo $indexVar["libreportcard_ui"]->Edit_Award_Student_Table($viewMode, $targetID, $filterByClassTeacher);
}
else if ($action == "Reload_Add_Student_Award_Layer")
{
    $StudentID = IntegerSafe($_POST['StudentID']);
    $ClassID = IntegerSafe($_POST['ClassID']);
    echo $indexVar["libreportcard_ui"]->Get_Add_Student_Award_Layer_UI($StudentID, $ClassID);
}
else if ($action == "Reload_Add_Award_Student_Layer")
{
    $AwardID = IntegerSafe($_POST['AwardID']);
    $ClassID = IntegerSafe($_POST['ClassID']);
    echo $indexVar["libreportcard_ui"]->Get_Add_Award_Student_Layer_UI($AwardID, $ClassID);
}
else if($action == "StudentByClass")
{
    # Get Class Student
    $classStudentAry = array();
    if($_POST["ClassID"]) {
        $classStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
    }

    # Get Excluded Student List
    $excludeStudentAry = array();
    if($_POST["ExcludeStudentIDList"] != '') {
        $excludeStudentAry = explode(',', $_POST["ExcludeStudentIDList"]);
    }

    $dataAry = array();
    foreach((array)$classStudentAry as $studentInfo)
    {
        if(!empty($excludeStudentAry) && in_array($studentInfo["UserID"], (array)$excludeStudentAry)){
            continue;
        }

        $studentName = $studentInfo["StudentName"];
        $studentClassName = $studentInfo["ClassName"];
        $studentClassNumber = $studentInfo["ClassNumber"];
        $studentDisplay = $studentName." (".$studentClassName."-".$studentClassNumber.")";

        $dataAry[] = array($studentInfo["UserID"], $studentDisplay);
    }

    $selectTags = " id='".$_POST["SelectionID"]."' name='".$_POST["SelectionName"]."' ".($_POST["isMultiple"] ? "multiple" : "")." size=10 ";
    $studentSelection = getSelectByArray($dataAry, $selectTags, "", $_POST["isAll"], $_POST["noFirst"]);
    if($_POST["withSelectAll"]) {
        $selectAllBtn = $indexVar["libreportcard_ui"]->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('".$_POST["SelectionID"]."', 1);", "selectAllBtn");
    }
    $returnContent = $studentSelection.$selectAllBtn;
    $returnContent .= $indexVar["libreportcard_ui"]->MultiSelectionRemark();

    echo $returnContent;
}

intranet_closedb();
?>