<?php

$viewMode = $_POST['viewMode']? $_POST['viewMode'] : $_GET['viewMode'];
$ClassID = $_POST['ClassID']? $_POST['ClassID'] : $_GET['ClassID'];
$ClassID = IntegerSafe($ClassID);
$AwardID = $_POST['AwardID']? $_POST['AwardID'] : $_GET['AwardID'];
$AwardID = IntegerSafe($AwardID);
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

if(($viewMode != 'CLASS' && $viewMode != 'AWARD') || ($viewMode == 'CLASS' && $ClassID == 0) || ($viewMode == 'AWARD' && $AwardID == 0))
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['SelectAward'], '?task=mgmt.award_generation.index&viewMode='.$viewMode, 1);
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAward'], '?task=mgmt.award_generation.generate', 0);

# View
$VIEWS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'], "javascript:js_Change_View_Mode('AWARD');", $img_src, $viewMode == 'AWARD');
$VIEWS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['Class'], "javascript:js_Change_View_Mode('CLASS');", $img_src, $viewMode == 'CLASS');
$htmlAry["ContentTopBtn"] = $indexVar["libreportcard_ui"]->GET_CONTENT_TOP_BTN($VIEWS_OBJ);

# Info Table
$htmlAry["infoTable"] = '';
if($viewMode == 'CLASS')
{
    $allKGClassArr = $indexVar["libreportcard"]->Get_All_KG_Class();
    $allKGClassArr = BuildMultiKeyAssoc((array)$allKGClassArr, 'YearClassID');
    
    $thisClassInfo = $allKGClassArr[$ClassID];
    $thisClassName = Get_Lang_Selection($thisClassInfo['ClassTitleB5'], $thisClassInfo['ClassTitleEN']);
    $thisLevelName = $indexVar["libreportcard"]->getYearName($thisClassInfo['YearID']);
    
    $htmlAry["infoTable"] .= '<table class="form_table_v30">' . "\n";
    $htmlAry["infoTable"] .= '<tr>' . "\n";
    $htmlAry["infoTable"] .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
    $htmlAry["infoTable"] .= '<td>' . $thisLevelName . '</td>' . "\n";
    $htmlAry["infoTable"] .= '</tr>' . "\n";
    $htmlAry["infoTable"] .= '<tr>' . "\n";
    $htmlAry["infoTable"] .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
    $htmlAry["infoTable"] .= '<td>' . $thisClassName . '</td>' . "\n";
    $htmlAry["infoTable"] .= '</tr>' . "\n";
    $htmlAry["infoTable"] .= '</table>' . "\n";
}
else if($viewMode == 'AWARD')
{
    $thisAwardInfo = $indexVar["libreportcard"]->Get_KG_Award_List($AwardID);
    $thisAwardInfo = $thisAwardInfo[0];
    $thisAwardName = Get_Lang_Selection($thisAwardInfo['AwardNameCh'], $thisAwardInfo['AwardNameEn']);
    
    $htmlAry["infoTable"] .= '<table class="form_table_v30">' . "\n";
    $htmlAry["infoTable"] .= '<tr>' . "\n";
    $htmlAry["infoTable"] .= '<td class="field_title">' . $Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'] . '</td>' . "\n";
    $htmlAry["infoTable"] .= '<td>' . $thisAwardName. '</td>' . "\n";
    $htmlAry["infoTable"] .= '</tr>' . "\n";
    $htmlAry["infoTable"] .= '</table>' . "\n";
}

$x .= '<div id="StudentTableDiv"></div>' . "\n";
$x .= '<br />' . "\n";
$x .= '</div>' . "\n";

# Buttons
$htmlAry["buttonDiv"] = "";
$htmlAry["buttonDiv"] .= '<div class="edit_bottom_v30">' . "\n";
$htmlAry["buttonDiv"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
$htmlAry["buttonDiv"] .= '</div>' . "\n";

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value='$ClassID'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='AwardID' id='AwardID' value='$AwardID'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='viewMode' id='viewMode' value='$viewMode'>\r\n";

$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

echo $indexVar["libreportcard_ui"]->Include_JS_CSS();
?>