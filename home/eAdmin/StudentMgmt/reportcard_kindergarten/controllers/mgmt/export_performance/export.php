<?php 
// Using:
/*
 *  Date: 2019-11-01 (Bill)
 *          show all topics
 * 	Date: 2019-05-30 (Bill)
 * 			added yearly export logic
 */

if((isset($_POST["TermID"]) || isset($_POST["isYearlyResult"])) && isset($_POST["ClassID"]) && isset($_POST["StudentID"]))
{
    include_once($indexVar['thisBasePath']."includes/libexporttext.php");
    $lexport = new libexporttext();
    
    $studentIdAry = $_POST["StudentID"];
    $targetTermId = $_POST["TermID"];
    $targetClassId = $_POST["ClassID"][0];
    
    // Form Level Info
    $allClassInfoArr = $indexVar["libreportcard"]->Get_All_KG_Class();
    $allClassInfoArr = BuildMultiKeyAssoc($allClassInfoArr, "YearClassID", "YearID", 1);
    $targetLevelId = $allClassInfoArr[$targetClassId];
    
    // Semester Info
    //$targetYearId = $indexVar["libreportcard"]->Get_Active_AcademicYearID();
    //$yearSemInfo = getTargetYearSemesterByIDs($targetYearId, $targetTermId, "b5");
    //$SemesterNumber = $indexVar["libreportcard"]->Get_Semester_Seq_Number($targetTermId);
    
    // Macau Category
    $topicMOInfo = $indexVar["libreportcard"]->Get_Macau_Category($returnAssocArr=true);
    $topicMOType = array_values($topicMOInfo);
    $topicMOTypeCode = array_keys($topicMOInfo);
    if(empty($topicMOType)) {
        $topicMOType = array('健康與體育', '語言', '個人、社交與人文', '數學與科學', '藝術');
    }
    if(empty($topicMOTypeCode)) {
        $topicMOTypeCode = array('A', 'B', 'C', 'D', 'E');
    }
    
    // Student Info
    $studentInfoAry = $indexVar["libreportcard"]->Get_Student_By_Class($targetClassId, $studentIdAry, 0, 0, 0, 1);
    $studentIdSize = count((array)$studentIdAry);
    
    if(isset($_POST["TermID"]))
    {
        // Student Cat Summary Scores
        $catSummaryScoreArr = $indexVar["libreportcard"]->GetStudentScoreSummary($studentIdAry, $targetClassId, $targetTermId);
        $catSummaryScoreArr = $catSummaryScoreArr[0];
        
        // Student Topics
        $catTopicsArr = $indexVar["libreportcard"]->getTopics("", "", $targetLevelId, true, $targetTermId, false, true);
        $catTopicsArr = BuildMultiKeyAssoc($catTopicsArr, array("CatTypeName", "TopicCatNameCh", "TopicID"));
        
        // Student Topic Scores
        $catTopicScoreArr = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($studentIdAry, $targetLevelId, "", "", $targetTermId);
        
        $ExportArr = array();
        for($i=0; $i<sizeof($studentIdAry); $i++)
        {
            $SummaryScore1 = "0";
            $SummaryScore2 = "0";
            
            // Student Info
            $studentId = $studentIdAry[$i];
            $studentName = $studentInfoAry[$studentId]["StudentNameCh"] != ''? $studentInfoAry[$studentId]["StudentNameCh"] : $studentInfoAry[$studentId]["StudentNameEn"];
            $studentClassName = $studentInfoAry[$studentId]["ClassTitleCh"] != ''? $studentInfoAry[$studentId]["ClassTitleCh"] : $studentInfoAry[$studentId]["ClassTitleEn"];
            $studentCatAScore = $catSummaryScoreArr[$studentId]["A"];
            $studentCatAScore = $studentCatAScore? $studentCatAScore : "0";
            $studentCatBScore = $catSummaryScoreArr[$studentId]["B"];
            $studentCatBScore = $studentCatBScore? $studentCatBScore : "0";
            $studentCatCScore = $catSummaryScoreArr[$studentId]["C"];
            $studentCatCScore = $studentCatCScore? $studentCatCScore : "0";
            $studentCatDScore = $catSummaryScoreArr[$studentId]["D"];
            $studentCatDScore = $studentCatDScore? $studentCatDScore : "0";
            $studentCatEScore = $catSummaryScoreArr[$studentId]["E"];
            $studentCatEScore = $studentCatEScore? $studentCatEScore : "0";
            
            $ExportArr[$i] = array();
            $ExportArr[$i][] = $studentClassName;
            $ExportArr[$i][] = $studentInfoAry[$studentId]["ClassNumber"];
            $ExportArr[$i][] = $studentName;
            $ExportArr[$i][] = $studentCatAScore;
            $ExportArr[$i][] = $studentCatBScore;
            $ExportArr[$i][] = $studentCatCScore;
            $ExportArr[$i][] = $studentCatDScore;
            $ExportArr[$i][] = $studentCatEScore;
            $SummaryScore1 += $studentCatAScore;
            $SummaryScore1 += $studentCatBScore;
            $SummaryScore1 += $studentCatCScore;
            $SummaryScore1 += $studentCatDScore;
            $SummaryScore1 += $studentCatEScore;
            
            $langTopicArr = $catTopicsArr[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]];
            $langTopicCount = count((array)$langTopicArr);
            if($langTopicCount > 0)
            {
                foreach($langTopicArr as $thisTopicCatType => $thisLangTopicArr)
                {
                    $thisLangTopicScoreTotal = "0";
                    $thisLangTopicCount = count((array)$thisLangTopicArr);
                    if($thisLangTopicCount > 0)
                    {
                        $langTopicTotalCount = 0;
                        foreach((array)$thisLangTopicArr as $thisLangTopicID => $thisTopicInfo)
                        {
                            $thisLangTopicScore = $catTopicScoreArr[$studentId][$thisLangTopicID]["Score"];
                            if($thisLangTopicScore > 0) {
                                $thisLangTopicScore = $thisLangTopicScore ? $thisLangTopicScore : "0";
                                $thisLangTopicScoreTotal += $thisLangTopicScore;
                                $langTopicTotalCount++;
                            }
                        }

                        //$thisLangTopicScoreTotal = $thisLangTopicScoreTotal / $thisLangTopicCount;
                        if($langTopicTotalCount > 0) {
                            $thisLangTopicScoreTotal = $thisLangTopicScoreTotal / $langTopicTotalCount;
                            $thisLangTopicScoreTotal = my_round($thisLangTopicScoreTotal, 2);
                        }
                    }
                    $ExportArr[$i][] = $thisLangTopicScoreTotal;
                    $SummaryScore1 += $thisLangTopicScoreTotal;
                }
            }
            $ExportArr[$i][] = $SummaryScore1;
            
            $behaveTopicArr = $catTopicsArr[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]];
            $behaveTopicCount = count((array)$behaveTopicArr);
            if($behaveTopicCount > 0)
            {
                foreach($behaveTopicArr as $thisTopicCatType => $thisBehaveTopicsAry)
                {
                    $thisBehaveTopicScoreTotal = "0";
                    $thisBehaveTopicsCount = count((array)$thisBehaveTopicsAry);
                    if($thisBehaveTopicsCount > 0)
                    {
                        $behaveTopicTotalCount = 0;
                        foreach((array)$thisBehaveTopicsAry as $thisBehaveTopicID => $thisTopicInfo)
                        {
                            $thisBehaveTopicScore = $catTopicScoreArr[$studentId][$thisBehaveTopicID]["Score"];
                            if($thisBehaveTopicScore > 0) {
                                $thisBehaveTopicScore = $thisBehaveTopicScore ? $thisBehaveTopicScore: "0";
                                $thisBehaveTopicScoreTotal += $thisBehaveTopicScore;
                                $behaveTopicTotalCount++;
                            }
                        }

                        //$thisBehaveTopicScoreTotal = $thisBehaveTopicScoreTotal / $thisBehaveTopicsCount;
                        if($behaveTopicTotalCount > 0) {
                            $thisBehaveTopicScoreTotal = $thisBehaveTopicScoreTotal / $behaveTopicTotalCount;
                            $thisBehaveTopicScoreTotal = my_round($thisBehaveTopicScoreTotal, 2);
                        }
                    }
                    $ExportArr[$i][] = $thisBehaveTopicScoreTotal;
                    $SummaryScore2 += $thisBehaveTopicScoreTotal;
                }
            }
            
            $ExportArr[$i][] = $SummaryScore2;
        }
        
        # Define column title
        $exportColumn = array();
        $exportColumn[] = '班別';
        $exportColumn[] = '學號';
        $exportColumn[] = '學生名稱';
        foreach((array)$topicMOType as $MOType) {
            $exportColumn[] = $MOType;
        }
        $langTopicArr = $catTopicsArr[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]];
        foreach((array)$langTopicArr as $langTopicType => $v) {
            $exportColumn[] = $langTopicType;
        }
        $exportColumn[] = '總分';
        $behaveTopicArr = $catTopicsArr[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]];
        foreach((array)$behaveTopicArr as $behaveTopicType => $v) {
            $exportColumn[] = $behaveTopicType;
        }
        $exportColumn[] = '總分';
    }
    else if(isset($_POST["isYearlyResult"]))
    {
        $dataTypeAry = array(1 => '學習成績及語文能力總分', 2 => '行為指標總分');
        
        $targetTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID(), $ReturnAsso=0, 'b5');
        foreach($targetTermAry as $thisTermIndex => $thisTermData) {
            if($thisTermIndex >= 3) {
                unset($targetTermAry[$thisTermIndex]);
            }
        }
        $targetTermIdAry = Get_Array_By_Key($targetTermAry, 'YearTermID');
        
        // loop terms
        $termCatSummaryScoreArr = array();
        $termCatTopicsArr = array();
        $termCatTopicScoreArr = array();
        foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
        {
            // Student Cat Summary Scores
            $catSummaryScoreArr = $indexVar["libreportcard"]->GetStudentScoreSummary($studentIdAry, $targetClassId, $targetTermId);
            $catSummaryScoreArr = $catSummaryScoreArr[0];
            $termCatSummaryScoreArr[$targetTermId] = $catSummaryScoreArr;
            
            // Student Topics
            $catTopicsArr = $indexVar["libreportcard"]->getTopics("", "", $targetLevelId, true, $targetTermId, false, true);
            $catTopicsArr = BuildMultiKeyAssoc($catTopicsArr, array("CatTypeName", "TopicCatNameCh", "TopicID"));
            $termCatTopicsArr[$targetTermId] = $catTopicsArr;
            
            // Student Topic Scores
            $catTopicScoreArr = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($studentIdAry, $targetLevelId, "", "", $targetTermId);
            $termCatTopicScoreArr[$targetTermId] = $catTopicScoreArr;
        }
        $includedCatTopicArr = array();
        
        $ExportArr = array();
        for($i=0; $i<sizeof($studentIdAry); $i++)
        {
            // Student Info
            $studentId = $studentIdAry[$i];
            $studentName = $studentInfoAry[$studentId]["StudentNameCh"] != '' ? $studentInfoAry[$studentId]["StudentNameCh"] : $studentInfoAry[$studentId]["StudentNameEn"];
            $studentClassName = $studentInfoAry[$studentId]["ClassTitleCh"] != '' ? $studentInfoAry[$studentId]["ClassTitleCh"] : $studentInfoAry[$studentId]["ClassTitleEn"];
            $studentClassNumber = $studentInfoAry[$studentId]["ClassNumber"] != '' ? $studentInfoAry[$studentId]["ClassNumber"] : '';
            
            // loop terms
            $termScoreArr = array();
            $termTotalScoreArr = array();
            foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
            {
                $SummaryScore1 = 0;
                $SummaryScore2 = 0;
                $SummaryScore = 0;
                
                $catSummaryScoreArr = $termCatSummaryScoreArr[$targetTermId];
                $catTopicsArr = $termCatTopicsArr[$targetTermId];
                $catTopicScoreArr = $termCatTopicScoreArr[$targetTermId];
                
                foreach($topicMOTypeCode as $thisTypeCode)
                {
                    $studentCatScore = $catSummaryScoreArr[$studentId][$thisTypeCode];
                    $studentCatScore = $studentCatScore? $studentCatScore : 0;
                    $termScoreArr[1]['MO'][$thisTypeCode][$targetTermId] = $studentCatScore;
                    
                    $SummaryScore1 += $studentCatScore;
                    $SummaryScore += $studentCatScore;
                }
                
                $langTopicArr = $catTopicsArr[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]];
                $langTopicCount = count((array)$langTopicArr);
                if($langTopicCount > 0)
                {
                    foreach($langTopicArr as $thisTopicCatType => $thisLangTopicArr)
                    {
                        $thisTopicScoreTotal = 0;
                        $thisLangTopicCount = count((array)$thisLangTopicArr);
                        if($thisLangTopicCount > 0)
                        {
                            $langTopicTotalCount = 0;
                            foreach((array)$thisLangTopicArr as $thisLangTopicID => $thisTopicInfo)
                            {
                                $thisLangTopicScore = $catTopicScoreArr[$studentId][$thisLangTopicID]["Score"];
                                if($thisLangTopicScore) {
                                    $thisLangTopicScore = $thisLangTopicScore ? $thisLangTopicScore : 0;
                                    $thisTopicScoreTotal += $thisLangTopicScore;
                                    $langTopicTotalCount++;
                                }
                            }

                            //$thisTopicScoreTotal = $thisTopicScoreTotal / $thisLangTopicCount;
                            if($langTopicTotalCount > 0) {
                                $thisTopicScoreTotal = $thisTopicScoreTotal / $langTopicTotalCount;
                                $thisTopicScoreTotal = my_round($thisTopicScoreTotal, 2);
                            }
                        }
                        $termScoreArr[1]['TOPIC'][$thisTopicCatType][$targetTermId] = $thisTopicScoreTotal;
                        
                        $SummaryScore1 += $thisTopicScoreTotal;
                        $SummaryScore += $thisTopicScoreTotal;
                    }
                }
                
                $behaveTopicArr = $catTopicsArr[$Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]];
                $behaveTopicCount = count((array)$behaveTopicArr);
                if($behaveTopicCount > 0)
                {
                    foreach($behaveTopicArr as $thisTopicCatType => $thisBehaveTopicsAry)
                    {
                        $thisTopicScoreTotal = 0;
                        $thisBehaveTopicsCount = count((array)$thisBehaveTopicsAry);
                        if($thisBehaveTopicsCount > 0)
                        {
                            $behaveTopicTotalCount = 0;
                            foreach((array)$thisBehaveTopicsAry as $thisBehaveTopicID => $thisTopicInfo)
                            {
                                $thisBehaveTopicScore = $catTopicScoreArr[$studentId][$thisBehaveTopicID]["Score"];
                                if($thisBehaveTopicScore) {
                                    $thisBehaveTopicScore = $thisBehaveTopicScore ? $thisBehaveTopicScore : 0;
                                    $thisTopicScoreTotal += $thisBehaveTopicScore;
                                    $behaveTopicTotalCount++;
                                }
                            }

                            //$thisTopicScoreTotal = $thisTopicScoreTotal / $thisBehaveTopicsCount;
                            if($behaveTopicTotalCount > 0) {
                                $thisTopicScoreTotal = $thisTopicScoreTotal / $behaveTopicTotalCount;
                                $thisTopicScoreTotal = my_round($thisTopicScoreTotal, 2);
                            }
                        }
                        $termScoreArr[2]['TOPIC'][$thisTopicCatType][$targetTermId] = $thisTopicScoreTotal;
                        
                        $SummaryScore2 += $thisTopicScoreTotal;
                        $SummaryScore += $thisTopicScoreTotal;
                    }
                }
                
                $termSummaryScoreArr[1][$targetTermId] = $SummaryScore1;
                $termSummaryScoreArr[2][$targetTermId] = $SummaryScore2;
                $termSummaryScoreArr[0][$targetTermId] = $SummaryScore;
            }
            
            $ExportArr[$i] = array();
            $ExportArr[$i][] = $studentClassName;
            $ExportArr[$i][] = $studentClassNumber;
            $ExportArr[$i][] = $studentName;
            
            // loop terms
            foreach($dataTypeAry as $dataType => $dataTypeName)
            {
                foreach((array)$termScoreArr[$dataType] as $thisType => $thisTypeScoreArr)
                {
                    foreach((array)$thisTypeScoreArr as $thisTypeCode => $thisTermTypeScoreArr)
                    {
                        $totalTypeScore = 0;
                        foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
                        {
                            $termTermScore = $thisTermTypeScoreArr[$targetTermId]? $thisTermTypeScoreArr[$targetTermId] : 0;
                            $ExportArr[$i][] = $termTermScore;
                            $totalTypeScore += $termTermScore;
                        }
                        $ExportArr[$i][] = $totalTypeScore;
                        
                        if($thisType == 'TOPIC')
                        {
                            $includedCatTopicArr[$dataType][$thisTypeCode] = true;
                        }
                    }
                }
                
                $totalSummaryScore = 0;
                foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
                {
                    $termSummaryScore = $termSummaryScoreArr[$dataType][$targetTermId] ? $termSummaryScoreArr[$dataType][$targetTermId] : 0;
                    $ExportArr[$i][] = $termSummaryScore;
                    $totalSummaryScore += $termSummaryScore;
                }
                $ExportArr[$i][] = $totalSummaryScore;
            }
            
//             $dataType = 0;
//             $totalSummaryScore = 0;
//             foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
//             {
//                 $termSummaryScore = $termSummaryScoreArr[$dataType][$targetTermId]? $termSummaryScoreArr[$dataType][$targetTermId] : 0;
//                 $ExportArr[$i][] = $termSummaryScore;
//                 $totalSummaryScore += $termSummaryScore;
//             }
//             $ExportArr[$i][] = $totalSummaryScore;
        }
        
        # Define column title
        $exportColumn = array();
        $exportColumn[] = '班別';
        $exportColumn[] = '學號';
        $exportColumn[] = '學生名稱';
        
        // loop terms
        foreach($dataTypeAry as $dataType => $dataTypeName)
        {
            if($dataType == 1)
            {
                foreach($topicMOType as $thisMPType)
                {
                    foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
                    {
                        $exportColumn[] = $targetTermAry[$targetTermIndex]['TermName'].$thisMPType;
                    }
                    $exportColumn[] = '全年'.$thisMPType;
                }
            }
            
            $topicArr = $includedCatTopicArr[$dataType];
            $topicCount = count((array)$topicArr);
            if($topicCount > 0)
            {
                foreach($topicArr as $thisTopicName => $thisTopicIncluded)
                {
                    foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
                    {
                        $exportColumn[] = $targetTermAry[$targetTermIndex]['TermName'].$thisTopicName;
                    }
                    $exportColumn[] = '全年'.$thisTopicName;
                }
            }
            
            foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
            {
                // $exportColumn[] = $targetTermAry[$targetTermIndex]['TermName'].$dataTypeName;
                $exportColumn[] = $targetTermAry[$targetTermIndex]['TermName'].'總分';
            }
            // $exportColumn[] = '全年'.$dataTypeName;
            $exportColumn[] = '全年總分';
        }
        
//         foreach($targetTermIdAry as $targetTermIndex => $targetTermId)
//         {
//             $exportColumn[] = $targetTermAry[$targetTermIndex]['TermName'].'總分';
//         }
//         $exportColumn[] = '全年總分';
    }
    
    $export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
    
    intranet_closedb();
    
    # Output the file to user browser
    $filename = 'student_summary.csv';
    $lexport->EXPORT_FILE($filename, $export_content);
}

?>