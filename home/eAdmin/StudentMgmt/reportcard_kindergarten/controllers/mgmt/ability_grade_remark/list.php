<?php
// editing by
/*
 *  Date: 2018-06-19 Philips
 *          Added getFormReportAbilityCode() to reduce the range into using ability target
 *
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_mgmt_abilityGradeRemark_TWCatID","selectTWCat");
$arrCookies[] = array("eRCkg_mgmt_abilityGradeRemark_TWLevel","selectTWLevel");
$arrCookies[] = array("eRCkg_mgmt_abilityGradeRemark_Year","Year");
$arrCookies[] = array("eRCkg_mgmt_abilityGradeRemark_Term","Term");
if(isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
}
else {
    updateGetCookies($arrCookies);
}

// Include DB Table
include_once ($indexVar['thisBasePath'] . "includes/libdbtable.php");
include_once ($indexVar['thisBasePath'] . "includes/libdbtable2007a.php");

// Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

// Get Search Value
if(isset($Year)&&!isset($YearID)){
    $YearID = $Year;
}
if(isset($Term)&&!isset($TermID)){
    $TermID = $Term;
}
$selectYear = $indexVar['libreportcard']->getYearName($YearID);
$searchText = addslashes(trim($searchStr));
$searchTWCat = addslashes(trim($selectTWCat));
$searchTWLevel = addslashes(trim($selectTWLevel));
$searchMOLevel = addslashes(trim($selectMOCat));
$searchYear = addslashes(trim($selectYear));

// Linked to Teaching Tools
$toolAbilityCode = $indexVar['libreportcard']->getFormReportAbilityCode($YearID, $TermID);
//$abilityCode = implode(',', $abilityCode);
// debug_pr($toolAbilityCode);

// Linked to Subjects
$subjectAbilityCode = $indexVar['libreportcard']->getFormReportSubjectAbilityCode($YearID, $TermID);
// debug_pr($subjectAbilityCode);

$abilityCode = array_values(array_unique(array_filter(array_merge((array)$toolAbilityCode, (array)$subjectAbilityCode))));
if(count($abilityCode) > 0)
{
    $abilityCode = implode(",", $abilityCode);
}

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
    $page_size = $ck_page_size;
}
$field = ($field == '') ? 0 : $field;
$order = ($order == '') ? 1 : $order;
$page = ($page == '') ? 1 : $page;
$pos = 0;

### DB table action buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: checkEdit();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = "SELECT
				aitw.Code as TWItemCode,
                ragr.Grade as Grade,
                IF(agra.Remarks IS NULL, agr.Remarks, agra.Remarks) as Remarks,
                IF(agrc.Remarks IS NULL, '--', agrc.Remarks) as RemarksCustom,
                CONCAT('<input type=\'checkbox\' class=\'checkbox\' name=\'AbilityRemarkID[]\' id=\'AbilityRemarkID_', agr.AbilityRemarkID, '\' value=', agr.AbilityRemarkID,'>') as edit_box
			FROM
				".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY aitw
                INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS agr ON agr.CatID = aitw.CatID
                INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADING_RANGE ragr ON ragr.GradingRangeID = agr.GradingRangeID
                LEFT OUTER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS_ALL agra ON agra.AbilityRemarkID = agr.AbilityRemarkID
                LEFT OUTER JOIN (SELECT * FROM ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS_CUSTOM WHERE YearID = '$YearID' AND YearTermID = '$TermID') agrc ON agrc.AbilityRemarkID = agr.AbilityRemarkID
			Where
                IF('$TermID' = '', 0, 1)
                ".((!empty($abilityCode))? "AND aitw.Code IN ($abilityCode)" : " AND 0")
			    .(trim($searchText) != "" ? " AND (aitw.Code LIKE '%$searchText%' OR agr.Remarks LIKE '%$searchText%' OR agra.Remarks LIKE '%$searchText%' OR agrc.Remarks LIKE '%$searchText%') " : "")
                .(trim($searchTWCat) != "" ? " AND aitw.Code LIKE '%$searchTWCat%' " : "")
                .(trim($searchTWLevel) != "" ? " AND aitw.Code LIKE '%$searchTWLevel%'" : "");
$li->IsColOff = "IP25_table";
$li->field_array = array("TWItemCode", "Grade", "Remarks", "RemarksCustom", "edit_box");
$li->fieldorder2 = ", aitw.Code asc, agr.GradingRangeID desc";
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->no_col = count($li->field_array) + 1;

$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='70px'>" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityRemarks']['Grading']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' >" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityRemarks']['Title']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='min-width:150px'>" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityRemarks']['NewComment']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>&nbsp;</td>\n";

// Search Bar
$htmlAry['searchBox'] = $indexVar['libreportcard_ui']->Get_Search_Box_Div('searchStr', stripslashes($searchStr));

// Filter
$htmlAry['typeSelection'] = "";
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_Category_Selection($selectTWCat); // Taiwan Category
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_ClassLevel_Selection($selectTWLevel); // Taiwan Class Level
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);
$htmlAry["typeSelection"] .= $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID);

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='YearName value='$selectYear' />";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='" . $li->pageNo . "'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='" . $li->field . "'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='" . $li->page_size . "'>";
?>