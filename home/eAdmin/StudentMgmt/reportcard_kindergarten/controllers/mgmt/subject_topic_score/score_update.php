<?
// Editing by 
/*
 * 	Date: 2018-08-08 (Bill)
 * 			Create file
 */

if($_POST["ClassID"] && $_POST["ClassLevelID"] && $_POST["SubjectID"] && $_POST["CodeID"] && !empty($_POST["mark"]))
{
	$studentIDAry = array_keys((array)$_POST["mark"]);
	$studentScoreAry = $indexVar["libreportcard"]->GET_STUDENT_SUBJECT_TOPIC_SCORE($studentIDAry, $_POST["CodeID"], "", $_POST["SubjectID"], $_POST["ClassID"]);
	
	$insertDataAry = array();
	$updateDataAry = array();
	foreach($_POST["mark"] as $_studentId => $_studentScore) {
		foreach($_studentScore as $_itemId  => $_topicScore)
		{
			$dataAry = array();
			if(isset($studentScoreAry[$_studentId][$_POST["CodeID"]][$_itemId]))
			{
			    $dataAry["TopicScoreID"] = $studentScoreAry[$_studentId][$_POST["CodeID"]][$_itemId]["TopicScoreID"];
				$dataAry["Score"] = $_topicScore;
				$dataAry["ModifiedBy"] = $UserID;
				$updateDataAry[] = $dataAry;
			}
			else
			{
				$dataAry["StudentID"] = $_studentId;
				$dataAry["CodeID"] = $_POST["CodeID"];
				$dataAry["ClassLevelID"] = $_POST["ClassLevelID"];
				$dataAry["ClassID"] = $_POST["ClassID"];
				$dataAry["SubjectID"] = $_POST["SubjectID"];
				$dataAry["TWItemID"] = $_itemId;
				$dataAry["Score"] = $_topicScore;
				$insertDataAry[] = $dataAry;
			}
		}
	}
	
	// Insert Topic Score
	$success_insert = true;
	if(count($insertDataAry) > 0) {
		$success_insert = $indexVar["libreportcard"]->INSERT_STUDENT_SUBJECT_TOPIC_SCORE($insertDataAry);
	}
	
	// Update Topic Score
	$success_update = true;
	if(count($updateDataAry) > 0) {
		$success_update = $indexVar["libreportcard"]->UPDATE_STUDENT_SUBJECT_TOPIC_SCORE($updateDataAry);
	}
}

$success = $success_insert && $success_update ? "1" : "0";
header("Location:index.php?task=mgmt.subject_topic_score.list&YearID=".$_POST["ClassLevelID"]."&subjectID=".$_POST["SubjectID"]."&success=".$success);
?>