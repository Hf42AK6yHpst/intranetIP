<?php
// Using:
/*
 *  Date: 2019-10-31 Bill
 *          - Create page
 */

if(isset($_POST["ClassID"]) && isset($_POST["SubjectID"]) && isset($_POST["CodeID"]))
{
    include_once($indexVar['thisBasePath']."includes/libexporttext.php");
    $lexport = new libexporttext();

    # Get Class
    $allClassAry = BuildMultiKeyAssoc($indexVar["libreportcard"]->Get_All_KG_Class(), "YearClassID");
    $thisClassInfo = $allClassAry[$_POST["ClassID"]];
    $thisClassName = Get_Lang_Selection($thisClassInfo["ClassTitleB5"], $thisClassInfo["ClassTitleEN"]);

    # Get Subject
    $thisClassLevelID = $thisClassInfo["YearID"];
    $allFormSubjectAry = BuildMultiKeyAssoc($indexVar["libreportcard"]->Get_KG_Form_Subject($thisClassLevelID), 'RecordID');
    $thisSubjectInfo = $allFormSubjectAry[$_POST["SubjectID"]];
    $thisSubjectName = Get_Lang_Selection($thisSubjectInfo["CH_DES"], $thisSubjectInfo["EN_DES"]);

    # Get Subject Mapping
    $currentSubjectMapping = $indexVar["libreportcard"]->getSubjectMapping($_POST['CodeID']);

    # Get Semester
    if($currentSubjectMapping["TermID"] > 0) {
        $allYearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
        $thisTermName = $allYearTermAry[$currentSubjectMapping["TermID"]];
    }

    # Get TimeTable
    if($currentSubjectMapping["TopicID"]) {
        $allTimeTableAry = BuildMultiKeyAssoc($indexVar["libreportcard"]->getYearTopicByYear($thisClassLevelID), 'TopicTimeTableID');
        $thisTimeTableInfo = $allTimeTableAry[$currentSubjectMapping["TopicID"]];
        $thisTimeTableName = Get_Lang_Selection($thisTimeTableInfo["CH_Name"], $thisTimeTableInfo["EN_Name"]);

        if(!$currentSubjectMapping["TermID"]) {
            $startYearTermInfo = getAcademicYearAndYearTermByDate($thisTimeTableInfo['StartDate']);
            $endYearTermInfo = getAcademicYearAndYearTermByDate($thisTimeTableInfo['EndDate']);

            $currentSubjectMapping["TermID"] = array();
            if($indexVar["libreportcard"]->AcademicYearID == $startYearTermInfo['AcademicYearID']) {
                $currentSubjectMapping["TermID"][] = $startYearTermInfo['YearTermID'];
            }
            if($indexVar["libreportcard"]->AcademicYearID == $endYearTermInfo['AcademicYearID']) {
                $currentSubjectMapping["TermID"][] = $endYearTermInfo['YearTermID'];
            }
            $currentSubjectMapping["TermID"] = array_values(array_filter(array_unique($currentSubjectMapping["TermID"])));
        }
    }

    # Get Class Student
    $thisClassStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
    $numOfClassStudent = count($thisClassStudentAry);

    # Get Taiwan Index
    if(!empty($currentSubjectMapping["CataArr"])) {
        $thisTWIndexIds = Get_Array_By_Key((array)$currentSubjectMapping["CataArr"], 'AbilityCatID');
        $thisTWIndexAry = $indexVar["libreportcard"]->Get_Macau_Taiwan_Category_Mapping($thisTWIndexIds, $targetType='TW');
        $thisTWIndexAry = BuildMultiKeyAssoc($thisTWIndexAry, 'TWItemID');
    }
    $numOfTWIndex = count($thisTWIndexAry);

    # Get Student Subject Score
    if($numOfTWIndex > 0) {
        $studentIDAry = Get_Array_By_Key((array)$thisClassStudentAry, "UserID");
        $studentScoreAry = $indexVar["libreportcard"]->GET_STUDENT_SUBJECT_TOPIC_SCORE($studentIDAry, $_POST["CodeID"], $thisTWIndexIds, $_POST["SubjectID"], $_POST["ClassID"]);
    }

    ## Header
    $headerArr = array();
    $headerArr[] = '班別';
    $headerArr[] = '學號';
    $headerArr[] = '學生名稱';
    foreach($thisTWIndexAry as $thisTWIndexInfo) {
        $headerArr[] = '('.$thisTWIndexInfo['TWItemCode'].') '.$thisTWIndexInfo['TWItemName'];
    }
    $headerArr[] = '總分';

    ## Student rows
    $exportArr = array();
    foreach($thisClassStudentAry as $thisClassStudent)
    {
        $dataArr = array();
        $dataArr[] = $thisClassStudent["ClassName"];
        $dataArr[] = $thisClassStudent["ClassNumber"];
        $dataArr[] = $thisClassStudent["StudentName"];

        // loop Ability Index
        $topic_score_total = 0;
        $topic_score_count = 0;
        foreach($thisTWIndexAry as $thisTWIndexInfo)
        {
            $topic_score = $studentScoreAry[$thisClassStudent["UserID"]][$_POST["CodeID"]][$thisTWIndexInfo["TWItemID"]];
            if(isset($topic_score)) {
                $topic_score = $topic_score["Score"];
                $topic_score = $topic_score ? $topic_score : "";
                if($topic_score != "") {
                    $topic_score_total += $topic_score;
                }
            } else {
                $topic_score = "";
            }
            $dataArr[] = $topic_score;

            $topic_score_count++;
        }

        // Calculate total average
        if($topic_score_count > 0) {
            $topic_score_total = $topic_score_total / $topic_score_count;
            $topic_score_total = my_round($topic_score_total, 2);
        }
        $dataArr[] = $topic_score_total;

        $exportArr[] = $dataArr;
    }

    $export_content .= $lexport->GET_EXPORT_TXT($exportArr, $headerArr, "\t", "\r\n", "\t", 0, "11");

    intranet_closedb();

    # Output the file to user browser
    $filename = 'summary.csv';
    $lexport->EXPORT_FILE($filename, $export_content);
}
?>