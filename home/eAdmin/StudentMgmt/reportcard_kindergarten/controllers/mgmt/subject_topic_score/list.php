<?
// Editing by 
/*
 *  Date 2019-10-31 (Bill)
 *          class teacher & subject teacher > view related data only
 *  Date: 2018-08-08 (Bill)
 * 			Create file
 */

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['SubjectIndexScore']['Title']);

### Message
if($success) {
	$returnMsg = $Lang["General"]["ReturnMessage"]["UpdateSuccess"];
} else {
	$returnMsg = "";
}
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

$AllYearIDAry = $indexVar["libreportcard"]->Get_All_KG_Form();
if(!$indexVar["libreportcard"]->IS_KG_ADMIN_USER())
{
    $AllYearIDAry = array();
    if($indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()) {
        $AllYearIDAry = array_merge($AllYearIDAry, $indexVar["libreportcard"]->Get_Teaching_Level($UserID));
    }
    if($indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER()) {
        $AllYearIDAry = array_merge($AllYearIDAry, $indexVar["libreportcard"]->Get_Teaching_Subject_Group_Related_Class($_SESSION["UserID"], true));
    }
}

### Year Selection
$YearID = isset($_POST["YearID"])? $_POST["YearID"] : $_GET["YearID"];
$YearID = $YearID? $YearID : $AllYearIDAry[0]['YearID'];

// if($indexVar["libreportcard"]->IS_KG_ADMIN_USER() || $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()){
$filterByClassTeacherWithSubjects = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER();
$htmlAry["yearSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($YearID, $onChange="document.form1.submit()", $allKGOptions=false, $noFirstTitle=true, 0, $filterByClassTeacherWithSubjects);

### Subject Selection
$SubjectID = isset($_POST["subjectID"])? $_POST["subjectID"] : $_GET["subjectID"];
$htmlAry["subjectSelection"] = $indexVar["libreportcard_ui"]->Get_Subject_Selection($YearID, $onChange="document.form1.submit()", $SubjectID, $noFirstTitle=false, $firstTitleType=1, $excludeBehaveLang=true);

// for selected Class Level
if($YearID > 0)
{
    if($SubjectID > 0)
    {
        # Get Form Subject settings
        $currentFormSubjectMap = $indexVar["libreportcard"]->getAllSubjectMapping($isDeleted=true, $CodeID='', $YearID, $SubjectID);
        $currentFormSubjectMap = $currentFormSubjectMap[0];
        if(!empty($currentFormSubjectMap))
        {
            $isUseTermSettings = $currentFormSubjectMap['isTerm'] == 1;
            if($isUseTermSettings) {
                ### Term Selection
                $TermID = isset($_POST["TermID"])? $_POST["TermID"] : $_GET["TermID"];
                $htmlAry["termSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID, $onChange="document.form1.submit()", $allYearOption=false, $noFirstTitle=false, $firstTitleType=1);
            } else {
                ### Timetable Selection
                $TimeTableID = isset($_POST["TimeTableID"])? $_POST["TimeTableID"] : $_GET["TimeTableID"];
                $htmlAry["timeTableSelection"] = $indexVar["libreportcard_ui"]->Get_Timetable_Selection($TimeTableID, $onChange='document.form1.submit()', $noFirstTitle=true, $YearID);
            }
        }
    }
    
    ### Class Subject DB table
    $htmlAry["dbTableContent"] = $indexVar["libreportcard_ui"]->Get_Form_Subject_List_Table($YearID, $SubjectID, $TermID, $TimeTableID);
}

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='SubjectID' id='SubjectID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='CodeID' id='CodeID' value=''>\r\n";
?>