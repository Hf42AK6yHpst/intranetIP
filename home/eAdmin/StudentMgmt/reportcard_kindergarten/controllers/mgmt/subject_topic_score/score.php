<?
// Editing by 
/*
 *  Date: 2019-10-31 Bill
 *          - Add export function
 *
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * 	Date: 2019-05-28 (Bill)
 * 			always display ability targets' column
 * 
 *  Date: 2019-02-08 (Philips)
 *          Added Input Lock with checking Input Score Period
 * 
 *  Date: 2018-08-08 (Bill)
 * 			Create file
 */

## Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['SubjectIndexScore']['Title']);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

# Navigation
$NavigationAry[] = array($Lang["eReportCardKG"]["Management"]["SubjectIndexScore"]["Title"], "javascript:goBack();");
$NavigationAry[] = array($Lang['eReportCardKG']['Management']['SubjectIndexScore']['InputScore']);
$htmlAry["Navigation"] = $indexVar["libreportcard_ui"]->GET_NAVIGATION_IP25($NavigationAry);

$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmScoreUpdate']);

// Valid Data
if($_POST["ClassID"] && $_POST["SubjectID"] && $_POST["CodeID"])
{
    # Get Subject Mapping
    $currentSubjectMapping = $indexVar["libreportcard"]->getSubjectMapping($_POST['CodeID']);
    
	# Get Class
	$allClassAry = BuildMultiKeyAssoc($indexVar["libreportcard"]->Get_All_KG_Class(), "YearClassID");
	$thisClassInfo = $allClassAry[$_POST["ClassID"]];
	$thisClassName = Get_Lang_Selection($thisClassInfo["ClassTitleB5"], $thisClassInfo["ClassTitleEN"]);
	$htmlAry["classNameDisplay"] = $thisClassName;
	
	# Get Subject
	$thisClassLevelID = $thisClassInfo["YearID"];
	$allFormSubjectAry = BuildMultiKeyAssoc($indexVar["libreportcard"]->Get_KG_Form_Subject($thisClassLevelID), 'RecordID');
	$thisSubjectInfo = $allFormSubjectAry[$_POST["SubjectID"]];
	$thisSubjectName = Get_Lang_Selection($thisSubjectInfo["CH_DES"], $thisSubjectInfo["EN_DES"]);
	$htmlAry["subjectNameDisplay"] = $thisSubjectName;
	
	# Get Semester
	if($currentSubjectMapping["TermID"] > 0) {
    	$allYearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
    	$thisTermName = $allYearTermAry[$currentSubjectMapping["TermID"]];
    	$htmlAry["termNameDisplay"] = $thisTermName;
	}
	
	# Get TimeTable
	if($currentSubjectMapping["TopicID"]) {
	    $allTimeTableAry = BuildMultiKeyAssoc($indexVar["libreportcard"]->getYearTopicByYear($thisClassLevelID), 'TopicTimeTableID');
	    $thisTimeTableInfo = $allTimeTableAry[$currentSubjectMapping["TopicID"]];
	    $thisTimeTableName = Get_Lang_Selection($thisTimeTableInfo["CH_Name"], $thisTimeTableInfo["EN_Name"]);
	    $htmlAry["timeTableNameDisplay"] = $thisTimeTableName;
	    
	    if(!$currentSubjectMapping["TermID"]) {
	        $startYearTermInfo = getAcademicYearAndYearTermByDate($thisTimeTableInfo['StartDate']);
	        $endYearTermInfo = getAcademicYearAndYearTermByDate($thisTimeTableInfo['EndDate']);
	        
	        $currentSubjectMapping["TermID"] = array();
	        if($indexVar["libreportcard"]->AcademicYearID == $startYearTermInfo['AcademicYearID']) {
	           $currentSubjectMapping["TermID"][] = $startYearTermInfo['YearTermID'];
	        }
	        if($indexVar["libreportcard"]->AcademicYearID == $endYearTermInfo['AcademicYearID']) {
	           $currentSubjectMapping["TermID"][] = $endYearTermInfo['YearTermID'];
	        }
	        $currentSubjectMapping["TermID"] = array_values(array_filter(array_unique($currentSubjectMapping["TermID"])));
	    }
	}
	
	# Get Class Student
	$thisClassStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);
	$numOfClassStudent = count($thisClassStudentAry);
	
	# Get Taiwan Index
	if(!empty($currentSubjectMapping["CataArr"])) {
	    $thisTWIndexIds = Get_Array_By_Key((array)$currentSubjectMapping["CataArr"], 'AbilityCatID');
	    $thisTWIndexAry = $indexVar["libreportcard"]->Get_Macau_Taiwan_Category_Mapping($thisTWIndexIds, $targetType='TW');
	    $thisTWIndexAry = BuildMultiKeyAssoc($thisTWIndexAry, 'TWItemID');
	}
	$numOfTWIndex = count($thisTWIndexAry);
	
	# Get Student Subject Score
	if($numOfTWIndex > 0) {
    	$studentIDAry = Get_Array_By_Key((array)$thisClassStudentAry, "UserID");
    	$studentScoreAry = $indexVar["libreportcard"]->GET_STUDENT_SUBJECT_TOPIC_SCORE($studentIDAry, $_POST["CodeID"], $thisTWIndexIds, $_POST["SubjectID"], $_POST["ClassID"]);
	}
}
// $isInputLocked = true;
$isInputLocked = $indexVar['libreportcard']->isInputScorePeriod($currentSubjectMapping["TermID"]) ? false : true;
$disabled = $isInputLocked? " disabled " : "";

# Form Subject Score Table
$tableContent = "";
$tableContent .= "<div class='score_table_outter'>";
$tableContent .= "<table class='score_table' width='100%' cellpadding='4' cellspacing='0'>";
	$tableContent .= "<tr>";
if(empty($thisClassStudentAry))
{
		$tableContent .= "<td width='100%' align='center' class='tablegreentop tabletopnolink'>&nbsp;</td>";
}
else
{
	// Table Column Style
	$_column_count = $numOfClassStudent ? $numOfClassStudent : 1;
	$_td_width = 75 / $_column_count;
	$_td_width = (int)$_td_width."%";
	
    	$tableContent .= "<td align='center' class='tablegreentop tabletopnolink static_display_col'>&nbsp;</td>";
    	$tableContent .= "<td align='center' class='tablegreentop tabletopnolink static_based_col'>&nbsp;</td>";
    	foreach($thisClassStudentAry as $thisClassStudent)
    	{
    		$thisStudentName = $thisClassStudent["StudentName"]." (".$thisClassStudent["ClassName"]." - ".$thisClassStudent["ClassNumber"].")";
    		$tableContent .= "<td align='center' class='tablegreentop tabletopnolink'>".$thisStudentName."</td>";
    	}
}
	$tableContent .= "</tr>";

if(empty($thisTWIndexAry))
{
	// No Record
	$tableContent .= "<tr>";
		$tableContent .= "<td align='center' colspan='".($numOfClassStudent + 1)."'>";
			$tableContent .= $Lang["eReportCardKG"]["General"]["NoRecord"] ;
		$tableContent .= "</td>";
	$tableContent .= "</tr>";
}
else
{
	$tr_count = 0;
	foreach($thisTWIndexAry as $thisTWIndexInfo)
	{
		$_tr_css = ($tr_count % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
		$_td_css = ($tr_count % 2 == 1) ? "attendanceouting" : "attendancepresent";
		$_indexName = '('.$thisTWIndexInfo['TWItemCode'].') '.$thisTWIndexInfo['TWItemName'];
		
		$tableContent .= "<tr class='".$_tr_css."'>";
		$tableContent .= "<td align='left' class='".$_td_css." tabletext static_display_col'>".$_indexName."</td>";
		$tableContent .= "<td align='left' class='".$_td_css." tabletext static_based_col'>".$_indexName."</td>";
		
		$td_count = 0;
		foreach($thisClassStudentAry as $thisClassStudent)
		{
		    $mark_value = $studentScoreAry[$thisClassStudent["UserID"]][$_POST["CodeID"]][$thisTWIndexInfo["TWItemID"]];
		    if(isset($mark_value)) {
		    	$mark_value = $mark_value["Score"];
		    	$mark_value = $mark_value ? $mark_value : "";
		    }
		    else {
		    	$mark_value = "";
		    }
		    $mark_id = "mark[$tr_count][$td_count]";
		    $mark_name = "mark[".$thisClassStudent["UserID"]."][".$thisTWIndexInfo["TWItemID"]."]";
		    $mark_tag = " id='".$mark_id."' name='".$mark_name."' ".$disabled ." class='tabletexttoptext' value='".$mark_value."' ";
		    
			$InputMethod = "<input type='number' class='textboxnum' ".$mark_tag." />";
	  		$tableContent .= "<td align='center' class='".$_td_css." tabletext'>".$InputMethod."</td>";
	  		
	  		$td_count++;
		}
		$tableContent .= "</tr>";
		
		$tr_count++;
	}
}
$tableContent .= "</table>";
$tableContent .= "</div>";
$htmlAry["scoreTableContent"] = $tableContent;

### content tool
$btnAry = array();
$btnAry[] = array('export', 'javascript: goExport();', $Lang['Btn']['Export']);
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# Button
$htmlAry["Button"] = "";
if(!$isInputLocked && $numOfClassStudent > 0 && $numOfTWIndex > 0)
{
	$htmlAry["Button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_submit, "button", "formSubmit()", "backBtn");
	$htmlAry["Button"] .= "&nbsp;&nbsp;";
}
$htmlAry["Button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value='".$_POST["ClassID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='SubjectID' id='SubjectID' value='".$_POST["SubjectID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='CodeID' id='CodeID' value='".$_POST["CodeID"]."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassLevelID' id='ClassLevelID' value='".$thisClassLevelID."'>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='FullMark' id='FullMark' value='5'>\r\n";

### Required JS CSS - Paste Score from Excel
echo $indexVar["libreportcard_ui"]->Include_CopyPaste_JS_CSS("2016");
echo $indexVar["libreportcard_ui"]->Include_Excel_JS_CSS();
?>