<?
// Editing by
/*
 * 	Date: 2018-03-14 (Bill)     [2018-0202-1046-39164]
 *          Create File (Copy logic from eReportCard)
 */

if($_POST["TermID"] && $_POST['ClassID'][0] && !empty($_POST["OtherInfoArr"]) && !empty($_POST["OtherInfoArr"][$_POST['ClassID'][0]]))
{
    $OtherInfoArr = $_POST["OtherInfoArr"];
    foreach((array)$OtherInfoArr as $thisClassID => $thisClassOtherInfoArr)
    {
        foreach((array)$thisClassOtherInfoArr as $thisStudentID => $thisStudentOtherInfoArr)
        {
            foreach((array)$thisStudentOtherInfoArr as $thisOtherInfoCode => $thisValue)
            {
                $OtherInfoArr[$thisClassID][$thisStudentID][stripslashes($thisOtherInfoCode)] = stripslashes($thisValue);
            }
        }
    }
    
    $success = $indexVar["libreportcard"]->updateStudentOtherInfoData($_POST["TermID"], $_POST["UploadType"], $OtherInfoArr);
}

$success = $success ? "1" : "0";
header("Location:index.php?task=mgmt.other_info.index&TermID=".$_POST["TermID"]."&UploadType=".$_POST["UploadType"]."&YearID=".$_POST["Year"]."&success=".$success);
?>