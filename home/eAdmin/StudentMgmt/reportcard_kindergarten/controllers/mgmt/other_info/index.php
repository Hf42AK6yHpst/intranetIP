<?php
// Using: 
/*
 *  Date: 2018-08-14 (Bill)
 *          Support multiple upload type
 *          
 * 	Date: 2018-03-14 (Bill)     [2018-0202-1046-39164]
 *          Create File (Copy logic from eReportCard)
 */

### Cookies setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_Mgmt_OtherInfo_YearID", "YearID");
$arrCookies[] = array("eRCkg_Mgmt_OtherInfo_TermID", "TermID");
if(isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

### Page Title
$UploadType = $_POST['UploadType'] ? $_POST['UploadType'] : ($_GET['UploadType'] ? $_GET['UploadType'] : $UploadType);
$UploadType = $UploadType == ''? 'summary' : $UploadType;
$TAGS_OBJ = $indexVar['libreportcard']->getOtherInfoTabObjArr($UploadType);

if($success) {
    $returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
} else {
    $returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

$btnAry = array();
$btnAry[] = array('import', 'javascript: goImport();');
// $btnAry[] = array('export', 'javascript: goExport();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

# Year Selection
$YearID = $_POST['YearID'] ? $_POST['YearID'] : ($_GET['YearID'] ? $_GET['YearID'] : $YearID);
$htmlAry["YearSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($YearID, $Onchange="reloadOtherInfoTableContent()");

# Semester Selection
$TermID = $_POST['TermID'] ? $_POST['TermID'] : ($_GET['TermID'] ? $_GET['TermID'] : $TermID);
if($TermID == '' && ($indexVar["libreportcard"]->AcademicYearID == Get_Current_Academic_Year_ID())) {
    $TermID = getCurrentSemesterID();
}
$htmlAry["TermSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID, $Onchange="reloadOtherInfoTableContent()", $allYearOption=false, $noFirstOption=true);

// not support import
// $isInputLocked = true;
// if($isInputLocked) {
	$htmlAry['contentTool'] = '';
// }

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='UploadType' id='UploadType' value='$UploadType'>\r\n";
?>