<?php
// Using:
/*
 * 	Date: 2018-03-14 (Bill)     [2018-0202-1046-39164]
 *          Create File (Copy logic from eReportCard)
 */

switch ($Task)
{
	case "Reload_Other_Info_Table":
	    echo $indexVar['libreportcard_ui']->Get_Other_Info_Class_Table($UploadType, $TermID, $YearID);
	break;
	
	case "Check_Other_Info_Data_Exist":
		if (is_numeric($YearClassID)) {
			$YearID = $YearClassID;
		} else {
			$ClassID = substr($YearClassID, 2);
		}
		echo count($indexVar['libreportcard']->getStudentOtherInfoData($UploadType, $YearTermID, $YearClassID, NULL, NULL, $YearID)) > 0? 1 : 0;
	break;
}

?>