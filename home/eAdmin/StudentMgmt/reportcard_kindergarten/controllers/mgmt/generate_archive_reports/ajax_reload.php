<?php
// Using:
/*
 */

$action = ($_POST["Action"])? $_POST["Action"] : $_GET["Action"];
if ($action == "Reload_Class_List")
{
    $YearID = IntegerSafe($_POST["YearID"]? $_POST["YearID"] : $_GET["YearID"]);
    echo $indexVar["libreportcard_ui"]->Get_Class_Selection('', $onChange='js_Reload_Archive_Status()', $withYearOptGroup=false, $noFirstTitle=false, $YearID, $selectionName='ClassID', $adminFilterByYearID=true);
}
else if ($action == "Reload_Archive_Date")
{
    $TermID = IntegerSafe($_POST["TermID"]? $_POST["TermID"] : $_GET["TermID"]);
    $YearID = IntegerSafe($_POST["YearID"]? $_POST["YearID"] : $_GET["YearID"]);
    $ClassID = IntegerSafe($_POST["ClassID"]? $_POST["ClassID"] : $_GET["ClassID"]);

    if($TermID && $YearID && $ClassID) {
        $LastArchiveInfo = $indexVar["libreportcard"]->GET_LASTEST_ARCHIVE_REPORT_INFO($YearID, $ClassID, $TermID);
        $LastArchiveInfo = $LastArchiveInfo[0];
    }
    echo $LastArchiveInfo != '' ? $LastArchiveInfo : $indexVar['emptySymbol'];
}

intranet_closedb();
?>