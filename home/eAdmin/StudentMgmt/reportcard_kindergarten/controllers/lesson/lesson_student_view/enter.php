<?php
// editing by 
/*
 * 	Date: 2017-10-18 (Bill)
 * 			Support Admin to access all classes
 */

# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("student");

# Get GET / POST Value
$classlevel_input = $_POST["classlevel_input"]? $_POST["classlevel_input"] : $_GET["classlevel_input"];
$class_input = $_POST["class_input"]? $_POST["class_input"] : $_GET["class_input"];
$timetable_input = $_POST["timetable_input"]? $_POST["timetable_input"] : $_GET["timetable_input"];
$topic_input = $_POST["topic_input"]? $_POST["topic_input"] : $_GET["topic_input"];
$zone_input = $_POST["zone_input"]? $_POST["zone_input"] : $_GET["zone_input"];
$student_input = $_POST["student_input"]? $_POST["student_input"] : $_GET["student_input"];

# Data Checking
if(empty($classlevel_input) || empty($class_input) || empty($timetable_input) || empty($topic_input) || empty($zone_input) || empty($student_input)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Get Class, Topic and Zone Info
$thisClassInfo = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, '', '', $class_input);
$thisTopicInfo = $indexVar['libreportcard']->Get_Form_Topic_Record($topic_input);
$thisZoneInfo = $indexVar['libreportcard']->Get_Learning_Zone_Record($zone_input);

# Admin can access all classes - Get Target Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisClassInfo))
{
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
	$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
	$targetClass = $AccessibleClasses[$class_input];
	
	$thisClassInfo = array();
	$thisClassInfo[0] = $targetClass;
}

# Get selected Student
$ClassStudentList = $indexVar['libreportcard']->Get_Student_By_Class($class_input, $student_input);
$thisClassStudent = $ClassStudentList[0];

# Data Checking
if(empty($thisClassInfo) || empty($thisTopicInfo) || empty($thisZoneInfo) || empty($ClassStudentList) || empty($thisClassStudent)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Get Student Info
$this_studentid = $thisClassStudent["UserID"];
$this_studentName = $thisClassStudent["StudentName"];
$this_studentUserLogin = $thisClassStudent["UserLogin"];

# Check Student in Zone
$StudentInZoneAry = $indexVar['libreportcard']->GET_STUDENT_IN_ZONE((array)$this_studentid, $classlevel_input, $timetable_input, $topic_input, $zone_input, "", 1);
$isStudentInZone = !empty($StudentInZoneAry) && !empty($StudentInZoneAry[$this_studentid]);

# Student Info Display
$this_studentPhoto = $indexVar['libreportcard']->Get_Student_Display_Photo($thisClassStudent);
$this_studentPhotoStyle = !$isStudentInZone? "photo_up_teacher" : "photo_up";
$htmlAry['studentDisplay'] = $indexVar['libreportcard_ui']->Get_Class_Student_Photo($this_studentid, $this_studentName, $this_studentPhoto, $this_studentPhotoStyle, "left:50px; top:4px;");

# Title
$htmlAry['TitleInfo'] = $indexVar['libreportcard_ui']->Get_Device_Title_Info($thisZoneInfo[0]["ZoneNameB5"], $thisTopicInfo[0]["TopicNameB5"], $thisClassInfo[0]["ClassName"]);

# Button
if(!$isStudentInZone)
	$htmlAry['submitBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("enter", "", "updateEnterStatus()");
if($isStudentInZone)
	$htmlAry['exitBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("exit", "", "selectTools()");
$htmlAry['changeBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("change", "", "backToStudentList()");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$class_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$topic_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='zone_input' id='zone_input' value='".$zone_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='student_input' id='student_input' value='".$student_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='timetable_input' id='timetable_input' value='".$timetable_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='classlevel_input' id='classlevel_input' value='".$classlevel_input."'>\r\n";

?>