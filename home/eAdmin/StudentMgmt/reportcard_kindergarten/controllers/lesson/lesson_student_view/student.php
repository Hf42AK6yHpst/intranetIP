<?php
// editing by 
/*
 *  Date: 2018-03-22 (Bill)     [2018-0202-1046-39164]
 *          Support Class Zone Quota Settings
 * 	Date: 2017-11-17 (Bill)
 * 			Support Specific Zone Quota Settings
 * 	Date: 2017-10-18 (Bill)
 * 			Support Admin to access all classes
 */
 
# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("student");

# Get GET / POST Value
$classlevel_input = $_POST["classlevel_input"]? $_POST["classlevel_input"] : $_GET["classlevel_input"];
$class_input = $_POST["class_input"]? $_POST["class_input"] : $_GET["class_input"];
$timetable_input = $_POST["timetable_input"]? $_POST["timetable_input"] : $_GET["timetable_input"];
$topic_input = $_POST["topic_input"]? $_POST["topic_input"] : $_GET["topic_input"];
$zone_input = $_POST["zone_input"]? $_POST["zone_input"] : $_GET["zone_input"];

# Data Checking
if(empty($class_input) || empty($topic_input) || empty($zone_input)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Get Class, Topic and Zone Info
$thisClassInfo = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, '', '', $class_input);
$thisTopicInfo = $indexVar['libreportcard']->Get_Form_Topic_Record($topic_input);
$thisZoneInfo = $indexVar['libreportcard']->Get_Learning_Zone_Record($zone_input);

# Admin can access all classes - Get Target Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisClassInfo[0]))
{
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
	$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
	$targetClass = $AccessibleClasses[$class_input];
	
	$thisClassInfo = array();
	$thisClassInfo[0] = $targetClass;
}

# Data Checking
if(empty($thisClassInfo[0]) || empty($thisTopicInfo[0]) || empty($thisZoneInfo[0])) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Get Timetable Info
$thisTimeTableInfo = $indexVar['libreportcard']->GetTimeTable($thisClassInfo[0]["ClassLevelID"], "", 1);
$thisTimeTableInfo = BuildMultiKeyAssoc((array)$thisTimeTableInfo, array("TopicID", "ZoneID"));
$thisTopicRelatedZone = $thisTimeTableInfo[$topic_input][$zone_input];
if(!empty($thisTopicRelatedZone)) {
	$thisTimetableID = $thisTopicRelatedZone["TopicTimeTableID"];
}
else {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Zone Quota
$ZoneQuota = $thisTopicRelatedZone["ZoneQuota"];
$ZoneQuota = $ZoneQuota ? $ZoneQuota : $thisZoneInfo[0]["ZoneQuota"];

# Class Zone Quota
$class_zone_quota = $indexVar["libreportcard"]->GetClassZoneQuotaFromMapping($thisTimetableID, $zone_input, $class_input);
if(!empty($class_zone_quota[0]))
{
    $ZoneQuota = $class_zone_quota[0]['ClassZoneQuota'] ? $class_zone_quota[0]['ClassZoneQuota'] : $ZoneQuota;
}

# Get Class Student
$ClassStudentList = $indexVar['libreportcard']->Get_Student_By_Class($class_input);

# Student Selection
//$htmlAry['studentDisplay'] = $indexVar['libreportcard_ui']->Get_Class_Student_Selection_Area($ClassStudentList, $thisClassInfo[0]["ClassLevelID"], $thisTimetableID, $topic_input, $zone_input, "all", "all", "130", "left:35px; top:4px;", $thisZoneInfo[0]["ZoneQuota"]);
$htmlAry['studentDisplay'] = $indexVar['libreportcard_ui']->Get_Class_Student_Selection_Area($ClassStudentList, $thisClassInfo[0]["ClassLevelID"], $thisTimetableID, $topic_input, $zone_input, "all", "all", "130", "left:35px; top:4px;", $ZoneQuota);

# Title
$htmlAry['TitleInfo'] = $indexVar['libreportcard_ui']->Get_Device_Title_Info($thisZoneInfo[0]["ZoneNameB5"], $thisTopicInfo[0]["TopicNameB5"], $thisClassInfo[0]["ClassName"]);

# Button
$htmlAry['backBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("back", "index.php?task=lesson.lesson_student_view.index");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='classlevel_input' id='classlevel_input' value='".$thisClassInfo[0]["ClassLevelID"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$class_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='timetable_input' id='timetable_input' value='".$thisTimetableID."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$topic_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='zone_input' id='zone_input' value='".$zone_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='student_input' id='student_input' value=''>\r\n";
?>