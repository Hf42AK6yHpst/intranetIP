<?php

$LogInfoArr = array();
$LogInfoArr[0] = array();
$LogInfoArr[0]["StudentID"] = $_POST["student_input"];
$LogInfoArr[0]["ClassLevelID"] = $_POST["classlevel_input"];
$LogInfoArr[0]["TimeTableID"] = $_POST["timetable_input"];
$LogInfoArr[0]["TopicID"] = $_POST["topic_input"];
$LogInfoArr[0]["ZoneID"] = $_POST["zone_input"];
$LogInfoArr[0]["ToolID"] = $_POST["tool_input"];

if($_POST["submit_flag"]==1) {
	# Student Leave zone
	$indexVar['libreportcard']->INSERT_STUDENT_LEAVE_LOG($LogInfoArr);
}
else {
	# Student Enter Zone
	$indexVar['libreportcard']->INSERT_STUDENT_ENTRY_LOG($LogInfoArr);
}

header("Location: index.php?task=lesson.lesson_student_view.student&class_input=".$_POST["class_input"]."&topic_input=".$_POST["topic_input"]."&zone_input=".$_POST["zone_input"]."&student_input=".$_POST["student_input"]."&timetable_input=".$_POST["timetable_input"]."&classlevel_input=".$_POST["classlevel_input"]);

?>