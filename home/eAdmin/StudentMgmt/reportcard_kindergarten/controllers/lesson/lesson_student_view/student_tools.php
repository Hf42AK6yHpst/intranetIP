<?php
// editing by 
/*
 * 	Date: 2018-05-02 Bill	Support Incomplete option
 * 	Date: 2017-10-18 Bill	Support Admin to access all classes
 * 	Date: 2017-10-09 Bill  	Use timestamp to prevent display cached images	[2017-0929-1827-43096]
 */

# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("student");

# Data Checking
if(empty($_POST["classlevel_input"]) || empty($_POST["class_input"]) || empty($_POST["timetable_input"]) || empty($_POST["topic_input"]) || empty($_POST["zone_input"]) || empty($_POST["student_input"])) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Get Class, Topic and Zone Info
$thisClassInfo = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, '', '', $_POST["class_input"]);
$thisTopicInfo = $indexVar['libreportcard']->Get_Form_Topic_Record($_POST["topic_input"]);
$thisZoneInfo = $indexVar['libreportcard']->Get_Learning_Zone_Record($_POST["zone_input"]);

# Admin can access all classes - Get Target Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisClassInfo))
{
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
	$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
	$targetClass = $AccessibleClasses[$_POST["class_input"]];
	
	$thisClassInfo = array();
	$thisClassInfo[0] = $targetClass;
}

# Get selected Student
$ClassStudentList = $indexVar['libreportcard']->Get_Student_By_Class($_POST["class_input"], $_POST["student_input"]);
$thisClassStudent = $ClassStudentList[0];

# Data Checking
if(empty($thisClassInfo) || empty($thisTopicInfo) || empty($thisZoneInfo) || empty($ClassStudentList) || empty($thisClassStudent)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_student_view.index");
}

# Get Teaching Tools Info
$thisTimeTableInfo = $indexVar['libreportcard']->GetTimeTable($thisClassInfo[0]["ClassLevelID"], "", 1);
$thisTimeTableInfo = BuildMultiKeyAssoc((array)$thisTimeTableInfo, array("TopicID", "ZoneID", "ToolCodeID"));
$thisTeachingTools = $thisTimeTableInfo[$_POST["topic_input"]][$_POST["zone_input"]];
$withTeachingTool = count((array)$thisTeachingTools) > 0;
if($withTeachingTool) {
	$withTeachingTool = false;
	foreach($thisTeachingTools as $thisToolCodeID => $currentTeacherTool) {
		if(!empty($thisToolCodeID)) {
			$withTeachingTool = true;
			break;
		}
	}
}

# Teaching Tool Selection
$htmlAry['teachingToolDisplay'] = "";
if($withTeachingTool)
{
	foreach($thisTeachingTools as $thisToolCodeID => $currentTeacherTool)
	{
		# Get Teaching Tool
		$currentTeacherTool = $indexVar['libreportcard']->Get_Equipment_Record($thisClassInfo[0]["ClassLevelID"], $thisToolCodeID);
		$currentTeacherTool = $currentTeacherTool[0];
		if(empty($currentTeacherTool))	continue;
		
		$htmlAry['teachingToolDisplay'] .= "<li class='tool-con'>\r\n";
		$htmlAry['teachingToolDisplay'] .= "	<div class='bg'></div>\r\n";
		$htmlAry['teachingToolDisplay'] .= "	<div class='photo'><img src='".$indexVar['thisImage'].$currentTeacherTool["PhotoPath"]."?t=".time()."'/></div>\r\n";
		$htmlAry['teachingToolDisplay'] .= "	<div class='check-box'>\r\n";
		$htmlAry['teachingToolDisplay'] .= "		<button class='check-btn $checkboxStyle' id='teaching_tool_".$thisToolCodeID."'>\r\n";
		$htmlAry['teachingToolDisplay'] .= "			<div>&nbsp;</div>\r\n";
		$htmlAry['teachingToolDisplay'] .= "		</button>\r\n";
		$htmlAry['teachingToolDisplay'] .= "	</div>\r\n";
		$htmlAry['teachingToolDisplay'] .= "</li>\r\n";
	}
	
	$htmlAry['teachingToolDisplay'] .= "<li class='tool-con'>\r\n";
	$htmlAry['teachingToolDisplay'] .= "	<div class='bg'></div>\r\n";
	$htmlAry['teachingToolDisplay'] .= "	<div class='photo'><img src='".$ercKindergartenConfig['imageFilePath']."/not_complete_tool.png'/></div>\r\n";
	$htmlAry['teachingToolDisplay'] .= "	<div class='check-box'>\r\n";
	$htmlAry['teachingToolDisplay'] .= "		<button class='check-btn $checkboxStyle' id='teaching_tool_0'>\r\n";
	$htmlAry['teachingToolDisplay'] .= "			<div>&nbsp;</div>\r\n";
	$htmlAry['teachingToolDisplay'] .= "		</button>\r\n";
	$htmlAry['teachingToolDisplay'] .= "	</div>\r\n";
	$htmlAry['teachingToolDisplay'] .= "</li>\r\n";
}

# Title
$htmlAry['TitleInfo'] = $indexVar['libreportcard_ui']->Get_Device_Title_Info($thisZoneInfo[0]["ZoneNameB5"], $thisTopicInfo[0]["TopicNameB5"], $thisClassInfo[0]["ClassName"]);

# Button
$htmlAry['confirmBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("confirm", "", "updateTools()");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='classlevel_input' id='classlevel_input' value='".$_POST["classlevel_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$_POST["class_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='timetable_input' id='timetable_input' value='".$_POST["timetable_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$_POST["topic_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='zone_input' id='zone_input' value='".$_POST["zone_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='tool_input' id='tool_input' value=''>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='student_input' id='student_input' value='".$_POST["student_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='submit_flag' id='submit_flag' value=''>\r\n";

?>