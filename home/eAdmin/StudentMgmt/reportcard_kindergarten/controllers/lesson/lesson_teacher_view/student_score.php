<?php
// editing by 
/*
 * 	Date: 2017-10-17 Bill: Support Admin to access all classes
 * 	Date: 2017-10-09 Bill: Use timestamp to prevent display cached images	[2017-0929-1827-43096]
 */

# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("teacher");

# Data Checking
if(empty($_POST["classlevel_input"]) || empty($_POST["class_input"]) || empty($_POST["timetable_input"]) || empty($_POST["topic_input"]) || empty($_POST["zone_input"]) || empty($_POST["student_input"])) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Get Class, Topic and Zone Info
$thisClassInfo = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, '', '', $_POST["class_input"]);
$thisTopicInfo = $indexVar['libreportcard']->Get_Form_Topic_Record($_POST["topic_input"]);
$thisZoneInfo = $indexVar['libreportcard']->Get_Learning_Zone_Record($_POST["zone_input"]);

# Admin can access all classes - Get Target Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisClassInfo))
{
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
	$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
	$targetClass = $AccessibleClasses[$_POST["class_input"]];
	
	$thisClassInfo = array();
	$thisClassInfo[0] = $targetClass;
}

# Get selected Student
$ClassStudentList = $indexVar['libreportcard']->Get_Student_By_Class($_POST["class_input"], $_POST["student_input"]);
$thisClassStudent = $ClassStudentList[0];

# Data Checking
if(empty($thisClassInfo) || empty($thisTopicInfo) || empty($thisZoneInfo) || empty($ClassStudentList) || empty($thisClassStudent)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Get Student Info
$this_studentid = $thisClassStudent["UserID"];
$this_studentName = $thisClassStudent["StudentName"];
$this_studentUserLogin = $thisClassStudent["UserLogin"];

# Get Teaching Tools
$thisTimeTableInfo = $indexVar['libreportcard']->GetTimeTable($thisClassInfo[0]["ClassLevelID"], "", 1);
$thisTimeTableInfo = BuildMultiKeyAssoc((array)$thisTimeTableInfo, array("TopicID", "ZoneID", "ToolCodeID"));
$thisTeachingTools = $thisTimeTableInfo[$_POST["topic_input"]][$_POST["zone_input"]];
$withTeachingTool = count((array)$thisTeachingTools) > 0;
if($withTeachingTool) {
	$withTeachingTool = false;
	foreach($thisTeachingTools as $thisToolCodeID => $currentTeacherTool) {
		if(!empty($thisToolCodeID)) {
			$withTeachingTool = true;
			break;
		}
	}
}

# Get Student Score
$thisStudentToolsScore = $indexVar['libreportcard']->GET_STUDENT_SCORE((array)$_POST["student_input"], $_POST["classlevel_input"], $_POST["timetable_input"], $_POST["topic_input"], $_POST["zone_input"]);
$thisStudentToolsScore = $thisStudentToolsScore[$_POST["student_input"]];
$thisStudentToolsScore = BuildMultiKeyAssoc((array)$thisStudentToolsScore, array("ToolID"));

# Student Info Display
$this_studentPhoto = $indexVar['libreportcard']->Get_Student_Display_Photo($thisClassStudent);
$this_studentPhotoStyle = empty($thisStudentToolsScore)? "photo_up_teacher" : "photo_up";
$htmlAry['studentDisplay'] = $indexVar['libreportcard_ui']->Get_Class_Student_Photo($this_studentid, $this_studentName, $this_studentPhoto, $this_studentPhotoStyle);

# Student Score Display
$htmlAry['studentScoreDisplay'] = "";
if($withTeachingTool)
{
	// loop Teaching Tools
	$i = 0;
	foreach($thisTeachingTools as $thisToolCodeID => $currentTeacherTool)
	{
		# Get Teaching Tool Info
		if(empty($thisToolCodeID))	continue;
		$currentTeacherTool = $indexVar['libreportcard']->Get_Equipment_Record($thisClassInfo[0]["ClassLevelID"], $thisToolCodeID);
		$currentTeacherTool = $currentTeacherTool[0];
		if(empty($currentTeacherTool))	continue;
		
		# Get Tool Score
		//$thisStudentScore = $thisStudentToolsScore[$thisToolCodeID]["Score"];
		$thisStudentScore = $thisStudentScore > 0? $thisStudentScore : "-1";
		
		# Build Marking Area
		$liStyle = $i > 0 && ($i % 3 == 0)? " style = 'margin-left: 0px;' " : "";
		$htmlAry['studentScoreDisplay'] .= "<li class='tool-con' $liStyle><!-- NO SPACE PLEASE -->\r\n";
		    $htmlAry['studentScoreDisplay'] .= "<div class='photo'><img src='".$indexVar['thisImage'].$currentTeacherTool["PhotoPath"]."?t=".time()."'/></div>\r\n";
			$htmlAry['studentScoreDisplay'] .= "<div class='name-area'>".$currentTeacherTool["CH_Name"]."</div>\r\n";
			$htmlAry['studentScoreDisplay'] .= "<div class='star-area'>\r\n";
				$htmlAry['studentScoreDisplay'] .= "<ul class='star-list' id='tool_score_".$currentTeacherTool["CodeID"]."'>\r\n";
				
				// loop scores
                $hasStarSelected = false;
				for($j=5; $j>0; $j--) {
					$isSelected = ($thisStudentScore > 0 && $thisStudentScore == $j)? " selected" : "";
					if($thisStudentScore > 0 && $thisStudentScore == $j) {
                        $hasStarSelected = true;
                    }

					$htmlAry['studentScoreDisplay'] .= "<li class='star-obj".$isSelected."'>\r\n";
					if($hasStarSelected) {
						$htmlAry['studentScoreDisplay'] .= "<div class='star-w' style='display: none'><img src='".$ercKindergartenConfig['imageFilePath']."/rating_".$j.".png'/></div>\r\n";
						$htmlAry['studentScoreDisplay'] .= "<div class='star-y'><img src='".$ercKindergartenConfig['imageFilePath']."/rating_".$j."o.png'/></div>\r\n";
                    } else {
                        $htmlAry['studentScoreDisplay'] .= "<div class='star-w'><img src='".$ercKindergartenConfig['imageFilePath']."/rating_".$j.".png'/></div>\r\n";
                        $htmlAry['studentScoreDisplay'] .= "<div class='star-y' style='display: none'><img src='".$ercKindergartenConfig['imageFilePath']."/rating_".$j."o.png'/></div>\r\n";
                    }
					$htmlAry['studentScoreDisplay'] .= "</li>\r\n";
				}
				
				$htmlAry['studentScoreDisplay'] .= "</ul>\r\n";
				$htmlAry['studentScoreDisplay'] .= "<input type='hidden' name='tool_score[".$currentTeacherTool["CodeID"]."]' id='tool_score_".$currentTeacherTool["CodeID"]."_val' value='".$thisStudentScore."'>\r\n";
			$htmlAry['studentScoreDisplay'] .= "</div>\r\n";
		$htmlAry['studentScoreDisplay'] .= "</li>";
		
		$i++;
	}
}
$htmlAry['studentScoreDisplay'] .= "\r\n";

# Title
$htmlAry['TitleInfo'] = $indexVar['libreportcard_ui']->Get_Device_Title_Info($thisZoneInfo[0]["ZoneNameB5"], $thisTopicInfo[0]["TopicNameB5"], $thisClassInfo[0]["ClassName"]);

# Button
if($withTeachingTool) {
	$htmlAry['submitBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("send", "", "updateStudentScore()");
}
$htmlAry['cancelBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("cancel", "", "backToZone()");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='classlevel_input' id='classlevel_input' value='".$_POST["classlevel_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$_POST["class_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='timetable_input' id='timetable_input' value='".$_POST["timetable_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$_POST["topic_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='zone_input' id='zone_input' value='".$_POST["zone_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='student_input' id='student_input' value='".$_POST["student_input"]."'>\r\n";

?>