<?php
// editing by 
/*
 * 	Date: 2017-10-17 (Bill)
 * 			Support Admin to access all classes
 */

# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("teacher");

# Data Checking
if(empty($_POST["class_input"]) || empty($_POST["topic_input"])) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Get Class and Topic Info
$thisClassInfo = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, "", "", $_POST["class_input"]);
$thisTopicInfo = $indexVar['libreportcard']->Get_Form_Topic_Record($_POST["topic_input"]);

# Admin can access all classes - Get Target Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisClassInfo[0]))
{
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
	$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
	$targetClass = $AccessibleClasses[$_POST["class_input"]];
	
	$thisClassInfo = array();
	$thisClassInfo[0] = $targetClass;
}

# Data Checking
if(empty($thisClassInfo[0]) || empty($thisTopicInfo[0])) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Get Timetable and Zone Info
$thisTimeTableInfo = $indexVar['libreportcard']->GetTimeTable($thisClassInfo[0]["ClassLevelID"], "", 1);
$thisTimeTableInfo = BuildMultiKeyAssoc((array)$thisTimeTableInfo, array("TopicID", "ZoneID"));
$thisTopicRelatedZone = $thisTimeTableInfo[$_POST["topic_input"]];

# Get TimeTableID
$thisTimetableID = "";
$thisTopicZoneCount = count((array)$thisTopicRelatedZone) > 0;
if($thisTopicZoneCount) {
	$firstTopicZone = array_shift(array_slice($thisTopicRelatedZone, 0, 1));
	$thisTimetableID = $firstTopicZone["TopicTimeTableID"];
}

# Data Checking
if(empty($thisTopicRelatedZone)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Zone Selection
$htmlAry['zoneDisplay'] = $indexVar['libreportcard_ui']->Get_Device_Zone_Selection_Area($thisTopicRelatedZone, $thisClassInfo[0]["YearID"]);

# Title
$htmlAry['TitleInfo'] = $indexVar['libreportcard_ui']->Get_Device_Title_Info("", $thisTopicInfo[0]["TopicNameB5"], $thisClassInfo[0]["ClassName"]);

# Button
$htmlAry['backBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("back", "index.php?task=lesson.lesson_teacher_view.index");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='classlevel_input' id='classlevel_input' value='".$thisClassInfo[0]["ClassLevelID"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$_POST["class_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='timetable_input' id='timetable_input' value='".$thisTimetableID."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$_POST["topic_input"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='zone_input' id='zone_input' value=''>\r\n";

?>