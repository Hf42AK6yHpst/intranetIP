<?php
// editing by 
/*
 *  Date: 2019-11-08 (Bill)
 *      - hard code > display '語' first
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

// Get Search Value
$searchText = addslashes(trim($searchStr));
$searchTWCat = addslashes(trim($selectTWCat));
$searchTWLevel = addslashes(trim($selectTWLevel));
$searchMOLevel = addslashes(trim($selectMOCat));

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$field = ($field=='')? 0 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
$pos = 0;

// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = "SELECT
				aitw.Code as TWItemCode,
				aitw.Name as TWItemName,
				aimo.Code as MOItemCode,
				aimo.Name as MOItemName,
                CASE
                    WHEN TWItemCode LIKE '語%' THEN 1
                    WHEN TWItemCode LIKE '身%' THEN 2
                    WHEN TWItemCode LIKE '認%' THEN 3
                    WHEN TWItemCode LIKE '社%' THEN 4
                    WHEN TWItemCode LIKE '情%' THEN 5
                    WHEN TWItemCode LIKE '美%' THEN 6
                    ELSE 9
                END as TWCatOrder
			FROM
				".$indexVar['thisDbName'].".RC_ABILITY_CATEGORY_INDEX_MAPPING aim
			    INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY aitw ON (aim.TWItemID = aitw.CatID)
			    INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY aimo ON (aim.MOItemID = aimo.CatID)
			Where 
				1 ".
			    (trim($searchText) != "" ? " AND (aitw.Code LIKE '%$searchText%' OR
                                                     aitw.Name LIKE '%$searchText%' OR
                                                     aimo.Code LIKE '%$searchText%' OR
                                                     aimo.Name LIKE '%$searchText%'
                                                )" : "" ).
			    (trim($searchTWCat) != "" ? " AND aitw.Code LIKE '%$searchTWCat%'" : "").
			    (trim($searchTWLevel) != "" ? " AND aitw.Code LIKE '%$searchTWLevel%'" : "").
			    (trim($searchMOLevel) != "" ? " AND aimo.Code LIKE '%$searchMOLevel%'" : "" );
$li->IsColOff = "IP25_table";
//$li->field_array = array("TWItemCode", "TWItemName", "MOItemCode", "MOItemName");
$li->field_array = array("CONCAT(TWCatOrder, TWItemCode)", "TWItemName", "MOItemCode", "MOItemName");
$li->fieldorder2 = ", TWItemCode ASC ";
$li->column_array = array(0, 0, 0, 0);
$li->wrap_array = array(0, 0);
$li->no_col = count($li->field_array)+1;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='40%'>".$li->column($pos++, $Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['MacauCode'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='40%'>".$li->column($pos++, $Lang['eReportCardKG']['Setting']['MacauAbilityIndex']['Name'])."</td>\n";

// Search Bar
$htmlAry['searchBox'] = $indexVar['libreportcard_ui']->Get_Search_Box_Div('searchStr', stripslashes($searchStr));

// Filter
$htmlAry['typeSelection'] = "";
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_Category_Selection($selectTWCat);			// Taiwan Category
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_ClassLevel_Selection($selectTWLevel);		// Taiwan Class Level
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Macau_Category_Selection($selectMOCat);			// Macau Category

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='".$li->pageNo."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='".$li->field."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='".$li->page_size."'>";
?>