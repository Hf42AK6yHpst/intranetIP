<?php
// Using:
/*
 * Change Log:
 * Date 2019-06-06 (Bill)
 *      Create file
 */

/* 
### Cookies setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_Settings_learningZone_YearID","YearID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AwardsList']['Title']);
if(isset($success)){
    $returnMsg = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}else{
	$returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

# Action Buttons
// New
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// Edit & Delete
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

/* 
# Year Selection
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);
 */

# Table Data
$table = '<table class="common_table_list_v30" id="ContentTable">';
$table .= '<thead>';
$table .= '<tr>';
    $table .= '<th style="width:3%;">#</th>';
    $table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['Code'].'</th>';
    $table .= '<th style="width:15%;">'.$Lang['eReportCardKG']['Setting']['Name'].'</th>';
    $table .= '<th style="width:15%;">'.$Lang['eReportCardKG']['Setting']['AwardsList']['AwardType'].'</th>';
    $table .= '<th style="width:3%;"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="CheckAll()"></th>';
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';

// loop Topics
$awards = $indexVar['libreportcard']->Get_KG_Award_List();
if(!empty($awards))
{
    $i = 1;
    foreach ((array)$awards as $this_award)
    {
		$table .= '<tr>';
    		$table .= '<td><span class="rowNumSpan">'.$i.'</span></td>';
    		$table .= '<td><span class="rowNumSpan">'.$this_award['AwardCode'].'</span></td>';
    		$table .= '<td><a class="tablelink" href="javascript:goEdit('.$this_award['AwardID'].');">'.Get_Lang_Selection($this_award['AwardNameCh'], $this_award['AwardNameEn']).'</a></td>';
    		$table .= '<td><span class="rowNumSpan">'.$Lang['eReportCardKG']['Setting']['AwardsList']['AwardTypeArr'][$this_award['AwardType']].'</span></td>';
    		if($this_award['AwardType'] == $ercKindergartenConfig['awardType']['Generate']) {
    		    $table .= '<td>&nbsp;</td>';
    		}
    		else {
                $table .= '<td><input type="checkbox" class="checkbox" name="checkbox[]" id="checkbox_'.$this_award['AwardID'].'" value="'.$this_award['AwardID'].'"></td>';
    		}
		$table .= '</tr>';
		$i++;
	}
}
else
{
	$table .= '<tr>';
    	$table .= '<td colspan="5" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
	$table .= '</tr>';
}
$table .= '</tbody>';
$table .= '</table>';
?>