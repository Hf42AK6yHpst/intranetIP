<?php
// Using:
/*
 * Change Log:
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * Date 2019-06-06 (Bill)
 *      Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AwardsList']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Get Topic Data
if ($_GET['isEdit'])
{
    $awardID = IntegerSafe($awardID);
    $data = $indexVar['libreportcard']->Get_KG_Award_List($awardID);
	$awardCode = $data[0]['AwardCode'];
    $awardNameCH = $data[0]['AwardNameCh'];
    $awardNameEn = $data[0]['AwardNameEn'];
    
	$navigation = $Lang['Btn']['Edit'];
}
else
{
	$navigation = $Lang['Btn']['New'];
}

# NavigationBar
$navigationAry[] = array($Lang['eReportCardKG']['Setting']['AwardsList']['Title'],'index.php?task=settings.award_list.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>