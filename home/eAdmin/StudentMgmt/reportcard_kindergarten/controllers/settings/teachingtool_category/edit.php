<?php
// editing by 
/*
 * 	Date 2017-10-17 Bill: Create File
 */

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TeachingToolCategory']['Title']);

$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);
echo $indexVar['libreportcard_ui']->Include_Thickbox_JS_CSS();

### Main Paga Data
if ($_GET['isEdit'])
{
	$data = $indexVar['libreportcard']->Get_Equipment_Category($catID);
	$code = $data[0]['Code'];
	$nameEn = $data[0]['EN_Name'];
	$nameCh = $data[0]['CH_Name'];
//	$yearID = $data[0]['YearID'];
//	$yearName = $data[0]['YearName'];
	$remarks = $data[0]['Remarks'];
	
	$navigation = $Lang['Btn']['Edit'];
}
else
{
	$navigation = $Lang['Btn']['New'];
}

//$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($yearID,'YearOnChange()');

$navigationAry[] = array($Lang['eReportCardKG']['Setting']['TeachingToolCategory']['Title'],'index.php?task=settings.teachingtool_category.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);
### Main Paga Data [END]

?>