<?php
// editing by 
/*
 * 	Date 2017-10-17 Bill: Create File
 */

### Cookies Setting
//$arrCookies = array();
//$arrCookies[] = array("eRCkg_settings_teachingTool_YearID","YearID");
//if(isset($clearCoo) && $clearCoo == 1) {
//	clearCookies($arrCookies);
//}
//else {
//	updateGetCookies($arrCookies);
//}

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TeachingToolCategory']['Title']);

### Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else{
	$returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

### Tool Bar Action Buttons
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB Table Action Buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

## Year Selection
//$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);

### Table Data
$data = $indexVar['libreportcard']->Get_Equipment_Category();
$data_Arr = Get_Array_By_Key($data, 'CatID');

$table = '<table class="common_table_list_v30" id="ContentTable">';
$table .= '<thead>';
$table .= '<tr>';
	$table .= '<th style="width:3%;">#</th>';
	$table .= '<th style="width:20%;">'.$Lang['eReportCardKG']['Setting']['Code'].'</th>';
	$table .= '<th style="width:74%;">'.$Lang['eReportCardKG']['Setting']['Name'].'</th>';
	$table .= '<th style="width:3%;"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="CheckAll()"></th>';
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';

$i = 1;
if(!empty($data))
{
	foreach ($data as $_data)
	{
		$table .= '<tr>';
			$table .= '<td><span class="rowNumSpan">'.$i.'</span></td>';
			$table .= '<td><span class="rowNumSpan">'.$_data['Code'].'</span></td>';
			$table .= '<td><a class="tablelink" href="javascript:goEdit('.$_data['CatID'].');">'.$_data['EN_Name'].'</a></td>';
			$table .= '<td><input type="checkbox" class="checkbox" name="checkbox[]" id="checkbox_'.$_data['CatID'].'" value="'.$_data['CatID'].'"></td>';
		$table .= '</tr>';
		$i++;
	}
}
else	// No Learning Tool Category
{
	$table .= '<tr>';
		$table .= '<td colspan="4" align="center">';
		$table .= $Lang['General']['NoRecordAtThisMoment'] ;
		$table .= '</td>';
	$table .= '</tr>';
}
$table .= '</tbody>';
$table .= '</table>';

?>