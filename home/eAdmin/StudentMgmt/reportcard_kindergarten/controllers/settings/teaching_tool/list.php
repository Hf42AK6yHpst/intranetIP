<?php
// editing by 
/* 
 * Change Log:
 * Date 2019-10-31 Bill:  class teacher > view own class level only
 * Date 2018-09-04 Bill:  Replace Category Filtering & Add keywork search
 * Date 2017-10-17 Bill:  Support Category Filtering
 * Date 2017-10-09 Bill:  Use timestamp to prevent display cached images	[2017-0929-1827-43096]
 * Date 2017-02-09 Villa: Add Cookies
 * Date 2017-02-06 Villa: CheckAll Function
 * Date 2017-01-25 Villa: Open the File
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_settings_teachingTool_YearID","YearID");
// $arrCookies[] = array("eRCkg_settings_teachingTool_CatID","ToolCatID");
// $arrCookies[] = array("eRCkg_settings_teachingTool_SearchZone","searchZone");
$arrCookies[] = array("eRCkg_settings_teachingTool_ZoneID","ZoneID");
$arrCookies[] = array("eRCkg_settings_teachingTool_SearchStr","searchStr");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
} else {
	updateGetCookies($arrCookies);
}

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TeachingTool']['Title']);

### Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
} else{
	$returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

### ToolBar Action Buttons
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB Table Action Buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

$filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();

## Year Selection
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);

## Category Selection
// $CatSelection = $indexVar['libreportcard_ui']->Get_Tool_Cat_Selection($ToolCatID);

## Zone Selection
$ZoneSearchSelection = $indexVar['libreportcard_ui']->Get_Zone_Selection($ZoneID, $YearID, 'document.form1.submit()', false, $filterByClassTeacher);

## Search Bar
$htmlAry['searchBox'] = $indexVar['libreportcard_ui']->Get_Search_Box_Div('searchStr', stripslashes($searchStr));

## Taiwan Category
$temp = $indexVar['libreportcard']->Get_Taiwan_Category();
$TWCata = BuildMultiKeyAssoc($temp, "CatID");

### Table Data
$searchText = addslashes(trim($searchStr));
// $data = $indexVar['libreportcard']->Get_Equipment_Record($YearID, "", $ToolCatID, $searchZone, $searchText);
$data = $indexVar['libreportcard']->Get_Equipment_Record($YearID, '', '', '', $ZoneID, $searchText, $chapter, $filterByClassTeacher);
$data_Arr = Get_Array_By_Key($data, 'CodeID');

# Learning Zone Mapping
$zone_Arr = Get_Array_By_Key($data, 'ZoneID');
$zone_Arr = $indexVar['libreportcard']->Get_Learning_Zone_Record($zone_Arr, '', false, $filterByClassTeacher);
$zone_Arr = BuildMultiKeyAssoc($zone_Arr, 'ZoneID', 'ZoneNameB5', 1, 0);
$zone_Arr[0] = '---';

## Taiwan Category Mapping
$temp = $indexVar['libreportcard']->Get_Taiwan_Category_Fm_Mapping($data_Arr);
foreach($temp as $_temp){
	$CataIDByCodeID[$_temp['CodeID']][] = $_temp['CatID'];
}

## Chapter List Selection
$ChapterSelection = $indexVar['libreportcard_ui']->Get_Chapter_Selection($chapter, 'document.form1.submit()', $YearID, $ZoneID, $filterByClassTeacher);

$table = '<table class="common_table_list_v30" id="ContentTable">';
$table .= '<thead>';
$table .= '<tr>';
	$table .= '<th style="width:2%;">#</th>';
	$table .= '<th style="width:3%;">'.$Lang['eReportCardKG']['Setting']['Code'].'</th>';
	$table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['Name'].'</th>';
	$table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['Year'].'</th>';
	$table .= '<th stlye="width:6%;">'.$Lang['eReportCardKG']['Setting']['Chapter'].'</th>';
	$table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['LearningZone']['Title'].'</th>';
	$table .= '<th style="width:32%;">'.$Lang['eReportCardKG']['Setting']['AbilityIndex'].'</th>';
	$table .= '<th style="width:25%;">'.$Lang['eReportCardKG']['Setting']['Photo'].'</th>';
	$table .= '<th style="width:2%;"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="CheckAll()"></th>';
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';

$i = 1;
if(!empty($data))
{
	foreach ($data as $_data)
	{
		if($_data['PhotoPath']){
			$photoDisplay = "<img class='lazy' data-src='".$indexVar['thisImage'].$_data['PhotoPath']."?t=".time()."' style='width:200px; height:auto;' src=''>";
		}
		else{
			$photoDisplay = "";
		}
		
		$table .= '<tr>';
			$table .= '<td><span class="rowNumSpan">'.$i.'</span></td>';
			$table .= '<td><span class="rowNumSpan">'.$_data['Code'].'</span></td>';
			$table .= '<td><a class="tablelink" href="javascript:goEdit('.$_data['CodeID'].');">'.$_data['EN_Name'].'</a></td>';
			$table .= '<td><span class="rowNumSpan">'.$_data['YearName'].'</span></td>';
			$table .= '<td><span class="rowNumSpan">'.$_data['Chapter'].'</span></td>';
			$table .= '<td><span class="rowNumSpan">'.$zone_Arr[$_data['ZoneID']].'</span></td>';
			$table .= '<td><span class="rowNumSpan">';
			foreach((array)$CataIDByCodeID[$_data['CodeID']] as $_Cat){
				$table .='['.$TWCata[$_Cat]['Code']."] &nbsp;".$TWCata[$_Cat]['Name'].'<br>';
			}
			$table .= '</span></td>';
			$table .= '<td>'.$photoDisplay.'</td>';
			$table .= '<td><input type="checkbox" class="checkbox" name="checkbox[]" id="checkbox_'.$_data['CodeID'].'" value="'.$_data['CodeID'].'"></td>';
		$table .= '</tr>';
		$i++;
	}
}
else	// No learning tool data
{
	$table .= '<tr>';
		$table .= '<td colspan="9" align="center">';
		$table .= $Lang['General']['NoRecordAtThisMoment'];
		$table .= '</td>';
	$table .= '</tr>';
}
$table .= '</tbody>';
$table .= '</table>';

?>