<?php
// editing by 
/*
 * Change Log:
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * Date	2017-02-09 Villa Add navigation Bar
 * Date 2017-02-06 Villa Fix En Name/ CH name exchanged
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['LearningZone']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);
echo $indexVar['libreportcard_ui']->Include_Thickbox_JS_CSS();

# Get Learning Zone Data
if ($_GET['isEdit']) {
	$data = $indexVar['libreportcard']->Get_Learning_Zone_Record($zoneID);
	$zoneCode = $data[0]['ZoneCode'];
	$zoneCodeCH = $data[0]['ZoneNameB5'];
	$zoneCodeEN = $data[0]['ZoneNameEN'];
	$zoneQuota = $data[0]['ZoneQuota'];
	$pictureType = $data[0]['PictureType'];
	$remarks = $data[0]['Remark'];
	$yearID = $data[0]['YearID'];
	$yearName = $data[0]['YearName'];
	$navigation = $Lang['Btn']['Edit'];
}else{
	$navigation = $Lang['Btn']['New'];
}
# Year Selection
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($yearID, false);

# Picture Selection
$PICSelection = "";
$PICSelection .= "<table width='100%'>";
$PICSelection .= "<tr>";
for($i=1; $i<6; $i++)
{
	$isChecked = $pictureType==$i? "checked" : "";;
	$PICSelection .= "<td style='text-align: center'>";
	$PICSelection .= "<label for='picture_".$i."'><img src='".$ercKindergartenConfig['imageFilePath']."/btn_subj_".$i.".png' width='100px'/></label><br>";
	$PICSelection .= "<input type='radio' value='".$i."' id='picture_".$i."' name='picture' ".$isChecked."/>";
	$PICSelection .= "</td>";
}
$PICSelection .= "</tr>";
$PICSelection .= "</table>";

# NavigationBar
$navigationAry[] = array($Lang['eReportCardKG']['Setting']['LearningZone']['Title'],'index.php?task=settings.learning_zone.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>