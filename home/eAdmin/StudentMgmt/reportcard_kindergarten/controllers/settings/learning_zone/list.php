<?php
// editing by 
/*
 * Change Log:
 * Date 2019-10-31 Bill class teacher > view own class level only
 * Date 2017-02-09 Villa Add Cookies Setting
 * Date 2017-02-06 Villa Add Check All Function
 */

### Cookies setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_Settings_formTopic_YearID","YearID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
} else {
	updateGetCookies($arrCookies);
}

if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$returnMsg = "";
}

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['LearningZone']['Title']);
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);
// $indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Action Buttons
// New
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// Edit & Delete
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

# Year Selection
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);

# Table Data
$table = '<table class="common_table_list_v30" id="ContentTable">';
$table .= '<thead>';
$table .= '<tr>';
$table .= '<th style="width:3%;">#</th>';
$table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['Code'].'</th>';
$table .= '<th style="width:15%;">'.$Lang['eReportCardKG']['Setting']['Name'].'</th>';
$table .= '<th style="width:15%;">'.$Lang['eReportCardKG']['Setting']['ApplyForm'].'</th>';
$table .= '<th style="width:3%;"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="CheckAll()"></th>';
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';

$i = 1;

// loop Topics
$filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();
$zone = $indexVar['libreportcard']->Get_Learning_Zone_Record("", $YearID, 1, $filterByClassTeacher);
if(!empty($zone)){
	foreach ((array)$zone as $this_zone){
		$table .= '<tr>';
		$table .= '<td><span class="rowNumSpan">'.$i.'</span></td>';
		$table .= '<td><span class="rowNumSpan">'.$this_zone['ZoneCode'].'</span></td>';
		$table .= '<td><a class="tablelink" href="javascript:goEdit('.$this_zone['ZoneID'].');">'.Get_Lang_Selection($this_zone['ZoneNameB5'], $this_zone['ZoneNameEN']).'</a></td>';
		$table .= '<td><span class="rowNumSpan">'.$this_zone['YearName'].'</span></td>';
		$table .= '<td><input type="checkbox" class="checkbox" name="checkbox[]" id="checkbox_'.$this_zone['ZoneID'].'" value="'.$this_zone['ZoneID'].'"></td>';
		$table .= '</tr>';
		$i++;
	}
}else{ // if zone no data
	$table .= '<tr>';
	$table .= '<td colspan="5" align="center">';
	$table .= $Lang['General']['NoRecordAtThisMoment'] ;
	$table .= '</td>';
	$table .= '</tr>';
}
$table .= '</tbody>';
$table .= '</table>';

?>