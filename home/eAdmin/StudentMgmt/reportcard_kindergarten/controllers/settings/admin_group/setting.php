<?php 

$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start();

// 2 situation >>> New | Edit
$groupID = $_REQUEST['AdminGroupIDArr'];
if(is_array($groupID)) $groupID = $groupID[0];

if($groupID) {
    $PAGE_NAVIGATION[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['MenuTitle'], "javascript:js_Back_To_Admin_Group_List()");
    $PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
    $SubmitBtnText = $Lang['Btn']['Submit'];
    
    $AdminGroupInfoArr = $indexVar['libreportcard']->Get_Admin_Group_Info($groupID);
    $AccessRightInfoArr = $indexVar['libreportcard']->Get_Admin_Group_Access_Right($groupID);
    $AccessRightInfoArr = Get_Array_By_Key($AccessRightInfoArr, 'AdminGroupRightName');
} else {
    $PAGE_NAVIGATION[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['MenuTitle'], "javascript:js_Back_To_Admin_Group_List()");
    $PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
    $SubmitBtnText = $Lang['Btn']['Continue'];
    
    ### Step Table
    $STEPS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][1], 1);
    $STEPS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][2], 0);
    $htmlAry['StepTable'] = $indexVar['libreportcard_ui']->GET_STEPS_IP25($STEPS_OBJ);
}

### Navigation
$htmlAry['PageNav'] = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### Continue Button
$btn_Continue = $indexVar['libreportcard_ui']->GET_ACTION_BTN($SubmitBtnText, "button", $onclick="js_Update_Group_Info();", $id="Btn_Submit");

### Cancel Button
$btn_Cancel = $indexVar['libreportcard_ui']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Back_To_Admin_Group_List()", $id="Btn_Cancel");

//$htmlAry['NAV'] = array(
//    'AdminGroupInfo' => $indexVar['libreportcard_ui']->GET_NAVIGATION2_IP25($Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupInfo']),
//    'AccessRight' => $indexVar['libreportcard_ui']->GET_NAVIGATION2_IP25($Lang['eReportCardKG']['Setting']['AdminGroup']['AccessRight']),
//);

$htmlAry['FormItem'] = "";
$formTableItems = array(
    'Code' => array(
        'name' => "AdminGroupCode",
        'value' => intranet_htmlspecialchars($AdminGroupInfoArr[0]['AdminGroupCode']),
        'fieldTitle' => $Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupCode'],
        'maxLength' => 100,
        'warning' => $indexVar['libreportcard_ui']->Get_Thickbox_Warning_Msg_Div("AdminGroupCodeWarningDiv"),
    ),
    'EnglishName' => array(
        'name' => "AdminGroupNameEn",
        'value' => intranet_htmlspecialchars($AdminGroupInfoArr[0]['AdminGroupNameEn']),
        'fieldTitle' => $Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupName'].' '.$Lang['General']['FormFieldLabel']['En'],
        'maxLength' => 100,
        'warning' => $indexVar['libreportcard_ui']->Get_Thickbox_Warning_Msg_Div("AdminGroupNameEnWarningDiv"),
    ),
    'ChineseName' => array(
        'name' => "AdminGroupNameCh",
        'value' => intranet_htmlspecialchars($AdminGroupInfoArr[0]['AdminGroupNameCh']),
        'fieldTitle' => $Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupName'].' '.$Lang['General']['FormFieldLabel']['Ch'],
        'maxLength' => 100,
        'warning' => $indexVar['libreportcard_ui']->Get_Thickbox_Warning_Msg_Div("AdminGroupNameChWarningDiv"),
    )
);
foreach($formTableItems as $k => $fti){
    $htmlAry['FormItem'] .= infoFormItem($fti['name'], $fti['value'], $fti['fieldTitle'], $fti['maxLength'], $fti['warning']);
}

$menuArr = $MODULE_OBJ['menu'];
$htmlAry['AccessRightTable'] = "";
foreach ((array)$menuArr as $thisMenu => $thisSubMenuArr)
{
    $thisChkID = $thisMenu.'_Chk';

    $x .= '<div class="able_board">'."\n";
        $x .= '<div class="form_sub_title_v30">'."\n";
            $x .= '<em>'."\n";
                $x .= '- <span class="field_title"> '.$thisSubMenuArr[0].' </span>-'."\n";
                $x .= '&nbsp;&nbsp;&nbsp;'."\n";
                $x .= $indexVar['libreportcard_ui']->Get_Checkbox($thisChkID, $thisChkID, 1, $isChecked=0, $Class='', $Lang['Btn']['SelectAll'], "js_Select_All_Checkboxes('".$thisMenu."')", $Disabled='')."\n";
            $x .= '</em>'."\n";
            $x .= '<p class="spacer"></p>'."\n";
        $x .= '</div>'."\n";

        $x .= '<table class="form_table_v30">'."\n";
            $x .= '<col class="field_title">'."\n";
            $x .= '<col class="field_c">'."\n";

    		foreach ((array)$thisSubMenuArr['Child'] as $thisSubMenu => $thisSubMenuInfoArr)
    		{
    		    $thisSubMenuTitle = $thisSubMenuInfoArr[0];
    		    $thisLink = $thisSubMenuInfoArr[1];
    		    $thisLink = str_replace('?task=', '', $thisLink);
    		    $LinkStructureAry = explode('.', $thisLink);
    		    $thisCheckboxValue = $LinkStructureAry[0].'.'.$LinkStructureAry[1];
    			$thisChecked = (in_array($thisCheckboxValue, (array)$AccessRightInfoArr))? 'checked' : '';

    			$x .= '<tr valign="top">'."\n";
                    $x .= '<td class="field_title">'.$thisSubMenuTitle.'</td>'."\n";
                    $x .= '<td>'."\n";
                        $x .= '<input type="checkbox" name="AccessiableMenuArr[]" class="'.$thisMenu.'" value="'.$thisCheckboxValue.'" '.$thisChecked.' />'."\n";
                    $x .= '</td>'."\n";
                $x .= '</tr>'."\n";
             }
         $x .= '</table>'."\n";
     $x .= '</div>'."\n";
}
$htmlAry['AccessRightTable'] = $x;

$htmlAry['buttonBar'] = $btn_Continue."&nbsp;".$btn_Cancel;
$htmlAry['hidden'] = '<input type="hidden" id="AdminGroupID" name="AdminGroupID" value="'.$AdminGroupInfoArr[0]['AdminGroupID'].'" />';

$jsWarnMsgArr = array();
$jsWarnMsgArr["AdminGroupNameEn"] = $Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Name'];
$jsWarnMsgArr["AdminGroupNameCh"] = $Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Name'];
$jsWarnMsgArr["AdminGroupCode"] = $Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['Blank']['Code'];
$jsWarnMsgArr["CodeInUse"] = $Lang['eReportCardKG']['Setting']['AdminGroup']['WarningArr']['InUse']['Code'];

$htmlAry['jsWarnMsgArr'] = "var jsWarnMsgArr = [];\n";
foreach($jsWarnMsgArr as $k => $warnMsg){
    $htmlAry['jsWarnMsgArr'] .= "jsWarnMsgArr[\"$k\"] = \"$warnMsg\";\n";
}

function infoFormItem($name, $value, $fieldTitle, $maxLength = 100, $warning = '')
{
    $x .= '<tr>'."\n";
        $x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$fieldTitle.'</td>'."\n";
        $x .= '<td>'."\n";
        $x .=   '<input name="'.$name.'" id="'.$name.'" class="textbox_name required" value="'.$value.'" maxlength="'.$maxLength.'">'."\n";
        $x .=   $warning;
        $x .= '</td>'."\n";
    $x .= '</tr>'."\n";
    return $x;
}
?>