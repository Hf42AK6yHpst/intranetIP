<?php 

if(!empty($_POST['UserIDArr'])){
    $UserIDArr = array_remove_empty($_POST['UserIDArr']);
    $UserIDArr = Get_User_Array_From_Common_Choose($UserIDArr, $UserTypeArr=array(1));
} else {
    $UserIDArr = array();
}

$AdminGroupID = $_POST['AdminGroupID'];
$FromNew = $_POST['FromNew'];
$isUpdate = $_POST['isUpdate'];
if($isUpdate) {
    // Update here
    $Success = $indexVar['libreportcard']->Update_Admin_Group_Member($AdminGroupID, $UserIDArr);
    $ReturnMsgKey = ($Success)? 'EditSuccess' : 'EditFailed';
} else {
    // Delete here
    $Success = $indexVar['libreportcard']->Delete_Admin_Group_Member($AdminGroupID, $UserIDArr);
    $ReturnMsgKey = ($Success)? 'DeleteSuccess' : 'DeleteFailed';
}

$NextPage = "?task=settings.admin_group.member";
$para = "AdminGroupID=$AdminGroupID&ReturnMsgKey=$ReturnMsgKey&FromNew=$FromNew";
header("Location: $NextPage&$para");
?>