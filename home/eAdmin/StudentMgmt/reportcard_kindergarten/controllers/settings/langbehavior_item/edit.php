<?
// Using: 
/*
 *  Date: 2019-10-31 Bill
 *          - class teacher > view own class level only
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 	Date: 2017-12-20 (Bill)
 * 			Get Topics related Term Settings
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

### Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Setting"]["LanguageBehaviorItem"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);

### Main Paga Data
if ($_GET["isEdit"] && $topicIdAry[0] > 0)
{
	$TopicArr = $indexVar["libreportcard"]->getTopics($topicIdAry[0], "", "", false, "", false, true);
	$TopicArr = $TopicArr[0];
	$Code = $TopicArr["Code"];
	$CatID = $TopicArr["CatID"];
	$Name_CH = $TopicArr["NameCh"];
	$Name_EN = $TopicArr["NameEn"];
	$TopicYearID = $TopicArr["YearID"];
	$TopicTermID = $TopicArr["TermID"];
	$TopicTermID = $TopicTermID ? $TopicTermID : "0";
	
	$navigation = $Lang["Btn"]["Edit"];
}
else
{
	$navigation = $Lang["Btn"]["New"];
}
### Main Paga Data END

### Navigation
$navigationAry[] = array($Lang["eReportCardKG"]["Setting"]["LanguageBehaviorItem"]["Title"],"index.php?task=settings.langbehavior_item.list");
$navigationAry[] = array($navigation);
$htmlAry["navigation"] = $indexVar["libreportcard_ui"]->GET_NAVIGATION_IP25($navigationAry);

## Year Selection
$htmlAry["yearSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($TopicYearID, " ", true);

### Term Selection
$htmlAry["termSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection($TopicTermID, "", true);

### Topic Category Selection
$htmlAry["topicCategorySelection"] = $indexVar["libreportcard_ui"]->Get_Topic_Cat_Select($CatID, " ");

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>