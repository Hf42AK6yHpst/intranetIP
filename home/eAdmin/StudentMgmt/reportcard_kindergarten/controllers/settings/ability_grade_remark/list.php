<?php
// editing by
/*
 *
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_settings_abilityGradeRemark_TWCatID","selectTWCat");
$arrCookies[] = array("eRCkg_settings_abilityGradeRemark_TWLevel","selectTWLevel");
if(isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
}
else {
    updateGetCookies($arrCookies);
}

// Include DB Table
include_once ($indexVar['thisBasePath'] . "includes/libdbtable.php");
include_once ($indexVar['thisBasePath'] . "includes/libdbtable2007a.php");

// Page Title
$TAGS_OBJ[] = array(
    $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['Title']
);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
// $btnAry[] = array('export', 'javascript: goExport();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

// Get Search Value
$searchText = addslashes(trim($searchStr));
$searchTWCat = addslashes(trim($selectTWCat));
$searchTWLevel = addslashes(trim($selectTWLevel));
$searchMOLevel = addslashes(trim($selectMOCat));

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
    $page_size = $ck_page_size;
}
$field = ($field == '') ? 0 : $field;
$order = ($order == '') ? 1 : $order;
$page = ($page == '') ? 1 : $page;
$pos = 0;

### DB table action buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: checkEdit();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = "SELECT
				aitw.Code as TWItemCode,
                ragr.Grade as Grade,
                ra.Remarks as Remarks,
                IF(agr.Remarks IS NULL, '--', agr.Remarks) as RemarksAll,
                CONCAT('<input type=\'checkbox\' class=\'checkbox\' name=\'AbilityRemarkID[]\' id=\'AbilityRemarkID_', ra.AbilityRemarkID, '\' value=', ra.AbilityRemarkID,'>') as edit_box
			FROM
				" . $indexVar['thisDbName'] . ".RC_ABILITY_INDEX_CATEGORY aitw
                INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS ra
                    ON (ra.CatID = aitw.CatID)
                INNER JOIN ".$indexVar['thisDbName']. ".RC_ABILITY_GRADING_RANGE ragr
                    ON (ragr.GradingRangeID = ra.GradingRangeID)
                LEFT OUTER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS_ALL agr
                    ON (agr.AbilityRemarkID = ra.AbilityRemarkID)
			Where
				1 " . (trim($searchText) != "" ? "	AND (aitw.Code LIKE '%$searchText%' OR
					 ra.Remarks LIKE '%$searchText%' OR agr.Remarks LIKE '%$searchText%')" : "")
                     . (trim($searchTWCat) != "" ? "	AND aitw.Code LIKE '%$searchTWCat%'" : "") 
                     . (trim($searchTWLevel) != "" ? "	AND aitw.Code LIKE '%$searchTWLevel%'" : "");
$li->IsColOff = 'IP25_table';
$li->field_array = array("TWItemCode", "Grade", "Remarks", "RemarksAll", "edit_box");
$li->fieldorder2 = ", aitw.Code asc, ra.GradingRangeID desc";
//ORDER BY TWItemCode asc, agr.GradingRangeID desc
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0);
$li->no_col = count($li->field_array) + 1;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='70px'>" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityRemarks']['Grading']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' >" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityRemarks']['Comment']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='min-width:150px;' >" . $li->column($pos ++, $Lang['eReportCardKG']['Setting']['AbilityRemarks']['OriComment']) . "</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>&nbsp;</td>\n";

// Search Bar
$htmlAry['searchBox'] = $indexVar['libreportcard_ui']->Get_Search_Box_Div('searchStr', stripslashes($searchStr));

// Filter
$htmlAry['typeSelection'] = "";
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_Category_Selection($selectTWCat); // Taiwan Category
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_ClassLevel_Selection($selectTWLevel); // Taiwan Class Level
                                                                                                             
// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='" . $li->pageNo . "'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='" . $li->field . "'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='" . $li->page_size . "'>";

?>