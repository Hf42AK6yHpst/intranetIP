<?php
// Editing by Philips
/*
 * Change Log:
 * 
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AbilityRemarks']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

$isEdit = $_GET["CodeID"] != '';
if(!$isEdit)
{
    $sql = "SELECT GradingRangeID, Grade, LowerLimit, UpperLimit FROM ".$indexVar['thisDbName'].".RC_ABILITY_GRADING_RANGE ORDER BY LowerLimit + 0";
    $result = $indexVar['libreportcard']->returnArray($sql);
    
    $IndexGradingRange = $_POST['IndexGradingRange']? $_POST['IndexGradingRange']: $result[0]['GradingRangeID'];
    $gradeSelection = getSelectByArray($result," name='IndexGradingRange' id='IndexGradingRange' onChange='refreshAvailableIndex()'" , $IndexGradingRange, 0, 1);
    
    $result = BuildMultiKeyAssoc($result, 'GradingRangeID');
    $targetGrading = $result[$IndexGradingRange];
    list($IndexGradingRange, $IndexGrade, $IndexLowerLimit, $IndexUpperLimit) = $targetGrading;
    $IndexCatID = $_POST['abilityIndexID'];

    $navigation = $Lang['Btn']['New'];
}
else
{
    # Get Category / Index Info
    $sql = "SELECT
                raic.Name, ragr.Grade, ragr.LowerLimit, ragr.UpperLimit, gr.Remarks as Remarks, IF(ra.Remarks IS NULL, gr.Remarks, ra.Remarks) as RemarkAll, gr.GradingRangeID, gr.CatID
            FROM
                ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY as raic
                INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS as gr ON gr.CatID = raic.CatID
                INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADING_RANGE as ragr ON ragr.GradingRangeID = gr.GradingRangeID
                LEFT OUTER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS_ALL as ra ON ra.AbilityRemarkID = gr.AbilityRemarkID
            WHERE
                gr.AbilityRemarkID = '".$_GET["CodeID"]."' ";
    $result = $indexVar['libreportcard']->returnArray($sql);
    
    //list($IndexCat, $IndexGrade, $IndexLowerLimit, $IndexUpperLimit, $IndexRemarks, $IndexRemarkAll, $IndexID) = $result[0];
    list($IndexCat, $IndexGrade, $IndexLowerLimit, $IndexUpperLimit, $IndexRemarks, $IndexRemarkAll, $IndexGradingRange, $IndexCatID) = $result[0];
    $IndexID = $_GET["CodeID"];

    $navigation = $Lang['Btn']['Edit'];
}

# Ability Index Selection
$indexSelection = $indexVar['libreportcard_ui']->Get_No_Remarks_Index_Selection($IndexGradingRange, $IndexCatID, 'changeRemarkAbility()');

# NavigationBar
$navigationAry[] = array($Lang['eReportCardKG']['Setting']['AbilityRemarks']['Title'], 'index.php?task=settings.ability_grade_remark.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>