<?php
// editing by Philips
/*
 * Change Log:
 * 
 */

//if(!isset($IndexID) || !isset($abilityIndexID) || !isset($IndexOriRemark)|| !isset($IndexRemark)) {
if(!isset($abilityIndexID) || !isset($IndexOriRemark)|| !isset($IndexRemark)) {
    header("Location: index.php?task=settings.ability_grade_remark.list");
}
else {
    if($isResetRemark) {
        $IndexRemark = '';
    } else {
        $returnVal = $indexVar['libreportcard']->updateAbilityOriRemark($abilityIndexID, $IndexGradingRange, $IndexOriRemark);
        if(!isset($IndexID) && $returnVal != '') {
            $IndexID = $returnVal;
        }
    }
    
    $result = $indexVar['libreportcard']->updateAbilityRemarkAll($IndexID, $IndexRemark);
    $result = $result? "1" : "0";
    
    header("Location: index.php?task=settings.ability_grade_remark.list&success=$result");
}
?>