<?php
// editing by 
/* 
 * Change Log:
 * Date 2019-10-31 Bill:  class teacher & subject teacher > view related data only
 * Date 2018-09-04 Bill:  Allow Subject Teacher access
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_settings_subjectMapping_YearID", "YearID");
//$arrCookies[] = array("eRCkg_settings_subjectMapping_CatID", "ToolCatID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['SubjectMapping']['Title']);

### Message
if($success) {
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
} else {
	$returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

### ToolBar Action Buttons
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB Table Action Buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

### Sorting define
if($sortBy == '') {
    $sortBy = 'Name';
}
if($sortOrder == '') {
    $sortOrder = 'desc';
}
$sortOptions = array(
    "sortBy" => $sortBy,
    "sortOrder" => $sortOrder
);

## Subject Mapping
$YearID = isset($_POST["YearID"])? $_POST["YearID"] : $_GET["YearID"];

$filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();
$data = $indexVar['libreportcard']->GetAllSubjectMapping(true, '', $YearID, '', $sortOptions, $filterByClassTeacher);

## Class Teacher - Get display form and subject
$needToCheck = false;
$displaySubjectAry = array();
if(!$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && !$indexVar["libreportcard"]->IS_KG_CLASS_TEACHER() && $indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER())
{
    $needToCheck = true;
    if(!empty($data)) { 
        $relatedYearIDArr = Get_Array_By_Key((array)$data, "YearID");
        $relatedYearIDArr = array_values(array_unique(array_filter($relatedYearIDArr)));
        foreach((array)$relatedYearIDArr as $thisYearID) {
            $thisFormSubjectArr = $indexVar["libreportcard"]->Get_KG_Form_Subject($thisYearID);
            if(!empty($thisFormSubjectArr)) {
                $thisFormSubjectArr = Get_Array_By_Key((array)$thisFormSubjectArr, "RecordID");
                $thisFormSubjectArr = array_values(array_unique(array_filter($thisFormSubjectArr)));
                $displaySubjectAry[$thisYearID] = $thisFormSubjectArr;
            }
        }
    }
}

## KG Form
$KGData = array();
$KGForm = $indexVar['libreportcard']->Get_All_KG_Form();
foreach($KGForm as $kgf) {
    $KGData[$kgf['YearID']] = $kgf['YearName'];
}

## Year Selection
$filterByClassTeacherWithSubjects = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER();
$htmlAry['yearSelection'] = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID, 'changeYear()', false, false, 0, $filterByClassTeacherWithSubjects);

## Taiwan Category
$temp = $indexVar['libreportcard']->Get_Taiwan_Category();
$TWCata = BuildMultiKeyAssoc($temp, "CatID");

## Sorting Initialization
$sortCol = array();
$sortImg = "<img src='/images/$sortOrder.gif' />";
if($sortBy == 'Name') {
    $sortName = 'sorted';
} else if($sortBy == 'TT') {
    $sortTT = 'sorted';
}

## Table Content
$table = '<table class="common_table_list_v30" id="ContentTable">';
$table .= '<thead>';
$table .= '<tr>';
	$table .= '<th style="width:3%;">#</th>';
	$table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['SubjectMapping']['Form'].'</th>';
	$table .= '<th style="width:24%;"><a href="javascript:tableSort(1)" class="'.$sortName.'">'.$Lang['eReportCardKG']['Setting']['SubjectMapping']['Subject'].( $sortName ? $sortImg : '').'</a></th>';
	$table .= '<th style="width:20%;"><a href="javascript:tableSort(2)" class="'.$sortTT.'">'.$Lang['eReportCardKG']['Setting']['SubjectMapping']['TermTimeTable'].( $sortTT ? $sortImg : '').'</a></th>';
	$table .= '<th style="width:40%;">'.$Lang['eReportCardKG']['Setting']['AbilityIndex'].'</th>';
	$table .= '<th style="width:3%;"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="CheckAll()"></th>';
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';

## Data Content
$i = 1;
if(!empty($data))
{
	foreach ($data as $_data)
	{
	    if($needToCheck && (!isset($displaySubjectAry[$_data['YearID']]) || !in_array($_data['SubjectID'], (array)$displaySubjectAry[$_data['YearID']]))) {
	        continue;
	    }
	    
		$table .= '<tr>';
			$table .= '<td><span class="rowNumSpan">'.$i.'</span></td>';
			$table .= '<td><span class="rowNumSpan">'.$KGData[$_data['YearID']].'</span></td>';
			$table .= '<td>'.$_data['Name'].'</a></td>';
			$table .= '<td><span class="rowNumSpan">'.$_data['TT'].'</span></td>';
// 			$table .= '<td class="ability_box"><span class="rowNumSpan">';
			$table .= '<td class=""><span class="rowNumSpan">';
			if($_data['CataArr']) {
                foreach($_data['CataArr'] as $_Cat) {
                    $table .='['.$TWCata[$_Cat['AbilityCatID']]['Code']."] &nbsp;".$TWCata[$_Cat['AbilityCatID']]['Name'].'<br>';
			    }
			} else {
			    $table .= '--';
			}
			$table .= '</span></td>';
			$table .= '<td><input type="checkbox" class="checkbox" name="checkbox[]" id="checkbox_'.$_data['CodeID'].'" value="'.$_data['CodeID'].'"></td>';
		$table .= '</tr>';
		$i++;
	}
}

// No learning tool data
if($i == 1) {
	$table .= '<tr>';
		$table .= '<td colspan="7" align="center">';
		$table .= $Lang['General']['NoRecordAtThisMoment'];
		$table .= '</td>';
	$table .= '</tr>';
}

$table .= '</tbody>';
$table .= '</table>';
$htmlAry['dbTable'] = $table;

## Hidden value for sorting
$hidden = "<input type='hidden' id='sortBy' name='sortBy' value='$sortBy' />";
$hidden .= "<input type='hidden' id='sortOrder' name='sortOrder' value='$sortOrder' />";
$htmlAry['hiddenField'] = $hidden;

## JS array
$sortArr = array('', 'Name', 'TT');
?>