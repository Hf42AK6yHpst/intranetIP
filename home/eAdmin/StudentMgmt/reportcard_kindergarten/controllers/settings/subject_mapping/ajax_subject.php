<?php 
global $indexVar;
if($action == 'yearSubject') {
    ## Get Subject Selection
    if($yearID != '') {
        $SubjectSelection = $indexVar['libreportcard_ui']->Get_Subject_Selection($yearID, "", $subjectID, $noFirstTitle=false, $firstTitleType=0, $excludeBehaveLang=true);
    }
    echo $SubjectSelection;
} else if($action == 'yearTopic') {
    ## Get Topic Selection
    if($yearID != '') {
        $TopicSelection = "<td class='field_title'>".$Lang['eReportCardKG']['Setting']['SubjectMapping']['TimeTable']."</td>";
        $TopicSelection .= "<td>" . $indexVar['libreportcard_ui']->Get_Topic_Selection($yearID, "", $TopicID) . "</td>";
    }
    echo $TopicSelection;
} else if($action == 'yearTerm') {
    ## Get Year Term Selection
    $TermSelection = "<td class='field_title'>".$Lang['eReportCardKG']['Setting']['SubjectMapping']['Term']."</td>";
    $TermSelection .= "<td>" . $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID, " ") . "</td>";
    echo $TermSelection;
} else if($action == 'checkSubjectTT') {
    ## Check Term Topic Validation
    if(isset($TopicID)) {
        $errorType = $indexVar['libreportcard']->checkSubjectTT('topic', $TopicID, $SubjectID, $YearID, $CodeID);
    } else {
        $errorType = $indexVar['libreportcard']->checkSubjectTT('term', $TermID, $SubjectID, $YearID, $CodeID);
    }
    echo $errorType;
}
?>