<?php
// editing by 
/*
 * 
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 */

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['SubjectMapping']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

### Navigation
// $navigationAry[] = array($Lang['eReportCardKG']['Setting']['SubjectMapping']['Title'],'index.php?task=settings.subject_mapping.list');
$navigationAry[] = array($_GET['isEdit'] && $_GET['codeID']? $button_edit : $button_new);
$htmlAry["Navigation"] = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Main Paga Data [START]
if ($_GET['isEdit'] && $_GET['codeID'])
{
    $data = $indexVar['libreportcard']->getSubjectMapping($_GET['codeID']);
	$yearID = $data['YearID'];
	$subjectID = $data['SubjectID'];
	if($data['isTerm'] == 1) {
	    $TermID = $data['TermID'];
	} else {
	    $TopicID = $data['TopicID'];
	}
	
	$CataArr = array();
	foreach($data['CataArr'] as $ca){
	    $CataArr[] = $ca['AbilityCatID'];
	}
	$catagoryArr = implode(",", $CataArr);
}

## Taiwan Category
// $temp = $indexVar['libreportcard']->Get_Taiwan_Category();
// $TWCata = BuildMultiKeyAssoc($temp, "CatID");
$CataData = $indexVar['libreportcard']-> Get_Taiwan_Category($yearID);

## Year Selection
$filterByClassTeacherWithSubjects = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER();
$htmlAry["yearSelection"] = $indexVar['libreportcard_ui']->Get_Year_Selection($yearID, $onChange=' YearOnChange(); ', $allKGOptions=false, $noFirstTitle=true, $firstTitleType=0, $filterByClassTeacherWithSubjects);

## Subject Selection
// $htmlAry["yearSelection"] = $indexVar['libreportcard_ui']->Get_Subject_Selection($yearID, '', $subjectID);
### Main Paga Data [END]

### ThickBox Related [START]
$thickBox = '';
$thickBox .= '<div id="CataThickBox" style="display:none">';
	$thickBox .= $indexVar['libreportcard_ui']->Get_Taiwan_Category_Selection($selectTWCat, "CatagoryOnChange()");
	$thickBox .= "<br>";
	$thickBox .= "<br>";
	$thickBox .= "<br>";
	$thickBox .= "<div id=CataSelectionBox align='center'>";
		$thickBox .= "<select multiple name='cata[]' id='cata' size='10'>";
		foreach ($CataData as $_CataData){
			$thickBox .= "<option value= {$_CataData['CatID']}>";
			$thickBox .= "[".$_CataData['Code']."]"."&nbsp".$_CataData['Name'];
			$thickBox .= "</option>";
		}
		$thickBox .= "</select>";
	$thickBox .= "</div>";
	$thickBox .= "<br>";
	$thickBox .= "<div align='center'>";
	   $thickBox .= '<input type="button" value="'.$button_submit.'" class="formbutton_v30 print_hide " onclick="ThickBoxGoSubmit()" id="submitBtn" name="submitBtn">';
	   $thickBox .= '&nbsp;&nbsp;';
       $thickBox .= '<input type="button" value="'.$button_back.'" class="formbutton_v30 print_hide " onclick="ThickBoxClose()" id="backBtn" name="backBtn">';
       $thickBox .= '&nbsp;&nbsp;';
       $thickBox .= '<p class="spacer"></p>';
	$thickBox .= "</div>";
$thickBox .= "</div>";

$thickBox .= "<select multiple id='cata_hidden' size='10' style='display:none;'>";
foreach ($CataData as $_CataData) {
	$thickBox .= "<option value= {$_CataData['CatID']}>";
	$thickBox .= "[".$_CataData['Code']."]"."&nbsp".$_CataData['Name'];
	$thickBox .= "</option>";
}
$thickBox .= "</select>";
$htmlAry["ThickBox"] = $thickBox;
### ThickBox Related [END]

# Build Thickbox
$thickBoxHeight = 300;
$thickBoxWidth = 620;
$htmlAry["ThickBoxLink"] = $indexVar['libreportcard_ui']->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "", $Lang['eReportCardKG']['Setting']['AbilityIndex'], "", 'CataThickBox', $Content='', 'pushMessageButton');

# Button
$htmlAry["Button"] = "";
$htmlAry["Button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_submit, "button", "goSubmit()", "submitBtn");
$htmlAry["Button"] .= "&nbsp;&nbsp;";
$htmlAry["Button"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' id='CodeID' name='CodeID' value='".$_GET['codeID']."' >";
$htmlAry["hiddenInputField"] .= "<input type='hidden' id='CataGoryID' name='CataGoryID' value='".$catagoryArr."' >";

### Required JS CSS - Thickbox
echo $indexVar['libreportcard_ui']->Include_Thickbox_JS_CSS();

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>