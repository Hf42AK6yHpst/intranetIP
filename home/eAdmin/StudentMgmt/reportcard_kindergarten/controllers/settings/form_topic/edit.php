<?php
// editing by 
/*
 * Change Log:
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * Date 2017-02-09 Villa Add navigation bar
 * Date 2017-02-06 Villa Fix En Name/ CH name exchanged
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['FormTopic']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);
echo $indexVar['libreportcard_ui']->Include_Thickbox_JS_CSS();

# Get Topic Data
if ($_GET['isEdit']) {
	$data = $indexVar['libreportcard']->Get_Form_Topic_Record($topicID);
	$topicCode = $data[0]['TopicCode'];
	$topicCodeCH = $data[0]['TopicNameB5'];
	$topicCodeEN = $data[0]['TopicNameEN'];
	$remarks = $data[0]['Remark'];
	$yearID = $data[0]['YearID'];
	$yearName = $data[0]['YearName'];
	$navigation = $Lang['Btn']['Edit'];
}else{
	$navigation = $Lang['Btn']['New'];
}

# Year Selection
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($yearID, false);

# NavigationBar
$navigationAry[] = array($Lang['eReportCardKG']['Setting']['FormTopic']['Title'],'index.php?task=settings.form_topic.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>