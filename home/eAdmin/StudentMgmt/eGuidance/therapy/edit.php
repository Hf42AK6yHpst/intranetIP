<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 * 	2017-07-20 [Cameron]
 * 		- fix bug: should redirect to listview page if TherapyID is empty
 * 
 * 	2017-02-24 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['THERAPY'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($TherapyID)) {
	$therapyID = (count($TherapyID)==1) ? $TherapyID[0] : '';
}
else {
	$therapyID = $TherapyID;
}

if (!$therapyID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();

	
$rs_therapy = $libguidance->getTherapy($therapyID);

if (count($rs_therapy) == 1) {
	$rs_therapy = $rs_therapy[0];
	
	$rs_therapy['Student'] = $libguidance->getTherapyStudent($therapyID);
	
	$rs_therapy['Teacher'] = $libguidance->getTherapyTeacher($therapyID);
}
else {
	header("location: index.php");
}

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageTherapy";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Therapy']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['Therapy'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$linterface->LAYOUT_START();
	
$form_action = "edit_update.php";
include("therapy.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


