<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-05-18 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-02-24 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['THERAPY'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['GroupName'] = standardizeFormPostValue($GroupName);
$dataAry['StartDate'] = $StartDate;
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_THERAPY',$dataAry,array('TherapyID'=>$TherapyID),false);

$ldb->Start_Trans();

## step 1: update therapy
$result[] = $ldb->db_db_query($sql);


## step 2: get therapy student that's not in updated student list
$rs_student = $libguidance->getTherapyStudentID($TherapyID,$StudentID);	// not in list student
if (count($rs_student)) {
	
	## step 3: delete therapy student that's not in updated student list
	$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_STUDENT WHERE TherapyID='".$TherapyID."' AND StudentID IN ('".implode("','",(array)$rs_student)."')";
	$result[] = $ldb->db_db_query($sql);
	
	## step 4: delete therapy student activity result that's not in updated student list
	$sql = "SELECT 
					r.ActivityID, r.StudentID 
			FROM 
					INTRANET_GUIDANCE_THERAPY_ACTIVITY_RESULT r
			INNER JOIN 
					INTRANET_GUIDANCE_THERAPY_ACTIVITY a ON a.ActivityID=r.ActivityID
			WHERE 	
					a.TherapyID='".$TherapyID."'
			AND 	StudentID IN ('".implode("','",(array)$rs_student)."')";
			
	$rs_activity_result = $ldb->returnResultSet($sql);
	if (count($rs_activity_result)) {
		foreach((array)$rs_activity_result as $r) {
			$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY_RESULT WHERE ActivityID='".$r['ActivityID']."' AND StudentID='".$r['StudentID']."'";
			$result[] = $ldb->db_db_query($sql);
		}
	}
}


$currentStudentList = $libguidance->getTherapyStudent($TherapyID);
$currentStudentList = BuildMultiKeyAssoc($currentStudentList, 'UserID',array('UserID'),1);

$activityAry = $libguidance->getTherapyActivity($TherapyID);

## step 5: update therapy student list
foreach((array)$StudentID as $id) {
	if (!in_array($id,$currentStudentList)) {	// add new therapy student record
		unset($dataAry);
		$dataAry['TherapyID'] = $TherapyID;
		$dataAry['StudentID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_THERAPY_STUDENT',$dataAry,array(),false,true,false);	// insert ignore
		$result[] = $ldb->db_db_query($sql);

		## step 6:
		## case when add new student to therapy, but there's already activity created
 		## add empty result for the student		
		foreach((array)$activityAry as $ls) {
			unset($dataAry);
			$dataAry['ActivityID'] = $ls['ActivityID'];
			$dataAry['StudentID'] = $id;
			$dataAry['Result'] = '';
			$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_THERAPY_ACTIVITY_RESULT',$dataAry,array(),false,false,false);
			$result[] = $ldb->db_db_query($sql);
		}
	}
}


## step 7: get therapy teacher that's not in updated teacher list
$rs_teacher = $libguidance->getTherapyTeacherID($TherapyID,$TeacherID);
if (count($rs_teacher)) {
	## step 8: delete therapy teacher that's not in updated teacher list
	$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_TEACHER WHERE TherapyID='".$TherapyID."' AND TeacherID IN ('".implode("','",$rs_teacher)."')";
	$result[] = $ldb->db_db_query($sql);
}

## step 9: update therapy teacher
foreach((array)$TeacherID as $id) {
	unset($dataAry);
	$dataAry['TherapyID'] = $TherapyID;
	$dataAry['TeacherID'] = $id;
	$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_THERAPY_TEACHER',$dataAry,array(),false,true,false);	// insert ignore
	$result[] = $ldb->db_db_query($sql);
}

## step 10: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$TherapyID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


