<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 * 	2017-11-13 [Cameron]
 * 		- build $classSelection for ej as it uses ClassName and YearName in db
 *  
 * 	2017-09-11 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['CLASSTEACHERMEETING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$libFCM = new form_class_manage();
$yearInfoArr = $libFCM->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
$yearInfoArr = current($yearInfoArr);
$academicYearStart = substr($yearInfoArr['AcademicYearStart'],0,10);
$academicYearEnd = substr($yearInfoArr['AcademicYearEnd'],0,10);

$lclass = new libclass();
if ($junior_mck) {
	$yearName = $yearInfoArr['YearNameEN'];
	$classList = $lclass->getClassList();	// class list of current academic year
	$classNameAry = array();
	if (count($classList)) {
		foreach((array)$classList as $c){
			$classNameAry[] = array($c['ClassName'], $c['ClassName']);
		}
	}
	$classSelection = getSelectByArray($classNameAry, "name='ClassName' id='ClassName' ");		// ej store ClassName and YearName in db
}
else {
	$classSelection = $lclass->getSelectClassID("name='YearClassID' id='YearClassID' ");		// ip store YearClassID in db
}
$rs_ctmeeting = array();
$meetingID = '';
$yearClassID = '';

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$linterface->LAYOUT_START();


$form_action = "new_update.php";
include("classteacher_meeting.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


