<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-11-13 [Cameron]
 * 		- build $classSelection for ej as it uses ClassName and YearName in db
 *  
 * 	2017-09-11 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['CLASSTEACHERMEETING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($MeetingID)) {
	$meetingID = (count($MeetingID)==1) ? $MeetingID[0] : '';
}
else {
	$meetingID = $MeetingID;
}

if (!$meetingID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();


$lclass = new libclass();
$rs_ctmeeting = array();
$rs_ctmeeting = $libguidance->getClassTeacherMeeting($meetingID);

if (count($rs_ctmeeting) == 1) {
	$rs_ctmeeting = $rs_ctmeeting[0];
	$libFCM = new form_class_manage();
	if ($junior_mck) {
		$yearName = $rs_ctmeeting['YearName'];
		$className = $rs_ctmeeting['ClassName'];
		
		$yearInfoArr = $libFCM->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
		$yearInfoArr = current($yearInfoArr);
		$currentYearName = $yearInfoArr['YearNameEN'];

		if ($yearName == $currentYearName) {		// current year
			$classList = $lclass->getClassList();	// class list of current academic year
			$academicYearStart = substr($yearInfoArr['AcademicYearStart'],0,10);
			$academicYearEnd = substr($yearInfoArr['AcademicYearEnd'],0,10);		
		}
		else {
			$classList = $libguidance->getPastClassList($yearName);
			$academicYearID = $libguidance->getAcademicYearIDByName($yearName);
			$yearInfoArr = $libFCM->Get_Academic_Year_List($academicYearID);
			$yearInfoArr = current($yearInfoArr);
			$academicYearStart = substr($yearInfoArr['AcademicYearStart'],0,10);
			$academicYearEnd = substr($yearInfoArr['AcademicYearEnd'],0,10);		
		}
		
		$classNameAry = array();
		if (count($classList)) {
			foreach((array)$classList as $c){
				$classNameAry[] = array($c['ClassName'], $c['ClassName']);
			}
		}
		$classSelection = getSelectByArray($classNameAry, "name='ClassName' id='ClassName' ", $className);		// ej store ClassName and YearName in db
		
	}
	else {
		$yearClassID = $rs_ctmeeting['YearClassID'];
		$classSelection = $lclass->getSelectClassID("name='YearClassID' id='YearClassID' ", $yearClassID);
		
		$libYC = new year_class($yearClassID);
		$yearInfoArr = $libFCM->Get_Academic_Year_List($libYC->AcademicYearID);
		$yearInfoArr = current($yearInfoArr);
		$academicYearStart = substr($yearInfoArr['AcademicYearStart'],0,10);
		$academicYearEnd = substr($yearInfoArr['AcademicYearEnd'],0,10);		
	}
}
else {
	header("location: index.php");
}

$academicYearStart = substr($academicYearStart,0,10);
$academicYearEnd = substr($academicYearEnd,0,10);

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$linterface->LAYOUT_START();


$form_action = "edit_update.php";
include("classteacher_meeting.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


