<?
/*
 * 	Log
 *
 *	2017-11-14 [Cameron]
 *		- ej uses ClassName, ip uses YearClassID in class selection option list
 *		- add meeting date checking against academic year duration
 *
 * 	2017-09-11 [Cameron]
 * 		create this file
 */
?>
<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>

<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">

<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
<? if ($junior_mck):?>	
	if ($('#ClassName').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'ClassName';
 		$('#ErrYearClassID').addClass('error_msg_show').removeClass('error_msg_hide');
	}
<? else: ?>
	if ($('#YearClassID').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'YearClassID';
 		$('#ErrYearClassID').addClass('error_msg_show').removeClass('error_msg_hide');
	}
<? endif; ?>
	
 	if (!check_date(obj.MeetingDate,"<?php echo $i_invalid_date; ?>")) {
 		return false;
 	}
 	
 	if ((compareDate($('#MeetingDate').val(),"<?=$academicYearStart?>") == -1) || (compareDate("<?=$academicYearEnd?>",$('#MeetingDate').val()) == -1)) {
 		error++;
 		if (focusField == '') focusField = 'MeetingDate';
 		$('#ErrMeetingDate').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
 	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

$(document).ready(function(){
	
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

<? if ($junior_mck):?>
	$('#ClassName').change(function(){
		if ($('#ClassName').val() != '') {
	        isLoading = true;
			$('#ClassStudent').html(loadingImg);
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '../ajax/ajax.php',
				data : {
					'action': 'getEJClassStudent',
					'YearName': $('#YearName').val(),
					'ClassName': $('#ClassName').val(),
					'MeetingID': $('#MeetingID').val()
				},		  
				success: update_student_list,
				error: show_ajax_error
			});
		}
		else {
			$('#ClassStudent').html('');
		}		
	});
	
<? else: ?>	
	$('#YearClassID').change(function(){
		if ($('#YearClassID').val() > 0) {
	        isLoading = true;
			$('#ClassStudent').html(loadingImg);
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '../ajax/ajax.php',
				data : {
					'action': 'getClassStudent',
					'YearClassID': $('#YearClassID').val(),
					'MeetingID': $('#MeetingID').val()
				},		  
				success: update_student_list,
				error: show_ajax_error
			});
		}
		else {
			$('#ClassStudent').html('');
		}		
	});
<? endif;?>

	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		
		if (checkForm(document.form1)) {
	        $('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

});


function notify_colleague_update(recordID) {
	if (checkForm(document.form1)) {
		tb_show('<?=$Lang['eGuidance']['NotifyUpdate']?>','../ajax/ajax_layout.php?action=NotifyColleagueUpdate&RecordID='+$('#'+recordID).val()+'&RecordType=CT'+recordID+'&height=450&width=650');
	}
}

function show_history_push_message() {
	tb_show('<?=$Lang['eGuidance']['PushMessage']['ViewStatus']?>','../ajax/ajax_layout.php?action=GetHistoryNotification&RecordID='+$('#MeetingID').val()+'&RecordType=CTMeeting&height=450&width=650');		
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#ClassStudent').html(ajaxReturn.html);
		isLoading = false;
	}
}


</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Class']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$classSelection?>
									<span class="error_msg_hide" id="ErrYearClassID"><?=$Lang['eGuidance']['Warning']['SelectClass']?></span>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['ctmeeting']['MeetingDate']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("MeetingDate",(empty($rs_ctmeeting['MeetingDate'])?date('Y-m-d'):$rs_ctmeeting['MeetingDate']))?>
									<span class="error_msg_hide" id="ErrMeetingDate"><?=sprintf($Lang['eGuidance']['Warning']['MeetingDate'],$academicYearStart,$academicYearEnd)?></span>
								</td>
							</tr>
							<tr valign="top">
								<td colspan="2"></td>
							</tr>
							
							<tr valign="top">
								<td colspan="2"><?=$linterface->GET_NAVIGATION2($Lang['eGuidance']['ctmeeting']['Comments'])?></td>
							</tr>
							<tr valign="top">
								<td colspan="2">
									<?=($junior_mck ? $libguidance_ui->getClassTeacherCommentTable($meetingID, $yearClassID='', $view=false, $yearName, $className) : $libguidance_ui->getClassTeacherCommentTable($meetingID,$yearClassID))?>		
								</td>
							</tr>
							
							<?=$libguidance_ui->getAttachmentLayout('CTMeeting',$meetingID)?>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>

						<? if ($meetingID): ?>
							<tr valign="top">
								<td class="tabletext" colspan="2"><a href="javascript:void(0)" id="HistoryNotify" onClick="show_history_push_message()"><?=$Lang['eGuidance']['PushMessage']['ViewStatus']?></a></td>
							</tr>
						<? endif;?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" id="MeetingID" name="MeetingID" value="<?=$meetingID?>">
<input type="hidden" id="hidNotifierID" name="hidNotifierID" value="">
<input type="hidden" id="submitMode" name="submitMode" value="submit">
<? if ($junior_mck):?>
	<input type="hidden" id="YearName" name="YearName" value="<?=$yearName?>">
<? endif;?>
</form>


<?=$libguidance_ui->getAttachmentUploadForm('CTMeeting',$meetingID)?>
