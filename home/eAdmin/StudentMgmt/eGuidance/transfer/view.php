<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 *  2018-05-25 [Cameron]
 *      - show student name without class
 *      
 * 	2017-08-04 [Cameron]
 * 		- add lock icon to indicate confidential record, don't allow to view if User is not in ConfidentialViewerID list of the record
 * 
 * 	2017-07-21 [Cameron]
 * 		- fix bug: should redirect to listview page if TransferID is empty
 * 
 * 	2017-03-21 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['TRANSFER']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$TransferID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();		// getTransferFrom() need this

$transferID = $TransferID;
$rs_transfer = $libguidance->getTransfer($transferID);
$showOther = false;
$transferReasonOther = '';

if (count($rs_transfer) == 1) {
	$rs_transfer = $rs_transfer[0];

	$transferReasonAry = explode('^~',$rs_transfer['TransferReason']);
	foreach((array)$transferReasonAry as $k=>$v) {
		if (strlen($v)>6 && substr($v,0,7)=="Other^:") {
			$transferReasonAry[$k] = 'Other';
			$transferReasonOther = substr($v,7);
			$showOther = true;
		}
	}
	
	$confidentialViewerInfo = '';
	if ($rs_transfer['IsConfidential']) {
		$confidentialViewerID = $rs_transfer['ConfidentialViewerID'];
		if (!in_array($_SESSION['UserID'],explode(',',$confidentialViewerID))) {
			header("location: index.php");
		}
		
		if ($confidentialViewerID) {
			$cond = ' AND UserID IN('.$confidentialViewerID.')';
			$confidentialViewerID = $libguidance->getTeacher('-1',$cond);
			$confidentialViewerInfo = $libguidance_ui->getUserNameList($confidentialViewerID);
		}
	}
	
	$rs_class_name = $libguidance->getClassByStudentID($rs_transfer['StudentID']);
	if (!empty($rs_class_name)) {
		$studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_transfer['StudentID']),'',true);
		$studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
	}
	else {
	    $studentInfo = $libguidance->getStudent($rs_transfer['StudentID']);
	    $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
	}
}
else {
	header("location: index.php");
}

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageTransfer";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Transfer']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['Transfer'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$linterface->LAYOUT_START();

?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?></td>
								<td class="tabletext"><?=$studentInfo?> <?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferDate']?></td>
								<td class="tabletext"><?=((empty($rs_transfer['TransferDate']) || ($rs_transfer['TransferDate'] == '0000-00-00'))?'-':$rs_transfer['TransferDate'])?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferFrom']?></td>
								<td class="tabletext">
								<?
									$x = $libguidance_ui->getTransferFrom($rs_transfer,'disabled');
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferTo']?></td>
								<td class="tabletext">
								<?
									$x = $libguidance_ui->getTransferTo($rs_transfer,'disabled');
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['IsClassTeacherKnow']?></td>
								<td class="tabletext">
									<input type="radio" name="IsClassTeacherKnow" id="Know" disabled value="1"<?=($rs_transfer['IsClassTeacherKnow']?' checked':'')?>><?=$Lang['General']['Yes']?><br>
									<input type="radio" name="IsClassTeacherKnow" id="NotKnow" disabled value="0"<?=(isset($rs_transfer['IsClassTeacherKnow']) && $rs_transfer['IsClassTeacherKnow']==0?' checked':'')?>><?=$Lang['General']['No']?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferReason']?></td>
								<td class="tabletext">
								<?
									$transfer_reason = $libguidance->getGuidanceSettingItem('Transfer','TransferReason');
									$x = '';
									foreach((array)$transfer_reason as $k=>$v) {
										$x .= '<input type="checkbox" name="TransferReason[]" id="TransferReason_'.$k.'" disabled value="'.$k.'"'. (in_array($k,(array)$transferReasonAry)?'checked':'').'>'.$v;
										$x .= ($k == 'Other') ? '':'<br>';  
									}
									echo $x;
								?>
									<input type="text" name="TransferReasonOther" id="TransferReasonOther" disabled value="<?=intranet_htmlspecialchars($transferReasonOther)?>" style="width:60%; display:<?=($showOther?'':'none')?>">
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Confidential']?><img src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock_own.gif" style="display:<?=($rs_transfer['IsConfidential']?'':'none')?>"></td>
								<td class="tabletext">
									<input type="radio" name="IsConfidential" id="IsConfidentialYes" value="1"<?=$rs_transfer['IsConfidential']?' checked':''?> disabled><label for="IsConfidentialYes"><?=$Lang['General']['Yes']?></label>
									<input type="radio" name="IsConfidential" id="IsConfidentialNo" value="0"<?=$rs_transfer['IsConfidential']?'':' checked'?> disabled><label for="IsConfidentialNo"><?=$Lang['General']['No']?></label>
									
									<div id="ConfidentialSection">
										<?=$confidentialViewerInfo?>
									</div>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['Remark']?></td>
								<td class="tabletext"><?=$rs_transfer['Remark'] ? nl2br($rs_transfer['Remark']) : '-'?></td>
							</tr>
						
							<?=$libguidance_ui->getAttachmentLayout('Transfer',$transferID,'view')?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
							<? 	if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['TRANSFER'])) {
									echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit.php?TransferID=$transferID'").'&nbsp';
								}
								echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");							
							?>									
									</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="StudentID" id="StudentID" value="<?=$rs_transfer['StudentID']?>">

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>