<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 *	2017-08-04 [Cameron]
 *		- handle submitAndNotify (send 'referral' notification to colleague)
 * 		- add TransferToType, TransferTo, IsConfidential and ConfidentialViewerID
 *
 * 	2017-05-19 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-03-08 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['TRANSFER'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$dataAry['TransferDate'] = $TransferDate;
$dataAry['TransferFromType'] = $TransferFromType;
switch($TransferFromType) {
	case 'Self':
		$transferFrom = '';
		break;
	case 'Classmate':
		$transferFrom = $FromStudentID;
		break;
	default:
		$transferFrom = ${"From$TransferFromType"};
		break;
}
$dataAry['TransferFrom'] = standardizeFormPostValue($transferFrom);

$dataAry['TransferToType'] = $TransferToType;
$transferTo = ${"To$TransferToType"};
$dataAry['TransferTo'] = standardizeFormPostValue($transferTo);

$dataAry['IsClassTeacherKnow'] = $IsClassTeacherKnow;
$transferReason = implode("^~",(array)$TransferReason);
if (in_array('Other',(array)$TransferReason)) {
	$transferReason .= "^:".$TransferReasonOther;
}
$dataAry['TransferReason'] = standardizeFormPostValue($transferReason);
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['IsConfidential'] = $IsConfidential;
if ($IsConfidential && !in_array($_SESSION['UserID'], (array)$ConfidentialViewerID)) {
	$ConfidentialViewerID[] = $_SESSION['UserID'];				// User who add the record are granted permission as default 
}
$dataAry['ConfidentialViewerID'] = implode(",",(array)$ConfidentialViewerID);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];

$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_TRANSFER',$dataAry,array('TransferID'=>$TransferID),false);

$ldb->Start_Trans();
## step 1: update transfer
$result[] = $ldb->db_db_query($sql);

## step 2: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$TransferID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();

	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($TransferID, $recordType='TransferID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'UpdateSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


