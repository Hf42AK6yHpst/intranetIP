<?
/*
 * 	Log
 *
 *  2018-05-25 [Cameron]
 *      - show student info and disable student selection for edit [case #M138226]
 *      
 *	2017-08-04 [Cameron]
 *		- add confidential field
 *		- add save and notify button
 * 		- add TransferToType and TransferTo
 *
 * 	2017-06-29 [Cameron]
 * 		- add label for radio button and checkbox item so that clicking lable is the same as clicking the radio button
 * 		- add loadingImg
 *  
 * 	2017-05-19 [Cameron]
 * 		fix bug: disable button after submit to avoid adding duplicate record
 */
?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">

<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	if(!check_text(obj.StudentID,'<?=$Lang['eGuidance']['Warning']['SelectStudent']?>'))
	{
		return false;
	}

 	if (!check_date(obj.TransferDate,"<?php echo $i_invalid_date; ?>")) {
 		return false;
 	}

	if ($(':input[name="TransferReason\[\]"]:checked').length <= 0) {
		alert('<?=$Lang['eGuidance']['transfer']['Warning']['SelectTransferReason']?>');
		return false;
	}

	if ($('#TransferReason_Other').attr('checked') && $('#TransferReasonOther').val() == '') {
		alert('<?=$Lang['eGuidance']['transfer']['Warning']['InputTransferReason']?>');
		$('#TransferReasonOther').focus();
		return false;
	}
	
	if (($('#Classmate').is(':checked')) && ($('#FromStudentID').val() == $('#StudentID').val())) {
		alert('<?=$Lang['eGuidance']['transfer']['Warning']['TransferFromClassmateButSelf']?>');
		$('#StudentID').focus();
		return false;
	}
	
 	if ($('#IsConfidentialYes').is(':checked')) {
 		if ($('#ConfidentialViewerID option').length == 0) {
		    alert('<?=$Lang['eGuidance']['Warning']['SelectConfidentialViewer']?>');
		    return false;
 		} 
 	}
	
	return true;
}

$(document).ready(function(){
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	$('#ClassName').change(function(){
        isLoading = true;
		$('#StudentNameSpan').html(loadingImg);
	
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '../ajax/ajax.php',
			data : {
				'action': 'getStudentNameByClass',
				'ClassName': $('#ClassName').val(),
				'onChange': 'changeClassmate()'
			},		  
			success: update_student_list,
			error: show_ajax_error
		});
		
	});

	$('#FromClassName').change(function(){
		getClassmate();
	});
	
	$(':input[name="TransferFromType"]').change(function() {

		var from = $(this).attr('id');
		hideTransferFrom();
		
		switch(from) {
			case 'Self':
				break;
			case 'Classmate':
				$('#FromStudentSpan').css("display","");
				break;
			default:
				$('#From'+from).css("display","");
				break;
		}
	});
	
	$('#TransferReason_Other').click(function(){
		if ($('#TransferReason_Other').attr('checked')) {
			$('#TransferReasonOther').css("display","");
		}
		else {
			$('#TransferReasonOther').css("display","none");
		}
	});

	$(':input[name="TransferToType"]').change(function() {

		var to = $(this).attr('id');
		hideTransferTo();
		$('#To'+to).css("display","");
	});

	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		if ($('#IsConfidentialYes').is(':checked')) {
			$('#ConfidentialViewerID option').attr('selected',true);
		}

		if (checkForm(document.form1)) {
	        $('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
		
});

function changeClassmate() {
	if (($('#Classmate').is(':checked')) && ($('#FromClassName').val() == $('#ClassName').val())) {
		getClassmate();
	}		
}

function getClassmate() {
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	if ($('#StudentID').val() == '') {
		alert('<?=$Lang['eGuidance']['Warning']['SelectStudent']?>');
		$('#FromClassName').val('');
		isLoading = false;
	}
	else {
        isLoading = true;
		$('#FromStudentNameSpan').html(loadingImg);
	
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '../ajax/ajax.php',
			data : {
				'action': 'getFromStudentNameByClass',
				'ClassName': $('#FromClassName').val(),
				'StudentID': $('#StudentID').val()
			},		  
			success: update_from_student_list,
			error: show_ajax_error
		});
	}
}

function notify_colleague_referral(recordID) {
	if ($('#IsConfidentialYes').is(':checked')) {
		$('#ConfidentialViewerID option').attr('selected',true);
	}
	if (checkForm(document.form1)) {
		tb_show('<?=$Lang['eGuidance']['NotifyReferral']?>','../ajax/ajax_layout.php?action=NotifyColleagueReferral&RecordID='+$('#'+recordID).val()+'&RecordType='+recordID+'&height=450&width=650');
	}
}

function show_history_push_message() {
	tb_show('<?=$Lang['eGuidance']['PushMessage']['ViewStatus']?>','../ajax/ajax_layout.php?action=GetHistoryNotification&RecordID='+$('#TransferID').val()+'&RecordType=Transfer&height=450&width=650');		
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function update_from_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#FromStudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function hideTransferFrom() {
	$(':input[name="TransferFromType"]').each(function(){
		var from = $(this).attr('id');
		if (from != 'Self') {
			$('#From'+from).css("display","none");
		}
	});
	if ($('#FromStudentSpan').length) {
		$('#FromStudentSpan').css("display","none");
	}
}

function hideTransferTo() {
	$(':input[name="TransferToType"]').each(function(){
		var to = $(this).attr('id');
		$('#To'+to).css("display","none");
	});
}

</script>
<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext">
								<?php if ($form_action == 'edit_update.php'):?>
									<?php echo $studentInfo;?>
								<?php else:?>
									<span><?=$Lang['eGuidance']['Class']?></span>&nbsp;<?=$classSelection?> &nbsp;
									<span><?=$Lang['eGuidance']['StudentName']?></span>&nbsp;<span id="StudentNameSpan"><?=$studentSelection?></span>
								<?php endif;?>	 
									<?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferDate']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("TransferDate",(empty($rs_transfer['TransferDate'])?date('Y-m-d'):$rs_transfer['TransferDate']))?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferFrom']?>
									</td>
								<td class="tabletext">
								<?
									$x = $libguidance_ui->getTransferFrom($rs_transfer);
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferTo']?>
									</td>
								<td class="tabletext">
								<?
									$x = $libguidance_ui->getTransferTo($rs_transfer);
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['IsClassTeacherKnow']?>
									</td>
								<td class="tabletext">
									<input type="radio" name="IsClassTeacherKnow" id="Know" value="1"<?=($rs_transfer['IsClassTeacherKnow']?' checked':'')?>><label for="Know"><?=$Lang['General']['Yes']?></label><br>
									<input type="radio" name="IsClassTeacherKnow" id="NotKnow" value="0"<?=(isset($rs_transfer['IsClassTeacherKnow']) && $rs_transfer['IsClassTeacherKnow']==0?' checked':'')?>><label for="NotKnow"><?=$Lang['General']['No']?></label>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['TransferReason']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext">
								<?
									$transfer_reason = $libguidance->getGuidanceSettingItem('Transfer','TransferReason');
									$x = '';
									foreach((array)$transfer_reason as $k=>$v) {
										$x .= '<input type="checkbox" name="TransferReason[]" id="TransferReason_'.$k.'" value="'.$k.'"'. (in_array($k,(array)$transferReasonAry)?'checked':'').'><label for="TransferReason_'.$k.'">'.$v.'</label>';
										$x .= ($k == 'Other') ? '':'<br>';  
									}
									echo $x;
								?>
									<input type="text" name="TransferReasonOther" id="TransferReasonOther" value="<?=intranet_htmlspecialchars($transferReasonOther)?>" style="width:60%; display:<?=($showOther?'':'none')?>">
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Confidential']?><img id="ConfidentialIcon" src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock.gif" style="display:<?=($rs_transfer['IsConfidential']?'':'none')?>"></td>
								<td class="tabletext">
									<input type="radio" name="IsConfidential" id="IsConfidentialYes" value="1"<?=$rs_transfer['IsConfidential']?' checked':''?>><label for="IsConfidentialYes"><?=$Lang['General']['Yes'].'('.$Lang['eGuidance']['Warning']['SelectAuthorizedUser'].')'?></label>
									<input type="radio" name="IsConfidential" id="IsConfidentialNo" value="0"<?=$rs_transfer['IsConfidential']?'':' checked'?>><label for="IsConfidentialNo"><?=$Lang['General']['No']?></label>
									
									<div id="ConfidentialSection" style="display:<?=($rs_transfer['IsConfidential']?'':'none')?>">
										<?=$libguidance_ui->getConfidentialViewerSelection($rs_transfer['ConfidentialViewerID'])?>
									</div>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['transfer']['Remark']?>
									</td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("Remark", $rs_transfer['Remark'], 80, 10)?></td>
							</tr>
						
							<?=$libguidance_ui->getAttachmentLayout('Transfer',$transferID)?>
						
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
						<? if ($transferID): ?>
							<tr valign="top">
								<td class="tabletext" colspan="2"><a href="javascript:void(0)" id="HistoryNotify" onClick="show_history_push_message()"><?=$Lang['eGuidance']['PushMessage']['ViewStatus']?></a></td>
							</tr>
						<? endif;?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['SubmitAndNotifyReferral'], "button", "notify_colleague_referral('TransferID')","btnSubmitAndNotify", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" id="TransferID" name="TransferID" value="<?=$transferID?>">
<input type="hidden" id="hidNotifierID" name="hidNotifierID" value="">
<input type="hidden" id="submitMode" name="submitMode" value="submit">
<?php if ($form_action == 'edit_update.php'):?>
	<input type="hidden" id="StudentID" name="StudentID" value="<?php echo $rs_transfer['StudentID'];?>">
<?php endif;?>

</form>

<?=$libguidance_ui->getAttachmentUploadForm('Transfer',$transferID)?>