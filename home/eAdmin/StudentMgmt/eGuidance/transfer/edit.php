<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-25 [Cameron]
 * - retrieve student info and disable student selection for edit [case #M138226]
 *
 * 2017-08-04 [Cameron]
 * - add TransferToType, TransferTo, IsConfidential and ConfidentialViewerID
 * - don't allow to edit if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-20 [Cameron]
 * - fix bug: should redirect to listview page if TransferID is empty
 *
 * 2017-03-08 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['TRANSFER'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (is_array($TransferID)) {
    $transferID = (count($TransferID) == 1) ? $TransferID[0] : '';
} else {
    $transferID = $TransferID;
}

if (! $transferID) {
    header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();

$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(), "name='StudentID' id='StudentID'");

$rs_transfer = $libguidance->getTransfer($transferID);
$showOther = false;
$transferReasonOther = '';

if (count($rs_transfer) == 1) {
    $rs_transfer = $rs_transfer[0];
    
    $transferReasonAry = explode('^~', $rs_transfer['TransferReason']);
    foreach ((array) $transferReasonAry as $k => $v) {
        if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
            $transferReasonAry[$k] = 'Other';
            $transferReasonOther = substr($v, 7);
            $showOther = true;
        }
    }
    
    if ($rs_transfer['IsConfidential']) {
        $confidentialViewerID = $rs_transfer['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $rs_transfer['ConfidentialViewerID'] = $libguidance->getTeacher('-1', $cond);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($rs_transfer['StudentID']);
    if (! empty($rs_class_name)) {
        // $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ",$rs_class_name);
        // $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name);
        // $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $rs_transfer['StudentID']);
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_transfer['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    } else {
        $studentInfo = $libguidance->getStudent($rs_transfer['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
} else {
    header("location: index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageTransfer";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array(
    $Lang['eGuidance']['menu']['Management']['Transfer']
);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['menu']['Management']['Transfer'],
    "index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include ("transfer.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


