<?
/**
 * modifying: 
 * 
 * Change Log:
 * 
 * 2017-11-13	Cameron
 * 		- ej should use getClassName(), ip uses getClassNameByLang()
 * 		- fix meta charset
 * 		- add followup column
 * 
 * 2017-09-15  	Cameron
 *  	- Create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

######## Init START ########
intranet_auth();
intranet_opendb();

$lclass = new libclass();
$libguidance = new libguidance();
$libguidance_ui = new libguidance_ui();
$resultData = array();
$reportDetails = array();

define('TABLE_TYPE_EMPTY', 'tableEmpty');
define('TABLE_TYPE_HORIZONTAL', 'tableHorizontal');
define('TABLE_TYPE_VERTICAL', 'tableVertical');


$ClassName = $_POST['ClassName'];
$AcademicYearID = $_POST['AcademicYearID'];

######## Init END ########


######## Access Right START ########
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('VIEW', (array)$permission['current_right']['REPORTS']['CLASSTEACHERREPORT'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
######## Access Right END ########



######## Init PDF START ########
$mpdf = new mPDF('','A4',0,'',5,5,5,15);
//$mpdf->mirrorMargins = 1;
######## Init PDF END ########


######## Load all Data START ########
#### Load Header Data START ####
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$report_title = "{$Lang['eGuidance']['report']['eGuidanceReport']} - {$Lang['eGuidance']['menu']['Reports']['ClassteacherReport']}";

#### Load Header Data END ####

#### Get Class Teacher Report START ####
$rs_ctreport = $libguidance->getClassTeacherReport($ClassName,$AcademicYearID);
//sortByColumn2(&$rs_ctreport,'MeetingDate');

//debug_pr($rs_ctreport);
$studentInfo = BuildMultiKeyAssoc($rs_ctreport, 'UserID', array('WebSAMSRegNo','StudentName'));
$commentInfo = BuildMultiKeyAssoc($rs_ctreport, array('UserID','MeetingDate'), array('ClassName','ClassNumber','Comment','Followup'));

//debug_pr($studentInfo);
//debug_pr($commentInfo);
//exit;

#### Get Class Teacher Report END ####

## Get Class Name Start ###
	$ClassID = $lclass->getClassID($ClassName);
	$ClassNameByLang = $junior_mck ? convert2unicode($lclass->getClassName($ClassID),true,1) : $lclass->getClassNameByLang($ClassID);
	
## Get Class Name End ###


######## Load all Data END ########


######## Pack data START ########
/**
 * Demo data
$table = 
	array(
		array(
		    'StudentRegNo' 	=> '#1511041',
		    'StudentName'	=> 'Sally Tang',
		    'data' 			=> array(
		    						array('5B', '1', '2017-05-12', 'Careless in lesson', 'follow-up'),
		                			array('4A', '2', '2016-10-08', 'Over-Talkative', 'meet parent'),
		                			array('3C', '3', '2015-11-04', 'Often conflict with others', 'refer to social worker')
		    					)
		    ),
		array(
		    'StudentRegNo' 	=> '#1511042',
		    'StudentName'	=> 'Dona Liu',
		    'data' 			=> array(
			                		array('5B', '2', '2017-05-12', 'Angry all the times', 'refer to psychologist'),
			                		array('4C', '10', '2016-10-08', 'Often crying', 'interview'),
			                		array('1B', '8', '2014-02-20', 'Silent, overdue homework', 'inform parent'),
			                		array('1B', '8', '2013-11-11', 'Depression', '-')
    							)
    		)    					
	);
 */
foreach($studentInfo as $studentID=>$rs){
    
    $data = array();
    foreach((array)$commentInfo[$studentID] as $meetingDate=>$com){
        $data[] = array(
            $com['ClassName'],
            $com['ClassNumber'],
            $meetingDate,
            $com['Comment'],
            $com['Followup']?$com['Followup']:'-'
        );
    }

    $table = array(
        'StudentRegNo' 	=> $rs['WebSAMSRegNo'],
        'StudentName'	=> $rs['StudentName'],
        'data' 			=> $data);
        
    $resultData[] = $table;
}
$countResultData = count($resultData);

$tableTitle = array( 
 	$Lang['eGuidance']['ctmeeting']['StudentRegNo'],
    $Lang['eGuidance']['ctmeeting']['StudentName'],
    $Lang['eGuidance']['Class'],
    $Lang['General']['ClassNumber'],
    $Lang['eGuidance']['ctmeeting']['MeetingDate'],
    $Lang['eGuidance']['ctmeeting']['Comments'],
    $Lang['eGuidance']['ctmeeting']['Followup']
);

######## Pack data END ########

################################ Load header to PDF START ################################
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=($junior_mck) ? 'big5-hkscs' : 'utf-8'?>" />
<title></title>
<style type="text/css">

body{color: #000;  font-family:  msjh !important; font-size:0.9em; line-height:0.9em; margin:0; padding:0; -webkit-print-color-adjust: exact; font-stretch: condensed; /*font-size-adjust: 0.5;*/}

/**** Header style START ****/
.schoolName, .reportType, .reportDetails{
    text-align: center;
}
.reportDetails{
    margin-top: 0;
}
.printTime{
    text-align: right;
}
/**** Header style END ****/


/**** Footer style START ****/
.footer{
    width: 100%;
}
.footer td{
    width: 33%;
    vertical-align: bottom; 
    font-family:  msjh !important;
    font-size: 8pt; 
    color: #000000; 
    font-weight: bold;
    padding: 3px;
    border: 0;
    border-top: 1px solid black;
}
/**** Footer style END ****/


/**** Basic table style START ****/
table{
    width: 200mm;
    border-collapse: collapse;
    margin-top: 3mm;
    overflow: visible;
}
.noRecordTd{
    text-align: center;
}

th, td{
    border: 1px solid #ddd;
    text-align: left;
    vertical-align: top;
    padding: 3mm;
}
tr.headerRow th{
    font-size: 1.5em;
    background-color: #487db4;
    color: white;
    text-align: center;
}

.field{
    text-align: center;
    white-space: nowrap;
    color: white;
    background-color: #59afc6;
}

.tableIndex td, .tableEmpty td{
    border: 0;
}
.tableEmpty td{
    font-size:1px !important;
}
.tableIndex td{
    padding-bottom: 0;
}
/**** Basic table style END ****/


/**** Class Teacher Meeting table style START ****/
.ctmeeting .col_1{
    width: 12mm;
}
.ctmeeting .col_2{
    width: 25mm;
}
.ctmeeting .col_3{
    width: 18mm;
}
.ctmeeting .col_4{
    width: 6mm;
}
.ctmeeting .col_5{
    width: 18mm;
}
.ctmeeting .col_6{
    width: 81mm;
}

/**** Class Teacher Meeting style END ****/

</style>
</head>

<?php if($countResultData == 0): ?>
    <h2 class="schoolName"><?=$school_name ?></h2>
    <h3 class="reportType"><?=$report_title ?></h3>
    
    <hr style="margin-top: 0;margin-bottom: 3mm;"/>
<?php endif; ?>


<?php
$pageHeader = ob_get_clean();
if($_GET['debug']){
    echo $pageHeader;
}else{
    $mpdf->WriteHTML($pageHeader);
}
@ob_end_clean();
################################ Load header to PDF END ################################


################################ Load Data to PDF START ################################
if($countResultData):
######## Header START ########
        ob_start();
?>
        <h2 class="schoolName"><?=$school_name ?></h2>
        <h3 class="reportType"><?=$report_title ?></h3>
        
        <hr style="margin-top: 0;margin-bottom: 3mm;"/>
<?php
        $header = ob_get_clean();
        $mpdf->WriteHTML($header);
        @ob_end_clean();
######## Header END ########


######## Footer START ########
        ob_start();
?>
        <table class="footer">
        	<tr>
                <td><?=$ClassNameByLang?></td>
                <td style="text-align: center;"></td>
        		<td style="text-align: right;">{PAGENO}</td>
        	</tr>
        </table>
<?php
        $footer = ob_get_clean();
        @ob_end_clean();
######## Footer END ########

    
######## Content START ########
    ob_start();
?>

<table class="ctmeeting">
	<thead>
		<tr>
	<? foreach($tableTitle as $fieldIndex => $title):?>
			<th class="field col_<?=$fieldIndex+1 ?>"><?=$title?></th>
	<? endforeach;?>
		</tr>
	</thead>
	
    <tbody class="tableVertical">
    <? foreach((array)$resultData as $index => $table):
		$dataCount = count($table['data']);		
	?>
		<tr>
			<td <?=($dataCount > 1 ? 'rowspan="'.$dataCount.'"' : '')?> class="value col_1" valign="top"><?=$table['StudentRegNo']?></td>
			<td <?=($dataCount > 1 ? 'rowspan="'.$dataCount.'"' : '')?> class="value col_2" valign="top"><?=$table['StudentName']?></td>
	<?			
	    if($dataCount):
	        foreach($table['data'] as $dataIndex => $data):
	        	if ($dataIndex > 0) {
	        		echo '<tr>';
	        	} 
				foreach($data as $fieldIndex => $value): 
				    $css = ' col_' . ($fieldIndex+3);
	?>
            		<td class="value <?=$css?>">
            			<?=nl2br($value)?>
            		</td>
    <?			endforeach;
    
        		echo '</tr>';
        		
    	    endforeach; 
	    else:
	?>
	    	<td colspan="<?=count($tableTitle)-2 ?>" class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
	    </tr>
   	<?  endif;?>
   	<? endforeach;?>
   	
	</tbody>
</table>

<?php 
    $page = ob_get_clean();
    if($_GET['debug']){
        echo $page;
    }else{
        $mpdf->SetHTMLFooter($footer);
        $mpdf->WriteHTML($page);
    }
    @ob_end_clean();

######## Content END ########


######## Print time START ########
ob_start();
?>

<h4 class="printTime"><?=$Lang['eGuidance']['report']['PrintTime']?>: <?=date('Y-m-d H:i:s') ?></h4>


<?php
        $page = ob_get_clean();
        if($_GET['debug']){
            echo $page;
        }else{
            $mpdf->SetHTMLFooter($footer);
            $mpdf->WriteHTML($page);
            if($hasPageBreak){
                $mpdf->WriteHTML('<pagebreak resetpagenum="1" suppress="off" />');
            }
        }
        @ob_end_clean();
######## Print time END ########

else:
    $mpdf->WriteHTML("<h1>{$Lang['General']['NoRecordAtThisMoment']}</h1>");
endif;
################################ Load Data to PDF END ################################



if(!$_GET['debug']){
    $mpdf->Output();
}
@ob_end_clean();

