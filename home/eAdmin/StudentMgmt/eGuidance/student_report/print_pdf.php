<?
/**
 * modifying:    
 * 
 * Change Log:
 *
 * 2020-06-24 Cameron
 *      - don't show the report section if there's no record in remark/comment in any activities for the student if NotShowSectionWithoutRecord is set [case #M151033]
 *        affected modules: Advanced Class, Therapy and SEN Service
 * 
 * 2020-06-22 Cameron
 *      - print sen pic signature table and teacher's suggestion table if $sys_custom['eGuidance']['StudentReport']['PrintSignatureAndSuggestion'] = true
 *      
 * 2020-06-19 Cameron
 *      - show customize report title if $sys_custom['eGuidance']['StudentReport']['SencaseTitle'] = true
 *      - hide teacher in charge for SEN case if PrintSenCasePICinStudentReport is false
 *      
 * 2020-05-14 Cameron
 *      - don't show the report section if there's no record in that section for the student if NotShowSectionWithoutRecord is set [case #M151033] 
 *      
 * 2020-05-08 Cameron
 *      - show SENTypeConfirmDate [case #F152883]
 *      
 * 2020-04-24 Cameron
 *      - Add SEN case filter: SEN Type, Support Mode, Adjustment Arrangement, Support Service [case #E150107]
 *      
 * 2020-03-03 Cameron
 *      - modify retrieving SEN case category and subcategory process to adapt the revised format. [case #D178788]
 *      
 * 2018-05-24 Cameron
 *      - show '-' for empty field in SEN
 *      
 * 2018-05-23 Cameron
 *      - fix: cast (array) to all foreach array loop
 *      
 * 2018-04-16 Cameron
 *      - Case type is retrieved from setup
 *  
 * 2018-04-11 Cameron
 *      - add optionSenService,optionSenCase and senStatus (added Quit) [case M130681]
 *      - print Parent ConfirmDate, NumberOfReport 
 *      
 * 2018-02-26 Cameron
 *      - don't show confidential record if the login user does not have right, these include:
 *      Contact, Guidance, SEN Case, Suspend Study, Case Referral, Self Improvement Scheme
 *      - show follow-up advise column in Meeting/Contact
 *      
 * 2017-11-07 Cameron
 * 		- should include libinterface.php before libguidance_ui.php
 * 		- fix meta charset
 * 		- retrieve student info by calling getStudentInfo2()
 * 
 * 2017-08-18 Cameron
 * 		- add SelfImprove 
 * 		- retrieve all settings from db rather than from configure file
 * 		- show SENType no matter confirmed or not
 * 		- add Tier in SENCase
 * 		- SEN case support service is in three level architecture
 * 		- add EndDate in Suspend
 * 		- add followup advice in guidance  
 * 
 * 2017-06-26 Pun
 *  - New file
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");
include_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

// ####### Init START ########
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$libguidance_ui = new libguidance_ui();
$resultData = array();
$reportDetails = array();

define('TABLE_TYPE_EMPTY', 'tableEmpty');
define('TABLE_TYPE_INDEX', 'tableIndex');
define('TABLE_TYPE_HORIZONTAL', 'tableHorizontal');
define('TABLE_TYPE_VERTICAL', 'tableVertical');

$DateType = $_POST['DateType'];
if ($DateType == 'YEAR') {
    $AcademicYearID = $_POST['AcademicYearID'];
    $AcademicYearTermID = $_POST['AcademicYearTermID'];
    $StartDate = getStartDateOfAcademicYear($AcademicYearID, $AcademicYearTermID);
    $EndDate = getEndDateOfAcademicYear($AcademicYearID, $AcademicYearTermID);
} else {
    $StartDate = $_POST['StartDate'];
    $EndDate = $_POST['EndDate'];
}
$YearID = $_POST['YearID'];
$YearClassID = $_POST['YearClassID'];
$StudentID = ($_POST['StudentID']) ? (array) $_POST['StudentID'] : '';
$reportType = (array) $_POST['reportType'];
$studentPrivateInfo = $_POST['studentPrivateInfo'];
$optionSenService = $_POST['optionSenService'];
$optionSenCase = $_POST['optionSenCase'];
$senStatus = $_POST['senStatus'];
$notShowStudentWithoutRecord = $_POST['NotShowStudentWithoutRecord'];
$notShowSectionWithoutRecord = $_POST['NotShowSectionWithoutRecord'];
$LogicalType = $_POST['LogicalType'];
$SenCaseTypeFilter = $_POST['SenCaseTypeFilter'];
$SenCaseSubTypeFilter = $_POST['SenCaseSubTypeFilter'];
$isTierAry = array();
for ($i=1;$i<=3;$i++) {
    $isTierAry[] = $_POST['IsTier'.$i];
}

$ItemID = $_POST['ItemID'];
$itemIDAry = array();
if (count($ItemID)) {
    foreach((array)$ItemID as $_itemIDAry) {
        foreach((array)$_itemIDAry as $__itemID) {
            $itemIDAry[] = $__itemID;
        }
    }
}

$ServiceItemID = $_POST['ServiceItemID'];
$serviceItemIDAry = array();
if (count($ServiceItemID)) {
    foreach((array)$ServiceItemID as $_serviceItemIDAry) {
        foreach((array)$_serviceItemIDAry as $__serviceItemID) {
            $serviceItemIDAry[] = $__serviceItemID;
        }
    }
}


$currentAcademicYearID = Get_Current_Academic_Year_ID();
// ####### Init END ########

// ####### Access Right START ########
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('VIEW', (array) $permission['current_right']['REPORTS']['STUDENTREPORT'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
// ####### Access Right END ########

// ####### Init PDF START ########
$mpdf = new mPDF('', 'A4', 0, '', 5, 5, 5, 15);

// $mpdf->mirrorMargins = 1;
// ####### Init PDF END ########

// ####### Helper function START ########
function sortByDate($a, $b)
{
    return strcmp($a['Date'], $b['Date']);
}
// ####### Helper function END ########

// ####### Load all Data START ########
// ### Load Header Data START ####
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
if ($sys_custom['eGuidance']['StudentReport']['SencaseTitle']) {
    $report_title = $Lang['eGuidance']['report']['SENStudentAnnualReport'];
}
else {
    $report_title = "{$Lang['eGuidance']['report']['eGuidanceReport']} - {$Lang['eGuidance']['menu']['Reports']['StudentReport']}";
}

if ($DateType == 'YEAR') {
    $academicYearTermInfo = getAcademicYearAndYearTermByDate($StartDate);
    $report_time = "{$academicYearTermInfo['AcademicYearName']} {$academicYearTermInfo['YearTermName']}";
} else {
    $report_time = "{$StartDate} {$Lang['General']['To']} {$EndDate}";
}
// ### Load Header Data END ####

// ### Get Student START ####
$allStudentInfo = array();
$allStudentId = array();

$allStudentInfo = $libguidance->getStudentInfo2($StudentID, $YearClassID, $YearID);
$allStudentId = Get_Array_By_Key($allStudentInfo, 'UserID');

$allStudentInfo = BuildMultiKeyAssoc($allStudentInfo, array(
    'UserID'
));
// debug_r($allStudentInfo);exit;
// ### Get Student END ####

// ### Get contact START ####
if (in_array('Contact', $reportType)) {
    $allContactInfo = array();
    $allContactTeacher = array();
    
    $allContactId = array();
    $rs = $libguidance->getContactByStudentId($allStudentId);
    
    foreach ((array) $rs as $r) {
        if ($r['ContactDate'] < $StartDate || $EndDate < $r['ContactDate']) {
            continue;
        }
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $allContactId[] = $r['ContactID'];
        
        $r['Date'] = $r['ContactDate'];
        $allContactInfo[$r['StudentID']][] = $r;
    }
    
    foreach ((array) $allContactInfo as $studentID => $data) {
        uasort($allContactInfo[$studentID], 'sortByDate');
    }
    
    $rs = $libguidance->getContactTeacher($allContactId);
    foreach ((array) $rs as $r) {
        $allContactTeacher[$r['ContactID']][] = $r;
    }
}
// ### Get contact END ####

// ### Get advanced class START ####
if (in_array('AdvancedClass', $reportType)) {
    $allAdvancedClassInfo = array();
    $allAdvancedClassTeacherInfo = array();
    $allAdvancedClassLessonInfo = array();
    $allAdvancedClassLessonResult = array();
    $allAdvancedClassClassStudentMapping = array();
    
    $allAdvancedClassId = array();
    $rs = $libguidance->getAdvancedClassByStudentId($allStudentId);
    foreach ((array) $rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        $allAdvancedClassInfo[$r['StudentID']][] = $r;
        $allAdvancedClassId[] = $r['ClassID'];
        $allAdvancedClassClassStudentMapping[$r['ClassID']][] = $r['StudentID'];
    }
    
    $rs = $libguidance->getAdvancedClassTeacher($allAdvancedClassId);
    foreach ((array) $rs as $r) {
        $allAdvancedClassTeacherInfo[$r['ClassID']][] = $r;
    }
    
    $rs = $libguidance->getAdvancedClassLesson($allAdvancedClassId);
    foreach ((array) $rs as $r) {
        $allAdvancedClassLessonInfo[$r['ClassID']][] = $r;
    }
    $allAdvancedClassLessonId = Get_Array_By_Key($rs, 'LessonID');
    
    $rs = $libguidance->getAdvancedClassLessonResult($allAdvancedClassLessonId);
    foreach ((array) $rs as $r) {
        $allAdvancedClassLessonResult[$r['LessonID']][$r['StudentID']] = $r;
    }
}
// ### Get advanced class END ####

// ### Get therapy START ####
if (in_array('Therapy', $reportType)) {
    $allTherapyInfo = array();
    $allTherapyTeacherInfo = array();
    $allTherapyActivityInfo = array();
    $allTherapyActivityResult = array();
    
    $allTherapyId = array();
    $rs = $libguidance->getTherapyByStudentId($allStudentId);
    foreach ((array) $rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        $r['Date'] = $r['StartDate'];
        $allTherapyInfo[$r['StudentID']][] = $r;
        $allTherapyId[] = $r['TherapyID'];
    }
    foreach ((array) $allTherapyInfo as $studentID => $data) {
        uasort($allTherapyInfo[$studentID], 'sortByDate');
    }
    
    $rs = $libguidance->getTherapyTeacher($allTherapyId);
    foreach ((array) $rs as $r) {
        $allTherapyTeacherInfo[$r['TherapyID']][] = $r;
    }
    
    $rs = $libguidance->getTherapyActivity($allTherapyId);
    foreach ((array) $rs as $r) {
        $allTherapyActivityInfo[$r['TherapyID']][] = $r;
    }
    $allTherapyActivityId = Get_Array_By_Key($rs, 'ActivityID');
    
    $rs = $libguidance->getTherapyActivityResult($allTherapyActivityId);
    foreach ((array) $rs as $r) {
        $allTherapyActivityResult[$r['ActivityID']][$r['StudentID']] = $r;
    }
}
// ### Get therapy END ####

// ### Get guidance START ####
if (in_array('Guidance', $reportType)) {
    $allGuidanceInfo = array();
    $allGuidanceTeacherInfo = array();
    
    $allGuidanceId = array();
    $rs = $libguidance->getGuidanceByStudentId($allStudentId);
    foreach ((array) $rs as $r) {
        if ($r['ContactDate'] < $StartDate || $EndDate < $r['ContactDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $caseTypeAry = explode('^~', $r['CaseType']);
        $caseTypeOther = '';
        foreach ((array) $caseTypeAry as $k => $v) {
            if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
                $caseTypeAry[$k] = 'Other';
                $caseTypeOther = substr($v, 7);
            }
        }
        
        $followupAdviceAry = explode('^~', $r['FollowupAdvice']);
        $followupAdviceOther = '';
        foreach ((array) $followupAdviceAry as $k => $v) {
            if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
                $followupAdviceAry[$k] = 'Other';
                $followupAdviceOther = substr($v, 7);
            }
        }
        
        $r['Date'] = "{$r['ContactDate']} {$guidanceInfo['ContactTime']}";
        $r['CaseTypeArr'] = $caseTypeAry;
        $r['CaseTypeOther'] = $caseTypeOther;
        $r['FollowupAdviceArr'] = $followupAdviceAry;
        $r['FollowupAdviceOther'] = $followupAdviceOther;
        $allGuidanceInfo[$r['StudentID']][] = $r;
        $allGuidanceId[] = $r['GuidanceID'];
    }
    foreach ((array) $allGuidanceInfo as $studentID => $data) {
        uasort($allGuidanceInfo[$studentID], 'sortByDate');
    }
    
    $rs = $libguidance->getGuidanceTeacher($allGuidanceId);
    foreach ((array) $rs as $r) {
        $allGuidanceTeacherInfo[$r['GuidanceID']][] = $r;
    }
}
// ### Get guidance END ####

// ### Get SEN service START ####
if (in_array('SEN', $reportType) && $optionSenService) {
    
    $allSenServiceInfo = array();
    $allSenServiceTeacherInfo = array();
    $allSenServiceActivityInfo = array();
    $allSenServiceActivityResult = array();
    
    $allSenServiceId = array();
    $rs = $libguidance->getSENServiceByStudentID($allStudentId);
    foreach ((array) $rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        $r['Date'] = $r['StartDate'];
        $allSenServiceInfo[$r['StudentID']][] = $r;
        $allSenServiceId[] = $r['ServiceID'];
    }
    foreach ((array) $allSenServiceInfo as $studentID => $data) {
        uasort($allSenServiceInfo[$studentID], 'sortByDate');
    }
    
    $rs = $libguidance->getSENServiceTeacher($allSenServiceId);
    foreach ((array) $rs as $r) {
        $allSenServiceTeacherInfo[$r['ServiceID']][] = $r;
    }
    
    $rs = $libguidance->getSENServiceActivity($allSenServiceId);
    foreach ((array) $rs as $r) {
        $allSenServiceActivityInfo[$r['ServiceID']][] = $r;
    }
    $allSenServiceActivityId = Get_Array_By_Key($rs, 'ActivityID');
    
    $rs = $libguidance->getSENServiceActivityResult($allSenServiceActivityId);
    foreach ((array) $rs as $r) {
        $allSenServiceActivityResult[$r['ActivityID']][$r['StudentID']] = $r;
    }
}
// ### Get SEN service END ####

// ### Get SEN case START ####
if (in_array('SEN', $reportType) && $optionSenCase) {
    
    // get SEN case type and subtype
    $sen_case_type = $libguidance->getAllSENCaseType();
    $senCaseTypeAssoc = BuildMultiKeyAssoc($sen_case_type, array(
        'Code'
    ), $IncludedDBField = array(
        'Name'
    ), $SingleValue = 1);
    $sen_case_subtype = $libguidance->getAllSENCaseSubType();
    $senCaseSubTypeAssoc = BuildMultiKeyAssoc($sen_case_subtype, array(
        'TypeCode',
        'Code'
    ), $IncludedDBField = array(
        'Name'
    ), $SingleValue = 1);
    
    $allSenCaseInfo = array();
    $allSenCaseTeacherInfo = array();
    
    $allSenCaseAdjustInfo = array(); // info pass to print
    $allSenCaseAdjustment = array(); // adjustment record in sen case
    $allSenCaseAdjustType = array();
    $allSenCaseAdjustItem = array(); // item under adjust type
    
    $allSenCaseServiceInfo = array();
    $allSenCaseService = array(); // service record in sen case
    $allSenCaseServiceType = array();
    $allSenCaseServiceItem = array(); // item under service type
                                      
    // # Basic info START ##
    $filterStudentIDAry = $libguidance->getFilterSENStudents($allStudentId, $LogicalType, $SenCaseTypeFilter, $SenCaseSubTypeFilter, $itemIDAry, $serviceItemIDAry, $isTierAry);
    if (count($filterStudentIDAry)) {
        $rs = $libguidance->getSENCase($filterStudentIDAry, $senStatus);
    }
    else {
        $rs = array();
    }
    
    foreach ((array) $rs as $r) {
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $senTypeAry = explode('^~', $r['SENType']);
        $senItemAry = array(); // items under SEN type: can be array or string , e.g. array(RD => array(RDC, RDE), MH =>"memtal health prob")
        $rsSENTypeConfirmDate = $libguidance->getSENTypeConfirmDate($r['StudentID']);
        $senTypeConfirmDateAry = array();
        
        foreach ((array) $senTypeAry as $k => $v) {
            
            if (strpos($v, '^:') !== false) {
                list ($senType, $senItemStr) = explode('^:', $v);
                $senTypeAry[$k] = $senType;
                if ($r['IsSEN'] && !empty($rsSENTypeConfirmDate[$senType]) && ($rsSENTypeConfirmDate[$senType] != '0000-00-00')) {
                    $senTypeConfirmDateAry[$senType] = $rsSENTypeConfirmDate[$senType];
                }
                else {
                    $senTypeConfirmDateAry[$senType] = '';
                }
                
                $pos = strpos($senItemStr, '^@');
                $senItems = $senItemStr;
                if ($pos !== false) {
                    $senItems = substr($senItemStr, 0, $pos);
                    $senItemText = substr($senItemStr,$pos+2);
                }
                else {
                    $senItemText = '';
                }
                
                if (strpos($senItems, '^#') !== false) {
                    $subItemName = '';
                    if ($senItemText) {
                        $subItemName .= '-'.$senItemText;
                    }
                    $subItemName .= ': ';                    
                    $subItemNameAry = array();
                    $senSubItemAry = explode('^#', $senItems);
                    foreach ((array) $senSubItemAry as $_subItemCode) {
                        $subItemNameAry[] = $senCaseSubTypeAssoc[$senType][$_subItemCode];
                    }
                    $subItemName .= implode(', ', $subItemNameAry);
                    $senItemAry[$senType] = $subItemName;
                } else {
                    if ($senCaseTypeAssoc[$senType] && $senCaseSubTypeAssoc[$senType][$senItems]) {
                        $senItemAry[$senType] = ': ' . $senCaseSubTypeAssoc[$senType][$senItems];
                    } else {
                        $senItemAry[$senType] = ': ' . $senItems;
                    }
                }
            }
            else if (strpos($v, '^@') !== false) {
                list ($senType, $itemText) = explode('^@', $v);
                $senTypeAry[$k] = $senType;
                if ($r['IsSEN'] && !empty($rsSENTypeConfirmDate[$senType]) && ($rsSENTypeConfirmDate[$senType] != '0000-00-00')) {
                    $senTypeConfirmDateAry[$senType] = $rsSENTypeConfirmDate[$senType];
                }
                else {
                    $senTypeConfirmDateAry[$senType] = '';
                }
                
                $subItemName = '';
                if ($itemText) {
                    $subItemName .= '-'.$itemText;
                }
                $senItemAry[$senType] = $subItemName;
            }
            else {
                $senItemAry[$k] = '';
                
                if ($r['IsSEN'] && !empty($rsSENTypeConfirmDate[$v]) && ($rsSENTypeConfirmDate[$v] != '0000-00-00')) {
                    $senTypeConfirmDateAry[$v] = $rsSENTypeConfirmDate[$v];
                }
                else {
                    $senTypeConfirmDateAry[$v] = '';
                }
            }
            
        }
        $r['SenTypeAry'] = $senTypeAry;
        $r['SenItemAry'] = $senItemAry;
        $r['SenTypeConfirmDateAry'] = $senTypeConfirmDateAry;
        $allSenCaseInfo[$r['StudentID']] = $r;
    }
    
    $rs = $libguidance->getSENCaseTeacher($allStudentId);
    foreach ((array) $rs as $r) {
        $allSenCaseTeacherInfo[$r['StudentID']][] = $r;
    }
    // # Basic info END ##
    
    // # Adjustment START ##
    $rs = $libguidance->getSENAdjustType();
    $allSenCaseAdjustType = BuildMultiKeyAssoc($rs, array(
        'TypeID'
    ));
    
    $rs = $libguidance->getSENAdjustItem();
    $allSenCaseAdjustItem = BuildMultiKeyAssoc($rs, array(
        'TypeID',
        'ItemID'
    ));
    
    $rs = $libguidance->getSENCaseAdjustment($allStudentId);
    foreach ((array) $rs as $r) {
        $allSenCaseAdjustment[$r['ItemID']][$r['StudentID']][] = $r;
    }
    foreach ((array) $allSenCaseAdjustment as $itemID => $caseAdjustments) {
        foreach ((array) $caseAdjustments as $studentId => $adjustments) {
            foreach ((array) $allSenCaseAdjustType as $typeID => $type) {
                foreach ((array) $allSenCaseAdjustItem[$typeID] as $itemID => $item) {
                    foreach ((array) $adjustments as $adjustment) {
                        if ($adjustment['ItemID'] == $itemID) {
                            $allSenCaseAdjustInfo[$studentId][$typeID][$itemID] = $adjustment['Arrangement'];
                        }
                    }
                }
            }
        }
    }
    // # Adjustment END ##
    
    // # Support START ##
    $rs = $libguidance->getServiceType();
    $allSenCaseServiceType = BuildMultiKeyAssoc($rs, array(
        'Type',
        'TypeID'
    ));
    
    $rs = $libguidance->getSENSupportService();
    $allSenCaseServiceItem = BuildMultiKeyAssoc($rs, array(
        'Type',
        'TypeID',
        'ServiceID'
    ));
    
    $rs = $libguidance->getSENCaseSupport($allStudentId);
    foreach ((array) $rs as $r) {
        $allSenCaseService[$r['ServiceID']][$r['StudentID']][] = $r;
    }
    foreach ((array) $allSenCaseService as $serviceID => $caseServices) {
        foreach ((array) $caseServices as $studentId => $services) {
            foreach ((array) $allSenCaseServiceType as $type => $subType) { // type = (Study|Other)
                foreach ((array) $subType as $typeID => $typeInfo) {
                    foreach ((array) $allSenCaseServiceItem[$type][$typeID] as $serviceID => $item) {
                        foreach ((array) $services as $service) {
                            if ($service['ServiceID'] == $serviceID) {
                                $allSenCaseServiceInfo[$studentId][$type][$typeID][$serviceID] = $service['Arrangement'];
                            }
                        }
                    }
                }
            }
        }
    }
    
    // # Support END ##
}
// ### Get SEN case END ####

// ### Get suspend START ####
if (in_array('Suspend', $reportType)) {
    $allSuspendInfo = array();
    
    $rs = $libguidance->getSuspend();
    foreach ((array) $rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $r['Date'] = $r['StartDate'];
        $allSuspendInfo[$r['StudentID']][] = $r;
    }
    foreach ((array) $allSuspendInfo as $studentID => $data) {
        uasort($allSuspendInfo[$studentID], 'sortByDate');
    }
}
// ### Get suspend END ####

// ### Get transfer START ####
if (in_array('Transfer', $reportType)) {
    $allTransferInfo = array();
    
    $rs = $libguidance->getTransfer();
    foreach ((array) $rs as $r) {
        if ($r['TransferDate'] < $StartDate || $EndDate < $r['TransferDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $transferReasonAry = explode('^~', $r['TransferReason']);
        foreach ((array) $transferReasonAry as $k => $v) {
            if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
                $transferReasonAry[$k] = 'Other';
                $transferReasonOther = substr($v, 7);
            }
        }
        
        $r['Date'] = $r['TransferDate'];
        $r['TransferReasonAry'] = $transferReasonAry;
        $r['TransferReasonOther'] = $transferReasonOther;
        
        $allTransferInfo[$r['StudentID']][] = $r;
    }
    foreach ((array) $allTransferInfo as $studentID => $data) {
        uasort($allTransferInfo[$studentID], 'sortByDate');
    }
}
// ### Get transfer END ####

// ### Get selfImprove START ####
if (in_array('SelfImprove', $reportType)) {
    $allSelfImproveInfo = array();
    $allSelfImproveTeacher = array();
    
    $allSelfImproveId = array();
    $rs = $libguidance->getSelfImproveByStudentId($allStudentId);
    foreach ((array) $rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $allSelfImproveId[] = $r['ImproveID'];
        
        $r['Date'] = $r['StartDate'];
        $allSelfImproveInfo[$r['StudentID']][] = $r;
    }
    
    foreach ((array) $allSelfImproveInfo as $studentID => $data) {
        uasort($allSelfImproveInfo[$studentID], 'sortByDate');
    }
    
    $rs = $libguidance->getSelfImproveTeacher($allSelfImproveId);
    foreach ((array) $rs as $r) {
        $allSelfImproveTeacher[$r['ImproveID']][] = $r;
    }
}
// ### Get selfImprove END ####

// ### Get no record student START ####
$noRecordStudentId = array();

if ($notShowStudentWithoutRecord) {
    foreach ((array) $allStudentId as $studentID) {
        if (! ((in_array('Contact', $reportType) && count($allContactInfo[$studentID])) || (in_array('AdvancedClass', $reportType) && count($allAdvancedClassInfo[$studentID])) || (in_array('Therapy', $reportType) && count($allTherapyInfo[$studentID])) || (in_array('Guidance', $reportType) && count($allGuidanceInfo[$studentID])) || (in_array('SEN', $reportType) && count($allSenServiceInfo[$studentID])) || (in_array('SEN', $reportType) && count($allSenCaseInfo[$studentID])) || (in_array('Suspend', $reportType) && count($allSuspendInfo[$studentID])) || (in_array('Transfer', $reportType) && count($allTransferInfo[$studentID])) || (in_array('SelfImprove', $reportType) && count($allSelfImproveInfo[$studentID])))) {
            $noRecordStudentId[] = $studentID;
        }
    }
}

if (false)
    foreach ((array) $allStudentId as $studentID) {
        debug_r($studentID);
        debug_r($allContactInfo[$studentID]);
        debug_r($allAdvancedClassInfo[$studentID]);
        debug_r($allTherapyInfo[$studentID]);
        debug_r($allGuidanceInfo[$studentID]);
        debug_r($allSenServiceInfo[$studentID]);
        debug_r($allSenCaseInfo[$studentID]);
        debug_r($allSuspendInfo[$studentID]);
        debug_r($allTransferInfo[$studentID]);
        debug_r($allSelfImproveInfo[$studentID]);
    }

// ### Get no record student END ####
// ####### Load all Data END ########

// ####### Pack data START ########
/**
 * Demo data
 * $table = array(
 * 'title' => 'abc',
 * 'class' => 'basicInfo',
 * 'colspan' => 4,
 * 'tbody' => array(
 * array(
 * 'type' => TABLE_TYPE_HORIZONTAL,
 * 'data-cell-colspan' => 3,
 * 'field' => array( 'Name' ),
 * 'data' => array('S1')
 * ),
 * array(
 * 'type' => TABLE_TYPE_VERTICAL,
 * 'field' => array( 'Date', 'Event', 'Teacher', 'Result' ),
 * 'data' => array(
 * array('2017-01-02', 'EV01', 'T1', 'Good'),
 * array('2017-01-03', 'EV02', 'T2', 'Bad'),
 * )
 * ),
 * )
 * );
 */
foreach ((array) $allStudentId as $studentID) {
    if (in_array($studentID, $noRecordStudentId)) {
        continue;
    }
    
    $allTable = array();
    
    $displaySTRN = $i_STRN . ': ';
    $displaySTRN .= $allStudentInfo[$studentID]['STRN'] ? $allStudentInfo[$studentID]['STRN'] : '-';
    $gender = $allStudentInfo[$studentID]['Gender'];
    $displayGender = $i_UserGender . ': ';
    if ($gender == 'M') {
        $displayGender .= $Lang['eGuidance']['personal']['GenderMale'];
    } elseif ($gender == 'F') {
        $displayGender .= $Lang['eGuidance']['personal']['GenderFemale'];
    } else {
        $displayGender .= '-';
    }
    $displayDateOfBirth = $i_UserDateOfBirth . ': ';
    $displayDateOfBirth .= ($allStudentInfo[$studentID]['DateOfBirth'] == '0000-00-00' || $allStudentInfo[$studentID]['DateOfBirth'] == '') ? '-' : $allStudentInfo[$studentID]['DateOfBirth'];
    
    // ### Basic information START ####
    $reportDetails[] = array(
        'reportTime' => $report_time,
        'studentName' => "{$allStudentInfo[$studentID]['StudentName']} ({$allStudentInfo[$studentID]['ClassName']} - {$allStudentInfo[$studentID]['ClassNumber']})",
        'STRN' => $displaySTRN,
        'gender' => $displayGender,
        'dateOfBirth' => $displayDateOfBirth
    );
    // ### Basic information END ####
    
    // ### Contact START ####
    if (in_array('Contact', $reportType)) {
        if (count($allContactInfo[$studentID])) {
            $contact_category = $libguidance->getGuidanceSettingItem('Contact', 'Category');
            
            $data = array();
            foreach ((array) $allContactInfo[$studentID] as $contactInfo) {
                $teacherHTML = array();
                foreach ((array) $allContactTeacher[$contactInfo['ContactID']] as $teacher) {
                    $teacherHTML[] = $teacher['Name'];
                }
                $teacherHTML = implode('<br />', $teacherHTML);
                
                $data[] = array(
                    $contactInfo['ContactDate'],
                    $contact_category[$contactInfo['Category']],
                    $teacherHTML,
                    $contactInfo['Details'],
                    $contactInfo['FollowupAdvice']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Contact'],
                'class' => 'contactInfo',
                'colspan' => 5,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['Date'],
                            $Lang['eGuidance']['contact']['ContactCategory'],
                            $Lang['eGuidance']['TeacherName'],
                            $Lang['eGuidance']['contact']['Details'],
                            $Lang['eGuidance']['FollowupAdvice']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Contact'],
                    'class' => 'contactInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Contact END ####
        
    // ### Advanced class START ####
    if (in_array('AdvancedClass', $reportType)) {
        if (count($allAdvancedClassInfo[$studentID])) {
            
            $data = array();
            foreach ((array) $allAdvancedClassLessonInfo as $classID => $lessonInfos) {
                foreach ((array) $lessonInfos as $lessonInfo) {
                    if (! in_array($studentID, $allAdvancedClassClassStudentMapping[$classID])) {
                        continue;
                    }
                    $lessonID = $lessonInfo['LessonID'];
                    
                    $teacherHTML = array();
                    foreach ((array) $allAdvancedClassTeacherInfo[$classID] as $teacher) {
                        $teacherHTML[] = $teacher['Name'];
                    }
                    $teacherHTML = implode('<br />', $teacherHTML);
                    
                    if ($allAdvancedClassLessonResult[$lessonID][$studentID]['Result']) {
                        $data[] = array(
                            $lessonInfo['LessonDate'],
                            $allAdvancedClassLessonResult[$lessonID][$studentID]['Result'],
                            $teacherHTML
                        );
                    }
                }
            }

            if ($notShowSectionWithoutRecord && (count($data) == 0)) {
                // by pass this record   
            }
            else {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['AdvancedClass'],
                    'class' => 'advancedClassInfo',
                    'colspan' => 3,
                    'tbody' => array(
                        array(
                            'type' => TABLE_TYPE_VERTICAL,
                            'field' => array(
                                $Lang['eGuidance']['Date'],
                                $Lang['eGuidance']['advanced_class']['TeacherComment'],
                                $Lang['eGuidance']['TeacherName']
                            ),
                            'data' => $data
                        )
                    )
                );
                $allTable[] = $table;
            }
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['AdvancedClass'],
                    'class' => 'advancedClassInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Advanced class END ####

    // ### Therapy START ####
    if (in_array('Therapy', $reportType)) {
        if (count($allTherapyInfo[$studentID])) {
            $tbodyArr = array();
            $index = 1;
            $hasIndex = count($allTherapyInfo[$studentID]) > 1;
            
            foreach ((array) $allTherapyInfo[$studentID] as $therapyInfo) {
                $therapyId = $therapyInfo['TherapyID'];
                
                $teacherHTML = array();
                foreach ((array) $allTherapyTeacherInfo[$therapyId] as $teacher) {
                    $teacherHTML[] = $teacher['Name'];
                }
                $teacherHTML = implode('<br />', $teacherHTML);
                
                $data = array();
                foreach ((array) $allTherapyActivityInfo[$therapyId] as $activityInfo) {
                    $activityID = $activityInfo['ActivityID'];
                    
                    $resultRemarkHTML = $allTherapyActivityResult[$activityID][$studentID]['Result'];
                    if ($notShowSectionWithoutRecord && (trim($resultRemarkHTML) == '') && (trim($activityInfo['Remark']) == '')) {
                        // by pass the record
                    }
                    else {
                        if ($activityInfo['Remark']) {
                            $resultRemarkHTML .= "<br /><br />{$Lang['eGuidance']['Remark']}: {$activityInfo['Remark']}";
                        }
                        
                        $data[] = array(
                            '',
                            $activityInfo['ActivityDate'],
                            $activityInfo['ActivityName'],
                            $resultRemarkHTML
                        );
                    }
                }
                
                if ($notShowSectionWithoutRecord && (count($data) == 0)) {
                    // by pass this record
                }
                else {
                    if ($hasIndex) {
                        $tbodyArr[] = array(
                            'type' => TABLE_TYPE_INDEX,
                            'index' => $index ++
                        );
                    }
                    
                    $tbodyArr[] = array(
                        'type' => TABLE_TYPE_HORIZONTAL,
                        'data-cell-colspan' => 3,
                        'field' => array(
                            $Lang['eGuidance']['therapy']['GroupName'],
                            $Lang['General']['StartDate'],
                            $Lang['eGuidance']['TeacherName']
                        ),
                        'data' => array(
                            $therapyInfo['GroupName'],
                            $therapyInfo['StartDate'],
                            $teacherHTML
                        )
                    );
                    
                    $tbodyArr[] = array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['therapy']['ActivityList'],
                            $Lang['eGuidance']['Date'],
                            $Lang['eGuidance']['therapy']['ActivityName'],
                            $Lang['eGuidance']['therapy']['ContentAndResult']
                        ),
                        'data' => $data
                    );
                }
            }
            
            if ($notShowSectionWithoutRecord && (count($tbodyArr) == 0)) {
                // by pass this record
            }
            else {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Therapy'],
                    'class' => 'therapyInfo',
                    'colspan' => 4,
                    'tbody' => $tbodyArr
                );
                $allTable[] = $table;
            }
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Therapy'],
                    'class' => 'therapyInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Therapy END ####
    
    // ### Guidance START ####
    if (in_array('Guidance', $reportType)) {
        if (count($allGuidanceInfo[$studentID])) {
            $tbodyArr = array();
            $index = 1;
            $hasIndex = count($allGuidanceInfo[$studentID]) > 1;
            
            $guidance_type = $libguidance->getGuidanceSettingItem('Guidance', 'CaseType');
            $followup_advice = $libguidance->getGuidanceSettingItem('Guidance', 'FollowupAdvice');
            $contact_stage = $libguidance->getGuidanceSettingItem('Guidance', 'ContactStage');
            
            foreach ((array) $allGuidanceInfo[$studentID] as $guidanceInfo) {
                $guidanceId = $guidanceInfo['GuidanceID'];
                
                $teacherHTML = array();
                foreach ((array) $allGuidanceTeacherInfo[$guidanceId] as $teacher) {
                    $teacherHTML[] = $teacher['Name'];
                }
                $teacherHTML = implode('<br />', $teacherHTML);
                
                $caseTypeArr = array();
                foreach ((array) $guidanceInfo['CaseTypeArr'] as $caseType) {
                    if ($caseType == 'Other') {
                        $caseTypeArr[] = $guidanceInfo['CaseTypeOther'];
                    } else {
                        $caseTypeArr[] = $guidance_type[$caseType];
                    }
                }
                $caseTypeHTML = implode('<br />', $caseTypeArr);
                
                $followupAdviceArr = array();
                foreach ((array) $guidanceInfo['FollowupAdviceArr'] as $followupAdvice) {
                    if ($followupAdvice == 'Other') {
                        $followupAdviceArr[] = $guidanceInfo['FollowupAdviceOther'];
                    } else {
                        $followupAdviceArr[] = $followup_advice[$followupAdvice];
                    }
                }
                $followupAdviceHTML = implode('<br />', $followupAdviceArr);
                
                $contactTime = substr($guidanceInfo['ContactTime'], 0, 5);
                
                if ($hasIndex) {
                    $tbodyArr[] = array(
                        'type' => TABLE_TYPE_INDEX,
                        'index' => $index ++
                    );
                }
                
                $tbodyArr[] = array(
                    'type' => TABLE_TYPE_HORIZONTAL,
                    'field' => array(
                        $Lang['eGuidance']['guidance']['CaseType'],
                        $Lang['eGuidance']['guidance']['ContactDate'],
                        $Lang['eGuidance']['guidance']['ContactPlace'],
                        $Lang['eGuidance']['TeacherName'],
                        $Lang['eGuidance']['guidance']['ContactPurpose'],
                        $Lang['eGuidance']['guidance']['ContactContent'],
                        $Lang['eGuidance']['guidance']['ContactFinding'],
                        $Lang['eGuidance']['guidance']['ContactStage'],
                        $Lang['eGuidance']['FollowupAdvice']
                    ),
                    'data' => array(
                        $caseTypeHTML,
                        "{$guidanceInfo['ContactDate']}&nbsp;&nbsp;{$Lang['eGuidance']['guidance']['ContactTime']}:&nbsp;{$contactTime}",
                        $guidanceInfo['ContactPlace'],
                        $teacherHTML,
                        $guidanceInfo['ContactPurpose'],
                        $guidanceInfo['ContactContent'],
                        $guidanceInfo['ContactFinding'],
                        $contact_stage[$guidanceInfo['ContactStage']],
                        $followupAdviceHTML
                    )
                );
            }
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Guidance'],
                'class' => 'guidanceInfo',
                'tbody' => $tbodyArr
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Guidance'],
                    'class' => 'guidanceInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Guidance END ####
    
    // ### Sen Service START ####
    if (in_array('SEN', $reportType) && $optionSenService) {
        if (count($allSenServiceInfo[$studentID])) {
            $sen_service_type = $libguidance->getGuidanceSettingItem('SENService', 'ServiceType');
            $tbodyArr = array();
            $index = 1;
            $hasIndex = count($allSenServiceInfo[$studentID]) > 1;
            
            foreach ((array) $allSenServiceInfo[$studentID] as $serviceInfo) {
                $serviceID = $serviceInfo['ServiceID'];
                
                $teacherHTML = array();
                foreach ((array) $allSenServiceTeacherInfo[$serviceID] as $teacher) {
                    $teacherHTML[] = $teacher['Name'];
                }
                $teacherHTML = implode('<br />', $teacherHTML);
                
                if (substr($serviceInfo['ServiceType'], 0, 5) == 'Other') {
                    $serviceType = substr($serviceInfo['ServiceType'], 7);
                } else {
                    $serviceType = $sen_service_type[$serviceInfo['ServiceType']];
                }
                
                $data = array();
                foreach ((array) $allSenServiceActivityInfo[$serviceID] as $activityInfo) {
                    $activityID = $activityInfo['ActivityID'];
                    
                    if ($notShowSectionWithoutRecord && (trim($allSenServiceActivityResult[$activityID][$studentID]['Result']) == '')) {
                        // by pass this record
                    }
                    else {
                        $data[] = array(
                            '',
                            $activityInfo['ActivityDate'],
                            $allSenServiceActivityResult[$activityID][$studentID]['Result']
                        );
                    }
                }
                
                if ($notShowSectionWithoutRecord && (count($data) == 0) ) {
                    // by pass this record
                }
                else {
                    if ($hasIndex) {
                        $tbodyArr[] = array(
                            'type' => TABLE_TYPE_INDEX,
                            'index' => $index ++
                        );
                    }
                    
                    $tbodyArr[] = array(
                        'type' => TABLE_TYPE_HORIZONTAL,
                        'data-cell-colspan' => 2,
                        'field' => array(
                            $Lang['eGuidance']['sen_service']['ServiceType'],
                            $Lang['General']['StartDate'],
                            $Lang['eGuidance']['TeacherName']
                        ),
                        'data' => array(
                            $serviceType,
                            $serviceInfo['StartDate'],
                            $teacherHTML
                        )
                    );
                    $tbodyArr[] = array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['sen_service']['ActivityList'],
                            $Lang['eGuidance']['Date'],
                            $Lang['eGuidance']['sen_service']['ExtraInfo']
                        ),
                        'data' => $data
                    );
                }
            }
            
            if ($notShowSectionWithoutRecord && (count($tbodyArr) == 0) ) {
                // by pass this record
            }
            else {
                $table = array(
                    'title' => $Lang['eGuidance']['sen_service']['SenService'],
                    'class' => 'senServiceInfo',
                    'colspan' => 3,
                    'tbody' => $tbodyArr
                );
                $allTable[] = $table;
            }
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['sen_service']['SenService'],
                    'class' => 'senServiceInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Sen Service END ####
    
    // ### Sen Case START ####
    if (in_array('SEN', $reportType) && $optionSenCase) {
        if (count($allSenCaseInfo[$studentID])) {
            $sen_case_type = $libguidance->getGuidanceSettingItem('SENCase', 'SENType');
            
            $fields = array();
            if ($libguidance->PrintSenCasePICinStudentReport) {
                $fields[] = $Lang['eGuidance']['TeacherName'];
            }
            $fields[] = $Lang['eGuidance']['sen']['SENStudent'];
            $fields[] = $Lang['eGuidance']['sen_case']['ConfirmDate'];
            $fields[] = $Lang['eGuidance']['guidance']['CaseType']; // should show SENType even if not confirmed [case #R102745 2017-07-03]
            $fields[] = $Lang['eGuidance']['sen_case']['Tier']['TierName'];
            $fields[] = $Lang['eGuidance']['sen_case']['FineTuneArrangement'];
            $fields[] = $Lang['eGuidance']['sen_case']['FineTuneSupportStudy'];
            $fields[] = $Lang['eGuidance']['sen_case']['FineTuneSupportOther'];
            $fields[] = $Lang['eGuidance']['sen_case']['NumberOfReport'];
            $fields[] = $Lang['eGuidance']['sen_case']['Remark'];
            
            // # Teacher START ##
            $teacherHTML = array();
            foreach ((array) $allSenCaseTeacherInfo[$studentID] as $teacher) {
                $teacherHTML[] = $teacher['Name'];
            }
            $teacherHTML = implode('<br />', $teacherHTML);
            // # Teacher END ##
            
            // # SEN Type START ##
            $senTypeHTML = array();
            foreach ((array) $allSenCaseInfo[$studentID]['SenTypeAry'] as $type) {
                $typeHTML = $sen_case_type[$type];
                if ($allSenCaseInfo[$studentID]['SenItemAry']) {
                    if (is_array($allSenCaseInfo[$studentID]['SenItemAry'][$type])) {
                        foreach ((array) $allSenCaseInfo[$studentID]['SenItemAry'][$type] as $vv) {
                            $typeHTML .= $vv;
                        }
                    } else if ($allSenCaseInfo[$studentID]['SenItemAry'][$type]) {
                        $typeHTML .= $allSenCaseInfo[$studentID]['SenItemAry'][$type];
                    }
                }
                
                if ($allSenCaseInfo[$studentID]['SenTypeConfirmDateAry'][$type]) {
                    $typeHTML .= ' ('. $Lang['eGuidance']['sen_case']['SENTypeConfirmDate'] . ': ' . $allSenCaseInfo[$studentID]['SenTypeConfirmDateAry'][$type] .')';
                }
                
                if (!empty($typeHTML)) {
                    $senTypeHTML[] = $typeHTML;
                }
            }
            $senTypeHTML = count($senTypeHTML) ? implode('<br />', $senTypeHTML) : '-';
            // # SEN Type END ##
            
            // # Tier START ##
            $tierHTML = array();
            $isTierEmpty = true;
            for ($i = 1; $i <= 3; $i ++) {
                if ($allSenCaseInfo[$studentID]["IsTier$i"]) {                    
                    $tierHTML[] = $Lang['eGuidance']['sen_case']['Tier']["Tier$i"] . ": <br />" .($allSenCaseInfo[$studentID]["Tier{$i}Remark"] ? $allSenCaseInfo[$studentID]["Tier{$i}Remark"] : '-');
                    $isTierEmpty = false;
                }
            }
            $tierHTML = $isTierEmpty ? '-' : implode('<br />', $tierHTML);
            // # Tier END ##
            
            // # Adjust START ##
            $nameField = Get_Lang_Selection('ChineseName', 'EnglishName');
            $adjustmentHTML = '';
            foreach ((array) $allSenCaseAdjustInfo[$studentID] as $typeID => $d1) {
                $adjustmentHTML .= "{$allSenCaseAdjustType[$typeID][$nameField]}<br />";
                foreach ((array) $d1 as $itemID => $arrangement) {
                    $adjustmentHTML .= "&nbsp;&nbsp;&nbsp;{$allSenCaseAdjustItem[$typeID][$itemID][$nameField]}";
                    if ($arrangement) {
                        $adjustmentHTML .= ": {$arrangement}";
                    }
                    $adjustmentHTML .= '<br />';
                }
            }
            if (empty($adjustmentHTML)) {
                $adjustmentHTML = '-';
            }
            // # Adjust END ##
            
            // # Service Study START ##
            $nameField = Get_Lang_Selection('ChineseName', 'EnglishName');
            
            $serviceStudyHTML = '';
            if ($allSenCaseServiceInfo[$studentID]['Study']) {
                foreach ((array) $allSenCaseServiceInfo[$studentID]['Study'] as $typeID => $d1) {
                    $serviceStudyHTML .= "{$allSenCaseServiceType['Study'][$typeID][$nameField]}<br />";
                    foreach ((array) $d1 as $itemID => $arrangement) {
                        $serviceStudyHTML .= "&nbsp;&nbsp;&nbsp;{$allSenCaseServiceItem['Study'][$typeID][$itemID][$nameField]}";
                        if ($arrangement) {
                            $serviceStudyHTML .= ": {$arrangement}";
                        }
                        $serviceStudyHTML .= '<br />';
                    }
                }
            }
            if (empty($serviceStudyHTML)) {
                $serviceStudyHTML = '-';
            }            
            // # Service Study END ##
            
            // # Service Other START ##
            $nameField = Get_Lang_Selection('ChineseName', 'EnglishName');
            $serviceOtherHTML = '';
            if ($allSenCaseServiceInfo[$studentID]['Other']) {
                foreach ((array) $allSenCaseServiceInfo[$studentID]['Other'] as $typeID => $d1) {
                    $serviceOtherHTML .= "{$allSenCaseServiceType['Other'][$typeID][$nameField]}<br />";
                    foreach ((array) $d1 as $itemID => $arrangement) {
                        $serviceOtherHTML .= "&nbsp;&nbsp;&nbsp;{$allSenCaseServiceItem['Other'][$typeID][$itemID][$nameField]}";
                        if ($arrangement) {
                            $serviceOtherHTML .= ": {$arrangement}";
                        }
                        $serviceOtherHTML .= '<br />';
                    }
                }
            }
            if (empty($serviceOtherHTML)) {
                $serviceOtherHTML = '-';
            }
            // # Service Other END ##
            
            $data = array();
            if ($libguidance->PrintSenCasePICinStudentReport) {
                $data[] = $teacherHTML;
            }
            if ($allSenCaseInfo[$studentID]['IsSEN'] == 1) {
                $data[] = $Lang['eGuidance']['sen']['Confirm'];
            } elseif ($allSenCaseInfo[$studentID]['IsSEN'] == 2) {
                $data[] = $Lang['eGuidance']['sen']['Quit'];
            } else {
                $data[] = $Lang['eGuidance']['sen']['NotConfirm'];
            }
            $data[] = ($allSenCaseInfo[$studentID]['ConfirmDate'] == '0000-00-00' || $allSenCaseInfo[$studentID]['ConfirmDate'] == '') ? '-' : $allSenCaseInfo[$studentID]['ConfirmDate'];
            $data[] = $senTypeHTML; // should show SENType even if not confirmed [case #R102745 2017-07-03]
            $data[] = $tierHTML;
            $data[] = $adjustmentHTML;
            $data[] = $serviceStudyHTML;
            $data[] = $serviceOtherHTML;
            $data[] = $allSenCaseInfo[$studentID]['NumberOfReport'] > 0 ? $allSenCaseInfo[$studentID]['NumberOfReport'] : '-';
            $data[] = $allSenCaseInfo[$studentID]['Remark'] ? $allSenCaseInfo[$studentID]['Remark'] : '-';
            
            $table = array(
                'title' => $Lang['eGuidance']['sen_service']['SenCase'],
                'class' => 'senCaseInfo',
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_HORIZONTAL,
                        'field' => $fields,
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['sen_service']['SenCase'],
                    'class' => 'senCaseInfo'
                );
                $allTable[] = $table;
            }
        }
        
        // print signature and suggestion of the teacher
        if ($sys_custom['eGuidance']['StudentReport']['PrintSignatureAndSuggestion']) {
            $fields = array();
            $fields[] = $Lang['eGuidance']['report']['SENCase']['ClassTeacher'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectTeacherChi'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectTeacherEng'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectTeacherMat'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectTeacherGes'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SchoolSocialWorker'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['Organizer'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['ParentSignature'];

            $table = array(
                'title' => $Lang['eGuidance']['report']['SENCase']['Signature'],
                'class' => 'senCaseSignature',
                'field' => $fields
            );
            $allTable[] = $table;
            
            unset($fields);
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectChi'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectEng'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectMat'];
            $fields[] = $Lang['eGuidance']['report']['SENCase']['SubjectGes'];
            $table = array(
                'title' => $Lang['eGuidance']['report']['SENCase']['TeacherSuggestion'],
                'class' => 'senCaseSuggestion',
                'field' => $fields
            );
            $allTable[] = $table;
        }
//        debug_pr($allTable);
    }
    // ### Sen Case END ####
    
    // ### Suspend START ####
    if (in_array('Suspend', $reportType)) {
        $data = array();
        if (count($allSuspendInfo[$studentID])) {
            foreach ((array) $allSuspendInfo[$studentID] as $suspendInfo) {
                $data[] = array(
                    $suspendInfo['StartDate'],
                    $suspendInfo['EndDate'],
                    ($suspendInfo['SuspendType'] == 'in') ? $Lang['eGuidance']['suspend']['InSchool'] : $Lang['eGuidance']['suspend']['OutSchool'],
                    $suspendInfo['Result']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Suspend'],
                'class' => 'suspendInfo',
                'colspan' => 4,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['General']['StartDate'],
                            $Lang['General']['EndDate'],
                            $Lang['eGuidance']['suspend']['SuspendType'],
                            $Lang['eGuidance']['suspend']['Result']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Suspend'],
                    'class' => 'suspendInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Suspend END ####
    
    // ### Transfer START ####
    if (in_array('Transfer', $reportType)) {
        if (count($allTransferInfo[$studentID])) {
            $transfer_reason = $libguidance->getGuidanceSettingItem('Transfer', 'TransferReason');
            $tbodyArr = array();
            $index = 1;
            $hasIndex = count($allTransferInfo[$studentID]) > 1;
            
            foreach ((array) $allTransferInfo[$studentID] as $transferInfo) {
                $transferID = $transferInfo['TransferID'];
                
                $transferReasonHTML = array();
                foreach ((array) $transferInfo['TransferReasonAry'] as $reason) {
                    if ($reason == 'Other') {
                        $transferReasonHTML[] = $transferInfo['TransferReasonOther'];
                    } else {
                        $transferReasonHTML[] = $transfer_reason[$reason];
                    }
                }
                $transferReasonHTML = implode('<br />', $transferReasonHTML);
                
                $data = array();
                $data[] = $transferInfo['TransferDate'];
                $data[] = $libguidance_ui->getTransferFromText($transferInfo);
                $data[] = $libguidance_ui->getTransferToText($transferInfo);
                $data[] = ($transferInfo['IsClassTeacherKnow']) ? $Lang['General']['Yes'] : $Lang['General']['No'];
                $data[] = $transferReasonHTML;
                $data[] = $transferInfo['Remark'];
                
                if ($hasIndex) {
                    $tbodyArr[] = array(
                        'type' => TABLE_TYPE_INDEX,
                        'index' => $index ++
                    );
                }
                
                $tbodyArr[] = array(
                    'type' => TABLE_TYPE_HORIZONTAL,
                    'field' => array(
                        $Lang['eGuidance']['transfer']['TransferDate'],
                        $Lang['eGuidance']['transfer']['TransferFrom'],
                        $Lang['eGuidance']['transfer']['TransferTo'],
                        $Lang['eGuidance']['transfer']['IsClassTeacherKnow'],
                        $Lang['eGuidance']['transfer']['TransferReason'],
                        $Lang['eGuidance']['transfer']['Remark']
                    ),
                    'data' => $data
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Transfer'],
                'class' => 'transferInfo',
                'tbody' => $tbodyArr
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Transfer'],
                    'class' => 'transferInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Transfer END ####
    
    // ### SelfImprove START ####
    if (in_array('SelfImprove', $reportType)) {
        if (count($allSelfImproveInfo[$studentID])) {
            $selfImprove_result = $libguidance->getGuidanceSettingItem('SelfImprove', 'Result');
            
            $data = array();
            foreach ((array) $allSelfImproveInfo[$studentID] as $selfImproveInfo) {
                $teacherHTML = array();
                foreach ((array) $allSelfImproveTeacher[$selfImproveInfo['ImproveID']] as $teacher) {
                    $teacherHTML[] = $teacher['Name'];
                }
                $teacherHTML = implode('<br />', $teacherHTML);
                
                $data[] = array(
                    $selfImproveInfo['StartDate'],
                    $selfImproveInfo['EndDate'],
                    $teacherHTML,
                    $selfImprove_result[$selfImproveInfo['Result']],
                    $selfImproveInfo['Remark']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['SelfImprove'],
                'class' => 'selfImproveInfo',
                'colspan' => 5,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['General']['StartDate'],
                            $Lang['General']['EndDate'],
                            $Lang['eGuidance']['TeacherName'],
                            $Lang['eGuidance']['selfimprove']['Result'],
                            $Lang['eGuidance']['Remark']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['SelfImprove'],
                    'class' => 'selfImproveInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### SelfImprove END ####
    
    $resultData[] = $allTable;
}
$countResultData = count($resultData);
// ####### Pack data END ########

// ############################### Load header to PDF START ################################
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=<?=($junior_mck) ? 'big5-hkscs' : 'utf-8'?>" />
<title></title>
<style type="text/css">
body {
	color: #000;
	font-family: msjh !important;
	font-size: 0.9em;
	line-height: 0.9em;
	margin: 0;
	padding: 0;
	-webkit-print-color-adjust: exact;
	font-stretch: condensed; /*font-size-adjust: 0.5;*/
}

/**** Header style START ****/
.schoolName, .reportType, .reportDetails {
	text-align: center;
}

.reportDetails {
	margin-top: 0;
}

.printTime {
	text-align: right;
}
/**** Header style END ****/

/**** Footer style START ****/
.footer {
	width: 100%;
}

.footer td {
	width: 33%;
	vertical-align: bottom;
	font-family: msjh !important;
	font-size: 8pt;
	color: #000000;
	font-weight: bold;
	padding: 3px;
	border: 0;
	border-top: 1px solid black;
}
/**** Footer style END ****/

/**** Basic table style START ****/
table {
	width: 200mm;
	border-collapse: collapse;
	margin-top: 3mm;
	overflow: visible;
}

.noRecordTd {
	text-align: center;
}

th, td {
	border: 1px solid #ddd;
	text-align: left;
	vertical-align: top;
	padding: 3mm;
}

tr.headerRow th {
	font-size: 1.5em;
	background-color: #487db4;
	color: white;
	text-align: center;
}

.tableVertical .field {
	text-align: center;
	white-space: nowrap;
	color: white;
	background-color: #59afc6;
}

.tableIndex td, .tableEmpty td {
	border: 0;
}

.tableEmpty td {
	font-size: 1px !important;
}

.tableIndex td {
	padding-bottom: 0;
}
/**** Basic table style END ****/

/**** Contact table style START ****/
.contactInfo .col_1 {
	width: 10mm;
}

.contactInfo .col_2 {
	width: 25mm;
}

.contactInfo .col_3 {
	width: 30mm;
}
/**** Contact table style END ****/

/**** Advanced class table style START ****/
.advancedClassInfo .col_1 {
	width: 10mm;
}

.advancedClassInfo .col_3 {
	width: 35mm;
}
/**** Advanced class table style END ****/

/**** Teherapy table style START ****/
.therapyInfo .col_1 {
	width: 40mm;
}

.therapyInfo .tableVertical th.col_1 {
	text-align: left;
	white-space: nowrap;
	color: black;
	background-color: white;
	border-bottom-width: 0;
}

.therapyInfo .tableVertical td.col_1 {
	border-bottom-width: 0;
	border-top-width: 0;
}

.therapyInfo .tableVertical td.last {
	border-bottom-width: 1px;
}

.therapyInfo .tableVertical .col_2 {
	width: 10mm;
}
/**** Teherapy table style END ****/

/**** Guidance table style START ****/
.guidanceInfo .col_1 {
	width: 40mm;
}
/**** Guidance table style END ****/

/**** SEN service table style START ****/
.senServiceInfo .col_1 {
	width: 40mm;
}

.senServiceInfo .tableVertical th.col_1 {
	text-align: left;
	white-space: nowrap;
	color: black;
	background-color: white;
	border-bottom-width: 0;
}

.senServiceInfo .tableVertical td.col_1 {
	border-bottom-width: 0;
	border-top-width: 0;
}

.senServiceInfo .tableVertical td.last {
	border-bottom-width: 1px;
}

.senServiceInfo .tableVertical .col_2 {
	width: 10mm;
}
/**** SEN service table style END ****/

/**** SEN case table style START ****/
.senCaseInfo .col_1 {
	width: 60mm;
}
.senFormImage {
    width: 11px;
    height: 11px;
}
.senCaseSignature td {
    text-align: center;
}
.pageBreakAvoid {
    page-break-inside: avoid;
}
/**** SEN case table style END ****/

/**** Suspend table style START ****/
.suspendInfo .col_1, .suspendInfo .col_2 {
	width: 10mm;
}
/**** Suspend table style END ****/

/**** Transfer table style START ****/
.transferInfo .col_1 {
	width: 60mm;
}
/**** Transfer table style END ****/

/**** No border table style START ****/
.noBorder table, .noBorder tr, .noBorder td {
	border: none;
}
/**** No border table style END ****/

</style>
</head>

<?php if($countResultData == 0): ?>
<h2 class="schoolName"><?=$school_name ?></h2>
<h3 class="reportType"><?=$report_title ?></h3>
<h4 class="reportDetails"><?=$report_time ?></h4>

<hr style="margin-top: 0; margin-bottom: 3mm;" />
<?php endif; ?>


<?php
$pageHeader = ob_get_clean();
if ($_GET['debug']) {
    echo $pageHeader;
} else {
    $mpdf->WriteHTML($pageHeader);
}
@ob_end_clean();
// ############################### Load header to PDF END ################################

// ############################### Load Data to PDF START ################################
if ($countResultData) :
    foreach ((array) $resultData as $resultIndex => $studentRecord) :
        $hasPageBreak = $resultIndex < $countResultData - 1;
        // ####### Header START ########
        ob_start();
        ?>
<h2 class="schoolName"><?=$school_name ?></h2>
<h3 class="reportType"><?=$report_title ?></h3>
<h4 class="reportDetails"><?=$reportDetails[$resultIndex]['reportTime'] ?></h4>
<h3 class="reportDetails"><?=$reportDetails[$resultIndex]['studentName'] ?></h3>
<?php if ($studentPrivateInfo):?>
<table style="border: 0px;">
	<tr>
		<td width="33%"><?php echo $reportDetails[$resultIndex]['STRN'];?></td>
		<td width="33%"><?php echo $reportDetails[$resultIndex]['gender'];?></td>
		<td width="33%"><?php echo $reportDetails[$resultIndex]['dateOfBirth'];?></td>
	</tr>
</table>
<?php endif;?>

<hr style="margin-top: 0; margin-bottom: 3mm;" />
<?php
        $header = ob_get_clean();
        $mpdf->WriteHTML($header);
        @ob_end_clean();
        // ####### Header END ########
        
        // ####### Footer START ########
        ob_start();
        ?>
<table class="footer">
	<tr>
		<td><?=$junior_mck ? convert2unicode($reportDetails[$resultIndex]['studentName'],true,1) : $reportDetails[$resultIndex]['studentName'] ?></td>
		<td style="text-align: center;"></td>
		<td style="text-align: right;">{PAGENO}</td>
	</tr>
</table>
<?php
        $footer = ob_get_clean();
        @ob_end_clean();
        // ####### Footer END ########
        
        // ####### Content START ########
        foreach ((array) $studentRecord as $index => $table) :
            ob_start();

            if ($table['class'] == 'senCaseSignature'):
?>

<div class="pageBreakAvoid">
    <div style="margin-left: 3mm; margin-top:5mm;"><?php echo $table['title'];?>:</div>
    
    <table class="<?php echo $table['class'];?>">
    	<tr>
    	<?php foreach ((array) $table['field'] as $field) :?>
    		<td><?php echo $field;?></td>
    	<?php endforeach;?>
    	</tr>
    	<tr>
    	<?php foreach ((array) $table['field'] as $field) :?>
    		<td>&nbsp;</td>
    	<?php endforeach;?>	
    	</tr>
    </table>
    
    <table class="noBorder">
    	<tr>
    		<td width="50%"></td>
    		<td width="8%" style="white-space: nowrap;"><?php echo $Lang['eGuidance']['report']['SENCase']['SignatureDate'];?>:</td>
    		<td width="42%"><hr style="margin-top: 4mm;" /></td>
    	</tr>
    </table>
</div>
<?php         
            elseif ($table['class'] == 'senCaseSuggestion'):
?>            
<div class="pageBreakAvoid">
    <div style="margin-left: 3mm; margin-top:5mm;"><?php echo $table['title'];?>:</div>
    <table class="<?php echo $table['class'];?>">
    	<tr>
    		<td colspan="8" style="text-align: center;"><?php echo $Lang['eGuidance']['report']['SENCase']['IsNeedAdjustmentNextYear'];?>
    			<img src="<?php echo $intranet_root;?>/images/<?php echo $LAYOUT_SKIN;?>/checkbox_off.png" border="0" class="senFormImage">
    			<?php echo $Lang['eGuidance']['report']['SENCase']['Add'];?>
    			<img src="<?php echo $intranet_root;?>/images/<?php echo $LAYOUT_SKIN;?>/checkbox.png" border="0" class="senFormImage">)
    		</td>
    	</tr>
    	<tr>            
    	<?php foreach ((array) $table['field'] as $field) :?>
    		<td rowspan="2"><?php echo $field;?>:</td>
    		<td><?php echo $Lang['eGuidance']['report']['SENCase']['Need'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    			<img src="<?php echo $intranet_root;?>/images/<?php echo $LAYOUT_SKIN;?>/checkbox_off.png" border="0" class="senFormImage">
    		</td>
    	<?php endforeach;?>
      	</tr>
      	<tr>
      	<?php foreach ((array) $table['field'] as $field) :?>
      		<td><?php echo $Lang['eGuidance']['report']['SENCase']['NotNeed'];?>&nbsp;
      			<img src="<?php echo $intranet_root;?>/images/<?php echo $LAYOUT_SKIN;?>/checkbox_off.png" border="0" class="senFormImage">
      		</td>
      	<?php endforeach;?>
      	</tr>
    </table>
</div>
<?php 
            else:       // start normal tbody
?>

<table class="<?=$table['class'] ?>">
	<tr class="headerRow">
		<th colspan="<?=($table['colspan'])?$table['colspan']:2 ?>"><?=$table['title'] ?></th>
	</tr>

<?php
            if (count($table['tbody'])) :
                foreach ((array) $table['tbody'] as $tbody) :
                    if ($tbody['type'] == TABLE_TYPE_EMPTY) :
                        ?>
	<tbody class="tableEmpty">
		<tr>
			<td></td>
		</tr>
	</tbody>
<?php
                     elseif ($tbody['type'] == TABLE_TYPE_INDEX) :
                        // // Index table START ////
                        ?>
    <tbody class="tableIndex">
		<tr>
			<td colspan="<?=($table['colspan'])?$table['colspan']:2 ?>">
				<h2>#<?=$tbody['index'] ?></h2>
			</td>
		</tr>
	</tbody>
<?php
                        // // Index table END ////
                    elseif ($tbody['type'] == TABLE_TYPE_HORIZONTAL) :
                        // // Horizontal table START ////
                        ?>
    <tbody class="tableHorizontal">
        			<?php foreach((array)$tbody['field'] as $fieldIndex => $field): ?>
		<tr>
			<th class="field col_1"><?=nl2br($field) ?></th>
			<td class="value col_2"
				colspan="<?=($tbody['data-cell-colspan'])?$tbody['data-cell-colspan']:1 ?>">
                    			<?=nl2br($tbody['data'][$fieldIndex]) ?>
                    		</td>
		</tr>
                	<?php endforeach; ?>
    </tbody>
<?php
                        // // Horizontal table END ////
                    elseif ($tbody['type'] == TABLE_TYPE_VERTICAL) :
                        // // Vertical table START ////
                        ?>
	<tbody class="tableVertical">
		<tr>
            			<?php foreach((array)$tbody['field'] as $fieldIndex => $field): ?>
                    		<th class="field col_<?=$fieldIndex+1 ?>">
                    			<?=nl2br($field) ?>
                    		</th>
                    	<?php endforeach; ?>
        </tr>
                	
<?php
                        $dataCount = count($tbody['data']);
                        if ($dataCount) :
                            foreach ((array) $tbody['data'] as $dataIndex => $data) :
                                ?>
        <tr>
<?php
                                foreach ($data as $fieldIndex => $value) :
                                    $css = ' col_' . ($fieldIndex + 1);
                                    if ($dataIndex == $dataCount - 1) {
                                        $css .= ' last';
                                    }
                                    ?>
                        		<td class="value <?=$css?>">
                        			<?=nl2br($value); ?>
                        		</td>
<?php
                                endforeach
                                ;
                                ?>
        </tr>
<?php
                            endforeach
                            ;
                        else :
                            ?>
        <tr>
			<td colspan="<?=count($tbody['field']) ?>" class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
		</tr>
<?php
                        endif;
                        ?>
    </tbody>
<?php
                        // // Vertical table END ////
                    endif;
                endforeach
                ;
            else :
                ?>
    <tr>
		<td colspan="<?=($table['colspan'])?$table['colspan']:2 ?>"
			class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
	</tr>
<?php
            endif;
            ?>
    
</table>
<?php
            endif;  // end normal tbody
        
            $page = ob_get_clean();
            if ($_GET['debug']) {
                echo $page;
            } else {
                $mpdf->SetHTMLFooter($footer);
                $mpdf->WriteHTML($page);
            }
            @ob_end_clean();
        endforeach
        ; // foreach($studentRecord as $index => $table)
          // ####### Content END ########
          
        // ####### Print time START ########
        ob_start();
        ?>

<h4 class="printTime"><?=$Lang['eGuidance']['report']['PrintTime']?>: <?=date('Y-m-d H:i:s') ?></h4>


<?php
        $page = ob_get_clean();
        if ($_GET['debug']) {
            echo $page;
        } else {
            $mpdf->SetHTMLFooter($footer);
            $mpdf->WriteHTML($page);
            if ($hasPageBreak) {
                $mpdf->WriteHTML('<pagebreak resetpagenum="1" suppress="off" />');
            }
        }
        @ob_end_clean();
        // ####### Print time END ########
    endforeach
    ; // foreach($resultData as $resultIndex => $studentRecord)

else :
    $mpdf->WriteHTML("<h1>{$Lang['General']['NoRecordAtThisMoment']}</h1>");
endif;
// ############################### Load Data to PDF END ################################

if (! $_GET['debug']) {
    $mpdf->Output();
}
@ob_end_clean();

