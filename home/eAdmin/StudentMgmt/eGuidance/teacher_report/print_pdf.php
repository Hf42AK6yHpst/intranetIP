<?
/**
 * modifying: 
 * 
 * Change Log:
 * 
 * 2020-05-14 Cameron
 *      - don't show the report section if there's no record in that section for the teacher if NotShowSectionWithoutRecord is set [case #M151033] 
 *      
 * 2020-03-03 Cameron
 *      - modify retrieving SEN case category and subcategory process to adapt the revised format. [case #D178788]
 *      
 * 2018-04-16 Cameron
 *      - Case type is retrieved from setup
 *  
 * 2018-02-26 Cameron
 *      - don't show confidential record if the login user does not have right, these include:
 *      Contact, Guidance, SEN Case, Suspend Study, Case Referral, Self Improvement Scheme
 *      - show follow-up advise column in Meeting/Contact
 *      
 * 2017-11-13 Cameron
 * 		- should include libinterface.php before libguidance_ui.php
 * 		- fix meta charset
 * 
 * 2017-08-21 Cameron
 * 		- add SelfImprove 
 * 		- retrieve all settings from db rather than from configure file
 * 		- show SENType no matter confirmed or not
 * 		- add followup advice in guidance  
 * 		- add individual study guide (suspend study)
 * 
 * 2017-06-28 Pun
 *  - New file
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");
include_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

// ####### Init START ########
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$libguidance_ui = new libguidance_ui();
$resultData = array();

define('TABLE_TYPE_EMPTY', 'tableEmpty');
define('TABLE_TYPE_INDEX', 'tableIndex');
define('TABLE_TYPE_HORIZONTAL', 'tableHorizontal');
define('TABLE_TYPE_VERTICAL', 'tableVertical');

$DateType = $_POST['DateType'];
if ($DateType == 'YEAR') {
    $AcademicYearID = $_POST['AcademicYearID'];
    $AcademicYearTermID = $_POST['AcademicYearTermID'];
    $StartDate = getStartDateOfAcademicYear($AcademicYearID, $AcademicYearTermID);
    $EndDate = getEndDateOfAcademicYear($AcademicYearID, $AcademicYearTermID);
} else {
    $StartDate = $_POST['StartDate'];
    $EndDate = $_POST['EndDate'];
}
$TeacherID = (array) $_POST['TeacherID'];
$notShowSectionWithoutRecord = $_POST['NotShowSectionWithoutRecord'];

$allTeacherName = array();
$reportDetails = array();
$currentAcademicYearID = Get_Current_Academic_Year_ID();
// ####### Init END ########

// ####### Access Right START ########
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('VIEW', (array) $permission['current_right']['REPORTS']['TEACHERREPORT'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
// ####### Access Right END ########

// ####### Init PDF START ########
$mpdf = new mPDF('', 'A4', 0, '', 5, 5, 5, 15);

// $mpdf->mirrorMargins = 1;
// ####### Init PDF END ########

// ####### Helper function START ########
function sortByDate($a, $b)
{
    return strcmp($a['Date'], $b['Date']);
}
// ####### Helper function END ########

// ####### Load all Data START ########
// ### Load Header Data START ####
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$report_title = "{$Lang['eGuidance']['report']['eGuidanceReport']} - {$Lang['eGuidance']['menu']['Reports']['TeacherReport']}";

if ($DateType == 'YEAR') {
    $academicYearTermInfo = getAcademicYearAndYearTermByDate($StartDate);
    $report_time = "{$academicYearTermInfo['AcademicYearName']} {$academicYearTermInfo['YearTermName']}";
} else {
    $report_time = "{$StartDate} {$Lang['General']['To']} {$EndDate}";
}
// ### Load Header Data END ####

// ### Get Teacher START ####
$allTeacherInfo = array();
$allTeacherId = $TeacherID;

$rs = $libguidance->getTeacher();
foreach ($rs as $r) {
    if (in_array($r['UserID'], $allTeacherId)) {
        $allTeacherInfo[$r['UserID']] = $r;
    }
}
// ### Get Teacher END ####

// ### Get contact START ####
if (in_array('Contact', $reportType)) {
    $allContactInfo = array();
    $allContactStudent = array();
    
    $rs = $libguidance->getContactByTeacherId($allTeacherId);
    foreach ($rs as $r) {
        if ($r['ContactDate'] < $StartDate || $EndDate < $r['ContactDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $r['Date'] = $r['ContactDate'];
        $allContactInfo[$r['TeacherID']][] = $r;
    }
    foreach ($allContactInfo as $teacherID => $data) {
        uasort($allContactInfo[$teacherID], 'sortByDate');
    }
    
    $allContactStudentId = Get_Array_By_Key($rs, 'StudentID');
    
    $rs = $libguidance->getStudent($allContactStudentId);
    foreach ($rs as $r) {
        $allStudentName[$r['StudentID']] = $r['Name'];
    }
}
// ### Get contact END ####

// ### Get advanced class START ####
if (in_array('AdvancedClass', $reportType)) {
    $allAdvancedClassInfo = array();
    $allAdvancedClassClassInfo = array();
    $allAdvancedClassStudentInfo = array();
    $allAdvancedClassLessonInfo = array();
    $allAdvancedClassClassTeacherMapping = array();
    
    $allAdvancedClassId = array();
    $rs = $libguidance->getAdvancedClassByTeacherId($allTeacherId);
    foreach ($rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        $allAdvancedClassClassInfo[$r['TeacherID']][$r['ClassID']] = $r;
        $allAdvancedClassId[] = $r['ClassID'];
        $allAdvancedClassClassTeacherMapping[$r['ClassID']][] = $r['TeacherID'];
    }
    
    $rs = $libguidance->getAdvancedClassStudent($allAdvancedClassId);
    foreach ($rs as $r) {
        $allAdvancedClassStudentInfo[$r['ClassID']][] = $r;
    }
    
    $rs = $libguidance->getAdvancedClassLesson($allAdvancedClassId);
    foreach ($rs as $r) {
        $allAdvancedClassLessonInfo[$r['ClassID']][] = $r;
    }
    
    foreach ($allAdvancedClassLessonInfo as $classID => $lessons) {
        $teacherIDs = (array) $allAdvancedClassClassTeacherMapping[$classID];
        
        $countLesson = count($lessons);
        if ($countLesson) {
            $countLesson = count($lessons);
            $lessonStartDate = $lessons[0]['LessonDate'];
            $lastIndex = max($countLesson - 1, 0);
            $lessonEndDate = $lessons[$lastIndex]['LessonDate'];
            
            foreach ($teacherIDs as $teacherID) {
                $allAdvancedClassInfo[$teacherID][$classID] = array(
                    'StartDate' => $lessonStartDate,
                    'EndDate' => $lessonEndDate,
                    'Remark' => $allAdvancedClassClassInfo[$teacherID][$classID]['Remark']
                );
            }
        }
    }
}
// ### Get advanced class END ####

// ### Get therapy START ####
if (in_array('Therapy', $reportType)) {
    $allTherapyInfo = array();
    $allTherapyTherapyInfo = array();
    $allTherapyStudentInfo = array();
    $allTherapyActivityInfo = array();
    $allTherapyActivityTeacherMapping = array();
    
    $allTherapyId = array();
    $rs = $libguidance->getTherapyByTeacherId($allTeacherId);
    foreach ($rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        $allTherapyTherapyInfo[$r['TeacherID']][$r['TherapyID']] = $r;
        $allTherapyId[] = $r['TherapyID'];
        $allTherapyActivityTeacherMapping[$r['TherapyID']][] = $r['TeacherID'];
    }
    
    $rs = $libguidance->getTherapyStudent($allTherapyId);
    foreach ($rs as $r) {
        $allTherapyStudentInfo[$r['TherapyID']][] = $r;
    }
    
    $rs = $libguidance->getTherapyActivity($allTherapyId);
    foreach ($rs as $r) {
        $allTherapyActivityInfo[$r['TherapyID']][] = $r;
    }
    
    foreach ($allTherapyActivityInfo as $therapyId => $activityInfos) {
        $teacherIDs = $allTherapyActivityTeacherMapping[$therapyId];
        foreach ($teacherIDs as $teacherID) {
            $allTherapyInfo[$teacherID][$therapyId] = $activityInfos;
        }
    }
}
// ### Get therapy END ####

// ### Get guidance START ####
if (in_array('Guidance', $reportType)) {
    $allGuidanceInfo = array();
    $allGuidanceStudentInfo = array();
    
    $allGuidanceStudentId = array();
    $rs = $libguidance->getGuidanceByTeacherId($allTeacherId);
    foreach ($rs as $r) {
        if ($r['ContactDate'] < $StartDate || $EndDate < $r['ContactDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $caseTypeAry = explode('^~', $r['CaseType']);
        $caseTypeOther = '';
        foreach ((array) $caseTypeAry as $k => $v) {
            if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
                $caseTypeAry[$k] = 'Other';
                $caseTypeOther = substr($v, 7);
            }
        }
        
        $followupAdviceAry = explode('^~', $r['FollowupAdvice']);
        $followupAdviceOther = '';
        foreach ((array) $followupAdviceAry as $k => $v) {
            if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
                $followupAdviceAry[$k] = 'Other';
                $followupAdviceOther = substr($v, 7);
            }
        }
        
        $r['Date'] = "{$r['ContactDate']} {$guidanceInfo['ContactTime']}";
        $r['CaseTypeArr'] = $caseTypeAry;
        $r['CaseTypeOther'] = $caseTypeOther;
        $r['FollowupAdviceArr'] = $followupAdviceAry;
        $r['FollowupAdviceOther'] = $followupAdviceOther;
        $allGuidanceInfo[$r['TeacherID']][$r['StudentID']][] = $r;
        $allGuidanceStudentId[] = $r['StudentID'];
    }
    foreach ($allGuidanceInfo as $teacherID => $d1) {
        foreach ($d1 as $studentID => $data) {
            uasort($allGuidanceInfo[$teacherID][$studentID], 'sortByDate');
        }
    }
    
    $rs = $libguidance->getStudent($allGuidanceStudentId);
    foreach ($rs as $r) {
        $allGuidanceStudentInfo[$r['StudentID']] = $r;
    }
}
// ### Get guidance END ####

// ### Get SEN service START ####
if (in_array('SEN', $reportType)) {
    $allSenServiceInfo = array();
    $allSenServiceServiceInfo = array();
    $allSenServiceStudentInfo = array();
    $allSenServiceActivityInfo = array();
    $allSenServiceActivityTeacherMapping = array();
    
    $allSenServiceId = array();
    $rs = $libguidance->getSENServiceByTeacherID($allTeacherId);
    foreach ($rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        $allSenServiceServiceInfo[$r['TeacherID']][$r['ServiceID']] = $r;
        $allSenServiceId[] = $r['ServiceID'];
        $allSenServiceActivityTeacherMapping[$r['ServiceID']][] = $r['TeacherID'];
    }
    
    $rs = $libguidance->getSENServiceStudent($allSenServiceId);
    foreach ($rs as $r) {
        $allSenServiceStudentInfo[$r['ServiceID']][] = $r;
    }
    
    $rs = $libguidance->getSENServiceActivity($allSenServiceId);
    foreach ($rs as $r) {
        $allSenServiceActivityInfo[$r['ServiceID']][] = $r;
    }
    
    foreach ($allSenServiceActivityInfo as $serviceID => $activities) {
        $teacherIDs = $allSenServiceActivityTeacherMapping[$serviceID];
        
        $countActivity = count($activities);
        if ($countActivity) {
            $activityStartDate = $activities[0]['ActivityDate'];
            $lastIndex = max($countActivity - 1, 0);
            $activityEndDate = $activities[$lastIndex]['ActivityDate'];
            
            foreach ($teacherIDs as $teacherID) {
                $allSenServiceInfo[$teacherID][$serviceID] = array(
                    'StartDate' => $activityStartDate,
                    'EndDate' => $activityEndDate,
                    'ServiceType' => $allSenServiceServiceInfo[$teacherID][$serviceID]['ServiceType']
                );
            }
        }
    }
}
// ### Get SEN service END ####

// ### Get SEN case START ####
if (in_array('SEN', $reportType)) {
    // get SEN case type and subtype
    $sen_case_type = $libguidance->getAllSENCaseType();
    $senCaseTypeAssoc = BuildMultiKeyAssoc($sen_case_type, array(
        'Code'
    ), $IncludedDBField = array(
        'Name'
    ), $SingleValue = 1);
    $sen_case_subtype = $libguidance->getAllSENCaseSubType();
    $senCaseSubTypeAssoc = BuildMultiKeyAssoc($sen_case_subtype, array(
        'TypeCode',
        'Code'
    ), $IncludedDBField = array(
        'Name'
    ), $SingleValue = 1);
    
    $allSenCaseInfo = array();
    $allSenCaseStudentInfo = array();
    
    // # Basic info START ##
    $rs = $libguidance->getSENCaseByTeacherID($allTeacherId);
    foreach ($rs as $r) {
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $senTypeAry = explode('^~', $r['SENType']);
        $senItemAry = array(); // items under SEN type: can be array or string , e.g. array(RD => array(RDC, RDE), MH =>"memtal health prob")
        foreach ((array) $senTypeAry as $k => $v) {
            
            if (strpos($v, '^:') !== false) {
                list ($senType, $senItemStr) = explode('^:', $v);
                $senTypeAry[$k] = $senType;
                $pos = strpos($senItemStr, '^@');
                $senItems = $senItemStr;
                if ($pos !== false) {
                    $senItems = substr($senItemStr, 0, $pos);
                    $senItemText = substr($senItemStr,$pos+2);
                }
                else {
                    $senItemText = '';
                }
                
                if (strpos($senItems, '^#') !== false) {
                    $subItemName = '';
                    if ($senItemText) {
                        $subItemName .= '-'.$senItemText;
                    }
                    $subItemName .= ': ';
                    $subItemNameAry = array();
                    $senSubItemAry = explode('^#', $senItems);
                    foreach ((array) $senSubItemAry as $_subItemCode) {
                        $subItemNameAry[] = $senCaseSubTypeAssoc[$senType][$_subItemCode];
                    }
                    $subItemName .= implode(', ', $subItemNameAry);
                    $senItemAry[$senType] = $subItemName;
                } else {
                    if ($senCaseTypeAssoc[$senType] && $senCaseSubTypeAssoc[$senType][$senItems]) {
                        $senItemAry[$senType] = ': ' . $senCaseSubTypeAssoc[$senType][$senItems];
                    } else {
                        $senItemAry[$senType] = ': ' . $senItems;
                    }
                }
            }
            else if (strpos($v, '^@') !== false) {
                list ($senType, $itemText) = explode('^@', $v);
                $senTypeAry[$k] = $senType;
                
                $subItemName = '';
                if ($itemText) {
                    $subItemName .= '-'.$itemText;
                }
                $senItemAry[$senType] = $subItemName;
            }
            else {
                $senItemAry[$k] = '';
            }
            
        }
        $r['SenTypeAry'] = $senTypeAry;
        $r['SenItemAry'] = $senItemAry;
        
        $allSenCaseInfo[$r['TeacherID']][] = $r;
        $allSenCaseStudentID[] = $r['StudentID'];
    }
    
    $rs = $libguidance->getStudent($allSenCaseStudentID);
    foreach ($rs as $r) {
        $allSenCaseStudentInfo[$r['StudentID']] = $r;
    }
    // # Basic info END ##
}
// ### Get SEN case END ####

// ### Get suspend START ####
if (in_array('Suspend', $reportType)) {
    $allSuspendInfo = array();
    $allSuspendStudent = array();
    
    $rs = $libguidance->getSuspendByTeacherId($allTeacherId);
    foreach ($rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $r['Date'] = $r['StartDate'];
        $allSuspendInfo[$r['TeacherID']][] = $r;
    }
    foreach ($allSuspendInfo as $teacherID => $data) {
        uasort($allSuspendInfo[$teacherID], 'sortByDate');
    }
    
    $allSuspendStudentId = Get_Array_By_Key($rs, 'StudentID');
    
    $rs = $libguidance->getStudent($allSuspendStudentId);
    foreach ($rs as $r) {
        $allStudentName[$r['StudentID']] = $r['Name'];
    }
}
// ### Get suspend END ####

// ### Get transfer START ####
if (in_array('Transfer', $reportType)) {
    $allTransferInfo = array();
    $allTransferStudentInfo = array();
    
    $allowTransferFromType = array(
        'DisciplineTeacher',
        'Teacher',
        'SocialWorker'
    );
    
    $allTransferStudentID = array();
    $rs = $libguidance->getTransfer();
    foreach ($rs as $r) {
        if ($r['TransferDate'] < $StartDate || $EndDate < $r['TransferDate']) {
            continue;
        }
        if (! in_array($r['TransferFromType'], $allowTransferFromType)) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $transferReasonAry = explode('^~', $r['TransferReason']);
        foreach ((array) $transferReasonAry as $k => $v) {
            if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
                $transferReasonAry[$k] = 'Other';
                $transferReasonOther = substr($v, 7);
            }
        }
        
        $r['Date'] = $r['TransferDate'];
        $r['TransferReasonAry'] = $transferReasonAry;
        $r['TransferReasonOther'] = $transferReasonOther;
        
        $allTransferInfo[$r['TransferFrom']][] = $r;
        $allTransferStudentID[] = $r['StudentID'];
    }
    foreach ($allTransferInfo as $teacherID => $data) {
        uasort($allTransferInfo[$teacherID], 'sortByDate');
    }
    
    $rs = $libguidance->getStudent($allTransferStudentID);
    foreach ($rs as $r) {
        $allTransferStudentInfo[$r['StudentID']] = $r;
    }
}
// ### Get transfer END ####

// ### Get selfImprove START ####
if (in_array('SelfImprove', $reportType)) {
    $allSelfImproveInfo = array();
    $allSelfImproveStudent = array();
    
    $rs = $libguidance->getSelfImproveByTeacherId($allTeacherId);
    foreach ($rs as $r) {
        if ($r['StartDate'] < $StartDate || $EndDate < $r['StartDate']) {
            continue;
        }
        
        if ($r['IsConfidential'] && ! empty($r['ConfidentialViewerID'])) {
            $confidentialViewerIDAry = explode(',', $r['ConfidentialViewerID']);
            if (! in_array($_SESSION['UserID'], $confidentialViewerIDAry)) {
                continue;
            }
        }
        
        $allSelfImproveId[] = $r['ImproveID'];
        
        $r['Date'] = $r['StartDate'];
        $allSelfImproveInfo[$r['TeacherID']][] = $r;
    }
    
    foreach ($allSelfImproveInfo as $teacherID => $data) {
        uasort($allSelfImproveInfo[$teacherID], 'sortByDate');
    }
    
    $allSelfImproveStudentId = Get_Array_By_Key($rs, 'StudentID');
    
    $rs = $libguidance->getStudent($allSelfImproveStudentId);
    foreach ($rs as $r) {
        $allStudentName[$r['StudentID']] = $r['Name'];
    }
}
// ### Get selfImprove END ####
// ####### Load all Data END ########

// ####### Pack data START ########
/**
 * Demo data
 * $table = array(
 * 'title' => 'abc',
 * 'class' => 'basicInfo',
 * 'colspan' => 4,
 * 'tbody' => array(
 * array(
 * 'type' => TABLE_TYPE_HORIZONTAL,
 * 'data-cell-colspan' => 3,
 * 'field' => array( 'Name' ),
 * 'data' => array('S1')
 * ),
 * array(
 * 'type' => TABLE_TYPE_VERTICAL,
 * 'field' => array( 'Date', 'Event', 'Teacher', 'Result' ),
 * 'data' => array(
 * array('2017-01-02', 'EV01', 'T1', 'Good'),
 * array('2017-01-03', 'EV02', 'T2', 'Bad'),
 * )
 * ),
 * )
 * );
 */
foreach ($allTeacherId as $teacherID) {
    
    $allTable = array();
    
    // ### Basic information START ####
    $reportDetails[] = array(
        'reportTime' => $report_time,
        'teacherName' => $allTeacherInfo[$teacherID]['Name']
    );
    // ### Basic information END ####
    
    // ### Contact START ####
    if (in_array('Contact', $reportType)) {
        if (count($allContactInfo[$teacherID])) {
            $contact_category = $libguidance->getGuidanceSettingItem('Contact', 'Category');
            
            $data = array();
            
            foreach ((array) $allContactInfo[$teacherID] as $contactInfo) {
                $data[] = array(
                    $contactInfo['ContactDate'],
                    $contact_category[$contactInfo['Category']],
                    $allStudentName[$contactInfo['StudentID']],
                    $contactInfo['Details'],
                    $contactInfo['FollowupAdvice']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Contact'],
                'class' => 'contactInfo',
                'colspan' => 5,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['Date'],
                            $Lang['eGuidance']['contact']['ContactCategory'],
                            $Lang['eGuidance']['StudentName'],
                            $Lang['eGuidance']['contact']['Details'],
                            $Lang['eGuidance']['FollowupAdvice']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Contact'],
                    'class' => 'contactInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Contact END ####
    
    // ### Advanced class START ####
    if (in_array('AdvancedClass', $reportType)) {
        if (count($allAdvancedClassInfo[$teacherID])) {
            $data = array();
            
            foreach ((array) $allAdvancedClassInfo[$teacherID] as $classID => $classInfo) {
                $studentHTML = array();
                foreach ((array) $allAdvancedClassStudentInfo[$classID] as $student) {
                    $studentHTML[] = $student['Name'];
                }
                $studentHTML = implode('<br />', $studentHTML);
                
                $data[] = array(
                    "{$classInfo['StartDate']} {$Lang['General']['To']} {$classInfo['EndDate']}",
                    $studentHTML,
                    $classInfo['Remark']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['AdvancedClass'],
                'class' => 'advancedClassInfo',
                'colspan' => 3,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['Date'],
                            $Lang['eGuidance']['StudentName'],
                            $Lang['eGuidance']['Remark']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['AdvancedClass'],
                    'class' => 'advancedClassInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Advanced class END ####
    
    // ### Therapy START ####
    if (in_array('Therapy', $reportType)) {
        if (count($allTherapyInfo[$teacherID])) {
            $data = array();
            
            foreach ((array) $allTherapyInfo[$teacherID] as $therapyId => $activities) {
                $studentHTML = array();
                foreach ((array) $allTherapyStudentInfo[$therapyId] as $student) {
                    $studentHTML[] = $student['Name'];
                }
                $studentHTML = implode('<br />', $studentHTML);
                
                $activityHTML = array();
                foreach ($activities as $activity) {
                    $activityHTML[] = "[{$activity['ActivityDate']}] {$activity['ActivityName']}";
                }
                $activityHTML = implode('<br />', $activityHTML);
                
                $data[] = array(
                    $allTherapyTherapyInfo[$teacherID][$therapyId]['GroupName'],
                    $activityHTML,
                    $studentHTML
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Therapy'],
                'class' => 'therapyInfo',
                'colspan' => 3,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['therapy']['GroupName'],
                            $Lang['eGuidance']['therapy']['ActivityName'],
                            $Lang['eGuidance']['StudentName']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Therapy'],
                    'class' => 'therapyInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Therapy END ####
    
    // ### Guidance START ####
    if (in_array('Guidance', $reportType)) {
        if (count($allGuidanceInfo[$teacherID])) {
            $tbodyArr = array();
            
            $guidance_type = $libguidance->getGuidanceSettingItem('Guidance', 'CaseType');
            $followup_advice = $libguidance->getGuidanceSettingItem('Guidance', 'FollowupAdvice');
            $contact_stage = $libguidance->getGuidanceSettingItem('Guidance', 'ContactStage');
            
            foreach ((array) $allGuidanceInfo[$teacherID] as $studentID => $guidanceInfos) {
                $tbodyArr[] = array(
                    'type' => TABLE_TYPE_INDEX,
                    'index' => $allGuidanceStudentInfo[$studentID]['Name']
                );
                
                $data = array();
                foreach ($guidanceInfos as $guidanceInfo) {
                    $guidanceId = $guidanceInfo['GuidanceID'];
                    
                    $caseTypeArr = array();
                    foreach ($guidanceInfo['CaseTypeArr'] as $caseType) {
                        if ($caseType == 'Other') {
                            $caseTypeArr[] = $guidanceInfo['CaseTypeOther'];
                        } else {
                            $caseTypeArr[] = $guidance_type[$caseType];
                        }
                    }
                    $caseTypeHTML = implode('<br />', $caseTypeArr);
                    
                    $followupAdviceArr = array();
                    foreach ($guidanceInfo['FollowupAdviceArr'] as $followupAdvice) {
                        if ($followupAdvice == 'Other') {
                            $followupAdviceArr[] = $guidanceInfo['FollowupAdviceOther'];
                        } else {
                            $followupAdviceArr[] = $followup_advice[$followupAdvice];
                        }
                    }
                    $followupAdviceHTML = implode('<br />', $followupAdviceArr);
                    
                    $contactTime = substr($guidanceInfo['ContactTime'], 0, 5);
                    
                    $data[] = array(
                        "{$guidanceInfo['ContactDate']}&nbsp;&nbsp;{$Lang['eGuidance']['guidance']['ContactTime']}:&nbsp;{$contactTime}",
                        $caseTypeHTML,
                        $contact_stage[$guidanceInfo['ContactStage']],
                        $followupAdviceHTML
                    );
                }
                
                $tbodyArr[] = array(
                    'type' => TABLE_TYPE_VERTICAL,
                    'field' => array(
                        $Lang['eGuidance']['guidance']['ContactDate'],
                        $Lang['eGuidance']['guidance']['CaseType'],
                        $Lang['eGuidance']['guidance']['ContactStage'],
                        $Lang['eGuidance']['FollowupAdvice']
                    ),
                    'data' => $data
                );
            }
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Guidance'],
                'class' => 'guidanceInfo',
                'colspan' => 4,
                'tbody' => $tbodyArr
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Guidance'],
                    'class' => 'guidanceInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Guidance END ####
    
    // ### Sen Service START ####
    if (in_array('SEN', $reportType)) {
        if (count($allSenServiceInfo[$teacherID])) {
            $sen_service_type = $libguidance->getGuidanceSettingItem('SENService', 'ServiceType');
            $data = array();
            
            foreach ((array) $allSenServiceInfo[$teacherID] as $serviceID => $serviceInfo) {
                
                $studentHTML = array();
                foreach ((array) $allSenServiceStudentInfo[$serviceID] as $student) {
                    $studentHTML[] = $student['Name'];
                }
                $studentHTML = implode('<br />', $studentHTML);
                
                if (substr($serviceInfo['ServiceType'], 0, 5) == 'Other') {
                    $serviceType = substr($serviceInfo['ServiceType'], 7);
                } else {
                    $serviceType = $sen_service_type[$serviceInfo['ServiceType']];
                }
                
                $data[] = array(
                    "{$serviceInfo['StartDate']} {$Lang['General']['To']} {$serviceInfo['EndDate']}",
                    $studentHTML,
                    $serviceType
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['sen_service']['SenService'],
                'class' => 'senServiceInfo',
                'colspan' => 3,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['Date'],
                            $Lang['eGuidance']['StudentName'],
                            $Lang['eGuidance']['sen_service']['ServiceType']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['sen_service']['SenService'],
                    'class' => 'senServiceInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Sen Service END ####
    
    // ### Sen Case START ####
    if (in_array('SEN', $reportType)) {
        if (count($allSenCaseInfo[$teacherID])) {
            $sen_case_type = $libguidance->getGuidanceSettingItem('SENCase', 'SENType');
            $data = array();
            
            foreach ((array) $allSenCaseInfo[$teacherID] as $caseInfo) {
                $studentID = $caseInfo['StudentID'];
                
                // # SEN Type START ##
                $senTypeHTML = array();
                foreach ((array) $caseInfo['SenTypeAry'] as $type) {
                    $typeHTML = $sen_case_type[$type];
                    // if ($caseInfo['SenItemAry']) {
                    // if (is_array($caseInfo['SenItemAry'][$type])) {
                    // $typeHTML .= ": ";
                    // $i = 0;
                    // foreach($caseInfo['SenItemAry'][$type] as $vv) {
                    // $typeHTML .= $i > 0 ? ", " : "";
                    // switch($vv) {
                    // case 'RDC':
                    // $typeHTML .= $Lang['eGuidance']['sen']['RD']['Chi'];
                    // break;
                    // case 'RDE':
                    // $typeHTML .= $Lang['eGuidance']['sen']['RD']['Eng'];
                    // break;
                    // }
                    // $i++;
                    // }
                    // }
                    // else if ($caseInfo['SenItemAry'][$type]){
                    // switch($caseInfo['SenItemAry'][$type]) {
                    // case 'RDC':
                    // $typeHTML .= ": ".$Lang['eGuidance']['sen']['RD']['Chi'];
                    // break;
                    // case 'RDE':
                    // $typeHTML .= ": ".$Lang['eGuidance']['sen']['RD']['Eng'];
                    // break;
                    // default:
                    // $typeHTML .= ": {$caseInfo['SenItemAry'][$type]}";
                    // break;
                    // }
                    // }
                    // }
                    if ($caseInfo['SenItemAry']) {
                        if (is_array($caseInfo['SenItemAry'][$type])) {
                            foreach ($caseInfo['SenItemAry'][$type] as $vv) {
                                $typeHTML .= $vv;
                            }
                        } else if ($caseInfo['SenItemAry'][$type]) {
                            $typeHTML .= $caseInfo['SenItemAry'][$type];
                        }
                    }
                    $senTypeHTML[] = $typeHTML;
                }
                $senTypeHTML = implode('<br />', $senTypeHTML);
                // # SEN Type END ##
                
                // $senTypeHTML = array();
                // foreach($caseInfo['SenTypeAry'] as $type){
                // $typeHTML = $guidance_cfg['sen_case_type'][ $type ];
                // if($type == 'Other'){
                // $typeHTML .= ": {$caseInfo['SenTypeOther']}";
                // }
                // $senTypeHTML[] = $typeHTML;
                // }
                // $senTypeHTML = implode('<br />', $senTypeHTML);
                
                $data[] = array(
                    $allSenCaseStudentInfo[$studentID]['Name'],
                    ($caseInfo['IsSEN']) ? $Lang['eGuidance']['sen']['Confirm'] : $Lang['eGuidance']['sen']['NotConfirm'],
                    $senTypeHTML
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['sen_service']['SenCase'],
                'class' => 'senCaseInfo',
                'colspan' => 3,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['StudentName'],
                            $Lang['eGuidance']['sen']['SENStudent'],
                            $Lang['eGuidance']['guidance']['CaseType']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {        
                $table = array(
                    'title' => $Lang['eGuidance']['sen_service']['SenCase'],
                    'class' => 'senCaseInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Sen Case END ####
    
    // ### Suspend START ####
    if (in_array('Suspend', $reportType)) {
        if (count($allSuspendInfo[$teacherID])) {
            
            $data = array();
            
            foreach ((array) $allSuspendInfo[$teacherID] as $suspendInfo) {
                $data[] = array(
                    $suspendInfo['StartDate'],
                    (empty($suspendInfo['EndDate']) || $suspendInfo['EndDate'] == '0000-00-00') ? '' : $suspendInfo['EndDate'],
                    $suspendInfo['SuspendType'] == 'in' ? $Lang['eGuidance']['suspend']['InSchool'] : $Lang['eGuidance']['suspend']['OutSchool'],
                    $allStudentName[$suspendInfo['StudentID']],
                    $suspendInfo['Result']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Suspend'],
                'class' => 'suspendInfo',
                'colspan' => 5,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['General']['StartDate'],
                            $Lang['General']['EndDate'],
                            $Lang['eGuidance']['suspend']['SuspendType'],
                            $Lang['eGuidance']['StudentName'],
                            $Lang['eGuidance']['suspend']['Result']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Suspend'],
                    'class' => 'suspendInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Suspend END ####
    
    // ### Transfer START ####
    if (in_array('Transfer', $reportType)) {
        if (count($allTransferInfo[$teacherID])) {
            $transfer_reason = $libguidance->getGuidanceSettingItem('Transfer', 'TransferReason');
            $data = array();
            
            foreach ((array) $allTransferInfo[$teacherID] as $transferInfo) {
                $studentID = $transferInfo['StudentID'];
                
                $transferReasonHTML = array();
                foreach ($transferInfo['TransferReasonAry'] as $reason) {
                    if ($reason == 'Other') {
                        $transferReasonHTML[] = $transferInfo['TransferReasonOther'];
                    } else {
                        $transferReasonHTML[] = $transfer_reason[$reason];
                    }
                }
                $transferReasonHTML = implode('<br />', $transferReasonHTML);
                
                $data[] = array(
                    $transferInfo['TransferDate'],
                    $allTransferStudentInfo[$studentID]['Name'],
                    $transferReasonHTML
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['Transfer'],
                'class' => 'transferInfo',
                'colspan' => 3,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['eGuidance']['transfer']['TransferDate'],
                            $Lang['eGuidance']['StudentName'],
                            $Lang['eGuidance']['transfer']['TransferReason']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['Transfer'],
                    'class' => 'transferInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### Transfer END ####
    
    // ### SelfImprove START ####
    if (in_array('SelfImprove', $reportType)) {
        if (count($allSelfImproveInfo[$teacherID])) {
            $selfImprove_result = $libguidance->getGuidanceSettingItem('SelfImprove', 'Result');
            
            $data = array();
            foreach ((array) $allSelfImproveInfo[$teacherID] as $selfImproveInfo) {
                $data[] = array(
                    $selfImproveInfo['StartDate'],
                    $selfImproveInfo['EndDate'],
                    $selfImprove_result[$selfImproveInfo['Result']],
                    $selfImproveInfo['Remark']
                );
            }
            
            $table = array(
                'title' => $Lang['eGuidance']['menu']['Management']['SelfImprove'],
                'class' => 'selfImproveInfo',
                'colspan' => 4,
                'tbody' => array(
                    array(
                        'type' => TABLE_TYPE_VERTICAL,
                        'field' => array(
                            $Lang['General']['StartDate'],
                            $Lang['General']['EndDate'],
                            $Lang['eGuidance']['selfimprove']['Result'],
                            $Lang['eGuidance']['Remark']
                        ),
                        'data' => $data
                    )
                )
            );
            $allTable[] = $table;
        } else {
            if (!$notShowSectionWithoutRecord) {
                $table = array(
                    'title' => $Lang['eGuidance']['menu']['Management']['SelfImprove'],
                    'class' => 'selfImproveInfo'
                );
                $allTable[] = $table;
            }
        }
    }
    // ### SelfImprove END ####
    
    $resultData[] = $allTable;
}
$countResultData = count($resultData);
// ####### Pack data END ########

// ############################### Load header to PDF START ################################
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=<?=($junior_mck) ? 'big5-hkscs' : 'utf-8'?>" />
<title></title>
<style type="text/css">
body {
	color: #000;
	font-family: msjh !important;
	font-size: 0.9em;
	line-height: 0.9em;
	margin: 0;
	padding: 0;
	-webkit-print-color-adjust: exact;
	font-stretch: condensed; /*font-size-adjust: 0.5;*/
}

/**** Header style START ****/
.schoolName, .reportType, .reportDetails {
	text-align: center;
}

.reportDetails {
	margin-top: 0;
}

.printTime {
	text-align: right;
}
/**** Header style END ****/

/**** Footer style START ****/
.footer {
	width: 100%;
}

.footer td {
	width: 33%;
	vertical-align: bottom;
	font-family: msjh !important;
	font-size: 8pt;
	color: #000000;
	font-weight: bold;
	padding: 3px;
	border: 0;
	border-top: 1px solid black;
}
/**** Footer style END ****/

/**** Basic table style START ****/
table {
	width: 200mm;
	border-collapse: collapse;
	margin-top: 3mm;
	overflow: visible;
}

.noRecordTd {
	text-align: center;
}

th, td {
	border: 1px solid #ddd;
	text-align: left;
	vertical-align: top;
	padding: 3mm;
}

tr.headerRow th {
	font-size: 1.5em;
	background-color: #487db4;
	color: white;
	text-align: center;
}

.tableVertical .field {
	text-align: center;
	white-space: nowrap;
	color: white;
	background-color: #59afc6;
}

.tableIndex td, .tableEmpty td {
	border: 0;
}

.tableEmpty td {
	font-size: 1px !important;
}

.tableIndex td {
	padding-bottom: 0;
}
/**** Basic table style END ****/

/**** Contact table style START ****/
.contactInfo .col_1 {
	width: 10mm;
}

.contactInfo .col_2 {
	width: 25mm;
}

.contactInfo .col_3 {
	width: 30mm;
}
/**** Contact table style END ****/

/**** Advanced class table style START ****/
.advancedClassInfo .col_1 {
	width: 10mm;
	text-align: center;
}

.advancedClassInfo .col_2 {
	width: 40mm;
}
/**** Advanced class table style END ****/

/**** Teherapy table style START ****/
.therapyInfo .col_3 {
	width: 40mm;
}
/**** Teherapy table style END ****/

/**** Guidance table style START ****/
.guidanceInfo .col_1 {
	width: 50mm;
}
/**** Guidance table style END ****/

/**** SEN service table style START ****/
.senServiceInfo .col_1, .senServiceInfo .col_2 {
	width: 50mm;
}
/**** SEN service table style END ****/

/**** SEN case table style START ****/
.senCaseInfo .col_1 {
	width: 50mm;
}

.senCaseInfo .col_2 {
	width: 20mm;
}
/**** SEN case table style END ****/

/**** Transfer table style START ****/
.transferInfo .col_1 {
	width: 10mm;
}

.transferInfo .col_2 {
	width: 50mm;
}
/**** Transfer table style END ****/
</style>
</head>

<?php if($countResultData == 0): ?>
    <h2 class="schoolName"><?=$school_name ?></h2>
<h3 class="reportType"><?=$report_title ?></h3>
<h4 class="reportDetails"><?=$report_time ?></h4>

<hr style="margin-top: 0; margin-bottom: 3mm;" />
<?php endif; ?>

<?php
$pageHeader = ob_get_clean();
if ($_GET['debug']) {
    echo $pageHeader;
} else {
    $mpdf->WriteHTML($pageHeader);
}
@ob_end_clean();
// ############################### Load header to PDF END ################################

// ############################### Load Data to PDF START ################################
if ($countResultData) :
    foreach ($resultData as $resultIndex => $teacherRecord) :
        $hasPageBreak = $resultIndex < $countResultData - 1;
        // ####### Header START ########
        ob_start();
        ?>
<h2 class="schoolName"><?=$school_name ?></h2>
<h3 class="reportType"><?=$report_title ?></h3>
<h4 class="reportDetails"><?=$reportDetails[$resultIndex]['reportTime'] ?></h4>
<h3 class="reportDetails"><?=$reportDetails[$resultIndex]['teacherName'] ?></h3>

<hr style="margin-top: 0; margin-bottom: 3mm;" />
<?php
        $header = ob_get_clean();
        $mpdf->WriteHTML($header);
        @ob_end_clean();
        // ####### Header END ########
        
        // ####### Footer START ########
        ob_start();
        ?>

<table class="footer">
	<tr>
		<td><?=$junior_mck ? convert2unicode($reportDetails[$resultIndex]['teacherName'],true,1) : $reportDetails[$resultIndex]['teacherName'] ?></td>
		<td style="text-align: center;"></td>
		<td style="text-align: right;">{PAGENO}</td>
	</tr>
</table>
<?php
        $footer = ob_get_clean();
        @ob_end_clean();
        // ####### Footer END ########
        
        // ####### Content START ########
        foreach ($teacherRecord as $index => $table) :
            ob_start();
            ?>
<table class="<?=$table['class'] ?>">
	<tr class="headerRow">
		<th colspan="<?=($table['colspan'])?$table['colspan']:2 ?>"><?=$table['title'] ?></th>
	</tr>

    
    <?php
            if (count($table['tbody'])) :
                foreach ($table['tbody'] as $tbody) :
                    if ($tbody['type'] == TABLE_TYPE_EMPTY) :
                        ?>
            	<tbody class="tableEmpty">
		<tr>
			<td></td>
		</tr>
	</tbody>
        	<?php
                     elseif ($tbody['type'] == TABLE_TYPE_INDEX) :
                        // // Index table START ////
                        ?>
            	<tbody class="tableIndex">
		<tr>
			<td colspan="<?=($table['colspan'])?$table['colspan']:2 ?>">
				<h2><?=$tbody['index'] ?></h2>
			</td>
		</tr>
	</tbody>
        	<?php
                        // // Index table END ////
                    elseif ($tbody['type'] == TABLE_TYPE_HORIZONTAL) :
                        // // Horizontal table START ////
                        ?>
                <tbody class="tableHorizontal">
        			<?php foreach($tbody['field'] as $fieldIndex => $field): ?>
                    	<tr>
			<th class="field col_1">
                    			<?=nl2br($field) ?>
                    		</th>
			<td class="value col_2"
				colspan="<?=($tbody['data-cell-colspan'])?$tbody['data-cell-colspan']:1 ?>">
                    			<?=nl2br($tbody['data'][$fieldIndex]) ?>
                    		</td>
		</tr>
                	<?php endforeach; ?>
            	</tbody>
        	<?php
                        // // Horizontal table END ////
                    elseif ($tbody['type'] == TABLE_TYPE_VERTICAL) :
                        // // Vertical table START ////
                        ?>
                <tbody class="tableVertical">
		<tr>
            			<?php foreach($tbody['field'] as $fieldIndex => $field): ?>
                    		<th class="field col_<?=$fieldIndex+1 ?>">
                    			<?=nl2br($field) ?>
                    		</th>
                    	<?php endforeach; ?>
                	</tr>
                	
        			<?php
                        $dataCount = count($tbody['data']);
                        if ($dataCount) :
                            foreach ($tbody['data'] as $dataIndex => $data) :
                                ?>
                    	<tr>
            				<?php
                                foreach ($data as $fieldIndex => $value) :
                                    $css = ' col_' . ($fieldIndex + 1);
                                    if ($dataIndex == $dataCount - 1) {
                                        $css .= ' last';
                                    }
                                    ?>
                        		<td class="value <?=$css?>">
                        			<?=nl2br($value) ?>
                        		</td>
                        	<?php
                                endforeach
                                ;
                                ?>
                    	</tr>
                	<?php
                            endforeach
                            ;
                        else :
                            ?>
            	    	<tr>
			<td colspan="<?=count($tbody['field']) ?>" class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
		</tr>
            	    <?php
                        endif;
                        ?>
            	</tbody>
        	<?php
                        // // Vertical table END ////
                    endif;
                endforeach
                ;
            else :
                ?>
    	<tr>
		<td colspan="<?=($table['colspan'])?$table['colspan']:2 ?>"
			class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
	</tr>
	<?php
            endif;
            ?>
    
</table>

<?php
            $page = ob_get_clean();
            if ($_GET['debug']) {
                echo $page;
            } else {
                $mpdf->SetHTMLFooter($footer);
                $mpdf->WriteHTML($page);
            }
            @ob_end_clean();
        endforeach
        ; // End foreach($teacherRecord as $index => $table)
                    // ####### Content END ########
                    
        // ####### Print time START ########
        ob_start();
        ?>

<h4 class="printTime"><?=$Lang['eGuidance']['report']['PrintTime']?>: <?=date('Y-m-d H:i:s') ?></h4>


<?php
        $page = ob_get_clean();
        if ($_GET['debug']) {
            echo $page;
        } else {
            $mpdf->SetHTMLFooter($footer);
            $mpdf->WriteHTML($page);
            if ($hasPageBreak) {
                $mpdf->WriteHTML('<pagebreak resetpagenum="1" suppress="off" />');
            }
        }
        @ob_end_clean();
        // ####### Print time END ########
    endforeach
    ; // foreach($resultData as $resultIndex => $studentRecord)

else :
    $mpdf->WriteHTML("<h1>{$Lang['General']['NoRecordAtThisMoment']}</h1>");
endif;
// ############################### Load Data to PDF END ################################

if (! $_GET['debug']) {
    $mpdf->Output();
}
@ob_end_clean();
 
