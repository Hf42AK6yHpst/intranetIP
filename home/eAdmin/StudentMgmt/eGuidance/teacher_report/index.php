<?
/**
 * Change Log:
 * 
 * 2020-05-14 Cameron
 *      - add report type option: NotShowSectionWithoutRecord [case #M151033]
 *      
 * 2018-03-14 Cameron
 *      - set default semester to current semester
 *      
 * 2017-11-13 Cameron
 * 		- hide AcademicYear and Term selection for ej as it cannot determined the start and end date of past years' terms
 * 
 * 2017-08-21 Cameron
 * 	- add report type: SelfImprove
 * 	- break teacher list into teaching and non-teaching
 * 
 * 2017-06-28 Pun
 *  - New file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");


######## Init START ########
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$linterface = new interface_html();
$libguidance_ui = new libguidance_ui();

$currentAcademicYearID = Get_Current_Academic_Year_ID();
######## Init END ########


######## Access Right START ########
$permission = $libguidance->getUserPermission();

if (!$permission['admin'] && !in_array('VIEW', (array)$permission['current_right']['REPORTS']['TEACHERREPORT'])) {	
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
######## Access Right END ########


######## Get Date selection START ########
if (!$junior_mck) {
	$academicYearHTML = getSelectAcademicYear($objName='AcademicYearID', $tag='', $noFirst=1, $noPastYear=0, $targetYearID=$currentAcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array());
	$currentSemesterID = $_POST['AcademicYearTermID'] ? $_POST['AcademicYearTermID'] : getCurrentSemesterID();
	$academicYearTermHTML = getSelectSemester2($tags=' id="AcademicYearTermID" name="AcademicYearTermID"', $selected=$currentSemesterID, $ParQuoteValue=1, $AcademicYearID=$currentAcademicYearID);
}
######## Get Date selection END ########


######## Get Start Date START ########
$StartDate = getStartDateOfAcademicYear($currentAcademicYearID);
$StartDate = substr($StartDate, 0, 10);
$EndDate = '';
######## Get Start Date END ########


######## Get Teacher Selection START ########
$teachingStaffAry = $libguidance->getTeacher($teacherType='1');
$teachingStaffAry = build_assoc_array($teachingStaffAry);
$nonTeachingStaffAry = $libguidance->getTeacher($teacherType='0');
$nonTeachingStaffAry = build_assoc_array($nonTeachingStaffAry);
$data = array (	$Lang['Identity']['TeachingStaff'] => $teachingStaffAry,
				$Lang['Identity']['NonTeachingStaff'] => $nonTeachingStaffAry
			  );
$teacherSelect = getSelectByAssoArray($data,"name='TeacherID[]' id='TeacherID[]' style='display:block'", '');

//$teacherSelect = $libguidance_ui->getTeacherList('TeacherID[]', '', 'block');
######## Get Teacher Selection END ########


######## Report Type START ########
$allReportType = array();
$allReportType[] = array(
    'value' => 'Contact',
    'title' => $Lang['eGuidance']['menu']['Management']['Contact']
);
$allReportType[] = array(
    'value' => 'AdvancedClass',
    'title' => $Lang['eGuidance']['menu']['Management']['AdvancedClass']
);
$allReportType[] = array(
    'value' => 'Therapy',
    'title' => $Lang['eGuidance']['menu']['Management']['Therapy']
);
$allReportType[] = array(
    'value' => 'Guidance',
    'title' => $Lang['eGuidance']['menu']['Management']['Guidance']
);
$allReportType[] = array(
    'value' => 'SEN',
    'title' => $Lang['eGuidance']['menu']['Management']['SEN']
);
$allReportType[] = array(
    'value' => 'Suspend',
    'title' => $Lang['eGuidance']['menu']['Management']['Suspend']
);
$allReportType[] = array(
    'value' => 'Transfer',
    'title' => $Lang['eGuidance']['menu']['Management']['Transfer']
);

if ($plugin['eGuidance_module']['SelfImprove']) {
	$allReportType[] = array(
	    'value' => 'SelfImprove',
	    'title' => $Lang['eGuidance']['menu']['Management']['SelfImprove']
	);
}
######## Report Type END ########


######## UI START ########
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageTeacherReport";
$CurrentPageName = $Lang['eGuidance']['name'];

$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Reports']['TeacherReport'], "", true);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

$star = $linterface->RequiredSymbol();
?>



<form id="form1" method="post" action="print_pdf.php" target="_blank">
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table class="form_table_v30">

				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Date'] . $star ?></td>
					<td>
					<? if ($junior_mck):?>
						<div class="dateTypeDiv">
							<input type="hidden" id="dateTypeDateRange" name="DateType" value="DATE_RANGE" />
    						<label for="StartDate"><?=$Lang['General']['From']?></label>
    						<?=$linterface->GET_DATE_PICKER("StartDate",$StartDate)?>
    						<label for="EndDate"><?=$Lang['General']['To']?></label>
    						<?=$linterface->GET_DATE_PICKER("EndDate",$EndDate)?>
						</div>
					<? else: ?>
						<div class="dateTypeDiv">
							<input type="radio" id="dateTypeYear" name="DateType" value="YEAR" checked/>
							<label for="AcademicYearID"><?=$Lang['General']['SchoolYear'] ?></label>
							<span id="academicYearDiv"><?=$academicYearHTML ?></span>
							<label for="AcademicYearTermID"><?=$Lang['General']['Term'] ?></label>
							<span id="academicYearTermDiv"><?=$academicYearTermHTML ?></span>
						</div>
						
						<div class="dateTypeDiv" style="margin-top: 15px;">
							<input type="radio" id="dateTypeDateRange" name="DateType" value="DATE_RANGE" />
    						<label for="StartDate"><?=$Lang['General']['From']?></label>
    						<?=$linterface->GET_DATE_PICKER("StartDate",$StartDate)?>
    						<label for="EndDate"><?=$Lang['General']['To']?></label>
    						<?=$linterface->GET_DATE_PICKER("EndDate",$EndDate)?>
						</div>
					<? endif;?>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName'] . $star ?></td>
					<td>
						<?=$teacherSelect ?>
					</td>
				</tr>

				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['report']['ReportType'] ?></td>
					<td>
						<input type="checkbox" name="NotShowSectionWithoutRecord" id="NotShowSectionWithoutRecord" value="1" checked>
						<label for="NotShowSectionWithoutRecord"><?=$Lang['eGuidance']['report']['NotShowSectionWithoutRecord']?></label>
					</td>
				</tr>

				<tr style="display:none;">
					<td valign="top" rowspan="2" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['report']['ReportType'] . $star ?></td>
					<td>
						<input type="checkbox" id="selectAllType" checked/>
						<label for="selectAllType"><?=$Lang['Btn']['SelectAll'] ?></label>

						<?php foreach($allReportType as $index=>$reportType): ?>
    						<?php if($index%3 == 0): ?>
    							<br />
    						<?php endif; ?>
    						
    						<input type="checkbox" id="reportType_<?=$reportType['value'] ?>" name="reportType[]" value="<?=$reportType['value'] ?>" checked/>
    						<label for="reportType_<?=$reportType['value'] ?>"><?=$reportType['title'] ?></label>&nbsp;&nbsp;
						<?php endforeach; ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>

</table>
<div class="tabletextremark"><?=$i_general_required_field?></div>

<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['report']['GenerateReport'], "submit")?>
</div>

</form>







<script>
$(document).ready(function(){
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	//////// Helper function START ////////
	function show_ajax_error() {
		alert('<?=$Lang['eGuidance']['error']['ajax']?>');
	}
	//////// Helper function END ////////
	
	
	//////// Date select START ////////
	$('#AcademicYearID, #academicYearTermDiv, #StartDate, #EndDate').click(function(){
		$(this).closest('div').find('[name="DateType"]').click();
	});
	$('#AcademicYearID').change(function(){
        $('#academicYearTermDiv').html(loadingImg);

		var data = {
			'AcademicYearID': $(this).val(),
			'tags': " id='AcademicYearTermID' name='AcademicYearTermID'"
		};
		
		$.ajax({
    		dataType: "json", 
            type: 'post',
            url: '../ajax/ajax.php?action=getAcademicYearTermSelect', 
            data: data,
            success: function(ajaxReturn) {
                var academicYearTermSelect = ajaxReturn['html'];
                $('#academicYearTermDiv').html(academicYearTermSelect);
            },
            error: show_ajax_error
		})
	});
	//////// Date select END ////////

	
	//////// SelectAll toggle START ////////
	$('#selectAllType').change(function(){
		var checked = $(this).attr('checked');

		var $checkbox = $(this).parent().find('input[id^="reportType_"]');
		$checkbox.attr('checked', checked);
	});
	$('input[id^="reportType_"]').change(function(){
		var checked = $(this).attr('checked');
		if(!checked){
			$('#selectAllType').attr('checked', false);
		}
	});
	//////// SelectAll toggle END ////////
	
	
	//////// Form submit checking START ////////
	$('#form1').submit(function(e){
		if($('[name="TeacherID\[\]"]').val() == ''){
			alert('<?=$Lang['eGuidance']['Warning']['SelectTeacher'] ?>');
			e.preventDefault();
			return;
		}

		if($('[name="reportType\[\]"]:checked').length == 0){
			alert('<?=$Lang['eGuidance']['Warning']['SelectReportType'] ?>');
			e.preventDefault();
			return;
		}
	});
	//////// Form submit checking END ////////
	
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();