<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 * 	2017-08-21 [Cameron]
 * 		- don't show NoRight column
 *  
 * 	2017-05-19 [Cameron]
 * 		- temporary disable report section
 * 
 * 	2017-03-15 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['SETTINGS']['PERMISSIONSETTING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$libguidance_ui = new libguidance_ui();

if (count($GroupID)==1) {
	$rs_group = $libguidance->getAccessRightGroupInfo($GroupID);

	if (count($rs_group) == 1) {
		$rs_group = $rs_group[0];
		$rs_access_right = $libguidance->getAccessRight($GroupID);
		$rs_access_right = BuildMultiKeyAssoc($rs_access_right, 'Function');
	}
}
$image_tick = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif' width='20' height='20' align='absmiddle'>";
$image_blank = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' align='absmiddle'>";

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PagePermissionSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Settings']['PermissionSetting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['settings']['Permission']['GroupList'], "index.php");
$PAGE_NAVIGATION[] = array($rs_group['GroupTitle'], "");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$subTag[] = array($Lang['eGuidance']['settings']['Permission']['GeneralAccessRight'], "view.php?GroupID=$GroupID", 1);
$subTag[] = array($Lang['eGuidance']['settings']['Permission']['MemberList'], "member_list.php?GroupID=$GroupID", 0);

$linterface->LAYOUT_START();

?>
<form name="form1" id="form1" method="post" action="view.php" onSubmit="return checkForm(form1)">
<table id="main_table" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['GroupTitle']?>
									</td>
								<td class="tabletext"><?=$rs_group['GroupTitle'] ? intranet_htmlspecialchars($rs_group['GroupTitle']) : '-'?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['GroupDescription']?>
									</td>
								<td class="tabletext"><?=$rs_group['GroupDescription'] ? nl2br($rs_group['GroupDescription']) : '-'?></td>
							</tr>
						</table>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tab_underline">
									<div class="shadetabs">
										<ul>
											<?=$libguidance_ui->getSubTag($subTag)?>
										</ul>
									</div>
								</td>
							</tr>
						</table>
						
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td><?=$linterface->GET_NAVIGATION2($Lang['eGuidance']['settings']['Permission']['GroupPermission']);?></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="form_sep_title"><i>- <?=$Lang['eGuidance']['menu']['main']['Management'] ?> -</i>
									<table class="common_table_list_v30 ">
										<tr class="tabletop">
											<th width="30%"><?=$Lang['eGuidance']['Item']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['View']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['Management']?></th>
	<!--										<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['NoRights']?></th>-->
										</tr>
								<? 	foreach((array)$Lang['eGuidance']['menu']['Management'] as $k=>$v) {
										if (!$plugin['eGuidance_module']['SelfImprove'] && ($k == 'SelfImprove')) {
											$x = '';
										}
										else {
											$x = '<tr>';
												$x .= '<td>'.$v.'</td>';
												$x .= '<td>'.($rs_access_right[$k]['Action']=='view' ? $image_tick : $image_blank).'</td>';
												$x .= '<td>'.($rs_access_right[$k]['Action']=='mgmt' ? $image_tick : $image_blank).'</td>';
	//											$x .= '<td>'.($rs_access_right[$k]['Action']=='none' ? $image_tick : $image_blank).'</td>';
											$x .= '<tr>';
										}
										echo $x;
									}
								?>
									</table>									
								</td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="form_sep_title"><i>- <?=$Lang['eGuidance']['menu']['main']['Reports'] ?> -</i>
									<table class="common_table_list_v30 ">
										<tr class="tabletop">
											<th width="30%"><?=$Lang['eGuidance']['Item']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['View']?></th>
<!--											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['NoRights']?></th>-->
										</tr>
								<? 	foreach((array)$Lang['eGuidance']['menu']['Reports'] as $k=>$v) {
										$x = '<tr>';
											$x .= '<td>'.$v.'</td>';
											$x .= '<td>'.($rs_access_right[$k]['Action']=='view' ? $image_tick : $image_blank).'</td>';
//											$x .= '<td>'.($rs_access_right[$k]['Action']=='none' ? $image_tick : $image_blank).'</td>';
										$x .= '<tr>';
										echo $x;
									}
								?>
									</table>									
								</td>
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="form_sep_title"><i>- <?=$Lang['eGuidance']['menu']['main']['Settings'] ?> -</i>
									<table class="common_table_list_v30 ">
										<tr class="tabletop">
											<th width="30%"><?=$Lang['eGuidance']['Item']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['View']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['Management']?></th>
<!--											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['NoRights']?></th>-->
										</tr>
								<? 	foreach((array)$Lang['eGuidance']['menu']['Settings'] as $k=>$v) {
										$x = '<tr>';
											$x .= '<td>'.$v.'</td>';
											$x .= '<td>'.($rs_access_right[$k]['Action']=='view' ? $image_tick : $image_blank).'</td>';
											$x .= '<td>'.($rs_access_right[$k]['Action']=='mgmt' ? $image_tick : $image_blank).'</td>';
//											$x .= '<td>'.($rs_access_right[$k]['Action']=='none' ? $image_tick : $image_blank).'</td>';
										$x .= '<tr>';
										echo $x;
									}
								?>
									</table>									
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
							<? 	if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {
									echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit.php?GroupID=$GroupID'").'&nbsp';
								}
								echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");							
							?>									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="GroupID" name="GroupID" value="<?=$GroupID?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>