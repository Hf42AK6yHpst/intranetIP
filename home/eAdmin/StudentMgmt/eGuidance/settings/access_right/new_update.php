<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-05-19 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-03-15 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();
	
$dataAry['GroupTitle'] = standardizeFormPostValue($GroupTitle);
$dataAry['GroupDescription'] = standardizeFormPostValue($GroupDescription);
$dataAry['GroupType'] = 'A';
$dataAry['DateInput'] = 'now()';
$dataAry['Module'] = $libguidance->Module;
$sql = $libguidance->INSERT2TABLE('ACCESS_RIGHT_GROUP',$dataAry,array(),false);

$ldb->Start_Trans();
## step 1: add to access right group
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
	$groupID = $ldb->db_insert_id();
	## step 2: add access right group setting
	foreach((array)$Perm as $section=>$sec) {
		foreach((array)$sec as $function=>$action) {
			if ($action != 'none') {
				unset($dataAry);
				$dataAry['GroupID'] = $groupID;
				$dataAry['Module'] = $libguidance->Module;
				$dataAry['Section'] = $section;
				$dataAry['Function'] = $function;
				$dataAry['Action'] = $action;
				$dataAry['DateInput'] = 'now()';
				if (!$junior_mck) {
					$dataAry['LastModifiedBy'] = $_SESSION['UserID'];	
				}
				$sql = $libguidance->INSERT2TABLE('ACCESS_RIGHT_GROUP_SETTING',$dataAry,array(),false);
				$result[] = $ldb->db_db_query($sql);
			}
		}		
	}
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'AddSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'AddUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


