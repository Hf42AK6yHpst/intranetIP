<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-03-16 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();
$dataAry = array();

if (($GroupID) && count($TeacherID)) {
	$ldb->Start_Trans();
	
	foreach((array)$TeacherID as $id) {
		unset($dataAry);
		$dataAry['GroupID'] = $GroupID;
		$dataAry['UserID'] = $id;
		$dataAry['DateInput'] = 'now()';
		$sql = $libguidance->INSERT2TABLE('ACCESS_RIGHT_GROUP_MEMBER',$dataAry,array(),false,true,false);	// insert ignore
		$result[] = $ldb->db_db_query($sql);
	}

	if (!in_array(false,$result)) {
		$ldb->Commit_Trans();
		$returnMsgKey = 'AddSuccess';
	}
	else {
		$ldb->RollBack_Trans();
		$returnMsgKey = 'AddUnsuccess';
	}
}
else {
	$returnMsgKey = 'AddUnsuccess';
}	

header("location: member_list.php?GroupID=".$GroupID."&returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


