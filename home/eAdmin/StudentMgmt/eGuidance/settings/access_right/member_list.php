<?php 
/*
 * 	Log
 * 
 *	2017-11-20 [Cameron]
 *		- fix cookie
 *
 * 	2017-03-16 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['SETTINGS']['PERMISSIONSETTING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$libguidance_ui = new libguidance_ui();

if (count($GroupID)==1) {
	$rs_group = $libguidance->getAccessRightGroupInfo($GroupID);
	if (count($rs_group) == 1) {
		$rs_group = $rs_group[0];
	}	
}

# Page heading setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PagePermissionSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Settings']['PermissionSetting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['settings']['Permission']['GroupList'], "index.php");
$PAGE_NAVIGATION[] = array($rs_group['GroupTitle'], "view.php?GroupID=".$GroupID);

$subTag[] = array($Lang['eGuidance']['settings']['Permission']['GeneralAccessRight'], "view.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eGuidance']['settings']['Permission']['MemberList'], "member_list.php?GroupID=$GroupID", 1);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;

$name_field = getNameFieldByLang('b.');

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT 
				$name_field as MemberName,
				LEFT(a.DateInput,10) as DateInput ";
if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {
	$sql .= ",CONCAT('<input type=\'checkbox\' name=\'MemberID[]\' value=\'', a.UserID, '\'>') ";
	$extra_column = 2;
	$checkbox_col = "<th width='1'>".$li->check("MemberID[]")."</th>\n";
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new_member.php?GroupID=$GroupID')",$button_new,"","","",0);
	$manage_record_bar = '<a href="javascript:checkRemove(document.form1,\'MemberID[]\',\'remove_member.php\')" class="tool_delete">'.$button_delete.'</a>';
}		
else {
	$extra_column = 1;
	$checkbox_col = "";
	$toolbar = "";
	$manage_record_bar  = '';
}
$sql .= "FROM
				ACCESS_RIGHT_GROUP_MEMBER as a 
				LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID=b.UserID)
		WHERE 
				GroupID='".$GroupID."'";

$li->field_array = array("MemberName", "DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='60%' >".$li->column($pos++, $Lang['eGuidance']['settings']['Permission']['MemberName'])."</th>\n";
$li->column_list .= "<th width='35%'>".$li->column($pos++, $Lang['eGuidance']['settings']['Permission']['InputDate'])."</th>\n";
$li->column_list .= $checkbox_col;



?>

<form name="form1" method="post" action="member_list.php">
<table id="main_table" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['GroupTitle']?></td>
								<td class="tabletext"><?=$rs_group['GroupTitle'] ? intranet_htmlspecialchars($rs_group['GroupTitle']) : '-'?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['GroupDescription']?></td>
								<td class="tabletext"><?=$rs_group['GroupDescription'] ? nl2br($rs_group['GroupDescription']) : '-'?></td>
							</tr>
						</table>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tab_underline">
									<div class="shadetabs">
										<ul>
											<?=$libguidance_ui->getSubTag($subTag)?>
										</ul>
									</div>
								</td>
							</tr>
						</table>
			
		
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="20%"><?=$toolbar ?></td>
								<td width="50%" align="center">&nbsp;</td>
								<td width="30%" style="float: right;">
									<div class="content_top_tool"  style="float: right;">
										<br style="clear:both" />
									</div>
								</td>
							</tr>
						</table>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="bottom">
									<div class="table_filter">
									</div> 
								</td>
								<td valign="bottom">
									<div class="common_table_tool">
										<?=$manage_record_bar ?>										
									</div>
								</td>
							</tr>
						</table>
					
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="bottom">
					
									<?=$li->display()?>
					 
								</td>
							</tr>
						</table>

					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
	
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

	<input type="hidden" name="GroupID" value="<?=$GroupID?>" />
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>