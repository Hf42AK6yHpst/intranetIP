<?
/*
 * Log
 *
 * 2018-04-13 [Cameron]
 * - create this file
 */
?>
<script type="text/javascript">

function checkForm(obj) 
{
	if(!check_text(obj.Code,'<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeCode']?>'))
	{
		return false;
	}
	if(!check_text(obj.ChineseName,'<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeChineseName']?>'))
	{
		return false;
	}
	if(!check_text(obj.EnglishName,'<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeEnglishName']?>'))
	{
		return false;
	}
	if(obj.Code.value.match(/[!"#$%&\\'*,\/:;<=>?@^`|~]/g)) {
		alert("<?= str_replace(array('\\','"'),array('\\\\','\"'),$Lang['eGuidance']['settings']['Warning']["SymbolsExclude"]) ?>");	
		return false;
	}		
	return true;
}

$(document).ready(function(){
	
	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    var url;
<? if ($form_action == "edit_sen_case_subtype_update.php"):?>
		url = '../../ajax/ajax.php?action=checkBeforeUpdateSenCaseSubType'
<? else:?>
		url = '../../ajax/ajax.php?action=checkBeforeAddSenCaseSubType'
<? endif;?>

		if (checkForm(document.form1)) {
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: url, 
	            data: $('#form1').serialize(),
	            async: false,            
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '1') {
		        			alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateCaseSubType']?>');
		        			$('input.actionBtn').attr('disabled', '');
		        		}
		        		else {
		        			$('#form1').submit();
		        		}
		        	}
		        	else {
		        		$('input.actionBtn').attr('disabled', '');
		        	}
		        },
				error: show_ajax_error
	        });
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
		
});

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}
	
</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
	<table id="main_table" width="98%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['CaseTypeCode']?>
									<span class="tabletextrequire">*</span></td>
									<td class="tabletext"><input type="text" name="Code" id="Code"
										class="textboxtext"
										value="<?=intranet_htmlspecialchars($rs_sen_case_subtype['Code'])?>"></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['CaseSubTypeChineseName']?>
									<span class="tabletextrequire">*</span></td>
									<td class="tabletext"><input type="text" name="ChineseName"
										id="ChineseName" class="textboxtext"
										value="<?=intranet_htmlspecialchars($rs_sen_case_subtype['ChineseName'])?>"></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['CaseSubTypeEnglishName']?>
									<span class="tabletextrequire">*</span></td>
									<td class="tabletext"><input type="text" name="EnglishName"
										id="EnglishName" class="textboxtext"
										value="<?=intranet_htmlspecialchars($rs_sen_case_subtype['EnglishName'])?>"></td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
									<td width="80%">&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='sen_case_subtype_index.php?SettingID=$settingID'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br>
			</td>
		</tr>
	</table>
	<input type=hidden id="SettingID" name="SettingID"
		value="<?=$settingID?>"> <input type=hidden id="SubTypeID"
		name="SubTypeID" value="<?=$subTypeID?>">
</form>
