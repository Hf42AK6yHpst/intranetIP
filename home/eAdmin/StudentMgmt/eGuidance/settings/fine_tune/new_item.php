<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-07-21 [Cameron]
 * 		- redirect to listview if $TypeID is empty
 * 
 * 	2017-03-16 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$TypeID) {
	header("location: index.php");
}

$linterface = new interface_html();

if (count($TypeID)==1) {
	$rs_adjust_type = $libguidance->getAdjustType($TypeID);
	if (count($rs_adjust_type) == 1) {
		$rs_adjust_type = current($rs_adjust_type);
		if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
			$adjust_type_name = $rs_adjust_type['ChineseName'];
		}
		else {
			$adjust_type_name = $rs_adjust_type['EnglishName'];
		}
	}	
}

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("Arrangement");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($adjust_type_name, "adjust_item_list.php?TypeID=$TypeID");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$linterface->LAYOUT_START();


$form_action = "new_item_update.php";
include("adjust_item.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


