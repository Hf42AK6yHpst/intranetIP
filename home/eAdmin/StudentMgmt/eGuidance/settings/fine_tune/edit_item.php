<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-07-21 [Cameron]
 * 		- fix bug: should redirect to listview page if ItemID is empty
 * 
 * 	2017-03-16 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($ItemID)) {
	$itemID = (count($ItemID)==1) ? $ItemID[0] : '';
}
else {
	$itemID = $ItemID;
}

if (!$itemID) {
	header("location: index.php");
}

$linterface = new interface_html();

$rs_adjust_item = $libguidance->getAdjustItem($itemID);
if (count($rs_adjust_item) == 1) {
	$rs_adjust_item = current($rs_adjust_item);
	$TypeID = $rs_adjust_item['TypeID'];
	if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
		$adjust_type_name = $rs_adjust_item['TypeChineseName'];
	}
	else {
		$adjust_type_name = $rs_adjust_item['TypeEnglishName'];
	}
}	

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("Arrangement");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($adjust_type_name, "adjust_item_list.php?TypeID=$TypeID");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$linterface->LAYOUT_START();


$form_action = "edit_item_update.php";
include("adjust_item.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>