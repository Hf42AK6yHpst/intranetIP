<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-07-21 [Cameron]
 * 		- pass $TypeID for return
 * 
 * 	2017-03-17 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");


intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

if (count($ServiceID) > 0) {
	$ldb->Start_Trans();
	// delete support service
	$sql = "DELETE FROM INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE WHERE ServiceID IN ('".implode("','",(array)$ServiceID)."')";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'DeleteSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'DeleteUnsuccess';
}

header("location: support_index.php?TypeID=".$TypeID."&returnMsgKey=".$returnMsgKey);

intranet_closedb();

?>


