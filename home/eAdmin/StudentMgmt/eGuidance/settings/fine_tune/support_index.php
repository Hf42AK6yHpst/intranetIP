<?php
/*
 * Log
 *
 * 2018-05-23 [Cameron]
 * - add reorder service item button and column
 * - set default order by SeqNo
 *
 * 2017-11-20 [Cameron]
 * - fix cookie and keyword search
 *
 * 2017-07-21 [Cameron] - modify to let item under a type
 *
 * 2017-05-19 [Cameron] keywork search: apply standardizeFormPostValue first, then Get_Safe_Sql_Like_Query, not need to apply intranet_htmlspecialchars
 *
 * 2017-03-16 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

// set cookies
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_page_no",
    "pageNo"
);
$arrCookies[] = array(
    "ck_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_page_field",
    "field"
);

$arrCookies[] = array(
    "ck_keyword",
    "keyword"
);
if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! $permission['current_right']['SETTINGS']['FINETUNESETTING']) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (! $TypeID) {
    header("location: service_type_index.php");
}

if (count($TypeID) == 1) {
    $rs_service_type = $libguidance->getServiceType($TypeID);
    if (count($rs_service_type) == 1) {
        $rs_service_type = current($rs_service_type);
    }
}

// Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("Support");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['settings']['FineTune']['ServiceType'],
    "service_type_index.php"
);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 3 : $field;

if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
    $nameFieldAry = array(
        'ChineseName',
        'EnglishName'
    );
} else {
    $nameFieldAry = array(
        'EnglishName',
        'ChineseName'
    );
}

$cond = '';
// if ($Type) {
// $cond .= " AND Type='".$Type."'";
// }

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != '') {
    $cond .= " AND (ChineseName LIKE '%" . $libguidance->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($keyword)) . "%' OR EnglishName LIKE '%" . $libguidance->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($keyword)) . "%')";
}

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT
				" . $nameFieldAry[0] . ",
				" . $nameFieldAry[1] . ",
				IF(WithText=1,'" . $Lang['General']['Yes'] . "','" . $Lang['General']['No'] . "') AS WithText, SeqNo ";
if ($permission['admin'] || in_array('MGMT', (array) $permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
    $sql .= ",CONCAT('<input type=\'checkbox\' name=\'ServiceID[]\' id=\'ServiceID[]\' value=\'', `ServiceID`,'\'>') ";
    $extra_column = 2;
    $checkbox_col = "<th width='1'>" . $li->check("ServiceID[]") . "</th>\n";
    $toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new_support.php?TypeID=$TypeID')", $button_new, "", "", "", 1);
    $toolbar .= "&nbsp;" . $linterface->GET_LNK_GENERATE("javascript:checkGet(document.form1,'reorder_service_item.php?TypeID=$TypeID')", $Lang['eGuidance']['settings']['Reorder'], "", "", "", 1);
    if (! $junior_mck) {
        $toolbar = '<div class="Conntent_tool">' . $toolbar . '</div>';
    }
    $manage_record_bar = '<a href="javascript:checkEdit(document.form1,\'ServiceID[]\',\'edit_support.php\')" class="tool_edit">' . $button_edit . '</a>';
    $manage_record_bar .= '<a id="btnRemove" href="" class="tool_delete">' . $button_delete . '</a>';
} else {
    $extra_column = 1;
    $checkbox_col = "";
    $toolbar = "";
    $manage_record_bar = '';
}
$sql .= "FROM
				INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE
		WHERE 	TypeID='" . $TypeID . "'" . $cond;

$li->field_array = array(
    $nameFieldAry[0],
    $nameFieldAry[1],
    'WithText',
    'SeqNo'
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + $extra_column;
$li->title = $Lang['General']['Record'];
$li->column_array = array(
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='35%' >" . $li->column($pos ++, $Lang['eGuidance']['settings']['FineTune']['Support'][$nameFieldAry[0]]) . "</th>\n";
$li->column_list .= "<th width='35%' >" . $li->column($pos ++, $Lang['eGuidance']['settings']['FineTune']['Support'][$nameFieldAry[1]]) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['eGuidance']['settings']['FineTune']['WithText']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['eGuidance']['settings']['SeqNo']) . "</th>\n";
$li->column_list .= $checkbox_col;

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters

// # Support Service Type Filter
// foreach((array)$guidance_cfg['support_service_type'] as $k=>$v) {
// $supportTypeList[] = array($k,$v);
// }
// $supportTypeFilter = getSelectByArray($supportTypeList,"name='Type' onChange='this.form.submit();'",$Type,0,0,$Lang['eGuidance']['settings']['FineTune']['Support']['AllSupportType']);
// ############ end Filters

?>
<script>
$(document).ready(function(){
	
	$('#btnRemove').click(function(e) {
	    e.preventDefault();
	    if ($('#ServiceID\\\[\\\]:checked').length<=0) {
            alert(globalAlertMsg2);
	    }
        else {
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: '../../ajax/ajax.php?action=checkBeforeDeleteSupportService', 
	            data: $('#form1').serialize(),
	            async: false,            
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '1') {
		        			alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSupportService']?>');
		        		}
		        		else {
		        			if(confirm(globalAlertMsg3)){
			        			$('#form1').attr('action','remove_support.php');
			        			$('#form1').submit();
		        			}
		        		}
		        	}
		        },
				error: show_ajax_error
	        });
        }
	});
	
});

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}
</script>

<form name="form1" id="form1" method="post" action="support_index.php">
	<table id="main_table" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="95%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" width="100%" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['ServiceTypeChineseName']?>
									</td>
									<td class="tabletext"><?=$rs_service_type['ChineseName'] ? intranet_htmlspecialchars($rs_service_type['ChineseName']) : '-'?></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['ServiceTypeEnglishName']?>
									</td>
									<td class="tabletext"><?=$rs_service_type['EnglishName'] ? intranet_htmlspecialchars($rs_service_type['EnglishName']) : '-'?></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['ServiceScope']?>
									</td>
									<td class="tabletext"><?=$rs_service_type['Type'] == 'Study' ? $Lang['eGuidance']['settings']['FineTune']['SupportType']['Study']: $Lang['eGuidance']['settings']['FineTune']['SupportType']['Other']?></td>
								</tr>
							</table>

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="50%"><?=$toolbar ?></td>
									<td width="20%" align="center">&nbsp;</td>
									<td width="30%" style="float: right;">
										<div class="content_top_tool" style="float: right;">
										<?=$htmlAry['searchBox']?>     
										<br style="clear: both" />
										</div>
									</td>
								</tr>
							</table>

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td valign="bottom">
										<div class="table_filter"><?=$supportTypeFilter ?></div>
									</td>
									<td valign="bottom">
										<div class="common_table_tool"><?=$manage_record_bar ?></div>
									</td>
								</tr>
							</table>

							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td valign="bottom"><?=$li->display()?></td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" /> <input
		type="hidden" name="numPerPage" value="<?php echo $li->page_size;?>" />
	<input type="hidden" name="TypeID" value="<?=$TypeID?>" />
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>