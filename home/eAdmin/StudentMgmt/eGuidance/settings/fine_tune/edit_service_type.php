<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-07-21 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['FINETUNESETTING'])) {	
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$TypeID) {
	header("location: service_type_index.php");
}

$linterface = new interface_html();

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("Support");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['settings']['FineTune']['ServiceType'], "service_type_index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$linterface->LAYOUT_START();

if (is_array($TypeID)) {
	$typeID = $TypeID[0];
}
else {
	$typeID = $TypeID;
}

$rs_service_type = $libguidance->getServiceType($typeID);
if (count($rs_service_type)) {
	$rs_service_type = current($rs_service_type);
}

$form_action = "edit_service_type_update.php";
include("service_type.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


