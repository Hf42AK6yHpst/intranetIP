<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-04-12 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$settingID = $_POST['SettingID'];
if (is_array($settingID)) {
    $settingID = $settingID[0];
}

if (! $settingID) {
    header("location: sen_case_type_index.php");
}

$linterface = new interface_html();

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("CaseType");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['settings']['FineTune']['Tab']['CaseType'],
    "sen_case_type_index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$rs_sen_case_type = $libguidance->getSENCaseType($settingID);
$form_action = "edit_sen_case_type_update.php";

$linterface->LAYOUT_START();

include ("sen_case_type.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


