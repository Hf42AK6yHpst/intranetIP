<?php
/*
 *  2020-06-19 Cameron
 *      - add PrintSenCasePICinStudentReport
 *      
 *  2019-08-13 Cameron
 *      - add AllowClassTeacherViewSEN and AllowSubjectTeacherViewSEN [case #M158130]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libguidance = new libguidance();
$data = array();
$data['AuthenticationSetting'] = $AuthenticationSetting;
$data['AllowClassTeacherViewSEN'] = $AllowClassTeacherViewSEN;
$data['AllowSubjectTeacherViewSEN'] = $AllowSubjectTeacherViewSEN;
$data['PrintSenCasePICinStudentReport'] = $PrintSenCasePICinStudentReport;

$success = $libguidance->updateGeneralSettings($data);


if($success > 0)
{
	// added for Authenticate when first time access eGuidance System
	$libguidance->AuthenticationSetting = $AuthenticationSetting;
	$libguidance->AllowClassTeacherViewSEN= $AllowClassTeacherViewSEN;
	$libguidance->AllowSubjectTeacherViewSEN= $AllowSubjectTeacherViewSEN;
	$libguidance->PrintSenCasePICinStudentReport= $PrintSenCasePICinStudentReport;
}

//header("Location: index.php");

intranet_closedb();
header("Location: index.php?clearCoo=1");
?>
