<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-05-18 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-02-22 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['StartDate'] = $StartDate;
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS',$dataAry,array(),false);

$ldb->Start_Trans();
## step 1: add to advanced class
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
	$ClassID = $ldb->db_insert_id();
	## step 2: add advanced class student
	foreach((array)$StudentID as $id) {
		unset($dataAry);
		$dataAry['ClassID'] = $ClassID;
		$dataAry['StudentID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT',$dataAry,array(),false,false,false);	// no DateModified field
		$result[] = $ldb->db_db_query($sql);
	}

	## step 3: add advanced class teacher
	foreach((array)$TeacherID as $id) {
		unset($dataAry);
		$dataAry['ClassID'] = $ClassID;
		$dataAry['TeacherID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER',$dataAry,array(),false,false,false);	// no DateModified field
		$result[] = $ldb->db_db_query($sql);
	}
	
	## step 4: update attachment				
	if (count($FileID) > 0) {
		$fileID = implode("','",$FileID);
		$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ClassID."' WHERE FileID IN ('".$fileID."')";
		$result[] = $ldb->db_db_query($sql);
	}
	
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'AddSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'AddUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


