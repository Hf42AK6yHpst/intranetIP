<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-07-20 [Cameron]
 * 		- fix bug: should redirect to listview page if ClassID is empty
 * 
 * 	2017-02-22 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($ClassID)) {
	$classID = (count($ClassID)==1) ? $ClassID[0] : '';
}
else {
	$classID = $ClassID;
}

if (!$classID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();

$rs_class = $libguidance->getAdvancedClass($classID);

if (count($rs_class) == 1) {
	$rs_class = $rs_class[0];
	
	$rs_class['Student'] = $libguidance->getAdvancedClassStudent($classID);
	
	$rs_class['Teacher'] = $libguidance->getAdvancedClassTeacher($classID);
}
else {
	header("location: index.php");
}

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageAdvancedClass";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['AdvancedClass']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['AdvancedClass'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$linterface->LAYOUT_START();

	
$form_action = "edit_update.php";
include("class.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


