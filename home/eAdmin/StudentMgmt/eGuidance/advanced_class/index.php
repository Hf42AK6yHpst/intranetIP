<?
/*
 * 	Log
 *
 *	2017-11-20 [Cameron]
 *		- fix cookie and keyword search, set default sorting to descending by date
 *
 * 	2017-07-19 [Cameron] TeacherPIC sort in English name
 *  
 * 	2017-05-19 [Cameron] keywork search: apply standardizeFormPostValue first, then Get_Safe_Sql_Like_Query, not need to apply intranet_htmlspecialchars
 *    
 * 	2017-02-22 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['ADVANCEDCLASS']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageAdvancedClass";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['AdvancedClass']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order;		// default descending
$field = ($field == "") ? 0 : $field;

$student_name = getNameFieldWithClassNumberByLang("us.");
$teacher_name = getNameFieldByLang();

$cond = '';
$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if($keyword!="")
{
	$kw = $libguidance->Get_Safe_Sql_Like_Query($keyword);
	$cond .= " AND (s.Student LIKE '%$kw%' OR t.Teacher LIKE '%$kw%' OR c.StartDate LIKE '%$kw%')";
	unset($kw);
}

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT
				CONCAT('<a href=\"lesson.php?ClassID=',c.ClassID,'\">',c.StartDate,'</a>'),
				s.Student,
				t.Teacher,
				c.Remark ";
if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {
	$sql .= ",CONCAT('<input type=\'checkbox\' name=\'ClassID[]\' id=\'ClassID[]\' value=\'', c.`ClassID`,'\'>') ";
	$extra_column = 2;
	$checkbox_col = "<th width='1'>".$li->check("ClassID[]")."</th>\n";
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')",$button_new,"","","",0);
	$manage_record_bar  = '<a href="javascript:checkEdit(document.form1,\'ClassID[]\',\'edit.php\')" class="tool_edit">'.$button_edit.'</a>';
	$manage_record_bar .= '<a id="btnRemove" href="" class="tool_delete">'.$button_delete.'</a>';
}		
else {
	$extra_column = 1;
	$checkbox_col = "";
	$toolbar = "";
	$manage_record_bar  = '';
}
$sql .= "FROM 
				INTRANET_GUIDANCE_ADVANCED_CLASS c
		LEFT JOIN (
				SELECT 	ClassID, 
						GROUP_CONCAT(DISTINCT ".$student_name." ORDER BY us.ClassName, us.ClassNumber SEPARATOR ', ') AS Student
				FROM 
						INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT s
				INNER JOIN 
						INTRANET_USER us ON us.UserID=s.StudentID
				GROUP BY ClassID
			) AS s ON s.ClassID=c.ClassID
		LEFT JOIN (
				SELECT 	ClassID, 
						GROUP_CONCAT(DISTINCT ".$teacher_name." ORDER BY EnglishName SEPARATOR ', ') AS Teacher,
						GROUP_CONCAT(DISTINCT EnglishName ORDER BY EnglishName SEPARATOR ', ') AS SortTeacher
				FROM 
						INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER t
				INNER JOIN 
						INTRANET_USER ut ON ut.UserID=t.TeacherID
				GROUP BY ClassID
			) AS t ON t.ClassID=c.ClassID
		WHERE 1 ".$cond;

$li->field_array = array("StartDate", "Student","SortTeacher","Remark");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$text_col = ($junior_mck) ? 19 : 18;
$li->column_array = array(0,0,0,$text_col);	// Remark keep line feed
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['General']['StartDate'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['eGuidance']['StudentName'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['eGuidance']['TeacherName'])."</th>\n";
$li->column_list .= "<th width='35%' >".$li->column($pos++, $Lang['eGuidance']['Remark'])."</th>\n";
$li->column_list .= $checkbox_col;


$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

?>
<script>
$(document).ready(function(){
	$('#btnRemove').click(function(e) {
	    e.preventDefault();
	    if ($('#ClassID\\\[\\\]:checked').length<=0) {
            alert(globalAlertMsg2);
	    }
        else {
			if(confirm(globalAlertMsg3)){
    			$('#form1').attr('action','remove.php');
    			$('#form1').submit();
			}
        }
	});

});

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}

</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool"  style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear:both" />
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
				<div class="table_filter">
				</div> 
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
					<?=$manage_record_bar ?>
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?= $li->display() ?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>