<?
/*
 * 	modifying:
 * 
 * 	Log
 *
 *  2018-05-25 [Cameron]
 *      - show student name without class
 *      
 * 	2017-08-03 [Cameron]
 * 		- add lock icon to indicate confidential record, don't allow to view if User is not in ConfidentialViewerID list of the record
 * 
 *	2017-07-21 [Cameron]
 *		- add follow-up advice
 *		- fix bug: should redirect to listview page if ContactID is empty
 *
 * 	2017-03-20 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CONTACT']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$ContactID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();


$teacherInfo = '';
$studentInfo = '';
$contactID = $ContactID;
$rs_contact = $libguidance->getContact($contactID);

if (count($rs_contact) == 1) {
	$rs_contact = $rs_contact[0];
	$rs_contact['Teacher'] = $libguidance->getContactTeacher($contactID);
	$teacherInfo = $libguidance_ui->getUserNameList($rs_contact['Teacher']);

	$confidentialViewerInfo = '';
	if ($rs_contact['IsConfidential']) {
		$confidentialViewerID = $rs_contact['ConfidentialViewerID'];
		if (!in_array($_SESSION['UserID'],explode(',',$confidentialViewerID))) {
			header("location: index.php");
		}
		
		if ($confidentialViewerID) {
			$cond = ' AND UserID IN('.$confidentialViewerID.')';
			$confidentialViewerID = $libguidance->getTeacher('-1',$cond);
			$confidentialViewerInfo = $libguidance_ui->getUserNameList($confidentialViewerID);
		}
	}

	$rs_class_name = $libguidance->getClassByStudentID($rs_contact['StudentID']);
	if (!empty($rs_class_name)) {
		$studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_contact['StudentID']),'',true);
		$studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
	}
	else {
	    $studentInfo = $libguidance->getStudent($rs_contact['StudentID']);
	    $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
	}	
}
else {
	header("location: index.php");
}


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageContact";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Contact']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['Contact'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$linterface->LAYOUT_START();

?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>
</script>

<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['contact']['ContactCategory']?>
									</td>
								<td class="tabletext">
								<?
									$contact_category = $libguidance->getGuidanceSettingItem('Contact','Category');
									$x = '';
									$nrItem = count($contact_category);
									
									$i = 1;
									foreach((array)$contact_category as $k=>$v) {
										$x .= '<input type="radio" name="Category" id="'.$k.'" disabled value="'.$k.'" '.(($rs_contact['Category']==$k)?'checked':'').'>'.$v;
										$x .= ($i < $nrItem) ? '<br>':'';
										$i++;  
									}
									echo $x;
								?>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Date']?></td>
								<td class="tabletext"><?=((empty($rs_contact['ContactDate']) || ($rs_contact['ContactDate'] == '0000-00-00'))?'-':$rs_contact['ContactDate'])?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?></td>
								<td class="tabletext"><?=$studentInfo?> <?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?></td>
								<td class="tabletext"><?=$teacherInfo?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Confidential']?><img src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock_own.gif" style="display:<?=($rs_contact['IsConfidential']?'':'none')?>"></td>
								<td class="tabletext">
									<input type="radio" name="IsConfidential" id="IsConfidentialYes" value="1"<?=$rs_contact['IsConfidential']?' checked':''?> disabled><label for="IsConfidentialYes"><?=$Lang['General']['Yes']?></label>
									<input type="radio" name="IsConfidential" id="IsConfidentialNo" value="0"<?=$rs_contact['IsConfidential']?'':' checked'?> disabled><label for="IsConfidentialNo"><?=$Lang['General']['No']?></label>
									
									<div id="ConfidentialSection">
										<?=$confidentialViewerInfo?>
									</div>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['contact']['Details']?></td>
								<td class="tabletext"><?=$rs_contact['Details'] ? nl2br($rs_contact['Details']) : '-'?></td>
							</tr>
							<?=$libguidance_ui->getAttachmentLayout('Contact',$contactID,'view')?>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['FollowupAdvice']?></td>
								<td class="tabletext"><?=$rs_contact['FollowupAdvice'] ? nl2br($rs_contact['FollowupAdvice']) : '-'?></td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									
							<? 	if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['CONTACT'])) {
									echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit.php?ContactID=$contactID'").'&nbsp';
								}
								echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");							
							?>									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="StudentID" id="StudentID" value="<?=$rs_contact['StudentID']?>">

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>