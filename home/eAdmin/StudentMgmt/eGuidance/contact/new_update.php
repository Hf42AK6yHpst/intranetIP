<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 *	2017-08-03 [Cameron]
 *		- handle submitAndNotify (send 'update' notification to colleague)
 * 		- add IsConfidential and ConfidentialViewerID
 *
 *	2017-07-21 [Cameron]
 *		- add follow-up advice
 *
 * 	2017-05-18 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-02-20 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['CONTACT'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$dataAry['Category'] = $Category;
$dataAry['ContactDate'] = $ContactDate;
$dataAry['Details'] = standardizeFormPostValue($Details);
$dataAry['IsConfidential'] = $IsConfidential;
if ($IsConfidential && !in_array($_SESSION['UserID'], (array)$ConfidentialViewerID)) {
	$ConfidentialViewerID[] = $_SESSION['UserID'];				// User who add the record are granted permission as default 
}
$dataAry['ConfidentialViewerID'] = implode(",",(array)$ConfidentialViewerID);
$dataAry['FollowupAdvice'] = standardizeFormPostValue($FollowupAdvice);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_CONTACT',$dataAry,array(),false);

$ldb->Start_Trans();
## step 1: add to contact
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
	$ContactID = $ldb->db_insert_id();
	## step 2: add contact teacher
	foreach((array)$TeacherID as $id) {
		unset($dataAry);
		$dataAry['ContactID'] = $ContactID;
		$dataAry['TeacherID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_CONTACT_TEACHER',$dataAry,array(),false,false,false);	// no DateModified field
		$result[] = $ldb->db_db_query($sql);
	}

	## step 3: add attachment				
	if (count($FileID) > 0) {
		$fileID = implode("','",$FileID);
		$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ContactID."' WHERE FileID IN ('".$fileID."')";
		$result[] = $ldb->db_db_query($sql);
	}
	
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	
	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($ContactID, $recordType='ContactID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'AddAndNotifySuccess' : 'AddSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'AddSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'AddUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


