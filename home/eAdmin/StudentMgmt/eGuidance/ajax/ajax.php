<?
//Using: 
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 *  2020-05-13 [Cameron]
 *      - add case reorderSenCaseType and reorderSenCaseSubType [case #M151032]
 *      
 *  2020-03-31 [Cameron]
 *      - fix getStudentInfo(), should convert Gender to unicode for ej php 5.4 [case #D178792]
 *      
 *  2018-05-21 [Cameron]
 *      - add case reorderAdjustType, reorderAdjustItem, reorderServiceType, reorderServiceItem [case #M138231]
 *      
 *  2018-04-12 [Cameron]
 *      - add case checkBeforeAddSenCaseType, checkBeforeUpdateSenCaseType, checkBeforeDeleteSenCaseType, 
 *      checkBeforeDeleteSenCaseSubType, checkBeforeAddSenCaseSubType, checkBeforeUpdateSenCaseSubType [case #M130681]
 *      
 *  2018-04-11 [Cameron]
 *      - add onChange event to StudentID in case getStudentNameByClassForEditSEN and getStudentNameByClassForNewSEN
 *  
 *	2017-11-14 [Cameron]
 *		- add case getEJClassStudent
 *
 * 	2017-11-07 [Cameron]
 * 		- modify getFromStudentNameByYearIdYearClassId() by calling getStudentInfo2() to support ej
 *  
 * 	2017-07-28 [Cameron]
 * 		- add case getConfidentialViewerName
 * 
 * 	2017-07-21 [Cameron]
 * 		- add case checkBeforeAddServiceType, checkBeforeUpdateServiceType
 * 
 * 	2017-07-18 [Cameron]
 * 		- add case getTeacherName
 * 
 * 	2017-06-29 [Pun]
 * 		- added 'getAcademicYearTermSelect', 'getFromStudentNameByYearIdYearClassId', for report
 * 
 * 	2017-05-23 [Cameron]
 * 		- retrieve getStudentNameListWClassNumberByClassName from libguidance.php for IP
 * 
 * 	2017-05-18 [Cameron]
 * 		- must convert class_name / ClassName from utf-8 to big5 for ej in getStudentNameByClass, getStudentNameByClassForEditSEN,
 * 			 getStudentNameByClassForNewSEN, getStudentNameByClassWithFilter, getFromStudentNameByClass
 * 		- exclude left student(RecordStatus=3) in getStudentNameByClassWithFilter()
 * 
 * 	2017-05-16 [Cameron] fix php5.4 json must pass data as utf-8
 *  
 * 	2017-02-13 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash

$lclass = new libclass();
$libguidance_ui = new libguidance_ui();

switch($action) {
    
    case 'getAcademicYearTermSelect':
        $tags = stripslashes($_POST['tags']);
        $academicYearID = $_POST['AcademicYearID'];
        
        $x = getSelectSemester2($tags, $selected="", $ParQuoteValue=1, $AcademicYearID=$academicYearID);
        $json['success'] = true;
        break;

    case 'getFromStudentNameByYearIdYearClassId':
        $YearID = $_POST['YearID'];
        $YearClassID = $_POST['YearClassID'];
        $allStudent = $libguidance->getStudentInfo2('',$YearClassID,$YearID);
        
        $data = array();
        foreach($allStudent as $student){
            $data[] = array(
                $student['UserID'],
                "{$student['ClassName']} - {$student['ClassNumber']} {$student['StudentName']}"
            );
        }
        
        $x = getSelectByArray($data, "name='StudentID' id='StudentID' class='formtextbox'", $selected="", $all=0, $noFirst=0, $FirstTitle=$Lang['eGuidance']['AllStudent'], $ParQuoteValue=1);
        $json['success'] = true;
        break;
        
	case 'getFromStudentNameByClass':
		$ClassName = $_POST['ClassName'];
		$ClassName = ($junior_mck) ? convert2unicode($ClassName, 1, $direction=0) : $ClassName;		// convert utf-8 to big5 for ej
		$excludeUserIdAry = array($_POST['StudentID']);	// exclude self
		if (!empty($ClassName)) {
			if ($junior_mck) {
				$data = $lclass->getStudentNameListWClassNumberByClassName($ClassName,'','',$excludeUserIdAry);
			}
			else {
				$data = $libguidance->getStudentNameListWClassNumberByClassName($ClassName,'',$excludeUserIdAry);
			}
		}
		else {
			$data = array();
		}
		$x = getSelectByArray($data, "name='FromStudentID' id='FromStudentID'");
		$json['success'] = true;
		break;
		
	case 'getStudentNameByClass':
		$ClassName = $_POST['ClassName'];
		$ClassName = ($junior_mck) ? convert2unicode($ClassName, 1, $direction=0) : $ClassName;		// convert utf-8 to big5 for ej
		$onChange = $_POST['onChange'] ? " onChange='".$_POST['onChange']."'":"";
		if (!empty($ClassName)) {
			if ($junior_mck) {
				$data = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
			}
			else {
				$data = $libguidance->getStudentNameListWClassNumberByClassName($ClassName);
			}
		}
		else {
			$data = array();
		}
		$x = getSelectByArray($data, "name='StudentID' id='StudentID'".$onChange);		
		$json['success'] = true;
		break;

		
	case 'getStudentNameByClassForEditSEN':
		$ClassName = $_POST['ClassName'];
		$ClassName = ($junior_mck) ? convert2unicode($ClassName, 1, $direction=0) : $ClassName;		// convert utf-8 to big5 for ej
		$OrgStudentID = $_POST['OrgStudentID'];
		
		if (!empty($ClassName)) {
			$excludeUserIdAry = $libguidance->getSENCaseByClass($ClassName,$OrgStudentID);
			if ($junior_mck) {
				$data = $lclass->getStudentNameListWClassNumberByClassName($ClassName,'','',$excludeUserIdAry);
			}
			else {
				$data = $libguidance->getStudentNameListWClassNumberByClassName($ClassName,'',$excludeUserIdAry);
			}
		}
		else {
			$data = array();
		}
		$x = getSelectByArray($data, "name='StudentID' id='StudentID' onChange='changeStudent();'");
		$json['success'] = true;
		break;

	case 'getStudentNameByClassForNewSEN':
		$ClassName = $_POST['ClassName'];
		$ClassName = ($junior_mck) ? convert2unicode($ClassName, 1, $direction=0) : $ClassName;		// convert utf-8 to big5 for ej
		
		if (!empty($ClassName)) {
			$excludeUserIdAry = $libguidance->getSENCaseByClass($ClassName);
			if ($junior_mck) {
				$data = $lclass->getStudentNameListWClassNumberByClassName($ClassName,'','',$excludeUserIdAry);
			}
			else {
				$data = $libguidance->getStudentNameListWClassNumberByClassName($ClassName,'',$excludeUserIdAry);
			}
		}
		else {
			$data = array();
		}
		$x = getSelectByArray($data, "name='StudentID' id='StudentID' onChange='changeStudent();'");
		$json['success'] = true;
		break;

	case 'getStudentNameByClassForEditPersonal':
		$ClassName = $_POST['ClassName'];
		$ClassName = ($junior_mck) ? convert2unicode($ClassName, 1, $direction=0) : $ClassName;		// convert utf-8 to big5 for ej
		$OrgStudentID = $_POST['OrgStudentID'];
		
		if (!empty($ClassName)) {
			$excludeUserIdAry = $libguidance->getPersonalCaseByClass($ClassName,$OrgStudentID);
			if ($junior_mck) {
				$data = $lclass->getStudentNameListWClassNumberByClassName($ClassName,'','',$excludeUserIdAry);
			}
			else {
				$data = $libguidance->getStudentNameListWClassNumberByClassName($ClassName,'',$excludeUserIdAry);
			}
		}
		else {
			$data = array();
		}
		$x = getSelectByArray($data, "name='StudentID' id='StudentID'");
		$json['success'] = true;
		break;

	case 'getStudentNameByClassForNewPersonal':
		$ClassName = $_POST['ClassName'];
		$ClassName = ($junior_mck) ? convert2unicode($ClassName, 1, $direction=0) : $ClassName;		// convert utf-8 to big5 for ej
		
		if (!empty($ClassName)) {
			$excludeUserIdAry = $libguidance->getPersonalCaseByClass($ClassName);
			if ($junior_mck) {
				$data = $lclass->getStudentNameListWClassNumberByClassName($ClassName,'','',$excludeUserIdAry);
			}
			else {
				$data = $libguidance->getStudentNameListWClassNumberByClassName($ClassName,'',$excludeUserIdAry);
			}
		}
		else {
			$data = array();
		}
		$x = getSelectByArray($data, "name='StudentID' id='StudentID'");
		$json['success'] = true;
		break;
		
	case 'getStudentNameByClassWithFilter':
		$class_name = $_POST['ClassName'];
		$class_name = ($junior_mck) ? convert2unicode($class_name, 1, $direction=0) : $class_name;		// convert utf-8 to big5 for ej
		
		$excludeUserIdAry = $_POST['ExcludeStudentID'];
		$data = $libguidance->getStudentNameByClassWithFilter($class_name,$excludeUserIdAry);
		
		$x = '<select name=AvailableStudentID[] id=AvailableStudentID style="min-width:200px; height:156px;" multiple>';
		for($i=0,$iMax=count($data); $i<$iMax; $i++) {
			$studentID = $data[$i]['UserID'];
			$studentName = $data[$i]['StudentName'];
			$x .= '<option value="'.$studentID.'">'.$studentName.'</option>';
		}
		$x .= '</select>';
		
		$json['success'] = true;
		break;
		
		
	case 'checkBeforeUpdateAdvancedClass':
		$isStudentUsed = $libguidance->checkAdvancedClassStudentUsed($ClassID,$StudentID);
		$x = ($isStudentUsed) ? '1' : '0';
		
		$json['success'] = true;
		break;
		

	case 'getTherapyNewActivity':
		$orderBy = "ClassName,ClassNumber";	
		$student = $libguidance->getTherapyStudent($TherapyID,$orderBy);
		$student = BuildMultiKeyAssoc($student, 'UserID');
		$activityID = '';
		
		$_activity['ActivityID'] = '';
		$_activity['ActivityDate'] = $ActivityDate;
		$_activity['ActivityName'] = '';
		$_activity['Remark'] = '';
		
		$x = $libguidance_ui->getTherapyActivityTableRow($RowNr, $_activity, $student);
		$json['success'] = true;
		break;
		
	case 'checkBeforeUpdateTherapy':
		$isStudentUsed = $libguidance->checkTherapyStudentUsed($TherapyID,$StudentID);
//		$isActivityCreated = $libguidance->checkTherapyActivityCreated($TherapyID,$TeacherID);
		$x = ($isStudentUsed) ? '1' : '0';
		
		$json['success'] = true;
		break;
		
//	case 'checkBeforeDeleteTherapy':
//		$activityExist = false;
//		if (count($TherapyID) > 0) {
//			foreach((array)$TherapyID as $cid) {
//				$activityExist = $libguidance->isTherapyHasActivity($cid);
//				if ($activityExist) {
//					break;
//				}
//			}
//		}
//	
//		$x = $activityExist ? '1' : '0';
//		$json['success'] = true;
//		break;				

	case 'getSENServiceNewActivity':
		$orderBy = "ClassName,ClassNumber";	
		$student = $libguidance->getSENServiceStudent($ServiceID,$orderBy);
		$student = BuildMultiKeyAssoc($student, 'UserID');
		$activityID = '';
		
		$_activity['ActivityID'] = '';
		$_activity['ActivityDate'] = $ActivityDate;
		
		$x = $libguidance_ui->getSENServiceActivityTableRow($RowNr, $_activity, $student);
		$json['success'] = true;
		break;
		
	case 'checkBeforeUpdateSENService':
		$isStudentUsed = $libguidance->checkSENServiceStudentUsed($ServiceID,$StudentID);
//		$isActivityCreated = $libguidance->checkSENServiceActivityCreated($ServiceID,$TeacherID);
		$x = ($isStudentUsed) ? '1' : '0';
		
		$json['success'] = true;
		break;
		
//	case 'checkBeforeDeleteSENService':
//		$activityExist = false;
//		if (count($ServiceID) > 0) {
//			foreach((array)$ServiceID as $cid) {
//				$activityExist = $libguidance->isSENServiceHasActivity($cid);
//				if ($activityExist) {
//					break;
//				}
//			}
//		}
//	
//		$x = $activityExist ? '1' : '0';
//		$json['success'] = true;
//		break;				

	case 'checkBeforeUpdateSENCase':
		$rs = $libguidance->getSENCase($StudentID);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;
		
	case 'checkBeforeAddGroup':
		$rs = $libguidance->checkDuplicateAccessRightGroup('',$GroupTitle);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;
		
	case 'checkBeforeUpdateGroup':
		$rs = $libguidance->checkDuplicateAccessRightGroup($GroupID,$GroupTitle);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeAddAdjustType':
		$rs = $libguidance->checkDuplicateAdjustType('',$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeUpdateAdjustType':
		$rs = $libguidance->checkDuplicateAdjustType($TypeID,$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeAddAdjustItem':
		$rs = $libguidance->checkDuplicateAdjustItem('',$TypeID,$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeUpdateAdjustItem':
		$rs = $libguidance->checkDuplicateAdjustItem($ItemID,$TypeID,$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeAddServiceType':
		$rs = $libguidance->checkDuplicateServiceType('',$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeUpdateServiceType':
		$rs = $libguidance->checkDuplicateServiceType($TypeID,$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeAddSupportService':
		$rs = $libguidance->checkDuplicateSupportService('',$TypeID,$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;

	case 'checkBeforeUpdateSupportService':
		$rs = $libguidance->checkDuplicateSupportService($ServiceID,$TypeID,$_POST['ChineseName'],$_POST['EnglishName']);
		$x = (count($rs)>0) ? '1' : '0';
		$json['success'] = true;
		break;
	
	case 'checkBeforeDeleteAdjustType':
		$isUsed = false;
		for($i=0,$iMax=count($TypeID);$i<$iMax;$i++) {
			$isUsed = $libguidance->isSENCaseUseAdjustment($TypeID[$i]);
			if ($isUsed) {
				break;
			}
		}
		$x = $isUsed ? '1' : '0';
		$json['success'] = true;
		break;				

	case 'checkBeforeDeleteAdjustItem':
		$isUsed = false;
		for($i=0,$iMax=count($ItemID);$i<$iMax;$i++) {
			$isUsed = $libguidance->isSENCaseUseAdjustment('',$ItemID[$i]);
			if ($isUsed) {
				break;
			}
		}
		$x = $isUsed ? '1' : '0';
		$json['success'] = true;
		break;				

	case 'checkBeforeDeleteServiceType':
		$isUsed = false;
		for($i=0,$iMax=count($TypeID);$i<$iMax;$i++) {
			$isUsed = $libguidance->isSENCaseUseSupportService($TypeID[$i]);
			if ($isUsed) {
				break;
			}
		}
		$x = $isUsed ? '1' : '0';
		$json['success'] = true;
		break;				

	case 'checkBeforeDeleteSupportService':
		$isUsed = false;
		for($i=0,$iMax=count($ServiceID);$i<$iMax;$i++) {
			$isUsed = $libguidance->isSENCaseUseSupportService('',$ServiceID[$i]);
			if ($isUsed) {
				break;
			}
		}
		$x = $isUsed ? '1' : '0';
		$json['success'] = true;
		break;				
		
	case 'getTeacherName':
		$teacherType = $_POST['TeacherType'];
		$excludeTeacherIDAry = $_POST['ExcludeTeacherID'];
		if (count($excludeTeacherIDAry)) {
			$cond = "AND UserID NOT IN ('".implode("','",$excludeTeacherIDAry)."')";
		}
		else {
			$cond = "";
		}
		$x = '<select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
			if ($teacherType == 1) {
				$teacherType = 1;
			}
			else {
				$teacherType = 0;
			}
			$data = $libguidance->getTeacher($teacherType,$cond);
			for($i=0,$iMax=count($data); $i<$iMax; $i++) {
				$staffID = $data[$i]['UserID'];
				$staffName = $data[$i]['Name'];
				$x .= '<option value="'.$staffID.'">'.$staffName.'</option>';
			}
		$x .= '</select>';
		
		$json['success'] = true;
		break;

	case 'getConfidentialViewerName':
		$groupCategory = $_POST['ConfidentialGroupCategory'];
		$excludeConfidentialViewerIDAry = $_POST['ExcludeConfidentialViewerID'];
		if (count($excludeConfidentialViewerIDAry)) {
			$cond = "AND UserID NOT IN ('".implode("','",$excludeConfidentialViewerIDAry)."')";
		}
		else {
			$cond = "";
		}
		$x = '<select name=AvailableConfidentialViewerID[] id=AvailableConfidentialViewerID style="min-width:200px; height:156px;" multiple>';
			if ($groupCategory == 'TeachingStaff') {
				$groupCategory = 1;
			}
			else {
				$groupCategory = 0;
			}
			$data = $libguidance->getTeacher($groupCategory,$cond);
			for($i=0,$iMax=count($data); $i<$iMax; $i++) {
				$staffID = $data[$i]['UserID'];
				$staffName = $data[$i]['Name'];
				$x .= '<option value="'.$staffID.'">'.$staffName.'</option>';
			}
		$x .= '</select>';
		
		$json['success'] = true;
		break;

	case 'getConfidentialGroup':
		$groupCategory = $_POST['ConfidentialGroupCategory'];
		$isLoading = $_POST['isLoading'];
		$group = $libguidance->getGroupList($groupCategory);
		$x = getSelectByAssoArray($group, 'name="ConfidentialGroup" id="ConfidentialGroup" onChange="changeGroup(\''.$isLoading.'\');"');
		$json['success'] = true;
		break;

	case 'getConfidentialGroupMember':
		$confidentialGroup = $_POST['ConfidentialGroup'];
		$excludeConfidentialViewerIDAry = $_POST['ExcludeConfidentialViewerID'];
		if (count($excludeConfidentialViewerIDAry)) {
			$cond = "AND u.UserID NOT IN ('".implode("','",$excludeConfidentialViewerIDAry)."')";
		}
		else {
			$cond = "";
		}
		$x = '<select name=AvailableConfidentialViewerID[] id=AvailableConfidentialViewerID style="min-width:200px; height:156px;" multiple>';
			$data = $libguidance->getGroupMember($confidentialGroup,$cond);
			
			for($i=0,$iMax=count($data); $i<$iMax; $i++) {
				$staffID = $data[$i]['UserID'];
				$staffName = $data[$i]['Name'];
				$x .= '<option value="'.$staffID.'">'.$staffName.'</option>';
			}
		$x .= '</select>';
		
		$json['success'] = true;
		break;

	case 'getNotifier':
		$groupCategory = $_POST['NotifierGroupCategory'];
		$excludeNotifierIDAry = $_POST['ExcludeNotifierID'];
		
		if (count($excludeNotifierIDAry)) {
			$cond = "AND UserID NOT IN ('".implode("','",$excludeNotifierIDAry)."')";
		}
		else {
			$cond = "";
		}
		$x = '<select name=AvailableNotifierID[] id=AvailableNotifierID style="min-width:200px; height:156px;" multiple>';
			if ($groupCategory == 'TeachingStaff') {
				$groupCategory = 1;
			}
			else {
				$groupCategory = 0;
			}
			$data = $libguidance->getTeacher($groupCategory,$cond);
			
			for($i=0,$iMax=count($data); $i<$iMax; $i++) {
				$staffID = $data[$i]['UserID'];
				$staffName = $data[$i]['Name'];
				$x .= '<option value="'.$staffID.'">'.$staffName.'</option>';
			}
		$x .= '</select>';
		
		$json['success'] = true;
		break;

	case 'getNotifierGroup':
		$groupCategory = $_POST['NotifierGroupCategory'];
		$isLoading = $_POST['isLoading'];
		$group = $libguidance->getGroupList($groupCategory);
		$x = getSelectByAssoArray($group, 'name="NotifierGroup" id="NotifierGroup" onChange="changeNotifierGroup(\''.$isLoading.'\');"');
		$json['success'] = true;
		break;

	case 'getNotifierGroupMember':
		$notifierGroup = $_POST['NotifierGroup'];
		$excludeNotifierIDAry = $_POST['ExcludeNotifierID'];
		if (count($excludeNotifierIDAry)) {
			$cond = "AND u.UserID NOT IN ('".implode("','",$excludeNotifierIDAry)."')";
		}
		else {
			$cond = "";
		}
		$x = '<select name=AvailableNotifierID[] id=AvailableNotifierID style="min-width:200px; height:156px;" multiple>';
			$data = $libguidance->getGroupMember($notifierGroup,$cond);
			
			for($i=0,$iMax=count($data); $i<$iMax; $i++) {
				$staffID = $data[$i]['UserID'];
				$staffName = $data[$i]['Name'];
				$x .= '<option value="'.$staffID.'">'.$staffName.'</option>';
			}
		$x .= '</select>';
		
		$json['success'] = true;
		break;

	case 'getClassStudent':
		$yearClassID = $_POST['YearClassID'];
		$meetingID = $_POST['MeetingID'];
		$x = $libguidance_ui->getClassTeacherCommentTable($meetingID, $yearClassID);
		$json['success'] = true;
		break;		

	case 'getEJClassStudent':
		$yearName = $_POST['YearName'];
		$className = $_POST['ClassName'];
		$meetingID = $_POST['MeetingID'];
		$x = $libguidance_ui->getClassTeacherCommentTable($meetingID, $yearClassID='', false, $yearName, $className);
		$json['success'] = true;
		break;		

	case 'getAcademicYearByClassName':
		$x = $libguidance_ui->getAcademicYearByClassName($_POST['ClassName']);
		$json['success'] = true;
		break;		

	case 'getStudentInfo':
	    $studentID = $_POST['StudentID'];
	    $studentInfo = $libguidance->getStudentInfo($studentID);
	    
        if (count($studentInfo)) {
            $json['STRN'] = $studentInfo['STRN'] ? $studentInfo['STRN'] : '-';
            $gender = $studentInfo['Gender'];
            if ($gender == 'M') {
                $genderStr = $Lang['eGuidance']['personal']['GenderMale'];
            }
            elseif ($gender == 'F') {
                $genderStr = $Lang['eGuidance']['personal']['GenderFemale'];
            }
            else {
                $genderStr = '-';
            }
            if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
                $json['Gender'] = convert2unicode($genderStr,true,1);
                unset($genderStr);
            }
            else {
                $json['Gender'] = $genderStr;
            }
            $json['DateOfBirth'] = ($studentInfo['DateOfBirth'] == '0000-00-00') ? '-' : $studentInfo['DateOfBirth'];
            $json['success'] = true;
        }
	    break;
	
	case 'checkBeforeAddSenCaseType':
	    $_POST['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']);
	    $_POST['ChineseName'] = ($junior_mck) ? convert2unicode( $_POST['ChineseName'], 1, $direction=0) :  $_POST['ChineseName'];		// convert utf-8 to big5 for ej
	    $_POST['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
	    $_POST['Code'] = standardizeFormPostValue($_POST['Code']);
	    
	    $rs = $libguidance->checkDuplicateSenCaseType('',$_POST['ChineseName'],$_POST['EnglishName'],$_POST['Code']);
	    $x = (count($rs)>0) ? '1' : '0';
	    $json['success'] = true;
	    break;
	
	case 'checkBeforeUpdateSenCaseType':
	    $_POST['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']);
	    $_POST['ChineseName'] = ($junior_mck) ? convert2unicode( $_POST['ChineseName'], 1, $direction=0) :  $_POST['ChineseName'];		// convert utf-8 to big5 for ej
	    $_POST['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
	    $_POST['Code'] = standardizeFormPostValue($_POST['Code']);
	    
	    $rs = $libguidance->checkDuplicateSenCaseType($_POST['SettingID'],$_POST['ChineseName'],$_POST['EnglishName'],$_POST['Code']);
	    $x = (count($rs)>0) ? '1' : '0';
	    $json['success'] = true;
	    break;
	    
	case 'checkBeforeDeleteSenCaseType':
	    $isUsed = false;
	    $settingIDAry = $_POST['SettingID'];
	    for($i=0,$iMax=count($settingIDAry);$i<$iMax;$i++) {
	        $isUsed = $libguidance->isSENCaseUseThisCaseType($settingIDAry[$i]);
	        if ($isUsed) {
	            break;
	        }
	    }
	    $x = $isUsed ? '1' : '0';
	    $json['success'] = true;
	    break;

	case 'checkBeforeDeleteSenCaseSubType':
	    $isUsed = false;
	    $subTypeIDAry = $_POST['SubTypeID'];
	    for($i=0,$iMax=count($subTypeIDAry);$i<$iMax;$i++) {
	        $isUsed = $libguidance->isSENCaseUseThisCaseSubType($subTypeIDAry[$i]);
	        if ($isUsed) {
	            break;
	        }
	    }
	    $x = $isUsed ? '1' : '0';
	    $json['success'] = true;
	    break;
	
	case 'checkBeforeAddSenCaseSubType':
	    $para = array();
	    $para['SettingID'] = $_POST['SettingID'];  
	    $para['SubTypeID'] = '';
	    $_POST['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']);
	    $para['ChineseName'] = ($junior_mck) ? convert2unicode( $_POST['ChineseName'], 1, $direction=0) :  $_POST['ChineseName'];		// convert utf-8 to big5 for ej
	    $para['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
	    $para['Code'] = standardizeFormPostValue($_POST['Code']);
	    $rs = $libguidance->checkDuplicateSenCaseSubType($para);
	    $x = (count($rs)>0) ? '1' : '0';
	    $json['success'] = true;
	    break;
	    
	case 'checkBeforeUpdateSenCaseSubType':
	    $para = array();
	    $para['SettingID'] = $_POST['SettingID'];
	    $para['SubTypeID'] = $_POST['SubTypeID'];
	    $_POST['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']);
	    $para['ChineseName'] = ($junior_mck) ? convert2unicode( $_POST['ChineseName'], 1, $direction=0) :  $_POST['ChineseName'];		// convert utf-8 to big5 for ej
	    $para['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
	    $para['Code'] = standardizeFormPostValue($_POST['Code']);
	    $rs = $libguidance->checkDuplicateSenCaseSubType($para);
	    $x = (count($rs)>0) ? '1' : '0';
	    $json['success'] = true;
	    break;
	    
	case 'reorderAdjustType':
	    $displayOrderString = $_POST['DisplayOrderString'];
	    $newOrderAry = $libguidance->getRecordIDAryByOrderString($displayOrderString, ",", "AdjustTypeRow_");
	    
	    $libguidance->Start_Trans();
	    $result = $libguidance->updateAdjustTypeDisplayOrder($newOrderAry);
	    if (in_array(false,$result))
	    {
	        $libguidance->RollBack_Trans();
	    }
	    else
	    {
	        $libguidance->Commit_Trans();
	        $json['success'] = true;
	    }
	    break;
	    
	case 'reorderAdjustItem':
	    $displayOrderString = $_POST['DisplayOrderString'];
	    $newOrderAry = $libguidance->getRecordIDAryByOrderString($displayOrderString, ",", "AdjustItemRow_");
	    
	    $libguidance->Start_Trans();
	    $result = $libguidance->updateAdjustItemDisplayOrder($newOrderAry);
	    if (in_array(false,$result))
	    {
	        $libguidance->RollBack_Trans();
	    }
	    else
	    {
	        $libguidance->Commit_Trans();
	        $json['success'] = true;
	    }
	    break;
	    
	case 'reorderServiceType':
	    $displayOrderString = $_POST['DisplayOrderString'];
	    $newOrderAry = $libguidance->getRecordIDAryByOrderString($displayOrderString, ",", "ServiceTypeRow_");
	    
	    $libguidance->Start_Trans();
	    $result = $libguidance->updateServiceTypeDisplayOrder($newOrderAry);
	    if (in_array(false,$result))
	    {
	        $libguidance->RollBack_Trans();
	    }
	    else
	    {
	        $libguidance->Commit_Trans();
	        $json['success'] = true;
	    }
	    break;
	    
	case 'reorderServiceItem':
	    $displayOrderString = $_POST['DisplayOrderString'];
	    $newOrderAry = $libguidance->getRecordIDAryByOrderString($displayOrderString, ",", "ServiceItemRow_");
	    
	    $libguidance->Start_Trans();
	    $result = $libguidance->updateServiceItemDisplayOrder($newOrderAry);
	    if (in_array(false,$result))
	    {
	        $libguidance->RollBack_Trans();
	    }
	    else
	    {
	        $libguidance->Commit_Trans();
	        $json['success'] = true;
	    }
	    break;
	    
	case 'reorderSenCaseType':
	    $displayOrderString = $_POST['DisplayOrderString'];
	    $newOrderAry = $libguidance->getRecordIDAryByOrderString($displayOrderString, ",", "SenCaseTypeRow_");
	    
	    $libguidance->Start_Trans();
	    $result = $libguidance->updateSENTypeDisplayOrder($newOrderAry);
	    if (in_array(false,$result))
	    {
	        $libguidance->RollBack_Trans();
	    }
	    else
	    {
	        $libguidance->Commit_Trans();
	        $json['success'] = true;
	    }
	    
	    break;

	case 'reorderSenCaseSubType':
	    $displayOrderString = $_POST['DisplayOrderString'];
	    $newOrderAry = $libguidance->getRecordIDAryByOrderString($displayOrderString, ",", "SenCaseSubTypeRow_");
	    
	    $libguidance->Start_Trans();
	    $result = $libguidance->updateSENTypeDisplayOrder($newOrderAry);       // SENType and SENSubType update the same fields, so it can call the same function 
	    if (in_array(false,$result))
	    {
	        $libguidance->RollBack_Trans();
	    }
	    else
	    {
	        $libguidance->Commit_Trans();
	        $json['success'] = true;
	    }
	    
	    break;
	    
//	case 'notifyUpdate':
//		$recordID = $_POST['RecordID'];
//		$recordType = $_POST['RecordType'];
//		$notifierID = $_POST['NotifierID'];
//		$json['success'] = $libguidance->sendNotifyToColleague($recordID, $recordType, $notifierID);
//		break;
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
}

$json['html'] = $x;
echo $ljson->encode($json);


intranet_closedb();
?>