<?
/*
 * modifying:
 *
 * note: person who is admin or who has any eGuidance MGMT right can access student personal info
 *
 * Log
 *
 * 2019-08-13 [Cameron]
 * - allow classTeacher and subjectTeacher to view SEN records of the related students in their corresponding class / subject class [case #M158130] 
 *      
 * 2018-02-28 [Cameron]
 * - no need to call phpversion_compare, otherwise need to convert $x to utf-8 at the end
 *
 * 2017-08-17 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! $permission['current_right']['MGMT'] && !$permission['isClassTeacher'] && !$permission['isSubjectTeacher']) {
    if ($junior_mck) {
        $characterset = 'big5'; // no need to call phpversion_compare, otherwise need to convert $x to utf-8 at the end
    } else {
        $characterset = 'utf-8';
    }
    header('Content-Type: text/html; charset=' . $characterset);
    
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if ($junior_mck) {
    $characterset = 'big5'; // no need to call phpversion_compare, otherwise need to convert $x to utf-8 at the end
} else {
    $characterset = 'utf-8';
}
header('Content-Type: text/html; charset=' . $characterset);

$fromThickbox = true;

ob_start();
include ($PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eGuidance/personal/view.php");
$x = ob_get_contents();
ob_end_clean();

echo $x;

intranet_closedb();
?>