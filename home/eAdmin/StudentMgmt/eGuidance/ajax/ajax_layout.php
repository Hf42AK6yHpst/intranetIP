<?
/*
 * 	Log 
 * 
 * 	Description: output html layout, no json format
 * 
 * 	2017-07-24 [Cameron]
 * 		- modify AddSupportItem to support category in service
 * 
 * 	2017-06-29 [Cameron]
 * 		- remove LessonDate parameter for getAdvancedClassLessonEdit()
 * 		- remove ActivityDate parameter for getTherapyActivityEdit()
 * 		- remove ActivityDate parameter for getSENServiceActivityEdit()
 * 
 * 	2017-05-17 [Cameron] fix php5.4 json must pass data as utf-8
 *   
 * 	2017-03-09 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}
header('Content-Type: text/html; charset='.$characterset);

$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];

$lclass = new libclass();
$libguidance_ui = new libguidance_ui();

switch($action) {

	case 'NewLesson':
		$orderBy = "ClassName,ClassNumber";	
		$student = $libguidance->getAdvancedClassStudent($ClassID,$orderBy);
		$student = BuildMultiKeyAssoc($student, 'UserID');
		$lessonID = '';
		$x = $libguidance_ui->getAdvancedClassLessonEdit($ClassID, $lessonID, $student, 'add_advanced_class_lesson');
		break;

	case 'EditLesson':
		$lesson = $libguidance->getAdvancedClassLesson('',$LessonID);
		if (count($lesson)) {
			$classID = $lesson[0]['ClassID'];
			$orderBy = "ClassName,ClassNumber";	
			$student = $libguidance->getAdvancedClassStudent($classID,$orderBy);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getAdvancedClassLessonEdit($classID, $LessonID, $student, 'update_advanced_class_lesson');
		}	
		break;
		
	case 'NewTherapyActivity':
		$orderBy = "ClassName,ClassNumber";	
		$student = $libguidance->getTherapyStudent($TherapyID,$orderBy);
		$student = BuildMultiKeyAssoc($student, 'UserID');
		$activityID = '';
		$x = $libguidance_ui->getTherapyActivityEdit($TherapyID, $activityID, $student, 'add_therapy_activity');
		break;

	case 'EditTherapyActivity':
		$activity = $libguidance->getTherapyActivity('',$ActivityID);
		if (count($activity)) {
			$therapyID = $activity[0]['TherapyID'];
			$orderBy = "ClassName,ClassNumber";	
			$student = $libguidance->getTherapyStudent($therapyID,$orderBy);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getTherapyActivityEdit($therapyID, $ActivityID, $student, 'update_therapy_activity');
		}	
		break;

	case 'NewSENServiceActivity':
		$orderBy = "ClassName,ClassNumber";	
		$student = $libguidance->getSENServiceStudent($ServiceID,$orderBy);
		$student = BuildMultiKeyAssoc($student, 'UserID');
		$activityID = '';
		$x = $libguidance_ui->getSENServiceActivityEdit($ServiceID, $activityID, $student, 'add_sen_service_activity');
		break;

	case 'EditSENServiceActivity':
		$activity = $libguidance->getSENServiceActivity('',$ActivityID);
		if (count($activity)) {
			$serviceID = $activity[0]['ServiceID'];
			$orderBy = "ClassName,ClassNumber";	
			$student = $libguidance->getSENServiceStudent($serviceID,$orderBy);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getSENServiceActivityEdit($serviceID, $ActivityID, $student, 'update_sen_service_activity');
		}	
		break;

	case 'AddAdjustmentItem':
		$x = $libguidance_ui->getNewAdjustmentItem($TypeID);
		break;

	case 'AddSupportItem':
		$x = $libguidance_ui->getNewSupportItem($TypeID);
		break;
	
	case 'NotifyColleagueUpdate':
		$recordID = $_GET['RecordID'];
		$recordType = $_GET['RecordType'];		// e.g. GuidanceID
		$x = $libguidance_ui->getNotifyColleagueSelection($recordID,$recordType);
		break;

	case 'NotifyColleagueReferral':
		$recordID = $_GET['RecordID'];
		$recordType = $_GET['RecordType'];		// e.g. GuidanceID
		$x = $libguidance_ui->getNotifyColleagueSelection($recordID,$recordType,'notifyReferral');
		break;

	case 'GetHistoryNotification':
		$recordID = $_GET['RecordID'];
		$recordType = $_GET['RecordType'];		// e.g. GuidanceID
		$x = $libguidance_ui->getHistoryPushMessage($recordID,$recordType);
		break;

}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
}

echo $x;

intranet_closedb();
?>