<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-25 [Cameron]
 * - retrieve student info and disable student selection for edit [case #M138226]
 *
 * 2017-07-28 [Cameron]
 * - add IsConfidential and ConfidentialViewerID
 * - don't allow to edit if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-21 [Cameron]
 * - add FollowupAdvice field
 *
 * 2017-07-20 [Cameron]
 * - fix bug: should redirect to listview page if GuidanceID is empty
 *
 * 2017-02-25 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['GUIDANCE'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (is_array($GuidanceID)) {
    $guidanceID = (count($GuidanceID) == 1) ? $GuidanceID[0] : '';
} else {
    $guidanceID = $GuidanceID;
}

if (! $guidanceID) {
    header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();

$classSelection = '';
$studentSelection = '';
$showOther = false;
$showFollowupAdviceOther = false;
$caseTypeOther = '';
$followupAdviceOther = '';
$caseTypeAry = array();
$followupAdviceAry = array();

$rs_guidance = $libguidance->getGuidance($guidanceID);

if (count($rs_guidance) == 1) {
    $rs_guidance = $rs_guidance[0];
    $caseTypeAry = explode('^~', $rs_guidance['CaseType']);
    foreach ((array) $caseTypeAry as $k => $v) {
        if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
            $caseTypeAry[$k] = 'Other';
            $caseTypeOther = substr($v, 7);
            $showOther = true;
        }
    }
    
    $followupAdviceAry = explode('^~', $rs_guidance['FollowupAdvice']);
    foreach ((array) $followupAdviceAry as $k => $v) {
        if (strlen($v) > 6 && substr($v, 0, 7) == "Other^:") {
            $followupAdviceAry[$k] = 'Other';
            $followupAdviceOther = substr($v, 7);
            $showFollowupAdviceOther = true;
        }
    }
    
    $rs_guidance['Teacher'] = $libguidance->getGuidanceTeacher($guidanceID);
    
    if ($rs_guidance['IsConfidential']) {
        $confidentialViewerID = $rs_guidance['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $rs_guidance['ConfidentialViewerID'] = $libguidance->getTeacher('-1', $cond);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($rs_guidance['StudentID']);
    if (! empty($rs_class_name)) {
        // $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ",$rs_class_name);
        // $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name);
        // $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $rs_guidance['StudentID']);
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_guidance['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    } else {
        $studentInfo = $libguidance->getStudent($rs_guidance['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
} else {
    header("location: index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageGuidance";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array(
    $Lang['eGuidance']['menu']['Management']['Guidance']
);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['menu']['Management']['Guidance'],
    "index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include ("guidance.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


