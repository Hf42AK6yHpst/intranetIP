<?
/*
 * 	Log
 *
 * 	2017-10-17 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('VIEW', (array)$permission['current_right']['REPORTS']['STUDENTPROBLEMREPORT'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageStudentProblemReport";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Reports']['StudentProblemReport'], "", true);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);



$teachingStaffAry = $libguidance->getTeacher($teacherType='1');
$teachingStaffAry = build_assoc_array($teachingStaffAry);
$nonTeachingStaffAry = $libguidance->getTeacher($teacherType='0');
$nonTeachingStaffAry = build_assoc_array($nonTeachingStaffAry);
$teacherTypeAry = array (	$Lang['Identity']['TeachingStaff'] => $teachingStaffAry,
							$Lang['Identity']['NonTeachingStaff'] => $nonTeachingStaffAry
						);
						
$rs_personal = array();

############# start Filters


############# end Filters

?>

<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>

<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
	
	$(".integer").each(function(){

		if (($.trim($(this).val()) != '') && ((isNaN($(this).val())) || ($(this).val() < 0) || ($(this).val() > 100) || ($(this).val().indexOf('.') != -1))) {
	
			error++;
			if (focusField == '') $(this).focus();
			
			id = $(this).attr('id');
			
			$('#Err'+id).addClass('error_msg_show').removeClass('error_msg_hide');

		}
	});

	$(".number").each(function(){

		if (($.trim($(this).val()) != '') && ((isNaN($(this).val())) || ($(this).val() < 0) || ($(this).val() > 100))) {
	
			error++;
			if (focusField == '') $(this).focus();
			
			id = $(this).attr('id');
			
			$('#Err'+id).addClass('error_msg_show').removeClass('error_msg_hide');

		}
	});
	
	if (($("input:radio[name='ReferralFromType']:checked").length == 0) && ($("input:radio[name='RadioPlaceOfBirth']:checked").length == 0) && ($("#YearInHongKong").val()=='')
		&& ($("input:radio[name='ParentStatus']:checked").length == 0) && ($("#TotalNbrBrotherSister").val()=='') && ($('input[name="Prob_Health\\[\\]"]:checked').length <=0)
		&& ($('input[name="Prob_Study\\[\\]"]:checked').length <=0) && ($('input[name="Prob_Peer\\[\\]"]:checked').length <=0) && ($('input[name="Prob_Grow\\[\\]"]:checked').length <=0)
		&& ($('input[name="Prob_Emotion\\[\\]"]:checked').length <=0) && ($('input[name="Prob_Sex\\[\\]"]:checked').length <=0) && ($('input[name="Prob_Behavior\\[\\]"]:checked').length <=0)
		&& ($('input[name="Prob_Family\\[\\]"]:checked').length <=0) && ($('input[name="Prob_Other\\[\\]"]:checked').length <=0) 
		&& ($("input:radio[name='IsReferralToSocialWorker']:checked").length == 0)
		) {
		error++;
		window.alert("<?=$Lang['eGuidance']['student_problem']['Warning']['PleaseSelectAtLeastOne']?>");
	}
	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
	
}


$(document).ready(function(){
	
	$(':input[name="ReferralFromType"]').change(function() {

		var from = $(this).attr('id');
		hideReferralFrom();
		
		$('#'+from+'_Span').css("display","");
		
	});
	
	$('#RefAcaReason_AcaOther').click(function(){
		if ($('#RefAcaReason_AcaOther').attr('checked')) {
			$('#AcaOther').css("display","");
		}
		else {
			$('#AcaOther').css("display","none");
		}
	});

	$('#RefDisReason_DisOther').click(function(){
		if ($('#RefDisReason_DisOther').attr('checked')) {
			$('#DisOther').css("display","");
		}
		else {
			$('#DisOther').css("display","none");
		}
	});

	$(':input[name="RadioPlaceOfBirth"]').change(function() {

		var id = $(this).attr('id');
		if (id == 'InOther') {
			$('#PlaceOfBirth').css("display","");	
		}
		else {
			$('#PlaceOfBirth').css("display","none");
		}
	});

	$("input:radio[name='ParentStatus']").change(function(){
		if ($('#ParentStatus_PSOther').attr('checked')) {
			$('#ParentStatusOther').css("display","");
		}
		else {
			$('#ParentStatusOther').css("display","none");
		}
	});
	
	$('#Prob_Health_Illness').click(function(){
		if ($('#Prob_Health_Illness').attr('checked')) {
			$('#Illness').css("display","");
		}
		else {
			$('#Illness').css("display","none");
		}
	});

	$('#Prob_Behavior_CriminalOffense').click(function(){
		if ($('#Prob_Behavior_CriminalOffense').attr('checked')) {
			$('#CriminalOffense').css("display","");
		}
		else {
			$('#CriminalOffense').css("display","none");
		}
	});

	$('#Prob_Other_OtherProblem').click(function(){
		if ($('#Prob_Other_OtherProblem').attr('checked')) {
			$('#OtherProblem').css("display","");
		}
		else {
			$('#OtherProblem').css("display","none");
		}
	});
	
	$(':input[name="IsReferralToSocialWorker"]').change(function() {
		var id = $(this).attr('id');
		if (id == 'IsReferralToSocialWorkerYes') {
			$('#RefToReasonLabel').css("display","");
			$('#RefToReasonSpan').css("display","");
		}
		else {
			$('#RefToReasonLabel').css("display","none");
			$('#RefToReasonSpan').css("display","none");
			$(':input[name="ReferralToReason\[\]"]').each(function() {
				$(this).attr('checked', false);
			});
			$('#RefToOther').val('');	
		}
	});
	
	$('#RefToReason_RefToOther').click(function(){
		if ($('#RefToReason_RefToOther').attr('checked')) {
			$('#RefToOther').css("display","");
		}
		else {
			$('#RefToOther').css("display","none");
		}
	});
	
	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
		
		if (checkForm(document.form1)) {
	        $('#form1').submit();
		}
	});

});

function hideReferralFrom() {
	$(':input[name="ReferralFromType"]').each(function(){
		var from = $(this).attr('id');
		$('#'+from+'_Span').css("display","none");
	});
}

</script>

<form id="form1" method="post" action="print_pdf.php" target="_blank">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">1. <?=$Lang['eGuidance']['personal']['ReferralFromAndReason']?></td>
								<td class="tabletext">
								<?	$referral_from = $libguidance->getGuidanceSettingItem('Personal','ReferralFrom');
									$x = '<table class="no_bottom_border" width="100%">';										
									foreach((array)$referral_from as $k=>$v) {
										$x .= '<tr>';
											$x .= '<td width="22%">';
												$x .= '<input type="radio" name="ReferralFromType" id="ReferralFrom_'.$k.'" value="'.$k.'"'. (($rs_personal['ReferralFromType'] == $k)?'checked':'').' '.$disabled.'>';
												$x .= '<label for="ReferralFrom_'.$k.'">'.$v.'</label>';
											$x .= '</td>';
											$x .= '<td>';
											
											$display = ($rs_personal['ReferralFromType']== $k)?'':'none';
												
											switch ($k) {
												case 'Teacher':
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
													$x .= getSelectByAssoArray($teacherTypeAry,"name='FromTeacher' id='FromTeacher' $disabled", $rs_personal['ReferralFrom']);
													$x .= '&nbsp;<input type="text" name="FromTeacherReason" id="FromTeacherReason" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$k]).'" style="width:60%;">';
													$x .= '</span>';
													break;
													
												case 'AcademicGroup':
													$refaca_reason = $libguidance->getGuidanceSettingItem('Personal','RefAcaReason');
													$i = 0;
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
													foreach((array)$refaca_reason as $kk=>$vv) {
														$x .= ($i > 0) ? '<br>' : '';
														$x .= '<input type="checkbox" name="RefAcaReason[]" id="RefAcaReason_'.$kk.'" value="'.$kk.'"'. (in_array($kk,(array)$refFromReasonAry)?'checked':'').' '.$disabled.'>';
														$x .= '<label for="RefAcaReason_'.$kk.'">'.$vv.'</label>';
														if ($kk == 'AcaOther') {
															$x .= '&nbsp;<input type="text" name="AcaOther" id="AcaOther" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$kk]).'" style="width:60%; display:'.(in_array($kk,(array)$refFromReasonAry)?'':'none').'">';
														}
														$i++;
													}
													$x .= '</span>';
													break;
													
												case 'DisciplineGroup':
													$refdis_reason = $libguidance->getGuidanceSettingItem('Personal','RefDisReason');
													$i = 0;
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
													foreach((array)$refdis_reason as $kk=>$vv) {
														$x .= ($i > 0) ? '<br>' : '';
														$x .= '<input type="checkbox" name="RefDisReason[]" id="RefDisReason_'.$kk.'" value="'.$kk.'"'. (in_array($kk,(array)$refFromReasonAry)?'checked':'').' '.$disabled.'>';
														$x .= '<label for="RefDisReason_'.$kk.'">'.$vv.'</label>';
														if ($kk == 'DisOther') {
															$x .= '&nbsp;<input type="text" name="DisOther" id="DisOther" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$kk]).'" style="width:60%; display:'.(in_array($kk,(array)$refFromReasonAry)?'':'none').'">';
														}
														$i++;
													}
													$x .= '</span>';
													break;
												
												case 'FromOther':
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
														$x .= '&nbsp;<input type="text" name="FromOther" id="FromOther" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$k]).'" style="width:60%;">';
													$x .= '</span>';											
													break;
											}
											$x .= '</td>';
										$x .= '</tr>';
									}
									$x .= '</table>';
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">2. <?=$Lang['eGuidance']['personal']['PlaceOfBirth']?></td>
								<td class="tabletext">
									<input type="radio" name="RadioPlaceOfBirth" id="InHongKong" value="Hong Kong" <?=($rs_personal['PlaceOfBirth'] == 'Hong Kong')?'checked':''?> <?=$disabled?>>
									<label for="InHongKong"><?=$Lang['eGuidance']['personal']['BornInHongKong']?></label>
									<input type="radio" name="RadioPlaceOfBirth" id="InOther" value="Other" <?=($rs_personal['PlaceOfBirth'] && $rs_personal['PlaceOfBirth'] != 'Hong Kong')?'checked':''?> <?=$disabled?>>
									<label for="InOther"><?=$Lang['eGuidance']['personal']['BornInOther']?></label>
									<input type="text" name="PlaceOfBirth" id="PlaceOfBirth" <?=$disabled?> value="<?=intranet_htmlspecialchars(($rs_personal['PlaceOfBirth'] && $rs_personal['PlaceOfBirth'] != 'Hong Kong')?$rs_personal['PlaceOfBirth']:'')?>" style="width:200px; display:<?=(!isset($rs_personal['PlaceOfBirth']) || $rs_personal['PlaceOfBirth'] == 'Hong Kong') ? 'none':''?>">
									<div>&nbsp;<?=$Lang['eGuidance']['personal']['YearInHongKong']?>
										<input type="text" class="number" name="YearInHongKong" id="YearInHongKong" <?=$disabled?> value="<?=$rs_personal['YearInHongKong'] ? $rs_personal['YearInHongKong'] : ''?>" style="width:40px;" maxlength="5">
										<span class="error_msg_hide" id="ErrYearInHongKong"><?=$Lang['eGuidance']['Warning']['InputNumber']?></span>
									</div>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">3. <?=$Lang['eGuidance']['personal']['FamilyStatus']?></td>
								<td class="tabletext">
									<table class="no_bottom_border">
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['ParentStatus']?></td>
											<td>
									<? 	$parent_status = $libguidance->getGuidanceSettingItem('Personal','ParentStatus');
										$i = 0;
										$x = '';
										foreach((array)$parent_status as $k=>$v) {
											$x .= ($i > 0) ? '<br>' : '';
											if ($k == 'PSOther') {
												if (!empty($rs_personal['ParentStatus']) && substr($rs_personal['ParentStatus'],0,7)==$k) {
													$selParentStatusOther = true;
													$strAry = explode("^:",$rs_personal['ParentStatus']);
													if (count($strAry) > 1) {
														$parentStatusOther = $strAry[1];
													}
													else {
														$parentStatusOther = '';
													}
												}
												else {
													$selParentStatusOther = false;
													$parentStatusOther = '';
												}
												$x .= '<input type="radio" name="ParentStatus" id="ParentStatus_'.$k.'" value="'.$k.'"'. ($selParentStatusOther?' checked':'').' '.$disabled.'>';
												$x .= '<label for="ParentStatus_'.$k.'">'.$v.'</label> ';
												$x .= '<input type="text" name="ParentStatusOther" id="ParentStatusOther" value="'.intranet_htmlspecialchars($parentStatusOther).'" style="width:60%; display:'.($selParentStatusOther?'':'none').'" '.$disabled.'>';
											}
											else {
												$x .= '<input type="radio" name="ParentStatus" id="ParentStatus_'.$k.'" value="'.$k.'"'. ($rs_personal['ParentStatus']==$k?'checked':'').' '.$disabled.'>';
												$x .= '<label for="ParentStatus_'.$k.'">'.$v.'</label>';
											}										  
											$i++;
										}
										echo $x;
									?>
											</td>
										</tr>
										
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['BrotherAndSister']?></td>
											<td>	
												<input type="text" class="integer" style="width: 40px;" name="TotalNbrBrotherSister" id="TotalNbrBrotherSister" value="<?=$rs_personal['TotalNbrBrotherSister'] ? $rs_personal['TotalNbrBrotherSister'] : ''?>">
												<?=$Lang['eGuidance']['personal']['People']?>
											</td>
										</tr>										
									</table>
								</td>
							</tr>

						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">4. <?=$Lang['eGuidance']['personal']['MainProblem']?></td>
								<td class="tabletext">
								<? 	$category = $libguidance->getGuidanceSettingItem('Personal','Category');
									$i = 1;
									$x = '';
									foreach((array)$category as $k=>$v) {
										$j = 0;
										$letter = numberToLetter($i,true);	// upper case
										$x .= '<table width="100%" class="no_bottom_border">';
											$x .= '<tr><td colspan="2" style="font-weight:bold;">'.$letter.'. '.$v.'</td></tr>';
											$x .= '<tr>';
												$x .= '<td width="20px;">';
												$x .= '<td>';
												$sub_item = $libguidance->getGuidanceSettingItem('Personal',$k);
												foreach((array)$sub_item as $kk=>$vv) {
													$x .= ($j > 0) ? '<br>' : '';
													$x .= '<input type="checkbox" name="Prob_'.$k.'[]" id="Prob_'.$k.'_'.$kk.'" value="'.$kk.'"'. (in_array($kk,(array)${"prob".$k."Ary"})?'checked':'').' '.$disabled.'>';
													$x .= '<label for="Prob_'.$k.'_'.$kk.'">'.$vv.'</label>';
													if ($kk == 'Illness') {
														$x .= '&nbsp;<input type="text" name="Illness" id="Illness" '.$disabled.'  value="'.intranet_htmlspecialchars($probHealthAry[$kk]).'" style="width:80%; display:'.(($probHealthAry[$kk])?'':'none').'" '.$disabled.'>';
													}
													else if ($kk == 'CriminalOffense') {
														$x .= '&nbsp;<input type="text" name="CriminalOffense" id="CriminalOffense" '.$disabled.'  value="'.intranet_htmlspecialchars($probBehaviorAry[$kk]).'" style="width:80%; display:'.(($probBehaviorAry[$kk])?'':'none').'" '.$disabled.'>';
													}
													else if ($kk == 'OtherProblem') {
														$x .= '&nbsp;<input type="text" name="OtherProblem" id="OtherProblem" '.$disabled.'  value="'.intranet_htmlspecialchars($probOtherAry[$kk]).'" style="width:80%; display:'.(($probOtherAry[$kk])?'':'none').'" '.$disabled.'>';
													}
													$j++;
												}
												$x .= '</td>';
											$x .= '</tr>';
										$x .= '</table>';						
										$i++;
									}
									echo $x;
								?>
								</td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">5. <?=$Lang['eGuidance']['personal']['FollowupStatus']?></td>
								<td class="tabletext">
									<table class="no_bottom_border" width="100%">
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['ReferredToSocialWorker']?></td>
											<td>
												<input type="radio" name="IsReferralToSocialWorker" id="IsReferralToSocialWorkerYes" value="1" <?=$disabled?>><label for="IsReferralToSocialWorkerYes"><?=$Lang['General']['Yes']?></label>
												<input type="radio" name="IsReferralToSocialWorker" id="IsReferralToSocialWorkerNo" value="0" <?=$disabled?>><label for="IsReferralToSocialWorkerNo"><?=$Lang['General']['No']?></label>
											</td>
										</tr>
										<tr>
											<td width="20%" id="RefToReasonLabel" style="display:<?=$rs_personal['IsReferralToSocialWorker'] ? '':'none'?>"><?=$Lang['eGuidance']['personal']['ReferredToReason']?></td>
											<td>
											<?
												$refto_reason = $libguidance->getGuidanceSettingItem('Personal','RefToReason');
												$i = 0;
												$x = '<span id="RefToReasonSpan" style="display:'.($rs_personal['IsReferralToSocialWorker'] ? '':'none').'">';
												foreach((array)$refto_reason as $k=>$v) {
													$x .= ($i > 0) ? '<br>' : '';
													$x .= '<input type="checkbox" name="ReferralToReason[]" id="RefToReason_'.$k.'" value="'.$k.'"'. (in_array($k,(array)$referralToReasonAry)?'checked':'').' '.$disabled.'>';
													$x .= '<label for="RefToReason_'.$k.'">'.$v.'</label>';
													if ($k == 'RefToOther') {
														$x .= '&nbsp;<input type="text" name="RefToOther" id="RefToOther" '.$disabled.'  value="'.intranet_htmlspecialchars($referralToReasonAry[$k]).'" style="width:80%; display:'.(in_array($k,(array)$referralToReasonAry)?'':'none').'" '.$disabled.'>';
													}
													$i++;
												}
												$x .= '</span>';
												echo $x;
											?>
											</td>
										</tr>
									</table>
								</td>
							</tr>																	
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['report']['GenerateReport'], "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>