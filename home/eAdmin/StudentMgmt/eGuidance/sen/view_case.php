<?php
/*
 * modifying:
 *
 * Log
 *
 *
 * 2020-05-08 [Cameron]
 * - retrieve SENTypeConfirmDate [case #F152883]
 * 
 * 2020-03-03 [Cameron]
 * - add line break before SEN case sub type in SENType with text
 * - retrieve $senItemTextAry according to the revised format [case #D178788]
 * 
 * 2019-08-13 [Cameron]
 * - allow classTeacher and subjectTeacher to view SEN records of the related students in their corresponding class / subject class [case #M158130] 
 *      
 * 2018-05-25 [Cameron]
 * - show student name without class
 *      
 * 2018-04-13 [Cameron]
 * - use setup for SEN case type
 *
 * 2018-04-11 [Cameron]
 * - show STRN, Gender and DateOfBirth of a student [case #M130681]
 *
 * 2018-04-10 [Cameron]
 * - add quit option to IsSEN field [case #M130681]
 * - add ConfirmDate and NumberOfReport fields
 *
 * 2017-08-03 [Cameron]
 * - add lock icon to indicate confidential record, don't allow to view if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-24 [Cameron]
 * - add category to support service
 * - add field: Tier
 *
 * 2017-07-21 [Cameron]
 * - fix bug: should redirect to listview page if $StudentID is empty
 *
 * 2017-07-20 [Cameron]
 * - need to show SEN type no matter it's confirmed or not
 * - add SEN Type: RD, MH, break ADHD to AD and HD. MH can specify what kind of mental health, RD contains sub-items
 *
 * 2017-03-21 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$filterClassOrStudentAry = array();
$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['SEN']) {
    if (!$permission['isClassTeacher'] && !$permission['isSubjectTeacher']) {
        $libguidance->NO_ACCESS_RIGHT_REDIRECT();
        exit;
    }
    else {
        $filterClassOrStudentAry = $libguidance->getFilterClassListOrUserList($permission);
    }
}


if (! $StudentID) {
    header("location: case_index.php");
}
else {  // need to check record permission of the student 
    if (count($filterClassOrStudentAry)) {
        $studentInfo = $libguidance->getStudentInfo($StudentID);
        $checkField = $junior_mck ? $studentInfo['ClassName'] : $StudentID;
        if (!in_array($checkField,$filterClassOrStudentAry)) {
            $libguidance->NO_ACCESS_RIGHT_REDIRECT();
            exit;
        }
    }
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();

$studentSelection = getSelectByArray(array(), "name='StudentID' id='StudentID'");

$nameField = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? 'ChineseName' : 'EnglishName';
// # get adjustment type
$rs_sen_adjust_type = $libguidance->getSENAdjustType();
$rs_sen_adjust_type = BuildMultiKeyAssoc($rs_sen_adjust_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment item
$rs_sen_adjust_item = $libguidance->getSENAdjustItem();
$rs_sen_adjust_item = BuildMultiKeyAssoc($rs_sen_adjust_item, array(
    'TypeID',
    'ItemID'
), array(
    $nameField,
    'WithText'
));

// # get study service type
$rs_sen_study_service_type = $libguidance->getServiceType('', 'Study');
$rs_sen_study_service_type = BuildMultiKeyAssoc($rs_sen_study_service_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment support study service
$rs_sen_support_study = $libguidance->getSENSupportService('', '', 'Study');
$rs_sen_support_study = BuildMultiKeyAssoc($rs_sen_support_study, array(
    'TypeID',
    'ServiceID'
), array(
    $nameField,
    'WithText'
));

// # get other service type
$rs_sen_other_service_type = $libguidance->getServiceType('', 'Other');
$rs_sen_other_service_type = BuildMultiKeyAssoc($rs_sen_other_service_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment support other service
$rs_sen_support_other = $libguidance->getSENSupportService('', '', 'Other');
$rs_sen_support_other = BuildMultiKeyAssoc($rs_sen_support_other, array(
    'TypeID',
    'ServiceID'
), array(
    $nameField,
    'WithText'
));

$teacherInfo = '';
$studentInfo = '';
$studentID = $StudentID;
$rs_sencase = $libguidance->getSENCase($studentID);
$senItemAry = array(); // items under SEN type: can be array or string , e.g. array(RD => array(RDC, RDE), MH =>"memtal health prob")
$senItemTextAry = array();
$rsSENTypeConfirmDate = $libguidance->getSENTypeConfirmDate($studentID);

if (count($rs_sencase) == 1) {
    $rs_sencase = $rs_sencase[0];
    $senTypeAry = explode('^~', $rs_sencase['SENType']);
    foreach ((array) $senTypeAry as $k => $v) {
        if (strpos($v, '^:') !== false) {
            list ($senType, $senItemStr) = explode('^:', $v);
            $senTypeAry[$k] = $senType;
            $pos = strpos($senItemStr, '^@');
            $senItems = $senItemStr;
            if ($pos !== false) {
                $senItems = substr($senItemStr, 0, $pos);
                $senItemTextAry[$senType] = substr($senItemStr,$pos+2);
            }
            if (strpos($senItems, '^#') !== false) {
                $senItemAry[$senType] = explode('^#', $senItems);
            } else {
                $senItemAry[$senType] = $senItems;
            }
        }
        else if (strpos($v, '^@') !== false) {
            list ($senType, $itemText) = explode('^@', $v);
            $senTypeAry[$k] = $senType;
            $senItemTextAry[$senType] = $itemText;
        }
    }
    
    $studentInfo = $libguidance->getStudentInfo($studentID);
    if (count($studentInfo)) {
        $rs_sencase['STRN'] = $studentInfo['STRN'] ? $studentInfo['STRN'] : '-';
        $gender = $studentInfo['Gender'];
        if ($gender == 'M') {
            $rs_sencase['Gender'] = $Lang['eGuidance']['personal']['GenderMale'];
        } elseif ($gender == 'F') {
            $rs_sencase['Gender'] = $Lang['eGuidance']['personal']['GenderFemale'];
        } else {
            $rs_sencase['Gender'] = '-';
        }
        $rs_sencase['DateOfBirth'] = ($studentInfo['DateOfBirth'] == '0000-00-00' || $studentInfo['DateOfBirth'] == '') ? '-' : $studentInfo['DateOfBirth'];
    }
    
    $rs_sencase['Teacher'] = $libguidance->getSENCaseTeacher($studentID);
    $teacherInfo = $libguidance_ui->getUserNameList($rs_sencase['Teacher']);
    
    $confidentialViewerInfo = '';
    if ($rs_sencase['IsConfidential']) {
        $confidentialViewerID = $rs_sencase['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: case_index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $confidentialViewerID = $libguidance->getTeacher('-1', $cond);
            $confidentialViewerInfo = $libguidance_ui->getUserNameList($confidentialViewerID);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($studentID);
    if (! empty($rs_class_name)) {
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_sencase['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    }
    else {
        $studentInfo = $libguidance->getStudent($rs_sencase['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
    
    $rs_sencase_adjustment = $libguidance->getSENCaseAdjustment($studentID);
    $rs_sencase_adjustment = BuildMultiKeyAssoc($rs_sencase_adjustment, 'ItemID', array(
        'ItemID',
        'Arrangement'
    ));
    
    $rs_sencase_support_study = $libguidance->getSENCaseSupport($studentID, '', 'Study');
    $rs_sencase_support_study = BuildMultiKeyAssoc($rs_sencase_support_study, 'ServiceID', array(
        'ServiceID',
        'Arrangement'
    ));
    
    $rs_sencase_support_other = $libguidance->getSENCaseSupport($studentID, '', 'Other');
    $rs_sencase_support_other = BuildMultiKeyAssoc($rs_sencase_support_other, 'ServiceID', array(
        'ServiceID',
        'Arrangement'
    ));
} else {
    header("location: case_index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSEN";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getSENTabs("Case");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    'SEN' . $Lang['eGuidance']['sen']['Tab']['Case'],
    "case_index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['View'],
    ""
);

$linterface->LAYOUT_START();

?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?></td>
								<td class="tabletext"><?=$studentInfo?> <?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?>
									<div>
 										<?php echo $i_STRN;?>:&nbsp;&nbsp;&nbsp;<span id="strnSpan"><?php echo $rs_sencase['STRN'];?></span><br>
 										<?php echo $i_UserGender;?>:&nbsp;&nbsp;&nbsp;<span
											id="genderSpan"><?php echo $rs_sencase['Gender'];?></span><br>
 										<?php echo $i_UserDateOfBirth;?>:&nbsp;&nbsp;&nbsp;<span
											id="dateOfBirthSpan"><?php echo $rs_sencase['DateOfBirth'];?></span>
									</div></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?></td>
								<td class="tabletext"><?=$teacherInfo?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Confidential']?><img src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock_own.gif" style="display:<?=($rs_sencase['IsConfidential']?'':'none')?>"></td>
								<td class="tabletext"><input type="radio" name="IsConfidential"
									id="IsConfidentialYes" value="1"
									<?=$rs_sencase['IsConfidential']?' checked':''?> disabled><label
									for="IsConfidentialYes"><?=$Lang['General']['Yes']?></label> <input
									type="radio" name="IsConfidential" id="IsConfidentialNo"
									value="0" <?=$rs_sencase['IsConfidential']?'':' checked'?>
									disabled><label for="IsConfidentialNo"><?=$Lang['General']['No']?></label>

									<div id="ConfidentialSection">
										<?=$confidentialViewerInfo?>
									</div></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen']['SENStudent']?></td>
								<td class="tabletext"><input type="radio" name="IsSEN"
									id="SENConfirm" disabled value="1"
									<?=($rs_sencase['IsSEN'] == 1?' checked':'')?>><?=$Lang['eGuidance']['sen']['Confirm']?><br>
									<input type="radio" name="IsSEN" id="SENNotConfirm" disabled
									value="0"
									<?=(isset($rs_sencase['IsSEN']) && $rs_sencase['IsSEN']==0?' checked':'')?>><?=$Lang['eGuidance']['sen']['NotConfirm']?><br>
									<input type="radio" name="IsSEN" id="SENQuit" disabled
									value="2" <?=($rs_sencase['IsSEN']==2?' checked':'')?>><label
									for="SENQuit"><?=$Lang['eGuidance']['sen']['Quit']?></label></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['ConfirmDate']?></td>
								<td class="tabletext"><?=(empty($rs_sencase['ConfirmDate']) || $rs_sencase['ConfirmDate'] == '0000-00-00')?'-':$rs_sencase['ConfirmDate']?></td>
							</tr>

							<tr valign="top" id="trSENType">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['CaseType']?></td>
								<td class="tabletext">
<?
$sen_case_type = $libguidance->getAllSENCaseType();
$senCaseTypeAssoc = BuildMultiKeyAssoc($sen_case_type, array(
    'Code'
), $IncludedDBField = array(
    'Name',
    'IsWithText'
));
$sen_case_subtype = $libguidance->getAllSENCaseSubType();
$senCaseSubTypeAssoc = BuildMultiKeyAssoc($sen_case_subtype, array(
    'TypeCode',
    'Code'
), $IncludedDBField = array(
    'Name'
), $SingleValue = 1);

$x = '<table width="100%">';
$x .= '<tr><td>'.$Lang['eGuidance']['guidance']['CaseType'].'</td><td id="SENTypeConfirmTitle" class="SENTypeConfirmDate" style="display:'.($rs_sencase['IsSEN'] ? '' : 'none').'">'.$Lang['eGuidance']['sen_case']['SENTypeConfirmDate'].'</td></tr>';
foreach ((array) $senCaseTypeAssoc as $k => $v) {
    $senTypeClass = '';
    if (count($senCaseSubTypeAssoc[$k]) && $v['IsWithText']) {
        $senTypeClass = 'withSubType withText';
    } elseif (count($senCaseSubTypeAssoc[$k])) {
        $senTypeClass = 'withSubType';
    } elseif ($v['IsWithText']) {
        $senTypeClass = 'withText';
    } else {
        $senTypeClass = '';
    }
    
    $x .= '<tr><td>';
    $x .= '<input type="checkbox" class="' . $senTypeClass . '" data-TypeCode="' . $k . '" name="SENType[]" id="SENType_' . $k . '" disabled value="' . $k . '"' . (in_array($k, (array) $senTypeAry) ? 'checked' : '') . '>';
    $x .= '<label for="SENType_' . $k . '">' . $v['Name'] . '</label>';
    
    if ($v['IsWithText']) {
        $x .= ' <input type="text" name="SENType' . $k . '" id="SENType' . $k . '" disabled value="' . intranet_htmlspecialchars($senItemTextAry[$k]) . '" style="width:60%; display:' . (in_array($k, (array) $senTypeAry) ? '' : 'none') . '">';
    }
    
    if (count($senCaseSubTypeAssoc[$k])) {
        $numOfSubType = 0;
        if ($senTypeClass == 'withSubType withText') {
            $x .= '<br>&nbsp;&nbsp;&nbsp;';
        }        
        $x .= ' <span class="subTypeSpan" id="subTypeSpan_' . $k . '" style="display:' . (in_array($k, (array) $senTypeAry) ? '' : 'none') . '">(';
        
        foreach ((array) $senCaseSubTypeAssoc[$k] as $_subTypeCode => $_subTypeName) {
            if ($numOfSubType > 0) {
                $x .= ' / ';
            }
            $x .= '<input type="checkbox" class="subTypeInput" data-TypeCode="' . $k . '" name="' . $k . '[]" id="' . $_subTypeCode . '" disabled value="' . $_subTypeCode . '"' . (in_array("$_subTypeCode", (array) $senItemAry[$k]) ? 'checked' : '') . '>';
            $x .= '<label for="' . $_subTypeCode . '">' . $_subTypeName . '</label>';
            
            $numOfSubType ++;
        }
        $x .= ')</span>';
    }
    
    $x .= '</td>';
    $x .= '<td id="tdSENTypeConfirmDate_'.$k.'" class="tabletext" style="display:'.($rs_sencase['IsSEN'] && in_array($k, (array) $senTypeAry) ? '' : 'none').'">';
    $x .= (empty($rsSENTypeConfirmDate[$k]) || $rsSENTypeConfirmDate[$k] == '0000-00-00')?'-':$rsSENTypeConfirmDate[$k];
    $x .= '</td></tr>';
//    $x .= '<br>';
}
$x .= '</table>';
echo $x;
?>								
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['Tier']['TierName']?></td>
								<td class="tabletext" valign="middle">
								<?
        $x = '';
        $x .= '<table class="no_bottom_border">';
        for ($i = 1; $i <= 3; $i ++) {
            $x .= '<tr><td style="vertical-align:top; width:80px; "><input type="checkbox" name="IsTier' . $i . '" id="IsTier' . $i . '" disabled value="1"' . ($rs_sencase["IsTier$i"] ? ' checked' : '') . '><label for="Tier_' . $i . '">' . $Lang['eGuidance']['sen_case']['Tier']["Tier$i"] . '</label></td>';
            $x .= '<td>' . nl2br($rs_sencase['Tier' . $i . 'Remark']) . '</td>';
            $x .= '</tr>';
        }
        $x .= '</table>';
        echo $x;
        ?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['FineTuneArrangement']?>
									</td>
								<td class="tabletext">
									<table class="no_bottom_border">
									<?
        
        $x = '';
        foreach ((array) $rs_sen_adjust_type as $k => $v) {
            $x .= '<tr><td><input type="checkbox" name="TypeID[]" id="AdjustType_' . $k . '" disabled onClick="CheckAjdustment(' . $k . ')">' . $v . '</td></tr>';
            foreach ((array) $rs_sen_adjust_item[$k] as $kk => $vv) {
                if ($vv['WithText']) {
                    $x .= '<tr><td>';
                    $x .= '<table class="no_bottom_border">';
                    $x .= '<tr><td style="padding-left:17px; vertical-align:top; width:80px; "><input type="checkbox" class="AdjustType_' . $k . '" name="ItemID[' . $k . '][]" id="AdjustItem_' . $k . '_' . $kk . '" disabled value="' . $kk . '"' . ($rs_sencase_adjustment[$kk] ? ' checked' : '') . '>' . $vv[$nameField] . '</td>';
                    $x .= '<td>' . nl2br($rs_sencase_adjustment[$kk]['Arrangement']) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td></tr>';
                } else {
                    $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="AdjustType_' . $k . '" name="ItemID[' . $k . '][]" id="AdjustItem_' . $k . '_' . $kk . '" disabled value="' . $kk . '"' . ($rs_sencase_adjustment[$kk] ? ' checked' : '') . '>' . $vv[$nameField] . '</td></tr>';
                }
            }
        }
        echo $x;
        ?>
									</table>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['FineTuneSupportStudy']?>
									</td>
								<td class="tabletext">
									<table class="no_bottom_border">
									<?
        
        $x = '';
        foreach ((array) $rs_sen_study_service_type as $k => $v) {
            $x .= '<tr><td><input type="checkbox" name="ServiceTypeID[]" id="ServiceType_' . $k . '" disabled onClick="CheckService(' . $k . ')"><label for="ServiceType_' . $k . '">' . $v . '</label></td></tr>';
            foreach ((array) $rs_sen_support_study[$k] as $kk => $vv) {
                if ($vv['WithText']) {
                    $x .= '<tr><td>';
                    $x .= '<table class="no_bottom_border">';
                    $x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" disabled value="' . $kk . '"' . ($rs_sencase_support_study[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td>';
                    $x .= '<td>' . nl2br($rs_sencase_support_study[$kk]['Arrangement']) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td></tr>';
                } else {
                    $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" disabled value="' . $kk . '"' . ($rs_sencase_support_study[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
                }
            }
        }
        echo $x;
        ?>
									</table>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['FineTuneSupportOther']?>
									</td>
								<td class="tabletext">
									<table class="no_bottom_border">
									<?
        
        $x = '';
        foreach ((array) $rs_sen_other_service_type as $k => $v) {
            $x .= '<tr><td><input type="checkbox" name="ServiceTypeID[]" id="ServiceType_' . $k . '" disabled onClick="CheckService(' . $k . ')"><label for="ServiceType_' . $k . '">' . $v . '</label></td></tr>';
            foreach ((array) $rs_sen_support_other[$k] as $kk => $vv) {
                if ($vv['WithText']) {
                    $x .= '<tr><td>';
                    $x .= '<table class="no_bottom_border">';
                    $x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" disabled value="' . $kk . '"' . ($rs_sencase_support_other[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td>';
                    $x .= '<td>' . nl2br($rs_sencase_support_other[$kk]['Arrangement']) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td></tr>';
                } else {
                    $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" disabled value="' . $kk . '"' . ($rs_sencase_support_other[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
                }
            }
        }
        echo $x;
        ?>
									</table>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['NumberOfReport']?>
								</td>
								<td class="tabletext"><?php echo ($rs_sencase['NumberOfReport'] ? $rs_sencase['NumberOfReport'] : '-');?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_case']['Remark']?></td>
								<td class="tabletext"><?=($rs_sencase['Remark'] ? nl2br($rs_sencase['Remark']) : '-')?></td>
							</tr>
						
							<?=$libguidance_ui->getAttachmentLayout('SENCase',$studentID,'view')?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img
						src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
						height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
							<?
    
    if ($permission['admin'] || in_array('MGMT', (array) $permission['current_right']['MGMT']['SEN'])) {
        echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit_case.php?StudentID=$studentID'") . '&nbsp';
    }
    echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='case_index.php'");
    ?>									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table> <br>
		</td>
	</tr>
</table>

<input type="hidden" name="StudentID" id="StudentID"
	value="<?=$rs_sencase['StudentID']?>">

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>