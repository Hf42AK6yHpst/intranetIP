<?php
/*
 * 	modifying: 
 * 
 * 	Log
 *
 *  2019-08-13 [Cameron]
 *      - allow classTeacher and subjectTeacher to view SEN records of the related students in their corresponding class / subject class [case #M158130] 
 *      
 * 	2017-11-06 [Cameron]
 * 		- adjust "Add Activity" button to left position (next to plus sign)
 *  
 * 	2017-06-29 [Cameron]
 * 		- move ActivityDate to Add / Edit Activity interface (thickbox), it can be changed
 * 		- remove ActivityDate parameter in addRow
 * 		- add 'Add Activity' button
 * 
 * 	2017-05-19 [Cameron]
 * 		- fix bug: show ServiceType by Lang
 * 
 * 	2017-02-28 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['SEN'] && !$permission['isClassTeacher'] && !$permission['isSubjectTeacher']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$ServiceID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
				

## get record details

$sen_service_type = $libguidance->getGuidanceSettingItem('SENService','ServiceType');

$rs_senservice = $libguidance->getSENService($ServiceID);
$rs_senservice = $rs_senservice[0];
if (count($sen_service_type)) {
	if (substr($rs_senservice['ServiceType'],0,5) == 'Other') {
		$serviceType = $sen_service_type['Other'] . substr($rs_senservice['ServiceType'],6);	// include :
	} 
	else {
		$serviceType = $sen_service_type[$rs_senservice['ServiceType']];
	}
}
else {
	$serviceType = '-';
}
$orderBy = "ClassName,ClassNumber";

if (!$permission['admin'] && !$permission['current_right']['MGMT']['SEN']) {
    $filterClassOrStudentAry = $libguidance->getFilterClassListOrUserList($permission);
    if ($junior_mck) {
        $condition = " AND u.ClassName IN ('".implode("','",$filterClassOrStudentAry)."')";
    }
    else {
        $condition = " AND u.UserID IN ('".implode("','",$filterClassOrStudentAry)."')";
    }
}
else {
    $condition = "";
}

$student = $libguidance->getSENServiceStudent($ServiceID,$orderBy,$condition);
if (count($student) == 0) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$student_list = $libguidance_ui->getUserNameList($student);
$student = BuildMultiKeyAssoc($student, 'UserID');

$teacher = $libguidance->getSENServiceTeacher($ServiceID);
$teacher_list = $libguidance_ui->getUserNameList($teacher);

$h_ActivityNavigation = $linterface->GET_NAVIGATION2($Lang['eGuidance']['sen_service']['ActivityList']);

### Activity Action Button
$BtnArr = array();
$BtnArr[] = array('delete', 'javascript:deleteActivity();');
$h_actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);

### Submit & Cancel Button
$h_submitBtn = $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checkForm();");
$h_cancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'");

if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['SEN'])) {
	$layoutMode = 'edit';
}
else {
	$layoutMode = 'view';
}

$today = date('Y-m-d');


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSEN";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getSENTabs("Service");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['sen_service']['SenService'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['sen_service']['ActivityManagement'], "");

$linterface->LAYOUT_START();

echo $linterface->Include_Thickbox_JS_CSS();
?>

<script type="text/javascript">

function addRow() {
	tb_show('<?=$Lang['eGuidance']['sen_service']['AddActivity']?>','../ajax/ajax_layout.php?action=NewSENServiceActivity&ServiceID='+$('#ServiceID').val()+'&height=500&width=800');
}

function edit_activity(activityID) {
	tb_show('<?=$Lang['eGuidance']['sen_service']['EditActivity']?>','../ajax/ajax_layout.php?action=EditSENServiceActivity&ActivityID='+activityID+'&height=500&width=800');
}

function deleteActivity() {
	if ($('input.ActivityChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else if (confirm(globalAlertMsg3)){
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_update.php?action=delete_sen_service_activity',
			data : $('#form1').serialize(),		  
			success: update_activity_list,
			error: show_ajax_error
		});
	}
}

function update_activity_list(ajaxReturn) {
	if (ajaxReturn != null) {
		if (ajaxReturn.action_result != null) {
			Get_Return_Message(ajaxReturn.action_result);
		}
		if (ajaxReturn.success) {
			$('#ActivityDiv').html(ajaxReturn.html);
		}
	}
}	

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

</script>
<form name="form1" id="form1" method="post">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<br style="clear:both;" />
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_service']['ServiceType']?>
									</td>
								<td class="tabletext"><?=$serviceType?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?>
									</td>
								<td class="tabletext"><?=$rs_senservice['StartDate']?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									</td>
								<td class="tabletext"><?=$student_list?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?>
									</td>
								<td class="tabletext"><?=$teacher_list?></td>
							</tr>
							
							<?=$libguidance_ui->getAttachmentLayout('SENService',$ServiceID,'view','view_current_attachment')?>
							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<br style="clear:both;" />
	
	<div style="float:left"><?=$h_ActivityNavigation?></div>
	<br style="clear:both;" />
	<?=($layoutMode == 'edit' ? $h_actionBtn : '')?>
	<div id="ActivityDiv"><?=$libguidance_ui->getSENServiceActivityTable($ServiceID,$student,$layoutMode)?></div>
	<br style="clear:both;" />

<? if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['SEN'])):?>
	<div><label>
		<span class="table_row_tool"><a class="newBtn add" onclick="javascript:addRow()" title="<?=$Lang['eGuidance']['sen_service']['AddActivity']?>"></a></span>
		<span><a href="#" style="float: left" onclick="javascript:addRow()"><?=$Lang['eGuidance']['sen_service']['AddActivity']?></a></span></label>
	</div>
<? endif;?>
	<input type="hidden" id="ServiceID" name="ServiceID" value="<?=$ServiceID?>" />
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


