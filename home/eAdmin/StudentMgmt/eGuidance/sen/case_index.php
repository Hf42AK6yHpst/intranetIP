<?
/*
 * Modified by: 
 * 
 * Log
 *
 * 2020-03-19 [Cameron]
 * - revised keyword search to compare student name without class and teacher name without title
 * - don't apply standardizeFormPostValue() and standardizeFormGetValue() to keyword search for IP, but leave it for ej
 * - known bug: sometimes search result is more than expected for Chinese Character because it does not know the starting point when use like 
 * 
 * 2020-03-18 [Cameron]
 * - add SENCase Type filter [case #M173271]
 * - keyword search supports searched by SenCaseSubType, SenCaseWithText
 * 
 * 2019-08-13 [Cameron]
 * - allow classTeacher and subjectTeacher to view SEN records of the related students in their corresponding class / subject class [case #M158130] 
 *      
 * 2018-05-25 [Cameron]
 * - fix: should retrieve all records (use left join instead of inner join that filter currrent academic year only) 
 *      
 * 2018-04-12 [Cameron]
 * - add Quit to $isSENList and IsSEN
 *
 * 2017-11-20 [Cameron]
 * - fix cookie and keyword search
 *
 * 2017-08-03 [Cameron]
 * - add return message UpdateSuccessNotifyFail, UpdateSuccessNotifyFail, AddAndNotifySuccess, UpdateAndNotifySuccess
 * - add lock icon next to student, don't allow to edit / view if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-20 [Cameron]
 * - always show SEN type no matter it's confirmed or not
 * - add argument $sentype_subitem to displayFormat_with_multiple_values_in_a_column
 *
 * 2017-07-19 [Cameron] TeacherPIC sort in English name
 *
 * 2017-06-27 [Cameron] fix bug: do not show SENType if IsSEN<>1 (void after 2017-07-20)
 *
 * 2017-06-15 [Cameron] disable academic year filter as class name and class number is retrieved from current year
 *
 * 2017-05-19 [Cameron] keywork search: apply standardizeFormPostValue first, then Get_Safe_Sql_Like_Query, not need to apply intranet_htmlspecialchars
 *
 * 2017-03-01 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libSenCaseTypeTable.php");

// set cookies
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_page_no",
    "pageNo"
);
$arrCookies[] = array(
    "ck_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_page_field",
    "field"
);

$arrCookies[] = array(
    "ck_ClassName",
    "ClassName"
);
$arrCookies[] = array(
    "ck_IsSEN",
    "IsSEN"
);
$arrCookies[] = array(
    "ck_keyword",
    "keyword"
);
$arrCookies[] = array(
    "ck_LogicalType",
    "LogicalType"
);

if (isset($_POST['SenCaseTypeFilter']) && count($_POST['SenCaseTypeFilter'])) {
    $SenCaseTypeFilterStr = implode(',',(array)$_POST['SenCaseTypeFilter']);    
}
else {
    $SenCaseTypeFilterStr = '';
}

if (isset($_POST['SenCaseSubTypeFilter']) && count($_POST['SenCaseSubTypeFilter'])) {
    $SenCaseSubTypeFilterStr = implode(',',(array)$_POST['SenCaseSubTypeFilter']);
}
else {
    $SenCaseSubTypeFilterStr = '';
}

$arrCookies[] = array(
    "ck_SenCaseTypeFilterStr",
    "SenCaseTypeFilterStr"
);

$arrCookies[] = array(
    "ck_SenCaseSubTypeFilterStr",
    "SenCaseSubTypeFilterStr"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

$SenCaseTypeFilterAry = ($SenCaseTypeFilterStr != '') ? explode(',',$SenCaseTypeFilterStr) : '';
$SenCaseSubTypeFilterAry = ($SenCaseSubTypeFilterStr != '') ? explode(',',$SenCaseSubTypeFilterStr) : '';


intranet_auth();
intranet_opendb();

$filterClassOrStudentAry = array();
$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['SEN']) {
    if (!$permission['isClassTeacher'] && !$permission['isSubjectTeacher']) {
        $libguidance->NO_ACCESS_RIGHT_REDIRECT();
        exit;
    }
    else {
        $filterClassOrStudentAry = $libguidance->getFilterClassListOrUserList($permission);
    }
}


// Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSEN";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getSENTabs("Case");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
if ($returnMsgKey == 'AddSuccessNotifyFail' || $returnMsgKey == 'UpdateSuccessNotifyFail' || $returnMsgKey == 'AddAndNotifySuccess' || $returnMsgKey == 'UpdateAndNotifySuccess') {
    $returnMsg = $Lang['eGuidance']['ReturnMessage'][$returnMsgKey];
} else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$joinClass = false;
$cond = '';
if ($IsSEN != '') {
    $cond .= " AND c.IsSEN='" . $IsSEN . "'";
}

if ($junior_mck) {
    if ($ClassName) {
        $cond .= " AND u.ClassName='" . $ClassName . "'";
    }
    
    if (count($filterClassOrStudentAry)) {
        $cond .= " AND u.ClassName IN ('" . implode("','", $filterClassOrStudentAry) . "')";
    }
    $joinClass = '';
    $AcademicYearID = $AcademicYearID ? $AcademicYearID : $currentAcademicYearID;
} else {
    $joinClass = true;
    if ($AcademicYearID) {
        $cond .= " AND ui.AcademicYearID='" . $AcademicYearID . "'";
    }
    if ($ClassName) {
        $cond .= " AND ui.ClassTitleEN='" . $ClassName . "'";
    }
    if (count($filterClassOrStudentAry)) {
        $cond .= " AND u.UserID IN ('" . implode("','", $filterClassOrStudentAry) . "')";
    }
    
    $AcademicYearID = $AcademicYearID ? $AcademicYearID : $currentAcademicYearID;
    $joinClass = "LEFT JOIN (
                        SELECT  ycu.UserID,
                                yc.AcademicYearID,
                                yc.ClassTitleEN
                        FROM
                                YEAR_CLASS_USER ycu
			            INNER JOIN
                                YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$AcademicYearID."'
                  ) AS ui ON ui.UserID=u.UserID";      // ui - user info
}

if ($LogicalType == 'and') {
    if ($SenCaseTypeFilterAry != '') {
        foreach((array)$SenCaseTypeFilterAry as $_senTypeCode) {
            $cond .= " AND CONCAT('^~',c.SENType) LIKE '%^~".$_senTypeCode."%'";
        }
    }
    
    if ($SenCaseSubTypeFilterAry != '') {
        foreach((array)$SenCaseSubTypeFilterAry as $_senSubTypeCode) {
            $cond .= " AND (c.SENType LIKE '%^:".$_senSubTypeCode."%' OR c.SENType LIKE '%^#".$_senSubTypeCode."%')";
        }
    }
}
else {
    if (($SenCaseTypeFilterAry != '') || ($SenCaseSubTypeFilterAry != '')) {
        $cond .= " AND (";
    }
    
    $addOr = false;
    if ($SenCaseTypeFilterAry != '') {
        for ($i=0, $iMax=count($SenCaseTypeFilterAry); $i<$iMax; $i++) {
            $_senTypeCode = $SenCaseTypeFilterAry[$i];
            if ($i>0) {
                $cond .= " OR ";
            }
            $cond .= "CONCAT('^~',c.SENType) LIKE '%^~".$_senTypeCode."%'";
        }
        $addOr = true;
    }
    
    if ($SenCaseSubTypeFilterAry != '') {
        for ($i=0, $iMax=count($SenCaseSubTypeFilterAry); $i<$iMax; $i++) {
            $_senSubTypeCode = $SenCaseSubTypeFilterAry[$i];
            if ($i>0 || $addOr) {
                $cond .= " OR ";
            }
            $cond .= "c.SENType LIKE '%^:".$_senSubTypeCode."%' OR c.SENType LIKE '%^#".$_senSubTypeCode."%'";
        }
    }
    
    if (($SenCaseTypeFilterAry != '') || ($SenCaseSubTypeFilterAry != '')) {
        $cond .= " )";
    }
}


$student_name = getNameFieldWithClassNumberByLang('u.', $isTitleDisabled=true);
$studentNameOnly = getNameFieldByLang2('u.');   // no class name & class number
$teacher_name = getNameFieldByLang();
$teacherNameOnly = getNameFieldByLang2();       // no title

if ($junior_mck || (intranet_phpversion_compare('5.4') != 'ELDER')) {
    $keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
}
else {
    $keyword = $_POST['keyword'] ? $_POST['keyword'] : $keyword;
}
$keyword = trim($keyword);

if ($keyword != "") {
    $kw = $libguidance->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND ($studentNameOnly LIKE '%$kw%' OR t.TeacherNameOnly LIKE '%$kw%'";
    $dashPos = strpos($kw, '-');
    if ($dashPos !== false) {
        $_senTypeName = substr($kw,0,$dashPos);
        $_senTypeDescription = substr($kw,$dashPos+1);
        
        $_senTypeCodeAry = $libguidance->getSearchSENCaseTypeCode($_senTypeName, true);
        if (count($_senTypeCodeAry)) {
            $_senTypeCode = $_senTypeCodeAry[0]['Code'];
            $cond .= " OR (CONCAT('^~',c.SENType) LIKE '%^~".$_senTypeCode."%' AND c.SENType LIKE '%^@%$_senTypeDescription%')";
        }
        else {
            $cond .= " OR c.SENType LIKE '%^@%$_senTypeDescription%' OR c.SENType LIKE '%^@%$kw%'";
        }
    }
    else {
        $cond .= " OR c.SENType LIKE '%^@%$kw%'";
    }    
    unset($kw);
    
    $senTypeCodeAry = $libguidance->getSearchSENCaseTypeCode($keyword);
    if (count($senTypeCodeAry)) {
        $senTypeCodeAry = Get_Array_By_Key($senTypeCodeAry, 'Code');
        if (count($senTypeCodeAry)) {
            for ($i=0, $iMax=count($senTypeCodeAry); $i<$iMax; $i++) {
                $_senTypeCode = $senTypeCodeAry[$i];
                $cond .= " OR CONCAT('^~',c.SENType) LIKE '%^~".$_senTypeCode."%'";
            }
        }
    }
    
    $senSubTypeCodeAry = $libguidance->getSearchSENCaseSubTypeCode($keyword);
    if (count($senSubTypeCodeAry)) {
        $senSubTypeCodeAry = Get_Array_By_Key($senSubTypeCodeAry, 'Code');
        if (count($senSubTypeCodeAry)) {
            for ($i=0, $iMax=count($senSubTypeCodeAry); $i<$iMax; $i++) {
                $_senSubTypeCode = $senSubTypeCodeAry[$i];
                $cond .= " OR c.SENType LIKE '%^:".$_senSubTypeCode."%' OR c.SENType LIKE '%^#".$_senSubTypeCode."%'";
            }
        }
    }
    $cond .= ")";
}

//$li = new libdbtable2007($field, $order, $pageNo);
$li = new libSenCaseTypeTable($field, $order, $pageNo);


$sql = "SELECT 	
				IF(IsConfidential=1,  
					IF(LOCATE(CONCAT(',','" . $_SESSION['UserID'] . "',','),CONCAT(',',ConfidentialViewerID,','))=0,
						 CONCAT(" . $student_name . ",'<img src=\"/images/" . $LAYOUT_SKIN . "/icalendar/icon_lock_own.gif\">'),
					CONCAT('<a href=\"view_case.php?StudentID=',c.StudentID,'\">'," . $student_name . ",'<img src=\"/images/" . $LAYOUT_SKIN . "/icalendar/icon_lock.gif\">','</a>')),
					CONCAT('<a href=\"view_case.php?StudentID=',c.StudentID,'\">'," . $student_name . ",'</a>')),
                CASE c.IsSEN
                    WHEN 1 THEN '" . $Lang['eGuidance']['sen']['Confirm'] . "'
                    WHEN 0 THEN '" . $Lang['eGuidance']['sen']['NotConfirm'] . "'
                    WHEN 2 THEN '" . $Lang['eGuidance']['sen']['Quit'] . "'
                END AS IsSEN,
				c.SENType,
				t.Teacher ";
if ($permission['admin'] || in_array('MGMT', (array) $permission['current_right']['MGMT']['SEN'])) {
    $sql .= ",IF(IsConfidential=1 AND LOCATE(CONCAT(',','" . $_SESSION['UserID'] . "',','),CONCAT(',',ConfidentialViewerID,','))=0,'',
				CONCAT('<input type=\'checkbox\' name=\'StudentID[]\' id=\'StudentID_', c.`StudentID`,'\' value=\'', c.`StudentID`,'\'>')) ";
    $extra_column = 2;
    $checkbox_col = "<th width='1%'>" . $li->check("StudentID[]") . "</th>\n";
    $toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new_case.php')", $button_new, "", "", "", 0);
    $manage_record_bar = '<a href="javascript:checkEdit(document.form1,\'StudentID[]\',\'edit_case.php\')" class="tool_edit">' . $button_edit . '</a>';
    $manage_record_bar .= '<a href="javascript:checkRemove(document.form1,\'StudentID[]\',\'remove_case.php\')" class="tool_delete">' . $button_delete . '</a>';
} else {
    $extra_column = 1;
    $checkbox_col = "";
    $toolbar = "";
    $manage_record_bar = '';
}
$sql .= ",$student_name as Student "; // for sorting
$sql .= "FROM 
				INTRANET_GUIDANCE_SEN_CASE c
		INNER JOIN
				INTRANET_USER u ON u.UserID=c.StudentID " . $joinClass . "
		LEFT JOIN (
				SELECT 	StudentID, 
						GROUP_CONCAT(DISTINCT " . $teacher_name . " ORDER BY EnglishName SEPARATOR ', ') AS Teacher,
                        GROUP_CONCAT(DISTINCT " . $teacherNameOnly. " ORDER BY EnglishName SEPARATOR ', ') AS TeacherNameOnly,
						GROUP_CONCAT(DISTINCT EnglishName ORDER BY EnglishName SEPARATOR ', ') AS SortTeacher
				FROM 
						INTRANET_GUIDANCE_SEN_CASE_TEACHER t
				INNER JOIN 
						INTRANET_USER ut ON ut.UserID=t.TeacherID
				GROUP BY StudentID
			) AS t ON t.StudentID=c.StudentID
		WHERE 1 " . $cond;

$li->field_array = array(
    "Student",
    "IsSEN",
    "SENType",
    "SortTeacher"
); // not need to specify SENType as it's not sort

$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + $extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='20%' >" . $li->column($pos ++, $Lang['eGuidance']['StudentName']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['eGuidance']['sen']['SENStudent']) . "</th>\n";
$li->column_list .= "<th width='38%' >" . $Lang['eGuidance']['sen']['SENType'] . "</th>\n";
$pos ++;
$li->column_list .= "<th width='20%' >" . $li->column($pos ++, $Lang['eGuidance']['TeacherName']) . "</th>\n";
$li->column_list .= $checkbox_col;

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters
// Acadermic Year Filter
// $yearFilter = $junior_mck ? '' : getSelectAcademicYear("AcademicYearID", 'onChange="this.form.submit();"', 1, 0, $AcademicYearID);

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' onChange='this.form.submit();'", $ClassName, "", $Lang['eGuidance']['AllClass'], $AcademicYearID);

// IsSEN Filter
$isSENList = array();
$isSENList[] = array(
    '1',
    $Lang['eGuidance']['sen']['Confirm']
);
$isSENList[] = array(
    '0',
    $Lang['eGuidance']['sen']['NotConfirm']
);
$isSENList[] = array(
    '2',
    $Lang['eGuidance']['sen']['Quit']
);

$isSENFilter = getSelectByArray($isSENList, "name='IsSEN' onChange='this.form.submit();'", $IsSEN, 0, 0, $Lang['eGuidance']['sen']['AllCase']);

// SEN Case Type filter
$libguidance_ui = new libguidance_ui();
$LogicalType = $LogicalType ? $LogicalType : 'and'; 
$senCaseTypeFilter = $libguidance_ui->getAllSENCaseTypeFilter($LogicalType, $SenCaseTypeFilterAry, $SenCaseSubTypeFilterAry);

// ############ end Filters

$sen_case_type = $libguidance->getGuidanceSettingItem('SENCase', 'SENType');
$multiple_value_column = array();
$multiple_value_column['SENType'] = $sen_case_type;
$sentype_subitem['RD']['RDC'] = $Lang['eGuidance']['sen']['RD']['Chi'];
$sentype_subitem['RD']['RDE'] = $Lang['eGuidance']['sen']['RD']['Eng'];

?>

<script>
$(document).ready(function(){
	$('#SenCaseTypeFilterLink').click(function(){
		$('#search_option').css('display','block');
	});

	$("#searchBtn_tb").click(function(){
		$('#form1').submit();
	});

	$('#cancelBtn_tb').click(function(){
		$('#search_option').css('display','none');
	});
	
	$('#TB_closeWindowButton').click(function(){
		$('#search_option').css('display','none');
	});
	
});
</script>

<form name="form1" id="form1" method="POST" action="case_index.php">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>
 	
	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="table-action-bar">
				<td valign="bottom">
					<div class="table_filter">
					<?=$classFilter?>
					<?=$isSENFilter?>
					<div class="selectbox_group selectbox_group_filter" style="margin-top:0px; margin-left:5px; float:right">
						<a href="javascript:void(0);" id="SenCaseTypeFilterLink"><?php echo $Lang['eGuidance']['sen']['SENType'];?></a>
					</div>
					
                 	<div id="search_option" class="selectbox_layer selectbox_group_layer" style="width: 400px; margin-left:208px; visibility: visible; display: none;">
                 		<div id="TB_title" style="float: right;"><a href="#" id="TB_closeWindowButton">X</a></div>
                		<div id="TB_ajaxContent" style="width:400px;height:300px; overflow:auto;">
                			<?php echo $senCaseTypeFilter;?>
                		</div>
                	</div>
					
				</div>
				</td>
				<td valign="bottom">
					<div class="common_table_tool">
					<?=$manage_record_bar?>
				</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">

 				<?=''//$li->displayFormat_with_multiple_values_in_a_column($width="", $othercss1="", $othercss_bottom="", $multiple_value_column, $splitter="^~", $otherSplitter="^:", false, $sentype_subitem)?>
 				<?php echo $li->display();?>
 				
			</td>
			</tr>
		</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" /> 
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>