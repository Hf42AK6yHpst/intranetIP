<?php
/*
 * modifying:
 *
 * Log
 *
 * 2020-05-08 [Cameron]
 * - add SENTypeConfirmDate [case #F152883]  
 *
 * 2020-03-02 [Cameron]
 * - change SENType format to {SENTypeCode}^~{SENTypeCode}^:{SubTypeCode}^#{SubTypeCode}^@{SENTypeText} so that it can store SENTypeText no matter IsWithText is true or not [case #D178788]
 * 
 * 2018-04-13 [Cameron]
 * - Case type is retrieved from setup
 *
 * 2018-04-10 [Cameron]
 * - add ConfirmDate and NumberOfReport fields [case #M130681]
 *
 * 2017-08-03 [Cameron]
 * - handle submitAndNotify (send 'update' notification to colleague)
 * - add IsConfidential and ConfidentialViewerID
 *
 * 2017-07-24 [Cameron]
 * - add Arrangement to INTRANET_GUIDANCE_SEN_CASE_SUPPORT
 * - add field: Tier
 *
 * 2017-07-20 [Cameron]
 * - add SEN Type: RD, MH, break ADHD to AD and HD. MH can specify what kind of mental health, RD contains sub-items
 *
 * 2017-05-19 [Cameron]
 * - apply standardizeFormPostValue() to text field
 *
 * 2017-03-02 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['SEN'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ldb = new libdb();
$result = array();

$intranetSENItems = $libguidance->getIntranetSENItems();
$intranetSENItems = BuildMultiKeyAssoc($intranetSENItems, 'Code', 'CodeID', 1);

$rs_sencase = $libguidance->getSENCase($StudentID);
if (count($rs_sencase) == 1) {
    $returnMsgKey = 'AddUnsuccess';
} else {
    foreach ((array) $SENType as $k => $v) {
        $SENType[$k] = $v;
        $SENTypeConfirmDate[$v] = ${"SENTypeConfirmDate_$v"};
        if (is_array($_POST[$v])) {
            $SENType[$k] .= "^:" . implode("^#", (array) $_POST[$v]);
        }
        
        if (! empty($_POST["SENType$v"])) {
            $SENType[$k] .= "^@" . $_POST["SENType$v"];
        }
    }
    
    $senType = implode("^~", (array) $SENType);
    
    $dataAry['StudentID'] = $StudentID;
    $dataAry['IsSEN'] = $IsSEN;
    $dataAry['SENType'] = standardizeFormPostValue($senType);
    $dataAry['Remark'] = standardizeFormPostValue($Remark);
    $dataAry['IsTier1'] = $IsTier1;
    $dataAry['IsTier2'] = $IsTier2;
    $dataAry['IsTier3'] = $IsTier3;
    $dataAry['Tier1Remark'] = standardizeFormPostValue($Tier1Remark);
    $dataAry['Tier2Remark'] = standardizeFormPostValue($Tier2Remark);
    $dataAry['Tier3Remark'] = standardizeFormPostValue($Tier3Remark);
    $dataAry['IsConfidential'] = $IsConfidential;
    $dataAry['ConfirmDate'] = standardizeFormPostValue($ConfirmDate);
    $dataAry['NumberOfReport'] = standardizeFormPostValue($NumberOfReport);
    
    if ($IsConfidential && ! in_array($_SESSION['UserID'], (array) $ConfidentialViewerID)) {
        $ConfidentialViewerID[] = $_SESSION['UserID']; // User who add the record are granted permission as default
    }
    $dataAry['ConfidentialViewerID'] = implode(",", (array) $ConfidentialViewerID);
    
    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
    $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE', $dataAry, array(), false, true);
    
    $ldb->Start_Trans();
    // # step 1: add to sen case
    $res = $ldb->db_db_query($sql);
    $result[] = $res;
    if ($res) {
        // # step 2: add sen case teacher
        if (count($TeacherID)) {
            // # step 2: add transfer teacher
            foreach ((array) $TeacherID as $id) {
                unset($dataAry);
                $dataAry['StudentID'] = $StudentID;
                $dataAry['TeacherID'] = $id;
                $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE_TEACHER', $dataAry, array(), false, false, false); // no DateModified field
                $result[] = $ldb->db_db_query($sql);
            }
        } else {
            $result[] = false; // no TeacherID, roll back
        }
        
        // # step 3: add sen case adjustment
        foreach ((array) $ItemID as $k => $adjItem) {
            foreach ((array) $adjItem as $itemID) {
                unset($dataAry);
                $dataAry['StudentID'] = $StudentID;
                $dataAry['ItemID'] = $itemID;
                $dataAry['Arrangement'] = standardizeFormPostValue($Arrangement[$k][$itemID]);
                $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT', $dataAry, array(), false, false, false); // no DateModified field
                $result[] = $ldb->db_db_query($sql);
            }
        }
        
        // # step 4: add sen case support
        foreach ((array) $ServiceItemID as $k => $serviceItem) {
            foreach ((array) $serviceItem as $serviceID) {
                unset($dataAry);
                $dataAry['StudentID'] = $StudentID;
                $dataAry['ServiceID'] = $serviceID;
                $dataAry['Arrangement'] = standardizeFormPostValue($ServiceArrangement[$k][$serviceID]);
                $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE_SUPPORT', $dataAry, array(), false, false, false); // no DateModified field
                $result[] = $ldb->db_db_query($sql);
            }
        }
        
        // # step 5: update INTRANET_USER.SpecialEducationNeeds
        if (! $junior_mck) { // IP only
            $sen = $IsSEN ? 'Y' : 'N';
            $sql = "UPDATE INTRANET_USER SET SpecialEducationNeeds='" . $sen . "' WHERE UserID='" . $StudentID . "'";
            $result[] = $ldb->db_db_query($sql);
        }
        // # step 6: update INTRANET_USER.SEN
        $sen_item_id = array();
        foreach ((array) $SENType as $k) {
            $sen_item_id[] = $intranetSENItems[$k];
        }
        if (count($sen_item_id)) {
            $sql = "UPDATE INTRANET_USER SET SEN='" . implode(',', (array) $sen_item_id) . "' WHERE UserID='" . $StudentID . "'";
            $result[] = $ldb->db_db_query($sql);
        }
        
        // # step 7: add attachment
        if (count($FileID) > 0) {
            $fileID = implode("','", $FileID);
            $sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='" . $StudentID . "' WHERE FileID IN ('" . $fileID . "')";
            $result[] = $ldb->db_db_query($sql);
        }
        
        // # step 8: add SENTypeConfirmDate
        foreach((array)$SENTypeConfirmDate as $_SENType=>$_SENTypeConfirmDate) {
            unset($dataAry);
            $dataAry['StudentID'] = $StudentID;
            $dataAry['SENType'] = $_SENType;
            $dataAry['ConfirmDate'] = $_SENTypeConfirmDate;
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SENTYPE_CONFIRM_DATE', $dataAry, array(), false, false, true);
            $result[] = $ldb->db_db_query($sql);
        }
        
    }
    
    if (! in_array(false, $result)) {
        $ldb->Commit_Trans();
        
        if ($submitMode == 'submitAndNotify') {
            $hidNotifierID = explode(',', $hidNotifierID);
            $notifyResult = $libguidance->sendNotifyToColleague($StudentID, $recordType = 'SENCaseID', $hidNotifierID);
            $returnMsgKey = $notifyResult ? 'AddAndNotifySuccess' : 'AddSuccessNotifyFail';
        } else {
            $returnMsgKey = 'AddSuccess';
        }
    } else {
        $ldb->RollBack_Trans();
        $returnMsgKey = 'AddUnsuccess';
    }
}

header("location: case_index.php?returnMsgKey=" . $returnMsgKey);

intranet_closedb();

?>


