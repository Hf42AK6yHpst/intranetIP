<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-05-19 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-02-28 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SEN'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$serviceType = $ServiceType;
if ($serviceType == 'Other') {
	$serviceType .= "^:".$ServiceTypeOther;
}
$dataAry['ServiceType'] = standardizeFormPostValue($serviceType);	
$dataAry['StartDate'] = $StartDate;
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_SERVICE',$dataAry,array('ServiceID'=>$ServiceID),false);

$ldb->Start_Trans();

## step 1: update sen service
$result[] = $ldb->db_db_query($sql);


## step 2: get sen service student that's not in updated student list
$rs_student = $libguidance->getSENServiceStudentID($ServiceID,$StudentID);	// not in list student
if (count($rs_student)) {
	
	## step 3: update sen service student that's not in updated student list
	$sql = "DELETE FROM INTRANET_GUIDANCE_SEN_SERVICE_STUDENT WHERE ServiceID='".$ServiceID."' AND StudentID IN ('".implode("','",(array)$rs_student)."')";
	$result[] = $ldb->db_db_query($sql);
	
	## step 4: delete sen service student activity result that's not in updated student list
	$sql = "SELECT 
					r.ActivityID, r.StudentID 
			FROM 
					INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT r
			INNER JOIN 
					INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY n ON n.ActivityID=r.ActivityID
			WHERE 	
					n.ClassID='".$ClassID."'
			AND 	StudentID IN ('".implode("','",(array)$rs_student)."')";
			
	$rs_activity_result = $ldb->returnResultSet($sql);
	if (count($rs_activity_result)) {
		foreach((array)$rs_activity_result as $r) {
			$sql = "DELETE FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT WHERE ActivityID='".$r['ActivityID']."' AND StudentID='".$r['StudentID']."'";
			$result[] = $ldb->db_db_query($sql);
		}
	}
}

$currentStudentList = $libguidance->getSENServiceStudent($ServiceID);
$currentStudentList = BuildMultiKeyAssoc($currentStudentList, 'UserID',array('UserID'),1);

$activityAry = $libguidance->getSENServiceActivity($ServiceID);

## step 5: update sen service student list
foreach((array)$StudentID as $id) {
	if (!in_array($id,$currentStudentList)) {	// add new ClassStudent record
		unset($dataAry);
		$dataAry['ServiceID'] = $ServiceID;
		$dataAry['StudentID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_STUDENT',$dataAry,array(),false,true,false);	// insert ignore
		$result[] = $ldb->db_db_query($sql);

		## step 6:
		## case when add new student to sen service, but there's already activity created
 		## add empty result for the student		
		foreach((array)$activityAry as $ls) {
			unset($dataAry);
			$dataAry['ActivityID'] = $ls['ActivityID'];
			$dataAry['StudentID'] = $id;
			$dataAry['Result'] = '';
			$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT',$dataAry,array(),false,false,false);
			$result[] = $ldb->db_db_query($sql);
		}
	}
}


## step 7: get sen service teacher that's not in updated teacher list
$rs_teacher = $libguidance->getSENServiceTeacherID($ServiceID,$TeacherID);
if (count($rs_teacher)) {
	## step 8: delete sen service teacher that's not in updated teacher list
	$sql = "DELETE FROM INTRANET_GUIDANCE_SEN_SERVICE_TEACHER WHERE ServiceID='".$ServiceID."' AND TeacherID IN ('".implode("','",$rs_teacher)."')";
	$result[] = $ldb->db_db_query($sql);
}

## step 9: update sen service teacher
foreach((array)$TeacherID as $id) {
	unset($dataAry);
	$dataAry['ServiceID'] = $ServiceID;
	$dataAry['TeacherID'] = $id;
	$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_TEACHER',$dataAry,array(),false,true,false);	// insert ignore
	$result[] = $ldb->db_db_query($sql);
}

## step 10: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ServiceID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


