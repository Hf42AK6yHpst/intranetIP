<?
/*
 * 	Log
 *
 *	2017-07-18 [Cameron]
 *		- support choosing teaching / non-teaching staff
 *
 * 	2017-06-29 [Cameron]
 * 		- add label for radio button item so that clicking lable is the same as clicking the radio button
 * 		- add loadingImg
 *  
 * 	2017-05-19 [Cameron]
 * 		fix bug: disable button after submit to avoid adding duplicate record
 */
?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	if ($("input:radio[name='ServiceType']:checked").length == 0) {
		alert('<?=$Lang['eGuidance']['sen_service']['Warning']['SelectServiceType']?>');
		$("input:radio[name='ServiceType']:first").focus();
		return false;
	}
	
	if ($('#ServiceType_Other').attr('checked') && $('#ServiceTypeOther').val() == '') {
		alert('<?=$Lang['eGuidance']['sen_service']['Warning']['InputServiceTypeName']?>');
		$('#ServiceTypeOther').focus();
		return false;
	}
	
 	if (!check_date(obj.StartDate,"<?php echo $i_invalid_date; ?>")) {
 		return false;
 	}
 	
	var studentObj = document.getElementById('StudentID');
	if(studentObj.length==0) {    
	    alert('<?=$Lang['eGuidance']['Warning']['SelectStudent']?>');
	    return false;
 	}
 	
	var teacherObj = document.getElementById('TeacherID');
	if(teacherObj.length==0) {    
	    alert('<?=$Lang['eGuidance']['Warning']['SelectTeacher']?>');
	    return false;
 	}
 	
	return true;
}

$(document).ready(function(){
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	
	$('#ClassName').change(function(){
		$('#StudentID').attr('checked',true);
		isLoading = true;
		$('#StudentNameSpan').html(loadingImg);
		var excludeStudentID = [];
		var i=0;
		$('#StudentID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeStudentID[i++] = $(this).val();
			}
		});

		if ($('#ClassName').val() == '') {
			$('#AvailableStudentID option').remove();
			isLoading = false;
			var blankOptionList = '<select name=AvailableStudentID[] id=AvailableStudentID size=10 multiple>';
			blankOptionList += '<option>';
			for (i=0;i<20;i++) {
				blankOptionList += '&nbsp;';
			}
			blankOptionList += '</option>';
			blankOptionList += '</select>';
			$('#StudentNameSpan').html(blankOptionList);			
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '../ajax/ajax.php',
				data : {
					'action': 'getStudentNameByClassWithFilter',
					'ClassName': $('#ClassName').val(),
					'ExcludeStudentID[]':excludeStudentID
				},		  
				success: update_student_list,
				error: show_ajax_error
			});
		}		
	});
	
	
	$("input:radio[name='ServiceType']").change(function(){
		if ($('#ServiceType_Other').attr('checked')) {
			$('#ServiceTypeOther').css("display","");
		}
		else {
			$('#ServiceTypeOther').css("display","none");
		}
	});


	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		$('#StudentID option').attr('selected',true);
		$('#TeacherID option').attr('selected',true);

		if (checkForm(document.form1)) {
			
<? if ($form_action == "edit_service_update.php"):?>
        $.ajax({
        	dataType: "json", 
            type: 'post',
            url: '../ajax/ajax.php?action=checkBeforeUpdateSENService', 
            data: $('#form1').serialize(),
            async: false,            
	        success: function(ajaxReturn) {
	        	if (ajaxReturn != null && ajaxReturn.success){
	        		if (ajaxReturn.html == '1') {
	        			var confirm = window.confirm('<?=$Lang['eGuidance']['sen_service']['Warning']['DeleteStudentWithActivity']?>');
	        			if (confirm) {
	        				$('#form1').submit();
	        			}
	        			else {
	        				$('input.actionBtn').attr('disabled', '');
	        			}
	        		}
	        		else {
	        			$('#form1').submit();
	        		}
	        	}
	        	else {
	        		$('input.actionBtn').attr('disabled', '');
	        	}
	        },
			error: show_ajax_error
        });
<? else:?>
        $('#form1').submit();
<? endif;?>  
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
				      
	});
		
});

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

</script>
<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['sen_service']['ServiceType']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext">
								<?
									$sen_service_type = $libguidance->getGuidanceSettingItem('SENService','ServiceType');
									$x = '';
									foreach((array)$sen_service_type as $k=>$v) {
										if ($k == 'Other') {
											$x .= '<input type="radio" name="ServiceType" id="ServiceType_'.$k.'" value="'.$k.'"'. (!empty($rs_senservice['ServiceType']) && substr($rs_senservice['ServiceType'],0,5)==$k?'checked':'').'>';
											$x .= '<label for="ServiceType_'.$k.'">'.$v.'</label>';
										}
										else {
											$x .= '<input type="radio" name="ServiceType" id="ServiceType_'.$k.'" value="'.$k.'"'. ($rs_senservice['ServiceType']==$k?'checked':'').'>';
											$x .= '<label for="ServiceType_'.$k.'">'.$v.'</label><br>';
										}										  
									}
									echo $x;
								?>
									<input type="text" name="ServiceTypeOther" id="ServiceTypeOther" value="<?=intranet_htmlspecialchars($serviceTypeOther)?>" style="width:60%; display:<?=($showOther?'':'none')?>">
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("StartDate",(empty($rs_senservice['StartDate'])?date('Y-m-d'):$rs_senservice['StartDate']))?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$libguidance_ui->getStudentSelection($rs_senservice['Student'])?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$libguidance_ui->getTeacherSelection($rs_senservice['Teacher'])?>
								</td>
							</tr>
							
							<?=$libguidance_ui->getAttachmentLayout('SENService',$serviceID)?>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="ServiceID" name="ServiceID" value="<?=$serviceID?>">
</form>

<?=$libguidance_ui->getAttachmentUploadForm('SENService',$serviceID)?>