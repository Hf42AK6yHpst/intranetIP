<?php
/*
 * modifying:
 *
 * Log
 *
 * 2020-05-07 [Cameron]
 * - retrieve SENTypeConfirmDate [case #F152883]
 * 
 * 2020-03-02 [Cameron]
 * - retrieve $senItemAry and $senItemTextAry according to the revised format [case #D178788]
 * 
 * 2018-05-25 [Cameron]
 * - retrieve student info and disable student selection for edit [case #M138226]
 *
 * 2018-04-11 [Cameron]
 * - retrieve STRN, Gender and DateOfBirth of a student [case #M130681]
 *
 * 2017-08-03 [Cameron]
 * - add IsConfidential and ConfidentialViewerID
 * - don't allow to edit if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-24 [Cameron]
 * - add category to support service
 *
 * 2017-07-20 [Cameron]
 * - fix bug: should redirect to listview page if StudentID is empty
 * - add SEN Type: RD, MH, break ADHD to AD and HD. MH can specify what kind of mental health, RD contains sub-items
 *
 * 2017-03-02 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['SEN'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (is_array($StudentID)) {
    $studentID = (count($StudentID) == 1) ? $StudentID[0] : '';
} else {
    $studentID = $StudentID;
}

if (! $studentID) {
    header("location: case_index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();

// $classSelection = '';
// $studentSelection = getSelectByArray(array(), "name='StudentID' id='StudentID'");

$nameField = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? 'ChineseName' : 'EnglishName';
// # get adjustment type
$rs_sen_adjust_type = $libguidance->getSENAdjustType();
$rs_sen_adjust_type = BuildMultiKeyAssoc($rs_sen_adjust_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment item
$rs_sen_adjust_item = $libguidance->getSENAdjustItem();
$rs_sen_adjust_item = BuildMultiKeyAssoc($rs_sen_adjust_item, array(
    'TypeID',
    'ItemID'
), array(
    $nameField,
    'WithText'
));

// # get study service type
$rs_sen_study_service_type = $libguidance->getServiceType('', 'Study');
$rs_sen_study_service_type = BuildMultiKeyAssoc($rs_sen_study_service_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment support study service
$rs_sen_support_study = $libguidance->getSENSupportService('', '', 'Study');
$rs_sen_support_study = BuildMultiKeyAssoc($rs_sen_support_study, array(
    'TypeID',
    'ServiceID'
), array(
    $nameField,
    'WithText'
));

// # get other service type
$rs_sen_other_service_type = $libguidance->getServiceType('', 'Other');
$rs_sen_other_service_type = BuildMultiKeyAssoc($rs_sen_other_service_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment support other service
$rs_sen_support_other = $libguidance->getSENSupportService('', '', 'Other');
$rs_sen_support_other = BuildMultiKeyAssoc($rs_sen_support_other, array(
    'TypeID',
    'ServiceID'
), array(
    $nameField,
    'WithText'
));

$rs_sencase = $libguidance->getSENCase($studentID);
$senItemAry = array(); // items under SEN type: can be array or string , e.g. array(RD => array(RDC, RDE), MH =>"memtal health prob")
$senItemTextAry = array();
$rsSENTypeConfirmDate = $libguidance->getSENTypeConfirmDate($studentID);

if (count($rs_sencase) == 1) {
    $rs_sencase = $rs_sencase[0];
    $senTypeAry = explode('^~', $rs_sencase['SENType']);
    
    foreach ((array) $senTypeAry as $k => $v) {
        if (strpos($v, '^:') !== false) {
            list ($senType, $senItemStr) = explode('^:', $v);
            $senTypeAry[$k] = $senType;
            $pos = strpos($senItemStr, '^@');
            $senItems = $senItemStr;
            if ($pos !== false) {
                $senItems = substr($senItemStr, 0, $pos);
                $senItemTextAry[$senType] = substr($senItemStr,$pos+2);
            }            
            if (strpos($senItems, '^#') !== false) {
                $senItemAry[$senType] = explode('^#', $senItems);
            } else {
                $senItemAry[$senType] = $senItems;
            }
        }
        else if (strpos($v, '^@') !== false) {
            list ($senType, $itemText) = explode('^@', $v);
            $senTypeAry[$k] = $senType;
            $senItemTextAry[$senType] = $itemText;
        }
    }

    $studentInfo = $libguidance->getStudentInfo($studentID);
    if (count($studentInfo)) {
        $rs_sencase['STRN'] = $studentInfo['STRN'] ? $studentInfo['STRN'] : '-';
        $gender = $studentInfo['Gender'];
        if ($gender == 'M') {
            $rs_sencase['Gender'] = $Lang['eGuidance']['personal']['GenderMale'];
        } elseif ($gender == 'F') {
            $rs_sencase['Gender'] = $Lang['eGuidance']['personal']['GenderFemale'];
        } else {
            $rs_sencase['Gender'] = '-';
        }
        $rs_sencase['DateOfBirth'] = ($studentInfo['DateOfBirth'] == '0000-00-00' || $studentInfo['DateOfBirth'] == '') ? '-' : $studentInfo['DateOfBirth'];
    }
    
    $rs_sencase['Teacher'] = $libguidance->getSENCaseTeacher($studentID);
    
    if ($rs_sencase['IsConfidential']) {
        $confidentialViewerID = $rs_sencase['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: case_index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $rs_sencase['ConfidentialViewerID'] = $libguidance->getTeacher('-1', $cond);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($studentID);
    if (! empty($rs_class_name)) {
        // $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ", $rs_class_name);
        // $excludeUserIdAry = $libguidance->getSENCaseByClass($rs_class_name, $studentID);
        // $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, '', $excludeUserIdAry);
        // $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $studentID);
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_sencase['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    }
    else {
        $studentInfo = $libguidance->getStudent($rs_sencase['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
    
    $rs_sencase_adjustment = $libguidance->getSENCaseAdjustment($studentID);
    $rs_sencase_adjustment = BuildMultiKeyAssoc($rs_sencase_adjustment, 'ItemID', array(
        'ItemID',
        'Arrangement'
    ));
    
    $rs_sencase_support_study = $libguidance->getSENCaseSupport($studentID, '', 'Study');
    $rs_sencase_support_study = BuildMultiKeyAssoc($rs_sencase_support_study, 'ServiceID', array(
        'ServiceID',
        'Arrangement'
    ));
    
    $rs_sencase_support_other = $libguidance->getSENCaseSupport($studentID, '', 'Other');
    $rs_sencase_support_other = BuildMultiKeyAssoc($rs_sencase_support_other, 'ServiceID', array(
        'ServiceID',
        'Arrangement'
    ));
} else {
    header("location: case_index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSEN";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getSENTabs("Case");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    'SEN' . $Lang['eGuidance']['sen']['Tab']['Case'],
    "case_index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "edit_case_update.php";
include ("sen_case.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


