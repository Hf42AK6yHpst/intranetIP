<?php
/*
 * modifying:
 *
 * Log
 *
 * 2020-05-08 [Cameron]
 * - add SENTypeConfirmDate [case #F152883]  
 * 
 * 2020-03-02 [Cameron]
 * - change SENType format to {SENTypeCode}^~{SENTypeCode}^:{SubTypeCode}^#{SubTypeCode}^@{SENTypeText} so that it can store SENTypeText no matter IsWithText is true or not [case #D178788]
 *
 * 2018-04-13 [Cameron]
 * - Case type is retrieved from setup
 *
 * 2018-04-10 [Cameron]
 * - add ConfirmDate and NumberOfReport fields [case #M130681]
 *
 * 2017-08-03 [Cameron]
 * - handle submitAndNotify (send 'update' notification to colleague)
 * - add IsConfidential and ConfidentialViewerID
 *
 * 2017-07-24 [Cameron]
 * - add Arrangement to INTRANET_GUIDANCE_SEN_CASE_SUPPORT
 * - add field: Tier
 *
 * 2017-07-20 [Cameron]
 * - add SEN Type: RD, MH, break ADHD to AD and HD. MH can specify what kind of mental health, RD contains sub-items
 *
 * 2017-05-19 [Cameron]
 * - apply standardizeFormPostValue() to text field
 *
 * 2017-03-02 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['SEN'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ldb = new libdb();
$result = array();
$dataAry = array();
$condAry = array();

$intranetSENItems = $libguidance->getIntranetSENItems();
$intranetSENItems = BuildMultiKeyAssoc($intranetSENItems, 'Code', 'CodeID', 1);

if ($OrgStudentID && $StudentID) {
    
    foreach ((array) $SENType as $k => $v) {
        $SENType[$k] = $v;
        $SENTypeConfirmDate[$v] = ${"SENTypeConfirmDate_$v"};
        if (is_array($_POST[$v])) {
            $SENType[$k] .= "^:" . implode("^#", (array) $_POST[$v]);
        }
        
        if (! empty($_POST["SENType$v"])) {
            $SENType[$k] .= "^@" . $_POST["SENType$v"];
        }
    }
    $senType = implode("^~", (array) $SENType);
    
    if ($StudentID != $OrgStudentID) {
        $dataAry['StudentID'] = $StudentID;
    }
    $dataAry['IsSEN'] = $IsSEN;
    $dataAry['SENType'] = standardizeFormPostValue($senType);
    $dataAry['Remark'] = standardizeFormPostValue($Remark);
    $dataAry['IsTier1'] = $IsTier1;
    $dataAry['IsTier2'] = $IsTier2;
    $dataAry['IsTier3'] = $IsTier3;
    $dataAry['Tier1Remark'] = standardizeFormPostValue($Tier1Remark);
    $dataAry['Tier2Remark'] = standardizeFormPostValue($Tier2Remark);
    $dataAry['Tier3Remark'] = standardizeFormPostValue($Tier3Remark);
    $dataAry['IsConfidential'] = $IsConfidential;
    $dataAry['ConfirmDate'] = standardizeFormPostValue($ConfirmDate);
    $dataAry['NumberOfReport'] = standardizeFormPostValue($NumberOfReport);
    
    if ($IsConfidential && ! in_array($_SESSION['UserID'], (array) $ConfidentialViewerID)) {
        $ConfidentialViewerID[] = $_SESSION['UserID']; // User who add the record are granted permission as default
    }
    $dataAry['ConfidentialViewerID'] = implode(",", (array) $ConfidentialViewerID);
    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
    $condAry['StudentID'] = $OrgStudentID;
    $sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_CASE', $dataAry, $condAry, false, true);
    
    $ldb->Start_Trans();
    // # step 1: update to sen case
    $result['UpdateSenCase'] = $ldb->db_db_query($sql);
    
    $itemList = array();
    foreach ((array) $ItemID as $k => $adjItem) {
        foreach ((array) $adjItem as $itemID) {
            $itemList[] = $itemID;
        }
    }
    
    // # step 2: delete removed sen case teachers
    if (count($TeacherID)) {
        $cond = " AND TeacherID NOT IN ('" . implode("','", (array) $TeacherID) . "')";
    } else {
        $cond = '';
    }
    $sql = "DELETE FROM INTRANET_GUIDANCE_SEN_CASE_TEACHER WHERE StudentID='" . $OrgStudentID . "'" . $cond;
    $result['DeleteSenCaseTeacher'] = $ldb->db_db_query($sql);
    
    // # step 3: delete adjustment not in selected list
    if (count($itemList)) {
        $cond = " AND ItemID NOT IN ('" . implode("','", $itemList) . "')";
    } else {
        $cond = '';
    }
    $sql = "DELETE FROM INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT WHERE StudentID='" . $OrgStudentID . "'" . $cond;
    $result['DeleteAdjustment'] = $ldb->db_db_query($sql);
    
    $serviceList = array_merge((array) $ServiceIDS, (array) $ServiceIDO);
    // # step 4: delete sen support service not in selected list
    if (count($serviceList)) {
        $cond = " AND ServiceID NOT IN ('" . implode("','", (array) $serviceList) . "')";
    } else {
        $cond = '';
    }
    $sql = "DELETE FROM INTRANET_GUIDANCE_SEN_CASE_SUPPORT WHERE StudentID='" . $OrgStudentID . "'" . $cond;
    $result['DeleteSenCaseSupport'] = $ldb->db_db_query($sql);
    
    // Student changed
    if ($StudentID != $OrgStudentID) {
        // # step 5: update sen case teacher
        unset($dataAry);
        unset($condAry);
        $dataAry['StudentID'] = $StudentID;
        $condAry['StudentID'] = $OrgStudentID;
        $sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_CASE_TEACHER', $dataAry, $condAry, false, false);
        $result['UpdateSenCaseTeacher'] = $ldb->db_db_query($sql);
    }
    
    // # step 6: add sen case teacher
    foreach ((array) $TeacherID as $id) {
        unset($dataAry);
        $dataAry['StudentID'] = $StudentID;
        $dataAry['TeacherID'] = $id;
        $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE_TEACHER', $dataAry, array(), false, true, false); // insert ignore
        $result['AddSenCaseTeacher_'.$id] = $ldb->db_db_query($sql);
    }
    
    $rs_sencase_adjustment = $libguidance->getSENCaseAdjustment($OrgStudentID);
    $rs_sencase_adjustment = BuildMultiKeyAssoc($rs_sencase_adjustment, 'ItemID', array(
        'ItemID'
    ), 1);
    
    // # step 7: add / update sen case adjustment
    foreach ((array) $ItemID as $k => $adjItem) {
        foreach ((array) $adjItem as $itemID) {
            unset($dataAry);
            unset($condAry);
            if (in_array($itemID, (array) $rs_sencase_adjustment)) { // update
                $dataAry['StudentID'] = $StudentID;
                $dataAry['Arrangement'] = standardizeFormPostValue($Arrangement[$k][$itemID]);
                $condAry['StudentID'] = $OrgStudentID;
                $condAry['ItemID'] = $itemID;
                $sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT', $dataAry, $condAry, false, false); // no DateModified field
                $result['UpdateAdjustment_'.$k.'_'.$itemID] = $ldb->db_db_query($sql);
            } else { // insert
                $dataAry['StudentID'] = $StudentID;
                $dataAry['ItemID'] = $itemID;
                $dataAry['Arrangement'] = standardizeFormPostValue($Arrangement[$k][$itemID]);
                $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE_ADJUSTMENT', $dataAry, array(), false, false, false); // no DateModified field
                $result['AddAdjustment_'.$k.'_'.$itemID] = $ldb->db_db_query($sql);
            }
        }
    }
    
    $rs_sencase_support = $libguidance->getSENCaseSupport($OrgStudentID);
    $rs_sencase_support = BuildMultiKeyAssoc($rs_sencase_support, 'ServiceID', array(
        'ServiceID'
    ), 1);
    
    // # step 8: add / update sen case support
    foreach ((array) $ServiceItemID as $k => $serviceItem) {
        foreach ((array) $serviceItem as $serviceID) {
            unset($dataAry);
            unset($condAry);
            if (in_array($serviceID, (array) $rs_sencase_support)) { // update
                $dataAry['StudentID'] = $StudentID;
                $dataAry['Arrangement'] = standardizeFormPostValue($ServiceArrangement[$k][$serviceID]);
                $condAry['StudentID'] = $OrgStudentID;
                $condAry['ServiceID'] = $serviceID;
                $sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_CASE_SUPPORT', $dataAry, $condAry, false, false); // no DateModified field
                $result['UpdateSupport_'.$k.'_'.$itemID] = $ldb->db_db_query($sql);
            } else { // insert
                $dataAry['StudentID'] = $StudentID;
                $dataAry['ServiceID'] = $serviceID;
                $dataAry['Arrangement'] = standardizeFormPostValue($ServiceArrangement[$k][$serviceID]);
                $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_CASE_SUPPORT', $dataAry, array(), false, false, false); // no DateModified field
                $result['AddSupport_'.$k.'_'.$itemID] = $ldb->db_db_query($sql);
            }
        }
    }
    
    // # step 9: update INTRANET_USER
    if (! $junior_mck) { // IP only
        $sen = $IsSEN ? 'Y' : 'N';
        $sql = "UPDATE INTRANET_USER SET SpecialEducationNeeds='" . $sen . "' WHERE UserID='" . $StudentID . "'";
        $result['UpdateIntranetUser_SpecialNeeds'] = $ldb->db_db_query($sql);
    }
    
    // # step 10: update INTRANET_USER.SEN
    $sen_item_id = array();
    foreach ((array) $SENType as $k) {
        $sen_item_id[] = $intranetSENItems[$k];
    }
    if (count($sen_item_id)) {
        $sql = "UPDATE INTRANET_USER SET SEN='" . implode(',', (array) $sen_item_id) . "' WHERE UserID='" . $StudentID . "'";
        $result['UpdateIntranetUser_SEN'] = $ldb->db_db_query($sql);
    }
    
    // # step 11: update attachment
    if (count($FileID) > 0) {
        $fileID = implode("','", $FileID);
        $sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='" . $StudentID . "' WHERE FileID IN ('" . $fileID . "') AND RecordID=0";
        $result['UpdateAttachment'] = $ldb->db_db_query($sql);
    }
    
    // # step 12: delete SENTypeConfirmDate
    $sql = "DELETE FROM INTRANET_GUIDANCE_SENTYPE_CONFIRM_DATE WHERE StudentID='".$StudentID."'";
    $result['DeleteSenTypeConfirmDate'] = $ldb->db_db_query($sql);
    
    // # step 13: add back SENTypeConfirmDate
    foreach((array)$SENTypeConfirmDate as $_SENType=>$_SENTypeConfirmDate) {
        unset($dataAry);
        $dataAry['StudentID'] = $StudentID;
        $dataAry['SENType'] = $_SENType;
        $dataAry['ConfirmDate'] = $_SENTypeConfirmDate;
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SENTYPE_CONFIRM_DATE', $dataAry, array(), false, false, true);
        $result['AddSenTypeConfirmDate_'.$_SENType] = $ldb->db_db_query($sql);
    }
    
} else { // no StudentID
    $result[] = false;
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    
    if ($submitMode == 'submitAndNotify') {
        $hidNotifierID = explode(',', $hidNotifierID);
        $notifyResult = $libguidance->sendNotifyToColleague($StudentID, $recordType = 'SENCaseID', $hidNotifierID);
        $returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
    } else {
        $returnMsgKey = 'UpdateSuccess';
    }
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}

header("location: case_index.php?returnMsgKey=" . $returnMsgKey);

intranet_closedb();

?>


