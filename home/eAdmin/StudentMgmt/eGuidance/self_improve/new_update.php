<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-08-17 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$plugin['eGuidance_module']['SelfImprove'] || (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SELFIMPROVE']))) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();
	
$dataAry['StudentID'] = $StudentID;
$dataAry['StartDate'] = $StartDate;
$dataAry['EndDate'] = $EndDate;
$dataAry['Result'] = $Result;
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['HisEndDate'] = $EndDate;
$dataAry['IsConfidential'] = $IsConfidential;
if ($IsConfidential && !in_array($_SESSION['UserID'], (array)$ConfidentialViewerID)) {
	$ConfidentialViewerID[] = $_SESSION['UserID'];				// User who add the record are granted permission as default 
}
$dataAry['ConfidentialViewerID'] = implode(",",(array)$ConfidentialViewerID);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SELFIMPROVE',$dataAry,array(),false);

$ldb->Start_Trans();
## step 1: add to self improve
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
	$ImproveID = $ldb->db_insert_id();
	
	## step 2: add teacher in charge
	foreach((array)$TeacherID as $id) {
		unset($dataAry);
		$dataAry['ImproveID'] = $ImproveID;
		$dataAry['TeacherID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SELFIMPROVE_TEACHER',$dataAry,array(),false,false,false);	// no DateModified field
		$result[] = $ldb->db_db_query($sql);
	}
	
	## step 3: add attachment				
	if (count($FileID) > 0) {
		$fileID = implode("','",$FileID);
		$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ImproveID."' WHERE FileID IN ('".$fileID."')";
		$result[] = $ldb->db_db_query($sql);
	}
	
}
else {
	$ImproveID = '';
}

if (!in_array(false,$result) && $ImproveID) {
	$ldb->Commit_Trans();
	
	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($ImproveID, $recordType='ImproveID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'AddAndNotifySuccess' : 'AddSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'AddSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'AddUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>
