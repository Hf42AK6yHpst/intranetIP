<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-08-17 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$plugin['eGuidance_module']['SelfImprove'] || (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SELFIMPROVE']))) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$dataAry['StartDate'] = $StartDate;
$dataAry['EndDate'] = $EndDate;
$dataAry['Result'] = $Result;
$dataAry['Remark'] = standardizeFormPostValue($Remark);

$rs_self_improve = $libguidance->getSelfImprove($ImproveID);
if (count($rs_self_improve) == 1) {
	$rs_self_improve = $rs_self_improve[0];
	$hisEndDate = $rs_self_improve['HisEndDate'];
	$hisEndDateAry = $hisEndDate ? explode(',',$hisEndDate) : array();
	if (!in_array($EndDate,$hisEndDateAry)) {
		$hisEndDateAry[] = $EndDate;
		sort($hisEndDateAry);
		$hisEndDate = implode(',',(array)$hisEndDateAry);
		$dataAry['HisEndDate'] = $hisEndDate; 
	} 
}

$dataAry['IsConfidential'] = $IsConfidential;
if ($IsConfidential && !in_array($_SESSION['UserID'], (array)$ConfidentialViewerID)) {
	$ConfidentialViewerID[] = $_SESSION['UserID'];				// User who add the record are granted permission as default 
}
$dataAry['ConfidentialViewerID'] = implode(",",(array)$ConfidentialViewerID);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SELFIMPROVE',$dataAry,array('ImproveID'=>$ImproveID),false);

$ldb->Start_Trans();
## step 1: update to self improve
$result[]  = $ldb->db_db_query($sql);

## step 2: update teacher in charge
if (count($TeacherID)) {
	$sql = "DELETE FROM INTRANET_GUIDANCE_SELFIMPROVE_TEACHER WHERE ImproveID='".$ImproveID."' AND TeacherID NOT IN ('".implode("','",(array)$TeacherID)."')";
	$result[] = $ldb->db_db_query($sql);
}

foreach((array)$TeacherID as $id) {
	unset($dataAry);
	$dataAry['ImproveID'] = $ImproveID;
	$dataAry['TeacherID'] = $id;
	$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SELFIMPROVE_TEACHER',$dataAry,array(),false,true,false);	// insert ignore
	$result[] = $ldb->db_db_query($sql);
}

## step 3: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ImproveID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();

	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($ImproveID, $recordType='ImproveID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'UpdateSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>
