<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-25 [Cameron]
 * - retrieve student info and disable student selection for edit [case #M138226]
 *
 * 2017-08-17 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $plugin['eGuidance_module']['SelfImprove'] || (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['SELFIMPROVE']))) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (is_array($ImproveID)) {
    $improveID = (count($ImproveID) == 1) ? $ImproveID[0] : '';
} else {
    $improveID = $ImproveID;
}

if (! $improveID) {
    header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(), "name='StudentID' id='StudentID'");

$rs_self_improve = $libguidance->getSelfImprove($improveID);

if (count($rs_self_improve) == 1) {
    $rs_self_improve = $rs_self_improve[0];
    $rs_self_improve['Teacher'] = $libguidance->getSelfImproveTeacher($improveID);
    
    if ($rs_self_improve['IsConfidential']) {
        $confidentialViewerID = $rs_self_improve['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $rs_self_improve['ConfidentialViewerID'] = $libguidance->getTeacher('-1', $cond);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($rs_self_improve['StudentID']);
    if (! empty($rs_class_name)) {
        // $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ",$rs_class_name);
        // $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name);
        // $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $rs_self_improve['StudentID']);
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_self_improve['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    } else {
        $studentInfo = $libguidance->getStudent($rs_self_improve['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
} else {
    header("location: index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSelfImprove";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array(
    $Lang['eGuidance']['menu']['Management']['SelfImprove']
);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['menu']['Management']['SelfImprove'],
    "index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include ("self_improve.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


