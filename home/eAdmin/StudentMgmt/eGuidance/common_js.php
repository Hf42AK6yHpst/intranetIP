<?
/*
 * 	modify by:
 * 
 * 	purpose: common js function for eGuidance
 * 
 * 	2017-07-18 Cameron - create this file
 * 
 */
 
 
?>

$(document).ready(function(){

	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	$('#TeacherType').change(function(){
		$('#TeacherID').attr('checked',true);
		isLoading = true;
		$('#TeacherSelectionSpan').html(loadingImg);
		var excludeTeacherID = [];
		var i = 0;
		$('#TeacherID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeTeacherID[i++] = $(this).val();
			}
		});

		if ($('#TeacherType').val() == '') {
			$('#AvailableTeacherID option').remove();
			isLoading = false;
			var blankOptionList = '<select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
			blankOptionList += '</select>';
			$('#TeacherSelectionSpan').html(blankOptionList);			
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
				data : {
					'action': 'getTeacherName',
					'TeacherType': $('#TeacherType').val(),
					'ExcludeTeacherID[]':excludeTeacherID
				},		  
				success: update_available_teacher_list,
				error: show_ajax_error
			});
		}		
	});
	
	$(':input[name="IsConfidential"]').change(function() {
		if ($('#IsConfidentialYes').is(':checked')) {
			$('#ConfidentialSection').css("display","");
			$('#ConfidentialIcon').css("display","");
			$('#TeacherID option').attr('selected',true);
			$('#ConfidentialViewerID option').attr('selected',true);
			if (($('#ConfidentialViewerID').val() == null) && ($('#TeacherID').val() != '')) {
				$('#TeacherID option').each(function() {
					if ($.trim($(this).val()) != '') {
						 $('#ConfidentialViewerID').append('<option value="'+$.trim($(this).val())+'">'+$(this).text()+'</option>');
					}
				});
			}
		}
		else {
			$('#ConfidentialSection').css("display","none");
			$('#ConfidentialIcon').css("display","none");
		}
	});

	$('#ConfidentialGroupCategory').change(function(){
		$('#ConfidentialViewerID').attr('checked',true);
		isLoading = true;
		$('#ConfidentialViewerSelectionSpan').html(loadingImg);
		var excludeConfidentialViewerID = [];
		var i = 0;
		$('#ConfidentialViewerID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeConfidentialViewerID[i++] = $(this).val();
			}
		});

		if ($('#ConfidentialGroupCategory').val() == '') {
			$('#AvailableConfidentialViewerID option').remove();
			$('#ConfidentialGroup option').remove();
			$('#ConfidentialGroup').css('display','none');
			isLoading = false;
			var blankOptionList = '<select name=AvailableConfidentialViewerID[] id=AvailableConfidentialViewerID style="min-width:200px; height:156px;" multiple>';
			blankOptionList += '</select>';
			$('#ConfidentialViewerSelectionSpan').html(blankOptionList);			
		}
		else if ($('#ConfidentialGroupCategory').val() == 'TeachingStaff' || $('#ConfidentialGroupCategory').val() == 'NonTeachingStaff') {
			$('#ConfidentialGroup option').remove();
			$('#ConfidentialGroup').css('display','none');
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
				data : {
					'action': 'getConfidentialViewerName',
					'ConfidentialGroupCategory': $('#ConfidentialGroupCategory').val(),
					'ExcludeConfidentialViewerID[]':excludeConfidentialViewerID
				},		  
				success: update_available_viewer_list,
				error: show_ajax_error
			});
		}
		else {			
 			$.ajax({
				dataType: "json",
				type: "POST",
				url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
				data : {
					'action': 'getConfidentialGroup',
					'ConfidentialGroupCategory': $('#ConfidentialGroupCategory').val(),
					'isLoading': isLoading
				},		  
				success: update_confidential_group_list,
				error: show_ajax_error
			});
		}		
	});
	
	
    $('#BtnAddAttachment').click(function(){
		if (navigator.userAgent.match(/msie/i)){
			$('#attachment').replaceWith($('#attachment').clone(true))
		    $('.attach_form').show();
		}
		else{
		    $('#attachment').click();
		}
		return false;
    });
	
	// upload attachment
    $('#attachment').change(function(){
		if ($('#attachment').val() != '') {
			if (navigator.userAgent.match(/msie/i)){
			    $('.attach_form').hide();
			}
			$(this).parent().submit();
			$(this).val('');	// empty it for next upload
		}
    });
    
	if ((navigator.userAgent.match(/Trident/i)) || (navigator.userAgent.match(/Edge/i))){
		var obj = document.getElementById('attachment'); 
	    obj.onchange = function() {
	    	if ($('#attachment').val() != '') {
				$(this).parent().submit();
				$(this).val('');	// empty it for next upload
	    	}
	    }
	}
	
});

function changeGroup(isLoading) {
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';	
	$('#ConfidentialViewerID').attr('checked',true);
	isLoading = true;
	$('#ConfidentialViewerSelectionSpan').html(loadingImg);
	var excludeConfidentialViewerID = [];
	var i = 0;
	$('#ConfidentialViewerID option').each(function() {
		if ($.trim($(this).val()) != '') {
			excludeConfidentialViewerID[i++] = $(this).val();
		}
	});

	if ($('#ConfidentialGroup').val() == '') {
		$('#AvailableConfidentialViewerID option').remove();
		isLoading = false;
		var blankOptionList = '<select name=AvailableConfidentialViewerID[] id=AvailableConfidentialViewerID style="min-width:200px; height:156px;" multiple>';
		blankOptionList += '</select>';
		$('#ConfidentialViewerSelectionSpan').html(blankOptionList);			
	}
	else {			
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
			data : {
				'action': 'getConfidentialGroupMember',
				'ConfidentialGroup': $('#ConfidentialGroup').val(),
				'ExcludeConfidentialViewerID[]': excludeConfidentialViewerID
			},		  
			success: update_available_viewer_list,
			error: show_ajax_error
		});
	}		
}

function update_available_teacher_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#TeacherSelectionSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

function update_available_viewer_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#ConfidentialViewerSelectionSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

function update_confidential_group_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		var blankOptionList = '<select name=AvailableConfidentialViewerID[] id=AvailableConfidentialViewerID style="min-width:200px; height:156px;" multiple>';
		blankOptionList += '</select>';
		$('#ConfidentialViewerSelectionSpan').html(blankOptionList);
		$('#ConfidentialGroup').replaceWith(ajaxReturn.html);
		isLoading = false;
	}
}

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}

function hideError() {
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function loadAttachment(attachment_layout, batchUploadTime){
	$('#current_attachment').show().html(attachment_layout);
	if (batchUploadTime != '') {
		$('#BatchUploadTime').val(batchUploadTime);
	}
}

function FileDeleteSubmit(fileID) {
	conf = confirm('<?=$Lang['eGuidance']['Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
	if (conf) {
		$.ajax({
			type: "POST",
			url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax_file_handling.php',
			data : {
				'action': 'removeAttachment',
				'FileID': fileID,
				'BatchUploadTime': $('#BatchUploadTime').val(),
				'Target': $('#Target').val(),
				'RecordID': $('#RecordID').val()
			},		  
			success: function(attachment_layout){
				loadAttachment(attachment_layout,'');
			},
			error: show_ajax_error
		});
	}
}

function ViewStudentInfo() {
	if ($('#StudentID').length && $('#StudentID').val() > 0) {
		load_dyn_size_thickbox_ip('<?=$Lang['eGuidance']['personal']['PersonalRecord']?>', 'onloadPersonalInfoThickBox("'+$('#StudentID').val()+'");', inlineID='', defaultHeight=450, defaultWidth=800);
	}
	else {
		alert('<?=$Lang['eGuidance']['Warning']['SelectStudent']?>');
	}
}

function onloadPersonalInfoThickBox(studentID) {
	$('div#TB_ajaxContent').load(
		"<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax_view_personal_info.php?StudentID=" + studentID,
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
