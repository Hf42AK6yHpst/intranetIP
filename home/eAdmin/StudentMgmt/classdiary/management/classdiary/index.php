<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$CurrentPageArr['ClassDiary'] = 1;
$CurrentPage['ClassDiary'] = 1;
$TAGS_OBJ[] = array($Lang['ClassDiary']['ClassDiary'],"",0);

$MODULE_OBJ = $libclassdiary_ui->GET_MODULE_OBJ_ARR();
$libclassdiary_ui->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $libclassdiary_ui->Get_Class_Diary_Index();

$libclassdiary_ui->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
function Get_Class_Diary_Table()
{
	var FromDate = $.trim($('input#FromDate').val());
	var ToDate = $.trim($('input#ToDate').val());
	var is_valid = true;
	
	if(!check_date_without_return_msg(document.getElementById('FromDate'))){
		$('#DateWarningLayer').html(jsLang['InvalidDateFormat']).show();
		$('input#FromDate').focus();
		is_valid = false;
	}else{
		$('#DateWarningLayer').html('').hide();
	}
	
	if(!check_date_without_return_msg(document.getElementById('ToDate'))){
		$('#DateWarningLayer').html(jsLang['InvalidDateFormat']).show();
		$('input#ToDate').focus();
		is_valid = false;
	}else{
		$('#DateWarningLayer').html('').hide();
	}
	
	if(FromDate > ToDate){
		$('#DateWarningLayer').html(jsLang['InvalidDateRange']).show();
		$('input#FromDate').focus();
		is_valid = false;
	}else{
		$('#DateWarningLayer').html('').hide();
	}
	
	if(is_valid){
		Block_Element('ClassDiaryTableDiv');
		$('div#ClassDiaryTableDiv').load(
			'ajax_load.php',
			{
				'task':'Get_Class_Diary_Table',
				'FromDate':FromDate,
				'ToDate':ToDate
			},
			function(data){
				UnBlock_Element('ClassDiaryTableDiv');
			}
		);
	}
}

$(document).ready(function(){
	Get_Class_Diary_Table();
});
</script>