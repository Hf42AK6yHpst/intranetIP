<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$CurrentPageArr['ClassDiary'] = 1;
$CurrentPage['ClassDiary'] = 1;
$TAGS_OBJ[] = array($Lang['ClassDiary']['ClassDiary'],"",0);

$MODULE_OBJ = $libclassdiary_ui->GET_MODULE_OBJ_ARR();
$libclassdiary_ui->LAYOUT_START(urldecode($_REQUEST['Msg']));

if(is_array($_REQUEST['DiaryID'])){
	$DiaryID = $_REQUEST['DiaryID'][0];
}else{
	$DiaryID = $_REQUEST['DiaryID'];
}

echo $libclassdiary_ui->Get_Class_Diary_Edit_Form($DiaryID);

$libclassdiary_ui->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
var MAX_NUMBER_OF_LESSON = <?=$libclassdiary->MAX_NUMBER_OF_LESSON?>;

function ResetClassStudents()
{
	for(var i=1;i<=MAX_NUMBER_OF_LESSON;i++){
		var StudentF = $('#StudentF'+i);
		var StudentE = $('#StudentE'+i);
		var StudentM = $('#StudentM'+i);
		StudentF.val('');
		StudentF.prev().html('');
		StudentE.val('');
		StudentE.prev().html('');
		StudentM.val('');
		StudentM.prev().html('');
	}
	
	var AbsentAMStudent = $('#AbsentAMStudent');
	var AbsentPMStudent = $('#AbsentPMStudent');
	var LateStudent = $('#LateStudent');
	var EarlyleaveStudent = $('#EarlyleaveStudent');
	AbsentAMStudent.val('');
	AbsentAMStudent.prev().html('');
	AbsentPMStudent.val('');
	AbsentPMStudent.prev().html('');
	LateStudent.val('');
	LateStudent.prev().html('');
	EarlyleaveStudent.val('');
	EarlyleaveStudent.prev().html('');
	
	ShowHideStudentPanel({},0,'');
	
	Get_Class_Subject_Selections();
}

function CheckSubmitClassDiary()
{
	Block_Element('EditForm');
	
	var is_valid = true;
	var DiaryID = $('#DiaryID').val();
	var is_edit = DiaryID != '';
	var ClassID = $.trim($('#ClassID').val());
	var DiaryDate = $.trim($('#DiaryDate').val());
	var LessonIDArray = [];
	var SubjectIDArray = [];
	var InstructionMediumPArray = [];
	var InstructionMediumEArray = [];
	var InstructionMediumCArray = [];
	var InstructionMediumBArray = [];
	var AssignmentArray = [];
	var SubmissionDateArray = [];
	var StudentFArray = [];
	var StudentEArray = [];
	var StudentMArray = [];
	var OthersArray = [];
	var AbsentAMStudent = $.trim($('#AbsentAMStudent').val());
	var AbsentPMStudent = $.trim($('#AbsentPMStudent').val());
	var LateStudent = $.trim($('#LateStudent').val());
	var EarlyleaveStudent = $.trim($('#EarlyleaveStudent').val());
	
	if(!is_edit){
		if(ClassID == ''){
			$('#ClassIDWarningDiv').show();
			is_valid = false;
		}else{
			$('#ClassIDWarningDiv').hide();
		}
		
		if(!check_date_without_return_msg(document.getElementById('DiaryDate'))){
			$('#DiaryDateWarningDiv').show();
			is_valid = false;
		}else{
			$('#DiaryDateWarningDiv').hide();
		}
	}
	
	for(var i=1;i<=MAX_NUMBER_OF_LESSON;i++){
		var LessonID = $('#LessonID'+i).val();
		var SubjectID = $('#SubjectID'+i).val();
		var Assignment = $.trim($('#Assignment'+i).val());
		var SubmissionDate = $('#SubmissionDate'+i).val();
		var InstructionMediumP = $('#InstructionMediumP'+i).is(':checked')?'1':'0';
		var InstructionMediumE = $('#InstructionMediumE'+i).is(':checked')?'1':'0';
		var InstructionMediumC = $('#InstructionMediumC'+i).is(':checked')?'1':'0';
		var InstructionMediumB = $('#InstructionMediumB'+i).is(':checked')?'1':'0';
		var StudentF = $('#StudentF'+i).val();
		var StudentE = $('#StudentE'+i).val();
		var StudentM = $('#StudentM'+i).val();
		var Others = $.trim($('#Others'+i).val());
		/*
		if(SubjectID == ''){
			$('#SubjectIDWarningDiv'+i).show();
			is_valid = false;
		}else{
			$('#SubjectIDWarningDiv'+i).hide();
		}
		
		if(Assignment == ''){
			$('#AssignmentWarningDiv'+i).show();
			is_valid = false;
		}else{
			$('#AssignmentWarningDiv'+i).hide();
		}
		*/
		if(SubjectID != ''){
			if(InstructionMediumP=='0' && InstructionMediumE=='0' && InstructionMediumC=='0' && InstructionMediumB=='0'){
				$('#MediumWarningDiv'+i).show();
				is_valid = false;
			}else{
				$('#MediumWarningDiv'+i).hide();
			}
			
			if(Assignment != ''){
				if(!check_date_without_return_msg(document.getElementById('SubmissionDate'+i))){
					$('#SubmissionDateWarningDiv'+i).show();
					is_valid = false;
				}else{
					$('#SubmissionDateWarningDiv'+i).hide();
				}
			}
		}
		LessonIDArray.push(LessonID);
		SubjectIDArray.push(SubjectID);
		InstructionMediumPArray.push(InstructionMediumP);
		InstructionMediumEArray.push(InstructionMediumE);
		InstructionMediumCArray.push(InstructionMediumC);
		InstructionMediumBArray.push(InstructionMediumB);
		AssignmentArray.push(encodeURIComponent(Assignment));
		SubmissionDateArray.push(SubmissionDate);
		StudentFArray.push(StudentF);
		StudentEArray.push(StudentE);
		StudentMArray.push(StudentM);
		OthersArray.push(encodeURIComponent(Others));
	}
	
	if(is_valid){
		$.post(
			'ajax_task.php',
			{
				'task':'Is_Class_Diary_Duplicated',
				'DiaryID':DiaryID,
				'ClassID':ClassID,
				'DiaryDate':DiaryDate
			},
			function(data){
				if(data=='1'){
					UnBlock_Element('EditForm');
					var class_name = $('#ClassID option:selected').text();
					var alert_msg = jsLang['DuplicatedClassDiary'];
					alert_msg = alert_msg.replace('<!--CLASSNAME-->',class_name);
					alert_msg = alert_msg.replace('<!--DATE-->',DiaryDate);
					alert(alert_msg);
				}else{
					$.post(
						'ajax_task.php',
						{
							'task':'Update_Class_Diary',
							'DiaryID':DiaryID,
							'ClassID':ClassID,
							'DiaryDate':DiaryDate,
							'AbsentAMStudent':AbsentAMStudent,
							'AbsentPMStudent':AbsentPMStudent,
							'LateStudent':LateStudent,
							'EarlyleaveStudent':EarlyleaveStudent,
							'LessonID[]':LessonIDArray,
							'SubjectID[]':SubjectIDArray,
							'InstructionMediumP[]':InstructionMediumPArray,
							'InstructionMediumE[]':InstructionMediumEArray,
							'InstructionMediumC[]':InstructionMediumCArray,
							'InstructionMediumB[]':InstructionMediumBArray,
							'Assignment[]':AssignmentArray,
							'SubmissionDate[]':SubmissionDateArray,
							'StudentF[]':StudentFArray,
							'StudentE[]':StudentEArray,
							'StudentM[]':StudentMArray,
							'Others[]':OthersArray
						},
						function(msg){
							if(msg.charAt(0)=='1'){
								if(is_edit){
									window.location.href = 'classdiary_detail.php?DiaryID='+DiaryID;
								}else{
									window.location.href = 'index.php?Msg=' + encodeURIComponent(msg);
								}
							}else{
								Get_Return_Message(msg);
								UnBlock_Element('EditForm');
							}
						}
					);
				}
			}
		);
	}else{
		UnBlock_Element('EditForm');
	}
}

function ShowHideStudentPanel(clickOnElement,showHide,fieldID)
{
	var panel = $('div#StudentPanel');
	if(showHide==1){
		var classID = $('#ClassID').val();
		if(classID == ''){
			alert(jsLang['PleaseSelectClass']);
			return false;
		}
		var clickOnObj = $(clickOnElement);
		var detail_panel = $('div#StudentPanelDetail');
		var pos = clickOnObj.position();
		var windowWidth = $(window).width();
		var offsetx = 0;
		if(pos.left + panel.width() >= windowWidth){
			offsetx = (pos.left + panel.width()) - windowWidth + 32;
		}
		detail_panel.html(jsLang['Loading']);
		panel.css({'position':'absolute','left':pos.left - offsetx,'top':pos.top+clickOnObj.height()});
		panel.show();
		detail_panel.load(
			'ajax_load.php',
			{
				'task':'Get_Student_Selection_Table',
				'ClassID':classID,
				'StudentID':$('#'+fieldID).val(),
				'FieldID':fieldID 
			},
			function(data){
				
			}
		);
	}else{
		panel.hide();
	}
}

function ApplyStudents(fieldID)
{
	Block_Element('StudentPanelDetail');
	
	var checkboxes = document.getElementsByName('Students[]');
	var studentIDArray = [];
	var fieldObj = $('#'+fieldID); 
	var spanObj = fieldObj.prev();
	var classID = $('#ClassID').val();
	
	for(var i=0;i<checkboxes.length;i++){
		if(checkboxes[i].checked == true){
			studentIDArray.push(checkboxes[i].value);
		}
	}
	
	fieldObj.val(studentIDArray.join(','));
	
	spanObj.load(
		'ajax_load.php',
		{
			'task':'Get_Student_Names',
			'ClassID':classID,
			'StudentID[]':studentIDArray 
		},
		function(data){
			UnBlock_Element('StudentPanelDetail');
			$('#StudentPanel').hide();
		}
	);
}

function Get_Class_Subject_Selections()
{
	var ClassID = $('#ClassID').val();
	Block_Element('EditForm');
	Get_Class_Subject_Selection(ClassID,1);
}

function Get_Class_Subject_Selection(ClassID,i)
{
	$.get(
		'ajax_load.php',
		{
			'task':'Get_Class_Subject_Selection',
			'ClassID':ClassID,
			'ID_Name':'SubjectID'+i 
		},
		function(data){
			$('#SubjectID'+i).replaceWith(data);
			if(i==MAX_NUMBER_OF_LESSON){
				UnBlock_Element('EditForm');
			}else{
				Get_Class_Subject_Selection(ClassID,i+1);
			}
		}
	);
}
</script>