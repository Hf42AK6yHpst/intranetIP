<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");

intranet_auth();
intranet_opendb();

$libclassdiary = new libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	intranet_closedb();
	exit;
}

$task = $_REQUEST['task'];
switch($task)
{
	case "Is_Class_Diary_Duplicated":
		$DiaryID = $_REQUEST['DiaryID'];
		$ClassID = $_REQUEST['ClassID'];
		$DiaryDate = $_REQUEST['DiaryDate'];
		if($DiaryID != ''){
			echo '0';
		}else{
			if($libclassdiary->Is_Class_Diary_Duplicated($ClassID,$DiaryDate)){
				echo "1"; // duplicated
			}else{
				echo "0";
			}
		}
	break;
	
	case "Update_Class_Diary":
		$is_edit = trim($_REQUEST['DiaryID'])!='';
		$success = $libclassdiary->Update_Class_Diary($_REQUEST);
		if($success){
			if($is_edit){
				echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
			}else{
				echo $Lang['General']['ReturnMessage']['AddSuccess'];
			}
		}else{
			if($is_edit){
				echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
			}else{
				echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
			}
		}
	break;
}


intranet_closedb();
?>