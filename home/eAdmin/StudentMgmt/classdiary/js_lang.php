<script type="text/javascript" language="JavaScripot">
var jsLang = {};
jsLang['PleaseSelectClass'] = "<?=$Lang['ClassDiary']['WarningMsg']['SelectClass']?>";
jsLang['Loading'] = "<?=$Lang['ClassDiary']['Loading']?>";
jsLang['DuplicatedClassDiary'] = "<?=$Lang['ClassDiary']['WarningMsg']['DuplicatedClassDiary']?>";
jsLang['ConfirmDeleteClassDiary'] = "<?=$Lang['ClassDiary']['WarningMsg']['ConfirmDeleteClassDiary']?>";
jsLang['InvalidDateFormat'] = "<?=$Lang['General']['InvalidDateFormat']?>";
jsLang['InvalidDateRange'] = "<?=$Lang['General']['JS_warning']['InvalidDateRange']?>";
jsLang['SelectAtLeastOneClass'] = "<?=$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneClass']?>";
jsLang['SelectAtLeastOneRecordType'] = "<?=$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneRecordType']?>";
jsLang['SelectAtLeastOneMediumOfInstruction'] = "<?=$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneMediumOfInstruction']?>";
</script>