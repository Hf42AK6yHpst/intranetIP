<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$CurrentPageArr['ClassDiary'] = 1;
$CurrentPage['ExportMailMerge'] = 1;
$TAGS_OBJ[] = array($Lang['ClassDiary']['ExportMailMerge'],"",0);

$MODULE_OBJ = $libclassdiary_ui->GET_MODULE_OBJ_ARR();
$libclassdiary_ui->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $libclassdiary_ui->Get_Report_Export_Mail_Merge();

$libclassdiary_ui->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
function Generate_Report()
{
	var is_valid = true;
	var StartDate = $.trim($('input#StartDate').val());
	var EndDate = $.trim($('input#EndDate').val());
	var ClassIDObjs = $('select[name=ClassID[]] option:selected');
	var ClassIDArray = [], ClassNameArray = [];
	for(var i=0;i<ClassIDObjs.length;i++){
		ClassIDArray.push(ClassIDObjs[i].value);
		ClassNameArray.push(encodeURIComponent(ClassIDObjs[i].text));
	}
		
	if(ClassIDArray.length==0){
		$('#ClassIDWarningDiv').html(jsLang['SelectAtLeastOneClass']).show();
		$('select[name=ClassID[]]').focus();
		is_valid = false;
	}else{
		$('#ClassIDWarningDiv').html('').hide();
	}
	
	$('#DateWarningLayer').html('').hide();
	if(StartDate=='' || !check_date_without_return_msg(document.getElementById('StartDate'))){
		$('#DateWarningLayer').html(jsLang['InvalidDateFormat']).show();
		$('input#StartDate').focus();
		is_valid = false;
	}
	
	if(EndDate=='' || !check_date_without_return_msg(document.getElementById('EndDate'))){
		$('#DateWarningLayer').html(jsLang['InvalidDateFormat']).show();
		$('input#EndDate').focus();
		is_valid = false;
	}
	
	if(StartDate > EndDate){
		$('#DateWarningLayer').html(jsLang['InvalidDateRange']).show();
		$('input#StartDate').focus();
		is_valid = false;
	}
	
	if(is_valid){
		document.form1.submit();
	}
	
}
</script>