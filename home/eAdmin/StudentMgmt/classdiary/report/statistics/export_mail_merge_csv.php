<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$lexport = new libexporttext();
$RecordType = array("F", "E", "M", "1", "2", "3", "4");
$data_arr = $libclassdiary_ui->Get_Report_Export_Mail_Merge_CSV($_REQUEST['StartDate'],$_REQUEST['EndDate'],$_REQUEST['ClassID'], $_REQUEST['ClassName'],$RecordType, $_REQUEST['ShowDates']==1, 1);

intranet_closedb();

$filename = "class_diary_for_mail_merge.csv";
$utf_content = "";
for ($i=0; $i<sizeof($data_arr); $i++)
{
	$utf_content .= implode("\t", $data_arr[$i]);
	$utf_content .= "\r\n";
}

$lexport->EXPORT_FILE($filename, $utf_content);
?>