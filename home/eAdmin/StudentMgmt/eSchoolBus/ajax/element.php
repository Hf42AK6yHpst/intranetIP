<?php
/*
 *  2020-06-19 Cameron
 *      - modify routeSelection_dateSearch(), allow to take attendance on Sat and Sun if the date is set as special day that need to go to school [case #Y188036]
 *
 *  2019-08-09 Cameron
 *      - pass $academicYearID in DatePicker case rather than use current academic year
 */
$linterface = $indexVar['linterface'];
$action = $_POST['action'];


switch ($action){
	
	case 'busStopSelection':
		$newRowNumber = $_POST['newRowNumber'];
		$name = $_POST['name'];
		echo $linterface->getBusStopsSelection('','name="'.$name.'['.$newRowNumber.']"');
	break;
	case 'timeSelection':
		$newRowNumber = $_POST['newRowNumber'];
		$name = $_POST['name'];
		echo $linterface->Get_Time_Selection($name, $DefaultTime='00:00:00', $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1), $parObjId=$newRowNumber);
	break;
	case 'vehicleSelection':
		$newVehicleNum = $_POST['newVehicleNum'];
		$name = $_POST['name'];
		echo $linterface->getVehicleSelection($tags='name="'.$name.'['.$newVehicleNum.']"', $selected="", $all=0, $noFirst=0, $FirstTitle="");
	break;
	case 'DatePicker':
		$newVehicleNum = $_POST['newVehicleNum'];
		$academicYearID = $_POST['academicYearID'];
		$name = $_POST['name'];
		$type = $_POST['type'];
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$fcm = new form_class_manage();
		//$AcademicYearID = Get_Current_Academic_Year_ID();
		$academicYearObj = new academic_year($academicYearID);
		$StartDate = date("Y-m-d", strtotime($academicYearObj->AcademicYearStart));
		$EndDate =  date("Y-m-d", strtotime($academicYearObj->AcademicYearEnd));
		if($type == 'start'){
			$DefaultValue = $StartDate;
		}else if($type == 'end'){
			$DefaultValue = $EndDate;
		}
		echo $linterface->GET_DATE_PICKER($Name=$name.'['.$newVehicleNum.']',$DefaultValue,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID=$name.'_'.$newVehicleNum,$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
	break;
	case 'textbox':
		$newVehicleNum = $_POST['newVehicleNum'];
		$name = $_POST['name'];
		echo $linterface->GET_TEXTBOX($name.'_'.$newVehicleNum, $name.'['.$newVehicleNum.']', '', $OtherClass='', $OtherPar=array('maxlength'=>255,'style'=>'width:200px','placeholder'=>$Lang['eSchoolBus']['Settings']['Route']['Remarks']));
	break;
	case 'routeSelection_dateSearch':
	    include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	    $lattend = new libcardstudentattend2();

		$date = $_POST['date'];
		$weekday_num = date("N", strtotime($date));
		$onchange = $_POST['onselect']; 
		switch($weekday_num){
			case 1:
				$weekday = 'MON';
			break;
			case 2:
				$weekday = 'TUE';
			break;
			case 3:
				$weekday = 'WED';
			break;
			case 4:
				$weekday = 'THU';
			break;
			case 5:
				$weekday = 'FRI';
			break;
			case 6:
			case 7:
			    $classListToTakeAttendance = $lattend->getClassListToTakeAttendanceByDate($date);
			    if (count($classListToTakeAttendance)) {
			        $weekday = ($weekday_num == 6) ? 'SAT' : 'SUN';
			    }
			    else {
			        echo ($weekday_num == 6) ? $Lang['eSchoolBus']['Managment']['Attendance']['Warning']['WeekendNotAvailable'] : $Lang['eSchoolBus']['Managment']['Attendance']['Warning']['SundayNotAvailable'];
			        return;
			    }
			    break;
			default:
				echo $Lang['eSchoolBus']['Managment']['Attendance']['Warning']['WeekendNotAvailable'];
				return;
			break;
		}	
		echo $linterface->getRouteSelection('id="routeID" name="routeID" onchange="'.$onchange.'"',$routeID,$type='',$AmPm='',$weekday,$date);
	break;
	case 'userList':

			$groupID = $_POST['groupID'];
			$roleID = $_POST['roleID'];
			$userIDAry = $_POST['userIDAry'];
			$groupCat = $_POST['groupCat'];
			$noMultiple = $_POST['noMultiple'];
			if($noMultiple){
				$multiple = false;
			}else{
				$multiple = true;
			}
			$db = $indexVar['db'];
			// 		debug_pr($groupCat);
			if($groupCat[0] == 'I'){
				$teaching = ($groupCat == 'I1')? 1: 0;
				$userIDAry = explode(',',$userIDAry);
				$userSelection = $linterface->Get_User_Selection($groupCat,$userIDAry,$multiple);
			}
			else if($groupCat[0] == 'G'){
				$intranetGroupID = substr($groupCat,1);
				$excludeUserCond = " UserID not in ('".implode("','", (array)$userIDAry)."')";
				include_once($intranet_root."/includes/libgroup.php");
				$libgroup = new libgroup($intranetGroupID);
				$groupUserArr = $libgroup->returnGroupUser('',$excludeUserCond);
				$userSelection = $linterface->Get_User_Selection($groupCat,$userIDAry,$multiple);
			}
		
			echo $userSelection;
		
	break;
	default:
		echo 'error';
	break;
}

?>