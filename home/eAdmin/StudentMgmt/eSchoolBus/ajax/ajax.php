<?
//Using:  
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 *  2019-04-16 [Cameron]
 *      - add filter HasRouteOrNot in case getStudentListByClass [case #Y159784]
 *      
 *  2019-03-06 [Cameron]
 *      - add filter ClassGroupID to case getStudentListByClass, getStudentListByBus [case #Y156987] 
 *      
 *  2019-01-29 [Cameron]
 *      - add case getTeacherName
 *      
 *  2019-01-25 [Cameron]
 *      - add parameter $takeOrNot for case getStudentListByBus [case #Y154128]
 *      
 *  2018-11-19 [Cameron]
 *      - create this file
 */

include_once($PATH_WRT_ROOT."includes/json.php");

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();
$linterface = $indexVar['linterface'];

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];

switch($action) {
    
    case 'checkHoliday':
        $date = $_POST['date'];
        $holidays = $indexVar['db']->getHoliday($date, $date);
        if (!empty($holidays[$date])) {
            $x = 1;     // is holiday
        }
        else {
            $x = '';    // not holiday
        }
        $json['success'] = true;
        break;
        
    case 'getClassBuilding':
        $date = $_POST['date'];
        
        $x = $linterface->getClassBuilding($date);
        $aytAry = getAcademicYearAndYearTermByDate($date);
        $academicYearID = $aytAry['AcademicYearID'];
        $json['AcademicYearID'] = $academicYearID;
        $json['success'] = true;
        break;
        
    case 'getClassList':
        $academicYearID = $_POST['academicYearID'];
        $tags =' id="YearClassID" name="YearClassID[]" multiple="multiple" size="10" ';
        $x = $linterface->getClassSelection($academicYearID, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="");
        $json['success'] = true;
        break;
        
    case 'getStudentListByBus':
        $date = $_POST['SearchDate'];
        //$timeSlot = $_POST['TimeSlot'];
        $timeSlot = 'PM';                   // only require to retrieve FromSchool record
        $buildingID = $_POST['BuildingID'];
        $takeOrNot = $_POST['TakeOrNot'];
        $classGroupIDAry = $_POST['ClassGroupID'];
        $x = $linterface->getStudentListByBus($date, $timeSlot, $buildingID, $takeOrNot, $printTitle=false, $classGroupIDAry);
        $json['success'] = true;
        break;
        
    case 'getStudentListByClass':
        $date = $_POST['SearchDate'];
        $timeSlot = $_POST['TimeSlot'];        
        $hasRouteOrNot = $_POST['HasRouteOrNot'];
        $buildingID = $_POST['BuildingID'];
        $takeOrNot = $_POST['TakeOrNot'];
        $yearClassIDAry = $_POST['YearClassID'];
        $classGroupIDAry = $_POST['ClassGroupID'];
        $x = $linterface->getStudentListByClass($date, $timeSlot, $buildingID, $takeOrNot, $hasRouteOrNot, $yearClassIDAry, $classGroupIDAry);
        $json['success'] = true;
        break;
 
    case 'getTeacherName':
        $teacherType = $_POST['TeacherType'];
        $excludeTeacherIDAry = $_POST['ExcludeTeacherID'];
        if (count($excludeTeacherIDAry)) {
            $cond = "AND UserID NOT IN ('".implode("','",$excludeTeacherIDAry)."')";
        }
        else {
            $cond = "";
        }
        $x = '<select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:80px;" multiple>';
        if ($teacherType == 1) {
            $teacherType = 1;
        }
        else {
            $teacherType = 0;
        }
        $data = $indexVar['db']->getTeacher($teacherType,$cond);
        for($i=0,$iMax=count($data); $i<$iMax; $i++) {
            $staffID = $data[$i]['UserID'];
            $staffName = $data[$i]['Name'];
            $x .= '<option value="'.$staffID.'">'.$staffName.'</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
}

$json['html'] = $x;
echo $ljson->encode($json);

intranet_closedb();
?>