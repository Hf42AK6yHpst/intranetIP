<?php
// Editing by 

$returnMsg = $indexVar['libSchoolBus']->getTempSession('ManagementSpecialArrangement');

$returnYearClassID = $indexVar['libSchoolBus']->getTempSession('ManagementSpecialArrangement_YearClassID');
$returnStudentID = $indexVar['libSchoolBus']->getTempSession('ManagementSpecialArrangement_StudentID');

$AcademicYearID = Get_Current_Academic_Year_ID();

$am_pm_ary = array('AM','PM');
$weekday_ary = array('MON','TUE','WED','THU','FRI');

$today = date("Y-m-d");
$parentSelectionOption = array(array('0',$Lang['eSchoolBus']['Settings']['Student']['ByParent']));

if($returnYearClassID != '' && !isset($YearClassID)){
	$YearClassID = $returnYearClassID;
}

if($returnStudentID != '' && !isset($StudentID)){
	$StudentID = $returnStudentID;
}

if(!isset($YearClassID) || !isset($StudentID))
{
	include_once($intranet_root.'/includes/form_class_manage.php');
	
	$fcm = new form_class_manage();
	$class_list = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, "", $__TeachingOnly=0);

	//$YearClassID = $class_list[0]['YearClassID'];
	for($i=0;$i<count($class_list);$i++)
	{
		$YearClassID = $class_list[$i]['YearClassID'];
		$student_list = $fcm->Get_Student_By_Class(array($YearClassID));
		if(count($student_list)>0)
		{
			$StudentID = $student_list[0]['UserID'];
			break;
		}
	}
}

$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$StudentID,'IsAssoc'=>1));
$user_routes = isset($records[$StudentID])? $records[$StudentID] : array();

$records = $indexVar['db']->getSpecialUserRouteRecords(array('UserID'=>$StudentID,'AcademicYearID'=>$AcademicYearID,'IsAssoc'=>1));
//$records = getSpecialUserRouteRecords(array('UserID'=>$StudentID,'AcademicYearID'=>$AcademicYearID,'IsAssoc'=>1));
$special_user_routes = isset($records[$StudentID])? $records[$StudentID] : array();

$hidden_fields .= $indexVar['linterface']->GET_HIDDEN_INPUT('AcademicYearID', 'AcademicYearID', $AcademicYearID);

$indexVar['linterface']->LAYOUT_START($returnMsg);
?>
<?=$indexVar['linterface']->INCLUDE_COMMON_CSS_JS()?>
<br />
<form name="form1" id="form1" action="index.php?task=management/specialArrangement" method="post" onsubmit="return false;">
	<?=$hidden_fields?>
	<table width="100%" cellpadding="2" class="form_table_v30">
		<col class="field_title">
		<col class="field_c">
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['SysMgr']['FormClassMapping']['Class']?></td>
			<td>
				<?=$indexVar['linterface']->getClassSelection($AcademicYearID, ' id="YearClassID" name="YearClassID" onchange="getStudentSelection();" ', $YearClassID, $__all=0, $__noFirst=1, $FirstTitle="")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Identity']['Student']?></td>
			<td>
				<span id="StudentSelectionContainer"><?=$indexVar['linterface']->getStudentSelectionByClass($YearClassID,  ' id="StudentID" name="StudentID" onchange="studentSelectionChanged();" ', $StudentID, $all=0, $noFirst=1, $FirstTitle="")?></span>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("StudentID_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']?></td>
			<td>
				
				<?php
				$x = '';
				foreach($am_pm_ary as $day)
				{
					$x .= '<div>';
					$x .= $Lang['eSchoolBus']['Settings']['Student'][$day];
					if(isset($user_routes['N'][$day])){
						$x .= '&nbsp;-&nbsp;'.Get_String_Display( $user_routes['N'][$day]['RouteName'].($user_routes['N'][$day]['Cars']!=''? ' ('.$user_routes['N'][$day]['Cars'].')' : ''));
						//$x .= $Lang['eSchoolBus']['Settings']['Student']['BusStop'].'&nbsp;';
						$x .= '&nbsp;-&nbsp;'.Get_String_Display( $user_routes['N'][$day]['BusStopsName']);
					}else{
						$x .= '&nbsp;'.$Lang['General']['EmptySymbol'];
					}
					$x .= '</div>';
				}
				
				echo $x;
				?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']?></td>
			<td>
				<table>
					<thead>
						<tr>
						<?php
						$x = '';
						foreach($weekday_ary as $weekday){
							$x .= '<td class="'.$weekday.'">'.$Lang['eSchoolBus']['Settings']['Route']['Weekday'][$weekday].'</td>'."\n";
						}
						echo $x;
						?>	
						</tr>
					</thead>
					<tbody>
						<tr>
						<?php
						$x = '';
						foreach($weekday_ary as $weekday)
						{
							$x .= '<td class="'.$weekday.'">'."\n";
								if(isset($user_routes['S'][$weekday]) && $user_routes['S'][$weekday]['ActivityID'] != ''){
									$x .= $schoolBusConfig['ActivityTypes'][$user_routes['S'][$weekday]['ActivityID']-1][1].'<br />';
									if($user_routes['S'][$weekday]['RouteCollectionID'] == '0'){
										$x .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
									}else{
										$x .= Get_String_Display($user_routes['S'][$weekday]['RouteName'].($user_routes['S'][$weekday]['Cars']!=''? ' ('.$user_routes['S'][$weekday]['Cars'].')' : '')).'<br />';
										$x .= Get_String_Display($user_routes['S'][$weekday]['BusStopsName']);
									}
								}else{
									$x .= $Lang['General']['EmptySymbol'];
								}
							$x .= '</td>'."\n";
						}
						echo $x;
						?>		
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eSchoolBus']['Management']['SpecialArrangement']['SpecialArrangementByDates']?></td>
			<td>
			<?php if(count($user_routes)==0){ ?>	
				<span class="red"><?=$Lang['eSchoolBus']['Management']['SpecialArrangement']['UnableToSetSpecialArrangements']?></span>
			<?php }else{ ?>
				<table id="SpecialArrangementTable">
					<thead>
						<tr>
							<th><?=$Lang['General']['Date']?></th>
							<th class="TUE"><?=$Lang['eSchoolBus']['Settings']['Student']['AM']?></th>
							<th class="WED"><?=$Lang['eSchoolBus']['Settings']['Student']['PM']?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$x = '';
					if(count($special_user_routes)>0)
					{
						//debug_pr($special_user_routes);
						foreach($special_user_routes as $date => $ary)
						{
							$am_uid = uniqid();
							$pm_uid = uniqid();
							$x .= '<tr class="special">
									<td>
										'.$indexVar['linterface']->GET_DATE_PICKER("Date[]",$date,$__OtherMember='onchange="dateChanged(this);"',$__DateFormat="yy-mm-dd",$__MaskFunction="",$__ExtWarningLayerID="",$__ExtWarningLayerContainer="",$__OnDatePickSelectedFunction="dateChanged(this);",$__ID='Date_'.uniqid()).'
									</td>
									<td>
										'.$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID, array('N'), array('AM'),' id="RouteCollectionID_'.$am_uid.'" name="RouteCollectionID[AM][]" onchange="routeSelectionChanged(this, \'AM\');" ', $__selected=$ary['AM']['RouteCollectionID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --', $parentSelectionOption,$date).'
										<span'.(in_array($ary['AM']['RouteCollectionID'],array('0',''))?' style="display:none;"':'').'>'
										.$indexVar['linterface']->getBusStopsSelectionByRouteCollection($ary['AM']['RouteCollectionID'], ' name="BusStopsID[AM][]" ', $__selected=$ary['AM']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --').'
										'.$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("RouteCollectionID_".$am_uid."_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'], "WarnMsgDiv").'
										</span>
									</td>
									<td>
										'.$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID, array('N','S'), array('PM','MON','TUE','WED','THU','FRI'),' id="RouteCollectionID_'.$pm_uid.'" name="RouteCollectionID[PM][]" onchange="routeSelectionChanged(this, \'PM\');" ', $__selected=$ary['PM']['RouteCollectionID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --', $parentSelectionOption,$date).'
										<span'.(in_array($ary['PM']['RouteCollectionID'],array('0',''))?' style="display:none;"':'').'>'
										.$indexVar['linterface']->getBusStopsSelectionByRouteCollection($ary['PM']['RouteCollectionID'], '  name="BusStopsID[PM][]" ', $__selected=$ary['PM']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --').'
										'.$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("RouteCollectionID_".$pm_uid."_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'], "WarnMsgDiv").'
										</span>
									</td>
									<td>'.$indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $__ParTitle=$Lang['Btn']['Delete'], $__ParOnClick="deleteRow(this);", $__ParClass="", $__WithSpan=1).'</td>
								</tr>'."\n";
						}
					}
					echo $x;
					?>
					</tbody>
				</table>
				<div id="Spinner" style="display:none;"><?=$indexVar['linterface']->Get_Ajax_Loading_Image()?></div>
				<?=$indexVar['linterface']->GET_SMALL_BTN(" + ", "button", $__ParOnClick="addRow();", "addBtn", $__ParOtherAttribute="", $__OtherClass="", $__ParTitle=$Lang['Btn']['Add'])?>
				<span class="tabletextremark"><?=$Lang['eSchoolBus']['Management']['SpecialArrangement']['FollowPreviousArrangementForNextRecord']?></span>
			<?php } ?>
			</td>
		</tr>
	</table>
	<?=$indexVar['linterface']->MandatoryField()?>
<?php if(count($user_routes)>0){ ?>		
<div class="edit_bottom_v30">
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submitBtn').'&nbsp;'?>
</div>
<?php } ?>
</form>
<script type="text/javascript" language="javascript">
var loading_img = '<?=$indexVar['linterface']->Get_Ajax_Loading_Image()?>';

function getStudentSelection()
{
	$('#StudentSelectionContainer').html(loading_img);
	$.post(
		'index.php?task=management/specialArrangement/ajax',
		{
			'ajax_task': 'getStudentSelection',
			'YearClassID':$('#YearClassID').val()
		},function(returnHtml){
			$('#StudentSelectionContainer').html(returnHtml);
			document.form1.submit();
		}
	);
}

function studentSelectionChanged()
{
	document.form1.submit();
}

function dateChanged(obj)
{
	var date_val = $(obj).val();
	$(obj).attr('disabled',true);
	$('#addBtn').attr('disabled',true);
	$.post(
		'index.php?task=management/specialArrangement/ajax',
		{
			'ajax_task': 'getSpecialArrangementRow',
			'AcademicYearID': $('#AcademicYearID').val(),
			'Date': date_val,
			'AutoIncrement': 0
		},
		function(returnHtml){
			$(obj).closest('tr.special').replaceWith(returnHtml);
			$('#Spinner').hide();
			$('#addBtn').attr('disabled',false);
		}
	);
}

function routeSelectionChanged(obj, typeName)
{
	var route_collection_id = $(obj).val();
	if(route_collection_id == '0' || route_collection_id == ''){
		$(obj).closest('td').find('span').hide();
	}else{
		$(obj).closest('td').find('span').show();
	}
	
	$.post(
		'index.php?task=management/specialArrangement/ajax',
		{
			'ajax_task': 'getBusStopsSelection',
			'RouteCollectionID': route_collection_id,
			'Name': 'BusStopsID['+typeName+'][]' 
		},function(returnHtml){
			$(obj).closest('td').find('span').find('select').replaceWith(returnHtml);
		}
	);
}

function addRow()
{
	var rows = $('tr.special');
	var prev_am_collection_id = '';
	var prev_am_busstop_id = '';
	var prev_pm_collection_id = '';
	var prev_pm_busstop_id = '';
	var last_date_val = getToday();
	if(rows.length > 0){
		last_date_val = $(rows[rows.length-1]).find('input').val();
		prev_am_collection_id = $(rows[rows.length-1]).find('[name="RouteCollectionID[AM][]"]').length>0? $(rows[rows.length-1]).find('[name="RouteCollectionID[AM][]"]').val() : '';
		prev_am_busstop_id = $(rows[rows.length-1]).find('[name="BusStopsID[AM][]"]').length>0? $(rows[rows.length-1]).find('[name="BusStopsID[AM][]"]').val() : '';
		prev_pm_collection_id = $(rows[rows.length-1]).find('[name="RouteCollectionID[PM][]"]').length>0? $(rows[rows.length-1]).find('[name="RouteCollectionID[PM][]"]').val() : '';
		prev_pm_busstop_id = $(rows[rows.length-1]).find('[name="BusStopsID[PM][]"]').length>0? $(rows[rows.length-1]).find('[name="BusStopsID[PM][]"]').val() : '';
	}
	$('#addBtn').attr('disabled',true);
	$('#Spinner').show();
	$.post(
		'index.php?task=management/specialArrangement/ajax',
		{
			'ajax_task': 'getSpecialArrangementRow',
			'AcademicYearID':  $('#AcademicYearID').val(),
			'Date': last_date_val,
			'AMRouteCollectionID': prev_am_collection_id,
			'AMBusStopID': prev_am_busstop_id,
			'PMRouteCollectionID': prev_pm_collection_id,
			'PMBusStopID': prev_pm_busstop_id,
			'AutoIncrement': 1
		},
		function(returnHtml){
			$('#SpecialArrangementTable tbody').append(returnHtml);
			$('#Spinner').hide();
			$('#addBtn').attr('disabled',false);
		}
	);
}

function deleteRow(obj)
{
	if(confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>')){
		$(obj).closest('tr').remove();
	}
}

function checkSubmitForm(formObj)
{
	var valid = true;
	
	var dates = $('[name="Date\\[\\]"]');
	var date_values = [];
	var am_routes = $('[name="RouteCollectionID\\[AM\\]\\[\\]"');
	var am_stops = $('[name="BusStopsID\\[AM\\]\\[\\]"');
	var pm_routes = $('[name="RouteCollectionID\\[PM\\]\\[\\]"');
	var pm_stops = $('[name="BusStopsID\\[PM\\]\\[\\]"');
	var row_count = $('tr.special').length;
	$('.WarnMsgDiv').hide();
	for(var i=0;i<row_count;i++){
		var dpwl = $('#DPWL-' + dates[i].id);
		if(dpwl.html() != ''){
			valid = false;
		}else if($.inArray(dates[i].value, date_values)>=0){
			valid = false;
			dpwl.html('<?=$Lang['eSchoolBus']['Settings']['Warning']['DuplicatedDate']?>').show();
		}else{
			date_values.push(dates[i].value);
		}
	
		var am_obj = $(am_routes[i]);
		var am_stop_obj = $(am_stops[i]);
		if(am_obj.val() != '' && am_obj.val()!='0' && am_stop_obj.val()==''){
			var id = am_routes[i].id;
			$('#'+id+'_Error').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop']?>').show();
			valid = false;
		}
		var pm_obj = $(pm_routes[i]);
		var pm_stop_obj = $(pm_stops[i]);
		if(pm_obj.val() != '' && pm_obj.val()!='0' && pm_stop_obj.val()==''){
			var id = pm_routes[i].id;
			$('#'+id+'_Error').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop']?>').show();
			valid = false;
		}
		if(am_obj.val() == '' && pm_obj.val() == ''){
			var id = am_routes[i].id;
			$('#'+id+'_Error').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop']?>').show();
			id = pm_routes[i].id;
			$('#'+id+'_Error').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop']?>').show();
			valid = false;
		}
	}
	
	if(valid){
		formObj.action = 'index.php?task=management/specialArrangement/edit_update';
		formObj.submit();
	}
}
</script>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>