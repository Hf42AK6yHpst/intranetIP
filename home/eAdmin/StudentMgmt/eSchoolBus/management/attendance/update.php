<?php
/*
 *  2020-09-14 Cameron
 *      - cast to array to suppress error for foreach loop
 */
// debug_pr($_POST);
$statusAry = $_POST['status'];	
$remarksAry = $_POST['remarks'];
$routeCollectionID = $_POST['routeCollectionID'];
$date = $_POST['date'];
$db = $indexVar['db'];
$today = date("Y-m-d");

$errorObj = false;

//Get exist attendance record
$userIDAry = array();
foreach((array)$statusAry as $userID => $status){
	$userIDAry[] = $userID;
}
$attendanceInfo = $db->getAttendanceInfo($routeCollectionID,$userIDAry,$date);
$attendanceAssoAryByUser = BuildMultiKeyAssoc($attendanceInfo, 'UserID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);

$tableName = $db->getAttendanceTableName();
$inputFieldNameArr = array('RouteCollectionID','UserID','Status','Remarks','AttendanceDate','DateInput','DateModified','InputBy','ModifiedBy');
$updateFieldNameArr = array('RouteCollectionID','UserID','Status','Remarks','AttendanceDate','DateModified','ModifiedBy');
$inputMultiRowValueArr = array();
foreach((array)$statusAry as $userID => $status){
	
	//check tuple is already exist
	if(!empty($attendanceAssoAryByUser[$userID])){
		$exist = true;
	}else{
		$exist = false;
	}
	
	if($status==''){
		//clear tuple in attendance table if exist
		if($exist){
			$db->permanentDeleteData($tableName, 'RouteCollectionID', array($routeCollectionID),$additionCond="AND UserID = '$userID' AND AttendanceDate = '$date'");
		}
	}else{
		//check is already exist
		if($exist){
			//Update
			$conditions = "UserID = '$userID' AND RouteCollectionID = '$routeCollectionID' AND AttendanceDate = '$date' ";
			$updateMultiRowValueArr = array(array($routeCollectionID,$userID,$status,$remarksAry[$userID],$date,$today,$_SESSION['UserID']));
			$success = $db->updateData($tableName, $updateFieldNameArr, $updateMultiRowValueArr, $conditions, $excludeCommonField=true);
			(!$success)? $errorObj = true:'';
		}else{
			//Insert
			$inputMultiRowValueArr[] = array($routeCollectionID,$userID,$status,$remarksAry[$userID],$date,$today,$today,$_SESSION['UserID'],$_SESSION['UserID']);
		}
	}
}

if(!empty($inputMultiRowValueArr)){
	$success = $db->insertData($tableName, $inputFieldNameArr, $inputMultiRowValueArr, $excludeCommonField=true);
	(!$success)? $errorObj = true:'';
}


if($errorObj){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}else{
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}

$is_edit = ($routeID)?true:false;
$indexVar['libSchoolBus']->setTempSession('ManagementAttendance',$returnMsg);
$path = "index.php?task=management/attendance";
$indexVar['libSchoolBus']->redirect($path);

?>