<?php
// Editing by 
/*
 *  2020-09-14 Cameron
 *      - check if there's record before submit
 */
$linterface = $indexVar['linterface'];
$returnMsg = $indexVar['libSchoolBus']->getTempSession('ManagementAttendance');
$linterface->LAYOUT_START($returnMsg);
$db = $indexVar['db'];

$routeID = $_POST['routeID'];

?>
<script>

function refreshRouteSelection(){
	var date = $('#SearchDate').val();
	var routeSearchSpan = $('#ajax_route_selection');
	routeSearchSpan.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?task=ajax/element",
		  type: "POST",
		  data: {
			action: 'routeSelection_dateSearch',
			date : date,
			onselect : 'onSelectRoute();'
		  },
		  dataType: "html",
		  success: function(response){
			  routeSearchSpan.html(response);
		  }
		});
}
function goSearch(){
	
	var canSubmit = true;
	$('.warnMsgDiv').hide();
	var date = $('#SearchDate').val();
	var routeID = $('#routeID option:selected').val();
	if(!routeID){
		$('#RouteIDEmptyWarnDiv').show();
		canSubmit = false;
	}
	var amPm = $('input[name="AmPm"]:checked').val();

	if(canSubmit){
		$('#tdOption').show();
		var formArea = $('#ajax_form');
		formArea.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
		$.ajax({
			  url: "index.php?task=ajax/form",
			  type: "POST",
			  data: {
				action: 'attendance',
				date : date,
				routeID : routeID,
				amPm:amPm
			  },
			  dataType: "html",
			  success: function(response){
				  formArea.html(response);
				  hide();
			  }
			});
	}
	
}
function onSelectRoute(){
	var label = $('#routeID :selected').parent().attr('label');
	if(label=='<?= $Lang['eSchoolBus']['Settings']['Route']['NormalRoute'] ?>'){
		//Normal route
		$('#ampm_td').show();
	}else if(label=='<?=$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute'] ?>'){
		//Special Route
		$('#ampm_td').hide();
	}
}
function showContact(btnElement,userID){
	var btnObj = $(btnElement);
	var layerObj = $('#InfoLayer');
	var posX = btnObj.offset().left -250;
	var posY = btnObj.offset().top + btnObj.height();
	
	if(document.lastClickedElement == btnElement && layerObj.is(':visible')){
		layerObj.hide('fast');
	}else{
		document.lastClickedElement = btnElement;
		$.post(
			'index.php?task=ajax/getGuardianInfo',
			{
				'StudentID': userID 
			},
			function(data){
				$('#InfoLayer .content').html(data);
				layerObj.css({'position':'absolute','top':posY+'px','left':posX+'px'});
				layerObj.show('fast');
			}
		);
	}
}
function goSubmit(){
    if ($("select[name^='status']").length > 0) {
	$('#ajax_form').submit();
}
    else {
        alert("<?php echo $Lang['General']['NoRecordAtThisMoment'];?>");
    }
}
function pushToAllStatus(value,busStopsID){
	var selectors = $('.row_'+busStopsID+' td select');
	if(selectors.length!=0){
		$.each(selectors,function(index,selector){
			$(selector).val(value);
		});
	}
}
function show(){
	$('#spanShowOption').hide('fast');
	$('#spanHideOption').show('fast');
	$('#select_panel').show('fast');
}
function hide(){
	$('#spanHideOption').hide('fast');
	$('#spanShowOption').show('fast');
	$('#select_panel').hide('fast');
}
</script>
<style>
.special {
	background-color: #ffcccc;
}

</style>
<div id="showHideOptionDiv">
	<table style="width:100%;">
		<tr>
			<td id="tdOption" class="report_show_option" style="display:none">
				<span id="spanShowOption">'
				<?= $linterface->Get_Show_Option_Link("javascript:show();", '', '', 'spanShowOption_reportOptionOuterDivBtn') ?>
				</span>
				<span id="spanHideOption" style="display:none">
				<?= $linterface->Get_Hide_Option_Link("javascript:hide();", '', '', 'spanHideOption_reportOptionOuterDivBtn')?>
				</span>
			</td>
		</tr>
	</table>
</div>
<div id="select_panel">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" class="form_table_v30">
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%"><?=$Lang['eSchoolBus']['Management']['Attendance']['Date'] ?></td>
			<td><?= $linterface->GET_DATE_PICKER('SearchDate',$DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="refreshRouteSelection()",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="refreshRouteSelection()",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")  ?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%"><?=$Lang['eSchoolBus']['Management']['Attendance']['Route'] ?></td>
			<td>
				<span id="ajax_route_selection"></span>
				<br>
				<?= $linterface->Get_Form_Warning_Msg('RouteIDEmptyWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], $Class='warnMsgDiv') ?>
			</td>
		</tr>
		<tr id="ampm_td" style="display:none">
			<td valign="top" nowrap="nowrap" class="field_title" width="30%"><?= $Lang['eSchoolBus']['Management']['Attendance']['AmPm'] ?></td>
			<td>
				<?= $linterface->Get_Radio_Button('AmPm_am', 'AmPm', $Value='AM', $isChecked=1, $Class="", $Display=$Lang['eSchoolBus']['Settings']['Student']['AM'], $Onclick="",$isDisabled=0) ?>
				<?= $linterface->Get_Radio_Button('AmPm_pm', 'AmPm', $Value='PM', $isChecked=0, $Class="", $Display=$Lang['eSchoolBus']['Settings']['Student']['PM'], $Onclick="",$isDisabled=0) ?>
			</td>
		</tr>
	</table>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->Get_Action_Btn($Lang['Btn']['View'], "button", "goSearch()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
		<p class="spacer"></p>
	</div>
	<br>
</div>
<div id="InfoLayer" class="selectbox_layer selectbox_layer_show_info" style="visibility:visible;display:none;">
	<div style="float:right">
		<a href="javascript:void(0);" onclick="$('#InfoLayer').toggle('fast');">
			<img border="0" src="/images/<?=$LAYOUT_SKIN?>/ecomm/btn_mini_off.gif">
		</a>
	</div>
	<p class="spacer"></p>
	<div class="content" style="min-width:200px;max-height:512px;overflow-y:auto;"></div>
</div>
<form id="ajax_form" method="post" action="index.php?task=management/attendance/update">


</form>	




<?php
$linterface->LAYOUT_STOP();
 ?>