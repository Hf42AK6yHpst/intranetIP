<?php
$linterface = $indexVar['linterface'];
$linterface->LAYOUT_START($returnMsg);
$busStopsID = $_POST['targetIdAry'][0];

if($busStopsID){
	//if having busStopID => get Info
	$db = $indexVar['db'];
	$busStopInfo = $db->getBusStopsInfo($busStopsID);
	$busStopsName = $busStopInfo[0]['BusStopsName'];
	$busStopsNameChi = $busStopInfo[0]['BusStopsNameChi'];
}

$linterface = $indexVar['linterface'];
$requiredSym = $linterface->RequiredSymbol();

### Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('busStopsID', 'busStopsID', $busStopsID);
$htmlAry['hiddenField'] = $hiddenF;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


$pages_arr = array(
		array($Lang['eSchoolBus']['SettingsArr']['BusStopsArr']['MenuTitle'],'index.php?task=settings/busStops'),
		array($routeID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);
?>
<script>
var canSubmit = true;
function checkForm(){
	$('.warnMsgDiv').hide();
	canSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	return canSubmit;
}
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    console.log(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}
function goCancel(){
	window.history.back();
}
function checkDuplication(dbField,value,excludeValue,warningBoxId){
	canSubmit = true;
	$('.warnMsgDiv').hide();
	$.post(
			'index.php?task=ajax/checkDuplicates',
			{
				'dbField': dbField,
				'value':value.trim(),
				'excludeValue':excludeValue
			},
			function(returnHtml){
				if(returnHtml=='OK'){
					
				}else if(returnHtml=='DUPLICATED'){
					$('#'+warningBoxId).show('fast');
					canSubmit = false;
				}
			}
		);
	
}
</script>
<?=$indexVar['linterface']->GET_NAVIGATION_IP25($pages_arr)?>
<form name='form1' id='form1' method="POST" action="index.php?task=settings/busStops/editUpdate">
<div class="table_board">
	<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$requiredSym.$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameChi'] ?></td>
		<td>
			<?= $linterface->GET_TEXTBOX('BusStopsNameChi', 'BusStopsNameChi', $busStopsNameChi, $OtherClass='requiredField textbox_name', $OtherPar=array('onchange'=>'checkDuplication(\'BusStopsNameChi\',this.value,\''.$busStopsNameChi.'\',\'BusStopsNameChiDuplicateWarnDiv\');')) ?>
			<?= $linterface->Get_Form_Warning_Msg('BusStopsNameChiEmptyWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], $Class='warnMsgDiv')  ?>
			<?= $linterface->Get_Form_Warning_Msg('BusStopsNameChiDuplicateWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['Duplicate'], $Class='warnMsgDiv')  ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$requiredSym.$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameEng'] ?></td>
		<td>
			<?= $linterface->GET_TEXTBOX('BusStopsName', 'BusStopsName', $busStopsName, $OtherClass='requiredField textbox_name', $OtherPar=array('onchange'=>'checkDuplication(\'BusStopsName\',this.value,\''.$busStopsName.'\',\'BusStopsNameDuplicateWarnDiv\');')) ?>
			<?= $linterface->Get_Form_Warning_Msg('BusStopsNameEmptyWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], $Class='warnMsgDiv')  ?>
			<?= $linterface->Get_Form_Warning_Msg('BusStopsNameDuplicateWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['Duplicate'], $Class='warnMsgDiv')  ?>
		</td>
	</tr>
	
	
</table>
	<br style="clear:both;" />
	<?=$linterface->MandatoryField()?>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
<?= $htmlAry['hiddenField'] ?>
</form>
<?php
$linterface->LAYOUT_STOP();
?>