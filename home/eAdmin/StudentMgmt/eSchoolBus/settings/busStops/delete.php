<?php

$busStopsIDAry = $_POST['targetIdAry'];
$db = $indexVar['db'];
$tableName = "INTRANET_SCH_BUS_BUS_STOPS";
$fieldNameArr = array("IsDeleted");
$multiRowValueArr = array(array(1));
$conditions = " BusStopsID IN ('".implode("','",$busStopsIDAry)."') ";
$success = $db->updateData($tableName, $fieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);


if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}else{
	$returnMsg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}

$path = "index.php?task=settings/busStops&returnMsg=$returnMsg";
$indexVar['libSchoolBus']->redirect($path);

?>