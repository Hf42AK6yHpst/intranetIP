<?php
$linterface = $indexVar['linterface'];
$returnMsg = $indexVar['libSchoolBus']->getTempSession('SettingsBusStop');
$linterface->LAYOUT_START($returnMsg);

$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$numPerPage = ($numPerPage == '') ? 50 : $numPerPage;

$htmlAry['dbtable'] = $linterface->getBusStopsDbTable(order,$field,$pageNo,$numPerPage,$keyword);

### db table hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $linterface->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $numPerPage);
$htmlAry['hiddenField'] = $hiddenF;

### Buttons
$btnAry = array();
$btnAry[] = array('new', 'javascript: addNew();');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

### DB table action buttons
##dbTable button array
// $dbTableBtnAry[] = array($btnClass, $btnHref, $displayLang);
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

## Search Box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

?>
<script>
function addNew(){
	location.href = ("index.php?task=settings/busStops/edit");
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?= $Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseOnlyOne'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?= $Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseOnlyOne'] ?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?task=settings/busStops/edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseAtLeastOne'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['eSchoolBus']['Settings']['Warning']['ConfirmDelete'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?task=settings/busStops/delete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
</script>
<form id="form1" name="form1" method="post">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	<div class="table_filter">
		<?= $filter ?>
	</div>
	<p class="spacer"></p>
	<br style="clear:both;" />
	<br style="clear:both;" />
		
	<?=$htmlAry['dbTableActionBtn']?>
	<?= $htmlAry['dbtable']  ?>
	<?= $htmlAry['hiddenField']?>
</form>

<?php
$linterface->LAYOUT_STOP();
 ?>