<?php
$db = $indexVar['db'];
//Get Table Name e.g. INTRANET_SCH_BUS_BUS_STOPS_[AcademicYearID]
//$db->getAttendanceTableName();
$tableName = 'INTRANET_SCH_BUS_BUS_STOPS';

$busStopsID = $_POST['busStopsID'];
$busStopsName = $_POST['BusStopsName'];
$busStopsNameChi = $_POST['BusStopsNameChi'];


$fieldNameArr = array("BusStopsName","BusStopsNameChi");
$multiRowValueArr = array(array($busStopsName,$busStopsNameChi));
if($busStopsID){
	//Update
	$conditions = " BusStopsID = '$busStopsID' ";
	$success = $db->updateData($tableName, $fieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);
}else{
	//Insert
	$success = $db->insertData($tableName, $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
}


if($busStopsID){
	if($success){
		$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	}else{
		$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	}
}else{
	if($success){
		$returnMsg = $Lang['General']['ReturnMessage']['AddSuccess'];
	}else{
		$returnMsg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
	}
}
$is_edit = ($routeID)?true:false;
$indexVar['libSchoolBus']->setTempSession('SettingsBusStop',$returnMsg);
$path = "index.php?task=settings/busStops";
$indexVar['libSchoolBus']->redirect($path);

?>