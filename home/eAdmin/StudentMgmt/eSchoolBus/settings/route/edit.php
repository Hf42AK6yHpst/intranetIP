<?php
/*
 *  Using:
 *
 *  2019-08-08 Cameron
 *      - add academicYear selection when add new route [case #Y166091]
 *      - pass academicYearID in checkDuplication(), vehicle startDate and endDate ajax
 *      - fix normal route checking in checkRouteCollection()
 *
 *  2019-02-04 Cameron
 *      - change default type to teaching staff for teacher selection
 *      
 *  2019-01-31 Cameron
 *      - change to use $Lang for addTeacher thickbox title [case #Y154206]
 *      
 *  2019-01-29 Cameron
 *      - change route teacher to be able to select multiple
 *      - use window.location instead of history.back() in goCancel() to avoid showing wrong returnMsg 
 */
$linterface = $indexVar['linterface'];
$linterface->LAYOUT_START($returnMsg);
$db = $indexVar['db'];


include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
$fcm = new form_class_manage();
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYearObj = new academic_year($currentAcademicYearID);

$routeID = $_POST['targetIdAry'][0];
if($routeID){
	$routeInfo = $db->getRouteInfo($routeID);
// 	debug_pr($routeInfo);
	$routeName = $routeInfo[0]['RouteName'];
	$remarks = $routeInfo[0]['Remarks'];
	$type = $routeInfo[0]['Type'];
	$StartDate = $routeInfo[0]['StartDate'];
	$EndDate = $routeInfo[0]['EndDate'];
	$isActive = $routeInfo[0]['IsActive'];
	$academicYearID = $routeInfo[0]['AcademicYearID'];
// 	$teacherID = $routeInfo[0]['TeacherID'];
// 	$teacherName = $routeInfo[0]['TeacherName'];

	$routeTeacherList = $linterface->getRouteTeacherList($routeID);
	
// 	debug_pr($routeInfo);
	//Get Route Collection
	$routeCollections = $db->getRouteCollection($routeID);
	$routeCollections = BuildMultiKeyAssoc($routeCollections, "RouteCollectionID");
	
	$routeVehicle = $db->getRouteVehicle($routeID);	
// 	debug_pr($routeVehicle);

	$AmRouteCollection = array();
	$PmRouteCollection = array();
	
	$SepcialRouteCollections = array();
	$routeCollectionIDAry = array();
	
	// To assign routeCollections
	foreach ($routeCollections as $routeCollectionID => $routeCollectionAry){
		if($type=='N'){
			// Normal Route
			if($routeCollectionAry['AmPm']=='AM'){
				// AM
				$AmRouteCollection = $routeCollectionAry;
			}else if($routeCollectionAry['AmPm']=='PM'){
				//PM
				$PmRouteCollection = $routeCollectionAry;
			}
		}else{
			// Special Route
			$SepcialRouteCollections[$routeCollectionAry['Weekday']]=$routeCollectionAry;
		}
		$routeCollectionIDAry[] = $routeCollectionID;
	}
	
	//To get RouteCollects Stops
	$routeCollectionStopsAry = $db->getRouteCollectionStops($routeCollectionIDAry);
	$routeCollectionStopsAry = BuildMultiKeyAssoc($routeCollectionStopsAry, "RouteCollectionID",array(),0,1);

    $academicYearObj = new academic_year($academicYearID);
    $academicYearSelection = Get_Lang_Selection($academicYearObj->YearNameB5,$academicYearObj->YearNameEN);
    $academicYearSelection .= '<input type="hidden" name="academicYearID" id="academicYearID" value="'.$academicYearID.'">';
    $academicStartDate = $currentAcademicYearObj->AcademicYearStart;
    $academicEndDate = $currentAcademicYearObj->AcademicYearEnd;
}
else {
    // new route record
    $routeTeacherList = '';

    $academicYearID = IntegerSafe($_GET['academicYearID']);
    if ($academicYearID) {
        if ($academicYearID != $currentAcademicYearID) {
            $academicYearObj = new academic_year($academicYearID);
            if ($academicYearObj->AcademicYearStart < $currentAcademicYearObj->AcademicYearStart) {
                $academicYearID = $currentAcademicYearID;       // force to use current academic year if select past academic year
            }
            unset($academicYearObj);
        }
    }
    else {
        $academicYearID = $currentAcademicYearID;
    }
    $tag = 'class="requiredField"';
    $noFirst = '';
    $noPastYear = 1;
    $academicYearSelection = getSelectAcademicYear('academicYearID',$tag, $noFirst, $noPastYear, $academicYearID);
    $academicYearObj = new academic_year($academicYearID);
    $academicStartDate = $academicYearObj->AcademicYearStart;
    $academicEndDate = $academicYearObj->AcademicYearEnd;
}

($StartDate)? '':$StartDate = date("Y-m-d", strtotime($academicStartDate));
($EndDate)? '':$EndDate =  date("Y-m-d", strtotime($academicEndDate));

$linterface = $indexVar['linterface'];
$requiredSym = $linterface->RequiredSymbol();

### Stops Route
//AssoAry of both Am / Pm Route Collection by stops as key
$amRouteCollectionID = $AmRouteCollection['RouteCollectionID'];
$pmRouteCollectionID = $PmRouteCollection['RouteCollectionID'];

$amRouteCollectionStopsAry = $routeCollectionStopsAry[$amRouteCollectionID];
$pmRouteCollectionStopsAry = $routeCollectionStopsAry[$pmRouteCollectionID];

$amRouteCollectionStopsAry = BuildMultiKeyAssoc($amRouteCollectionStopsAry, "BusStopsID");
$pmRouteCollectionStopsAry = BuildMultiKeyAssoc($pmRouteCollectionStopsAry, "BusStopsID");

$countOfStops_N = count($amRouteCollectionStopsAry);


### Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeID', 'routeID', $routeID);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[AM]', 'routeCollectionID[AM]', $amRouteCollectionID);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[PM]', 'routeCollectionID[PM]', $pmRouteCollectionID);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[MON]', 'routeCollectionID[MON]', $SepcialRouteCollections['MON']['RouteCollectionID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[TUE]', 'routeCollectionID[TUE]', $SepcialRouteCollections['TUE']['RouteCollectionID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[WED]', 'routeCollectionID[WED]', $SepcialRouteCollections['WED']['RouteCollectionID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[THU]', 'routeCollectionID[THU]', $SepcialRouteCollections['THU']['RouteCollectionID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('routeCollectionID[FRI]', 'routeCollectionID[FRI]', $SepcialRouteCollections['FRI']['RouteCollectionID']);
//$hiddenF .= $linterface->GET_HIDDEN_INPUT('chooseIndividual', 'chooseIndividual', $teacherID);
$htmlAry['hiddenField'] = $hiddenF;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$pages_arr = array(
		array($Lang['eSchoolBus']['SettingsArr']['RouteArr']['MenuTitle'],'index.php?task=settings/route&academicYearID='.$academicYearID),
		array($routeID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);
?>
<?=$indexVar['linterface']->INCLUDE_COMMON_CSS_JS()?>
<?= $linterface->Include_Thickbox_JS_CSS(); ?>
<script>

var canSubmit = true;
function checkForm(){
	$('.warnMsgDiv').hide();
	localCanSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
            localCanSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});

	collectionCanSubmit = checkRouteCollection();

	return (localCanSubmit && collectionCanSubmit) ;
}
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    console.log(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
//		$('#TeacherID option').attr('selected',true);
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}
function goCancel(){
//	window.history.back();
	window.location = "index.php?task=settings/route&academicYearID=" + $('#academicYearID').val();
}

//page js
function clickTypeRadio(type){
	switch(type){
		case 'N':
			$('#normal_route').show();
			$('#special_route').hide();
		break;
		case 'S':
			$('#normal_route').hide();
			$('#special_route').show();
		break;
	}
}
var lastNum_N = <?=$countOfStops_N ?>;
function addNewStops(){
	var row = '<tr id="newRow_'+(++lastNum_N)+'"><td>'+lastNum_N+'</td><td id="new_busStops_Sel"></td><td id="new_am_time"></td><td id="new_pm_time"></td><td><a href="javascript:deleteNewRow('+lastNum_N+');">x</a></td></tr>';
	$('#busStops_table tbody').append(row);
	load_ajax_busStopsSel('busStops_N');
	load_ajax_timeSel('N');
	newRowNumber--;
}
function addNewStops_S(weekday){
	var row = '<tr id="newRow_'+(++lastNum_N)+'"><td>*</td><td id="new_busStops_Sel"></td><td id="new_special_time"></td><td><a href="javascript:deleteNewRow('+lastNum_N+');">x</a></td></tr>';
	$('#busStops_table_s_'+weekday+' tbody').append(row);
	load_ajax_busStopsSel('busStops_S['+weekday+']');
	load_ajax_timeSel('S','Time_'+weekday);
	newRowNumber--;
}
var newRowNumber = -1;
function load_ajax_busStopsSel(name){
	var td = $('#new_busStops_Sel');
	td.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?task=ajax/element",
		  type: "POST",
		  data: {
			action: 'busStopSelection',
			newRowNumber : newRowNumber,
			name:name
		  },
		  dataType: "html",
		  success: function(response){
			 td.html(response);
		  }
		});
	td.removeAttr( "id" );
}
function load_ajax_timeSel(type,name){
	switch(type){
	case 'N':
		var am = $('#new_am_time');
		var pm = $('#new_pm_time');
		am.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
		pm.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
		$.ajax({
			  url: "index.php?task=ajax/element",
			  type: "POST",
			  data: {
				action: 'timeSelection',
				newRowNumber : newRowNumber,
				name : 'Am_Time'
			  },
			  dataType: "html",
			  success: function(response){
				  am.html(response);
			  }
			});
		$.ajax({
			  url: "index.php?task=ajax/element",
			  type: "POST",
			  data: {
				action: 'timeSelection',
				newRowNumber : newRowNumber,
				name : 'Pm_Time'
			  },
			  dataType: "html",
			  success: function(response){
				  pm.html(response);
			  }
			});
		am.removeAttr( "id" );
		pm.removeAttr( "id" );
		break;
	case 'S':
		var timeDiv = $('#new_special_time');
		timeDiv.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
		$.ajax({
			  url: "index.php?task=ajax/element",
			  type: "POST",
			  data: {
				action: 'timeSelection',
				newRowNumber : newRowNumber,
				name : name
			  },
			  dataType: "html",
			  success: function(response){
				  timeDiv.html(response);
			  }
			});
		timeDiv.removeAttr( "id" );
		break;
	}
}
function deleteExistingRow(RouteStopsID){
	var deleteAry = [];
	$('#RouteStops_'+RouteStopsID).remove();
}
function deleteNewRow(num){
	$('#newRow_'+num).remove();
}
function deleteNewVehicle(num){
	$('#newVehicle_'+num).remove();
}
function deleteExistVehicle(vehicleID){
	
	var deleteAry = [];
	$('#hiddenArea').after(
			"<input type='hidden' name='deleteExistingVehicleID[]' value="+vehicleID+" />"
	);
	$('#existingVehicle_'+vehicleID).remove();
}
function checkRouteCollection(){

	var type = $('input[name="type"]:checked').val();
	var elements;
	if(type=='N'){
		$('#normal_route').each(function(){
			//elements = $(this).find(':input');
            elements = $(this).find(':input[name^="busStops_"]');
        });

		if(elements.length==0){
			$('#warning_route_N').html('<?=$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'] ?>');
			$('#warning_route_N').show();
			//TODO
			return false;
		}else{
		    var allRoutesEmpty = false;
			elements.each(function(e,f){    // e,f denotes key/val or index/val pair
				if(f.value==''){
				    allRoutesEmpty = true;
				}
			});
            if (allRoutesEmpty) {
                //something empty
                $('#warning_route_N').html('<?=$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'] ?>');
                $('#warning_route_N').show();
                return false;
            }
        }
	}else if(type=='S'){
		$('#special_route').each(function(){
			elements = $(this).find(':input');
		});
		if(elements.length==0){
			$('#warning_route_S').html('<?=$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'] ?>');
			$('#warning_route_S').show();
			return false;
		}else{
			elements.each(function(e,f){
				if(f.value==''){
					//something empty
					$('#warning_route_S').html('<?=$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'] ?>');
			$('#warning_route_S').show();
					return false;
				}
			});
		}
	}else{
		return false;
	}

	return true;
}
var newVehicleNum = -1;
function addNewVehicle(){
	var content = '<div id="newVehicle_'+newVehicleNum+'"><span id="new_vehicle_sel"></span> <?=$Lang['eSchoolBus']['General']['From'] ?> <span id="new_vehicle_start_date"></span> <?=$Lang['eSchoolBus']['General']['To'] ?> <span id="new_vehicle_end_date"></span><span id="new_vehicle_remarks"></span> <a href="javascript:deleteNewVehicle('+newVehicleNum+');">x</a></div><br><br>';
	$('#vehicle_div').append(content);
	
	var newVehicleSelDiv = $('#new_vehicle_sel');
	newVehicleSelDiv.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?task=ajax/element",
		  type: "POST",
		  data: {
			action: 'vehicleSelection',
			newVehicleNum : newVehicleNum,
			name : 'vehicleID'
		  },
		  dataType: "html",
		  success: function(response){
			  newVehicleSelDiv.html(response);
		  }
		});
	newVehicleSelDiv.removeAttr( "id" );

	var vehicleStartDate = $('#new_vehicle_start_date');
	vehicleStartDate.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?task=ajax/element",
		  type: "POST",
		  data: {
			action: 'DatePicker',
			newVehicleNum : newVehicleNum,
            academicYearID: $('#academicYearID').val(),
			name : 'vehicleStartDate',
			type: 'start'
		  },
		  dataType: "html",
		  success: function(response){
			  vehicleStartDate.html(response);
		  }
		});
	vehicleStartDate.removeAttr( "id" );

	var vehicleEndDate = $('#new_vehicle_end_date');
	vehicleEndDate.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?task=ajax/element",
		  type: "POST",
		  data: {
			action: 'DatePicker',
			newVehicleNum : newVehicleNum,
            academicYearID: $('#academicYearID').val(),
			name : 'vehicleEndDate',
			type: 'end'
		  },
		  dataType: "html",
		  success: function(response){
			  vehicleEndDate.html(response);
		  }
		});
	vehicleEndDate.removeAttr( "id" );

	var vehicleRemarks = $('#new_vehicle_remarks');
	vehicleRemarks.html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
		  url: "index.php?task=ajax/element",
		  type: "POST",
		  data: {
			action: 'textbox',
			newVehicleNum : newVehicleNum,
			name : 'vehicleRemarks'
		  },
		  dataType: "html",
		  success: function(response){
			  vehicleRemarks.html(response);
		  }
		});
	vehicleRemarks.removeAttr( "id" );
	newVehicleNum--;
}

function addTeacher(){
	load_dyn_size_thickbox_ip('<?php echo $Lang['eSchoolBus']['Settings']['Route']['AddTeacher'];?>', 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=750);
}
function onloadThickBox() {
	var selectedTeacherID = '';
	var teacherType;
	if ($(':input[name="RouteTeacherID\\[\\]"]').length ) {
		$(':input[name="RouteTeacherID\\[\\]"]').each(function(){
			if (selectedTeacherID != '' ) {
				selectedTeacherID += ',';
			}
			selectedTeacherID += $(this).val();
		});
	}

	if ($('select#TeacherType').length){
		teacherType = $('select#TeacherType').val();
	}
	else {
		teacherType = '1';
	}
	
	$('div#TB_ajaxContent').load(
			"index.php?task=settings/route/add_route_teacher", 
			{ 
				selectedTeacherID : selectedTeacherID,
				teacherType : teacherType
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}
function getNotAvalUser(){
	//return empty array for template/add_user.php
	var notavalUser = [];
	
	return notavalUser;
}
function Add_Selected_User(userID,name){
	userID = userID.substring(1);
	addToChooseIndividual(userID,name);
}
function Add_Role_Member(){
	var userID = $('[name="AvalUserList[]"] option:selected').val();
	var name = $('[name="AvalUserList[]"] option:selected').text();
	addToChooseIndividual(userID,name);
}
function addToChooseIndividual(userID,name){
	$('#chooseIndividual').val(userID);
	$('#chooseIndividualSpan').html(name);	
	window.top.tb_remove(); 
}
function checkDuplication(dbField,value,excludeValue,warningBoxId){
	canSubmit = true;
	$('.warnMsgDiv').hide();
	$.post(
			'index.php?task=ajax/checkDuplicates',
			{
				'dbField': dbField,
				'value':value.trim(),
				'excludeValue':excludeValue,
                'academicYearID':$('#academicYearID').val()
			},
			function(returnHtml){
				if(returnHtml=='OK'){
					
				}else if(returnHtml=='DUPLICATED'){
					$('#'+warningBoxId).show('fast');
					canSubmit = false;
				}
			}
		);
	
}

function deleteRouteTeacher(teacherID)
{
	$('div#TeacherDiv_' + teacherID).remove();
}


</script>
<?=$indexVar['linterface']->GET_NAVIGATION_IP25($pages_arr)?>
<form name='form1' id='form1' method="POST" action="index.php?task=settings/route/editUpdate">
<div class="table_board">
	<table class="form_table_v30">

    <tr>
        <td class="field_title"><?php echo $requiredSym . $Lang['General']['SchoolYear']; ?></td>
        <td>
            <?php echo $academicYearSelection; ?>
            <?= $linterface->Get_Form_Warning_Msg('academicYearIDEmptyWarnDiv', $Lang['General']['JS_warning']['SelectSchoolYear'], $Class='warnMsgDiv')  ?>
        </td>
    </tr>

	<tr>
		<td class="field_title"><?=$requiredSym. $Lang['eSchoolBus']['Settings']['Route']['RouteName'] ?></td>
		<td>
			<?= $linterface->GET_TEXTBOX('RouteName', 'RouteName', $routeName, $OtherClass='requiredField textbox_name', $OtherPar=array('onchange'=>'checkDuplication(\'RouteName\',this.value,\''.$routeName.'\',\'RouteNameDuplicateWarnDiv\');')) ?>
			<?= $linterface->Get_Form_Warning_Msg('RouteNameEmptyWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], $Class='warnMsgDiv')  ?>
			<?= $linterface->Get_Form_Warning_Msg('RouteNameDuplicateWarnDiv', $Lang['eSchoolBus']['Settings']['Warning']['Duplicate'], $Class='warnMsgDiv')  ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Route']['Remarks'] ?></td>
		<td>
			<?= $linterface->GET_TEXTAREA('remarks', $remarks, $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='') ?>
			
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$requiredSym.$Lang['eSchoolBus']['Settings']['Route']['Type'] ?></td>
		<td>
			<div id="radio_div">
			<?php
				$n_checked = 1;
				$s_checked = 0;
				if($type=='S'){
					$n_checked = 0;
					$s_checked = 1;
				}
			?>
			<?= $linterface->Get_Radio_Button('type_N', 'type', $Value='N', $n_checked, $Class="", $Display=$Lang['eSchoolBus']['Settings']['Route']['NormalRoute'], $Onclick="clickTypeRadio(this.value);",$isDisabled=0) ?>
			<?= $linterface->Get_Radio_Button('type_S', 'type', $Value='S', $s_checked, $Class="", $Display=$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute'], $Onclick="clickTypeRadio(this.value);",$isDisabled=0) ?>
			</div>
			<div id="normal_route" <?= ($type=='S')?' style="display:none"':'' ?>>
				<!-- Table for normal route -->
				<table id="busStops_table">
					<thead>
						<tr>
							<th> </th><th><?=$Lang['eSchoolBus']['Settings']['Route']['BusStop'] ?></th><th><?=$Lang['eSchoolBus']['Settings']['Route']['AmTime'] ?></th><th><?=$Lang['eSchoolBus']['Settings']['Route']['PmTime'] ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						for($i=0;$i<$countOfStops_N;$i++){
// 							debug_pr($routeCollectionStopsAry);
							$_busStopsID = $routeCollectionStopsAry[$AmRouteCollection['RouteCollectionID']][$i]['BusStopsID'];
							$_RouteStopsID_am = $routeCollectionStopsAry[$AmRouteCollection['RouteCollectionID']][$i]['RouteStopsID'];
							$_RouteStopsID_pm = $routeCollectionStopsAry[$PmRouteCollection['RouteCollectionID']][($countOfStops_N-$i-1)]['RouteStopsID'];
						?>
						
						<tr id="RouteStops_<?=$_RouteStopsID_am ?>">
							<td><?= ($i+1) ?></td>
							<td><?= $linterface->getBusStopsSelection($_busStopsID,'name="busStops_N[]"'); ?></td>
							
							<td><?= $linterface->Get_Time_Selection('Am_Time', $DefaultTime=$amRouteCollectionStopsAry[$_busStopsID]['Time'], $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1), $parObjId='') ?></td>
							<td><?= $linterface->Get_Time_Selection('Pm_Time', $DefaultTime=$pmRouteCollectionStopsAry[$_busStopsID]['Time'], $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1), $parObjId='') ?></td>
							<td><a href="javascript:deleteExistingRow(<?=$_RouteStopsID_am ?>);deleteExistingRow(<?=$_RouteStopsID_pm ?>);">x</a></td>
						</tr>
						
						<?php
						}
						?>
						
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td><?=$Lang['eSchoolBus']['Settings']['Route']['ArriveOrLeaveTime'] ?></td>
							<?php
							$am_arrival_time = $routeCollections[$AmRouteCollection['RouteCollectionID']]['Time'];
							$pm_leave_time = $routeCollections[$PmRouteCollection['RouteCollectionID']]['Time'];
							?>
							<td><?= $linterface->Get_Time_Selection('Am_Arrive_Leave_Time', $DefaultTime=$am_arrival_time, $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1)) ?></td>
							<td><?= $linterface->Get_Time_Selection('Pm_Arrive_Leave_Time', $DefaultTime=$pm_leave_time, $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1)) ?></td>
						</tr>
					</tfoot>
				</table>
				<a href="javascript:addNewStops()">+</a>
				<br>
				<?= $linterface->Get_Form_Warning_Msg('warning_route_N', $Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], $Class='warnMsgDiv tabletextrequire')  ?>
			</div>
			<div id="special_route" <?= ($type!='S')?' style="display:none"':'' ?>>
				<?php 
				$specialRouteKeyAry = array('MON','TUE','WED','THU','FRI');
				for($i=0;$i<5;$i++){
					switch($specialRouteKeyAry[$i]){
						case 'MON':
						$tableClass ="class='Monday'";	
						break;
						case 'TUE':
							$tableClass ="class='Tuesday'";
						break;
						case 'WED':
							$tableClass ="class='Wednesday'";
						break;
						case 'THU':
							$tableClass ="class='Thursday'";
						break;
						case 'FRI':
							$tableClass ="class='Friday'";
						break;
					}
				?>
				
					<table <?=$tableClass ?> style="width:300px;">
						<tr>
							<th><?= $Lang['eSchoolBus']['Settings']['Route']['Weekday'][$specialRouteKeyAry[$i]] ?></th>
						</tr>
						<tr>			
							<td>
								<div id="busStop_div_<?=$specialRouteKeyAry[$i] ?>">
									<table id="busStops_table_s_<?=$specialRouteKeyAry[$i] ?>">
										<thead>
											<tr>
												<th> </th><th><?=$Lang['eSchoolBus']['Settings']['Route']['BusStop'] ?></th><th><?=$Lang['eSchoolBus']['Settings']['Route']['PmTime'] ?></th>
											</tr>
											<tr>
											<td></td>
											<td><?=$Lang['eSchoolBus']['Settings']['Route']['LeaveTime'] ?></td>
											<td><?= $linterface->Get_Time_Selection($specialRouteKeyAry[$i].'_Leave_Time', $DefaultTime=$SepcialRouteCollections[$specialRouteKeyAry[$i]]['Time'], $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1)) ?></td>
											</tr>
										</thead>
										<tbody>
											<?php
												$thisRouteCollectionInfo = $SepcialRouteCollections[$specialRouteKeyAry[$i]];
												$thisRouteCollectionID = $thisRouteCollectionInfo['RouteCollectionID'];
												$thisRouteCollectionStopsAry = $routeCollectionStopsAry[$thisRouteCollectionID];
												foreach((array)$thisRouteCollectionStopsAry as $key => $routeStopsInfo){
											?>
													<tr id="RouteStops_<?=$routeStopsInfo['RouteStopsID'] ?>">
														<td><?=($key+1) ?></td>
														<td><?= $linterface->getBusStopsSelection($routeStopsInfo['BusStopsID'],'name="busStops_S['.$specialRouteKeyAry[$i].'][]"'); ?></td>
														<td><?= $linterface->Get_Time_Selection('Time_'.$specialRouteKeyAry[$i], $DefaultTime=$routeStopsInfo['Time'], $others_tab='',$hideSecondSeletion=1, $intervalArr=array(1,1,1), $parObjId='') ?></td>
														<td><a href="javascript:deleteExistingRow(<?=$routeStopsInfo['RouteStopsID'] ?>);">x</a></td>
													</tr>
											<?php		
												}
											?>
										</tbody>
									</table>
								</div>
							</td>	
						</tr>
						<tfoot>
						<tr><td><a href="javascript:addNewStops_S('<?= $specialRouteKeyAry[$i] ?>')">+</a></td></tr>
						</tfoot>
					</table>
					<br>
				<?php	
				}
				?>
				<?= $linterface->Get_Form_Warning_Msg('warning_route_S', $Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], $Class='warnMsgDiv tabletextrequire')  ?>
			</div>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$requiredSym.$Lang['eSchoolBus']['Settings']['Route']['Dates'] ?></td>
		<td>
			<label><?=$Lang['eSchoolBus']['Settings']['Route']['StartDate'] ?></label>
			<?= $linterface->GET_DATE_PICKER('StartDate', $StartDate); ?>
			
			<label><?=$Lang['eSchoolBus']['Settings']['Route']['EndDates'] ?></label>
			<?= $linterface->GET_DATE_PICKER('EndDate', $EndDate); ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$requiredSym ?><?=$Lang['eSchoolBus']['Settings']['Route']['IsActive'] ?></td>
		<td>
			<?= $linterface->Get_Radio_Button('status_1', 'status', $Value=1, $isChecked=($isActive==1||empty($routeID)) ,$Class="", $Display=$Lang['eSchoolBus']['Settings']['Route']['Status']['Active'], $Onclick="",$isDisabled=0) ?>
			<?= $linterface->Get_Radio_Button('status_0', 'status', $Value=0, $isChecked=(!$isActive && !empty($routeID)), $Class="", $Display=$Lang['eSchoolBus']['Settings']['Route']['Status']['Inactive'], $Onclick="",$isDisabled=0) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Route']['Teacher'] ?></td>
		<td>
<!--			
			<a href="javascript:addTeacher();" ><?= $Lang['Btn']['Edit'] ?></a>
-->
			<span id="selectedTeacherSpan"><?php echo $routeTeacherList;?></span>
			<a href="javascript:addTeacher();">+</a>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?= $Lang['eSchoolBus']['SettingsArr']['VehicleArr']['MenuTitle'] ?></td>
		<td>
			<div id="vehicle_div">
			<?php 
			$countOfVehicle = count($routeVehicle);
			if($countOfVehicle>0){
				foreach ($routeVehicle as $_routeVehicle){
			?>
			<div id="existingVehicle_<?=$_routeVehicle['RouteVehicleID'] ?>">
			<?=$linterface->getVehicleSelection($tags='name="vehicleID['.$_routeVehicle['RouteVehicleID'].']"', $selected=$_routeVehicle['VehicleID'], $all=0, $noFirst=0, $FirstTitle="") ?>
			<?=$Lang['eSchoolBus']['General']['From'] ?>
			<?=$linterface->GET_DATE_PICKER($Name='vehicleStartDate['.$_routeVehicle['RouteVehicleID'].']',$DefaultValue=$_routeVehicle['StartDate'],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID='vehicleStartDate_'.$_routeVehicle['RouteVehicleID'],$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum") ?>
			<?=$Lang['eSchoolBus']['General']['To'] ?>
			<?=$linterface->GET_DATE_PICKER($Name='vehicleEndDate['.$_routeVehicle['RouteVehicleID'].']',$DefaultValue=$_routeVehicle['EndDate'],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID='vehicleEndDate_'.$_routeVehicle['RouteVehicleID'],$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum") ?>
			<?=$linterface->GET_TEXTBOX('vehicleRemarks_'.$_routeVehicle['RouteVehicleID'], 'vehicleRemarks['.$_routeVehicle['RouteVehicleID'].']', $_routeVehicle['Remarks'], $OtherClass='', $OtherPar=array('maxlength'=>255,'style'=>'width:200px','placeholder'=>$Lang['eSchoolBus']['Settings']['Route']['Remarks'])) ?>
			<a href="javascript:deleteExistVehicle(<?= $_routeVehicle['RouteVehicleID']?>);">x</a>
			</div>
			<br>
			<br>
			<?php
				}
			}else{
				
			}
			?>
			</div>
			<a href="javascript:addNewVehicle();">+</a>
		</td>
	</tr>
</table>
	<br style="clear:both;" />
	<?=$linterface->MandatoryField()?>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
<div id="hiddenArea"><?= $htmlAry['hiddenField'] ?></div>
</form>
<?php
$linterface->LAYOUT_STOP();
?>