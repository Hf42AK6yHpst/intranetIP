<?php
/*
 *  2019-08-09 Cameron
 *      - pass academicYearID to return
 */
$routeIDIDAry = $_POST['targetIdAry'];
$academicYearID = IntegerSafe($_POST['academicYearID']);
$db = $indexVar['db'];
$tableName = "INTRANET_SCH_BUS_ROUTE";
$fieldNameArr = array("IsDeleted");
$multiRowValueArr = array(array(1));
$conditions = " RouteID IN ('".implode("','",$routeIDIDAry)."') ";
$success = $db->updateData($tableName, $fieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);


if($success){
	
	$returnMsg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}else{
	$returnMsg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}
$indexVar['libSchoolBus']->setTempSession('SettingsRoute',$returnMsg);
$path = "index.php?task=settings/route&academicYearID=".$academicYearID;
$indexVar['libSchoolBus']->redirect($path);

?>