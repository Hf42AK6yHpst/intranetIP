<?php 
/*
 *  2018-06-06 Cameron
 *      - create this file
 */

$linterface = $indexVar['linterface'];
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
if ($returnMsgKey) {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
else {
    $retrnMsg = '';
}
$linterface->LAYOUT_START($returnMsg);

$db = $indexVar['db'];
$isApplyLeaveCutoffTimeSettings = $db->isApplyLeaveCutoffTimeSettings();
$cutoffTimeYes = '';
$cutoffTimeNo = '';
if ($isApplyLeaveCutoffTimeSettings) {
    $cutoffTimeYes = ' checked ';   // contain space to avoid concating string to previous and next string after format php
}
else {
    $cutoffTimeNo = ' checked ';
}
$daysBeforeApplyLeave = $db->getSchoolBusSettings('daysBeforeApplyLeave');

$classGroupAry = $db->getClassGroup();
$cutoffTimeSettingsAry = $db->getApplyLeaveCutoffTimeSettings();

$linterface = $indexVar['linterface'];

### action buttons
$htmlAry['editBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel();",'cancelBtn');

?>

<style>
.editTable{
	display:none;
}
</style>
<script language="javascript">
$( document ).ready(function() {
  	$('#submitBtn').hide();
  	$('#cancelBtn').hide();
<?php if (!$isApplyLeaveCutoffTimeSettings):?>
	$('#cutoffTimingRow').hide();
<?php endif;?>
  	
  	$("input:radio[name='enableApplyLeaveCutOffTiming']").change(function(){
  	  	if ($(this).val() == '1') {
			$('#cutoffTimingRow').show();
  	  	}
  	  	else {
  	  		$('#cutoffTimingRow').hide();
  	  	}
  	});
});

function goEdit(){
	$('.editTable').show();
	$('.viewTable').hide();
	$('#editBtn').hide();
	$('#submitBtn').show();
	$('#cancelBtn').show();
}

function goSubmit(){
	$('form#form1').attr('action', 'index.php?task=settings/cutoffTime/editUpdate').submit();
}

function goCancel(){
	document.form1.reset();
	$('.editTable').hide();
	$('.viewTable').show();
	$('#submitBtn').hide();
    $('#cancelBtn').hide();
    $('#editBtn').show();
}
</script>

<form name='form1' id='form1' method="POST">
<div class="table_board">
	<table class="form_table_v30">
	<tr>
		<td class="field_title"><?php echo $Lang['StudentAttendance']['ApplyLeaveAry']['enableApplyLeaveCutOffTiming']; ?></td>
		<td class="viewTable">
			<?php echo $isApplyLeaveCutoffTimeSettings ? $Lang['General']['Yes']: $Lang['General']['No'];?>
		</td>
		<td class="editTable">
			<input type="radio" name="enableApplyLeaveCutOffTiming" id="enableApplyLeaveCutOffTimingYes" value="1" <?php echo $cutoffTimeYes;?>><label for="enableApplyLeaveCutOffTimingYes"><?php echo $Lang['General']['Yes'];?></label>&nbsp;
			<input type="radio" name="enableApplyLeaveCutOffTiming" id="enableApplyLeaveCutOffTimingNo" value="0" <?php echo $cutoffTimeNo;?>><label for="enableApplyLeaveCutOffTimingNo"><?php echo $Lang['General']['No'];?></label>
		</td>
	</tr>

	<tr id="cutoffTimingRow">
		<td class="field_title"><?php echo $Lang['eSchoolBus']['Settings']['ApplyLeave']['ApplyLeaveCutOffTiming']; ?></td>
		<td>
			<table class="common_table_list_v30">
				<thead>
					<tr>
						<td class="rights_not_select_sub" width="33%"><?php echo $Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'];?></td>
						<td class="rights_not_select_sub" width="33%"><?php echo $Lang['eSchoolBus']['Settings']['ApplyLeave']['GotoSchool'];?></td>
						<td class="rights_not_select_sub" width="33%"><?php echo $Lang['eSchoolBus']['Settings']['ApplyLeave']['LeaveSchool'];?></td>
					</tr>
				</thead>
				<tbody>
    			<?php foreach((array)$classGroupAry as $classGroupID => $groupName):?>
    			<?php    $gotoSchoolHour = 'gotoSchoolHour_'.$classGroupID;
    			         $gotoSchoolMin = 'gotoSchoolMin_'.$classGroupID;
    			         $leaveSchoolHour = 'leaveSchoolHour_'.$classGroupID;
    			         $leaveSchoolMin = 'leaveSchoolMin_'.$classGroupID;
    			?>
        			<tr>
        				<td><?php echo $groupName;?></td>
        				<td class="viewTable"><?php echo $cutoffTimeSettingsAry[$classGroupID]['1'];?></td>
        				<td class="viewTable"><?php echo $cutoffTimeSettingsAry[$classGroupID]['2'];?></td>
        				<td class="editTable">
        				<?php echo $linterface->Get_Time_Selection_Box($gotoSchoolHour, 'hour', substr($cutoffTimeSettingsAry[$classGroupID]['1'],0,2), $__others_tab='', $__interval=1, $gotoSchoolHour).':'.$linterface->Get_Time_Selection_Box($gotoSchoolMin, 'min', substr($cutoffTimeSettingsAry[$classGroupID]['1'],3,2), $__others_tab='', $__interval=1, $gotoSchoolMin);?>
        				</td>
        				<td class="editTable">
						<?php echo $linterface->Get_Time_Selection_Box($leaveSchoolHour, 'hour', substr($cutoffTimeSettingsAry[$classGroupID]['2'],0,2), $__others_tab='', $__interval=1, $leaveSchoolHour).':'.$linterface->Get_Time_Selection_Box($leaveSchoolMin, 'min', substr($cutoffTimeSettingsAry[$classGroupID]['2'],3,2), $__others_tab='', $__interval=1, $leaveSchoolMin);?>        				
        				</td>
        			</tr>
    			<?php endforeach;?>
    			</tbody>
			</table>
		</td>
	</tr>
	
	<tr>
		<td class="field_title"><?php echo $Lang['eSchoolBus']['Settings']['ApplyLeave']['DaysBeforeApplyLeave']; ?></td>
		<td class="viewTable">
			<?php echo $daysBeforeApplyLeave?>
		</td>
		<td class="editTable">
			<input type="number" name="daysBeforeApplyLeave" value="<?php echo $daysBeforeApplyLeave?>"  min="0"/>
		</td>
	</tr>
	
</table>
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>
<?php
$linterface->LAYOUT_STOP();
?>