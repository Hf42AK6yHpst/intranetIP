<?php


$returnMsg = $indexVar['libSchoolBus']->getTempSession('SettingsVehicle');

$CurrentPage = 'settings/vehicle';

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$keyword = trim(urldecode(stripslashes($_REQUEST['keyword'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_eschoolbus_settings_vehicle_page_size", "numPerPage");
$arrCookies[] = array("ck_eschoolbus_settings_vehicle_page_number", "pageNo");
$arrCookies[] = array("ck_eschoolbus_settings_vehicle_page_order", "order");
$arrCookies[] = array("ck_eschoolbus_settings_vehicle_page_field", "field");	
$arrCookies[] = array("ck_eschoolbus_settings_vehicle_page_keyword", "keyword");
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 1;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;
if(!isset($numPerPage) || $numPerPage == '') $numPerPage = 50;

$pageSizeChangeEnabled = true;


$columns = array(
	array('CarNum',$Lang['eSchoolBus']['Settings']['Vehicle']['CarNumber'],'16%'),
	array('CarPlateNum',$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber'],'16%'),
	array('DriverName',$Lang['eSchoolBus']['Settings']['Vehicle']['Driver'],'16%'),
	array('DriverTel',$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'],'16%'),
	array('Seat',$Lang['eSchoolBus']['Settings']['Vehicle']['NumberOfSeat'],'10%'),
	array('RecordStatus',$Lang['eSchoolBus']['Settings']['Vehicle']['Status'],'10%'),
	array('DateModified',$Lang['General']['LastModified'],'16%')
);

$column_ary = array(18,18,18,18,13,18,18);

$sql = "SELECT 
			CarNum,
			CarPlateNum,
			DriverName,
			DriverTel,
			Seat,
			IF(RecordStatus='1','".$Lang['eSchoolBus']['Settings']['Vehicle']['Active']."',CONCAT('<span style=\"color:red\">','".$Lang['eSchoolBus']['Settings']['Vehicle']['Inactive']."','</span>')) as RecordStatus,
			DateModified,
			CONCAT('<input type=\"checkbox\" name=\"VehicleID[]\" value=\"',VehicleID,'\" />') as CheckBox 
		FROM INTRANET_SCH_BUS_VEHICLE 
		WHERE RecordStatus<>'0' ";
if($keyword != ''){
	$esc_keyword = $indexVar['db']->Get_Safe_Sql_Like_Query($keyword);
	$sql .= " AND (CarNum LIKE '%$esc_keyword%' OR CarPlateNum LIKE '%$esc_keyword%' OR DriverName LIKE '%$esc_keyword%')";
}
//debug_pr($sql);
$table_content = $indexVar['linterface']->getDbTable($sql, $columns, $field, $order, $pageNo, $numPerPage, true, $column_ary, 'VehicleID[]');

$tool_buttons = array();
$tool_buttons[] = array('edit','javascript:checkEdit(document.form1,\'VehicleID[]\',\'index.php?task=settings/vehicle/edit\');','','','');
$tool_buttons[] = array('delete','javascript:checkRemove(document.form1,\'VehicleID[]\',\'index.php?task=settings/vehicle/delete\')','','');

$indexVar['linterface']->LAYOUT_START($returnMsg);
?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$indexVar['linterface']->GET_LNK_NEW('index.php?task=settings/vehicle/edit', $Lang['Btn']['New'], $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1)?>
		</div>
		<div class="Conntent_search"><input type="text" id="keyword" name="keyword" value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>"></div>
		<br style="clear:both;">
	</div>
	<div class="table_filter">
		
	</div>
	<br style="clear:both">	
	<?=$indexVar['linterface']->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$table_content?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$order?>">
	<input type="hidden" id="field" name="field" value="<?=$field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$numPerPage?>">
</form>	
<br /><br />
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>