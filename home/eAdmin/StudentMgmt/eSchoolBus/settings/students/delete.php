<?php
/*
 *  2019-08-09 Cameron
 *      - apply IntegerSafe
 *      - pass $AcademicYearID for redirect
 */
$StudentID = IntegerSafe($_POST['StudentID']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$FormID = IntegerSafe($_POST['FormID']);

$success = $indexVar['db']->permanentDeleteData('INTRANET_SCH_BUS_USER_ROUTE', 'UserID', $StudentID," AND AcademicYearID='".$AcademicYearID."' ");
// also delete special arrangements when there are no normal arrangements
$success = $success && $indexVar['db']->permanentDeleteData('INTRANET_SCH_BUS_SPEICAL_ARRANGEMENT', 'UserID', $StudentID," AND AcademicYearID='".$AcademicYearID."' ");

$indexVar['libSchoolBus']->setTempSession('SettingsStudents',$success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess']);
$indexVar['libSchoolBus']->setTempSession('SettingsStudents_AcademicYearID', $AcademicYearID);
$indexVar['libSchoolBus']->setTempSession('SettingsStudents_FormID', $FormID);

intranet_closedb();
$indexVar['libSchoolBus']->redirect("index.php?task=settings/students&AcademicYearID=".$AcademicYearID);
?>