<?php
// Editing by 
/*
 * !!!!! Please edit this file with UTF-8 encoding !!!!!
 */

include_once($intranet_root."/includes/libexporttext.php");

$lexport = new libexporttext();

$filename = "eSchoolBus_student_settings_import_sample.csv";
$file_format = array('Login ID','AM Normal Route','AM Normal Route Bus Stop','PM Normal Route','PM Normal Route Bus Stop',
					'Monday ASA Route Activity','Monday ASA Route','Monday ASA Route Bus Stop',
					'Tueday ASA Route Activity','Tueday ASA Route','Tueday ASA Route Bus Stop',
					'Wednesday ASA Route Activity','Wednesday ASA Route','Wednesday ASA Route Bus Stop',
					'Thursday ASA Route Activity','Thursday ASA Route','Thursday ASA Route Bus Stop',
					'Friday ASA Route Activity','Friday ASA Route','Friday ASA Route Bus Stop',
					'Start Date','End Date');
$samples = array();
$samples[] = array('s160001','Normal route 1','Bus stop 1','Normal route 1','Bus stop 2',
					$schoolBusConfig['ActivityTypes'][0][1],'ASA route 1','Bus stop 3',
					'','','',
					$schoolBusConfig['ActivityTypes'][1][1],$Lang['eSchoolBus']['Settings']['Student']['ByParent'],'',
					'','','',
					'','','',
					'2015-09-01','2016-07-08');

	
$content = $lexport->GET_EXPORT_TXT($samples, $file_format, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename,$content);

?>