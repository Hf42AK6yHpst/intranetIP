<?php
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');

$db = $indexVar['db'];
$academicYearID = $_POST['academicYearID'];
$type = $_POST['type'];
$weekdayAry = $_POST['weekday'];
$format =  $_POST['Format'];
// debug_pr($format);



//Get All Route of Certain type / weekday of the route

$filterAry = array();
$additionalCond = '';
$filterAry['route.AcademicYearID'] = $academicYearID;
if($type=='S'){
	$additionalCond .= " AND ";
	$additionalCond .= " collection.Weekday IN ('".implode("','",$weekdayAry)."') ";
}
$routeInfo = $db->getRouteInfo($routeIDAry='',$isDeleted=0,$type=$type,$filterAry,$additionalCond);
$routeCollectionIdAry = Get_Array_By_Key($routeInfo, "RouteCollectionID");
$routeCollectionIdAry = array_values(array_unique($routeCollectionIdAry));
$busStopIdAry = Get_Array_By_Key($routeInfo, "BusStopsID");
$busStopIdAry = array_unique($busStopIdAry);

if($type=='N'){
	$routeInfo = BuildMultiKeyAssoc($routeInfo, array('RouteID','AmPm'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
}else{
	$routeInfo = BuildMultiKeyAssoc($routeInfo, array('RouteID','Weekday'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
}

//Get RouteCollectionAry 
$routeCollectionAry = $db->getRouteCollection(array(),$routeCollectionIdAry);
$routeCollectionAry = BuildMultiKeyAssoc($routeCollectionAry, array('RouteCollectionID'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);
// debug_pr($routeCollectionAry);

//Get User Count for each route collection and stops
$userCountAry = $db->getRouteCollectionUserCount($routeCollectionIdAry);
$userCountAry = BuildMultiKeyAssoc($userCountAry, array('RouteCollectionID','BusStopsID'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);

//Get BusStops Info
$busStopInfoAry = $db->getBusStopsInfo($busStopIdAry);
$busStopInfoAry = BuildMultiKeyAssoc($busStopInfoAry, array('BusStopsID'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);


//Generate XLS
if($format=='EXCEL'){
	$currentPageAry = array(
			'AM'=>array('A',1),
			'PM'=>array('A',1),
			'MON'=>array('A',1),
			'TUE'=>array('A',1),
			'WED'=>array('A',1),
			'THU'=>array('A',1),
			'FRI'=>array('A',1)
	);
	
	
	/** PHPExcel_IOFactory */
	include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
	include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");
	$objPHPExcel = new PHPExcel();
	// Set properties
	$objPHPExcel->getProperties()->setCreator("eClass")
	->setLastModifiedBy("eClass")
	->setTitle("eClass eSchoolBus Route Arrangement")
	->setSubject("eClass eSchoolBus Route Arrangement")
	->setDescription("eClass eSchoolBus Route Arrangement")
	->setKeywords("eClass eSchoolBus Route Arrangement")
	->setCategory("eClass");
	
	$objPHPExcel = new PHPExcel();
	if($type=='N'){
		//Create xls sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$ActiveSheet['AM'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['AM']->setTitle('AM');
		
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(1);
		$ActiveSheet['PM'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['PM']->setTitle('PM');
	}else{
		$objPHPExcel->setActiveSheetIndex(0);
		$ActiveSheet['MON'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['MON']->setTitle('MON');
		
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(1);
		$ActiveSheet['TUE'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['TUE']->setTitle('TUE');
		
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(2);
		$ActiveSheet['WED'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['WED']->setTitle('WED');
		
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(3);
		$ActiveSheet['THU'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['THU']->setTitle('THU');
		
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(4);
		$ActiveSheet['FRI'] = $objPHPExcel->getActiveSheet();
		$ActiveSheet['FRI']->setTitle('FRI');
	}
	foreach ((array)$routeInfo as $_routeID => $_routeAryByWeekday){
		$_sumOfCount = 0;
		foreach($_routeAryByWeekday as $__AmPmOrWeekDay => $__stopsAry){
// 			debug_pr($__stopsAry);
			if($type=='N'){
				$_weekday_or_type = $Lang['eSchoolBus']['Settings']['Route']['NormalRoute'];
			}else{
				$_weekday_or_type = $Lang['eSchoolBus']['Settings']['Route']['Weekday'][$__AmPmOrWeekDay];
			}
			$__startBox = getCurrentPage($__AmPmOrWeekDay);
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(getCurrentPage($__AmPmOrWeekDay), $_weekday_or_type);
			$__routeCollectionID = $__stopsAry[0]['RouteCollectionID'];
			$__arrive_leave_time = date('H:i',strtotime($routeCollectionAry[$__routeCollectionID][Time]));
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $__stopsAry[0]['RouteName'].' ('.$__arrive_leave_time.')');
			$ActiveSheet[$__AmPmOrWeekDay]->mergeCells(getCurrentPage($__AmPmOrWeekDay).':'.nextColumn($__AmPmOrWeekDay,true));
			nextRow($__AmPmOrWeekDay);
			
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(getCurrentPage($__AmPmOrWeekDay), $Lang['eSchoolBus']['Settings']['BusStops']['BusStopName']);
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent']);
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $Lang['eSchoolBus']['Report']['RouteArrangement']['Time']);
			nextRow($__AmPmOrWeekDay);
			foreach ($__stopsAry as $___stopsInfo){
				
				$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(getCurrentPage($__AmPmOrWeekDay), $busStopInfoAry[$___stopsInfo['BusStopsID']][Get_Lang_Selection('BusStopsNameChi','BusStopsName')]);
				$__count = $userCountAry[$___stopsInfo['RouteCollectionID']][$___stopsInfo['BusStopsID']]['CountUser'];
				if(!$__count){
					$__count = 0;
				}
				$_sumOfCount = $_sumOfCount + $__count;
				$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $__count);
				$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), date('h:i',strtotime($___stopsInfo['Time'])));
				nextRow($__AmPmOrWeekDay);
			}
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $_sumOfCount);
			$_sumOfCount = 0;
			
			$__endBox = nextColumn($__AmPmOrWeekDay);
			setBorder($ActiveSheet[$__AmPmOrWeekDay],$__startBox,$__endBox);
			
			nextRow($__AmPmOrWeekDay);
			nextRow($__AmPmOrWeekDay);
		}
	}
	
	
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	$dirPath = $PATH_WRT_ROOT.'file/eSchoolBus';
	if(!is_dir($dirPath)){
		mkdir($dirPath);
	}
	
	$fileName = 'eSchoolBus_routeArrangement_'.$type.'_'.$_SESSION['UserID'].'.xls';
	$exportFileName = 'eSchoolBus_routeArrangement_'.$type.'.xls';
	
	$path2write = $dirPath.'/'.$fileName;
	$successArr[]= $objWriter->save($path2write);
	output2browser(get_file_content($path2write), $exportFileName);
}else{
	//HTML
	$x .= '<table width="98%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$indexVar['linterface']->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
					</tr>
				</table>';
	$x .= '<div></div>';
	$is_print  =true;
	$header_class = $is_print ? ' class="eSporttdborder eSportprinttabletitle"' :'';
	$header_row_class = $is_print ? ' class="tabletop"' : '';
	$td_class = $is_print ? ' class="eSporttdborder eSportprinttext"' :'';
	
	foreach ((array)$routeInfo as $_routeID => $_routeAryByWeekday){
		$_sumOfCount = 0;
		foreach($_routeAryByWeekday as $__AmPmOrWeekDay => $__stopsAry){
				
			if($type=='N'){
				$_weekday_or_type = $Lang['eSchoolBus']['Settings']['Route']['NormalRoute'].' ('.$Lang['eSchoolBus']['Settings']['Student'][$__AmPmOrWeekDay].')';
			}else{
				$_weekday_or_type = $Lang['eSchoolBus']['Settings']['Route']['Weekday'][$__AmPmOrWeekDay];
			}
			$x .= '<table '.($is_print?'align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="98%"':'class="common_table_list_v30" width="98%" ').'>'."\n";
				$x.='<thead>';
				$x.= '<tr'.$header_row_class.'>';
					$x.= '<th '.$header_class.'  width="1%">';
						$x.= $_weekday_or_type;
					$x.= '</th>';
					$x.= '<th '.$header_class.' colspan="2" width="1%">';
						$x.= $__stopsAry[0]['RouteName'];
					$x.= '</th>';
				$x.='</tr>';
				
				$x.= '<tr'.$header_row_class.'>';
					$x.= '<th '.$header_class.'  width="1%">';
						$x.= $Lang['eSchoolBus']['Settings']['BusStops']['BusStopName'];
					$x.= '</th>';
					$x.= '<th '.$header_class.' width="1%">';
						$x.= $Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent'];
					$x.= '</th>';
					$x.= '<th '.$header_class.' width="1%">';
						$x.= $Lang['eSchoolBus']['Report']['RouteArrangement']['Time'];
					$x.= '</th>';
				$x.='</tr>';
				
				$x.='</thead>';
			foreach ($__stopsAry as $___stopsInfo){
				$x .= '<tr>';
				$x .= '<td'.$td_class.'>'.$busStopInfoAry[$___stopsInfo['BusStopsID']][Get_Lang_Selection('BusStopsNameChi','BusStopsName')].'</td>';
				$__count = $userCountAry[$___stopsInfo['RouteCollectionID']][$___stopsInfo['BusStopsID']]['CountUser'];
				if(!$__count){
					$__count = 0;
				}
				$_sumOfCount = $_sumOfCount + $__count;
				$x .= '<td'.$td_class.'>'.$__count.'</td>';
				$x .= '<td'.$td_class.'>'.date('h:i',strtotime($___stopsInfo['Time'])).'</td>';
				$x .= '</tr>';
			}
			$x .= '<tr>';
			$x .= '<td'.$td_class.'></td>';
			$x .= '<td'.$td_class.'>'.$_sumOfCount.'</td>';
			$x .= '<td'.$td_class.'></td>';
			$x .= '</tr>';
			$x .= '<table>';
			$x .= '<br>';
		}
	}
	
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	echo $x;
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	
	
}


function getCurrentPage($key){
	global $currentPageAry;
	return $currentPageAry[$key][0].$currentPageAry[$key][1];
}
function nextColumn($key,$getResultOnly=false){
	global $currentPageAry;
	$alpha = $currentPageAry[$key][0];
	if($alpha=='Z'){
		return 'AA';
	}
	$alpha = chr(ord($alpha)+1) ;
	if($getResultOnly){
		return $alpha.$currentPageAry[$key][1];
	}
	$currentPageAry[$key][0] = $alpha ;
	return getCurrentPage($key);
}
function nextRow($key,$getResultOnly=false){
	global $currentPageAry;
	$num = $currentPageAry[$key][1];
	$num = $num + 1;
	$alpha = 'A';
	if($getResultOnly){
		return $alpha.$num;
	}
	$currentPageAry[$key][1] = $num ;
	$currentPageAry[$key][0] = $alpha;
	return getCurrentPage($key);
}
function setBorder($ActiveSheet,$startBox,$endBox){
	
	$ActiveSheet->getStyle($startBox.":".$endBox)->applyFromArray(
		array(
				'borders' => array(
						'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('rgb' => '000000')
						)
				)
		)
	);
}
?>