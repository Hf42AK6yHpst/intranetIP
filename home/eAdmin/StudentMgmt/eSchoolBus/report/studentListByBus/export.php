<?php
/*
 *  using:
 *  
 *  2019-03-06 [Cameron]
 *      - add ClassGroupID filter [case #Y156987] 
 *      
 *  2019-01-25 [Cameron]
 *      - add parameter $takeOrNot [case #Y154128]
 *      
 *  2018-11-21 Cameron
 *      - create this file
 */
 
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();

$db = $indexVar['db']; 
$linterface = $indexVar['linterface'];

$date = $_POST['SearchDate'];
$timeSlot = 'PM';
$buildingID = $_POST['BuildingID'];
$takeOrNot = $_POST['TakeOrNot'];
$classGroupIDAry = $_POST['ClassGroupID'];

$map = $db->getStudentListByBus($date, $timeSlot, $buildingID, $takeOrNot, $classGroupIDAry);

$presentList = $map['present'];
$presentCountAry = $map['presentCount'];
$absentList = $map['absent'];
$absentCountAry = $map['absentCount'];
$vehicleAry = $map['vehicleAssoc'];
$nrColumn = count($presentCountAry);
if ($nrColumn == 0) {
    $nrColumn = count($absentCountAry);
}
$formatDate = date('d/m',strtotime($date));
//debug_pr($vehicleAry);
$dataAry = array();
$rowCount = 0;

$classGroupAry = $db->getClassGroup();
$dispClassGroup = $Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'] . " : ";
if ($classGroupIDAry != '') {
    if (count($classGroupIDAry) == count($classGroupAry)) {
        $dispClassGroup .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
    }
    else {
        $dispClassGroupAry = array();
        foreach((array)$classGroupIDAry as $_classGroupID) {
            $dispClassGroupAry[] = $classGroupAry[$_classGroupID];
        }
        $dispClassGroup .= implode(", ", $dispClassGroupAry);
        unset($dispClassGroupAry);
    }
}
else {
    $dispClassGroup .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
}

// all or not taking bus
if ($takeOrNot == '0' || $takeOrNot == '2' ) {
    
    // Report Name
    $dataAry[$rowCount++][0] = $Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['NotTakeBus'];

    // Class Group
    $dataAry[$rowCount][] = $dispClassGroup;
    $rowCount++;

    // Campus (Location)
    $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['Campus'];
    if ($buildingID) {
        foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
            $dataAry[$rowCount][] = $_vehicleAry['BuildingName'];
        }
    }
    else {
        $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['All'];
    }
    $rowCount++;
    
    // Date
    $dataAry[$rowCount][] = '';
    for ($j=0; $j<$nrColumn; $j++) {
        $dataAry[$rowCount][] = $Lang['General']['Date'].':'.$formatDate;
    }
    $rowCount++;
    
    // Bus Name
    $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['Bus'];
    foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
        $dataAry[$rowCount][] = $_vehicleAry['CarNum'];
    }
    $rowCount++;
    
    // Student List that not taking the specific bus
    for ($i=0, $iMax=count($absentList); $i<$iMax; $i++) {
        $dataAry[$rowCount][] = '';
        for ($j=0; $j<$nrColumn; $j++) {
            $dataAry[$rowCount][] = $absentList[$i][$j];
        }
        $rowCount++;
    }
    
    // absent summary
    $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['Attendance']['TotalAbsent'];
    foreach((array)$absentCountAry as $_vehicleID=>$_absent) {
        $dataAry[$rowCount][] = $_absent;
    }
    $rowCount++;
}


if ($takeOrNot == '0') {
    $dataAry[$rowCount][] = '';
    $rowCount++;
}

// all or not taking bus
if ($takeOrNot == '0' || $takeOrNot == '1' ) {
    
    // Report Name
    $dataAry[$rowCount++][0] = $Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['MenuTitle'];
    
    // Class Group
    $dataAry[$rowCount][] = $dispClassGroup;
    $rowCount++;
    
    // Campus (Location)
    $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['Campus'];
    if ($buildingID) {
        foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
            $dataAry[$rowCount][] = $_vehicleAry['BuildingName'];
        }
    }
    else {
        $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['All'];
    }
    $rowCount++;
    
    // Date
    $dataAry[$rowCount][] = '';
    for ($j=0; $j<$nrColumn; $j++) {
        $dataAry[$rowCount][] = $Lang['General']['Date'].':'.$formatDate;
    }
    $rowCount++;
    
    // Bus Name
    $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['Bus'];
    foreach((array)$vehicleAry as $_vehicleID=>$_vehicleAry) {
        $dataAry[$rowCount][] = $_vehicleAry['CarNum'];
    }
    $rowCount++;
    
    // Student List that taking the specific bus
    for ($i=0, $iMax=count($presentList); $i<$iMax; $i++) {
        $dataAry[$rowCount][] = '';
        for ($j=0; $j<$nrColumn; $j++) {
            $dataAry[$rowCount][] = $presentList[$i][$j];
        }
        $rowCount++;
    }
    
    // present summary
    $dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['Attendance']['TotalOnBus'];
    foreach((array)$presentCountAry as $_vehicleID=>$_present) {
        $dataAry[$rowCount][] = $_present;
    }
    $rowCount++;
}

$numOfRow = count($dataAry);
for($i=0;$i<$numOfRow;$i++){
    $numOfColumn = count($dataAry[$i]);
    for($j=0;$j<$numOfColumn;$j++){
        $dataAry[$i][$j]=str_replace($seperator,"\n",$dataAry[$i][$j]);
    }
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

$filename = "bus_taking_list_at_specific_date.csv";

$lexport->EXPORT_FILE($filename,$export_text);
?>