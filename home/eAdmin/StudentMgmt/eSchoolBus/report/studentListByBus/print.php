<?php
/*
 *  Editing by
 *  
 *  2019-03-06 [Cameron]
 *      - add ClassGroupID filter [case #Y156987] 
 *      
 *  2019-01-25 [Cameron]
 *      - add parameter $takeOrNot [case #Y154128]
 *      
 *  2018-11-21 Cameron
 *      - create this file
 * 
 */
 
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$linterface = $indexVar['linterface'];

$date = $_POST['SearchDate'];
$timeSlot = 'PM';
$buildingID = $_POST['BuildingID'];
$takeOrNot = $_POST['TakeOrNot'];
$classGroupIDAry = $_POST['ClassGroupID'];

$htmlAry['resultTable']= $linterface->getStudentListByBus($date, $timeSlot, $buildingID, $takeOrNot, $printTitle=true, $classGroupIDAry);

?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN('Print', "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<?=$htmlAry['resultTable']?>
