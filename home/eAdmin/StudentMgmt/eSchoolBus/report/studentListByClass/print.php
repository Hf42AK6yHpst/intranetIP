<?php
/*
 *  Editing by
 *  
 *  2019-04-16 [Cameron]
 *      - add filter hasRouteOrNot to getStudentListByClass() [case #Y159784]
 *      
 *  2019-03-06 [Cameron]
 *      - add ClassGroupID filter [case #Y156987] 
 *      
 *  2018-11-22 Cameron
 *      - create this file
 * 
 */
 
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$linterface = $indexVar['linterface'];

$date = $_POST['SearchDate'];
$timeSlot = $_POST['TimeSlot'];
$hasRouteOrNot = $_POST['HasRouteOrNot'];
$buildingID = $_POST['BuildingID'];
$takeOrNot = $_POST['TakeOrNot'];
$yearClassIDAry = $_POST['YearClassID'];
$classGroupIDAry = $_POST['ClassGroupID'];

$htmlAry['resultTable']= $linterface->getStudentListByClass($date, $timeSlot, $buildingID, $takeOrNot, $hasRouteOrNot, $yearClassIDAry, $classGroupIDAry);

?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN('Print', "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print" style="text-decoration: none">
			<?php echo $Lang['eSchoolBus']['ReportArr']['StudentListByClassArr']['MenuTitle'];?>
		</td>
	</tr>
	<tr>
		<td style="text-decoration: none">
			<?php echo $Lang['General']['Date'] . ' : ' . $date;?>
		</td>
	</tr>
</table>

<?=$htmlAry['resultTable']?>
