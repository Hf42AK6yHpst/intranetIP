<?php
// Editing by 
/*
 * 2019-01-29 Cameron
 * - modify to support multiple teachers for the same route [case #Y154206]
 */
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');

if($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($AcademicYearID) || !isset($VehicleID)){
	intranet_closedb();
	exit;
}

include_once($intranet_root."/includes/libfilesystem.php");

$lf = new libfilesystem();

$teacher_name_field = getNameFieldByLang2('u.');
$class_name_field = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$bus_stop_name_field = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');

$normal_records = array();
$asa_records = array();

for($i=0;$i<count($VehicleID);$i++){
	/*
	// find regular routes
	$sql = "SELECT 
				v.VehicleID,
				v.CarNum,
				v.DriverName,
				v.DriverTel,
				v.CarPlateNum,
				v.Seat,
				r.RouteID,
				r.RouteName,
				$teacher_name_field as TeacherName,
				u.MobileTelNo as TeacherTel,
				GROUP_CONCAT(rc.RouteCollectionID) as RouteCollectionID, 
				rc.AmPm 
			FROM INTRANET_SCH_BUS_VEHICLE as v
			INNER JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as vm ON vm.VehicleID=v.VehicleID 
			INNER JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=vm.RouteID AND r.IsDeleted='0' 
			INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteID=r.RouteID AND rc.IsDeleted='0'
			INNER JOIN INTRANET_USER as u ON u.UserID=r.TeacherID 
			WHERE v.VehicleID='".$VehicleID[$i]."' AND v.RecordStatus='1' AND r.AcademicYearID='$AcademicYearID' AND r.IsActive='1' AND r.Type='N' AND r.IsDeleted='0' 
			GROUP BY r.RouteID 
			ORDER BY r.RouteName,rc.RouteCollectionID ";
	
	$normal_routes = $indexVar['db']->returnResultSet($sql);
	$normal_routes_size = count($normal_routes);
	
	// for each regular route, find related bus stops, and related students that take the route
	for($j=0;$j<$normal_routes_size;$j++){
		
		$record = array();
		$record['RouteInfo'] = $normal_routes[$j];
		
		$sql = "SELECT 
					r.RouteID,
					r.RouteName,
					rc.RouteCollectionID,
					rc.AmPm,
					bs.BusStopsID,
					$bus_stop_name_field as BusStopsName,
					TIME_FORMAT(rcsm.Time,'%H:%i') as Time,
					COUNT(ur.UserRouteID) as StudentCount  
				FROM INTRANET_SCH_BUS_ROUTE as r
				INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteID=r.RouteID AND rc.IsDeleted='0' 
				INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP as rcsm ON rcsm.RouteCollectionID=rc.RouteCollectionID 
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=rcsm.BusStopsID AND bs.IsDeleted='0' 
				LEFT JOIN INTRANET_SCH_BUS_USER_ROUTE as ur ON ur.RouteCollectionID=rc.RouteCollectionID AND ur.AcademicYearID='$AcademicYearID' AND ur.BusStopsID=bs.BusStopsID 
				WHERE r.RouteID='".$normal_routes[$j]['RouteID']."' AND rc.RouteCollectionID IN (".$normal_routes[$j]['RouteCollectionID'].")
				GROUP BY rc.RouteCollectionID,rc.AmPm,bs.BusStopsID 
				ORDER BY rc.RouteCollectionID,rcsm.Order ";
		
		$bus_stops = $indexVar['db']->returnResultSet($sql);
		//debug_pr($bus_stops);
		
		$sql = "SELECT 
					ur.UserID,
					$class_name_field as ClassName,
					ycu.ClassNumber,
					IF(au.UserID IS NOT NULL,CONCAT(au.EnglishName,' / ',au.ChineseName),CONCAT(u.EnglishName,' / ',u.ChineseName)) as StudentName,
					bs.BusStopsID,
					$bus_stop_name_field as BusStopName 
				FROM INTRANET_SCH_BUS_USER_ROUTE as ur 
				INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=ur.UserID 
				INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
				INNER JOIN YEAR as y ON y.YearID=yc.YearID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=ur.UserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=ur.UserID 
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=ur.BusStopsID AND bs.IsDeleted='0' 
				WHERE ur.AcademicYearID='$AcademicYearID' AND ur.RouteCollectionID IN (".$normal_routes[$j]['RouteCollectionID'].") 
				ORDER BY bs.BusStopsID";
		$students = $indexVar['db']->returnResultSet($sql);
		//debug_pr($students);
		
		$record['BusStops'] = $bus_stops;
		$record['Students'] = $students;
		
		$normal_records[] = $record;
	}
	
	//debug_pr($normal_records);
	*/
	// find ASA routes
	$sql = "SELECT 
				v.VehicleID,
				v.CarNum,
				v.DriverName,
				v.DriverTel,
				v.CarPlateNum,
				v.Seat,
				r.RouteID,
				r.RouteName,
				/*$teacher_name_field as TeacherName,
				u.MobileTelNo as TeacherTel,*/
                t.Teacher as TeacherName,
                t.TeacherTel,
				rc.RouteCollectionID, 
				rc.Weekday 
			FROM INTRANET_SCH_BUS_VEHICLE as v
			INNER JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as vm ON vm.VehicleID=v.VehicleID 
			INNER JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=vm.RouteID AND r.IsDeleted='0' 
			INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteID=r.RouteID AND rc.IsDeleted='0'
            INNER JOIN (SELECT 
                                GROUP_CONCAT($teacher_name_field) AS Teacher, 
                                GROUP_CONCAT(u.MobileTelNo) AS TeacherTel,
                                t.RouteID 
                        FROM 
                                INTRANET_SCH_BUS_ROUTE_TEACHER t
                        INNER JOIN INTRANET_USER u ON u.UserID=t.TeacherID
                                GROUP BY t.RouteID
                    ) AS t ON (t.RouteID = r.RouteID)
			/*INNER JOIN INTRANET_USER as u ON u.UserID=r.TeacherID*/ 
			WHERE v.VehicleID='".$VehicleID[$i]."' AND v.RecordStatus='1' AND r.AcademicYearID='$AcademicYearID' AND r.IsActive='1' AND r.Type='S' AND r.IsDeleted='0' 
			ORDER BY r.RouteName,rc.RouteCollectionID ";
	
	$asa_routes = $indexVar['db']->returnResultSet($sql);
	$asa_routes_size = count($asa_routes);
// debug_pr($sql);
// debug_pr($asa_routes);	
	//debug_pr($routes);
	
	// for each ASA route, find related bus stops, and related students that take the route 
	for($j=0;$j<$asa_routes_size;$j++){
		
		$record = array();
		$record['RouteInfo'] = $asa_routes[$j];
		
		$sql = "SELECT 
					r.RouteID,
					r.RouteName,
					rc.RouteCollectionID,
					rc.Weekday,
					bs.BusStopsID,
					$bus_stop_name_field as BusStopsName,
					TIME_FORMAT(rcsm.Time,'%H:%i') as Time,
					COUNT(ur.UserRouteID) as StudentCount  
				FROM INTRANET_SCH_BUS_ROUTE as r
				INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteID=r.RouteID AND rc.IsDeleted='0' 
				INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP as rcsm ON rcsm.RouteCollectionID=rc.RouteCollectionID 
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=rcsm.BusStopsID AND bs.IsDeleted='0' 
				LEFT JOIN INTRANET_SCH_BUS_USER_ROUTE as ur ON ur.RouteCollectionID=rc.RouteCollectionID AND ur.AcademicYearID='$AcademicYearID' AND ur.BusStopsID=bs.BusStopsID 
				WHERE r.RouteID='".$asa_routes[$j]['RouteID']."' AND rc.RouteCollectionID='".$asa_routes[$j]['RouteCollectionID']."' 
				GROUP BY rc.RouteCollectionID, rcsm.BusStopsID 
				ORDER BY rc.RouteCollectionID,rcsm.Order ";
		
		$bus_stops = $indexVar['db']->returnResultSet($sql);
		//debug_pr($bus_stops);
		
		$sql = "SELECT 
					ur.UserID,
					$class_name_field as ClassName,
					ycu.ClassNumber,
					IF(au.UserID IS NOT NULL,CONCAT(au.EnglishName,' / ',au.ChineseName),CONCAT(u.EnglishName,' / ',u.ChineseName)) as StudentName,
					bs.BusStopsID,
					$bus_stop_name_field as BusStopName 
				FROM INTRANET_SCH_BUS_USER_ROUTE as ur 
				INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=ur.UserID 
				INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
				INNER JOIN YEAR as y ON y.YearID=yc.YearID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=ur.UserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=ur.UserID 
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=ur.BusStopsID AND bs.IsDeleted='0' 
				WHERE ur.AcademicYearID='$AcademicYearID' AND ur.RouteCollectionID='".$asa_routes[$j]['RouteCollectionID']."'
				ORDER BY bs.BusStopsID";
		$students = $indexVar['db']->returnResultSet($sql);
		//debug_pr($students);
		
		$record['BusStops'] = $bus_stops;
		$record['Students'] = $students;
		
		$asa_records[] = $record;
	}
}

$asa_records_size = count($asa_records);

//debug_pr($records);

/*
if(in_array($Format,array('HTML','PRINT')))
{
	$x .= '<table width="98%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$indexVar['linterface']->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>';
	$x .= '<div></div>';
	
	$cur_route_id = '';
	
	for($i=0;$i<$asa_records_size;$i++){
		
		$route = $asa_records[$i]['RouteInfo'];
		$bus_stops = $asa_records[$i]['BusStops'];
		$students = $asa_records[$i]['Students'];
		
		$bus_stops_size = count($bus_stops);
		$students_size = count($students);
		
		if($cur_route_id != $route['RouteID'])
		{
			if($i > 0) $x .= '<br />';
			$x .= '<table width="98%">';
				$x .= '<tbody>';
					$x .= '<tr><td colspan="3">'.$route['RouteName'].'</td></tr>';
					$x .= '<tr>
								<td>'.$Lang['eSchoolBus']['Settings']['Vehicle']['Driver'].': '.$route['DriverName'].'</td>
								<td>'.$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber'].': '.$route['CarPlateNum'].'</td>
								<td>'.$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'].': '.$route['DriverTel'].'</td>
							</tr>';
					$x .= '<tr>
								<td>'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Teacher'].': '.$route['TeacherName'].'</td>
								<td>&nbsp;</td>
								<td>'.$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'].': '.$route['TeacherTel'].'</td>
							</tr>';
				$x .= '</tbody>';
			$x .= '</table><br />';
		
		}
		
		$x .= '<table class="common_table_list_v30" width="98%">';
			$x .= '<thead>';
				$x .= '<tr><th colspan="4">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusRoute'].'</th><th>'.$Lang['eSchoolBus']['Settings']['Route']['Weekday'][$route['Weekday']].'</th></tr>';
				$x .= '<tr>
							<th width="30%">'.$Lang['eSchoolBus']['Settings']['Route']['BusStop'].'</th>
							<th width="10%">'.$Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent'].'</th>
							<th width="10%">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusNumber'].'</th>
							<th width="10%">'.$Lang['eSchoolBus']['Settings']['Route']['PmTime'].'</th>
							<th width="40%">'.$Lang['eSchoolBus']['Settings']['Route']['Remarks'].'</th>
						</tr>';
			$x .='</thead>';
			$x .= '<tbody>';	
				
				$total_student_count = 0;
				for($j=0;$j<$bus_stops_size;$j++){
					$x .= '<tr>';
						$x .= '<td>'.$bus_stops[$j]['BusStopsName'].'</td>';
						$x .= '<td>'.$bus_stops[$j]['StudentCount'].'</td>';
						$x .= '<td>&nbsp;</td>';
						$x .= '<td>'.$bus_stops[$j]['Time'].'</td>';
						$x .= '<td>&nbsp;</td>';
					$x .= '</tr>';
					$total_student_count += $bus_stops[$j]['StudentCount'];
				}
			$x .= '<tr><td>&nbsp;</td><td>'.$total_student_count.'</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
			$x .= '</tbody>';
		$x .= '</table><br />';
		
		$x .= '<table class="common_table_list_v30" width="98%">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th width="20%">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Name'].'</th>
							<th width="20%">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['FormClass'].'</th>
							<th width="20%">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['PickupAddress'].'</th>
							<th width="20%">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['ContactPhone'].'</th>
							<th width="20%">'.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['EmailAddress'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			$x .= '<tbody>';
			for($j=0;$j<$students_size;$j++){
				$x .= '<tr>';
					$x .= '<td>'.$students[$j]['StudentName'].'</td>';
					$x .= '<td>'.$students[$j]['ClassName'].' ('.$students[$j]['ClassNumber'].')'.'</td>';
					$x .= '<td>'.$students[$j]['BusStopName'].'</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
				$x .= '</tr>';
			}
			$x .= '</tbody>';
		$x .= '</table><br />';
		
		$x .= '<br />';
		
		$cur_route_id = $route['RouteID'];
	}
	
	
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	echo $x;
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	
}
*/

if(in_array($Format, array('EXCEL',''))){
	
	include_once($intranet_root."/includes/phpxls/Classes/PHPExcel/IOFactory.php");
	include_once($intranet_root."/includes/phpxls/Classes/PHPExcel.php");
	
	$objPHPExcel = new PHPExcel();
	// Set properties
	$objPHPExcel->getProperties()->setCreator("eClass")
	->setLastModifiedBy("eClass")
	->setTitle("eClass eSchoolBus - ASA School Bus Student Contact List")
	->setSubject("eClass eSchoolBus - ASA School Bus Student Contact List")
	->setDescription("eClass eSchoolBus - ASA School Bus Student Contact List")
	->setKeywords("eClass eSchoolBus - ASA School Bus Student Contact List")
	->setCategory("eClass");

	$sheet_index = 0;
	$cur_car_num = '';
	
	$row = 0;
	
	for($i=0;$i<$asa_records_size;$i++){
		
		$route = $asa_records[$i]['RouteInfo'];
		$bus_stops = $asa_records[$i]['BusStops'];
		$students = $asa_records[$i]['Students'];
		
		$bus_stops_size = count($bus_stops);
		$students_size = count($students);
		
		$car_num = $route['CarNum'];
		
		if($cur_car_num != $car_num)
		{
			if($sheet_index > 0)
			{
				$objPHPExcel->createSheet();
			}
			$objPHPExcel->setActiveSheetIndex($sheet_index);
			$ActiveSheet[$car_num] = $objPHPExcel->getActiveSheet();
			$ActiveSheet[$car_num]->setTitle($car_num);
			
			$row = 0;
		}
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':F'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$route['RouteName']);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getFont()->setBold(true);
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':B'.$row);
		$ActiveSheet[$car_num]->mergeCells('C'.$row.':D'.$row);
		//$ActiveSheet[$car_num]->mergeCells('E'.$row.':F'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Settings']['Vehicle']['Driver'].': '.$route['DriverName']);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber'].': '.$route['CarPlateNum']);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'].': '.$route['DriverTel']);
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':B'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Teacher'].': '.$route['TeacherName']);
		$ActiveSheet[$car_num]->mergeCells('C'.$row.':D'.$row);
		//$ActiveSheet[$car_num]->mergeCells('E'.$row.':E'.$row,$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'].': '.$route['TeacherTel']);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'].': '.$route['TeacherTel']);
		
		$row += 2;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusRoute']);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday'][$route['Weekday']]);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getFont()->setBold(true);
		
		
		$row += 1;
		$table_start_row = $row;
		$table_end_row = $row;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Settings']['Route']['BusStop']);
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent']);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusNumber']);
		$ActiveSheet[$car_num]->setCellValue('D'.$row,$Lang['eSchoolBus']['Settings']['Route']['PmTime']);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Settings']['Route']['Remarks']);
		
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$total_student_count = 0;
		for($j=0;$j<$bus_stops_size;$j++){
			$row += 1;
			$ActiveSheet[$car_num]->setCellValue('A'.$row,$bus_stops[$j]['BusStopsName']);
			$ActiveSheet[$car_num]->setCellValue('B'.$row,$bus_stops[$j]['StudentCount']);
			$ActiveSheet[$car_num]->setCellValue('D'.$row,$bus_stops[$j]['Time']);
			
			$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$total_student_count += $bus_stops[$j]['StudentCount'];
		}
		
		$row += 1;
		$table_end_row = $row;
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$total_student_count);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$ActiveSheet[$car_num]->getStyle('A'.$table_start_row.':E'.$table_end_row)->applyFromArray(
		    array(
		        'borders' => array(
		            'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                'color' => array('rgb' => '000000')
		            )
		        )
		    )
		);
		
		$row += 2;
		$table_start_row = $row;
		$table_end_row = $row;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Name']);
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['FormClass']);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['PickupAddress']);
		$ActiveSheet[$car_num]->setCellValue('D'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['ContactPhone']);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['EmailAddress']);
		
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		for($j=0;$j<$students_size;$j++){
			$row += 1;
			if($j == $students_size - 1) $table_end_row = $row;
			$ActiveSheet[$car_num]->setCellValue('A'.$row,$students[$j]['StudentName']);
			$ActiveSheet[$car_num]->setCellValue('B'.$row,$students[$j]['ClassName'].' ('.$students[$j]['ClassNumber'].')');
			$ActiveSheet[$car_num]->setCellValue('C'.$row,$students[$j]['BusStopName']);
			
			$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		
		$ActiveSheet[$car_num]->getStyle('A'.$table_start_row.':E'.$table_end_row)->applyFromArray(
		    array(
		        'borders' => array(
		            'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                'color' => array('rgb' => '000000')
		            )
		        )
		    )
		);
		
		$row += 1;
		
		if($cur_car_num != $car_num)
		{
			$sheet_index += 1;
		}
		
		$cur_car_num = $route['CarNum'];
	}
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	$tmp_folder = $intranet_root.'/file/eSchoolBus';
	if(!is_dir($tmp_folder)){
		$lf->folder_new($tmp_folder);
	}
	$tmp_file_name = 'tmp'.md5(uniqid()).'.xls';
	$tmp_file = $tmp_folder.'/'.$tmp_file_name;
	
	file_put_contents($tmp_file,'');
	chmod($tmp_file, 0777);
	
	$objWriter->save($tmp_file);
	
	$output_filename = 'asa_school_bus_students_contact_list.xls';
	$content = get_file_content($tmp_file);
	$lf->file_remove($tmp_file);
	intranet_closedb();
	output2browser($content, $output_filename);
}

?>