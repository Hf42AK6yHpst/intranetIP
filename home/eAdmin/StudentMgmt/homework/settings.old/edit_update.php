<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$lhomework = new libhomework2007();
$lgeneralsettings = new libgeneralsettings();

/*$disabled = ($_GET['disabled']) ? $_GET['disabled'] : 0;
$teacherSearchDisabled = ($_GET['teacherSearchDisabled']) ? $_GET['teacherSearchDisabled'] : 0;
$subjectSearchDisabled = ($_GET['subjectSearchDisabled']) ? $_GET['subjectSearchDisabled'] : 0;
$startFixed = ($_GET['startFixed']) ? $_GET['startFixed'] : 0;
$parentAllowed = ($_GET['parentAllowed']) ? $_GET['parentAllowed'] : 0;
$nonTeachingAllowed = ($_GET['nonTeachingAllowed']) ? $_GET['nonTeachingAllowed'] : 0;
$allowSubLeader = ($_GET['allowSubLeader']) ? $_GET['allowSubLeader'] : 0;
$allowExport = ($_GET['allowExport']) ? $_GET['allowExport'] : 0;
$allowPast = ($_GET['allowPast']) ? $_GET['allowPast'] : 0;
*/

$data = array();
$data['disabled'] = $disabled;
$data['teacherSearchDisabled'] = $teacherSearchDisabled;
$data['subjectSearchDisabled'] = $subjectSearchDisabled;
$data['startFixed'] = $startFixed;
$data['parentAllowed'] = $parentAllowed;
$data['nonteachingAllowed'] = $nonTeachingAllowed;
$data['subjectLeaderAllowed'] = $allowSubLeader;
$data['exportAllowed'] = $allowExport;
$data['pastInputAllowed'] = $allowPast;
$data['DeafultHandinRequired'] = $DeafultHandinRequired;
$data['useHomeworkCollect'] = $useHomeworkCollect;


# store in DB
$lgeneralsettings->Save_General_Setting($lhomework->Module, $data);

# set $this->
$lhomework->disabled = $disabled;			
$lhomework->teacherSearchDisabled = $teacherSearchDisabled;
$lhomework->subjectSearchDisabled = $subjectSearchDisabled;
$lhomework->startFixed = $startFixed;
$lhomework->parentAllowed = $parentAllowed;
$lhomework->nonteachingAllowed = $nonTeachingAllowed;
$lhomework->subjectLeaderAllowed = $allowSubLeader;				
$lhomework->exportAllowed = $allowExport;
$lhomework->pastInputAllowed = $allowPast;			
$lhomework->DeafultHandinRequired = $DeafultHandinRequired;			
$lhomework->useHomeworkCollect = $useHomeworkCollect;			

# update session
foreach($data as $name=>$val)
	$_SESSION["SSV_PRIVILEGE"]["homework"][$name] = $val;

intranet_closedb();
header("Location: index.php?msg=update");
?>