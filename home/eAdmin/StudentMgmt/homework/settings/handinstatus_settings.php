<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

$lhomework = new libhomework2007();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$linterface = new interface_html();
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Settings_HandInStatusSettings";
# Left menu 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HandInStatusSettings']);

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) {
	$handInStatusArr = $lhomework->getAllHandInStatus();
} else {
	$handInStatusArr = $lhomework->getAllHandInStatus(TRUE);
}

# build content table
$table_row = 0;
$table .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";

$table .= "<tr>";
$table .= "<td class='tabletop'>". $Lang['SysMgr']['Homework']['HandInStatusValue'] ."</td>";
$table .= "<td class='tabletop'>". $eDiscipline['Value'] ."</td>";
$table .= "</tr>";

$is_enabled = $Lang['eHomework']["Enabled"];
$is_disabled = $Lang['eHomework']["Disabled"];

if (count($handInStatusArr) > 0) {
	foreach ($handInStatusArr as $kk => $vv) {
		$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
		$table .= "<tr class='". $tablerow_class ."'>";
		$table .= "<td>". Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"]) ."</td>";
		$table .= "<td>". ($vv["isDisabled"] ? $is_disabled : $is_enabled) ."</td>";
		$table .= "</tr>";
	}
}

$table .= "</table>";
?>
<form name="form1" method="post" action="handinstatus_settings_edit.php">
<br />
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<tr> 
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><?=$table?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
</tr>
<?php if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) { ?>
<tr>
	<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<span class="dotline">
						<?= $linterface->GET_ACTION_BTN($button_edit, "submit")?>
					</span>
				</td>
			</tr>
		</table>
	</td>
</tr>
<?php } ?>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>