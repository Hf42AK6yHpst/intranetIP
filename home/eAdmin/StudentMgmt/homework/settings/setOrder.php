<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

$lhomework->updateHomeworkTypeOrder($old,$new);

intranet_closedb();

header("Location: homeworkType.php?xmsg=update");

?>
