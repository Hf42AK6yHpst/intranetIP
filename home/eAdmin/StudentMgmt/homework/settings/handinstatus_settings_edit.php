<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();



if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");


$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Settings_HandInStatusSettings";


$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HandInStatusSettings'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['Edit'], "");

# Left menu 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HandInStatusSettings']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$is_enabled = $Lang['eHomework']["Enabled"];
$is_disabled = $Lang['eHomework']["Disabled"];

$handInStatusArr = $lhomework->getAllHandInStatus();

# Start layout
$linterface->LAYOUT_START();
?>

<br />   
<form name="form1" method="post" action="handinstatus_settings_edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION4($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<?php
if (count($handInStatusArr) > 0) {
	foreach ($handInStatusArr as $kk => $vv) {
?>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?php echo Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"]); ?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="handinstatus[<?php echo $vv["HandInStatusID"]; ?>]" value="0" <?php echo ($vv["isDisabled"] ? "":"checked"); ?> id="status_<?php echo $vv["HandInStatusID"]; ?>_1"> <label for="status_<?php echo $vv["HandInStatusID"]; ?>_1"><?php echo $is_enabled?></label> 
				<input type="radio" name="handinstatus[<?php echo $vv["HandInStatusID"]; ?>]" value="1" <?php echo ($vv["isDisabled"] ? "checked":""); ?> id="status_<?php echo $vv["HandInStatusID"]; ?>_0"> <label for="status_<?php echo $vv["HandInStatusID"]; ?>_0"><?php echo $is_disabled?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>
<?php
	}
}
?>
<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_update, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>
</table>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>