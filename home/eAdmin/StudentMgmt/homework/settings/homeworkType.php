<?php
# using: henry
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lhomework = new libhomework2007();


$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Settings_HomeworkType";

# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

$pageSizeChangeEnabled = true;

# Table initialization
//$order = ($order == "") ? 1 : $order;
//$field = ($field == "") ? $sortField : $field;
$order = 1;
$field = 1;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$li = new libdbtable2007($field, $order, $pageNo);

/*
$selectStatus = "<SELECT name='status' id='status' onChange='document.form1.submit();'>";
$selectStatus .= "<OPTION value=''".(($status=="")?" selected" : "").">-- $i_general_all --</OPTION>";
$selectStatus .= "<OPTION value='1'".(($status=="1")?" selected" : "").">$i_general_active</OPTION>";
$selectStatus .= "<OPTION value='0'".(($status=="0")?" selected" : "").">$i_general_inactive</OPTION>";
$selectStatus .= "</SELECT>";
*/

//$conds = ($status!='') ? " AND RecordStatus=$status" : " AND RecordStatus!='-1'";
$conds = " AND recordStatus!='-1'";

$maxOrder = $lhomework->getHomeworkTypeMaxOrder();
if($maxOrder=="" || $maxOrder=="0") {
	$maxOrder = "999999999";	
}

$sql = "SELECT 
			CONCAT('<a href=\'homeworkType_edit.php?typeID=',TypeID,'&status=".$status."\'>',TypeName,'</a>'), 
			CASE(DisplayOrder)
				WHEN $maxOrder THEN CONCAT('<a href=setOrder.php?old=',DisplayOrder,'&new=1 class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Top\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif border=0></a> <a href=setOrder.php?old=',DisplayOrder,'&new=',DisplayOrder-1,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Up\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif border=0></a>')
				WHEN 1 THEN CONCAT('<img src={$image_path}/{$LAYOUT_SKIN}/10x10.gif width=32 height=2><a href=setOrder.php?old=1&new=2 class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Down\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif border=0></a> <a href=setOrder.php?old=1&new=',$maxOrder,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Bottom\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif border=0></a>')
				ELSE CONCAT('<a href=setOrder.php?old=',DisplayOrder,'&new=1 class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Top\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif border=0></a> <a href=setOrder.php?old=',DisplayOrder,'&new=',DisplayOrder-1,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Up\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif border=0></a> <a href=setOrder.php?old=',DisplayOrder,'&new=',DisplayOrder+1,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Down\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif border=0></a> <a href=setOrder.php?old=',DisplayOrder,'&new=',$maxOrder,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Bottom\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif border=0></a>') END,
			CASE(RecordStatus)
				WHEN 1 THEN '$i_general_active'
				WHEN 0 THEN '$i_general_inactive'
				ELSE '' END,
			CONCAT('<input type=\'checkbox\' name=\'typeID[]\' id=\'typeID[]\' value=\'', TypeID ,'\'>') 
		FROM 
			INTRANET_HOMEWORK_TYPE 
		WHERE 
			DateInput IS NOT NULL 
			$conds
		";

$li->field_array = array("TypeName", "DisplayOrder", "Status");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->fieldorder2 = ", DisplayOrder";
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='5%'>#</td>\n";
$li->column_list .= "<td width='60%' class='tabletop tabletopnolink'>$i_Homework_Type</td>\n";$pos++;
$li->column_list .= "<td width='30%' class='tabletop tabletopnolink'>$i_general_DisplayOrder</td>\n";$pos++;
$li->column_list .= "<td width='30%' class='tabletop'>$i_general_status</td>\n";
$li->column_list .= "<td class='tabletop tabletoplink' width='5%'>".$li->check("typeID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("homeworkType_new.php?status=$status",$button_new,"","","",0);

# Left menu 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkType']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function edit(obj, emt, page) {
	obj.action = page;
	obj.submit();
}

function remove(obj, emt, page) {
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,emt)==0)
			alert(globalAlertMsg2);
        else{
			if(confirm(alertConfirmRemove)){
				obj.action=page;
				obj.submit();
			}
        }
}
-->
</script>
<form name="form1" method="post" action="">
<br />
<table width="96%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<tr> 
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><?=$toolbar?></td>
				<td align="right"><?=$linterface->GET_SYS_MSG($xmsg)?></td>
			</tr>
			<tr class="table-action-bar">
				<td align="right" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="28" align="right" valign="bottom" >
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>&nbsp;</td>
										<td align="right" valign="bottom">
											<table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
													<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
														<table border="0" cellspacing="0" cellpadding="2">
															<tr>
																<td nowrap="nowrap"><a href="javascript:edit(document.form1,'typeID[]','homeworkType_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																<td nowrap="nowrap"><a href="javascript:remove(document.form1,'typeID[]','homeworkType_remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
															</tr>
														</table>
													</td>
													<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2"><?=$li->display();?></td>
			</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />


</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
