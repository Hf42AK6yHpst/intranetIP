<?php
// Modifing by : 
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lexport = new libexporttext();

$filename = "MonthlyHomeworkList.csv";


if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $lhomework->isViewerGroupMember($UserID)){
	
	$className = $eHomework_Homework_List_All_Classes;
	if($classID!="") {
		$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID=$classID";
		$clsName = $lhomework->returnVector($sql);
		$className = $clsName[0];
	}
	
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
		$sname = $lhomework->returnVector($sql,1);
		$subjectName = $sname[0];
	}

	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];

	if ($subjectGroupID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
		$sgname = $lhomework->returnVector($sql,1);
		$subjectGroupName = $sgname[0];
	}

	$cond = (($subjectID=="" || $subjectID==-1) && $subjectGroupID=="")? "a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : (($subjectGroupID!="") ? " a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : "a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID");
	$data = $lhomework->exportMonthlyHomeworkList($ts, "", $cond, "", $classID);
}


else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
	
	$className = $eHomework_Homework_List_All_Classes;
	if($classID!="") {
		$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID=$classID";
		$clsName = $lhomework->returnVector($sql);
		$className = $clsName[0];
	}
	
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
		$sname = $lhomework->returnVector($sql,1);
		$subjectName = $sname[0];
	}

	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];

	if ($subjectGroupID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
		$sgname = $lhomework->returnVector($sql,1);
		$subjectGroupName = $sgname[0];
	}

	$cond = (($subjectID=="" || $subjectID==-1) && $subjectGroupID=="")? "a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : (($subjectGroupID!="") ? " a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : "a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID");
	$data = $lhomework->exportMonthlyHomeworkList($ts, "", $cond, "", $classID);
}

else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]){
	
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', a.EN_DES, a.CH_DES) AS SubjectName, 
				IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroupName FROM ASSESSMENT_SUBJECT AS a 
				LEFT OUTER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS AS c on c.SubjectGroupID = b.SubjectGroupID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d on d.SubjectGroupID = c.SubjectGroupID
				WHERE d.UserID = $UserID AND a.RecordID = $subjectID";
		
		$result = $lhomework->returnArray($sql,2);
		$subjectName = $result[0]['SubjectName'];
		$subjectGroupName = $result[0]['SubjectGroupName'];
	}
	
	
	# Select Subjects
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
		
	if(sizeof($subject)!=0){
		$allSubjects = " a.SubjectID IN (";
		for ($i=0; $i < sizeof($subject); $i++)
		{	
			list($ID)=$subject[$i];
			$allSubjects .= $ID.",";
		}
		$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
		$allSubjects .= " AND";
	}
	else{
		$allSubjects ="";
	}
	
	$cond = ($subjectID=="" || $subjectID==-1)? "$allSubjects a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
	$data = $lhomework->exportMonthlyHomeworkList($ts, "", $cond, 1);
}

# Set $ts to sunday of the week
$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
$today = date('Y-m-d');

# Get Cycle days
$start = mktime(0,0,0,$month,1,$year);
$end = mktime(0,0,0,$month+1,1,$year);
$lcal = new libcalevent();
$row = $lcal->returnCalendarArray($month,$year);
$lcycle = new libcycleperiods();
$cycle_days = $lcycle->getCycleInfoByDateRange(date('Y-m-d',$start),date('Y-m-d',$end));


for ($i=0; $i<sizeof($row); $i++)
{
  $week_ts[] = $ts+$i*86400;
}


$exportColumn = array();
		
for ($i=0; $i<7; $i++)
{
	$day = $Lang['SysMgr']['Homework']['WeekDay'][$i];
	array_push($exportColumn, $day);
}

if(sizeof($data)!=0)
{
	$m = 0;
	$new_m = $m;
	$HW_Flag = 0;
	$maxHw = 0;
	$lang = $intranet_session_language=="b5" ? 1 : 0;
	
	for($i=0; $i<sizeof($row); $i++){		# loop for each day
		
		$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
		
		$pos = $i%7;
		$HW_Flag = 0;
		
		if($row[$i] !='') {				# with the date
			$recordDate = date('Y-m-d',$row[$i]);
			$cycle = $cycle_days[$recordDate][$lang] ? " (".$cycle_days[$recordDate][$lang].")" : "";
			
			$day = date('j',$row[$i]);
			$ExportArr[$m][$pos] = $day.$cycle;
			
			$new_m = $m+1;
				
			for($j=0; $j<sizeof($data); $j++){
				
				list($hid,$subject,$subjectgroup,$startdate,$duedate,$loading,$title,$description,$subjectGroupID,$subjectID,$AttachmentPath, $TypeID) = $data[$j];
				
				$displayDate = ($lhomework->useStartDateToGenerateList) ? $startdate : $duedate;
				
				if ($current == $displayDate){
					
					if ($loading==0)
					{
						$tload = 0;
					}
					else if ($loading <= 16)
					{
						$tload = $loading/2;
					}
					else
					{
						$tload = "$i_Homework_morethan 8";
					}
					$useHwType = "";
					if($lhomework->useHomeworkType) {
						$homeworkTypeInfo = $lhomework->getHomeworkType($TypeID);
						$typeName = ($homeworkTypeInfo[0]['TypeName']) ? $homeworkTypeInfo[0]['TypeName'] : "---";
						$useHwType = $typeName;					
					}
	
					$text1 = $Lang['SysMgr']['Homework']['Topic'].": ".$title;
					$text2 = $Lang['SysMgr']['Homework']['Subject'].": ".$subject;
					$text3 = $Lang['SysMgr']['Homework']['SubjectGroup'].": ".$subjectgroup;
					$text4 = $Lang['SysMgr']['Homework']['Workload'].": $tload ".$Lang['SysMgr']['Homework']['Hours'];
					$text5 = ($lhomework->useStartDateToGenerateList) ? ($Lang['SysMgr']['Homework']['DueDate'].": ".$duedate) : ($Lang['SysMgr']['Homework']['StartDate'].": ".$startdate);
					$text6 = $i_Homework_HomeworkType.": ".$useHwType;
					
					//echo $text1.'/'.$text2.'/'.$text3.'/'.$text4.'/'.$text5.'/'.$new_m.'<br>';
					$ExportArr[$new_m][$pos] = $text1;
					$new_m++;
					$ExportArr[$new_m][$pos] = $text2;
					$new_m++;
					$ExportArr[$new_m][$pos] = $text3;
					$new_m++;
					if($lhomework->useHomeworkType) {
						$ExportArr[$new_m][$pos] = $text6;
						$new_m++;
					}
					$ExportArr[$new_m][$pos] = $text4;
					$new_m++;
					$ExportArr[$new_m][$pos] = $text5;
					$new_m++;
					$ExportArr[$new_m][$pos] = "";
					$new_m++;

					$HW_Flag++;
				} 
				
			}
			$maxHw = ($HW_Flag>$maxHw) ? $HW_Flag : $maxHw;
			if($pos==6) {						# move to a new row per 7 days
				if($maxHw!=0) 
					$m = $new_m + ($maxHw*6);
				else 
					$m = $new_m;
					
				$ExportArr[$m][] = "\n";	
				$HW_Flag = 0;
				$maxHw = 0;
				$m++;
			}
		}
	}
}

else{
	$ExportArr[0][0] = $i_no_record_exists_msg;
}

$export_content = $lexport->GET_EXPORT_TXT2($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

# display the month & year on the top
if($month>=10)
	$export_content = $eEnrollment['month'][$month]."/".$year."\n".$export_content;
else 
	$export_content = $eEnrollment['month'][substr($month,-1)]."/".$year."\n".$export_content;


intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
