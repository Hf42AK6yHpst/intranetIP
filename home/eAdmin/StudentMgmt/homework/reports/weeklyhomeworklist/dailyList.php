<?php
# Modifing by :
/*
 *  2020-10-23 (Cameron)
 *      fix: don't pass firstValue as -1 to getCurrentTeachingSubjects() for non-teaching staff when retrieving subject list
 *
 *  2020-09-22 (Cameron)
 *      create this file [case #U176404]
 */

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$sys_custom['eHomework']['DailyHomeworkList']) || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
    {
        header("location: ../../settings/index.php");
        exit;
    }
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$linterface = new interface_html();

$CurrentPage = "Reports_WeekHomeworkList";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['DailyHomeworkList'];

$TAGS_OBJ = $lhomework->getReportHomeworkListTabs('ByDate');

$today = date('Y-m-d');
# Current Year
$yearID = Get_Current_Academic_Year_ID();
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
if($yearTermID=="")
    $yearTermID = 0;

$startDateOfCurrentAcademicYear = getStartDateOfAcademicYear($yearID);
$endDateOfCurrentAcademicYear = getEndDateOfAcademicYear($yearID);

# Set TimeStamp
if ($ts == ""){
    $ts = time();
}

$ts = mktime(0,0,0,date('m',$ts),date('d',$ts),date('Y',$ts));      // beginning timestamp of the date
$filterDate = date('Y-m-d', $ts);

$subjectID = ($subjectID=="")? -1 :$subjectID;

if($subjectGroupID && $subjectID==-1)
{
    $TempSubjectInfo = $lhomework->RetrieveSubjectbySubjectGroupID($subjectGroupID);
    $subjectID = $TempSubjectInfo[0]['SubjectID'];
}

# Non-Teaching Staff Mode
if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
    $classes = $lhomework->getAllClassInfo();
    $selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class();"', $classID, 0, 0, $i_general_all_classes);

    $subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
    $subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;

    $selectedSubject = $lhomework->getCurrentTeachingSubjects('name="subjectID" id="subjectID" onChange="js_change_subject();"', $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);

    $subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
    $selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID id=subjectGroupID", $subjectGroupID, "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
}

# Teacher Mode
else if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $lhomework->isViewerGroupMember($UserID)){

    if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || !$_SESSION['isTeaching'] || $lhomework->isViewerGroupMember($UserID))		# eHomework Admin
        $classes = $lhomework->getAllClassInfo();
    else														# Class teacher / subject teacher
        $classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
    $selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class();"', $classID, 0, 0, $i_general_all_classes);

    $subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
    $display = (sizeof($subject)==0 && sizeof($classes)==0)? false:true;
    if($display){
        $selectedSubject = $lhomework->getCurrentTeachingSubjects('name="subjectID" id="subjectID" onChange="js_change_subject();"', $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);

        $subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID, $classID);
        $selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID id=subjectGroupID", $subjectGroupID, "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
    }
    else {
        $selectedSubject = '';
        $selectedSubjectGroup = '';
    }
}

# Student Mode with Subject Leader Allowed
else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]){

    $selectClass = '';
    $selectedSubjectGroup = '';

    # Select Subjects
    $subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID, $classID);
    $display = (sizeof($subject)==0)? false:true;

    if($display){
        $selectedSubject = $lhomework->getCurrentStudyingSubjects('name="subjectID"', $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
    }
    else {
        $selectedSubject = '';
    }

}


# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">

    $(document).ready(function(){

        $('#btnExport').click(function(){
            var original_action = document.form1.action;
            var url = "daily_export.php";
            if(!check_date(form1.filterDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) {
                return false;
            //} else if ((compareDate(form1.filterDate.value,'<?php //echo $startDateOfCurrentAcademicYear;?>//') == -1) || (compareDate('<?php //echo $endDateOfCurrentAcademicYear;?>//', form1.filterDate.value) == -1)) {
            //    alert("<?php //echo $Lang['SysMgr']['Homework']['warning']['OutOfCurrentAcademicYear'];?>//");
            //    return false;
            } else {
                document.form1.action = url;
                document.form1.submit();
                document.form1.action = original_action;
            }
        });

        $('#filterDate').change(function(){
           if ($(this).val().length == 10) {
               js_change_date();
           }
        });
    });

    function js_change_class()
    {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: 'ajax.php',
            data : {
                'action': 'getSubjectList',
                'classID': $('#classID').val(),
                'yearID': $('#yearID').val(),
                'yearTermID': $('#yearTermID').val()
            },
            success: update_subject_list,
            error: show_ajax_error
        });

    }

    function js_change_subject()
    {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: 'ajax.php',
            data : {
                'action': 'getSubjectGroupList',
                'classID': $('#classID').val(),
                'yearID': $('#yearID').val(),
                'yearTermID': $('#yearTermID'),
                'subjectID': $('#subjectID').val()
            },
            success: update_subject_group_list,
            error: show_ajax_error
        });
    }

    function js_change_date()
    {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: 'ajax.php',
            data : {
                'action': 'getAcademicYearAndYearTermByDate',
                'filterDate': $('#filterDate').val()
            },
            success: update_academic_year_and_yearterm,
            error: show_ajax_error
        });
    }

    function js_update_class_list()
    {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: 'ajax.php',
            data : {
                'action': 'getClassList',
                'yearID': $('#yearID').val(),
                'yearTermID': $('#yearTermID').val()
            },
            success: update_class_list,
            error: show_ajax_error
        });
    }
    function show_ajax_error() {
        alert('<?=$Lang['General']['AjaxError']?>');
    }

    function update_subject_list(ajaxReturn)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            $('span#subject').html(ajaxReturn.subject);
            $('span#subjectGroup').html(ajaxReturn.subjectGroup);
        }
    }

    function update_subject_group_list(ajaxReturn)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            $('span#subjectGroup').html(ajaxReturn.subjectGroup);
        }
    }

    function update_academic_year_and_yearterm(ajaxReturn)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            $('#yearID').val(ajaxReturn.AcademicYearID);
            $('#yearTermID').val(ajaxReturn.YearTermID);
            js_update_class_list();
        }
    }

    function update_class_list(ajaxReturn)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            if (ajaxReturn['selectClass'] != ''){
                $('span#class').html(ajaxReturn.selectClass);
            }
        }
    }


</script>

<form name="form1" method="post" action="" onSubmit="return false">
    <table width="98%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="5">
                    <tr>
                        <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['General']['Date']?></td>
                        <td align="left" valign="top">
                            <?=$linterface->GET_DATE_PICKER("filterDate",$today,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="js_change_date();",$ID="filterDate")?>
                        </td>
                    </tr>

                <?php if ($selectClass): ?>
                    <tr>
                        <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['Header']['Menu']['Class']?></td>
                        <td width="567" align="left" valign="top">
							<span id="class">
								<?=$selectClass?>
							</span>
                        </td>
                    </tr>
                <?php endif;?>

                <?php if ($selectedSubject): ?>
                    <tr>
                        <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?></td>
                        <td width="567" align="left" valign="top">
							<span id="subject">
								<?=$selectedSubject?>
							</span>
                        </td>
                    </tr>
                <?php endif;?>

                <?php if ($selectedSubjectGroup): ?>
                    <tr>
                        <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></td>
                        <td width="567" align="left" valign="top">
							<span id="subjectGroup">
								<?=$selectedSubjectGroup?>
							</span>
                        </td>
                    </tr>
                <?php endif;?>

                </table>
            </td>
        </tr>

        <tr>
            <td class="dotline">
                <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
            </td>
        </tr>

        <tr>
            <td align="center">
                <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Export'], 'button', $ParOnClick="", $ParName="btnExport")?>
            </td>
        </tr>

    </table>

    <input type="hidden" name="yearID" id="yearID" value="<?=$yearID?>"/>
    <input type="hidden" name="yearTermID" id="yearTermID" value="<?=$yearTermID?>"/>

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>