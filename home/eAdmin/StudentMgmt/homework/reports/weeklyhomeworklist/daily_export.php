<?php
// Modifing by :
/*
 *  2020-10-19 Cameron
 *      - show outline gridline for cell
 *
 *  2020-10-05 Cameron
 *      - create this file
 */

@SET_TIME_LIMIT(216000);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');        // php version <= 5.2 require this
date_default_timezone_set('Asia/Hong_Kong');

$PATH_WRT_ROOT = "../../../../../../";
//$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
//$lf = new libfilesystem();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$sys_custom['eHomework']['DailyHomeworkList']) || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
    {
        header("location: ../../settings/index.php");
        exit;
    }
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}





## Step 1: get filters
$date = $_POST['filterDate'];
$classID = IntegerSafe($_POST['classID']);
$subjectID = IntegerSafe($_POST['subjectID']);
$subjectGroupID = IntegerSafe($_POST['subjectGroupID']);
$yearID = IntegerSafe($_POST['yearID']);
$yearTermID = IntegerSafe($_POST['yearTermID']);


## Step 2: get result
$data = $lhomework->exportDailyHomeworkList($date, $classID, $subjectID, $subjectGroupID, $yearID, $yearTermID);
//debug_pr($data);


## Step 3: output excel file
$objPHPExcel = new PHPExcel();    // Create new PHPExcel object

//    $file_name = 'daily_homework_by_class_' . $date . '.xlsx';
$file_name = 'daily_homework_by_class_' . $date . '.xls';

// Set properties
$objPHPExcel->getProperties()->setCreator("eClass")
    ->setLastModifiedBy("eClass")
    ->setTitle($Lang['SysMgr']['Homework']['DailyHomeworkList'])
    ->setSubject('Daily Homework by Class')
    ->setDescription('')
    ->setKeywords("eHomework")
    ->setCategory("eHomework");

// Set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('新細明體');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);

$cellBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '666666'),
        ),
    ),
);

$sheetIndex = 0;        // one class per sheet

if (($data != false) && count($data['StudentHomework'])) {
    $studentHomework = $data['StudentHomework'];
    $classHomework = $data['ClassHomework'];
    $classList = $data['ClassList'];

    foreach((array)$classList as $classID=>$className) {
        // Create sheet
        if ($sheetIndex > 0) {
            $objPHPExcel->createSheet();
        }

        // Set active sheet
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $activeSheet = $objPHPExcel->getActiveSheet();
        $activeSheet->setTitle($className);

        // build header row
        $activeSheet->setCellValue('A1', $Lang['SysMgr']['Homework']['Class']);
        $activeSheet->setCellValue('B1', $Lang['SysMgr']['Homework']['ClassNumber']);
        $activeSheet->setCellValue('C1', $Lang['SysMgr']['Homework']['StudentName']);
        for ($k=0;$k<3;$k++) {
            $activeSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($k) . '1')->applyFromArray($cellBorderOutline);
        }

        $i = 3;
        foreach((array)$classHomework[$classID] as $homeworkID=>$homeworkInfo) {
            $columnTitle = $homeworkInfo['SubjectGroupName']."\n".$homeworkInfo['Title'];
            $activeSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($i) . '1', $columnTitle);
            $activeSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($i) . '1')->getAlignment()->setWrapText(true);
            $activeSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($i))->setAutoSize(true);
            $activeSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($i). '1')->applyFromArray($cellBorderOutline);
            $i++;
        }
//        $activeSheet->getStyle('A1:' . (PHPExcel_Cell::stringFromColumnIndex($i - 1)) . '1')->getFont()->setBold(true);

        $i = 2;
        foreach((array)$studentHomework[$classID] as $classNumber=>$homeworkInfo) {
            $currentHomework = current($homeworkInfo);
            $studentName = $currentHomework['StudentName'];

            $activeSheet->setCellValue('A' . $i, $className);
            $activeSheet->setCellValue('B' . $i, $classNumber);
            $activeSheet->setCellValue('C' . $i, $studentName);

            for ($k=0;$k<3;$k++) {
                $activeSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($k) . $i)->applyFromArray($cellBorderOutline);
            }

            $j = 0;
            foreach((array)$classHomework[$classID] as $homeworkID=>$classHomeworkInfo) {
                $activeSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($j+3) . $i, '');
                if (!isset($homeworkInfo[$homeworkID]) || $homeworkInfo[$homeworkID]['HandinRequired'] == 0) {
                    $activeSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($j+3) . $i)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'cccccc')
                            )
                        )
                    );
                }
                $activeSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($j+3).$i)->applyFromArray($cellBorderOutline);
                $j++;
            }
            $i++;
        }

//        debug_pr(PHPExcel_Cell::stringFromColumnIndex($j+2).($i-1));
//        $activeSheet->getStyle('A1:'.(PHPExcel_Cell::stringFromColumnIndex($j+2).($i-1)))->applyFromArray($cellBorderOutline);
//        $ActiveSheet[$className] = $activeSheet;

        $sheetIndex++;
    }
    $objPHPExcel->setActiveSheetIndex(0);       // reset default to first sheet
}       // has data

else {
    // Set active sheet
    $objPHPExcel->setActiveSheetIndex($sheetIndex);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $activeSheet->setTitle('No Record');

    $activeSheet->setCellValue('A1',$Lang['SysMgr']['Homework']['NoRecord']);
}

intranet_closedb();

// output file
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');

//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');

exit;

//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//
//    $tmp_folder = $intranet_root.'/file/homework';
//    if(!is_dir($tmp_folder)){
//        $lf->folder_new($tmp_folder);
//    }
//    $tmp_file_name = 'tmp'.md5(uniqid()).'.xls';
//    $tmp_file = $tmp_folder.'/'.$tmp_file_name;
//
//    //shell_exec('touch '.$tmp_file);
//    //shell_exec('chmod 777 '.$tmp_file);
//    file_put_contents($tmp_file,'');
//    chmod($tmp_file, 0777);
//
//    $objWriter->save($tmp_file);
//
//    $content = get_file_content($tmp_file);
//    $lf->file_remove($tmp_file);
//    intranet_closedb();
//    output2browser($content, $file_name);








?>
