<?php
# using by : 

###############################################
#
#   Date:   2019-09-06 (Tommy)
#           change access permission checking
#
#	Date:	2019-05-13 (Bill)
#           - prevent SQL Injection
#
#	Date:	2017-10-12 (Bill)	[2017-1009-1539-28235]
#			Date Range Searching > Start Date 
#
###############################################

$CurrentPageArr['eAdminHomework'] = 1;

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else
{
	//if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
	    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

### Handle SQL Injection + XSS [START]
$yearID = IntegerSafe($yearID);
$yearTermID = IntegerSafe($yearTermID);

$classID = IntegerSafe($classID);
$teacher = IntegerSafe($teacher);
$subject = IntegerSafe($subject);
$subjectGroup = IntegerSafe($subjectGroup);

if(!intranet_validateDate($from)) {
    $from = date('Y-m-d');
}
if(!intranet_validateDate($to)) {
    $to = date('Y-m-d');
}
### Handle SQL Injection + XSS [END]

$lexport = new libexporttext();

$ExportArr = array();

// [Removed] not use $cond for searching [2015-0203-1618-41066]
//$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;

$teachers = $lhomework->getTeachers($yearID, $yearTermID);

# SQL Statement
/*
if(sizeof($teachers)!=0){
	$allTeachers = " a.PosterUserID IN (";
	for ($i=0; $i < sizeof($teachers); $i++)
	{	
		list($teachID)=$teachers[$i];
		$allTeachers .= $teachID.",";
	}
	$allTeachers = substr($allTeachers,0,strlen($allTeachers)-1).")";
	$allTeachers .= " AND";
}
else{
	$allTeachers ="";
}
*/

if($teacher!=""){
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($teacher, $subject, $yearID, $yearTermID, $classID);
}

else{
	$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subject, $yearID, $yearTermID, $classID);
}
	
if(sizeof($subjectGroups)!=0){
	$allGroups = " a.ClassGroupID IN (";
	for ($i=0; $i < sizeof($subjectGroups); $i++)
	{	
		list($groupID)=$subjectGroups[$i];
		$allGroups .= $groupID.",";
	}
	$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
	$allGroups .= " AND";
}
else{
	$allGroups ="";
	$subjectGroup ="";	
}

//$date_conds = " date(a.LastModified) >= '$from' AND date(a.LastModified) <= '$to'";
$date_conds = " DATE(a.StartDate) >= '$from' AND DATE(a.StartDate) <= '$to'";

$conds = ($yearID=="")? "" : " a.AcademicYearID = '$yearID' AND";
$conds .= ($yearTermID=="")? "" : " a.YearTermID = '$yearTermID' AND";

//$conds .= ($teacher=="")? $allTeachers : " a.PosterUserID = $teacher AND";
//$conds .= ($subject=="")? "" : " a.SubjectID = $subject AND";
$conds .= ($subjectGroup=="")? $allGroups : " a.ClassGroupID = '$subjectGroup' AND"; 

// [Removed] not use $cond for searching [2015-0203-1618-41066]
//$conds .= ($s=="")? "" : " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
//							$searchBySubject
//							or a.Title like '%$s%'
//							) AND";

// Use $having for searching [2015-0203-1618-41066]
$having = "";
if(trim($s)!=""){
	$having = " HAVING SubjectGroup LIKE '%$s%' OR title LIKE '%$s%'";
	$having .= ($lhomework->subjectSearchDisabled == 0) ? " OR Subject LIKE '%$s%'" : "" ;
	$having .= ($lhomework->teacherSearchDisabled == 0)? " OR name like '%$s%'" : "" ;
	$having .= ($lhomework->teacherSearchDisabled == 0) ? " OR teacher_name like '%$s%'" : "" ;
}
				
$filename = "Homework_Report_".$from."_".$to.".csv";

/*if ($teacher != "")
{
	$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID = '$teacher'";
	$name = $lhomework->returnVector($sql,1);
	$filename = "Homework_Report_".$name[0]."_AllSubjectGroups_".$from."_".$to.".csv";
}

if ($subject != "")
{
	$sql = "SELECT EN_DES FROM ASSESSMENT_SUBJECT WHERE RecordID = '$subject'";
	$sname = $lhomework->returnVector($sql,1);
	$filename = "Homework_Report_".$name[0]."_".$sname[0]."_AllSubjectGroups_".$from."_".$to.".csv";
}

if ($subjectGroup != "")
{
	$sql = "SELECT ClassTitleEN FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = '$subjectGroup'";
	$sgname = $lhomework->returnVector($sql,1);
	$filename = "Homework_Report_".$name[0]."_".$sname[0]."_".$sgname[0]."_".$from."_".$to.".csv";
}*/

# Select Data from DB
// add $having input [2015-0203-1618-41066]
$row = $lhomework->exportHomeworkReport($date_conds, $conds, $classID, $having);

// Create data array for export
for($i=0; $i<sizeof($row); $i++){
	$m = 0;
	$ExportArr[$i][$m] = $row[$i][0];			// Subject
	$m++;
	$ExportArr[$i][$m] = $row[$i][1];			// Subject Group
	$m++;
	if($lhomework->useHomeworkType) {
		$ExportArr[$i][$m] = $row[$i][7];		// Topic
		$m++;
	}
	$ExportArr[$i][$m] = $row[$i][2];			// Topic
	$m++;
	$ExportArr[$i][$m] = html_entity_decode(str_replace(array("\n", "\r", "\t", ","), array(" ", " ", "", ""), $row[$i][3]), ENT_QUOTES);		// Description
	$m++;
	$ExportArr[$i][$m] = $row[$i][8];			// Poster Name
	$m++;	
	$ExportArr[$i][$m] = $row[$i][9];			// Subject Group Teacher Nmae [Added - 2015-02-11]
	$m++;	
	$ExportArr[$i][$m] = $row[$i][4];			// Start Date
	$m++;
	$ExportArr[$i][$m] = $row[$i][5];			// Due Date
	$m++;
	$ExportArr[$i][$m] = $row[$i][6];			// Loading
	$m++;
}
if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}

$exportColumn = array($Lang['SysMgr']['Homework']['Subject'], $Lang['SysMgr']['Homework']['SubjectGroup']);
if($lhomework->useHomeworkType) {
	//$exportColumn = array_push($exportColumn, $i_Homework_HomeworkType);	
	$exportColumn[] = $i_Homework_HomeworkType;
}

// Add CSV header [2015-0203-1618-41066]
//$exportColumn = array_merge($exportColumn, array($Lang['SysMgr']['Homework']['Topic'], $Lang['SysMgr']['Homework']['Description'], $Lang['SysMgr']['Homework']['TeacherSubjectLeader'], $Lang['SysMgr']['Homework']['StartDate'], $Lang['SysMgr']['Homework']['DueDate'], $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")"));	
// Change CSV Header
$exportColumn = array_merge($exportColumn, array($Lang['SysMgr']['Homework']['Topic'], $Lang['SysMgr']['Homework']['Description'], $Lang['General']['LastModifiedBy'], $Lang['SysMgr']['Homework']['SubjectGroupTeacher'], $Lang['SysMgr']['Homework']['StartDate'], $Lang['SysMgr']['Homework']['DueDate'], $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")"));	

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>