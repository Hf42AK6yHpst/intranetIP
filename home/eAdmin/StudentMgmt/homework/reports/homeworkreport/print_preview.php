<?php
# using by : 

###############################################
#
#   Date:   2019-09-06 (Tommy)
#           change access permission checking
#
#	Date:	2019-05-13 (Bill)
#           - prevent SQL Injection
#
#	Date:	2017-10-12 (Bill)	[2017-1009-1539-28235]
#			Date Range Searching > Start Date 
#
###############################################

$CurrentPageArr['eAdminHomework'] = 1;

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else
{
	//if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
	    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

### Handle SQL Injection + XSS [START]
$yearID = IntegerSafe($yearID);
$yearTermID = IntegerSafe($yearTermID);

$classID = IntegerSafe($classID);
$teacher = IntegerSafe($teacher);
$subject = IntegerSafe($subject);
$subjectGroup = IntegerSafe($subjectGroup);

if(!intranet_validateDate($from)) {
    $from = date('Y-m-d');
}
if(!intranet_validateDate($to)) {
    $to = date('Y-m-d');
}
### Handle SQL Injection + XSS [END]

$linterface = new interface_html();

$s = addcslashes($s, '_');

// [Removed] not use $cond for searching [2015-0203-1618-41066]
//$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "";

$teachers = $lhomework->getTeachers($yearID, $yearTermID);

# SQL Statement
/*
if(sizeof($teachers)!=0){
	$allTeachers = " a.PosterUserID IN (";
	for ($i=0; $i < sizeof($teachers); $i++)
	{	
		list($teachID)=$teachers[$i];
		$allTeachers .= $teachID.",";
	}
	$allTeachers = substr($allTeachers,0,strlen($allTeachers)-1).")";
	$allTeachers .= " AND";
}
else{
	$allTeachers ="";
}
*/

if($teacher != ""){
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($teacher, $subject, $yearID, $yearTermID, $classID);
}
else{
	$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subject, $yearID, $yearTermID, $classID);
}

if(sizeof($subjectGroups)!=0){
	$allGroups = " a.ClassGroupID IN (";
	for ($i=0; $i < sizeof($subjectGroups); $i++)
	{	
		list($groupID) = $subjectGroups[$i];
		$allGroups .= $groupID.",";
	}
	$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
	$allGroups .= " AND";
}
else{
	$allGroups = "";
	$subjectGroup = "";
}

//$date_conds = " date(a.LastModified) >= '$from' AND date(a.LastModified) <= '$to'";
$date_conds = " DATE(a.StartDate) >= '$from' AND DATE(a.StartDate) <= '$to'";

$conds = ($yearID=="")? "" : " a.AcademicYearID = '$yearID' AND";
$conds .= ($yearTermID=="")? "" : " a.YearTermID = '$yearTermID' AND";

//$conds .= ($teacher=="")? $allTeachers : " a.PosterUserID = $teacher AND";
//$conds .= ($subject=="")? "" : " a.SubjectID = $subject AND";
$conds .= ($subjectGroup=="")? $allGroups : " a.ClassGroupID = '$subjectGroup' AND";

// [Removed] not use $cond for searching [2015-0203-1618-41066]
//$conds .= ($s=="")? "" : " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
//							$searchBySubject
//							or a.Title like '%$s%'
//							) AND";

// Use $having for searching [2015-0203-1618-41066]
$having = "";
if(trim($s)!=""){
	$having = " HAVING SubjectGroup LIKE '%$s%' OR title LIKE '%$s%'";
	$having .= ($lhomework->subjectSearchDisabled == 0) ? " OR Subject LIKE '%$s%'" : "" ;
	$having .= ($lhomework->teacherSearchDisabled == 0)? " OR name like '%$s%'" : "" ;
	$having .= ($lhomework->teacherSearchDisabled == 0) ? " OR teacher_name like '%$s%'" : "" ;
}

$yearTermName= $Lang['SysMgr']['Homework']['AllYearTerms'];
$teacherName= $Lang['SysMgr']['Homework']['AllTeachers'];
$subjectName= $Lang['SysMgr']['Homework']['AllSubjects'];
$subjectGroupName= $Lang['SysMgr']['Homework']['AllSubjectGroups'];

if ($yearID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', YearNameEN, YearNameB5) FROM ACADEMIC_YEAR WHERE AcademicYearID = '$yearID'";
	$name = $lhomework->returnVector($sql,1);
	$yearName = $name[0];
}

if ($yearTermID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', YearTermNameEN, YearTermNameB5) FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '$yearTermID'";
	$name = $lhomework->returnVector($sql,1);
	$yearTermName = $name[0];
}

if ($teacher != "")
{
	$sql = "SELECT IF((EnglishName!='' AND ChineseName!='' AND EnglishName != ChineseName), Concat(EnglishName,' ',ChineseName), EnglishName) FROM INTRANET_USER WHERE UserID = '$teacher'";
	$name = $lhomework->returnVector($sql,1);
	$teacherName = $name[0];
}

if($classID != "")
{
	$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID = '$classID'";
	$clsName = $lhomework->returnVector($sql);
	$className = $clsName[0];
}

if ($subject != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = '$subject'";
	$sname = $lhomework->returnVector($sql,1);
	$subjectName = $sname[0];
}

if ($subjectGroup != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = '$subjectGroup'";
	$sgname = $lhomework->returnVector($sql,1);
	$subjectGroupName = $sgname[0];
}

# Select Data from DB
// add $having input [2015-0203-1618-41066]
$x = $lhomework->printHomeworkReport($date_conds, $conds, $classID, $having);
?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['HomeworkReport']?></b></td>
	</tr>
</table>        

<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['AcademicYear']?> : </td><td align="left" valign="top"><?=$yearName?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['YearTerm']?> : </td><td align="left" valign="top"><?=$yearTermName?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Teacher']?> : </td><td align="left" valign="top"><?=$teacherName?></td></tr>
	<? if($className!='') { ?>
		<tr><td align="left" valign="top" width="100"><?=$i_ClassName?> : </td><td align="left" valign="top"><?=$className?></td></tr>
	<? } ?>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subjectName?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> : </td><td align="left" valign="top"><?=$subjectGroupName?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['From']?> : </td><td align="left" valign="top"><?=$from?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['To']?> : </td><td align="left" valign="top"><?=$to?></td></tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>