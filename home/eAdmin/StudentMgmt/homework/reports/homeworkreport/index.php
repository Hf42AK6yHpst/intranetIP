<?php
# using by : 

###############################################
#
#   Date:   2019-09-06 (Tommy)
#           change access permission checking 
#
#	Date:	2017-10-12 (Bill)	[2017-1009-1539-28235]
#			Date Range Searching > Start Date 
#
###############################################

$CurrentPageArr['eAdminHomework'] = 1;

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else{
    //if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
	{
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();

$CurrentPage = "Reports_HomeworkReport";

# Page
$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkReport'];

# Tag
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkReport'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Date Menu
$today = date('Y-m-d');
$temp = strtotime('-0 day',  strtotime($today));
$temp = date('m', $temp);
if($temp < 9) {
	$year = date('Y', strtotime('-1 year',  strtotime($today)));
}
else {
	$year = date('Y', strtotime('-0 day',  strtotime($today)));
}
$datefrom = ($from == "") ? "$year-09-01" : $from;
$dateto = ($to == "") ? $today : $to;

// [Removed] not use $cond for searching [2015-0203-1618-41066]
//$s = addcslashes($s, '_');
//$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
//$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;
$allowExport = $lhomework->exportAllowed;
	
# Sort field
$sortField = 0;

# Change Page Size
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") 
	$page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# Table Initialization
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create DB Table
$li = new libdbtable2007($field, $order, $pageNo);

# Target Year and Year Selection
$academicYear = $lhomework->GetAllAcademicYear();
$yearID = ($yearID=="")? Get_Current_Academic_Year_ID() : $yearID;
$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" id=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);

# Valid Target Year
if($yearID!="")
{
	if($yid != $yearID) {
		$yearTermID = "";
		$teacherID = "";
		$subjectID = "";
	}
	
	# Target Semester and Semester Selection
	$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
	$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" id=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
	if($ytid != $yearTermID){
		$teacherID = "";
		$subjectID = "";
	}
	
	# Teacher / Subject Leaders Selection
	$teachers = $lhomework->getTeachers($yearID, $yearTermID);
// 	$selectedTeachers = $lhomework->getCurrentTeachers("name=\"teacherID\" onChange=\"reloadForm()\"", $teachers, $teacherID, $Lang['SysMgr']['Homework']['AllTeachers']);
	
	# Option - All Teachers / Subject Leaders
	$selectedCreator = "<select name='teacherID' id='teacherID' onChange='reloadForm()'>";
	// [Removed] for Last Modified By filtering [2015-0203-1618-41066]
//	$selectedCreator .= "<option value='' class='select_form_group'>-- ". $Lang['SysMgr']['Homework']['AllTeachersSubjectLeaders'] ." --</option>";
	$selectedCreator .= "<option value='' class='select_form_group'>-- ". $Lang['SysMgr']['Homework']['AllLastModifiedBy'] ." --</option>";
	
	# Option - Teachers
	$AllTeacherStr = "";
	$selectedCreator .= "<option value='-1' class='select_form_group' ". ($teacherID==-1 ? "selected":"").">-- ". $Lang['SysMgr']['Homework']['AllTeachers'] ." --</option>";
	for ($i=0; $i<sizeof($teachers); $i++)
	{
		list($thisID, $teacherName) = $teachers[$i];
		$selected_str = ($thisID==$teacherID? "SELECTED":"");
		$selectedCreator .= "<OPTION value='$thisID' class='select_class' $selected_str>&nbsp;&nbsp;$teacherName</OPTION>\n";
		$AllTeacherStr .= $thisID.",";
	}
		
	# Option - Subject Leaders 
	$AllSubjectLeaderStr = "";
	$SubjectLeaders = $lhomework->getSubjectLeaders($yearID, $yearTermID);
	$selectedCreator .= "<option value='-2' class='select_form_group' ". ($teacherID==-2 ? "selected":"").">-- ". $Lang['SysMgr']['Homework']['AllSubjectLeaders'] ." --</option>";
	for ($i=0; $i < sizeof($SubjectLeaders); $i++)
	{
		list($SLID, $SLName) = $SubjectLeaders[$i];
		$selected_str = ($SLID==$teacherID? "SELECTED":"");
		$selectedCreator .= "<OPTION value='$SLID' class='select_class' $selected_str>&nbsp;&nbsp;$SLName</OPTION>\n";
		$AllSubjectLeaderStr .= $SLID.",";
	}
	$selectedCreator .= "</select>";
	
	if($tid != $teacherID){
		$subjectID = "";
	}
	
	# Class Selection
	$classes = $lhomework->getAllClassInfo();
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $i_general_all_classes);
	
	# Get Teaching Subjects and Subject Selection
	if($teacherID!=""){
		$subject = $lhomework->getTeachingSubjectList($teacherID, $yearID, $yearTermID, $classID);
	}

	else{
		$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
	}
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
	
	# Get Target Subject Groups and Subject Group Selection
	if($subjectID!="")
	{
		if($sid != $subjectID){
			$subjectGroupID="";
		}
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($teacherID, $subjectID, $yearID, $yearTermID, $classID);
		$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" id=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
	}
	else{
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($teacherID, $subjectID, $yearID, $yearTermID, $classID);
	}
}

# Drop Down List
//$filterbar = "$selectedYear $selectedYearTerm $selectedTeachers $selectedSubject $selectedSubjectGroups";
$filterbar = "$selectedYear $selectedYearTerm $selectedCreator $selectClass $selectedSubject $selectedSubjectGroups";

# Print link
$printPreviewLink = "print_preview.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&teacher=$teacherID&subject=$subjectID&subjectGroup=$subjectGroupID&from=$datefrom&to=$dateto&s=$s";

# Export link
$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&teacher=$teacherID&subject=$subjectID&subjectGroup=$subjectGroupID&from=$datefrom&to=$dateto&s=$s";

# Toolbar: Print, Export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );
if($allowExport) {
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );
}

# SQL Statement
# Teacher Filtering
if(sizeof($teachers)!=0) {
	$allTeachers = " a.PosterUserID IN (";
	$allTeachers .= substr($AllTeacherStr,0,strlen($AllTeacherStr)-1).")";
	$allTeachers .= " AND";
	
// 	for ($i=0; $i < sizeof($teachers); $i++)
// 	{	
// 		list($teachID)=$teachers[$i];
// 		$allTeachers .= $teachID.",";
// 	}
// 	$allTeachers = substr($allTeachers,0,strlen($allTeachers)-1).")";
// 	$allTeachers .= " AND";
}
else{
	$allTeachers ="";
}

# Subject Leader Filtering
if(sizeof($SubjectLeaders)!=0) {
	$allSubjectLeaders = " a.PosterUserID IN (";
	$allSubjectLeaders .= substr($AllSubjectLeaderStr,0,strlen($AllSubjectLeaderStr)-1).")";
	$allSubjectLeaders .= " AND";
}
else{
	$allSubjectLeaders ="";
}

# Subject Group Filtering
if(sizeof($subjectGroups)!=0){
	$allGroups = " a.ClassGroupID IN (";
	for ($i=0; $i < sizeof($subjectGroups); $i++)
	{	
		list($groupID)=$subjectGroups[$i];
		$allGroups .= $groupID.",";
	}
	$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
	$allGroups .= " AND";
}
else{
	$allGroups =""; 
	$subjectGroupID ="";
}

# Conditions
// searching period - use start date and due date [DM#2849]
//$date_conds = " date(a.LastModified) >= '$datefrom' AND date(a.LastModified) <= '$dateto'";
//$date_conds = " ((date(a.StartDate) >= '$datefrom' AND date(a.StartDate) <= '$dateto') OR (date(a.DueDate) >= '$datefrom' AND date(a.DueDate) <= '$dateto'))";
//$date_conds = " date(a.StartDate) >= '$datefrom' AND date(a.DueDate) <= '$dateto'";

# Date
$date_conds = " DATE(a.StartDate) >= '$datefrom' AND DATE(a.StartDate) <= '$dateto'";

# Poster
//$conds = ($teacherID=='')? "$allTeachers" : " a.PosterUserID = $teacherID AND";
switch($teacherID)
{
	case -1:
		$conds = $allTeachers;
		break;
	case -2:
		$conds = $allSubjectLeaders;
		break;
	case "":
		break;
	default:
		$conds = " a.PosterUserID = $teacherID AND";
		break;
}

# Subject Groups
$conds .= ($subjectGroupID=='')? " $allGroups" : " a.ClassGroupID = $subjectGroupID AND";

// [Removed] not use $cond for searching [2015-0203-1618-41066]
//$conds .= ($s=='')? "": " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
//							   $searchByTeacher 
//							   $searchBySubject
//							   or a.Title like '%$s%'
//						      ) AND" ;

// [2015-0203-1618-41066] Use $having for searching
$having = "";
$s = addcslashes($s, '_');
if(trim($s)!='') {
	$having = " HAVING SubjectGroup LIKE '%$s%' OR title LIKE '%$s%'";
	$having .= ($lhomework->teacherSearchDisabled == 0)? " OR name like '%$s%'" : "" ;
	$having .= ($lhomework->teacherSearchDisabled == 0) ? " OR teacher_name like '%$s%'" : "" ;
	$having .= ($lhomework->subjectSearchDisabled == 0) ? " OR Subject LIKE '%$s%'" : "" ;
}

# Fields
$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";

$name = getNameFieldWithClassNumberByLang("d.");
$teacher_name = getNameFieldByLang("u.");

$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
		   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup, ";
if($lhomework->useHomeworkType) {
	$fields .= " IF(e.TypeName!='', e.TypeName, '---'), ";	
}
// Update SQL to query Teacher of Subject Group [2015-0203-1618-41066]
$fields .= "CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')),
		   $name as name,
		   IFNULL(GROUP_CONCAT(DISTINCT $teacher_name SEPARATOR ',<br>'), '--') as teacher_name,
		   a.StartDate, 
		   a.DueDate, 
		   a.Loading/2,
		   a.Title as title";
//$dbtables = "INTRANET_HOMEWORK as a 
//			LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID 
//			LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
//			inner join INTRANET_USER as d on (d.UserID=a.PosterUserID)";
$dbtables = "INTRANET_HOMEWORK as a
			LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID 
			LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
			INNER JOIN INTRANET_USER as d ON (d.UserID = a.PosterUserID)
			LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as sct ON (sct.SubjectGroupID = a.ClassGroupID)
			LEFT OUTER JOIN INTRANET_USER u ON (sct.UserID = u.UserID) ";
if($lhomework->useHomeworkType) {
	$dbtables .= " LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE e ON (e.TypeID=a.TypeID)";	
}			
$conds .= " $date_conds AND a.AcademicYearID = $yearID";
if($classID!="") {
	$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";	
	$conds .= " AND ycu.YearClassID='$classID'";
}
$conds .= ($yearTermID=='')? "" : " AND a.YearTermID = $yearTermID";

// Added $having to SQL [2015-0203-1618-41066]
$sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.HomeworkID $having";

// Set count_mode - prevent error message [2015-0203-1618-41066]
$li->count_mode = 1;
$li->field_array = array("Subject", "SubjectGroup");
if($lhomework->useHomeworkType) {
	array_push($li->field_array, "e.TypeName");	
}
$li->field_array = array_merge($li->field_array, array("a.Title", "name", "teacher_name", "a.StartDate","a.DueDate", "a.Loading"));
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = 2;

# Table Column
$pos = 0;
// Set all width = 20% to 15% [2015-0203-1618-41066]
$li->column_list .= "<td width='5%'>#</td>\n";
$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
if($lhomework->useHomeworkType) {
	$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $i_Homework_HomeworkType)."</td>\n";
}
$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
// Change Column Title
$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['General']['LastModifiedBy'])."</td>\n";
$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroupTeacher'])."</td>\n";
$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
$li->column_list .= "<td width='5%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
function reloadForm() {
	if (!check_date(document.form1.from,"<?php echo $i_invalid_date; ?>")) return false;
    if (!check_date(document.form1.to,"<?php echo $i_invalid_date; ?>")) return false;
    if(!checkalldate(document.form1.from.value, document.form1.to.value)) return false;
	
	document.form1.s.value = "";
	document.form1.pageNo.value = 1;
	document.form1.submit();
}

function checkalldate(t1, t2){
    if (compareDate(t2, t1) < 0) {alert ("<?=$Lang['SysMgr']['Homework']['ToDateWrong']?>"); return false;}
    return true;
}

function goSearch() {
	document.form1.s.value = document.form2.text.value;
	document.form1.pageNo.value = 1;
	document.form1.submit();
}

function viewHmeworkDetail(id){
	newWindow('../../management/homeworklist/view.php?hid='+id,1);
}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right"><input name="text" id="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
							<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?>
						</td>
					</tr>
				</table>
			</form>
			
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="30" align="left" valign="bottom">
								 <table>
										<tr>
											<td><?=$i_Homework_startdate?>: <?=$Lang['SysMgr']['Homework']["From"]?></td>
											<td align="left" valign="top">
												<INPUT TYPE="text" NAME="from" id="from" SIZE=10 maxlength=10 onChange="reloadForm()" value='<?=$datefrom?>'><?=$linterface->GET_CALENDAR("form1", "from", "reloadForm();")?>
											</td>
											<td valign="top"><?=$Lang['SysMgr']['Homework']["To"]?></td>
											<td align="left" valign="top">
												<INPUT TYPE="text" NAME="to" id="to" SIZE=10 maxlength=10 onChange="reloadForm()" value='<?=$dateto?>'><?=$linterface->GET_CALENDAR("form1", "to", "reloadForm();")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>			
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$li->display()?>
					</td>
				</tr>
			</table>
			
			<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="s" id="s" value="<?=$s?>"/>
			<input type="hidden" name="yid" id="yid" value="<?=$yearID?>"/>
			<input type="hidden" name="ytid" id="ytid" value="<?=$yearTermID?>"/>
			<input type="hidden" name="tid" id="tid" value="<?=$teacherID?>"/>
			<input type="hidden" name="sid" id="sid" value="<?=$subjectID?>"/>
			
			</form>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>