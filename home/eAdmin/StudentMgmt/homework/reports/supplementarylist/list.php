<?php
// Modifing by : 
		
##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######		
		
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Reports_SupplementaryList";

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['SupplementaryList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SupplementaryList'], "");

$_SESSION['targetList'] = $targetListResult;
$_SESSION['studentList'] = $studentListResult;

# Date link
if($dateChoice=='YEAR') {
	$dateLink = "dateChoice=YEAR&yearID=$yearID&yearTermID=$yearTermID";
}
else { 
	$dateLink = "dateChoice=DATE&startDate=$startDate&endDate=$endDate";
	//$currentYearTerm = getAcademicYearAndYearTermByDate($startDate);
	$currentYearTerm = getAcademicYearInfoAndTermInfoByDate($startDate);
	$yearID = $currentYearTerm[0];
	$yearTermID = $currentYearTerm[2];
	# if YearTermID of start date & end date are different, then homework should be included both semesters
	$currentYearTerm2 = getAcademicYearInfoAndTermInfoByDate($endDate);
	$endDate_yearTermID = $currentYearTerm2[2];
	if($endDate_yearTermID!=$yearTermID)
		$yearTermID = "";
}

# Print link

$printPreviewLink = "print_preview.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday";
# Export link
$exportLink = "export.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday";

# Toolbar: print, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );

$allowExport = $lhomework->exportAllowed;
if($allowExport) {
	$toolbar .= $linterface->GET_LNK_EXPORT("$exportLink", "", "", "", "", 0 );
}


$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
list($yearName, $yearTermName) = $Name[0];
if($yearTermID=="") $yearTermName = $Lang['SysMgr']['Homework']['AllYearTerms'];

$data = $lhomework->getSupplementaryList($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate, $endDate, $dateChoice, $times, $range, $group, $order, $DueDateEqualToToday);

$result = "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0'>";
$result .= "<tr>";
$result .= "<td class='tablebluetop tabletopnolink' width='4%'>#</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['Class']."</td>";
if($group==1)
	$result .= "<td class='tablebluetop tabletopnolink' width='15%'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='15%'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='20%'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
if($group==1)
	$result .= "<td class='tablebluetop tabletopnolink' width='20%'>".$Lang['SysMgr']['Homework']['StudentName']."</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='20%'>".$Lang['SysMgr']['Homework']['SupplementaryCount']."</td>";
$result .= "</tr>";


if(sizeof($data)!=0){
	for($i=0; $i<sizeof($data); $i++){
		$css = ($i%2)? "class='tabletext tablerow_total seperator_right'" : "";
		list($className, $calssNumber, $subject, $subjectGroup, $studentName, $recordCount) = $data[$i];
		$result .= "<tr $css>";
		$result .= "<td>".($i+1)."</td>";
		$result .= "<td>".$className."</td>";
		if($group==1)
			$result .= "<td>".$calssNumber."</td>";
		$result .= "<td>".$subject."</td>";
		$result .= "<td>".$subjectGroup."</td>";
		if($group==1)
			$result .= "<td>".$studentName."</td>";
		$result .= "<td>".$recordCount."</td>";
		$result .= "</tr>";
	}
}

else{
	$result .= "<tr>";
	$result .= "<td align=\"center\" colspan=\"7\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>";
	$result .= "</tr>";
}

$result .= "</table>";

$linterface->LAYOUT_START();
	
?>
<script language="javascript">
function back(){
	window.location = "index.php";
}

function viewNotHandinDetail(subjectGroupID, userID, yearID, yearTermID, yearClassID){
   newWindow('./view.php?subjectGroupID='+subjectGroupID+'&studentID='+userID+'&yearID='+yearID+'&yearTermID='+yearTermID+'&yearClassID='+yearClassID+'&group=<?=$group?>&startDate=<?=$startDate?>&endDate=<?=$endDate?>&dateChoice=<?=$dateChoice?>&DueDateEqualToToday=<?=$DueDateEqualToToday?>',1);
}
</script>

<form name="form1">
	<table width="100%" border="0" cellpadding="5" cellspacing="5">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="2" width="300">
					<tr>
						<td><?=$toolbar?></td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="5" cellpadding="5">
				<? if($dateChoice=='YEAR') { ?>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['AcademicYear']?></td>
						<td width="567" align="left" valign="top">
							<?=$yearName?>
						</td>
					</tr>
					
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['YearTerm']?></td>
						<td width="567" align="left" valign="top">
							<?=$yearTermName?>
						</td>
					</tr>
				<? } else { ?>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$iDiscipline['Period']?></td>
						<td width="567" align="left" valign="top">
							<?=$startDate." ".$i_To." ".$endDate?>
						</td>
					</tr>
				<? } ?>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				<?=$result?>
			</td>
		</tr>
		
		<tr>
			<td class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		
		<tr>
			<td align="center">
				<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:back()")?>
			</td>
		
		</tr>
	</table>
	<input type="hidden" name="targetList[]" value="<?=$targetListResult?>"/>
	<input type="hidden" name="studentList[]" value="<?=$studentListResult?>"/>
</form>

 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>