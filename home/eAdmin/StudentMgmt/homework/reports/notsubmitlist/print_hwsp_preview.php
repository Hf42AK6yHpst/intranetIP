<?php
// Modifing by : 

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	
	intranet_auth();
	intranet_opendb();

	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	else{
// 		if(
// 			$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
// 			|| !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']
// 			|| !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]
// 				|| $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)
// 			)
// 		)
	    if(
	        $_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
	        || !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']
	        || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]
	            && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)
	            )
	        )
		{
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
	
	$yearID = Get_Current_Academic_Year_ID();
	$handin_param = array(
			"yearID" => $yearID,
			"selectedDate" => (isset($_GET["selectedDate"])) ? $_GET["selectedDate"] : date("Y-m-d"),
			"custOrderBy" => "studentID",
			"isReport" => true,
			"callBy" => basename(__FILE__)
	);
	
	$handin_param = array(
			"yearID" => $yearID,
			"dataopt" => (isset($_GET["dataopt"])) ? $_GET["dataopt"] : "duedate_range",
			"startDate" => (isset($_GET["startDate"])) ? $_GET["startDate"] : date("Y-m-d"),
			"endDate" => (isset($_GET["endDate"])) ? $_GET["endDate"] : date("Y-m-d"),
			"selectedDate" => (isset($_GET["selectedDate"])) ? $_GET["selectedDate"] : date("Y-m-d"),
			"custOrderBy" => "studentID",
			"HandInStatusID" => "-20",
			"isReport" => true,
			"callBy" => basename(__FILE__)
	);
	
	$showTotalViolation = false;
	$print_title = "";
	switch ($_GET["hwrp_type"]) {
		case "notsubmit" :
			$print_title = $Lang['SysMgr']['Homework']['InputNotSubmitRecord'];
			$handin_param["HandInStatusID"] = "-1";
			break;
		case "cancel_violation":
			$print_title = $Lang['SysMgr']['Homework']['InputCancelViolationRecord'];
			$handin_param["cancelOnly"] = true;
			$cancelViolation = true;
		case "hw_violation":
			if (empty($print_title)) {
				$print_title = $Lang['SysMgr']['Homework']['InputViolationRecord'];
			}
			$handin_param["HandInStatusID"] = "-20";
			if ($_GET["hiddenname"] == "1") {
				$showTotalViolation = true;
			}
			break;
		default:
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
			break;
	}
	
	$handin_studentsArr = $lhomework->getStudentsByParam($handin_param);
	
	$tableResult = '<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">';
	$tableResult .= "<thead>";
	$tableResult .= "<tr>";
	$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>#</th>";
	$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['Class'] . " / " . $Lang['SysMgr']['Homework']['ClassNumber'] . "</th>";
	if ($_GET["hiddenname"] != "1") {
		$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['StudentName'] . "</th>";
	}
	$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['DueDateForViolation'] . "</th>";
	$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['Subject'] . "</th>";
	$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['Topic'] . "</th>";
	if ($cancelViolation) {
		$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['CancelViolationDate'] . "</th>";
	} else if ($showTotalViolation) {
		$tableResult .= "<th class='eHomeworktdborder eHomeworkprinttabletitle'>" . $Lang['SysMgr']['Homework']['TotalViolation'] . "</th>";
	}
	$tableResult .= "</tr>";
	$tableResult .= "</thead>";

	if (count($handin_studentsArr) > 0) {
		if ($showTotalViolation && !$cancelViolation) {
			foreach ($handin_studentsArr as $kk => $vv) {
				$studentIDMarkAsViolation[$vv["UserID"]] = 0;
			}
			$studentIDMarkAsViolation = $lhomework->getTotalViolationByUserIDs(array_keys($studentIDMarkAsViolation));
		}
		$i = 1;
		foreach ($handin_studentsArr as $kk => $vv) {
			$studentName = $vv["StudentName"];
			if (empty($studentName)) $studentName = $vv["UserLogin"];
			$tableResult .= '<tbody>';
			$tableResult .= '<tr>';
			$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $i . "</td>\n";
			$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $vv["ClassTitle"] . " / " . $vv["ClassNumber"] . "</td>\n";
			if ($_GET["hiddenname"] != "1") {
				$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $studentName . "</td>\n";
			}
			$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $vv["DueDate"] . "</td>\n";
			$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $vv["Subject"] . "</td>\n";
			$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $vv['Title'] . "</td>\n";
			if ($cancelViolation) {
				$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . $vv['CancelViolationDate'] . "</td>\n";
			} else if ($showTotalViolation) {
				$tableResult .= '	<td class="eHomeworktdborder eHomeworkprinttext">' . (isset($studentIDMarkAsViolation[$vv['StudentID']]) ? $studentIDMarkAsViolation[$vv['StudentID']] : 0) . "</td>\n";
			}
			$tableResult .= '</tr>';
			$tableResult .= '</tbody>';
			$i++;
		}
	}
	
	$tableResult .= '</table>';
	
	$linterface = new interface_html();
	if(!isset($status) || $status=="") $status = "-1";
?>
	<table width='100%' align='center' class='print_hide' border=0>
		<tr>
			<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		</tr>
	</table>
<!-- Homework Title -->
	<table width='100%' align="center" border="0">
		<tr>
			<td align="center" ><b><?php echo $handin_param["selectedDate"] . " - " . $print_title; ?></b></td>
		</tr>
	</table>
<?
	echo $tableResult . "<br>";
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
	intranet_closedb();
?>