<?php
// Modifing by
/*
 *  2020-10-09 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

else{
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
        if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
        {
            header("location: ../../settings/index.php");
            exit;
        }
        include_once($PATH_WRT_ROOT."includes/libaccessright.php");
        $laccessright = new libaccessright();
        $laccessright->NO_ACCESS_RIGHT_REDIRECT();
        exit;
    }
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$startDateAcademicYearInfo = getAcademicYearAndYearTermByDate($startDate);
$endDateAcademicYearInfo = getAcademicYearAndYearTermByDate($endDate);
$ret = ($startDateAcademicYearInfo['AcademicYearID'] == $endDateAcademicYearInfo['AcademicYearID']) ? 1 : 0;
echo $ret;      // return true if the same

intranet_closedb();

?>