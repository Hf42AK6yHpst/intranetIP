<?php
// Modifing by : 

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######		
	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
	include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();
	
	$libsms = new libsmsv2();
	$lhomework = new libhomework2007();
	$luser = new libuser();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
// 		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}
	

	if(!isset($status) || $status=="" || $status != "-1")
	{
		die("This function is to inform parents for their children's records of not handing homework only!");
	}
	//$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);
	
	$ExportArr = array();
		
	$filename = "HandinStatusList.csv";
	
	# Select Data from DB
	$order = 1;  # must order by student in order to group different subjects
	$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);

	$previous_user_id = 0;
	$SubjectsInvolved = array();
	
	if ($yearID!="" && $yearTermID!="")
	{
		$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
		list($yearName, $yearTermName) = $Name[0];
	}
	
	$libuser = new libuser($UserID);
	$notifyuser = $libuser->UserNameLang();
	
	$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
	
	
	$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
	$MsgNotifyDateTime = date("Y-m-d H:i:s");

	$NotifyMessageID = -1;
	
	$namefield = ($intranet_default_lang =="b5" || $intranet_default_lang == "gb") ? 'ChineseName' : 'EnglishName';

	# get parent app users list
	$sql = "select distinct UserID from INTRANET_API_REQUEST_LOG";
	$parentAppUserArr = $libuser->returnArray($sql);
	$parentAppUserIDArr = array();
	foreach ((array)$parentAppUserArr as $parentAppUserInfo) {
		$parentAppUserIDArr[] = $parentAppUserInfo['UserID'];
	}
	
	for($i=0; $i<sizeof($row); $i++)
	{
		list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID) = $row[$i];
		//if (($previous_user_id!=$StudentID && $i>0) || $i==sizeof($row)-1)
		if ($StudentID!=$row[$i+1]["UserID"])
		{			
			# message content
			$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
			$SubjectsText = implode(", ", $SubjectsInvolved);
			$StudentNameApps = $StudentName;
			if ($ClassName!="" && $ClassNumber!="")
			{
				$StudentNameApps .= " ({$ClassName}-{$ClassNumber})";
			}       
			if ($yearName!="" || $yearTermName!="")
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['In'] . $yearName . " " . $yearTermName;
			} elseif ($startDate==$endDate)
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['On']. $startDate;
			} else
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['From'] . $startDate. $Lang['AppNotifyMessage']['Homework']['To'] .$endDate. $Lang['AppNotifyMessage']['Homework']['Period'];
			}
			$MessageContent = str_replace("[DateInvolved]", $DateInvolved, str_replace("[Subjects]", $SubjectsText, str_replace("[StudentName]", $StudentNameApps, $Lang['AppNotifyMessage']['Homework']['Alert'])));
			
			# get Parent's userlogin
			$sql = "SELECT iu.UserLogin, iu.UserID from INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu WHERE ip.StudentID='{$StudentID}' AND ip.ParentID=iu.UserID  AND iu.RecordStatus=1 ";
			$recipient_rows = $lhomework->returnResultSet($sql);
			$recipient_parent_userlogin = array();
			$parentAppParentArr = array();
			$nonParentAppParentArr = array();
			for ($ri=0; $ri<sizeof($recipient_rows); $ri++)
			{
				$parentID = $recipient_rows[$ri]["UserID"];
				
				# check Parent App users and record parentID
				if (in_array($parentID, (array)$parentAppUserIDArr)) {
					$recipient_parent_userlogin[$ri] = $recipient_rows[$ri]["UserLogin"];
					$parentAppParentArr[$ri] = $parentID;
				} else {
					# record non Parent App users
					if (!in_array($parentID, (array)$nonParentAppParentArr)) {
						$nonParentAppParentArr[$ri] = $parentID;
					}
				}
			}
			
			# generate push message content
			$MessageTitle = $Lang['AppNotifyMessage']['Homework']['Title'] . " [".$DateInvolved."]";
			$ASLParam = array(
						"SchID" => $SchoolID,
						"MsgNotifyDateTime" => $MsgNotifyDateTime,
						"MessageTitle" => $MessageTitle,
						"MessageContent" => $MessageContent,
						"NotifyFor" => $notifyfor,
						"IsPublic" => "N",
						"NotifyUser" => $notifyuser,
						"TargetUsers" => array(
							"TargetUser" => $recipient_parent_userlogin
						)
				);
			$ReturnMessage = "";
			
			# send by push message first
			if (sizeof($recipient_parent_userlogin)>0)
			{
				$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
				if ($Result)
				{
					if ($NotifyMessageID==-1)
					{
						$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE(NotifyDateTime,MessageTitle,MessageContent,NotifyFor,IsPublic,NotifyUser,NotifyUserID,RecordStatus,DateInput,DateModified)
								VALUES ('$MsgNotifyDateTime','".addslashes($MessageTitle)."','MULTIPLE MESSAGES','".$notifyfor."','N','".addslashes($notifyuser)."','$UserID','1',NOW(),NOW())";
					
						$success = $lhomework->db_db_query($sql);
						$NotifyMessageID = $lhomework->db_insert_id();
					}
				
					if ($NotifyMessageID>0)
					{
						for ($ri=0; $ri<sizeof($parentAppParentArr); $ri++)
						{
							$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_TARGET(NotifyMessageID,TargetType,TargetID,MessageTitle,MessageContent,DateInput)
									VALUES ('$NotifyMessageID','U','".$parentAppParentArr[$ri]."','".addslashes($MessageTitle)."','".addslashes($MessageContent)."',NOW())";
							$target_success = $lhomework->db_db_query($sql);
							
							$SentTotal ++;
						}
					}
				}
			}
			########### Send SMS to non Parent App users OR send SMS to all users when Parent App fail ###########
			if (sizeof($nonParentAppParentArr)>0 || !$Result) {
				// debug_r($Result);
				if ($Result) {
					$smsParentArr = $nonParentAppParentArr;
				} else {
					$smsParentArr = array_merge((array)$nonParentAppParentArr, (array)$parentAppParentArr);
				}
				
				foreach ((array)$smsParentArr as $smsParentUserID) {
					$ParentID = $smsParentUserID;
					$sql = "
							SELECT 
								UserID, 
								TRIM(MobileTelNo) as Mobile,
								$namefield as UserName
							FROM 
								INTRANET_USER 
							WHERE 
								UserID = $ParentID
							ORDER BY 
								ClassName,
								ClassNumber,
								EnglishName
						";
					$users = $libsms->returnArray($sql, 3);
					
					// list($userid,$mobile) = $users[$i];
					$ParentID = $users[0]['UserID'];
					$mobile = $users[0]['Mobile'];
					$ParentName = $users[0]['UserName'];
					
					// debug_pr($ParentID);
					if ($ClassNumber==0)
					{
						$ClassNumber = "";
					}
					$mobile = $libsms->parsePhoneNumber($mobile);
					
					if($libsms->isValidPhoneNumber($mobile)){
					// debug_pr($mobile);
						$recipientData[] = array($mobile,$StudentID,addslashes($ParentName),$MessageContent);
						$valid_list[] = array($ParentID,$ParentName,$mobile,$StudentName,$ClassName,$ClassNumber);
					}else{
						$reason =$i_SMS_Error_NovalidPhone;
						$error_list[] = array($ParentID,$ParentName,$mobile,$StudentName,$ClassName,$ClassNumber,$reason);
					}
				}
			}
			$SubjectsInvolved = array();
		} else
		{
			//debug($i, $previous_user_id, $StudentID );
			//debug_r( $row[$i]);
			$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
		}
		//$previous_user_id = $StudentID;
	}
	
	# send sms
	if(sizeof($recipientData)>0){
		$targetType = 3;
		$picType = 2;
		$adminPIC =$PHP_AUTH_USER;
		$userPIC = $UserID;
		$frModule= "";
		$deliveryTime = $time_send;
		$isIndividualMessage=true;
		$sms_message = ""; #### Use original text - not htmlspecialchars processed
		$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
		$isSent = 1;
		$smsTotal = sizeof($recipientData);
	}
	
	intranet_closedb();
	
	// if ($SentTotal>0 && $SentTotal!="")
	// {
		// echo "<font color='#DD5555'>".str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['Homework']['send_result']). "</font>";
	// } else
	// {
		// echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['send_failed']."</font>";
	// }
	
	if (($SentTotal>0 && $SentTotal!="") || ($isSent && ($returnCode > 0 && $returnCode==true)))
	{
		if ($SentTotal == "") {
			$SentTotal = 0;
		}
		if ($smsTotal == "") {
			$smsTotal = 0;
		}
		
		$returnMsg = str_replace("[smsTotal]", $smsTotal, str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS_send_result']));
		echo "<font color='#DD5555'>".$returnMsg. "</font>";

	} else {
		echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['SMS_send_failed']."</font>";
	}
	
	
?>