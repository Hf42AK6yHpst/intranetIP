<?php
// Modifing by : 

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######		
	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
	include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();
	
	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
// 		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	

	if(!isset($status) || $status=="" || $status != "-1")
	{
		die("This function is to inform parents for their children's records of not handing homework only!");
	}
	//$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);
	
	$ExportArr = array();
		
	$filename = "HandinStatusList.csv";
	
	# Select Data from DB
	$order = 1;  # must order by student in order to group different subjects
	$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);

	$previous_user_id = 0;
	$SubjectsInvolved = array();
	
	if ($yearID!="" && $yearTermID!="")
	{
		$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
		list($yearName, $yearTermName) = $Name[0];
	}
	
	$libuser = new libuser($UserID);
	$notifyuser = $libuser->UserNameLang();
	
	$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
	
	
	$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
	$MsgNotifyDateTime = date("Y-m-d H:i:s");

	$NotifyMessageID = -1;
	
	# get parent app users list
	$sql = "select distinct UserID from INTRANET_API_REQUEST_LOG";
	$parentAppUserArr = $libuser->returnArray($sql);
	$parentAppUserIDArr = array();
	foreach ((array)$parentAppUserArr as $parentAppUserInfo) {
		$parentAppUserIDArr[] = $parentAppUserInfo['UserID'];
	}

	for($i=0; $i<sizeof($row); $i++)
	{
		list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID) = $row[$i];
		//if (($previous_user_id!=$StudentID && $i>0) || $i==sizeof($row)-1)
		if ($StudentID!=$row[$i+1]["UserID"])
		{			
			$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
			# send the message
			$SubjectsText = implode(", ", $SubjectsInvolved);
			//debug($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $SubjectsText, $StudentID);
			$StudentNameApps = $StudentName;
			if ($ClassName!="" && $ClassNumber!="")
			{
				$StudentNameApps .= " ({$ClassName}-{$ClassNumber})";
			}
			if ($yearName!="" || $yearTermName!="")
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['In'] . $yearName . " " . $yearTermName;
			} elseif ($startDate==$endDate)
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['On']. $startDate;
			} else
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['From'] . $startDate. $Lang['AppNotifyMessage']['Homework']['To'] .$endDate. $Lang['AppNotifyMessage']['Homework']['Period'];
			}
			$MessageCentent = str_replace("[DateInvolved]", $DateInvolved, str_replace("[Subjects]", $SubjectsText, str_replace("[StudentName]", $StudentNameApps, $Lang['AppNotifyMessage']['Homework']['Alert'])));
					
			# get Parent's userlogin
			$sql = "SELECT iu.UserLogin, iu.UserID from INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu WHERE ip.StudentID='{$StudentID}' AND ip.ParentID=iu.UserID  AND iu.RecordStatus=1 ";
			$recipient_rows = $lhomework->returnResultSet($sql);
			$recipient_parent_userlogin = array();
			$parentAppParentArr = array();
			for ($ri=0; $ri<sizeof($recipient_rows); $ri++)
			{
				$parentID = $recipient_rows[$ri]["UserID"];
				
				# check Parent App user
				if (in_array($parentID, $parentAppUserIDArr)) {
					$recipient_parent_userlogin[] = $recipient_rows[$ri]["UserLogin"];
					$parentAppParentArr[$ri] = $parentID;
				} else {
					# record non Parent App UserID
					$nonParentAppUserArr[] = $parentID;
				}
			}
			
			$MessageTitle = $Lang['AppNotifyMessage']['Homework']['Title'] . " [".$DateInvolved."]";
			$ASLParam = array(
						"SchID" => $SchoolID,
						"MsgNotifyDateTime" => $MsgNotifyDateTime,
						"MessageTitle" => $MessageTitle,
						"MessageContent" => $MessageCentent,
						"NotifyFor" => $notifyfor,
						"IsPublic" => "N",
						"NotifyUser" => $notifyuser,
						"TargetUsers" => array(
							"TargetUser" => $recipient_parent_userlogin
						)
				);
			$ReturnMessage = "";
			// debug_r($ASLParam);
			if (sizeof($recipient_parent_userlogin)>0)
			{
				$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
			} else
			{
				$Result = false;
			}
			if ($Result)
			{
				// debug_r($ASLParam);
				if ($NotifyMessageID==-1)
				{
					$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE(NotifyDateTime,MessageTitle,MessageContent,NotifyFor,IsPublic,NotifyUser,NotifyUserID,RecordStatus,DateInput,DateModified)
							VALUES ('$MsgNotifyDateTime','".addslashes($MessageTitle)."','MULTIPLE MESSAGES','".$notifyfor."','N','".addslashes($notifyuser)."','$UserID','1',NOW(),NOW())";
				
					$success = $lhomework->db_db_query($sql);
					$NotifyMessageID = $lhomework->db_insert_id();
				}
			
				if ($NotifyMessageID>0)
				{
					for ($ri=0; $ri<sizeof($parentAppParentArr); $ri++)
					{
						$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_TARGET(NotifyMessageID,TargetType,TargetID,MessageTitle,MessageContent,DateInput)
								VALUES ('$NotifyMessageID','U','".$parentAppParentArr[$ri]."','".addslashes($MessageTitle)."','".addslashes($MessageCentent)."',NOW())";
						$target_success = $lhomework->db_db_query($sql);
					}
				}
				$SentTotal ++;
			} else
			{
				
			}
			$SubjectsInvolved = array();
		} else
		{
			//debug($i, $previous_user_id, $StudentID );
			//debug_r( $row[$i]);
			$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
		}
		//$previous_user_id = $StudentID;
	}
	intranet_closedb();
	
	if ($SentTotal>0 && $SentTotal!="")
	{
		echo "<font color='#DD5555'>".str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['Homework']['send_result']). "</font>";
	} else
	{
		echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['send_failed']."</font>";
	}
?>