<?php
// Modifing by :
/*
 *
 *  2020-10-08 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
if(!$plugin['eHomework']){
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
} elseif($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$sys_custom['eHomework']['CustomizedPrintAndExport']) || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$lhomework->isViewerGroupMember($UserID))) {
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
    {
        header("location: ../../settings/index.php");
        exit;
    }
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

## get filters
$targetList = IntegerSafe($_POST['targetList']);
$studentList = IntegerSafe($_POST['studentList']);
$times = IntegerSafe($_POST['times']);
$range = IntegerSafe($_POST['range']);
$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$subjectID = IntegerSafe($_POST['subjectID']);
$targetID = IntegerSafe($_POST['targetID']);
$status = IntegerSafe($_POST['status']);
$printType = IntegerSafe($_POST['printType']);

$academicYearTermInfo = getAcademicYearAndYearTermByDate($startDate);
$yearID = $academicYearTermInfo['AcademicYearID'];
$yearTermID = $academicYearTermInfo['YearTermID'];

# if YearTermID of start date & end date are different, then homework should be included both semesters
$academicYearTermInfo2 = getAcademicYearAndYearTermByDate($endDate);
$endDateYearTermID = $academicYearTermInfo2['YearTermID'];
if($endDateYearTermID != $yearTermID) {
    $yearTermID = "";
}

$linterface = new interface_html();

$displayStatus = '';
$statusAry = $lhomework->Get_Handin_Status();
foreach($statusAry as $_statusAry) {
    if ($_statusAry[0] == $status) {
        $displayStatus = $_statusAry[1];
        break;
    }
}


$data = $lhomework->getCustomizedStatusReport($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $times, $range, $status, $printType);


$reportFilter = <<<FILTER
    <table width='95%' border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td width="10%">{$Lang['Header']['Menu']['Period']}: </td>
            <td>{$startDate} {$Lang['General']['To']} {$endDate} </td>
        </tr>
        <tr>
            <td width="10%">{$Lang['eHomework']['HandinStatus']}: </td>
            <td>{$displayStatus} </td>
        </tr>
    </table>
FILTER;

$x = '';
if (count($data['data'])) {
    $i = 0;
    if ($printType == 1) {      // by form
        $formAry = $data['form'];
        $homeworkAry = $data['data'];
        foreach((array) $formAry as $formID=>$formName) {
            $pageBreakStyle = $i > 0 ? 'style="page-break-before:always;"' : '';
            if ($i > 0) {
                $x .= "<br><br>";
            }
            $i++;       // form counter
            $j = 1;     // count number of record for each form

            // report title
            $x .= '<table width="100%" align="center" border="0"'.$pageBreakStyle.'>';
                $x .= '<tr>';
                    $x .= '<td align="center" style="font-weight:bold;">'.$Lang['eHomework']['HandinStatusReport'].'</td>';
                $x .= '</tr>';
            $x .= '</table>'."\n";

            $x .= $reportFilter;

            // form
            $x .= '<table width="95%" border="0" cellpadding="5" cellspacing="0">';
                $x .= '<tr>';
                    $x .= '<td width="10%">'.$Lang['SysMgr']['Homework']['Form2'].': </td>';
                    $x .= '<td>'.$formName.'</td>';
                $x .= '</tr>';
            $x .= '</table>'."\n";

            $x .= "<br><br>";

            // details
            $x .= '<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0">';
            $x .= '<thead>';
            $x .= '<tr>';
                $x .= '<td width="1%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">#</td>';
                $x .= '<td width="5%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Class'].'</td>';
                $x .= '<td width="5%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['ClassNumber'].'</td>';
                $x .= '<td width="10%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['StudentName'].'</td>';
                $x .= '<td width="10%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Subject'].'</td>';
                $x .= '<td width="15%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['SubjectGroup'].'</td>';
                $x .= '<td width="20%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Title'].'</td>';
                $x .= '<td width="24%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Content'].'</td>';
                $x .= '<td width="10%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['DueDate'].'</td>';
            $x .= '</tr>';
            $x .= '</thead>';


            $x .= '<tbody>';
            foreach((array)$homeworkAry[$formID] as $yearClassID=>$_homeworkAry) {
                foreach((array)$_homeworkAry as $handinRecordID=>$__homeworkAry) {
                    $x .= '<tr>';
                        $x .= '<td width="1%" class="eHomeworktdborder eHomeworkprinttext">'.$j.'</td>';
                        $x .= '<td width="5%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['ClassName'].'</td>';
                        $x .= '<td width="5%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['ClassNumber'].'</td>';
                        $x .= '<td width="10%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['StudentName'].'</td>';
                        $x .= '<td width="10%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['Subject'].'</td>';
                        $x .= '<td width="15%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['SubjectGroup'].'</td>';
                        $x .= '<td width="20%" class="eHomeworktdborder eHomeworkprinttext">'.($__homeworkAry['Title'] ? $__homeworkAry['Title'] : '--').'</td>';
                        $x .= '<td width="24%" class="eHomeworktdborder eHomeworkprinttext">'.($__homeworkAry['Description'] ? $__homeworkAry['Description'] : '--').'</td>';
                        $x .= '<td width="10%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['DueDate'].'</td>';
                    $x .= '</tr>';
                    $j++;
                }
            }

            $x .= '</tbody>';
            $x .= '</table>'."\n";
        }
    }


    else {      // by class
        $classAry = $data['class'];
        $homeworkAry = $data['data'];
        foreach((array) $classAry as $yearClassID=>$className) {
            $pageBreakStyle = $i > 0 ? 'style="page-break-before:always;"' : '';
            if ($i > 0) {
                $x .= "<br><br>";
            }
            $i++;       // yearClass counter
            $j = 1;     // count number of record for each class

            // report title
            $x .= '<table width="100%" align="center" border="0"'.$pageBreakStyle.'>';
            $x .= '<tr>';
            $x .= '<td align="center" style="font-weight:bold;">'.$Lang['eHomework']['HandinStatusReport'].'</td>';
            $x .= '</tr>';
            $x .= '</table>'."\n";

            $x .= $reportFilter;

            // class
            $x .= '<table width="95%" border="0" cellpadding="5" cellspacing="0">';
            $x .= '<tr>';
            $x .= '<td width="10%">'.$Lang['SysMgr']['Homework']['Class'].': </td>';
            $x .= '<td>'.$className.'</td>';
            $x .= '</tr>';
            $x .= '</table>'."\n";

            $x .= "<br><br>";

            // details
            $x .= '<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<td width="1%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">#</td>';
            $x .= '<td width="5%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Class'].'</td>';
            $x .= '<td width="5%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['ClassNumber'].'</td>';
            $x .= '<td width="10%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['StudentName'].'</td>';
            $x .= '<td width="10%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Subject'].'</td>';
            $x .= '<td width="15%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['SubjectGroup'].'</td>';
            $x .= '<td width="20%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Title'].'</td>';
            $x .= '<td width="24%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['Content'].'</td>';
            $x .= '<td width="10%" valign="middle" class="eHomeworktdborder eHomeworkprinttabletitle">'.$Lang['SysMgr']['Homework']['DueDate'].'</td>';
            $x .= '</tr>';
            $x .= '</thead>';


            $x .= '<tbody>';
            foreach((array)$homeworkAry[$yearClassID] as $handinRecordID=>$__homeworkAry) {
                $x .= '<tr>';
                $x .= '<td width="1%" class="eHomeworktdborder eHomeworkprinttext">'.$j.'</td>';
                $x .= '<td width="5%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['ClassName'].'</td>';
                $x .= '<td width="5%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['ClassNumber'].'</td>';
                $x .= '<td width="10%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['StudentName'].'</td>';
                $x .= '<td width="10%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['Subject'].'</td>';
                $x .= '<td width="15%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['SubjectGroup'].'</td>';
                $x .= '<td width="20%" class="eHomeworktdborder eHomeworkprinttext">'.($__homeworkAry['Title'] ? $__homeworkAry['Title'] : '--').'</td>';
                $x .= '<td width="24%" class="eHomeworktdborder eHomeworkprinttext">'.($__homeworkAry['Description'] ? $__homeworkAry['Description'] : '--').'</td>';
                $x .= '<td width="10%" class="eHomeworktdborder eHomeworkprinttext">'.$__homeworkAry['DueDate'].'</td>';
                $x .= '</tr>';
                $j++;
            }

            $x .= '</tbody>';
            $x .= '</table>'."\n";
        }
    }
}
else {
    $x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">';
        $x .= '<tr>';
            $x .= '<td align="center">'.$Lang['SysMgr']['Homework']['NoRecord'].'</td>';
        $x .= '</tr>';
    $x .= '</table>'."\n";
}

?>

    <table width='100%' align='center' class='print_hide' border=0>
        <tr>
            <td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
        </tr>
    </table>

    <br>

<?=$x?>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>