<?php
// Modifing by : 

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

session_start();
$targetList = $_SESSION["targetList"];
$studentList = $_SESSION["studentList"];

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
$leClassApp = new libeClassApp();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}


if(!isset($status) || $status=="" || $status != "-1")
{
	die("This function is to inform parents for their children's records of not handing homework only!");
}
//$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);


# Select Data from DB
$order = 1;  # must order by student in order to group different subjects
$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);
$studentIdAry = Get_Array_By_Key($row, 'UserID');


$previous_user_id = 0;
$SubjectsInvolved = array();

if ($yearID!="" && $yearTermID!="") {
	$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
	list($yearName, $yearTermName) = $Name[0];
}



### get parent student mapping
$sql = "SELECT 
				ip.ParentID, ip.StudentID
		from 
				INTRANET_PARENTRELATION AS ip 
				Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
		WHERE 
				ip.StudentID IN ('".implode("','", (array)$studentIdAry)."') 
				AND iu.RecordStatus = 1";
$studentParentAssoAry = BuildMultiKeyAssoc($lhomework->returnResultSet($sql), 'StudentID', array('ParentID'), $SingleValue=1, $BuildNumericArray=1);



### build push message info array
$messageInfoAry = array();
$pushMsgCount = 0;
for($i=0; $i<sizeof($row); $i++)
{
	list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID) = $row[$i];
	//if (($previous_user_id!=$StudentID && $i>0) || $i==sizeof($row)-1)
	if ($StudentID!=$row[$i+1]["UserID"])
	{			
		$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
		# send the message
		$SubjectsText = implode(", ", $SubjectsInvolved);
		//debug($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $SubjectsText, $StudentID);
		$StudentNameApps = $StudentName;
		if ($ClassName!="" && $ClassNumber!="") {
			$StudentNameApps .= " ({$ClassName}-{$ClassNumber})";
		}
		
		if ($yearName!="" || $yearTermName!="") {
			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['In'] . $yearName . " " . $yearTermName;
			$MessageCentent = $Lang['AppNotifyMessage']['Homework']['Alert']['app']['single_day'];
			$MessageCentent = str_replace("[StartDate]", $DateInvolved, $MessageCentent);
		} 
		else if ($startDate==$endDate) {
//			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['On']. $startDate;
			$DateInvolved = $startDate;
			$MessageCentent = $Lang['AppNotifyMessage']['Homework']['Alert']['app']['single_day'];
			$MessageCentent = str_replace("[StartDate]", $startDate, $MessageCentent);
		} 
		else {
//			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['From'] . $startDate. $Lang['AppNotifyMessage']['Homework']['To'] .$endDate. $Lang['AppNotifyMessage']['Homework']['Period'];
			
			$MessageCentent = $Lang['AppNotifyMessage']['Homework']['Alert']['app']['period'];
			$MessageCentent = str_replace("[StartDate]", $startDate, $MessageCentent);
			$MessageCentent = str_replace("[EndDate]", $endDate, $MessageCentent);
		}
		$MessageCentent = str_replace("[Subjects]", $SubjectsText, $MessageCentent);
		$MessageCentent = str_replace("[StudentName]", $StudentNameApps, $MessageCentent);
//		$MessageCentent = str_replace("[DateInvolved]", $DateInvolved, str_replace("[Subjects]", $SubjectsText, str_replace("[StudentName]", $StudentNameApps, $Lang['AppNotifyMessage']['Homework']['Alert'])));
		$MessageTitle = $Lang['AppNotifyMessage']['Homework']['Title'];
		
		
		$_parentIdAry = $studentParentAssoAry[$StudentID];
		$_numOfParent = count($_parentIdAry);
		if ($_numOfParent > 0) {
			for ($j=0; $j<$_numOfParent; $j++) {
				$__parentId = $_parentIdAry[$j];
				$messageInfoAry[$pushMsgCount]['relatedUserIdAssoAry'][$__parentId] = array($StudentID);
			}
			
			$messageInfoAry[$pushMsgCount]['messageTitle'] = $MessageTitle;
			$messageInfoAry[$pushMsgCount]['messageContent'] = $MessageCentent;
			$pushMsgCount++;
		}
		
		$SubjectsInvolved = array();
	} else
	{
		//debug($i, $previous_user_id, $StudentID );
		//debug_r( $row[$i]);
		$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
	}
	//$previous_user_id = $StudentID;
}


$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, 'MULTIPLE MESSAGES');
$statisticsAry = $leClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

intranet_closedb();

if ($notifyMessageId > 0) {
	echo "<font color='#DD5555'>".str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['Homework']['send_result']). "</font>";

} else
{
	echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['send_failed']."</font>";

}
?>