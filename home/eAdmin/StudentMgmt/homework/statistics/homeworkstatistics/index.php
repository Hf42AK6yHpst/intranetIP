<?php
// Modifing by 
###### Change Log [Start] ######
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date	:	2010-11-18 YatWoon
#				add "Total WorkLoad" data
#
#	Date	:	2010-09-29 Henry Chow
#				modified printHomeworkStatistics(), display homework "statistics" depends on $classID
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
    // 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


$CurrentPage = "Statistics_HomeworkStatistics";

# page title
$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkStatistics'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkStatistics'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Date Menu
$today = date('Y-m-d');
$temp = strtotime('-0 day',  strtotime($today));
$temp = date('m', $temp);

if($temp < 9)
	$year = date('Y', strtotime('-1 year',  strtotime($today)));
	else
		$year = date('Y', strtotime('-0 day',  strtotime($today)));
		
		$datefrom = ($from == "") ? "$year-09-01" : $from;
		$dateto = ($to == "") ? $today : $to;
		
		$s = addcslashes($s, '_');
		$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', f.EN_DES, f.CH_DES) like '%$s%'" : "" ;
		
		# Select sort field
		$sortField = 0;
		
		# change page size
		if ($page_size_change == 1)
		{
			setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
			$ck_page_size = $numPerPage;
		}
		
		if (isset($ck_page_size) && $ck_page_size != "")
			$page_size = $ck_page_size;
			
			$pageSizeChangeEnabled = true;
			
			# Table initialization
			$order = ($order == "") ? 1 : $order;
			$field = ($field == "") ? $sortField : $field;
			$pageNo = ($pageNo == "") ? 1 : $pageNo;
			
			# Create a new dbtable instance
			$li = new libdbtable2007($field, $order, $pageNo);
			
			# Academic Year
			$academicYear = $lhomework->GetAllAcademicYear();
			$academicYearID = ($academicYearID=="")? Get_Current_Academic_Year_ID():$academicYearID;
			
			$selectedYear = $lhomework->getCurrentYear("name=\"academicYearID\" onChange=\"reloadForm()\"", $academicYear, $academicYearID, "", false);
			
			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($academicYearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			
			# Form Menu
			$forms = $lhomework->getForms($academicYearID);
			$selectedForms = $lhomework->getCurrentForms("name=\"formID\" onChange=\"reloadForm()\"", $forms, $formID, $Lang['SysMgr']['Homework']['AllForms']);
			
			# Class Menu
			$classes = $lhomework->getAllClassInfo('',$formID);
			$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="reloadForm()"', $classID, 0, 0, $i_general_all_classes);
			
			if(($fid!="" && $fid != $formID) || ($ytid!="" && $ytid != $yearTermID)){
				$subjectID="";
			}
			
			if($formID!="")
				$subject = $lhomework->getFormSubjectList($formID, $academicYearID, $yearTermID, $classID);
				else
					$subject = $lhomework->getFormSubjectList("", $academicYearID, $yearTermID, $classID);
					
					$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
					
					
					$filterbar = "$selectedYear $selectedYearTerm $selectedForms $selectClass $selectedSubject";
					
					# SQL statement
					if(sizeof($forms)!=0){
						$allForms = " c.YearID IN (";
						for ($i=0; $i < sizeof($forms); $i++)
						{
							list($yearID)=$forms[$i];
							$allForms .= $yearID.",";
						}
						$allForms = substr($allForms,0,strlen($allForms)-1).")";
					}
					else{
						$allForms ="";
					}
					
					if(sizeof($subject)!=0){
						$allSubjects = " AND f.RecordID IN (";
						for ($i=0; $i < sizeof($subject); $i++)
						{
							list($subjID)=$subject[$i];
							$allSubjects .= $subjID.",";
						}
						$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
					}
					else{
						$allSubjects ="";
						$subjectID ="";
					}
					
					$yearTerm = ($yearTermID=="")? "" : "AND d.YearTermID = $yearTermID";
					# set conditions
					$date_conds = " AND unix_timestamp(d.DueDate) >= unix_timestamp('$datefrom') AND unix_timestamp(d.DueDate) <= unix_timestamp('$dateto') AND d.AcademicYearID = $academicYearID $yearTerm";
					$conds = ($academicYearID=='')? "" : " h.AcademicYearID = $academicYearID AND";
					$conds .= ($yearTermID=='')? "" : " g.YearTermID = $yearTermID AND";
					$conds .= ($formID=='')? "$allForms" : " c.YearID = $formID";
					$conds .= ($subjectID=='')? "$allSubjects" : " AND f.RecordID = $subjectID ";
					$conds .= ($s=='')? "": " AND (IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) like '%$s%'
					$searchBySubject
					) ";
					
					$fields = "IF('$intranet_session_language' = 'en', g.YearTermNameEN, g.YearTermNameB5) AS YearTermName,
					IF('$intranet_session_language' = 'en', f.EN_DES, f.CH_DES) AS Subject,
					IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup, count(DISTINCT d.HomeworkID),
					(sum(d.Loading)/(count(if(d.Loading=2,1,0))/count(DISTINCT d.HomeworkID))*0.5) as TotalLoading
					";
					/*
					 $dbtables = "SUBJECT_TERM_CLASS AS a
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION AS b ON b.SubjectGroupID = a.SubjectGroupID
					 LEFT OUTER JOIN YEAR AS c ON c.YearID = b.YearID
					 LEFT OUTER JOIN INTRANET_HOMEWORK AS d ON (d.ClassGroupID = a.SubjectGroupID $date_conds)
					 LEFT OUTER JOIN SUBJECT_TERM AS e ON e.SubjectGroupID = a.SubjectGroupID
					 LEFT OUTER JOIN ASSESSMENT_SUBJECT AS f ON f.RecordID = e.SubjectID
					 LEFT OUTER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = e.YearTermID
					 LEFT OUTER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
					 */
					$dbtables = "SUBJECT_TERM_CLASS AS a
					INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION AS b ON b.SubjectGroupID = a.SubjectGroupID
					INNER JOIN YEAR AS c ON c.YearID = b.YearID
					LEFT OUTER JOIN INTRANET_HOMEWORK AS d ON (d.ClassGroupID = a.SubjectGroupID $date_conds)
					LEFT OUTER JOIN SUBJECT_TERM AS e ON e.SubjectGroupID = a.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS f ON f.RecordID = e.SubjectID
					INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = e.YearTermID
					INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
					
					
					if($classID!="") {
						$dbtables .= " INNER JOIN SUBJECT_TERM_CLASS_USER k ON (k.SubjectGroupID=e.SubjectGroupID)
					INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=k.UserID)";
						$conds .= " AND ycu.YearClassID=$classID";
					}
					
					$conds .= " GROUP BY a.SubjectGroupID";
					$sql = "SELECT $fields FROM $dbtables WHERE $conds ASC";
					
					$li->field_array = array("YearTermName", "Subject", "SubjectGroup", "count(d.HomeworkID)", "TotalLoading");
					$li->sql = $sql;
					$li->no_col = sizeof($li->field_array)+1;
					$li->IsColOff = 2;
					$li->fieldorder2 = ", ".Get_Lang_Selection("a.ClassTitleB5", "a.ClassTitleEN");
					
					
					# TABLE COLUMN
					$pos = 0;
					$li->column_list .= "<td width='5%'>#</td>\n";
					$li->column_list .= "<td width='25%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['YearTerm'])."</td>\n";
					$li->column_list .= "<td width='25%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
					$li->column_list .= "<td width='40%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
					$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']["HomeworkCount"])."</td>\n";
					$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']["TotalWorkload"])."</td>\n";
					
					$allowExport = $lhomework->exportAllowed;
					
					# Print link
					$printPreviewLink = "print_preview.php?yearID=$academicYearID&yearTermID=$yearTermID&formID=$formID&classID=$classID&subjectID=$subjectID&from=$datefrom&to=$dateto&s=$s";
					# Export link
					$exportLink = "export.php?yearID=$academicYearID&yearTermID=$yearTermID&formID=$formID&classID=$classID&subjectID=$subjectID&from=$datefrom&to=$dateto&s=$s";
					
					# Toolbar: new, import, export
					$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );
					if($allowExport)
						$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);
						
						# Start layout
						$linterface->LAYOUT_START();
						?>
<script language="javascript">
	function reloadForm() {
		if (!check_date(document.form1.from,"<?php echo $i_invalid_date; ?>")) return false;
        if (!check_date(document.form1.to,"<?php echo $i_invalid_date; ?>")) return false;
        if(!checkalldate(document.form1.from.value, document.form1.to.value)) return false;
		
		document.form1.s.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
	
	function checkalldate(t1, t2){
        if (compareDate(t2, t1) < 0) {alert ("<?=$Lang['SysMgr']['Homework']['ToDateWrong']?>"); return false;}
        return true;
	}
	
	function goSearch() {
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right"><input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
							<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="30" align="left" valign="bottom">
								 <table>
										<tr>
											<td><?=$Lang['SysMgr']['Homework']["From"]?></td>
											<td align="left" valign="top">
												<INPUT TYPE="text" NAME="from" SIZE=10 maxlength=10 onChange="reloadForm()" value='<?=$datefrom?>'><?=$linterface->GET_CALENDAR("form1", "from", "reloadForm();")?>
											</td>
											<td valign="top"><?=$Lang['SysMgr']['Homework']["To"]?></td>
											<td align="left" valign="top">
												<INPUT TYPE="text" NAME="to" SIZE=10 maxlength=10 onChange="reloadForm()" value='<?=$dateto?>'><?=$linterface->GET_CALENDAR("form1", "to", "reloadForm();")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>			
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$li->display()?>
					</td>
				</tr>
			</table>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
				<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
				<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
				<input type="hidden" name="page_size_change" value=""/>
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
				<input type="hidden" name="s" value="<?=$s?>"/>
				<input type="hidden" name="fid" value="<?=$formID?>"/>
				<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>