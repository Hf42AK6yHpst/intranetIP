<?php
# using by : 

###############################################
#
#   Date    2019-09-06 (Tommy)
#           - change access permission checking
#
#	Date:	2019-05-14 (Bill)
#           - prevent SQL Injection
#
###############################################
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

### Handle SQL Injection + XSS [START]
$academicYearID = IntegerSafe($academicYearID);
$yearTermID = IntegerSafe($yearTermID);

$formID = IntegerSafe($formID);
$classID = IntegerSafe($classID);
$subjectID = IntegerSafe($subjectID);

if(!intranet_validateDate($datefrom)) {
    $datefrom = date('Y-m-d');
}
if(!intranet_validateDate($dateto)) {
    $dateto = date('Y-m-d');
}
### Handle SQL Injection + XSS [END]

$linterface = new interface_html();

$academicYearTerm = $lhomework->getAllAcademicYearTerm($academicYearID);
$lhomework->GenerateClassStats($academicYearID, $yearTermID, $formID, $classID, $subjectID, $datefrom, $dateto, $academicYearTerm);

$sql = "SELECT ClassName, TotalAmount, TotalSubject, TotalLoading, TotalLate, TotalNoSubmission FROM TEMP_HW_CLASS_STATS ";

$TableColumns = array("ClassName", "TotalAmount", "TotalSubject", "TotalLoading", "TotalLate", "TotalNoSubmission");
if ($field=="")
{
	$field = 0;
}
if ($order=="")
{
	$field = 1;
}
$sql .= " ORDER BY " . $TableColumns[$field] . " " . (($order==1) ? "ASC" : "DESC");
$data = $lhomework->returnResultSet($sql);

$yearTerm = $Lang['SysMgr']['Homework']['AllYearTerms'];						
$form = $Lang['SysMgr']['Homework']['AllForms'];
$subject = $Lang['SysMgr']['Homework']['AllSubjects'];
$className = $Lang['SysMgr']['FormClassMapping']['AllClass'];

if ($academicYearID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', YearNameEN, YearNameB5) FROM ACADEMIC_YEAR WHERE AcademicYearID = '$academicYearID'";
	$yearName = $lhomework->returnVector($sql,1);
	$year = $yearName[0];
}

if ($yearTermID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', YearTermNameEN, YearTermNameB5) FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '$yearTermID'";
	$yearTermName = $lhomework->returnVector($sql,1);
	$yearTerm = $yearTermName[0];
}

if ($formID != "")
{
	$sql = "SELECT YearName FROM YEAR WHERE YearID = '$formID'";
	$formName = $lhomework->returnVector($sql,1);
	$form = $formName[0];
}

if($classID != "") {
	$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID = '$classID'";
	$clsName = $lhomework->returnVector($sql);
	$className = $clsName[0];
}


if ($subjectID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = '$subjectID'";
	$subjectName = $lhomework->returnVector($sql,1);
	$subject = $subjectName[0];
}

# Select Data from DB
$x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eHomeworktableborder'>";
$x .= "<tr>";
$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Class']."</td>";
$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['HomeworkCount']."</td>";
$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['RelatedSubject']."</td>";
$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['TotalWorkload']." (".$Lang['SysMgr']['Homework']['Hours'].")</td>";
$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['LateSubmitted']."</td>";
$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['NotSubmitted']."</td>";
$x .= "</tr>";
if (sizeof($data)!=0)
{
	for ($i=0; $i<sizeof($data); $i++)
	{
		$RowObj = $data[$i];
		
		$x .= "<tr class='$css'>";
		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$RowObj["ClassName"]."</td>";
		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$RowObj["TotalAmount"]."</td>";
		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$RowObj["TotalSubject"]."</td>";
		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$RowObj["TotalLoading"]."</td>";
		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$RowObj["TotalLate"]."</td>";
		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$RowObj["TotalNoSubmission"]."</td>";
		$x .= "</tr>\n";
	}
}
else
{
	$x .="<tr><td colspan=\"6\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
}
	
$x .= "</table>\n";
?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
    	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['StatsByClass']?></b></td>
	</tr>
</table>        

<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['AcademicYear']?> : </td><td align="left" valign="top"><?=$year?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['YearTerm']?> : </td><td align="left" valign="top"><?=$yearTerm?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Form']?> : </td><td align="left" valign="top"><?=$form?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$i_ClassName?> : </td><td align="left" valign="top"><?=$className?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subject?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['From']?> : </td><td align="left" valign="top"><?=$datefrom?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['To']?> : </td><td align="left" valign="top"><?=$dateto?></td></tr>

</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>