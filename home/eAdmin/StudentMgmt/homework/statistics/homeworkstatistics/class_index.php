<?php
// Modifing by 
###### Change Log [Start] ######
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date	:	2014-01-27 Yuen
#				added this stats
#
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


$CurrentPage = "Statistics_StatsByClass";

# page title
$PAGE_TITLE = $Lang['SysMgr']['Homework']['StatsByClass'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['StatsByClass'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Date Menu
$today = date('Y-m-d');
$temp = strtotime('-0 day',  strtotime($today));
$temp = date('m', $temp);

if($temp < 9)
	$year = date('Y', strtotime('-1 year',  strtotime($today)));
else
	$year = date('Y', strtotime('-0 day',  strtotime($today)));
				
$datefrom = ($from == "") ? "$year-09-01" : $from;
$dateto = ($to == "") ? $today : $to;

# Select sort field
$sortField = 0;

# change page size
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") 
	$page_size = $ck_page_size;

$pageSizeChangeEnabled = true;

# Table initialization
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$li = new libdbtable2007($field, $order, $pageNo);

# Academic Year
$academicYear = $lhomework->GetAllAcademicYear();
$academicYearID = ($academicYearID=="")? Get_Current_Academic_Year_ID():$academicYearID;

$selectedYear = $lhomework->getCurrentYear("name=\"academicYearID\" onChange=\"reloadForm()\"", $academicYear, $academicYearID, "", false);

# Current Year Term
$academicYearTerm = $lhomework->getAllAcademicYearTerm($academicYearID);
$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);

# Form Menu
$forms = $lhomework->getForms($academicYearID);
$selectedForms = $lhomework->getCurrentForms("name=\"formID\" onChange=\"reloadForm()\"", $forms, $formID, $Lang['SysMgr']['Homework']['AllForms']);

# Class Menu
$classes = $lhomework->getAllClassInfo('',$formID);
$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="reloadForm()"', $classID, 0, 0, $i_general_all_classes);

if(($fid!="" && $fid != $formID) || ($ytid!="" && $ytid != $yearTermID)){
	$subjectID="";
}

if($formID!="")
	$subject = $lhomework->getFormSubjectList($formID, $academicYearID, $yearTermID, $classID);
else
	$subject = $lhomework->getFormSubjectList("", $academicYearID, $yearTermID, $classID);
	
$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);


$filterbar = "$selectedYear $selectedYearTerm $selectedForms $selectClass $selectedSubject";

$lhomework->GenerateClassStats($academicYearID, $yearTermID, $formID, $classID, $subjectID, $datefrom, $dateto, $academicYearTerm);



$sql = "SELECT ClassName, TotalAmount, TotalSubject, TotalLoading, TotalLate, TotalNoSubmission FROM TEMP_HW_CLASS_STATS";
//debug($sql);
$li->field_array = array("ClassName", "TotalAmount", "TotalSubject", "TotalLoading", "TotalLate", "TotalNoSubmission");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = 2;
$li->HideIndex = true;


# TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Class'])."</td>\n";
$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HomeworkCount'])."</td>\n";
$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['RelatedSubject'])."</td>\n";
$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['TotalWorkload'])." (".$Lang['SysMgr']['Homework']['Hours'].")</td>\n";
$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['LateSubmitted'])."</td>\n";
$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['NotSubmitted'])."</td>\n";

$allowExport = $lhomework->exportAllowed;

# Print link
$printPreviewLink = "class_print.php?academicYearID=$academicYearID&yearTermID=$yearTermID&formID=$formID&classID=$classID&subjectID=$subjectID&datefrom=$datefrom&dateto=$dateto&order=$order&field=$field";
# Export link
$exportLink = "class_export.php?academicYearID=$academicYearID&yearTermID=$yearTermID&formID=$formID&classID=$classID&subjectID=$subjectID&datefrom=$datefrom&dateto=$dateto&order=$order&field=$field";

# Toolbar: new, import, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );
if($allowExport)
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
	function reloadForm() {
		if (!check_date(document.form1.from,"<?php echo $i_invalid_date; ?>")) return false;
        if (!check_date(document.form1.to,"<?php echo $i_invalid_date; ?>")) return false;
        if(!checkalldate(document.form1.from.value, document.form1.to.value)) return false;
		
		document.form1.s.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
	
	function checkalldate(t1, t2){
        if (compareDate(t2, t1) < 0) {alert ("<?=$Lang['SysMgr']['Homework']['ToDateWrong']?>"); return false;}
        return true;
	}
	
	function goSearch() {
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="30" align="left" valign="bottom">
								 <table>
										<tr>
											<td><?=$Lang['SysMgr']['Homework']["From"]?></td>
											<td align="left" valign="top">
												<INPUT TYPE="text" NAME="from" SIZE=10 maxlength=10 onChange="reloadForm()" value='<?=$datefrom?>'><?=$linterface->GET_CALENDAR("form1", "from", "reloadForm();")?>
											</td>
											<td valign="top"><?=$Lang['SysMgr']['Homework']["To"]?></td>
											<td align="left" valign="top">
												<INPUT TYPE="text" NAME="to" SIZE=10 maxlength=10 onChange="reloadForm()" value='<?=$dateto?>'><?=$linterface->GET_CALENDAR("form1", "to", "reloadForm();")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>			
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$li->display()?>
					</td>
				</tr>
			</table>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
				<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
				<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
				<input type="hidden" name="page_size_change" value=""/>
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
				<input type="hidden" name="s" value="<?=$s?>"/>
				<input type="hidden" name="fid" value="<?=$formID?>"/>
				<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
