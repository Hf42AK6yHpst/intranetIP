<?php
// modifying : 

###### Change Log [Start] ######
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date	:	2010-11-18 YatWoon
#				add "Total WorkLoad" data
#
#	Date	:	2010-09-29 Henry Chow
#				modified exportHomeworkStatistics(), display homework "statistics" depends on $classID
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}


$lexport = new libexporttext();

$ExportArr = array();

$academicYearTerm = $lhomework->getAllAcademicYearTerm($academicYearID);

$lhomework->GenerateClassStats($academicYearID, $yearTermID, $formID, $classID, $subjectID, $datefrom, $dateto, $academicYearTerm);



$sql = "SELECT ClassName, TotalAmount, TotalSubject, TotalLoading, TotalLate, TotalNoSubmission FROM TEMP_HW_CLASS_STATS ";

$TableColumns = array("ClassName", "TotalAmount", "TotalSubject", "TotalLoading", "TotalLate", "TotalNoSubmission");
if ($field=="")
{
	$field = 0;
}
if ($order=="")
{
	$field = 1;
}
$sql .= " ORDER BY " . $TableColumns[$field] . " " . (($order==1) ? "ASC" : "DESC");

$row = $lhomework->returnResultSet($sql);

// Create data array for export
for ($i=0; $i<sizeof($row); $i++)
{
	$ExportArr[$i][] = $row[$i]["ClassName"];
	$ExportArr[$i][] = $row[$i]["TotalAmount"];
	$ExportArr[$i][] = $row[$i]["TotalSubject"];
	$ExportArr[$i][] = $row[$i]["TotalLoading"]." (".$Lang['SysMgr']['Homework']['Hours'].")";
	$ExportArr[$i][] = $row[$i]["TotalLate"];
	$ExportArr[$i][] = $row[$i]["TotalNoSubmission"];
}

if (sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}


$exportColumn = array($Lang['SysMgr']['Homework']['Class'], $Lang['SysMgr']['Homework']['HomeworkCount'], $Lang['SysMgr']['Homework']['RelatedSubject'], $Lang['SysMgr']['Homework']['TotalWorkload'], $Lang['SysMgr']['Homework']['LateSubmitted'], $Lang['SysMgr']['Homework']['NotSubmitted']);	

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();


$filename = "Homework_Statistics_byClass_".$from."_".$to.".csv";

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
