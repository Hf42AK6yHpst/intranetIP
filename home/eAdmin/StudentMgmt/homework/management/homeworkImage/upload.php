<?php
// Editing by 
/*
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lf = new libfilesystem();

//empty the tmp user folder
   $lf->folder_remove_recursive($intranet_root.'/file/homeworkImage/tmp/'.$UserID);
   
# Remarks list
for($i=1; $i<=sizeof($Lang['AccountMgmt']['RemarksList']);$i++)
	$remarks .= $i.'. '.$Lang['AccountMgmt']['RemarksList'][$i-1]."<br>";

$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn","hidden='hidden'");
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='index.php'", "backBtn");

# navigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkImage'], "index.php");
$PAGE_NAVIGATION[] = array($button_upload, "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkImage";

$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkImage']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$PageNavigation ?></td>
							</tr>
						</table>
					</td>
					<td align="right"></td>
				</tr>
				</table>
				<br>
			<form name="form3" action="upload_update.php" method="post" onSubmit="return doImageValidation()" enctype="multipart/form-data">
			<table id="image_table" height="100%" width=600 border="0" cellpadding=2 cellspacing=1 align=center>
				<tr>				
					<td colspan="2">
					<div id="container">
						<div id="filelist"><?=$Lang['General']['Loading']?></div>
						<br />
						<a id="pickfiles" href="javascript:;">[<?=$i_select_file?>]</a> 
						<a id="uploadfiles" href="javascript:;">[<?=$Lang['General']['UploadFiles']?>]</a>
					</div>	
					</td></tr>
				<tr><td colspan="2"><span class=extraInfo><?=$Lang['homework_import']['hw_list_import_remark']?></span></td></tr>
			</table>		
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$submitBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
			<br><br>
		</td>
	</tr>
</table>
<input type="hidden" name="date" id="date" value="<?=$date?>"/>
</form>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.gears.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.browserplus.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.html4.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.html5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/jquery.plupload.queue/jquery.plupload.queue.php"></script>
<link rel=stylesheet href="<?=$PATH_WRT_ROOT?>templates/jquery/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css">
<script>
$(document).ready(function() {
		
	$(function() {
	$("#container").pluploadQueue({

		runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
		flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
		silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
		url : 'upload_tmp.php',
		max_file_size : '100mb',
		chunk_size : '1mb',
		unique_names : false,
		resize : {width : 2400, height : 1800, quality : 100},
		filters : [
			{title : "Image files", extensions : "jpg"}
		],
		init : {
			UploadComplete: function(up, files) {
				//$('#isUploaded').val(1);
				//document.form3.submit();
					$('#submitBtn').show();
				}
			}
		});
	});		
});
</script>
<?		
		
$linterface->LAYOUT_STOP();		
?>