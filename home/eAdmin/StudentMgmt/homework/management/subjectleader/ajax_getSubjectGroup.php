<?php

	$PATH_WRT_ROOT = "../../../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();

	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
	{
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}


	$linterface = new interface_html();
	$lhomework = new libhomework2007();

	$CurrentPageArr['eAdminHomework'] = 1;

	# Get Year and Year Term 
	$currentYearTerm = $lhomework->getCurrentAcademicYearAndYearTerm();
	$yearID = $currentYearTerm[0]['AcademicYearID'];
	$yearTermID = $currentYearTerm[0]['YearTermID'];

	# Get Subject Group
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID);
	$subjectGroupID = ($subjectGroupID=="")? $subjectGroups[0][0]:$subjectGroupID;
	
	//$selectedSubjectGroup = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" id=\"subjectGroupID\" onChange=\"getStudentList()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
	$selectedSubjectGroup = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" id=\"subjectGroupID\" onChange=\"getStudentList()\"", $subjectGroups, $subjectGroupID, "", false);

	echo $selectedSubjectGroup;

	intranet_closedb();
?>