<?php
// Modifing by : henry chow
		
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lhomework = new libhomework2007();
$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Management_SubjectLeader";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['SubjectLeader'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php", 1); 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ViewSubjectLeaderByClass'], "classView.php", 0); 


$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['AssignSubjectLeader'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Select sort field
$sortField = 0;

# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") 
	$page_size = $ck_page_size;

/*$pageSizeChangeEnabled = true;*/

# Table initialization
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$ltable = new libdbtable2007($field, $order, $pageNo);
# if user is eHomework admin, then he can assign all the subject/subject group's leadr
$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;

# Class Menu
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
	$classes = $lhomework->getAllClassInfo();
else
	$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
	
$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="this.form.method=\'get\'; this.form.action=\'\'; document.getElementById(\'studentID\').selectedIndex=0; document.getElementById(\'subjectGroupID[]\').selectedIndex=0;changeSelection()"', $classID, 0, 0, $i_general_all_classes);


$filterbar1 = $selectClass;
$filterbar2 = "<SELECT NAME='studentID' id='studentID'><option>-- ".$i_alert_pleaseselect."--</option></SELCET>";
$filterbar3 = "<SELECT NAME='subjectGroupID[]' id='subjectGroupID[]' multiple></SELCET>";
	
# Start layout
$linterface->LAYOUT_START();

?>

<script language=javascript>

function changeSelection()
{
	$('#spanContent').load(
		'ajax_loadSubjectLeaderSelection.php', 
		{ 
			yearTermID: document.getElementById('yearTermID').value,
			classID: document.getElementById('classID').value,
			studentID: document.getElementById('studentID').value,
			subjectGroupID: document.getElementById('subjectGroupID[]').value
			}, 
		function (data){
		}); 
}

function checkform() {
	if(countOption(document.addSubjectLeader.elements['subjectGroupID[]'])==0) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['SysMgr']['Homework']['SubjectGroup']?>");
		return false;
	} else {
		document.addSubjectLeader.action = "new_update.php";
		document.addSubjectLeader.method = "POST";
	}
}

function SelectAll(obj) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

</script>

<form name="addSubjectLeader" method="post" action="new_update.php" onSubmit="return checkform()">
<table width="100%" order="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="navigation">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>
<div id="spanContent">
<table class="form_table_v30">
	<tr>
		<td class="field_title" width="30%" valign="top"><span class="tabletextrequire">*</span><?=$Lang['SysMgr']['Homework']['Class']?></td>
		<td width="567" align="left" valign="top">
			<?=$filterbar1?>
		</td>
	</tr>
	<tr>
		<td class="field_title" width="30%" valign="top"><span class="tabletextrequire">*</span><?=$i_UserStudentName?></td>
		<td width="567" align="left" valign="top">
			<?=$filterbar2?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title" width="30%" valign="top"><span class="tabletextrequire">*</span><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></td>
		<td width="567" align="left" valign="top">
			<?=$filterbar3?>
		</td>
	</tr>
</table>				
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
			</table>
		</td>
	</tr>
</table>
</div>
<input type="hidden" name="yearTermID" id="yearTermID" value="<?=$yearTermID?>">
</form>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>