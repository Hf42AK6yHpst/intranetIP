<?php
// Modifing by : 

/**************************************
 * Date: 2013-01-22 (Rita) 
 * Details:  add id="addSubjectLeader"  to form
 ***************************************/
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();

$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Management_SubjectLeader";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['SubjectLeader'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php", 0); 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ViewSubjectLeaderByClass'], "classView.php", 1); 


$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['AssignSubjectLeader'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();


# Class Menu
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID))
	$classes = $lhomework->getAllClassInfo();
else
	$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
	
$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="reloadTable(this.value)"', $classID, 0, 0, "-- $i_alert_pleaseselect --");

# Start layout
$linterface->LAYOUT_START();

?>

<script language=javascript>
<!--
function reloadTable(classID) {
	if(classID!="") {
		$('#spanContent').load(
			'ajax_loadClassViewTable.php', 
			{ 
				classID: document.getElementById('classID').value
				}, 
			function (data){
				$('#spanContent').show();
			}); 
	} else {
		$('#spanContent').hide();
	}
}

function Update_Subject_Leader() {

	var PostString = Get_Form_Values(document.getElementById("addSubjectLeader"));
	var PostVar = PostString;
	
	Block_Element("spanContent");
	$.post('ajax_update_subject_leader.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				//alert(data);
				Get_Return_Message(data);
				//document.getElementById('spanDisplay').innerHTML = data;
				reloadTable();
			}
		});

}

-->
</script>

<form name="addSubjectLeader" id="addSubjectLeader" method="post">
<span id="spanDisplay"></span>
<!--
<table width="100%" order="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="navigation">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>
-->
<table class="form_table_v30">
	<tr>
		<td class="field_title" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Class']?></td>
		<td width="567" align="left" valign="top">
			<?=$selectClass?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="spanContent"></div>
		</td>
	</tr>
	
</table>				

</form>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>