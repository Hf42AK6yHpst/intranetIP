<?php
# modifying by : henry chow
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

$currentYearTerm = $lhomework->getCurrentAcademicYearAndYearTerm();
$yearID = $currentYearTerm[0]['AcademicYearID'];
$yearTermID = $currentYearTerm[0]['YearTermID'];

if(isset($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"]))
	$addCond = " OR 1";

if($task=='subjectGroupList')
{
	$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';
	$sql = "SELECT a.ClassCode, 
			IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup,
			IF('$intranet_session_language' = 'en', d.EN_DES, d.CH_DES) AS Subject
			FROM SUBJECT_TERM_CLASS AS a 
			LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN SUBJECT_TERM AS c ON c.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS d ON d.RecordID = c.SubjectID
			INNER JOIN ACADEMIC_YEAR_TERM AS e ON e.YearTermID = c.YearTermID 
			INNER JOIN ACADEMIC_YEAR AS f ON f.AcademicYearID = e.AcademicYearID
			LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=a.SubjectGroupID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
			LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID)
			WHERE (b.UserID = '$UserID' OR yct.UserID='$UserID' $addCond) AND f.AcademicYearID = $yearID AND e.YearTermID = $yearTermID
			GROUP BY a.SubjectGroupID
			ORDER BY Subject";


    $array = $lhomework->returnArray($sql);
	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
	$data .="<tr class=\"tabletop\">
				<td>#</td>
				<td>".$Lang['SysMgr']['Homework']['SubjectGroupCode']."</td>
				<td>".$Lang['SysMgr']['Homework']['Subject']."</td>
				<td>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>
			</tr>";
	
	for($a=0;$a<sizeof($array);$a++)
	{
		$subjectGroupCode = intranet_htmlspecialchars($array[$a]['ClassCode']);
		$subject = intranet_htmlspecialchars($array[$a]['Subject']);
		$subjectGroup = intranet_htmlspecialchars($array[$a]['SubjectGroup']);
		if($subjectGroupCode!='')
		{
			$data .="<tr class=\"tablerow1\">
						<td>".($a+1)."</td>
						<td>".$subjectGroupCode."</td>
						<td>".$subject."</td>
						<td>".$subjectGroup."</td>
					</tr>";
		}
	}
	
	if(sizeof($array)==0){
		$data .="<tr class=\"tablerow1\" >
					<td colspan=\"4\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>
				</tr>"; 
	}

	$data .="</table>";	
}

else if($task=='subjectList')
{	
	$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';
	$sql = "SELECT c.CODEID, 
			IF('$intranet_session_language' = 'en', c.EN_DES, c.CH_DES) AS Subject 
			FROM SUBJECT_TERM AS a
			INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = a.SubjectID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = a.YearTermID 
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			WHERE b.UserID = $UserID AND e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID AND c.RecordStatus = 1 AND CMP_CODEID is NULL 
			GROUP BY c.CODEID ORDER BY c.CODEID";

	$array = $lhomework->returnArray($sql);
	
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
	$data .="<tr class=\"tabletop\">
				<td>#</td>
				<td>".$Lang['SysMgr']['Homework']['SubjectCode']."</td>
				<td>".$Lang['SysMgr']['Homework']['Subject']."</td>
			</tr>";
	
	for($a=0;$a<sizeof($array);$a++)
	{
		$SubjectCode = intranet_htmlspecialchars($array[$a]['CODEID']);
		$SubjectName = intranet_htmlspecialchars($array[$a]['Subject']);
		
		if($SubjectCode!='')
		{
			$data .="<tr class=\"tablerow1\">
						<td>".($a+1)."</td>
						<td>".$SubjectCode."</td>
						<td>".$SubjectName."</td>
					</tr>";
		}
	}
	
	if(sizeof($array)==0){
		$data .="<tr class=\"tablerow1\" >
					<td colspan=\"3\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>
				</tr>"; 
	}
	
	$data .="</table>";	
}

$output .= '<tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
							<div id="list" style="overflow: auto; z-index:1; height: 150px;">
								'.$data.'
							</div>
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
