<?php
# modifying by : henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

# Get Year and Year Term 
$currentYearTerm = $lhomework->getCurrentAcademicYearAndYearTerm();
$yearID = $currentYearTerm[0]['AcademicYearID'];
$yearTermID = $currentYearTerm[0]['YearTermID'];

# Get Subject
$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID);
$subjectID = ($subjectID=="-1" || $subjectID=="")? $subject[0][0]:$subjectID;

$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"getSubjectGroup(this.value)\"", $subject, $subjectID, "", false);
//$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"getSubjectGroup(this.value)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true, -1);

# Get Subject Group
$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID);
$subjectGroupID = ($subjectGroupID=="" || $subjectGroupID=="-1")? $subjectGroups[0][0]:$subjectGroupID;

//$selectedSubjectGroup = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" id=\"subjectGroupID\" onChange=\"getStudentList()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
$selectedSubjectGroup = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" id=\"subjectGroupID\" onChange=\"getStudentList()\"", $subjectGroups, $subjectGroupID, "", false);

# Output Result
$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="19">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
								<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
								<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
						  </tr>
						</table>
					</td>
				</tr>
		
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
								<td align="left" bgcolor="#FFFFF7">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<table>
													<tr>
														<td height="30" align="left" valign="top">
															'.$selectedSubject.'
														</td>&nbsp;&nbsp;
														
														<td height="30" align="left" valign="top">
															<div id="selectSubjectGroup">
																'.$selectedSubjectGroup.'
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td align="left" valign="top" colspan="2">
												<div id="list" style="overflow: auto; z-index:1; height: 100px;">
												</div>
											</td>
										</tr>
										
										<tr>
											<td>
												<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
													<tr>
														<td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
							</tr>
							
							<tr>
								<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
								<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
								<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
							 </tr>
						</table>
					</td>
				 </tr>
			</table>';

	echo $output;
	intranet_closedb();
?>