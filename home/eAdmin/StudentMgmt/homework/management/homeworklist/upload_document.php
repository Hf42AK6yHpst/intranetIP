<?php
// Editing by 
/*
 * 2017-06-27 (Carlos): $sys_custom['LivingHomeopathy'] created.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$sys_custom['LivingHomeopathy'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) && !$lhomework->isViewerGroupMember($UserID)))
{
	intranet_closedb();
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		//header("location: ../../settings/index.php");	
		exit;
	}
	//include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	//$laccessright = new libaccessright();
	//$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lf = new libfilesystem();

$is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

function getNextFreeFolder($base_dir,$return_full_path=false)
{	
	$LIMIT = 30000;
	//$LIMIT = 5; // test value
	$absolute_target_folder = $base_dir;
	
	$subfolder_number = 0;
	$file_count = 0;
	$folder_to_find = $absolute_target_folder."/".$subfolder_number;
	$rel_folder_to_find = $subfolder_number;
	
	if(!is_dir($folder_to_find)){
		mkdir($folder_to_find,0777);
	}
	$number_of_attempt = 0;
	do
	{
		$number_of_attempt+=1;
		$cmd = "find $folder_to_find -maxdepth 1 | wc -l";
		$file_count = intval(shell_exec($cmd));
		if($file_count >= $LIMIT)
		{
			$subfolder_number += 1;
			$folder_to_find = $absolute_target_folder."/".$subfolder_number;
			$rel_folder_to_find = $subfolder_number;
			if(!is_dir($folder_to_find)){
				mkdir($folder_to_find,0777);
			}
		}else{
			break;
		}
		if($number_of_attempt > $LIMIT) break; // avoid forever loop
	}while($file_count >= $LIMIT);
	
	if($return_full_path){
		return $folder_to_find;
	}else{
		return $rel_folder_to_find;
	}
}

function buildReturnScript($studentId, $success, $returnHtml)
{
	$x = '<script type="text/javascript" language="javascript">'."\n";
	$x.= 'if(window.top.teacherDocumentUploaded){window.top.teacherDocumentUploaded('.$studentId.','.$success.',\''.$returnHtml.'\');}'."\n";
	$x.= '</script>'."\n";
	
	return $x;
}

$document_size_limit = isset($sys_custom['eHomework']['HomeworkDocumentSizeLimit']) && is_numeric($sys_custom['eHomework']['HomeworkDocumentSizeLimit'])? $sys_custom['eHomework']['HomeworkDocumentSizeLimit'] : 100; // default 100MB
$HomeworkID = IntegerSafe($_POST['hid']);
$StudentID = IntegerSafe($_POST['StudentID']);
$file_field_name = 'TeacherDocument_'.$StudentID;

if(count($_FILES)==0 || $_FILES[$file_field_name]['error'] > 0 || $_FILES[$file_field_name]['size']> $document_size_limit*1024*1024){
	intranet_closedb();
	echo buildReturnScript($StudentID,0,'error');
	exit;
}

$handin_records = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$HomeworkID,'StudentID'=>$StudentID));

if(count($handin_records) == 0){
	intranet_closedb();
	echo buildReturnScript($StudentID,0,'record not found');
	exit;
}

$handin_record = $handin_records[0];

$target_dir = $file_path.'/file/homework_document';
if(!file_exists($target_dir) || !is_dir($target_dir)){
	$lf->folder_new($target_dir);
}else{
	chmod($target_dir,0777);
}

$subfolder = getNextFreeFolder($target_dir);
$target_dir .= '/'.$subfolder;

$uniq_folder = md5(uniqid().time().$_SESSION['UserID']);
$subfolder .= '/'.$uniq_folder;
$target_dir .= '/'.$uniq_folder;
if(!file_exists($target_dir) || !is_dir($target_dir)){
	$lf->folder_new($target_dir);
}else{
	chmod($target_dir,0777);
}

$file_name = $_FILES[$file_field_name]["name"];
if($is_magic_quotes_active){
	$file_name = stripslashes($file_name);
}

$base_name = $lf->get_file_basename($file_name);
$save_file_path = $subfolder.'/'.$base_name;
$target_path = $target_dir.'/'.$base_name;

$copy_success = move_uploaded_file($_FILES[$file_field_name]["tmp_name"], $target_path);

if($copy_success){
	$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET TeacherDocument='".$lhomework->Get_Safe_Sql_Query($save_file_path)."',TeacherDocumentUploadTime=NOW(),TeacherDocumentUploadedBy='".$_SESSION['UserID']."' WHERE RecordID='".$handin_record['RecordID']."'";
	$success = $lhomework->db_db_query($sql);
	
	if($success)
	{
		$handin_documents = $lhomework->getHomeworkHandinListRecords(array('RecordID'=>$handin_record['RecordID'],'GetTeacherName'=>1));
		
		$teacher_document_name = substr($handin_documents[0]['TeacherDocument'],strrpos($handin_documents[0]['TeacherDocument'],'/')+1);
		$teacher_document_url = getEncryptedText($file_path.'/file/homework_document/'.$handin_documents[0]['TeacherDocument']);
		$teacher_document_display .= '<table><tr>';
		$teacher_document_display .= '<td><a href="/home/download_attachment.php?target_e='.$teacher_document_url.'">'.intranet_htmlspecialchars($teacher_document_name).'</a></td>';
		$teacher_document_display .= '<td><span class="table_row_tool"><a href="javascript:void(0);" class="delete_dim" onclick="deleteTeacherDocument('.$handin_documents[0]['RecordID'].','.$handin_documents[0]['StudentID'].');" title="'.$Lang['Btn']['Delete'].'"></a></span></td>';
		$teacher_document_display .= '</tr></table>';
		$teacher_document_display .= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_documents[0]['TeacherDocumentUploadTime'],str_replace('<!--NAME-->',$handin_documents[0]['TeacherDocumentUploaderName'],$Lang['eHomework']['TeacherDocumentSubmittedRemark'])).')</div>';

		echo buildReturnScript($StudentID,1,$teacher_document_display);
	}else{
		
		echo buildReturnScript($StudentID,0,'');
	}
}else{
	$lf->folder_remove($target_dir);
	echo buildReturnScript($StudentID,0,'');
}

intranet_closedb();
?>