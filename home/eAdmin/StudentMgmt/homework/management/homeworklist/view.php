<?php

// Modifing by : 

/********************** Change Log ***********************
#
#	Date    :	2019-05-08 (Bill)
#               prevent SQL Injection
#
#	Date	:	2017-06-28 (Carlos)
#				$sys_custom['LivingHomeopathy'] - redirect to eService for student and parent users.
#
#	Date	:	2015-03-11 (Bill)
#				display subject teacher name and change field title
#
#	Date	:	2010-10-28 (YatWoon)
#				update insertHomeworkListRecord(), add CreatedBy field
#
#************************************************************************
*/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// 2015-03-11 added
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
// 2015-03-11 added
$scm = new subject_class_mapping();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($sys_custom['LivingHomeopathy'] && ($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"]))){
	intranet_closedb();
	header("Location:/home/eService/homework/management/homeworklist/view.php?hid=".$hid);
	exit;
}

$linterface = new interface_html("popup.html");

$CurrentPageArr['eAdminHomework'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Homework']['HomeworkList'];

$hid = IntegerSafe($hid);
$record = $lhomework->returnRecord($hid);

list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath, $handinRequired, $collectRequired, $PostUserID, $CreatedBy)=$record; 
$lu = new libuser($PostUserID);
$PosterName2 = $lu->UserName();

$lu2 = new libuser($CreatedBy);
$CreatedByName = $lu2->UserName();

if($lhomework->useHomeworkType) {
	$typeInfo = $lhomework->getHomeworkType($TypeID);
	$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
}

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

// 2015-03-11 - Get Subject Teacher Name
$subjectTeacherName = "";
$subjectTeacherList = $scm->Get_Subject_Group_Teacher_Info((array)$subjectGroupID);
if(count($subjectTeacherList) > 0){
	$delim = '';
	foreach($subjectTeacherList as $subjectTeacher){
		$lu3 = new libuser($subjectTeacher['UserID']);
		$teacherName = $lu3->UserName();
		$subjectTeacherName .= $teacherName? $delim.$teacherName : "--";
		$delim = ', ';
		unset($lu3);
	}
}

$description = nl2br($lhomework->convertAllLinks($description,40));

if($AttachmentPath != NULL) {
	$displayAttachment = $lhomework->displayAttachment($hid);
}

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>
function click_print()
{
	with(document.form1)
    {
        submit();
	}
}
</SCRIPT>

<br /> 
<form name="form1" action="print_preview.php" method="post" target = "_blank">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<!-- subject-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?></td>
									<td width="567" align="left" valign="top">
										<?=$subjectName?>
									</td>
								</tr>
								
								<!-- subject Group-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></td>
									<td width="567" align="left" valign="top">
										<?=$subjectGroupName?>
									</td>
								</tr>
								
								<? if($lhomework->useHomeworkType) { ?>
    								<!-- Homework Type-->
    								<tr>
    									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Homework_HomeworkType?></td>
    									<td width="567" align="left" valign="top">
    										<?=$typeName?>
    									</td>
    								</tr>
								<? } ?>
								
								<!-- topic -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Topic']?></td>
									<td width="567" align="left" valign="top">
									  <?=$title?>
									</td>
								</tr>
								
								<!-- description -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?></td>
									<td width="567" align="left" valign="top">
										<?php
											if($description!="") {
												echo $description;
											}
											else {
												echo "--";
											}
										?>
									</td>
								</tr>
								
								<!-- attachment -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Attachment']?></td>
									<td width="567" align="left" valign="top">
										<?php
											if($AttachmentPath!=NULL) {
												echo $displayAttachment;
											}
											else {
												echo $Lang['SysMgr']['Homework']['NoAttachment'];
											}
										?>
									</td>
								</tr>
								<!-- end of attachment -->

								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Workload']?></td>
									<td width="567" align="left" valign="top">
										<?=($loading/2)." ".$Lang['SysMgr']['Homework']['Hours']?>
									</td>
								</tr>
								  
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['StartDate']?></td>
									<td width="567" align="left" valign="top">
										<?=$start?>
									</td>
								</tr>

								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']["DueDate"]?></td>
									<td width="567" align="left" valign="top">
										<?=$due?>
									</td>
								</tr>
								
								<tr>	                      	
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['HandinRequired']?></td>
									<td width="567" align="left" valign="top">
										<?=($handinRequired=="1"? $i_general_yes: $i_general_no)?>
									</td>						
								</tr>
								
								<? if($lhomework->useHomeworkCollect){?>
    								<tr>
    									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Homework_Collected_By_Class_Teacher?></td>
    									<td width="567" align="left" valign="top">
    										<?=($collectRequired=="1"? $i_general_yes:$i_general_no)?>
    									</td>
    								</tr>
								<? } ?>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['General']['LastModifiedBy']?></td>
									<td width="567" align="left" valign="top">
										<?=($PosterName2==""? "--":$PosterName2)?>
									</td>
								</tr>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['CreatedBy']?></td>
									<td width="567" align="left" valign="top">
										<?=($CreatedBy ? $CreatedByName : $PosterName2)?>
									</td>
								</tr>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroupTeacher']?></td>
									<td width="567" align="left" valign="top">
										<?=($subjectTeacherName ? $subjectTeacherName : "--")?>
									</td>
								</tr>
													
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="hid" value="<?=$hid?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>