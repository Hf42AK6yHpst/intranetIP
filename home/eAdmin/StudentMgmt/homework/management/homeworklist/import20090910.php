<?php
// Modifying by: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$lhomework = new libhomework2007();
$linterface = new interface_html();

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_HomeworkList";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkList'], ""); 

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# page netvigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['ImportRecord'], "");

$layer_html = "<div id=\"ref_list\" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>";

# sample file
$sample_file = "sample_import_homework_unicode.csv";

# instruction
$format_str = $layer_html;
$format_str .= "<p><span id=\"GM__to\"><strong>".$i_Homework_import_points."</strong></span></p>";
$format_str .= "<ol>
				<li>$i_Homework_import_instruction1</li>
				<li>$i_Homework_import_instruction2</li>
				<li>$i_Homework_import_instruction3</li>
				</ol>";
   
$format_str .= "<p>$i_Homework_import_reference : ";
$format_str .= "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">$i_Homework_import_sample</a>,
				<a class=\"tablelink\" href=javascript:show_ref_list('subjectGroupList','cc_click')>".$Lang['SysMgr']['Homework']['SubjectGroupCode']."</a>,<span id=\"cc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('subjectList','sc_click')>".$Lang['SysMgr']['Homework']['SubjectCode']."</a><span id=\"sc_click\">&nbsp;</span></p>";

$linterface->LAYOUT_START();
?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_gm.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}
</script>
<br />

<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td class="navigation">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>

<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr><td class="tabletext" width="20%" align="left"><?=$Lang['SysMgr']['Homework']['ImportSourceFile']?>: </td><td class="tabletext" width="85%"><input class="file" type="file" name="csvfile"></td></tr>
						</table>
					</td>	
				</tr>
			
				<tr>
					<td class="tabletext"><?=$format_str?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID'") ?>
		</td>
	</tr>
</table>
<input type="hidden" id="task" name="task"/>
<input type="hidden" name="yearID" value="<?=$yearID?>"/>
<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
<input type="hidden" name="subjectID" value="<?=$subjectID?>"/>
<input type="hidden" name="subjectGroupID" value="<?=$subjectGroupID?>"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
