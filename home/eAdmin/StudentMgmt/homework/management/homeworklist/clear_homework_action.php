<?php
// modifying by : 

########## Change Log [Start] ####################
#
#	Date	:	2010-09-20 Henry Chow
#				insert into Deletion Log (table "MODULE_RECORD_DELETE_LOG") on every deletion of Homework
#
########### Change Log [End] #####################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $sys_custom['eHomework']['HideClearHomeworkRecords'])
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($delHomeworkRec=='period' && $from!='' && $to!='')
	$text = " (Period $from to $to)";

$str = "Action : Clear Homework $text";

# insert into MODULE_RECORD_DELETE_LOG (Deletion Log)
$sql = "INSERT INTO MODULE_RECORD_DELETE_LOG (Module, Section, RecordDetail, DelTableName, DelRecordID, LogDate, LogBy) VALUES ('".$lhomework->Module."', 'Homework', '$str', 'INTRANET_HOMEWORK', '0', NOW(), '$UserID')";
$ldb->db_db_query($sql) or die(mysql_error());

$sql = "DELETE FROM INTRANET_HOMEWORK";
if($delHomeworkRec=='period' && $from!='' && $to!='')
	$sql .=" WHERE startdate between '$from' and '$to'";

$ldb->db_db_query($sql);

header ("Location: clear_homework.php?msg=homework_clear");

intranet_closedb();
?>