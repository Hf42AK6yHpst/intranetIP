<?php
# using: 

########## Change Log
#
#	Date:	2019-05-08 (Bill)
#           prevent SQL Injection
#
#	Date:	2017-09-15	Bill	[2017-0915-1047-59236]
#			Set default subject group in index page only if 1 subject group is selected
#
#	Date:	2010-11-19	YatWoon
#			Subject Group select option changes to multiple selection [wish list]
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && ($_SESSION['isTeaching'] && $lhomework->ViewOnly)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lu = new libuser($UserID);
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lu->isTeacherStaff() || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
{
    if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->nonteachingAllowed || $lu->teaching == 1 || $lhomework->subjectLeaderAllowed)
    {
        $haveAccess = true;
    }
}

if ($haveAccess)
{
    ### Handle SQL Injection + XSS [START]
    $typeID = IntegerSafe($typeID);
    $subjectID = IntegerSafe($subjectID);
    $subjectGroupID = IntegerSafe($subjectGroupID);
    $yearID = IntegerSafe($yearID);
    $yearTermID = IntegerSafe($yearTermID);
    
    $loading = (float)$loading;
    $handin_required = IntegerSafe($handin_required);
    $collect_required = IntegerSafe($collect_required);
    ### Handle SQL Injection + XSS [END]
    
	# Check date
	# Start date >= curdate() && Start date <= due date
	$start_stamp = strtotime($startdate);
	$due_stamp = strtotime($duedate);
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
	//$cond = ($lhomework->pastInputAllowed) ? compareDate($due_stamp,$start_stamp) < 0 : (compareDate($start_stamp,$today) < 0 || compareDate($due_stamp,$start_stamp)<0);
	
	$success = 1;
	if(!intranet_validateDate($startdate) || !intranet_validateDate($duedate))
	{
	    $success = 0;
	}
	else if ($lhomework->pastInputAllowed)
	{
		if (compareDate($due_stamp, $start_stamp) < 0)
		{
			$success = 0;  
		}
	}
	else
	{
		if (compareDate($start_stamp, $today) < 0 || compareDate($due_stamp, $start_stamp) < 0)
		{
            $success = 0;
		}
	}
	
	if ($success == 1)
	{
		/*
		$sql = "SELECT SubjectID from SUBJECT_TERM WHERE SubjectGroupID = '".$subjectGroupID[0]."'";
		$result = $lhomework->returnArray($sql);
		$subjectID = $result[0]['SubjectID'];
		*/
		
		$title = intranet_htmlspecialchars($title);
		$description = intranet_htmlspecialchars(trim($description));
		
		$nameField = getNameFieldByLang("");
		$sql = "SELECT $nameField FROM INTRANET_USER WHERE UserID = '$UserID'";
		$result = $lhomework->returnVector($sql);
		$postername = $result[0];
		
		$today_str = date("Y-m-d",$today);
		$handinRequired = $handin_required == ""? 0 : 1;
		$collectRequired = $collect_required  == ""? 0 : 1;
		$AcademicYearID = $yearID;
		$YearTermID = $yearTermID;
		
		$homeworkType = 1;
		foreach($subjectGroupID as $k=>$d)
		{
			$fieldnames = "ClassGroupID, SubjectID, PosterUserID, PosterName, StartDate, DueDate, Loading, Title, Description, RecordType, LastModified, TypeID, HandinRequired, CollectRequired, AcademicYearID, YearTermID, CreatedBy";            
			$fieldvalues = "'$d', '$subjectID', '$UserID', '$postername', '$startdate', '$duedate', '$loading', '$title', '$description', '$homeworkType', now(), '$typeID', '$handinRequired', '$collectRequired', '$AcademicYearID', '$YearTermID', '$UserID'";            
			$sql = "INSERT INTO INTRANET_HOMEWORK ($fieldnames) VALUES ($fieldvalues)";
			$success = $lhomework->db_db_query($sql);
			$HomeworkID = $lhomework->db_insert_id();
            
			# Upload Files
			$attachment = session_id()."_h";
			$homkworkdir = "$file_path/file/homework/";
			$path = "$file_path/file/homework/$attachment$HomeworkID/";
			$lf = new libfilesystem();
            
			$hasAttachment = false;
			$attachment_size = $attachment_size==""?0:$attachment_size;	
			for ($i=1; $i<=$attachment_size; $i++)
			{
				$key = "filea$i";
				$loc = ${"filea$i"};
	
				//Getting file name
				$file1 = ${"filea$i"."_name"};
				// $file_hidden = ${"filea$i"."_hidden"};
				$file_hidden = ${"hidden_userfile_name$i"};
				$file = ($file_hidden!="") ? $file_hidden : $file1;
				$des = "$path/".stripslashes($file);
				
				if ($loc == "none" || trim($loc) == "")
				{
				    // do nothing
				} 
				else
				{
					if (strpos($file,"."==0))
					{
					    // do nothing
					} 
					else
					{
						if (!$hasAttachment)
						{
							 # add by Kelvin Ho 2008-11-06  make sure directory 'homework' exists
							 $lf->folder_new ($homkworkdir);
							 $lf->folder_new ($path);
							 $hasAttachment = true;
						}
						$lf->lfs_copy($loc, $des);
					}
				}
			}
            
			# Update attachment in DB
			if ($hasAttachment)
			{
				$sql = "UPDATE INTRANET_HOMEWORK SET AttachmentPath = '$attachment' WHERE HomeworkID = '$HomeworkID'";
				$lhomework->db_db_query($sql);
			}
			
			# End of upload files
		}
	}
    
	if ($success == 0)
	{
		$msg = "add_failed";
	}
	else
	{
		$msg = "add";
	}
	
	// Set preset Subject Group
	$presetSubjectGroup = "";
	if(count((array)$subjectGroupID) == 1) {
		$presetSubjectGroup = $subjectGroupID[0];
	}
	
	//header ("Location: index.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID[0]&msg=$msg");
	header ("Location: index.php?subjectID=$subjectID&subjectGroupID=$presetSubjectGroup&msg=$msg");
}
else
{
    header ("Location: index.php");
}

intranet_closedb();
?>