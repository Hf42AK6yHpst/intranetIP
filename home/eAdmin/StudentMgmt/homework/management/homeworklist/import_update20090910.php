<?
// Modifying by:
session_start();
$tempData = $_SESSION["tempData"];

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$limport = new libimporttext();
$lo = new libfilesystem();
$lhomework = new libhomework2007();
$lu = new libuser();

$linterface = new interface_html();

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_HomeworkList";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

# Left menu
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "");

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# page netvigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['ImportRecord'], "");

$allSubjectCode = array();
$allSubjectName = array();
$allSubjectGroupCode = array();
$allSubjectGroupName = array();
			
$subjectCodeName = $lhomework->getAllSubjectCode($yearID, $yearTermID);
$subjectGroupCodeName = $lhomework->getAllSubjectGroupCode($yearID, $yearTermID);

# Put All Subject Code and Name into Array
for($i=0; $i<sizeof($subjectCodeName);$i++){
	list($code, $name)= $subjectCodeName[$i];
	array_push($allSubjectCode, $code);
	array_push($allSubjectName, $name);
}

# Put All Subject Group Code and Name into Array
for($i=0; $i<sizeof($subjectGroupCodeName);$i++){
	list($code, $name)= $subjectGroupCodeName[$i];
	array_push($allSubjectGroupCode, $code);
	array_push($allSubjectGroupName, $name);
}


### List out the import result
$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"4%\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"9%\">".$Lang['SysMgr']['Homework']['Subject']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"12%\">".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"18%\">".$Lang['SysMgr']['Homework']['Topic']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"12%\">".$Lang['SysMgr']['Homework']['StartDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"12%\">".$Lang['SysMgr']['Homework']['DueDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10%\">".$Lang['SysMgr']['Homework']['Workload']." ".$Lang['SysMgr']['Homework']['Hours']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"20%\">".$Lang['SysMgr']['Homework']['Description']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10%\">".$Lang['SysMgr']['Homework']['HandinRequired']."</td>";
$x .= "</tr>";


# List out the record rows
for ($a=0; $a<sizeof($tempData);$a++){

	$sname = $allSubjectName[array_search($tempData[$a][0], $allSubjectCode)];
	$sgname = $allSubjectGroupName[array_search($tempData[$a][1], $allSubjectGroupCode)];
	
	$description = ($tempData[$a][7]=="")? "--":$tempData[$a][7];
	
	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">".$sname."</td>";
	$x .= "<td class=\"tabletext\">".$sgname."</td>";
	$x .= "<td class=\"tabletext\">".$tempData[$a][6]."</td>";
	$x .= "<td class=\"tabletext\">".$tempData[$a][2]."</td>";
	$x .= "<td class=\"tabletext\">".$tempData[$a][3]."</td>";
	$x .= "<td class=\"tabletext\">".($tempData[$a][4]/2)."</td>";
	$x .= "<td class=\"tabletext\">".$description."</td>";
	$handin = $tempData[$a][8]==1 ? "Yes":"No";
	$x .= "<td class=\"tabletext\">".$handin."</td>";
	$x .= "</tr>";
}
	
	# Insert records into INTRANET_HOMEWORK
	$homewostListRecordID = $lhomework->insertHomeworkListRecord($tempData);
	
	if($homewostListRecordID!=0)
	{
		$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'")."&nbsp;".$linterface->GET_ACTION_BTN($Lang['SysMgr']['Homework']['ImportOtherRecords'], "button", "window.location='import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'");
	}
	else
	{
		$xmsg="import_failed";
		$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'");	
	}
	
$x .= "</table>";


$linterface->LAYOUT_START();
?>
<br />

<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td class="navigation">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td align="center">
			<?= $x ?>
		</td>
	</tr>
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>