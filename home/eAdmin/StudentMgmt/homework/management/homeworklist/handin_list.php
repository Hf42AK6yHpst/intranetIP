<?php
// modifying : 

###################################
#
#   Date    :   2020-10-15 (Bill)   [2020-0910-1206-37073]
#               not auto create hand-in list records    ($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records'])
#
#   Date    :   2020-09-23 (Bill)   [2020-0915-1021-34073]
#               added $excludeCount > fix cannot update submission status if all students are 'No Need Submit'
#
#	Date    :	2019-05-07 (Bill)   [2019-0507-0951-59254]
#               prevent SQL Injection
#
#   Date    :   2018-05-14 (Anna)
#               added DeletedUserLegend after table #138644
#
#	Date	:	2017-10-30 (Bill)	[2017-1027-1111-55236]
#				fixed: Subject Teacher cannot update Hand-in List status if Not allow class teacher to manage homework of his/her class : true
#
#	Date	:	2017-06-30 (Carlos) $sys_custom['LivingHomeopathy'] display student handed-in homework document, allow teacher to upload marked homework document and rate the homework.
#
#	Date	:	2016-08-10 (Bill)	[2016-0519-1005-58207]
#	Detail	:	Update logic that check if current user can update Hand-in List status according to user role and system settings
#				- View Group Member can view status only
#
###################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$MODULE_OBJ['title'] = $Lang['SysMgr']['Homework']['HomeworkHandinlist'];

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$CurrentPageArr['eAdminHomework'] = 1;

### Handle SQL Injection + XSS [START]
$hid = IntegerSafe($hid);
$handin_status = IntegerSafe($handin_status);
$handin_user_id = IntegerSafe($handin_user_id);
### Handle SQL Injection + XSS [END]

$record = $lhomework->returnRecord($hid);
list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath, $handinRequired, $collectRequired) = $record; 
$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

$handinCount = 0;
$totalCount = 0;
$excludeCount = 0;      // [2020-0915-1021-34073]
$singleTable = false;

if($lhomework->useHomeworkType) {
	$typeInfo = $lhomework->getHomeworkType($TypeID);
	$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
}

// Current User Role
$isNonTeachingStaff = $_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'];
$isNonTeachingStaffAllManage = $isNonTeachingStaff && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"];
$isViewGroupMemeber = $lhomework->isViewerGroupMember($UserID);
$isSubjectLeader = $lhomework->isSubjectLeader($UserID);

/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus(TRUE);
$notShowStatus = array("Supplementary", "NotSubmittedRecord", "DueDateNotSubmitted", "SupplementarySubmitted", "SuppNotSubmitted", "DueDateAbsence", "ABSHomeworkReturn");
/******************************/

# Check whether User can submit the form or not
$allowToSubmit = false;
if($lhomework->ClassTeacherCanViewHomeworkOnly)
{
	/*
	$teachingSg = $lhomework->getTeachingSubjectList($UserID, Get_Current_Academic_Year_ID(), GetCurrentSemesterID());

	$NoOfTeachingGroups = count($teachingSg);
	for($a=0; $a<$NoOfTeachingGroups; $a++) {
		list($sbj_id, $sbj_name) = $teachingSg[$a];

		if($sbj_id==$subjectID) $allowToSubmit = true;	
	}
	*/
	
	// Admin or Non-teaching Staff or Subject Leader can always submit
	// if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $isNonTeachingStaffAllManage || $isSubjectLeader){
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
		$allowToSubmit = true;
	}
	// [2017-1027-1111-55236] Check if Teacher teaching this subject group
	else {
 		//$relatedGroups = $lhomework->getSubjectGroupIDByTeacherID($UserID, Get_Current_Academic_Year_ID(), GetCurrentSemesterID());
 		//$relatedGroups = Get_Array_By_Key((array)$relatedGroups, "SubjectGroupID");
 		//$allowToSubmit = in_array($subjectGroupID, (array)$relatedGroups);
		$IsHomeworkSubjectGroupTeacher = $lhomework->IsTeacherTeachingTargetSubjectGroup($UserID, $subjectGroupID);
		$allowToSubmit = $IsHomeworkSubjectGroupTeacher? true : false;
	}
}
else if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
{
	$allowToSubmit = true;
	
	// [2016-0519-1005-58207] Viewer Group Member Checking
	$needToCheckSubjectGroup = !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$isNonTeachingStaffAllManage && !$isSubjectLeader && $isViewGroupMemeber;
	if($needToCheckSubjectGroup)
	{
		// Check if User teaching this class or subject group
		$relatedGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), "", 1);
		$relatedGroups = Get_Array_By_Key((array)$relatedGroups, "SubjectGroupID");
		if(!in_array($subjectGroupID, (array)$relatedGroups)) {
			$allowToSubmit = false;
		}
	}
}
if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'])
{
    $allowToSubmit = false;
}

$update = $update==""? 0 : $update;
if(sizeof($handin_status) > 0 && sizeof($handin_user_id) > 0 && $update == 1)
{
	$handin = false;
	$notHandin = false;
	$late = false;
	$redo = false;
	$noNeedHandin = false;

    // [2020-0910-1206-37073] Create hand-in list records if not exist
	if($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records'])
	{
        $data = $lhomework->handinList($hid);

        /*
        if($sys_custom['LivingHomeopathy'])
        {
            $allowToSubmit = false;
        }
        */

		$insertSql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,MainStatusRecordDate,DateInput,DateModified) VALUES";

		$values = "";
		if (sizeof($data) != 0)
		{
			for ($i=0; $i<sizeof($data); $i++)
			{
				$rec = $data[$i];
                $uid = $rec['UserID'];
                //$handinStatus = $rec['RecordStatus'];
                $handinID = $rec['RecordID'];
                //$SuppRecordStatus = $rec['SuppRecordStatus'];
                $studentStatus = $rec['StudentStatus'];

                /*
				list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title,$due,$SuppRecordStatus,$studentStatus,$LeaveStudent) = $data[$i];

				$handinStatus = $handinStatus == "" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;
                if (in_array($SuppRecordStatus, array(7,-11)) && !$allowToSubmit)
                {
                    $handinStatus = $SuppRecordStatus;
                }
                */

                # Set values of records in INTRANET_HOMEWORK_HANDIN_LIST
                $RecordStatus = $sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1;
				if($handinID == "" && $studentStatus != '3') {
					$y = "('$hid','$uid','$RecordStatus',NOW(),NOW(),NOW()),";
					$values .= $y;
				}
			}
		    
			# Insert if records not exists
			if($values != "") {
				$values = substr($values,0,strrpos($values,","));
				$insertSql .= $values;
				$lhomework->db_db_query($insertSql);
			}
		}
	}
	
	$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='1', MainStatusRecordDate=NOW() WHERE HomeworkID='$hid' AND StudentID IN ( ";
	if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) {
		$sql2 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='-1', MainStatusRecordDate=NOW(), SuppRecordStatus='-20', SuppRecordDate=NOW() WHERE RecordStatus != '-1' AND HomeworkID='$hid' AND StudentID IN ( ";
	}
	else {
		$sql2 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='-1', MainStatusRecordDate=NOW() WHERE HomeworkID='$hid' AND StudentID IN ( ";
	}
	$sql3 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='2', MainStatusRecordDate=NOW() WHERE HomeworkID='$hid' AND StudentID IN ( ";
	//$sql4 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='3', MainStatusRecordDate=NOW() WHERE HomeworkID=$hid AND StudentID IN( ";
	$sql5 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='4', MainStatusRecordDate=NOW() WHERE HomeworkID='$hid' AND StudentID IN ( ";
	$sql6 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='5', MainStatusRecordDate=NOW() WHERE HomeworkID='$hid' AND StudentID IN ( ";
	if($sys_custom['eHomework_Status_Supplementary']) {
		$sql7 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='6', MainStatusRecordDate=NOW() WHERE HomeworkID='$hid' AND StudentID IN ( ";
	}
	
	for($i=0;$i<sizeof($handin_status);$i++)
	{
		$value = $handin_status[$i];
		if($value==1) {
			$handin = true;
			$sql .= "'".$handin_user_id[$i]."',";
		}
		else if($value==-1) {
			$notHandin = true;
			$sql2 .= "'".$handin_user_id[$i]."',";
		}
		else if($value==2) {
			$late = true;
			$sql3 .= "'".$handin_user_id[$i]."',";
		}
		/*else if($value==3){
			$redo=true;
			$sql4.=$handin_user_id[$i].",";
		}*/
		else if($value==4) {
			$noNeedHandin = true;
			$sql5 .= "'".$handin_user_id[$i]."',";
		}
		else if($value==5) {
			$UnderProcessing = true;
			$sql6 .= "'".$handin_user_id[$i]."',";
		}
		else if($sys_custom['eHomework_Status_Supplementary'] && $value==6) {
			$Supplementary = true;
			$sql7 .= "'".$handin_user_id[$i]."',";
		}
	}
	
	$sql = substr($sql,0,strlen($sql)-1).")";
	$sql2 = substr($sql2,0,strlen($sql2)-1).")";
	$sql3 = substr($sql3,0,strlen($sql3)-1).")";
	//$sql4 = substr($sql4,0,strlen($sql4)-1).")";
	$sql5 = substr($sql5,0,strlen($sql5)-1).")";
	$sql6 = substr($sql6,0,strlen($sql6)-1).")";
	if($sys_custom['eHomework_Status_Supplementary']) {
		$sql7 = substr($sql7,0,strlen($sql7)-1).")";
	}
	
	if($handin) {
		$lhomework->db_db_query($sql);
	}
	if($notHandin) {
		$lhomework->db_db_query($sql2);
	}
	if($late) {
		$lhomework->db_db_query($sql3);
	}
	/*if($redo)
		$lhomework->db_db_query($sql4);*/
	if($noNeedHandin) {
		$lhomework->db_db_query($sql5);
	}
	if($UnderProcessing) {
		$lhomework->db_db_query($sql6);
	}
	if($sys_custom['eHomework_Status_Supplementary'] && $Supplementary) {
		$lhomework->db_db_query($sql7);
	}
	
	# Update Confirm User
	$sql_confirm = "UPDATE INTRANET_HOMEWORK SET HandinConfirmUserID='$UserID', HandinConfirmTime=NOW() WHERE HomeworkID='$hid'";
	$lhomework->db_db_query($sql_confirm);
}

$order = $order==""? 0 : $order;
$order_field=$order_field==""? 1 : $order_field;

$data = $lhomework->handinList($hid);

if($sys_custom['LivingHomeopathy'])
{
	$allowToSubmit = false;
	$include_js = '<script type="text/javascript" language="JavaScript" src="/templates/jquery/jquery.blockUI.js"></script>'."\n";
	$include_js.= '<script type="text/javascript" language="JavaScript" src="/templates/jquery/jquery.scrollTo-min.js"></script>'."\n";
	$document_size_limit = isset($sys_custom['eHomework']['HomeworkDocumentSizeLimit']) && is_numeric($sys_custom['eHomework']['HomeworkDocumentSizeLimit'])? $sys_custom['eHomework']['HomeworkDocumentSizeLimit'] : 100; // default 100MB
	$handin_documents = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$hid, 'StudentIDToRecord'=>1, 'GetTeacherName'=>1));
}

$insertSql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,MainStatusRecordDate,DateInput,DateModified) VALUES";

$values = "";
if (sizeof($data) != 0)
{
	$reset = "<a href=\"javascript:document.form1.reset()\" class=\"contenttool\">$button_reset</a>";
	$setAllToHandin = "<a href=\"javascript:setAllToHandin()\" class=\"contenttool\">".$Lang['SysMgr']['Homework']['SetAllToSubmitted']."</a>";
	if(date("Y-m-d") <= $due) {
		$setAllToNotHandin = "<a href=\"javascript:setAllToNotHandin()\" class=\"contenttool\">".$Lang['SysMgr']['Homework']['SetAllToNotSubmitted']."</a>";
	}
	$setAllToLate = "<a href=\"javascript:setAllToLate()\" class=\"contenttool\">".$Lang['SysMgr']['Homework']['SetAllToLateSubmitted']."</a>";
	//$setAllToRedo = "<a href=\"javascript:setAllToRedo()\" class=\"contenttool\">".$Lang['SysMgr']['Homework']['SetAllToRedo']."</a>";
	
	if(date("Y-m-d") <= $due) {
		$setNotHandinToNoNeedHandin = "<a href=\"javascript:setNotHandinToNoNeedHandin()\" class=\"contenttool\">".$Lang['SysMgr']['Homework']['SetNotSubmittedToNoNeedSubmit']."</a>";
	}
	else {
		$setNotHandinToNoNeedHandin = "<a href=\"javascript:setNotHandinToNoNeedHandin()\" class=\"contenttool\">".$Lang['SysMgr']['Homework']['SetExpiredWithoutSubmissionToNoNeedSubmit']."</a>";
	}
	
	if($sys_custom['LivingHomeopathy']){
		$reset = "&nbsp;";
		$setAllToHandin = "&nbsp;";
		$setAllToNotHandin = "&nbsp;";
		$setAllToLate = "&nbsp;";
		$setNotHandinToNoNeedHandin = "&nbsp;";
		$col_widths = array(3,8,8,6,25,25,25);
	}
	else{
		$col_widths = array(5,35,45,30);
	}
	
	$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>";
	$x .= "<tr>";
	$x .= "<td width='".$col_widths[0]."%' class='tablebluetop tabletopnolink'>#</td>";
	$x .= "<td width='".$col_widths[1]."%' class='tablebluetop tabletopnolink'>$i_ClassNameNumber</td>";
	$x .= "<td width='".$col_widths[2]."%' class='tablebluetop tabletopnolink'>$i_UserStudentName</td>";
	if($sys_custom['LivingHomeopathy']){
		$x .= "<td width='".$col_widths[3]."%' class='tablebluetop tabletopnolink'>".$Lang['General']['Status2']."</td>";
		$x .= "<td width='".$col_widths[4]."%' class='tablebluetop tabletopnolink'>".$Lang['eHomework']['HandedInHomework']."</td>";
		$x .= "<td width='".$col_widths[5]."%' class='tablebluetop tabletopnolink'>".$Lang['eHomework']['Mark']."</td>";
		$x .= "<td width='".$col_widths[6]."%' class='tablebluetop tabletopnolink'>".$Lang['eHomework']['Rating']."</td>";
	}
	else{
		$x .= "<td width='".$col_widths[3]."%' class='tablebluetop tabletopnolink'>$i_Homework_handin_change_status</td>";
	}
	$x .= "</tr>";
	
	// $notSubmitStr = date("Y-m-d") <= $due ? $Lang['SysMgr']['Homework']['NotSubmitted'] : $Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'];
	if (date("Y-m-d") <= $due) {
		$notSubmitStr = $Lang['SysMgr']['Homework']['NotSubmitted'];
	}
	else {
		$notSubmitStr = Get_Lang_Selection($handInStatusArr["-1"]["HandInStatusTitleB5"], $handInStatusArr["-1"]["HandInStatusTitleEN"]);
	}
	
	for ($i=0; $i<sizeof($data); $i++)
	{
		$totalCount++;
		$rec = $data[$i];
		list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title,$due,$SuppRecordStatus,$studentStatus,$LeaveStudent) = $data[$i];
		
		$str_status = "";
		$css = "";
		$handinStatus = $handinStatus=="" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;
		if (in_array($SuppRecordStatus, array(7,-11)) && !$allowToSubmit)
	    {
	        $handinStatus = $SuppRecordStatus;
		}
		
		/* Replace by Database value
		switch($handinStatus){
				case 1 : $str_status=$Lang['SysMgr']['Homework']['Submitted']; $css="attendancepresent"; $handinCount++; break;
				case -1: $str_status=$notSubmitStr;$css="attendanceabsent"; break;
				case 2 : $str_status=$Lang['SysMgr']['Homework']['LateSubmitted']; $css="attendanceabsent"; $handinCount++; break;
				//case 3 : $str_status=$Lang['SysMgr']['Homework']['Redo']; $css="attendanceabsent"; break;
				case 4 : $str_status=$Lang['SysMgr']['Homework']['NoNeedSubmit']; $css="attendanceouting"; $totalCount--; break;
				case 6 : $str_status=$Lang['SysMgr']['Homework']['Supplementary']; $css="attendanceabsent"; $handinCount++; break;
				default: $str_status=$Lang['SysMgr']['Homework']['Submitted'];$css="attendancepresent";
		}
		*/
		if ($handinStatus != -1) {
			$str_status = Get_Lang_Selection($handInStatusArr[$handinStatus]["HandInStatusTitleB5"], $handInStatusArr[$handinStatus]["HandInStatusTitleEN"]);
		}
		else {
			$str_status = $notSubmitStr;
		}
		
		switch($handinStatus){
			case 1 : $css="attendancepresent"; $handinCount++; break;
			case -1: $css="attendanceabsent"; break;
			case 2 : $css="attendanceabsent"; $handinCount++; break;
			//case 3 : $css="attendanceabsent"; break;
            // [2020-0915-1021-34073] use $excludeCount > fix cannot update status
			//case 4 : $css="attendanceouting"; $totalCount--; break;
            case 4 : $css="attendanceouting"; $excludeCount++; break;
			case 6 : $css="attendanceabsent"; $handinCount++; break;
			default: $css="attendancepresent";
		}
        
		if (((!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) && !$sys_custom['LivingHomeopathy']) && ($allowToSubmit))
		{
			$select_status="<SELECT name='handin_status[]'>";
			/* Replace by Database value
			$select_status.="<OPTION value='1'".($handinStatus==1?"SELECTED":"").">".$Lang['SysMgr']['Homework']['Submitted']."</OPTION>";
			$select_status.="<OPTION value='-1'".($handinStatus==-1?"SELECTED":"").">".$notSubmitStr."</OPTION>";
			$select_status.="<OPTION value='2'".($handinStatus==2?"SELECTED":"").">".$Lang['SysMgr']['Homework']['LateSubmitted']."</OPTION>";
			//$select_status.="<OPTION value='3'".($handinStatus==3?"SELECTED":"").">".$Lang['SysMgr']['Homework']['Redo']."</OPTION>";
			$select_status.="<OPTION value='4'".($handinStatus==4?"SELECTED":"").">".$Lang['SysMgr']['Homework']['NoNeedSubmit']."</OPTION>";
			$select_status.="<OPTION value='5'".($handinStatus==5?"SELECTED":"").">".$Lang['SysMgr']['Homework']['UnderProcessing']."</OPTION>";
			if($sys_custom['eHomework_Status_Supplementary'] && date("Y-m-d") > $due) {
				$select_status.="<OPTION value='6'".($handinStatus==6?"SELECTED":"").">".$Lang['SysMgr']['Homework']['Supplementary']."</OPTION>";
			}
			*/
			if (count($handInStatusArr) > 0)
			{
				$suppArr = array();
				foreach ($handInStatusArr as $kk => $vv) {
					if ($vv["isDisabled"] !== "1" && !in_array($vv["HandInStatusLabel"], $notShowStatus)) {
						$hi_value = Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"]);
						if ($vv["HandInStatusValue"] == 6) {
							if($sys_custom['eHomework_Status_Supplementary'] && date("Y-m-d") > $due) {
								$select_status.="<OPTION value='" . $vv["HandInStatusValue"] . "' ".($handinStatus==$kk?"SELECTED":"").">". $hi_value ."</OPTION>";
							}
						}
						else {
							$select_status.="<OPTION value='" . $vv["HandInStatusValue"] . "' ".($handinStatus==$kk?"SELECTED":"").">". $hi_value ."</OPTION>";
						}
					}
				}
			}
			$select_status.="<input type=hidden name='handin_user_id[]' value='$uid'>";
			$select_status.="</SELECT>\n";
		}
		# Viewer Group Member
		else
		{
			/* Replace by Database value
			switch($handinStatus) {
				case 1 : $select_status = $Lang['SysMgr']['Homework']['Submitted']; break;
				case -1 : $select_status = $notSubmitStr; break;
				case 2 : $select_status = $Lang['SysMgr']['Homework']['LateSubmitted']; break;
				case 4 : $select_status = $Lang['SysMgr']['Homework']['NoNeedSubmit']; break;
				case 5 : $select_status = $Lang['SysMgr']['Homework']['UnderProcessing']; break;
				case 6 : $select_status = $Lang['SysMgr']['Homework']['Supplementary']; break;
				default : break;
			}
			*/
			$select_status = $str_status;
		}
		
		if($LeaveStudent == null){
		    $x .= "";
        }
        else{
            $x .= "<tr>";
            $x .= "<td class='tabletext $css' align = 'left'>".($i+1)."</td>";
            $x .= "<td class='$css' align = 'left'>$className ($classNumber)</td>";
            $x .= "<td class='$css' align = 'left'>$studentName</td>";
            $x .= "<td class='$css' align = 'left'>$select_status</td>";
            if($sys_custom['LivingHomeopathy'])
            {
                if(isset($handin_documents[$uid]) && $handin_documents[$uid]['StudentDocument']!=''){
                    $student_document_name = substr($handin_documents[$uid]['StudentDocument'],strrpos($handin_documents[$uid]['StudentDocument'],'/')+1);
                    $student_document_url = getEncryptedText($file_path.'/file/homework_document/'.$handin_documents[$uid]['StudentDocument']);
                    $student_document_display = '<a href="/home/download_attachment.php?target_e='.$student_document_url.'">'.intranet_htmlspecialchars($student_document_name).'</a>';
                    $student_document_display.= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_documents[$uid]['StudentDocumentUploadTime'],$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
                }
                else{
                    $student_document_display = $Lang['eHomework']['NotSubmitted'];
                }
                
                $teacher_document_display = '';
                $teacher_document_display .= '<div id="UploadSpinner_'.$uid.'" style="display:none">'.$linterface->Get_Ajax_Loading_Image(0,$Lang['eHomework']['Uploading']).'</div>';
                $display_uploader = (isset($handin_documents[$uid]) && $handin_documents[$uid]['TeacherDocument'] == '' && $handin_documents[$uid]['StudentDocument']!='')? 1: 0;
                $teacher_document_display .= '<div id="TeacherDocumentUpload_'.$uid.'" '.($display_uploader?'':'style="display:none"').'>
						<input type="file" name="TeacherDocument_'.$uid.'" id="TeacherDocument_'.$uid.'" value="" '.(!$display_uploader?'disabled':'').' />'.$linterface->GET_SMALL_BTN($Lang['Btn']['Upload'], 'button', 'uploadTeacherDocument('.$uid.');', 'UploadBtn_'.$uid, $___ParOtherAttribute="", $___OtherClass="", $___ParTitle='').'
						<div class="tabletextremark">('.str_replace('<!--SIZE-->',$document_size_limit,$Lang['eHomework']['FileSizeLimitRemark']).')</div>
						<div id="TeacherDocumentWarning_'.$uid.'" class="red warning" style="display:none">'.$Lang['General']['PleaseSelectFiles'].'</div>
						<iframe id="FileUploadFrame_'.$uid.'" name="FileUploadFrame_'.$uid.'" style="display:none;"></iframe>
					</div>';
                $teacher_document_display .= '<div id="TeacherDocumentLink_'.$uid.'">';
                if(isset($handin_documents[$uid]) && $handin_documents[$uid]['TeacherDocument']!='')
                {
                    $teacher_document_name = substr($handin_documents[$uid]['TeacherDocument'],strrpos($handin_documents[$uid]['TeacherDocument'],'/')+1);
                    $teacher_document_url = getEncryptedText($file_path.'/file/homework_document/'.$handin_documents[$uid]['TeacherDocument']);
                    $teacher_document_display .= '<table><tr>';
                    $teacher_document_display .= '<td><a href="/home/download_attachment.php?target_e='.$teacher_document_url.'">'.intranet_htmlspecialchars($teacher_document_name).'</a></td>';
                    $teacher_document_display .= '<td><span class="table_row_tool"><a href="javascript:void(0);" class="delete_dim" onclick="deleteTeacherDocument('.$handin_documents[$uid]['RecordID'].','.$handin_documents[$uid]['StudentID'].');" title="'.$Lang['Btn']['Delete'].'"></a></span></td>';
                    $teacher_document_display .= '</tr></table>';
                    $teacher_document_display .= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_documents[$uid]['TeacherDocumentUploadTime'],str_replace('<!--NAME-->',$handin_documents[$uid]['TeacherDocumentUploaderName'],$Lang['eHomework']['TeacherDocumentSubmittedRemark'])).')</div>';
                }
                $teacher_document_display .= '</div>';
                
                $score_input_text = '';
                if(isset($handin_documents[$uid]) && $handin_documents[$uid]['TextScore']!=''){
                    $score_input_text = $handin_documents[$uid]['TextScore'];
                }
                
                $score_input_display = '';
                if(isset($handin_documents[$uid]) && $handin_documents[$uid]['StudentDocument']!='')
                {
                    $score_input_display.= $linterface->GET_TEXTAREA("TextScore_".$uid, $score_input_text, $___taCols=40, $___taRows=5, $___OnFocus = "", $___readonly = "", $___other=' style="width:98%;" ', $___class='', $___taID='', $___CommentMaxLength='');
                    $score_input_display.= '<div style="text-align:right;">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], 'button', 'updateTextScore('.$uid.');', 'UpdateScoreBtn'.$uid, $___ParOtherAttribute="", $___OtherClass="", $___ParTitle='').'</div>';
                    $score_input_display.= '<div id="TextScoreRemark_'.$uid.'" class="tabletextremark">';
                    if($handin_documents[$uid]['TextScoreUpdaterName'] != '' && trim($score_input_text)!=''){
                        $score_input_display.= '('.str_replace('<!--DATETIME-->',$handin_documents[$uid]['TextScoreUpdateTime'],str_replace('<!--NAME-->',$handin_documents[$uid]['TextScoreUpdaterName'],$Lang['eHomework']['RatedRemark'])).')';
                    }
                    $score_input_display.= '</div>';
                }
                
                $x .= "<td class='$css' align='left'>".$student_document_display."</td>";
                $x .= "<td class='$css' align='left'>".$teacher_document_display."</td>";
                $x .= "<td class='$css' align='left'>".$score_input_display."</td>";
            }
            $x .= "</tr>";
        }
		
	
		# Set values of records in INTRANET_HOMEWORK_HANDIN_LIST 
		$RecordStatus = $sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1;
		
		if($handinID == "" && $studentStatus != '3') {
			$y = "('$hid','$uid','$RecordStatus',NOW(),NOW(),NOW()),";
			$values .= $y;
		}
	}
//	$x .= "<tr><td>".$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']."</td></tr>";
	
	$x .= "</table>\n";
    
    $x .= "<tr><td><table width='90%'><tr><td></td><td width='95%'>".$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']."</td></tr></table></td></tr>";
    
	# Insert if records not exists
    // [2020-0910-1206-37073] not auto create hand-in list records
	if($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records']){
		// do nothing
	}
	else if($values!="") {
		$values = substr($values,0,strrpos($values,","));
		$insertSql .= $values;
		$lhomework->db_db_query($insertSql);
	}
	
	if($confirmUserID!=""){
		$confirm_user_name_field = getNameFieldByLang();
		$temp = $lhomework->returnVector("SELECT $confirm_user_name_field FROM INTRANET_USER WHERE UserID='$confirmUserID'",1);
		$confirm_user_name = $temp[0];
		$last_modify = $confirmTime;
		$last_modify_by = $confirm_user_name;
	}
	else{
		$last_modify ="--";
	}
}
else
{
	//$NoMsg = $i_Notice_NoStudent;
	$NoMsg = "This homework has not been assigned to any students.";
	$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>";
	$x .= "<tr>";
	$x .= "<td align='center' class='tabletext'>$NoMsg</td>";
	$x .= "</tr>\n";
	$x .= "</table>\n";
	
	$last_modify ="--";
}
?>

<?=$include_js?>

<script type="text/javascript" language="javascript">
function checkform(formObj){
	if(confirm("<?=$i_SmartCard_Confirm_Update_Attend?>")){
		formObj.update.value="1";
		formObj.submit();
	}
}

function orderBy(id){
	formObj=document.form1;
	if(formObj==null) return;
	
	old_field = formObj.order_field.value;
	if(old_field==id) {
		formObj.order.value=formObj.order.value=='1'?'0':'1';
	}
	else {
		formObj.order.value='1';
	}
	
	formObj.order_field.value=id;
	formObj.update.value="0";
	formObj.submit();
}

function setNotHandinToNoNeedHandin(){
	formObj = document.form1;
	if(formObj==null) return;
	
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		if(objStatus[i].selectedIndex==1) {
			objStatus[i].options[3].selected=true;
		}
	}
}

function setAllToHandin(){
	formObj = document.form1;
	if(formObj==null) return;
	
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[0].selected=true;
	}
}

function setAllToNotHandin(){
	formObj = document.form1;
	if(formObj==null) return;
	
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[1].selected=true;
	}
}

function setAllToLate(){
	formObj = document.form1;
	if(formObj==null) return;
	
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[2].selected=true;
	}
}

function setAllToRedo(){
	formObj = document.form1;
	if(formObj==null) return;
	
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[3].selected=true;
	}
}

function click_print(){
	document.form1.action = "handin_list_print_preview.php?";
    document.form1.target = "_blank";
    document.form1.submit();
    
    document.form1.target = "_self";
    document.form1.action = "";  
}

function csv_export(){
	window.location="handin_list_export.php?hid=<?=$hid?>&subject=<?=$subjectName?>&subjectGroup=<?=addslashes($subjectGroupName)?>";
}

<?php if($sys_custom['LivingHomeopathy']){ ?>
function uploadTeacherDocument(studentId)
{
	$('.warning').hide();
	if($('#TeacherDocument_'+studentId).val() == ''){
		$('#TeacherDocumentWarning_'+studentId).show();
		return;
	}
	Block_Document();
	$('#TeacherDocumentUpload_'+studentId).hide();
	$('#UploadSpinner_'+studentId).show();
	
	var obj = document.form1;
	var old_action = obj.action;
	var old_target = obj.target;
	var old_encoding = obj.encoding;
	
	if($('#StudentID').length == 0) {
		$(document.getElementsByName('hid')[0]).after('<input type="hidden" id="StudentID" name="StudentID" value="'+studentId+'" />');
	}
	else{
		$('#StudentID').val(studentId);
	}
	
	obj.action = "upload_document.php";
	obj.target = "FileUploadFrame_" + studentId;
	obj.encoding = "multipart/form-data";
	obj.method = 'post';
	obj.submit();
	
	obj.action = old_action;
	obj.target = old_target;
	obj.encoding = old_encoding;
}

function teacherDocumentUploaded(studentId,success,returnHtml)
{
	UnBlock_Document();
	$('#UploadSpinner_'+studentId).hide();
	$('#TeacherDocumentLink_'+studentId).html(returnHtml);
	if(success){
		$('#TeacherDocumentUpload_'+studentId).hide();
		Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
	}
	else{
		$('#TeacherDocumentUpload_'+studentId).show();
		Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
	}
	Scroll_To_Top();
}

function deleteTeacherDocument(recordId,studentId)
{
	if(confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>'))
	{
		Block_Document();
		$.post(
			'delete_document.php',
			{
				"RecordID":recordId 
			},
			function(returnCode){
				UnBlock_Document();
				if(returnCode == '1'){ // success
					$('#TeacherDocumentLink_'+studentId).html('');
					$('#TeacherDocumentUpload_'+studentId).show();
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
				}
				else{
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>');
				}
				Scroll_To_Top();
			}
		);
	}
}

function updateTextScore(studentId)
{
	if(confirm('<?=$Lang['eHomework']['ConfirmUpdateRatingMsg']?>'))
	{
		Block_Document();
		var homeworkId = document.getElementsByName('hid')[0].value;
		$.post(
			'update_text_score.php',
			{
				"HomeworkID":homeworkId,
				"StudentID":studentId,
				"TextScore":encodeURIComponent($('#TextScore_'+studentId).val()) 
			},
			function(returnJson){
				UnBlock_Document();
				var result_obj = {};
				if(JSON && JSON.parse){
					result_obj = JSON.parse(returnJson);
				}
				else{
					eval('result_obj='+returnJson+';');
				}
				if(result_obj['result'] == '1'){
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
					$('#TextScoreRemark_'+studentId).html(result_obj['content']);
				}
				else{
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
				}
				Scroll_To_Top();
			}
		);
	}
}
<?php } ?>
</script>

<form name="form1" action="handin_list.php" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<!-- Homework Detail -->
		<tr>
			<td>
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<br />
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Subject']?></span></td>
									<td class='tabletext'><?=$subjectName?></td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
									<td class='tabletext'><?=$subjectGroupName?></td>
								</tr>
								
							   	<? if($lhomework->useHomeworkType) { ?>
    								<tr valign='top'>
    									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Homework_HomeworkType?></span></td>
    									<td class='tabletext'><?=$typeName?></td>
    								</tr>
								<? } ?>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Topic']?></span></td>
									<td class='tabletext'><?=$title?></td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['StartDate']?></span></td>
									<td class='tabletext'><?=$start?></td>
									<td align="right">
    									<?php
    									if (((!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) && !$sys_custom['LivingHomeopathy']) && ($allowToSubmit)) {
    		                                echo $reset; 
    									}
    									else {
    									    echo "&nbsp;";
    									}
    									?>
									</td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['DueDate']?></span></td>
									<td class='tabletext'><?=$due?></td>
									<td align="right">
									<?php
    									if (((!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) && !$sys_custom['LivingHomeopathy']) && ($allowToSubmit)) {
    		                                echo $setAllToHandin; 
    									}
    									else {
    									    echo "&nbsp;";
    									}
									?>
									</td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Handin']."/".$Lang['SysMgr']['Homework']['Total']?></span></td>
									<td class='tabletext'><?="$handinCount/".($totalCount - $excludeCount)?></td>
									<td align="right">
									<?php
    									if (((!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) && !$sys_custom['LivingHomeopathy']) && ($allowToSubmit)) {
    		                                echo $setAllToNotHandin; 
    									}
    									else {
    									    echo "&nbsp;";
    									}
									?>
									</td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_LastModified?></span></td>
									<td class='tabletext'><?=$last_modify?></td>
									<td align="right">
									<?php
    									if (((!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) && !$sys_custom['LivingHomeopathy']) && ($allowToSubmit)) {
    		                                echo $setAllToLate; 
    									}
    									else {
    									    echo "&nbsp;";
    									}
									?>
									</td>
								</tr>
								
								<!-- Last Modified by -->
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['LastModifiedBy']?></span></td>
									<td class='tabletext'><?=$last_modify_by?></td>
									<td align="right">
									<?php
    									if (((!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) && !$sys_custom['LivingHomeopathy']) && ($allowToSubmit)) {
    		                                echo $setNotHandinToNoNeedHandin; 
    									}
    									else {
    									    echo "&nbsp;";
    									}
									?></td>
								</tr>
								
								<tr>
									<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<!-- Hand-in List -->
		<tr>
			<td><?=$x?></td>
		</tr>
		
		<!-- Button //-->
		<tr>
			<td>
				<? if($totalCount != 0) { ?>
					<table width="95%" border="0" cellspacing="0" cellpadding="2" align="center">
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
						<tr>
							<td align="center">
								<? if($allowToSubmit) {
                                    echo $linterface->GET_ACTION_BTN($button_submit, "button", "checkform(document.form1)");
								} ?>
								<?= $linterface->GET_ACTION_BTN($button_export, "button", "csv_export()") ?>                					
								<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
								<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()") ?>
							</td>
						</tr>
					</table>
				<? } ?>
			</td>
		</tr>
	</table>
	<br />
	
	<input type="hidden" name="hid" value="<?=$hid?>">
	<input type="hidden" name="subjectGroup" value="<?=$subjectGroupName?>">
	<input type="hidden" name="order_field" value="<?=$order_field?>">
	<input type="hidden" name="referer" value="<?=$referer?>">
	<input type="hidden" name="order"  value="<?=$order?>">
	<input type="hidden" name="update" value="">
</form>

<br><br><br>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>