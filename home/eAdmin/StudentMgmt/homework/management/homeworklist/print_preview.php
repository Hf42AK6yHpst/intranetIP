<?php
// Modifing by Bill

/********************** Change Log ***********************
#
#	Date    :	2019-05-08 (Bill)
#               prevent SQL Injection
#
#	Date	:	2015-03-11 (Bill)
#				display subject teacher name and change field title
#
#	Date	:	2010-10-28 (YatWoon)
#				update insertHomeworkListRecord(), add CreatedBy field
#
#************************************************************************
*/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// 2015-03-11 added
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
// 2015-03-11 added
$scm = new subject_class_mapping();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$CurrentPageArr['eAdminHomework'] = 1;

$hid = IntegerSafe($hid);
$record = $lhomework->returnRecord($hid);

list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath,$handinRequired,$collectRequired,$PostUserID, $CreatedBy)=$record; 
$lu = new libuser($PostUserID);
$PosterName2 = $lu->UserName();

$lu2 = new libuser($CreatedBy);
$CreatedByName = $lu2->UserName();

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

// 2015-03-11 Get Subject Teacher Name
$subjectTeacherName = "";
$subjectTeacherList = $scm->Get_Subject_Group_Teacher_Info((array)$subjectGroupID);
if(count($subjectTeacherList) > 0){
	$delim = '';
	foreach($subjectTeacherList as $subjectTeacher){
		$lu3 = new libuser($subjectTeacher['UserID']);
		$teacherName = $lu3->UserName();
		$subjectTeacherName .= $teacherName? $delim.$teacherName : "--";
		$delim = ', ';
		unset($lu3);
	}
}

if($lhomework->useHomeworkType) {
	$typeInfo = $lhomework->getHomeworkType($TypeID);
	$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
}

$description = nl2br($description);

if($AttachmentPath != NULL){
	$displayAttachment = $lhomework->displayAttachment2($hid);
}
?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
    	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['HomeworkList']?></b></td>
	</tr>
</table>        

<table width="50%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td></td><td></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subjectName?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> : </td><td align="left" valign="top"><?=$subjectGroupName?></td></tr>
	<? if($lhomework->useHomeworkType) {?>
		<tr><td align="left" valign="top"><?=$i_Homework_HomeworkType?> : </td><td align="left" valign="top"><?=$typeName?></td></tr>
	<? } ?>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Topic']?> : </td><td align="left" valign="top"><?=$title?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?> : </td><td align="left" valign="top"><?=($description==""? "--": $description)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Attachment']?> : </td><td align="left" valign="top"><?=($AttachmentPath==NULL? $i_AnnouncementNoAttachment: $displayAttachment)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Workload']?> : </td><td align="left" valign="top"><?=($loading/2)." ".$Lang['SysMgr']['Homework']['Hours']?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['StartDate']?> : </td><td align="left" valign="top"><?=$start?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']["DueDate"]?> : </td><td align="left" valign="top"><?=$due?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['HandinRequired']?> : </td><td align="left" valign="top"><?=($handinRequired=="1"? $i_general_yes :$i_general_no)?></td></tr>
	<? if($lhomework->useHomeworkCollect) {?>
		<tr><td align="left" valign="top"><?=$i_Homework_Collected_By_Class_Teacher?> : </td><td align="left" valign="top"><?=($collectRequired=="1"? $i_general_yes :$i_general_no)?></td></tr>
	<? } ?>
	<tr><td align="left" valign="top"><?=$Lang['General']['LastModifiedBy']?> : </td><td align="left" valign="top"><?=($PosterName2==""? "--":$PosterName2)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['CreatedBy']?> : </td><td align="left" valign="top"><?=($CreatedBy ? $CreatedByName : $PosterName2)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroupTeacher']?> : </td><td align="left" valign="top"><?=($subjectTeacherName ? $subjectTeacherName : "--")?></td></tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>