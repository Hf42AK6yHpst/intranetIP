<?php
// modifying : 

###################################
#
#	Date    :	2019-05-08 (Bill)
#               prevent SQL Injection
#
#	Date	:	2016-08-10 (Bill)	[2016-0519-1005-58207]
#	Detail	:	Update logic that check if current user can update Hand-in List status according to user role and system settings
#				- View Group Member can view status only
#
###################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$MODULE_OBJ['title'] = $Lang['SysMgr']['Homework']['HomeworkHandinlist'];

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$CurrentPageArr['eAdminHomework'] = 1;

### Handle SQL Injection + XSS [START]
$hid = IntegerSafe($hid);
$handin_status = IntegerSafe($handin_status);
$handin_user_id = IntegerSafe($handin_user_id);
### Handle SQL Injection + XSS [END]

$record= $lhomework->returnRecord($hid);
list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath,$handinRequired,$collectRequired)=$record; 

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);
$handinCount = 0;
$totalCount = 0;
$singleTable = false;

if($lhomework->useHomeworkType) {
	$typeInfo = $lhomework->getHomeworkType($TypeID);
	$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
}

// Current User Role
$isNonTeachingStaff = $_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'];
$isNonTeachingStaffAllManage = $isNonTeachingStaff && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"];
$isViewGroupMemeber = $lhomework->isViewerGroupMember($UserID);
$isSubjectLeader = $lhomework->isSubjectLeader($UserID);

/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus();
$notShowStatus = array( "LateSubmitted", "UnderProcessing", "NoNeedSubmit", "Supplementary", "NotSubmittedRecord", "DueDateNotSubmitted" );
/******************************/

# check whether current user can submit the form or not
$allowToSubmit = false;

if($lhomework->ClassTeacherCanViewHomeworkOnly) {
	/*
	$teachingSg = $lhomework->getTeachingSubjectList($UserID, Get_Current_Academic_Year_ID(), GetCurrentSemesterID());

	$NoOfTeachingGroups = count($teachingSg);
	for($a=0; $a<$NoOfTeachingGroups; $a++) {
		list($sbj_id, $sbj_name) = $teachingSg[$a];

		if($sbj_id==$subjectID) $allowToSubmit = true;	
	}
	*/
	// Admin or Non-teaching Staff or Subject Leader can always submit
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $isNonTeachingStaffAllManage || $isSubjectLeader){
		$allowToSubmit = true;
	}
	// check if user teach this subject group
	else{
		$relatedGroups = $lhomework->getSubjectGroupIDByTeacherID($UserID, Get_Current_Academic_Year_ID(), GetCurrentSemesterID());
		$relatedGroups = Get_Array_By_Key((array)$relatedGroups, "SubjectGroupID");
		$allowToSubmit = in_array($subjectGroupID, (array)$relatedGroups);
	}
}
else if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
	$allowToSubmit = true;

	// [2016-0519-1005-58207] Viewer Group Member extra checking
	$needToCheckSubjectGroup = !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$isNonTeachingStaffAllManage && !$isSubjectLeader && $isViewGroupMemeber;
	if($needToCheckSubjectGroup)
	{
		// check if user teach this class or subject group
		$relatedGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), "", 1);
		$relatedGroups = Get_Array_By_Key((array)$relatedGroups, "SubjectGroupID");
		if(!in_array($subjectGroupID, (array)$relatedGroups))
			$allowToSubmit = false;
	}
}

$update = $update==""? 0 : $update;

if(sizeof($handin_user_id)>0 && $update==1)
{
	$handin = false;
	$notHandin = false;
	$late = false;
	$redo = false;
	$noNeedHandin = false;
	$sqlParam = array();
	
	foreach ($handin_user_id as $kk => $vv) {
		if (isset($handin_status[$kk])) {
			$key = $handin_status[$kk];
		}
		else {
			$key = "-1";
		}
		$sqlParam[$key][] = $vv;
	}
	
	$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='-1' WHERE HomeworkID = '".$hid."' AND StudentID NOT IN (".implode(", ", $handin_user_id).")";
	$lhomework->db_db_query($sql);
	if (count($sqlParam) > 0) {
		foreach ($sqlParam as $kk => $handinUserIDs) {
			if (count($handinUserIDs) > 0) {
				$sql  = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='".$kk."' WHERE HomeworkID='".$hid."' AND StudentID IN (".implode(", ", $handinUserIDs).")";
				$lhomework->db_db_query($sql);
			}
		}
	}
	
	/* update confirm user */
	$sql_confirm = "UPDATE INTRANET_HOMEWORK SET HandinConfirmUserID = '$UserID', HandinConfirmTime=NOW() WHERE HomeworkID = '$hid'";
	$lhomework->db_db_query($sql_confirm);
	/* need to change Status */
}

$order = $order==""? 0 : $order;
$order_field = $order_field==""? 1 : $order_field;

$data = $lhomework->handinList($hid);

$insertSql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,DateInput,DateModified) VALUES";
$values = "";

if (sizeof($data)!=0)
{
	// $notSubmitStr = date("Y-m-d") <= $due ? $Lang['SysMgr']['Homework']['NotSubmitted'] : $Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'];
	
	if (date("Y-m-d") <= $due) {
		$notSubmitStr = $Lang['SysMgr']['Homework']['NotSubmitted'];
	}
	else {
		$notSubmitStr = Get_Lang_Selection($handInStatusArr["-1"]["HandInStatusTitleB5"], $handInStatusArr["-1"]["HandInStatusTitleEN"]);
	}
	
	$x .= "<table width='98%' border='0' cellspacing='0' cellpadding='5' align='center' id='handincontent'>";
	$x .= "<tr valign=bottom>";
	$x .= "<td width='5%' class='tablebluetop tabletopnolink'>#</td>";

	if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
		$x .= "<td width='15%' class='tablebluetop tabletopnolink'>$i_ClassNameNumber</td>";
		$x .= "<td width='10%' class='tablebluetop tabletopnolink'>$i_UserStudentName</td>";
		$totalwidth = 80;
		
		if (count($handInStatusArr) > 0) {
			$wid = floor($totalwidth / count($handInStatusArr));
			foreach ($handInStatusArr as $kk => $vv) {
				if ($vv["isDisabled"] !== "1" && !in_array($vv["HandInStatusLabel"], $notShowStatus)) {
					if ($vv["HandInStatusValue"] != "-1") {
						$statusLabel = Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"]);
					} else {
						$statusLabel = $notSubmitStr;
					}
					$x .= "<td width='" . $wid . "%' align='center' class='tablebluetop tabletopnolink'>" . $statusLabel; 
					$x .= "<br><button class='checkhdr' rel='" . $vv["HandInStatusLabel"] . "' style='padding:4px padding-left:5px; padding-right:5px; margin-top:4px; font-size:0.85em;'><nobr>" . $Lang['Btn']['SelectAll'] . "</nobr></button>";
					$x .= "</td>";
				}
			}
		}
	}
	else {
		$x .= "<td width='35%' class='tablebluetop tabletopnolink'>$i_ClassNameNumber</td>";
		$x .= "<td width='45%' class='tablebluetop tabletopnolink'>$i_UserStudentName</td>";
		$x .= "<td width='30%' class='tablebluetop tabletopnolink'>$i_Homework_handin_change_status</td>";
	}
	$x .= "</tr>";
	
	for ($i=0; $i<sizeof($data); $i++)
	{
		$totalCount++;
		
		list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title) = $data[$i];
		$str_status = "";
		$css = "";
		$handinStatus = $handinStatus=="" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;

		if ($handinStatus != -1) {
			$str_status = Get_Lang_Selection($handInStatusArr[$handinStatus]["HandInStatusTitleB5"], $handInStatusArr[$handinStatus]["HandInStatusTitleEN"]);
		}
		else {
			$str_status = $notSubmitStr;
		}
        
		switch($handinStatus){
			case 1 : $css="attendancepresent"; $handinCount++; break;
			case -1: $css="attendanceabsent"; break;
			case 2 : $css="attendanceabsent"; $handinCount++; break;
			//case 3 : $css="attendanceabsent"; break;
			case 4 : $css="attendanceouting"; $totalCount--; break;
			case 6 : $css="attendanceabsent"; $handinCount++; break;
			default: $css="attendancepresent";
		}
        
		if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
			$x .= "<tr>";
			$x .= "<td class='tabletext' align = 'left'>".($i+1)."</td>";
			$x .= "<td align = 'left'>$className ($classNumber)</td>";
			$x .= "<td align = 'left'>$studentName <input type=hidden name='handin_user_id[" . $uid . "]' value='" . $uid . "'></td>";
			if (count($handInStatusArr) > 0) {
				$wid = floor(65 / count($handInStatusArr));
				foreach ($handInStatusArr as $kk => $vv) {
					if ($vv["isDisabled"] !== "1" && !in_array($vv["HandInStatusLabel"], $notShowStatus)) {
						if (count($handInStatusArr) > 0) {
							$select_status = "<input type='checkbox' class='" . $vv["HandInStatusLabel"] . " row_" . $i . "' name='handin_status[" . $uid . "]' value='" . $vv["HandInStatusValue"] . "' rel='row_" . $i . "'";
							if ($vv["HandInStatusValue"] == $handinStatus) {
								$select_status .= " checked";
							}
							$select_status .= ">";
						}
						$x .= "<td width='" . $wid . "%' align='center'>$select_status</td>";
					}
				}
			}
			$x .= "</tr>";
		}
		else {	# viewer group member
			/* Replace by Database value
			switch($handinStatus) {
				case 1 : $select_status = $Lang['SysMgr']['Homework']['Submitted']; break;
				case -1 : $select_status = $notSubmitStr; break;
				case 2 : $select_status = $Lang['SysMgr']['Homework']['LateSubmitted']; break;
				case 4 : $select_status = $Lang['SysMgr']['Homework']['NoNeedSubmit']; break;
				case 5 : $select_status = $Lang['SysMgr']['Homework']['UnderProcessing']; break;
				case 6 : $select_status = $Lang['SysMgr']['Homework']['Supplementary']; break;
				default : break;
			}
			*/
			$select_status = $str_status;
			$x .= "<tr>";
			$x .= "<td class='tabletext $css' align = 'left'>".($i+1)."</td>";
			$x .= "<td class='$css' align = 'left'>$className ($classNumber)</td>";
			$x .= "<td class='$css' align = 'left'>$studentName</td>";
			$x .= "<td class='$css' align = 'left'>$select_status</td>";
			$x .= "</tr>";
		}
		
		# set values of records in INTRANET_HOMEWORK_HANDIN_LIST 
		$RecordStatus = $sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1;
		if($handinID==""){
			$y ="($hid,$uid,$RecordStatus,NOW(),NOW()),";
			$values .= $y;
		}
	}
	$x .= "</table>\n";
	
	# insert if records not exists
	if($values!=""){
		$values = substr($values,0,strrpos($values,","));
		$insertSql .=$values;
		$lhomework->db_db_query($insertSql);
	}
	
	if($confirmUserID!=""){
		$confirm_user_name_field = getNameFieldByLang();
		$temp = $lhomework->returnVector("SELECT $confirm_user_name_field FROM INTRANET_USER WHERE UserID='$confirmUserID'",1);
		$confirm_user_name = $temp[0];
		$last_modify = $confirmTime;
		$last_modify_by = $confirm_user_name;
	}
	else{
		$last_modify ="--";
	}
}
else
{
	//$NoMsg = $i_Notice_NoStudent;
	$NoMsg = "This homework has not been assigned to any students.";
	$x .= "<table width='98%' border='0' cellspacing='0' cellpadding='5' align='center'>";
	$x .= "<tr>";
	$x .= "<td align='center' class='tabletext'>$NoMsg</td>";
	$x .= "</tr>\n";
	$x .= "</table>\n";
	
	$last_modify ="--";
}
 
?>
<script language="javascript">
function checkform(formObj){
	if(confirm("<?=$i_SmartCard_Confirm_Update_Attend?>")){
		formObj.update.value="1";
		formObj.submit();
	}
}

function orderBy(id){
	formObj=document.form1;
	if(formObj==null) return;
	old_field = formObj.order_field.value;
	if(old_field==id)
		formObj.order.value=formObj.order.value=='1'?'0':'1';
	else formObj.order.value='1';
	formObj.order_field.value=id;
	formObj.update.value="0";
	formObj.submit();
}

function setNotHandinToNoNeedHandin(){
	formObj = document.form1;
	if(formObj==null)return;
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		if(objStatus[i].selectedIndex==1)
			objStatus[i].options[3].selected=true;
	}
}

function setAllToHandin(){
	formObj = document.form1;
	if(formObj==null)return;
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[0].selected=true;
	}
}

function setAllToNotHandin(){
	formObj = document.form1;
	if(formObj==null)return;
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[1].selected=true;
	}
}

function setAllToLate(){
	formObj = document.form1;
	if(formObj==null)return;
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[2].selected=true;
	}
}

function setAllToRedo(){
	formObj = document.form1;
	if(formObj==null)return;
	objStatus = document.getElementsByName("handin_status[]");
	for(i=0;i<objStatus.length;i++){
		objStatus[i].options[3].selected=true;
	}
}

function click_print(){
	document.form1.action = "handin_list_print_preview.php?";
    document.form1.target = "_blank";
    document.form1.submit();
    document.form1.target = "_self";
    document.form1.action = "";  
}

function csv_export()
{
  window.location="handin_list_export.php?hid=<?=$hid?>&subject=<?=$subjectName?>&subjectGroup=<?=addslashes($subjectGroupName)?>";
}

var appJS = {
	vars: {
		container: "#handincontent"
	},
	listeners: {
		buttonHandler: function(e) {
			e.preventDefault();
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined"){
				var allLength = $(appJS.vars.container + ' input.' + strREL).length;
				var selectedLength = $(appJS.vars.container + ' input.' + strREL + ":checked").length;
				appJS.func.clearAll();
				if (allLength > selectedLength) { 
					$(appJS.vars.container + ' input.' + strREL).each(function() {
						$(this).attr("checked",true);
					});
				}
			}
			appJS.func.highlight();
		},
		inputClickHandler: function(e) {
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined"){
				var currObj = this;
				$(currObj).addClass('clicked');
				$('input.' + strREL).each(function() {
					if (!$(this).hasClass('clicked')) {
						$(this).removeAttr('checked');
					}
				});
				$(currObj).removeClass('clicked');
			}
			appJS.func.highlight();
		},
		trHoverHandler: function(e) {
			switch (e.type) {
				case "mouseover":
					$(this).css({"background-color": "#efefef"});
					break;
				default:
					try {
						switch ($(this).attr('data-col')) {
							case "dc_red":
								$(this).css({"background-color": "#ffe0e1"});
								break;
							case "dc_green":
								$(this).css({"background-color": "#e6ffde"});
								break;
							default:
								$(this).css({"background-color": "#fff"});
								break;
						}
					} catch (err) {
						
					}
					break;
			}
		}
	},
	func: {
		highlight: function() {
			if ($(appJS.vars.container).find('tr:not(:first-child)').length > 0) {
				$(appJS.vars.container).find('tr:not(:first-child)').each(function() {
					$(this).css({"background-color": "#fff"});
					$(this).attr('data-col', "dc_normal");
					var selectedOBJ = $(this).find('input[type="checkbox"]:checked').eq(0);
					if (selectedOBJ.hasClass('notSubmitStr') || selectedOBJ.hasClass('SuppNotSubmitted')) {
						$(this).css({"background-color": "#ffe0e1"});
						$(this).attr('data-col', "dc_red");
					} else if (selectedOBJ.hasClass('DueDateAbsence') || selectedOBJ.hasClass('ABSHomeworkReturn')) {
						$(this).css({"background-color": "#e6ffde"});
						$(this).attr('data-col', "dc_green");
					}
				});
			}
		},
		clearAll: function() {
			if ($(appJS.vars.container).find('input[type="checkbox"]').length > 0) {
				$(appJS.vars.container).find('input[type="checkbox"][checked]').each(function() {
					$(this).removeAttr('checked');
				});
			}
		},
		init: function() {
			$(appJS.vars.container).find('tr:not(:first-child) td').css({ "border-top":"1px solid #b8b8b8" });
			$(appJS.vars.container).find('button.checkhdr').bind('click', appJS.listeners.buttonHandler);
			$(appJS.vars.container).find('input[type="checkbox"]').bind('click', appJS.listeners.inputClickHandler);
			$(appJS.vars.container).find('tr:not(:first-child)').bind('mouseover mouseleave', appJS.listeners.trHoverHandler);
			appJS.func.highlight();
		}
	}
};

$(document).ready(function() {
	appJS.func.init();
});

</script>
<form name="form1" action="<?php echo basename(__FILE__); ?>" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<!-- Homework Detail -->
		<tr>
			<td>
				<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<br />
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Subject']?></span></td>
									<td class='tabletext'><?=$subjectName?></td>
									
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
									<td class='tabletext'><?=$subjectGroupName?></td>
									
								</tr>
							   <? if($lhomework->useHomeworkType) { ?>
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Homework_HomeworkType?></span></td>
									<td class='tabletext'><?=$typeName?></td>
									
								</tr>
								<? } ?>
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Topic']?></span></td>
									<td class='tabletext'><?=$title?></td>
									
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['StartDate']?></span></td>
									<td class='tabletext'><?=$start?></td>
									<td align="right"><?=$reset?></td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['DueDate']?></span></td>
									<td class='tabletext'><?=$due?></td>
									<td align="right"><?=$setAllToHandin?></td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Handin']."/".$Lang['SysMgr']['Homework']['Total']?></span></td>
									<td class='tabletext'><?="$handinCount/$totalCount"?></td>
									<td align="right"><?=$setAllToNotHandin?></td>
								</tr>
								
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_LastModified?></span></td>
									<td class='tabletext'><?=$last_modify?></td>
									<td align="right"><?=$setAllToLate?></td>
								</tr>
								
								<!--Last Modified by -->
								<tr valign='top'>
									<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['LastModifiedBy']?></span></td>
									<td class='tabletext'><?=$last_modify_by?></td>
									<td align="right"><?=$setNotHandinToNoNeedHandin?></td>
								</tr>
								
								<tr>
									<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
								</tr>        
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<!-- Hand-in List -->
		<tr>
			<td><?=$x?></td>
		</tr>        

		<!-- Button //-->
		<tr>
			<td>
				<?if($totalCount!=0) { ?>
					<table width="95%" border="0" cellspacing="0" cellpadding="2" align="center">
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
						
						<tr>
							<td align="center">
								<?
									if($allowToSubmit) {
										echo $linterface->GET_ACTION_BTN($button_submit, "button", "checkform(document.form1)"); 
									} ?>
								<?= $linterface->GET_ACTION_BTN($button_export, "button", "csv_export()") ?>                					
								<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
								<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()") ?>
							</td>
						</tr>
					</table>
				<? } ?>
			</td>
		</tr>
	</table>                        
	<br />

	<input type="hidden" name="hid" value="<?=$hid?>">
	<input type="hidden" name="subjectGroup" value="<?=$subjectGroupName?>">
	<input type="hidden" name="order_field" value="<?=$order_field?>">
	<input type="hidden" name="referer" value="<?=$referer?>">
	<input type="hidden" name="order"  value="<?=$order?>">
	<input type="hidden" name="update" value="">
</form>
<br><br><br>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>