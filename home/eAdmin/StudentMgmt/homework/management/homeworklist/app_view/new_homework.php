<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply intranet_auth()
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";

//set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//switch (strtolower($parLang)) {
//	case 'en':
//		$intranet_hardcode_lang = 'en';
//		break;
//	default:
//		$intranet_hardcode_lang = 'b5';
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$typeID = IntegerSafe($typeID);
### Handle SQL Injection + XSS [END]

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

$timestamp =  time();
$date_time_array =  getdate($timestamp);
$hours =  $date_time_array[ "hours"];
$minutes =  $date_time_array["minutes"];
$seconds =  $date_time_array[ "seconds"];
$month =  $date_time_array["mon"];
$day =  $date_time_array["mday"] + 7;
$year =  $date_time_array["year"];

// use mktime to recreate the unix timestamp
$timestamp =  mktime($hours, $minutes, $seconds ,$month, $day, $year);
$pre_startdate = date('Y-m-d',strtotime($today));
$pre_duedate = date('Y-m-d',$timestamp);
// $pre_startdate =$today;
// $pre_duedate = date('Y-m-d',$timestamp);

$default_upload_fields = 1;

intranet_auth();
intranet_opendb();

$libdb = new libdb();

$linterface = new interface_html();
$lhomework = new libhomework2007();

# Current Year Term
$yearID = Get_Current_Academic_Year_ID();
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID ? $yearTermID : 0;

$sql = "SELECT TypeID, TypeName FROM INTRANET_HOMEWORK_TYPE WHERE RecordStatus=1 ORDER BY DisplayOrder";
$homeworkType = $lhomework->returnArray($sql, 2);

$selectHomeworkType = getSelectByArray($homeworkType, "name='typeID' id='typeID'", $typeID, 0, 0);
echo $libeClassApp->getAppWebPageInitStart();
?>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>

<script>
	jQuery(document).ready( function() {
		var msg = "<?=$msg?>";
		if(msg=="add"){
			alert("<?=$Lang['ePost']['ReturnMessage']['AddSuccess']?>");
	    }
	    else if(msg=="add_failed"){
			alert("<?=$Lang['ePost']['ReturnMessage']['AddUnsuccess']?>");
		}		
		
		$('#div_content').load(
			'ajax_drop_down.php',
			{
				UserID: <?=$UserID?>,
				yearID: <?=$yearID?>,
				yearTermID: <?=$yearTermID?>,
				parLang: "<?=$parLang?>",
				token: "<?=$token?>",
		    	uid: "<?=$uid?>",
		    	ul: "<?=$ul?>"
			},
			function (data){
				// do nothing
			}
		); 
		
	    $.datepicker.setDefaults({
	        dateFormat: 'yy-mm-dd'
	    });
	});
	
	function goback(){
		 window.history.back()
    }
    
	function js_change_class(flag){
		if(flag==1) {
			document.getElementById('subjectID').selectedIndex = -1;
		}
		//else
		//	document.getElementById('classID').selectedIndex = -1;
		
		$('#div_content').load(
			'ajax_drop_down.php', 
			{
				UserID: <?=$UserID?>,
				yearID: <?=$yearID?>,
				yearTermID: <?=$yearTermID?>,
				parLang: "<?=$parLang?>",	
				<? if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])||$sys_custom['eClassTeacherApp']['eHomeworkCanNewAllClassesHomeowrk']) { ?>
				classID: document.form1.classID.value, 
				<? } ?>
				//subjectID: document.addhomework.subjectID.value
				subjectID: document.getElementById('subjectID').value,
				//subjectGroupID: document.addhomework.subjectGroupID[].value 
				token: "<?=$token?>",
		    	uid: "<?=$uid?>",
		    	ul: "<?=$ul?>"
			},
			function (data){
				// do nothing
			}
		);
	}
	
    function submit(){
       var pastInputallowed = "<?=$lhomework->pastInputAllowed?>";
       if(pastInputallowed==""){
    	   pastInputallowed = 0;
       }
       else{
       	   if("<?=$lhomework->pastInputAllowed?>"=="1"){
       	    	pastInputallowed = 1;
       	   }
       	   else if("<?=$lhomework->pastInputAllowed?>"=="0") {
    	        pastInputallowed = 0;
       	   }
       }
       
   	   if (confirm("<?=$Lang['StudentAttendance']['TeacherApp']['AreYouSureToSubmit'] ?>"))
   	   {
   	   	   if($('select[name="subjectGroupID[]"]').val() == null){
              alert("<?=$Lang['eHomework']['PleaseSelectSubjectGroup']?>")
   	   	   }
   	   	   else if(document.form1.title.value==""){
              alert("<?=$Lang['eHomework']['MissingTitle']?>");
              document.form1.title.focus();
   	   	   }
   	   	   else if(!pastInputallowed&&document.form1.startdate.value <"<?=date("Y-m-d");?>"){ 
            	  alert("<?=$i_Homework_new_startdate_wrong?>");
                  document.form1.startdate.focus();         
   	   	   }
   	   	   else if(document.form1.startdate.value > document.form1.duedate.value){
		   	   	 alert("<?=$i_Homework_new_duedate_wrong?>");
		         document.form1.duedate.focus();     
   	   	   }
   	   	   else{
   	          document.form1.action = "new_homework_update.php";
   	          document.form1.submit(); 
     	   }
   	   	}
    }
</script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<style type="text/css">
#body{
   background:#ffffff;
}

.form_table tr td{vertical-align:center; line-height:30px; padding-top:5px; padding-bottom:5px; padding-left:2px; padding-right:2px;border-bottom:2px solid #EFEFEF;}
</style>
</head>

	<body>
		<div data-role="page" id="body">			
            <form id="form1" name="form1" method="post" enctype="multipart/form-data">
            <div data-role="header" data-position="fixed"  style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
            	    <table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0">
					     <tr>
        			        <td style="background-color: #E7E7E7;border:0" align="center">
        			            <?=$Lang['SysMgr']['Homework']['NewRecord'] ?>
        			        </td>
        			     </tr>
                </table>
            </div>
            <div data-role="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
			   		<div id="div_content">
				    </div>
    			    <table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0"> 
    			        <? if($lhomework->useHomeworkType) { ?>
    						<tr>
    						   <td style="padding-left:10px" width="65px">
        			              <?=$i_Homework_HomeworkType?>
        			           </td>
        			           <td  colspan="2"  style="padding-right:10px">
        			              <?=$selectHomeworkType?>
        			           </td>
    						</tr>
						<? } ?>
    			    	
    			        <tr>
    			           <td style="padding-left:10px" width="65px">
    			              <?=$Lang['SysMgr']['Homework']['Topic']?>
    			           </td>
    			           <td  colspan="2"  style="padding-right:10px">
    			              <input type="text" name="title" MAXLENGTH="140">
    			           </td>
    			        </tr>
    			        <tr>
                           <td style="padding-left:10px" nowrap="nowrap">
                              <?=$Lang['SysMgr']['Homework']['StartDate']?>
                           </td>
					       <td colspan="2"  style="padding-right:10px">					    
					          <input type="text" data-role="date" id="startdate" name="startdate" value="<?=$pre_startdate?>">
					       </td>
                        </tr>
                        <tr>
                           <td style="padding-left:10px">
                              <?=$Lang['SysMgr']['Homework']["DueDate"]?>
                           </td>
					       <td colspan="2"  style="padding-right:10px">
					          <input type="text" data-role="date" id="duedate" name="duedate" value="<?=$pre_duedate?>">
					       </td>
                        </tr>
                        <tr>
                           <td style="padding-left:10px">
                              <?=$Lang['SysMgr']['Homework']['Workload']?>
                           </td>
					       <td width="100px">
					          <?=$lhomework->getSelectLoading("name=loading")?>
					       </td>					          
					       <td >
					          <?=$Lang['SysMgr']['Homework']['Hours']?>
					       </td>
                        </tr>                                            
                        <tr>	                      	
							<td style="padding-left:10px"><?=$Lang['SysMgr']['Homework']['HandinRequired']?></td>
							<td colspan="2" style="padding-bottom:10px"><input type="checkbox" name="handin_required" value=1 <?=($lhomework->DeafultHandinRequired? "checked":"")?>></td>						
						</tr>
						<? if($lhomework->useHomeworkCollect)
						{
							# check the checkbox default on or not ($sys_custom['eHomework']['CollectionRequiredDefaultOn'])
							$checked = $sys_custom['eHomework']['CollectionRequiredDefaultOn'] ? "checked" : "";
						?> 
    						<tr>
    							<td style="padding-left:10px" colspan="2"><?=$i_Homework_Collect_Required?></td>
    							<td style="padding-bottom:10px"><input type="checkbox" name="collect_required" value=1 <?=$checked ?>></td>
    						</tr>
						<? } ?>
						<tr>
							<td style="padding-left:10px"><?=$Lang['SysMgr']['Homework']['Description']?></td>
							<td colspan="2"  style="padding-right:10px"><?=$linterface->GET_TEXTAREA("description","");?></td>
						</tr>
						<tr>
							<td style="padding-left:10px"><?=$Lang['SysMgr']['Homework']['Attachment']?></td>
							<td colspan="2" >
                               <input type=file style="width:100%" class=file name="multiplefile[]" id="multiplefile[]" size=30 multiple>
							</td>
						</tr>
					</table>
                    <div id="div1"></div>
                    <input type="hidden" value=<?=$token?> name="token">
                    <input type="hidden" value=<?=$uid?> name="uid">
                    <input type="hidden" value=<?=$ul?> name="ul">
                    <input type="hidden" value=<?=$yearID?> name="yearID">
                    <input type="hidden" value=<?=$yearTermID?> name="yearTermID">
                    <?=$show_list?>
            </div>
            <div data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">
               <a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$button_submit?></a>
            </div>
            </form>
	    </div>
	</body>
</html>

<?php
intranet_closedb();
?>