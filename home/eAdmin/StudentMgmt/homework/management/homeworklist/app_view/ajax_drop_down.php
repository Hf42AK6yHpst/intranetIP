<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply token checking
#           - added data checking
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";

switch (strtolower($parLang)) {
	case 'en':
		$intranet_hardcode_lang = 'en';
		break;
	default:
		$intranet_hardcode_lang = 'b5';
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$subjectID = IntegerSafe($subjectID);
$yearID = IntegerSafe($yearID);
$yearTermID = IntegerSafe($yearTermID);
$classID = IntegerSafe($classID);
### Handle SQL Injection + XSS [END]

$_SESSION['UserID'] = $UserID;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}

$lhomework = new libhomework2007();
$ldb = new libdb();

$userObj = new libuser($UserID);
$targetUserType = $userObj->RecordType;
$targetIsTeaching = $userObj->teaching;
$UserRightTarget = new user_right_target();
if(empty($_SESSION["SSV_USER_ACCESS"])){	
	$_SESSION["SSV_USER_ACCESS"] = $UserRightTarget->Load_User_Right($UserID);
}

# Non-Teaching Staff Mode or is admin
 if(((($targetUserType==USERTYPE_STAFF) && !$targetIsTeaching) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"]))
 {
 	# class filter
 	$classes = $lhomework->getAllClassInfo();
 	//if($classID=="" && ($subjectID=="" || $subjectID=="-1") && $subjectGroupID=="") $classID = $classes[0][0];
 	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class(1);"', $classID, 0, 0, $i_general_all_classes);

 	$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
 	$subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0]:$subjectID;
	
 	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"js_change_class(2);\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);		
 	if($subjectID!=""){
 		$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
		if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
 // 		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID onChange=\"js_change_class();\"", $subjectGroupID, 1, 1);
 		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID[] id=subjectGroupID[] multiple size=10", $subjectGroupID, 1, 1);
 	}
	
 	if($sid!="" && $sid != $subjectID){
 		$subjectGroupID="";
 	}
 
 	$filterbar3 = $selectClass;
 	$filterbar = $selectedSubject;
 	$filterbar2 = $selectedSubjectGroup;

 }
# Student Mode with Subject Leader Allowed
// else if($targetUserType==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
// {
// 	echo "student";
// 	# Subject Menu
// 	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
// 	$subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0]:$subjectID;
	
// 	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"js_change_class(2);\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);
			
// 	if($subjectID!=""){
// 		$subjectGroups = $lhomework->getLeaderingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID);
// 		if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
// 		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID[] id=subjectGroupID[] multiple size=10", $subjectGroupID, 1,1);
// 	}
	
// 	if($sid!="" && $sid != $subjectID){
// 		$subjectGroupID="";
// 	}
	
// 	$filterbar = $selectedSubject;
// 	$filterbar2 = $selectedSubjectGroup;
	
// }
# Teacher Mode
else if($targetUserType==USERTYPE_STAFF && $targetIsTeaching)
{
	$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
	$subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0]:$subjectID;
	
	if($sys_custom['eClassTeacherApp']['eHomeworkCanNewAllClassesHomeowrk'])
	{
	    $classes = $lhomework->getAllClassInfo();
	    $subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
	    $subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0]:$subjectID;
	    if($subjectID!=""){
			$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
			if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
		}
	}
	else
	{
		if($lhomework->ClassTeacherCanViewHomeworkOnly) {
			//debug_pr($_POST);
			//$subjectGroups = $lhomework->getSubjectGroupIDByTeacherID($UserID);
			/*
			for($a=0; $a<count($subjectGroups); $a++) {
				
				list($sgid, $sgname) = $subjectGroups[$a];
				$sql = "SELECT SubjectID, ".Get_Lang_Selection("SUB.CH_DES", "SUB.EN_DES")." AS SubjectName FROM ASSESSMENT_SUBJECT SUB INNER JOIN SUBJECT_TERM st ON (st.SubjectID=SUB.RecordID) WHERE st.SubjectGroupID='$sgid'";
				$ary = $lhomework->returnArray($sql);
				
				if(sizeof($ary)>0) {
					$subject[] = array($ary[0]['SubjectID'], $ary[0]['SubjectName']);	
				}	
				
				$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." as classname FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=ycu.UserID) INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID='".GetCurrentSemesterID()."') INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=st.SubjectGroupID AND stct.UserID='$UserID') GROUP BY yc.YearClassID";
				$classes = $lhomework->returnArray($sql);
				
			}
			*/
			//debug_pr($_POST);
			$classes = $lhomework->getClassesInvolvedBySubjectGroupTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
	
			$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
			//debug_pr($_POST);
			$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID, $classID,1);
			
		} else {
			$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
			$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
			//debug_pr($subject);
			# check is the subjectid value pass from form page is in the subject list (change class)
			$in=0;
			foreach($subject as $k=>$d) {
				$in = ($subject[$k][0] == $subjectID) ? 1 : $in;
			}
			$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID, $classID,1);
		}	
	}
    
	# class select menu
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class(1);"', $classID, 0, 0, "-- ".$i_alert_pleaseselect."--");
	# subject select menu
	//$subjectID = (!$in)? $subject[0][0]:$subjectID;
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"js_change_class(2);\"", $subject, $subjectID, "-- ".$Lang['SysMgr']['Homework']['AllSubjects']." --", false);
		
	# subject group select menu	
	//echo $subjectID;
	if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
	$selectedSubjectGroup = $lhomework->getCurrentTeachingGroups("name=subjectGroupID[] id=subjectGroupID[] multiple size=10", $subjectGroups, $subjectGroupID, "-- ".$i_alert_pleaseselect." ".$Lang['SysMgr']['Homework']['SubjectGroup']." --", false);
	
	
	$filterbar = $selectedSubject;
	$filterbar2 = $selectedSubjectGroup;
	$filterbar3 = $selectClass;
 }
if($filterbar2=="") $filterbar2 = "<select name=\"subjectGroupID[]\" id=\"subjectGroupID[]\" multiple size=10></select>";

$x = '<table class="form_table" width="100%" border="0" cellpadding="0" cellspacing="0">';
if($targetUserType!=USERTYPE_STUDENT || ($targetUserType==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
$x .= '
<tr>
	<td style="padding-left:15px" width="80px">'. $i_general_class .'</td><td>'. $filterbar3 .'</td>
</tr>
';
}
$x .= '
<tr>
	<td style="padding-left:15px">'. $Lang['SysMgr']['Homework']['Subject'] .'</td><td>'. $filterbar .'</td>
</tr>

<tr>
	<td style="padding-left:15px">'. $Lang['SysMgr']['Homework']['SubjectGroup'] .'</td><td>'. $filterbar2 .'</td>
</tr>
</table>
';

intranet_closedb();
echo $x;
?>