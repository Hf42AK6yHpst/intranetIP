<?php
# using: 

##################################################################
## - 2019-05-08 Bill
##   prevent SQL Injection
## - 2017-06-30 Carlos
##   $sys_custom['LivingHomeopathy'] - added display columns [Handed In Homework], [Marking] and [Rating].
## - 2016-08-09 Bill
##   Fixed: no access right problem
## - 2009-12-01 Yat Woon
##   add handin status 5 for "Under Processing"
##################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

//if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$lu = new libuser($UserID);
/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus();
/******************************/
$CurrentPageArr['eAdminHomework'] = 1;

$hid = IntegerSafe($hid);
$record= $lhomework->returnRecord($hid);
list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath, $handinRequired, $collectRequired)=$record; 

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

if($lhomework->useHomeworkType) {
	$typeInfo = $lhomework->getHomeworkType($TypeID);
	$typeName = ($typeInfo[0]['TypeName']) ? $typeInfo[0]['TypeName'] : "---";
}

/*
if (!$lu->isTeacherStaff())
{
     header("Location: /close.php");
     exit();
}
*/

$data = $lhomework->handinList($hid);

$handinCount = 0;
$totalCount = 0;

if($sys_custom['LivingHomeopathy']) {
	$handin_documents = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$hid,'StudentIDToRecord'=>1,'GetTeacherName'=>1));
}

if (sizeof($data)!=0)
{
	$totalCount = sizeof($data);
	
	$x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eHomeworktableborder'>";
	$x .= "<tr>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Class']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>$i_ClassNumber</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>$i_UserStudentName</td>";
	if($sys_custom['LivingHomeopathy']){
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['General']['Status2']."</td>";
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['eHomework']['HandedInHomework']."</td>";
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['eHomework']['Mark']."</td>";
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['eHomework']['Rating']."</td>";
	}
	else{
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>$i_Homework_handin_current_status</td>";
	}
	$x .= "</tr>";
	
	// $notSubmitStr = date("Y-m-d") <= $due ? $Lang['SysMgr']['Homework']['NotSubmitted'] : $Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'];
	if (date("Y-m-d") <= $due) {
		$notSubmitStr = $Lang['SysMgr']['Homework']['NotSubmitted'];
	}
	else {
		$notSubmitStr = Get_Lang_Selection($handInStatusArr["-1"]["HandInStatusTitleB5"], $handInStatusArr["-1"]["HandInStatusTitleEN"]);
	}
	
	for ($i=0; $i<sizeof($data); $i++)
	{
	    list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$topic, $due,$SuppRecordStatus,$studentStatus,$LeaveStudent) = $data[$i];

	    if (!(empty($handinID) && $studentStatus == '3')) {
	        $str_status = "";
    		$css = "";
    		$handinStatus = $handinStatus==""? -1 : $handinStatus;
    		if (in_array($SuppRecordStatus, array(7,-11)))
    		{
    		    $handinStatus = $SuppRecordStatus;
    		}
    		
    		if ($handinStatus != -1) {
    			$str_status = Get_Lang_Selection($handInStatusArr[$handinStatus]["HandInStatusTitleB5"], $handInStatusArr[$handinStatus]["HandInStatusTitleEN"]);
    		}
    		else {
    			$str_status = $notSubmitStr;
    		}
    		/*
    		switch($handinStatus){
    				case 1 : $str_status=$Lang['SysMgr']['Homework']['Submitted']; $handinCount++;break;
    				case -1: $str_status=$notSubmitStr; break;
    				case 2 : $str_status=$Lang['SysMgr']['Homework']['LateSubmitted']; $handinCount++; break;
    				//case 3 : $str_status=$Lang['SysMgr']['Homework']['Redo']; break;
    				case 4 : $str_status=$Lang['SysMgr']['Homework']['NoNeedSubmit']; break;
    				case 5 : $str_status=$Lang['SysMgr']['Homework']['UnderProcessing']; break;
    				case 6 : $str_status=$Lang['SysMgr']['Homework']['Supplementary']; $handinCount++; break;
    				default: $str_status=$Lang['SysMgr']['Homework']['Submitted'];
    		}
    		*/
    		
    		$className = ($className=="")? "--" : $className;
    		$classNumber = ($classNumber=="")? "--" : $classNumber;
    		$studentName = ($studentName=="")? "--" : $studentName;
    		$str_status = ($str_status=="")? "--" : $str_status;
    		
    		$x .= "<tr>";
    		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
    		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$className</td>";
    		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$classNumber</td>";
    		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$studentName</td>";
    		$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$str_status</td>";
    		if($sys_custom['LivingHomeopathy']){
    			if(isset($handin_documents[$uid]) && $handin_documents[$uid]['StudentDocument']!=''){
    				$student_document_name = substr($handin_documents[$uid]['StudentDocument'],strrpos($handin_documents[$uid]['StudentDocument'],'/')+1);
    				$student_document_display = intranet_htmlspecialchars($student_document_name);
    				$student_document_display.= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_documents[$uid]['StudentDocumentUploadTime'],$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
    			}
    			else{
    				$student_document_display = $Lang['eHomework']['NotSubmitted'];
    			}
    			$teacher_document_display = '';
    			if(isset($handin_documents[$uid]) && $handin_documents[$uid]['TeacherDocument']!='')
    			{
    				$teacher_document_name = substr($handin_documents[$uid]['TeacherDocument'],strrpos($handin_documents[$uid]['TeacherDocument'],'/')+1);
    				$teacher_document_display .= intranet_htmlspecialchars($teacher_document_name);
    				$teacher_document_display .= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_documents[$uid]['TeacherDocumentUploadTime'],str_replace('<!--NAME-->',$handin_documents[$uid]['TeacherDocumentUploaderName'],$Lang['eHomework']['TeacherDocumentSubmittedRemark'])).')</div>';
    			}
    			$score_input_text = '';
    			if(isset($handin_documents[$uid]) && $handin_documents[$uid]['TextScore']!=''){
    				$score_input_text = $handin_documents[$uid]['TextScore'];
    			}
    			$score_input_display = '';
    			if(isset($handin_documents[$uid]) && $handin_documents[$uid]['StudentDocument']!='')
    			{
    				$score_input_display.= $score_input_text;
    				$score_input_display.= '<div class="tabletextremark">';
    				if($handin_documents[$uid]['TextScoreUpdaterName'] != '' && trim($score_input_text)!=''){
    					$score_input_display.= '('.str_replace('<!--DATETIME-->',$handin_documents[$uid]['TextScoreUpdateTime'],str_replace('<!--NAME-->',$handin_documents[$uid]['TextScoreUpdaterName'],$Lang['eHomework']['RatedRemark'])).')';
    				}
    				$score_input_display.= '</div>';
    			}
    			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$student_document_display."</td>";
    			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$teacher_document_display."</td>";
    			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$score_input_display."</td>";
    		}		 
    		$x .= "</tr>\n";
	    }
	}
	$x .= "</table>\n";
	
	if($confirmUserID!=""){
		$confirm_user_name_field = getNameFieldByLang();
		$temp = $lhomework->returnVector("SELECT $confirm_user_name_field FROM INTRANET_USER WHERE UserID='$confirmUserID'",1);
		$confirm_user_name = $temp[0];
		$last_modify = $confirmTime; 
		$last_modify_by = $confirm_user_name;
	}
	else{
		$last_modify ="--";
	}
}

/*else
{
	//$NoMsg = $i_Notice_NoStudent;
	$NoMsg = "This notice has not been issued to any students.";
	$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='1'>\n";
	$x .= "<tr>";
	$x .= "<td align='center' class='tabletext'>$NoMsg</td>";
	$x .= "</tr>\n";
	$x .= "</table>\n";
	
	$last_modify ="--";
}*/

?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<table width='100%' align='center' border=0>
	<tr>
    	<td align='center' class='eSportprinttitle'><b><?=$Lang['SysMgr']['Homework']['HomeworkHandinlist']?><br /></b></td>
	</tr>
</table>
<br/>
<table width="90%" border="0" cellpaddinh="3" cellspacing="0" align="center">
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Subject']?></span></td>
		<td class='tabletext'><?=$subjectName?></td>
	</tr>
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
		<td class='tabletext'><?=$subjectGroupName?></td>
	</tr>
	<? if($lhomework->useHomeworkType) { ?>
    	<tr>
    		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Homework_HomeworkType?></span></td>
    		<td class='tabletext'><?=$typeName?></td>
    	</tr>
	<? } ?>
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Topic']?></span></td>
		<td class='tabletext'><?=$title?></td>
	</tr>
	
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['StartDate']?></span></td>
		<td class='tabletext'><?=$start?></td>
	</tr>
	
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['DueDate']?></span></td>
		<td class='tabletext'><?=$due?></td>
	</tr>
	
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['Handin']."/".$Lang['SysMgr']['Homework']['Total']?></span></td>
		<td class='tabletext'><?="$handinCount/$totalCount"?></td>
	</tr>
	
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_LastModified?></span></td>
		<td class='tabletext'><?=$last_modify?></td>
	</tr>
	
	<tr>
		<td width='20%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$Lang['SysMgr']['Homework']['LastModifiedBy']?></span></td>
		<td class='tabletext'><?=$last_modify_by?></td>
	</tr>
</table>
<br/>
<br/>
<?=$x ?>

<?
echo '<br><table width="90%" border="0" cellpaddinh="3" cellspacing="0" align="center">';
echo "<tr>";
echo  "<td colspan=2>" . $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . "</td>";
echo "</tr>";
echo "</table>";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>