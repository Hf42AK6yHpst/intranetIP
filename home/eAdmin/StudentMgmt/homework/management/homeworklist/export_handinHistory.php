<?php
# using: 
##### Change Log [Start] #####
#
#   Date    :   2019-09-05 (Tommy)
#               fixed: non-teaching user is not allowed to export 
#
#	Date	:	2015-04-17 (Bill)
#				fixed: allow export history homework list of All Year Terms	[2015-0414-1116-51066] 
#
#	Date	:	2010-12-01 (Henry Chow)
#				change the export format to IP25 standard (with 2 rows of heading)
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

//if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



$lexport = new libexporttext();

$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', c.EN_DES, c.CH_DES) like '%$s%'" : "" ;

$ExportArr = array();

$allSemesters = getSemesters(Get_Current_Academic_Year_ID());
$handInStatusArr = $lhomework->getAllHandInStatus(TRUE);
$notShowStatus = array("Supplementary", "NotSubmittedRecord", "DueDateNotSubmitted", "SupplementarySubmitted", "SuppNotSubmitted", "DueDateAbsence", "ABSHomeworkReturn");

if($flag==0){	# current
	$yearTerm = ($yearTermID=="")? " AND (a.YearTermID=".GetCurrentSemesterID()." OR a.DueDate>=CURDATE())" : "AND (a.YearTermID = '$yearTermID' OR a.DueDate>=CURDATE())";
	$conds = "a.AcademicYearID = $yearID AND a.DueDate >= CURDATE() $yearTerm";
	//$filename = "Homework_List_ToDoList_AllSubjectGroups.csv";
	$filename = "Homework_List_ToDoList.csv";
}	
else{			# history
	// 2015-0414-1116-51066 - fixed: allow export homework list of All Year Terms
	// $yearTerm = ($yearTermID=="")? " AND a.YearTermID=".GetCurrentSemesterID() : "AND a.YearTermID = '$yearTermID'";
	$yearTerm = ($yearTermID=="")? "" : "AND a.YearTermID = '$yearTermID'";

	$conds = "a.AcademicYearID = $yearID AND a.DueDate < CURDATE() $yearTerm";
	
	# 20191211 Philips - Add Export period option and handinRequired
	if($startDate && $endDate){
		$conds .= " AND DATE_FORMAT(a.DueDate, '%Y-%m-%d') >= '$startDate' AND DATE_FORMAT(a.DueDate, '%Y-%m-%d') <= '$endDate' ";
	}
	if(isset($handinRequired)){
		$conds .= " AND a.HandinRequired = '" . ($handinRequired ? '1' : '0' ) . "' ";
	}
	//$filename = "Homework_List_History_AllSubjectGroups.csv";
	$filename = "Homework_List_History.csv";
}

/*
if($picID != ''){
	$conds .= " AND a.PosterUserID = $picID";
}
*/


if($_SESSION['UserType']==USERTYPE_STUDENT) {	# subject leader

	# subject leader of subject group
	$sql = "SELECT ClassID FROM INTRANET_SUBJECT_LEADER WHERE UserID='$UserID'";
	$sbjGp1 = $lhomework->returnVector($sql);
	
	# student in subject group
	$sql = "SELECT distinct t.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER u LEFT OUTER JOIN SUBJECT_TERM t ON (t.SubjectGroupID=u.SubjectGroupID) WHERE u.UserID='$UserID' AND t.YearTermID=".getCurrentSemesterID();
	$sbjGp2 = $lhomework->returnVector($sql);
	
	$sbjGpID = (sizeof($sbjGp1)>0 && sizeof($sbjGp2)>0) ? array_merge($sbjGp1, $sbjGp2) : ((sizeof($sbjGp1)==0) ? $sbjGp2 : $sbjGp1); 
	
	if(sizeof($sbjGpID)>0) {
		$conds .= " AND a.ClassGroupId IN (".implode(',',$sbjGpID).")";
	}


} else {	# teacher

	if($subjectID != ''){

		if ($subjectGroupID != '')
		{

			$conds .= " AND a.ClassGroupID = $subjectGroupID";
		}
	
		else{

			if($picID!='')
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($picID, $subjectID, $yearID, $yearTermID, $classID);
			
			else
				$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
//debug_pr($subjectGroups);
			if(sizeof($subjectGroups)!=0){

					$allGroups = " AND a.ClassGroupID IN (";
					for ($i=0; $i < sizeof($subjectGroups); $i++)
					{	
						list($groupID)=$subjectGroups[$i];
						$allGroups .= $groupID.",";
					}
					$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			} else if($noRecord==1) {
				$allGroups = " AND a.ClassGroupId IN ('') ";	
			} else{
				$allGroups ="";
			}
			
			$conds .= $allGroups;
		}

	} else {
		if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
			$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID, $classID);

			if(sizeof($subjectGroups)!=0){
				$delim = "";
				for($i=0; $i<sizeof($subjectGroups); $i++) {
					list($gid, $gname) = $subjectGroups[$i];
					
					$allGroups .= $delim.$gid;
					$delim = ",";
				}
				$conds .= " AND a.ClassGroupID IN ($allGroups)";
			}	
		}
			
	}
}



if ($s != '')
{
	$conds .= " AND (
				a.Title like '%$s%' 
				$searchByTeacher 
				$searchBySubject
			)";
}

# Select Data from DB
$row = $lhomework->exportHomeworkListRecord($conds, $classID, $showHomeworkID = true);

// Create data array for export
for($i=0; $i<sizeof($row); $i++){
	$subjectName = $row[$i][0];		//Subject Name
	$subjectGroupName = $row[$i][1];		//Subject Group Name
	$topic = $row[$i]['Title'];		//Topic
	$loading = $row[$i][3];		//Loading
	$teacher = $row[$i]['Name'];		//PIC Name
	$start = $row[$i]['StartDate'];		//Start Date
	$dueDate = $row[$i]['DueDate'];		//Due Date	
	$hid = $row[$i]['HomeworkID'];
	$data = $lhomework->handinList($hid);
	if (date("Y-m-d") <= $dueDate) {
		$notSubmitStr = $Lang['SysMgr']['Homework']['NotSubmitted'];
	}
	else {
		$notSubmitStr = Get_Lang_Selection($handInStatusArr["-1"]["HandInStatusTitleB5"], $handInStatusArr["-1"]["HandInStatusTitleEN"]);
	}
	
	$lt = new libuser('',$teacher);
	$tName = $lt->UserName(false);
	### Homework List History V2 START
	foreach($data as $studentHandin){
		// 0 - Subject 1 - Subject Group Name 2 - teacher 3 - startDate 4 - DueDate 5 - Class 6 - Class No 7 - Student Name 8 - Homework Status
		list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title, $due, $SuppRecordStatus, $studentStatus,$LeaveStudent) = $studentHandin;
		$handinStatus = $handinStatus=="" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;
		$lu = new libuser($uid);
		$sName = $lu->UserName(false);
		if (in_array($SuppRecordStatus, array(7,-11)) && !$allowToSubmit)
		{
			$handinStatus = $SuppRecordStatus;
		}
		if ($handinStatus != -1) {
			$str_status = Get_Lang_Selection($handInStatusArr[$handinStatus]["HandInStatusTitleB5"], $handInStatusArr[$handinStatus]["HandInStatusTitleEN"]);
		}
		else {
			$str_status = $notSubmitStr;
		}
		$studentRow = array();
		$studentRow[] = $subjectName;
		$studentRow[] = $subjectGroupName;
		$studentRow[] = $topic;
		$studentRow[] = $tName;
		$studentRow[] = $start;
		$studentRow[] = $dueDate;
		$studentRow[] = $className;
		$studentRow[] = $classNumber;
		$studentRow[] = $sName;
		$studentRow[] = $str_status;
		$ExportArr[] = $studentRow;
	}
// 	debug_pr($row);die();
	### Homework List History V2 END
	$m = 0;
	/*
	$ExportArr[$i][$m] = $row[$i]['CODEID'];	//Subject Code
	$m++;
	$ExportArr[$i][$m] = $row[$i][0];		//Subject Name
	$m++;
	$ExportArr[$i][$m] = $row[$i]['ClassCode'];	//Subject Group Code
	$m++;
	$ExportArr[$i][$m] = $row[$i][1];		//Subject Group Name
	$m++;

	$ExportArr[$i][$m] = $row[$i][2];		//Topic
	$m++;
	$ExportArr[$i][$m] = $row[$i][3];		//Loading			
	$m++;
	$ExportArr[$i][$m] = $row[$i]['Name'];		//PIC Name
	$m++;
	$ExportArr[$i][$m] = $row[$i][4];		//Start Date		
	$m++;
	$ExportArr[$i][$m] = $row[$i][5];		//Due Date			
	$m++;
	//$ExportArr[$i][$m] = html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $row[$i][6]), ENT_QUOTES); 	 
	$ExportArr[$i][$m] = $row[$i][6];		//Description
	$m++;
	$ExportArr[$i][$m] = ($row[$i][7]=="1")? "Yes" : "No";		//Handin Required			
	//$ExportArr[$i][$m] = $row[$i]['HandinRequired'];
	$m++;
	*/
	/*
	if($lhomework->useHomeworkCollect)
	{
		$ExportArr[$i][$m] = ($row[$i]['CollectRequired']=="1")? "Yes" : "No";		//Collect  Required			
		//$ExportArr[$i][$m] = ($row[$i]['CollectRequired']==1) ? $i_general_yes : $i_general_no;
		$m++;
	}
	if($lhomework->useHomeworkType) {		//Homework Type
		
		$homeworkTypeInfo = $lhomework->getHomeworkType($row[$i][9]);
		$ExportArr[$i][$m] = ($homeworkTypeInfo[0]['TypeName']) ? $homeworkTypeInfo[0]['TypeName'] : "---";
				
		$ExportArr[$i][$m] = ($row[$i]['TypeID']=="0") ? "" : $row[$i]['TypeID'];
		$m++;
	}	
	*/
}

if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}

/*
$exportColumn[] = $Lang['SysMgr']['Homework']['Subject'];
$exportColumn[] = $Lang['SysMgr']['Homework']['SubjectGroup'];
if($lhomework->useHomeworkType)
	$exportColumn[] = $i_Homework_HomeworkType;	
$exportColumn[] = $Lang['SysMgr']['Homework']['Topic'];
$exportColumn[] = $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")";
$exportColumn[] = $Lang['SysMgr']['Homework']['StartDate'];
$exportColumn[] = $Lang['SysMgr']['Homework']['DueDate'];
$exportColumn[] = $Lang['SysMgr']['Homework']['Description'];
$exportColumn[] = $Lang['SysMgr']['Homework']['HandinRequired'];	
$exportColumn[] = $i_general_Teacher;	

if($lhomework->useHomeworkCollect)
	$exportColumn[] = $i_Homework_Collect_Required;

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
*/
# define column title (2 dimensions array, 1st row is english, 2nd row is chinese)
//$exportColumn[0] = array($Lang['formClassMapping']['ExportClassStudent_Column'][0][0], $Lang['formClassMapping']['ExportClassStudent_Column'][0][1], $Lang['formClassMapping']['ExportClassStudent_Column'][0][2], $Lang['formClassMapping']['ExportClassStudent_Column'][0][3]);
//$exportColumn[1] = array($Lang['formClassMapping']['ExportClassStudent_Column'][1][0], $Lang['formClassMapping']['ExportClassStudent_Column'][1][1], $Lang['formClassMapping']['ExportClassStudent_Column'][1][2], $Lang['formClassMapping']['ExportClassStudent_Column'][1][3]);

$i = 0;	
foreach($Lang['Homework']['HandinHistoryExportColumn']['EN'] as $col) {
	$exportColumn[0][] = $col;
}

$i = 0;
foreach($Lang['Homework']['HandinHistoryExportColumn']['CH'] as $col) {
	$exportColumn[1][] = $col;	
}
//debug_pr($exportColumn);
/*
$exportColumn[0] = array("","","","","","","","","","","","","");
$exportColumn[1] = array("","","","","","","","","","","","","");	
	*/
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");


intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
