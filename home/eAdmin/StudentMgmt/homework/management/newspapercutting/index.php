<?php
// Modifing by : 

##### Change Log [Start] ######

###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$lclass = new libclass();

# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

$pageSizeChangeEnabled = true;

# Table initialization
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$li = new libdbtable2007($field, $order, $pageNo);

# Settings
$s = addcslashes($s, '_');
$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;

$today = date('Y-m-d');
$subjectID = ($subjectID=="" || $subjectID=="-1")? -1 :$subjectID;

//$today = "2011-02-10";

if($subjectGroupID && $subjectID==-1)
{
	$TempSubjectInfo = $lhomework->RetrieveSubjectbySubjectGroupID($subjectGroupID);
	$subjectID = $TempSubjectInfo[0]['SubjectID'];
}
	
$yearID = Get_Current_Academic_Year_ID();


# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID ? $yearTermID : 0;

# Filter - Teacher List
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
	if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID)) {		# eHomework Admin
		$classes = $lhomework->getAllClassInfo();
	}
	else {		

		$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
	}
		
		//echo $_SESSION['isTeaching'];
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="reloadForm()"', $classID, 0, 0, $i_general_all_classes);
}

# Filter - Subject 
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) || $lhomework->isViewerGroupMember($UserID))
{
	$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"document.form1.subjectGroupID.selectedIndex=0; reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true, -1);
}
else	# Subject Leader
{
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
	$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
}


if($_SESSION['UserType']!=USERTYPE_STUDENT || 
	($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) || 
	$lhomework->isViewerGroupMember($UserID)
	)
{
	# Filter - Subject Groups
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID, $classID);
	$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
} else {	# subject group of subject leader
	$subjectGroups = $lhomework->getStudyingSubjectGroupListByLeader($UserID, $yearID, $yearTermID);
	//debug_pr($subjectGroup);
}
//debug_pr($subjectGroups);

# Filter display
$filterbar = "$selectClass $selectedSubject $selectedSubjectGroups";

# subject groups of other semesters of selected year
$allSemesters = getSemesters($yearID);
$allSubjectGroupsOfYear = array();
if(sizeof($allSemesters)>0) {
	foreach($allSemesters as $termId=>$termName) {
		$temp = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $termId, $classID);
		//debug_pr($temp);
		if(sizeof($temp)>0) {
			for($i=0; $i<sizeof($temp); $i++) {
				if(!in_array($temp[$i][0], $allSubjectGroupsOfYear))
					$allSubjectGroupsOfYear[] = $temp[$i][0];	
			}	
		}
	}
}
//debug_pr($allSubjectGroupsOfYear);
$sgid = $subjectGroupID;

if($sid!="" && $sid != $subjectID){
	$subjectGroupID="";
}




# SQL statement

if(sizeof($subjectGroups)!=0)
{
	//echo "###";
	if($sgid!="") {
		$allGroups = " a.ClassGroupID = '$sgid' AND ";	
	} else {
		
		if($_SESSION['UserType']==USERTYPE_STAFF) {
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($allSubjectGroupsOfYear); $i++)
			{
				$groupID = $allSubjectGroupsOfYear[$i];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .=" AND";
		} else {	# subject leader
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($subjectGroups); $i++)
			{
				$groupID = $subjectGroups[$i][0];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .=" AND";
		}
	}
	
} else if(sizeof($classes)==0 && sizeof($subject)==0) {
	$allGroups = " a.ClassGroupID IN ('') AND ";	
	$noRecord = 1;
} else{		# class teacher only (not subject group teacher & class student not in any subject group)
	
		$allGroups ="";
		$subjectGroupID ="";
		$conds .= " a.ClassGroupID IN ('') AND ";
	
}

	# set conditions
	$date_conds = "AND a.DueDate >= CURDATE()";
	$conds .= ($subjectGroupID=='')? "$allGroups" : " a.ClassGroupID = $subjectGroupID AND";
	$conds .= ($s=='')? "": "(IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
								or a.Title like '%$s%'
								$searchByTeacher
								$searchBySubject
							) AND";
    $conds .= " Type=2 AND";//type = 2 for newspaper cutting
	$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";

	$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
			   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,";

	$fields .= " CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc))),
			   a.StartDate, a.DueDate,
				concat('<a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goSubmissionDetailsPage(', a.HomeworkID, ', 1);\">', IFNULL((Select count(iawa.ArticleID) From INTRANET_APP_WEEKLYDIARY_ARTICLE as iawa Where iawa.HomeworkID = a.HomeworkID Group By iawa.HomeworkID), '0'), '</a> / <a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goSubmissionDetailsPage(', a.HomeworkID, ', 0);\">', count(d.UserID), '</a>')";
	$fields .= ", a.LastModified";

	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || !$lhomework->OnlyCanEditDeleteOwn || $lhomework->isViewerGroupMember($UserID)) {
		$fields .= ", CONCAT('<input type=checkbox name=HomeworkID[] id=HomeworkID value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";
	} else if($_SESSION['UserType']==USERTYPE_STAFF) {
		$fields .= ", IF(('".($lhomework->ClassTeacherCanViewHomeworkOnly)."'=0 OR '".($lhomework->ClassTeacherCanViewHomeworkOnly)."'='' OR a.PosterUserID='$UserID' OR a.CreatedBy='$UserID' OR sl.UserID!='' OR stct.SubjectGroupID IS NOT NULL),CONCAT('<input type=checkbox name=HomeworkID[] id=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">'),'-')";	
	} else {	# student account (check subject leader)
		$fields .= ", IF((a.PosterUserID='$UserID' OR a.CreatedBy='$UserID'),CONCAT('<input type=checkbox name=HomeworkID[] id=HomeworkID value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">'),'')";
	}
	
	$dbtables = "INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
				LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
				";

	$conds .= " (a.YearTermID = $yearTermID OR a.DueDate>=CURDATE()) AND";
	
	if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
	{
		$conds .= " d.UserID = $UserID AND a.AcademicYearID = $yearID $date_conds";
		$conds .= ($subjectID!="" && $subjectID!=-1) ? " AND a.SubjectID=$subjectID" : "";
	}
	else
	{
		# do not map by user, should allow for same subject group
		$conds .= " a.AcademicYearID = $yearID $date_conds";
	}	
	
	
	
	if($classID!="") {
		$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)
						LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";	
		$conds .= " AND ycu.YearClassID='$classID'";
	}
	
	$dbtables .= " LEFT OUTER JOIN INTRANET_SUBJECT_LEADER sl ON ((sl.UserID=a.PosterUserID OR sl.UserID=a.CreatedBy) and sl.ClassID=a.ClassGroupID)";
	$fields .= ", sl.UserID";
	
	$sql = "SELECT $fields FROM $dbtables LEFT JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID='$UserID') WHERE $conds GROUP BY a.HomeworkID";
//	debug_pr($sql);
//	die();
//	echo $sql;
	/*
	# Editable List 
	$sqlEditableList = "SELECT a.HomeworkID FROM $dbtables INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID='$UserID') WHERE $conds Group BY a.HomeworkID";
	$tempHW = $lhomework->returnVector($sqlEditableList);
	*/
	
	$li->field_array = array("Subject", "SubjectGroup");
	$li->field_array = array_merge($li->field_array, array("a.Title", "a.StartDate", "a.DueDate"));
	$li->sql = $sql;
	//echo $sql;
	
	$li->no_col = sizeof($li->field_array)+3;
		
	$li->field_array[] = "a.LastModified";	
	$li->no_col++;

	$li->IsColOff = "IP25_table";
	$li->count_mode = 1;

	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<th width='3%'>#</th>\n";
	$li->column_list .= "<th width='12%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['Subject'])."</th>\n";
	$li->column_list .= "<th width='15%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</th>\n";
	$li->column_list .= "<th class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['Topic'])."</th>\n";
	$li->column_list .= "<th width='12%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</th>\n";
	$li->column_list .= "<th width='12%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</th>\n";
	$li->column_list .= "<th width='10%' class='tabletop tabletopnolink'>".$Lang['SysMgr']['Homework']['Submitted'].' / '.$Lang['General']['Total']."</th>\n";
	$li->column_list .= "<th width='12%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['LastModified'])."</th>\n";
	$li->column_list .= "<th width='3%' class='tabletop tabletoplink'>".$li->check("HomeworkID[]")."</th>\n";

if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
# Toolbar: new, import, export
$toolbar = $linterface->GET_LNK_NEW("add.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$sgid", "", "", "", "",0);
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

# Start layout
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_NewspaperCutting";
$PAGE_TITLE = $Lang['SysMgr']['Homework']['NewspaperCutting'];
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();	
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);

$linterface->LAYOUT_START();
	
?>

<script language="javascript">
<?
/*
$tempAry = "var tempAry = new Array();\n";

if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
	
	for($i=0; $i<sizeof($tempHW); $i++) {
		$tempAry .= "tempAry[$i] = \"".$tempHW[$i]."\";\n";
	}
}
echo $tempAry;
*/
?>

$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		
		}
	});
});

function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
            if(confirm(alertConfirmRemove)){
            obj.action=page;
            obj.method="post";
            obj.submit();
            }
    }
}


function reloadForm() {
	document.form1.s.value = "";
	document.form1.pageNo.value = 1;
	document.form1.submit();
}


function goSearch() {
	document.form1.s.value = document.form2.text.value;
	document.form1.pageNo.value = 1;
	document.form1.submit();
}


function viewHmeworkDetail(id)
{
	newWindow('./view.php?hid='+id,1);
}


function viewHandinList(id)
{
	newWindow('./handin_list.php?hid='+id,10);
}

function doExport()
{
	if(document.getElementById("exportType1").checked) {
		self.location.href = "<?=$exportLink?>";
	} else {
		if(document.form2.exportDate.value=="") {
			alert("<?=$i_alert_pleasefillin." ".$Lang['eHomework']['ExportDate']?>");
			document.form2.exportDate.focus();
		} else if(!check_date(document.form2.exportDate, "<?=$Lang['General']['InvalidDateFormat']?>!")) { 
			return false;
		} else {
			document.form1.form1ExportDate.value = document.form2.exportDate.value;
			document.form1.action = "export_collection_list.php";
			document.form1.submit();
			document.form1.action = "";
		}
	}
}

function checkEditable(flag) {
	var success = 1;
	/*
	<?if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {?>
	var hwLen = document.getElementById('HomeworkID[]').length;
	
	for(var i=0; i<hwLen; i++) {
		var thisValue = document.form1.HomeworkID[i].value;
		var thisChk = document.form1.HomeworkID[i].checked;
		
		if(thisChk && jQuery.inArray(thisValue, tempAry)==-1) {
			alert("");
			success = 0;
			return;
		}
	}
	<?}?>
	*/
	if(success) {
		if(flag=='edit')
			checkEdit(document.form1,'HomeworkID[]', 'edit.php');
		else
			removeCat(document.form1,'HomeworkID[]', 'remove_update.php?subjectID=<?=$subjectID?>&subjectGroupID=<?=$subjectGroupID?>');
	}
}

function goSubmissionDetailsPage(parHwId, parFilter) {
	window.location = '../weeklydiary/submission_list.php?HomeworkID=' + parHwId + '&submissionStatus=' + parFilter + '&fromPage=newsCutIndex';
}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="400">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right">
						
						<?=$htmlAry['searchBox']?>
						
					<!--	<input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>"> 
						<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?> -->
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="get" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
										</tr>
									</table>
									<div style="clear:both" /></div>
									<div class="table_board">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<? 
												//if ((!$lhomework->ViewOnly && ($_SESSION['UserType']==USERTYPE_STAFF || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))) || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])) {
												if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] 
													|| !$lhomework->ViewOnly 
													|| $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] 
													|| ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
												) {
												?>
											
													<div class="common_table_tool"> <a title="Edit" class="tool_edit" href="javascript:void(0);" onclick="javascript:checkEditable('edit');" /><?=$button_edit?></a>
													<a title="<?=$button_delete?>" class="tool_delete" href="javascript:void(0);" onclick="javascript:checkEditable('delete')" /><?=$button_delete?></a>
													</div>
															
												<? 
												} else{?>
													<br/>
													<br/>
												<? } ?>
											</td>
										</tr>
									</table>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?
						
						echo $li->display();
						
						/*
						if($display){
							echo $li->display();
						}
						else{
							if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
								echo $Lang['SysMgr']['Homework']['HomeworkListWarningSubjectLeader'];
							else
								echo $Lang['SysMgr']['Homework']['HomeworkListWarning'];
						}
						*/
						?>
					</td>

				</tr>
			</table><br>
			<input type="hidden" name="pageNo" name="pageNo" value="<?=$li->pageNo; ?>"/>
			<input type="hidden" name="order" id="order" value="<?=$li->order; ?>"/>
			<input type="hidden" name="field" id="field" value="<?=$li->field; ?>"/>
			<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
			<input type="hidden" name="s" id="s" value="<?=$s?>"/>
			<input type="hidden" name="sid" id="sid" value="<?=$subjectID?>"/>
			<input type="hidden" name="form1ExportDate" id="form1ExportDate" value=""/>
			<input type="hidden" name="form1ExportType" id="form1ExportType" value="<?=$exportType?>"/>
			

			
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>