<?php

// Modifing by : 

/********************** Change Log ***********************
#************************************************************************
*/


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// 2015-03-11 added
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$lhomework = new libhomework2007();
// 2015-03-11 added
$scm = new subject_class_mapping();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html("popup.html");

$CurrentPageArr['eAdminHomework'] = 1;

$MODULE_OBJ['title'] = $Lang['SysMgr']['Homework']['NewspaperCutting'];

$sql = "select HomeworkID,ClassGroupID,SubjectID,StartDate,DueDate,Title,Description,LastModified,PosterName,PosterUserID,CreatedBy from INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING where HomeworkID = '$hid'";
$record = $libdb->returnArray($sql);

list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $title, $description, $lastModified, $PosterName, $PostUserID, $CreatedBy)=$record[0]; 

$lu = new libuser($PostUserID);
$PosterName2 = $lu->UserName();

$lu2 = new libuser($CreatedBy);
$CreatedByName = $lu2->UserName();

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

// 2015-03-11 - Get Subject Teacher Name
$subjectTeacherName = "";
$subjectTeacherList = $scm->Get_Subject_Group_Teacher_Info((array)$subjectGroupID);
if(count($subjectTeacherList) > 0){
	$delim = '';
	foreach($subjectTeacherList as $subjectTeacher){
		$lu3 = new libuser($subjectTeacher['UserID']);
		$teacherName = $lu3->UserName();
		$subjectTeacherName .= $teacherName? $delim.$teacherName : "--";
		$delim = ', ';
		unset($lu3);
	}
}

$description = nl2br($lhomework->convertAllLinks($description,40));

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>
function click_print()
{
	with(document.form1)
    {
        submit();
	}
}

</SCRIPT>


<br />   
<form name="form1" action="print_preview.php" method="post" target = "_blank">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<!-- subject-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?></td>
									<td width="567" align="left" valign="top">
										<?=$subjectName?>
									</td>
								</tr>
								
								<!-- subject Group-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></td>
									<td width="567" align="left" valign="top">
										<?=$subjectGroupName?>
									</td>
								</tr>

								<!-- topic -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Topic']?></td>
									<td width="567" align="left" valign="top">
									  <?=$title?>
									</td>
								</tr>
								
								<!-- description -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?></td>
									<td width="567" align="left" valign="top">
										<?php
											if($description!="")
												echo $description;
											else
												echo "--";
										?>
									</td>
								</tr>

								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['StartDate']?></td>
									<td width="567" align="left" valign="top">
										<?=$start?>
									</td>
								</tr>

								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']["DueDate"]?></td>
									<td width="567" align="left" valign="top">
										<?=$due?>
									</td>
								</tr>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['General']['LastModifiedBy']?></td>
									<td width="567" align="left" valign="top">
										<?=($PosterName2==""? "--":$PosterName2)?>
									</td>
								</tr>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['CreatedBy']?></td>
									<td width="567" align="left" valign="top">
										<?=($CreatedBy ? $CreatedByName : $PosterName2)?>
									</td>
								</tr>
								
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroupTeacher']?></td>
									<td width="567" align="left" valign="top">
										<?=($subjectTeacherName ? $subjectTeacherName : "--")?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
														
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
						</td>
					</tr>
				</table>										
							
			</td>
		</tr>
	</table>
	<input type="hidden" name="hid" value="<?=$hid?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>