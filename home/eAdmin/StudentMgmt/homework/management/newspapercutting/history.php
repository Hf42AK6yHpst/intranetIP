<?php
// Modifing by : 
	
##### Change Log [Start] ######
#
###### Change Log [End] #######

	$PATH_WRT_ROOT = "../../../../../../";

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

	intranet_auth();
	intranet_opendb();
	
	# Create a new homework instance
	$lhomework = new libhomework2007();
	
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
	{
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}


	# Temp Assign memory of this page
	ini_set("memory_limit", "150M");

	# Create a new interface instance
	$linterface = new interface_html();



	$CurrentPageArr['eAdminHomework'] = 1;

	# Select current page
	$CurrentPage = "Management_NewspaperCutting";

	# Page title
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['NewspaperCutting'];

	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

	# Select sort field
	$sortField = 0;

	# change page size
	if ($page_size_change == 1)
	{
		setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		$ck_page_size = $numPerPage;
	}

	if (isset($ck_page_size) && $ck_page_size != "")
		$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;
	
	function resetSubjectGroupID($subjectGroupID, $subjectGroups)
	{
		for ($i=0; $i<sizeof($subjectGroups); $i++)
		{
			if ($subjectGroupID==$subjectGroups[$i][0])
			{
				return $subjectGroupID;
			}
		}
		return "";
	}

	# Table initialization
	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);

	# Settings
	$s = addcslashes($s, '_');
	$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
	$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;
	$allowExport = $lhomework->exportAllowed;
	
	# class menu
	if($_SESSION['UserType']!=USERTYPE_STUDENT) {
		// [2015-1117-1137-20073] Viewer Group member should able to select all classes
		if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID))		# eHomework Admin
			$classes = $lhomework->getAllClassInfo();
		else														# Class teacher / subject teacher
			$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
			
		$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $i_general_all_classes);
	}
	
	// [2015-0901-1009-21066] fixed: drop down list filtering not work, use data from $_GET[] only if $_POST[] is not set
	$yearID = isset($_POST['yearID'])? $_POST['yearID'] : $_GET['yearID'];
	$yearTermID = isset($_POST['yearTermID'])? $_POST['yearTermID'] : $_GET['yearTermID'];
	$subjectID = isset($_POST['subjectID'])? $_POST['subjectID'] : $_GET['subjectID'];
	$subjectGroupID = isset($_POST['subjectGroupID'])? $_POST['subjectGroupID'] : $_GET['subjectGroupID'];
	$sid = isset($_POST['sid'])? $_POST['sid'] : $_GET['sid'];

	# Teacher Mode with Teaching
	if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'])
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
		//$yearTerm 

		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){
			
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}

			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;
			# Subject Menu
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
			$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);		
			if($subjectID != "-1"){
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,$subjectID, $yearID, $yearTermID, $classID);
			}
			else
			{
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,"", $yearID, $yearTermID, $classID);
			}
			
			$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);

			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);
			if ($yearTermID!="" && $yearTermID>0)
			{
				# only display subject group for particular term		
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
			} else
			{
				$subjectGroupID = "";
			}
			
			$filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject $selectedSubjectGroups";
		}
		# Export link
		$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&picID=$UserID&s=$s&flag=1";
		/*
		if($allowExport)
			$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);
		*/
		
		# SQL statement
		if(sizeof($subjectGroups)!=0){
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($subjectGroups); $i++)
			{
				list($groupID)=$subjectGroups[$i];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .=" AND";
			
		} else if(sizeof($classes)==0 && sizeof($subject)==0) {
			$allGroups = " a.ClassGroupID IN ('') AND ";
			$noRecord = 1;
		} else{
			$allGroups ="";
			$subjectGroupID ="";

			$sbjGps = $lhomework->getSubjectGroupIDByTeacherID($UserID, $yearID);
			if(sizeof($sbjGps)>0) {
				for($i=0; $i<sizeof($sbjGps); $i++) {
					$temp[$i] = $sbjGps[$i][0];
				}
				$allGroups .= " a.ClassGroupID IN (".implode(',',$temp).") AND ";
			}
			
		}
		
		# cater with normal teacher without any subject group
		if(sizeof($subjectGroups)==0 && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
		{
			$subjectGroupID = -1;
		}
		
		# set conditions
		$date_conds = "AND a.DueDate < CURDATE()";
		$conds = ($subjectGroupID=='' || $subjectGroupID==-1)? "$allGroups" : " a.ClassGroupID = $subjectGroupID AND";
		
		$conds .= ($s=='')? "": "(IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
									or a.Title like '%$s%'
									$searchByTeacher
									$searchBySubject
								) AND";
        $conds .= " Type=2 AND";//type = 2 for newspaper cutting

		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";

		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,";
		   
		$fields .= " CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc))),
				   a.StartDate, a.DueDate,
					concat('<a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goSubmissionDetailsPage(', a.HomeworkID, ', 1);\">', IFNULL((Select count(iawa.ArticleID) From INTRANET_APP_WEEKLYDIARY_ARTICLE as iawa Where iawa.HomeworkID = a.HomeworkID Group By iawa.HomeworkID), '0'), '</a> / <a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goSubmissionDetailsPage(', a.HomeworkID, ', 0);\">', count(F.UserID), '</a>')";

		if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
			$fields .= ", CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";
		} else {
			$fields .= ", IF('".($lhomework->ClassTeacherCanViewHomeworkOnly)."'=0 OR '".($lhomework->ClassTeacherCanViewHomeworkOnly)."'='' OR stct.SubjectGroupID IS NOT NULL, CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">'),'-')";
		}
		
		$dbtables = "INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
					LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)";
					
		# do not map by user, should allow for same subject group
		$conds .= " a.AcademicYearID = $yearID $date_conds";
		$conds .=($yearTermID=='')? "" : " AND a.YearTermID =$yearTermID";
					
		if($classID!="") {
			$dbtables .= " LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";	
			$conds .= " AND ycu.YearClassID='$classID'";
		}

		$sql = "SELECT $fields FROM $dbtables LEFT JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID='$UserID') WHERE $conds GROUP BY a.HomeworkID";
		//echo $sql;
		$li->field_array = array("Subject", "SubjectGroup");
		if($lhomework->useHomeworkType) {
			$li->field_array[] = "TypeName";	
		}
		$li->field_array = array_merge($li->field_array, array("a.Title", "a.StartDate", "a.DueDate"));
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+3;
//		if($lhomework->useHomeworkCollect)
//				$li->no_col++;
		$li->IsColOff = 2;
		$li->count_mode = 1;


		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		$li->column_list .= "<td lass='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletopnolink'>".$Lang['SysMgr']['Homework']['Submitted'].' / '.$Lang['General']['Total']."</td>\n";
		$li->column_list .= "<td width='5%' class='tabletop tabletopnolink'>".$li->check("HomeworkID[]")."</td>\n";
	}

	# Teacher Mode with Non Teaching
	else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;

		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){

			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}

			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;

			# Subject Menu
			$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);

			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
			
			
			
			
			
			if ($subjectID!="" && $yearTermID!="" && $yearTermID>0){

				$sgid = $subjectGroupID;
				if($sid != $subjectID){
					$subjectGroupID="";
				}

				# Subject Group Menu
				$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
				
				$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
			
			
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
			} else
			{
				$subjectGroupID = "";
			}

			$filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject $selectedSubjectGroups";
		}
		$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&s=$s&flag=1";
		/*
		if($allowExport)
			$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);
		*/
		
		# SQL statement
		if(sizeof($subjectGroups)!=0){
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($subjectGroups); $i++)
			{
				list($groupID)=$subjectGroups[$i];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .=" AND";
		}
		else{
			$allGroups ="";
			$subjectGroupID ="";
		}

		# SQL statement
		$date_conds = "a.DueDate < CURDATE()";
		$conds = ($subjectGroupID=='' || $subjectGroupID==-1)? "$allGroups" : " a.ClassGroupID = $subjectGroupID AND";
		$conds .= ($s=='')? "": " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
									or a.Title like '%$s%'
									$searchByTeacher
									$searchBySubject
								) AND";
        $conds .= " Type=2 AND";//type = 2 for newspaper cutting

		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
				   CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc))),
				   a.StartDate, a.DueDate, CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";

		$dbtables = "INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID";

		$conds .= " a.AcademicYearID = $yearID AND $date_conds";
		$conds .=($yearTermID=='')? "" : "AND a.YearTermID =$yearTermID";

		$sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.HomeworkID";

		
		$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+2;
		$li->IsColOff = 2;

		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletopnolink'>".$li->check("HomeworkID[]")."</td>\n";

	}

	# Subject Leader
	else if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;

		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){

			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}

			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);

			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;

			# Subject Menu
			$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
			$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);

			$filterbar = "$selectedYear $selectedYearTerm $selectedSubject";
		}
		# SQL statement
		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			$allSubjects .= " AND";
		}
		else{
			$allSubjects ="";
		}
		
		# Set conditions
		$date_conds = "AND a.DueDate < CURDATE()";
		$conds = ($subjectID=='-1')? "$allSubjects" : " a.SubjectID = $subjectID AND";
		$conds .= ($s=='')? "": " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
									or a.Title like '%$s%'
									$searchByTeacher
									$searchBySubject
								) AND";
        $conds .= " Type=2 AND";//type = 2 for newspaper cutting
		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
				   CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc))),
				   a.StartDate, a.DueDate";

		$dbtables = "INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)";

		$conds .= " d.UserID = $UserID AND a.AcademicYearID = $yearID $date_conds";
		$conds .=($yearTermID=='')? "" : "AND a.YearTermID =$yearTermID";

		$sql = "SELECT $fields FROM $dbtables WHERE $conds";

		//echo $sql;
		$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->IsColOff = 2;


		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";

		$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&s=$s&flag=1";
	}

	
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);


$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

	# Start layout
	$linterface->LAYOUT_START();
?>

<script language="javascript">


$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		
		}
	});
});

	function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
	}


	function reloadForm() {
		document.form1.s.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}


	function goSearch() {
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}


	function viewHmeworkDetail(id)
	{
		   newWindow('./view.php?hid='+id,1);
	}


	function viewHandinList(id)
	{
		   newWindow('./handin_list.php?hid='+id,10);
	}
	
	function goSubmissionDetailsPage(parHwId, parFilter) {
		window.location = '../weeklydiary/submission_list.php?HomeworkID=' + parHwId + '&submissionStatus=' + parFilter + '&fromPage=newsCutHistory';
	}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right"> 
						<?= $htmlAry['searchBox']?>
					<!--	 <input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
							<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?> -->
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											</td>
											<td align="right" valign="bottom">
											<br/>
											<br/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="table-action-bar">
								<td>
								</td>
								<td align="right" valign="bottom">
									<? 
									
									if ((!$lhomework->ViewOnly && ($_SESSION['UserType']==USERTYPE_STAFF || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])) || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])) {
									?>
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
												<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td nowrap><a href="javascript:checkEdit(document.form1,'HomeworkID[]','edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
															<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
															<td nowrap><a href="javascript:removeCat(document.form1,'HomeworkID[]','remove_update.php?subjectID=<?=$subjectID?>&subjectGroupID=<?=$subjectGroupID?>')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
														</tr>
													</table>
												</td>
												<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
											</tr>
										</table>
									<? 
									} else{?>
										<br/>
										<br/>
									<? } ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$li->display()?>
					</td>
				</tr>
			</table><br>
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="s" value="<?=$s?>"/>
			<!--input type="hidden" name="cid" value="<?=$childrenID?>"/-->
			<input type="hidden" name="yid" value="<?=$yearID?>"/>
			<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
			<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			<input type="hidden" name="history" value="1"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>