<?php
/*
 * Change Log:
 * Date: 2019-09-24 Philips Added onSelectChapterInThickBox()
 * Date: 2018-10-15 Bill    Disable submit button > prevent any unwanted data issues    [2018-1015-1724-00096]
 * Date: 2018-05-02 Bill    Display Warning Box after updated Zone Settings
 * Date: 2018-03-22 Bill    Support Class Zone Quota Settings   [2018-0202-1046-39164]
 * Date: 2017-12-20 Bill	Set Default End Time "23:59:59" when add TimeTable
 * Date: 2017-11-17 Bill  	support Quota settings in Zone
 * Date: 2017-10-17 Bill  	added js function onSelectCatInThickBox()
 * Date: 2017-02-09 Villa	modified GoBack()
 */
?>

<?=$NivagationBar?>
<br>

<?php if($isEdit) { ?>
    <div>
        <?=$WarningBox_Static?>
    </div>
<?php } ?>

<div style="display:none;" id="NeedConfirmMsg">
	<?=$WarningBox?>
</div>
<br>
<form name='form1' id='form1' method="POST" action='index.php?task=settings.topic.update'>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Topic']['Name']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" maxlength="25" class="textboxnum requiredField" name="Name" id="Name" value="<?=$name?>">
                <br/>
				<div style="display:none;" class="warnMsgDiv" id="Name_Warn">
					<span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Topic']['Name']?></span>
				</div>
			</td>
		</tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Topic']['TargetForm']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$YearSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Topic']['TargetForm']?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Topic']['Rubrics']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td id='RubricSettingSelection'>
                <?=$RubricSettingSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Rubric_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Topic']['Rubrics']?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Topic']['AssessmentPeriod']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$AssessmentDateStart?>
                <?=$Lang['General']['To']?>
                <?=$AssessmentDateEnd?>
                <br/>
                <!--
                <div style="display:none;" class="warnMsgDiv" id="ExamPeriod_Warn">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateFormat"]?></span>
                </div>
                -->
                <div style="display:none;" class="warnMsgDiv" id="ExamPeriod_Warn2">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateRange"]?></span>
                </div>
                <?=$LoadingImage?>
            </td>
        </tr>
		<tr> 
			<td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Topic']['InputPeriod']?>
				<span class="tabletextrequire">*</span>
			</td>
            <td>
                <?=$InputDateStart?>
                <?=$Lang['General']['To']?>
                <?=$InputDateEnd?>
                <!--
                <div style="display:none;" class="warnMsgDiv" id="MarkPeriod_Warn">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateFormat"]?></span>
                </div>
                -->
                <div style="display:none;" class="warnMsgDiv" id="MarkPeriod_Warn2">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateRange"]?></span>
                </div>
                <div style="display:none;" class="warnMsgDiv" id="MarkPeriod_Warn3">
                    <span class="tabletextrequire">*<?=$Lang['PowerPortfolio']['Setting']['ImportWarning']['WrongInputPeriod']?></span>
                </div>
                <div style="display:none;" class="warnMsgDiv" id="MarkPeriod_Warn4">
                    <span class="tabletextrequire">*<?=$Lang['PowerPortfolio']['Setting']['ImportWarning']['WrongInputPeriod2']?></span>
                </div>
                <?=$LoadingImage?>
            </td>
		</tr>
	</tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Topic']['Rubrics'])?></p>
<div id="rubicsSelectDiv">
    <!-- /Create Rubics -->
</div>

<input type='hidden' name='RubricIndexList' id='RubricIndexList' value='' />
<input type='hidden' name='SettingID' id='SettingID' value='<?=$SettingID?>' />
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>
<?=$thickBox?>

<!--<script src="<?=$indexVar["thisBasePath"]?>templates/json2.js"></script>-->
<link href="<?=$indexVar["thisBasePath"]?>templates/fontawesome/css/all.css" rel="stylesheet">
<link href="<?=$indexVar["thisBasePath"]?>templates/2009a/css/PowerPortfolio/style.css" rel="stylesheet">
<!--<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="<?=$indexVar["thisBasePath"]?>templates/2009a/js/PowerPortfolio/demo.js"></script>-->
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.js"></script>

<script>

$(document).ready(function(){
	if('<?=$isEdit?>') { 	// input record to the form
		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {
	}
});

function intialize_Data()
{
	
	$('.LoadingImg').hide();

	$('#ExamPeriodStart').val('<?=$ExamPeriodStart?>');
	$('#ExamPeriodEnd').val('<?=$ExamPeriodEnd?>');
	$('#MarkPeriodStart').val('<?=$MarkPeriodStart?>');
	$('#MarkPeriodEnd').val('<?=$MarkPeriodEnd?>');
	loadSelectDiv(function(){
		reloadSelected();
	});
	
	reEnableSubmitBtn();
}

function CheckForm()
{
	// Hide Warning Message
	let check = true;
	/*
	$('#Name_Warn').hide();
	$('#Year_Warn').hide();
	$('#ExamPeriod_Warn').hide();
	$('#ExamPeriod_Warn2').hide();
	$('#MarkPeriod_Warn').hide();
	$('#MarkPeriod_Warn2').hide();
	*/
    $('.warnMsgDiv').hide();

    if(!check_date_without_return_msg($('#MarkPeriodStart')[0]) || !check_date_without_return_msg($('#MarkPeriodEnd')[0])){
        $('#MarkPeriodStart').focus();
        check = false;
    }
    if (compareDate($('#MarkPeriodStart').val(), $('#MarkPeriodEnd').val()) > 0){
        $('#MarkPeriodStart').focus();
        $('#MarkPeriod_Warn2').show();
        check = false;
    } else {
        if (compareDate($('#ExamPeriodStart').val(), $('#MarkPeriodStart').val()) > 0){
            $('#MarkPeriodStart').focus();
            $('#MarkPeriod_Warn3').show();
            check = false;
        }
        if (compareDate($('#ExamPeriodEnd').val(), $('#MarkPeriodEnd').val()) > 0){
            $('#MarkPeriodStart').focus();
            $('#MarkPeriod_Warn4').show();
            check = false;
        }
    }
    if(!check_date_without_return_msg($('#ExamPeriodStart')[0]) || !check_date_without_return_msg($('#ExamPeriodEnd')[0])){
        $('#ExamPeriodStart').focus();
        check = false;
    }
    if (compareDate($('#ExamPeriodStart').val(), $('#ExamPeriodEnd').val()) > 0){
        $('#ExamPeriodStart').focus();
        $('#ExamPeriod_Warn2').show();
        check = false;
    }
    if($('#RubricSettingID').val()==''){
        $('#RubricSettingID').focus();
        $('#Rubric_Warn').show();
        check = false;
    }
    if($('#YearID').val()==''){
        $('#YearID').focus();
        $('#Year_Warn').show();
        check = false;
    }
	if($('#Name').val()==''){
        $('#Name').focus();
		$('#Name_Warn').show();
		check = false;
	}

	return check;
}

function formDataChecking()
{
}

function YearOnChange()
{
	let yearID = $('#YearID').val();
	$.ajax({
        url: 'index.php?task=settings.topic.ajax',
        data: {'Action': 'DisplayRubricSettingSelect', 'YearID': yearID},
        method: 'POST',
        success: function(html){
            $('#RubricSettingSelection select').remove();
            $('#RubricSettingSelection').prepend(html);
        }
    });
}

function toggleSelected(ele){
	let code = $(ele).parent().parent().find('div.text').attr('alt-code');
	if(RubricIndexValue[code]){
		RubricIndexValue[code] = false;
		$(ele).toggleClass('selected');
	} else {
		RubricIndexValue[code] = true;
		$(ele).toggleClass('selected');
	}
}

function reloadSelected(){
	if(Object.keys(RubricIndexValue).length != 0){
		for(let code in RubricIndexValue){
			if(RubricIndexValue[code]){
				$('div.text[alt-code="'+code+'"]').parent().parent()
				.find('a.btn-select').toggleClass('selected');
			}
		}
	}
}

function RubricSettingOnChange(val){
	if(val != ''){
		loadSelectDiv(function(){});
	}
}

function loadSelectDiv(callback)
{
    let rubricsID = $('#RubricSettingID').val();
    $.ajax({
        url: 'index.php?task=settings.topic.ajax',
        data: {'Action': 'DisplayRubricSelectDiv', 'rubricSettingID': rubricsID, 'topicSettingID': '<?=$SettingID?>'},
        method: 'POST',
        success: function(html){
            $('#rubicsSelectDiv').html(html);
            callback();
        }
    });
}

function goSubmit(){
	if(CheckForm()){
		genRubricIndexList();
		$('#form1').submit();
	}
}

function genRubricIndexList(){
	resultAry = [];
	for(let code in RubricIndexValue){
		if(code == 'in_array') continue;
		else if(RubricIndexValue[code])resultAry.push(code);
	}
	$('#RubricIndexList').val(resultAry.join(','));
}

function goBack(){
	 window.location.href = "index.php?task=settings.topic.list";
}

function disableSubmitBtn() {
	$('#submitBtn').attr('disabled','disabled');
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
}

function reEnableSubmitBtn() {
	$('#submitBtn').removeAttr('disabled');
	$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
}

function toggleSelectAll(){
	let hasFalse = false;
	let keys = Object.keys(RubricIndexValue);
	for(let i=0; i<keys.length; i++){
		if(!RubricIndexValue[keys[i]]) hasFalse = true;
	}
	if(hasFalse || keys.length==0){
		// select All
		$('.btn-select').each(function(){
			if(!$(this).hasClass('selected')){
				$(this).click();
			}
		});
		$('.btn-select.all').addClass('selected');
	} else {
		// unselect
		$('.btn-select').removeClass('selected');
		RubricIndexValue = [];
	}
	
}
</script>