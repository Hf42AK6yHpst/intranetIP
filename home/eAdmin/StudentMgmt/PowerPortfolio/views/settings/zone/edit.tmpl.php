<?=$NivagationBar?>
<br> 
<br>

<form name='form1' id="form1" method="POST" action="index.php?task=settings.zone.update">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Zone']['Name']?> <span class="tabletextrequire">*</span></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="Name" id="Name" value="<?=$name?>">
				<div style="display:none;" class="warnMsgDiv" id="Name_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Zone']['Name']?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Zone']['Topic']?> <span class="tabletextrequire">*</span></td>
			<td>
				<?=$topicSelecctor?>
				<div style="display:none;" class="warnMsgDiv" id="Topic_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Zone']['Topic']?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Zone']['Quota']?> <span class="tabletextrequire">*</span></td>
			<td>
				<input type="text" maxlength="255" class="textboxnum requiredField" name="Quota" id="Quota" value="<?=$quota?>">
				<div style="display:none;" class="warnMsgDiv" id="Quota_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Zone']['Quota']?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Zone']['Image']?> <span class="tabletextrequire">*</span></td>
			<td>
				<?=$PICSelection?>
				<div style="display:none;" class="warnMsgDiv" id="Pic_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputNum']?></span>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
	
	<input type='hidden' id='ZoneID' name='ZoneID' value=<?=$ZoneID?> >
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$button_save?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$button_back?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<!--<script src="<?=$indexVar["thisBasePath"]?>templates/json2.js"></script>-->
<link href="<?=$indexVar["thisBasePath"]?>templates/fontawesome/css/all.css" rel="stylesheet">
<link href="<?=$indexVar["thisBasePath"]?>templates/2009a/css/PowerPortfolio/style.css" rel="stylesheet">
<!--<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="<?=$indexVar["thisBasePath"]?>templates/2009a/js/PowerPortfolio/demo.js"></script>-->
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.js"></script>

<script>
$(document).ready(function(){
	if('<?=$isEdit?>') { 	// input record to the form
		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {
	}
});

function intialize_Data()
{
	
	$('.LoadingImg').hide();
	
	reEnableSubmitBtn();
}

function checkForm(){
	let check = true;

	/*
	$('#Name_Warn').hide();
	$('#Quota_Warn').hide();
	$('#Topic_Warn').hide();
	*/
    $('.warnMsgDiv').hide();

    if($('#Quota').val()==''){
        $('#Quota').focus();
        $('#Quota_Warn').show();
        check = false;
    }
    if($('#TopicSettingID').val()==''){
        $('#TopicSettingID').focus();
        $('#Topic_Warn').show();
        check = false;
    }
	if($('#Name').val()==''){
        $('#Name').focus();
		$('#Name_Warn').show();
		check = false;
	}

	return check;
}

function TopicOnChange(){
	
}

function goSubmit(){
	if(checkForm()){
		$('#form1').submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=settings.zone.list";
}
function goURL(){
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>zone<?=$PowerPortfolioConfig['taskSeparator']?>list';
}
</script>