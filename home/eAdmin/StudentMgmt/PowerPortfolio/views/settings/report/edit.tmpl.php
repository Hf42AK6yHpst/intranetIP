<?php
/*
 * Change Log:
 * Date: 2019-09-24 Philips Added onSelectChapterInThickBox()
 * Date: 2018-10-15 Bill    Disable submit button > prevent any unwanted data issues    [2018-1015-1724-00096]
 * Date: 2018-05-02 Bill    Display Warning Box after updated Zone Settings
 * Date: 2018-03-22 Bill    Support Class Zone Quota Settings   [2018-0202-1046-39164]
 * Date: 2017-12-20 Bill	Set Default End Time "23:59:59" when add TimeTable
 * Date: 2017-11-17 Bill  	support Quota settings in Zone
 * Date: 2017-10-17 Bill  	added js function onSelectCatInThickBox()
 * Date: 2017-02-09 Villa	modified GoBack()
 */
?>

<?=$NivagationBar?>
<br>
<br>
<form name='form1' id='form1' method="POST">
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Report']['Name']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" maxlength="255" class="textboxtext" name="Name" id="Name" value="<?=$Name?>">
                <br/>
				<div style="display:none;" class="warnMsgDiv" id="Name_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['Name']?></span>
				</div>
			</td>
		</tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Report']['TargetForm']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$YearSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
                    <span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['TargetForm']?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Report']['Period']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$InputDateStart?>
                <?=$Lang['General']['To']?>
                <?=$InputDateEnd?>
                <br/>
                <!--
                <div style="display:none;" class="warnMsgDiv" id="Date_Warn">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateFormat"]?></span>
                </div>
                -->
                <div style="display:none;" class="warnMsgDiv" id="Date2_Warn">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateRange"]?></span>
                </div>
                <?=$LoadingImage?>
            </td>
        </tr>
	</tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Report']['Cover'])?></p>
<table class="form_table_v30">
    <tbody>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['CoverIncludeInfo']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="radio" name="CoverIncludeInfo" id="CoverIncludeInfo_Y" value='1' <?=($CoverIncludeInfo? 'checked' : '')?>/>
            <label for="CoverIncludeInfo_Y"> <?=$Lang['General']['Yes']?> </label>
            <input type="radio" name="CoverIncludeInfo" id="CoverIncludeInfo_N" value="0" <?=(!$CoverIncludeInfo? 'checked' : '')?>/>
            <label for="CoverIncludeInfo_N"> <?=$Lang['General']['No']?> </label>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['CoverTitle']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="text" maxlength="255" class="textboxtext" name="CoverTitle" id="CoverTitle" value="<?=$CoverTitle?>" placeholder="<?=$Lang['PowerPortfolio']['Settings']['Report']['CoverTitleDefault']?>">
            <br/>
            <div style="display:none;" class="warnMsgDiv" id="Title_Warn">
                <span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['CoverTitle']?></span>
            </div>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['CoverSubTitle']?>
        </td>
        <td>
            <input type="text" maxlength="255" class="textboxtext" name="CoverSubTitle" id="CoverSubTitle" value="<?=$CoverSubTitle?>">
        </td>
    </tr>
    </tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Report']['CAReport'])?></p>
<table class="form_table_v30">
    <tbody>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['CAReportTitle']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="text" maxlength="255" class="textboxtext" name="CAReportTitle" id="CAReportTitle" value="<?=$CAReportTitle?>" placeholder="<?=$Lang['PowerPortfolio']['Settings']['Report']['CAReportTitleDefault']?>">
            <br/>
            <div style="display:none;" class="warnMsgDiv" id="CATitle_Warn">
                <span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['CAReport']?></span>
            </div>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['CAReportSelectTopic']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <span id="selectTopicDiv"></span>
            <?=$indexVar["libpowerportfolio_ui"]->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Topics()")?>
            <br>
            <?=$indexVar["libpowerportfolio_ui"]->MultiSelectionRemark()?>
            <br/>
            <div style="display:none;" class="warnMsgDiv" id="Topic_Warn">
				<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['CAReportSelectTopic']?></span>
			</div>
        </td>
    </tr>
    </tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Report']['ActivityRecords'])?></p>
<table class="form_table_v30">
    <tbody>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['ActivitySelect']?>
        </td>
        <td>
            <span id="selectActivityDiv"></span>
            <?=$indexVar["libpowerportfolio_ui"]->GET_SMALL_BTN($Lang["Btn"]["Clear"], "button", "js_UnSelect_All_Activities()")?>
            <?=$indexVar["libpowerportfolio_ui"]->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Activities()")?>
            <br>
            <?=$indexVar["libpowerportfolio_ui"]->MultiSelectionRemark()?>
            <br/>
            <div style="display:none;" class="warnMsgDiv" id="Activity_Warn">
				<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['ActivitySelect']?></span>
			</div>
        </td>
    </tr>
    </tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Report']['TermAssessment'])?></p>
<table class="form_table_v30">
    <tbody>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentIncluded']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="radio" name="TermAssessmentIncluded" id="TermAssessmentIncluded_Y" value='1' <?=($TermAssessmentIncluded? 'checked' : '')?>/>
            <label for="TermAssessmentIncluded_Y"> <?=$Lang['General']['Yes']?> </label>
            <input type="radio" name="TermAssessmentIncluded" id="TermAssessmentIncluded_N" value="0" <?=(!$TermAssessmentIncluded? 'checked' : '')?>/>
            <label for="TermAssessmentIncluded_N"> <?=$Lang['General']['No']?> </label>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentTitle']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="text" maxlength="255" class="textboxtext" name="TermAssessmentTitle" id="TermAssessmentTitle" value="<?=$TermAssessmentTitle?>" placeholder="<?=$Lang['PowerPortfolio']['Settings']['Report']['TermAssessment']?>">
            <br/>
            <div style="display:none;" class="warnMsgDiv" id="TATitle_Warn">
                <span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentTitle']?></span>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Report']['TermComment'])?></p>
<table class="form_table_v30">
    <tbody>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['TermCommentIncluded']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="radio" name="TermCommentIncluded" id="TermCommentIncluded_Y" value='1' <?=($TermCommentIncluded? 'checked' : '')?>/>
            <label for="TermCommentIncluded_Y"> <?=$Lang['General']['Yes']?> </label>
            <input type="radio" name="TermCommentIncluded" id="TermCommentIncluded_N" value="0" <?=(!$TermCommentIncluded? 'checked' : '')?>/>
            <label for="TermCommentIncluded_N"> <?=$Lang['General']['No']?> </label>
        </td>
    </tr>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['TermCommentTitle']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="text" maxlength="255" class="textboxtext" name="TermCommentTitle" id="TermCommentTitle" value="<?=$TermCommentTitle?>" placeholder="<?=$Lang['PowerPortfolio']['Settings']['Report']['TermComment']?>">
            <br/>
            <div style="display:none;" class="warnMsgDiv" id="TCTitle_Warn">
                <span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Report']['TermCommentTitle']?></span>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Report']['PageLastPage'])?></p>
<table class="form_table_v30">
    <tbody>
    <tr>
        <td class="field_title">
            <?=$Lang['PowerPortfolio']['Settings']['Report']['PageLastPageIncludeInfo']?>
            <span class="tabletextrequire">*</span>
        </td>
        <td>
            <input type="radio" name="PageLastPageIncludeInfo" id="PageLastPageIncludeInfo_Y" value='1' <?=($PageLastPageIncludeInfo? 'checked' : '')?>/>
            <label for="PageLastPageIncludeInfo_Y"> <?=$Lang['General']['Yes']?> </label>
            <input type="radio" name="PageLastPageIncludeInfo" id="PageLastPageIncludeInfo_N" value="0" <?=(!$PageLastPageIncludeInfo? 'checked' : '')?>/>
            <label for="PageLastPageIncludeInfo_N"> <?=$Lang['General']['No']?> </label>
        </td>
    </tr>
    </tbody>
</table>

<input type='hidden' name='RubricIndexList' id='RubricIndexList' value='' />
<input type='hidden' name='ReportID' id='ReportID' value='<?=$ReportID?>' />
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>
<?=$thickBox?>

<!--<script src="<?=$indexVar["thisBasePath"]?>templates/json2.js"></script>
<link href="<?=$indexVar["thisBasePath"]?>templates/fontawesome/css/all.css" rel="stylesheet">
<link href="<?=$indexVar["thisBasePath"]?>templates/2009a/css/PowerPortfolio/style.css" rel="stylesheet">
<!--<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="<?=$indexVar["thisBasePath"]?>templates/2009a/js/PowerPortfolio/demo.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.js"></script>-->

<script>

$(document).ready(function(){
	if('<?=$isEdit?>') { 	// input record to the form
		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {
        YearOnChange();
	}
});

function intialize_Data()
{
	$('.LoadingImg').hide();

	$('#StartDate').val('<?=$StartDate?>');
	$('#EndDate').val('<?=$EndDate?>');
    initYearOnChange();
	
	reEnableSubmitBtn();
}

function CheckForm()
{
// 	disableSubmitBtn();
	let check = true;

	// Hide Warning Message
	$('.warnMsgDiv').hide();

	if($('#TermCommentTitle').val()!='' && $.trim($('#TermCommentTitle').val())==''){
        $('#TermCommentTitle').focus();
        $('#TermCommentTitle').val('');
        //$('#TCTitle_Warn').show();
        check = false;
    }
    if($('#TermAssessmentTitle').val()!='' && $.trim($('#TermAssessmentTitle').val())==''){
        $('#TermAssessmentTitle').focus();
        $('#TermAssessmentTitle').val('');
        //$('#TATitle_Warn').show();
        check = false;
    }
    /*if($('#ActivityID\\[\\]').val()==null){
        $('#ActivityID\\[\\]').focus();
        $('#Activity_Warn').show();
        check = false;
    }*/
    if($('#TopicID\\[\\]').val()==null){
        $('#TopicID\\[\\]').focus();
        $('#Topic_Warn').show();
        check = false;
    }
    if($('#CoverTitle').val()!='' && $.trim($('#CoverTitle').val())==''){
        $('#CoverTitle').focus();
        //$('#CATitle_Warn').show();
        $('#CoverTitle').val('');
        check = false;
    }
    if($('#CAReportTitle').val()!='' && $.trim($('#CAReportTitle').val())==''){
        $('#CAReportTitle').focus();
        $('#CAReportTitle').val('');
        //$('#Title_Warn').show();
        check = false;
    }
    if(!check_date_without_return_msg($('#StartDate')[0]) || !check_date_without_return_msg($('#EndDate')[0])){
        $('#StartDate').focus();
        check = false;
    }
    if (compareDate($('#StartDate').val(), $('#EndDate').val()) > 0){
        $('#StartDate').focus();
        $('#Date2_Warn').show();
        check = false;
    }
    if($('#YearID').val()==''){
        $('#YearID').focus();
        $('#Year_Warn').show();
        check = false;
    }
	if($.trim($('#Name').val())==''){
        $('#Name').focus();
		$('#Name_Warn').show();
        check = false;
	}
	
	return check;
}

function formDataChecking()
{
}

function YearOnChange()
{
	let yearID = $('#YearID').val();
    let reportID = $('#ReportID').val();
	$.ajax({
        url: 'index.php?task=settings.report.ajax',
        data: {'Action': 'DisplayTopicSelect', 'YearID': yearID, 'ReportID': reportID},
        method: 'POST',
        success: function(html){
            $('#selectTopicDiv').html(html);
            js_Select_All_Topics();
        }
    });
    $.ajax({
        url: 'index.php?task=settings.report.ajax',
        data: {'Action': 'DisplayActivitySelect', 'YearID': yearID, 'ReportID': reportID},
        method: 'POST',
        success: function(html){
            $('#selectActivityDiv').html(html);
            js_Select_All_Activities();
        }
    });
}

function initYearOnChange()
{
    let yearID = $('#YearID').val();
    let reportID = $('#ReportID').val();
    $.ajax({
        url: 'index.php?task=settings.report.ajax',
        data: {'Action': 'DisplayTopicSelect', 'YearID': yearID, 'ReportID': reportID},
        method: 'POST',
        success: function(html){
            $('#selectTopicDiv').html(html);
            //js_Select_All_Topics();
        }
    });
    $.ajax({
        url: 'index.php?task=settings.report.ajax',
        data: {'Action': 'DisplayActivitySelect', 'YearID': yearID, 'ReportID': reportID},
        method: 'POST',
        success: function(html){
            $('#selectActivityDiv').html(html);
            //js_Select_All_Activities();
        }
    });
}

/*
function toggleSelected(ele){
	let code = $(ele).parent().parent().find('div.text').attr('alt-code');
	if(RubricIndexValue[code]){
		RubricIndexValue[code] = false;
		$(ele).toggleClass('selected');
	} else {
		RubricIndexValue[code] = true;
		$(ele).toggleClass('selected');
	}
}

function reloadSelected(){
	if(Object.keys(RubricIndexValue).length != 0){
		for(let code in RubricIndexValue){
			if(RubricIndexValue[code]){
				$('div.text[alt-code="'+code+'"]').parent().parent()
				.find('a.btn-select').toggleClass('selected');
			}
		}
	}
}

function RubricSettingOnChange(val){
	if(val != ''){
		loadSelectDiv(function(){});
	}
}

function loadSelectDiv(callback)
{
    let rubricsID = $('#RubricSettingID').val();
    $.ajax({
        url: 'index.php?task=settings.topic.ajax',
        data: {'Action': 'DisplayRubricSelectDiv', 'rubricSettingID': rubricsID, 'topicSettingID': '<?=$SettingID?>'},
        method: 'POST',
        success: function(html){
            $('#rubicsSelectDiv').html(html);
            callback();
        }
    });
}
*/

function goSubmit(){
	if(CheckForm()){
		//genRubricIndexList();
        $('#form1').attr('action', 'index.php?task=settings.report.update');
		$('#form1').submit();
	}
}

/*
function genRubricIndexList(){
	resultAry = [];
	for(let code in RubricIndexValue){
		if(code == 'in_array') continue;
		else if(RubricIndexValue[code])resultAry.push(code);
	}
	$('#RubricIndexList').val(resultAry.join(','));
}
*/

function js_Select_All_Topics()
{
    js_Select_All('TopicID[]');
}

function js_Select_All_Activities()
{
    js_Select_All('ActivityID[]');
}

function js_UnSelect_All_Activities()
{
    $('select[name="ActivityID\\[\\]"] option').removeAttr('selected');
}

function goBack(){
	 window.location.href = "index.php?task=settings.topic.list";
}

function disableSubmitBtn() {
	$('#submitBtn').attr('disabled','disabled');
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
}

function reEnableSubmitBtn() {
	$('#submitBtn').removeAttr('disabled');
	$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
}
</script>