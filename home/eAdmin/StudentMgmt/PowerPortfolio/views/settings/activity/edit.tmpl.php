<?php
/*
 * Change Log:
 */
?>

<?=$NivagationBar?>
<br>

<?php if($isEdit) { ?>
    <div>
        <?=$WarningBox_Static?>
    </div>
<?php } ?>

<div style="display:none;" id="NeedConfirmMsg">
	<?=$WarningBox?>
</div>
<br>
<form name='form1' id='form1' method="POST" action='index.php?task=settings.activity.update'>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['ReportName']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" class="textboxtext requiredField" name="ReportName" id="ReportName" value="<?=$reportName?>">
                <br/>
				<div style="display:none;" class="warnMsgDiv" id="ReportName_Warn">
					<span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['ReportName']?></span>
				</div>
			</td>
		</tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Activity']['TargetForm']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$YearSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['TargetForm']?></span>
                </div>
            </td>
        </tr>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['ApplyTopic']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type='radio' name='ApplyTopic' id='ApplyTopic_y' value='1' onChange='ApplyTopicOnChange(this.value)' <?=$applyTopic ? 'checked' :''?>/> <label for='ApplyTopic_y'><?=$Lang['General']['Yes']?></label>
				&nbsp;
				<input type='radio' name='ApplyTopic' id='ApplyTopic_n' value='0' onChange='ApplyTopicOnChange(this.value)' <?=$applyTopic ? '' :'checked'?>/> <label for='ApplyTopic_n'><?=$Lang['General']['No']?></label>
				<br/>
				<div id='TopicSelectDiv' style='<?=$applyTopic ? '' : 'display:none;'?>'>
					<?=$topicSelecctor?>
				</div>
                <div style="display:none;" class="warnMsgDiv" id="Topic_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTopic']?></span>
                </div>
			</td>
		</tr>
		<tr id='RubricCodeRow' style='<?=$applyTopic ? '' : 'display:none;'?>'>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['ObserveArea']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<div id='RubricCodeDiv'></div>
                <div style="display:none;" class="warnMsgDiv" id="RubricCode_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['ObserveArea']?></span>
                </div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTitle']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" class="textboxtext requiredField" name="ActivityTitle" id="ActivityTitle" value="<?=$activityTitle?>">
				<br/>
                <div style="display:none;" class="warnMsgDiv" id="ActivityTitle_Warn">
					<span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTitle']?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['Name']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" class="textboxtext requiredField" name="ActivityName" id="ActivityName" value="<?=$activityName?>">
				<br/>
                <div style="display:none;" class="warnMsgDiv" id="ActivityName_Warn">
					<span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['Name']?></span>
				</div>
			</td>
		</tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Activity']['Date']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$activityDateBox?>
                <br/>
                <!--
                <div style="display:none;" class="warnMsgDiv" id="ActivityDate_Warn">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateFormat"]?></span>
                </div>
                <div style="display:none;" class="warnMsgDiv" id="ActivityDate_Warn2">
                    <span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateRange"]?></span>
                </div>
                -->
            </td>
        </tr>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['Duration']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" maxlength="4" class="textboxnum requiredField" name="ActivityTime" id="ActivityTime" value="<?=$activityTime?>">
				<br/>
                <div style="display:none;" class="warnMsgDiv" id="ActivityTime_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['Duration']?></span>
                </div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['Location']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" class="textboxtext requiredField" name="Location" id="Location" value="<?=$location?>">
                <br/>
				<div style="display:none;" class="warnMsgDiv" id="Location_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['Location']?></span>
                </div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Activity']['Type']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<?=$contentTypeSelector?>
			</td>
		</tr>
	</tbody>
</table>

<input type='hidden' name='ActivitySettingID' id='ActivitySettingID' value='<?=$ActivitySettingID?>' />
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<!--<script src="<?=$indexVar["thisBasePath"]?>templates/json2.js"></script>-->
<link href="<?=$indexVar["thisBasePath"]?>templates/fontawesome/css/all.css" rel="stylesheet">
<link href="<?=$indexVar["thisBasePath"]?>templates/2009a/css/PowerPortfolio/style.css" rel="stylesheet">
<!--<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="<?=$indexVar["thisBasePath"]?>templates/2009a/js/PowerPortfolio/demo.js"></script>-->
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.js"></script>

<script>

$(document).ready(function(){
	if('<?=$isEdit?>') { 	// input record to the form
		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {
        YearOnChange();
//         setTimeout(function(){$('#TopicSettingID').attr('disabled', true)}, 1500);	
	}
});

function intialize_Data()
{
	$('.LoadingImg').hide();

	$('#ActivityDate').val('<?=$activityDate?>');
	if(<?=$applyTopic ? 'true' : 'false'?>){
		initRubricSelect($('#TopicSettingID').val(), '<?=$rubricCode?>');

        $('#ActivityTitle')
            .attr('disabled', true)
            .parent().parent().hide();
	}
	
	reEnableSubmitBtn();
}

function CheckForm()
{
	let check = true;

	/*
	$('#ReportName_Warn').hide();
	$('#ActivityTitle_Warn').hide();
	$('#ActivityName_Warn').hide();
	$('#Year_Warn').hide();
	$('#ActivityDate_Warn').hide();
	$('#ActivityDate_Warn2').hide();
	$('#ActivityTime_Warn').hide();
	$('#Location_Warn').hide();
    */
    $('.warnMsgDiv').hide();

    if($('#Location').val()==''){
        $('#Location').focus();
        $('#Location_Warn').show();
        check = false;
    }
    if($('#ActivityTime').val()==''){
        $('#ActivityTime').focus();
        $('#ActivityTime_Warn').show();
        check = false;
    }
    if(!check_date_without_return_msg($('#ActivityDate')[0])){
        $('#ActivityDate').focus();
        check = false;
    }
    if($('#ActivityName').val()==''){
        $('#ActivityName').focus();
        $('#ActivityName_Warn').show();
        check = false;
    }
    if($('#ActivityTitle').attr('disabled')){
//         $("select[name^='RubricCode']").each(function(){
//             if($(this).val()==''){
//                 $(this).focus();
//                 $('#RubricCode_Warn').show();
//                 check = false;
//             }
//         })
		if($("select.bottomRubric").length != 1 || $("select.bottomRubric").val() == ''){
			$('#RubricCode_Warn').show();
	        check = false;
		}
        if($('#TopicSettingID').val()==''){
            $('#TopicSettingID').focus();
            $('#Topic_Warn').show();
            check = false;
        }
    } else {
        if($('#ActivityTitle').val()==''){
            $('#ActivityTitle').focus();
            $('#ActivityTitle_Warn').show();
            check = false;
        }
    }
    if($('#YearID').val()==''){
        $('#YearID').focus();
        $('#Year_Warn').show();
        check = false;
    }
	if($('#ReportName').val()==''){
        $('#ReportName').focus();
		$('#ReportName_Warn').show();
		check = false;
	}

	return check;
}

function YearOnChange()
{
	let yearID = $('#YearID').val();
	if(yearID =='') return;
	$.ajax({
		url: 'index.php?task=settings.activity.ajax',
		method: 'POST',
		data: {'Action': 'GetTopicSelecct', 'YearID': yearID},
		success: function(html){
			$('#TopicSelectDiv').html(html);
		}
	});
}

function ApplyTopicOnChange(val)
{
    $('#RubricCode_Warn').hide();
    $('#Topic_Warn').hide();
    $('#ActivityTitle_Warn').hide();

	if(val=='1'){
		$('#TopicSelectDiv').show();
		$('#TopicSettingID').removeAttr('disabled');
		$('#RubricCodeRow').show();
		$('#ActivityTitle')
		.attr('disabled', true)
		.parent().parent().hide();
	} else {
		$('#TopicSelectDiv').hide();
		$('#TopicSettingID').attr('disabled', true);
		$('#RubricCodeRow').hide();
		$('#ActivityTitle')
		.removeAttr('disabled')
		.parent().parent().show();
	}
}

function TopicOnChange(){
	let value = $('#TopicSettingID').val();
	initRubricSelect(value, '');
}
function initRubricSelect(topicSettingID, code){
	if(topicSettingID=='') return;
	$.ajax({
		url: 'index.php?task=settings.activity.ajax',
		method: 'POST',
		data: {'Action': 'GetRubricCodeSelect', 'TopicSettingID': topicSettingID, 'Code' : code},
		success: function(html){
			$('#RubricCodeDiv').html(html);
		}
	});
}
function RubricCodeOnChange(level, code){
	if(level==2||code=='') return;
	let topicSettingID = $('#TopicSettingID').val();
	$.ajax({
		url: 'index.php?task=settings.activity.ajax',
		method: 'POST',
		data: {'Action': 'GetRubricCodeNextSelect', 'TopicSettingID': topicSettingID, 'Code' : code, 'Level': level+1},
		success: function(html){
			for(let i=2;i>level;i--) $('#RubricCode_'+i).remove();
			for(let i=2;i>level;i--) $('#RubricCode_'+level).next().remove();
			$('#RubricCode_'+level).after(html);
		}
	});
}
function ContentTypeOnChange(){
	
}
function goSubmit(){
	if(CheckForm()){
		$('#form1').submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=settings.activity.list";
}

function disableSubmitBtn() {
	$('#submitBtn').attr('disabled','disabled');
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
}

function reEnableSubmitBtn() {
	$('#submitBtn').removeAttr('disabled');
	$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
}
</script>