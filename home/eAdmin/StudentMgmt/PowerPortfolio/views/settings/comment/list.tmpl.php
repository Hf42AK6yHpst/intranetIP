<script type="text/javascript">
$(document).ready( function() {
	
});

function goNew() {
	load_dyn_size_thickbox_ip('<?=$Lang['PowerPortfolio']['Settings']['Comment']['Category']?>', 'onloadThickBox('+''+');');
}

function goEdit(CommentID) {
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>comment<?=$PowerPortfolioConfig['taskSeparator']?>edit_cat&CatID='+CommentID;
}
function goImport(){
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>comment<?=$PowerPortfolioConfig['taskSeparator']?>import';
}

function checkEdit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push($(this).val());
	});
	if(EditID.length==1){
		goEditCat(EditID[0]);
	}
	else if(EditID.length > 1){
		alert("<?=$Lang['PowerPortfolio']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['PowerPortfolio']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}

function goEditCat(CommentID){
	load_dyn_size_thickbox_ip('<?=$Lang['PowerPortfolio']['Settings']['Comment']['Category']?>', 'onloadThickBox('+CommentID+');');
}

function onloadThickBox(CommentID)
{
	$.ajax({
		url: 'index.php?task=settings.comment.ajax',
		method: 'POST',
		data: {'Action': 'EditComment', 'CommentID': CommentID, 'IsCat': true},
		success: function(html){
			$('div#TB_ajaxContent').html(html);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}

function updateComment(){
	document.thickbox_form.method = "POST";
	document.thickbox_form.action = "index.php?task=settings.comment.update_cat&isEdit=1";
	document.thickbox_form.submit();
}

function checkCopy(){
	
}
function checkExport(){
	
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<!--<?=$htmlAry['searchBox']?>-->
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>