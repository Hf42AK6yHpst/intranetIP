<?php
// Using: 
/*
 */
ini_set("memory_limit", "1024M");
set_time_limit(2*3600);
include_once($indexVar['thisBasePath']."includes/SimpleImage.php");

$simple_image = new SimpleImage();

$YearClassID = $_REQUEST['YearClassID'];
$ActivitySettingID = $_REQUEST['ActivitySettingID'];
$durationAry = $_POST['duration'];
$locationAry = $_POST['location'];
$imageUploadAry = $_FILES['imageUpload'];
$existImgSrcAry = $_POST['existImgSrc'];
$descriptionAry = $_POST['description'];
$dataAry = array();
$result = array();
$fileUploadAry= array();
// debug_pr($_FILES);debug_pr($_POST);die();
if(!empty($durationAry)){
	#############
	# 1. Clear Old Record
	#############
	$studentIDAry = array_keys($durationAry);
	$result['delete'] = $indexVar['libpowerportfolio']->Delete_Activity_Record($ActivitySettingID, $studentIDAry);
	foreach($studentIDAry as $studentID){
		$_activityDate = $_POST['date_'.$studentID];
		$_duration = $durationAry[$studentID];
		$_location = $locationAry[$studentID];
		$_description = $descriptionAry[$studentID];
		if($existImgSrcAry[$studentID] && !$imageUploadAry['name'][$studentID]){
			$_imgSrc = $existImgSrcAry[$studentID];
		} else {
			$_fileName = $imageUploadAry['name'][$studentID];
			if($_fileName){
				$target_dir = $indexVar['saveImage'].'activity_record/';
				// 		$target_dir = $intranet_root ."/file/power_portfolio/".$thisSchoolYearName."/images/".'activity_record/';
				if($_fileName) {
					$indexVar["libfilesystem"]->folder_new($target_dir);
					
					$file_ext = $indexVar["libfilesystem"]->file_ext($_fileName);
					$PhotoPath = basename($ActivitySettingID).'_'.$studentID.$file_ext;
					//$target_file = $target_dir.$PhotoPath;
					$_imgSrc = $PhotoPath;
					$fileUploadAry[] = $studentID;
				}
			} else {
				$_imgSrc = '';
			}
		}
		$row = array(
				'StudentID' => $studentID,
				'ActivitySettingID' => $ActivitySettingID,
				'ActivityDate' => $_activityDate,
				'Duration' => $_duration,
				'Location' => $_location,
				'imgSrc' => $_imgSrc,
				'Description' => $_description
		);
		$dataAry[] = $row;
	}
	$result['Insert'] = $indexVar['libpowerportfolio']->Insert_Activity_Record($dataAry);
}

function handleImageOrientation($image_path, $image_type)
{
	# Create image object
	if($image_type == 'jpg' || $image_type == 'jpeg') {
		$image_obj = imagecreatefromjpeg($image_path);
	} else {
		$image_obj = imagecreatefrompng($image_path);
	}
	
	# Rotate image if orientation is incorrect
	if (function_exists('exif_read_data'))
	{
		$exif = @exif_read_data($image_path);
		if($exif && isset($exif['Orientation']))
		{
			$orientation = $exif['Orientation'];
			if($orientation != 1)
			{
				$deg = 0;
				switch ($orientation) {
					case 3:
						$deg = 180;
						break;
					case 6:
						$deg = 270;
						break;
					case 8:
						$deg = 90;
						break;
				}
				if ($deg) {
					$image_obj = imagerotate($image_obj, $deg, 0);
				}
			}
		}
	}
	
	# Create jpeg file
	if($image_type == 'jpg' || $image_type == 'jpeg') {
		imagejpeg($image_obj, $image_path, 100);
	} else {
		imagepng($image_obj, $image_path, 100);
	}
	
	//return $image_obj;
}
if(sizeof($fileUploadAry) > 0 && $result['Insert'] > 0)
{
	foreach($fileUploadAry as $sid){
		$_targetPath = basename($ActivitySettingID).'_'.$sid.$indexVar["libfilesystem"]->file_ext($imageUploadAry['name'][$sid]);
		$max_width = 300;
		$max_height = 300;
		
		### File [Start]
		$uploadOk = 1;
		$target_file = $target_dir.$_targetPath;
		$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
		
		// Check if image file is a actual image or fake image
		$check = getimagesize($imageUploadAry["tmp_name"][$sid]);
		if($check !== false) {
			$uploadOk = 1;
		}
		else {
			$UploadError[] = "File is not an image.";
			$uploadOk = 0;
		}
		
		// Define as update process and delete the orginal one
		if (file_exists($target_file)) {
			$indexVar["libfilesystem"]->file_remove($target_file);
			// unlink($target_file);
		}
		
		if($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")
		{
			// Check file size
			// if oversize => resize the image size
			if ($imageUploadAry["size"][$sid]> 500000)
			{
// 				$target_photo_path = $imageUploadAry['tmp_name'][$sid];
// 				$simple_image->load($target_photo_path);
// 				$image_width = $simple_image->getWidth();
// 				$image_height = $simple_image->getHeight();
// 				$simple_image->resize(1080,720);
// 				$simple_image->save($target_photo_path);
				
				if($imageFileType == 'jpg' || $imageFileType == 'jpeg') {
					$image = imagecreatefromjpeg($imageUploadAry["tmp_name"][$sid]);
				} else {
					$image = imagecreatefrompng($imageUploadAry["tmp_name"][$sid]);
				}
				
				$old_width  = imagesx($image);
				$old_height = imagesy($image);
				$scale      = min($max_width/$old_width, $max_height/$old_height);
				$new_width  = ceil($scale*$old_width);
				$new_height = ceil($scale*$old_height);
				
				// Create new empty image
				$new = imagecreatetruecolor($new_width, $new_height);
				
				// Keep the transparent background for png
				imagealphablending($new, false);
				imagesavealpha($new, true);
				$transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
				imagefilledrectangle($new, 0, 0, $old_width, $old_height, $transparent);
				
				// Resize old image into new
				imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
				
				// Catch the imagedata
				imagepng($new, $imageUploadAry["tmp_name"][$sid]); //update the fileToupload with the resize image
				
				// Destroy resources
				imagedestroy($image);
				imagedestroy($new);
			}
		}
		
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
			$UploadError[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			$UploadError[] = "Sorry, your file was not uploaded.";
		}
		// if everything is ok, try to upload file
		else {
			if (move_uploaded_file($imageUploadAry["tmp_name"][$sid], $target_file)) {
				// echo "The file ". basename( $imageUploadAry["name"]). " has been uploaded.";
				
// 				if($imageUploadAry["tmp_name"] && ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")) {
// 				   handleImageOrientation($target_file, $imageFileType);
// 				}
			}
			else{
				// $uploadOk = 0;
				$UploadError[] = "Sorry, there was an error uploading your file.";
			}
		}
		### File [End]
	}
}
if(!empty($UploadError)){
	header('location: index.php?task=mgmt.activity.edit_class&YearClassID='.$YearClassID.'&ActivitySettingID='.$ActivitySettingID.'&uploadError[]='.implode(",&uploadError[]=",$UploadError));
}else {
	header('location: index.php?task=mgmt.activity.index_class&YearClassID='.$YearClassID);
}
?>