<?php
// Using: 
/*
 */
ini_set("memory_limit", "1024M");
set_time_limit(2*3600);
include_once($indexVar['thisBasePath']."includes/SimpleImage.php");

$simple_image = new SimpleImage();

$YearClassID = $_REQUEST['YearClassID'];
$ActivitySettingID = $_REQUEST['ActivitySettingID'];
$studentID = $_REQUEST['StudentID'];
$duration = $_POST['duration'];
$location = $_POST['location'];
$imageUpload = $_FILES['imageUpload'];
$existImgSrc = $_POST['existImgSrc'];
$description = $_POST['description'];
$activityDate = $_POST['date'];
$dataAry = array();
$result = array();
$fileUploadAry= array();
// debug_pr($_FILES);debug_pr($_POST);die();
#############
# 1. Clear Old Record
#############
$result['delete'] = $indexVar['libpowerportfolio']->Delete_Activity_Record($ActivitySettingID, array($studentID));
if($existImgSrc && !$imageUpload['name']){
	$_imgSrc = $existImgSrc;
} else {
	$_fileName = $imageUpload['name'];
	if($_fileName){
		$target_dir = $indexVar['saveImage'].'activity_record/';
// 		$target_dir = $intranet_root ."/file/power_portfolio/".$thisSchoolYearName."/images/".'activity_record/';
		if($_fileName) {
			$indexVar["libfilesystem"]->folder_new($target_dir);
			
			$file_ext = $indexVar["libfilesystem"]->file_ext($_fileName);
			$PhotoPath = basename($ActivitySettingID).'_'.$studentID.$file_ext;
			//$target_file = $target_dir.$PhotoPath;
			$_imgSrc = $PhotoPath;
			$fileUploadAry[] = $studentID;
		}
	} else {
		$_imgSrc = '';
	}
}
$row = array(
		'StudentID' => $studentID,
		'ActivitySettingID' => $ActivitySettingID,
		'ActivityDate' => $activityDate,
		'Duration' => $duration,
		'Location' => $location,
		'imgSrc' => $_imgSrc,
		'Description' => $description
);
$dataAry[] = $row;
$result['Insert'] = $indexVar['libpowerportfolio']->Insert_Activity_Record($dataAry);

function handleImageOrientation($image_path, $image_type)
{
	# Create image object
	if($image_type == 'jpg' || $image_type == 'jpeg') {
		$image_obj = imagecreatefromjpeg($image_path);
	} else {
		$image_obj = imagecreatefrompng($image_path);
	}
	
	# Rotate image if orientation is incorrect
	if (function_exists('exif_read_data'))
	{
		$exif = @exif_read_data($image_path);
		if($exif && isset($exif['Orientation']))
		{
			$orientation = $exif['Orientation'];
			if($orientation != 1)
			{
				$deg = 0;
				switch ($orientation) {
					case 3:
						$deg = 180;
						break;
					case 6:
						$deg = 270;
						break;
					case 8:
						$deg = 90;
						break;
				}
				if ($deg) {
					$image_obj = imagerotate($image_obj, $deg, 0);
				}
			}
		}
	}
	
	# Create jpeg file
	if($image_type == 'jpg' || $image_type == 'jpeg') {
		imagejpeg($image_obj, $image_path, 100);
	} else {
		imagepng($image_obj, $image_path, 100);
	}
	
	//return $image_obj;
}
if(sizeof($fileUploadAry) > 0 && $result['Insert'] > 0)
{
		$_targetPath = basename($ActivitySettingID).'_'.$studentID.$indexVar["libfilesystem"]->file_ext($imageUpload['name']);
		$max_width = 300;
		$max_height = 300;
		
		### File [Start]
		$uploadOk = 1;
		$target_file = $target_dir.$_targetPath;
// 		debug_pr($target_file);die();
		$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
		
		// Check if image file is a actual image or fake image
		$check = getimagesize($imageUpload["tmp_name"]);
		if($check !== false) {
			$uploadOk = 1;
		}
		else {
			$UploadError[] = "File is not an image.";
			$uploadOk = 0;
		}
		
		// Define as update process and delete the orginal one
		if (file_exists($target_file)) {
			$indexVar["libfilesystem"]->file_remove($target_file);
			// unlink($target_file);
		}
		
		if($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")
		{
			// Check file size
			// if oversize => resize the image size
			if ($imageUpload["size"][$sid]> 500000)
			{
// 				$target_photo_path = $imageUpload['tmp_name'][$sid];
// 				$simple_image->load($target_photo_path);
// 				$image_width = $simple_image->getWidth();
// 				$image_height = $simple_image->getHeight();
// 				$simple_image->resize(1080,720);
// 				$simple_image->save($target_photo_path);
				
				if($imageFileType == 'jpg' || $imageFileType == 'jpeg') {
					$image = imagecreatefromjpeg($imageUpload["tmp_name"]);
				} else {
					$image = imagecreatefrompng($imageUpload["tmp_name"]);
				}
				
				$old_width  = imagesx($image);
				$old_height = imagesy($image);
				$scale      = min($max_width/$old_width, $max_height/$old_height);
				$new_width  = ceil($scale*$old_width);
				$new_height = ceil($scale*$old_height);
				
				// Create new empty image
				$new = imagecreatetruecolor($new_width, $new_height);
				
				// Keep the transparent background for png
				imagealphablending($new, false);
				imagesavealpha($new, true);
				$transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
				imagefilledrectangle($new, 0, 0, $old_width, $old_height, $transparent);
				
				// Resize old image into new
				imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
				
				// Catch the imagedata
				imagepng($new, $imageUpload["tmp_name"][$sid]); //update the fileToupload with the resize image
				
				// Destroy resources
				imagedestroy($image);
				imagedestroy($new);
			}
		}
		
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
			$UploadError[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			$UploadError[] = "Sorry, your file was not uploaded.";
		}
		// if everything is ok, try to upload file
		else {
			if (move_uploaded_file($imageUpload["tmp_name"], $target_file)) {
				// echo "The file ". basename( $imageUpload["name"]). " has been uploaded.";
				
// 				if($imageUpload["tmp_name"] && ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")) {
// 				   handleImageOrientation($target_file, $imageFileType);
// 				}
			}
			else{
				// $uploadOk = 0;
				$UploadError[] = "Sorry, there was an error uploading your file.";
			}
		}
		### File [End]
}
if(!empty($UploadError)){
	header('location: index.php?task=mgmt.activity.edit_stu&YearClassID='.$YearClassID.'&ActivitySettingID='.$ActivitySettingID.'&StudentID='.$studentID.'&uploadError[]='.implode(",&uploadError[]=",$UploadError));
}else {
	header('location: index.php?task=mgmt.activity.list_stu&YearClassID='.$YearClassID.'&ActivitySettingID='.$ActivitySettingID);
}
?>