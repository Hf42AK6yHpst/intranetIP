<?php
// Using : 
/*
 * 	Date: 2018-01-11 (Bill)
 * 			Create file
 */

# Student Selection
if($_POST["Type"] == "StudentByClass")
{
	# Get Class Student
	$classStudentAry = array();
	if($_POST["ClassID"]) {
		$classStudentAry = $indexVar["libpowerportfolio"]->Get_Student_By_Class($_POST["ClassID"]);
	}
	
	# Get Excluded Student List
	$excludeStudentAry = array();
	if($_POST["ExcludeStudentIDList"] != '') {
	    $excludeStudentAry = explode(',', $_POST["ExcludeStudentIDList"]);
	}
	
	$dataAry = array();
	foreach((array)$classStudentAry as $studentInfo)
	{
	    if(!empty($excludeStudentAry) && in_array($studentInfo["UserID"], (array)$excludeStudentAry)){
	        continue;
	    }
	    
		$studentName = $studentInfo["StudentName"];
		$studentClassName = $studentInfo["ClassName"];	
		$studentClassNumber = $studentInfo["ClassNumber"];
		$studentDisplay = $studentName." (".$studentClassName."-".$studentClassNumber.")";
		
		$dataAry[] = array($studentInfo["UserID"], $studentDisplay);
	}
	
	$selectTags = " id='".$_POST["SelectionID"]."' name='".$_POST["SelectionName"]."' ".($_POST["isMultiple"] ? "multiple" : "")." size=10 ";
	$studentSelection = getSelectByArray($dataAry, $selectTags, "", $_POST["isAll"], $_POST["noFirst"]);
	if($_POST["withSelectAll"]) {
		$selectAllBtn = $indexVar["libpowerportfolio_ui"]->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('".$_POST["SelectionID"]."', 1);", "selectAllBtn");
	}
	$returnContent = $studentSelection.$selectAllBtn;
	$returnContent .= $indexVar["libpowerportfolio_ui"]->MultiSelectionRemark();
}

echo $returnContent;
?>