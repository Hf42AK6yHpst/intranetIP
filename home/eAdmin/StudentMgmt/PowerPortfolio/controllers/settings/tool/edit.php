<?php
// editing by 
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * 	Date: 2019-09-24 Philips Added Chapter Column
 * 	Date: 2017-10-17 Bill	Support Category Selection
 * 	Date: 2017-10-09 Bill	Use timestamp to prevent display cached images	[2017-0929-1827-43096]
 */

### Page Title
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Tool']['Title']);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);
echo $indexVar['libpowerportfolio_ui']->Include_Thickbox_JS_CSS();

## Taiwan Category
$temp = $indexVar['libpowerportfolio']->Get_Taiwan_Category();
$TWCata = BuildMultiKeyAssoc($temp, "CatID");

### Main Paga Data
if ($isEdit)
{
	$toolSetting = $indexVar['libpowerportfolio']->Get_Tool_Setting($ToolID);
	if(!empty($toolSetting)) $toolSetting = $toolSetting[0];
	$name = $toolSetting['Name'];
	$photoPath = $toolSetting['PhotoSRC'];
	$topicSettingID = $toolSetting['TopicSettingID'];
	$rubricCode = $toolSetting['RubricCode'];
	$yearID = $toolSetting['YearID'];
	$zoneID = $toolSetting['ZoneID'];
	if($photoPath) { 	// Have set photo
		$IsPhotoDispaly = true;
		$photoDispaly = "<img src='".$indexVar['thisImage'].$photoPath."?t=".time()."' style='width:300px'>";
	}
	else { 				// Not yet set photo
		$IsPhotoDispaly = false;
		$photoDispaly = ''; 
	}
	$navigation = $Lang['Btn']['Edit'];
	$ZoneSelection = $indexVar['libpowerportfolio_ui']->Get_Zone_Selection($zoneID, 'ZoneOnChange()', $topicSettingID);
	$TopicSelection = $indexVar['libpowerportfolio_ui']->Get_TopicSetting_Selection($topicSettingID, 'TopicOnChange()', $yearID);
}else{
	$IsPhotoDispaly = false;
	$photoDispaly = '';
	$navigation = $Lang['Btn']['New'];
}

$YearSelection = $indexVar['libpowerportfolio_ui']->Get_Year_Selection($yearID, 'YearOnChange()', false, true, 0, false, true);
$addRubricIndexBtn = $indexVar['libpowerportfolio_ui']->GET_LNK_NEW('javascript:AddRubricIndex()',$Lang['PowerPortfolio']['Settings']['Tool']['AddRubricIndex']);

$navigationAry[] = array($Lang['PowerPortfolio']['Settings']['Tool']['Title'],'index.php?task=settings.tool.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($navigationAry);
### Main Paga Data END

$WarningBox = $indexVar["libpowerportfolio_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>