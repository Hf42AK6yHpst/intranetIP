<?php 

// Not allow to access if data not existed
if(isset($_SESSION['PowerportfolioImportData'])){
	$data = $_SESSION['PowerportfolioImportData'];
	$result = array('Insert'=>0);
	$_lastCatName = '';
	foreach($data as $row){
		list($catName, $comment) = $row;
		if($catName != $_lastCatName){
			$_lastCatName = $catName;
			$table = $indexVar['libpowerportfolio']->DBName.'.RC_COMMENT';
			$cols = "CommentID";
			$conds = "Content = '{$catName}' AND CatID IS NULL";
			$sql = "SELECT $cols FROM $table WHERE $conds";
			$commentInfo = $indexVar['libpowerportfolio']->returnVector($sql);
			if(!empty($commentInfo)){
				$catID = $commentInfo[0];
			} else {
				$catID = $indexVar['libpowerportfolio']->Insert_Comment_Cat(array('Content' => $catName));
			}
		}
		$indexVar['libpowerportfolio']->Insert_Comment($catID, array('Content' => $comment));
		
		$result['Insert']++;
	}
	$htmlAry['ResultMsg'] = "<p>{$Lang['PowerPortfolio']['Setting']['ImportWarning']['InsertCount']}: {$result['Insert']}</p>";
	
	$buttons .= "";
	$buttons .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href = 'index.php?task=settings.comment.list'");
	$htmlAry['Btn'] = $buttons;
	
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
	$pageStep = $indexVar['libpowerportfolio_ui']->GET_STEPS($STEPS_OBJ);
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['PowerPortfolio']['Settings']['Comment']['Title'], "");
	
	$Navigation = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION($PAGE_NAVIGATION);
	
	$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start();
} else {
	No_Access_Right_Pop_Up();
}

// clear data
unset($_SESSION['PowerportfolioImportData']);

?>