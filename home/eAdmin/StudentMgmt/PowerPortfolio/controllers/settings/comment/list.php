<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Comment']['Title']);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$field = ($field=='')? 0 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
$pos = 0;

$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$btnAry[] = array('import', 'javascript: goImport();');
$htmlAry['contentTool'] = $indexVar['libpowerportfolio_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB table action buttons
$btnAry = array();
// $btnAry[] = array('copy', 'javascript: checkCopy();');
// $btnAry[] = array('export', 'javascript: checkExport();');
$btnAry[] = array('edit', 'javascript: checkEdit();');
$btnAry[] = array('delete', 'javascript: checkRemove(document.form1,\'CommentID[]\',\'index.php?task=settings.comment.update_cat&isDelete=1\');');
$htmlAry['dbTableActionBtn'] = $indexVar['libpowerportfolio_ui']->Get_DBTable_Action_Button_IP25($btnAry);
$conds = "cat.CatID IS NULL";
$sql = "SELECT 
            CONCAT('<a href=\"javascript:goEditCat(', cat.CommentID,')\">', cat.Content, '</a>') as CommentCat, 
			CONCAT('<a href=\"javascript:goEdit(', cat.CommentID,')\">', COUNT(cm.CommentID), '</a>') as CommentCount,
            CONCAT('<input type=\'checkbox\' class=\'checkbox\' name=\'CommentID[]\' id=\'CommentID', cat.CommentID, '\' value=', cat.CommentID,'>') as edit_box 
        FROM 
			".$indexVar['thisDbName'].".RC_COMMENT cat
            LEFT OUTER JOIN ".$indexVar['thisDbName'].".RC_COMMENT cm
			ON cm.CatID = cat.CommentID
		WHERE $conds
		Group By cat.CommentID
";

// debug_pr($sql);
// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("CommentCat", "CommentCount","edit_box");
$li->column_array = array(0, 0, 0);
$li->wrap_array = array(0, 0, 0);
$li->no_col = count($li->field_array)+1;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Comment']['Category'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='75%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Comment']['Count'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>" . $indexVar['libpowerportfolio_ui']->Get_Checkbox('checkAll', 'checkAll', '', false, 'checkbox', '',"Check_All_Options_By_Class('checkbox', this.checked);") . "</td>\n";

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='".$li->pageNo."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='".$li->field."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='".$li->page_size."'>";

### Required JS CSS - ThickBox
echo $indexVar["libpowerportfolio_ui"]->Include_Thickbox_JS_CSS();
?>