<?php 

$isEdit = $_REQUEST['isEdit'];
$isDelete = $_REQUEST['isDelete'];

if($isDelete){
	$CommentID = $_REQUEST['CommentID'];
	$CatID = $_REQUEST['CatID'];
	if(!$CatID){
		foreach((array)$CommentID as $cid){
			$result[] = $indexVar['libpowerportfolio']->Delete_Comment_Cat($cid);
		}
		header('location: index.php?task=settings.comment.list');
	} else {
		foreach((array)$CatID as $cid){
			$result[] = $indexVar['libpowerportfolio']->Delete_Comment($cid);
		}
		header('location: index.php?task=settings.comment.edit_cat&CatID='.$CatID);
	}
} else if($isEdit) {
	$CommentID = $_REQUEST['CommentID'];
	$CatID = $_REQUEST['CatID'];
	$content = $_REQUEST['CommentContent'];
	$dataAry = array('Content'=>$content);
	if($CommentID){
		// Update
		if($CatID){
			$result = $indexVar['libpowerportfolio']->Update_Comment($CommentID, $dataAry);
            header('location: index.php?task=settings.comment.edit_cat&CatID='.$CatID);
		}else{
            $result = $indexVar['libpowerportfolio']->Update_Comment_Cat($CommentID, $dataAry);
            header('location: index.php?task=settings.comment.list');
		}
	} else {
		// Insert
		if($CatID){
			$result = $indexVar['libpowerportfolio']->Insert_Comment($CatID, $dataAry);
			header('location: index.php?task=settings.comment.edit_cat&CatID='.$CatID);
		}else{
			$CatID = $indexVar['libpowerportfolio']->Insert_Comment_Cat($dataAry);
			header('location: index.php?task=settings.comment.list');
		}
	}
}
?>