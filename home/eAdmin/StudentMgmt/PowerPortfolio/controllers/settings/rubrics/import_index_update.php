<?php 

// Not allow to access if data not existed
if(isset($_SESSION['PowerportfolioImportData'])){
	$data = $_SESSION['PowerportfolioImportData'];
	$result = array('Insert'=>0);
	$_lastSettingName = '';
	$_lastCatName = '';
	$_lastSubCatName = '';
	
	$table = $table = $indexVar['libpowerportfolio']->DBName.'.RC_RUBRIC_SETTING';
	$cols = "RubricSettingID, Name, LevelNum";
	
	foreach($data as $row){
		list($name,$cat,$subCat,$item) = $row;
		if($name != $_lastSettingName){
			
			if($settingID!=''){
				$rubricsIndexAry[$settingID] = $itemAry;
				$itemAry = array();
			}
			
			$conds = "Name = '{$name}'";
			$sql = "SELECT $cols FROM $table WHERE $conds";
			$settingInfo = $indexVar['libpowerportfolio']->returnArray($sql);
			$settingInfo = $settingInfo[0];
			$settingID = $settingInfo['RubricSettingID'];
			$maxLevel = $settingInfo['LevelNum'];
			
			$_thisCat = 0;
			$_thisSubCat = 0;
			$_thisCount = 1;
			
			$_lastSettingName = $name;
		}
		if($cat != $_lastCatName){
			$_thisCat++;
			$itemAry[] = array(
					'Code' => "{$_thisCat}",
					'Name' => $cat,
					'Type' => 'RubricIndex',
					'Level' => '0',
					'UpperCat' => ''
			);
			$_thisSubCat = 0;
			$_thisCount = 1;
			$_lastCatName = $cat;
		}
		if($subCat!= $_lastSubCatName && $maxLevel > 2){
			$_thisSubCat++;
			$itemAry[] = array(
					'Code' => "{$_thisCat}.{$_thisSubCat}",
					'Name' => $subCat,
					'Type' => 'RubricIndex',
					'Level' => '1',
					'UpperCat' => "{$_thisCat}"
			);
			$_thisCount = 1;
			$_lastSubCatName = $subCat;
		}
		$itemAry[] = array(
				'Code' => ($maxLevel == 2 ? 
						"{$_thisCat}.{$_thisCount}" : 
						"{$_thisCat}.{$_thisSubCat}.{$_thisCount}"),
				'Name' => $item,
				'Type' => 'RubricIndex',
				'Level' => $maxLevel-1,
				'UpperCat' => ($maxLevel == 2 ? "{$_thisCat}" : "{$_thisCat}.{$_thisSubCat}")
		);
		$_thisCount++;
	}
	$rubricsIndexAry[$settingID] = $itemAry;
	foreach($rubricsIndexAry as $rubricSettingID => $itemAry){
		$indexVar['libpowerportfolio']->Delete_Ability_Index_Item($rubricSettingID);
		$returnValue = $indexVar['libpowerportfolio']->Upsert_Rubric_Ability_Index_Item($rubricSettingID,$itemAry);
		$result['Insert']++;
	}
	
	$htmlAry['ResultMsg'] = "<p>{$Lang['PowerPortfolio']['Setting']['ImportWarning']['InsertCount']} ({$Lang['PowerPortfolio']['Settings']['Rubrics']['Title']}): {$result['Insert']}</p>";
	
	$buttons .= "";
	$buttons .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href = 'index.php?task=settings.rubrics.list'");
	$htmlAry['Btn'] = $buttons;
	
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
	$pageStep = $indexVar['libpowerportfolio_ui']->GET_STEPS($STEPS_OBJ);
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['PowerPortfolio']['Settings']['Rubrics']['SubTitle'], "");
	
	$Navigation = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION($PAGE_NAVIGATION);
	
	$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start();
} else {
	No_Access_Right_Pop_Up();
}

// clear data
unset($_SESSION['PowerportfolioImportData']);

?>