<?php
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Zone']['Title']);
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['PowerPortfolio']['Settings']['Zone']['Title'], "");

$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

$ColumnTitleArr = array();
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Zone']['Name'];
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Zone']['TopicCode'];
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Zone']['Quota'];
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Zone']['ImageCode'];

$ColumnPropertyArr = array(1,1,1,1);
$RemarksArr = array();

$RemarksArr[1] = " [<a href='javascript:Load_Reference(\"Topic\")' class='tablelink' id='importRef_Topic'>".$Lang['PowerPortfolio']['Settings']['Zone']['ImportReference']['Topic']."</a>]";
$RemarksArr[3] = " [<a href='javascript:Load_Reference(\"Image\")' class='tablelink' id='importRef_Image'>".$Lang['PowerPortfolio']['Settings']['Zone']['ImportReference']['Image']."</a>]";

$format_str.= $indexVar['libpowerportfolio_ui']->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);

$sample_csv = "<a class=\"tablelink\" href=\"index.php?task=settings.zone.ajax&Action=GetImportSample\">[". $i_general_clickheredownloadsample ."]</a><br>";

$Navigation = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION($PAGE_NAVIGATION);
$pageStep = $indexVar['libpowerportfolio_ui']->GET_STEPS($STEPS_OBJ);
$btnSubmit = $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($button_submit, "submit", "");
$btnCancel = $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($button_cancel, "button", "window.location.href = 'index.php?task=settings.zone.list'");
?>