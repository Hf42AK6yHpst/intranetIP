<?php 
$Action = $_REQUEST['Action'];
switch($Action){
	case 'GetImportReference':
		$targetType = $_REQUEST['Target'];
		$RemarkDetailsArr = array();
		switch($targetType){
			case 'Topic':
				$title = '<th>';
					$title .= $Lang['General']['Code'];
				$title .= '</th>';
				$title .= '<th>';
					$title .= $Lang['PowerPortfolio']['Settings']['Zone']['Topic'];
				$title .= '</th>';
				$topicAry = $indexVar['libpowerportfolio']->Get_Rubric_Topics();
				foreach($topicAry as $topic){
					$RemarkDetailsArr[] = '<td>'.$topic['TopicSettingID'].'</td><td>'.$topic['Name'].'</td>';
				}
				break;
			case 'Image':
				$title = '<th>';
					$title .= $Lang['General']['Code'];
				$title .= '</th>';
				$title .= '<th>';
					$title .= $Lang['PowerPortfolio']['Settings']['Zone']['Iamge'];
				$title .= '</th>';
				for($i=1; $i<6; $i++)
				{
					$PICSelection = "<td>$i</td>";
					$PICSelection .= "<td style='text-align: center'>";
						$PICSelection .= "<img src='".$PowerPortfolioConfig['imageFilePath']."/btn_subj_".$i.".png' width='100px'/>";
					$PICSelection .= "</td>";
					$RemarkDetailsArr[] = $PICSelection;
				}
				break;
			default:
				break;
		}
		$html .= '';
		$thisRemarksType = 'type';
		$html .='<div class = "stickyHeader" id= "stickyRemarks" style = "background-color: #f3f3f3;position: sticky; top: 0; margin-bottom: 1px;">';
			$html .= '<div style="display: inline;">';
			$html .= '&nbsp';
			$html .= '</div>';
			$html .= '<div style="display: inline; float: right; padding-right: 2px;">';
			$html .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
			$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="tableDiv" style="overflow-y: auto; max-height: 250px;">';
			$html .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
				$html .= '<tbody>'."\r\n";
					$html .= '<tr>'."\r\n";
						$html .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
							$html .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
								$html .= '<thead>'."\n";
									$html .= '<tr>'."\n";
										$html .= $title."\n";
									$html .= '</tr>'."\n";
								$html .= '</thead>'."\n";
								$html .= '<tbody>'."\n";
									foreach ($RemarkDetailsArr as $RemarkDetail){
										$html .= '<tr>'."\n";
										$html .= $RemarkDetail."\n";
										$html .= '</tr>'."\n";
									}
								$html .= '</tbody>'."\n";
							$html .= '</table>'."\r\n";
					$html .= '</td>'."\r\n";
					$html .= '</tr>'."\r\n";
				$html .= '</tbody>'."\r\n";
			$html .= '</table>'."\r\n";
		$html .= '</div>';
		
		echo $html;
		break;
	case 'GetImportSample':
		include_once($PATH_WRT_ROOT.'includes/libexporttext.php');
		$libexport = new libexporttext();
		$fileName = "Import_Sample_Zone.csv";
		$ColumnDef = array(
				$Lang['PowerPortfolio']['Settings']['Zone']['Name'],
				$Lang['PowerPortfolio']['Settings']['Zone']['TopicCode'],
				$Lang['PowerPortfolio']['Settings']['Zone']['Quota'],
				$Lang['PowerPortfolio']['Settings']['Zone']['ImageCode']
		);
		$data = array();
		$topicAry = $indexVar['libpowerportfolio']->Get_Rubric_Topics();
		$topic = current($topicAry);
		$data[] = array('Sample Zone', 
				$topic['TopicSettingID'] ? $topic['TopicSettingID'] : '1',
				'10',
				'1'
		);
		
		$content = $libexport->GET_EXPORT_TXT($data, $ColumnDef);
		$libexport->EXPORT_FILE($fileName, $content);
		break;
	default:
		break;
}

?>