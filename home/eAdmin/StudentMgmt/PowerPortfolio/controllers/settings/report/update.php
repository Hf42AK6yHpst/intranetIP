<?php
// Using: 
/*
 */
//$RubricIndexList = $_POST['RubricIndexList'];

$colAry = array(
	'Name' => 'Name',
	'YearID' => 'YearID',
	'StartDate' => 'StartDate',
	'EndDate' => 'EndDate',
	'CoverIncludeInfo' => 'CoverIncludeInfo',
	'CoverTitle' => 'CoverTitle',
	'CoverSubTitle' => 'CoverSubTitle',
	'CAReportTitle' => 'CAReportTitle',
    'TopicID' => 'TopicID',
    'ActivityID' => 'ActivityID',
	'TermAssessmentIncluded' => 'TermAssessmentIncluded',
	'TermAssessmentTitle' => 'TermAssessmentTitle',
	'TermCommentIncluded' => 'TermCommentIncluded',
	'TermCommentTitle' => 'TermCommentTitle',
	'PageLastPageIncludeInfo' => 'PageLastPageIncludeInfo'
);
$dataAry = array();
foreach($colAry as $key => $col){
    if($key == 'TopicID' || $key == 'ActivityID') {
        $dataAry[$col] = implode(',', (array)$_POST[$key]);
    } else {
	    $dataAry[$col] = $_POST[$key];
    }
}

if($isDelete){
	foreach((array)$ReportID as $sid){
		$indexVar['libpowerportfolio']->Delete_Report_Setting($sid);
	}
} else {
	if($ReportID != ''){
		//edit
		$indexVar['libpowerportfolio']->Update_Report_Setting($ReportID, $dataAry);
	} else {
		//new
        $ReportID = $indexVar['libpowerportfolio']->Insert_Report_Setting($dataAry);
	}
}
header('location: index.php?task=settings.report.list');