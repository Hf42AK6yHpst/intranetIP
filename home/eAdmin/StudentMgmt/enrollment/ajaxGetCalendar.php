<?php

#Based on IntranetIP25/StudentMgmt/disciplinev12/reports/ajaxGetSemester.php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$linterface = new interface_html();

$time = strtotime($CurrentMonth);
$MovetoMonth = date ("Y-m-d", mktime(0, 0, 0, date("n",$time) + $Move, 1, date("Y",$time)));

$DateArr = $IsEvent?$libenroll->GET_ENROL_EVENT_DATE($EnrolGroupID):$libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);    
$tableDates = array();
for ($i = 0; $i < sizeof($DateArr); $i++) {	
	if (!in_array(date("Y-m-d", strtotime($DateArr[$i][2])), $tableDates)) {
		$tableDates[] = date("Y-m-d", strtotime($DateArr[$i][2]));
	}
}

echo ($Move==0?"":$MovetoMonth."|=|").$libenroll->GET_CALENDAR($MovetoMonth, "", $tableDates, $EnrolGroupID, $IsEvent);

intranet_closedb();

?>