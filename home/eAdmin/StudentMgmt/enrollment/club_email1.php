<?php
## Using By : Max
###########################################
##	Modification Log:
##	2010-01-08 Max (201001081039)
##	Send Email for Enrollment Result
###########################################
#Based on member_email.php
/*Debug using para*/
$HR = "<hr />";
$BR = "<br />";
/*Debug using para*/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();


$pageSizeChangeEnabled = true;

// This is used in $libenroll->GET_MODULE_OBJ_ARR();
$LibUser = new libuser($UserID);


if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();	
	
	// Page redirection if the user is not admin/master/normal user
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID']))) {
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	}
	
	if ($type == "activity")
	{
		// This is used in $libenroll->GET_MODULE_OBJ_ARR();
		$CurrentPage = "PageMgtActivity";

	}
	else
	{
		// This is used in $libenroll->GET_MODULE_OBJ_ARR();
		$CurrentPage = "PageMgtClub";
		
	}	

	
	// Define the selection of [Record][Process] tabs
	$current_tab = 1;
	
    include_once("management_tabs.php");
        
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		//debug_r($MODULE_OBJ);
		
	/*
	 * S: This is used to check the login status of a user
	 * If a user is valid
	 *  load the page
	 * Else
	 *  shows "You have no priviledge to access this page."
	 */
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();

	    
	    
	    /* Start Used by Steps */
	    $STEPS_OBJ[] = array("Select student(s)", 1);
        $STEPS_OBJ[] = array("Compose email", 0);
	    /* End Used by Steps */
	     
	$ldiscipline = new libdisciplinev12();
	
	$lclass = new libclass();
	$linterface = new interface_html();
	
	# School year
	$currentAcademicYearID = Get_Current_Academic_Year_ID();
	$yearStart = getStartDateOfAcademicYear($currentAcademicYearID);
	$yearEnd = getEndDateOfAcademicYear($currentAcademicYearID);
	
	# Form
	$ClassLvlArr = $lclass->getLevelArray();
	$SelectedFormArr = explode(",", $SelectedForm);
	$SelectedFormTextArr = explode(",", $SelectedFormText);
	$selectFormHTML = "<select name=\"rankTargetDetail[]\" id=\"rankTargetDetail[]\" multiple size=\"5\">\n";
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
		if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
			$selectFormHTML .= " selected";
		}
		$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
	}
	$selectFormHTML .= "</select>\n";

# Class

$ClassListArr = $lclass->getClassList($selectYear);
$ClassListArr = $lclass->getClassList($currentAcademicYearID);

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);
$selectClassHTML = "<select name=\"rankTargetDetail[]\" id=\"rankTargetDetail[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassListArr[$i][0], $SelectedClassArr)) {
		$selectClassHTML .= " selected";
	}
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',1);
$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1,0);
$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,1);

$all_item = array();

# Items
if($record_type==1) {
	$selectedItem = $good_items;	
} else {
	$selectedItem = $mis_items;	
}

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);


$selectItemIDHTML = "<select name=\"ItemID[]\" id=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			if (in_array(addslashes(intranet_undo_htmlspecialchars($r_itemName)), $SelectedItemIDTextArr) && in_array(addslashes(intranet_undo_htmlspecialchars($r_itemID)), $SelectedItemIDArr)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";
$linterface->LAYOUT_START();
?>
<!-- TODO: A javascript of checking form maybe needed -->
<!-- S:copy from IntranetIP25/StudentMgmt/disciplinev12/reports/goodconduct_misconduct/index.php -->
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str, choice)
{
	//alert("showResult :\nstr - "+str+"\nchoice - "+choice+"\nYearID - <?=$currentAcademicYearID?>"+"\nStudentFlag - "+document.getElementById("studentFlag").value);
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
	url = "get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+<?=$currentAcademicYearID?>;
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
</script>

<script language="javascript">

/*S:show or hide the students' list after selected classes*/
function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}
/*E:show or hide the students' list after selected classes*/
function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function doPrint()
{
	document.form1.action = "index_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "_self";
}

function doExport()
{
	document.form1.action = "index_export.php";
	document.form1.submit();
	document.form1.action = "";
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	/*S:Masked for testing*/
	<?php/*
	var choiceSelected;
	var choice = "";
	
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.form1.textFromDate.value > document.form1.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('rankTarget').value=='#') {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
		return false;
	}	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");
		return false;
	}	 	
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	if (document.getElementById('selectMisCat').value!=1 && document.getElementById('selectMisCat').value!=2 && countOption(document.getElementById('ItemID[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_GoodConductMisconduct_Titles?>');
		return false;
	}
	document.getElementById('Period').value = jPeriod;
	document.getElementById('StatType').value = jStatType;
	getSelectedString(document.getElementById('rankTargetDetail[]'), 'SelectedForm');
	getSelectedTextString(document.getElementById('rankTargetDetail[]'), 'SelectedFormText');
	getSelectedString(document.getElementById('rankTargetDetail[]'), 'SelectedClass');
	getSelectedTextString(document.getElementById('rankTargetDetail[]'), 'SelectedClassText');
	getSelectedString(document.getElementById('ItemID[]'), 'SelectedItemID');
	getSelectedTextString(document.getElementById('ItemID[]'), 'SelectedItemIDText');
	document.getElementById('submit_flag').value='YES';
	return true;
	*/?>
return true;
	/*E:Masked for testing*/
}
</script>
<!-- E:copy from IntranetIP25/StudentMgmt/disciplinev12/reports/goodconduct_misconduct/index.php -->

<form name="form1" action='club_email_update.php' method='post' onSubmit="return checkform(this);"><br/>
	<!-- Start Navigation -->
	<table width="100%" border="0" cellspacing="4" cellpadding="4">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
	</table>
	<!-- End Navigation -->
	
	<!-- Start Steps -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/></td>
		</tr>
	</table>
	<!-- End Steps -->
	
	<!-- S:Choose students -->
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
				<table width="88%" border="0" cellpadding="5" cellspacing="0">
					<tr align="left">
						<td width="50">&nbsp;</td>
						<td>
							<!-- S:-option- -->
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
								<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
									<?=$optionString?>
								</tr>
							</table>
							<!-- E:-option- -->
							<!-- S:Option Panel -->
							<span id="spanOptionContent">
							<table width="100%" border="0" cellpadding="5" cellspacing="0" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
								<!-- S:Period -->
								<tr valign="top">
									<td height="57" width="10%" valign="top" nowrap="nowrap" class="formfieldtitle">
										<?=$iDiscipline['Period']?>
									</td>
									<td width="90%">
										<table border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td height="30" colspan="6">
													Year <?=date("Y", strtotime($yearStart))?>-<?=date("Y", strtotime($yearEnd))?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- E:Period -->
								
								<!-- S:Target -->
								<tr valign="top">
									<td valign="top" nowrap="nowrap" class="formfieldtitle">
										<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
									</td>
									<td>
										<table width="0%" border="0" cellspacing="2" cellpadding="0">
											<tr>
												<td valign="top">
													<table border="0">
														<tr>
															<td valign="top">
																<select name="rankTarget" id="rankTarget" onChange="showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
																	<option value="#">-- <?=$i_general_please_select?> --</option>
																	<option value="form" <? if($rankTarget=="form") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
																	<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
																	<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
																</select>
															</td>
															<td>
																<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
																<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5" id="rankTargetDetail[]"></select>
																<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
															</td>
															<td>
																<div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:1;<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
																	<select name="studentID[]" multiple size="5" id="studentID[]"></select>
																	<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
																</div>
															</td>
														</tr>
														<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
													</table>
												</td>
												<td valign="top"><br /></td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- E:Target -->
								
								<!-- S:Remark of Mandatory Field -->
								<tr>
									<td valign="top" nowrap="nowrap">
										<span class="tabletextremark"><?=$i_general_required_field?></span>
									</td>
									<td>&nbsp;</td>
								</tr>
								<!-- S:Remark of Mandatory Field -->
								
								<!-- S:Action Button(s) -->
								<tr>
									<td>&nbsp;</td>
									<!-- TODO make the compose email wording retrieve from the lang file -->
									<td><?= $linterface->GET_ACTION_BTN("Compose Email", "submit", "", "submitBtn01")?>
									</td>
								</tr>
								<!-- S:Action Button(s) -->
							</table>
							</span>
							<!-- E:Option Panel -->
						</td>
					</tr>
				</table>
				<br />
				<?php/* S:sNU
					<?=$detail_table?>
				E:sNU */?>
				<br />
				
			</td>
		</tr>
		<?=$tableButton?>
		<tr>
			<td>
				<span id="spanStatistics" style="display:none"></span>
			<td>
		</tr>
	</table>
	
	<!-- S:Storing parameters to next page -->
	<input type="hidden" name="currentAcademicYearID" id="currentAcademicYearID" value="<?=$currentAcademicYearID?>" />
	
	
	<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
	<?php/*<input type="hidden" name="categoryID" id="categoryID" value="<?=$categoryID?>" />*/?>
	<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
	<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$rankTarget?>" />
	<?php/*<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />*/?>
	<?php/*<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />*/?>
	<?php/*<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />*/?>
	<? if($submit_flag=="YES") {?>
	<?php/*<input type="hidden" name="ByField" id="ByField" value="<?=implode(",", $parByFieldArr)?>" />*/?>
	<?php/*<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />*/?>
	<?php/*<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(stripslashes(implode(",", $parStatsFieldTextArr)))?>" />*/?>
	<?php/*<input type="hidden" name="newItemID" id="newItemID" value="<? if(sizeof($parStatsFieldArr)>0) echo implode(",", $parStatsFieldArr)?>">*/?>
	<? } ?>
	
	
	<input type="hidden" name="SelectedForm" id="SelectedForm" />
	<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
	<input type="hidden" name="SelectedClass" id="SelectedClass" />
	<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
	<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
	<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
	<input type="hidden" name="id" id="id" value="">
	<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">
	<input type="hidden" name="showStatButton" id="showStatButton" value="0" />
	<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
	<!-- E:Storing parameters to next page -->
</form>
<br />
<!-- E:Choose students -->
<?=$initialString?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>

<script language="javascript">
/*S:Initially set [Form] as the preset selection*/
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form","");
}
<?
if($submitBtn01 == "")
	echo "init();\n";
?>
<?
if($submitBtn01 != "") {
	echo "showResult(\"$rankTarget\", \"$jsRankTargetDetail\");";
}
?>
/*E:Initially set [Form] as the preset selection*/

/*S:sNU wont have student initially and hence the get_live.php may also not useful*/
<?php/*
<? if(sizeof($studentID)>0) { ?>
var xmlHttp2;

initialStudent();	
	
function initialStudent() {
	xmlHttp2 = GetXmlHttpObject()
	url = "get_live.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}
<? } ?>

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}
*/?>
/*E:sNU wont have student initially and hence the get_live.php may also not useful*/
changeTerm('<?=$selectYear?>');

//-->
</script>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
    /*E: This is used to check the login status of a user*/
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>