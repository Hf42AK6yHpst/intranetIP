<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();

# Check access right
if (!$plugin['eEnrollment'])
{
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();	
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
{
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageTranserOLE";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], "", 1);

$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step1'], 1);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step2'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step3'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step4'], 0);
		
$linterface->LAYOUT_START();


$RecordType = $RecordType ? $RecordType : "club";

$TypeSelect = '';
$TypeSelect = $eEnrollment['Record_Type'].": <SELECT name='RecordType' onChange='this.form.submit()'>\n";
$TypeSelect.= "<OPTION value='club'  ". ($RecordType=="club"? "selected":"") .">". $eEnrollment['Club_Records'] ."</OPTION>\n";
$TypeSelect.= "<OPTION value='activity' ". ($RecordType=="activity"? "selected":"") .">". $eEnrollment['Activity_Records'] ."</OPTION>\n";
$TypeSelect.= "</SELECT>\n";

# Transfer button
$transferBtn = $linterface->GET_SMALL_BTN($eEnrollment['proceed_to_step2'], "button", "ClickTransfer();");

# Table setting
if($RecordType=="club")
{
	$TitleField = Get_Lang_Selection('b.TitleChinese', 'b.Title');
	$sql = "SELECT 
				$TitleField as title,  
				if(left(min(c.ActivityDateStart),10) = left(max(c.ActivityDateEnd),10), left(min(c.ActivityDateStart),10), concat(left(min(c.ActivityDateStart),10) , ' - ' , left(max(c.ActivityDateEnd),10))) as dateperiod,
				a.OLE_ProgramID,
				a.EnrolGroupID,
				a.Semester
			FROM 
				INTRANET_ENROL_GROUPINFO as a
				LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				LEFT OUTER JOIN INTRANET_ENROL_GROUP_DATE as c ON c.EnrolGroupID  = a.EnrolGroupID
			WHERE
				(c.RecordStatus IS NULL OR c.RecordStatus = 1)
				AND
				b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				And
				b.RecordType = 5
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				$TitleField
			";
	$infoArr = $libenroll->returnArray($sql, 4);
	
	# Build GroupIDArr
	$EnrolGroupIDArr = array();
	$infoSize = count($infoArr);
	for ($i=0; $i<$infoSize; $i++)
	{
		$EnrolGroupIDArr[] = $infoArr[$i]['EnrolGroupID'];
	}
	
	# Get number of members of each group
	$numOfMemberArr = $libenroll->GET_ARR_NUMBER_OF_MEMBER($EnrolGroupIDArr, "club");
	
	# Build Associate Array
	// $numOfMemberMap[#GroupID] = #numOfMember
	$numOfMemberMap = array();
	$infoMember = count($numOfMemberArr);
	for ($i=0; $i<$infoMember; $i++)
	{
		$thisGroupID = $numOfMemberArr[$i]['EnrolGroupID'];
		$thisNumOfMember = $numOfMemberArr[$i]['COUNT(*)'];
		$numOfMemberMap[$thisGroupID] = $thisNumOfMember;
	}
	
	
	$WarningMsgBox = $linterface->Get_Warning_Message_Box('', $Lang['eEnrolment']['TransferBasedOnUILang'], $others="");
}
else
{
	# Get Current Year Club
	$CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club();
	$EnrolGroupID_conds = '';
	if (count($CurrentYearClubArr) > 0)
	{
		$CurrentYearClubList = implode(',', $CurrentYearClubArr);
		$EnrolGroupID_conds = " Or a.EnrolGroupID In ($CurrentYearClubList) ";
	}
		
	$sql ="
		SELECT
			a.EventTitle as title,
			if(left(min(b.ActivityDateStart),10) = left(max(b.ActivityDateEnd),10), left(min(b.ActivityDateStart),10), concat(left(min(b.ActivityDateStart),10) , ' - ' , left(max(b.ActivityDateEnd),10))) as dateperiod,
			a.OLE_ProgramID,
			a.EnrolEventID
		FROM
			INTRANET_ENROL_EVENTINFO as a
			LEFT JOIN INTRANET_ENROL_EVENT_DATE as b on b.EnrolEventID = a.EnrolEventID
		WHERE
			(a.EnrolGroupID = '' Or a.EnrolGroupID Is Null $EnrolGroupID_conds)
			And (b.RecordStatus IS NULL OR b.RecordStatus = 1)
		GROUP BY
			a.EnrolEventID
		ORDER BY
			a.EventTitle
	";
	$infoArr = $libenroll->returnArray($sql, 4);
	
	# Build GroupIDArr
	$EnrolGroupIDArr = array();
	$infoSize = count($infoArr);
	for ($i=0; $i<$infoSize; $i++)
	{
		$EnrolGroupIDArr[] = $infoArr[$i]['EnrolEventID'];
	}
	
	# Get number of members of each group
	$numOfMemberArr = $libenroll->GET_ARR_NUMBER_OF_MEMBER($EnrolGroupIDArr, "activity");
	
	# Build Associate Array
	// $numOfMemberMap[#GroupID] = #numOfMember
	$numOfMemberMap = array();
	$infoMember = count($numOfMemberArr);
	for ($i=0; $i<$infoMember; $i++)
	{
		$thisGroupID = $numOfMemberArr[$i]['EnrolEventID'];
		$thisNumOfMember = $numOfMemberArr[$i]['COUNT(*)'];
		$numOfMemberMap[$thisGroupID] = $thisNumOfMember;
	}
}

# Build Table
$display .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' bgcolor='#CCCCCC'>\n";

# Title Row
$display .= "<tr>\n";
	$display .= "<td class='tabletop tabletopnolink'>#</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>".$ec_iPortfolio['title']."</td>\n";
	if ($RecordType=="club")
	{
		$display .= "<td class='tabletop tabletopnolink'>".$i_SettingsSemester."</td>\n";
	}
	$display .= "<td class='tabletop tabletopnolink'>".$eEnrollment['Date'] ." / " . $eEnrollment['Period']."</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>".$eEnrollment['No_of_Student']."</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>". $eEnrollment['Already_Transfered'] ."</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>";
		$display .= "<input type='checkbox' onClick=\"(this.checked)?setChecked(1,this.form,'ID[]'):setChecked(0,this.form,'ID[]')\">";
	$display .= "</td>\n";
$display .= "</tr>\n";

# Contents
for ($i=0; $i<$infoSize; $i++)
{
	list($thisTitle, $thisDatePeriod, $thisOLE_ProgramID, $thisID, $Semester) = $infoArr[$i];
	$count = $i+1;
	$css = ($i % 2) ? "tablerow1" : "tablerow2";
	
	$display .= "<tr>\n";
		# count
		$display .= "<td class='$css tabletext'>$count</td>\n";
		# Title
		$display .= "<td class='$css tabletext'>$thisTitle</td>\n";
		# Semester
		if ($RecordType=="club")
		{
			if ($Semester == '' || $Semester == 0)
				$Semester = $i_ClubsEnrollment_WholeYear;
			else
			{
				$objTerm = new academic_year_term($Semester);
				$Semester = $objTerm->Get_Year_Term_Name();
			}
			$display .= "<td class='$css tabletext'>$Semester</td>\n";
		}
		# Date / Period
		$display .= "<td class='$css tabletext'>$thisDatePeriod</td>\n";
		# Number of Students
		if ($numOfMemberMap[$thisID] > 0)
		{
			# add link to the number if members > 0
			$thisDisplay = "<a href='javascript:void(0)' class='tablelink' onClick='javascript:newWindow(\"student_list_popup.php?RecordType=$RecordType&FromStep=1&ParID=$thisID\", 10)'>";
			$thisDisplay .= $numOfMemberMap[$thisID];
			$thisDisplay .= "</a>";
		}
		else
		{
			$thisDisplay = 0;
		}
		$display .= "<td class='$css tabletext'>".$thisDisplay."</td>\n";		
		# Already Transfer
		$display .= "<td class='$css tabletext'>";
		$display .= ($thisOLE_ProgramID != NULL)? $i_general_yes : $i_general_no;
		$display .= "</td>\n";
		# Checkbox
		$display .= "<td class='$css tabletext'>";
		if ($thisDatePeriod == NULL || $numOfMemberMap[$thisID] == NULL || $numOfMemberMap[$thisID] == 0)
		{
			# disable checkbox if no student or no activity date
			$display .= "<input type='checkbox' DISABLED>";
		}
		else
		{
			$display .= "<input type='checkbox' name=ID[] value='$thisID'>";
		}
		$display .= "</td>\n";
	$display .= "</tr>\n";
}

$display .= "</table>\n";

?>
<script language="javascript">
<!--
function ClickTransfer()
{
	var obj = document.form1;
	var element = "ID[]";
	
	if(countChecked(obj,element)>0) 
	{
 		obj.action='transfer_ole2.php';
 		obj.submit();
    } else {
		alert(globalAlertMsg2);
    }
}
//-->
</script>

<br />
<form name="form1" method="get" action="transfer_ole.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr><td align="center"><?=$WarningMsgBox?></td></tr>
<tr>
    <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    	<td align="right" valign="bottom" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                    </tr>

                    <tr>
                    	<td align="left" valign="bottom">&nbsp;<?=$TypeSelect?></td>
                    	<td align="right" valign="bottom" height="28"><?=$transferBtn?></td>
                    </tr>
                    
                    <tr>
                    	<td align="center" colspan='2'>
                    		<?=$display?>
                    	</td>
                    </tr>


                </table>
            </td>
        </tr>
        </table>
    </td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="order" value="<?=$li->order?>" />

</form>
</br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>