<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$avgSourceAry = $_POST['exportAry'];
$numRow = count($avgSourceAry);
$dataAry = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry[$i][$_col++] = $avgSourceAry[$i][0];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][1];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][2];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][3];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][4];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][5];
}
//debug_pr($dataAry);
    //$htmlAry['resultTable'] = $libenroll_report->getStudentWithoutEnrolmentReportResultTableHtml($dataAry);
$resultTable = "";
$resultTable .= "<table class='common_table_list_v30 view_table_list_v30' border='1'>";
$resultTable .= "<tr>";
$resultTable .= "<th>Name</th>";
$resultTable .= "<th style=\"text-align:center;\">Semester</th>";
$resultTable .= "<th style=\"text-align:center;\">Member</th>";
$resultTable .= "<th style=\"text-align:center;\">Time(s) of Activity</th>";
$resultTable .= "<th style=\"text-align:center;\">Activity Hour(s)</th>";
$resultTable .= "<th style=\"text-align:center;\">Average Attendance</th>";
$resultTable .= "</tr>";
for ($j=0; $j<$numRow; $j++){
    $resultTable .= "<tr>";
    $resultTable .= "<td>". $dataAry[$j][0]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][1]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][2]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][3]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][4]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][5]. "</td>";
    $resultTable .= "</tr>";
}
$resultTable .= "</table>";
?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<? echo ($resultTable)?>
<?
intranet_closedb();
?>