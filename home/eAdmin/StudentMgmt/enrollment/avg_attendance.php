<?php
// using: 

// #########################################
// Date: 2020-03-31 Tommy
// - modifided permission checking
//
// Date: 2020-01-30 Tommy
// - Add permission checking
// - set max chart size to fix some chart being too big and not shown
//
// Date: 2018-06-20 Vito
// Add export and print function
//
// Date: 2018-03-19 Philips
// Add HighCharts
//
// Date: 2015-12-1 Kenneth
// Add Category Filter, with Sql edited
//
// Date: 2013-06-08 Shan
// updated UI table style
//
// Date: 2015-04-13 Omas
// Added column 'number of member','Activity times', 'Activity Hours'
// Performance imporvement
//
// Date: 2013-01-04 Rita
// amend Average Attendance % calculation - exclude date (with all exempt)
//
// Date: 2012-12-11 Rita
// amend Average Attendance % calculation
//
// Date: 2011-09-08 YatWoon
// lite version should not access this page
//
// Date: 2011-03-10 YatWoon
// Fixed: activity > only retrieve current year's activity
//
// Date: 2011-02-24 YatWoon
// Fixed cannot display title if selected "Activity", due to activity hasn't chinese title
//
// #########################################
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");
// include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

// $libgroup = new libgroup($GroupID);

if ($plugin['eEnrollment'] && ! $plugin['eEnrollmentLite']) {
    include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
    $libenroll = new libclubsenrol();
    
    $LibUser = new libuser($UserID);
    
    $CurrentPage = "PageMgtActAvgAttendance";
    $CurrentPageArr['eEnrolment'] = 1;
    
    $MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
    
    $TAGS_OBJ[] = array(
        $eEnrollmentMenu['act_event_status_average_attendance'],
        "",
        1
    );
    
    $libenroll = new libclubsenrol();
    
    $role = array("Admin", "ClassTeacher");
    
    if ($libenroll->hasAccessRight($_SESSION['UserID'], $role)) {
        include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
        
        $linterface = new interface_html();    
        $linterface->LAYOUT_START();
        
        if ($showTarget == "club") {
            
            $title_order = $intranet_session_language == "en" ? "ig.Title" : "ig.TitleChinese";
            $SemNameField = ($intranet_session_language == 'en') ? 'YearTermNameEN' : 'YearTermNameB5';
            // ############################################################################################
            //
            // Edit by Kenneth
            // - Add Category Filter
            // - Edited SQL
            //
            // ############################################################################################
            // $sql = "SELECT
            // iegi.EnrolGroupID,
            // ig.Title,
            // IF (iegi.Semester='' OR iegi.Semester IS NULL, '$i_ClubsEnrollment_WholeYear', ayt.$SemNameField),
            // ig.TitleChinese
            //
            // FROM
            // INTRANET_ENROL_GROUPINFO as iegi
            // INNER JOIN
            // INTRANET_GROUP as ig
            // ON (iegi.GroupID = ig.GroupID)
            // Left Outer Join
            // ACADEMIC_YEAR_TERM as ayt
            // On (iegi.Semester = ayt.YearTermID)
            //
            // WHERE
            // ig.RecordType = 5
            // And
            // ig.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
            // ORDER BY
            // ". $title_order;
            
            $sql = 'SELECT
						iegi.EnrolGroupID,
						ig.Title,
						IF(iegi.Semester="" OR iegi.Semester IS NULL, "' . $i_ClubsEnrollment_WholeYear . '",ayt.' . $SemNameField . '),
						ig.TitleChinese
					FROM
						INTRANET_USERGROUP as iug
						Left Outer Join INTRANET_USER as iu On (iug.UserID = iu.UserID And iu.RecordType = 2) 
						INNER JOIN INTRANET_ENROL_GROUPINFO as iegi ON  (iug.GroupID = iegi.GroupID) 
						INNER JOIN INTRANET_GROUP as ig ON (iegi.GroupID = ig.GroupID)
						Left Outer Join 
						ACADEMIC_YEAR_TERM as ayt
						On (iegi.Semester = ayt.YearTermID)		
					WHERE 
						
						ig.AcademicYearID = "' . Get_Current_Academic_Year_ID() . '"
						AND iegi.GroupCategory IN (\'' . implode("','", (array) $targetCategory) . '\')
					GROUP BY
						iug.EnrolGroupID';
//             debug_pr($sql);
            $result = $libenroll->returnArray($sql);
        } else if ($showTarget == "activity") { 
            // only retrieve current year activity
            // Get Current Year Club
            $CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club();
            $EnrolGroupID_conds = '';
            if (count($CurrentYearClubArr) > 0) {
                $CurrentYearClubList = implode(',', $CurrentYearClubArr);
                $EnrolGroupID_conds = " Or EnrolGroupID In ($CurrentYearClubList) ";
            }
            
            // $sql = "SELECT EnrolEventID, EventTitle FROM INTRANET_ENROL_EVENTINFO
            // where
            // (EnrolGroupID = '' Or EnrolGroupID Is Null $EnrolGroupID_conds)
            // AND AcademicYearID='".Get_Current_Academic_Year_ID()."'
            // ORDER BY EventTitle";
            
            $sql = "SELECT 
	       			EnrolEventID, 
	       			EventTitle 
	       			FROM INTRANET_ENROL_EVENTINFO 
	       					
	       			WHERE
	       			(EnrolGroupID = '' Or EnrolGroupID Is Null $EnrolGroupID_conds)
					AND AcademicYearID='" . Get_Current_Academic_Year_ID() . "'
					AND EventCategory IN ( '" . implode("','", (array) $targetCategory) . "' ) 
	       			ORDER BY EventTitle";
            $result = $libenroll->returnArray($sql, 2);
        }
        
        // Tool bar
        if (isset($showTarget)) {
            $toolbarDivDisplay = '';
        }
        else {
            $toolbarDivDisplay = 'display:none;';
        }
        $x = '';
        $x .= '<div id="contentToolDiv" class="content_top_tool" style="'.$toolbarDivDisplay.'">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
        $x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $htmlAry['contentToolDiv'] = $x;
        
        $tableContent = '';
        $tableContent .= $htmlAry['contentToolDiv'];
        $tableContent .= '<br />';
        $tableContent .= "<table width='100%' cellpadding='0' cellspacing='0' border='0'   class='common_table_list_v30 view_table_list_v30'>";
        $tableContent .= "<tr class='tablebluetop tabletopnolink'><th warp='nowrap' width='40%'>";
        $tableContent .= ($showTarget == "club") ? $eEnrollmentMenu['club'] : $eEnrollmentMenu['activity'];
        $tableContent .= "</th>";
        
        if ($showTarget == "club") {
            $tableContent .= "<th warp='nowrap' width='20%' style=\"text-align:center\">" . $i_SettingsSemester . "</th>";
        }
        $displayLangNumPeople = ($showTarget == "club") ? $eEnrollment['member'] : $eEnrollment['participant'];
        $tableContent .= "<th warp='nowrap' width='10%' style=\"text-align:center\">" . $displayLangNumPeople . "</td>";
        $tableContent .= "<th warp='nowrap' width='10%' style=\"text-align:center\">" . $Lang['eEnrolment']['Club_Activity_Times'] . "</th>";
        $tableContent .= "<th warp='nowrap' width='10%' style=\"text-align:center\">" . $Lang['eEnrolment']['Club_Activity_Hours'] . "</th>";
        $tableContent .= "<th warp='nowrap' width='10%' style=\"text-align:center\">" . $eEnrollment['AverageAttendance'] . "</th></tr>";
        
        // add num Member (Omas 2015-04-13)
        // reducing num of query by move out of the loop (Omas 2015-04-13)
        if ($showTarget == "club") {
            // debug_pr($result);
            $targetEnrolGroupIDArr = Get_Array_By_Key($result, '0');
            /**
             * multiple Enrol Group has bug (find by Frankie), please don't call function with all Group ID
             */
            // $ClubAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($targetEnrolGroupIDArr,'',$AcademicYearID);
            if (count($targetEnrolGroupIDArr) > 0) {
                $ClubAttendanceInfoArr = array();
//                 debug_pr("ID is " . $AcademicYearID);
                foreach ($targetEnrolGroupIDArr as $kk => $vv) {
//                     debug_pr($vv);
                    $tmp = $libenroll->Get_Club_Attendance_Info(array($vv), '', $AcademicYearID);
//                     debug_pr($tmp);
                    $ClubAttendanceInfoArr[$vv] = $tmp[$vv];
//                     debug_pr($tmp[$vv]);
                }
            }
//             debug_pr($libenroll->Get_Club_Attendance_Info(array($vv), '', $AcademicYearID));
            // debug_pr($ClubAttendanceInfoArr[789]);
            // Num of group Member
            $sql = 'SELECT
						EnrolGroupID,
						Count(*) as numMember
					FROM
						INTRANET_USERGROUP as iug
						Left Outer Join INTRANET_USER as iu On (iug.UserID = iu.UserID And iu.RecordType = 2)
					WHERE 
						EnrolGroupID IN ("' . implode('","', $targetEnrolGroupIDArr) . '")
						AND iu.UserID IS NOT NULL
					GROUP BY
						EnrolGroupID
					';
//             debug_pr($sql);
            $GroupNumMemberAry = $libenroll->returnResultSet($sql);
//             debug_pr($GroupNumMemberAry);
            $GroupNumMemberAssoAry = BuildMultiKeyAssoc($GroupNumMemberAry, 'EnrolGroupID', array(
                'numMember'
            ), 1);
            // debug_pr($GroupNumMemberAssoAry);
        } else if ($showTarget == "activity") {
            $targetEnrolEventIDArr = Get_Array_By_Key($result, '0');
            $EventAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($targetEnrolEventIDArr, '', $AcademicYearID);
            
            // Num of group Member
            $sql = 'SELECT
						EnrolEventID,
						Count(*) as numMember
					FROM
						INTRANET_ENROL_EVENTSTUDENT as iees
						Left Outer Join INTRANET_USER as iu On (iees.StudentID = iu.UserID And iu.RecordType = 2)
					WHERE 
						EnrolEventID IN ("' . implode('","', $targetEnrolEventIDArr) . '")
						AND iu.UserID IS NOT NULL
						AND iees.RecordStatus = 2
					GROUP BY 
						EnrolEventID';
            // debug_pr($sql);
            $EventNumMemberAry = $libenroll->returnResultSet($sql);
            // debug_pr($EventNumMemberAry);
            $EventNumMemberAssoAry = BuildMultiKeyAssoc($EventNumMemberAry, 'EnrolEventID', array(
                'numMember'
            ), 1);
        }
        
        if (sizeof($result) == 0) {
            $tableContent .= "<tr><td align='center' colspan='2' class='tabletext tablerow2' height='80'>$i_no_record_exists_msg</td></tr>\n";
        }
        
        // Loop each club / activity
        $exportHiddenField = '';
        for ($k = 0; $k < sizeof($result); $k ++) { // foreach club / activity
            
            $css = ($k % 2 == 0) ? 1 : 2;
            
            list ($GroupID, $name, $semName, $TitleChinese, $NumOfMember) = $result[$k];
            
            if ($showTarget == "club") {
                // $EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
                $EnrolGroupID = $GroupID;               
//                 debug_pr($EnrolGroupID);
                // comment by omas 20150413
                // $DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);
                // GroupDateID, EnrolGroupID, ActivityDateStart, ActivityDateEnd, DateModified, LastModifiedBy, LastModifiedRole              
                $DisplayName = $intranet_session_language == "en" ? $name : $TitleChinese;
                // 2013-01-04 added
                // ## Get Club Attendance Records
                $AcademicYearID = Get_Current_Academic_Year_ID();
//                 debug_pr($AcademicYearID);
                // $ClubAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($EnrolGroupID,'',$AcademicYearID);
//                 debug_pr($ClubAttendanceInfoArr[$EnrolGroupID]['AttendancePercentageRounded']);
                if (isset($GroupNumMemberAssoAry[$GroupID])) {
                    $NumOfMember = $GroupNumMemberAssoAry[$GroupID];
                } else {
                    $NumOfMember = '0';
                }
                
                $totalMeetingDate = count($ClubAttendanceInfoArr[$GroupID]['MeetingDateArr']);
//                 debug_pr($totalMeetingDate);
                $totalMeetingHours = 0;
                if ($totalMeetingDate > 0) {
                    foreach ($ClubAttendanceInfoArr[$GroupID]['MeetingDateArr'] as $MeetingdayAry) {
                        $timeDifferentAry = explode(':', $MeetingdayAry['Hours']);
                        $hours = $timeDifferentAry[0];
                        $minutes = $timeDifferentAry[1];
                        $totalMeetingHours = $totalMeetingHours + $hours + ($minutes / 60);
                        $totalMeetingHours = round($totalMeetingHours, 2);
                    }
//                     debug_pr($totalMeetingHours);
//                     debug_pr($ClubAttendanceInfoArr);
                }
                $totalAttendance = $ClubAttendanceInfoArr[$EnrolGroupID]['AttendancePercentageRounded'];
//                 debug_pr($totalAttendance);
            } else if ($showTarget == "activity") {
                // echo $GroupID.'/'.$name.'/';
                $EnrolEventID = $GroupID;
                // comment by omas 20150413
                // $EventInfoArr = $libenroll->IS_EVENTINFO_EXISTS($GroupID);
                // $DateArr = $libenroll->GET_ENROL_EVENT_DATE($EventInfoArr);
                $DisplayName = $name;
                
                // 2013-01-04 added
                // ## Get Club Attendance Records
                $AcademicYearID = Get_Current_Academic_Year_ID();
                // comment by omas 20150413
                // $EventAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($EnrolEventID,'',$AcademicYearID);
                $totalAttendance = $EventAttendanceInfoArr[$EnrolEventID]['AttendancePercentageRounded'];
                
                if (isset($EventNumMemberAssoAry[$GroupID])) {
                    $NumOfMember = $EventNumMemberAssoAry[$GroupID];
                } else {
                    $NumOfMember = '0';
                }
                
                $totalMeetingDate = count($EventAttendanceInfoArr[$EnrolEventID]['MeetingDateArr']);
                
                $totalMeetingHours = 0;
                if ($totalMeetingDate > 0) {
                    foreach ($EventAttendanceInfoArr[$EnrolEventID]['MeetingDateArr'] as $MeetingdayAry) {
                        $timeDifferentAry = explode(':', $MeetingdayAry['Hours']);
                        $hours = $timeDifferentAry[0];
                        $minutes = $timeDifferentAry[1];
                        $totalMeetingHours = $totalMeetingHours + $hours + ($minutes / 60);
                        $totalMeetingHours = round($totalMeetingHours, 2);
                    }
                }
            }
            
            // $totalAttendance = array();
            
            // $ParArr['RecordStatus'] = 2;
            // if($showTarget=="club"){
            // $ParArr['EnrolGroupID'] = $EnrolGroupID;
            // }else{
            // $ParArr['EnrolEventID'] = $EnrolEventID;
            // }
            // $StuList = ($showTarget=="club") ? $libenroll->GET_APPLY_STUDENT($ParArr) : $libenroll->GET_APPLY_EVENT_STUDENT($ParArr);
            //
            // //echo $name.' | '.sizeof($StuList).' | '.sizeof($DateArr)."<br>";
            //
            // $totalMeeting = 0;
            // $attended = 0;
            // $exempted = 0;
            // $numOfAttendancePerDayArray = 0;
            // $attendancePerDayArray = array();
            //
            // # Loop each date
            // $numOfDateArr = count($DateArr);
            // for ($i = 0; $i<$numOfDateArr; $i++) {
            // $totalMeetingPerDay = 0;
            // $attendedPerDay = 0;
            // $exemptedPerDay = 0;
            //
            // $tempGroupDateID = $DateArr[$i]['GroupDateID'];
            //
            // if($showTarget=="club")
            // {
            // $AttendDataArr['GroupDateID'] = $DateArr[$i][0];
            // $AttendDataArr['EnrolGroupID'] = $EnrolGroupID;
            // }
            // else
            // {
            // $AttendDataArr['EventDateID'] = $DateArr[$i][0];
            // $AttendDataArr['EnrolEventID'] = $EnrolEventID;
            // }
            //
            // # Loop each student
            // $numOfStuList = count($StuList);
            // for ($j = 0; $j<$numOfStuList; $j++) { # foreach student in the club/activity
            // $thisStudent = new libuser($StuList[$j][0]);
            // $thisStudentID = $StuList[$j][0];
            //
            // $AttendDataArr['StudentID'] = $StuList[$j][0];
            // $attend = ($showTarget=="club") ? $libenroll->IS_ATTEND_GROUP_MEETING($AttendDataArr) : $libenroll->IS_ATTEND_EVENT_MEETING($AttendDataArr);
            // $exempt = ($showTarget=="club") ? $libenroll->IS_EXEMPT_GROUP_MEETING($AttendDataArr) : $libenroll->IS_EXEMPT_EVENT_MEETING($AttendDataArr);
            //
            // if (date("Y-m-d", strtotime($DateArr[$i][2])) <= date("Y-m-d")) {
            // $totalMeetingPerDay++;
            // $totalArr[date("Ymd", strtotime($DateArr[$i][2]))]++;
            // if ($attend){
            // $attendedPerDay++;
            // //$addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
            // }
            // if ($exempt){
            // $exemptedPerDay++;
            // }
            // }
            //
            // }
            //
            // $totalMeetingExceptExemptPerDay = $totalMeetingPerDay - $exemptedPerDay;
            // //$attendancePerDay = (($totalMeetingPerDay > 0)&&($attendedPerDay > 0)) ? my_round(($attendedPerDay / $totalMeetingPerDay * 100), 2): 0;
            //
            // # Exculde future date records
            // $thisActivityDateStart = $DateArr[$i]['ActivityDateStart'];
            // if ($libenroll->Is_Future_Date($thisActivityDateStart) == true){
            // $attendancePerDay = '---';
            // }
            // else
            // {
            // if(($totalMeetingPerDay > 0)&&($attendedPerDay > 0)){
            // $attendancePerDay = my_round(($attendedPerDay / $totalMeetingExceptExemptPerDay * 100), 2);
            // }else{
            // $attendancePerDay = 0;
            // }
            //
            // if($totalMeetingPerDay==$exemptedPerDay & $exemptedPerDay!=0){
            // //do nth
            // }else{
            // $attendancePerDayArray[] = $attendancePerDay;
            // }
            // }
            // }
            
            // $attendance = (($totalMeeting > 0)&&($attended > 0)) ? intval($attended / $totalMeeting * 100) : 0;
            // if (sizeof($DateArr)>0 && ($totalMeeting > 0) && ($attended > 0))
            // $totalAttendance[] = $attended / $totalMeeting * 100;
            //
            
            // Caculate attendace
            // comment 2015-04-13 Omas
            // $numOfAttendancePerDayArray = count($attendancePerDayArray);
            // if($numOfAttendancePerDayArray>0){
            //
            // $totalAttendance = array_sum($attendancePerDayArray)/$numOfAttendancePerDayArray;
            // }
            $tableContent .= "<tr ><td>" . $DisplayName . "</td>";
            // debug_pr($DisplayName);
            if ($showTarget == "club") {
                $tableContent .= "<td style=\"text-align:center\">" . $semName . "</td>";
            }
            $tableContent .= "<td style=\"text-align:center\">" . $NumOfMember . "</td>";
            $tableContent .= "<td style=\"text-align:center\">" . $totalMeetingDate . "</td>";
            $tableContent .= "<td style=\"text-align:center\">" . $totalMeetingHours . "</td>";
            
            // $tableContent .= "<td style=\"text-align:center\">".$libenroll->getAverage($totalAttendance, 2, "", "%")."</td></tr>";
            
            $tableContent .= "<td style=\"text-align:center\">" . $libenroll->getAverage($totalAttendance, 2, "", "%") . "</td></tr>";

            $exportHiddenField .= '<input type="hidden" name="exportAry['.$k.'][0]" value="'.$DisplayName.'" />';
            $exportHiddenField .= '<input type="hidden" name="exportAry['.$k.'][1]" value="'.$semName.'" />';
            $exportHiddenField .= '<input type="hidden" name="exportAry['.$k.'][2]" value="'.$NumOfMember.'" />';
            $exportHiddenField .= '<input type="hidden" name="exportAry['.$k.'][3]" value="'.$totalMeetingDate.'" />';
            $exportHiddenField .= '<input type="hidden" name="exportAry['.$k.'][4]" value="'.$totalMeetingHours.'" />';
            $exportHiddenField .= '<input type="hidden" name="exportAry['.$k.'][5]" value="'.$libenroll->getAverage($totalAttendance, 2, "", "%").'" />';
            //echo ($exportAry[$k][5]);
        }
        $tableContent .= "</table>";
//         debug_pr($ClubAttendanceInfoArr);
        // ## Club Category Selection
        $ClubCategoryArr = $libenroll->GET_CATEGORY_LIST();
        // debug_pr($ClubCategoryArr);
        if (count($targetCategory) == 0) {
            $targetCategory = Get_Array_By_Key($ClubCategoryArr, 'CategoryID');
        }
        $CategorySelection = getSelectByArray($ClubCategoryArr, " id=\"targetCategory\" name=\"targetCategory[]\" multiple size=10 ", $targetCategory, 0, 1, "- " . $i_general_please_select . " -");
        $CategorySelection .= "&nbsp;" . $linterface->Get_Small_Btn($Lang['Btn']['SelectAll'], "button", "js_Select_All_With_OptGroup('targetCategory',1)");
        
        $CategorySelection .= $linterface->spacer();
        $CategorySelection .= $linterface->MultiSelectionRemark();
        
        $categoryRowHTML = '';
        $categoryRowHTML .= "<tr>";
        $categoryRowHTML .= "<td class='field_title'>" . $linterface->RequiredSymbol() . $eEnrollment['category'] . "</td>";
        $categoryRowHTML .= "<td valign='top' class='tabletext''>";
        $categoryRowHTML .= $CategorySelection; // Club selection list
        $categoryRowHTML .= "</td>";
        $categoryRowHTML .= "</tr>";
        
        ?>
<script language="javascript">
		$( document ).ready(function() {
  			$('#showTargetSelect option[value="<?=$showTarget?>"]').attr('selected', 'selected');
		});
		
		function checkForm(){
			var selector = $('#showTargetSelect').val();
			if(selector=='#'){
				alert("Please select a target");
				return;
			}
			
			var selectedValue = $('#targetCategory').val();
			if(selectedValue == null){
				alert("<?= $Lang['eEnrolment']['Warning']['SelectCategory'] ?>");
			}else{
				form1.submit();
			}
		}
	</script>
	
	<script type = "text/javascript">
	function refreshReport() {
		var canSubmit = true;
		var isFocused = false;
		
		$('div.warnMsgDiv').hide();
		
		// choose enrollment source
		var enrolmentSourceChkName = getJQuerySaveId('enrolmentSourceAry[]');
		if ($('input[name="' + enrolmentSourceChkName + '"]:checked').length == 0) {
			canSubmit = false;
			$('div#enrolmentSourceWarnMsgDiv').show();
			if (!isFocused) {
				$('input#enrolmentSourceChk_club').focus();
				isFocused = true;
			}
		}
		
		// choose club term if selected club as enrollment source
		if ($('input#enrolmentSourceChk_club').attr('checked')) {
			if ($('input.clubTermChk:checked').length == 0) {
				canSubmit = false;
				$('div#clubTermWarnMsgDiv').show();
				if (!isFocused) {
					$('input#clubTermId_0').focus();
					isFocused = true;
				}
			}
		}
		
		var targetCategories = $('#targetCategory').val();
		
		
		
		if (canSubmit) {
			Block_Document();
			if (Trim($('div#reportOptionOuterDiv').html()) == '') {
				saveFormValues();
				
				$('div#reportOptionDiv > div').clone(true, true).appendTo('div#reportOptionOuterDiv');
				$('div#reportOptionDiv').html('');
				$('div#showHideOptionDiv').show();		
				loadFormValues();
			}
			else {
				$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
			}
			
			$('div#reportResultDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
			$.ajax({
				url:      	"ajax_reload.php",
				type:     	"POST",
				data:     	$("#form1").serialize() + '&RecordType=reportResult'+ '&targetCategory='+targetCategories,
				success:  	function(xml) {
								$('div#reportResultDiv').html(xml);
								Scroll_To_Top();
								UnBlock_Document();
								createChart(); // Added by Philips (For highcharts graph)
				}
			});
		}
	}
	function goExport() {
//		$('input#selectedAvg').val(<?php echo ($exportAry); ?>);
		$('form#form2').attr('target', '_self').attr('action', 'export_avg_attendance.php').submit();
	}
	function goPrint() {
		$('form#form2').attr('target', '_blank').attr('action', 'print_avg_attendance.php').submit();
	}
	</script>
<br />
<form id="form1" name="form1" method="post">
	<table class="form_table_v30">
		<tr>
			<td class="field_title" valign="top"><?=$linterface->RequiredSymbol().$eEnrollment['Target']?></td>
			<td valign="top" class="tabletext"><select name="showTarget"
				id="showTargetSelect">
					<option value="#">- <?=$i_general_please_select?> -</option>
					<option value="club"><?=$eEnrollmentMenu['club']?></option>
					<option value="activity"><?=$eEnrollmentMenu['activity']?></option>
			</select></td>
		</tr>
		<?=$categoryRowHTML?>
		</table>
</form>

<!-- form for passing value to export_avg_attendance.php -->
<form id="form2" name="form2" method="post">
	<input type="hidden" id="selectedAvgHidden" name="selectedAvg" value="">
	<?php echo $exportHiddenField ?>
</form>

<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($eEnrollment['generate_or_update'], "button", "checkForm()")?> &nbsp;
	</div>

<?php if(isset($showTarget)) { ?>		 
		<?php echo $tableContent ?>		 
<?php } ?>
<!--Start of Philips Edited -->
<?  if(isset($showTarget)){ ?>
<?           
        $resultArray = array(
            'name' => '',
            'sem' => '',
            'num' => '',
            'date' => '',
            'hour' => '',
            'attend' => ''
);
?>
<?            
if ($showTarget == 'club') {
    for ($st = 0; $st < sizeof($result); $st ++) {
        list ($GroupID, $name, $semName, $TitleChinese, $NumOfMember) = $result[$st];
        $DisplayName = $intranet_session_language == "en" ? $name : $TitleChinese;
        $AcademicYearID = Get_Current_Academic_Year_ID();
        if (isset($GroupNumMemberAssoAry[$GroupID])) {
            $NumOfMember = $GroupNumMemberAssoAry[$GroupID];
        }
        else {
            $NumOfMember = '0';
        }
        $totalMeetingDate = count($ClubAttendanceInfoArr[$GroupID]['MeetingDateArr']);
        $totalMeetingHours = 0;
        if ($totalMeetingDate > 0) {
            foreach ($ClubAttendanceInfoArr[$GroupID]['MeetingDateArr'] as $MeetingdayAry) {
                $timeDifferentAry = explode(':', $MeetingdayAry['Hours']);
                $hours = $timeDifferentAry[0];
                $minutes = $timeDifferentAry[1];
                $totalMeetingHours = $totalMeetingHours + $hours + ($minutes / 60);
                $totalMeetingHours = round($totalMeetingHours, 2);
            }
//             debug_pr($totalAttendance);
        }
        $totalAttendance = $ClubAttendanceInfoArr[$GroupID]['AttendancePercentageRounded'];
        $resultArray['name'] .= '"' . $DisplayName . '",';
        $resultArray['sem'] .= '"' . $AcademicYearID . '",';
        $resultArray['num'] .= $NumOfMember . ',';
        $resultArray['date'] .= $totalMeetingDate . ',';
        $resultArray['hour'] .= $totalMeetingHours . ',';
        $resultArray['attend'] .= $libenroll->getAverage($totalAttendance, 2, "", "") . ',';
        
    }
}
else if ($showTarget == 'activity') {
    for ($st = 0; $st < sizeof($result); $st ++) {
        list ($EnrolEventID, $name) = $result[$st];
        $NumOfMember;
        $DisplayName = $name;
        $AcademicYearID = Get_Current_Academic_Year_ID();
        if (isset($EventNumMemberAssoAry[$EnrolEventID])) {
            $NumOfMember = $EventNumMemberAssoAry[$EnrolEventID];
        } 
        else {
            $NumOfMember = '0';
        }
        $totalMeetingDate = count($EventAttendanceInfoArr[$EnrolEventID]['MeetingDateArr']);
        $totalMeetingHours = 0;
        if ($totalMeetingDate > 0) {
            foreach ($EventAttendanceInfoArr[$EnrolEventID]['MeetingDateArr'] as $MeetingdayAry) {
                $timeDifferentAry = explode(':', $MeetingdayAry['Hours']);
                $hours = $timeDifferentAry[0];
                $minutes = $timeDifferentAry[1];
                $totalMeetingHours = $totalMeetingHours + $hours + ($minutes / 60);
                $totalMeetingHours = round($totalMeetingHours, 2);
            }
        }
        $totalAttendance = $EventAttendanceInfoArr[$EnrolEventID]['AttendancePercentageRounded'];
        $resultArray['name'] .= '"' . $DisplayName . '",';
        $resultArray['sem'] .= '"' . $AcademicYearID . '",';
        $resultArray['num'] .= $NumOfMember . ',';
        $resultArray['date'] .= $totalMeetingDate . ',';
        $resultArray['hour'] .= $totalMeetingHours . ',';
        $resultArray['attend'] .= $libenroll->getAverage($totalAttendance, 2, "", "") . ',';
    }
}
?>
<table class="com_chart_result" style="width: 100%; margin: 0 auto;">
	<tbody>
		<tr>
			<td>
				<div id="chart_number" style="width: 100%; height: <?=sizeof($result)*52?>px; min-height: 450px; max-height: 600px; margin: 0 auto;clear: left; float: left;"></div>
			</td>
		</tr>
		<tr>
			<td>
				<div id="chart_date" style="width: 100%; height: <?=sizeof($result)*52?>px; min-height: 450px; max-height: 600px; margin: 0 auto;clear: left; float: left;"></div>
			</td>
		</tr>
		<tr>
			<td>
				<div id="chart_hour" style="width: 100%; height: <?=sizeof($result)*52?>px; min-height: 450px; max-height: 600px; margin: 0 auto;clear: left; float: left;"></div>
			</td>
		</tr>
		<tr>
			<td>
				<div id="chart_total" style="width: 100%; height: <?=sizeof($result)*52?>px; min-height: 450px; max-height: 600px; margin: 0 auto;clear: left; float: left;"></div>
			</td>
		</tr>
	</tbody>
</table>

<script type="text/javascript"
	src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript"
	src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
<?
$clLang = ($intranet_session_language == 'en') ? 'en' : 'chi';
$CHART_LANG = array(
    'chi' => array(
        'num' => array(
            '"會員數量"',
            '"人"'
        ),
        'date' => array(
            '"總活動日數"',
            '"日"'
        ),
        'hour' => array(
            '"總活動時數"',
            '"小時"'
        ),
        'attend' => array(
            '"總合出席率"',
            '"百分比(%)"'
        )
    ),
    'en' => array(
        'num' => array(
            '"Number of Member"',
            '"Person(s)"'
        ),
        'date' => array(
            '"Total Dates of Activity"',
            '"Day(s)"'
        ),
        'hour' => array(
            '"Total Hours of Activity"',
            '"Hour(s)"'
        ),
        'attend' => array(
            '"Total Attendance rate"',
            '"percent(%)"'
        )
    )
);
?>
drawChart('chart_number', {
	name: [<?=$resultArray['name']?>],
	array: [<?=$resultArray['num']?>],
	title: <?=$CHART_LANG[$clLang]['num'][0]?>,
	unit: <?=$CHART_LANG[$clLang]['num'][1]?>,
	color: 'lightblue',
});
drawChart('chart_date', {
	name: [<?=$resultArray['name']?>],
	array: [<?=$resultArray['date']?>],
	title: <?=$CHART_LANG[$clLang]['date'][0]?>,
	unit: <?=$CHART_LANG[$clLang]['date'][1]?>,
	color: 'darkblue',
});
drawChart('chart_hour', {
	name: [<?=$resultArray['name']?>],
	array: [<?=$resultArray['hour']?>],
	title: <?=$CHART_LANG[$clLang]['hour'][0]?>,
	unit: <?=$CHART_LANG[$clLang]['hour'][1]?>,
	color: 'brown'
});
drawChart('chart_total', {
	name: [<?=$resultArray['name']?>],
	array: [<?=$resultArray['attend']?>],
	title: <?=$CHART_LANG[$clLang]['attend'][0]?>,
	unit: <?=$CHART_LANG[$clLang]['attend'][1]?>,
	color: 'orange',
	type: 'percent'
});
function drawChart(chartid, obj){
	var chart = Highcharts.chart(chartid, {
		chart: {
			type: 'bar'
		},
		title: {
			text: obj.title
		},
		xAxis:{
			categories: obj.name,
			title:{
				text: null
			}
		},
		yAxis: {
			min: 0,
			max: obj.type=='percent' ? 100 : undefined,
			title: {
				text: obj.unit,
				align: 'high'
			},
	 	},
		tooltip: {
			formatter: function(){
				return this.x + '<br /><b>' + this.y + '</b> ' + obj.unit;
			}
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			enabled: false
		},
		credits: {
			enabled: false
		},
		series: [
			{name: '',
			data: obj.array,
			color: obj.color}
		]
	});
	return chart;
}
</script>
<? }?>


<!--End of Philips Edited   -->
<?
        $linterface->LAYOUT_STOP();
    } else {
        echo "You have no priviledge to access this page.";
    }
} else {
    
    intranet_closedb();
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
?>