<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
if (is_file($PATH_WRT_ROOT."includes/libqb.php"))
{
    include_once($PATH_WRT_ROOT."includes/libqb.php");
}
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$UID = IntegerSafe($UID);

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageMgtActEventStat";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $lg = new libgroup($GroupID);
        $TAGS_OBJ[] = array($lg->Title." ".$i_GroupAssignAdminRight, "", 1);

	    ################################################################

if (is_array($UID)) $UID = $UID[0];
$lu = new libuser($UID);
//$lg = new libgroup($GroupID);
$all_checked = "";
if ($lg->hasAllAdminRights($UID))
{
    $all_checked = "CHECKED";
}
$availableRights = $lg->getSelectCurrentAdminRights_ip20($UID);

################################################################

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE=javascript>
function checkform(obj)
{
         if (countChecked(obj,'grouprights[]')==0)
         {
             alert(globalAlertMsg2);
             return false;
         }
         return true;
}
function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouprights[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouprights[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}
</SCRIPT>

<form name='form1' method='post' action='member_setadmin.php' onSubmit="return checkform(this)">
<br/>
<table width='90%' border='0' cellpadding='4' cellspacing='0'>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?php echo $i_UserLogin; ?></td>
	<td class="tabletext"><?=$lu->UserLogin?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserEnglishName; ?></td>
	<td class="tabletext"><?=$lu->EnglishName?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserChineseName; ?></td>
	<td class="tabletext"><?=$lu->ChineseName?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserClassName; ?></td>
	<td class="tabletext"><?=$lu->ClassName?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserClassNumber; ?></td>
	<td class="tabletext"><?=$lu->ClassNumber?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_GroupAssignAdminRight_AllRights; ?></td>
	<td class="tabletext"><input type='checkbox' name='alltools' value='1' <?=$all_checked?> onClick="allToolsChecked(this.form)"></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><?=$i_GroupAssignAdminRight_AvailableAdminRights?></td>
	<td class="tabletext"><?=$availableRights?></td>
</tr>
</table>

<input type=hidden name='GroupID' value="<?=$GroupID?>">
<input type=hidden name='EnrolGroupID' value="<?=$EnrolGroupID?>">
<input type=hidden name='UID' value="<?=$UID?>">
<input type=hidden name='adminflag' value="A">
<input type=hidden name='AcademicYearID' value="<?=$AcademicYearID?>">

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
</div>
</td></tr>
</table>

</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>