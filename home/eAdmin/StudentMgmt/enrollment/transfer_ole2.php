<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//include_once($PATH_WRT_ROOT."includes/libportfolio-wah.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio{$LAYOUT_SKIN}-wah.php");

// include_once($PATH_WRT_ROOT."includes/libportfolio.php");
// include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();

# Check access right
if (!$plugin['eEnrollment'])
{
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();	
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))){
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageTranserOLE";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], "", 1);

$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step1'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step2'], 1);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step3'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step4'], 0);

$linterface->LAYOUT_START();

$ID_Str = implode(",",$ID);
$RecordTypeStr = $RecordType=="club" ? $eEnrollment['Club_Records'] : $eEnrollment['Activity_Records'];
$TypeSelect = $eEnrollment['Record_Type'].": " . $RecordTypeStr ;

//$LibPortfolio = new libportfolio2007();
$LibPortfolio = new libpf_slp();
$LibWord = new libwordtemplates_ipf(1);
$file_array = $LibWord->file_array;

$DefaultELEArray = $LibPortfolio->GET_ELE();


// buiild ELEArr for js function
$eleIDArr = array();
foreach ($DefaultELEArray as $ELE_ID => $ELE_Name)
{
	$eleIDArr[] = $ELE_ID;
}


// # build ELE list

// foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
// {
// 	for($i=0;$i<sizeof($ID);$i++)
// 		${"ELEList_".$ID[$i]} .= "<INPUT type='checkbox' name='ele_". $ID[$i] ."[]' value='".$ELE_ID."' id='".$ELE_ID."_". $ID[$i] ."'><label for='".$ELE_ID."_". $ID[$i]."'>".$ELE_Name."</label><br />";
// }

if($RecordType=="club")
{
	$TitleField = Get_Lang_Selection('b.TitleChinese', 'b.Title');
	$sql = "SELECT 
				a.EnrolGroupID,
				$TitleField, 
				a.OLE_ProgramID
			FROM 
				INTRANET_ENROL_GROUPINFO as a
				LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
			WHERE
				a.EnrolGroupID in ($ID_Str)
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				$TitleField
			";
}
else
{
	$sql ="
		SELECT
			EnrolEventID,
			EventTitle,
			OLE_ProgramID
		FROM
			INTRANET_ENROL_EVENTINFO 
		WHERE
			EnrolEventID in ($ID_Str)	
		ORDER BY
			EventTitle
	";
}
$result = $libenroll->returnArray($sql);


$FormChkArr = unserialize(rawurldecode($_REQUEST['FormChkArr']));
$TargetFormChkAllArr = unserialize(rawurldecode($_REQUEST['TargetFormChkAllArr']));

### Global Target Form Checkboxes
$libYear = new Year();
$FormArr = $libYear->Get_All_Year_List();
$numOfForm = count($FormArr);
$NumOfFormInRow = 5;

$GlobalFormChkTable = '';
$GlobalFormChkTable .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
	
	$thisChkID = "TargetFormChkAll_Global";
	$thisChkName = 'TargetFormChkAllArr[Global]';
	$thisChkOnClick = "js_Select_All_Target_Form('Global', this.checked);";
	$thisChkChecked = (isset($_REQUEST['TargetFormChkAllArr']))? $TargetFormChkAllArr['Global'] : 1;
	
	$GlobalFormChkTable .= '<tr><td>'."\n";
		$GlobalFormChkTable .= $linterface->Get_Checkbox($thisChkID, $thisChkName, 1, $thisChkChecked, '', $eEnrollment['all'], $thisChkOnClick, $Disabled='');
	$GlobalFormChkTable .= '</td></tr>'."\n";
		
	for ($i=0; $i<$numOfForm; $i++)
	{
		if (($i % $NumOfFormInRow) == 0)
		{
			### row start
			$GlobalFormChkTable .= '<tr>'."\n";
		}
		
		### Build Checkboxes
		$thisYearID = $FormArr[$i]['YearID'];
		$thisYearName = $FormArr[$i]['YearName'];
		$thisChkID = 'TargetFormChk_Global';
		$thisChkName = 'FormChkArr[Global]['.$thisYearID.']';
		$thisChkClass = 'TargetFormChk_YearID_Global TargetFormChk_GroupID_Global';
		$thisChkOnClick = 'js_Checked_Target_Form_Checkbox(this.id, this.checked);';
		$thisChkChecked = (isset($_REQUEST['FormChkArr']))? $FormChkArr['Global'][$thisYearID]: 1;
		
		$GlobalFormChkTable .= '<td>'."\n";
			$GlobalFormChkTable .= $linterface->Get_Checkbox($thisChkID, $thisChkName, $thisYearID, $thisChkChecked, $thisChkClass, $thisYearName, $thisChkOnClick, $Disabled='');
		$GlobalFormChkTable .= '</td>'."\n";
		
		if ( ( $i>0 && (($i % $NumOfFormInRow) == ($NumOfFormInRow-1)) ) || ($i==$numOfForm-1) )
		{
			### row end
			$GlobalFormChkTable .= '</tr>'."\n";
		}
	}
$GlobalFormChkTable .= '</table>'."\n";

### Local Target Form Checkboxes
$TargetFormChkTableArr = array();
$numOfResult = count($result);
for($i=0; $i<$numOfResult; $i++)
{
	list($thisGroupID, $thisTitle, $thisOLE_ID) = $result[$i];
	
	$thisFormChkTable = '';
	$thisFormChkTable .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
	
		$thisChkID = 'TargetFormChkAll_'.$thisGroupID;
		$thisChkName = 'TargetFormChkAllArr['.$thisGroupID.']';
		$thisChkOnClick = "js_Select_All_Target_Form('$thisGroupID', this.checked)";
		$thisChkChecked = (isset($_REQUEST['TargetFormChkAllArr']))? $TargetFormChkAllArr[$thisGroupID] : 1;
		
		$thisFormChkTable .= '<tr><td>'."\n";
			$thisFormChkTable .= $linterface->Get_Checkbox($thisChkID, $thisChkName, 1, $thisChkChecked, '', $eEnrollment['all'], $thisChkOnClick, $Disabled='');
		$thisFormChkTable .= '</td></tr>'."\n";
	
		for ($j=0; $j<$numOfForm; $j++)
		{
			if (($j % $NumOfFormInRow) == 0)
			{
				### row start
				$thisFormChkTable .= '<tr>'."\n";
			}
			
			### Build Checkboxes
			$thisYearID = $FormArr[$j]['YearID'];
			$thisYearName = $FormArr[$j]['YearName'];
			$thisChkID = 'TargetFormChk_'.$thisGroupID.'_'.$thisYearID;
			$thisChkName = 'FormChkArr['.$thisGroupID.']['.$thisYearID.']';
			$thisChkClass = 'TargetFormChk_YearID_'.$thisYearID.' TargetFormChk_GroupID_'.$thisGroupID;
			$thisChkOnClick = 'js_Checked_Target_Form_Checkbox(this.id, this.checked);';
			$thisChkChecked = (isset($_REQUEST['FormChkArr']))? $FormChkArr[$thisGroupID][$thisYearID]: 1;
			
			$thisFormChkTable .= '<td>'."\n";
				$thisFormChkTable .= $linterface->Get_Checkbox($thisChkID, $thisChkName, $thisValue=1, $thisChkChecked, $thisChkClass, $thisYearName, $thisChkOnClick, $Disabled='');
			$thisFormChkTable .= '</td>'."\n";
			
			if ( ( $j>0 && (($j % $NumOfFormInRow) == ($NumOfFormInRow-1)) ) || ($j==$numOfForm-1) )
			{
				### row end
				$thisFormChkTable .= '</tr>'."\n";
			}
		}
	$thisFormChkTable .= '</table>'."\n";
	
	$TargetFormChkTableArr[$thisGroupID] = $thisFormChkTable;
}
?>

<script language="javascript">

function UpdateHourSetting(value){
    var obj = document.form1;
	switch (value)
	{
             case 1:
                  obj.leastAttendance.disabled = true;
                  obj.manualHours.disabled = true;
                  break;
             case 2:
                  obj.leastAttendance.disabled = false;
                  obj.manualHours.disabled = true;
                  break;
             case 3:
                  obj.leastAttendance.disabled = true;
                  obj.manualHours.disabled = false;
                  break;
	}
}

</script>

<br />
<form name="form1" method="POST" action="transfer_ole2a.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
			</td>
		</tr>
		<tr>
        	<td align="center">
                <table width="96%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							
								<? if(sizeof($result) > 1) {?>
								<tr>
									<td colspan="2">
										<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
											<tr class="tabletop tabletopnolink">
												<td>
													<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle" />
													<span class="sectiontitle_table"><?=$eEnrollment['global_settings']?></span>
												</td>
											</tr>
											<tr class="tablerow2">
												<td width="80%" valign="top">
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
													<?
														# if back from step3, get the most updated data and settings
				                                		$tempCategory = (isset($category_global))? $category_global : "";
														$tempELEAry = (isset($ele_global))? $ele_global : $tempELEAry;
																									
														if (isset($hours_setting_global))
														{
															if ($hours_setting_global == "studentHours")
															{
																$checkStudentHours = "CHECKED";
						                                		$checkAbovePercentage = "";
						                                		$checkManualInput = "";	
															}
															elseif ($hours_setting_global == "abovePercentage")
															{
																$checkStudentHours = "";
						                                		$checkAbovePercentage = "CHECKED";
						                                		$checkManualInput = "";	
															}
															elseif ($hours_setting_global == "manualInput")
															{
																$checkStudentHours = "";
						                                		$checkAbovePercentage = "";
						                                		$checkManualInput = "CHECKED";	
															}
														}
														else
														{
															$checkStudentHours = "CHECKED";
					                                		$checkAbovePercentage = "";
					                                		$checkManualInput = "";	
														}
														
														$tb_least_Attendance = (isset($leastAttendance_global))? $leastAttendance_global : "";
														$tb_manualHours = (isset($manualHours_global))? $manualHours_global : "";
														
														if (isset($zeroHour_global))
														{
															if ($zeroHour_global == "transfer")
															{
																$checkTransfer = "CHECKED";
				                                				$checkNotTransfer = "";	
															}
															elseif ($zeroHour_global == "notTransfer")
															{
																$checkTransfer = "";
				                                				$checkNotTransfer = "CHECKED";	
															}
														}
														else
														{
															$checkTransfer = "CHECKED";
				                                			$checkNotTransfer = "";
														}
														
														if (isset($studentSetting_global))
														{
															if ($studentSetting_global == "allMember")
															{
																$checkAllMember = "CHECKED";
				                                				$checkActiveOnly = "";
															}
															elseif ($studentSetting_global == "activeOnly")
															{
																$checkAllMember = "";
				                                				$checkActiveOnly = "CHECKED";
															}
														}
														else
														{
															$checkAllMember = "CHECKED";
				                                			$checkActiveOnly = "";
														}	
													?>
														
														<!-- category -->
														<tr valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['OLE_Category']?></span>
															</td>
															<td>
																<?=$linterface->GET_SELECTION_BOX($file_array, " id='category_global' name ='category_global' ","",$tempCategory);?>
															</td>
															<td>
																<?=$linterface->GET_SMALL_BTN($iDiscipline['SetAll'], "button", "javascript:changeCategory()");?>
															</td>
														</tr>
											
														<!-- ele -->
														<?
														// # build ELE list
				                                		$ELEList = "";
														foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
														{
															$checked = "";
															if (is_array($tempELEAry))
															{
																$checked = in_array($ELE_ID, $tempELEAry) ? "CHECKED" : "";
															}
															$ELEList .= "<INPUT type='checkbox' name='ele_global[]' value='".$ELE_ID."' id='".$ELE_ID."_global' $checked><label id='ELE_labal_".$ELE_ID."_". $tempGroupID."' for='".$ELE_ID."_". $tempGroupID."'>".$ELE_Name."</label><br />";
														}
														?>
														<tr id="row_ele" valign="top" <?=$DisplayStyle?> >
															<td valign="top" nowrap="nowrap" class="formfieldtitle">
															<span class="tabletext"><?=$eEnrollment['OLE_Components']?></span>
															</td>
															<td><?=$ELEList ?></td>
															<td valign="top">
																<?=$linterface->GET_SMALL_BTN($iDiscipline['SetAll'], "button", "javascript:changeELE()");?>
															</td>
															
														</tr>
														
														<!-- Hours -->
														<tr valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['hours_setting']?></span>
															</td>
															<td>
																<!-- Student Attended Hours -->
																<input type="radio" name="hours_setting_global" value="studentHours" id="studentHours_global" <?=$checkStudentHours?> onClick="changeHourSettingStatus('global', 0)">
																	<label for="studentHours">
																		<?=$eEnrollment['total_hours_of_student']?>
																	</label>
																</input>
																<br />
																
																<!-- Depends on Student Attendance -->
																<input type="radio" name="hours_setting_global" value="abovePercentage" id="abovePercentage_global" <?=$checkAbovePercentage?> onClick="changeHourSettingStatus('global', 1)">
																	<label for="abovePercentage">
																		<?=$eEnrollment['total_hours_if_above_percentage']?>
																		<input class="textboxnum" type="text" name="leastAttendance_global" id="leastAttendance_global" maxlength="6" value="<?=$tb_least_Attendance?>" onClick="changeHourSettingStatus('global', 1)"> %
																		<?=$eEnrollment['total_hours_if_above_percentage2']?>
																	</label>
																</input>
																<br />
																
																<!-- Manual input Hours -->
																<input type="radio" name="hours_setting_global" value="manualInput" id="manualInput_global" <?=$checkManualInput?> onClick="changeHourSettingStatus('global', 2)">
																	<label for="manualInput">
																		<?=$eEnrollment['manual_input_hours']?> <input class="textboxnum" type="text" name="manualHours_global" id="manualHours_global" maxlength="6" value="<?=$tb_manualHours?>" onClick="changeHourSettingStatus('global', 2)"> <?=$eEnrollment['unit_hour']?>
																	</label>
																</input>
															</td>
															<td valign="top">
																<?=$linterface->GET_SMALL_BTN($iDiscipline['SetAll'], "button", "javascript:changeHours()");?>
															</td>
														</tr>
													
														<!-- Transfer if zero hours? -->
														<tr valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['transfer_if_zero_hour']?></span>
															</td>
															<td>
																<input type="radio" name="zeroHour_global" value="transfer" id="transferZero_global" <?=$checkTransfer?>> <label for="transfer"><?=$i_general_yes?></label>
																<input type="radio" name="zeroHour_global" value="notTransfer" id="notTransferZero_global" <?=$checkNotTransfer?>> <label for="notTransfer"><?=$i_general_no?></label>
															</td>
															<td valign="top">
																<?=$linterface->GET_SMALL_BTN($iDiscipline['SetAll'], "button", "javascript:changeTransferZero()");?>
															</td>
														</tr>
														
														<!-- Transfer Student Setting -->
														<tr valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['transfer_student_setting']?></span>
															</td>
															<td>
																<input type="radio" name="studentSetting_global" value="allMember" id="allMember_global" <?=$checkAllMember?>> <label for="allMember"><?=$eEnrollment['all_member']?></label>
																<input type="radio" name="studentSetting_global" value="activeOnly" id="activeOnly_global" <?=$checkActiveOnly?>> <label for="activeOnly"><?=$eEnrollment['active_member_only']?></label>
															</td>
															<td valign="top">
																<?=$linterface->GET_SMALL_BTN($iDiscipline['SetAll'], "button", "javascript:changeActive('','')");?>
															</td>
														</tr>
														
														<!-- Target Form -->
														<tr valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['add_activity']['act_target']?></span>
															</td>
															<td>
																<?= $GlobalFormChkTable ?>
															</td>
															<td valign="top">
																<?=$linterface->GET_SMALL_BTN($iDiscipline['SetAll'], "button", "javascript:js_Apply_All_TargetForm();");?>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										
											<tr>
												<td height="1" class="dotline" colspan="2"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
											</tr>
										</table>
										
										<br /> 
									</td>
								</tr>
								<? } ?>
                                <tr>
                                	<td colspan="2">
                                        <?
                                        $allGroupIDArr = array();
                                    	$noActiveMemberGroupIDArr = array();
                                    	for($i=0;$i<sizeof($result);$i++)
                                    	{
											list($tempGroupID, $tempTitle, $tempOLE_ID) = $result[$i];
											
											$allGroupIDArr[] = $tempGroupID;
											
											# check if the group has active member or not
											if ($RecordType=="club")
											{
												$sql = "SELECT isActiveMember FROM INTRANET_USERGROUP WHERE GroupID = '$tempGroupID'";
											}
											else
											{
												$sql = "SELECT isActiveMember FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$tempGroupID'";
											}												
											$activeMemberArr = $libenroll->returnVector($sql);
											
											$noActiveMember = (in_array("1", $activeMemberArr))? 0: 1;
											
											# retrieve OLE_PROGRAM data if already transfer
	                                		$OLE_Program_data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID($tempOLE_ID);
	                                		if (count((array)$OLE_Program_data) > 0)
	                                		{
		                                		list($tempStartDate, $tempEndDate, $tempTitle, $tempCategory, $tempELE, $tempOrg, $tempDetails, $tempSchoolRemarks) = $OLE_Program_data;
		                                		$tempELEAry = explode(",", $tempELE);
		                                		$tempOrg = $tempOrg;
		                                		$tempDetails = $tempDetails;
		                                		$tempSchoolRemarks = $tempSchoolRemarks;
                                			}
                                			else {
                                				$tempStartDate = '';
                                				$tempEndDate = '';
                                				$tempCategory = '';
                                				$tempELE = '';
                                				$tempELEAry = array();
                                				$tempOrg = '';
                                				$tempDetails = '';
                                				$tempSchoolRemarks = '';
                                			}
                                			
	                                		# settings
	                                		$checkStudentHours = "CHECKED";
	                                		$checkAbovePercentage = "";
	                                		$checkManualInput = "";
	                                		$tb_least_Attendance = "";
	                                		$tb_manualHours = "";
	                                		$checkTransfer = "CHECKED";
	                                		$checkNotTransfer = "";
	                                		$checkAllMember = "CHECKED";
	                                		$checkActiveOnly = "";
	                                		
	                                		# if back from step3, get the most updated data and settings
	                                		$tempCategory = (isset(${"category_".$tempGroupID}))? ${"category_".$tempGroupID} : $tempCategory;
											$tempELEAry = (isset(${"ele_".$tempGroupID}))? ${"ele_".$tempGroupID} : $tempELEAry;
											$tempOrg = (isset(${"organization_".$tempGroupID}))? intranet_htmlspecialchars(stripslashes(stripslashes(${"organization_".$tempGroupID}))) : $tempOrg;
											$tempDetails = (isset(${"details_".$tempGroupID}))? stripslashes(stripslashes(${"details_".$tempGroupID})) : $tempDetails;
											$tempSchoolRemarks = (isset(${"SchoolRemarks_".$tempGroupID}))? stripslashes(stripslashes(${"SchoolRemarks_".$tempGroupID})) : $tempSchoolRemarks;
															
											if (isset(${"hours_setting_".$tempGroupID}))
											{
												if (${"hours_setting_".$tempGroupID} == "studentHours")
												{
													$checkStudentHours = "CHECKED";
			                                		$checkAbovePercentage = "";
			                                		$checkManualInput = "";	
												}
												elseif (${"hours_setting_".$tempGroupID} == "abovePercentage")
												{
													$checkStudentHours = "";
			                                		$checkAbovePercentage = "CHECKED";
			                                		$checkManualInput = "";	
												}
												elseif (${"hours_setting_".$tempGroupID} == "manualInput")
												{
													$checkStudentHours = "";
			                                		$checkAbovePercentage = "";
			                                		$checkManualInput = "CHECKED";	
												}
											}
											
											$tb_least_Attendance = (isset(${"leastAttendance_".$tempGroupID}))? ${"leastAttendance_".$tempGroupID} : $tb_least_Attendance;
											$tb_manualHours = (isset(${"manualHours_".$tempGroupID}))? ${"manualHours_".$tempGroupID} : $tb_manualHours;
											
											if (isset(${"zeroHour_".$tempGroupID}))
											{
												if (${"zeroHour_".$tempGroupID} == "transfer")
												{
													$checkTransfer = "CHECKED";
	                                				$checkNotTransfer = "";	
												}
												elseif (${"zeroHour_".$tempGroupID} == "notTransfer")
												{
													$checkTransfer = "";
	                                				$checkNotTransfer = "CHECKED";	
												}
											}
											
											$rowActive = "";
											if (isset(${"studentSetting_".$tempGroupID}))
											{
												if (${"studentSetting_".$tempGroupID} == "allMember")
												{
													$checkAllMember = "CHECKED";
	                                				$checkActiveOnly = "";
												}
												elseif (${"studentSetting_".$tempGroupID} == "activeOnly")
												{
													$checkAllMember = "";
	                                				$checkActiveOnly = "CHECKED";
	                                				$rowActive = ($noActiveMember)? "DISABLED" : "";
												}
											}	
																															
											
										?>
										<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
											<tr class="tabletop tabletopnolink">
												<td>
													<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle" />
													<span class="sectiontitle_table"><?=$tempTitle?></span>
													<?
														if ($noActiveMember)
														{
															$noActiveMemberGroupIDArr[] = $tempGroupID;
														}
														echo toolBarSpacer();
														$visibility = ($rowActive)? "visible" : "hidden";
														echo "<span id=\"alert_{$tempGroupID}\" style=\"visibility: $visibility\"><font color=\"red\">(No Active Member)</font></span>";	
													?>
												</td>
											</tr>
                                        <?	
										                                	                                		
                                		// # build ELE list
                                		$ELEList = "";
										foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
										{
											if (is_array($tempELEAry))
												$checked = in_array($ELE_ID, $tempELEAry) ? "CHECKED" : "";
											$ELEList .= "<INPUT type='checkbox' name='ele_". $tempGroupID ."[]' value='".$ELE_ID."' id='".$ELE_ID."_". $tempGroupID ."' $checked><label id='ELE_labal_".$ELE_ID."_". $tempGroupID."' for='".$ELE_ID."_". $tempGroupID."'>".$ELE_Name."</label><br />";
										}												

										?>
											<tr id="row_<?=$tempGroupID?>">
												<td valign="top">
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<!-- category -->
														<tr valign="top" id="row_category_<?=$tempGroupID?>" <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['OLE_Category']?></span>
															</td>
															<td>
																<?=$linterface->GET_SELECTION_BOX($file_array, " id='category_".$tempGroupID."' name ='category_". $tempGroupID."'","",$tempCategory);?>
															</td>
														</tr>
											
														<!-- ele -->
														<tr id="row_ele_<?=$tempGroupID?>" valign="top" <?=$DisplayStyle?> <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle">
															<span class="tabletext"><?=$eEnrollment['OLE_Components']?></span>
															</td>
															<td><?=$ELEList ?></td>
															
														</tr>
														
														<!-- organization -->
														<tr id="row_organization_<?=$tempGroupID?>" valign="top" <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$ec_iPortfolio['organization']?></div></span></td>
															<td><input id="organization_<?=$tempGroupID?>" name="organization_<?=$tempGroupID?>" type="text" value="<?=$tempOrg?>" maxlength="256" class="textboxtext"></td>
														</tr>
														<!-- details -->
														<tr id="row_details_<?=$tempGroupID?>" valign="top" <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['details']?></span></td>
															<td><?=$linterface->GET_TEXTAREA("details_".$tempGroupID, $tempDetails);?></td>
														</tr>
														<!-- school remarks -->
														<tr id="row_remarks_<?=$tempGroupID?>" valign="top" <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remarks']?></span></td>
															<td><?=$linterface->GET_TEXTAREA("SchoolRemarks_".$tempGroupID, $tempSchoolRemarks);?></td>
														</tr>
														
														<!-- Hours -->
														<tr id="row_hours_<?=$tempGroupID?>" valign="top" <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['hours_setting']?></span>
															</td>
															<td>
																<!-- Student Attended Hours -->
																<input type="radio" name="hours_setting_<?=$tempGroupID?>" value="studentHours" id="studentHours_<?=$tempGroupID?>" <?=$checkStudentHours?> onClick="changeHourSettingStatus(<?=$tempGroupID?>, 0)">
																	<label for="studentHours">
																		<?=$eEnrollment['total_hours_of_student']?>
																	</label>
																</input>
																<br />
																
																<!-- Depends on Student Attendance -->
																<input type="radio" name="hours_setting_<?=$tempGroupID?>" value="abovePercentage" id="abovePercentage_<?=$tempGroupID?>" <?=$checkAbovePercentage?> onClick="changeHourSettingStatus(<?=$tempGroupID?>, 1)">
																	<label for="abovePercentage">
																		<?=$eEnrollment['total_hours_if_above_percentage']?>
																		<input class="textboxnum" type="text" name="leastAttendance_<?=$tempGroupID?>" id="leastAttendance_<?=$tempGroupID?>" maxlength="6" value="<?=$tb_least_Attendance?>" onClick="changeHourSettingStatus(<?=$tempGroupID?>, 1)"> %
																		<?=$eEnrollment['total_hours_if_above_percentage2']?>
																	</label>
																</input>
																<br />
																
																<!-- Manual input Hours -->
																<input type="radio" name="hours_setting_<?=$tempGroupID?>" value="manualInput" id="manualInput_<?=$tempGroupID?>" <?=$checkManualInput?> onClick="changeHourSettingStatus(<?=$tempGroupID?>, 2)">
																	<label for="manualInput">
																		<?=$eEnrollment['manual_input_hours']?> <input class="textboxnum" type="text" name="manualHours_<?=$tempGroupID?>" id="manualHours_<?=$tempGroupID?>" maxlength="6" value="<?=$tb_manualHours?>" onClick="changeHourSettingStatus(<?=$tempGroupID?>, 2)"> <?=$eEnrollment['unit_hour']?>
																	</label>
																</input>
															</td>
														</tr>
													
														<!-- Transfer if zero hours? -->
														<tr id="row_transferZero_<?=$tempGroupID?>" valign="top" <?=$rowActive?>>
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['transfer_if_zero_hour']?></span>
															</td>
															<td>
																<input type="radio" name="zeroHour_<?=$tempGroupID?>" value="transfer" id="transferZero_<?=$tempGroupID?>" <?=$checkTransfer?>> <label for="transfer"><?=$i_general_yes?></label>
																<input type="radio" name="zeroHour_<?=$tempGroupID?>" value="notTransfer" id="notTransferZero_<?=$tempGroupID?>" <?=$checkNotTransfer?>> <label for="notTransfer"><?=$i_general_no?></label>
															</td>
														</tr>
														
														<!-- Transfer Student Setting -->
														<tr id="row_activeSetting_<?=$tempGroupID?>" valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['transfer_student_setting']?></span>
															</td>
															<td>
																<input type="radio" name="studentSetting_<?=$tempGroupID?>" value="allMember" id="allMember_<?=$tempGroupID?>" onClick="changeActive(1, <?=$tempGroupID?>);" <?=$checkAllMember?>> <label for="allMember"><?=$eEnrollment['all_member']?></label>
																<input type="radio" name="studentSetting_<?=$tempGroupID?>" value="activeOnly" id="activeOnly_<?=$tempGroupID?>" onClick="changeActive(0, <?=$tempGroupID?>);" <?=$checkActiveOnly?>> <label for="activeOnly"><?=$eEnrollment['active_member_only']?></label>
															</td>
														</tr>
														
														<!-- Target Form -->
														<tr valign="top">
															<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
																<span class="tabletext"><?=$eEnrollment['add_activity']['act_target']?></span>
															</td>
															<td>
																<?=$TargetFormChkTableArr[$tempGroupID]?>
															</td>
														</tr>
													</table>
												</td>
											</tr>	
												
											<tr>
												<td height="1" class="dotline" colspan="2"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
											</tr>
											<br />
										</table>
									<?                                       		
                                		} // End for-loop
									?>
										<br />
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center">
													<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:checkForm(document.form1);", "submitBtn") ?>&nbsp;
													<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "") ?>&nbsp;
													<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='transfer_ole.php?RecordType=$RecordType'") ?>&nbsp;
												</td>
											</tr>
										</table>
                                	</td>
                                </tr>
                        	</table>
                        </td>
                	</tr>
				</table>
			</td>
		</tr>
	</table>

<script language="javascript">

function checkForm(obj){
	/* Place js at the bottom because $noActiveMemberGroupIDArr is defined after the generation of tables */
		
	//var submitBtn = document.getElementById('submitBtn');
	//submitBtn.disabled = true;
	
	// check if the data input is valid
	<? for ($iPHP=0; $iPHP<sizeof($allGroupIDArr); $iPHP++) { ?>
		var targetID = '<?=$allGroupIDArr[$iPHP]?>';
		
		var abovePercentageRadioBtn = document.getElementById('abovePercentage_' + targetID);
		if (abovePercentageRadioBtn.checked)
		{
			var leastAttendanceTB = document.getElementById('leastAttendance_' + targetID);
			if (!check_percentage(leastAttendanceTB,'<?=$eEnrollment['js_percentage_alert']?>'))
			{
				//submitBtn.disabled = false;
				return false;
			}
		}
		
		var manualInputRadioBtn = document.getElementById('manualInput_' + targetID);
		if (manualInputRadioBtn.checked)
		{
			var manualHoursTB = document.getElementById('manualHours_' + targetID);
			if (isNaN(parseFloat(manualHoursTB.value)) || parseFloat(manualHoursTB.value) < 0 || !IsNumeric(manualHoursTB.value))
			{
				//submitBtn.disabled = false;
				alert("<?=$eEnrollment['js_hours_alert']?>");
				return false;
			}
		}
		
		// Check if at least select one Target Form
		var jsNumOfSelectedForm = $('input.TargetFormChk_GroupID_' + targetID + ':checked').length;
		if (jsNumOfSelectedForm == 0)
		{
			alert('<?=$eEnrollment['js_target_group_alert']?>');
			$('input.TargetFormChk_GroupID_' + targetID).focus();
			return false;
		}
		
		var rowCategory = document.getElementById('row_category_' + targetID);
		rowCategory.disabled = false;
		
		var rowELE = document.getElementById('row_ele_' + targetID);
		rowELE.disabled = false;
		
		var rowOrganization = document.getElementById('row_organization_' + targetID);
		rowOrganization.disabled = false;
		
		var rowDetails = document.getElementById('row_details_' + targetID);
		rowDetails.disabled = false;
		
		var rowRemarks = document.getElementById('row_remarks_' + targetID);
		rowRemarks.disabled = false;
		
		var rowHours = document.getElementById('row_hours_' + targetID);
		rowHours.disabled = false;
		
		var rowTransferZero = document.getElementById('row_transferZero_' + targetID);
		rowTransferZero.disabled = false;	
		
	<? } ?>
	
	obj.submit();	
		
}

function changeActive(state, ownID)
{
	/* 	
		state = 1 => all members
		state = 0 => active member only
	*/
	
	// change active member selection
	if (ownID == "")
	{
		var allMemberGlobal = document.getElementById('allMember_global');
		
		if (allMemberGlobal.checked)
		{
			state = 1;
		}
		else
		{
			state = 0;
		}		
		
		// global setting
		<? for ($iPHP=0; $iPHP<sizeof($allGroupIDArr); $iPHP++) { ?>
			var targetID = '<?=$allGroupIDArr[$iPHP]?>';
		
			var rowActiveSetting = document.getElementById('row_activeSetting_' + targetID);
			var allMembersRadioBtn = document.getElementById('allMember_' + targetID);
			var activeOnlyRadioBtn = document.getElementById('activeOnly_' + targetID);	
			if (state)
			{
				allMembersRadioBtn.checked = true;
				activeOnlyRadioBtn.checked = false;
			}
			else
			{
				allMembersRadioBtn.checked = false;
				activeOnlyRadioBtn.checked = true;
			}
		<? } ?>
		
		// change layout if club has no active member
		<? for ($iPHP=0; $iPHP<sizeof($noActiveMemberGroupIDArr); $iPHP++) { ?>
		
			var targetID = '<?=$noActiveMemberGroupIDArr[$iPHP]?>';
			
			var alertSpan = document.getElementById('alert_' + targetID);
			if (state)
			{
				alertSpan.style.visibility = 'hidden';
			}
			else
			{
				alertSpan.style.visibility = 'visible';
			}
			
			var rowCategory = document.getElementById('row_category_' + targetID);
			if (state)
				rowCategory.disabled = false;
			else
				rowCategory.disabled = true;
				
			var rowELE = document.getElementById('row_ele_' + targetID);
			if (state)
				rowELE.disabled = false;
			else
				rowELE.disabled = true;
				
			var rowOrganization = document.getElementById('row_organization_' + targetID);
			if (state)
				rowOrganization.disabled = false;
			else
				rowOrganization.disabled = true;
				
			var rowDetails = document.getElementById('row_details_' + targetID);
			if (state)
				rowDetails.disabled = false;
			else
				rowDetails.disabled = true;
				
			var rowRemarks = document.getElementById('row_remarks_' + targetID);
			if (state)
				rowRemarks.disabled = false;
			else
				rowRemarks.disabled = true;
				
			var rowHours = document.getElementById('row_hours_' + targetID);
			if (state)
				rowHours.disabled = false;
			else
				rowHours.disabled = true;
				
			var rowTransferZero = document.getElementById('row_transferZero_' + targetID);
			if (state)
				rowTransferZero.disabled = false;
			else
				rowTransferZero.disabled = true;		
		<? } ?>
	}
	else
	{
		// local setting
		var targetID = ownID;
		
		var rowActiveSetting = document.getElementById('row_activeSetting_' + targetID);
		var allMembersRadioBtn = document.getElementById('allMember_' + targetID);
		var activeOnlyRadioBtn = document.getElementById('activeOnly_' + targetID);	
		if (state)
		{
			allMembersRadioBtn.checked = true;
			activeOnlyRadioBtn.checked = false;
		}
		else
		{
			allMembersRadioBtn.checked = false;
			activeOnlyRadioBtn.checked = true;
		}
		
		<? for ($iPHP=0; $iPHP<sizeof($noActiveMemberGroupIDArr); $iPHP++) { ?>
		
			var targetID = '<?=$noActiveMemberGroupIDArr[$iPHP]?>';
			
			if (targetID == ownID)
			{
				var alertSpan = document.getElementById('alert_' + targetID);
				if (state)
				{
					alertSpan.style.visibility = 'hidden';
				}
				else
				{
					alertSpan.style.visibility = 'visible';
				}
				
				var rowCategory = document.getElementById('row_category_' + targetID);
				if (state)
					rowCategory.disabled = false;
				else
					rowCategory.disabled = true;
					
				var rowELE = document.getElementById('row_ele_' + targetID);
				if (state)
					rowELE.disabled = false;
				else
					rowELE.disabled = true;
					
				var rowOrganization = document.getElementById('row_organization_' + targetID);
				if (state)
					rowOrganization.disabled = false;
				else
					rowOrganization.disabled = true;
					
				var rowDetails = document.getElementById('row_details_' + targetID);
				if (state)
					rowDetails.disabled = false;
				else
					rowDetails.disabled = true;
					
				var rowRemarks = document.getElementById('row_remarks_' + targetID);
				if (state)
					rowRemarks.disabled = false;
				else
					rowRemarks.disabled = true;
					
				var rowHours = document.getElementById('row_hours_' + targetID);
				if (state)
					rowHours.disabled = false;
				else
					rowHours.disabled = true;
					
				var rowTransferZero = document.getElementById('row_transferZero_' + targetID);
				if (state)
					rowTransferZero.disabled = false;
				else
					rowTransferZero.disabled = true;	
			}
		<? } ?>
	}
	
}

function changeCategory()
{
	// global setting
	var globalCategory = document.getElementById('category_global');
	var globalValue = globalCategory.value;
	
	<? for ($iPHP=0; $iPHP<sizeof($allGroupIDArr); $iPHP++) { ?>
		var targetID = '<?=$allGroupIDArr[$iPHP]?>';
		
		var categorySelection = document.getElementById('category_' + targetID);
		categorySelection.value = globalValue;
		
	<? } ?>	
}

function changeELE()
{
	<? for ($iPHP=0; $iPHP<sizeof($eleIDArr); $iPHP++) { ?>
		var targetELEID = '<?=$eleIDArr[$iPHP]?>';

		var globalValue = document.getElementById(targetELEID + '_global').checked;
	
		<? for ($jPHP=0; $jPHP<sizeof($allGroupIDArr); $jPHP++) { ?>
			var targetID = '<?=$allGroupIDArr[$jPHP]?>';
			
			var ELEChkBox = document.getElementById(targetELEID + '_' + targetID);
			ELEChkBox.checked = globalValue;
		
		<? } ?>	
	<? } ?>	
}

function changeHours()
{
	var abovePercentageRadioBtn = document.getElementById('abovePercentage_global');
	if (abovePercentageRadioBtn.checked)
	{
		var leastAttendanceTB = document.getElementById('leastAttendance_global')
		if (!check_percentage(leastAttendanceTB,'<?=$eEnrollment['js_percentage_alert']?>'))
		{
			return false;
		}
	}
	
	var manualInputRadioBtn = document.getElementById('manualInput_global');
	if (manualInputRadioBtn.checked)
	{
		var manualHoursTB = document.getElementById('manualHours_global');
		if (isNaN(parseFloat(manualHoursTB.value)) || parseFloat(manualHoursTB.value) < 0 || !IsNumeric(manualHoursTB.value))
		{
			alert("<?=$eEnrollment['js_hours_alert']?>");
			return false;
		}
	}
		
	var globalValueStudentHours = document.getElementById('studentHours_global').checked;
	
	var globalValueAbovePercentage = document.getElementById('abovePercentage_global').checked;
	var globalValueLeastAttendance = document.getElementById('leastAttendance_global').value;
	
	var globalValueManualInput = document.getElementById('manualInput_global').checked;
	var globalValueManualHours = document.getElementById('manualHours_global').value;
	
	<? for ($iPHP=0; $iPHP<sizeof($allGroupIDArr); $iPHP++) { ?>
		var targetID = '<?=$allGroupIDArr[$iPHP]?>';
		
		var studentHoursRadioBtn = document.getElementById('studentHours_' + targetID);
		studentHoursRadioBtn.checked = globalValueStudentHours;
		
		var abovePercentageRadioBtn = document.getElementById('abovePercentage_' + targetID);
		abovePercentageRadioBtn.checked = globalValueAbovePercentage;		
		var leastAttendanceTB = document.getElementById('leastAttendance_' + targetID);
		leastAttendanceTB.value = globalValueLeastAttendance;		
		
		var manualInputRadioBtn = document.getElementById('manualInput_' + targetID);
		manualInputRadioBtn.checked = globalValueManualInput;
		var manualHoursTB = document.getElementById('manualHours_' + targetID);
		manualHoursTB.value = globalValueManualHours;	
		
		
	<? } ?>	
}

function changeTransferZero()
{
	var globalValueTransterZero = document.getElementById('transferZero_global').checked;
	
	<? for ($iPHP=0; $iPHP<sizeof($allGroupIDArr); $iPHP++) { ?>
		var targetID = '<?=$allGroupIDArr[$iPHP]?>';
		
		var transferZeroRadioBtn = document.getElementById('transferZero_' + targetID);
		var notTransferZeroRadioBtn = document.getElementById('notTransferZero_' + targetID);
		
		transferZeroRadioBtn.checked = globalValueTransterZero;
		notTransferZeroRadioBtn.checked = !globalValueTransterZero;
	<? } ?>	
}

function changeHourSettingStatus(targetID, state)
{
	var studentHoursRadioBtn = document.getElementById('studentHours_'+targetID);
	var abovePercentageRadioBtn = document.getElementById('abovePercentage_'+targetID);
	var leastAttendanceTb = document.getElementById('leastAttendance_'+targetID);	
	var manualInputRadioBtn = document.getElementById('manualInput_'+targetID);
	var manualHoursTb = document.getElementById('manualHours_'+targetID);	
	
	if (state==0)
	{
		studentHoursRadioBtn.checked = true;
		abovePercentageRadioBtn.checked = false;
		manualInputRadioBtn.checked = false;	
		leastAttendanceTb.value = '';
		manualHoursTb.value = '';
	}
	else if (state==1)
	{
		studentHoursRadioBtn.checked = false;
		abovePercentageRadioBtn.checked = true;
		manualInputRadioBtn.checked = false;	
		leastAttendanceTb.focus();
		manualHoursTb.value = '';
	}
	else if (state==2)
	{
		studentHoursRadioBtn.checked = false;
		abovePercentageRadioBtn.checked = false;
		manualInputRadioBtn.checked = true;	
		leastAttendanceTb.value = '';
		manualHoursTb.focus();
	}
}

function js_Apply_All_TargetForm()
{
	var jsThisYearID;
	var jsThisChecked;
	$('input.TargetFormChk_YearID_Global').each( function() {
		jsThisYearID = $(this).val();
		jsThisChecked = $(this).attr('checked');
		
		$('input.TargetFormChk_YearID_' + jsThisYearID).each( function () {
			$(this).attr('checked', jsThisChecked);
			js_Checked_Target_Form_Checkbox($(this).attr('id'), jsThisChecked);
		});
	});
}

function js_Select_All_Target_Form(jsGroupID, jsChecked)
{
	$('input.TargetFormChk_GroupID_' + jsGroupID).attr('checked', jsChecked);
}

function js_Checked_Target_Form_Checkbox(jsElementID, jsChecked)
{
	if (jsChecked == false)
	{
		var TmpIDArr = jsElementID.split('_');		// TargetFormChk_$thisGroupID_$thisYearID
		var jsGroupID = TmpIDArr[1];
		
		$('input#TargetFormChkAll_' + jsGroupID).attr('checked', jsChecked);
	}
}
</script>


<? for($i=0;$i<sizeof($ID);$i++) {?>
<input type="hidden" name="ID[]" value="<?=$ID[$i]?>" />
<? } ?>
<input type="hidden" name="RecordType" value="<?=$RecordType?>" />
<input type="hidden" name="noActiveMemberGroupIDArr" value="<?=rawurlencode(serialize($noActiveMemberGroupIDArr));?>">

</form>
</br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>