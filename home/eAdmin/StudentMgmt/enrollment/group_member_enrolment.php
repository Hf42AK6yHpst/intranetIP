<?php
#using : 

############################################################################
##
##	Date:	2014-10-31	(Omas)
##			Improvement : Also showing joined club and student maximum no. want to join  
##			 
##
#############################################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
$ApplicantID = IntegerSafe($StudentID);
$LibUser = new libuser($ApplicantID);

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin');

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $Lang['eEnrolment']['StudentClubEnrolInfo'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$studentEnrollmentInfoAry = $libenroll->Get_Student_Applied_Club_Info($ApplicantID, $enrolGroupIdAry='', $recordStatusAry='', $categoryIdAry='', $AcademicYearID);
$numOfEnrollmentInfo = count($studentEnrollmentInfoAry);


if (!$libenroll->UseCategorySetting) {
	for ($i=0; $i<$numOfEnrollmentInfo; $i++) {
		$studentEnrollmentInfoAry[$i]['CategoryID'] = 0;
	}
}
else{
	$categoryAry = $libenroll->GET_CATEGORY_LIST();
	$categoryAssoAry = BuildMultiKeyAssoc($categoryAry, 'CategoryID', $IncludedDBField=array('CategoryName'), $SingleValue=1);
	unset($categoryAry);
}#Omas

$studentEnrollmentAssoAry = BuildMultiKeyAssoc($studentEnrollmentInfoAry, 'CategoryID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($studentEnrollmentInfoAry);

##Student enrolled data
$x = '';
if ($numOfEnrollmentInfo == 0) {
	$x .= $Lang['General']['NoRecordAtThisMoment'];
}
else {
	foreach ((array)$studentEnrollmentAssoAry as $_categoryId => $_clubEnrollmentInfoAry) {
		$_categoryName = $_clubEnrollmentInfoAry[0]['CategoryName'];
		$_numOfEnrollment = count($_clubEnrollmentInfoAry);
		
		if ($libenroll->UseCategorySetting) {
			$x .= '<span style="font-size:1.1em; font-weight:bold; color:#006600; float:left; line-height:20px;">'.$_categoryName.'</span>';
		}
		
		$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th style="width:8%;">'.$Lang['eEnrolment']['Priority'].'</th>'."\n";
					$x .= '<th style="width:42%;">'.$eEnrollment['club_name'].'</th>'."\n";
					$x .= '<th style="width:30%;">'.$Lang['eEnrolment']['MeetingSchedule'].'</th>'."\n";
					$x .= '<th style="width:10%;">'.$eEnrollment['status'].'</th>'."\n";
					if ($libenroll->tiebreak == 1) {
						$x .= '<th style="width:10%;">'.$eEnrollment['position'].'</th>'."\n";
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
				for ($i=0; $i<$_numOfEnrollment; $i++) {
					$__enrolGroupId = $_clubEnrollmentInfoAry[$i]['EnrolGroupID'];
					$__clubName = $_clubEnrollmentInfoAry[$i]['Title'];
					$__choice = $_clubEnrollmentInfoAry[$i]['Choice'];
					$__recordStatus = $_clubEnrollmentInfoAry[$i]['RecordStatus'];
					$__recordStatusDisplay = $libenroll_ui->Get_Enrollment_Status_Display($__recordStatus);
					
					$__dateAry = $libenroll->GET_ENROL_GROUP_DATE($__enrolGroupId);
					$__numOfDate = count($__dateAry);
					$__dateDisplayAry = array();
					if ($__numOfDate == 0) {
						$__dateDisplayAry[] = $Lang['General']['EmptySymbol'];
					}
					else {
						for ($j=0; $j<$__numOfDate; $j++) {
							$___startDateTime = $__dateAry[$j]['ActivityDateStart'];
							$___endDateTime = $__dateAry[$j]['ActivityDateEnd'];
							
							$___dateDisplay = '';
							$___dateDisplay .= date('Y-m-d', strtotime($___startDateTime));
							$___dateDisplay .= ' ('.date('D', strtotime($___startDateTime)).') ';
							$___dateDisplay .= date('H:i', strtotime($___startDateTime)).'-'.date('H:i', strtotime($___endDateTime));
							
							$__dateDisplayAry[] = $___dateDisplay;
						}
					}
					$__dateDisplay = implode('<br />', (array)$__dateDisplayAry);
					
					
					if ($libenroll->tiebreak == 1) {
						$__position = $libenroll->GET_STUDENT_GROUP_POSITION($__enrolGroupId);
					}
					
					$x .= '<tr>'."\n";
						$x .= '<td>'.$__choice.'</td>'."\n";
						$x .= '<td>'.$__clubName.'</td>'."\n";
						$x .= '<td>'.$__dateDisplay.'</td>'."\n";
						$x .= '<td>'.$__recordStatusDisplay.'</td>'."\n";
						if ($libenroll->tiebreak == 1) {
							$x .= '<td>'.$__position.'</td>'."\n";
						}
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br />'."\n";
	}
}
$htmlAry['tableDisplay'] = $x;

##Student Info

$StudentNameDisplay = Get_Lang_Selection($LibUser->ChineseName,$LibUser->EnglishName); 
$StudentClassName = $LibUser->ClassName."-".$LibUser->ClassNumber;
$WantMax = $libenroll->Get_Student_WantMax($ApplicantID);
$NumCategory = count($WantMax);

function EmptyStringTo0($String){
	if($String==''){
		$result = '0' ;
	}
	else{
		$result = $String;
	}
	return $result;
}

$StudentInfoTable.= '<table class="form_table_v30">'."\r\n";
	$StudentInfoTable .= '<tbody>'."\n";
		$StudentInfoTable .= '<tr>'."\n";
			$StudentInfoTable .= '<td class="field_title">'.$Lang['AccountMgmt']['StudentName'].'</td>'."\n";
			$StudentInfoTable .= '<td>'.$StudentNameDisplay.'&nbsp;('.$StudentClassName.')</td>'."\n";
		$StudentInfoTable .= '</tr>'."\n";
		$StudentInfoTable .= '<tr>'."\n";
			$StudentInfoTable .= '<td class="field_title">'.$Lang['eEnrolment']['MaxStudentWant'].'</td>'."\n";
			if ($libenroll->UseCategorySetting) {
				$StudentInfoTable .= '<td>'."\n";
					$StudentInfoTable .='<table class="common_table_list_v30 view_table_list_v30">'."\n";
						$StudentInfoTable .= '<tr>'."\n";
							$StudentInfoTable .= '<th>'.$Lang['eEnrolment']['ClubCategory'].'</th>'."\n";
							$StudentInfoTable .= '<th>'.$i_CampusMail_New_NumberOfEntry.'</th>'."\n";
						$StudentInfoTable .= '</tr>'."\n";
					for($i=0; $i<$NumCategory; $i++){
						$StudentInfoTable .= '<tr>'."\n";
							$StudentInfoTable .= '<td>'.$WantMax[$i]['CategoryName'].'</td>'."\n";
							$StudentInfoTable .= '<td>'.EmptyStringTo0($WantMax[$i]['Max']).'</td>'."\n";
						$StudentInfoTable .= '</tr>'."\n";	
					}
				$StudentInfoTable .= '</table>'."\n";
				$StudentInfoTable .= '</td>'."\n";
			}
			else{
				$StudentInfoTable .= '<td>'.EmptyStringTo0($WantMax[0]['Max']).'</td>'."\n";
			}
		$StudentInfoTable .= '</tr>'."\n";
	$StudentInfoTable .= '</tbody>'."\n";
$StudentInfoTable .= '</table>'."\n";
$StudentInfoTable .= '<br />'."\n";

##Joined Club
$studentClubInfoAry= $libenroll-> Get_Student_Club_Info($ApplicantID);


$numOfClub = count($studentClubInfoAry);
$JoinedClubTable .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$JoinedClubTable .= '<thead>'."\n";
		$JoinedClubTable .= '<tr>'."\n";
			$JoinedClubTable .= '<th style="width:8%;">'.'#'.'</th>'."\n";
			$JoinedClubTable .= '<th style="width:42%;">'.$eEnrollment['club_name'].'</th>'."\n";
			$JoinedClubTable .= '<th style="width:30%;">'.$i_ActivityRole.'</th>'."\n";
			if ($libenroll->UseCategorySetting) {
				$JoinedClubTable .= '<th style="width:42%;">'.$Lang['eEnrolment']['ClubCategory'].'</th>'."\n";
			}
		$JoinedClubTable .= '</tr>'."\n";
	$JoinedClubTable .= '</thead>'."\n";
	$JoinedClubTable .= '<tbody>'."\n";
	if($numOfClub == 0){
		$JoinedClubTable .= '<tr><td align="center" colspan="4">';
		$JoinedClubTable .= $Lang['General']['NoRecordAtThisMoment'];
		$JoinedClubTable .= '</td></tr>';
	}
	else{
		for ($i=0; $i<$numOfClub; $i++) {
			$_ClubNum = $i + 1;
			$_ClubName = $studentClubInfoAry[$i]['ClubTitle'];
			$_ClubNameCh = $studentClubInfoAry[$i]['ClubTitleCh'];
			$_ClubNameDisplay = Get_Lang_Selection($_ClubNameCh, $_ClubName);
			$_ClubPosition = Get_String_Display($studentClubInfoAry[$i]['RoleTitle']);
			if ($libenroll->UseCategorySetting) {
				$_CategoryID = $studentClubInfoAry[$i]['CategoryID'];
				$_CategoryName = $categoryAssoAry[$_CategoryID];
			}

			$JoinedClubTable .= '<tr>'."\n";
				$JoinedClubTable .= '<td>'.$_ClubNum.'</td>'."\n";
				$JoinedClubTable .= '<td>'.$_ClubNameDisplay.'</td>'."\n";
				$JoinedClubTable .= '<td>'.$_ClubPosition.'</td>'."\n";
				if ($libenroll->UseCategorySetting) {
					$JoinedClubTable .= '<td>'.$_CategoryName.'</td>'."\n";
				}
			$JoinedClubTable .= '</tr>'."\n";
		}
	}
	$JoinedClubTable .= '</tbody>'."\n";
$JoinedClubTable .= '</table>'."\n";
$JoinedClubTable .= '<br />'."\n";



$htmlAry['closeBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "self.close();", "close_btn");
?>

<script language="Javascript">
</script>
<br />
<div class="content_top_tool">
		
</div>

<div="table_board">
	<?=$linterface->GET_NAVIGATION2_IP25($Lang['StudentRegistry']['StudInfo']);?><br style="clear:both;">
	<?=$StudentInfoTable?><br><br>
</div>
	
<div class="table_board">
	<?=$linterface->GET_NAVIGATION2_IP25($Lang['eEnrolment']['AppliedClub']);?><br style="clear:both;">
	<?=$htmlAry['tableDisplay']?><br><br>
</div>

<div class="table_board">
	<?=$linterface->GET_NAVIGATION2_IP25($Lang['eEnrolment']['JoinedClub']);?><br style="clear:both;">
	<?=$JoinedClubTable?><br>
</div>

<div class="edit_bottom">
	<span></span>
	<p class="spacer"></p>
	<?=$htmlAry['closeBtn']?>
	<p class="spacer"></p>
</div>
<? 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>