<?php
# using: yat

######### Change Log Start ################
#
#	Date:	2010-07-22	YatWoon
#			Add AcademicYearID 
#
#	Date:	2010-01-28	YatWoon
#			Add "School Activity" option
#
######### Change Log Start ################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$libenroll->IS_CLUB_PIC())
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$TempEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);

# No need to delete
//$libenroll->DEL_EVENTSTUDENT($EnrolEventID);

$EventArr['EnrolEventID'] = $EnrolEventID;
$EventArr['Description'] = trim($Discription);
if ($set_age) $EventArr['UpperAge'] = $UpperAge;
if ($set_age) $EventArr['LowerAge'] = $LowerAge;
$EventArr['Gender'] = $gender;
$EventArr['attach'] = $_FILES['attach'];
$EventArr['Quota'] = $TempEventArr['Quota'];
$EventArr['EventTitle'] = trim($act_title);
$EventArr['EventCategory'] = $sel_category;
$EventArr['ApplyStartTime'] = $TempEventArr['ApplyStartTime'];
$EventArr['ApplyEndTime'] = $TempEventArr['ApplyEndTime'];
$EventArr['ApplyMethod'] = $TempEventArr['ApplyMethod'];
$EventArr['EnrolGroupID'] = $EnrolGroupID;
$EventArr['PaymentAmount'] = $PaymentAmount;
$EventArr['isAmountTBC'] = $isAmountTBC;
$EventArr['Nature'] = $nature;
$EventArr['AcademicYearID'] = Get_Current_Academic_Year_ID();

if ($attachname0 != "") $EventArr['attach']['name'][0] = $attachname0;
if ($attachname1 != "") $EventArr['attach']['name'][1] = $attachname1;
if ($attachname2 != "") $EventArr['attach']['name'][2] = $attachname2;
if ($attachname3 != "") $EventArr['attach']['name'][3] = $attachname3;
if ($attachname4 != "") $EventArr['attach']['name'][4] = $attachname4;


if ($libenroll->IS_EVENTINFO_EXISTS($EnrolEventID)) {
	$EnrolEventID = $libenroll->EDIT_EVENTINFO($EventArr);
} else {
	$EnrolEventID = $libenroll->ADD_EVENTINFO($EventArr);
}
$libenroll->DEL_EVENTCLASSLEVEL($EnrolEventID);

$ClasslvlArr['EnrolEventID'] = $EnrolEventID;
for ($i = 0; $i < sizeof($classlvl); $i++) {
	$ClasslvlArr['ClassLevelID'] = $classlvl[$i];
	$libenroll->ADD_EVENTCLASSLEVEL($ClasslvlArr);
}
intranet_closedb();
header("Location: event_new1b.php?EnrolEventID=$EnrolEventID&isNew=$isNew");
?>