<?php
// Using: 

/***********************************
 * Date: 2012-12-20 Rita
 * Details: create this page for updating approval reason
 * 
 * 
 ***********************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$linterface = new interface_html();


# Check If Has eEnrolment Plugin 
if ($plugin['eEnrollment'])
{
	# Check If Has Access Right 
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID) && !$libgroup->hasAdminBasicInfo($UserID) && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	{
		//header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	# Obtain Post Values
	$approvalReason = $_POST['approvalReason'];
	//$UserIDWithEnrolReason = $_POST['UserIDWithEnrolReason'];	
	$data = unserialize(rawurldecode($data));
	$EnrolEventID = $_POST['EnrolEventID'];
	$filter = $_POST['filter'];

	# Loop User with Reason
	$SuccessArr = array();
	$numOfUsers = count($data);
	for($i=0;$i<$numOfUsers;$i++){
		$thisReason = '';
		$thisStudentID = $data[$i];
		$thisReason = standardizeFormPostValue($approvalReason[$i]);		
		
		$SuccessArr['INTRANET_ENROL_REASON'][] = $libenroll->Update_Event_Enrol_Reason($EnrolEventID,$thisStudentID,$thisReason);
				
	}
		
		
	if (in_array(false, $SuccessArr))
	{
		$libenroll->RollBack_Trans();
		$response_msg = '5';
	}
	else
	{
		$libenroll->Commit_Trans();
		$response_msg = '2';		
		
	// send email to all student 
	$RecordType = "activity";
	$successResult = false;
	$libenroll->Send_Enrolment_Result_Email($data, $EnrolEventID, $RecordType, $successResult);
			
	}

	header("Location: event_member.php?EnrolEventID=$EnrolEventID&msg=$response_msg&filter=$filter");
	
	intranet_closedb();

}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
