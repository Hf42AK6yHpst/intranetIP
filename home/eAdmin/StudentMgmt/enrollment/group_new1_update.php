<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$GroupArr['GroupID'] = $GroupID;
$GroupArr['Discription'] = $Discription;
if ($set_age) $GroupArr['UpperAge'] = $UpperAge;
if ($set_age) $GroupArr['LowerAge'] = $LowerAge;
$GroupArr['Gender'] = $gender;
$GroupArr['attach'] = $_FILES['attach'];
$GroupArr['GroupCategory'] = $sel_category;
$GroupArr['PaymentAmount'] = $PaymentAmount;

if ($libenroll->IS_GROUPINFO_EXISTS($GroupID)) {
	$EnrolGroupID = $libenroll->EDIT_GROUPINFO($GroupArr);
} else {
	$EnrolGroupID = $libenroll->ADD_GROUPINFO($GroupArr);
}

$libenroll->DEL_GROUPSTUDENT($GroupID);

$libenroll->DEL_GROUPSTAFF($EnrolGroupID);
$libenroll->DEL_GROUPCLASSLEVEL($EnrolGroupID);

$PICArr['EnrolGroupID'] = $EnrolGroupID;
$PICArr['StaffType'] = "PIC";
for ($i = 0; $i < sizeof($pic); $i++) {
	$PICArr['UserID'] = $pic[$i];
	$libenroll->ADD_GROUPSTAFF($PICArr);
}

$PICArr['StaffType'] = "HELPER";
for ($i = 0; $i < sizeof($helper); $i++) {
	$PICArr['UserID'] = $helper[$i];
	$libenroll->ADD_GROUPSTAFF($PICArr);
}

$ClasslvlArr['EnrolGroupID'] = $EnrolGroupID;
for ($i = 0; $i < sizeof($classlvl); $i++) {
	$ClasslvlArr['ClassLevelID'] = $classlvl[$i];
	$libenroll->ADD_GROUPCLASSLEVEL($ClasslvlArr);
}

intranet_closedb();
header("Location: group_new2.php?GroupID=$GroupID");
?>