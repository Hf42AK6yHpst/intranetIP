<?php
// Using: 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$AcademicYearID = trim(stripslashes($_POST['AcademicYearID']));
$FromYearTermID = trim(stripslashes($_POST['FromYearTermID']));
$ToYearTermID = trim(stripslashes($_POST['ToYearTermID']));
$CopyClubArr = unserialize(rawurldecode($_REQUEST['CopyClubArr']));
$CopyMemberArr = unserialize(rawurldecode($_REQUEST['CopyMemberArr']));
$CopyActivityArr = unserialize(rawurldecode($_REQUEST['CopyActivityArr']));


$TermMappingArr = array();
$TermMappingArr[$FromYearTermID] = $ToYearTermID;


//$SuccessArr['Reset_Enrollment_Data'] = $libenroll->Reset_Enrollment_Data();



$SuccessArr = array();
$failedCount = 0;
$successCount = 0;
$libenroll->Start_Trans();
foreach ($CopyClubArr as $thisEnrolGroupID => $thisCopy) {
	$CopyMember = $CopyMemberArr[$thisEnrolGroupID];
	$CopyActivity = $CopyActivityArr[$thisEnrolGroupID];
	
	$NewEnrolGroupID = $libenroll->Clone_Club($thisEnrolGroupID, $NewGroupID='', $TermMappingArr);
	
	if ($NewEnrolGroupID==0 || $NewEnrolGroupID=='')
		$SuccessArr[$thisEnrolGroupID]['Copy_GroupAndClub'] = false;
	else
	{
		$SuccessArr[$thisEnrolGroupID]['Copy_GroupAndClub'] = true;
		
		if ($CopyMember) {
			$SuccessArr[$thisEnrolGroupID]['Copy_Member'] = $libenroll->Copy_Club_Member($thisEnrolGroupID, $NewEnrolGroupID);
		}
			
		if ($CopyActivity) {
			$SuccessArr[$thisEnrolGroupID]['Copy_Activity'] = $libenroll->Copy_Activity_Of_Club($thisEnrolGroupID, $NewEnrolGroupID);
		}
	}
	
	if (in_array(false, $SuccessArr[$thisEnrolGroupID]))
		$failedCount++;
	else
		$successCount++;
}

if (in_array(false, $SuccessArr))
{
	$libenroll->RollBack_Trans();
}
else
{
	$libenroll->Commit_Trans();
	$libenroll->Add_Copy_Log($FromAcademicYearID, $ToAcademicYearID, $TermMappingArr, $CopyClubArr, $CopyMemberArr, $CopyActivityArr, $EnrolGroupIDMappingArr);
}

intranet_closedb();

header('Location: '.$PATH_WRT_ROOT.'home/eAdmin/StudentMgmt/enrollment/copy_club_step3.php?From=copyByTerm&SuccessCount='.$successCount);
?>