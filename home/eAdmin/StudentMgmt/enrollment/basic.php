<?php
//# using: 
/********************************************************
 *  Modification Log
 * 
 *  2020-06-08 [Tommy]
 *  - only get can use enrolment class for Target Form #R171077
 * 
 *  2020-04-29 [Tommy]
 *  - add new row for $sys_custom['eEnrollment']['setTargetForm'] #R171077
 * 
 *  2020-02-25 Henry
 * 		-  Added parseInt to var _applyMin _applyMax _enrollMax _enrollMin 
 * 
 * 	2020-01-30 Henry
 * 		-  Added EnrollMin logic
 * 
 *  2018-04-27  Anna
 *      - Added Committee Recruitment Setting

 *  2017-10-04 Anna
 *  	- Add lookupClashestoSendPushMessagetoClubPIC setting 
 *  
 *  2015-01-26 Kenneth
 * 		- Add class 'simple' / 'advanced' to hide/show elements in js: changemode()
 * 
 *  2015-06-01 Omas
 * 		- Clear all button disabled
 * 	
 *  2014-12-02 Omas --- (hided on 2015-01-02)
 * 		- New setting $libenroll->notCheckTimeCrash
 * 
 * 	2014-11-13	Omas
 * 		- Comment $enrolInfo improve Performance
 * 
 *  2014-10-14	Pun
 * 				Add round report for SIS customization
 *
 * 	2013-08-02  Cameron
 * 		- Don't show Target Applicant if ($sys_custom['eEnrolment']['ClubTargetApplicant']) is true
 * 		  because this flag allow user to control the target applicant for individual club
 * 
 *  2013-03-01  Rita
 * 		- add Disallow PIC to add/remove members option
 * 
 *  2013-02-26  Rita
 * 		- add Default Attendance Status Contol for customization
 * 
 * 	2012-12-21	Ivan
 * 		- added term-based application quota settings
 * 
 *  2012-11-01  Rita
 *  	- add $chkDisableHelperModificationRightLimitation for attendance helper right control
 * 
 *	2011-09-07	YatWoon
 *		- lite version implementation
 *
 * 	2010-08-02  Thomas
 * 		- Add options 'Disable Add/Update enrollment data' in Advanced settings
 *        Once activated, users can't update or add enrollment data
 *
 ********************************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$libSCM_ui = new subject_class_mapping_ui();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	$CurrentPage = "PageSysSettingBasic";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

	//if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
	//$enrolInfo = $libenroll->getEnrollmentInfo(); #comment by Omas

    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($Lang['eEnrolment']['GeneralSetting'], "", 1);
        if(!$plugin['SIS_eEnrollment']){
        	$TAGS_OBJ[] = array($Lang['eEnrolment']['CategorySetting'], "category_setting_club.php", 0);
        }
        if($sys_custom['eEnrolment']['CommitteeRecruitment']){
            $TAGS_OBJ[] = array($Lang['eEnrolment']['CommitteeRecruitmentSetting'], "committee_recruitment_setting.php", 0);
        }
        
        # tags 
        //$current_tab = 1;
        //include_once("settings_tabs.php");

        $linterface->LAYOUT_START();
        
$basicmode = $libenroll->mode;
$AppStart = $libenroll->AppStart;
$AppStartHour = $libenroll->AppStartHour;
$AppStartMin = $libenroll->AppStartMin;
$AppEnd = $libenroll->AppEnd;
$AppEndHour = $libenroll->AppEndHour;
$AppEndMin = $libenroll->AppEndMin;
$GpStart = $libenroll->GpStart;
$GpEnd = $libenroll->GpEnd;
$defaultMin = $libenroll->defaultMin;
$defaultMax = $libenroll->defaultMax;
$tiebreak = $libenroll->tiebreak;
$Description = $libenroll->Description;
$EnrollMax = $libenroll->EnrollMax;
$enrollPersontype = $libenroll->enrollPersontype;
$disableStatus = $libenroll->disableStatus;
($libenroll->activeMemberPer == NULL)? $activeMemberPer=0 : $activeMemberPer=$libenroll->activeMemberPer;
$oneEnrol = $libenroll->oneEnrol;
$notCheckTimeCrash = $libenroll->notCheckTimeCrash;
$onceEmail = $libenroll->onceEmail;
$caterPriority = $libenroll->caterPriority;
$targetSemester = $libenroll->targetSemester;
$disableCheckingNoOfClubStuWantToJoin = $libenroll->disableCheckingNoOfClubStuWantToJoin;
$disableUpdate = $libenroll->disableUpdate;

$disableHelperModificationRightLimitation = $libenroll->disableHelperModificationRightLimitation;
$disallowPICtoAddOrRemoveMembers =  $libenroll->disallowPICtoAddOrRemoveMembers;
if($plugin['eClassTeacherApp']){
	$lookupClashestoSendPushMessagetoClubPIC = $libenroll->lookupClashestoSendPushMessagetoClubPIC;
}

$AnnounceEnrolmentResultDate = $libenroll->AnnounceEnrolmentResultDate;
$AnnounceEnrolmentResultHour = $libenroll->AnnounceEnrolmentResultHour;
$AnnounceEnrolmentResultMin = $libenroll->AnnounceEnrolmentResultMin;
$quotaSettingsType = $libenroll->quotaSettingsType;

# Default Attendance Status
if($libenroll->enableClubDefaultAttendaceSelectionControl()){
	$defaultAttendanceStatus = $libenroll->defaultAttendanceStatus;
}

if($disableStatus==1)
{
	$chkDisableStauts = "CHECKED";	
}
else
{
	$chkDisableStauts = "";	
}

$chkOneEnrol = ($oneEnrol==1)? "CHECKED" : "";
$ShowNotCheckTimeCrash = ($oneEnrol==1)? "none" : "block";
$chkNotCheckTimeCrash = ($notCheckTimeCrash == 1)? "CHECKED" : "";
$chkCaterPriority = ($caterPriority==1)? "CHECKED" : "";
$chkDisableCheckingNoOfClubStuWantToJoin = ($disableCheckingNoOfClubStuWantToJoin==1)? "CHECKED" : "";
$chkOnceEmail = ($onceEmail==1)? "checked='checked'" : "";
$chkDisableUpdate = ($disableUpdate==1)? "checked='checked'" : "";
$chkDisableHelperModificationRightLimitation = ($disableHelperModificationRightLimitation==1)? "checked='checked'" : "";
$chkDisallowPICtoAddOrRemoveMembers = ($disallowPICtoAddOrRemoveMembers==1)? "checked='checked'" : "";
if($plugin['eClassTeacherApp']){
	$chkDisablelookupClashestoSendPushMessagetoClubPIC = ($lookupClashestoSendPushMessagetoClubPIC==1) ?"checked=checked":"";
}

$basicmode += 0;
$chkDisabled = ($basicmode==0? "CHECKED":"");
$chkSimple = ($basicmode==1? "CHECKED":"");
$chkAdvanced = ($basicmode==2? "CHECKED":"");
$min_numbers = array (0,1,2,3,4,5,6,7,8,9);
$max_numbers = array (array(0,$i_ClubsEnrollment_NoLimit));
for ($i=1; $i<10; $i++)
{
     $max_numbers[] = array($i,$i);
}

$minSelection = getSelectByValue($min_numbers,"name='defaultMin'",$defaultMin);
$maxSelection = getSelectByArray($max_numbers,"name='defaultMax'",$defaultMax);
$enrollMaxSelection = getSelectByArray($max_numbers,"name='EnrollMax'",$EnrollMax);
$tiebreak += 0;
$chkRan = ($tiebreak==0? "SELECTED":"");
$chkTime = ($tiebreak==1? "SELECTED":"");
$tiebreakerSelection = "<SELECT name=tiebreak>\n";
$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
$tiebreakerSelection.= "</SELECT>\n";

$chkStu = ($enrollPersontype==0? "SELECTED":"");
$chkPa = ($enrollPersontype==1? "SELECTED":"");
$enrollPersontypeSelection = "<SELECT name='enrollPersontype'>\n";
$enrollPersontypeSelection.= "<OPTION value=0 $chkStu>".$eEnrollment['student']."</OPTION>\n";
$enrollPersontypeSelection.= "<OPTION value=1 $chkPa>".$eEnrollment['parent']."</OPTION>\n";
$enrollPersontypeSelection.= "</SELECT>\n";


## Semester Selection
$tags = ' name="TargetSemester" id="TargetSemester" ';
$selectedSemester = ($targetSemester == '')? getCurrentSemester() : $targetSemester;
$SemesterSelection = $libSCM_ui->Get_Term_Selection('TargetSemester', Get_Current_Academic_Year_ID(), $selectedSemester, $OnChange='', $NoFirst=1);

## Category Selection
//$CatSelection = $libenroll->GET_CATEGORY_SEL($sel_category, $sel_category, 'this.form.submit();', $Lang['General']['PleaseSelect']);

$htmlAry['quotaSettingsYearBaseRadio'] = $linterface->Get_Radio_Button('quotaSettingsType_YearBase', 'quotaSettingsType', 'YearBase', ($libenroll->quotaSettingsType=='YearBase'), $Class="", $eEnrollment['YearBased'], "clickedQuotaSettingsType(this.value);", $isDisabled=0);
$htmlAry['quotaSettingsTermBaseRadio'] = $linterface->Get_Radio_Button('quotaSettingsType_TermBase', 'quotaSettingsType', 'TermBase', ($libenroll->quotaSettingsType=='TermBase'), $Class="", $eEnrollment['SemesterBased'], "clickedQuotaSettingsType(this.value);", $isDisabled=0);

## Round Selection
if($plugin['SIS_eEnrollment']){
	$selectedRound = $libenroll->targetRound;
	$roundSelection = $libenroll->Get_Round_Selection('TargetRound', $selectedRound, '', false);
}

## Application Quota Table
$htmlAry['applicationQuotaTable'] = $libenroll_ui->Get_Enrollment_Application_Quota_Settings_Table($enrolConfigAry['Club'], $CategoryID=0);
if($plugin['SIS_eEnrollment']){
	$htmlAry['applicationQuotaTable'] = '';
	$htmlAry['applicationQuotaTableRound'] = $libenroll_ui->Get_Enrollment_Application_Quota_Settings_Table_Round($enrolConfigAry['Club'], $CategoryID=0);
}


if($libenroll->enableClubDefaultAttendaceSelectionControl()){
	### default attendance status 
	$libclubsenrol_ui = new libclubsenrol_ui();
	$htmlAry['attendanceDefaultSelection']  = $libclubsenrol_ui->Get_Student_Attendance_Selection('DefaultAttendanceStatus', 'DefaultAttendanceStatus', $defaultAttendanceStatus);
}	

if($sys_custom['eEnrollment']['setTargetForm']){
    include_once($PATH_WRT_ROOT."includes/libclass.php");
    $libenrolllass = new libclass();
    $ClassLvlArr = $libenrolllass->getLevelArray();
    $disabledClassArr = $libenroll->getMinMax();
    $sql = "SELECT * FROM GENERAL_SETTING WHERE SettingName = 'Club_TargetForm'";
    $result = $libenroll->returnArray($sql);
    $selectedForm = array();
    $allClassName = array();
    $disabledClassNameArr = array();
    $canUseEnrolClass = array();

    if(!empty($result[0])){
        $selectedForm = explode(",", $libenroll->settingTargetForm);
    }else{
        foreach($ClassLvlArr as $classlvl){
            $selectedForm[] = $classlvl["LevelName"];
        }
    }

    if(sizeof($disabledClassArr) > 0){
        foreach ($disabledClassArr as $disabledClassID => $classVal){
            $disabledClassNameArr[] = $libenrolllass->getLevelName($disabledClassID);
        }
    }
    
    for($i = 0; $i < sizeof($ClassLvlArr); $i++){
        $allClassName[] = $ClassLvlArr[$i]["LevelName"];
    }

    $canUseEnrolClass = array_diff($allClassName, $disabledClassNameArr);
    
    $classNum = 0;
    if ($sys_custom['eEnrolment']['HideClassLevel']) {
        $ClassLvlChk = "<input type=\"hidden\" id=\"classlvlall\">";
        foreach($canUseEnrolClass as $className){
            $checked = "";
            for ($j = 0; $j < sizeof($selectedForm); $j++) {
                if (in_array($className,$selectedForm)) {
                    $checked = " checked ";
                    break;
                }
            }
            $ClassLvlChk .= "<input type=\"hidden\" id=\"classlvl{$classNum}\" name=\"classlvl[]\" value=\"".$className."\">";
            $classNum++;
        }
    } else {
        if(sizeof($canUseEnrolClass) == sizeof($selectedForm) || empty($result[0])){
            $allChecked = " checked ";
        }
        $ClassLvlChk = "<input type=\"checkbox\" $allChecked id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]'); \"><label for=\"classlvlall\">".$eEnrollment['all']."</label>";
        $ClassLvlChk .= "<table class=\"form_table_v30\">";
        foreach($canUseEnrolClass as $className){
            $checked = "";
            for ($j = 0; $j < sizeof($selectedForm); $j++) {
                if (in_array($className,$selectedForm)) {
                    $checked = " checked ";
                    break;
                }
            }
            
            if (($classNum % 10) == 0) $ClassLvlChk .= "<tr>";
            $ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$classNum}\" name=\"classlvl[]\" value=\"".$className."\"><label for=\"classlvl{$classNum}\">".$className."</label></td>";
            if (($classNum % 10) == 9) $ClassLvlChk .= "</tr>";
            $classNum++;
        }
        $ClassLvlChk .= "</table>";
    }
}

?>

<script language="javascript">
$(document).ready( function() {	
	clickedQuotaSettingsType('<?=$libenroll->quotaSettingsType?>');
});

function changemode(obj,value){
         switch (value)
         {
                 case 0:
                 	  $('.simple').hide();
                 	  $('.advanced').hide();
                      obj.AppStart.disabled = true;
                      obj.AppEnd.disabled = true;                 
                      obj.enrolStartHour.disabled = true;
                      obj.enrolStartMin.disabled = true;
                      obj.enrolEndHour.disabled = true;
                      obj.enrolEndMin.disabled = true;
                      //obj.GpStart.disabled = true;
                      //obj.GpEnd.disabled = true;
                      obj.Description.disabled = true;
                      //obj.defaultMin.disabled = true;
                      //obj.defaultMax.disabled = true;
                      //obj.EnrollMax.disabled = true;
                      $('select.ApplyMinSel').attr('disabled', 'disabled').val(0);
                      $('select.ApplyMaxSel').attr('disabled', 'disabled').val(0);
                      $('select.EnrollMaxSel').attr('disabled', 'disabled').val(0);
                      $('select.EnrollMinSel').attr('disabled', 'disabled').val(0);
                      obj.tiebreak.disabled = true;
                      obj.enrollPersontype.disabled = true;
                      obj.disableStatus.disabled = true;
                      obj.activeMemberPer.disabled = true;
                      obj.oneEnrol.disabled = true;
                      obj.caterPriority.disabled = true;
                      obj.TargetSemester.disabled = true;
                      obj.disableCheckingNoOfClubStuWantToJoin.disabled = true;
                      obj.disableUpdate.disabled = true;
                      obj.disableHelperModificationRightLimitation.disabled = true;
                      obj.disallowPICtoAddOrRemoveMembers.disabled = true;
                      obj.onceEmail.disabled = true;
                      <?php if($libenroll->enableClubDefaultAttendaceSelectionControl()){?>
                      	obj.defaultAttendanceStatus.disabled = true;
                      <?}?>
                      break;
                 case 1:
                 	  $('.simple').show();
                 	  $('.advanced').hide();
                 	  $('#quotaSettingsType_TermBase').removeAttr('checked');
                 	  $('#quotaSettingsType_YearBase').attr('checked', 'checked');
                 	  clickedQuotaSettingsType('YearBase')
                      obj.AppStart.disabled = false;
                      obj.AppEnd.disabled = false;
                      obj.enrolStartHour.disabled = false;
                      obj.enrolStartMin.disabled = false;
                      obj.enrolEndHour.disabled = false;
                      obj.enrolEndMin.disabled = false;
                      //obj.GpStart.disabled = false;
                      //obj.GpEnd.disabled = false;
                      obj.Description.disabled = false;
                      //obj.defaultMin.disabled = false;
                      //obj.defaultMax.disabled = false;
                      //obj.EnrollMax.disabled = false;
                      $('select.ApplyMinSel').attr('disabled', '');
                      $('select.ApplyMaxSel').attr('disabled', '');
                      $('select.EnrollMaxSel').attr('disabled', '');
                      $('select.EnrollMinSel').attr('disabled', '');
                      obj.tiebreak.disabled = true;
                      obj.enrollPersontype.disabled = false;
                      obj.disableStatus.disabled = true;
                      obj.activeMemberPer.disabled = true;
                      obj.oneEnrol.disabled = true;
                      obj.caterPriority.disabled = false;
                      obj.TargetSemester.disabled = true;
                      obj.disableCheckingNoOfClubStuWantToJoin.disabled = true;
                      obj.disableUpdate.disabled = true;
                      obj.disableHelperModificationRightLimitation.disabled = true;
                      obj.disallowPICtoAddOrRemoveMembers.disabled = true;
                      obj.onceEmail.disabled = true;
                      <?php if($libenroll->enableClubDefaultAttendaceSelectionControl()){?>
                      	obj.defaultAttendanceStatus.disabled = true;
                      <?}?>
                     break;
                 case 2:
                      $('.simple').show();
                 	  $('.advanced').show();
                      obj.AppStart.disabled = false;
                      obj.AppEnd.disabled = false;
                      obj.enrolStartHour.disabled = false;
                      obj.enrolStartMin.disabled = false;
                      obj.enrolEndHour.disabled = false;
                      obj.enrolEndMin.disabled = false;
                      //obj.GpStart.disabled = false;
                      //obj.GpEnd.disabled = false;
                      obj.Description.disabled = false;
                      //obj.defaultMin.disabled = false;
                      //obj.defaultMax.disabled = false;
                      //obj.EnrollMax.disabled = false;
                      $('select.ApplyMinSel').attr('disabled', '');
                      $('select.ApplyMaxSel').attr('disabled', '');
                      $('select.EnrollMaxSel').attr('disabled', '');
                      $('select.EnrollMinSel').attr('disabled', '');
                      obj.tiebreak.disabled = false;
                      obj.enrollPersontype.disabled = false;
                      
                      <? if($plugin['eEnrollmentLite']) {?>
	                      obj.disableStatus.disabled = true;
	                      obj.activeMemberPer.disabled = true;
	                      obj.onceEmail.disabled = true;
                      <? } else { ?> 
	                      obj.disableStatus.disabled = false;
	                      obj.activeMemberPer.disabled = false;
	                      obj.onceEmail.disabled = false;
                      <? } ?>
                      obj.oneEnrol.disabled = false;
                      obj.caterPriority.disabled = false;
                      obj.TargetSemester.disabled = false;
                      obj.disableCheckingNoOfClubStuWantToJoin.disabled = false;
                      obj.disableUpdate.disabled = false;
                      obj.disableHelperModificationRightLimitation.disabled = false;
                      obj.disallowPICtoAddOrRemoveMembers.disabled = false;
                      <?php if($libenroll->enableClubDefaultAttendaceSelectionControl()){?>
                     	 obj.defaultAttendanceStatus.disabled = false;
                      <?}?>
                      break;
         }
}

function checkform(obj){
	if ((obj.basicmode[1].checked)||(obj.basicmode[2].checked))
	{
//		if (obj.defaultMax.value != 0 && obj.defaultMin.value > obj.defaultMax.value)
//		{
//			alert("<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>");
//			return false;
//		}
//		if (obj.defaultMax.value != 0)
//		{
//			//if ( (obj.EnrollMax.value == 0) || (obj.EnrollMax.value < obj.defaultMin.value) || (obj.EnrollMax.value > obj.defaultMax.value) )
//			if ( (obj.EnrollMax.value == 0) || (obj.EnrollMax.value > obj.defaultMax.value) )
//			{
//				alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
//				return false;
//			}
//		}
		
		var isVaild = true;
		$('select.ApplyMinSel').each( function() {
			var _id = $(this).attr('id');
			var _idPieces = _id.split('_');
			var _termNum = _idPieces[1];
			var _categoryId = _idPieces[2];
			
			var _applyMin = parseInt($(this).val());
			var _applyMax = parseInt($('select#ApplyMax_' + _termNum + '_' + _categoryId + '_Sel').val());
			var _enrollMax = parseInt($('select#EnrollMax_' + _termNum + '_' + _categoryId + '_Sel').val());
			var _enrollMin = parseInt($('select#EnrollMin_' + _termNum + '_' + _categoryId + '_Sel').val());
			
			if (_applyMax != 0 && _applyMin > _applyMax) {
				isVaild = false;
				
				alert('<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>');
				$(this).focus();
				return false; 	// break
			}
			
			if (_enrollMax != 0 && _enrollMin > _enrollMax) {
				isVaild = false;
				
				alert('<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>');
				$('select.EnrollMinSel').focus();
				return false; 	// break
			}
			
			if (_applyMax != 0) {
				if ( (_enrollMax == 0) || (_enrollMax > _applyMax) ) {
					isVaild = false;
					
					alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
					$(this).focus();
					return false; 	// break
				}
			}
			
			if (_enrollMin != 0 && _enrollMin > _applyMin) {
				isVaild = false;
				
				alert('<?=$Lang['eEnrolment']['jsWarning']['EnrollMinWrong']?>');
				$('select.ApplyMinSel').focus();
				return false; 	// break
			}
		});
		
		if (isVaild == false) {
			return false;
		}
		
				
		if (obj.AppStart.value != '')
		{
			if (!check_date(obj.AppStart,"<?php echo $i_invalid_date; ?>")) return false;
		}
		if (obj.AppEnd.value != '')
		{
			if (!check_date(obj.AppEnd,"<?php echo $i_invalid_date; ?>")) return false;
		}

		if (obj.AppStart.value > obj.AppEnd.value)
		{
			alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
			return false;
		}
		else if (obj.AppStart.value == obj.AppEnd.value)
		{
			// check hours
			if (obj.enrolStartHour.value > obj.enrolEndHour.value)
			{
				alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
				return false;
			}
			else if (obj.enrolStartHour.value == obj.enrolEndHour.value)
			{
				// check minutes
				if (obj.enrolStartMin.value > obj.enrolEndMin.value)
				{
					alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
					return false;
				}
			}
		}
		
		var announceDate = Trim($('input#AnnounceEnrolmentResultDate').val());
		if (announceDate!='' && !compareTimeByObjId('AppEnd', 'enrolEndHour', 'enrolEndMin', '', 'AnnounceEnrolmentResultDate', 'AnnounceEnrolmentResultHour', 'AnnounceEnrolmentResultMin', '')) {
			alert('<?=$Lang['eEnrolment']['jsWarning']['AnnounceResultTimeInvalid']?>');
			$('input#AnnounceEnrolmentResultDate').focus();
			return false;
		}

		/* Hide Manual Approval Period
		if (obj.GpStart.value != '')
		{
			if (!check_date(obj.GpStart,"<?php echo $i_invalid_date; ?>")) return false;
		}
		*/
		
		/* NO NEED to restrict the start time of approval
		if (obj.AppEnd.value > obj.GpStart.value)
		{
			alert("<?=$i_ClubsEnrollment_AppEndGpStart?>");
			return false;
		}
		*/

		/* Hide Manual Approval Period
		if (obj.GpEnd.value != '')
		{
			if (!check_date(obj.GpEnd,"<?php echo $i_invalid_date; ?>")) return false;
		}
		if (obj.GpStart.value > obj.GpEnd.value)
		{
			alert("<?=$i_ClubsEnrollment_GpStartGpEnd?>");
			return false;
		}
		*/
		
		if (obj.disableStatus.checked)
		{
			obj.disableStatus.value = 1;
		}
		else
		{
			obj.disableStatus.value = 0;
		}
		
		if (isNaN(parseInt(obj.activeMemberPer.value)) || parseInt(obj.activeMemberPer.value) < 0 || parseInt(obj.activeMemberPer.value) > 100)
		{
			alert("<?=$eEnrollment['active_member_alert']?>");
			return false;
		}
		else
		{
			if (!IsNumeric(obj.activeMemberPer.value))
			{
				alert("<?=$eEnrollment['active_member_alert']?>");
				return false;
			}
		}
		

	}

//	if (obj.basicmode[1].checked)
//	{
//		if (obj.defaultMax.value != 0 && obj.defaultMin.value > obj.defaultMax.value)
//		{
//			alert("<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>");
//			return false;
//		}
//		if ( (obj.defaultMin.value > obj.EnrollMax.value) ||
//			 (obj.EnrollMax.value > obj.defaultMax.value) )
//		{
//			alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
//			return false;
//		}
//	}
	obj.action = "basic_update.php";
	obj.submit();
	return true;
}

function showHideAlert()
{
	var alertSpan = document.getElementById("removeAlert");
	var state = alertSpan.style.visibility;
	
	if (state == "hidden")
	{
		alertSpan.style.visibility = "visible";
	}
	else
	{
		alertSpan.style.visibility = "hidden";
	}
}

function clickedQuotaSettingsType(targetType) {
	if (targetType == 'YearBase') {
		$('.termCol').hide();
		$('span#quotaSettingsTermNameSpan_0').html('<?=$Lang['eEnrolment']['WholeYear']?>');
	}
	else {
		$('.termCol').show();
		$('span#quotaSettingsTermNameSpan_0').html('<?=$Lang['eEnrolment']['WholeYearClub']?>');
	}
}

function showHideDiv(){
	
	if($('input[name="oneEnrol"]').attr('checked')){
		$('div#oneEnrolDiv').css('display', 'none');
		$('input#notCheckTimeCrash').attr('checked', false);
	}
	else{
		$('div#oneEnrolDiv').css('display', 'block');
	}
}


</script>

<br/>
<form name="form1" action="" method="post">
	<div>
		<table width="90%" border="0" cellpadding="4" cellspacing="0" align="center">
			<tr><td colspan="2" class="tabletext" align="right"><? if ($msg == 2) echo $linterface->GET_SYS_MSG("update"); ?></td></tr>
		</table>
			
		<table id="filterOptionTable" class="form_table_v30">
			<!-- Mode Selection -->
			<tr>
				<td colspan="2" class="field_title">
					<input type="radio" id="mode0" name="basicmode" value="0" onClick="changemode(this.form,0)" <?=$chkDisabled?>><label for="mode0"><?=$i_ClubsEnrollment_Disable?></label>
					<input type="radio" id="mode1" name="basicmode" value="1" onClick="changemode(this.form,1)" <?=$chkSimple?>><label for="mode1"><?=$i_ClubsEnrollment_Simple?></label>
					<input type="radio" id="mode2" name="basicmode" value="2" onClick="changemode(this.form,2)" <?=$chkAdvanced?>><label for="mode2"><?=$i_ClubsEnrollment_Advanced?></label>
				</td>
			</tr>
			
			<!-- Enrolment Stage -->
			<tr class="simple">
				<td class="field_title">
					<?= $eEnrollment['app_period']?>
				</td>
				<td >
					<?= $linterface->GET_DATE_PICKER("AppStart",$AppStart) ?>
					<?= getTimeSel("enrolStart", $AppStartHour, $AppStartMin) ?>
					&nbsp;<?= $eEnrollment['to']?>&nbsp;
					<?= $linterface->GET_DATE_PICKER("AppEnd",$AppEnd) ?>
					<?= getTimeSel("enrolEnd", $AppEndHour, $AppEndMin) ?>
				</td>
			</tr>
			
            <?php 					
            if($sys_custom['eEnrollment']['setTargetForm']){
                if ($sys_custom['eEnrolment']['HideClassLevel']) {
                	echo $ClassLvlChk;
                } else { ?>
                	<tr class="simple">
                		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_target']?> </td>
                		<td class="tabletext"><?= $ClassLvlChk?></td>
                	</tr>
                <?php 
                } 
            }
            ?>
			
			<!-- Announce Enrolment Result Date -->
			<tr>
				<td class="field_title">
					<?= $Lang['eEnrolment']['AnnounceEnrolmentResultTime'] ?>
				</td>
				<td >
					<?= $linterface->GET_DATE_PICKER("AnnounceEnrolmentResultDate", $AnnounceEnrolmentResultDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1) ?>
					<?= getTimeSel("AnnounceEnrolmentResult", $AnnounceEnrolmentResultHour, $AnnounceEnrolmentResultMin) ?>
				</td>
			</tr>
			
			<!-- Manual Approval Stage (Hided) -->
			<?
			/*
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
					<?= $eEnrollment['approve_stage']?>
				</td>
				<td class="tabletext">
					<input class="textboxnum" type="text" name="GpStart" size="10" maxlength="10" value="<?php echo $GpStart; ?>">
					<?= $linterface->GET_CALENDAR("form1", "GpStart")?>
					<?= $eEnrollment['to']?>
					<input class="textboxnum" type="text" name="GpEnd" size="10" maxlength="10" value="<?php echo $GpEnd; ?>">
					<?= $linterface->GET_CALENDAR("form1", "GpEnd")?>
				</td>
			</tr>
			*/
			?>
			
			<!- Instruction -->
			<tr class="simple">
				<td class="field_title"><?php echo $i_ClubsEnrollment_ApplicationDescription; ?></td>
				<td><textarea name="Description" rows="5" cols="40" class="textboxtext"><?php echo $Description; ?></textarea></td>
			</tr>
			
			<!- Target Semester -->
			<?php
			if($plugin['SIS_eEnrollment']){ 
				$hideSemester = 'style="display:none;"'
			?>
				<tr>
					<td class="field_title"><?= $Lang['SIS_eEnrollment']['setting']['currentRound']?></td>
					<td><?=$roundSelection?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$Lang['eEnrolment']['ApplicationQuota']?></td>
					<td>
						<?=$htmlAry['applicationQuotaTableRound']?>
					</td>
				</tr>
			<?php 
			} 
			?>
			<tr <?=$hideSemester?> class="advanced">
				<td class="field_title"><?= $eEnrollment['target_enrolment_semester']?></td>
				<td><?=$SemesterSelection?></td>
			</tr>
			
			<!- Default minimum number of applications -->
			<!--tr>
				<td class="field_title"><?= $eEnrollment['default_min_club']?></td>
				<td><?=$minSelection?></td>
			</tr-->
			
			<!- Default maximum number of applications -->
			<!--tr>
				<td class="field_title"><?= $eEnrollment['default_max_club']?></td>
				<td><?=$maxSelection?></td>
			</tr-->
			
			<!- Default maximum number of enrolled clubs -->
			<!--tr>
				<td class="field_title"><?= $eEnrollment['default_enroll_max_club']?></td>
				<td><?=$enrollMaxSelection?></td>
			</tr-->
			
			<tr <?=$hideSemester?> class="simple">
				<td class="field_title"><?=$Lang['eEnrolment']['ApplicationQuota']?></td>
				<td>
					<?=$htmlAry['quotaSettingsYearBaseRadio']?> 
					<span class="advanced" style="display:inline">
					<?=$htmlAry['quotaSettingsTermBaseRadio']?>
					</span>
					<br />
					<?=$htmlAry['applicationQuotaTable']?>
				</td>
			</tr>
			
			<tr class="simple">
				<td class="field_title"><?=$Lang['eDiscipline']['CaterPriority']?></td>
				<td><input type="checkbox" name="caterPriority" value="1" <?=$chkCaterPriority?>></td>
			</tr>
			<!- Rule for tie-breaker -->
			<tr class="advanced"><td class="field_title"><?php echo $i_ClubsEnrollment_TieBreakRule; ?></td><td><?=$tiebreakerSelection?></td></tr>
			
			<!-- Applicant's Type -->
<?	if (!$sys_custom['eEnrolment']['ClubTargetApplicant']) { ?>				
			<tr class="simple"><td class="field_title"><?php echo $eEnrollment['enroll_persontype'] ?></td><td><?=$enrollPersontypeSelection?></td></tr>
<? 	} 	?>	
			<!-- Active Member Percentage -->
			<? if($plugin['eEnrollmentLite']) 
					$active_member_per_short = "<font color='cccccc'><i>". $eEnrollment['active_member_per_short'] ."</i></font>";
				else
					$active_member_per_short = $eEnrollment['active_member_per_short'];
			?>
			<tr class="advanced">
				<td class="field_title"><?=$active_member_per_short?></td>
				<td ><input class="textboxnum" type="text" name="activeMemberPer" maxlength="6" value="<?=$activeMemberPer?>"> %</td>
			</tr>
			
			<!-- Disable changing of applicants' status after payment -->
			<? if($plugin['eEnrollmentLite']) 
					$disable_status = "<font color='cccccc'><i>". $eEnrollment['disable_status'] ."</i></font>";
				else
					$disable_status = $eEnrollment['disable_status'];
			?>
			<tr class="advanced">
				<td class="field_title"><?=$disable_status?></td>
				<td><input type="checkbox" name="disableStatus" value="1" <?=$chkDisableStauts?>></td>
			</tr>
			
			<!-- Enrol one activity only if Time Crash Occurs -->
			
			<!-- comment on 2015-01-02 and notCheckTimeCrash setting will always return 0
			<tr class="advanced">
				<td class="field_title"><//?=$Lang['eEnrolment']['Settings']['one_club_when_time_crash'] ?></td>
				<td><input type="checkbox" name="oneEnrol" value="1" onchange="showHideDiv();" <?=$chkOneEnrol?>>
					<div id="oneEnrolDiv" style="display:<?=$ShowNotCheckTimeCrash?>; border:1px solid #CCCCCC;">
						<p><input type="checkbox" id="notCheckTimeCrash" name="notCheckTimeCrash" value="1" <?=$chkNotCheckTimeCrash?>>&nbsp;<?=$Lang['eEnrolment']['Settings']['NotCheckTimeCrash']?></input></p>
					</div>
				</td>
			</tr>
			-->
			<tr class="advanced">
				<td class="field_title"><?=$Lang['eEnrolment']['Settings']['one_club_when_time_crash'] ?></td>
				<td><input type="checkbox" name="oneEnrol" value="1"  <?=$chkOneEnrol?>>
				</td>
			</tr>
			
			<!- Disable Checking Of Number Of Club a Student Want To Join -->
			<tr class="advanced">
				<td class="field_title"><?=$Lang['eEnrolment']['DisableCheckingOfNumberOfClubStudentWantToJoin']?></td>
				<td><input type="checkbox" name="disableCheckingNoOfClubStuWantToJoin" value="1" <?=$chkDisableCheckingNoOfClubStuWantToJoin?> /></td>
			</tr>
			
			<!- Send email at once -->
			<? if($plugin['eEnrollmentLite']) 
					$SendEmailAtOnce = "<font color='cccccc'><i>". $Lang['eEnrolment']['SendEmailAtOnce'] ."</i></font>";
				else
					$SendEmailAtOnce = $Lang['eEnrolment']['SendEmailAtOnce'];
			?>
			<tr class="advanced">
				<td class="field_title"><?=$SendEmailAtOnce?></td>
				<td><input type="checkbox" name="onceEmail" value="1" <?=$chkOnceEmail?> /></td>
			</tr>
		
			<!- Disable Add/Update function-->
			<tr class="advanced">
				<td class="field_title"><?=$Lang['eEnrolment']['disableUpdate']?></td>
				<td><input type="checkbox" name="disableUpdate" value="1" <?=$chkDisableUpdate?> /></td>
			</tr>
			
			<!- Disable Helper Modification Right's Limitation function-->
			<tr class="advanced">
				<td class="field_title"><?=	$Lang['eEnrolment']['disableHelperModificationRightLimitation']?></td>
				<td><input type="checkbox" name="disableHelperModificationRightLimitation" value="1" <?=$chkDisableHelperModificationRightLimitation?> /></td>
			</tr>
			
			<!- Disallow PIC to add/remove members -->
			<tr class="advanced">
				<td class="field_title"><?php echo $Lang['eEnrolment']['disallowPICtoAddOrRemoveMembers'] ; ?></td>
				<td><input type="checkbox" name="disallowPICtoAddOrRemoveMembers" value="1" value="1" <?=$chkDisallowPICtoAddOrRemoveMembers?> ><?=toolBarSpacer()?><span id="removeAlert" style="visibility:hidden"><font color="red"><?=$eEnrollment['disallowPICtoAddOrRemoveMembers'] ?></font></span></td>
			</tr>

			<?php if($plugin['eClassTeacherApp']){ ?>
			<!- Allow send push message to club PIC when check clash -->
			<tr class="advanced">
				<td class="field_title"><?php echo $Lang['eEnrolment']['lookupClashestoSendPushMessagetoclubPIC'] ; ?></td>
				<td><input type="checkbox" name="lookupClashestoSendPushMessagetoClubPIC" value="1" value="1" <?=$chkDisablelookupClashestoSendPushMessagetoClubPIC?> ></td>
			</tr>
			<?php }?>
			
			<?php if($libenroll->enableActivityDefaultAttendaceSelectionControl()){ ?>
			<!- Enable Attendance Default Selection function-->
			<tr class="advanced">
				<td class="field_title"><?=$i_StudentAttendance_default_attend_status?></td>
				<td><?=$htmlAry['attendanceDefaultSelection']?></td>
			</tr>
			<?}?>
			
			<!- Clear all records -->
			<tr>
				<td class="field_title"><span class="tabletextremark"><?php echo $i_ClubsEnrollment_ClearRecord; ?></span></td>
				<td><input type="checkbox" name="clearRecord123" value="1" onClick="showHideAlert();" disabled><?=toolBarSpacer()?><span class="tabletextremark"><?=$Lang['eEnrolment']['Settings']['ClearDataIsMoved']?></span><span id="removeAlert" style="visibility:hidden"><font color="red"><?=$eEnrollment['alert_all_enrolment_records_deleted_club']?></font></span></td>
			</tr>	
		</table>
	</div>
	<br />
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_save, "button", "checkform(document.form1)")?>
		<!--<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>-->
	</div>
</form>

<SCRIPT LANGUAGE=Javascript>
changemode(document.form1,<?=$basicmode?>);
</SCRIPT>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>