<?php
// Using: Ivan

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$LibUser = new libuser($UserID);

$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

if (!$isEnrolAdmin)
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");

$From = $_REQUEST['From'];
$SuccessCount = $_REQUEST['SuccessCount'];

# Check access right
$linterface = new interface_html();
### Title / Menu
$CurrentPage = "PageMgtClub";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# tags 
$tab_type = "club";
$current_tab = 1;
include_once("management_tabs.php");
    
### Navigation
$navigationTitle = ($From=='copyByTerm')? $Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherTerm'] : $Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherYear'];
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "javascript:js_Cancel()"); 
$PAGE_NAVIGATION[] = array($navigationTitle, ""); 
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

### Step Information
$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][1], 0);
$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][2], 1);

$ReturnMessage = $Lang['SysMgr']['SubjectClassMapping']['ReturnMessage'][$ReturnMessageKey];
$linterface->LAYOUT_START($ReturnMessage); 
?>

<script language="javascript">
function js_Cancel()
{
	window.location = "group.php";
}
</script>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr><td><?= $PageNavigation ?></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
	
	<tr><td align="center"><?=$x?></td></tr>
		
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			
			<tr>
				<td class='tabletext' align='center'><?=$SuccessCount?> <?=$Lang['eEnrolment']['CopyClub']['ClubCopiedSuccessfully']?></td>
			</tr>
			
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<?=$linterface->GET_ACTION_BTN($Lang['eEnrolment']['CopyClub']['BackToClubManagement'], "button", "window.location='group.php'"); ?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<br />


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
