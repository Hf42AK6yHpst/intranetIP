<?php

/******************************************************
 *  modification log
 *  2019-08-09 Ray
 *  - Added SubmittedAbsentLateRecord
 *	2018-07-03 Pun [121822] [ip.2.5.9.7.1]
 *  - Added save remarks
 *  20160725 Anna:
 *  	- add function CHANGE_CLUB_ATTENDENCE_STATUS
 *  20160725 Omas:
 *  	- loop POST change to loop attendanceArr
 *  20160630 Omas:
 *  	- fix php 5.4 cant save problem
 * 	20100310 Marcus:
 * 		- Modified coding to cater exempt to attendance.
 * 
 * *****************************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$json = new JSON_obj();

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();

$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);


$DataArr['EnrolGroupID'] = $EnrolGroupID;
$DataArr['GroupID'] = $GroupID;
$DataArr['StaffRole'] = $_POST['StaffRole'];

//$libenroll->DEL_GROUP_ATTENDENCE($_POST['GroupID']);
## delete updated dates only
$disabledArr = unserialize(rawurldecode($disabledArr));

//get all group dates
//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
$sql = "SELECT 
			GroupDateID 
		FROM 
			INTRANET_ENROL_GROUP_DATE 
		WHERE 
			EnrolGroupID = '$EnrolGroupID'";
$allDateArr = $libenroll->returnVector($sql);




//delete those date not DISABLED only
$notDeleteDateArr = array();
$deleteDateArr = array();
foreach ($disabledArr as $key => $value)
{
	if ($value == "DISABLED")
	{
		$notDeleteDateArr[] = $key;	
	}	
}
$deleteDateArr = array_diff($allDateArr, $notDeleteDateArr);
$deleteDateList = implode(",", $deleteDateArr);

// delete old records
//$sql = "DELETE FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE EnrolGroupID = ".$EnrolGroupID." AND GroupDateID IN ( $deleteDateList )";
//$libenroll->db_db_query($sql);

## Mark by Omas ... 20150529 - improve logic here~~~~
// $postArr = $_POST;
$postArr = $_POST['attendanceArr'];
$postRemarkArr = $_POST['attendanceRemarkArr'];
$jsonAttendanceRemark = $json->encode($postRemarkArr);
if (sizeof($postArr) > 0) {
//	20160630 fix php 5.4 can't save problem
// 	while (list($key, $value) = each($_POST)) {
	foreach((array)$postArr as $key => $value){
	    if (is_numeric($key))
	    {
	    	$DataArr['GroupDateID'] = $key;
// 	    	while (list($StudentID, $Status) = each($value))
			foreach((array)$value as $StudentID => $Status)
	    	{
	    		if($Status)
	    		{
	    			# add new records
				    $DataArr['StudentID'] = $StudentID;
				    $DataArr['Status'] = $Status;
	    			$DataArr['Remark'] = $postRemarkArr[$key][$StudentID];
				    
				    $libenroll->ADD_STU_GROUP_ATTENDENCE($DataArr);
//				    for ($i = 0; $i < sizeof($value); $i++) {
//					    $DataArr['StudentID'] = $value[$i];
//					    $libenroll->ADD_STU_GROUP_ATTENDENCE($DataArr);
//				    }

					if($sys_custom['eEnrolment']['SubmitAbsentLateRecordsToExternalSystem']) {
						$TargetStatus = false;
						if ($Status == ENROL_ATTENDANCE_ABSENT) {
							$TargetStatus = CARD_STATUS_ABSENT;
						} else if ($Status == ENROL_ATTENDANCE_LATE) {
							$TargetStatus = CARD_STATUS_LATE;
						}

						if($TargetStatus !== false) {
							$EventDateArr = $libenroll->GET_ENROL_GROUP_DATE($DataArr['EnrolGroupID'], '', '', $DataArr['GroupDateID']);
							if ($EventDateArr) {
								$RecordDate = date("Y-m-d", strtotime($EventDateArr[0]['ActivityDateStart']));
								$lcardattend->InsertSubmittedAbsentLateRecord($StudentID, $RecordDate, '', $DataArr['EnrolGroupID'], '', $TargetStatus, '', '');

							}
						}
					}
	    		}
	    	}
	    }
	}
}

//debug_pr($postArr);
$jsonAttendance = $json->encode($postArr);
//debug_pr($jsonAttendance);

// die();
// CHANGE_CLUB_ATTENDENCE_STATUS($EnrolGroupID, $jsonAttendance);

// function CHANGE_CLUB_ATTENDENCE_STATUS($EnrolGroupID, $jsonAttendance) {
//	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT.'includes/liblog.php');
	
// 	global $libenroll;
		
// 	$sql = "Select
// 				EventTitle
// 			From
// 				INTRANET_ENROL_EVENTINFO
// 			Where
// 				EnrolEventID = '".$ParEnrolEventID."'";
		
// 	$sql = "SELECT
// 				groupattendance.EnrolGroupID,
// 				groupattendance.StudentID,
// 				groupattendance.DateModified,
// 				groupattendance.RecordStatus,
// 				groupattendance.LastModifiedBy
				
				
// 			FROM
// 				INTRANET_ENROL_EVENT_ATTENDANCE as groupattendance
// 				INNER JOIN INTRANET_ENROL_GROUPINFO as groupinfo on (groupinfo.EnrolGroupID = groupattendance.EnrolGroupID)
// 			WHERE
// 				groupattendance.EnrolGroupID = '$EnrolGroupID'
	
// 	";
// 	$allDateArr = $libenroll->returnArray($sql);	
		
	# insert log
	$liblog = new liblog();
// 	$tmpAry = array();
// 	$tmpAry['EnrolGroupID'] = $EnrolGroupID;
//  	$tmpAry['StudentID'] = $StudentID;
	
	
	$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Student_Club_Attendance_Status', $jsonAttendance, ' INTRANET_ENROL_GROUP_ATTENDANCE', $EnrolGroupID);
	$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Student_Club_Attendance_Remark', $jsonAttendanceRemark, ' INTRANET_ENROL_GROUP_ATTENDANCE', $EnrolGroupID);
		
// 	return $ReturnVal;
// }

intranet_closedb();
header("Location: club_attendance_mgt.php?AcademicYearID=$AcademicYearID&EnrolGroupID=".$EnrolGroupID."&msg=2");
?>