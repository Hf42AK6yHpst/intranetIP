<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$MODULE_OBJ['title'] = $button_select." ".$eEnrollment['select'][$ppl_type];
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
    echo "You have no priviledge to access this page.";
    exit();
}

//group-based class-based selection drop-down list
$select_base_array = array (
				array ('class', $i_general_class),
				array ('club', $eEnrollment['club'])
				//array ('subject_group', $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'])
);
$select_base = getSelectByArray($select_base_array, "name=\"targetBase\" onChange=this.form.submit()",$targetBase, 0, 0);

include_once($PATH_WRT_ROOT."includes/libclass.php");
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

//selection of all groups
if ($targetBase == 'club')
{
	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	$lgroup = new libgroup();
	$sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = 5 ORDER BY Title";
	$result = $lgroup->returnArray($sql,2);
	$select_group = getSelectByArray($result, "name=\"targetGroup\" onChange=\"this.form.action='';this.form.submit()\"", $targetGroup,0,0);
}

//selection of all groups
if ($targetBase == 'subject_group')
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
}


if ($type==1 || $type==3)	//from activity => group/class-based selection
{	
	if ($targetBase == 'class' && $targetClass != ""){
			
		if (isset($EnrolEventID)){
			//filter existing members in the list
			$select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"","",0,$type,$EnrolEventID);
		}
		else{
			$select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
		}		
	}
	else if ($targetBase == 'club' && $targetGroup != ""){
		if (isset($EnrolEventID)){
			//filter existing members in the list
			$select_students = $lgroup->getStudentSelectByGroup($targetGroup,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"","",0,$type,$EnrolEventID);
		}
		else{
			$select_students = $lgroup->getStudentSelectByGroup($targetGroup,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
		}		
	}
	else if ($targetBase == 'subject_group' && $targetSubjectGroup != ""){
	}
	
}
else			//class-based selection
{
	// edited by Ivan (24 July 08) adding $GroupID to hide existing member in the list
	if ($targetClass != ""){
			
		if (isset($EnrolGroupID)){
			$select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"","",0,$type,$EnrolGroupID);
		}
		else if (isset($EnrolEventID)){
			$select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"","",0,$type,$EnrolEventID);
		}
		else{
			$select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
		}		
	}
}
    
    
################################################################

$linterface->LAYOUT_START();
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.document.form1.flag.value = 0;
     par.generalFormSubmitCheck(par.document.form1);
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

</script>

<form name="form1" action="index.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        	
		<?
		if($type==1 || $type==3)
		{
		?>
			<tr>
				<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $eEnrollment['selection_mode']?>:</span></td>
				<td width="80%"><?php echo $select_base; ?></td>
			</tr>
			<tr><td colspan="2" height="5"></td></tr>	
			<?
			if($targetBase == 'class')
			{			
			?>
				<tr>
					<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $eEnrollment['class_name'] ?>:</span></td>
					<td width="80%"><?php echo $select_class; ?></td>
				</tr>
				<?
				if($targetClass != "")
				{
				?>
					<tr><td colspan="2" height="5"></td></tr>	
					<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td colspan="2" height="5"></td></tr>
					<tr>
						<td valign="top" nowrap="nowrap" ><span class="tabletext"><?= $eEnrollment['student']?>: </span></td>
						<td width="80%" style="align: left">
					       	<table border="0" cellpadding="0" cellspacing="0" align="left">
								<tr> 
									<td>
					       				<?php echo $select_students; ?>
					       			</td>
					       			<td style="vertical-align:bottom">        
								        <table width="100%" border="0" cellspacing="0" cellpadding="6">
											<tr> 
												<td align="left"> 
													<?= $linterface->GET_SMALL_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
												</td>
											</tr>
											<tr> 
												<td align="left"> 
													<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
												</td>
											</tr>
										</table>
					       			</td>
					       		</tr>
					       	</table>
						</td>
					</tr>
				<?
				}
				?>
			<?
			}
			else if ($targetBase == 'club')
			{
			?>
				<tr>
					<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $eEnrollment['club']?>:</span></td>
					<td width="80%"><?php echo $select_group; ?></td>
				</tr>
				<?
				if($targetGroup != "")
				{
				?>
					<tr><td colspan="2" height="5"></td></tr>	
					<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td colspan="2" height="5"></td></tr>
					<tr>
						<td valign="top" nowrap="nowrap" ><span class="tabletext"><?= $eEnrollment['student']?>: </span></td>
						<td width="80%" style="align: left">
					       	<table border="0" cellpadding="0" cellspacing="0" align="left">
								<tr> 
									<td>
					       				<?php echo $select_students; ?>
					       			</td>
					       			<td style="vertical-align:bottom">        
								        <table width="100%" border="0" cellspacing="0" cellpadding="6">
											<tr> 
												<td align="left"> 
													<?= $linterface->GET_SMALL_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
												</td>
											</tr>
											<tr> 
												<td align="left"> 
													<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
												</td>
											</tr>
										</table>
					       			</td>
					       		</tr>
					       	</table>
						</td>
					</tr>
				<?
				}
				?>
			<?
			}
			?>
		<?
		}
		else
		{
		?>
			<tr>
				<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $eEnrollment['class_name'] ?>:</span></td>
				<td width="80%"><?php echo $select_class; ?></td>
			</tr>
			<tr><td colspan="2" height="5"></td></tr>	
			<?
			if($targetClass != "")
			{
			?>
				<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td valign="top" nowrap="nowrap" ><span class="tabletext"><?= $eEnrollment['student']?>: </span></td>
					<td width="80%" style="align: left">
				       	<table border="0" cellpadding="0" cellspacing="0" align="left">
							<tr> 
								<td>
				       				<?php echo $select_students; ?>
				       			</td>
				       			<td style="vertical-align:bottom">        
							        <table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
				       			</td>
				       		</tr>
				       	</table>
					</td>
				</tr>
			<?
			}
			?>
		<?	
		} 
		?>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
<input type="hidden" name="ppl_type" value="<?php echo $ppl_type; ?>" />
<input type="hidden" name="type" value="<?php echo $type; ?>" />
<input type="hidden" name="EnrolEventID" value="<?php echo $EnrolEventID; ?>" />

</form>

<script language="javascript">
if (document.form1.elements['targetID[]'])
	SelectAll(document.form1.elements['targetID[]']);
</script>

	<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
?>