<?
//Using: 
/*
 * 	Log
 * 
 * 	Description: output json format data
 * 
 *  check if booking status does not allow to delete booking, if so, also not allow to delete course
 *
 *  2018-08-06 [Cameron]
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$y = '';

$linterface = new libclubsenrol_ui();

    $enrolEventIDAry = IntegerSafe($_POST['EnrolEventID']);
    $retAry = array();
    
    if (count($enrolEventIDAry)) {
        
        $sql = "SELECT  
                            ieed.EnrolEventID,
                    FROM
                    		INTRANET_ENROL_EVENT_DATE ieed
                    INNER JOIN
                    		INTRANET_ENROL_EVENTINFO iee ON iee.EnrolEventID=ieed.EnrolEventID
                    INNER JOIN
                            INTRANET_ENROL_EBOOKING_RELATION ieer ON ieer.MeetingDateID=ieed.EventDateID
                    INNER JOIN 
                            INTRANET_EBOOKING_ROOM_BOOKING_DETAILS ierbd ON ierbd.BookingID=ieer.BookingRecordID
                    WHERE
                            ieed.RecordStatus='1'
                    AND
                            ieer.IsDelete='0'
                    AND
                    		ieed.EnrolEventID IN ('".implode("','",(array)$enrolEventIDAry)."')
                    AND     
                            ierbd.BookingStatus NOT IN (-1,0,999)
                    GROUP BY ieed.EnrolEventID";
        $checkAry = $libenroll->returnResultSet($sql);
        if (count($checkAry)) {
            foreach((array)$checkAry as $_check) {
                $enrolEventID = $_check['EnrolEventID'];
                $retAry[] = $enrolEventID;            
            }
        }
    }
    $x = count($retAry) ? implode(',',$retAry) : '';
    $json['success'] = true;
        
if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
}

$json['html'] = $x;

echo $ljson->encode($json);

intranet_closedb();
?>