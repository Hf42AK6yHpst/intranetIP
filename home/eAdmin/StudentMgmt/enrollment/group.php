<?php
// Using: 

/******************************************
 * Modification Log:
 * 
 *  2020-02-26 Tommy 
 *  - added new export button for export term performance
 *  
 *  2020-01-07 Tommy #Q164968
 *  - added "refresh performance" for reloading performance in enrollment
 * 
 *  2020-01-06 Tommy #Q164968
 *  - added new export -> attendance and performance for 'heungto'
 * 
 *  2019-09-24 Cameron
 *  - set memory and execution time to unlimit for HKPF 
 *  
 *  2018-08-21 Anna
 *  - added $sys_custom['eEnrolment']['ClubHelperCanAddMeeting']
 * 
 *  2018-08-09 Cameron
 *  - fix: checkEdit(), remove double AcademicYear parameter
 *  - fix: add action value to Check_Go_Search() and click_order(), so that it would clear previous action status
 *  
 *  2018-08-07 Cameron
 *  - show total number of course(s) in a curriculum for HKPF
 *  
 *  2018-08-04 Cameron
 *  - hide items for HKPF
 * 
 *  2017-12-13 Pun 
 *  - added NCS cust
 * 
 *  2017-09-27 (Anna) #D125932 
 *  - fix bug for club helper can manage member
 * 
 *  2017-08-14 (Omas) #P110324 
 *  		Modified ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
 *  
 * 	2017-06-19 Villa
 *	- Modified ReplySlip ThickBox to dynamic
 *
 *  2017-05-31 Anna
 *  - Modified export/export_club_info/export_attendance_record
 *  
 *  2017-04-21 Villa
 *  - Add ReplySlip Related 
 *  
 * 	2015-06-12 Evan
 *  - Add payment function
 *  - $thisClubInfoArr = $groups[$i]; added to retrive the payment detail
 * 
 * 	2015-06-08
 * 	- Fix the bug of failed in searching backslash and double quotation mark.
 * 
 *	2014-11-13 
 * 	- Performance Improve: $isAnyGroupAdmin, $thisIsClubPIC, $thisIsClubHelper, $isThisGroupAdminWithUserMgmtRight only check when the user is not Admin 
 *  
 *  2014-10-14
 *  - Add round report for SIS customization
 * 
 *  2013-05-28 Rita
 *  - add generate active member
 * 
 *  2012-11-06 Rita 
 * 	- add export attendance record btn
 * 
 *	2012-03-02 YatWoon [2012-0109-1024-31066]
 *	- add OrderBy, OrderDesc 

 *	2011-11-22	YatWoon
 *	- add intranet_htmlspecialchars to keyword, to cater special character (e.g. &)
 *
 *	2011-10-31	YatWoon
 *	- add customization access right checking Customization_HeungToChecking() # For Heung To only: only admin can add/edit/delete member (for club only)
 * 
 *	2011-09-19	Henry Chow
 *	- Admin of Group (with access right of Member Mgmt) can edit member list (flag : $isThisGroupAdminWithUserMgmtRight)
 *
 *	2011-09-07	YatWoon
 *	- lite version implementation
 *
 *	2011-02-07 Marcus
 *	- Added Export Club Info
 *
 *	2010-12-14 YatWoon
 *	- display club name according to UI lang sesstion
 *
 *	2010-12-02	YatWoon
 *	- $sys_custom['YeoCheiMan_generate_performance']
 *	- IP25 UI standard
 *
 * 		20100802 Thomas
 * 			- Check the data is freezed or not
 *			  Show alert message when adding / updating the data
 *
 ******************************************/

set_time_limit(0);
ini_set('memory_limit',-1);

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}

intranet_auth();
intranet_opendb();


//if(!$plugin['eEnrollment'])
//{
//	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	$laccessright = new libaccessright();
//	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//	exit;
//}

# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_sel_category", "sel_category");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Semester", "Semester");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Keyword", "keyword");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_PIC_View", "pic_view");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Helper_View", "helper_view");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Helper_OrderBy", "OrderBy");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Helper_OrderDesc", "OrderDesc");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Helper_AcademicYearID", "AcademicYearID");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Helper_Semester", "Semester"); 
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Round", "Round"); 

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$pic_view = '';
	$helper_view = '';
}
else 
	updateGetCookies($arrCookies);
	
if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$libenroll = new libclubsenrol($AcademicYearID);


$LibUser = new libuser($_SESSION['UserID']);
$lurlparahandler = new liburlparahandler('', '', $enrolConfigAry['encryptionKey']);

$keyword = isset($keyword)? intranet_htmlspecialchars($keyword) : '';
$sel_category = isset($sel_category)? $sel_category : '';
$Semester = isset($Semester) && $Semester != ''? $Semester : 0;
$Round = isset($Round) && $Round != ''? $Round : '';

$AcademicYearID = IntegerSafe($AcademicYearID);
$Semester = IntegerSafe($Semester);
$sel_category = IntegerSafe($sel_category);
$Round = IntegerSafe($Round);


if (isset($_GET['pic_view']) && $_GET['pic_view'] != '')
{
	$pic_view = $_GET['pic_view'];
	$helper_view = '';
}

if (isset($_GET['helper_view']) && $_GET['helper_view'] != '')
{
	$helper_view = $_GET['helper_view'];
	$pic_view = '';
}


if ($plugin['eEnrollment'])
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	$isNormalUser = $libenroll->IS_NORMAL_USER($_SESSION['UserID']);
	$haveClubMgmt = $libenroll->HAVE_CLUB_MGT();
	$haveActivityMgmt = $libenroll->HAVE_EVENT_MGT();
	
	$isClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID']);
	$isClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID']);

	if(!$isEnrolAdmin){
		$isAnyGroupAdmin = $libenroll->checkAnyIntranetGroupAdminWithUserMgmtRight();
	}
	if ( ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT) && !($isEnrolAdmin) && !($isEnrolMaster) && !$haveClubMgmt && !$haveActivityMgmt && !$isAnyGroupAdmin)
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
		
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)) {
		if ($helper_view)
			$CurrentPage = "PageClubAttendanceMgt";
			
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	//$libenroll->UPDATE_GROUPS_APPROVED();

    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        # tags 
        $tab_type = "club";
        $current_tab = 1;
        include_once("management_tabs.php");
        
        # Acadermic Year Selection        	
		$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.Semester.selectedIndex=0;document.form1.action=\'group.php\';document.form1.submit();"', 1, 0, $AcademicYearID);
// 		debug_pr(getSelectAcademicYear('21'));
        # category filter
        $category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "document.form1.action='group.php'; document.form1.submit()", $eEnrollment['all_categories']);
// 		if (isset($sel_category) && $sel_category != "")
// 		{
// 			$category_conds = " AND iegi.GroupCategory = $sel_category ";
// 		}
		
		/*
		$allYearsInfo = GetAllAcademicYearInfo();
		$yearsAry = array();
		for($a=0, $a_max=sizeof($allYearsInfo); $a<$a_max; $a++) {
			list($yearid, $yearEn, $yearB5) = $allYearsInfo[$a];
			$yearsAry[$a] = array($yearid, Get_Lang_Selection($yearB5, $yearEn)); 	
		}
		$yearFilter = getSelectByArray($yearsAry, 'name="AcademicYearID" id="AcademicYearID" onChange=""', $AcademicYearID, 0, 0, $Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear']);
		*/
		
		//if ($Semester == '')
		//	$semester_conds = " AND (iegi.Semester Is NULL Or iegi.Semester = '$Semester') ";
		//else
		//	$semester_conds = " AND (iegi.Semester = '$Semester') ";	
		$SemesterFilter = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, $OnChange="document.form1.action='group.php'; document.form1.submit()", $NoFirst=1, $NoPastTerm=0, $withWholeYear=1);
		if($plugin['SIS_eEnrollment']){
			$SemesterFilter = $libenroll->Get_Round_Selection('Round', $Round,"document.form1.action='group.php'; document.form1.submit()");
		}
		
// 		if ($Semester != '')
// 		{
// 				$SemesterAry = array($Semester);
// 				if (in_array(0, $SemesterAry))
// 					$semester_conds = " And (iegi.Semester In ('".implode("','", $SemesterAry)."') Or iegi.Semester Is Null Or iegi.Semester = '')";
// 				else
// 					$semester_conds = " And iegi.Semester In ('".implode("','", $SemesterAry)."') ";
							
// 				if($plugin['SIS_eEnrollment']){
// 					if (in_array(0, $SemesterAry))
// 						$semester_conds = " And (iegi.RoundNumber In ('".implode("','", $SemesterAry)."') Or iegi.RoundNumber Is Null Or iegi.RoundNumber = '')";
// 					else
// 						$semester_conds = " And iegi.RoundNumber In ('".implode("','", $SemesterAry)."') ";
// 				}
// 		}
		
		
		# Search box
		$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

		//do not search for the initial textbox text
// 		if ($keyword == $eEnrollment['enter_name']) $keyword = "";
// 		$name_conds = " AND ig.Title LIKE '%".$keyword."%' ";
		//debug_pr($name_conds);
        $linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
        
        $isTeacher = ($_SESSION['UserType']==USERTYPE_STAFF)? true : false;
        
//        if (($isEnrolAdmin)||($isEnrolMaster))
//        {
//	        //show all clubs if the user is an admin/master/teacher
//	     	//$quotas = $libenroll->getGroupQuota();
//			//$ClubMemberInfoArr = $libenroll->Get_Club_Member_Info('', array(2), Get_Current_Academic_Year_ID(), $WithIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, array($Semester), $keyword, array($sel_category));
//			//$groups = $libenroll->getGroupsForEnrolSet("",$category_conds, $name_conds, $semester_conds);
//		}
//        else
//        {
//	     	//show the clubs which the user have management right of
//	    	if ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)
//			{
//				// student / parent => show the clubs according to PIC or Helper view
//				if ($pic_view)
//					$EnrolGroupArr = $libenroll->Get_PIC_Club($_SESSION['UserID']);
//				else
//					$EnrolGroupArr = $libenroll->Get_Helper_Club($_SESSION['UserID']);
//			}
//			else
//			{
//				// teacher or staff => show the clubs which the user have management right of
//				$EnrolGroupArr = $libenroll->GET_MANAGEMENT_CLUB($_SESSION['UserID']);
//			}
//			
//	    	//$quotas = $libenroll->getGroupQuota($EnrolGroupArr);
//	    	//$ClubMemberInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupArr, array(2), Get_Current_Academic_Year_ID());
//	    	//$groups = $libenroll->getGroupsForEnrolSet($EnrolGroupArr,$category_conds, $name_conds, $semester_conds);
//        }
        

		# Order
		$OrderBy = $OrderBy ? $OrderBy : "ClubCode";
		$OrderDesc = $OrderDesc ? $OrderDesc : "asc";
		$OrderIcon = "<image src='{$image_path}/{$LAYOUT_SKIN}/icon_sort_". substr($OrderDesc,0,1) ."_off.gif' border=0 align='absmiddle'>";

        $groups = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category, stripslashes($keyword), array($Semester), $OrderBy, $OrderDesc, $ApplyUserType='', $Round);
         
      	$IsClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], '', "Club", $AcademicYearID);
      	
      	if ($helper_view==0 && ($isEnrolAdmin || $isEnrolMaster || $IsClubPIC ||  ($isClubHelper && $sys_custom['eEnrolment']['ClubHelperCanAddMeeting']))) {
       		$ScheduleHeader = '<th>'.$eEnrollment['add_club_activity']['step_2'].'</th>';
        }
     
      
        if ($isEnrolAdmin || $isEnrolMaster)
        {
        	
        	$toolsBar = "<div class=\"common_table_tool\">";
        	
	     	$menuBar = "";
// 	     	$menuBar2 = "";
	     	
	     	if (!$sys_custom['project']['HKPF']) {
    			$toolsBar .= "
    				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:Js_View_Active_Member_List(document.form1,'all_active_member.php?AcademicYearID=$AcademicYearID&type=club')")  ."\" class=\"tool_approve\">". $eEnrollment['button']['active_member'] ."</a>
    				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitForm(document.form1,'group_update.php?AcademicYearID=$AcademicYearID&type=enable','enable')")  ."\" class=\"tool_approve\">". $eEnrollment['enable_online_registration'] ."</a>
    				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitForm(document.form1,'group_update.php?AcademicYearID=$AcademicYearID&type=disable','disable')")  ."\" class=\"tool_reject\">". $eEnrollment['disable_online_registration'] ."</a>";
	     	}
			$toolsBar .= "
                <a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkEdit(document.form1,'EnrolGroupID[]','group_edit.php?AcademicYearID=$AcademicYearID')")  ."\" class=\"tool_edit\">". $eEnrollment['Edit_Club_Info'] ."</a>
				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRemove(document.form1,'EnrolGroupID[]','group_remove.php?AcademicYearID=$AcademicYearID&')")  ."\" class=\"tool_delete\">". $button_delete ."</a>
				</div>
			";
			
			if($libenroll->AllowToEditPreviousYearData) {
				# Left hand Tools bar (New, Import Member)
				$menuBar = "<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "group_new.php?AcademicYearID=$AcademicYearID&Semester=$Semester&Round=$Round") ."\" class=\"new\"> ".$Lang['Btn']['New']."</a>";
				
				if(!$plugin['SIS_eEnrollment'] && !$sys_custom['project']['HKPF']){
					# import parameters
					if($libenroll->Customization_HeungToChecking())
					{
				        $type = "clubAllMember";
						$import_parameters = "AcademicYearID=$AcademicYearID&type=$type&Semester=$Semester&Round=$Round";
						/*
						$menuBar .= "<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "import.php?".$import_parameters) ."\" class=\"import\"> ".$Lang['Btn']['Import']."</a>";
						
						*/ 
						# import member
						$importMember .= "<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "import.php?".$import_parameters) ."\" class=\"sub_btn\"> ".$Lang['eEnrolment']['Btn']['ExportMember2']."</a>";
						
						# import club info
						$importClubInfo .= "<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "import_club_info.php?".$import_parameters) ."\" class=\"sub_btn\"> ".$Lang['eEnrolment']['Btn']['ExportClubInfo2']."</a>";
						
						# import menu
						$menuBar .= '<div class="btn_option" id="ImportDiv">
											<a onclick="js_Clicked_Option_Layer(\'import_option\', \'btn_import\');" id="btn_import" class="import option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Import'].'</a>
											<br style="clear: both;">
											<div onclick="js_Clicked_Option_Layer_Button(\'import_option\', \'btn_import\');" id="import_option" class="btn_option_layer" style="visibility: hidden;">
											'.$importMember.'
											'.$importClubInfo.'
											</div>
									</div>';
						
					}
				}
			}
				
			if(!$plugin['SIS_eEnrollment'] && !$sys_custom['project']['HKPF']){
				# export parameters
		        $type = 6;
			//	$export_parameters = "type=$type&category_conds=$category_conds&name_conds=$name_conds&semester_conds=$semester_conds&AcademicYearID=$AcademicYearID";
				$export_parameters = "type=$type&CategoryID=$sel_category&Keyword=$keyword&Semester=$Semester&AcademicYearID=$AcademicYearID";
				if ($sys_custom['eEnrolment']['ClubMemberInfo_ExtraExportInfo']) {
					$exportFile = 'export_member_info_extra.php';
					$lurlparahandler->setParaDecrypted($export_parameters."&recordType=".$enrolConfigAry['Club']);
					$lurlparahandler->performAction(liburlparahandler::actionEncrypt);
					$export_parameters = 'p='.$lurlparahandler->getParaEncrypted();
				}
				else {
					$exportFile = 'export.php';
				}
				$exportMember = "<a href=\"".$exportFile."?".$export_parameters."\" class=\"sub_btn\"> ".$Lang['eEnrolment']['Btn']['ExportMember2']."</a>";
				
				if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
				    $exportMember2 = "<a href=\"".$exportFile."?".$export_parameters."&showTerm=1\" class=\"sub_btn\"> ". $Lang['eEnrolment']['Btn']['ExportMemberPerformance']."</a>";
				}
				
				# export Clubs information
			//	$exportClubInfo = "<a href=\"export_club_info.php?AcademicYearID=$AcademicYearID\" class=\"sub_btn\"> ".$Lang['eEnrolment']['Btn']['ExportClubInfo2']."</a>";
				$exportClubInfo = "<a href=\"export_club_info.php?AcademicYearID=$AcademicYearID&Keyword=$keyword&Semester=$Semester&Category=$sel_category\" class=\"sub_btn\"> ".$Lang['eEnrolment']['Btn']['ExportClubInfo2']."</a>";

				# export attendance records
			//	$exportAttendanceRecord = "<a href=\"export_attendance_record.php?AcademicYearID=$AcademicYearID&type=2&Semester=$Semester&Round=$Round\" class=\"sub_btn\"> ".$i_Profile_Attendance."</a>";
				$exportAttendanceRecord = "<a href=\"export_attendance_record.php?AcademicYearID=$AcademicYearID&type=2&Semester=$Semester&Round=$Round&Keyword=$keyword&Category=$sel_category\" class=\"sub_btn\"> ".$i_Profile_Attendance."</a>";
				
				# export attendance records with performance
//				$UserID = $_SESSION["UserID"];
//				if($UserID == 1){
				if($sys_custom['eEnrolment']['Refresh_Performance']){
				    //           				$exportAttendancePerformance = "<a href=\"export_attendance_performance.php?AcademicYearID=$AcademicYearID&type=2&Semester=$Semester&Round=$Round&Keyword=$keyword&Category=$sel_category\" class=\"sub_btn\">".$Lang['eEnrolment']['Refresh_Performance']['Export']."</a>";
				    $exportAttendancePerformance = "<a href=\"export_attendance_performance2.php?AcademicYearID=$AcademicYearID&type=2&Semester=$Semester&Round=$Round&Keyword=$keyword&Category=$sel_category\" class=\"sub_btn\">".$Lang['eEnrolment']['Refresh_Performance']['Export']."</a>";
    				}
//				}

				
				# export
				$menuBar .= '
					<div class="btn_option"  id="ExportDiv"  >
	
						<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Export'].'</a>
						<br style="clear: both;">
						<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
						'.$exportMember.'
                        '.$exportMember2.'
						'.$exportClubInfo.'
						'.$exportAttendanceRecord.'
                        '.$exportAttendancePerformance.'
						</div>
					</div>'; 
				
				# Copy from Last Year
				if($plugin['eEnrollmentLite'])
				{
					$menuBar .= "<i class=\"copy\"> ".$Lang['eEnrolment']['CopyClub']['CopyFromOtherYearTerm']."</i>";
				} 
				else  
				{
					if ($libenroll->AllowToEditPreviousYearData && $isEnrolAdmin)
					{
						//$menuBar .= "<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "javascript:js_Go_Copy_Club();") ."\" class=\"copy\"> ".$Lang['eEnrolment']['CopyClub']['CopyFromOtherYearTerm']."</a>";
						
						### Export Button
						$CopyOptionArr = array();
						$CopyOptionArr[] = array('javascript:js_Go_Copy_Club();', $Lang['eEnrolment']['FromOtherYear']);
						$CopyOptionArr[] = array('javascript:js_Go_Copy_Club_By_Term();', $Lang['eEnrolment']['FromOtherTerm']);
						$menuBar .= $linterface->Get_Content_Tool_v30('copy', $href="javascript:void(0);", $text='', $CopyOptionArr, $other="", $divID='CopyDiv');
					}
				}
				
				if($sys_custom['eEnrolment']['Refresh_Performance'] && $AcademicYearID == Get_Current_Academic_Year_ID()){
				    $refreshPerformance = "<a href=\"refresh_performance.php?AcademicYearID=$AcademicYearID&type=2&Semester=$Semester\" class=\"generate option_layer\"> " .$Lang['eEnrolment']['Refresh_Performance']['Name']."</a>";
    				$menuBar .= $refreshPerformance;
				}
			}
        }
        else
        {
	     	$toolsBar = "&nbsp;";
	     	$menuBar = "&nbsp;";
// 	     	$menuBar2 = "&nbsp;";
        }
        
        if($libenroll->AllowToEditPreviousYearData && $sys_custom['YeoCheiMan_generate_performance'])
		{
			$menuBar .= "<a href=\"javascript:CickGenPerformance();\" class=\"generate_btn\"> ". $Lang['eEnrolment']['GeneratePerformance'] ."</a>";
		}
        
        # System message		
		if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
		if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
		if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
		

?>

<script language="javascript">
function PaymentConfirm(obj, jParID) {
	AlertPost(obj, 'payment.php?EnrolGroupID='+jParID, '<?= $eEnrollment['js_prepayment_alert']?>');
}

function js_Go_Copy_Club() {
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'copy_club_step1.php';
    jsFormObj.method = "POST";
    jsFormObj.submit();
}

function js_Go_Copy_Club_By_Term() {
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'copy_club_by_term_step1.php';
    jsFormObj.method = "POST";
    jsFormObj.submit();
}

function checkEdit(obj,element,page){
        if(countChecked(obj,element)==1) {
	        	var id = document.getElementsByName(element);
	        	var len = id.length;
	        	var targetIndex;
	        	for( i=0 ; i<len ; i++) {
	                if (id[i].checked)
	                {
	                	targetIndex=i;
	                	break;
                	}
		        }
                obj.action=page+'&EnrolGroupID='+id[targetIndex].value;
                obj.submit();
        } else {
                alert(globalAlertMsg1);
        }
}
function submitForm(obj,page,action)
{
	if(countChecked(obj,'EnrolGroupID[]')==0)
    	alert(globalAlertMsg2);
    else{
	    if(action == 'enable')
	    { 
	    	if(confirm(globalAlertMsgEnable)){
	            obj.action = page;
	            obj.method = "POST";
	            obj.submit();
	            return;
            }
    	}
    	else if(action == 'disable')
	    { 
	    	if(confirm(globalAlertMsgDisable)){
	            obj.action = page;
	            obj.method = "POST";
	            obj.submit();
	            return;
            }
    	}
    }
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) { // enter 
		document.form1.action='group.php';
		document.form1.submit();
	}
	else
		return false;
}

function CickGenPerformance()
{
	window.location="generate_club_performance.php";
}

function click_order(o, d)
{
	document.form1.action='group.php';
	document.form1.OrderBy.value = o;
	document.form1.OrderDesc.value = d;	
	document.form1.submit();
}

function Js_View_Active_Member_List(obj,page)
{
	if(countChecked(obj,'EnrolGroupID[]')==0)
    	alert(globalAlertMsg2);
    else{
        obj.action = page;
        obj.method = "POST";
        obj.submit();
        return;
    }
}
function onLoadThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	load_dyn_size_thickbox_ip('<?=$eEnrollment['replySlip']['ReplySlipDetail']?>', 'onLoadReplySlipThickBox('+ReplySlipGroupMappingID+','+EnrolGroupID+','+ReplySlipID+');');
}
function onLoadReplySlipThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	$.ajax({
		url: "ReplySlip_ajax_ReplySlipReply_view.php",
		type: "POST",
		data: {"ReplySlipGroupMappingID": ReplySlipGroupMappingID, "EnrolGroupID": EnrolGroupID,"ReplySlipID":ReplySlipID},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
		}
	});
}
function goExport(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
    	document.form1.action = "ReplySlip_ajax_ReplySlipReply_view.php?ReplySlipGroupMappingID="+ReplySlipGroupMappingID+"&EnrolGroupID="+EnrolGroupID+"&ReplySlipID="+ReplySlipID+"&isExport="+1;
    	document.form1.target = "_blank";
    	document.form1.method = "post";
    	document.form1.submit();
}
</script>

<form name="form1" id="form1" method="post">


<div class="content_top_tool">
	<div class="Conntent_tool"><?=$menuBar?></div>
	<div class="Conntent_search"><?=$searchTag?></div>
	<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<div class="table_filter">
<?php if (!$sys_custom['project']['HKPF']):?>		
			<?=$yearFilter?>
			<?= $SemesterFilter ?>
			<?= $category_selection ?>
<?php endif;?>			
		</div> 
	</td>
	<?if($libenroll->AllowToEditPreviousYearData) {?>
	<td valign="bottom"><?=$toolsBar?></td>
	<?}?>
</tr>
</table>
</div>

<table class="common_table_list">
<thead>
	<tr>
		<th class="num_check">#</th>
		<th><b><a href="javascript:click_order('ClubCode','<?= $OrderBy=="ClubCode" ? ($OrderDesc=="desc"?"asc":"desc") : "asc" ?>')"><?=$Lang['eEnrolment']['ClubCode']?><?= $OrderBy=="ClubCode" ? $OrderIcon : "" ?></a></b></th>
		<th><b><a href="javascript:click_order('ClubName','<?= $OrderBy=="ClubName" ? ($OrderDesc=="desc"?"asc":"desc") : "asc" ?>')"><?=$i_ClubsEnrollment_ClubName?><?= $OrderBy=="ClubName" ? $OrderIcon : "" ?></a></b></th>
		
<?php if (!$sys_custom['project']['HKPF']):?>
		<?=$ScheduleHeader?>
		
	    <?php 
	    if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] != ""){ 
	    }else if($plugin['SIS_eEnrollment']){
			echo "<th>{$Lang['SIS_eEnrollment']['Round']}</th>";
		}else{
			echo "<th>{$Lang['General']['Term']}</th>";
		}

		?>
		<th><?=$eEnrollment['add_activity']['act_category']?></th>
	    <?php if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role'] == ""){ ?> 
		<th align="center"><?=$i_ClubsEnrollment_OnlineRegistration?></th>
		<?php } ?>
		<th align="center"><?=$eEnrollment['add_activity']['act_quota']?></th>
		<th align="center"><?=$eEnrollment['member']?></th>
		<? if ($isEnrolAdmin||$isEnrolMaster||$haveClubMgmt) { ?>
			<th align="center"><?=$eEnrolment['attendance_title']?></th>
		<? } ?>

		<?php if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] != ""){ ?>
		<? }else if ($isEnrolAdmin||$isEnrolMaster||$thisIsClubPIC || $pic_view ||$isClubPIC) { ?>
			<th align="center"><?=$eEnrollment['payment']?></th>
		<? } ?>
		<? if($sys_custom['eEnrolment']['ReplySlip']){?>
		<th align="center"><?=$eEnrollment['replySlip']['Signed_Total']?></th>
		<?php }?>

<?php else: // HKPF show number of courses in the curriculum?>
		<th align="center"><?=$Lang['eEnrolment']['curriculumTemplate']['curriculum']['numberOfCourse']?></th>
<?php endif;?>
		
		<? if($isEnrolAdmin||$isEnrolMaster){ ?>
			<th class="num_check"><input type="checkbox" onClick="(this.checked)?setChecked(1,document.form1,'EnrolGroupID[]'):setChecked(0,document.form1,'EnrolGroupID[]')"></th>
		<?}?>
		
	</tr>
</thead>

<?php
	$displayedRecord = 0;
	$numOfClub = count($groups);
	
	for ($i=0; $i<$numOfClub; $i++)
	{
		 $thisClubInfoArr = $groups[$i];
		 $EnrolGroupID = $groups[$i]['EnrolGroupID'];
		 
		 //ReplySlip Relation
		 if($sys_custom['eEnrolment']['ReplySlip']){
			 echo $linterface->Include_Thickbox_JS_CSS();
			 $NumOfMember= $groups[$i]['NumOfMember'];
			 $ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolGroupID);
			 $ReplySlipRelation = $ReplySlipRelation[0];
			 $ReplySlipGroupMappingID = $ReplySlipRelation['ReplySlipGroupMappingID'];
			 $ReplySlipID = $ReplySlipRelation['ReplySlipID'];
			 
			 if($ReplySlipGroupMappingID){
			 	$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
			 	$MemberList = $libenroll->getEventStudentInfo($EnrolGroupID);
			 	//$NumOfSigned = count($ReplySlipReply);
			 	$repliedStudentId = Get_Array_By_Key( $ReplySlipReply, 'StudentID');
			 	$memberStudentId = Get_Array_By_Key( $MemberList, 'StudentID');
			 	$repliedStudentIdArr = array_intersect($repliedStudentId, $memberStudentId);
			 	$NumOfSigned = count($repliedStudentIdArr);
// 			 	$ReplySlip_SignTotal = $linterface->Get_Thickbox_Link('820', '1640', "",$eEnrollment['replySlip']['ReplySlipDetail'], "onLoadReplySlipThickBox($ReplySlipGroupMappingID,$EnrolGroupID,$ReplySlipID)","FakeLayer",$NumOfSigned.'/'.$NumOfMember);
			 	//$ReplySlip_SignTotal = '<a href="javasrcipt:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$EnrolGroupID.','.$ReplySlipID.')">'.'<img src="/images/2009a/eOffice/icon_resultview.gif" border="0" align="absmiddle">'.'</a>';
			 	$ReplySlip_SignTotal = '<a href="javascript:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$EnrolGroupID.','.$ReplySlipID.')";>'.$NumOfSigned.'/'.$NumOfMember.'</a>';
			 }else{
			 	$ReplySlipReply = '';
			 	$NumOfSigned = '0';
			 	$ReplySlip_SignTotal = '-';
			 }
		 }
		 
		 if(!$isEnrolAdmin){
		 	$thisIsClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club", $AcademicYearID);
	     	$thisIsClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolGroupID, "Club", $AcademicYearID);
	     	$isThisGroupAdminWithUserMgmtRight = $libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID);
		 }
	     $this_TitleDisplay = Get_Lang_Selection($groups[$i]['TitleChinese'],$groups[$i]['Title']);
	     
	     
	     if ($pic_view==1 && (!$thisIsClubPIC && !$isThisGroupAdminWithUserMgmtRight))
	     	continue;
	     if ($helper_view==1 && !$thisIsClubHelper)
	     	continue;
	     
	   
	     # Quota
	     $quota = $groups[$i]['Quota'];
// 	     $sys_custom['eEnrolment']['ClubHelperCanAddMeeting'] = true;
	     #Schedule
	     if ($helper_view==0 && (($isEnrolAdmin) || ($isEnrolMaster) || $thisIsClubPIC || ($thisIsClubHelper && $sys_custom['eEnrolment']['ClubHelperCanAddMeeting']))) {
	     	//$ScheduleCell = '<td class="tabletext"><a href="group_new2.php?AcademicYearID='.$AcademicYearID.'&EnrolGroupID='.$EnrolGroupID.'&Semester='.$Semester.'"><img height="18" width="18" border="0" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_calendar_on.gif"></a></td>';
	     	$_para = 'AcademicYearID='.$AcademicYearID.'&Semester='.$Semester.'&Round='.$Round.'&recordId='.$EnrolGroupID.'&recordType='.$enrolConfigAry['Club'];
	     	$lurlparahandler->setParaDecrypted($_para);
			$lurlparahandler->doEncrypt();
			$_paraEncrpted = $lurlparahandler->getParaEncrypted();
			$ScheduleCell = '<td class="tabletext"><a href="meeting_date.php?p='.$_paraEncrpted.'"><img height="18" width="18" border="0" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_calendar_on.gif"></a></td>';
	     }
	     else {
	     	$ScheduleCell = '';
	     }
	     		
	     # Allow Online Registration
	     $thisRecordStatus = $groups[$i]['UseOnlineRegistration'];
	     $isAllowOnlineRegistration = ($thisRecordStatus == 1)? true : false;
	     $chkAllow = ($isAllowOnlineRegistration)? 'CHECKED' : '';
	     
	     # Semester Display
	     if($plugin['SIS_eEnrollment']){
	     	$SemesterDisplay = $groups[$i]['RoundNumber'];
	     }else{
	     	$SemesterDisplay = $groups[$i]['YearTermName'];
	     }
	     
	     # Category Display
	     $CategoryDisplay = ($groups[$i]['CategoryName']=="") ? "-" : $groups[$i]['CategoryName'];
	     
	     # number of courses in a curriculum
	     if ($sys_custom['project']['HKPF']) {
	         $numberOfCourse = $libenroll->getNumberOfCourseByCurriculum($EnrolGroupID);
	     }
	     
	     if (
				($isNormalUser && ($thisIsClubPIC||$thisIsClubHelper)||
				(!$isNormalUser)||($isEnrolAdmin)||($isEnrolMaster)||($isThisGroupAdminWithUserMgmtRight))
			)
			{
				
			$displayedRecord++;
	     ?>
		     <tr class="tablerow<?= ((($displayedRecord-1) % 2) + 1)?>">
		     	 <td class="tabletext"> <?=$displayedRecord?> </td>
		     	<td class="tabletext"> <?=($groups[$i]['GroupCode']?$groups[$i]['GroupCode']:'-')?> </td>
		     	 <!--Club Name -->
			     <td class="tabletext">
			     <? if ($isEnrolAdmin
			     		|| $isEnrolMaster 
			     		|| $thisIsClubPIC) { ?>
			     	<? /* if ($chkAllow == "CHECKED") { */ /* add link if using eEnrolment */ ?>
<?php if (!$sys_custom['project']['HKPF']):?>
			     		<a href="group_setting.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$EnrolGroupID?>&Semester=<?=$Semester?>&Round=<?=$Round?>" class="tablelink">
<?php endif;?>			     		
			     	<? /* } */?>
			     <? } ?>
			     <?=$this_TitleDisplay?>
			     <? if ($isEnrolAdmin||$isEnrolMaster) { ?>
			     	<? /* if ($chkAllow == "CHECKED") { */ ?>
<?php if (!$sys_custom['project']['HKPF']):?>			     	
			     		</a>
<?php endif;?>			     		
			     	<? /* } */ ?>
			     <? } ?>
			     </td>

<?php if (!$sys_custom['project']['HKPF']):?>			     
			     <!--Schedule -->
			     <?=$ScheduleCell?>
			     
			     <!--Semester -->
			     <?php if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role'] == ""){ ?>
			     <td class="tabletext"><?=$SemesterDisplay?></td>
			     <?php } ?>
			     
			     <!--Category -->
			     <td class="tabletext"><?=$CategoryDisplay?></td>			     
			     
			     <!--Online Registration -->
	    		<?php if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role'] == ""){ ?> 
			     	<td align="center">
				     <? if ($chkAllow == "CHECKED") { ?>
				     	<?/*
					     <a class="tablelink" href="group_new1a.php?GroupID=<?=$id?>">
					     <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icons_manage.gif" alt="<?= $eEnrollment['club_setting']?>" border="0">
					     </a>
					     */
					    ?>
					     <img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_tick_green.gif" width="18" height="18" border="0" align="absmiddle">
				     <? } else { ?>
				     	<!-- <?= $i_general_disabled ?> -->
				     	<img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_delete_b.gif" width="18" height="18" border="0" align="absmiddle">
				     <? } ?>
				     </td>
			     <?php } ?>
			     
			     <!--Club Quota -->
			     <td align="center" class="tabletext">
			     	<? if ($quota != '' && $quota == 0) { ?>
			     		<?=$i_ClubsEnrollment_NoLimit?>
			     	<? } else { ?>
			     		<?=$quota?>
			     	<? } ?>
			     </td>     
			     
			     <!--Member -->
			     <td align="center" class="tabletext">
			     	<? 	if (( $isEnrolAdmin || $isEnrolMaster || $thisIsClubPIC || $isThisGroupAdminWithUserMgmtRight) && !$thisIsClubHelper)
			     		{ 
			     		//	debug_pr($isThisGroupAdminWithUserMgmtRight);
				    		// set link to the member list if the user is admin/master 	
				    ?>
			     		<a class="tablelink" href="member_index.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$EnrolGroupID?>&Semester=<?=$Semester?>&Round=<?=$Round?>">
			     	<? } ?>
			     	
			     	<?= $groups[$i]['NumOfMember'] ?>		
			     	
			     	<? 	if (( $isEnrolAdmin || $isEnrolMaster || $thisIsClubPIC || $isThisGroupAdminWithUserMgmtRight) && !$thisIsClubHelper) 
			     		{ 
				    ?>
			     		</a>
			     	<? } ?> 
			     </td>
			     
			     <!--Attendance -->
			     <? if ($isEnrolAdmin||$isEnrolMaster||$haveClubMgmt) { ?>
				     <td align="center">
				     	<? if(!$plugin['eEnrollmentLite']) { ?>
				     	<a href="club_attendance_mgt.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$EnrolGroupID?>&Semester=<?=$Semester?>&Round=<?=$Round?>&sel_category=<?=$category[0][1]?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/eOffice/icon_viewstat.gif" border="0"></a>
				     	<? } else {?>
				     	<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/eOffice/icon_viewstat_dim.gif" border="0"> 
				     	<? } ?>
					 </td>
				 <? } ?>
			     
			     <!--Payment -->
			     <?php if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] != ""){ ?>
			     <? }else if ($isEnrolAdmin||$isEnrolMaster||$thisIsClubPIC) { ?>
			     <td align="center">
			     <? if ($libenroll->ALLOW_PAYMENT($EnrolGroupID)) {?>
			     	
						<? if ($plugin['payment'] && $thisClubInfoArr['PaymentAmount']!=0 && $thisClubInfoArr['isAmountTBC']!=1  && $groups[$i]['NumOfMember']>0) { ?>	
						<? if(!$plugin['eEnrollmentLite']) { ?>	
								<a href="javascript:PaymentConfirm(document.form1, <?=$EnrolGroupID?>)" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_payment.gif" border="0" alt="<?= $eEnrollment['payment']?>"></a>
						<? } else {?>
							<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_payment_dim.gif" border="0" alt="<?= $eEnrollment['payment']?>">
						<? } ?>
					 <? } else { 
						 	print "---"; 
						}?>
					 
					<?} 
					else{
						print $eEnrollment['processed']; 
					}?>
				</td>
				<?}?>
<!-- 				Signed_Total -->
				<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
			 	    <td><?=$ReplySlip_SignTotal?></td>
			     <?php }?>

<?php else: // HKPF show number of courses in the curriculum?>
					<td><?php echo $numberOfCourse;?></td>			     
<?php endif;?>			     
			     <!--Checkbox -->
			     <? if ($isEnrolAdmin||$isEnrolMaster) { ?>
			     	<td align="center" valign="top">
				     	<input type="checkbox" name="EnrolGroupID[]" value="<?=$EnrolGroupID?>">
			     	</td>
			     <? } ?>
			     

			     
		     </tr>
	     <?
     	}
	}
	if ($displayedRecord == 0)
	{
		print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"10\">".$i_no_record_exists_msg."</td></tr>";
	}
	?>

</table>

<input type="hidden" name="pic_view" value="<?=$pic_view?>">
<input type="hidden" name="helper_view" value="<?=$helper_view?>">
<input type="hidden" name="OrderBy" value="<?=$OrderBy?>">
<input type="hidden" name="OrderDesc" value="<?=$OrderDesc?>">
	
</form>

<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>