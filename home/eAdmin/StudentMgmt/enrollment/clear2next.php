<?php
//using: 

############# Change Log [Start] #################
#
#   Date: 2020-04-29 Tommy
#        - added only remove target form for $sys_custom['eEnrollment']['setTargetForm']  #R171077
#
############# Change Log [end] #################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_default_lang.php");      # Use default lang
intranet_auth();
intranet_opendb();

$lclub = new libclubsenrol();
$EnrolGroupIDList = trim(urldecode(stripslashes($_REQUEST['EnrolGroupIDList'])));

### Send Mail for notifying
$status_array = array($i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved);

$laccess = new libaccess();
if ($laccess->retrieveAccessCampusmailForType(2))     # if can access campusmail
{
    $lc = new libcampusmail();
    $start = $lclub->AppStart." ".$lclub->AppStartHour.":".$lclub->AppStartMin;
    $end = $lclub->AppEnd." ".$lclub->AppEndHour.":".$lclub->AppEndMin;

    $sql = "SELECT StudentID, Max, Approved, DateModified FROM INTRANET_ENROL_STUDENT";
    $students = $lc->returnArray($sql,4);

    for ($i=0; $i<sizeof($students); $i++)
    {
         list($sid,$max,$approved,$dateMod) = $students[$i];
         $sql = "SELECT a.Choice, b.Title, a.RecordStatus
                 FROM INTRANET_ENROL_GROUPSTUDENT as a
                      LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
                 WHERE a.StudentID = '$sid'
				 ORDER BY Choice";
         $choices = $lc->returnArray($sql,3);

         $info = "";
         $info .= "$i_ClubsEnrollment_Mail1\n";
         for ($j=0; $j<sizeof($choices); $j++)
         {
              list($cho,$name,$status) = $choices[$j];
              $info .= "$name ($i_ClubsEnrollment_MailChoice ".($j+1).") : ".$status_array[$status]."\n";
         }
         $info .= "$i_ClubsEnrollment_Mail2 $start $i_ClubsEnrollment_Mail3 $end\n";
         $info .= "$i_ClubsEnrollment_Mail4\n";
    }
    $recipient = array($sid);
    $lc->sendMail($recipient,$i_ClubsEnrollment_MailSubject,$info);
}


### Delete 
$li = new libdb();
$unhandledStudentArr = array();
if($sys_custom['eEnrollment']['setTargetForm']){
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $libuser = new libuser();
    
    $EnrolGroupID = explode(",", $EnrolGroupIDList);
    $ClubInfoArr = $lclub->Get_Club_Student_Enrollment_Info($EnrolGroupID, '', 0, '', '', 0, 1, 1, 1, '', '', '', 0);
    $classlvlArr = explode(",",$lclub->settingTargetForm);
    
    //get student id in club student record
    for($i = 0; $i < sizeof($EnrolGroupID); $i++){
        $club_student = $ClubInfoArr[$EnrolGroupID[$i]]["StatusStudentArr"];
        if(sizeof($club_student) != 0){
            for($k = 0; $k < sizeof($club_student); $k++){
                for($j = 0; $j < sizeof($club_student[$k]); $j++){
                    $form = $libuser->Get_User_Studying_Form($club_student[$k][$j]["StudentID"], $AcademicYearID);
                    if($club_student[$k][$j]["RecordStatus"] == 0 && in_array($form[0]["YearName"], $classlvlArr)){
                        $unhandledStudentArr[] = $club_student[$k][$j]["StudentID"];
                    }
                }
            }
        }
    }
    $unhandledStudent = " AND StudentID IN ('".implode("','", $unhandledStudentArr)."')";
}else{
    $unhandledStudent = "";
}

$sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT WHERE RecordStatus = 0 And EnrolGroupID In ($EnrolGroupIDList)".$unhandledStudent;
$li->db_db_query($sql);

### Add Log
$LogArr = array();
$EnrolGroupIDArr = explode(',', $EnrolGroupIDList);
$ClubInfoArr = $lclub->Get_Club_Student_Enrollment_Info($EnrolGroupIDArr);
foreach ((array)$ClubInfoArr as $thisEnrolGroupID => $thisClubInfoArr)
{
	$thisGroupTitle = $thisClubInfoArr['GroupTitle'];
	$thisTermName = $thisClubInfoArr['YearTermName'];
	
	$LogArr[] = 'EnrolGroupID:::'.$thisEnrolGroupID.'|||Title:::'.$thisGroupTitle.'|||TermName:::'.$thisTermName;
}
$LogText = implode('<--ClubSeparator-->', $LogArr);

$sql = "Insert Into MODULE_RECORD_DELETE_LOG
			(Module, Section, RecordDetail, DelTableName, DelRecordID, LogDate, LogBy)
		Values
			('eEnrolment', 'Clear_Unhandled_Data', '".$li->Get_Safe_Sql_Query($LogText)."', 'INTRANET_ENROL_GROUPSTUDENT', null, now(), '".$_SESSION['UserID']."')
		";
$Result['AddLog'] = $li->db_db_query($sql);

intranet_closedb();
header("Location: club_status.php?msg=15");
?>