<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lg = new libgroup();
$lclass = new libclass();
$linterface = new interface_html();
$lc = new libclubsenrol_cust();
        
$CurrentPage = "PageMgtPerformanceAdjustment";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['PerformanceAdjustment']);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

//check if the user is a class teacher if the user is not a enrol admin/master
if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$libTeaching = new libteaching($_SESSION['UserID']);
	$result = $libTeaching->returnTeacherClass($_SESSION['UserID'], Get_Current_Academic_Year_ID());
	
	if (count($result)==0)
	{
		$isClassTeacher = false;
	}
	else
	{
		$isClassTeacher = true;
		$targetClassID = $result[0][0];
	}
}
if ($isClassTeacher)
{
	$TitleField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEn');
	//get the class name
	$sql = "SELECT 
					yc.$TitleField, ycu.ClassNumber 
			FROM 
					YEAR_CLASS as yc
					INNER JOIN
					YEAR_CLASS_USER as ycu
					On (yc.YearClassID = ycu.YearClassID)
			WHERE 
					yc.YearClassID = '$targetClassID'
			Order By 
					ycu.ClassNumber
			";
	$result = $libenroll->returnArray($sql,1);
	$classSelection = $result[0][0];
	$targetClass = $result[0][0];
	$showTarget = "class";
}
else
{
	//class-based selection drop-down list
	$classSelection = $lclass->getSelectClassID("name=\"targetClassID\" onChange='changeTarget(\"class\");'", $targetClassID, 1);
	$tempClub = $libenroll->GET_GROUPINFO('', Get_Current_Academic_Year_ID(), $ClubType='') ;
	$clubInfoArray = array();
	$p = 0;
	
	for($i=0; $i<sizeof($tempClub); $i++) {
 	$clubInfoArray[$p][] = $tempClub[$i]['EnrolGroupID']; 	
 	$clubInfoArray[$p][] = $intranet_session_language=="en" ? $tempClub[$i]['TitleWithSemester'] : $tempClub[$i]['TitleChineseWithSemester'];
 	$p++;
	}

	$clubSelection = getSelectByArray($clubInfoArray," name=\"targetClub\" onChange='changeTarget(\"club\");'",$targetClub);
}

##### House Group Info 
$temp_HouseGroup = $lg->returnGroupsByCategory(4, Get_Current_Academic_Year_ID());
$p = 0;
for($i=0; $i<sizeof($temp_HouseGroup); $i++) {
 	$HouseInfoArray[$p][] = $temp_HouseGroup[$i]['GroupID']; 	
 	$HouseInfoArray[$p][] = $temp_HouseGroup[$i]['Title'];
 	$p++;
	}
$houseSelection = getSelectByArray($HouseInfoArray," name=\"targetHouse\" onChange='changeTarget(\"house\");'",$targetHouse);

if($showTarget=="class" && isset($targetClassID) && $targetClassID!="")
{
	$YearClassObj = new year_class($targetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
	$studentNameArr = $YearClassObj->ClassStudentList;
} 
else if($showTarget=="club" && isset($targetClub) && $targetClub!="") 
{
	$studentIDArr = array();
	$studentNameArr = array();
	$studentNameArr = $libenroll->Get_Club_Member_Info(array($targetClub), $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
} 
else if($showTarget=="house" && isset($targetHouse) && $targetHouse!="") 
{
	$IndicatorPrefix = '<span class="tabletextrequire">';
	$IndicatorSuffix = "</span>";
	
	$temp_stuInfo = $lg->returnGroupUser4Display($targetHouse, 2);
	for($i=0; $i<sizeof($temp_stuInfo); $i++) {
	$studentIDArr[] = $temp_stuInfo[$i]['UserID'];
	$studentNameArr[$i]['UserID'] = $temp_stuInfo[$i]['UserID'];
	
	$studentNameArr[$i]['StudentName'] = (empty($temp_stuInfo[$i]['ClassName']) ? $IndicatorPrefix."^".$IndicatorSuffix:"") . $temp_stuInfo[$i][3];
	$studentNameArr[$i]['ClassName'] = $temp_stuInfo[$i]['ClassName'];
	$studentNameArr[$i]['ClassNumber'] = $temp_stuInfo[$i]['ClassNumber'];
	}
}

$table_rows = "";
if(!empty($studentNameArr))
{
	$UserID_str = "";
	# retrieve string of StudentID
	foreach($studentNameArr as $k=>$d)
	{
		$UserID_str .= $d['UserID'] . ",";
	}
	$UserID_str = substr($UserID_str, 0 , strlen($UserID_str)-1);
	
	# retrieve club perform 
	$result = $lc->returnClubPerformanceByUserIDs($UserID_str);
	$performance_ary = array();
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['UserID']] = $d['performance'] ? $d['performance'] : 0;
	}
			
	# retrieve activity perform 
	$result = $lc->returnActivityPerformanceByUserIDs($UserID_str);
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['StudentID']] += $d['performance'] ? $d['performance'] : 0;
	}
	
	# retrieve adjuestment
	$result = $lc->returnAdjustmentByUserIDs($UserID_str);
	$adj_ary = array();
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
 		{
			$adj_ary[$d['UserID']]['Adjustment'] = $d['Adjustment'];
			$adj_ary[$d['UserID']]['DateModified'] = $d['DateModified'];
		}
	}
	
	foreach($studentNameArr as $k=>$d)
	{
		$this_rows = "";

		$this_StudentID = $d['UserID'];
		$this_ClassName = $d['ClassName'] ? $d['ClassName'] : $Lang['General']['EmptySymbol'];
		$this_ClassNumber = $d['ClassNumber'] ? $d['ClassNumber'] : $Lang['General']['EmptySymbol'];
		$this_StudentName = $d['StudentName'] ? $d['StudentName'] : $Lang['General']['EmptySymbol'];
		$this_Performance = $performance_ary[$this_StudentID] ? $performance_ary[$this_StudentID] : 0;
		$this_adjustment = $adj_ary[$this_StudentID]['Adjustment'] ? $adj_ary[$this_StudentID]['Adjustment'] : 0;
		$this_last_modified = $adj_ary[$this_StudentID]['DateModified'] ? $adj_ary[$this_StudentID]['DateModified'] : $Lang['General']['EmptySymbol'];
		
		$this_rows .= "<tr>";
		$this_rows .= "<td>". $this_ClassName ."</td>";
		$this_rows .= "<td>". $this_ClassNumber ."</td>";
		$this_rows .= "<td>". $this_StudentName ."</td>";
		$this_rows .= "<td><span id='perf_". $this_StudentID."'>". $this_Performance ."</span></td>";
		# adjustment box
		$this_rows .= "<td><input type='text' class='textboxnum2' name='adjust_". $this_StudentID."' value='$this_adjustment' onChange='changeAdjustment($this_StudentID)'>
						<br><span id='div_adj_err_msg_". $this_StudentID."'></span></td>";
		# total performance
		$this_total = $this_Performance + $this_adjustment;
		$this_rows .= "<td><span id='total_". $this_StudentID ."'>". $this_total ."</span></td>";
		$this_rows .= "<td>". $this_last_modified ."</td>";
		
		$this_rows .= "</tr>";
		
		$table_rows .= $this_rows;
	}	
}

?>

<script language="javascript">
function showSpan(span) {
	document.getElementById(span).style.display="inline";
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
}

function changeTarget(ch_target)
{
	if(ch_target=="class")
	{
		document.form1.targetHouse[0].selected = true;	
		document.form1.targetClub[0].selected = true;	
	}
	else if(ch_target=="club")
	{
		document.form1.targetHouse[0].selected = true;	
		document.form1.targetClassID[0].selected = true;	
	}
	else if(ch_target=="house")
	{
		document.form1.targetClub[0].selected = true;	
		document.form1.targetClassID[0].selected = true;	
	}
	document.form1.submit();
}

function changeAdjustment(id)
{
	eval("var obj=document.form1.adjust_"+ id +";");
	
	if(isNaN(obj.value))
	{
		eval("document.getElementById('div_adj_err_msg_"+ id +"').innerHTML = '<font color=red><?=$Lang['eEnrolment']['InvalidData']?></font>';");
	}
	else
	{
		var p = eval("document.getElementById('perf_"+ id +"').innerHTML");
		var total = parseInt(p) + parseInt(obj.value);
		
		eval("document.getElementById('total_"+ id +"').innerHTML = '<font color=red>"+ total +"</font>';");
		eval("document.getElementById('div_adj_err_msg_"+ id +"').innerHTML = '';");
	}
	
}

function click_reset()
{
	document.form1.reset();
	var temp=0;
	
	<? if(!empty($studentNameArr)) {
		foreach($studentNameArr as $k=>$d)
		{
	?>
			document.getElementById('div_adj_err_msg_<?=$d['UserID']?>').innerHTML = "";
			temp = parseInt(document.getElementById('perf_<?=$d['UserID']?>').innerHTML) + parseInt(document.form1.adjust_<?=$d['UserID']?>.value);
			document.getElementById('total_<?=$d['UserID']?>').innerHTML = temp ;
	<? 
		}
	} ?>
	
}

function check_form()
{
	var pass=1;
	
	<? if(!empty($studentNameArr)) {
		foreach($studentNameArr as $k=>$d)
		{
	?>
		if(isNaN(document.form1.adjust_<?=$d['UserID']?>.value))	pass=0;
	<? 
		}
	} ?>
	
	if(pass)
	{
		document.form1.action="update.php";
		document.form1.submit();
	}
}	

function click_print()
{
	newWindow('print.php?showTarget=<?=$showTarget?>&targetClassID=<?=$targetClassID?>&targetClub=<?=$targetClub?>&targetHouse=<?=$targetHouse?>', 8);
}

function click_export()
{
	window.location='export.php?showTarget=<?=$showTarget?>&targetClassID=<?=$targetClassID?>&targetClub=<?=$targetClub?>&targetHouse=<?=$targetHouse?>';
}
</script>

<? if(!empty($showTarget) && !(empty($targetClassID) && empty($targetClub) && empty($targetHouse))) {?>
<div class="content_top_tool">
<div class="Conntent_tool">
	<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
	<a href="javascript:click_print();" class="print"> <?=$Lang['Btn']['Print']?></a>
</div>
<br style="clear:both" />
</div>
<? } ?>
    
<form name="form1" action="index.php" method="get">
<div class="table_content">

<div class="table_board">
<table class="form_table_v30">
<tr>
	<td nowrap="nowrap" class="field_title"><?=$i_Discipline_Target?></td>
	<td>
		<? if(($isClassTeacher && !($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) || ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) {?>
			<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target01" value="class" <? if($showTarget=="" || $showTarget=="class") echo "checked"; ?> onClick="showSpan('spanClass'); hideSpan('spanClub');hideSpan('spanHouse');"><? } ?><? if((!$studentView) || $showTarget=="class") { ?><label for="target01"><?=$i_general_class?></label><? } ?>
		<? } ?>
		<? if($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) {?>
			<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target02" value="club" <? if($showTarget=="club") echo "checked";?> onClick="showSpan('spanClub'); hideSpan('spanClass');hideSpan('spanHouse');"><? } ?><? if((!$studentView) || $showTarget=="club") { ?><label for="target02"><?=$eEnrollmentMenu['club']?></label><? } ?>
			<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target03" value="house" <? if($showTarget=="house") echo "checked";?> onClick="showSpan('spanHouse'); hideSpan('spanClass');hideSpan('spanClub');"><? } ?><? if((!$studentView) || $showTarget=="house") { ?><label for="target03"><?=$i_House?></label><? } ?>
		<? } ?>
		<br>
		<div id="spanClass" style="display:<? if($showTarget=="" || $showTarget=="class") {echo "inline";} else {echo "none";} ?>">
			<?= $classSelection ?>
		</div>
		<div id="spanClub" style="display:<? if($showTarget=="club") {echo "inline";} else {echo "none";} ?>">
			<?=$clubSelection?>
		</div>
		<div id="spanHouse" style="display:<? if($showTarget=="house") {echo "inline";} else {echo "none";} ?>">
			<?=$houseSelection?>
		</div>
	</td>
</tr>
</table>
</div>
 
<? if(!empty($showTarget) && !(empty($targetClassID) && empty($targetClub) && empty($targetHouse))) {?>
<!--- Content table //-->
<table class="common_table_list_v30 edit_table_list_v30">
<thead>
<tr>
	<th><?=$i_UserClassName?></th>
	<th><?=$i_UserClassNumber?></th>
	<th><?=$i_UserStudentName?></th>
	<th><?=$Lang['eEnrolment']['ClubActTotalPerformance']?></th>
	<th><?=$Lang['eEnrolment']['Adjustment']?></th>	
	<th><?=$Lang['eEnrolment']['TotalPerformance']?></th>	
	<th><?=$Lang['General']['LastModified']?></th>	
</tr>
</thead>
  
<?=$table_rows?>


</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "button", "check_form();")?> 
<?= $linterface->GET_ACTION_BTN($button_reset, "button", "click_reset();")?>
<p class="spacer"></p>
</div>
<? } ?>

<input type="hidden" name="UserID_str" value="<?=$UserID_str?>">
</form>
</div>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>