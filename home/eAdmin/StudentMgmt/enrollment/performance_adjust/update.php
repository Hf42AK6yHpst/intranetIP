<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lc = new libclubsenrol_cust();

if($UserID_str!="")
{
	$performance_ary = array();

	# retrieve club perform 
 	$result = $lc->returnClubPerformanceByUserIDs($UserID_str);
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['UserID']] = $d['performance'] ? $d['performance'] : 0;
	}
			
 	# retrieve activity perform 
	$result = $lc->returnActivityPerformanceByUserIDs($UserID_str);
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['StudentID']] += $d['performance'] ? $d['performance'] : 0;
	}
	
	$UserID_Ary = explode(",",$UserID_str);
	
	foreach($UserID_Ary as $k=>$d)
	{
		$this_UserID = $d;
		$this_performance = $performance_ary[$this_UserID] ? $performance_ary[$this_UserID] : 0;
		$this_Adjustment = ${"adjust_".$this_UserID};
		$this_total = $this_performance + $this_Adjustment;
		
		$dataAry = array();
		$dataAry['UserID'] = $this_UserID;
		$dataAry['this_Adjustment'] = $this_Adjustment;
		$dataAry['this_total'] = $this_total;
		
		$lc->InsertUpdateStudentTotalPerformance($dataAry);
	}
	
}

header("Location: index.php?showTarget=$showTarget&targetClassID=$targetClassID&targetClub=$targetClub&targetHouse=$targetHouse&xmsg=UpdateSuccess");

intranet_closedb();
?>