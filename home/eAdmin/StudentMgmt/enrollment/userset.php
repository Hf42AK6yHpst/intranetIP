<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	$CurrentPage = "PageUserSet";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
	if ((!$lc->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$lc->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$lc->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	$enrolInfo = $lc->getEnrollmentInfo();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['user_set'], "", 1);

        $linterface->LAYOUT_START();

$Sql = "
		CREATE TABLE IF NOT EXISTS INTRANET_ENROL_ACCESS_LEVEL (
		 LevelNum int(11) NOT NULL,
		 LevelName varchar(255),
		 UNIQUE LevelNum (LevelNum)
		 )
		";
$libenroll->db_db_query($Sql);

// check if record exists and run it
$Sql = "SELECT COUNT(*) FROM INTRANET_ENROL_ACCESS_LEVEL";
$TempCount = $libenroll->returnVector($Sql);
if ($TempCount[0] == 0) {
	/*
	User Level
	0: $eEnrollment['user_role']['normal_user'] = "Normal User 一般使用者";
	1: $eEnrollment['user_role']['enrollment_master'] = "Enrollment Master 學會報名主任";
	2: $eEnrollment['user_role']['enrollment_admin'] = "Enrollment Admin 學會報名管理員";
	*/
	$Sql = "
			INSERT INTO
						INTRANET_ENROL_ACCESS_LEVEL
						(LevelNum, LevelName)
			VALUES
						(0, '".$eEnrollment['user_role']['normal_user']."'),
						(1, '".$eEnrollment['user_role']['enrollment_master']."'),
						(2, '".$eEnrollment['user_role']['enrollment_admin']."')
			";
	$libenroll->db_db_query($Sql);
}


$basic_admin_level = $eEnrollment['user_role']['normal_user'];
$max_admin_level_name = $eEnrollment['user_role']['enrollment_admin'];
$max_admin_level = 2;

$namefield = getNamefieldWithLoginByLang("a.");
$sql = "SELECT $namefield,
               IF(b.UserID IS NULL,'$basic_admin_level',IF(b.UserLevel>$max_admin_level,'$max_admin_level_name',c.LevelName)),
               CONCAT('<input type=checkbox name=StaffID[] value=', a.UserID ,'>')
               FROM INTRANET_USER as a
                    LEFT OUTER JOIN INTRANET_ENROL_USER_ACL as b ON a.UserID = b.UserID
                    LEFT OUTER JOIN INTRANET_ENROL_ACCESS_LEVEL as c ON b.UserLevel = c.LevelNum
               WHERE a.RecordType = 1 AND a.RecordStatus = 1
               ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.UserLogin", "c.LevelName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width=50% class='tabletoplink'>".$li->column($pos++, "$i_UserName ($i_UserLogin)")."</td>\n";
$li->column_list .= "<td width=50% class='tabletoplink'>".$li->column($pos++, $i_Discipline_System_Field_AdminLevel)."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("StaffID[]")."</td>\n";



#$lteaching = new libteaching();
#$toolbar = "<a class=iconLink href=\"import.php\">".importIcon()."$button_import</a>";
$admin_levels = $libenroll->getAdminLevels();
#$array = build_assoc_array($admin_levels);
$select_admin_level = getSelectByArray($admin_levels,"name=TargetLevel",0,0,1);
$functionbar .= $select_admin_level;
//$functionbar .= "<a href=\"javascript:checkAlert(document.form1,'StaffID[]','userset_update.php','$i_Discipline_System_alert_ChangeAdminLevel')\"><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar .= $linterface->GET_SMALL_BTN($button_update, "button", "javascript:checkAlert(document.form1,'StaffID[]','userset_update.php','$i_Discipline_System_alert_ChangeAdminLevel')");

if ($msg == 2)
{
     //$response_msg = $i_Discipline_System_alert_Approved;
     $response_msg = $linterface->GET_SYS_MSG("update");
}

?>
<form name="form1" method="get">

<br/>

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td align="left"></td>
	<td align="right"><?=$response_msg?></td>
</tr>
<tr>
	<td align="left"><?= $toolbar; ?><?= $searchbar; ?></td>
	<td align="right">
	
		<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td width="21" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_t_01.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="21" height="22" border="0"></td>
	  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_t_02.gif"><table border="0" cellspacing="0" cellpadding="2">
	      <tr>
	        <td nowrap="nowrap"><?= $functionbar?></td>
	      </tr>
	    </table></td>
	  <td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_t_03.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="6" height="22"></td>
	</tr>
	</table>
	</td>
</tr>
</table>

<?=$li->display("96%")?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">

</form>
<br/>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>