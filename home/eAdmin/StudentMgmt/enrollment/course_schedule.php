<?php
# using:  

/* 
 * Change Log
 *
 * 2019-02-19 Cameron
 *  - set max_input_time
 *  
 * 2019-02-14 Cameron
 *  - set execution time and memory to unlimit
 *  
 * 2019-02-04 Cameron
 *  - enlarge time limit and memory limit  ( default is 24 min and 800M)
 *  
 * 2018-12-18 Cameron
 *  - allow Qualified Instructor in Settings > Category to manipulate the course lesson with those categories
 * 
 * 2018-12-12 Cameron
 *  - add function changeLessonClassGroup()
 *  
 * 2018-11-07 Cameron
 *  - disable class time conflict checking as class can split into multiple lesson in different location
 *  
 * 2018-11-01 Cameron
 *  - fix js error for time conflict
 *  
 * 2018-10-30 Cameron
 *  - show conflict row and class name (the last scanned in loop, not all row and classes) in checkForm()
 *  
 * 2018-10-26 Cameron
 *  - show time conflict classes in checking before submit
 *  
 * 2018-10-16 Cameron
 *  - pass $getClassAndInstructor=false to getIntake()
 *  
 * 2018-10-11 Cameron
 *  - add function changeLessonClass()
 *  
 * 2018-10-10 Cameron
 *  - add parameter $intakeID to addLesson()
 *
 * 2018-09-19 Cameron
 *  - fix: add intakeEndDate checking for lesson 
 *  
 * 2018-08-31 Cameron
 *  - fix: applyTimeToDate for ad-hoc added lesson
 *  
 * 2018-08-17 Cameron 
 *  - add time overlap checking in checkForm()
 *  
 * 2018-08-14 Cameron
 *  - pass category in getLocationSelection()
 * 
 * 2018-08-13 Cameron
 *  - allow instructor (non-admin staff) to manage course schedule on himself/herself
 *   
 * 2018-07-26 Cameron
 *  - create this file
 */

@set_time_limit(0);                // max execution time: unlimit
@ini_set('max_input_time', 21600);
@ini_set('memory_limit',-1);       // max memory: unlimit

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
$recordId = IntegerSafe($_GET['recordId']);     // EnrolEventID (CourseID)
$recordType = $_GET['recordType'];

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new libclubsenrol_ui();
$libenroll = new libclubsenrol($AcademicYearID);

####################################################
########## Access Right Checking [Start] ###########
####################################################
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);

$canAccess = true;
if ($recordType == $enrolConfigAry['Activity']) {
    $recordInfoAry= $libenroll->GET_EVENTINFO($recordId);
	if (!$isEnrolAdmin) {
	    if ($sys_custom['project']['HKPF'] && $_SESSION['UserType']==USERTYPE_STAFF) {
	        $canAccess = ($libenroll->checkTrainerLesson($_SESSION['UserID'], $recordId) || $libenroll->checkIsAllowedInstructor($recordId, $_SESSION['UserID']));
	    }
	    else {
		    $canAccess = false;
	    }
	}
	
	if($sys_custom['project']['HKPF'] && $plugin['eBooking']){
		$has_location_booking = true;
		$eventDatesAry = $libenroll->GET_ENROL_EVENT_DATE($recordId);
	}
}

if (!$canAccess) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
####################################################
########### Access Right Checking [End] ############
####################################################


$courseCode = $recordInfoAry['ActivityCode'];
$courseTitle = $recordInfoAry['EventTitle'];
$courseCategoryID = $recordInfoAry['EventCategory'];

// $intakeClassID = $recordInfoAry['IntakeClassID'];
// if ($intakeClassID) {
//     $intakeClassAry = $libenroll->getIntakeClass('',$intakeClassID);
//     if (count($intakeClassAry)) {
//         $classLocationID = $intakeClassAry[$intakeClassID]['LocationID'];
//         $intakeID = $intakeClassAry[$intakeClassID]['IntakeID'];
//         $capacity = $intakeClassAry[$intakeClassID]['NumberOfStudent'];
//     }
//     else {
//         $classLocationID = '';
//         $intakeID = '';
//         $capacity = 0;
//     }
// }
// else {  // combined course
//     $intakeID = $libenroll->getIntakeIDByEnrolEventID($recordId);
//     $capacityAry = $libenroll->getNumberOfIntakeStudents($intakeID);
//     $capacity = $capacityAry[$intakeID];
//     $classLocationID = '';
// }

$intakeID = $libenroll->getIntakeIDByEnrolEventID($recordId);
$intakeClassAssoc = $libenroll->getIntakeClass($intakeID);

if ($intakeID) {
    $intakeAry = $libenroll->getIntake($intakeID, $getClassAndInstructor=false);
    $intakeStartDate = $intakeAry[$intakeID]['StartDate'];
    $intakeEndDate = $intakeAry[$intakeID]['EndDate'];
}
else {
    $intakeStartDate = '';
    $intakeEndDate = '';
}

$courseScheduleTable = $linterface->getCourseScheduleTable($recordId);

####################################################
############## Layout Display [Start] ##############
####################################################
$CurrentPage = "PageMgtActivity";
$backPage = "event.php";

$CurrentPageArr['eEnrolment'] = 1;

$cancelBtnPara = "AcademicYearID=$AcademicYearID";
$picView_HiddenField = '';


### top menu
$tab_type = strtolower($recordType);
$current_tab = 1;
include_once("management_tabs.php");

### left-hand menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### Navigation
$PAGE_NAVIGATION[] = array($eEnrollment['schedule_settings']['scheduling_times'], "");
$h_pageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$manage_record_bar = '<a id="btnRemove" href="" class="tool_delete">'.$button_delete.'</a>';

####################################################
############### Layout Display [End] ###############
####################################################
?>

<script language="javascript">
var isLoading = true;
var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

//return true the two time slot is overlapped in the same date
// ts1 and te1 are ref point start and point end, ts2 and te2 are time slot to compare 
function isTimeOverlap(hr_s1, min_s1, hr_e1, min_e1, hr_s2, min_s2, hr_e2, min_e2) {
	var ts1 = str_pad(hr_s1, 2) + str_pad(min_s1, 2);
	var te1 = str_pad(hr_e1, 2) + str_pad(min_e1, 2);
	var ts2 = str_pad(hr_s2, 2) + str_pad(min_s2, 2);	// ref start
	var te2 = str_pad(hr_e2, 2) + str_pad(min_e2, 2);	// ref end

	if (ts1 == te1 && ts1 >= ts2 && ts1<=te2) {			// ref point is within compared timeslot
		return true;
	}
	else if (ts2 == te2 && ts2 >= ts1 && ts2 <= te1) {	// compared point is within ref timeslot
		return true;
	}
	else if (ts1 < te2 && te2 <= te1) {		// compared end point is within ref timslot (overlapped on right part)
		return true;
	}
	else if (ts2 >= ts1 && ts2 < te1) {		// compared start point is within ref timslot (overlapped on left part)
		return true;
	}
	else if (ts2 < ts1 && ts1 < te1 && te1 < te2) {	// compared time slot fully overlapped ref time slot
		return true;
	}
	else {
		return false;
	}
}

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
 	$(':input[name^="LessonTitle"]').each(function(){
	    var thisID = $(this).attr('id');
	    thisID = thisID.replace(/\[/g,"\\\[");
	    thisID = thisID.replace(/\]/g,"\\\]");
 		if ($.trim($(this).val()) == '') {
 	 		error++;
 	 		if (focusField == '') focusField = thisID;
			$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});

 	$(':input[name^="NewLessonTitle"]').each(function(){
	    var thisID = $(this).attr('id');
 		if ($.trim($(this).val()) == '') {
 	 		error++;
 	 		if (focusField == '') focusField = thisID;
			$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});
 	
 	
	$(':input[name^="StartHour"]').each(function(){
		var thisID = $(this).attr('id');
 		var ary = $(this).attr('id').split('\]\[');
 		var eventDateID = ary[0].replace('StartHour\[','');
 		var row = ary[1].replace('\]','');
 		var startHour = $(this).val();
 		var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
 		var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
 		var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();

 		if (!validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
 	 		error++;
 	 		if (focusField == '') {
 	 		    thisID = thisID.replace(/\[/g,"\\\[");
 		  	    thisID = thisID.replace(/\]/g,"\\\]");
 	 	 		focusField = thisID;
 	 		}
			$('#ErrTimeLogic_' + eventDateID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
 		}

 		// check if there's time conflict in the schedule
 		var refDateID = $('#LessonDate_' +eventDateID+'_'+row).attr('id');
 		var refDate = $('#'+ refDateID).val();
 		var refLocationID = $('#Location\\['+eventDateID+'\\]\\['+row+'\\]').val();

 		var refCheckedClassID = [];
 		var thisClassID;
 		var classIDAry; 		
 		if (eventDateID == '0') {		// new record
 			$('input:checkbox[id^="NewLessonClass_' + row + '"]:checked').each(function(){
 				thisClassID = $(this).attr('id');
 				classIDAry = thisClassID.split('_');
 				refCheckedClassID.push(classIDAry[2]);
 			});
 		}
 		else {
 			$('input:checkbox[id^="LessonClass_' + eventDateID + '"]:checked').each(function(){
 				thisClassID = $(this).attr('id');
 				classIDAry = thisClassID.split('_');
 				refCheckedClassID.push(classIDAry[2]);
 			});
 		}

 		$(':input[name^="LessonDate_"]:not([id="'+refDateID+'"])').each(function(){
 	 		var cmpDate = $(this).val();
			var cmpDateID = $(this).attr('id');
	 		var cmpAry = $(this).attr('id').split('_');	// 2D array
	 		var cmpCourseID = cmpAry[1];				// EventDateID
	 		var cmpRow = cmpAry[2];
	 		var cmpClassID;
	 		var cmpClassIDAry;
	 		var isSameClass = false;
	 		var byPass = false;
	 		var conflictClassName = '';
	 		var displayError = '';

	 		
//  	 		if (cmpCourseID == '0') {		// new record
// 	 			$('input:checkbox[id^="NewLessonClass_' + cmpRow + '"]:checked').each(function(){
// 	 				cmpClassID = $(this).attr('id');
// 	 				cmpClassIDAry = cmpClassID.split('_');
// 	 				if (refCheckedClassID.indexOf(cmpClassIDAry[2]) != -1) {
// 						isSameClass = true;
// 						conflictClassName = $("label[for='" + cmpClassID + "']").html();
// 	 				}
// 	 			});
// 	 		}
// 	 		else {
// 	 			$('input:checkbox[id^="LessonClass_' + cmpCourseID + '"]:checked').each(function(){
// 	 				cmpClassID = $(this).attr('id');
// 	 				cmpClassIDAry = cmpClassID.split('_');
// 	 				if (refCheckedClassID.indexOf(cmpClassIDAry[2]) != -1) {
// 						isSameClass = true;
// 						conflictClassName = $("label[for='" + cmpClassID + "']").html();
// 	 				}
// 	 			});
// 	 		}
	 		
	 		
 	 		var cmpLocationID = $('#Location\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
// 			if (cmpDate == refDate && ((refLocationID != '' && refLocationID == cmpLocationID) || (isSameClass == true))) {
			if (cmpDate == refDate && (refLocationID != '' && refLocationID == cmpLocationID)) {

		 		var cmpStartHour = $('#StartHour\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
		 		var cmpStartMinute = $('#StartMinute\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
		 		var cmpEndHour = $('#EndHour\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
		 		var cmpEndMinute = $('#EndMinute\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();

		 		// case when generated from template, use default intake start date and time
		 		if (cmpStartHour == '0' && cmpEndHour == '0' && cmpStartMinute == '0' && cmpEndMinute == '0') {		
					byPass = true;
		 		}
		 		
		 		if (byPass == false && isTimeOverlap(cmpStartHour, cmpStartMinute, cmpEndHour, cmpEndMinute, startHour, startMinute, endHour, endMinute)) {
			 		var eventDateRow = $('#ErrTimeLogic_' + eventDateID + '_' + row).parent().parent().find('td.rowNum').text();
			 		var cmpDateRow = $('#ErrTimeLogic_' + cmpCourseID + '_' + cmpRow).parent().parent().find('td.rowNum').text();
			 		displayError = '<?php echo $Lang['eEnrolment']['Warning']['TimeOverlap'];?>' + ' #' + cmpDateRow;
// 			 		if (conflictClassName != '') {
// 						displayError += ': ' + conflictClassName;
// 			 		} 
		 			$('#ErrTimeLogic_' + eventDateID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html(displayError);
		 			
			 		displayError = '<?php echo $Lang['eEnrolment']['Warning']['TimeOverlap'];?>' + ' #' + eventDateRow;
// 			 		if (conflictClassName != '') {
// 						displayError += ': ' + conflictClassName;
// 			 		}
		 			$('#ErrTimeLogic_' + cmpCourseID + '_' + cmpRow).addClass('error_msg_show').removeClass('error_msg_hide').html(displayError);
		 			error++;
		 	 		if (focusField == '') {
		 	 	 		focusField = $('#ErrTimeLogic_' + cmpCourseID + '_' + cmpRow).attr('id');
		 	 		}
		 		}
			}
 		});
 		
	});

 	$(':input[name^="LessonDate"]').each(function(){
	    var thisID = $(this).attr('id');
	    var thisDate = $.trim($(this).val());
	    var intakeStartDate = '<?php echo $intakeStartDate;?>';
	    var intakeEndDate = '<?php echo $intakeEndDate;?>';
 		if (compareDate(thisDate, intakeStartDate) == '-1') {
 	 		error++;
 	 		if (focusField == '') focusField = thisID;
			$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['beforeStartDate'];?>');
 		}
 		else if (compareDate(intakeEndDate, thisDate) == '-1') {
 	 		error++;
 	 		if (focusField == '') focusField = thisID;
			$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['afterEndDate'];?>');
 		} 	
 	});
	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function applyTimeToDate(selectedDateOnly) {
	var canApply = true;
	
	if (!compareTimeByObjId('', 'globalHourStart', 'globalMinStart', '', '', 'globalHourEnd', 'globalMinEnd', '')) {
		$('div#global_invalidTimeRangeWarningDiv').show();
		canApply = false;
	}
	else {
		$('div#global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canApply) {
		var selectedPara = '';
		if (selectedDateOnly == 1) {
			selectedPara = ':checked';
		}
		
		var targetStartHour = $('select#globalHourStart').val();
		var targetStartMin = $('select#globalMinStart').val();
		var targetEndHour = $('select#globalHourEnd').val();
		var targetEndMin = $('select#globalMinEnd').val();
		var _row, _idxAry;
		
		$('input.dateChk' + selectedPara).each( function() {
			var _eventDateID = $(this).attr('id').replace('dateChk_', '');
			if (_eventDateID.indexOf('_') == -1) {
				_row = 0;
				_eventDateID = parseInt(_eventDateID);
			}
			else {
				_idxAry = _eventDateID.split('_');
				_eventDateID = parseInt(_idxAry[0]);
				_row = parseInt(_idxAry[1]);
			}
			$('select#StartHour\\[' + _eventDateID+'\\]\\['+ _row +'\\]').val(targetStartHour);
			$('select#StartMinute\\[' + _eventDateID+'\\]\\['+ _row +'\\]').val(targetStartMin);
			$('select#EndHour\\[' + _eventDateID+'\\]\\['+ _row +'\\]').val(targetEndHour);
			$('select#EndMinute\\[' + _eventDateID+'\\]\\['+ _row +'\\]').val(targetEndMin);

			$('select#StartHour\\[' + _eventDateID+'\\]\\['+ _row +'\\]').change();
		});
	}
}

function changeLessonDate(obj)
{
	var $this = $(obj);
	var lessonDateID = $this.attr('id');
	var str = lessonDateID.split('_');
	var prefix = str[0];
	var eventDateID = str[1];
	var row = str[2];
	var lessonDate = $this.val();
	var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var originalLocationID = $('#OriginalLocationID_'+eventDateID).val();
	var intakeStartDate = '<?php echo $intakeStartDate;?>';
	var intakeEndDate = '<?php echo $intakeEndDate;?>';
	
	if (compareDate(lessonDate, intakeStartDate) == '-1') {
		$('#Err' + lessonDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['beforeStartDate'];?>');
	}
	else if (compareDate(intakeEndDate, lessonDate) == '-1') {
		$('#Err' + lessonDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['afterEndDate'];?>');
	}
	else { 	
		$('#Err' + lessonDateID).addClass('error_msg_hide').removeClass('error_msg_show');
	}

    isLoading = true;
    $('#LocationCol_'+eventDateID+'_'+row).html(loadingImg);
	getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID);
}

function validateTimeLogic(startHour, startMinute, endHour, endMinute)
{
	var hourStart = (startHour=='')? '00' : str_pad(startHour, 2);
	var minStart = (startMinute=='')? '00' : str_pad(startMinute, 2);
	var timeTextStart = hourStart + minStart;
	
	var hourEnd = (endHour=='')? '00' : str_pad(endHour, 2);
	var minEnd = (endMinute=='')? '00' : str_pad(endMinute, 2);
	var timeTextEnd = hourEnd + minEnd;
	if (timeTextStart > timeTextEnd) {
		return false;
	}
	else {
		return true;
	}
}

function getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID)
{
	var capacity = 0;
	var classLocationID = '';
	var numberOfSelectedClass = 0;
	
	if (eventDateID == '0') {		// new record
		$('input:checkbox[id^="NewLessonClass_' + row + '"]:checked').each(function(){
			capacity += parseInt($(this).attr('data-capacity'));
			classLocationID = $(this).attr('data-locationID');
			numberOfSelectedClass++;
		});
	}
	else {
		$('input:checkbox[id^="LessonClass_' + eventDateID + '"]:checked').each(function(){
			capacity += parseInt($(this).attr('data-capacity'));
			classLocationID = $(this).attr('data-locationID');
			numberOfSelectedClass++;
		});
	}
	if (numberOfSelectedClass > 1) {
		classLocationID = '';
	}
//console.log('capacity = ' + capacity);
//console.log('locationID = ' + classLocationID);
	$.ajax({
		dataType: "json",
		async: true,			
		type: "POST",
		url: './curriculum_template/ajax.php',
		data : {
			'action': 'getAvailableLocationListInSchedule',
			'lessonDate': lessonDate,
			'startHour': startHour,
			'startMinute': startMinute,
			'endHour': endHour,
			'endMinute': endMinute,
			'eventDateID': eventDateID,
			'row': row,
			'capacity': capacity,
			'originalLocationID': originalLocationID,
			'classLocationID': classLocationID,
			'categoryID': '<?php echo $courseCategoryID;?>'
		},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#LocationCol_'+eventDateID+'_'+row).html(ajaxReturn.html);
				isLoading = false;
			}
		},
		error: show_ajax_error
	});
}

function addLesson()
{
	var rowCounter = parseInt($('#rowCounter').val());		// unique row id
	var rowNum = parseInt($('#dateTimeSettingTable td.rowNum').length) + 1;		// row number to show, may not correct if response data hasn't returned on this click, need to re-order by reorderRowNum()   
	$('#rowCounter').val(rowCounter+1);
	$.ajax({
		dataType: "json",
		type: "POST",
		url: './curriculum_template/ajax.php',
		data : {
			'action': 'addLesson',
			'rowCounter': rowCounter,
			'rowNum': rowNum,
			'categoryID': '<?php echo $courseCategoryID;?>',
			'capacity': '0',
			'intakeStartDate': '<?php echo $intakeStartDate;?>',
			'intakeID': '<?php echo $intakeID;?>'
		},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#dateTimeSettingTable').append(ajaxReturn.html);
				reorderRowNum();				
			}
		},
		error: show_ajax_error
	});
}

function changeDateTimeSelect(obj)
{
	$this = $(obj);	
	var ary = $this.attr('id').split('\]\[');	// 2D array
	var idx = ary[0].indexOf('[');
	if (idx != -1 ) {
		var eventDateID = ary[0].substr(idx+1);	
 		var row = ary[1].replace('\]','');
 		var lessonDate = $('#LessonDate_'+eventDateID+'_'+row).val();
		var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
 		var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
 		var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
 		var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
 		var originalLocationID = $('#OriginalLocationID_'+eventDateID).val();
 		
		var errMsgObj = $('#ErrTimeLogic_' + eventDateID + '_' + row);
 		if (validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
 	        isLoading = true;
 			$('#LocationCol_'+eventDateID+'_'+row).html(loadingImg);
 	 	 		
 			//errMsgObj.addClass('error_msg_hide').removeClass('error_msg_show');
 			$('[id^="ErrTimeLogic_"]').addClass('error_msg_hide').removeClass('error_msg_show');
			getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID);
 		}
 		else {
 			errMsgObj.addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
 		}
	}
}

function changeLessonClass(obj)
{
	$this = $(obj);
	var id = $this.attr('id');
	var ary = id.split('_');	// 2D array
	var name = ary[0];
	var eventDateID;
	var row;
	var originalLocationID;
	if (name == 'NewLessonClass') {
		eventDateID = 0;
		row = ary[1];
		originalLocationID = 0;
	}		
	else {
		eventDateID = ary[1];
		row = 0;
		originalLocationID = $('#OriginalLocationID_'+eventDateID).val();
	}
	
	var lessonDate = $('#LessonDate_'+eventDateID+'_'+row).val();
	var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	
	var errMsgObj = $('#ErrTimeLogic_' + eventDateID + '_' + row);
	if (validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
        isLoading = true;
		$('#LocationCol_'+eventDateID+'_'+row).html(loadingImg);
 	 		
		$('[id^="ErrTimeLogic_"]').addClass('error_msg_hide').removeClass('error_msg_show');
		getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID);
	}
	else {
		errMsgObj.addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
	}
}

function changeLessonClassGroup(obj)
{
	$this = $(obj);
	var id = $this.attr('id');
	var ary = id.split('_');	// 2D array
	var name = ary[0];
	var eventDateID = ary[1];

	name = name.replace('Group','');
	
	$(':input[id^="'+name+'_'+eventDateID+'_"]').each(function(){
		$(this).attr('checked', $this.attr('checked'));
		$(this).change();
	});
}

function reorderRowNum()
{
	var i = 1;
	$('.rowNum').each(function(){
		$(this).html(i++);
	});
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

$(document).ready(function() {

	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

	    $(':select[name*="Instructor"]').each(function(){
		    var thisID = $(this).attr('id');
		    thisID = thisID.replace(/\[/g,"\\\[");
		    thisID = thisID.replace(/\]/g,"\\\]");
			$('#' + thisID + ' option').attr('selected',true);
	    });
	    
		if (checkForm(document.form1)) {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: './curriculum_template/ajax.php?action=checkeBooking4LessonUpdate',
				data : $('#form1').serialize(),
				success: function(ajaxReturn){
					if (ajaxReturn != null && ajaxReturn.success){
						var isRoomBooked = ajaxReturn.html;
						var timeConflict = ajaxReturn.html2;
// console.log(isRoomBooked);
// console.log(timeConflict);
						var isNewRoomBooked = ajaxReturn.isNewRoomBooked;
						var newTimeConflict = ajaxReturn.newTimeConflict;
						var newError = ajaxReturn.newError;
							
						var error = ajaxReturn.error;
						var courseID, row, ary;
						var isBooked = false;
						var isTimeConflict = false;
						var errorAry, conflictAry, errorDescription, i;
						
						$(':input[name^="Location"]').each(function(){
						    var thisID = $(this).attr('id');
						    thisID = thisID.replace(/\[/g,"\\\[");
						    thisID = thisID.replace(/\]/g,"\\\]");
							ary = $(this).attr('id').split('\]\[');	// 2D array
					 		eventDateID = ary[0].replace('Location\[','');
					 		row = ary[1].replace('\]','');

					 		if (eventDateID == '0') {	// new record
    							if ((isNewRoomBooked != '') && (typeof isNewRoomBooked[row] != 'undefined') && (isNewRoomBooked[row] == true)) {
    								errorAry = newError[row];
    								errorDescription = '';
    								for(i=0; i<errorAry.length;i++) {
    									switch (errorAry[i][0]) {
        									case '101':
        										errorDescription += '<?php echo $Lang['eEnrolment']['booking']['unavailablePeriod'];?>';
        										break;
        									case '301':
        										errorDescription += '<?php echo $Lang['eEnrolment']['booking']['violateBookingRule'];?>';
        										break;
        									case '201':
        									default:	
        										errorDescription += '<?php echo $Lang['eEnrolment']['booking']['roomBooked'];?>';
        										break;
    									}
    									if (i != errorAry.length - 1) {
    										errorDescription += ',';
    									}
    								}
    								
    								isBooked = true;
    								$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide').html(errorDescription);
    								$('input.actionBtn').attr('disabled', '');
    							}
    
    					 		if ((newTimeConflict != '') && (typeof newTimeConflict[row] != 'undefined')) {
    					 			conflictAry = newTimeConflict[row];
    					 			for (i=0; i<conflictAry.length;i++) {
										switch (conflictAry[i][0]) {
											case '102':
												$('#ErrInstructor\\[0\\]\\['+row+'\\]').addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable'];?>');
												break;
// 											case '202':
//												$('#ErrNewLessonClass_'+row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictClass'];?>' + conflictAry[i][1]);
// 												break;
											default:
												$('#ErrNewLessonClass_'+row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictUnknown'];?>');
												break;
										}
    					 			}
    					 			
    					 			isTimeConflict = true;
    								$('input.actionBtn').attr('disabled', '');
    							}
					 		}		// end new record
					 		else {
    							if ((isRoomBooked != '') && (typeof isRoomBooked[eventDateID] != 'undefined') && (isRoomBooked[eventDateID] == true)) {
    								errorAry = error[eventDateID];
    								errorDescription = '';
    								for(i=0; i<errorAry.length;i++) {
    									switch (errorAry[i][0]) {
        									case '101':
        										errorDescription += '<?php echo $Lang['eEnrolment']['booking']['unavailablePeriod'];?>';
        										break;
        									case '301':
        										errorDescription += '<?php echo $Lang['eEnrolment']['booking']['violateBookingRule'];?>';
        										break;
        									case '201':
        									default:	
        										errorDescription += '<?php echo $Lang['eEnrolment']['booking']['roomBooked'];?>';
        										break;
    									}
    									if (i != errorAry.length - 1) {
    										errorDescription += ',';
    									}
    								}
    								
    								isBooked = true;
    								//$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
    								$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide').html(errorDescription);
    								$('input.actionBtn').attr('disabled', '');
    							}
    
    					 		if ((timeConflict != '') && (typeof timeConflict[eventDateID] != 'undefined')) {
    					 			conflictAry = timeConflict[eventDateID];
    					 			for (i=0; i<conflictAry.length;i++) {
										switch (conflictAry[i][0]) {
											case '102':
												$('#ErrInstructor\\['+eventDateID+'\\]\\[0\\]').addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable'];?>');
												break;
// 											case '202':
//												$('#ErrLessonClass_'+eventDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictClass'];?>' + conflictAry[i][1]);
// 												break;
											default:
												$('#ErrNewLessonClass_'+eventDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictUnknown'];?>');
												break;
										}
    					 			}
    					 			
    					 			isTimeConflict = true;    					 			
    								$('input.actionBtn').attr('disabled', '');
    							}
					 		}	// end old record							
						});

// 		        		if (isBooked == true) {
//		        			alert('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['locationOrTimeNotAvailable'];?>');
// 		        			$('input.actionBtn').attr('disabled', '');
// 		        		}
// if (isBooked) {
// 	console.log('booked');
// }
// if (isTimeConflict) {
// 	console.log('conflict');
// }
						if (!isBooked && !isTimeConflict) {
	           				$('#form1').submit();
		        		}
					}
					else {
						$('input.actionBtn').attr('disabled', '');
					}
				},
				error: function(){
					$('input.actionBtn').attr('disabled', '');
					show_ajax_error();
				}
			});
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});


	$('.dateTimeSelect').change(function(){
		changeDateTimeSelect($(this)[0]);	// selector to DOM obj
	});

	$('#btnRemove').click(function(e){
		e.preventDefault();
		if ($('.dateChk:checked').length == 0 ) {
			alert(globalAlertMsg2);
		}
		else {
			if(confirm("<?php echo $Lang['eEnrolment']['course']['warning']['confirmDeleteLesson'];?>")){
				$('.dateChk:checked').each(function(){
					var ary = $(this).attr('id').split('_');
					if (ary.length == 2) {
						var eventDateID = ary[1];
						var eventDateIDToDelete = $('#eventDateIDToDelete').val();
						if (eventDateIDToDelete == ''){
							eventDateIDToDelete = eventDateID;
						}
						else {
							eventDateIDToDelete = eventDateIDToDelete + ',' + eventDateID;
						}
						$('#eventDateIDToDelete').val(eventDateIDToDelete);
						$('#EventDateID_'+eventDateID).remove();
						$('#OriginalLocationID_'+eventDateID).remove();
					}
    				$(this).parent().parent().remove();
    				reorderRowNum();
				});
			}
		}
	});
	
});

</script>
<br />

<form id="form1" name="form1" action="course_schedule_update.php" method="post">

	<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td align="left" class="navigation"><?php echo $h_pageNavigation;?></td>
		</tr>
	</table>
	
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['code'];?></td>
			<td><?php echo $courseCode;?></td>
		</tr>
		<tr>
			<td class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['title'];?></td>
			<td><?php echo $courseTitle;?></td>
		</tr>
<!--		
		<tr>
			<td class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfStudent'];?></td>
			<td><?php echo $capacity;?></td>
		</tr>
-->
	</table>
	<br />
	
	<div class="table_board">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    		<tr class="table-action-bar">
    			<td valign="bottom">
    				<div class="table_filter">&nbsp;</div> 
    			</td>
    			<td valign="bottom">
    				<div class="common_table_tool">
    					<?=$manage_record_bar?>					
    				</div>
    			</td>
    		</tr>
    	</table>
	</div>
	
<!-- 	<br style="clear:both;" /> -->
	<div id="meetingDateTimeDiv"><?php echo $courseScheduleTable;?></div>
	
	<div>
		<span class="table_row_tool"><a id="AddBtn" class="newBtn add" onclick="javascript:addLesson()" title="<?php echo $Lang['eEnrolment']['course']['addLesson'];?>"></a></span>
	</div>

	<br style="clear:both;" />
	
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<?php echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
		<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='event.php?AcademicYearID={$AcademicYearID}'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>
	</div>
	<div id="HiiddenDiv"></div>
	<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="<?php echo $AcademicYearID;?>" />
	<input type="hidden" id="recordType" name="recordType" value="<?php echo $recordType;?>" />
	<input type="hidden" id="recordId" name="recordId" value="<?php echo $recordId;?>" />
	<input type=hidden id="rowCounter" name="rowCounter" value="0">
	<input type=hidden id="eventDateIDToDelete" name="eventDateIDToDelete" value="">
</form>
<?php
$linterface->LAYOUT_STOP();
?>