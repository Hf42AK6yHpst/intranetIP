<?php
#Modify :
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

//$appQuotaAry = $_POST['appQuotaAry'];


$libenroll = new libclubsenrol();
$lgs = new libgeneralsettings();
$lfcm = new form_class_manage();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ExistSettingsArr = $lgs->Get_General_Setting($libenroll->ModuleTitle);

$SettingArr = array();
$SettingArr['Activity_UseCategorySetting'] 		= $CategorySettingStatus;

if($CategorySettingStatus)
{
	if($sel_category)
	{
		$SettingArr['Activity_DefaultMin'.$sel_category] 		= $defaultMin;
		$SettingArr['Activity_DefaultMax'.$sel_category] 		= $defaultMax;
		$SettingArr['Activity_EnrollMax'.$sel_category] 		= $EnrollMax;
	}
	else // when activate category setting, copy value from general setting to category setting.
	{
		$CatList =  $libenroll->GET_CATEGORY_LIST();
		$numOfCat = count($CatList);
		
		foreach($CatList as $CatInfo)
		{
			$thisCatID = $CatInfo['CategoryID'];
			if(!$ExistSettingsArr['Activity_DefaultMin'.$thisCatID] && !$ExistSettingsArr['Activity_DefaultMax'.$thisCatID] && !$ExistSettingsArr['Activity_EnrollMax'.$thisCatID])
			{
				$SettingArr['Activity_DefaultMin'.$thisCatID] 		= $libenroll->Event_defaultMin;
				$SettingArr['Activity_DefaultMax'.$thisCatID] 		= $libenroll->Event_defaultMax;
				$SettingArr['Activity_EnrollMax'.$thisCatID] 		= $libenroll->Event_EnrollMax;
			}
		}		

//		$termAry = $lfcm->Get_Academic_Year_Term_List(Get_Current_Academic_Year_ID());
//		$numOfTerm = count($termAry);
//		$termAry[$numOfTerm]['YearTermID'] = 0;
//		$numOfTerm = count($termAry);
//		
//		$settingsAry = $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Activity']);
//		$settingsAssoAry = BuildMultiKeyAssoc($settingsAry, array('CategoryID', 'TermNumber', 'SettingName'));
//		
//		$quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();
//		$numOfQuotaType = count($quotaTypeAry);
//		
//		//$appQuotaAry[QuotaType][TermNum][CategoryID] = data
//		$appQuotaAry = array();
//		for ($i=0; $i<$numOfCat; $i++) {
//			$_categoryId = $CatList[$i]['CategoryID'];
//			
//			for ($j=0; $j<$numOfTerm; $j++) {
//				$__yearTermId = $termAry[$j]['YearTermID'];
//				
//				if ($__yearTermId == 0) {
//					$__termNumber = 0;
//				}
//				else {
//					$__termNumber = $j + 1;
//				}
//				
//				if (!isset($settingsAssoAry[$_categoryId][$__termNumber])) {
//					// if the category settings is not set, set the data same as the default settings
//					for ($k=0; $k<$numOfQuotaType; $k++) {
//						$___quotaType = $quotaTypeAry[$k];
//						
//						$appQuotaAry[$___quotaType][$__termNumber][$_categoryId] = $settingsAssoAry[0][$__termNumber][$___quotaType]['SettingValue'];
//					}
//				}
//			}
//		}
	}
	
//	$Success['EditQuotaSettings'] = $libenroll->Save_Application_Quota_Settings($enrolConfigAry['Activity'], $appQuotaAry);
}

$Success['EditGeneralSettings'] = $lgs->Save_General_Setting($libenroll->ModuleTitle, $SettingArr);

header("Location: category_setting_event.php?msg=2&sel_category=".$sel_category);
?>