<?php
# using: 

#########################################
#   Date:   2019-09-10 Henry
#           - pass $AcademicYearID to Get_Activity_Participant_Info()
#
#   Date:   2018-10-16 Anna
#           - Change display title by lang in clubAttendance #P150858
#
#	Date:	2015-01-04 Kenneth
#			- Add filter of meetings in clubs and activity
#
#	Date:	2015-12-01 Kenneth
#			- Add Category Filter
#			- Change seperated sql to local function getStudentFromAllGroup(), getStudentFromAllEvent()
#
#	Date:	2014-10-14
#			Add round report for SIS customization
#
#	Date:	2012-01-06	YatWoon
#			Fixed: Cannot retrieve the "AcademicYearID" variable [Case#2012-0106-0947-14073]
#
##########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if (empty($targetCategory)){
	$conds_category_group ='';
	$conds_category_event ='';
}else{
	$conds_category_group = " AND d.GroupCategory IN ( ".$targetCategory." ) ";
	$specialJoinGroup = ' INNER JOIN INTRANET_ENROL_GROUPINFO as d ON  (a.GroupID = d.GroupID) ';
	$conds_category_event = " AND d.EventCategory IN ( ".$targetCategory." ) ";
	$specialJoinEvent = ' INNER JOIN INTRANET_ENROL_EVENTINFO as d ON  (a.EnrolEventID = d.EnrolEventID) ';
}

if ($plugin['eEnrollment'])
{
	$AcademicYearID = $AcademicYearID?$AcademicYearID:Get_Current_Academic_Year_ID();
	
	$AcademicYearID = IntegerSafe($AcademicYearID);
	$EnrolGroupID = IntegerSafe($EnrolGroupID);
	$EnrolEventID = IntegerSafe($EnrolEventID);
	
	$linterface = new interface_html("popup.html");
	$libenroll = new libclubsenrol($AcademicYearID);
	$libenroll_ui = new libclubsenrol_ui();
	$lg = new libgroup();
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
		
	if ($type=="club")
	{
		//construct table title
		$header .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		$header .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
		<td width=\"10\" class=\"eSporttdborder eSportprinttabletitle\">#</td>
		<td width=\"15%\" wrap=\"nowrap\" class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassName</td>
		<td width=\"15%\" wrap=\"nowrap\" class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassNumber </td>
		<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName </td>
		</tr>\n";
		
		//get all groups and members of that group
		//$groups = $libenroll->returnAllGroupMemberList($targetSemester);
		$ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info($EnrolGroupIDArr='', Get_Current_Academic_Year_ID(), $ShowUserRegOnly=1, array($targetSemester), $ClubKeyword='', $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1);
		if($plugin['SIS_eEnrollment']){
			$ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info($EnrolGroupIDArr='', Get_Current_Academic_Year_ID(), $ShowUserRegOnly=1, $targetRound, $ClubKeyword='', $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1);
		}
		foreach ($ClubInfoArr as $thisEnrolGroupID => $thisClubInfoArr)
		{
			/*
		     list($thisEnrolGroupID,$name,$quota,$members) = $groups[$i];
		     if ($quota==0) $quota = $i_ClubsEnrollment_NoQuota;
		     $size = sizeof($members);
		     */
		    		     	
// 		    $name = $thisClubInfoArr['GroupTitle'];
		    $name = $intranet_session_language=="en" ? $thisClubInfoArr['GroupTitle'] : $thisClubInfoArr['TitleChinese'];
			$quota = $thisClubInfoArr['Quota'];
			$members = $thisClubInfoArr['StatusStudentArr'][2];
			
			 if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($thisEnrolGroupID))
		     	continue;
			 
		     //construct information above the table
			 $x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			 $x .= "<tr><td>";
			 $x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
		     $x .= "<tr class=\"tabletext\"align=\"left\"><td>$i_ClubsEnrollment_GroupName</td><td width=\"10\"></td><td>$name</td></tr>\n";
		     $x .= "<tr class=\"tabletext\"align=\"left\"><td>$i_ClubsEnrollment_Quota</td><td width=\"10\"></td><td>$quota</td></tr>\n";
		     $x .= "<tr class=\"tabletext\"align=\"left\"><td>$i_ClubsEnrollment_CurrentSize</td><td width=\"10\"></td><td>$size</td></tr>\n";
		     $x .= "</table>\n";
		     $x .= "</td></tr>";
			 $x .= "</table>\n";
			 
		     $x .= "$header";
		     
		     //construct content of the table
		     $numOfMember = count($members);
		     for ($j=0; $j<$numOfMember; $j++)
		     {
		          $count = $j+1;
		          $css = (($j%2)+1);
		          
				  $class = $members[$j]['ClassName'];
				  $classnumber = $members[$j]['ClassNumber'];
				  $name = $members[$j]['StudentName'];

		          $x .= "<tr class=\"eSporttdborder eSportprinttext\">
		          			<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
		          			<td class=\"eSporttdborder eSportprinttext\">{$class}&nbsp</td>
		          			<td class=\"eSporttdborder eSportprinttext\">{$classnumber}&nbsp</td>
		          			<td class=\"eSporttdborder eSportprinttext\">{$name}&nbsp</td>
		          		</tr>\n";
		     }
		     if (sizeof($members)==0)
		     {
		         $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_ClubsEnrollment_NoMember</td></tr>\n";
		     }
		     $x .= "</table>\n<br>\n";
		}
	}
	else if ($type=="activity")
	{
		//construct table title
		$header .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		$header .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
		<td width=\"10\" class=\"eSporttdborder eSportprinttabletitle\">#</td>
		<td width=\"15%\" wrap=\"nowrap\" class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassName</td>
		<td width=\"15%\" wrap=\"nowrap\" class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassNumber</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>
		</tr>\n";
		
		//get all activities and students of that activity
		$sql = "SELECT EnrolEventID, EventTitle, Quota, Approved, EnrolGroupID FROM INTRANET_ENROL_EVENTINFO WHERE AcademicYearID='$AcademicYearID' Order By EventTitle Asc";
		
		$actInfoArr = $libenroll->returnArray($sql,4);
		
		$ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info('', $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1);
		
		
		for ($i=0; $i<sizeof($actInfoArr); $i++)
		{
			list($thisEnrolEventID, $title, $quota, $approved, $thisEnrolGroupID) = $actInfoArr[$i];
		    
		    ### If the activity does not belong to any club
			### Hide the activity if the user is a normal user and the user is not the activity's PIC
			if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && !$libenroll->IS_EVENT_PIC($thisEnrolEventID) && ($thisEnrolGroupID==''||$thisEnrolGroupID==0))
			{
				continue;
			}
			
			### If the activity is belong to a club
			### Hide the activity if the user is not the activity's PIC and the activity's club's PIC and the user is a normal user
			if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && $thisEnrolGroupID!='' && $thisEnrolGroupID!=0 && !$libenroll->IS_CLUB_PIC($thisEnrolGroupID) && !$libenroll->IS_EVENT_PIC($thisEnrolEventID))
			{
				continue;
			}
			
			if ($quota==0) $quota = $i_ClubsEnrollment_NoQuota;
			
		    //construct information above the table
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr><td>";
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
		    $x .= "<tr class=\"tabletext\"align=\"left\"><td>{$eEnrollment['act_name']}</td><td width=\"10\"></td><td>$title</td></tr>\n";
		    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$i_ClubsEnrollment_GroupQuota1</td><td width=\"10\"></td><td>$quota</td></tr>\n";
		    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$i_ClubsEnrollment_GroupApprovedCount</td><td width=\"10\"></td><td>$approved</td></tr>\n";
		    $x .= "</table>\n";
		    $x .= "</td></tr>";
			$x .= "</table>\n";
			 
		    $x .= "$header";
		    
		    //get infomration of all students joining the activity
		    /*
		    $name_field = getNameFieldByLang("a.");
		    $sql = "SELECT $name_field, a.ClassName, a.ClassNumber
		    		FROM INTRANET_USER as a LEFT JOIN INTRANET_ENROL_EVENTSTUDENT as b ON a.UserID = b.StudentID
		    		WHERE b.EnrolEventID = '$eventID' AND b.RecordStatus = 2 and a.RecordType = 2 and a.RecordStatus = 1 and a.ClassName is not null and a.ClassNumber is not null
		    		ORDER BY a.ClassName, a.ClassNumber";
		    $eventMemberArr = $libenroll->returnArray($sql,3);
		    */
		    $eventMemberArr = $ActivityInfoArr[$thisEnrolEventID]['StatusStudentArr'][2];
		    if (!is_array($eventMemberArr))
		    	$eventMemberArr = array();
		    
		    //construct content of the table
		    for ($j=0; $j<sizeof($eventMemberArr); $j++)
		    {
			    $count = $j+1;
		        $css = (($j%2)+1);
		        $x .= "<tr class=\"eSporttdborder eSportprinttext\">
		        		<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$eventMemberArr[$j]['ClassName']}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$eventMemberArr[$j]['ClassNumber']}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$eventMemberArr[$j]['StudentName']}&nbsp</td>
		        		</tr>\n";
		    }
		    if (sizeof($eventMemberArr)==0)
		    {
		        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
		    }
		    $x .= "</table>\n<br>\n";
		    
		}
	}
	else if ($type=="classPerform")
	{
		//construct table title
		$header .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		$header .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
		<td width=10 class=\"eSporttdborder eSportprinttabletitle\">#</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassName</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassNumber</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">{$Lang["eEnrolment"]["NoOfEmptyClubPerformance"]}</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">{$Lang["eEnrolment"]["NoOfEmptyActPerformance"]}</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">{$Lang['eEnrolment']['perform_score_comment']}</td>
		</tr>\n";
		
		
		//get name of the student
    	$name_field = getNameFieldByLang('USR.');
    	if($showTarget=="class")
    	{
	       	//$sql = "SELECT UserID, $name_field, ClassNumber, ClassName FROM INTRANET_USER USR WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '$class' ORDER BY 0+ClassNumber";
	       	$YearClassObj = new year_class($class,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
	       	$ClassName = $YearClassObj->Get_Class_Name();
	       	
        	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
        	$studentNameArr = $YearClassObj->ClassStudentList;
        	
    	}
	    else if($showTarget=="club")
	    {
    		$studentNameArr = $libenroll->Get_Club_Member_Info(array($club), $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
    	} else if($showTarget=="house") {
	       	$IndicatorPrefix = '<span class="tabletextrequire">';
			$IndicatorSuffix = "</span>";
			
	       	$temp_stuInfo = $lg->returnGroupUser4Display($house, 2);
	       	for($i=0; $i<sizeof($temp_stuInfo); $i++) {
				$studentIDArr[] = $temp_stuInfo[$i]['UserID'];
				$studentNameArr[$i]['UserID'] = $temp_stuInfo[$i]['UserID'];
				
				$studentNameArr[$i]['StudentName'] = (empty($temp_stuInfo[$i]['ClassName']) ? $IndicatorPrefix."^".$IndicatorSuffix:"") . $temp_stuInfo[$i][3];
				$studentNameArr[$i]['ClassName'] = $temp_stuInfo[$i]['ClassName'];
				$studentNameArr[$i]['ClassNumber'] = $temp_stuInfo[$i]['ClassNumber'];
	     	}
       	}
       	
	 	//construct information above the table
	 	if($showTarget=="class") {
			$x = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr><td>";
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
		    $x .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_general_class}</td><td width=\"10\"></td><td>$ClassName</td></tr>\n";
		    $x .= "</table>\n";
		    $x .= "</td></tr>";
			$x .= "</table>\n";
		}
	 	
	 	$x .= $header ;
	 		
	 	$SumOver = 0;
	 	//loop through all students to get performance and construct table contents
		
		
	 	for ($i=0; $i<sizeOf($studentNameArr); $i++)
	 	{
	     	$studentID = $studentNameArr[$i]['UserID'];
	     	$studentName = $studentNameArr[$i]['StudentName'];
	     	$className = ($studentNameArr[$i]['ClassName'] == "")? $Lang['General']['EmptySymbol'] : $studentNameArr[$i]['ClassName'];
			$classNumber = ($studentNameArr[$i]['ClassNumber'] == "")? $Lang['General']['EmptySymbol'] : $studentNameArr[$i]['ClassNumber'];
			
			/**
			 * Add by Kenneth
			 * 	- Category filtering
			 */
						
	     	//get club performance
	     	//$sql = "SELECT Performance FROM INTRANET_USERGROUP WHERE UserID = '$studentID'";
	     	$sql = "SELECT  a.Performance
	     			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID $specialJoinGroup
	     			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".$AcademicYearID."' $conds_category_group";
	     	$clubPerformArr = $libenroll->returnArray($sql,1);
	     	
	     	
	     	//get activity performance
	     	//$sql = "SELECT Performance FROM INTRANET_ENROL_EVENTSTUDENT WHERE StudentID = '$studentID'";
	     	$sql = "SELECT a.Performance
	     			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID $specialJoinEvent
	     			WHERE a.StudentID = '$studentID'  AND a.RecordStatus = 2" .$conds_category_event;
	     	$actPerformArr = $libenroll->returnArray($sql,1);
	     	
	     	//loop through all performances to get the total mark
	     	$totalMark =  $emptyActPerformance = $emptyClubPerformance = 0;
	     	for ($j=0; $j<sizeof($clubPerformArr); $j++)
	     	{
		     	if (is_numeric($clubPerformArr[$j][0]))
		     	{
			     	$totalMark += $clubPerformArr[$j][0];
		     	}
		     	else if (empty($clubPerformArr[$j][0]))
		     	{
			     	$emptyClubPerformance ++;
		     	}
	     	}
	     	for ($j=0; $j<sizeof($actPerformArr); $j++)
	     	{
		     	if (is_numeric($actPerformArr[$j][0]))
		     	{
			     	$totalMark += $actPerformArr[$j][0];
		     	}	
		     	else if (empty($actPerformArr[$j][0]))
		     	{
			     	$emptyActPerformance ++;
		     	}
	     	}
	     	
	     	$SumOver += $totalMark;
	     	
	     	//construct content of the table
	     	$count = $i+1;
	        $css = (($i%2)+1);
	        $x .= "<tr class=\"eSporttdborder eSportprinttext\">
	        		<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
					<td class=\"eSporttdborder eSportprinttext\">{$className}&nbsp</td>
	        		<td class=\"eSporttdborder eSportprinttext\">{$classNumber}&nbsp</td>
	        		<td class=\"eSporttdborder eSportprinttext\">{$studentName}&nbsp</td>
					<td class=\"eSporttdborder eSportprinttext\">{$emptyClubPerformance}&nbsp</td>
					<td class=\"eSporttdborder eSportprinttext\">{$emptyActPerformance}&nbsp</td>	        		
					<td class=\"eSporttdborder eSportprinttext\">{$totalMark}&nbsp</td>
	        		</tr>\n";
	 	}
	 	
	 	if (sizeof($studentNameArr)==0)
	    {
	        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
	    }
	    
	    $x .= '<tr>';
		$x .= '<td colspan=6 align=right class="eSporttdborder eSportprinttext">'. $Lang['eEnrolment']['total_overall_perform'] .'</td>';
	    $x .= '<td valign="top" class="eSporttdborder eSportprinttext"><b>'.$SumOver.'</b></td>';
    	$x .= '</tr>';
			    	
	    $x .= "</table>\n<br>\n";
		
		$x .= $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
	}
	else if ($type=="student")
	{
		//get info of the student
	 	$name_field = getNameFieldByLang();
	 	$sql = "SELECT ".$name_field.", ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$studentID'";
	 	$studentInfoArr = $libenroll->returnArray($sql,3);
	 	
	 	
		//construct information above the table
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
		$x .= "<tr><td>";
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_UserStudentName}</td><td width=\"10\"></td><td>{$studentInfoArr[0][0]}</td></tr>\n";
	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_UserClassName}</td><td width=\"10\"></td><td>{$studentInfoArr[0][1]}</td></tr>\n";
	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_UserClassNumber}</td><td width=\"10\"></td><td>{$studentInfoArr[0][2]}</td></tr>\n";
	    $x .= "</table>\n";
	    $x .= "</td></tr>";
		$x .= "</table>";
		
		################################## Club Table ##################################
		//construct table title
		$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
		<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>
		<td width=\"84%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['club_name']}</td>
		<td width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['performance']}</td>
		</tr>\n";

		//get all groups of student
	 	$sql = "SELECT b.Title, a.Performance, b.TitleChinese
	 			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID ".$specialJoinGroup."
	 			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".$AcademicYearID."' ".$conds_category_group;
	 	
	 	$clubPerformArr = $libenroll->returnArray($sql);
		
		//loop through each club record to construct table content
		$totalPerform = 0;
	 	for ($i=0; $i<sizeOf($clubPerformArr); $i++)
	 	{
	 		if(empty($clubPerformArr[$i]['Performance']))
     		{
     			$curPerform = "&nbsp;";
     			$emptyClubPerform++;
     		}
     		else if (is_numeric($clubPerformArr[$i]['Performance']))
	     	{
		     	$curPerform = $clubPerformArr[$i]['Performance'];
		     	$totalPerform += $curPerform;
	     	}
	     	else
	     	{
		     	$curPerform = $clubPerformArr[$i]['Performance'];
	     	}
	     	
	     	//construct content of the table
	     	$count = $i+1;
	        $css = (($i%2)+1);
	        $titleDisplay = $intranet_session_language=="en" ? $clubPerformArr[$i]['Title'] : $clubPerformArr[$i]['TitleChinese'];
	        $x .= "<tr class=\"eSporttdborder eSportprinttext\">
	        		<td width=\"1\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
	        		<td width=\"84%\" class=\"eSporttdborder eSportprinttext\">{$titleDisplay}&nbsp</td>
	        		<td width=\"15%\" class=\"eSporttdborder eSportprinttext\">{$curPerform}&nbsp</td>
	        		</tr>\n";
	     	
	 	}
	 	if (sizeof($clubPerformArr)==0)
	    {
	        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
	    }
	    else
	    {
		 	$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
			<td colspan=\"2\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['total_club_performance']}</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">{$totalPerform}</td>
			</tr>";
	    }
	    $x .= "</table>\n<br>\n";
	    
	    
	    ################################## Activity Table ##################################
		//construct table title
		$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
		<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>
		<td width=\"84%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['act_name']}</td>
		<td  width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['performance']}</td>
		</tr>\n";
	 	
		//get all activities of student
	 	$sql = "SELECT b.EventTitle, a.Performance
	 			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID ".$specialJoinEvent."
	 			WHERE a.StudentID = '$studentID'  AND a.RecordStatus = 2".$conds_category_event;
	 	$actPerformArr = $libenroll->returnArray($sql,2);
		
		//loop through each club record to construct table content
		$totalPerform = 0;
	 	for ($i=0; $i<sizeOf($actPerformArr); $i++)
	 	{
	 		if(empty($actPerformArr[$i]['Performance']))
     		{
     			$curPerform = "&nbsp;";
     			$emptyActPerform ++;
     		}
     		else if (is_numeric($actPerformArr[$i]['Performance']))
	     	{
		     	$curPerform = $actPerformArr[$i]['Performance'];
		     	$totalPerform += $curPerform;
	     	}
	     	else
	     	{
		     	$curPerform = $actPerformArr[$i]['Performance'];
	     	}
	     	
	     	//construct content of the table
	     	$count = $i+1;
	        $css = (($i%2)+1);
	        $x .= "<tr class=\"eSporttdborder eSportprinttext\">
	        		<td width=\"1%\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
	        		<td width=\"84%\" class=\"eSporttdborder eSportprinttext\">{$actPerformArr[$i]['EventTitle']}&nbsp</td>
	        		<td width=\"15%\" class=\"eSporttdborder eSportprinttext\">{$curPerform}&nbsp</td>
	        		</tr>\n";
	     	
	 	}
	 	if (sizeof($actPerformArr)==0)
	    {
	        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
	    }
	    else
	    {
		 	$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
			<td colspan=\"2\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['total_act_performance']}</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">{$totalPerform}</td>
			</tr>";
	    }
	    $x .= "</table>\n<br>\n";
	    
	}
//	else if ($type=="clubAttendance")
//	{
//		$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
//		
//		//get academic year
//	 	$title = GET_ACADEMIC_YEAR(date("Y-m-d"))." ".$eEnrollment['year'];
//	 	//get club title
//	 	$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '$GroupID'";
//	 	$result = $libenroll->returnArray($sql,1);
//	 	$groupTitle = $result[0][0];
//	 		 	
//	 	$x = "";
//	 	
//		//construct information above the table
//		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
//		$x .= "<tr><td>";
//		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
//	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$title</td></tr>\n";
//	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$groupTitle"." "."$i_Attendance</td></tr>\n";
//	    $x .= "</table>\n";
//	    $x .= "</td></tr>";
//		$x .= "</table>";
//		
//		//get attendance record
//		//$li = new libgroup($GroupID);
//		//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
//		
//		// get date data from the db and make the array
//		$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);	//GroupDateID, EnrolGroupID, ActivityDateStart, ActivityDateEnd
//		$numOfDate = count($DateArr);
//		
//		// construct a date title array
//		for ($i = 0; $i < sizeof($DateArr); $i++) {
//			$DateTitleArr[$i] .= date("Y-m-d", strtotime($DateArr[$i][2]));
//			$DateTitleArr[$i] .= " ";
//			$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][2]));
//			$DateTitleArr[$i] .= "-";
//			$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][3]));
//		}
//		
//		//construct table title
//		$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
//		$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
//				<td width=\"30%\" class=\"eSporttdborder eSportprinttabletitle\" nowrap>{$eEnrollment['student_name']}</td>";
//		for ($i=0; $i<sizeof($DateTitleArr); $i++)
//		{
//			$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">{$DateTitleArr[$i]}</td>";
//		}
//		$lastTitle = $eEnrollment['studnet_attendence']." (".$eEnrollment['up_to']." ".date("Y-m-d").")";
//		$x .= "<td width=\"20%\" class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">$lastTitle</td>";
//		$x .= "</tr>\n";
//		
//		// get members information
//		/*
//		$sql = "SELECT b.UserID, b.EnglishName, b.ChineseName, b.ClassName, b.ClassNumber
//				FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_USER as b ON a.UserID = b.UserID
//				WHERE (a.EnrolGroupID = ".$EnrolGroupID." AND b.RecordType = 2 and b.RecordStatus = 1 and b.ClassName is not null and b.ClassNumber is not null)
//				ORDER BY b.ClassName, b.ClassNumber
//				";
//		$StuArr = $libenroll -> returnArray($sql, 3);
//		*/
//		$StuArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=0, $ReturnAsso=0);
//		$numOfMember = count($StuArr);	
//							
//		$totalAttendance = array();
//		// calculate the attendance of students
//		for ($i = 0; $i < $numOfMember; $i++) {
//			$totalMeeting = 0;
//			$attended = 0;
//						
//			// get the attendance record for each date and calculate the attendance percentage
//			for ($j = 0; $j < $numOfDate; $j++) 
//			{
//				$AttendDataArr['EnrolGroupID'] = $EnrolGroupID;
//				$AttendDataArr['GroupDateID'] = $DateArr[$j][0];
//				$AttendDataArr['StudentID'] = $StuArr[$i]['UserID'];
//				$attend = $libenroll->Get_Group_Attendance_Status($DateArr[$j][0], $StuArr[$i]['UserID'], $EnrolGroupID);
//					
//				if (date("Y-m-d", strtotime($DateArr[$j][2])) <= date("Y-m-d")) {
//					$totalMeeting++;					
//					$totalArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//					if (in_array($attend, array(1,2))) {
//						$attended++;
//						$StuArr[$i]['Attended'][$j] = $attend;
//						$addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//					}
//					else
//					{
//						$StuArr[$i]['Attended'][$j] = $attend;
//					}
//				}
//				else
//				{
//					//ignore the data of future event
//					$StuArr[$i]['Attended'][$j] = 'f';
//					continue;
//				}
//
//			}
//			$StuArr[$i]['Attendance'] = (($totalMeeting > 0)&&($attended > 0))? my_round(($attended / $totalMeeting * 100), 2)."%" : $StuArr[$i]['Attendance'] = "0%";
//			$totalAttendance[] = (($totalMeeting > 0)&&($attended > 0))? ($attended / $totalMeeting * 100) : 0;
//		}
//		
//		// calculate the date attendance
//		for ($j = 0; $j < $numOfDate; $j++) {
//			if ( ($totalArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0) &&
//				 ($addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0))
//			{
//				$TmpDateAttendance = $addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] / $totalArr[date("Ymd", strtotime($DateArr[$j][2]))];
//				$DateAttendance[$j] = my_round(($TmpDateAttendance * 100), 2);
//				$DateAttendance[$j] .= "%";
//			} else {
//				$DateAttendance[$j] = "0%";
//			}
//			
//			if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) {
//				$DateAttendance[$j] = "-";
//			}
//		}
//		
//		// organize the results in an array
//		for($i=0; $i<$numOfMember; $i++){
//			/*
//			if ($intranet_session_language == "en"){	//current language is Eng
//				if ($StuArr[$i]['EnglishName'] == NULL || $StuArr[$i]['EnglishName'] == ""){	//no English Name => show Chinese
//					$printInfoArr[$i][0] = $StuArr[$i]['ChineseName'];
//				}
//				else{
//					$printInfoArr[$i][0] = $StuArr[$i]['EnglishName'];
//				}
//			}
//			else{
//				if ($StuArr[$i]['ChineseName'] == NULL || $StuArr[$i]['ChineseName'] == ""){	//no English Name => show Chinese
//					$printInfoArr[$i][0] = $StuArr[$i]['EnglishName'];
//				}
//				else{
//					$printInfoArr[$i][0] = $StuArr[$i]['ChineseName'];
//				}
//			}
//			
//			// add class name and class number to the name field
//			if (($StuArr[$i]['ClassName']!=NULL || $StuArr[$i]['ClassName']!="") && ($StuArr[$i]['ClassNumber']!=NULL || $StuArr[$i]['ClassNumber']!="")){
//				$printInfoArr[$i][0] .= " (".$StuArr[$i]['ClassName']."-".$StuArr[$i]['ClassNumber'].")";
//			}				
//			*/
//			
//			$thisStudentName = $StuArr[$i]['StudentName'];
//			$thisClassName = $StuArr[$i]['ClassName'];
//			$thisClassNumber = $StuArr[$i]['ClassNumber'];
//			
//			$printInfoArr[$i][0] = $StuArr[$i]['StudentName'];
//			if ($thisClassName != '' && $thisClassNumber != '')
//				$printInfoArr[$i][0] .= " (".$thisClassName."-".$thisClassNumber.")";
//			
//			for($j=0; $j<sizeof($DateTitleArr); $j++)
//			{
//				$printInfoArr[$i][$j+1] = $StuArr[$i]['Attended'][$j];
//			}
//			
//			$printInfoArr[$i][sizeof($DateTitleArr)+1] = $StuArr[$i]['Attendance'];
//		}
//		
//		//loop through each record to construct table content
//	 	for ($i=0; $i<sizeOf($printInfoArr); $i++)
//	 	{
//	 		//construct content of the table
//	     	$count = $i+1;
//	        $css = (($i%2)+1);
//	        $x .= "<tr class=\"eSporttdborder eSportprinttext\">
//	        		<td class=\"eSporttdborder eSportprinttext\" nowrap>{$printInfoArr[$i][0]}&nbsp</td>";
//	        
//	        //attendance info
//	        for ($j=0; $j<sizeof($DateTitleArr); $j++)
//	        {
//		        $x .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">";
//		        
//		        if ($printInfoArr[$i][$j+1]=='f')
//		        {
//			        //club in the future => show "-"
//			        $x .= "-";
//			    }
//		        else if ($printInfoArr[$i][$j+1]==3)
//		        {
//			        //absent
//			        $x .= "<img src=\"$PATH_WRT_ROOT/images/{$LAYOUT_SKIN}/icon_absent_bw.gif\">&nbsp";
//		        }
//		        else if ($printInfoArr[$i][$j+1]==1)
//		        {
//			        //present
//			        $x .= "<img src=\"$PATH_WRT_ROOT/images/{$LAYOUT_SKIN}/icon_present_bw.gif\">&nbsp";
//		        }
//		        else if ($printInfoArr[$i][$j+1]==2)
//		        {
//			        //exempt
//			        $x .= "<img src=\"$PATH_WRT_ROOT/images/{$LAYOUT_SKIN}/icon_exempt.gif\">&nbsp";
//		        } 
//		        else if ($printInfoArr[$i][$j+1]=='')
//		        {
//		        	$x .= $Lang['General']['EmptySymbol'];
//		        }
//			    
//			    $x .= "</td>";		        
//	        }
//	        
//	        $x .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">{$printInfoArr[$i][sizeof($DateTitleArr)+1]}&nbsp</td>";
//	        $x .= "</tr>\n";
//	     	
//	 	}
//	 	if (sizeof($printInfoArr)==0)
//	    {
//		    $totalCol = sizeof($DateArr) + 2;
//	        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"$totalCol\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
//	    }
//	    else
//	    {
//		 	//show total attendance record
//		 	$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
//				<td width=\"20%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['act_attendence']}</td>";
//			for ($i = 0; $i < sizeof($DateArr); $i++) {
//				$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">".$DateAttendance[$i]."</td>";
//			}
//			
//			// Average Attendance
//			$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">".($libenroll->getAverage($totalAttendance, 2, "", "%"))."</td>";
//			$x .= "</tr>";
//	    }
//		
//	}
//	else if ($type=="activityAttendance")
//	{
//		//get academic year
//	 	$title = GET_ACADEMIC_YEAR(date("Y-m-d"))." ".$eEnrollment['year'];
//	 	//get activity title
//	 	$sql = "SELECT EventTitle FROM INTRANET_ENROL_EVENTINFO WHERE EnrolEventID = '$EnrolEventID'";
//	 	$result = $libenroll->returnArray($sql,1);
//	 	$actTitle = $result[0][0];
//	 		 	
//	 	$x = "";
//	 	
//	 	//construct information above the table
//		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
//		$x .= "<tr><td>";
//		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
//	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$title</td></tr>\n";
//	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$actTitle"." "."$i_Attendance</td></tr>\n";
//	    $x .= "</table>\n";
//	    $x .= "</td></tr>";
//		$x .= "</table>";
//		
//		// get date data from the db and make the array
//		$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID);
//		
//		// construct a date title array
//		for ($i = 0; $i < sizeof($DateArr); $i++) {
//			$DateTitleArr[$i] .= date("m-d", strtotime($DateArr[$i][2]));
//			$DateTitleArr[$i] .= " ";
//			$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][2]));
//			$DateTitleArr[$i] .= "-";
//			$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][3]));
//		}
//		
//		//construct table title
//		$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
//		$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
//				<td width=\"30%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['student_name']}</td>";
//		for ($i=0; $i<sizeof($DateTitleArr); $i++)
//		{
//			$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">{$DateTitleArr[$i]}</td>";
//		}
//		$lastTitle = $eEnrollment['studnet_attendence']." (".$eEnrollment['up_to']." ".date("Y-m-d").")";
//		$x .= "<td width=\"20%\" class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">$lastTitle</td>";
//		$x .= "</tr>\n";
//		
//		/*
//		$sql = "
//					SELECT
//							a.UserID, a.EnglishName, a.ChineseName, a.ClassName, a.ClassNumber
//					FROM
//							INTRANET_USER as a, INTRANET_ENROL_EVENTSTUDENT as b
//					WHERE
//							a.UserID = b.StudentID
//						AND
//							b.EnrolEventID = '".$EnrolEventID."'
//						AND
//							b.RecordStatus = 2
//						AND 
//							a.RecordType = 2 and a.RecordStatus = 1 and a.ClassName is not null and a.ClassNumber is not null
//					ORDER BY
//							a.ClassName, a.ClassNumber, a.EnglishName
//				";
//		$StuArr = $libenroll -> returnArray($sql, 3);
//		*/
//		$ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info(array($EnrolEventID), $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1);
//		$StuArr = (array)$ActivityInfoArr[$EnrolEventID]['StatusStudentArr'][2];
//		$numOfStudent = count($StuArr);
//		
//		$totalAttendance = array();
//		// calculate the attendance
//		for ($i = 0; $i < $numOfStudent; $i++) {
//			$totalMeeting = 0;
//			$attended = 0;
//		
//			// get the attendance record for each date and calculate the attendance percentage
//			for ($j = 0; $j < sizeof($DateArr); $j++) 
//			{
//				$attend = $libenroll->Get_Event_Attendance_Status($DateArr[$j][0],$StuArr[$i]['UserID']);
//				
//				if (date("Y-m-d", strtotime($DateArr[$j][2])) <= date("Y-m-d")) {
//					$totalMeeting++;					
//					$totalArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//					if (in_array($attend, array(ENROL_ATTENDANCE_PRESENT, ENROL_ATTENDANCE_EXEMPT)))
//					{
//						$attended++;
//						$StuArr[$i]['Attended'][$j] = $attend;
//						$addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//					}
//					else
//					{
//						$StuArr[$i]['Attended'][$j] = $attend;
//					}
//				}
//				else
//				{
//					$StuArr[$i]['Attended'][$j] = 'f';
//				}
//			}
//			$StuArr[$i]['Attendance'] = (($totalMeeting > 0)&&($attended > 0))? my_round(($attended / $totalMeeting * 100), 2)."%" : $StuArr[$i]['Attendance'] = "0%";
//			$totalAttendance[] = (($totalMeeting > 0)&&($attended > 0))? ($attended / $totalMeeting * 100) : 0;
//			
//			# show "-" for future event
//			if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) { 
//				$StuArr[$i]['Attendance'] = "-";
//			}
//		}
//			
//		for ($j = 0; $j < sizeof($DateArr); $j++) {
//			if ( ($totalArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0) &&
//				 ($addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0))
//			{
//				$TmpDateAttendance = $addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] / $totalArr[date("Ymd", strtotime($DateArr[$j][2]))];
//				$DateAttendance[$j] = my_round(($TmpDateAttendance * 100), 2);
//				$DateAttendance[$j] .= "%";
//			} else {
//				$DateAttendance[$j] = "0%";
//			}
//			
//			# show "-" for future event
//			if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) { 
//				$DateAttendance[$j] = "-";
//			}
//		}
//		
//		// organize the results in an array
//		for($i=0; $i<sizeof($StuArr); $i++){
//			/*
//			if ($intranet_session_language == "en"){	//current language is Eng
//				if ($StuArr[$i]['EnglishName'] == NULL || $StuArr[$i]['EnglishName'] == ""){	//no English Name => show Chinese
//					$printInfoArr[$i][0] = $StuArr[$i]['ChineseName'];
//				}
//				else{
//					$printInfoArr[$i][0] = $StuArr[$i]['EnglishName'];
//				}
//			}
//			else{
//				if ($StuArr[$i]['ChineseName'] == NULL || $StuArr[$i]['ChineseName'] == ""){	//no English Name => show Chinese
//					$printInfoArr[$i][0] = $StuArr[$i]['EnglishName'];
//				}
//				else{
//					$printInfoArr[$i][0] = $StuArr[$i]['ChineseName'];
//				}
//			}
//			
//			// add class name and class number to the name field
//			if (($StuArr[$i]['ClassName']!=NULL || $StuArr[$i]['ClassName']!="") && ($StuArr[$i]['ClassNumber']!=NULL || $StuArr[$i]['ClassNumber']!="")){
//				$printInfoArr[$i][0] .= " (".$StuArr[$i]['ClassName']."-".$StuArr[$i]['ClassNumber'].")";
//			}				
//			*/
//			
//			$thisStudentName = $StuArr[$i]['StudentName'];
//			$thisClassName = $StuArr[$i]['ClassName'];
//			$thisClassNumber = $StuArr[$i]['ClassNumber'];
//			
//			$printInfoArr[$i][0] = $StuArr[$i]['StudentName'];
//			if ($thisClassName != '' && $thisClassNumber != '')
//				$printInfoArr[$i][0] .= " (".$thisClassName."-".$thisClassNumber.")";
//			
//			for($j=0; $j<sizeof($DateTitleArr); $j++){
//				$printInfoArr[$i][$j+1] = $StuArr[$i]['Attended'][$j];
//			}
//			
//			$printInfoArr[$i][sizeof($DateTitleArr)+1] = $StuArr[$i]['Attendance'];
//		}
//		
//		//loop through each club record to construct table content
//	 	for ($i=0; $i<sizeOf($printInfoArr); $i++)
//	 	{
//	 		//construct content of the table
//	     	$count = $i+1;
//	        $css = (($i%2)+1);
//	        $x .= "<tr class=\"eSporttdborder eSportprinttext\">
//	        		<td class=\"eSporttdborder eSportprinttext\" nowrap>{$printInfoArr[$i][0]}&nbsp</td>";
//	        
//	        for ($j=0; $j<sizeof($DateTitleArr); $j++)
//	        {
//		        $x .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">";
//		        
//		        if ($printInfoArr[$i][$j+1]==3)
//		        {
//			        //absent
//			        $x .= "<img src=\"$PATH_WRT_ROOT/images/{$LAYOUT_SKIN}/icon_absent_bw.gif\">&nbsp";
//		        }
//		        else if ($printInfoArr[$i][$j+1]==1)
//		        {
//			        //present
//			        $x .= "<img src=\"$PATH_WRT_ROOT/images/{$LAYOUT_SKIN}/icon_present_bw.gif\">&nbsp";
//		        }
//		        else if ($printInfoArr[$i][$j+1]==2)
//		        {
//			        //exempt
//			        $x .= "<img src=\"$PATH_WRT_ROOT/images/{$LAYOUT_SKIN}/icon_exempt.gif\">&nbsp";
//		        }
//		        else if ($printInfoArr[$i][$j+1]=='f' || $printInfoArr[$i][$j+1]==0)
//		        {
//			        //activity in the future => show "-"
//			        $x .= $Lang['General']['EmptySymbol'];
//			    }
//			    
//			    $x .= "</td>";
//		        
//	        }
//	        
//	        $x .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">{$printInfoArr[$i][sizeof($DateTitleArr)+1]}&nbsp</td>";
//	        $x .= "</tr>\n";
//	 	}
//	 	if (sizeof($printInfoArr)==0)
//	    {
//		    $totalCol = sizeof($DateArr) + 2;
//	        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"$totalCol\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
//	    }
//	    else
//	    {
//		 	//show total attendance record
//		 	$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
//				<td width=\"20%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['act_attendence']}</td>";
//			for ($i = 0; $i < sizeof($DateArr); $i++) {
//				$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">".$DateAttendance[$i]."</td>";
//			}
//			$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" align=\"center\">".($libenroll->getAverage($totalAttendance, 2, "", "%"))."</td>";
//			$x .= "</tr>";
//	    }
//	}
	else if ($type=="clubAttendance" || $type=="activityAttendance") {
		if ($type=="clubAttendance") {
			$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
			$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
			$isClubPIC = $libenroll->IS_CLUB_PIC($EnrolGroupID);
			$isClubHelper = $libenroll->IS_CLUB_HELPER($EnrolGroupID);
			
			## temp
			$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
			if (!$isEnrolAdmin && !$isEnrolMaster && !$isClubPIC && !$isClubHelper)
				$canEdit = 0;
			else
				$canEdit = 1;
			
			if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
			{
				No_Access_Right_Pop_Up();
			}
			
			
			$recordId = IntegerSafe($_GET['EnrolGroupID']);
			
			$dateIdFieldName = 'GroupDateID';
			
			$recordInfoAry = $libenroll->Get_Club_Info_By_EnrolGroupID($recordId);

			$recordTitle = Get_Lang_Selection($recordInfoAry['TitleChinese'],$recordInfoAry['Title']);
			if ($sys_custom['eEnrolment']['HkugaPriSch_Lottery']) {
				$clubCode = $recordInfoAry['GroupCode'];
				if ($clubCode != '') {
					$recordTitle = $clubCode.' - '.$recordTitle;
				}
			}
			
			$meetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($recordId);
			$studentInfoAry = $libenroll->Get_Club_Member_Info($recordId, array(USERTYPE_STUDENT));
			$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
			$memberAttendanceInfoArr = $libenroll->Get_Student_Club_Attendance_Info($studentIdAry, array($recordId));
			$overallAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($recordId, "", $AcademicYearID);
		}
		else if ($type=="activityAttendance") {
			$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
			$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
			$isActivityPIC = $libenroll->IS_EVENT_PIC($EnrolEventID);
			$isActivityHelper = $libenroll->IS_EVENT_HELPER($EnrolEventID);
			
			$EventInfo = $libenroll->GET_EVENTINFO($EnrolEventID);
			$ActivityEnrolGroupID = $EventInfo['EnrolGroupID'];
			$IsActivityClubPIC = $libenroll->IS_CLUB_PIC($ActivityEnrolGroupID);
			
			
			if (!$isEnrolAdmin && !$isEnrolMaster && !$isActivityPIC && !$isActivityHelper && !$IsActivityClubPIC)
				$canEdit = false;
			else
				$canEdit = true;
			
			
			if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
			{
				No_Access_Right_Pop_Up();
			}
			
			
			$recordId = IntegerSafe($_GET['EnrolEventID']);
			
			$dateIdFieldName = 'EventDateID';
			
			$recordInfoAry = $libenroll->Get_Activity_Info_By_Id($recordId);
			$recordInfoAry = $recordInfoAry[$recordId];
			$recordTitle = $recordInfoAry['EventTitle'];
			
			$meetingDateInfoAry = $libenroll->GET_ENROL_EVENT_DATE($recordId);
			$studentInfoAry = $libenroll->Get_Activity_Participant_Info($recordId, $AcademicYearID);
			$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
			$memberAttendanceInfoArr = $libenroll->Get_Student_Activity_Attendance_Info($studentIdAry, array($recordId));
			$overallAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($recordId, "", $AcademicYearID);
		}
		
		
		if ($sys_custom['eEnrolment']['HkugaPriSch_Lottery']) {
			if ($type=="clubAttendance") {
				$picArr = $libenroll->GET_GROUPSTAFF($recordId, "PIC");
			}
			else if ($type=="activityAttendance") {
				$picArr = $libenroll->GET_EVENTSTAFF($recordId, "PIC");
			}
		 	
		 	include_once($PATH_WRT_ROOT."includes/libuser.php");
		 	$LibUser = new libuser($_SESSION['UserID']);
		 	$delim = "";
		 	for($a=0; $a<count($picArr); $a++) {
			 	$picName .= $delim.$LibUser->getNameWithClassNumber($picArr[$a]['UserID']);
			 	$delim = ", ";
		 	}
			
			$Location = $recordInfoAry['Location'];
			$locationAndPicTr = "<tr class=\"tabletext\"><td align=\"left\">".$Lang['eEnrolment']['Location']." : $Location </td><td align=\"right\">".$eEnrollment['All_Member_Import_FileDescription_PICTitle']." : $picName </td></tr>";
	 	}
		
		$academicYearTitle = GET_ACADEMIC_YEAR(date("Y-m-d"))." ".$eEnrollment['year'];
		//debug_pr($meetingDateInfoAry[0]['ActivityDateStart']);
		$numOfMeeting = count($meetingDateInfoAry);
		$numOfStudent = count($studentIdAry);
		
		//$fromMeetingNum = 0;
		//$toMeetingNum = 7;
		if(is_null($fromMeetingNum)){
			$fromMeetingNum=0;
		}
		if(is_null($toMeetingNum)){
			$toMeetingNum=$numOfMeeting-1;
		}
		
		//debug_pr($fromMeetingNum);
		//debug_pr($toMeetingNum);
				
		$x = '';
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
		$x .= "<tr><td>";
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$academicYearTitle</td></tr>\n";
	    $x .= "<tr class=\"tabletext\"align=\"left\"><td>$recordTitle $i_Attendance</td></tr>\n";
	    $x .= $locationAndPicTr;
	    $x .= "</table>\n";
	    $x .= "</td></tr>";
		$x .= "</table>";
		$x .= '<table width="100%" border="0" cellpadding="2" cellspacing="0" class="eSporttableborder">'."\n";
			$x .= '<tr class="eSporttdborder eSportprinttabletitle">'."\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitl" style="width:20%;">'.$eEnrollment['student_name'].'</td>'."\n";
				
				for ($i=$fromMeetingNum; $i<=$toMeetingNum; $i++) {
					$_dateStart = $meetingDateInfoAry[$i]['ActivityDateStart'];
					$_dateEnd = $meetingDateInfoAry[$i]['ActivityDateEnd'];
					
					$_dateDisplay = '';
					$_dateDisplay .= date("Y-m-d", strtotime($_dateStart));
					$_dateDisplay .= "<br />";
					$_dateDisplay .= date("H:i", strtotime($_dateStart));
					$_dateDisplay .= "-";
					$_dateDisplay .= date("H:i", strtotime($_dateEnd));
					
					$x .= '<td class="eSporttdborder eSportprinttabletitl" style="text-align:center;">'.$_dateDisplay.'</td>'."\n";
				}
				$x .= '<td class="eSporttdborder eSportprinttabletitl" style="width:10%; text-align:center;">'.$eEnrollment['studnet_attendence'].'<br />('.$eEnrollment['up_to'].' '.date("Y-m-d").')</td>'."\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitl" style="width:10%; text-align:center;">'.$Lang['eEnrolment']['AttendanceHour'].'<br />('.$eEnrollment['up_to'].' '.date("Y-m-d").')</td>'."\n";
			$x .= '</tr>'."\n";
			
			if ($numOfStudent == 0) {
				$numOfCol = 1 + $numOfMeeting + 2;
				$x .= '<tr><td colspan="'.$numOfCol.'" style="text-align:center;">'.$i_no_record_exists_msg.'</td></tr>'."\n";
			}
			else {
				for ($i=0; $i<$numOfStudent; $i++) {
					$_studentId = $studentInfoAry[$i]['UserID'];
					$_studentName = Get_Lang_Selection($studentInfoAry[$i]['ChineseName'], $studentInfoAry[$i]['EnglishName']);
					$_className = $studentInfoAry[$i]['ClassName'];
					$_classNumber = $studentInfoAry[$i]['ClassNumber'];
					$_attendancePercentage = $memberAttendanceInfoArr[$_studentId][$recordId]['AttendancePercentageRounded'];
					$_attendanceHours = $memberAttendanceInfoArr[$_studentId][$recordId]['Hours'];
					
					$x .= '<tr class="eSporttdborder eSportprinttext">'."\n";
						$x .= '<td class="eSporttdborder eSportprinttext" nowrap>'.$_studentName.' ('.$_className.' - '.$_classNumber.')</td>'."\n";
						
						// meeting date status icon
						for ($j=$fromMeetingNum; $j<=$toMeetingNum; $j++) {
							$__dateId = $meetingDateInfoAry[$j][$dateIdFieldName];
							$__attendanceStatus = $memberAttendanceInfoArr[$_studentId][$recordId][$__dateId]['RecordStatus'];
							
							$__dateStart = $meetingDateInfoAry[$j]['ActivityDateStart'];
							$__dateStart = date("Y-m-d", strtotime($__dateStart));
							
							$__display = '';
							if ($sys_custom['eEnrolment']['HkugaPriSch_Lottery'] && $libenroll->Is_Future_Date($__dateStart)) {
								$__display = '&nbsp;';
							}
							else {
								$__display = $libenroll_ui->Get_Attendance_Icon($__attendanceStatus, $BW=true);
							}
							$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:center;">'.$__display.'</td>'."\n";
						}
						
						// attendance percentage
						$_display = ($_attendancePercentage=='')? 0 : $_attendancePercentage;
						$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:center;">'.$_display.'%</td>'."\n";
						
						// total attendance hour
						$_display = ($_attendanceHours=='')? 0 : $_attendanceHours;
						$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:center;">'.$_display.'</td>'."\n";
					$x .= '</tr>'."\n";
				}
				
				$x .= '<tr class="eSporttdborder eSportprinttabletitle">'."\n";
					$x .= '<td class="eSporttdborder eSportprinttabletitle">'.$eEnrollment['act_attendence'].'</td>'."\n";
					
					// Meeting Dates average attendance percentage
					for ($i=$fromMeetingNum; $i<=$toMeetingNum; $i++) {
						$_dateId = $meetingDateInfoAry[$i][$dateIdFieldName];
						$_dateStart = $meetingDateInfoAry[$i]['ActivityDateStart'];
						$_dateStart = date("Y-m-d", strtotime($_dateStart));
						
						if ($libenroll->Is_Future_Date($_dateStart)) {
							$_display = ($sys_custom['eEnrolment']['HkugaPriSch_Lottery'])? '&nbsp;' : $Lang['General']['EmptySymbol'];
						}
						else {
							$_dateAverageAttendancePercentage = $overallAttendanceInfoArr[$recordId]['MeetingDateArr'][$_dateId]['AttendancePercentageRounded'];
							$_display = ($_dateAverageAttendancePercentage=='')? '0%' : $_dateAverageAttendancePercentage.'%';
						}
						$x .= '<td class="eSporttdborder eSportprinttabletitle" style="text-align:center;">'.$_display.'</td>'."\n";
					}
					
					// Club or Activity average attendance percentage
					$averageAtendancePercentage = $overallAttendanceInfoArr[$recordId]['AttendancePercentageRounded'];
					$display = ($averageAtendancePercentage=='')? 0 : $averageAtendancePercentage;
					$x .= '<td class="eSporttdborder eSportprinttabletitle" style="text-align:center;">'.$display.'%</td>'."\n";
					
					// Club or Activity average attendance hours
					$averageAtendanceHour = $overallAttendanceInfoArr[$recordId]['AttendancePercentageHourRounded'];
					$display = ($averageAtendanceHour=='')? 0 : $averageAtendanceHour;
					$x .= '<td class="eSporttdborder eSportprinttabletitle" style="text-align:center;">'.$Lang['General']['EmptySymbol'].'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		$x .= '</table>'."\n";
	}
	else if ($type=="classEnrol")
	{
		$lclass = new libclass();
    	//get name of the student
    	if($showTarget=="class")
    	{
	     	//$studentInfoArr = $lclass->getStudentNameListByClassName($class);	//UserID, $name_field, ClassNumber
	     	
	     	$YearClassObj = new year_class($class,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
	     	$ClassName = $YearClassObj->Get_Class_Name();
	     	
        	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
        	
        	$studentInfoArr = $YearClassObj->ClassStudentList;
        	
    	}
		else if($showTarget=="club") 
		{
	     	$studentIDArr = array();
	     	$studentInfoArr = array();
	     	
	     	$name_field = getNameFieldByLang('USR.');
	     	$sql = "SELECT 
							USR.UserID, USR.ClassName, USR.ClassNumber, $name_field 
					FROM 
							INTRANET_USERGROUP USRGP 
	     					LEFT OUTER JOIN 
							INTRANET_USER USR ON (USRGP.UserID=USR.UserID) 
							LEFT OUTER JOIN 
							INTRANET_GROUP GP ON (GP.GroupID=USRGP.GroupID)
	     					LEFT OUTER JOIN 
							INTRANET_ENROL_GROUPINFO iegi ON (iegi.EnrolGroupID = USRGP.EnrolGroupID)
	     			WHERE 
							iegi.EnrolGroupID = '$club' And GP.AcademicYearID = '".$AcademicYearID."'
							AND
							USR.RecordType = 2
	     			ORDER BY USR.ClassName, USR.ClassNumber";
	     	
	     	$studentInfo = $libenroll->returnArray($sql);
	     	for($i=0; $i<sizeof($studentInfo); $i++) {
				list($uID, $clsName, $clsNo, $name) = $studentInfo[$i];
				$studentIDArr[] = $uID;
				$studentInfoArr[$i]['StudentID'] = $uID;
				$studentInfoArr[$i]['StudentName'] = $name;
				$studentInfoArr[$i]['ClassName'] = $clsName;
				$studentInfoArr[$i]['ClassNumber'] = $clsNo;
	     	}
		} else if($showTarget=="house") 
		{
			$IndicatorPrefix = '<span class="tabletextrequire">';
			$IndicatorSuffix = "</span>";
			
	       	$temp_stuInfo = $lg->returnGroupUser4Display($house, 2);
	       	
	       	for($i=0; $i<sizeof($temp_stuInfo); $i++) {
				$studentIDArr[] = $temp_stuInfo[$i]['UserID'];
				$studentInfoArr[$i]['StudentID'] = $temp_stuInfo[$i]['UserID'];
				$studentInfoArr[$i]['StudentName'] = $temp_stuInfo[$i][3];
				$studentInfoArr[$i]['ClassName'] = $temp_stuInfo[$i]['ClassName'];
				$studentInfoArr[$i]['ClassNumber'] = $temp_stuInfo[$i]['ClassNumber'];
	     	}
		}
		
		for ($j=0; $j<sizeof($studentInfoArr); $j++)
     	{
	     	$tableInfo = "";
	     	$clubTableTitle = "";
	     	$clubTableContent = "";
	     	$actTableTitle = "";
	     	$actTableContent = "";
	     	
	     	//construct information above the table
			$tableInfo .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$tableInfo .= "<tr><td>";
			$tableInfo .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
		    $tableInfo .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_UserStudentName}</td><td width=\"10\"></td><td>{$studentInfoArr[$j]['StudentName']}</td></tr>\n";
		    $tableInfo .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_UserClassName}</td><td width=\"10\"></td><td>{$studentInfoArr[$j]['ClassName']}</td></tr>\n";
		    $tableInfo .= "<tr class=\"tabletext\"align=\"left\"><td>{$i_UserClassNumber}</td><td width=\"10\"></td><td>{$studentInfoArr[$j]['ClassNumber']}</td></tr>\n";
		    $tableInfo .= "</table>\n";
		    $tableInfo .= "</td></tr>";
			$tableInfo .= "</table>";
			
			################################## Club Table ##################################
			//construct table title
			$clubTableTitle .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
			$clubTableTitle .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
			<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>
			<td width=\"84%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['club_name']}</td>
			<td width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['performance']}</td>
			</tr>\n";
		 	

		 	
	
			$this_studentID = $studentInfoArr[$j]['StudentID'] ? $studentInfoArr[$j]['StudentID'] : $studentInfoArr[$j]['UserID'];
			//get all groups of student
		 	$sql = "SELECT b.Title, a.Performance, b.TitleChinese
		 			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID ".$specialJoinGroup."
		 			WHERE a.UserID = '".$this_studentID."' AND b.RecordType = 5 And b.AcademicYearID = '".$AcademicYearID."' " .$conds_category_group;
		 			
		 	$clubPerformArr = $libenroll->returnArray($sql);
		 	
			//loop through each club record to construct table content
			$totalPerform = 0;
		 	for ($i=0; $i<sizeOf($clubPerformArr); $i++)
		 	{
		 		if(empty($clubPerformArr[$i]['Performance']))
	     		{
	     			$curPerform = "&nbsp;";
	     			$emptyClubPerform++;
	     		}
	     		else if (is_numeric($clubPerformArr[$i]['Performance']))
		     	{
			     	$curPerform = $clubPerformArr[$i]['Performance'];
			     	$totalPerform += $curPerform;
		     	}
		     	else
		     	{
			     	$curPerform = $clubPerformArr[$i]['Performance'];
		     	}
		     	
		     	//construct content of the table
		     	$count = $i+1;
		        $css = (($i%2)+1);
		        $titleDisplay = $intranet_session_language=="en" ? $clubPerformArr[$i]['Title'] : $clubPerformArr[$i]['TitleChinese'];
		        $clubTableContent .= "<tr class=\"eSporttdborder eSportprinttext\">
		        		<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$titleDisplay}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$curPerform}&nbsp</td>
		        		</tr>\n";
		     	
		 	}
		 	if (sizeof($clubPerformArr)==0)
		    {
		        $clubTableContent .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
		    }
		    else
		    {
			 	$clubTableContent .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
				<td colspan=\"2\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['total_club_performance']}</td>
				<td class=\"eSporttdborder eSportprinttabletitle\">{$totalPerform}</td>
				</tr>";
		    }
		    $clubTableContent .= "</table>\n<br>\n";
		    
		    ################################## Activity Table ##################################
			//construct table title
			$actTableTitle .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
			$actTableTitle .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
			<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>
			<td width=\"84%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['act_name']}</td>
			<td width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['performance']}</td>
			</tr>\n";
		 	
			$this_studentID = $studentInfoArr[$j]['StudentID'] ? $studentInfoArr[$j]['StudentID'] : $studentInfoArr[$j]['UserID'];
			//TODO
			//get all activities of student
		 	$sql = "SELECT b.EventTitle, a.Performance
		 			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID ".$specialJoinEvent."
		 			WHERE a.StudentID = '".$this_studentID."' AND a.RecordStatus = 2" .$conds_category_event;
		 	$actPerformArr = $libenroll->returnArray($sql,2);
			
			//loop through each club record to construct table content
			$totalPerform = 0;
		 	for ($i=0; $i<sizeOf($actPerformArr); $i++)
		 	{
		 		if(empty($actPerformArr[$i]['Performance']))
	     		{
	     			$curPerform = "&nbsp;";
	     			$emptyActPerform ++;
	     		}
	     		else if (is_numeric($actPerformArr[$i]['Performance']))
		     	{
			     	$curPerform = $actPerformArr[$i]['Performance'];
			     	$totalPerform += $curPerform;
		     	}
		     	else
		     	{
			     	$curPerform = $actPerformArr[$i]['Performance'];
		     	}
		     	
		     	//construct content of the table
		     	$count = $i+1;
		        $css = (($i%2)+1);
		        $actTableContent .= "<tr class=\"eSporttdborder eSportprinttext\">
		        		<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$actPerformArr[$i]['EventTitle']}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$curPerform}&nbsp</td>
		        		</tr>\n";
		     	
		 	}
		 	if (sizeof($actPerformArr)==0)
		    {
		        $actTableContent .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
		    }
		    else
		    {
			 	$actTableContent .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
				<td colspan=\"2\" class=\"eSporttdborder eSportprinttabletitle\">{$eEnrollment['total_act_performance']}</td>
				<td class=\"eSporttdborder eSportprinttabletitle\">{$totalPerform}</td>
				</tr>";
		    }
		    $actTableContent .= "</table>\n<br />\n<br /><br />";		    
		    
		    $x .= $tableInfo.$clubTableTitle.$clubTableContent.$actTableTitle.$actTableContent;
     	}
     			
	}
		
	
	$FooterTable = '';
	if ($type=="activityAttendance" || $type=="clubAttendance")
	{
		$FooterTable .= '<table align="left" border="0">';
			$FooterTable .= "<tr>\n";
	        	$FooterTable .= "<td>\n";
//			        $FooterTable .= '<div>'."\n";
//						$FooterTable .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT, $BW=1).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
//						$FooterTable .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//						$FooterTable .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT, $BW=1).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
//						$FooterTable .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//						$FooterTable .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT, $BW=1).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
//						if ($libenroll->enableAttendanceLateStatusRight()) {
//							$FooterTable .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//							$FooterTable .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_LATE, $BW=1).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Late'].'</span>'."\n";
//						}
//					$FooterTable .= '</div>'."\n";
					$FooterTable .= $libenroll_ui->Get_Attendance_Icon_Remarks($BW=1);
				$FooterTable .= "</td>\n";
	        $FooterTable .= "</tr>\n";
	        $FooterTable .= '<tr><td>'.$libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent')).'</td></tr>';
		$FooterTable .= '</table>';	
	} 
	
	################################################################
	
	#####################
	#	Selection box	#
	#####################
	
	function getDateSeletion($label='',$name,$selected=0){
		global $meetingDateInfoAry;
		$x='';
		$x.=$label.'
			<select id="'.$name.'" name="'.$name.'MeetingNum" >';
		
		$numOfMeetingDate = count($meetingDateInfoAry);
		for($i=0;$i<$numOfMeetingDate;$i++){
			if($selected==$i){
				$x.='<option value="'.$i.'" selected>';
			}
			else{
				$x.='<option value="'.$i.'" >';
			}
				$x.=$meetingDateInfoAry[$i]['ActivityDateStart'];
			$x.='</option>';
		}
		//debug_pr($meetingDateInfoAry[0]['ActivityDateStart']);
			  
		$x.='</select>'."\r\n";
		
		return $x;
	}
	//debug_pr($_GET);
	
	$meetingSelectionsHTML = '';
	if($type=='clubAttendance'||$type=='activityAttendance'){
		$meetingSelectionsHTML.='<tr>';
			$meetingSelectionsHTML.='<td>';
				$meetingSelectionsHTML.='<form id="meetingSeletionForm" name="meetingSeletionForm" method="GET" action="print.php" onsubmit="return checkForm()">';
					$meetingSelectionsHTML.='<input type="hidden" name="AcademicYearID" value="'.$AcademicYearID.'">';
					$meetingSelectionsHTML.= '<input type="hidden" name="EnrolGroupID" value="'.$EnrolGroupID.'">';
					$meetingSelectionsHTML.= '<input type="hidden" name="type" value="'.$type.'">';
					$meetingSelectionsHTML.= '<input type="hidden" name="EnrolEventID" value="'.$EnrolEventID.'">';
					$meetingSelectionsHTML.= getDateSeletion('From','from',$fromMeetingNum);
					$meetingSelectionsHTML.= getDateSeletion('To','to',$toMeetingNum);
					$meetingSelectionsHTML.='<input type="submit" class="formsmallbutton" value="'.$Lang['Btn']['Submit'].'"/>';
				$meetingSelectionsHTML.='</form>';
			$meetingSelectionsHTML.='</td>';
		$meetingSelectionsHTML.='</tr>';
	}
	?>
	<script>
	function checkForm(){
		if(parseInt($('#from').val()) > parseInt($('#to').val()) ){
			alert('<?=$Lang['eEnrolment']['Alert']['NotInASCOrder']?>');
			return false;
		}
		return true;
	}
	</script>
	<table width="100%" align="center" class="print_hide" border="0">
	<?=$meetingSelectionsHTML?>
	<tr>
		<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
	</table>
	<?=$x?>
	<br>
	
	<?= $FooterTable ?>
	
	<?
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	intranet_closedb();

}
else
{
?>
You have no priviledge to access this page.
<?
}
?>