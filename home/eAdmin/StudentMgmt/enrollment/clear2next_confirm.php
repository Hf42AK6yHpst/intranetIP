<?php
// Using: 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$LibUser = new libuser($UserID);
$Semester = trim(urldecode(stripslashes($_REQUEST['Semester'])));
$EnrolGroupIDList = trim(urldecode(stripslashes($_REQUEST['EnrolGroupIDList'])));

if ($plugin['eEnrollment'])
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
	if (!$isEnrolAdmin)
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$linterface = new interface_html();
        
    # tags 
    $tab_type = "club";
    $current_tab = 2;
    include_once("management_tabs.php");
  
    $linterface->LAYOUT_START();
    $EnrolGroupIDArr = explode(',', $EnrolGroupIDList);
    echo $libenroll_ui->Get_Clear_Unhandled_Data_Confirm_UI($EnrolGroupIDArr);
?>

<script language="javascript">
function js_Go_Back(jsReturnMsgKey)
{
	window.location = "club_status.php";
}

function js_Go_Clear_Data()
{
	jsEnrolGroupIDList = $('input#EnrolGroupIDList').val();
	
	if (jsEnrolGroupIDList == '')
	{
		alert('<?=$Lang['eEnrolment']['ClearUnhandledData']['jsWarningArr']['NoData']?>');
		return false;
	}
	
	if (confirm('<?=$Lang['eEnrolment']['ClearUnhandledData']['jsWarningArr']['Delete']?>'))
	{
		var jsFormObj = document.getElementById('form1');
		jsFormObj.action = 'clear2next.php';
		jsFormObj.submit();
	}
}
</script>

<?
	$linterface->LAYOUT_STOP();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>