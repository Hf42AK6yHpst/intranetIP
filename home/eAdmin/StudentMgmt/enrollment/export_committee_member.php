<?php

//using: anna

#################################
#		Change Log

#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();	

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$filename = "committee_recruitment_member.csv";

# Header Column
$exportColumn = array("ClubCode", "ClubName","Term","ClassName","ClassNumber","UserLogin","StudentName","Status","Priority");
// $conds = $Semester == '0'?  "AND (iegi.Semester = '".$Semester."' OR iegi.Semester IS NULL)" :"and iegi.Semester = '".$Semester."' ";

$conds = " and iecs.EnrolGroupID = '".$EnrolGroupID."'";
# Data Row
$clsName = "iu.ClassName";
$name_field = getNameFieldByLang2("iu.");
$sql = "SELECT
            ig.GroupCode,ig.Title,ayt.YearTermNameEN, 
            IF($clsName IS NULL OR $clsName='', '".$EmptySymbol."', $clsName) as ClassName,
        	IF(iu.ClassNumber IS NULL OR iu.ClassNumber='', '".$EmptySymbol."', iu.ClassNumber) as ClassNumber,
            iu.UserLogin as UserLogin,
        	$name_field as StudentName,
        	CASE 
                 WHEN iecs.RecordStatus = 1  THEN '".$Lang['General']['Approved']."'
                 WHEN iecs.RecordStatus = 0  THEN '".$Lang['General']['Rejected']."'
                 ELSE '".$EmptySymbol."' 
             END as RecordStatus,
             iecs.Priority,
             ig.AcademicYearID
		                
		FROM INTRANET_ENROL_COMMITTEE_STUDENT as iecs 
			 Inner Join INTRANET_ENROL_GROUPINFO as iegi ON (iegi.EnrolGroupID = iecs.EnrolGroupID)
             LEFT JOIN ACADEMIC_YEAR_TERM AS ayt ON (ayt.YearTermID = iegi.Semester)
			 Inner Join INTRANET_GROUP as ig On (ig.GroupID = iecs.GroupID AND ig.AcademicYearID='$AcademicYearID')
			 Inner Join INTRANET_USER as iu ON (iecs.StudentID = iu.UserID)			    
                         	
        WHERE			
			 ig.RecordType = 5
             AND iecs.ApplyStatus = '1'
			 And ig.AcademicYearID = '".$AcademicYearID."'
             $conds
			";


$result = $libenroll->returnArray($sql,4);

$exportData = array();

for($i=0;$i<count($result);$i++){
    $thisGroupCode= $result[$i]['GroupCode'];
    $thisTitle= $result[$i]['Title'];
    $thisSemester= $result[$i]['YearTermNameEN'];
    $thisClassName= $result[$i]['ClassName'];
    $thisClassNumber= $result[$i]['ClassNumber'];
    $thisUserLogin= $result[$i]['UserLogin'];
    $thisStudentName= $result[$i]['StudentName'];
    $thisRecordStatus= $result[$i]['RecordStatus'];
    $thisPriority = ($result[$i]['Priority'] == ''|| $result[$i]['Priority'] == '0')?'':$result[$i]['Priority'];
    
    $exportData[$i][] = $thisGroupCode;
    $exportData[$i][] = $thisTitle;
    $exportData[$i][] = $thisSemester;
    $exportData[$i][] = $thisClassName;
    $exportData[$i][] = $thisClassNumber;
    $exportData[$i][] = $thisUserLogin;
    $exportData[$i][] = $thisStudentName;
    $exportData[$i][] = $thisRecordStatus;	
    $exportData[$i][] = $thisPriority;
}

$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);

?>