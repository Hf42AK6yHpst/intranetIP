<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID); 
$tableDates = array();

for ($i = 0; $i < sizeof($DateArr); $i++) {	
	if (!in_array(date("Y-m-d", strtotime($DateArr[$i][2])), $tableDates)) {
		$tableDates[] = date("Y-m-d", strtotime($DateArr[$i][2]));
		$StartHour[] = date("H", strtotime($DateArr[$i][2]));
		$StartMin[] = date("i", strtotime($DateArr[$i][2]));
		$EndHour[] = date("H", strtotime($DateArr[$i][3]));
		$EndMin[] = date("i", strtotime($DateArr[$i][3]));
	}
}



$StartDateTimeStamp = strtotime($StartDate." 00:00:00");
$DayOfStartDate = date('w', $StartDateTimeStamp);

$EndDateTimeStamp = strtotime($EndDate." 23:59:59");

$Rows = "";

$DayOfWeek[0] = 1;
$DayOfWeek[1] = 1;
$DayOfWeek[2] = 1;
$DayOfWeek[3] = 1;
$DayOfWeek[4] = 1;
$DayOfWeek[5] = 1;
$DayOfWeek[6] = 1;

for($i=0;$i<7;$i++)
{
	if($DayOfWeek[$i])
	{
		$DaysB4Start = (($i-$StartDay)+7)%7;
		$tempDateTimeStamp = strtotime(date("Y-m-d", $StartDateTimeStamp)." +$DaysB4Start day");
		
		while($tempDateTimeStamp < $EndDateTimeStamp)
		{
			$tempDate = date("Y-m-d", $tempDateTimeStamp);
			
			foreach($tableDates as $key => $value)
			{
				if($tempDate == $value)
				{
					unset($tableDates[$key]);
					$tableDates = array_values($tableDates);
					break;
				}
			}
			
			$td1 = "<td>".$tempDate."<input type='hidden' name='tableDates[]' id='tableDates[]' value='".$tempDate."'></td>";
			$td2 = "<td>";
			
			#Generate Start Time Hour
			$td2 .= "<select id='StartHour[]' name='StartHour[]'>";
			for ($count = 0; $count < 24; $count++) {
				($count < 10) ? $temp = "0".$count : $temp = $count;
				$td2 .= "<option value='".$temp."'".($temp == $Start_h? " Selected" : "").">".$temp."</option>";
			}
			$td2 .= "</select> : ";
			
			#Generate Start Time Minute	
			$td2 .= "<select id='StartMin[]' name='StartMin[]'>";
			for ($count = 0; $count < 56; $count+= 5) {
				($count < 10) ? $temp = "0".$count : $temp = $count;
				$td2 .= "<option value='".$temp ."'" .($temp == $Start_m? " Selected" : "").">".$temp."</option>";
			}
			$td2 .= "</select>";
				
			$td2 .= "<span class=\"tabletext\"> ".$eEnrollment['to']." </span>";
			
						#Generate End Time Hour
			$td2 .= "<select id='EndHour[]' name='EndHour[]'>";
			for ($count = 0; $count < 24; $count++) {
				($count < 10) ? $temp = "0".$count : $temp = $count;
				$td2 .= "<option value='".$temp."'" .($temp == $End_h? " Selected" : "").">".$temp."</option>";
			}
			$td2 .= "</select> : ";
				
			#Generate End Time Minute
			$td2 .= "<select id='EndMin[]' name='EndMin[]'>";
			for ($count = 0; $count < 56; $count+= 5) {
				($count < 10) ? $temp = "0".$count : $temp = $count;
				$td2 .= "<option value='".$temp."'".($temp == $End_m? " Selected" : "").">".$temp."</option>";
				}
			$td2 .= "</select>";
			$td2 .= "</td>";
			
			$td3 = "<td><input type='checkbox' id='toSet[]' name='toSet[]' value='".$tempDate."'></td>";
			//echo "<table><tr>".$td1.$td2.$td3."</tr></table>";
			
			$Row .= "<tr id=\"timetr".$tempDate."\">".$td1.$td2.$td3."</tr>";
			
			$tempDateTimeStamp = strtotime($tempDate." +7 day");
		}
	}
}
debug_r($tableDates);
//echo $Row;

//echo date("Y-m-d H:i:s", strtotime("2010-07-29 15:20:43"));

intranet_closedb();
?>
