<?php
// Editing by 

######### Change Log Start ################
#	Date:	2017-09-14 Anna
#			- added Websams 
#
#	Date:	2017-06-19 Villa
#			- not create the replyslip if no question is set
#
#	Date:	2017-04-20 Villa
#			- add ReplySlip Instruction
#
#	Date:	2017-04-19 Villa
#			- add ReplySlip Related
#
#	Date:	2017-03-17 Villa
#			- add checking for $PICArr['StaffType'] = "PIC"/ "helper" - prevent inserting record when UserID=0
#
#	Date:	2016-07-22 Cara
#			- Add $ModifiedBy
#
#	Date:	2014-11-21 Omas
#			Update target house by Change_Club_Event_GroupMapping()
#
#	Date:	2014-10-14 Pun
#			- Add round report for SIS customization
#
#	Date:	2014-07-22 Bill
#			- Continue to date settting for new group setting
#
#	Date:	2014-05-02 Carlos
#			Customization $sys_custom['eEnrolment']['TWGHCYMA'] - added INTRANET_GROUP relation
#
#	Date:	2013-08-01 Cameron
#			- Add Enrollment Setting (ApplyUserType)			
#
#	Date:	2011-09-30 Henry Chow
#			- update OLE Default Setting at the bottom
#
#############################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lurlparahandler = new liburlparahandler('', '', $enrolConfigAry['encryptionKey']);

$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
$sel_category = IntegerSafe($_POST['sel_category']);
$set_age = IntegerSafe($_POST['set_age']);
$LowerAge = IntegerSafe($_POST['LowerAge']);
$UpperAge = IntegerSafe($_POST['UpperAge']);
$PaymentAmount = IntegerSafe($_POST['PaymentAmount']);
$Quota = IntegerSafe($_POST['Quota']);
$MinQuota = IntegerSafe($_POST['MinQuota']);
$GroupID = IntegerSafe($_POST['GroupID']);
$Semester = IntegerSafe($_POST['Semester']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$applySettingsToClubOfOtherTerms = $_REQUEST['applySettingsToClubOfOtherTerms'];
$Round = IntegerSafe($_POST['Round']);
$ModifiedBy = $_SESSION['UserID'];

$WebSAMSCode= $_POST['WebSAMSSTACode'];
$WebSAMSSTAType = $_POST['WebSAMSSTAType'];


$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club", $AcademicYearID)))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
$li = new libfilesystem();

//INSERT INTO TableName (### Field List ###) Values (### Value List ###)

/******************************* Step1 Start *************************************/

$GroupArr['GroupID'] = $GroupID;
$GroupArr['EnrolGroupID'] = $EnrolGroupID;
$GroupArr['Discription'] = $Discription;
if ($set_age) $GroupArr['UpperAge'] = $UpperAge;
if ($set_age) $GroupArr['LowerAge'] = $LowerAge;
$GroupArr['Gender'] = $gender;
$GroupArr['attach'] = $_FILES['attach'];
$GroupArr['GroupCategory'] = $sel_category;
$GroupArr['PaymentAmount'] = $PaymentAmount;
$GroupArr['Quota'] = $Quota;
$GroupArr['MinQuota'] = $MinQuota;
$GroupArr['isAmountTBC'] = $isAmountTBC;
$GroupArr['Location'] =  str_replace(array('<', '>', ';', 'javascript:', '"', '\''),'',$Location);
$GroupArr['ApplyUserType'] = $ApplyUserType;
$GroupArr['ModifiedBy'] = $ModifiedBy;
if($sys_custom['eEnrolment']['TWGHCYMA']){
	$GroupArr['RelatedGroupID'] = $RelatedGroupID;
}
if($plugin['SIS_eEnrollment']){
	$GroupArr['RoundNumber'] = $Round;
}
$GroupArr['WebSAMSCode'] = $WebSAMSCode;
$GroupArr['WebSAMSSTAType'] = $WebSAMSSTAType;

if($sys_custom['eEnrolment']['ReplySlip'] &&  $_POST['qStr']){
	$Detail = $_POST['qStr'];
// 	$Detail= preg_replace('#\s+#','<br>',trim($Detail));
	$DisplayQuestionNumber = $_POST['DisplayQuestionNumber'];
	$AllFieldsReq = $_POST['AllFieldsReq'];
	$ReplySlipTitle = $_POST['ReplySlipTitle'];
	$ReplySlipInstruction = $_POST['ReplySlipInstruction'];
	$ReplySlipID = $_POST['ReplySlipID'];
	$ReplySlipID = $libenroll->saveReplySlip($ReplySlipID,$ReplySlipTitle,$Detail,$DisplayQuestionNumber,$AllFieldsReq,$IsTemplate ='0',$IsDeleted='0',$ReplySlipInstruction);
	$libenroll->saveReplySlipRelation($ReplySlipID,$EnrolGroupID);
}
// debug_pr($_POST);
// die;
//if ($attachname0 != "") $GroupArr['attach']['name'][0] = $attachname0;
//if ($attachname1 != "") $GroupArr['attach']['name'][1] = $attachname1;
//if ($attachname2 != "") $GroupArr['attach']['name'][2] = $attachname2;
//if ($attachname3 != "") $GroupArr['attach']['name'][3] = $attachname3;
//if ($attachname4 != "") $GroupArr['attach']['name'][4] = $attachname4;

if ($libenroll->IS_GROUPINFO_EXISTS($EnrolGroupID)) {
	$EnrolGroupID = $libenroll->EDIT_GROUPINFO($GroupArr);
} else {
	// Must have INRANET_ENROL_GROUPINFO for eEnrolment1.2
	//$EnrolGroupID = $libenroll->ADD_GROUPINFO($GroupArr);
}

//$libenroll->DEL_GROUPSTUDENT($GroupID);

##Form Mapping
$SuccessArr['Delete_Old_Form_Mapping'] = $libenroll->DEL_GROUPCLASSLEVEL($EnrolGroupID);

$ClasslvlArr['EnrolGroupID'] = $EnrolGroupID;
for ($i = 0; $i < sizeof($classlvl); $i++) {
	$ClasslvlArr['ClassLevelID'] = $classlvl[$i];
	$libenroll->ADD_GROUPCLASSLEVEL($ClasslvlArr);
}

##Group Mapping
$libenroll->Change_Club_Event_GroupMapping($enrolConfigAry['Club'], $EnrolGroupID, $houseIdAry);

/******************************* Step1 End *************************************/

/******************************* Step2 Start *************************************/

//record all UserID of staff before updating for deletion purpose
//otherwise, we do not know which staff has been removed since $pic and $helper only includes the new staff list
$sql = "
			SELECT
						UserID
			FROM
						INTRANET_ENROL_GROUPSTAFF
			WHERE
						EnrolGroupID = '".$EnrolGroupID."'
		";
$oldStaffArr = $libenroll->returnArray($sql,1);

$libenroll->DEL_GROUPSTAFF($EnrolGroupID);

$PICArr['EnrolGroupID'] = $EnrolGroupID;
$PICArr['StaffType'] = "PIC";
for ($i = 0; $i < sizeof($pic); $i++) {
	if($pic[$i]){ #B114166 Prevent inserting UserID = 0
		$PICArr['UserID'] = $pic[$i];
		$libenroll->ADD_GROUPSTAFF($PICArr);
		
		//check if the staff is removed (for deletion later)
		for ($j = 0; $j < sizeof($oldStaffArr); $j++){
			if ($PICArr['UserID'] == $oldStaffArr[$j]['UserID']){
				$oldStaffArr['notRemoved'][$j][0] = true;
			}
		}
	}
		
	/* moved into ADD_GROUPSTAFF
	//check if the satff is already a member
	$sql = "SELECT COUNT(DISTINCT a.UserID) FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$PICArr['UserID']."'))";
	$result = $libenroll->returnArray($sql);	
	
	// add staff to the member list if the staff is not in the group
	if ($result[0][0]==0){
		$sql = "INSERT INTO INTRANET_USERGROUP
					(GroupID, EnrolGroupID, UserID, RecordType, DateInput, DateModified, ApprovedBy)
				VALUES
					('$GroupID', '".$EnrolGroupID."', '".$PICArr['UserID']."', 'A', now(), now(), '".$_SESSION['UserID']."')
				";	
		$libenroll->db_db_query($sql);
	}
	*/
}

$PICArr['StaffType'] = "HELPER";
for ($i = 0; $i < sizeof($helper); $i++) {
	if($helper[$i]){ #B114166 Prevent inserting UserID = 0
		$PICArr['UserID'] = $helper[$i];
		$libenroll->ADD_GROUPSTAFF($PICArr);
		
		# if is Student => do not add to member list
		# otherwise, add to member list
		$sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$PICArr['UserID']."'";
		$personTypeArr = $libenroll->returnVector($sql);
		$personType = $personTypeArr[0];
		
		//check if the staff is removed for deletion later
		for ($j = 0; $j < sizeof($oldStaffArr); $j++){
			if ($PICArr['UserID'] == $oldStaffArr[$j]['UserID']){
				$oldStaffArr['notRemoved'][$j][0] = true;
			}
		}
	}
	
	/* moved into ADD_GROUPSTAFF
	if ($personType != 2)
	{
		//check if the staff is already a member
		$sql = "SELECT COUNT(DISTINCT a.UserID) FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$PICArr['UserID']."'))";
		$result = $libenroll->returnArray($sql);
		
		// add staff to the member list if the staff is not in the group
		if ($result[0][0]==0){
			$sql = "INSERT INTO INTRANET_USERGROUP
						(GroupID, EnrolGroupID, UserID, RecordType, DateInput, DateModified, ApprovedBy)
					VALUES
						('$GroupID', '".$EnrolGroupID."', '".$PICArr['UserID']."', 'A', now(), now(), '".$_SESSION['UserID']."')
					";	
			$libenroll->db_db_query($sql);
		}
	}
	*/
}

//delete staff from member list if is removed by the user
for ($i = 0; $i < sizeof($oldStaffArr); $i++) {
	$sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$oldStaffArr[$i]['UserID']."'";
	$personTypeArr = $libenroll->returnVector($sql);
	$personType = $personTypeArr[0];
	
	if (!$oldStaffArr['notRemoved'][$i][0] && $personType != 2){
		$sql = "DELETE FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$oldStaffArr[$i]['UserID']."'))";
		$libenroll->db_db_query($sql);
	}	
}

/******************************* Step2 End *************************************/

if($plugin['iPortfolio']) {
	/*************** Step 3 - Update OLE Default setting [Start] *****************/
	
	$dataAry['CategoryID'] = $category;
	$dataAry['OLE_Component'] = (count($ele_global)==0) ? "" : implode(',', $ele_global);
	$dataAry['Organization'] = trim(stripslashes($organization));
	$dataAry['Details'] = trim(stripslashes($details));
	$dataAry['SchoolRemark'] = trim(stripslashes($SchoolRemarks));
	$dataAry['TitleLang'] = $titleLang;
	$dataAry['IntExt'] = $intExt;
	
	$libenroll->Update_Enrolment_Default_OLE_Setting('club', $EnrolGroupID, $dataAry);
	
	/*************** Step 3 - Update OLE Default Setting [End] *******************/
}

if ($applySettingsToClubOfOtherTerms) {
	$originalOleSetting = $libenroll->Get_Enrolment_Default_OLE_Setting('club', array($EnrolGroupID));
			
	$newOleSetting = array();
	$newOleSetting['CategoryID'] = $originalOleSetting[0]['CategoryID'];
	$newOleSetting['OLE_Component'] = $originalOleSetting[0]['OLE_Component'];
	$newOleSetting['Organization'] = $originalOleSetting[0]['Organization'];
	$newOleSetting['Details'] = $originalOleSetting[0]['Details'];
	$newOleSetting['SchoolRemark'] = $originalOleSetting[0]['SchoolRemark'];
	$newOleSetting['TitleLang'] = $originalOleSetting[0]['TitleLang'];
	$newOleSetting['IntExt'] = $originalOleSetting[0]['IntExt'];
	
	$groupClubInfoAry = $libenroll->Get_Club_Info_By_GroupID($GroupID);
	$numOfGroupClub = count($groupClubInfoAry);
	
	for ($i=0; $i<$numOfGroupClub; $i++) {
		$thisEnrolGroupID = $groupClubInfoAry[$i]['EnrolGroupID'];
		
		if ($EnrolGroupID == $thisEnrolGroupID) {
			continue;
		}
		
		$SuccessArr['ApplySettingsToOtherTerm'][$thisEnrolGroupID]['DeleteOldFormMapping'] = $libenroll->DEL_GROUPCLASSLEVEL($thisEnrolGroupID);
		$SuccessArr['ApplySettingsToOtherTerm'][$thisEnrolGroupID]['DeleteOldClubStaff'] = $libenroll->DEL_GROUPSTAFF($thisEnrolGroupID);
		
		// Copy Basic Club Info
		$SuccessArr['ApplySettingsToOtherTerm'][$thisEnrolGroupID]['CopyClubInfo'] = $libenroll->Copy_Club_Info($EnrolGroupID, $thisEnrolGroupID);
		
		// Copy Club Staff
		$SuccessArr['ApplySettingsToOtherTerm'][$thisEnrolGroupID]['CopyClubStaff'] = $libenroll->Copy_Club_Staff($EnrolGroupID, $thisEnrolGroupID);
		
				
		if($plugin['iPortfolio']) {
			// Copy Default OLE Setting
			$successArr[$_enrolGroupId]['copyOleSettings'] = $libenroll->Update_Enrolment_Default_OLE_Setting('club', $thisEnrolGroupID, $newOleSetting);			
		}
	}
}

//header("Location: group_new1b.php?EnrolGroupID=$EnrolGroupID&Semester=$Semester");
$link = "group.php?msg=2&pic_view=$pic_view&AcademicYearID=$AcademicYearID&Semester=$Semester";
if ($page_link != "") $link = $page_link;

// For new group setting
if(IntegerSafe($DirectToMeetingGroup)){
	$_para = 'AcademicYearID='.$AcademicYearID.'&Semester='.$Semester.'&recordId='.$EnrolGroupID.'&recordType='.$enrolConfigAry['Club'].'&DirectToMemberGroup=1';
	
	// encrypt the data
	$lurlparahandler->setParaDecrypted($_para);
	$lurlparahandler->doEncrypt();
	$_paraEncrpted = $lurlparahandler->getParaEncrypted();
	
	$link = "meeting_date.php?p=$_paraEncrpted";
}

intranet_closedb();

//debug_pr($link);
header("Location: $link");
?>