<?php

# using: 

##### Change Log [Start] #####
#	Date	:	2015-06-08
#				Update UI
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'] && !$plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageSysSettingClassLvl";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	$enrolInfo = $lc->getEnrollmentInfo();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['class_level_access'], "", 1);
        
        # tags 
        //$current_tab = 2;
        //include_once("settings_tabs.php");

        $linterface->LAYOUT_START();

$lf = new libfilesystem();
$le = new libclubsenrol();
$lc = new libclass();
$minmax_array = $le->getMinMax();
$lvls = $lc->getLevelArray();
?>

<script language="javascript">

function checkform(obj){
<?
//for ($i=0; $i<sizeof($lvls); $i++)
//{
//	print "\tif (!(IsNumeric(obj.min{$i}.value) || obj.min{$i}.value == '')) { alert('{$i_ServiceMgmt_System_Warning_Numeric}'); return false; } \n";
//	print "\tif (!(IsNumeric(obj.max{$i}.value) || obj.max{$i}.value == '')) { alert('{$i_ServiceMgmt_System_Warning_Numeric}'); return false; } \n";
//	print "\tif (!(IsNumeric(obj.enrollmax{$i}.value) || obj.enrollmax{$i}.value == '')) { alert('{$i_ServiceMgmt_System_Warning_Numeric}'); return false; } \n";
//}
?>
	var numOfLevel = $('input#num').val();
	var i;
	for (i=0; i<numOfLevel; i++) {
		var applyMinTbId = 'min' + i;
		var applyMaxTbId = 'max' + i;
		var enrollMaxTbId = 'enrollmax' + i;
	
		var applyDefaultChecked = $('input#Def' + i).attr('checked');
		var applyMinVal = $('input#' + applyMinTbId).val();
		var applyMaxVal = $('input#' + applyMaxTbId).val();
		var enrollMaxVal = $('input#' + enrollMaxTbId).val();
		var notUserEnrolChecked = $('input#NotUse' + i).attr('checked');
		
		if (applyDefaultChecked || notUserEnrolChecked) {
			// no need checking
		}
		else {
			if (!IsNumeric(applyMinVal) || applyMinVal=='') {
				$('input#' + applyMinTbId).focus();
				alert('<?=$i_ServiceMgmt_System_Warning_Numeric?>');
				return false;
			}
			else if (!IsNumeric(applyMaxVal) || applyMaxVal=='') {
				$('input#' + applyMaxTbId).focus();
				alert('<?=$i_ServiceMgmt_System_Warning_Numeric?>');
				return false;
			}
			else if (!IsNumeric(enrollMaxVal) || enrollMaxVal=='') {
				$('input#' + enrollMaxTbId).focus();
				alert('<?=$i_ServiceMgmt_System_Warning_Numeric?>');
				return false;
			}
		}
	}
}


function selectAlt(obj,target)
{
         if (obj.checked)
         {
             target.checked = false;
         }
}

</script>

<div align="right"><? if ($msg == 2) print $linterface->GET_SYS_MSG("update"); ?></div>
<div align="center"><font class="tabletext" style="color: red"><!--<?= $i_ClubsEnrollment_Warning_NotUse ?>-->&nbsp;</font></div>

<form name="form1" action="student_update.php" method="post" onSubmit="return checkform(this);" enctype="multipart/form-data">

<center><div style="width:99%;">
<table class="common_table_list_v30 edit_table_list_v30">
	<thead>
		<tr>
			<th><?=$i_ClassLevelName?></td>
			<th><?=$i_ClubsEnrollment_UseDefault?></td>
			<th><?= $eEnrollment['setting']['club_upper_limit']?></td>
			<th><?= $eEnrollment['setting']['club_lower_limit']?></td>
			<th><?= $eEnrollment['default_enroll_max_club']?></td>
			<th><?= $eEnrollment['DoNoUseEnrolment']?></td>
		</tr>
	</thead>
<?php
for ($i=0; $i<sizeof($lvls); $i++)
{
     $id = $lvls[$i][0];

     if ($minmax_array[$id]===false)
     {
         $chkDef = "";
         $min = "";
         $max = "";
         $enrollmax = "";
         $disabled = "CHECKED";
     }
     else if (!is_array($minmax_array[$id]))
     {
         $chkDef = "CHECKED";
         $min = "";
         $max = "";
         $enrollmax = "";
         $disabled = "";
         $checked = "";
     }
     else
     {
         $chkDef = "";
         $min = $minmax_array[$id][0];
         $max = $minmax_array[$id][1];
         $enrollmax = $minmax_array[$id][2];
         $disabled = "";         
         $checked = ($minmax_array[$id][3]) ? "checked" : "";
     }
     
     ?>
     <tbody>
     	<tr>
	     <td><?=$lvls[$i][1]."<input type=hidden id=LevelID$i name=LevelID$i value=$id>"?></td>
	     <td><input type="checkbox" id="Def<?=$i?>" name="Def<?=$i?>" <?=$chkDef?> value="1" onClick="selectAlt(this.form.Def<?=$i?>,this.form.NotUse<?=$i?>)"></td>
	     <td><input type="text" size="2" id="min<?=$i?>" name="min<?=$i?>" value="<?=$min?>" class="textboxnum" onblur=" if ((document.getElementById('min<?=$i?>').value != '')||(document.getElementById('max<?=$i?>').value != '')||(document.getElementById('enrollmax<?=$i?>').value != '')) { document.form1.Def<?=$i?>.checked = false; } else { document.form1.Def<?=$i?>.checked = true; }"></td>
	     <td><input type="text" size="2" id="max<?=$i?>" name="max<?=$i?>" value="<?=$max?>" class="textboxnum" onblur=" if ((document.getElementById('min<?=$i?>').value != '')||(document.getElementById('max<?=$i?>').value != '')||(document.getElementById('enrollmax<?=$i?>').value != '')) { document.form1.Def<?=$i?>.checked = false; } else { document.form1.Def<?=$i?>.checked = true; }"></td>
	     <td><input type="text" size="2" id="enrollmax<?=$i?>" name="enrollmax<?=$i?>" value="<?=$enrollmax?>" class="textboxnum" onblur=" if ((document.getElementById('min<?=$i?>').value != '')||(document.getElementById('max<?=$i?>').value != '')||(document.getElementById('enrollmax<?=$i?>').value != '')) { document.form1.Def<?=$i?>.checked = false; } else { document.form1.Def<?=$i?>.checked = true; }"></td>
	     <td><input type="checkbox" id="NotUse<?=$i?>" name="NotUse<?=$i?>" <?=$disabled?> <?= $checked?> value="1" onClick="selectAlt(this.form.NotUse<?=$i?>,this.form.Def<?=$i?>)""></td>
     	</tr>
     </tbody>
     <?
}
?>
</table>
</div>
<input type="hidden" id="num" name="num" value="<?=sizeof($lvls)?>">

<div class="edit_bottom_v30">
<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
</div>


</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
	intranet_closedb();
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
?>