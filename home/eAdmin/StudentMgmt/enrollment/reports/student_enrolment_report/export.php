<?php
## Modifying By : 
##### Change Log [Start] #####
#
#	Date	:	2015-03-23 Omas
#				Add this page for handling export data
# 	
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$libenroll = new libclubsenrol();
$lc = new libclass();

$NoOfTarget = count($_POST['rankTargetDetail']);

// Get Target StudentID
if($_POST['rankTarget']=="form") {
	$studentID = array();
	
	for($i=0; $i<$NoOfTarget; $i++) {
		
		$temp = $lc->storeStudent(0,$_POST['rankTargetDetail'][$i], Get_Current_Academic_Year_ID());
				
		if(count($temp)>0){
			$studentID = array_merge($studentID, $temp);
		}
	}
}else if($_POST['rankTarget']=="class") {
		
			$studentID = array();
			for($i=0; $i<$NoOfTarget; $i++) {
				$temp = $lc->storeStudent(0,"::".$_POST['rankTargetDetail'][$i], Get_Current_Academic_Year_ID());
				if(count($temp)>0){
					$studentID = array_merge($studentID, $temp);
				}
			}
} else if($_POST['rankTarget']=="student") {

			$studentID = $_POST['studentID'];

}

if($ExportFlag == 1){
	$ActivityDataAssoAry = $libenroll->TKOGSS_Get_Activity_Details_PerYear($studentID, $_POST['selectYear']);
	$ClubDataAssoAry = $libenroll->TKOGSS_Get_Club_Details($studentID, $_POST['selectYear']);
}
else{
	$ActivityDataAssoAry = $libenroll->TKOGSS_Get_Activity_Details($studentID, $_POST['selectYear']);
	$ClubDataAssoAry = $libenroll->TKOGSS_Get_Club_Details($studentID, $_POST['selectYear']);
}

// Max Enrol Activity of each cat
foreach((array)$ActivityDataAssoAry as $_UserID => $_CategoryAry){
	foreach((array)$_CategoryAry as $__CatId => $__EnrolInfoAry){
		$__studentCatNumEnrol = count($__EnrolInfoAry);
		if($__maxNumOfEnrol[$__CatId] == 0 || $__maxNumOfEnrol[$__CatId]<$__studentCatNumEnrol){
		$__maxNumOfEnrol[$__CatId] = $__studentCatNumEnrol;
		}
	}
}
$maxNumOfEnrolActvities = $__maxNumOfEnrol;

// Max Enrol club of each cat
foreach((array)$ClubDataAssoAry as $_UserID => $_CategoryAry){
	foreach((array)$_CategoryAry as $__CatId => $__EnrolInfoAry){
		$__studentCatNumEnrol = count($__EnrolInfoAry);
		if($__maxNumOfEnrol[$__CatId] == 0 || $__maxNumOfEnrol[$__CatId]<$__studentCatNumEnrol){
		$__maxNumOfEnrol[$__CatId] = $__studentCatNumEnrol;
		}
	}
}
$maxNumOfEnrolClubs = $__maxNumOfEnrol;

$CategoryType_Array = $libenroll->GET_CATEGORY_LIST();
$CategoryType_AssosArray = BuildMultiKeyAssoc($CategoryType_Array,'CategoryID');
$CatIDAry = Get_Array_By_Key($CategoryType_Array, 'CategoryID');

// calculate the number of colume of each cat
$maxNumOfEnrol = array();
foreach((array)$CatIDAry as $_CatID){
	$_ClubCatMaxNum = $maxNumOfEnrolClubs[$_CatID];
	$_ActiCatMaxNum = $maxNumOfEnrolActvities[$_CatID];
	
	$maxNumOfEnrol[$_CatID] = $_ClubCatMaxNum >= $_ActiCatMaxNum ?  $_ClubCatMaxNum:$_ActiCatMaxNum;
}

// prepare export column
$exportColumn = array();
$exportColumn = array('Cls_name', 'Class_no.', 'Reg_no', 'Ch_name', 'En_name', 'Hk_ID', 'Dob', 'Sex');

foreach((array)$maxNumOfEnrol as $_CatId => $_Num ){
	
	if($_Num > 0){
		$_CatName = $CategoryType_AssosArray[$_CatId]['CategoryName'];
		preg_match("/\((.*?)\)/", $_CatName, $_CatNameRegexAry);
		if(empty($_CatNameRegexAry)){
			$_name = $_CatName;
		}else{
			$_name = $_CatNameRegexAry[1];	
		}
		
		for($__i = 1; $__i<=$_Num; $__i++){
			$exportColumn[] = $_name.'yr'.$__i;
			$exportColumn[] = $_name.$__i;
			$exportColumn[] = $_name.'post'.$__i;
			$exportColumn[] = $_name.'score'.$__i;
			$exportColumn[] = $_name.'ref'.$__i;
		}
		$exportColumn[] = $_name.'total';
	}
	else{
		continue;
	}
}

$LibUser = new libuser('','',$studentID);

$ExportArr = array();
$row = 0;
foreach((array)$studentID as $_UserID){
	$LibUser->LoadUserData($_UserID);
	$ExportArr[$row][] = $LibUser->ClassName;		//Cls_name
	$ExportArr[$row][] = $LibUser->ClassNumber;		//Class_no
	$ExportArr[$row][] = $LibUser->WebSamsRegNo;	//Reg_no
	$ExportArr[$row][] = $LibUser->ChineseName;		//Ch_name
	$ExportArr[$row][] = $LibUser->EnglishName;		//En_name
	$ExportArr[$row][] = $LibUser->HKID;			//Hk_ID
	$ExportArr[$row][] = $LibUser->DateOfBirth;		//Dob
	$ExportArr[$row][] = $LibUser->Gender;			//Sex

	foreach((array)$maxNumOfEnrol as $__CatId => $__Num ){
		$mergeClubActivityAry = $libenroll->TKOGSS_Merge_Club_Activities_Details($ClubDataAssoAry,$ActivityDataAssoAry,$_UserID,$__CatId);
		
		for($___i =  0; $___i < $__Num ; $___i++){
			
			$___StudentActivityInfoAry = $mergeClubActivityAry[$___i];
			$ExportArr[$row][] = $___StudentActivityInfoAry['Year'];		//Year
			$ExportArr[$row][] = $___StudentActivityInfoAry['Title'];		//activity name
			$ExportArr[$row][] = $___StudentActivityInfoAry['Role']; 		//post
			$ExportArr[$row][] = $___StudentActivityInfoAry['Performance'];	//score
			$ExportArr[$row][] = $___StudentActivityInfoAry['Ref'];			//ref
			
			$___score = $___StudentActivityInfoAry['Performance']+0;
			$_studentCatTotalScore += $___score;
		}
		if($__Num > 0){
			//total
			$ExportArr[$row][] = $_studentCatTotalScore;
			$_studentCatTotalScore = 0;
		}
	}
	$row++;
}

intranet_closedb();

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

// Output the file to user browser
$filename = "student_enrollment_info_list";
$filename = $filename.".csv";
$lexport->EXPORT_FILE($filename, $export_content);

?>