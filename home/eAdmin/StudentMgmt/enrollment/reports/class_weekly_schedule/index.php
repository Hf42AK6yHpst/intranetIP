<?php
/*
 *  Using:
 *  
 *  2018-10-23 Cameron
 *      - modify changeCourse(), change getCourseCategory to getCourseCategoryAndLesson so that it also update LessonTitleSelect
 *      
 *  2018-10-16 Cameron
 *      - void updateNumberOfStudent()
 *      - add function updateCourseCategory() 
 *      
 *  2018-10-15 Cameron
 *      - add function editLesson()
 *      
 *  2018-10-04 Cameron
 *      - increase the width of thickbox layout for adding new lesson
 *      
 *  2018-09-19 Cameron
 *      - add script to set addLesson div height to the same as its parent td in refreshReport()
 *      - modify changeCourse() to hide error when change course
 *      
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-09-13 Cameron
 *      - add function changeCourse(), updateNumberOfStudent() <- void
 *      
 *  2018-09-12 Cameron
 *      - add function to suport adding new lesson: addLesson(), Show_Edit_Background(), Hide_Edit_Background()
 *      
 *  2018-09-06 Cameron
 *      - set empty column width of the table to 0 (td.empty)   
 *      - temporary disable Block_Document and UnBlock_Document for HKPF
 *      
 *  2018-09-03 Cameron
 *      - add css lessonBorder_conflict
 *      
 *  2018-08-28 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();

if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$linterface = new interface_html();
$libenroll_report = new libclubsenrol_report();
$libenroll_ui = new libclubsenrol_ui();

# setting the current page
$CurrentPage = "PageClassWeeklySchedule";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClassWeeklySchedule']['ReportName'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($returnMsg);

?>

<?php echo $libenroll_report->getClassWeeklyScheduleLayout($intakeID='');?>

<link rel="stylesheet" href="<?php echo $PATH_WRT_ROOT;?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $PATH_WRT_ROOT;?>templates/jquery/jquery.datepick.css" type="text/css" />
<script type="text/javascript" src= "<?php echo $PATH_WRT_ROOT;?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?php echo $PATH_WRT_ROOT;?>templates/jquery/jquery.datepick.js"></script>

<style type="text/css">
    td.empty {
        padding: 0;
        background-color:#FFFFFF; 
        border-bottom: 1px solid #BCBCBC;
        border-right: 1px dotted #BCBCBC;
    }
    td.eventDate {
        background-color:#d7ece9; 
        border-bottom: 1px solid #BCBCBC; 
        border-right: 1px solid #BCBCBC; 
        border-left: 1px solid #BCBCBC;
    }
    td.eventDateItem {
        background-color:#FFFFFF; 
        text-align:left;
    }
	.lessonBorder_normal {
	    color: #000000;
	    border-top: 1px solid #4db308;
	    border-bottom: 1px solid #4db308;
	    border-right: 1px solid #4db308;
	    border-left: 1px solid #4db308;
	}
	.lessonBorder_conflict {
	    color: #2f75e9;
	    border-top: 1px solid #2f75e9;
	    border-bottom: 1px solid #2f75e9;
	    border-right: 1px solid #2f75e9;
	    border-left: 1px solid #2f75e9;
	}	
</style>

<script language='JavaScript'>
var isLoading = true;
var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

$(document).ready( function() {
	var prevIntakeID = '';	

	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar', mandatory: true});
	$('#WeekStartDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
	});

	$('#IntakeID').focus(function(){
		prevIntakeID = $("#IntakeID").val();  
	}).change(function(){
		isLoading = true;
		var intakeID = $("#IntakeID").val();
		if (intakeID) {
    		$("#tdIntakeClassSelection").html(loadingImg);
    		prevIntakeID = intakeID;
    		
    		$.ajax({
    			dataType: "json",
    			type: "POST",
    			url: 'ajax.php',
    			data : {
    				'action': 'getClassList',
    				'IntakeID' : intakeID
    			},		  
    			success: updateClassList,
    			error: show_ajax_error
    		});
		}
		else {
			$("#IntakeID").val(prevIntakeID);
		}
	});

	refreshReport();

});

function js_Show_Thick_Box()
{
	$('a#thickboxButton').click();
}

function jsChangeWeek()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			'action': 'getDisplayWeekByCal',
			'WeekStartDate' : $("#WeekStartDate").val()
		},		  
		success: updateScheduleList,
		error: show_ajax_error
	});
}

function updateClassList(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$("#tdIntakeClassSelection").html(ajaxReturn.html);
		isLoading = false;
		refreshReport();
	}	
}

function updateScheduleList(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$("#WeekStartTimeStamp").val(ajaxReturn.html);
		$("#DIV_WeeklyDisplay").html(ajaxReturn.html2);
		
		refreshReport();
	}	
}

function show_ajax_error() 
{
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

function changeClass(obj) 
{
	refreshReport();
}

function refreshReport() 
{
//	Block_Document();

	$.ajax({
		url:      	"ajax.php",
		dataType: 	"json",
		type:     	"POST",
		data:     	$("#form2").serialize() + '&action=getClassWeeklyScheduleTable',
		success:  	function(ajaxReturn) {
						if (ajaxReturn != null && ajaxReturn.success){
							$('#reportResultDiv').html(ajaxReturn.html);
							Scroll_To_Top();
//							UnBlock_Document();
							initThickBox();

							$('div.addLesson').each(function(){
								$(this).height($(this).closest('td').height());
							});
						}
		},
		error: show_ajax_error
	});
}
 
function jsNextWeek()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			'action': 'getDisplayWeekByNav',
			'WeekStartTimeStamp' : $("#WeekStartTimeStamp").val(),
			'offset' : 7
		},		  
		success: updateScheduleList,
		error: show_ajax_error
	});
}

function jsPrevWeek()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			'action': 'getDisplayWeekByNav',
			'WeekStartTimeStamp' : $("#WeekStartTimeStamp").val(),
			'offset' : -7
		},		  
		success: updateScheduleList,
		error: show_ajax_error
	});
}

function jsPrintClassWeeklySchedule()
{
	$('form#form2').attr('target', '_blank').attr('action', 'print.php').submit();
}

function jsExportClassWeeklySchedule(){
	$('form#form2').attr('target', '_self').attr('action', 'export.php').submit();
}

function jsSaveSetting(){
	var StartTime = $('#StartHour').val() + ':' + $('#StartMin').val() + ':00';
	var EndTime = $('#EndHour').val() + ':' + $('#EndMin').val() + ':00';
	var TimeInterval = $('#timeInterval').val();  
	
	js_Hide_ThickBox();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			action: 'saveWeelyScheduleSetting',
			StartTime: StartTime,
			EndTime: EndTime,
			TimeInterval: TimeInterval
		},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				refreshReport();
			}	
		},
		error: show_ajax_error
	});
}

function checkTime()
{
	var StartHour = $('#StartHour').val();
	var EndHour = $('#EndHour').val();
	var StartMin = $('#StartMin').val();
	var EndMin = $('#EndMin').val();
	var TimeInterval = $('#timeInterval').val(); 
	var HourDiff = 0;
	var MinDiff = 0;
	
	HourDiff = EndHour - StartHour;
	MinDiff = EndMin - StartMin;
	
	var valid = 1;
	
	if(StartHour > EndHour){
		valid = 0;
	}
	else if(StartHour == EndHour && StartMin > EndMin){
		valid = 0;
	}
	
	if(TimeInterval == 0 ){
		valid = 0;
	}
	
	if(HourDiff ==0 && MinDiff == 0){
		valid = 0;
	}
	else if(HourDiff <0){
		valid = 0;
	}
	
	if(valid > 0){
		jsSaveSetting();
	}
	else{
		alert('<?php echo $Lang['eEnrolment']['report']['warning']['selectCorrectTime'];?>');
		return false;
	}
	
}

function Show_Edit_Background(Obj) 
{
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) 
{
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

function addLesson(intakeClassID, lessonDate, startTime, targetEndTime, maxEndTime) 
{
	tb_show('<?php echo $Lang['eEnrolment']['course']['addLesson'];?>','ajax.php?action=addLesson&IntakeClassID='+intakeClassID+'&LessonDate='+lessonDate+'&StartTime='+startTime+'&TargetEndTime='+targetEndTime+'&MaxEndTime='+maxEndTime+'&height=400&width=1100');
}

function editLesson(eventDateID, intakeClassID) 
{
	tb_show('<?php echo $Lang['eEnrolment']['course']['editLesson'];?>','ajax.php?action=editLesson&EventDateID='+eventDateID+'&IntakeClassID='+intakeClassID+'&height=400&width=1100');
}

function changeCourse(obj)
{
	var $this = $(obj);
	if ($this.val() != '') {
		$('span#ErrCourse').addClass('error_msg_hide').removeClass('error_msg_show');
		isLoading = true;
		$("td#tdNumberOfStudent").html(loadingImg);
		$("select#LessonTitleSelect").html(loadingImg);
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax.php',
			data : {
				'action': 'getCourseCategoryAndLesson',
				'CourseID' : $this.val() 
			},		  
			success: updateCourseCategoryAndLesson,
			error: show_ajax_error
		});

	}
}

function updateCourseCategoryAndLesson(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$("input#CourseCategoryID").val(ajaxReturn.html);
		$("select#LessonTitleSelect").replaceWith(ajaxReturn.html2);
		isLoading = false;
		changeDateTimeSelect(document.getElementById("StartHour[0][0]"));
	}	
}
</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
