<?php
/*
 *  Using:
 *  
 *  2018-12-11 Cameron
 *      - reduce space from top margin
 *      - enlarge print font from 12px to 16px
 *      - print sub-heading (not stored in db)
 *  
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-09-06 Cameron
 *      - retrieve weekly schedule for all classes if it's empty
 *      - fix: add css for grid layout
 *   
 *  2018-08-30 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();

if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$linterface = new interface_html("popup.html");
$libenroll_report = new libclubsenrol_report();

$CurrentPageArr['eEnrolment'] = 1;

$weekStartTimeStamp = $_POST['WeekStartTimeStamp'];
$startDate = date('Y-m-d', $weekStartTimeStamp);
$intakeID = IntegerSafe($_POST['IntakeID']);
$classID = IntegerSafe($_POST['IntakeClassID']);


$intakeTitle = '';
$className = '';
$scheduleAry = array();

if ($intakeID) {
    $intakeInfoAry = $libenroll->getIntakeList($intakeID);
    if (count($intakeInfoAry)){
        $intakeTitle = $intakeInfoAry[0]['Title'];
    }
    
    if ($classID == '') {   // all classes
        $classList = $libenroll->getIntakeClass($intakeID);
        
        foreach ((array)$classList as $_class) {
            $_classID = $_class['ClassID'];
            $_className = $_class['ClassName'];
            $_scheduleTable = $libenroll_report->getClassWeeklyScheduleTable($intakeID, $_classID, $startDate, $fromPrint=1);
            
            $_reportTitle = sprintf($Lang['eEnrolment']['report']['classWeeklySchedule']['title'], $intakeTitle, $_className, $startDate);
            $scheduleAry[] = array('ReportTitle'=>$_reportTitle, 'ReportContent'=>$_scheduleTable);
        }
    }
    else {
        $scheduleTable = $libenroll_report->getClassWeeklyScheduleTable($intakeID, $classID, $startDate, $fromPrint=1);
        
        $classInfoAry = $libenroll->getIntakeClass($intakeID, $classID);
        if (count($classInfoAry)) {
            $className = $classInfoAry[$classID]['ClassName'];
        }
        $reportTitle = sprintf($Lang['eEnrolment']['report']['classWeeklySchedule']['title'], $intakeTitle, $className, $startDate);
        $scheduleAry[] = array('ReportTitle'=>$reportTitle, 'ReportContent'=>$scheduleTable);
    }
}

$schoolNameInReport = $Lang['schoolNameInReport'];

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

?>

<link rel="stylesheet" href="<?php echo $PATH_WRT_ROOT;?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $PATH_WRT_ROOT;?>templates/jquery/jquery.datepick.css" type="text/css" />
<script type="text/javascript" src= "<?php echo $PATH_WRT_ROOT;?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?php echo $PATH_WRT_ROOT;?>templates/jquery/jquery.datepick.js"></script>

<style type="text/css">
    td.empty {
        padding: 0;
        background-color:#FFFFFF; 
        border-bottom: 1px solid #BCBCBC;
        border-right: 1px dotted #BCBCBC;
    }
    td.eventDate {
        background-color:#d7ece9; 
        border-bottom: 1px solid #BCBCBC; 
        border-right: 1px solid #BCBCBC; 
        border-left: 1px solid #BCBCBC;
    }
    td.eventDateItem {
        background-color:#FFFFFF; 
        text-align:left;
    }
	.lessonBorder_normal {
	    color: #000000;
	    border-top: 1px solid #4db308;
	    border-bottom: 1px solid #4db308;
	    border-right: 1px solid #4db308;
	    border-left: 1px solid #4db308;
	}
	.lessonBorder_conflict {
	    color: #2f75e9;
	    border-top: 1px solid #2f75e9;
	    border-bottom: 1px solid #2f75e9;
	    border-right: 1px solid #2f75e9;
	    border-left: 1px solid #2f75e9;
	}
	td {
	    font-size: 16px;
	}	
</style>

<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print" style="text-decoration: none; padding-top:0px;">
			<?php echo $schoolNameInReport;?>
		</td>
	</tr>
</table>


<?php foreach((array)$scheduleAry as $schedule):?>
<div style="page-break-inside: avoid;">
<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print" style="text-decoration: none">
			<?php echo $schedule['ReportTitle'];?>
		</td>
	</tr>
	<tr>
		<td align="center" class="page_title_print" style="text-decoration: none">
			<?php echo $_POST['SubHeading'];?>
		</td>
	</tr>	
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?php echo $schedule['ReportContent'];?>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>
</div>
<?php endforeach;?>

<?php

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
 
intranet_closedb();
?>
