<?php
/*
 *  using:
 *  
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-09-06 Cameron
 *      - retrieve weekly schedule for all classes if it's empty
 *        
 *  2018-08-31 Cameron
 *      - create this file
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();

if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$weekStartTimeStamp = $_POST['WeekStartTimeStamp'];
$startDate = date('Y-m-d', $weekStartTimeStamp);
$intakeID = IntegerSafe($_POST['IntakeID']);
$classID = IntegerSafe($_POST['IntakeClassID']);

//debug_pr($scheduleAry);
$intakeTitle = '';
$className = '';
$scheduleAry = array();
if ($intakeID) {
    $intakeInfoAry = $libenroll->getIntakeList($intakeID);
    if (count($intakeInfoAry)){
        $intakeTitle = $intakeInfoAry[0]['Title'];
    }
    
    if ($classID == '') {   // all classes
        $classList = $libenroll->getIntakeClass($intakeID);
        
        foreach ((array)$classList as $_class) {
            $_classID = $_class['ClassID'];
            $_className = $_class['ClassName'];
            $_scheduleContent = $libenroll_report->getClassWeeklyScheduleData($intakeID, $_classID, $startDate);
            
            $_reportTitle = sprintf($Lang['eEnrolment']['report']['classWeeklySchedule']['title'], $intakeTitle, $_className, $startDate);
            $scheduleAry[] = array('ReportTitle'=>$_reportTitle, 'ReportContent'=>$_scheduleContent);
        }
    }
    else {
        $classInfoAry = $libenroll->getIntakeClass($intakeID, $classID);
        if (count($classInfoAry)) {
            $className = $classInfoAry[$classID]['ClassName'];
        }
        
        $reportTitle = sprintf($Lang['eEnrolment']['report']['classWeeklySchedule']['title'], $intakeTitle, $className, $startDate);
        $scheduleContent = $libenroll_report->getClassWeeklyScheduleData($intakeID, $classID, $startDate);
        $scheduleAry[] = array('ReportTitle'=>$reportTitle, 'ReportContent'=>$scheduleContent);
    }
    
}

$schoolNameInReport = $Lang['schoolNameInReport'];

$filename = "class_weekly_schedule_report.csv";
    
$finalAry = array();
$finalAry[0][0] = $schoolNameInReport;
$finalAry[1][0] = '';    // empty row

$noOfClass = 0;
$headerRowStart = 0;
foreach((array) $scheduleAry as $_scheduleAry) {
    
    $headerAry = array();
    
    if ($noOfClass > 0) {
        $headerAry[$headerRowStart++][0] = '';    // empty row
    }
    $headerAry[$headerRowStart][0] = $_scheduleAry['ReportTitle'];
    $headerAry[++$headerRowStart][0] = '';    // empty row
    
    $headerAry[++$headerRowStart][] = $Lang['General']['Date'];
    $headerAry[$headerRowStart][] = $Lang['General']['Time'];
    $headerAry[$headerRowStart][] = $Lang['eEnrolment']['report']['eventOrLesson'];
    $headerAry[$headerRowStart][] = $Lang['eEnrolment']['report']['staff'];
    $headerAry[$headerRowStart][] = $Lang['eEnrolment']['report']['location'];
    
    $headerAry[++$headerRowStart][0] = '';    // empty row
    
    $dataAry = array();    
    $row = 0;
    
    for ($i=0; $i<7; $i++) {
        $_startTimeStamp = strtotime($startDate);
        $_thisDateTimeStamp = strtotime('+'.$i.' day',$_startTimeStamp);
        $_thisDate = date('Y-m-d',$_thisDateTimeStamp);
        $_weekDay = date('w',$_thisDateTimeStamp);
        $_disWeekDay = ($_weekDay>=0 && $_weekDay <=6) ? $Lang['eEnrolment']['report']['weekday'][$_weekDay] : '';
        
        $__scheduleAry = array();
        foreach((array)$_scheduleAry['ReportContent'][$_thisDate] as $__eventDateID=>$__schedule) {
            $__activityDateStart = $__schedule['StartTime'];
            $__scheduleAry[$__activityDateStart][$__eventDateID] = $__schedule;
        }
        
        ksort($__scheduleAry);
        $j = 0;
        
        foreach((array)$__scheduleAry as $__thisScheduleAry) {
            $k = 0;
            foreach((array)$__thisScheduleAry as $___thisScheduleAry) {
                $dataAry[$row][] = ($j == 0 && $k == 0) ? $_thisDate. ' ('.$_disWeekDay.')': '';
                $dataAry[$row][] = $___thisScheduleAry['TimeSlot'];
                $dataAry[$row][] = $___thisScheduleAry['Lesson'];
                $dataAry[$row][] = $___thisScheduleAry['Instructor'];
                $dataAry[$row][] = $___thisScheduleAry['Location'];
                $row++;
                $k++;
            }
            $j++;
        }
    }    

    $finalAry = array_merge($finalAry, $headerAry, $dataAry);
    $noOfClass++;
}

$export_text = $lexport->GET_EXPORT_TXT($finalAry, $headerAry="", $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);


intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>