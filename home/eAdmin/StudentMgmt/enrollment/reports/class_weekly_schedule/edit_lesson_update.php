<?php
/*
 *  Using:
 *  
 *  2019-02-22 [Cameron] add parameter ENROL_TYPE_ACTIVITY to UpdateEnrolEbookingRelation()
 *   
 *  2018-10-23 [Cameron] support select or text input for LessonTitle
 *    
 * 	2018-10-18 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5';
    }
}
else {
    $characterset = 'utf-8';
}
header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();
$json['success'] = false;

$result = array();
$dataAry = array();
$condAry = array();

$enrolEventID = IntegerSafe($_POST['Course']);          // EnrolEventID
$eventDateID = IntegerSafe($_POST['EventDateID']);      // Lesson ID
$startHourAry = $_POST['StartHour'];
$startMinuteAry = $_POST['StartMinute'];
$endHourAry = $_POST['EndHour'];
$endMinuteAry = $_POST['EndMinute'];
$instructorAry = $_POST['Instructor'];
$locationAry = $_POST['Location'];
$lessonClassAry = $_POST['LessonClass'];

$oldEventDateAry = $libenroll->GET_ENROL_EVENT_DATE($ParEnrolEventID='', $SQL_startdate='', $SQL_enddate='', $eventDateID);

$libenroll->Start_Trans();
    
    // Step 1: update lesson in INTRANET_ENROL_EVENT_DATE
    $lessonTitle = $_POST['LessonInputBy'] ? standardizeFormPostValue($_POST['LessonTitleSelect']) : standardizeFormPostValue($_POST['LessonTitle']);
    $lessonDate = $_POST['LessonDate_0_0'];
    $startHour = str_pad(IntegerSafe($startHourAry[0][0]), 2, "0", STR_PAD_LEFT);
    $startMinute = str_pad(IntegerSafe($startMinuteAry[0][0]), 2, "0", STR_PAD_LEFT);
    $activityDateStart = $lessonDate.' '.$startHour.':'.$startMinute.':00';
    $endHour = str_pad(IntegerSafe($endHourAry[0][0]), 2, "0", STR_PAD_LEFT);
    $endMinute = str_pad(IntegerSafe($endMinuteAry[0][0]), 2, "0", STR_PAD_LEFT);
    $activityDateEnd = $lessonDate.' '.$endHour.':'.$endMinute.':00';
    $locationID = $locationAry[0][0];
    
    unset($dataAry);
    $dataAry['EnrolEventID'] = $enrolEventID;
    $dataAry['ActivityDateStart'] = $activityDateStart;
    $dataAry['ActivityDateEnd'] = $activityDateEnd;
    $dataAry['LocationID'] = $locationID;     // store locationID instead of location name for HKPF
    $dataAry['Lesson'] = $lessonTitle;
    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
    
    unset($condAry);
    $condAry['EventDateID'] = $eventDateID;
    $sql = $libenroll->UPDATE2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,$condAry,false);
    $result["UpdateEventDate"] = $libenroll->db_db_query($sql);
        
    // Step 2. delete event date trainer
    $sql = "DELETE FROM INTRANET_ENROL_EVENT_DATE_TRAINER WHERE EventDateID='".$eventDateID."'";
    $result["DeleteEventDateTrainer_{$eventDateID}"] = $libenroll->db_db_query($sql);
    
    
    $instructorAry = array_unique((array)$instructorAry[0][0]);      // remove duplicate instructor
    
    // Step 3: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
    foreach((array)$instructorAry as $__instructor) {
        if ($__instructor) {       // filter out empty record
            unset($dataAry);
            $dataAry['EventDateID'] = $eventDateID;
            $dataAry['TrainerID'] = $__instructor;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
            $result["AddTrainer_{$__instructor}"] = $libenroll->db_db_query($sql);
        }
    }
    
    // Step 4. delete lesson-class
    $sql = "DELETE FROM INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS WHERE EventDateID='".$eventDateID."'";
    $result["DeleteEventDateIntakeClass_{$eventDateID}"] = $libenroll->db_db_query($sql);
    
    // Step 5: add record to lesson-class
    foreach((array)$lessonClassAry[0] as $__intakeClassID => $__checkedVal) {
        unset($dataAry);
        $dataAry['EventDateID'] = $eventDateID;
        $dataAry['IntakeClassID'] = $__intakeClassID;
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, false, false);
        $result["AddNewLessonClass_{$eventDateID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
        
        // Step 6: add course-class
        unset($dataAry);
        $dataAry['EnrolEventID'] = $enrolEventID;
        $dataAry['IntakeClassID'] = $__intakeClassID;
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
        $result["AddNewCourseClass_{$enrolEventID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
    }
    
    
    // Step 7: check if booking exist
    $enrolBookingRelationAry = $libenroll->GetEnrolEbookingRelation($eventDateID, ENROL_TYPE_ACTIVITY);
    $startTime = $startHour.':'.$startMinute.':00';
    $endTime = $endHour.':'.$endMinute.':00';
    //debug_pr($enrolBookingRelationAry);
    
    // don't process booking if start time < end time or location is not selected
    if (($endTime >= $startTime) && $locationID) {
        // Step 8: add / update booking records:
        if (count($enrolBookingRelationAry)) {  // booking record exist
            $bookingID= $enrolBookingRelationAry['BookingRecordID'];
            
            unset($dataAry);
            $dataAry['Date'] = $lessonDate;
            $dataAry['StartTime'] = $startTime;
            $dataAry['EndTime'] = $endTime;
            $dataAry['ModifiedBy'] = $_SESSION['UserID'];
            $condAry = array('BookingID'=>$bookingID);
            $sql = $libenroll->UPDATE2TABLE('INTRANET_EBOOKING_RECORD',$dataAry,$condAry,false);
            $result["UpdateEBookingRecord_{$bookingID}"] = $libenroll->db_db_query($sql);
            
            
            unset($dataAry);
            $dataAry['FacilityID'] = $locationID;
            $dataAry['ModifiedBy'] = $_SESSION['UserID'];
            $condAry = array('BookingID'=>$bookingID);
            $sql = $libenroll->UPDATE2TABLE('INTRANET_EBOOKING_BOOKING_DETAILS',$dataAry,$condAry,false);
            $result["UpdateEBookingDetails_{$bookingID}"] = $libenroll->db_db_query($sql);
            
        }
        else {      // new booking
            $requestAry = array();
            $bookingAry = array();
            $bookingAry['BookingType'] = 'room';
            $bookingAry['Date'] = $lessonDate;
            $bookingAry['StartTime'] = $startTime;
            $bookingAry['EndTime'] = $endTime;
            $bookingAry['RoomID'] = $locationID;
            $requestAry['BookingDetails'][0] = $bookingAry;
            
            // Step 9: add booking record
            if (($endTime > $startTime) && $locationID) {
                $requestAry['BookingFrom'] = 'eEnrol';
                $libebooking_api = new libebooking_api();
                $bookingResultAry = $libebooking_api->RequestBooking($requestAry);
                //debug_pr($bookingResultAry);
                if (count($bookingResultAry)) {
                    $bookingResult = array_pop($bookingResultAry);  // one element
                    $result["AddBooking_{$eventDateID}"] = $bookingResult['Status'];
                    $bookingID = $bookingResult['BookingID'];
                    $result["AddBookingRelation_{$eventDateID}"] = $libenroll->UpdateEnrolEbookingRelation($eventDateID, $bookingID, ENROL_TYPE_ACTIVITY);
                    
                }
            }
        }
    }

    
    if (count($oldEventDateAry)) {
        $oldEnrolEventID = $oldEventDateAry[0]['EnrolEventID'];
        if ($oldEnrolEventID != $enrolEventID) {
            $intakeClassIDToRemoveAry = $libenroll->getIntakeClassIDToRemove($oldEnrolEventID);
            if (count($intakeClassIDToRemoveAry)) {
                $sql = "DELETE FROM INTRANET_ENROL_EVENT_INTAKE_CLASS WHERE EnrolEventID='".$oldEnrolEventID."' AND IntakeClassID IN ('".implode("','",$intakeClassIDToRemoveAry)."')";
                $result["DeleteCourseClass_{$oldEnrolEventID}"] = $libenroll->db_db_query($sql);
            }
        }
    }
    
    $intakeClassIDToRemoveAry = $libenroll->getIntakeClassIDToRemove($enrolEventID);
    if (count($intakeClassIDToRemoveAry)) {
        $sql = "DELETE FROM INTRANET_ENROL_EVENT_INTAKE_CLASS WHERE EnrolEventID='".$enrolEventID."' AND IntakeClassID IN ('".implode("','",$intakeClassIDToRemoveAry)."')";
        $result["DeleteCourseClass_{$enrolEventID}"] = $libenroll->db_db_query($sql);
    }
    
    
if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $json['updateResult']= 'UpdateSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $json['updateResult']= 'UpdateUnsuccess';
}

echo $ljson->encode($json);


intranet_closedb();
?>