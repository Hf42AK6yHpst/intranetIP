<?php
/*
 *  Using:
 *  
 *  2019-02-22 [Cameron] add parameter ENROL_TYPE_ACTIVITY to UpdateEnrolEbookingRelation()
 *   
 *  2018-10-23 [Cameron] support select or text input for LessonTitle
 *  
 *  2018-10-18 [Cameron] add intake-class and course-class
 *  
 * 	2018-09-14 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5';
    }
}
else {
    $characterset = 'utf-8';
}
header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();
$json['success'] = false;

$result = array();
$dataAry = array();
 
$enrolEventID = IntegerSafe($_POST['Course']);      // EnrolEventID
$startHourAry = $_POST['StartHour'];
$startMinuteAry = $_POST['StartMinute'];
$endHourAry = $_POST['EndHour'];
$endMinuteAry = $_POST['EndMinute'];
$instructorAry = $_POST['Instructor'];
$locationAry = $_POST['Location'];
$lessonClassAry = $_POST['LessonClass'];

$libenroll->Start_Trans();
    
    // Step 1: Add new lesson to INTRANET_ENROL_EVENT_DATE
    $lessonTitle = $_POST['LessonInputBy'] ? standardizeFormPostValue($_POST['LessonTitleSelect']) : standardizeFormPostValue($_POST['LessonTitle']);
    $lessonDate = $_POST['LessonDate_0_0'];
    $startHour = str_pad(IntegerSafe($startHourAry[0][0]), 2, "0", STR_PAD_LEFT);
    $startMinute = str_pad(IntegerSafe($startMinuteAry[0][0]), 2, "0", STR_PAD_LEFT);
    $activityDateStart = $lessonDate.' '.$startHour.':'.$startMinute.':00';
    $endHour = str_pad(IntegerSafe($endHourAry[0][0]), 2, "0", STR_PAD_LEFT);
    $endMinute = str_pad(IntegerSafe($endMinuteAry[0][0]), 2, "0", STR_PAD_LEFT);
    $activityDateEnd = $lessonDate.' '.$endHour.':'.$endMinute.':00';
    $locationID = $locationAry[0][0];
    
    unset($dataAry);
    $dataAry['EnrolEventID'] = $enrolEventID;
    $dataAry['ActivityDateStart'] = $activityDateStart;
    $dataAry['ActivityDateEnd'] = $activityDateEnd;
    $dataAry['LocationID'] = $locationID;     // store locationID instead of location name for HKPF
    $dataAry['Lesson'] = $lessonTitle;
    $dataAry['RecordStatus'] = $enrolConfigAry['MeetingDate_RecordStatus']['Active'];
    $dataAry['DateInput'] = 'now()';
    $dataAry['InputBy'] = $_SESSION['UserID'];
    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
    $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,array(),false);
    $res = $libenroll->db_db_query($sql);
    $result["AddEventDate"] = $res;
    if ($res) {
        $eventDateID = $libenroll->db_insert_id();
        
        $instructorAry = array_unique((array)$instructorAry[0][0]);      // remove duplicate instructor
        
        // Step 2: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
        foreach((array)$instructorAry as $__instructor) {
            if ($__instructor) {       // filter out empty record
                unset($dataAry);
                $dataAry['EventDateID'] = $eventDateID;
                $dataAry['TrainerID'] = $__instructor;
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
                $result["AddTrainer_{$__instructor}"] = $libenroll->db_db_query($sql);
            }
        }
        
        // Step 3: add record to lesson-class
        foreach((array)$lessonClassAry[0] as $__intakeClassID => $__checkedVal) {
            unset($dataAry);
            $dataAry['EventDateID'] = $eventDateID;
            $dataAry['IntakeClassID'] = $__intakeClassID;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, false, false);
            $result["AddLessonClass_{$eventDateID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
            
            // Step 4: add course-class
            unset($dataAry);
            $dataAry['EnrolEventID'] = $enrolEventID;
            $dataAry['IntakeClassID'] = $__intakeClassID;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
            $result["AddCourseClass_{$enrolEventID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
        }
        
        // Step 5: prepare adding booking records:
        $requestAry = array();
        $bookingAry = array();
        $startTime = $startHour.':'.$startMinute.':00';
        $endTime = $endHour.':'.$endMinute.':00';
        $bookingAry['BookingType'] = 'room';
        $bookingAry['Date'] = $lessonDate;
        $bookingAry['StartTime'] = $startTime;
        $bookingAry['EndTime'] = $endTime;
        $bookingAry['RoomID'] = $locationID;
        $requestAry['BookingDetails'][0] = $bookingAry;
        
        if (($endTime > $startTime) && $locationID) {
            // Step 6: add booking record
            $requestAry['BookingFrom'] = 'eEnrol';
            $libebooking_api = new libebooking_api();
            $bookingResultAry = $libebooking_api->RequestBooking($requestAry);
            if (count($bookingResultAry)) {
                $bookingResult = array_pop($bookingResultAry);  // one element
                $result["AddBooking"] = $bookingResult['Status'];
                $bookingID = $bookingResult['BookingID'];
                $result["AddBookingRelation"] = $libenroll->UpdateEnrolEbookingRelation($eventDateID, $bookingID, ENROL_TYPE_ACTIVITY);
            }
        }
    }
    
    
if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $json['updateResult']= 'AddSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $json['updateResult']= 'AddUnsuccess';
}

echo $ljson->encode($json);


intranet_closedb();
?>