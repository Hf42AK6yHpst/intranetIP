<?php
## Modifying By: anna

#######################################
#  Date:   2019-06-10 Anna
#          delete $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'], for general school
#
#   Date: 2018-11-01 Anna
#       - ADDED $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC']
#   Date: 2018-06-21    Philips
#       - Add Student Selection
#
#	Date: 2016-10-29	Kenneth
#		- New Created
#
#######################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isEnrolPIC = $libenroll->IS_CLUB_PIC() ||  $libenroll->IS_EVENT_PIC();

// if ($isEnrolAdmin || ($isEnrolPIC && $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'])){
if ($isEnrolAdmin || $isEnrolPIC ){
    
}else{
    No_Access_Right_Pop_Up();
}
//     $libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageStudentECASchedule";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['StudentECASchedule']['ReportName'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();	



// Option Div after result table created
$divName = 'reportOptionOuterDiv';
$tdId = 'tdOption';
$x = '';
$x .= '<div id="showHideOptionDiv" style="display:none;">'."\n";
	$x .= '<table style="width:100%;">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td id="tdOption" class="report_show_option">'."\n";
				$x .= '<span id="spanShowOption_'.$divName.'">'."\n";
					$x .= $linterface->Get_Show_Option_Link("javascript:showOption();", '', '', 'spanShowOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<span id="spanHideOption_'.$divName.'" style="display:none">'."\n";
					$x .= $linterface->Get_Hide_Option_Link("javascript:hideOption();", '', '', 'spanHideOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<div id="'.$divName.'" style="display:none;">'."\n";
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionOuterDiv'] = $x;

// Option Div
$divName = 'reportOptionDiv';
$x = '';
$x .= '<div id="'.$divName.'">'."\n";
	$x .= '<div>'."\n";
		$x .= '<table id="filterOptionTable" class="form_table_v30">'."\n";
			
			# Period
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['Period'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<table class="inside_form_table">'."\n";
						$x.='<tr>';
							$today = date('Y-m-d');
							$aWeekLater = date('Y-m-d',strtotime($today . "+6 days"));
							$x.= '<td>'.$Lang['General']['From'].'</td>';
							$x.='<td >'.$linterface->GET_DATE_PICKER('fromDate',$today).'</td>';
							$x.= '<td>'.$Lang['General']['To'].'</td>';
							$x.='<td>'.$linterface->GET_DATE_PICKER('toDate',$aWeekLater).'</td>';
						$x.='</tr>';
					$x .= '</table>'."\n";
					
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Display Type
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['StudentECASchedule']['DisplayType'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x.= $linterface->Get_Radio_Button('displayType_row','displayType','row',1, $Class="", $Display=$Lang['eEnrolment']['StudentECASchedule']['Daily'], $Onclick="",$isDisabled=0);
					$x.= $linterface->Get_Radio_Button('displayType_summary','displayType','summary',0, $Class="", $Display=$Lang['eEnrolment']['StudentECASchedule']['Summary'], $Onclick="",$isDisabled=0);
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";			
			
			# Class
			$libclass = new libclass();
			$studentSource_classSel = $libclass->getSelectClassID('id="classSel" name="classIdAry[]" multiple="multiple" size="10"  onchange="changedStudentSourceSelection(\'formClass\');"', $selected="", $DisplaySelect=2);
			$studentSource_classSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('classSel', 1, ' onchange="."changedStudentSourceSelection(\'formClass\');"."');");
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Class'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($studentSource_classSel, $studentSource_classSelectAllBtn);
					//$x .= $linterface->Get_Form_Warning_Msg('studentSource_formClassWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectClass'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			# Student
			$studentSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('studentSel', 1);");
			$x .= '<tr id="studentTr">'."\n";
			 $x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['student'].'</td>'."\n";
			 $x .= '<td>'."\n";
			     $x .= $linterface->Get_MultipleSelection_And_SelectAll_Div('', $studentSelectAllBtn, 'studentSelectionSpan');
			     $x .= $linterface->Get_Form_Warning_Msg('studentSelectionWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectClass'], $Class='warnMsgDiv')."\n";
			 $x .= '</td>'."\n";
			$x .= '</tr>'."\n";

			# Data Source
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['StudentECASchedule']['DataSource'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x.= $linterface->Get_Checkbox('checkbox_club', 'checkbox_club', 'club', $isChecked=1, $Class='', $Display=$eEnrollment['club'], $Onclick='', $Disabled='');
					$x.= $linterface->Get_Checkbox('checkbox_activity', 'checkbox_activity', 'activity', $isChecked=1, $Class='', $Display=	$eEnrollment['activity'], $Onclick='', $Disabled='');
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";	
			
			
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .= '<br style="clear:both;" />'."\n";
	
	$x .= '<div style="text-align:left;">'."\n";
		$x .= $linterface->MandatoryField();
	$x .= '</div>'."\n";
	$x .= '<br style="clear:both;" />'."\n";
	
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionDiv'] = $x;

// Tool bar
$x = '';
$x .= '<div id="contentToolDiv" class="content_top_tool" style="display:none;">'."\n";
	$x .= '<div class="Conntent_tool">'."\n";
		$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
		$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['contentToolDiv'] = $x;

// Result Div
$htmlAry['reportResultDiv'] = '<div id="reportResultDiv"></div>';

$linterface->LAYOUT_START();
?>
<script type="text/javascript">
$(document).ready(function () {
	js_Select_All('classSel', 1, '');
	changedStudentSourceSelection('formClass');
});

function checkForm(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var displayType = $('input[name="displayType"]:checked').val();
	var classArr = [];
	$('#classSel :selected').each(function(i, selected){ 
	  classArr[i] = $(selected).val(); 
	});
	var isClub = $('#checkbox_club').is(":checked");
	var isActivity = $('#checkbox_activity').is(":checked");

	
	//check Period
	if(fromDate==''||toDate==''){
		alert("Please select time Period");
		return false;
	}
	
	if((new Date(fromDate).getTime()> new Date(toDate).getTime())){
		alert("From Date must not greater than To Date");
		return false;
	}
	
	//check display type
	if(displayType==''){
		alert("Please select the display type");
		return false;
	}
	
	//check class
	if(classArr==''){
		alert("Please select at least one class");
		return false;
	}
	
	//check Data Source
	if(!(isClub||isActivity)){
		alert("Please select at least one data source");
		return false;
	}
	return true;
}

function changedStudentSourceSelection(targetSource) {
	var selectionId = getSelectionIdByStudentSource(targetSource);
	
	var selectedIdAry = new Array();
	$('select#' + selectionId + ' option:selected').each( function() {
		selectedIdAry[selectedIdAry.length] = $(this).val();
	});
	var selectedIdList = selectedIdAry.join(',');
	
	$('span#studentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'studentSelection',
			SourceType: targetSource,
			SelectedIdList: selectedIdList,
			studentSelId: 'studentSel',
			studentSelName: 'studentIdAry[]'
		},
		function(ReturnData) {
			js_Select_All('studentSel', 1);
		}
	);
}

function getSelectionIdByStudentSource(targetSource) {
	var selectionId = '';
	if (targetSource == 'formClass') {
		selectionId = 'classSel';
	}
	
	return selectionId;
}


function refreshReport() {
	
	var isValid = checkForm();

	if(isValid){
		$('div.warnMsgDiv').hide();
		Block_Document();
		if (Trim($('div#reportOptionOuterDiv').html()) == '') {
			hideOption();
			$('div#showHideOptionDiv').show();
			$('div#contentToolDiv').show();
		}else{
			$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
		}
		$('div#reportResultDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$.ajax({
			url:      	"ajax_reload.php",
			type:     	"POST",
			data:     	$("#form1").serialize() + '&RecordType=reportResultIndividual',
			success:  	function(xml) {
							$('div#reportResultDiv').html(xml);
							Scroll_To_Top();
							UnBlock_Document();
			}
		});
	}
}
function showOption(){
	$('div#reportOptionDiv').show();
	$('#spanShowOption_reportOptionOuterDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').show();
	$('#tdOption').removeClass( "report_show_option" );
}
function hideOption(){
	$('div#reportOptionDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').hide();
	$('#spanShowOption_reportOptionOuterDiv').show();
	$('#tdOption').addClass( "report_show_option" );
}
function goExport() {
	$('form#form2').attr('target', '_self').attr('action', 'export.php').submit();
}

function goPrint() {
	$('form#form2').attr('target', '_blank').attr('action', 'print.php').submit();
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['reportOptionOuterDiv']?>
	<br />
	
	<?=$htmlAry['reportOptionDiv']?>
	<?=$htmlAry['contentToolDiv']?>
	<?=$htmlAry['reportResultDiv']?>
	<br />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>