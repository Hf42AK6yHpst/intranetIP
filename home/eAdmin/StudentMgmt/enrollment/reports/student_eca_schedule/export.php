<?php
# using: 
#  Date:   2019-06-10 Anna
#          delete $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'], for general school
#
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();

// $libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isEnrolPIC = $libenroll->IS_CLUB_PIC() ||  $libenroll->IS_EVENT_PIC();

// if ($isEnrolAdmin || ($isEnrolPIC && $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'])){
if ($isEnrolAdmin || $isEnrolPIC ){
    
}else{
    No_Access_Right_Pop_Up();
}

$enrolmentSourceAry = $_POST['enrolmentSourceAry'];
$clubTermIdAry = $_POST['clubTermIdAry'];

$headerAry = $_POST['headerArr'];
$dataAry = $_POST['contentArr'];
$seperator = $_POST['seperator'];

foreach($headerArr as &$header){
	$header = standardizeFormPostValue($header);
}
foreach($dataAry as &$contentRow){
	foreach($contentRow as &$data){
		$data = standardizeFormPostValue($data);
	}
}

/**
 * Turn <br/> to /n
 */
$numOfRow = count($dataAry);
for($i=0;$i<$numOfRow;$i++){
	$numOfColumn = count($dataAry[$i]);
	for($j=0;$j<$numOfColumn;$j++){
		$dataAry[$i][$j]=str_replace($seperator,"\n",$dataAry[$i][$j]);
	}
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "student_eca_schedule.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>