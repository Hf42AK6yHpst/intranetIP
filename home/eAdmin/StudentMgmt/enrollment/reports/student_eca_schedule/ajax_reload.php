<?php
// using: 
#######################################
#  Date:   2019-06-10 Anna
#          delete $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'], for general school
#
#   Date: 2018-11-01 Anna
#       - ADDED $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC']
#
#   Date: 2018-06-21    Philips
#       - added recordType == reportResultIndividual
#
#	Date: 2016-05-23	Kenneth
#		- modified insertIntoWeekdayContentArr(), fix error by php 5.4
#
#	Date: 2015-10-29	Kenneth
#		- New Created
#
#######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
// $libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isEnrolPIC = $libenroll->IS_CLUB_PIC() ||  $libenroll->IS_EVENT_PIC();

// if ($isEnrolAdmin || ($isEnrolPIC && $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'])){
if ($isEnrolAdmin || $isEnrolPIC ){
        
}else{
    No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$libenroll_report = new libclubsenrol_report();

$libclass = new libclass();
$libuser =new libuser();

$RecordType = $_POST['RecordType'];
$classIdAry = $_POST['classIdAry'];
$fromDate = $_POST['fromDate'];
$toDate = $_POST['toDate'];
$displayType = $_POST['displayType'];
$isClub = ($_POST['checkbox_club']!='')?1:0;
$isActivity = ($_POST['checkbox_activity']!='')?1:0;

/**
 * Global var for displayType = summary
 */
$isWeekdays['Sun']=false;
$isWeekdays['Mon']=false;
$isWeekdays['Tue']=false;
$isWeekdays['Wed']=false;
$isWeekdays['Thu']=false;
$isWeekdays['Fri']=false;
$isWeekdays['Sat']=false;

$weekdaysContentArr['Sun']='';
$weekdaysContentArr['Mon']='';
$weekdaysContentArr['Tue']='';
$weekdaysContentArr['Wed']='';
$weekdaysContentArr['Thu']='';
$weekdaysContentArr['Fri']='';
$weekdaysContentArr['Sat']='';

/**
 * $seperator definded the way to seperate 2 activity title in single box
 */
$seperator = "<br />";

//$seperator= intranet_undo_htmlspecialchars($seperator);


$datesArr = createDateRangeArray($fromDate,$toDate);
$HeaderArr = getHeader($displayType);
//debug_pr($HeaderArr);

$contentArr = '';
if ($RecordType == 'reportResultIndividual'){
    $_userIDArr = $studentIdAry;
    if($isClub){
        $returnGroupArr = $libenroll->getEnrolGroupInfoByActivityDate($fromDate,$toDate,$_userIDArr);
    }
    if($isActivity){
        $returnActivityArr = $libenroll->getEnrolEvnetInfoByActivityDate($fromDate,$toDate,$_userIDArr);
    }
    $mergedArr = array_merge((array)$returnGroupArr,(array)$returnActivityArr);
    $resultAssoAry = BuildMultiKeyAssoc($mergedArr, array('UserID', 'StartDate'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
    $_userObj = new libuser('', '', $_userIDArr);
    //debug_pr($resultAssoAry);
    $numOfStudentId=count($_userIDArr);
    for($j=0;$j<$numOfStudentId;$j++){ //For Each Students
        $weekdaysContentArr=''; //clear $weekdaysContentArr , reset every student
        $__columnContentArr = '';
        $__userID = $_userIDArr[$j];
        //debug_pr($__userID);
        $_userObj->LoadUserData($__userID);
        
        $__classNum = $_userObj->ClassNumber;
        $_className = $_userObj->ClassName;
        $__chineseName = $_userObj->ChineseName;
        $__englishName = $_userObj->EnglishName;
        
        $__columnContentArr[]=$_className;
        $__columnContentArr[]=$__classNum;
        $__columnContentArr[]=$__englishName;
        $__columnContentArr[]=$__chineseName;
        
        $numOfDataSet = count($datesArr);
        for($k=0;$k<$numOfDataSet;$k++){	//For Each Dates
            $___date = $datesArr[$k];
            //debug_pr($___date);
            $activityArrInThisDate = (array)$resultAssoAry[$__userID][$___date];
            sortByColumn2($activityArrInThisDate, 'ActivityDateStart');
            $___activityTitleArr='';
            $TimeAry = '';
            $ShowAry = '';
            $numOfActivityInThisDate =count($activityArrInThisDate);
            for($l=0;$l<$numOfActivityInThisDate;$l++){	//Decompose array to single line
                $____activityTitle = $activityArrInThisDate[$l]['Title'];
                $___activityTitleArr[]=$____activityTitle;
                // 					debug_pr($activityArrInThisDate);
                //get activity start/end date, location
                $ActivityDateStart = $activityArrInThisDate[$l]['ActivityDateStart'];
                $StartTime = substr($ActivityDateStart,10,-3);
                $ActivityDateEnd = $activityArrInThisDate[$l]['ActivityDateEnd'];
                $EndTime = substr($ActivityDateEnd,10,-3);
                $StartEndTime = $StartTime.' -'.$EndTime;
                $Location = $activityArrInThisDate[$l]['Location']== null? '':'('.$activityArrInThisDate[$l]['Location'].')';
                
                $ShowAry[] = $StartEndTime.$seperator.$____activityTitle.$seperator.$Location.$seperator;
            }
            //		$activityTitleInSingleLine = implode($seperator,(array)$___activityTitleArr);
            $activityTitleInSingleLine = implode($seperator,(array)$ShowAry);
            
            if($displayType=='row'){
                //	$__columnContentArr[]=$activityTitleInSingleLine;
                $__columnContentArr[]=$activityTitleInSingleLine;
            }else if($displayType=='summary'){
                insertIntoWeekdayContentArr((array)$ShowAry,$___date);
            }
        }
        if($displayType=='row'){
            $contentArr[] = $__columnContentArr;
            
        }else if($displayType=='summary'){
            writeToWeekdayContentRow($__columnContentArr);
        }
    }
    
    /**
     * Set Table width
     */
    $numOfHeader = count($HeaderArr);
    /**
     * total width equals to 100%-3% (3% is for '#' column)
     */
    $widthForFirstFourColumn = 8;
    for($i=0;$i<$numOfHeader;$i++){
        if($i<4){
            $widthArr[] = $widthForFirstFourColumn.'%';
        }
        else{
            $widthArr[] = (100-3-4*$widthForFirstFourColumn)/($numOfHeader-4).'%';
        }
    }
    
    
    /**
     * Form for export
     */
    $html['formForExport'] = getHiddenFormForExport($HeaderArr,$contentArr,$widthArr);
    echo $libenroll_report->getGeneralEnrolmentReportTableHtml($HeaderArr,$contentArr,$widthArr).$html['formForExport'];
    
}

if ($RecordType == 'reportResult') {

	/**
	 * What I want:
	 * 	1. Class Name, Class Number, Student Name (Eng& Chi) 
	 * 	2. ALL of the ENROL_GROUP in that period
	 */
    
	 $numOfClassId = count($classIdAry);
	 for($i=0;$i<$numOfClassId;$i++){	//For Each Classes
	 	
	 	$_classId = $classIdAry[$i];
	 	$_className = $libclass->getClassName($_classId);
	 	/**
	 	 * Get All Student
	 	 */
	 	$_studentIDArr = $libclass->getStudentByClassId($_classId);
	 	
	 	$_userIDArr = getUserIDArrFromStudentInfo($_studentIDArr);
	 	//debug_pr($_userIDArr);
	 	$_userObj = new libuser('', '', $_userIDArr);
	 	if($isClub){
	 		$returnGroupArr = $libenroll->getEnrolGroupInfoByActivityDate($fromDate,$toDate,$_userIDArr);
	 	}
	 	if($isActivity){
	 		$returnActivityArr = $libenroll->getEnrolEvnetInfoByActivityDate($fromDate,$toDate,$_userIDArr);
	 	}
	 	$mergedArr = array_merge((array)$returnGroupArr,(array)$returnActivityArr);
	 	$resultAssoAry = BuildMultiKeyAssoc($mergedArr, array('UserID', 'StartDate'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
	 	//debug_pr($resultAssoAry);
	 	$numOfStudentId=count($_studentIDArr);
	 	for($j=0;$j<$numOfStudentId;$j++){ //For Each Students
	 		$weekdaysContentArr=''; //clear $weekdaysContentArr , reset every student
	 		$__columnContentArr = '';
	 		$__userID = $_userIDArr[$j];
	 		//debug_pr($__userID);
	 		$_userObj->LoadUserData($__userID);
	 		
	 		$__classNum = $_userObj->ClassNumber;
	 		$__chineseName = $_userObj->ChineseName;
	 		$__englishName = $_userObj->EnglishName;
	 		
	 		$__columnContentArr[]=$_className;
	 		$__columnContentArr[]=$__classNum;
	 		$__columnContentArr[]=$__englishName;
	 		$__columnContentArr[]=$__chineseName;
	 		
	 		$numOfDataSet = count($datesArr);
			for($k=0;$k<$numOfDataSet;$k++){	//For Each Dates
				$___date = $datesArr[$k];
				//debug_pr($___date);
				$activityArrInThisDate = (array)$resultAssoAry[$__userID][$___date];
				sortByColumn2($activityArrInThisDate, 'ActivityDateStart');
				$___activityTitleArr='';
				$TimeAry = '';
				$ShowAry = '';
				$numOfActivityInThisDate =count($activityArrInThisDate);
				for($l=0;$l<$numOfActivityInThisDate;$l++){	//Decompose array to single line
					$____activityTitle = $activityArrInThisDate[$l]['Title'];
					$___activityTitleArr[]=$____activityTitle;
// 					debug_pr($activityArrInThisDate);
					//get activity start/end date, location
					$ActivityDateStart = $activityArrInThisDate[$l]['ActivityDateStart'];
					$StartTime = substr($ActivityDateStart,10,-3);
					$ActivityDateEnd = $activityArrInThisDate[$l]['ActivityDateEnd'];
					$EndTime = substr($ActivityDateEnd,10,-3);
					$StartEndTime = $StartTime.' -'.$EndTime;
					$Location = $activityArrInThisDate[$l]['Location']== null? '':'('.$activityArrInThisDate[$l]['Location'].')';
					
					$ShowAry[] = $StartEndTime.$seperator.$____activityTitle.$seperator.$Location.$seperator;
				}
		//		$activityTitleInSingleLine = implode($seperator,(array)$___activityTitleArr);
				$activityTitleInSingleLine = implode($seperator,(array)$ShowAry);
					
				if($displayType=='row'){
				//	$__columnContentArr[]=$activityTitleInSingleLine;
					$__columnContentArr[]=$activityTitleInSingleLine;
				}else if($displayType=='summary'){
					insertIntoWeekdayContentArr((array)$ShowAry,$___date);
				}
			}
			if($displayType=='row'){
				$contentArr[] = $__columnContentArr;

			}else if($displayType=='summary'){
				writeToWeekdayContentRow($__columnContentArr);
			}
	 	}
	 }
	 
	
	  //debug_pr($html['formForExport']);
	  
	 /**
	  * Set Table width
	  */
	  $numOfHeader = count($HeaderArr);
	  /**
	   * total width equals to 100%-3% (3% is for '#' column)
	   */
	   $widthForFirstFourColumn = 8;
	  for($i=0;$i<$numOfHeader;$i++){
	  		if($i<4){
	  			$widthArr[] = $widthForFirstFourColumn.'%';
	  		}
	  		else{
	  			$widthArr[] = (100-3-4*$widthForFirstFourColumn)/($numOfHeader-4).'%';
	  		}
	  }
	  
	  
	  /**
	  * Form for export
	  */
	$html['formForExport'] = getHiddenFormForExport($HeaderArr,$contentArr,$widthArr);
	 echo $libenroll_report->getGeneralEnrolmentReportTableHtml($HeaderArr,$contentArr,$widthArr).$html['formForExport'];

}
function isWhichWeekday($date) {
    $weekday = date("D", strtotime($date));
    return $weekday;
}
function getUserIDArrFromStudentInfo($StudentIDArr){
	foreach((array) $StudentIDArr as $_userId => $_userDataAry ){
		$userIDArr[] = $_userDataAry['UserID'];
	}
	return $userIDArr;
}
function createDateRangeArray($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}
function getDayTypeLang($datetype){
	global $Lang;
	switch ($datetype){
		case 'Sun':
			return  $Lang['General']['DayType4']['0'];
		break;
		case 'Mon':
			return  $Lang['General']['DayType4']['1'];
		break;
		case 'Tue':
			return  $Lang['General']['DayType4']['2'];
		break;
		case 'Wed':
			return  $Lang['General']['DayType4']['3'];
		break;
		case 'Thu':
			return  $Lang['General']['DayType4']['4'];
		break;
		case 'Fri':
			return  $Lang['General']['DayType4']['5'];
		break;
		case 'Sat':
			return  $Lang['General']['DayType4']['6'];
		break;
	}
}
function getHeader($displayType){
	global $datesArr;
	global $i_ClassName,$Lang;
	$HeaderArr = array($i_ClassName,$Lang['General']['ClassNumber'],$Lang['General']['StudentNameEng'],$Lang['General']['StudentNameChi']);
	if($displayType=='row'){	
		for($i=0;$i<count($datesArr);$i++){
			$HeaderArr[] = $datesArr[$i] .' ('. getDayTypeLang(isWhichWeekday($datesArr[$i])).')' ;
		}
		
	}else if($displayType=='summary'){
		$weekdayHeaderArr = getWeekdayHeaderArr($datesArr);
		$HeaderArr = array_merge($HeaderArr,$weekdayHeaderArr);
	}
	return $HeaderArr;
}

function getWeekdayHeaderArr($datesArr){
	global $isWeekdays,$Lang;
		
	if(count($datesArr)>=7){
		$isWeekdays['Sun'] = true;
		$isWeekdays['Mon'] = true;
		$isWeekdays['Tue'] = true;
		$isWeekdays['Wed'] = true;
		$isWeekdays['Thu'] = true;
		$isWeekdays['Fri'] = true;
		$isWeekdays['Sat'] = true;
	}else{
		for($i=0;$i<count($datesArr);$i++){
			$_weekday = isWhichWeekday($datesArr[$i]);
			$isWeekdays[$_weekday] = true;
		}
	}
	($isWeekdays['Sun'])?$HeaderArr[]=$Lang['General']['DayType4']['0']:'';
	($isWeekdays['Mon'])?$HeaderArr[]=$Lang['General']['DayType4']['1']:'';
	($isWeekdays['Tue'])?$HeaderArr[]=$Lang['General']['DayType4']['2']:'';
	($isWeekdays['Wed'])?$HeaderArr[]=$Lang['General']['DayType4']['3']:'';
	($isWeekdays['Thu'])?$HeaderArr[]=$Lang['General']['DayType4']['4']:'';
	($isWeekdays['Fri'])?$HeaderArr[]=$Lang['General']['DayType4']['5']:'';
	($isWeekdays['Sat'])?$HeaderArr[]=$Lang['General']['DayType4']['6']:'';
	
	return $HeaderArr;
}
function insertIntoWeekdayContentArr($activityTitleArr,$date){
	global $weekdaysContentArr;
	$weekday = isWhichWeekday($date);
	

//    	$weekdaysContentArr[$weekday] = array_merge((array)$weekdaysContentArr[$weekday],$activityTitleArr);

	//20160523 Kenneth - fix error from php 5.4
	foreach($activityTitleArr as $_value){
		if(!is_array($weekdaysContentArr)||$weekdaysContentArr[$weekday]==''){
			$weekdaysContentArr[$weekday] = array();
		}
		array_push($weekdaysContentArr[$weekday],$_value);
	}


// 	debug_pr($weekdaysContentArr);
}
function writeToWeekdayContentRow($currentcolumnContentArr){
	global $isWeekdays,$contentArr,$weekdaysContentArr;

	($isWeekdays['Sun'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Sun',$currentcolumnContentArr):'';
	($isWeekdays['Mon'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Mon',$currentcolumnContentArr):'';
	($isWeekdays['Tue'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Tue',$currentcolumnContentArr):'';
	($isWeekdays['Wed'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Wed',$currentcolumnContentArr):'';
	($isWeekdays['Thu'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Thu',$currentcolumnContentArr):'';
	($isWeekdays['Fri'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Fri',$currentcolumnContentArr):'';
	($isWeekdays['Sat'])?$currentcolumnContentArr = getWeekdayCumulativeContentArr('Sat',$currentcolumnContentArr):'';
	//debug_pr($currentcolumnContentArr);
	
	
	$contentArr[] = $currentcolumnContentArr;
}

function getWeekdayCumulativeContentArr($weekday,$oldcolumnContentArr){
	global $isWeekdays,$contentArr,$weekdaysContentArr,$seperator;
	$weekdayArr = $weekdaysContentArr[$weekday];
	$weekdayArr = array_filter(array_unique($weekdayArr));	//Filter out empty array (array_filter) AND clear duplicates (array_unique)
	$weekdayContent = implode($seperator,$weekdayArr);	
	$currentcolumnContentArr = array_merge($oldcolumnContentArr,(array)$weekdayContent);
	return $currentcolumnContentArr;
}
function getHiddenFormForExport($HeaderArr,$contentArr,$widthArr){
	/**
	  * Form for export
	  */
	global $seperator;
	$html['formForExport']='<form id="form2" name="form2" method="post">';
	$numOfHeader = count($HeaderArr);
	for($i=0;$i<$numOfHeader;$i++){
		$html['formForExport'] .= '<input type="hidden" name="headerArr[]" value="'.$HeaderArr[$i].'">';
	}
	$numOfRow = count($contentArr);
	for($i=0;$i<$numOfRow;$i++){
		$numOfColumn = count($contentArr[$i]);
		for($j=0;$j<$numOfColumn;$j++){
			$html['formForExport'] .= '<input type="hidden" name="contentArr['.$i.'][]" value="'.$contentArr[$i][$j].'">';
	 
		}
		
	}
	$html['formForExport'] .= '<input type="hidden" name="seperator" value="'.$seperator.'" >';
	$numOfWidthArr = count($widthArr);
	for($i=0;$i<$numOfWidthArr;$i++){
		$html['formForExport'] .= '<input type="hidden" name="widthArr[]" value="'.$widthArr[$i].'" >';
	}
	
	$html['formForExport'] .= '</form>'; 
	
	return $html['formForExport'];
}
?>