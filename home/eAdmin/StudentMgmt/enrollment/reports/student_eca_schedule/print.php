<?php
## Modifying By: 
#  Date:   2019-06-10 Anna
#          delete $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'], for general school
#
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();

$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isEnrolPIC = $libenroll->IS_CLUB_PIC() ||  $libenroll->IS_EVENT_PIC();

if ($isEnrolAdmin || $isEnrolPIC){
//     if ($isEnrolAdmin || ($isEnrolPIC && $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'])){
        
}else{
    No_Access_Right_Pop_Up();
}


// $libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$headerArr = $_POST['headerArr'];
$contentArr= $_POST['contentArr'];
$widthArr= $_POST['widthArr'];

foreach($headerArr as &$header){
	$header = standardizeFormPostValue($header);
}
foreach($contentArr as &$contentRow){
	foreach($contentRow as &$data){
		$data = standardizeFormPostValue($data);
	}
}


$htmlAry['resultTable'] = $libenroll_report->getGeneralEnrolmentReportTableHtml($headerArr, $contentArr,$widthArr);
?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN('Print', "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<?=$htmlAry['resultTable']?>
<?
intranet_closedb();
?>