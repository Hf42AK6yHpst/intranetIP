<?php
## Modifying By: 
/*
 * Change Log:
 * Date:	2017-03-30 Frankie - handle User's Date Range
 * Date 2017-01-26 Villa: B112166 Allow 0hours/ times
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# user access right checking
$libenroll = new libclubsenrol();
$le = new libexporttext();

$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$rankTargetDetail_str = implode(",",(array)$rankTargetDetail);
if($rankTarget!="student")	$studentID = array();
$selectedClub = implode(",",(array)$activityCategory);

# prepare report data 
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = substr(getStartDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
	$SQL_enddate = substr(getEndDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
}
if (sizeof($studentID)>0) {
	$studentID_str = implode(",",$studentID);
} else {
	$userList = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
	$studentID_str = implode(",",$userList);
}

$sortType = $sortByOrder ? "desc" : "asc";
switch($sortBy)
{
	case 0:
		$order_by = "ClassName $sortType, ClassNumber $sortType";	
		break;
	case 1:
		$order_by = "ClassName, ClassNumber";	
		break;
	case 2:
		$order_by = "hours $sortType, ClassName, ClassNumber";	
		break;
}

$name_field = getNameFieldByLang("d.");

// $sql ="
// 	select 
// 		c.StudentID as UserID,  d.ClassName, d.ClassNumber, $name_field, e.ActivityDateStart, b.EventTitle, TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) as hours
// 	from 
// 			INTRANET_ENROL_EVENTSTUDENT as c
// 			inner join INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
// 			inner join INTRANET_ENROL_EVENTINFO as b on (b.EnrolEventID=c.EnrolEventID)
// 			inner join INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (e.ActivityDateStart >= '$SQL_startdate' and e.ActivityDateStart <= '$SQL_enddate'))
// 			inner join INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
// 		where
// 			c.EnrolEventID in ($selectedClub)
// 			and c.StudentID in ($studentID_str)
// 	order by
// 		$order_by, e.ActivityDateStart
// ";
$sql ="
		SELECT
			c.StudentID as UserID,
			d.ClassName,
			d.ClassNumber,
			$name_field,
			e.ActivityDateStart,
			b.EventTitle,
			IF(f.RecordStatus=3,
				'00:00:00',
				TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)
			) as hours
		from
			INTRANET_ENROL_EVENTSTUDENT as c
		INNER JOIN
			INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
		INNER JOIN
			INTRANET_ENROL_EVENTINFO as b on (b.EnrolEventID=c.EnrolEventID)
		INNER JOIN
			INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (e.ActivityDateStart >= '$SQL_startdate' and e.ActivityDateStart <= '$SQL_enddate'))
		INNER JOIN
			INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID";
if ($libenroll->enableUserJoinDateRange()) {
	$sql .=" 
		AND (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%' OR EnrolAvailiableDateStart <= ActivityDateStart)
		AND (EnrolAvailiableDateEnd IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%' OR EnrolAvailiableDateEnd >= ActivityDateEnd)";
}
$sql .=") 
		WHERE
			c.EnrolEventID in ($selectedClub)
		AND
			c.StudentID in ($studentID_str)
		ORDER BY
			$order_by, e.ActivityDateStart
		";
$result = $libenroll->returnArray($sql);

# times sql
if($times)
{
	$times_sym = $timesRange ? "<=" : ">=";
	$having_sql[] = " times $times_sym $times ";
}
if($hours)
{
	$hours_sym = $hoursRange ? "<=" : ">=";
	$having_sql[] = " hours $hours_sym '". ($hours<10?"0":"") ."$hours:00:00' ";
}
$having_con = !empty($having_sql) ? "having " . implode(" and " , $having_sql) : "";

$sortType = $sortByOrder ? "desc" : "asc";
switch($sortBy)
{
	case 0:
		$order_by = "d.ClassName $sortType, d.ClassNumber $sortType";	
		break;
	case 1:
		$order_by = "times $sortType";	
		break;
	case 2:
		$order_by = "hours $sortType";	
		break;
}

$name_field = getNameFieldByLang("d.");
// $sql ="
// 	select 
// 		c.StudentID as UserID,  d.ClassName, d.ClassNumber, $name_field, count(c.EnrolEventID) as times,
// 		SEC_TO_TIME(SUM(TIME_TO_SEC(IF (TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) < TIME('00:00:00'), '00:00:00', TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart))))) as hours
// 	from 
// 		INTRANET_ENROL_EVENTSTUDENT as c
// 		inner join INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
// 		inner join INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
// 		inner join INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
// 	where
// 		c.EnrolEventID in ($selectedClub)
// 		and c.StudentID in ($studentID_str)
// 	group by 
// 		d.UserID
// 	$having_con
// 	order by
// 		$order_by, d.ClassName, d.ClassNumber
// ";
			$sql ="
					SELECT
						c.StudentID as UserID,
						d.ClassName,
						d.ClassNumber,
						$name_field,
						SUM(
							IF(f.RecordStatus=3,'0','1')
						) as times,
						SEC_TO_TIME(
							SUM(
								TIME_TO_SEC(
									IF( f.RecordStatus=3,
										'00:00:00',
										IF( TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'),
											'00:00:00',
											TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart)
										)
									)
								)
							)
						) as hours,
						f.RecordStatus
					FROM
						INTRANET_ENROL_EVENTSTUDENT as c
					INNER JOIN
						INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
					INNER JOIN
						INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
					INNER JOIN
						INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID ";
if ($libenroll->enableUserJoinDateRange()) {
	$sql .=" 
		AND (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%' OR EnrolAvailiableDateStart <= ActivityDateStart)
		AND (EnrolAvailiableDateEnd IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%' OR EnrolAvailiableDateEnd >= ActivityDateEnd)";
}
$sql .=") WHERE
						c.EnrolEventID in ($selectedClub)
					AND
						c.StudentID in ($studentID_str)
					GROUP BY
						d.UserID
						$having_con
					ORDER BY
						$order_by, d.ClassName, d.ClassNumber
					";
$result_sort = $libenroll->returnArray($sql);

$exportColumn = array();
$data_ary = array();
$display = "";

$data_ary_sort = array();

if(!empty($result) && !empty($result_sort))
{		
	
	### build data array
	foreach($result as $k=>$d)
	{
		list($this_UserID, $this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours) = $d;
		$data_ary[$this_ClassName][$this_name][] = array($this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours);
	}
		
	if($displayUnit==0)	# student
	{
		
		### build sorting array
		foreach($result_sort as $kt=>$dt)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $dt;
			$data_sorting[$this_ClassName][] = $this_student;
		}

// 		if(count($data_sorting)>1)
// 			$class_sorted = array_merge(array_flip(array_keys($data_sorting)), $data_ary);
// 		else 
// 			$class_sorted=$data_ary;
//		Villa
		foreach ($data_sorting as $_class => $_data_sorting){
			foreach ($_data_sorting as $student_name){
				$class_sorted[$_class][$student_name] = $data_ary[$_class][$student_name];
			}
		}

		foreach($class_sorted as $this_ClassName=>$d)
		{	
			if (count($data_sorting[$this_ClassName])>1)
				$name_sorted = array_merge(array_flip($data_sorting[$this_ClassName]), $d);
			else 
				$name_sorted = $d;
							
			foreach($name_sorted as $this_name=>$d1)
			{	
				$tempex_ary = array();
				$tempex_ary[] = array($this_ClassName);
				$tempex_ary[] = array($this_name);
				$tempex_ary[] = array($i_UserName,$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"],$i_ClassNumber,$Lang['General']['Date'],$i_ClubsEnrollment_GroupName, $Lang['eEnrolment']['ActivityParticipationReport']['Hour(s)']);
				
				$this_student_total_hours = 0;
				$this_student_total_times = 0;
				
				foreach($d1 as $k1=>$d11)
				{
					list($this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours) = $d11;		
					$this_hr = $libenroll->timeToDecimal($this_hours);
					$this_date = substr($this_date,0,10);
					$this_student_total_hours += $this_hr;
					$this_student_total_times++;
						
					$tempex_ary[] = array($this_name, $this_ClassName, $this_ClassNumber, $this_date, $this_title, $this_hr);
					
				}
				$tempex_ary[] = array('','','','',$list_total,$this_student_total_hours);
				
				// Time and Hour setting check
				$hourMatch = 1;
				if($times)
				{
					if(($timesRange && $this_student_total_times > $times) || (!$timesRange && $this_student_total_times < $times)){
						$tempex_ary=array();
						$this_student_total_times=0;
					}
				}
				if($hours)
				{
					if(($hoursRange && $this_student_total_hours > $hours) || (!$hoursRange && $this_student_total_hours < $hours)){
						$tempex_ary=array();
						$this_student_total_hours=0;
						$hourMatch = 0;
					}
				}
				if($this_student_total_times!=0 && $hourMatch){
					$temp_array_size = count($tempex_ary);
					for($i=0;$i < $temp_array_size;$i++){
						if(count($exportColumn) == 0 && $i == 0){
							$exportColumn[] = $tempex_ary[$i];
						} else {
							$export_ary[] = $tempex_ary[$i];
						}
					}
				}
			}
		}
	}
	else				# class
	{
		### build data array
		foreach($result_sort as $k=>$d)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d;
			$this_hr = $libenroll->timeToDecimal($this_hours);
			$data_ary_sort[$this_ClassName]['t'] += $this_times;
			$data_ary_sort[$this_ClassName]['h'] += $this_hr;
		}
		
		if($sortBy!=0)
		{
			# sorting 
			foreach ($data_ary_sort as $key => $row) {
			    $t[$key]  = $row['t'];
			    $h[$key] = $row['h'];
			}

			switch($sortBy)
			{
				case 1:
					$sort_field = $t;	
					break;
				case 2:
					$sort_field = $h;	
					break;
			}
			$sort_order = $sortByOrder ? SORT_DESC : SORT_ASC;
			array_multisort($sort_field, $sort_order, $data_ary_sort);
		}

		if(count($data_ary_sort)>1)
			$class_sorted = array_merge(array_flip(array_keys($data_ary_sort)), $data_ary);
		else 
			$class_sorted = $data_ary;

		foreach($class_sorted as $this_ClassName=>$d)
		{	
			
			$tempex_ary = array();	
			$tempex_ary[] = array($this_ClassName);
			$tempex_ary[] = array($i_UserName,$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"],$i_ClassNumber,$Lang['General']['Date'],$i_ClubsEnrollment_GroupName, $Lang['eEnrolment']['ActivityParticipationReport']['Hour(s)']);
				
			$this_class_total_hours = 0;
			
			//debug_pr($d);
			
			foreach($d as $this_name=>$d1)
			{	
				
				$tempex1_ary = array();	
								
				$this_student_total_hours = 0;
				$this_student_total_times = 0;
			
				foreach($d1 as $k1=>$d11)
				{
					list($this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours) = $d11;		
					$this_hr = $libenroll->timeToDecimal($this_hours);
					$this_date = substr($this_date,0,10);
					$this_student_total_hours += $this_hr;
					$this_student_total_times++;
						
					$tempex1_ary[] = array($this_name, $this_ClassName, $this_ClassNumber, $this_date, $this_title, $this_hr);			
				}
				
				// Time and Hour setting check
				if($times)
				{
					if(($timesRange && $this_student_total_times > $times) || (!$timesRange && $this_student_total_times < $times)){
						$tempex1_ary=array();
						$this_student_total_hours=0;
					}
				}
				if($hours)
				{
					if(($hoursRange && $this_student_total_hours > $hours) || (!$hoursRange && $this_student_total_hours < $hours)){
						$tempex1_ary=array();
						$this_student_total_hours=0;
					}
				}
				$temp_student_array_size = count($tempex1_ary);
				for($i=0;$i < $temp_student_array_size;$i++){
					$tempex_ary[] = $tempex1_ary[$i];
				}
				$this_class_total_hours += $this_student_total_hours;
						
			}
			if($this_class_total_hours!=0){
				$temp_array_size = count($tempex_ary);
				for($i=0;$i < count($tempex_ary);$i++){	
					if(count($exportColumn) == 0 && $i == 0){
						$exportColumn[] = $tempex_ary[$i];
					} else {
						$export_ary[] = $tempex_ary[$i];
					}
				}
				$export_ary[] = array('','','','',$list_total,$this_class_total_hours);
			}		
		}
	}
}

$utf_content = $le->GET_EXPORT_TXT($export_ary, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

intranet_closedb();

$filename = "activity_participation_report.csv";
$le->EXPORT_FILE($filename, $utf_content); 
	
?>