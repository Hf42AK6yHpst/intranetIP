<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

if (!$plugin['eEnrollment'] || !($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


# Check access right
/*
if (!$plugin['eEnrollment'])
{
	echo "You have no priviledge to access this page.";
	exit();
}

if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))){
	echo "You have no priviledge to access this page.";
	exit();
}
*/

# Menu Display
$linterface = new interface_html("popup.html");
$MODULE_OBJ['title'] = $eEnrollment['send_club_enrolment_notification'];
$linterface->LAYOUT_START();

# Initialization
$libclass = new libclass();

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

if (!isset($field))
{
    $field = 0;
}
switch ($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	case 4: $field = 4; break;
	case 5: $field = 5; break;
	default: $field = 0; break;
}
if ($order=="" || !isset($order))
{
	$order = ($order == 0) ? 1 : 0;
}


# build SQL
# should be same as $libenroll->returnNoHandinList()
$ex_levels = $libenroll->getClassLevelIDNotUseEnrollment();
if (sizeof($ex_levels)!= 0)
{
	$ex_level_list = implode(",",$ex_levels);
	//$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 AND ClassLevelID IN ($ex_level_list)";
	$sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE RecordStatus = 1 AND AcademicYearID='".Get_Current_Academic_Year_ID()."' AND YearID IN ($ex_level_list)";
	$classes = $libenroll->returnVector($sql);
	if (sizeof($classes)!=0)
	{
		$ex_class_list = "'".implode("','",$classes)."'";
		$conds = " AND a.ClassName NOT IN ($ex_class_list)";
	}
}

$conds_ClassName = ($targetClassName!="")? " AND a.ClassName = '".$targetClassName."' " : "";
$name_field = getNameFieldByLang("a.");

$ClubOfCurrentYear = $libenroll->Get_EnrolGroupID_Of_Current_Year();
$sql = "SELECT 
				a.ClassName, 
				a.ClassNumber, 
				$name_field, 
				CONCAT('<input type=\'checkbox\' id=\'email_', a.UserID ,'\' name=\'EmailStudentID[]\' value=\'', a.UserID ,'\'>')
		FROM 
				INTRANET_USER as a
				LEFT OUTER JOIN INTRANET_ENROL_GROUPSTUDENT as b ON a.UserID = b.StudentID AND b.EnrolGroupID IN (".implode(",",(array)$ClubOfCurrentYear).")
		WHERE 
				a.RecordType = 2 
				AND a.RecordStatus = 1 
				AND b.EnrolGroupStudentID IS NULL
				$conds
				$conds_ClassName
		";


# Table Info
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.ClassName", "a.ClassNumber", $name_field, "a.UserID");
$li->fieldorder2 = ", a.ClassNumber";
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

# Table Title
$li->column_list .= "<td width=1 class=tableTitle><span class='tabletoplink'>#</span></td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_ClassName)."</td>\n";
$li->column_list .= "<td class=tableTitle>".$li->column(1, $i_ClassNumber)."</td>\n";
$li->column_list .= "<td class=tableTitle>".$li->column(2, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("EmailStudentID[]")."</td>\n";


## Class Selection
$selectionAttributes = "id=\"targetClassName\" name=\"targetClassName\" onchange=\"document.form1.submit()\"";
$select_class = $libclass->getSelectClass($selectionAttributes, $targetClassName, "", $i_general_all_classes);

## Right-handed buttons
$TableActionArr = array();
$TableActionArr[] = array("javascript:jsGoCompose()", $PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/iMail/icon_compose.gif", "imgNotify", $eEnrollment['notify_by_email']);

$TableActionHTML = "";
$TableActionHTML .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
$TableActionHTML .= "<tr><td align=\"right\">\n";
$TableActionHTML .= $libenroll->GET_TABLE_ACTION_BTN($TableActionArr);
$TableActionHTML .= "</td></tr></table>\n";


## System Message
if ($msg == "success")
	$SysMsg = $linterface->GET_SYS_MSG("",$i_con_msg_email);
else if ($msg == "failed")
	$SysMsg = $linterface->GET_SYS_MSG("","<font color=\"red\">".$eEnrollment['failed']."</font>");
else
	$SysMsg = "";

$msg = "";
?>

<script language="javascript">

function jsGoCompose(obj,page)
{
	checkRole(document.form1, "EmailStudentID[]", "compose_notification.php");
}

function initizalizeCheckbox()
{
	var jsStudentIDList = "<?=$EmailStudentIDList?>";
	var jsStudentIDArr = jsStudentIDList.split(",");
	
	if (jsStudentIDList == "") return;
	
	var i = 0;
	for (i=0; i<jsStudentIDArr.length; i++)
	{
		var thisStudentID = jsStudentIDArr[i];
		document.getElementById("email_" + thisStudentID).checked = true;
	}
}

</script>

<form name="form1" method="POST">
	<br />
	<table width="96%" border="0" cellpadding="0" cellspacing="0">
		<tr><td align="right" colspan="2"><?=$SysMsg?></td></tr>
		<tr class="table-action-bar">
			<td align="left"><?=$select_class?></td>
			<td align="right"><?=$TableActionHTML?></td>
		</tr>
		<tr><td colspan="2"> <?=$li->display("100%")?> </td></tr>
	</table>
	<br />
	
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td align="center" colspan="6">
			<div style="padding-top: 5px">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close();")?>
			</div>
		</td></tr>
	</table>
	<br />
	<br />

	
	<input type="hidden" name="field" value="<?=$field?>">
	<input type="hidden" name="order" value="<?=$order?>">
	<input type="hidden" name="pageNo" value="<?=$pageNo?>">
	<input type="hidden" name="page_size_change" value="">
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
	
	<script language="javascript">
		initizalizeCheckbox();
	</script>
</form>

<?		
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

