<?php
// using: 
###########################################
## Modification Log:
##
##  Date:   2020-04-23 Tommy
##          fix: cannot output number of student is PRESENT, ABSENT or EXEMPT, 
##               change GetActStudentStatusNumber() to GetActStudentStatus()
##
###########################################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# user access right checking
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);



if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageAttendanceStatisticReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang["eEnrolment"]['ReportArr']["AttendanceStatistic"], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();


# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=0; showResult(\"form\",\"\")'", "", $selectYear);
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";


$TargetCategoryList = implode(",",(array)$TargetCategory);

$TargetCategoryAry = $TargetCategory;

#Category
$CategorySelection = $libenroll->GET_CATEGORY_SEL($TargetCategoryAry, $TargetCategoryAry, 'filterByCategory();', $ParSelLang, $noFirst=1, $TargetCategoryList,$IsMultiple=1,false,$DeflautName='TargetCategory');
$CategorySelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All('TargetCategory',1);");
$CategorySelection .= $linterface->spacer();
$CategorySelection .= $linterface->MultiSelectionRemark();
$CategorySelection .= '<br />'."\n";	
$CategorySelection .= '<br />'."\n";

$categoryRowHTML = $CategorySelection;	//Club selection list

$x .= '<tr>'."\n";
	$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['record_type'].'</td>'."\n";
	$x .= '<td>'."\n";
		$CheckedAll = ($club && $activity)?true: false;
		$CheckedClub = $club? true: false; 
		$CheckedActivity = $activity? true: false;
		$x .= $linterface->Get_Checkbox('GlobalChk', $Name, $Value, $CheckedAll, $Class="", $Lang['Btn']['SelectAll'], 'clickedCheckbox(\'global\', this.checked);');
		$x .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
		$x .= $linterface->Get_Checkbox('Chk_club', 'club[]', 'club', $CheckedClub, $Class='', $eEnrollment['club'], 'clickedCheckbox(\'club\', this.checked);');
		$x .= $linterface->Get_Checkbox('Chk_activity', 'activity[]', 'activity', $CheckedActivity, $Class="", $eEnrollment['activity'], 'clickedCheckbox(\'activity\', this.checked);');
		$x .= $linterface->Get_Form_Warning_Msg('WarnMsgDiv', $Lang['eEnrolment']['WarningArr']['ChooseSource'], $Class='warnMsgDiv')."\n";
		$x .= '<br />'."\n";
	$x .= '</td>'."\n";
$x .= '</tr>'."\n";

//category
$x .= '<tr>'."\n";
	$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['category'].'</td>'."\n";
$x .= '<td>'."\n";

	$x .= $categoryRowHTML;

	//club
	$x .= '<div id="Div_club">'."\n";
		$clubSel = $libenroll_ui->Get_Club_Selection('clubSel', 'clubIdAry[]', $clubIdAry, $CategoryID='', $IsAll=0, $NoFirst=1, '', $AcademicYearID='', $IncludeEnrolGroupIdAry='', $Disabled=false, $Class='', $isMultiple=true);
		$clubSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('clubSel', 1);");
		$x .= '<b>'.$eEnrollment['club'].'</b>'."\n";
			$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($clubSel, $clubSelectAllBtn, 'clubSelectionBox')."\n";
			$x .= $linterface->Get_Form_Warning_Msg('clubWarnMsgDiv', $Lang['General']['ClubParticipationReport']['SelectAtLeastOneClub'], $Class='warnMsgDiv')."\n";
		$x .= '<br />'."\n";
	$x .= '</div>'."\n";
	
	//activity
	$x .= '<div id="Div_activity">'."\n";
		$activitySel = $libenroll_ui->Get_Activity_Selection('activitySel', 'activityIdAry[]', $activityIdAry, $CategoryID='', $IsAll=0, $NoFirst=1, '', $AcademicYearID='', $isMultiple=true);
		$activitySelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('activitySel', 1);");
		$x .= '<b>'.$eEnrollment['activity'].'</b>'."\n";
			$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($activitySel, $activitySelectAllBtn, 'activitySelectionBox')."\n";
			$x .= $linterface->Get_Form_Warning_Msg('activityWarnMsgDiv',  $Lang['General']['ActivityParticipationReport']['SelectAtLeastOneActivity'], $Class='warnMsgDiv')."\n";
		$x .= '<br />'."\n";
	$x .= '</div>'."\n";

	$x .= '</td>'."\n";
$x .= '</tr>'."\n";


$tool = '';
$tool .= '<div id="contentToolDiv" class="content_top_tool">'."\n";
$tool .= '<div class="Conntent_tool">'."\n";
$tool .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
$tool .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
$tool .= '</div>'."\n";
$tool .= '</div>'."\n";
$htmlAry['contentToolDiv'] = $tool;

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();

$linterface->LAYOUT_START();
	
//debug_pr($_POST);

$rankTargetDetail_str = implode(",",(array)$rankTargetDetail);


$selectedClub = implode(",",(array)$clubIdAry);
$selectedActivity = implode(",",(array)$activityIdAry);



# prepare report data 
if ($radioPeriod == "YEAR") 
{
//	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = substr(getStartDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
	$SQL_enddate = substr(getEndDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
}

$name_field = getNameFieldByLang("ig.");
$Club_MeetingAry = $libenroll->Get_Club_Meeting_Info_By_Date($clubIdAry,0,$SQL_startdate,$SQL_enddate);
//$club_meeting = $libenroll->Get_Club_Meeting_Date($clubIdAry,0);


$NumberOfclub = sizeof($Club_MeetingAry);


// /$display = "";
$display .= $linterface->GET_NAVIGATION2_IP25($this_ClassName);
$display .= "<table class='common_table_list_v30 view_table_list_v30'>";
	$display .= "<tr>";
		$display .= "<th width='100'> ".$eEnrollment['club']."/".$eEnrollment['activity']."</th>";
		$display .= "<th width='100'>".$Lang['eEnrolment']['MeetingSchedule']."</th>";
		$display .= "<th width='100'>".$Lang["eEnrolment"]['AttendanceStatistic']['TotalNum']."</th>";
		$display .= "<th width='100'>".$Lang["eEnrolment"]['AttendanceStatistic']['PresentNum']."</th>";
		$display .= "<th width='100'>".$Lang["eEnrolment"]['AttendanceStatistic']['AbsentNum']."</th>";
		$display .= "<th width='100'>".$Lang["eEnrolment"]['AttendanceStatistic']['ExemptNum']."</th>";
		if($sys_custom['eEnrolment']['Attendance_Late']){
			$display .= "<th width='100'>".$Lang["eEnrolment"]['AttendanceStatistic']['LateNum']."</th>";
		}
		if($sys_custom['eEnrolment']['Attendance_EarlyLeave']){
			$display .= "<th width='100'>".$Lang["eEnrolment"]['AttendanceStatistic']['EarlyLeaveNum']."</th>";
		}
	$display .= "</tr>";


//debug_pr($ByGroupID);

if(!empty($club)){
	for($i=0;$i<$NumberOfclub;$i++){

		$ClubEventStartDate = $Club_MeetingAry[$i]['ActivityDateStart'];
		$ClubEventEndDate = $Club_MeetingAry[$i]['ActivityDateEnd'];
		$ClubGroupDateID = $Club_MeetingAry[$i]['GroupDateID'];
		$ClubEnrolGroupID = $Club_MeetingAry[$i]['EnrolGroupID'];
		$EnrolGroupInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($ClubEnrolGroupID);
		
		$ClubStartDate = substr($ClubEventStartDate,0,16);
		$ClubEndDate = substr($ClubEventEndDate,11,5);
		
		$StartDate = substr($ClubEventStartDate,0,10);
		$EndDate =  substr($ClubEventEndDate,0,10);
		
		$ClubTitle = Get_Lang_Selection($EnrolGroupInfo['TitleChinese'],$EnrolGroupInfo['Title']);

		$NumberofClubStudent = $libenroll->Get_Club_Member_Info($ClubEnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0);
//		debug_pr($NumberofClubStudent);
		$StudentIDArr = Get_Array_By_Key($NumberofClubStudent, 'StudentID');
		$StudentIDList = implode(",",(array)$StudentIDArr);
	
	
		$NumbOfClubMumber = sizeof($StudentIDArr);
	
		$StudentNumberResultArr = $libenroll->GetClubStudentStatusNumber($ClubEnrolGroupID,$ClubGroupDateID,$StudentIDList);
		
		$StudentStatus = BuildMultiKeyAssoc($StudentNumberResultArr,RecordStatus,array(StudentID,RecordStatus),0,1);
		
		
		$PresentStudent = $StudentStatus[ENROL_ATTENDANCE_PRESENT];		
		$AbsentStudent = $StudentStatus[ENROL_ATTENDANCE_ABSENT];
		$ExemptStudent = $StudentStatus[ENROL_ATTENDANCE_EXEMPT];
		
		$NumberOfPresent = sizeof($PresentStudent);
		$NumberOfAbsent= sizeof($AbsentStudent);
		$NumberOfExempt = sizeof($ExemptStudent);
		if($sys_custom['eEnrolment']['Attendance_Late']){
			$LateStudent = $StudentStatus[ENROL_ATTENDANCE_LATE];
			$NumberOfLate = sizeof($LateStudent);
			$NumberOfPresent += $NumberOfLate;
		}
		if($sys_custom['eEnrolment']['Attendance_EarlyLeave']){
			$EarlyLeaveStudent = $StudentStatus[ENROL_ATTENDANCE_EARLY_LEAVE];
			$NumberOfEarlyLeave = sizeof($EarlyLeaveStudent);
			$NumberOfPresent += $NumberOfEarlyLeave;
		}
		
	//	debug_pr($presentStudent);
		
		
		$display .= "<tr>";
		$display .= "<td >$ClubTitle</td>";
		
		$display .= "<td>$ClubStartDate--$ClubEndDate</td>";
		$display .= "<td>$NumbOfClubMumber</td>";
		$display .= "<td>$NumberOfPresent</td>";
		$display .= "<td>$NumberOfAbsent</td>";
		$display .= "<td>$NumberOfExempt</td>";
		if($sys_custom['eEnrolment']['Attendance_Late']){
				$display .= "<td>$NumberOfLate</td>";
		}
		if($sys_custom['eEnrolment']['Attendance_EarlyLeave']){
			$display .= "<td>$NumberOfEarlyLeave</td>";
		}
	
		$display .= "</tr>";		
	}	
}


	$Activity_MeetingAry = $libenroll->Get_Activity_Meeting_Info_By_Date($activityIdAry,0,$SQL_startdate,$SQL_enddate);
	$NumberOfActivity = sizeof($Activity_MeetingAry);


if(!empty($activity)){

	for($i=0;$i<$NumberOfActivity;$i++){

		$ActivityEventStartDate = $Activity_MeetingAry[$i]['ActivityDateStart'];
		$ActivityEventEndDate = $Activity_MeetingAry[$i]['ActivityDateEnd'];
		$ActivityEventDateID = $Activity_MeetingAry[$i]['EventDateID'];
		$ActivityEnrolEventID = $Activity_MeetingAry[$i]['EnrolEventID'];
		
		$ActivityTitle = $libenroll->GET_EVENT_TITLE($ActivityEnrolEventID);
//		debug_pr($EnrolActivityTitle);
	//	$ClubTitle = $EnrolGroupInfo['Title'];
		$ActStartDate = substr($ActivityEventStartDate,0,16);
		$ActEndDate = substr($ActivityEventEndDate,11,5);
		
		
		$ActivityMemberInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($ActivityEnrolEventID, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ActiveMemberOnly=0, $FormIDArr='');
		$ActivityMemberInfoArr = (array)$ActivityMemberInfoArr[$ActivityEnrolEventID]['StatusStudentArr'][2];
		
		$StudentIDArr = Get_Array_By_Key($ActivityMemberInfoArr, 'StudentID');
		$NumOfActivityMumber = sizeof($StudentIDArr);
		
		$StudentIDList = implode(",",(array)$StudentIDArr);
		
	
// 		$StudentArr = $libenroll->GetActStudentStatusNumber($ActivityEnrolEventID,$ActivityEventDateID,$StudentIDList);
		$StudentArr = $libenroll->GetActStudentStatus($ActivityEnrolEventID,$ActivityEventDateID,$StudentIDList);
		
		$StudentStatus = BuildMultiKeyAssoc($StudentArr,RecordStatus,array(StudentID,RecordStatus),0,1);
		$PresentStudentAry = $StudentStatus[ENROL_ATTENDANCE_PRESENT];
		$AbsentStudentAry = $StudentStatus[ENROL_ATTENDANCE_ABSENT];
		$ExemptStudentAry = $StudentStatus[ENROL_ATTENDANCE_EXEMPT];
		
		
		$NumberOfPresent = sizeof($PresentStudentAry);
		$NumberOfAbsent= sizeof($AbsentStudentAry);
		$NumberOfExempt = sizeof($ExemptStudentAry);
		
		if($sys_custom['eEnrolment']['Attendance_Late']){
			$LateStudentAry = $StudentStatus[ENROL_ATTENDANCE_LATE];
			$NumberOfLate= sizeof($LateStudentAry);
		}
		
		if($sys_custom['eEnrolment']['Attendance_EarlyLeave']){
			$EarlyLeaveStudent = $StudentStatus[ENROL_ATTENDANCE_EARLY_LEAVE];
			$NumberOfEarlyLeave = sizeof($EarlyLeaveStudent);
			$NumberOfPresent += $NumberOfEarlyLeave;
		}
		
		if($ActivityTitle !== null ){
		$display .= "<tr>";
			$display .= "<td >$ActivityTitle</td>";	
			$display .= "<td>$ActStartDate--$ActEndDate</td>";
			$display .= "<td>$NumOfActivityMumber</td>";
			$display .= "<td>$NumberOfPresent</td>";
			$display .= "<td>$NumberOfAbsent</td>";
			$display .= "<td>$NumberOfExempt</td>";
			if($sys_custom['eEnrolment']['Attendance_Late']){
				$display .= "<td>$NumberOfLate</td>";
			}
			if($sys_custom['eEnrolment']['Attendance_EarlyLeave']){
				$display .= "<td>$NumberOfEarlyLeave</td>";
			}
		$display .= "</tr>";
		
		}
	}
}
$display .= "</table>";
?>

<script language="javascript">
$(document).ready(
	function () {

		changeTerm($("#selectYear").val(), true);
		
		if($('input#Chk_club').attr('checked') == false){
			$('div#Div_club').hide();
		}
		if($('input#Chk_activity').attr('checked') == false ){
		
			$('div#Div_activity').hide();
			}
	
		//js_Select_All('clubSel', 1);
		//js_Select_All('activitySel', 1);
		//filterByCategory();
	}
);
function hideOptionLayer()
{
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}


function changeTerm(val, isInitialize) {
	isInitialize = isInitialize || false;
	
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	$('span#spanSemester').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"../../ajaxGetSemester.php", 
			{ 
				year: val
			},	
		
			function(ReturnData) {
				//	console.log(ReturnData);
			//	js_Select_All('spanSemester', 1);
			}
		);

	changesemester('', isInitialize);
}

function changesemester(value, isInitialize){
	isInitialize = isInitialize || false;
	
	var semester =	 value;
	filterByCategory(isInitialize);
}


function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function clickedCheckbox(targetSource, elementChecked) {
	if (targetSource == 'global') {
		Set_Checkbox_Value('attendanceSource[]', elementChecked);
		
		clickedCheckbox('club', elementChecked);
		clickedCheckbox('activity', elementChecked);
	}
	else {
		Uncheck_SelectAll('GlobalChk', elementChecked);
	
		var selectionDivId = 'Div_' + targetSource;
//		console.log(selectionDivId);
		if (elementChecked) {
			$('div#' + selectionDivId).show();
		}
		else {
			$('div#' + selectionDivId).hide();
		}
	}
}


function filterByCategory(isInitialize){
	isInitialize = isInitialize || false;

	var selectedCategory = $('#TargetCategory').val();
	var CategoryIDList =selectedCategory.join(',');
	

	var selectedClub = $('#clubSel').val();
	if(selectedClub == null ){
		var selectedClubList = '';
		}
	else{
	var selectedClubList = selectedClub.join(',');
	}
	
	var selectedAct = $('#activitySel').val();
	if(selectedAct == null){
		var selectedActList='';
	}else{
		var selectedActList = selectedAct.join(',');
	}
	
	
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if(jPeriod=="DATE")		
	{
		// force display current year's clubs
		var yearID = <?=$AcademicYearID?>;
	}
	else
	{
		// display selected school year's clubs
		var yearID = document.form1.selectYear.value;
	}
		$('span#clubSelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
				"../ajax_reload.php", 
				{ 
					Action: 'clubSelection',
					CategoryIDList: CategoryIDList,
					SelectionID: 'clubSel',
				//	SelectionName: getJQuerySaveId('clubIdAry[]'),
					SelectionName: 'clubIdAry[]',
					TargetEnrolGroupID: selectedClubList,
				//	OnChange: 'changedSelection(\'club\');',
					AcademicYearID:yearID
					
				},
				function(ReturnData) {
					if (isInitialize) {

					}else{
					js_Select_All('clubSel', 1);
					}
				}
			);
	
			$('span#activitySelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
				"../ajax_reload.php", 
				{ 
					Action: 'activitySelection',
					CategoryIDList: CategoryIDList,
					SelectionID: 'activitySel',
					SelectionName: 'activityIdAry[]',
				//	SelectionName: getJQuerySaveId('activityIdAry[]'),
					//OnChange: 'changedSelection(\'activity\');',
					TargetEnrolEventID: selectedActList,
					AcademicYearID: yearID
						
				},
				function(ReturnData) {
					if (isInitialize) {
					}
					else{
					js_Select_All('activitySel', 1);
					}
				}
			);
}




function getSelectionId(targetType) {
	var selectionId = '';
	if (targetType == 'club') {
		selectionId = 'clubSel';
	}
	else if (targetType == 'activity') {
		selectionId = 'activitySel';
	}	
	return selectionId;
}

function checkForm(){

	var canSubmit = true;
	var isFocused = false;

	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate( document.form1.textFromDate.value,  document.form1.textToDate.value) > 0) 
	{	canSubmit = false;
		
	}
	
	///// club 
	if ($('input#Chk_club').attr('checked') && $('select#clubSel option:selected').length==0) {
	
		canSubmit = false;
		$('div#clubWarnMsgDiv').show();
		if (!isFocused) {
			$('input#clubSel').focus();
			isFocused = true;
		}
		
	}

	/////activity
	if ($('input#Chk_activity').attr('checked') && $('select#activitySel option:selected').length==0) {
		canSubmit = false;
		$('div#activityWarnMsgDiv').show();
		if (!isFocused) {
			$('input#activitySel').focus();
			isFocused = true;
		}
		
	}

	if (canSubmit == false ) {
		return false
		}
	
	
}

function goExport() {
//	window.location.href = 'export.php';
	$('form#form1').attr('target', '_self').attr('action', 'export.php').submit();
	$('form#form1').attr('target', '').attr('action', 'result.php');
	
	
//	document.form1.action = "export.php";
}

function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
	$('form#form1').attr('target', '').attr('action', 'result.php');
}


</script>
<div id="div_form" class="report_option report_hide_option">
	<span id="spanShowOption" class="spanShowOption">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
		
				
	<p class="spacer"></p> 
	<span class="Form_Span" style="display:none">
		<form name="form1" id="form1" method="post" onSubmit="return checkForm();" action="result.php">
		
			<table class="form_table_v30">
				<tr valign="top">
					<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
					<td>
						<table class="inside_form_table">
							<tr>
								<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true; filterByCategory();">
									<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  onClick="filterByCategory();">
									<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
									<?=$selectSchoolYearHTML?>&nbsp;
									<?php 
							
									if($plugin['SIS_eEnrollment']){ 
										$hideSemester = ' style="display:none;"';
									}
									?>
									<span <?=$hideSemester?>>
										<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
										<span id="spanSemester" onclick = "changesemester(this.value)"><?=$selectSemesterHTML?></span>
									</span>
								</td>
							</tr>
							<tr>
								<td onClick="document.form1.radioPeriod_Date.checked=true; filterByCategory();"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE")?" checked":"" ?> onClick="filterByCategory();"> <?=$i_From?> </td>
								<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='filterByCategory();' ")?>
								<br><?=$linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg',$i_invalid_date, $Class='warnMsgDiv')?></td>
								<td><?=$i_To?> </td>
								<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='filterByCategory();' ")?>
								
							</tr>
						</table>
					</td>
				</tr>
				
				
				<!-- Category -->
					<?=$x?>	
				
			</table>

            <?=$linterface->MandatoryField();?>
	
			<div class="edit_bottom_v30">
				<p class="spacer"></p>
					<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
				<p class="spacer"></p>
			</div>
			
	</form>
	</span>
</div>

<?=$htmlAry['contentToolDiv']?>
<?php echo $display;?>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>