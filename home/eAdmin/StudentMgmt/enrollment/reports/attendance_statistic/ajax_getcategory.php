<?php
# Modifying By:
/*
 * Change Log:
 * Date:	2017-03-30 Frankie - handle User's Date Range
 */

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

# authentication and open db
intranet_auth();
intranet_opendb();

# declare common using library
$li = new libdb();
$linterface = new interface_html();
$libclubsenrol = new libclubsenrol();

$CategoryIDList = trim(stripslashes($categoryIDList));
$CategoryIDAry = explode(',', $CategoryIDList);


if($Action=="CategoryElements")
{
	$elements = array();
	for($i=0;$i<sizeof($CategoryIDAry);$i++){
		$categoryID = $CategoryIDAry[$i];
		
		$tempGroupInfo = $libclubsenrol->getGroupInfoforSel($categoryID, $AcademicYearID);
		foreach ($tempGroupInfo as $k => $d)
		{
			$elements[$d[0]] =$d[1];
		}
		$multipleSelectionBox = $linterface->Get_Multiple_Selection_Box($elements, "activityCategory", "activityCategory[]", 10,0, explode(",",$selectedClubs));
		$x = $multipleSelectionBox;	
	}	
}

if($Action=="ActivityCategoryElements")
{
	$elements = array();
	for($i=0;$i<sizeof($CategoryIDAry);$i++){
		$categoryID = $CategoryIDAry[$i];
	
		//$tempGroupInfo = $libclubsenrol->getGroupInfoforSel($categoryID, $AcademicYearID);
		$tempGroupInfo = $libclubsenrol->GET_EVENTINFO_LIST($categoryID, '', $AcademicYearID);
		foreach ($tempGroupInfo as $k => $d)
		{
			$elements[$d['EnrolEventID']] = $d['EventTitle'];
		}
		$multipleSelectionBox = $linterface->Get_Multiple_Selection_Box($elements, "activityCategory", "activityCategory[]", 10,0, explode(",",$selectedClubs));
		$x = $multipleSelectionBox;

	}
}


echo $x;
intranet_closedb();
?>