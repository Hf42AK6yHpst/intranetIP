<?php
# using: 
############## Change Log [start] #### 
#	Date:	2017-10-09	Anna
#			Added dispaly pic name
#
#	Date:	2017-01-18	Villa
#			B111852 Fix - cannot export specific term problem
#
#	Date:	2015-08-27	Omas
#			Fix - cannot export all club problem
#
#	Date:	2015-08-17	Omas #R81016 
#			Improved - if student dont have any club and activity can still export the name out  
#
#	Date:	2015-05-26	Omas #J31222 
#			remove flag $sys_custom['eEnrolment']['heungto_enrolment_report'] to general deploy the feature
#
#	Date:	2014-07-23	Bill
#	- Export file with related club
#
########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();

$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin');

//debug_pr($_POST);

$showTarget = $_POST['showTarget'];		// 'class' or 'club'
$targetClassId = $_POST['targetClassID'];
$targetClub = $_POST['targetClub'];
$display_club = $_POST['display_club'];
$display_activity = $_POST['display_activity'];
$display_role = $_POST['display_role'];
$display_attendance = $_POST['display_attendance'];
$display_performance = $_POST['display_performance'];
$display_email = $_POST['display_email'];
$display_picname = $_POST['display_picname'];
//$display_belong__to_group = $POST['display_belong__to_group'];
$selected_printing = $_POST['selected_printing'];
$selected_student = $_POST['selected_student'];
$exportStudentViewClubArr = unserialize(rawurldecode($_POST['exportStudentViewClubArr']));
$exportStudentViewActivityArr = unserialize(rawurldecode($_POST['exportStudentViewActivityArr']));

$headerAry = array();
$dataAry = array();
$exportStudentViewClubArr = array();
$exportStudentViewActivityArr = array();

$headerAry[] = $i_UserClassName;
$headerAry[] = $i_UserClassNumber;
$headerAry[] = $i_UserStudentName;
if ($display_email) {
	$headerAry[] = $Lang['eEnrolment']['show_email'];
}

$headerAry[] = $Lang['eEnrolment']['record_type'];
$headerAry[] = $Lang['eEnrolment']['RecordName'];
// Header: for related group
if($display_belong__to_group){
	$headerAry[] = $eEnrollment['belong_to_group'];
}
if ($display_role) {
	$headerAry[] = $eEnrollment['role'];
}
if ($display_attendance) {
	$headerAry[] = $eEnrollment['attendence'];
}
if ($display_performance) {
	$headerAry[] = $eEnrollment['performance'];
}
if($display_picname){
	$headerAry[] = $Lang['eEnrolment']['ClubPIC'];
}



$studentIdAry = array();
$studentNameAry = array();
if ($showTarget == 'class' && isset($targetClassId) && $targetClassId != '') {
	foreach ((array)$targetClassId as $thisTargetClassId) {
		$YearClassObj = new year_class($thisTargetClassId,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
		$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
		$studentNameAry =  array_merge($studentNameAry,$YearClassObj->ClassStudentList);
	}
	$studentIdAry = Get_Array_By_Key($studentNameAry, 'UserID');
}
else if ($showTarget == 'club' && isset($targetClub) && $targetClub != '') {
 	$studentNameAry = $libenroll->Get_Club_Member_Info((array)$targetClub, $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
	$studentIdAry = Get_Array_By_Key($studentNameAry, 'UserID');
	$studentIdAry = array_unique($studentIdAry);
}


# Cater selected student only [Start] ###
if($selected_printing) {
	$studentIdAry = $selected_student;
	$new_studentNameAry = array();
	
	foreach($studentNameAry as $k => $d) {
		if(in_array($d['UserID'], $selected_student)) {
			$new_studentNameAry[] = $d;
		}	
	}
	$studentNameAry = $new_studentNameAry;
}
# Cater selected student only [End] ###

if($Semester == -999)
	$Semester = '';
if(isset($Semester)) {
 	//$term_group_list_temp = $libenroll->getGroupInfoList(0, $Semester, $EnrolGroupIDArr='', $getRegOnly=0, $ParCond="", $AcademicYearID);
//  	$Semester = '';
// 	B111852 Villa
 	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', (array)$Semester);
 	$term_group_list = array();
 	if(!empty($term_group_list_temp))
 	{
     	foreach($term_group_list_temp as $k=>$d)
     		$term_group_list[] = $d['EnrolGroupID'];
 	}
}


foreach($studentIdAry as $k => $studentID) {
	$studentView = true;
   
   ################################## Club Table ##################################
 	//get all groups of student
 	$title_order = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
 	$sql = "SELECT b.Title, a.Performance, a.EnrolGroupID, c.Title as Role, b.TitleChinese,u.UserEmail
 			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
 			LEFT JOIN INTRANET_ROLE as c ON c.RoleID = a.RoleID
 			INNER JOIN INTRANET_USER as u ON a.UserID = u.UserID
 			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
 			ORDER BY ". $title_order;
 	$clubPerformArr = $libenroll->returnArray($sql);
 	$numOfClub = sizeof($clubPerformArr);

 	//loop through each club record to construct table content
 	$index_i = 0;
 	
 	for ($i=0; $i<$numOfClub; $i++)
 	{
     	//if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
     	if( isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
		{
			continue;
		}
		
 		# Role
 		$thisRole = $clubPerformArr[$i]['Role'];
 		 
 		#Email 
    	$thisEmail = $clubPerformArr[$i]['UserEmail'];
    	
 		# Attendance
 		$ParDataArr['EnrolGroupID'] = $clubPerformArr[$i]['EnrolGroupID'];
 		$ParDataArr['StudentID'] = $studentID;
 		$thisAttendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($ParDataArr)."%";
 		
 		# Performance
 		$thisPerformance = $clubPerformArr[$i]['Performance'];
 		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
 		
 		
 		## PIC
 		$ClubPICArr = $libenroll->GET_GROUPSTAFF($clubPerformArr[$i]['EnrolGroupID'],'PIC');
 		$ClubPICName= array();
 		for($k=0;$k<sizeof($ClubPICArr);$k++){
 			$ClubPICID =$ClubPICArr[$k]['UserID'];
 			$ClubPICNameAry = $libenroll->Get_User_Info_By_UseID($ClubPICID);
 			$ClubPICName[] = Get_Lang_Selection($ClubPICNameAry[0]['ChineseName'], $ClubPICNameAry[0]['EnglishName']);
 			
 		}
 		$thisPICNameList = implode(',',$ClubPICName);
 		
 		$exportStudentViewClubArr[$studentID][$index_i][] = Get_Lang_Selection($clubPerformArr[$i]['TitleChinese'], $clubPerformArr[$i]['Title']);
    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisRole;
    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisAttendance;
    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisPerformance;
    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisEmail;
    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisPICNameList;
    	$index_i++;
    	
   
 	}
 
 	
	################################## Enf of Club Table ##################################
	
	################################## Activity Table #####################################
	// Get related club title
 	$name = $intranet_session_language=="en" ? "Title" : "TitleChinese";
	$targetClubSql = " IF(d.$name='' OR d.$name is null,'-',d.$name) as RelatedGroup";
	$targetClubTable = " Left Outer Join INTRANET_ENROL_GROUPINFO as c On (b.EnrolGroupID = c.EnrolGroupID)
						 Left Outer Join INTRANET_GROUP as d On (c.GroupID = d.GroupID)";
	
	$studentActivityArr = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($studentID);
	$enrolEventIdAry = Get_Array_By_Key($studentActivityArr, 'EnrolEventID');
 	$sql = "SELECT b.EventTitle, a.Performance, a.EnrolEventID, u.UserEmail, a.RoleTitle as Role, $targetClubSql
 			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID
			$targetClubTable
			INNER JOIN INTRANET_USER as u ON u.UserID = a.StudentID
 			WHERE a.StudentID = '$studentID'  AND a.RecordStatus = 2 AND a.EnrolEventID!='' And a.EnrolEventId In ('".implode("','", (array)$enrolEventIdAry)."')
 			ORDER BY b.EventTitle
 			";
 	$actPerformArr = $libenroll->returnArray($sql,2);
   	$numOfAct = sizeof($actPerformArr);
   	
 	//loop through each club record to construct table content
 	$totalPerform = 0;
 	for ($i=0; $i<$numOfAct; $i++)
 	{
 		# Role
 		$thisRole = $actPerformArr[$i]['Role'];
 		
 		#Email
 		$thisEmail = $actPerformArr[$i]['UserEmail'];
 		
 		# Attendance
 		$ParDataArr['EnrolEventID'] = $actPerformArr[$i]['EnrolEventID'];
 		$ParDataArr['StudentID'] = $studentID;
 		$thisAttendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($ParDataArr)."%";
 		
 		# Performance
 		$thisPerformance = $actPerformArr[$i]['Performance'];
 		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
	 				
 		
 		##PIC
 		$EventPICArr = $libenroll->GET_EVENTSTAFF($actPerformArr[$i]['EnrolEventID'],'PIC');
 		$EventPICName = array();
 		for($k=0;$k<sizeof($EventPICArr);$k++){
 			$EventPICID = $EventPICArr[$k]['UserID'];
 			$EventPICNameAry = $libenroll->Get_User_Info_By_UseID($EventPICID);
 			$EventPICName[] = Get_Lang_Selection($EventPICNameAry[0]['ChineseName'],$EventPICNameAry[0]['EnglishName']);
 		}
 		$thisPICNameList= implode(', ',$EventPICName);
 		
	    $exportStudentViewActivityArr[$studentID][$i][] = $actPerformArr[$i]['EventTitle'];
	    $exportStudentViewActivityArr[$studentID][$i][] = $thisRole;
	    $exportStudentViewActivityArr[$studentID][$i][] = $thisAttendance;
	    $exportStudentViewActivityArr[$studentID][$i][] = $thisPerformance;
	    $exportStudentViewActivityArr[$studentID][$i][] = $thisEmail;
	    
	    // Set related group if $display_belong__to_group is set
	    if($display_belong__to_group){
 			$exportStudentViewActivityArr[$studentID][$i][] = $actPerformArr[$i]['RelatedGroup'];
 		} else {
 			$exportStudentViewActivityArr[$studentID][$i][] = "";
 		}
 		$exportStudentViewActivityArr[$studentID][$i][] = $thisPICNameList;
		
 	}
 	
	################################## End of Activity Table ##############################
}


################################################################
//construct information above the table
$printed_std = array();
$iCount = 0;

foreach($studentNameAry as $k => $d) {
	$student_id = $d['UserID'];
	
	if(in_array($student_id,$printed_std)) continue;
	
	$student_name = strip_tags($d['StudentName']);
	$student_classname = $d['ClassName'];
	$student_classnumber = $d['ClassNumber'];
	
	# Club Table
	if($display_club) {
		if($SizeOfClub = count($exportStudentViewClubArr[$student_id])) {
			for ($k=0; $k<$SizeOfClub; $k++) {
				list($thisTitle, $thisRole, $thisAttendance, $thisPerformance,$thisEmail,$thisPICNameList) = $exportStudentViewClubArr[$student_id][$k];
				if($k == 0){
					$dataAry[$iCount][] = $student_classname;
					$dataAry[$iCount][] = $student_classnumber;
					$dataAry[$iCount][] = $student_name;
					if($display_email){
						$dataAry[$iCount][] = $thisEmail;
					}
					$dataAry[$iCount][] = $Lang['eEnrolment']['Club'];
				} else {
					$dataAry[$iCount][] = "";
					$dataAry[$iCount][] = "";
					$dataAry[$iCount][] = "";
					if($display_email){
						$dataAry[$iCount][] = "";
					}
					$dataAry[$iCount][] = "";
				}
				$dataAry[$iCount][] = $thisTitle;
				// Group table no need to show related group
				if($display_belong__to_group){
 					$dataAry[$iCount][] = " - ";
 				}
				if ($display_role) {
					$dataAry[$iCount][] = $thisRole;
				}
				if ($display_attendance) {
					$dataAry[$iCount][] = $thisAttendance;
				}
				if ($display_performance) {
					$dataAry[$iCount][] = $thisPerformance;
				}
				if($display_picname){
					$dataAry[$iCount][] = $thisPICNameList;
				}
				$iCount++;
			}
		}
	}
	
 	# Activity Table
 	if($display_activity) {
		if($SizeOfAct = count($exportStudentViewActivityArr[$student_id])) {
			for ($i=0; $i<$SizeOfAct; $i++) {
				list($thisTitle, $thisRole, $thisAttendance, $thisPerformance,$thisEmail, $thisRelatedGroup,$thisPICNameList) = $exportStudentViewActivityArr[$student_id][$i];
				if($i == 0){
					$dataAry[$iCount][] = $student_classname;
					$dataAry[$iCount][] = $student_classnumber;
					$dataAry[$iCount][] = $student_name;
					
					if ($display_email) {
					$dataAry[$iCount][] = $thisEmail;
					}
					$dataAry[$iCount][] = $Lang['eEnrolment']['Activity'];
				} else {
					$dataAry[$iCount][] = "";
					$dataAry[$iCount][] = "";
					$dataAry[$iCount][] = "";
					if($display_email){
						$dataAry[$iCount][] = "";
					}
					$dataAry[$iCount][] = "";
				}

				$dataAry[$iCount][] = $thisTitle;
				// Add data to file
				if($display_belong__to_group){
					$dataAry[$iCount][] = $thisRelatedGroup;				
				}
				if ($display_role) {
					$dataAry[$iCount][] = $thisRole;
				}
				if ($display_attendance) {
					$dataAry[$iCount][] = $thisAttendance;
				}
				if ($display_performance) {
					$dataAry[$iCount][] = $thisPerformance;
				}
				
				if($display_picname){
					$dataAry[$iCount][] = $thisPICNameList;
				}
				$iCount++;
			}
		}
	}
 	

 	#R81016 by Omas 20150817
 	if($SizeOfClub==0&&$SizeOfAct==0){
 		$dataAry[$iCount][] = $student_classname;
		$dataAry[$iCount][] = $student_classnumber;
		$dataAry[$iCount][] = $student_name;
		$dataAry[$iCount][] = '';
		$dataAry[$iCount][] = '';
		if($display_belong__to_group){
			$dataAry[$iCount][] = '';				
		}
		if ($display_role) {
			$dataAry[$iCount][] = '';
		}
		if ($display_attendance) {
			$dataAry[$iCount][] = '';
		}
		if ($display_performance) {
			$dataAry[$iCount][] = '';
		}
		if ($display_email) {
			$dataAry[$iCount][] = '';
		}
		if ($display_picname) {
			$dataAry[$iCount][] = '';
		}
		$iCount++; 
 	}
 	
 	$printed_std[] = $student_id;
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "enrolment_summary_detailed.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>