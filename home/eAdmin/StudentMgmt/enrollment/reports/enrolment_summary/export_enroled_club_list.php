<?php
# using: 

############## Change Log [start] ####
#
#   Date:   2020-03-12  Tommy
#           added overall mark for Case #Q178421
#
#   Date:   2020-03-10  Tommy
#           added semester mark for Case #Q178421
#
#	Date:	2017-01-19	Omas
#			Q111907 as request changed the portion - add back flag to control column grade => $sys_custom['eEnrolment']['heungto_enrolment_report'] 
#
#	Date:	2017-01-18	Villa
#			B111852 Fix - cannot export specific term problem
#
#	Date:	2015-05-26	Omas #J31222 
#			remove flag $sys_custom['eEnrolment']['heungto_enrolment_report'] to general deploy the feature
#
#	Date:	2011-10-11	YatWoon
#			Customization: $sys_custom['eEnrolment']['heungto_enrolment_report']
#
########################################


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();


if ($plugin['eEnrollment'])
{
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$lay = new academic_year($AcademicYearID);
	$TermInfoAry = $lay->Get_Term_List(0);
	$isLastTerm = $TermInfoAry[0]['YearTermID'] == $Semester ? 0 : 1;

	# defince score
	if($sys_custom['eEnrolment']['heungto_enrolment_report']){
    	$score_ary = array( "A"=>6,
                    	    "B"=>5,
                    	    "C"=>4,
                    	    "D"=>3,
                    	    "E"=>2,
                    	    "F"=>1,
    	                    "U"=>0
    	);
	}else{
	    $score_ary = array( "A"=>5,
                	        "B"=>4,
                	        "C"=>3,
                	        "D"=>2,
                	        "E"=>1,
                	        "F"=>0
	    );
	}
	$libenroll = new libclubsenrol();
	
	$studentIDArr = array();
	$studentNameArr = array();
	if($showTarget=="class" && isset($targetClassID) && $targetClassID!="") 
	{
		$studentNameArr = array();
		foreach((array)$targetClassID as $thistargetClassID)
		{
			$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
			$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^',1);
			$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
		}
		$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
	}

	$libenroll = new libclubsenrol();
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC()))
		header("Location: ../../enrollment/");

	$exportStudentViewClubArr = array();
 	$exportStudentViewActivityArr = array();
 	
 	######################################################################
 	######################################################################
 	######################################################################
 	if($isLastTerm)
 	{
// 	 	$StudentTerm1Total = array();
	 	$StudentTerm1Avg = array();
	 	
	 	# need to calculate first term data 
	 	$firstTerm = $TermInfoAry[0]['YearTermID'];
	 	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', array($firstTerm));
     	$term_group_list = array();
     	if(!empty($term_group_list_temp))
     	{
	     	foreach($term_group_list_temp as $k=>$d)
	     		$term_group_list[] = $d['EnrolGroupID'];
     	}
     	
     	foreach($studentIDArr as $k=>$studentID)
		{
			$thisStudentTotal = 0;
			$thisStudentClubNo = 0;
			
	     	//get all groups of student
	     	$title_order = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
	     	$sql = "SELECT b.Title, a.Performance, a.EnrolGroupID, c.Title as Role, b.TitleChinese
	     			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
	     			LEFT JOIN INTRANET_ROLE as c ON c.RoleID = a.RoleID
	     			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
	     			ORDER BY ". $title_order;
	     			
	     	$clubPerformArr = $libenroll->returnArray($sql);
	     	$numOfClub = sizeof($clubPerformArr);
	     	
	     	//loop through each club record to construct table content
			$StudentTerm1ClubNo[$studentID] = 0;
	     	for ($i=0; $i<$numOfClub; $i++)
	     	{
		     	//if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
		     	if(isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
				{
					continue;
				}
				$thisStudentClubNo++;
				
	     		# Performance
	     		$thisPerformance = $clubPerformArr[$i]['Performance'];
	     		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
		    	$thisStudentTotal += $score_ary[strtoupper($thisPerformance)];
	     	}
	     	
	     	if($thisStudentClubNo)
	     	{
				$StudentTerm1Avg[$studentID] = ceil($thisStudentTotal/$thisStudentClubNo);
     		}
		}
 	}
 	######################################################################
 	######################################################################
 	######################################################################
 	 
 	if(isset($Semester))
 	{
     	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', (array)$Semester);
     	$term_group_list = array();
     	if(!empty($term_group_list_temp))
     	{
	     	foreach($term_group_list_temp as $k=>$d)
	     		$term_group_list[] = $d['EnrolGroupID'];
     	}
 	}
			     	
	foreach($studentIDArr as $k=>$studentID)
	{
		$studentView = true;
	   
	   ################################## Club Table ##################################
     	//get all groups of student
     	$title_order = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
     	$sql = "SELECT b.Title, a.Performance, a.EnrolGroupID, c.Title as Role, b.TitleChinese
     			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
     			LEFT JOIN INTRANET_ROLE as c ON c.RoleID = a.RoleID
     			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
     			ORDER BY ". $title_order;
     	$clubPerformArr = $libenroll->returnArray($sql);
     	$numOfClub = sizeof($clubPerformArr);
     	
     	//loop through each club record to construct table content
     	$index_i = 0;
     	for ($i=0; $i<$numOfClub; $i++)
     	{
	     	//if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
	     	if(isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
			{
				continue;
			}
			
     		# Role
     		$thisRole = $clubPerformArr[$i]['Role'];
     		
     		# Attendance
     		$ParDataArr['EnrolGroupID'] = $clubPerformArr[$i]['EnrolGroupID'];
     		$ParDataArr['StudentID'] = $studentID;
     		$thisAttendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($ParDataArr)."%";
     		
     		# Performance
     		$thisPerformance = $clubPerformArr[$i]['Performance'];
     		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
			$exportStudentViewClubArr[$studentID][$index_i][] = $intranet_session_language=="en" ? $clubPerformArr[$i]['Title'] : $clubPerformArr[$i]['TitleChinese'];
	    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisRole;
	    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisAttendance;
	    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisPerformance;
	    	$index_i++;
     	}
 		################################## Enf of Club Table ##################################
	}
 		
	################################################################
	$dataAry = array();
	$export_header = array($i_UserClassName,$i_UserClassNumber,$i_UserStudentName);
	$max_club_no = 0;
	foreach($studentNameArr as $k=>$d)
	{
		$row = array();
		$student_id = $d['UserID'];
		
		$student_name = $d['StudentName'];
		$student_classname = $d['ClassName'];
		$student_classnumber = $d['ClassNumber'];
		
		$row = array($student_classname, $student_classnumber, $student_name);
		
		$row2 = array();
		$total_score = 0;
		if($SizeOfClub = count($exportStudentViewClubArr[$student_id]))
		{
			for ($k=0; $k<$SizeOfClub; $k++)
			{
				list($thisTitle, $thisRole, $thisAttendance, $thisPerformance) = $exportStudentViewClubArr[$student_id][$k];
				$row2[] = $thisTitle;
				$row2[] = $thisPerformance;
				$total_score  += $score_ary[strtoupper($thisPerformance)];
			}
		}
		
		if($SizeOfClub)
		{
			$avg_score = ceil($total_score/$SizeOfClub);
			$total_grade = array_keys($score_ary, $avg_score);
			$this_term_grade = $total_grade[0];
		}
		else
		{
			$avg_score = 0;
			if($sys_custom['eEnrolment']['heungto_enrolment_report']){
			     $this_term_grade = "U";
			}else{
			     $this_term_grade = "F";
			}
		}
		
		if($sys_custom['eEnrolment']['heungto_enrolment_report'] && $sys_custom['eEnrolment']['Refresh_Performance']){
    		switch ($this_term_grade){
    		    case "A":
    		        $_mark = 95;
    		        break;
    		    case "B":
    		        $_mark = 85;
    		        break;
    		    case "C":
    		        $_mark = 75;
    		        break;
    		    case "D":
    		        $_mark = 65;
    		        break;
    		    case "E":
    		        $_mark = 55;
    		        break;
    		    case "F":
    		        $_mark = 40;
    		        break;
    		    case "U":
    		        $_mark = 0;
    		        break;
    		}
		}
		
		if($sys_custom['eEnrolment']['heungto_enrolment_report']){
			if($isLastTerm)
			{
				//$year_avg_score = ceil(($StudentTerm1Avg[$student_id] + $avg_score)/2);
				$year_avg_score = ceil($StudentTerm1Avg[$student_id] *0.4 + $avg_score *0.6);
				$year_grade = array_keys($score_ary, $year_avg_score);
				$row[] = $year_grade[0];
				
				if($sys_custom['eEnrolment']['heungto_enrolment_report'] && $sys_custom['eEnrolment']['Refresh_Performance']){
				    switch ($year_grade[0]){
				        case "A":
				            $_yearMark = 95;
				            break;
				        case "B":
				            $_yearMark = 85;
				            break;
				        case "C":
				            $_yearMark = 75;
				            break;
				        case "D":
				            $_yearMark = 65;
				            break;
				        case "E":
				            $_yearMark = 55;
				            break;
				        case "F":
				            $_yearMark = 40;
				            break;
				        case "U":
				            $_yearMark = 0;
				            break;
				    }
				    $row[] = $_yearMark;
				}
			}
			$row[] = $this_term_grade;
			if($sys_custom['eEnrolment']['Refresh_Performance']){
			    $row[] = $_mark;
			}
		}
		
		$row = array_merge($row, $row2);
		$max_club_no = $SizeOfClub > $max_club_no ? $SizeOfClub : $max_club_no;

		$dataAry[] = $row;
 	}
	################################################################
	
 	if($sys_custom['eEnrolment']['heungto_enrolment_report']){
		# whole year grade
		if($isLastTerm)
		{
			$export_header[] = $Lang['eEnrolment']['OverallGrade'];
			
			if($sys_custom['eEnrolment']['heungto_enrolment_report'] && $sys_custom['eEnrolment']['Refresh_Performance'])
			    $export_header[] = $Lang['eEnrolment']['OverallMark'];
		}
		
		# Term grade
		$export_header[] = $Lang['eEnrolment']['SemesterGrade'];
		if($sys_custom['eEnrolment']['Refresh_Performance']){
		      $export_header[] = $Lang['eEnrolment']['SemesterMark'];
		}
 	}
 	
	for($i=0;$i<$max_club_no;$i++)
	{
		$export_header[] = $eEnrollmentMenu['club'];
		$export_header[] = $eEnrollment['performance'];
	}
		
	$le = new libexporttext();
	$export_text = $le->GET_EXPORT_TXT($dataAry, $export_header);
    $filename = "enroled_club_list.csv";
    $le->EXPORT_FILE($filename,$export_text); 
	
	intranet_closedb();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>