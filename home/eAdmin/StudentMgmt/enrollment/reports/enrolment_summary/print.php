<?php

############## Change Log [start] ####
#
#	Date:	2011-02-02	Marcus
#			cater multi selection
#############################################


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	$linterface = new interface_html("popup.html");
	$libenroll = new libclubsenrol();
	$libenroll_ui = new libclubsenrol_ui();
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC()))
		header("Location: ../../enrollment/");
		
	$applyPageBreak = $_POST['simpleApplyPageBreak'];
	if ($applyPageBreak) {
		$pageBreakStyle = 'page-break-after:always;';
	}
	
	if ($PrintType=="class_view")
	{
		$GroupStudentAssoc = unserialize(rawurldecode($exportGroupStudentAssoc));
		
		$DateRowHTML = "";
		if ($usePeriod == "on") {
			$DateRowHTML .= "<tr class=\"tabletext\"align=\"left\">";
				$DateRowHTML .= "<td>".$eEnrollment['Period']."</td>";
				$DateRowHTML .= "<td width=\"10\"></td>";
				$DateRowHTML .= "<td>".$i_From." ".$textFromDate." ".$i_To." ".$textToDate."</td>";
			$DateRowHTML .= "</tr>";
		}
		
		//construct table title
		$header = "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		$header .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
			<td width=10 class=\"eSporttdborder eSportprinttabletitle\">#</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassName</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassNumber</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollmentMenu['club']."</td>
			<td class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollmentMenu['activity']."</td>
		</tr>\n";

		$studentInfoArr = unserialize(rawurldecode($studentInfoArr));
		$tempClub = $libenroll->Get_All_Club_Info();
       	$ClubInfoAssoc = BuildMultiKeyAssoc($tempClub,"EnrolGroupID");
		
		foreach((array)$GroupStudentAssoc as $ClassClubID => $thisStudentIDArr)
		{
			$thisStudentIDArr = array_values($thisStudentIDArr);
		
			//construct information above the table
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr><td>";
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
		    	$x .= "<tr class=\"tabletext\"align=\"left\"><td>{$eEnrollment['Target']} : </td><td width=\"10\"></td><td>";
		    	
		    	if($showTarget=="class") {
		    		$YearClassObj = new year_class($ClassClubID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=false);
		    		$targetClass = $YearClassObj->Get_Class_Name();
	    		
			    	$x .= $targetClass;
	    		} else {
		    		//$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID=$targetClub";
		    		//$result = $libenroll->returnVector($sql);
//		    		$ClubInfoArr = $libenroll->GET_GROUPINFO($ClassClubID);
		    		$x .= $ClubInfoAssoc[$ClassClubID]['TitleWithSemester'];
	    		}
		    	$x .= "</td></tr>\n";
		    	$x .= $DateRowHTML;
		    $x .= "</table>\n";
		    $x .= "</td></tr>";
			$x .= "</table>\n";
		 	
		 	$x .= $header ;
		 		
//		 	$studentIDArr = unserialize(rawurldecode($studentIDArr));
			$studentIDArr = $thisStudentIDArr;

			
			//loop through all students to get performance and construct table contents
		 	for ($i=0; $i<sizeOf($studentIDArr); $i++)
		 	{
		     	$studentID = $studentIDArr[$i];
		     	$className = $studentInfoArr[$studentID]['ClassName'];
		     	$classNumber = $studentInfoArr[$studentID]['ClassNumber'];
		     	$studentName = $studentInfoArr[$studentID]['StudentName'];
		     	$clubInfoArr = $studentInfoArr[$studentID]['ClubInfoArr'];
		     	$activityInfoArr = $studentInfoArr[$studentID]['ActivityInfoArr'];
		     	
		     	$thisClubTitleArr = array();
		     	$thisActivityTitleArr = array();
		     	
		     	for ($j=0; $j<count($clubInfoArr); $j++)
		     	{
			     	$thisTitle = $clubInfoArr[$j][1];
			     	$thisClubTitleArr[] = $thisTitle;
		     	}
		     	for ($j=0; $j<count($activityInfoArr); $j++)
		     	{
			     	$thisTitle = $activityInfoArr[$j][1];
			     	$thisActivityTitleArr[] = $thisTitle;
		     	}
		     	
		     	$clubDisplay = implode("<br />", $thisClubTitleArr);
		     	$activityDisplay = implode("<br />", $thisActivityTitleArr);
		     	
		     	//construct content of the table
		     	$count = $i+1;
		        $css = (($i%2)+1);
		        $x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">
		        		<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
						<td class=\"eSporttdborder eSportprinttext\">{$className}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$classNumber}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$studentName}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$clubDisplay}&nbsp</td>
		        		<td class=\"eSporttdborder eSportprinttext\">{$activityDisplay}&nbsp</td>
		        		</tr>\n";
		 	}
		 	if (sizeof($studentIDArr)==0)
		    {
		        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"6\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
		    }
		    $x .= "</table>\n";
	    	$x .= '<div style="'.$pageBreakStyle.'">'.$libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent')).'<br /></div>'."\n\n";
		}
	}
	else if ($PrintType=="student_view")
	{
		$exportStudentViewClubArr = unserialize(rawurldecode($exportStudentViewClubArr));
		$exportStudentViewActivityArr = unserialize(rawurldecode($exportStudentViewActivityArr));
		
		$targetClass = $_POST['targetClass'];
		$studentName = $_POST['studentName'];
		
		//debug_r($_POST);
		//debug_r($exportStudentViewClubArr);
		//debug_r($exportStudentViewActivityArr);
		
		//construct information above the table
		$x = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
		$x .= "<tr><td>";
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
	    	$x .= "<tr class=\"tabletext\" align=\"left\"><td>{$i_general_class}</td><td width=\"10\"></td><td>$targetClass</td></tr>\n";
	    	$x .= "<tr class=\"tabletext\" align=\"left\"><td>{$i_UserStudentName}</td><td width=\"10\"></td><td>$studentName</td></tr>\n";
	    $x .= "</table>\n";
	    $x .= "</td></tr>";
		$x .= "</table>\n";
		
		$x .= "<br />\n";
		
		# Club Table
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr class=\"tabletext\"><td>".$eEnrollment['club']."</td></tr>";
			$x .= "<tr>";
				$x .= "<td>";
					//construct table title
					$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
						$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
									<td width=\"10\"  class=\"eSporttdborder eSportprinttabletitle\">#</td>
									<td width=\"40%\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['club_name']."</td>
									<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['role']."</td>
									<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['attendence']."</td>
									<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['performance']."</td>
								</tr>\n";
						if($SizeOfClub=count($exportStudentViewClubArr))
						{
							for ($i=0; $i<$SizeOfClub; $i++)
							{
								list($thisTitle, $thisRole, $thisAttendance, $thisPerformance) = $exportStudentViewClubArr[$i];
								
								$count = $i+1;
								$css = (($i%2)+1);
								$x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$thisTitle}&nbsp</td>";
									$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisRole}&nbsp</td>";
									$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisAttendance}&nbsp</td>";
									$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisPerformance}&nbsp</td>";
								$x .= "</tr>";
							}
						}
						else
						{
							$x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
								$x .= "<td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">".$Lang['General']['NoRecordAtThisMoment']."</td>";
							$x .= "</tr>";
						}
					$x .= "</table>";
	 			$x .= "</td>";
	 		$x .= "</tr>";
	 	$x .= "</table>";
	 	
	 	$x .= "<br />";
	 	
	 	# Activity Table
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr class=\"tabletext\"><td>".$eEnrollment['activity']."</td></tr>";
			$x .= "<tr>";
				$x .= "<td>";
					//construct table title
					$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
						$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
									<td width=\"10\"  class=\"eSporttdborder eSportprinttabletitle\">#</td>
									<td width=\"40%\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['act_name']."</td>
									<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['role']."</td>
									<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['attendence']."</td>
									<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['performance']."</td>
								</tr>\n";
						if($SizeOfAct = count($exportStudentViewActivityArr))
						{
							for ($i=0; $i<$SizeOfAct; $i++)
							{
								list($thisTitle, $thisRole, $thisAttendance, $thisPerformance) = $exportStudentViewActivityArr[$i];
								
								$count = $i+1;
								$css = (($i%2)+1);
								$x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$thisTitle}&nbsp</td>";
									$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisRole}&nbsp</td>";
									$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisAttendance}&nbsp</td>";
									$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisPerformance}&nbsp</td>"; 
								$x .= "</tr>";
							}
						}
						else
						{
							$x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
								$x .= "<td colspan=\"5\" align=\"center\" class=\"eSporttdborder eSportprinttext\">".$Lang['General']['NoRecordAtThisMoment']."</td>";
							$x .= "</tr>";
						}
					$x .= "</table>";
	 			$x .= "</td>";
	 		$x .= "</tr>";
	 	$x .= "</table>";
	 	$x .= $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
	}
	
	################################################################
	
	?>
	<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
	</table>
	<?=$x?>
	
	<?
	//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	intranet_closedb();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>