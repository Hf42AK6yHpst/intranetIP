<?php
# using: 

############## Change Log [start] ####
#	Date:	2017-10-09	Anna
#			Add $display_picname
#
#	Date:	2017-03-29	Anna
#			Add UserEmail column
#
#	Date:	2015-08-27	Omas
#			Fix - cannot print all club problem
#
#	Date:	2015-05-26	Omas #J31222 
#			remove flag $sys_custom['eEnrolment']['heungto_enrolment_report'] to general deploy the feature
#
#	Date:	2014-07-23	Bill
#			Display related group in activity table
#
#	Date:	2012-02-15	YatWoon
#			Improved: add coding to display selected student only [Case#2011-0223-1816-24071]
#
#	Date:	2011-10-11	YatWoon 
#			Customization: $sys_custom['eEnrolment']['heungto_enrolment_report']
#
#	Date:	2011-02-02	Marcus
#			cater multi selection
########################################


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	
	$lclass = new libclass();
	$libenroll = new libclubsenrol();
	
// debug_pr($targetClass);
// debug_pr($targetClub);

$studentIDArr = array();
$studentNameArr = array();

$applyPageBreak = $_POST['detailedApplyPageBreak'];

if($showTarget=="class" && isset($targetClassID) && $targetClassID!="") 
{
	/*
	$studentIDArr = $lclass->getClassStudentListOrderByClassNo($targetClassID); //UserID
	$studentNameArr = $lclass->getStudentNameListByClassName($targetClassID);	//UserID, $name_field, ClassNumber
	*/
	$studentNameArr = array();
	foreach((array)$targetClassID as $thistargetClassID)
	{
		$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
		$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
		$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
	}
	$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
} else {
 	
 	/*
 	$name_field = getNameFieldByLang('USR.');
 	$sql = "SELECT USR.UserID, USR.ClassName, USR.ClassNumber, $name_field FROM INTRANET_USERGROUP USRGP 
 			LEFT OUTER JOIN INTRANET_USER USR ON (USRGP.UserID=USR.UserID) 
 			LEFT OUTER JOIN INTRANET_GROUP GP ON (GP.GroupID=USRGP.GroupID)
 			WHERE USRGP.EnrolGroupID=$targetClub AND USR.RecordType=2 AND USR.RecordStatus=1 And GP.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
 			ORDER BY USR.ClassName, USR.ClassNumber";
 	$studentInfo = $libenroll->returnArray($sql);
 	
 	for($i=0; $i<sizeof($studentInfo); $i++) {
		list($uID, $clsName, $clsNo, $name) = $studentInfo[$i];
		$studentIDArr[] = $uID;
		$studentNameArr[$i][] = $uID;
		$studentNameArr[$i][] = $name;
		$studentNameArr[$i][] = $clsName." - ".$clsNo;
 	}
 	*/
 	
 	$studentNameArr = $libenroll->Get_Club_Member_Info((array)$targetClub, $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
	$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
	$studentIDArr = array_unique($studentIDArr);
	
}

# Cater selected student only [Start] ###
if($selected_printing)
{
	$studentIDArr = $selected_student;
	$new_studentNameArr = array();
	
	foreach($studentNameArr as $k=>$d)
	{
		if(in_array($d['UserID'], $selected_student))
		{
			$new_studentNameArr[] = $d;
		}	
	}
	$studentNameArr = $new_studentNameArr;
}
# Cater selected student only [End] ###

	$linterface = new interface_html("popup.html");
	$libenroll = new libclubsenrol();
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC()))
		header("Location: ../../enrollment/");
	
		$exportStudentViewClubArr = array();
     	$exportStudentViewActivityArr = array();

     	if($Semester == -999)
			$Semester = '';
     	if(isset($Semester))
     	{
	     	//$term_group_list_temp = $libenroll->getGroupInfoList(0, $Semester, $EnrolGroupIDArr='', $getRegOnly=0, $ParCond="", $AcademicYearID);
//	     	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', array($Semester));
     		$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', (array)$Semester);
     		$term_group_list = array();
	     	if(!empty($term_group_list_temp))
	     	{
		     	foreach($term_group_list_temp as $k=>$d)
		     		$term_group_list[] = $d['EnrolGroupID'];
	     	}
     	}
   //  	debug_pr($term_group_list);
			     	
	foreach($studentIDArr as $k=>$studentID)
	{
		$studentView = true;
	   
	   ################################## Club Table ##################################
     	//get all groups of student
//      	$title_order = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
//      	$sql = "SELECT b.Title, b.TitleChinese, a.Performance, a.EnrolGroupID, c.Title as Role, u.UserEmail
//      			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
//      			LEFT JOIN INTRANET_ROLE as c ON c.RoleID = a.RoleID
//      			INNER JOIN INTRANET_USER as u ON u.UserID =a.UserID
//      			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
//      			ORDER BY ". $title_order;
//      	$clubPerformArr = $libenroll->returnArray($sql);
//      	$numOfClub = sizeof($clubPerformArr);
		$title_order = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
		$sql = "SELECT b.Title, a.Performance, a.EnrolGroupID, c.Title as Role, b.TitleChinese
		FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
		LEFT JOIN INTRANET_ROLE as c ON c.RoleID = a.RoleID
		WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
 			ORDER BY ". $title_order;
		$clubPerformArr = $libenroll->returnArray($sql);
		$numOfClub = sizeof($clubPerformArr);
		

     	//loop through each club record to construct table content
  		$index_i = 0;

     	for ($i=0; $i<$numOfClub; $i++)
     	{   
     
	     	//if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
	     	if(isset($Semester) && $Semester!="" && !in_array($clubPerformArr[$i]['EnrolGroupID'], $term_group_list))
			{
				continue;
			}

			# Role
     		$thisRole = $clubPerformArr[$i]['Role'];
    
     		# Attendance
     		$ParDataArr['EnrolGroupID'] = $clubPerformArr[$i]['EnrolGroupID'];
     		$ParDataArr['StudentID'] = $studentID;
     		$thisAttendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($ParDataArr)."%";
     		
     		# Performance
     		$thisPerformance = $clubPerformArr[$i]['Performance'];
     		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
     		
     		#Email
     	//	$thisEmail =$clubPerformArr[$i]['UserEmail'];
     		
     		
     		##PIC
     		$ClubPICArr = $libenroll->GET_GROUPSTAFF($clubPerformArr[$i]['EnrolGroupID'],'PIC');
     		$ClubPICName= array();
     		for($k=0;$k<sizeof($ClubPICArr);$k++){
     			$ClubPICID =$ClubPICArr[$k]['UserID'];
     			$ClubPICNameAry = $libenroll->Get_User_Info_By_UseID($ClubPICID);
     			$ClubPICName[] = Get_Lang_Selection($ClubPICNameAry[0]['ChineseName'], $ClubPICNameAry[0]['EnglishName']);
     			
     		}
     		$thisPICNameList = implode(',',$ClubPICName);
     		
		//	$exportStudentViewClubArr[$studentID][$index_i][] = $thisTitle;
     		$exportStudentViewClubArr[$studentID][$index_i][] = Get_Lang_Selection($clubPerformArr[$i]['TitleChinese'], $clubPerformArr[$i]['Title']);
     		$exportStudentViewClubArr[$studentID][$index_i][] = $thisRole;
	    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisAttendance;
	    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisPerformance;
	//    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisEmail;
	    	$exportStudentViewClubArr[$studentID][$index_i][] = $thisPICNameList;
	    	$index_i++;
	 
     	}

 		################################## Enf of Club Table ##################################
 		


 		################################## Activity Table #####################################
 		//get all activities of student
 		$studentActivityArr = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($studentID);
 		$enrolEventIdAry = Get_Array_By_Key($studentActivityArr, 'EnrolEventID');
 		
 		// Get related club title
 		$name = $intranet_session_language=="en" ? "Title" : "TitleChinese";
		$targetClubSql = " IF(d.$name='' OR d.$name is null,'-',d.$name) as RelatedGroup";
		$targetClubTable = " Left Outer Join INTRANET_ENROL_GROUPINFO as c On (b.EnrolGroupID = c.EnrolGroupID)
							 Left Outer Join INTRANET_GROUP as d On (c.GroupID = d.GroupID)";
 		
     	$sql = "SELECT b.EventTitle, a.Performance, a.EnrolEventID, a.RoleTitle as Role, $targetClubSql
     			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID
				$targetClubTable
			
     			WHERE a.StudentID = '$studentID'  AND a.RecordStatus = 2 AND a.EnrolEventID!='' And a.EnrolEventId In ('".implode("','", (array)$enrolEventIdAry)."')
     			ORDER BY b.EventTitle
     			";
     	$actPerformArr = $libenroll->returnArray($sql,2);
	   	$numOfAct = sizeof($actPerformArr);
	   	
     	//loop through each club record to construct table content
     	$totalPerform = 0;
     	for ($i=0; $i<$numOfAct; $i++)
     	{
     		# Role
     		$thisRole = $actPerformArr[$i]['Role'];
     		
     		# Attendance
     		$ParDataArr['EnrolEventID'] = $actPerformArr[$i]['EnrolEventID'];
     		$ParDataArr['StudentID'] = $studentID;
     		$thisAttendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($ParDataArr)."%";
     		
     		# Performance
     		$thisPerformance = $actPerformArr[$i]['Performance'];
     		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
		 		
     		#Email
     	//	$thisEmail= $actPerformArr[$i-1]['UserEmail'];
     		 
     		##PIC
     		$EventPICArr = $libenroll->GET_EVENTSTAFF($actPerformArr[$i]['EnrolEventID'],'PIC');
     		$EventPICName = array();
     		for($k=0;$k<sizeof($EventPICArr);$k++){
     			$EventPICID = $EventPICArr[$k]['UserID'];
     			$EventPICNameAry = $libenroll->Get_User_Info_By_UseID($EventPICID);
     			$EventPICName[] = Get_Lang_Selection($EventPICNameAry[0]['ChineseName'],$EventPICNameAry[0]['EnglishName']);
     		}
     		$thisPICNameList= implode(', ',$EventPICName);
     		
		    $exportStudentViewActivityArr[$studentID][$i][] = $actPerformArr[$i]['EventTitle'];
		    $exportStudentViewActivityArr[$studentID][$i][] = $thisRole;
		    $exportStudentViewActivityArr[$studentID][$i][] = $thisAttendance;
		    $exportStudentViewActivityArr[$studentID][$i][] = $thisPerformance;
		//    $exportStudentViewActivityArr[$studentID][$i][] = $thisEmail;
		    $exportStudentViewActivityArr[$studentID][$i][] = $actPerformArr[$i]['RelatedGroup'];
		    $exportStudentViewActivityArr[$studentID][$i][] = $thisPICNameList;
     	}
 		################################## End of Activity Table ##############################
	}
 		
	################################################################
	//construct information above the table
	$page_count = 0;
	$printed_std = array();
	foreach($studentNameArr as $k=>$d)
	{
//		list($student_id, $student_name, $student_classnumber) = $d;
		
		$student_id = $d['UserID'];
		
		if(in_array($student_id,$printed_std)) continue;
		
		$student_name = $d['StudentName'];
		$student_classname = $d['ClassName'];
		$student_classnumber = $d['ClassNumber'];
		$sql = "SELECT UserEmail
				FROM INTRANET_USER 
				WHERE UserID = '$student_id'
				";
		$EmailResult = $libenroll->returnArray($sql,1);

		$Email = $EmailResult[0]['UserEmail'];
// 	/	debug_pr($EmailResult);
		
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
		$x .= "<tr><td>";
		$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
	    	$x .= "<tr class=\"tabletext\" align=\"left\"><td>{$i_general_class}</td><td width=\"10\"></td><td>$student_classname</td></tr>\n";
	    	$x .= "<tr class=\"tabletext\" align=\"left\"><td>{$i_UserStudentName}</td><td width=\"10\"></td><td>$student_name</td></tr>\n";
	    	if($display_email){
	    		$x .= "<tr class=\"tabletext\" align=\"left\"><td>{$Lang['eEnrolment']['show_email']}</td><td width=\"10\"></td><td>$Email</td></tr>\n";
	    	}
	    $x .= "</table>\n";
	    $x .= "</td></tr>";
		$x .= "</table>\n";
		
		$x .= "<br />\n";
		
		# Club Table
		if($display_club)
		{
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr class=\"tabletext\"><td>".$eEnrollment['club']."</td></tr>";
			$x .= "<tr>";
				$x .= "<td>";
					//construct table title
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
						$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">";
						$x .= "<td width=\"3%\"  class=\"eSporttdborder eSportprinttabletitle\">#</td>";
						$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['club_name']."</td>";
						if($display_role)
							$x .= "<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['role']."</td>";
						if($display_attendance)
							$x .= "<td width=\"20%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['attendence']."</td>";
						if($display_performance)
							$x .= "<td width=\"30%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['performance']."</td>";
// 						if($display_email)
// 							$x .= "<td width=\"30%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['show_email']."</td>";
						if($display_picname)
							$x .= "<td width=\"30%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['ClubPIC']."</td>";
							
						$x .= "</tr>\n";
												
						if($SizeOfClub = count($exportStudentViewClubArr[$student_id]))
						{
							for ($k=0; $k<$SizeOfClub; $k++)
							{
								list($thisTitle, $thisRole, $thisAttendance, $thisPerformance,$thisPICNameList) = $exportStudentViewClubArr[$student_id][$k];
								$count = $k+1;
								$css = (($k%2)+1);
								$x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$thisTitle}&nbsp</td>";
									if($display_role)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisRole}&nbsp</td>";
									if($display_attendance)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisAttendance}&nbsp</td>";
									if($display_performance)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisPerformance}&nbsp</td>";
									//if($display_email)
									//	$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisEmail}&nbsp</td>";										
									if($display_picname)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisPICNameList}&nbsp</td>";
										
									$x .= "</tr>";
							}
						}
						else
						{
							$Colspan=2;
							if($display_role) $Colspan++;
							if($display_attendance) $Colspan++;
							if($display_performance) $Colspan++;
						//	if($display_email) $Colspan++;
							if($display_picname) $Colspan++;
							$x .= "<tr>";
								$x .= "<td colspan=\"$Colspan\" align=\"center\" class=\"eSporttdborder eSportprinttext\">".$Lang['General']['NoRecordAtThisMoment']."</td>"; 
							$x .= "</tr>";
						}
					$x .= "</table>";
	 			$x .= "</td>";
	 		$x .= "</tr>";
	 		$x .= "</table>";
 		}
 		
 		if($display_club && $display_activity)
	 	$x .= "<br />";
	 	
	 	# Activity Table
	 	if($display_activity)
	 	{
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr class=\"tabletext\"><td>".$eEnrollment['activity']."</td></tr>";
			$x .= "<tr>";
				$x .= "<td>";
					//construct table title
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
						$x .= "<tr class=\"eSporttdborder eSportprinttabletitle\">";
						$x .= "<td width=\"3%\"  class=\"eSporttdborder eSportprinttabletitle\">#</td>";
						$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['act_name']."</td>";
						// Column for related group
						if($display_belong__to_group)
							$x .= "<td width=\"26%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['belong_to_group']."</td>";
						if($display_role)
							$x .= "<td width=\"16%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['role']."</td>";
						if($display_attendance)
							$x .= "<td width=\"14%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['attendence']."</td>";
						if($display_performance)
							$x .= "<td width=\"14%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$eEnrollment['performance']."</td>";
// 						if($display_email)
// 							$x .= "<td width=\"14%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['show_email']."</td>";
						if($display_picname)
							$x .= "<td width=\"14%\" align=\"center\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['ClubPIC']."</td>";
						$x .= "</tr>\n";
						
						if($SizeOfAct = count($exportStudentViewActivityArr[$student_id]))
						{
							for ($i=0; $i<$SizeOfAct; $i++)
							{
								list($thisTitle, $thisRole, $thisAttendance, $thisPerformance, $thisRelatedGroup,$thisPICNameList) = $exportStudentViewActivityArr[$student_id][$i];
								
								$count = $i+1;
								$css = (($i%2)+1);
								$x .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>";
									$x .= "<td class=\"eSporttdborder eSportprinttext\">{$thisTitle}&nbsp</td>";
									// Display content of related group
									if($display_belong__to_group)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisRelatedGroup}&nbsp</td>"; 
									if($display_role)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisRole}&nbsp</td>";
									if($display_attendance)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisAttendance}&nbsp</td>"; 
									if($display_performance)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisPerformance}&nbsp</td>"; 
// 									if($display_email)
// 										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisEmail}&nbsp</td>"; 
									if($display_picname)
										$x .= "<td align=\"center\" class=\"eSporttdborder eSportprinttext\">{$thisPICNameList}&nbsp</td>";
								$x .= "</tr>";
							}
						}
						else
						{
							$Colspan=2;
							if($display_belong__to_group) $Colspan++;
							if($display_role) $Colspan++;
							if($display_attendance) $Colspan++;
							if($display_performance) $Colspan++;
					//		if($display_email) $Colspan++;
							if($display_picname) $Colspan++;
							$x .= "<tr>";
								$x .= "<td colspan=\"$Colspan\" align=\"center\" class=\"eSporttdborder eSportprinttext\">".$Lang['General']['NoRecordAtThisMoment']."</td>"; 
							$x .= "</tr>";
						}
					$x .= "</table>";
	 			$x .= "</td>";
	 		$x .= "</tr>";
	 		$x .= "</table>";
 		}
	 	
	 	$page_count++;
	 	if($page_count < sizeof($studentNameArr)) {
			$breakStyle = ($applyPageBreak)? "style='page-break-after:always'" : "";
	 	}	
	 	else {
	 		$breakStyle = "";
	 	}
	 	
	 	$x .= "<div ". $breakStyle .">&nbsp;</div>";	
	 	
	 	$printed_std[] = $student_id;
 	}
	################################################################
	?>
	
	
	<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
	</table>
	<?=$x?>
	<br>
	
	<?
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	intranet_closedb();

}
else
{
?>
You have no priviledge to access this page.
<?
}
?>