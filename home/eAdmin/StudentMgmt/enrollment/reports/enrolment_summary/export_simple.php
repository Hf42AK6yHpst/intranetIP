<?php
# using: 
############## Change Log [start] ####
#
#
########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();

$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin');

//debug_pr($_POST);

$showTarget = $_POST['showTarget'];		// 'class' or 'club'
$groupStudentAssoc = unserialize(rawurldecode($_POST['exportGroupStudentAssoc']));
$studentInfoAry = unserialize(rawurldecode($_POST['studentInfoArr']));

$headerAry = array();
$dataAry = array();


if ($showTarget == 'club') {
	$headerAry[] = $Lang['eEnrolment']['ClubName'];
}
$headerAry[] = $i_UserClassName;
$headerAry[] = $i_UserClassNumber;
$headerAry[] = $i_UserStudentName;
$headerAry[] = $eEnrollmentMenu['club'];
$headerAry[] = $eEnrollmentMenu['activity'];

$tempClubInfoAry = $libenroll->Get_All_Club_Info();
$clubInfoAssoAry = BuildMultiKeyAssoc($tempClubInfoAry, "EnrolGroupID");
unset($tempClubInfoAry);


$iCount = 0;
foreach((array)$groupStudentAssoc as $_classClubId => $_studentIdAry) {
	$_studentIdAry = array_values($_studentIdAry);
	$_numOfStudent = count($_studentIdAry);
	
	if ($showTarget=="class") {
		$_yearClassObj = new year_class($_classClubId, $GetYearDetail=true, $GetClassTeacherList=false, $GetClassStudentList=false);
		$_targetDataName = $_yearClassObj->Get_Class_Name();
	} 
	else if ($showTarget=="club") {
		$_targetDataName = $clubInfoAssoAry[$_classClubId]['TitleWithSemester'];
	}
	
	//loop through all students to get performance and construct table contents
 	for ($i=0; $i<$_numOfStudent; $i++) {
     	$__studentId = $_studentIdAry[$i];
     	$__className = $studentInfoAry[$__studentId]['ClassName'];
     	$__classNumber = $studentInfoAry[$__studentId]['ClassNumber'];
     	$__studentName = strip_tags($studentInfoAry[$__studentId]['StudentName']);
     	$__clubInfoAry = $studentInfoAry[$__studentId]['ClubInfoArr'];
     	$__activityInfoAry = $studentInfoAry[$__studentId]['ActivityInfoArr'];
     	
     	$__clubTitleAry = Get_Array_By_Key($__clubInfoAry, 1);
     	$__clubDisplay = implode("\n", $__clubTitleAry);
     	
     	$__activityTitleAry = Get_Array_By_Key($__activityInfoAry, 1);
     	$__activityDisplay = implode("\n", $__activityTitleAry);
     	
     	$jCount = 0;
     	if ($showTarget == 'club') {
     		$dataAry[$iCount][$jCount++] = $_targetDataName;
     	}
     	$dataAry[$iCount][$jCount++] = $__className;
     	$dataAry[$iCount][$jCount++] = $__classNumber;
     	$dataAry[$iCount][$jCount++] = $__studentName;
     	$dataAry[$iCount][$jCount++] = $__clubDisplay;
     	$dataAry[$iCount][$jCount++] = $__activityDisplay;
     	
     	$iCount++;
 	}
}


$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "enrolment_summary_simple1.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>