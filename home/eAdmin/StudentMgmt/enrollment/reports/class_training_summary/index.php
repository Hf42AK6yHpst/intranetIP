<?php
/*
 *  Using:
 *  
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-08-15 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$linterface = new libclubsenrol_ui();
$intakeSelection = $linterface->getIntakeSelection();
$intakeList = $libenroll->getIntakeList();
if (count($intakeList)){
    $intakeDate = $intakeList[0]['StartDate']. ' ~ ' . $intakeList[0]['EndDate'];
}
else {
    $intakeDate = '';
}

# setting the current page
$CurrentPage = "PageClassTrainingSummary";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClassTrainingSummary']['ReportName'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();	



// Option Div after result table created
$divName = 'reportOptionOuterDiv';
$tdId = 'tdOption';
$x = '';
$x .= '<div id="showHideOptionDiv" style="display:none;">'."\n";
	$x .= '<table style="width:100%;">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td id="tdOption" class="report_show_option">'."\n";
				$x .= '<span id="spanShowOption_'.$divName.'">'."\n";
					$x .= $linterface->Get_Show_Option_Link("javascript:showOption();", '', '', 'spanShowOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<span id="spanHideOption_'.$divName.'" style="display:none">'."\n";
					$x .= $linterface->Get_Hide_Option_Link("javascript:hideOption();", '', '', 'spanHideOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<div id="'.$divName.'" style="display:none;">'."\n";
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionOuterDiv'] = $x;

// Option Div
$divName = 'reportOptionDiv';
$x = '';
$x .= '<div id="'.$divName.'">'."\n";
	$x .= '<div>'."\n";
		$x .= '<table id="filterOptionTable" class="form_table_v30">'."\n";
			
			# Intake selection
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['curriculumTemplate']['intake']['intake'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $intakeSelection.'&nbsp;<span id="IntakeDate">'.$intakeDate.'</span>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Classes selection
			$classSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('Class', 1);");
			$x .= '<tr id="studentTr">'."\n";
			$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['curriculumTemplate']['intake']['class'].'</td>'."\n";
			 $x .= '<td>'."\n";
			     $x .= $linterface->Get_MultipleSelection_And_SelectAll_Div('', $classSelectAllBtn, 'classSelectionSpan');
			     $x .= $linterface->Get_Form_Warning_Msg('classSelectionWarnMsgDiv', $Lang['eEnrolment']['report']['warning']['selectClass'], $Class='warnMsgDiv')."\n";
			 $x .= '</td>'."\n";
			$x .= '</tr>'."\n";

			# Display Type
			$x .= '<tr>'."\n";
    			$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['report']['displayType'].'</td>'."\n";
    			$x .= '<td>'."\n";
        			$x.= $linterface->Get_Radio_Button('displayType_summary','displayType','summary',1, $Class="", $Display=$Lang['eEnrolment']['report']['type']['summary'], $Onclick="",$isDisabled=0);
        			$x.= $linterface->Get_Radio_Button('displayType_detail','displayType','detail',0, $Class="", $Display=$Lang['eEnrolment']['report']['type']['detail'], $Onclick="",$isDisabled=0);
    			$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .= '<br style="clear:both;" />'."\n";
	
	$x .= '<div style="text-align:left;">'."\n";
		$x .= $linterface->MandatoryField();
	$x .= '</div>'."\n";
	$x .= '<br style="clear:both;" />'."\n";
	
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionDiv'] = $x;

// Tool bar
$x = '';
$x .= '<div id="contentToolDiv" class="content_top_tool" style="display:none;">'."\n";
	$x .= '<div class="Conntent_tool">'."\n";
		$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
		$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['contentToolDiv'] = $x;

// Result Div
$htmlAry['reportResultDiv'] = '<div id="reportResultDiv"></div>';

$linterface->LAYOUT_START();
?>
<script type="text/javascript">
var isLoading = true;
var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

$(document).ready(function () {
	js_Select_All('Class', 1, '');
	changeIntake();

	$('#Intake').change(function(){
		changeIntake();
	});
});

function checkForm(){

	//check intake
	if($("#Intake").val()==''){
		alert("<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['selectIntake'];?>");
		return false;
	}

	if ($("#Class option:selected").length == 0) {
		alert("<?php echo $Lang['eEnrolment']['report']['warning']['selectClass'];?>");
		return false;
	}
	
	return true;
}

function changeIntake() {
	isLoading = true;
	$('#classSelectionSpan').html(loadingImg);
	$('div#contentToolDiv').hide();
	$('#reportResultDiv').html('');		// clear the result

	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			'action': 'getClassList',
			'Intake': $("#Intake").val()
		},		  
		success: update_class_list,
		error: show_ajax_error
	});
}


function update_class_list(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		isLoading = false;
		$('#classSelectionSpan').html(ajaxReturn.html);
		$('#IntakeDate').html(ajaxReturn.html2);
		js_Select_All('Class', 1, '');
	}
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

function refreshReport() {
	
	var isValid = checkForm();

	if(isValid){
		$('div.warnMsgDiv').hide();
		Block_Document();

		if (Trim($('div#reportOptionOuterDiv').html()) == '') {
			hideOption();
			$('div#showHideOptionDiv').show();
			$('div#contentToolDiv').show();
		}else{
			$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
		}
		$('div#reportResultDiv').html(loadingImg);
		$.ajax({
			url:      	"ajax.php",
			dataType: 	"json",
			type:     	"POST",
			data:     	$("#form1").serialize() + '&action=getClassTrainingReport',
			success:  	function(ajaxReturn) {
							if (ajaxReturn != null && ajaxReturn.success){
    							$('#reportResultDiv').html(ajaxReturn.html);
    							Scroll_To_Top();
    							UnBlock_Document();
							}
			}
		});
	}
}
function showOption(){
	$('div#reportOptionDiv').show();
	$('#spanShowOption_reportOptionOuterDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').show();
	$('#tdOption').removeClass( "report_show_option" );
}
function hideOption(){
	$('div#reportOptionDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').hide();
	$('#spanShowOption_reportOptionOuterDiv').show();
	$('#tdOption').addClass( "report_show_option" );
}
function goExport() {
	$('form#form2').attr('target', '_self').attr('action', 'export.php').submit();
}

function goPrint() {
	$('form#form2').attr('target', '_blank').attr('action', 'print.php').submit();
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['reportOptionOuterDiv']?>
	<br />
	
	<?=$htmlAry['reportOptionDiv']?>
	<?=$htmlAry['contentToolDiv']?>
	<?=$htmlAry['reportResultDiv']?>
	<br />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>