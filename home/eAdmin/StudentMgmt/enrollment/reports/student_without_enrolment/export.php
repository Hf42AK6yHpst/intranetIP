<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$enrolmentSourceAry = $_POST['enrolmentSourceAry'];
$clubTermIdAry = $_POST['clubTermIdAry'];

$headerAry = $libenroll_report->getStudentWithoutEnrolmentReportResultHeaderAry($showMainGuardianInfo);
$dataAry = $libenroll_report->getStudentWithoutEnrolmentReportResultAry($enrolmentSourceAry, $clubTermIdAry, $AcademicYearID);

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "student_without_enrolment.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>