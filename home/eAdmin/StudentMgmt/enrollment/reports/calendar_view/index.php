<?php
# using: 

############## Change Log [start] ####
#
#   Date:   2020-01-30 (Tommy)
#       - add permission checking
#
#   Date:   2018-09-06 (Cameron)
#       - temporary disable Block_Document and UnBlock_Document for HKPF
#
#   Date:  2018-09-04 (Cameron) [ip.2.5.9.10.1]
#       - comment out extra unused element in getCalendarView
#
#   Date : 2018-08-10 Cameron
#       - set view only for HKPF
#
#   Date : 2018-06-21 Marco Yu
#		- add export function
#
#	Date : 2017-04-27 Villa #W107649 
#		- Allow All Staff which can access Enrolment can also access this report
#
#	Date : 2015-12-10 Omas 
#		- Create this page
#
############## Change Log [End] ####

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();
	if ($libenroll->hasAccessRight($_SESSION['UserID'], "AnyAdminOrHelperOfClubAndActivity")||$sys_custom['eEnrolment']['EnableLocation_tlgcCust'])
    {
        $linterface = new interface_html();
        $CurrentPage = "PageReportCalendar";
		$CurrentPageArr['eEnrollment'] = 1;
       	$TAGS_OBJ[] = array($Lang['eEnrolment']['Report']['CalendarView'], "", 1);
       	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		$linterface->LAYOUT_START();
		
		## export btn
		$x='';
		$buttonHref = '';
		$x .= $linterface->Get_Content_Tool_v30('export', "javascript:goToExport();");
		$htmlAry['toolbar'] = $x;
		
		## print btn
		$toolbar = $linterface->GET_LNK_PRINT("#", "", "jsPrint();", "", "", 0);
		$prev_month_link = "<a href='#' onClick='jsChangeMonthByArrow(-1)'><img height='21' border='0' align='absmiddle' width='21' src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_prev_off.gif'></a>";
		$next_month_link = "<a href='#' onClick='jsChangeMonthByArrow(+1)'><img height='21' border='0' align='absmiddle' width='21' src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_next_off.gif'></a>";
		
		$selectDateLayer = '';
		$selectDateLayer .= $prev_month_link;
		
		# Get Month selection
		$numMonth =count($Lang['General']['month']);
		for($i = 0; $i<$numMonth ; $i++){
			if($i != 0 && $i != 13){
				$monthArr[($i-1)] = $Lang['General']['month'][$i];
			}
		}
		$curMonth = date('m');		
		$selectDateLayer .= getSelectByAssoArray($monthArr, 'id="monthSelect" onChange="javascript:isTargetCalendar();"','',0,1);
										
		# Get Year selection
		$curYear = date('Y');
		$yearArr[$curYear-1] = $curYear-1;
		$yearArr[$curYear] = $curYear;
		$yearArr[$curYear+1] = $curYear+1;
		$selectDateLayer .= getSelectByAssoArray($yearArr, 'id="yearSelect" onChange="javascript:isTargetCalendar();"','',0,1);									
		
		# Go Btn
		$selectDateLayer .= $linterface->GET_SMALL_BTN($Lang['ePost']['Filter']['Go'], "button", "jsChangeMonth();",'goBtn');
		$selectDateLayer .= $next_month_link;
		
		$htmlAry['timeStringHidden'] = $linterface->GET_HIDDEN_INPUT('timeString2', 'timeString2', 'aaa');
    }
?>
<script language="javascript">
var currentDate = new Date();
var currentMM = currentDate.getMonth();
var currentYYYY = currentDate.getFullYear();
// js month is start from 0
var correctMM =  (currentMM+1);
var MonthLang = new Array("<?=implode('","',$Lang['General']['month'])?>");

$(document).ready( function() {
	$('#CalendarDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	getCalendarView();
	isTargetCalendar();
});

function isTargetCalendar(){
	if($('#monthSelect').val() == currentMM && $('#yearSelect').val() == currentYYYY){
		$('#goBtn').attr('disabled','disabled');
	}
	else{
		$('#goBtn').removeAttr('disabled');
	}
}

function changeSelected(id,value){
	var ddl = document.getElementById(id);
	var opts = ddl.options.length;
	for (var i=0; i<opts; i++){
	    if (ddl.options[i].value == value){
	        ddl.options[i].selected = true;
	        break;
	    }
	}
}

function jsChangeMonth(){
	currentDate.setYear($('#yearSelect').val());
	currentDate.setMonth($('#monthSelect').val());
	currentDate.setDate('01');
	
	currentDD = currentDate.getDate();
	currentMM = currentDate.getMonth();
	correctMM =  (currentMM+1);
	currentYYYY = currentDate.getFullYear();
	getCalendarView();
}

function jsChangeMonthByArrow(change){
	//2016-03-31 Kenneth
	//Set Date First, set month after => cater bug of 03-31 -> 05-01
	currentDate.setDate('01');
	currentDate.setMonth(currentDate.getMonth() + change);
	currentDD = currentDate.getDate();
	currentMM = currentDate.getMonth();
	correctMM =  (currentMM+1);
	currentYYYY = currentDate.getFullYear();
	getCalendarView();
}

function getCalendarView(){
	changeSelected('monthSelect',currentMM);
	changeSelected('yearSelect',currentYYYY);
// 	displayLang = MonthLang[correctMM] + " - " + currentYYYY;
// 	$('#DisplayMonth').html(displayLang);
<?php if (!$sys_custom['project']['HKPF']):?>
	Block_Document();
<?php endif;?>	
	$.ajax({
				type: 'POST',
				url: "ajax_task.php",
				data: {
						"action"		:	"getCalendarView",
<?php 
    if ($sys_custom['project']['HKPF']) {
        echo "\"viewOnly\"		:	1,";
    }
?>
						"timeString" 	:	currentYYYY + '-' + correctMM
						},
				success: function(responseText){
					$('#CalendarDiv').html(responseText);
<?php if (!$sys_custom['project']['HKPF']):?>					
					UnBlock_Document();
<?php endif;?>					
				}
	});
}

function jsPrint(){
	$('form#form1').submit();	
}



function click_export(export_type)
{
	//checkOption(document.form1.elements['activityCategory[]']);
	if (export_type == 'details'){
		document.form1.action = "export.php";
	}
	document.form1.submit();
	document.form1.action = "index.php";
}

function goToExport(){
	//changeSelected('monthSelect',currentMM);
	//changeSelected('yearSelect',currentYYYY);
	//displayLang = MonthLang[correctMM] + " - " + currentYYYY;
	//$('#DisplayMonth').html(displayLang);
	//Block_Document();
	
	$('input#timeString2').val(currentYYYY + '-' + correctMM);
//alert(currentYYYY + '-' + correctMM);
//alert($('input#timeString').val());
    $('form#form2').attr('action','export.php').submit();
	
}

</script>
<br>
<form name="form2" id="form2" method="POST" >
<table border='0' width='100%' cellpadding='0' cellspacing='0'>
	<tr><td>
<div class="Conntent_tool">
		<?=$htmlAry['toolbar']?>
</div>
<?=$toolbar?>
</td></tr>

</table>
<div style="text-align:center;"><?=$selectDateLayer?></div>
<table border="0" width="100%" cellspacing="10" cellpadding="0">
	<tbody>
		<tr>
			<td style="padding: 5px; background-color:rgb(219,237,255)">
				<div id="CalendarDiv" style="background-color:#FFF;"></div>
			</td>
		</tr>
	</tbody>
</table>
<?php echo $htmlAry['timeStringHidden'] ?>
</form>
<?
	intranet_closedb();
	$linterface->LAYOUT_STOP();
}
else
{
	intranet_closedb();
?>
	You have no priviledge to access this page.
<?
}
?>