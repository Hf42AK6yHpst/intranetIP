<?
# using: 

############## Change Log [start] ####
#
#  Date:   2019-06-10 Anna
#          delete $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC'], for general school
#
#   Date : 2018-09-14 Cameron
#       - allow instructor to view report
#
#   Date : 2018-09-06 Cameron
#       - also show lesson title for HKPF
#
#   Date : 2018-08-06 Cameron   
#       - admin can view other person's calendar for HKPF
#
#   Date : 2018-08-04 Cameron
#       - retrieve meeting date event (lesson) by Login User for HKPF
#       - also show location and hide club(group) info for HKPF
#	Date : 2017-04-27 Villa #W107649
#		- Add cust $sys_custom['eEnrolment']['EnableLocation_tlgcCust']: Show Meeting Location
#	Date : 2017-03-24 Villa
#		- Modified $groupInfoArr add paras
#	Date : 2016-08-22 Ivan [ip.2.5.7.10.1]
#		- Improved to click the club / activity name to go to attendance page
#	Date : 2016-01-18 Omas
#		- Fixed cannot display Jan to Sept , disalbe submit if already is that month
#	Date : 2015-12-10 Omas
#		- Create this page
#
############## Change Log [End] ####
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$libenroll = new libclubsenrol();
$linterface = new interface_html();

$viewOnly = (IntegerSafe($_POST['viewOnly']) == 1)? true : false;

// Get Group Meeting Date & Group Info
if($_POST['timeString'] != ''){
    // normal flow
    $date_condition = $_POST['timeString'];
    $dateArr = explode('-',$date_condition);
    $year = $dateArr[0];
    $month = $dateArr[1];
    
    // str_pad to the month
    $date_condition = $year.'-'.str_pad($month,2,'0',STR_PAD_LEFT);
}
else{
    // print page
    $date_condition = $_POST['year'].'-'.str_pad($_POST['month'],2,'0',STR_PAD_LEFT);
    
    $isPrintMode = true;
}


$month_display = date('F Y',strtotime($date_condition.'-01'));

if ($sys_custom['project']['HKPF']) {
    // Get Lesson schedule date & lesson info
    if ($_POST['srcFrom'] == 'portal') {
        $eventDateArr = $libenroll->getMeetingDateOfTheMonthByUser($date_condition,$_SESSION['UserID']);
    }
    else if ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->isInstructor()){
        $eventDateArr = $libenroll->getMeetingDateOfTheMonthByUser($date_condition);
    }
    else {
        header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
    }
}
else {
    $isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
    $isEnrolPIC = $libenroll->IS_CLUB_PIC() ||  $libenroll->IS_EVENT_PIC();
//     if(!$isEnrolAdmin && $isEnrolPIC && $sys_custom['eEnrolment']['ECAandMonthlyCalendarAllowEnrolPIC']){
    if(!$isEnrolAdmin && $isEnrolPIC){
        $viewOnly = 1;
    }
    // Get Group Meeting Date & Group Info
    $groupDateArr = $libenroll->getMeetingDateOfTheMonth('Club',$date_condition);
    $enrolGroupIDArr = array_unique(Get_Array_By_Key($groupDateArr,'EnrolGroupID'));
    $groupInfoArr = BuildMultiKeyAssoc( $libenroll->Get_All_Club_Info($enrolGroupIDArr, $AcademicYearID='', $CategoryIDArr='', $Keyword='', $YearTermIDArr='', $OrderBy='', $OrderDesc='asc', $ApplyUserType='', $Round='', $GetLangDisplay = true) , 'EnrolGroupID' );
    
    $groupDateAssocArr = array();
    foreach((array)$groupDateArr as $_ary){
        $groupDateAssocArr[$_ary['Date']][] = $_ary;
    }

    // Get Event Meeting Date & Event Info
    $eventDateArr = $libenroll->getMeetingDateOfTheMonth('Event',$date_condition);
}
$enrolEventIDArr = array_unique(Get_Array_By_Key($eventDateArr,'EnrolEventID'));
$enrolInfoArr = $libenroll->Get_Activity_Info_By_Id($enrolEventIDArr);

$eventDateAssocArr = array();
foreach((array)$eventDateArr as $_ary){
    $eventDateAssocArr[$_ary['Date']][] = $_ary;
}

// Start Prepare calendar
$first_of_month = mktime (0,0,0, $month, 1, $year);
$maxdays   = date('t', $first_of_month); # number of days in the month
$date_info = getdate($first_of_month);   # get info about the first day of the month
$month = $date_info['mon'];
$year = $date_info['year'];
$weekday = $date_info['wday']; #weekday (zero based) of the first day of the month

$calendar = '';
if($print){
    $calendar .= '<table width="100%">';
    $calendar .= '<tr class="print_hide" ><th> </th><th align="right">'.$linterface->GET_BTN($button_print, "button", "javascript:window.print();").'</th></tr>';
    $calendar .= '<tr><th colspan="2">'.$month_display.'</th></tr>';
    $calendar .= '</table>';
}
$calendar .= "
	  <table class='booking_table_week' width='100%' border='0' cellspacing='0' id='monthly' align='center'>";
$calendar .= "<colgroup>
      				<col class='Sun' />
					<col class='Mon' />
					<col class='Tue' />
					<col class='Wed' />
					<col class='Thu' />
					<col class='Fri' />
					<col class='Sat' />
					<col />
				</colgroup>";
$calendar .= "<thead>";
$calendar .= 		"<tr>
					<th class='tabletop'>Sun</th>
					<th class='tabletop'>Mon</th>
					<th class='tabletop'>Tue</th>
					<th class='tabletop'>Wed</th>
					<th class='tabletop'>Thu</th>
					<th class='tabletop'>Fri</th>
					<th class='tabletop' style='border-right: 1px solid #BCBCBC;'>Sat</th>
					</tr>";
$calendar .= "</thead>";
$calendar .= "<tbody><tr>";

$calendar .= "<tr ".(($print) ? "" : "height=\"115\"").">";

# set class of cell: normal or print
$monthCellClass = ($print) ? "monthCellPrint" : "monthCell";
$monthTodayCellClass = ($print) ? "monthTodayCellPrint" : "monthTodayCell";

# empty cell before start of the month
if($weekday > 0) {
    for ($i=0; $i<$weekday; $i++) {
        $calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
    }
}

for ($day=1; $day<=$maxdays; $day++) {
    $ts = mktime(0,0,0,$month,$day,$year);
    
    $academicYearAry = getAcademicYearAndYearTermByDate($year.'-'.$month.'-'.$day);
    $academicYearId = $academicYearAry['AcademicYearID'];
    
    $calendar .= "<td style='width:14%;min-width:120px;overflow-x:hidden; vertical-align:top;' class=\"$monthCellClass\" id=\"$year-$month-$day\"".">";
    $groupMeetingArr = (!empty($groupDateAssocArr[$day])) ? $groupDateAssocArr[$day] : array() ;
    $eventMeetingArr = (!empty($eventDateAssocArr[$day])) ? $eventDateAssocArr[$day] : array() ;
    // creating
    $day_event = '';
    if(!empty($groupMeetingArr) || !empty($eventMeetingArr) ){
        // sorting
        sortByColumn2($groupMeetingArr,'startTime',0,0, 'endTime');
        sortByColumn2($eventMeetingArr,'startTime',0,0, 'endTime');
        
        // meeting dates table
        $day_event .= '<table width="100%" border="0" style="border-collapse:collapse;">';
        foreach((array)$groupMeetingArr as $_groupDateInfo){
            $_enrolGroupID = $_groupDateInfo['EnrolGroupID'];
            $_startTime = substr($_groupDateInfo['startTime'],0,5);
            $_endTime = substr($_groupDateInfo['endTime'],0,5);
            $_groupTitle = $groupInfoArr[$_enrolGroupID]['Title'];
            $_location = $_groupDateInfo['Location'];
            
            if ($isPrintMode || $viewOnly) {
                $_groupTitleDisplay = $_groupTitle;
            }
            else {
                $_groupTitleDisplay = '<a class="tablelink" target="_blank" href="../../club_attendance_mgt.php?AcademicYearID='.$academicYearId.'&EnrolGroupID='.$_enrolGroupID.'">'.$_groupTitle.'</a>';
            }
            
            $_display = $_startTime.' - '.$_endTime.' '.$_groupTitleDisplay;
            if($sys_custom['eEnrolment']['EnableLocation_tlgcCust'] || $sys_custom['project']['HKPF']){
                if($_location){
                    $_display .= '<br>'.' ('.$_location.' )';
                }
            }
            $day_event .= '<tr>';
            $day_event .= '<td style="border:0 ;">'.$_display.'</td>';
            $day_event .= '</tr>';
        }
        foreach((array)$eventMeetingArr as $_eventDateInfo){
            $_enrolEventID = $_eventDateInfo['EnrolEventID'];
            $_startTime = substr($_eventDateInfo['startTime'],0,5);
            $_endTime = substr($_eventDateInfo['endTime'],0,5);
            $_eventTitle = $enrolInfoArr[$_enrolEventID]['EventTitle'];
            $_location = $_eventDateInfo['Location'];
            
            if ($isPrintMode || $viewOnly) {
                $_eventTitleDisplay = $sys_custom['project']['HKPF'] ? ($_eventTitle. ' - ' .$_eventDateInfo['Lesson']) : $_eventTitle; 
            }
            else {
                $_eventTitleDisplay = '<a class="tablelink" target="_blank" href="../../event_attendance_mgt.php?AcademicYearID='.$academicYearId.'&EnrolEventID='.$_enrolEventID.'">'.$_eventTitle.'</a>';
            }
            
            $_display = $_startTime.' - '.$_endTime.' '.$_eventTitleDisplay;
            if($sys_custom['eEnrolment']['EnableLocation_tlgcCust'] || $sys_custom['project']['HKPF']){
                if($_location){
                    $_display .= '<br>'.' ('.$_location.' )';
                }
            }
            $day_event .= '<tr>';
            $day_event .= '<td style="border:0 ;">'.$_display.'</td>';
            $day_event .= '</tr>';
        }
        $day_event .= '</table>';
    }
    
    $calendar .= "$day<br style=\"clear: both;\"/>$day_event";
    $calendar .= "</div></div>";
    $calendar .= "</td>";
    $weekday++;
    
    // determine starting at new row
    if($weekday == 7) {
        $calendar .= "</tr>";
        $calendar .= ($day != $maxdays) ? "<tr ".(($print) ? "" : "height=\"115\"").">" : "";
        $weekday = ($day != $maxdays) ? 0 : $weekday;
    }
}

// empty cell after end of month
if($weekday != 7) {
    for ($i=0; $i<(7-$weekday); $i++) {
        $calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
    }
    
    $calendar .= "</tr>";
}
$calendar .= "</tbody></table>";

// form submit for printing page
$calendar .= '<form id="form1" name="form1" target="_blank" action="print.php" method="post">';
$calendar .= $linterface->GET_HIDDEN_INPUT('month', 'month', $month);
$calendar .= $linterface->GET_HIDDEN_INPUT('year', 'year', $year);
$calendar .= $linterface->GET_HIDDEN_INPUT('print', 'print', 1);
$calendar .= '</form>';

echo $calendar;
?>
