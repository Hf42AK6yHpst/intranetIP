<?php
## Modifying By: 

/************************************
 * 	Date:		2015-01-27 (Bill)
 * 	Details:	Display school remarks
 *	Date:		2014-06-19 (Bill) - 2014-09-25 (Bill) 
 * 	Details: 	Modify layout
 * 	Date:		2014-08-11 (Bill)
 * 	Details:	Radio: Date range - academic year according to event date
 *	Date:		2013-04-19 (Rita) 
 * 	Details: 	Create this page
 ***********************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libuser = new libuser();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# Check User Access Right
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	# Check flag
	if(!$libenroll->enableCCHPWEnrolParentNotice()){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();

# Obtain Post Value
$rankTarget = $_POST['rankTarget'];
$rankTargetDetail = $_POST['rankTargetDetail'];
$studentID = $_POSR['studentID'];
$radioPeriod = $_POST['radioPeriod'];
$selectYear = $_POST['selectYear'];
$selectSemester = $_POST['selectSemester'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
$sel_category = $_POST['sel_category'];
$enrolEventID = $_POST['activityCategory'];
$studentID = $_POST['studentID'];
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester);
	$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester);
	
	$academicYearId = $selectYear;
	
	# Radio: Year
	// All activities in the same academic year
	$academicYearObj = new academic_year($selectYear);
	$academicYearName = $academicYearObj->Get_Academic_Year_Name();

}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
	
	// Not affected by School Year Selection
	$selectYear = "";
	
//	$academicInfoAry = getAcademicYearAndYearTermByDate($textFromDate);
//	$academicYearId = $academicInfoAry['AcademicYearID'];
}


$formIDArr = array();
$studentIDArr = array();
if($rankTarget == 'form'){
	$formIDArr = $rankTargetDetail;
}elseif($rankTarget =='class'){
	$studentIDArr = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
}elseif($rankTarget =='student'){
	$studentIDArr = $studentID;
}

# Activity Date Info
$thisActivityDateInfoArr = $libenroll->GET_ENROL_EVENT_DATE($enrolEventID,$SQL_startdate,$SQL_enddate);
$thisActivityDateInfoIDArrAssoc = BuildMultiKeyAssoc($thisActivityDateInfoArr, array('EnrolEventID','EventDateID'));
$thisActivityDateInfoIDArr = array_keys($thisActivityDateInfoIDArrAssoc);	
//debug_pr($thisActivityDateInfoIDArrAssoc);
//debug_pr($selectYear);
//debug_pr($formIDArr);
//debug_pr($studentIDArr);
//debug_pr($sel_category);
# Activity Info 
if($thisActivityDateInfoIDArr){
	$activityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($thisActivityDateInfoIDArr, '', $selectYear, '0', '1' , '1' , '0', $formIDArr,'', $ReturnAsso=true, $studentIDArr, $sel_category);
}
//debug_pr($activityInfoArr);
$data_ary = array();
$display = "";

# Loop Each Activity

foreach ((array)$activityInfoArr as $thisActivityID=>$thisEventInfo){
	
	## Activity Info
	$thisOrganization = $thisEventInfo['EventTitle']?$thisEventInfo['EventTitle']:$Lang['General']['EmptySymbol'];
	$thisActivityContent = removeHTMLtags($thisEventInfo['Description'])?removeHTMLtags($thisEventInfo['Description']):$Lang['General']['EmptySymbol'];
	$thisGatheringLocation = $thisEventInfo['GatheringLocation']?$thisEventInfo['GatheringLocation']:$Lang['General']['EmptySymbol'];
	$thisDismissLocation = $thisEventInfo['DismissLocation']?$thisEventInfo['DismissLocation']:$Lang['General']['EmptySymbol'];
	$thisGroupId = $thisEventInfo['EnrolGroupID'];
	
	# Staff Info
	$activityStaffInfo = $libenroll->GET_EVENTSTAFF($thisActivityID);
	$numOfActivityStaffInfo = count($activityStaffInfo);
	
	$thisPicIDArr = array();
	for($i=0;$i<$numOfActivityStaffInfo;$i++){
		$thisStaffType = $activityStaffInfo[$i]['StaffType'];	
		if($thisStaffType == 'PIC'){
			$thisPicIDArr[]= $activityStaffInfo[$i]['UserID'];
		}
	}
	for($i=0;$i<$numOfActivityStaffInfo;$i++){
		$thisStaffType = $activityStaffInfo[$i]['StaffType'];	
		if($thisStaffType == 'HELPER'){
			$thisPicIDArr[]= $activityStaffInfo[$i]['UserID'];
		}
	}
	
	# PIC
	$thisUserInfoArr =  $libuser->returnUser('','',$thisPicIDArr);
	$numOfUserInfoArr = count($thisUserInfoArr);
	$thisUserNameArr = array();
	for($i=0;$i<$numOfUserInfoArr;$i++){
		$thisUserChineseName = $thisUserInfoArr[$i]['ChineseName'];
		$thisUserEnglishName = $thisUserInfoArr[$i]['EnglishName'];
		$thisUserName = $thisUserChineseName?$thisUserChineseName:$thisUserEnglishName;
		$thisUserNameArr[]= $thisUserName;
	}
	
	$thisPICArrString = implode("、",$thisUserNameArr);
	$thisPICArrString = $thisPICArrString?$thisPICArrString:$Lang['General']['EmptySymbol'];
	
	## Club Info
	$thisClubInfo = $libenroll->getGroupInfoList('','',(array)$thisGroupId);
	$thisClubTitle = $thisClubInfo[0]['Title']?$thisClubInfo[0]['Title']:$Lang['General']['EmptySymbol'];

	## Activity Student 
	$thisActivityStudentInfo =  $thisEventInfo['StatusStudentArr'];
	
	# Payment Amount
	$thisPaymentAmount = $thisEventInfo['PaymentAmount']?'$'.$thisEventInfo['PaymentAmount']:$Lang['General']['EmptySymbol'];
				
	## Location
	$thisLocation = $thisEventInfo['Location']?$thisEventInfo['Location']:$Lang['General']['EmptySymbol'];
	
	## Organizer
	$thisOrganizer = $thisEventInfo['Organizer']?$thisEventInfo['Organizer']:$Lang['General']['EmptySymbol'];
	
	## Remarks
	$oleInfoAry = $libenroll->Get_Enrolment_Default_OLE_Setting('activity', array($thisActivityID));
	$schoolRemarks = nl2br($oleInfoAry[0]['SchoolRemark']);
	$schoolRemarks = ($schoolRemarks=='')? '&nbsp;' : $schoolRemarks;
	
	## Activity Date Time	
	$thisEventInfoArr = $thisActivityDateInfoIDArrAssoc[$thisActivityID];
	//debug_pr($thisEventInfoArr);
	foreach ((array)$thisEventInfoArr as $thisEventDateID=>$thisEventDateInfo){
		$thisActivityDateStart = $thisEventDateInfo['ActivityDateStart'];
		$thisActivityDateEnd = $thisEventDateInfo['ActivityDateEnd']; 
		
		# Radio: Date Range
		// Each activities could belong to different academic year
		if ($radioPeriod != "YEAR") {
			// Get academic year according to activity date
			$academicInfoAry = getAcademicYearAndYearTermByDate($thisActivityDateStart);
			$academicYearId = $academicInfoAry['AcademicYearID'];
			if(!$academicYearObj || $academicYearObj->AcademicYearID != $academicYearId){
				$academicYearObj = new academic_year($academicYearId);
				$academicYearName = $academicYearObj->Get_Academic_Year_Name();
			}
		}
		
		$thisActivityDateStartDateTimeArr = explode(' ', $thisActivityDateStart);
		$thisActivityDateEndDateTimeArr = explode(' ', $thisActivityDateEnd);
		
		if($thisActivityDateStartDateTimeArr[0]==$thisActivityDateEndDateTimeArr[0])
		{
			$thisActivityDate = $thisActivityDateStartDateTimeArr[0];
		}else{
			$thisActivityDate = $thisActivityDateStartDateTimeArr[0] . ' 至 ' . $thisActivityDateEndDateTimeArr[0];	
		}
		
		//$thisActivityTime = $thisActivityDateStartDateTimeArr[1] . ' 至 ' . $thisActivityDateEndDateTimeArr[1];	
		
		$thisActivityDate = $thisActivityDate?$thisActivityDate:$Lang['General']['EmptySymbol'];	
		$thisActivityStartTime = $thisActivityDateStartDateTimeArr[1]?substr($thisActivityDateStartDateTimeArr[1], 0, 5):$Lang['General']['EmptySymbol'];	
		$thisActivityEndTime = $thisActivityDateEndDateTimeArr[1]?substr($thisActivityDateEndDateTimeArr[1], 0, 5):$Lang['General']['EmptySymbol'];	
		
		/*	Remove Looping for each students
		foreach ((array)$thisActivityStudentInfo as $thisRecordStatus=>$thisStudentInfoArr){
			
			if($thisRecordStatus=='2'){
			//$numOfStudentInfoArr = count($thisStudentInfoArr);
			//for($j=0;$j<$numOfStudentInfoArr;$j++){
			
			
	 		$thisStudentId = $thisStudentInfoArr[$j]['StudentID'];	
			$thisStudentName = $thisStudentInfoArr[$j]['ChineseName']? $thisStudentInfoArr[$j]['ChineseName']:$thisStudentInfoArr[$j]['StudentName']; 
			$thisStudentName = $thisStudentName?$thisStudentName:$Lang['General']['EmptySymbol'];
			
			$thisStudentClassName = $thisStudentInfoArr[$j]['ClassName']?$thisStudentInfoArr[$j]['ClassName']:$Lang['General']['EmptySymbol'];
			$thisStudentClassNum = $thisStudentInfoArr[$j]['ClassNumber']?$thisStudentInfoArr[$j]['ClassNumber']:$Lang['General']['EmptySymbol'];
			
		
			##### Display Table Start #####
			if($j>0){
				$pageBreakCss = "page-break-after: always;";
			}
		*/
		########   Header Start   ########
			$display .= "<center><table width='1000px' style='" . $pageBreakCss . " border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '>";
				
				# Form Number
				$display .= "<tr>";
					$display .= "<td align='right' style='font-size:16px;'>表格編組： N/LWL01(2)</td>";
				$display .= "</tr>";
				
				# School Name
				$display .= "<tr>";
					//$display .= "<td style='line-height: 20px;' align='center'>";
					$display .= "<td align='center'>";
					$display .= "青松侯寶垣中學 <br />".$academicYearName."<br />培育委員會<br />全方位學習組 <br />課外活動家長通知書<br /> 
								 <br />";
					$display .= "</td>";
				$display .= "</tr>";
				
			########   Header End   ########
				$display .= "<tr>";
					$display .= "<td align='left'>";
					$display .= "敬啟者:";
					$display .= "</td>";
				$display .= "</tr>";
					
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td align='left'>";
						$display .= "<span style='display: table-cell; line-height:150%'>　　貴子弟 _________________________________ ( _________ 班 ________ 號 ) 現已參加".$thisOrganizer." 所舉辦的".$thisOrganization." ， 詳情如下：</span>"
									;
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						
						$display .= "<table width='100%' style='border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體;'>";
						$display .= "<tr>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
								$display .= "<span style='display: table-cell; width: 120px;'>日　　期：</span>
											 <span style='width:250px; display:table-cell; text-align:left;'>".date("d-m-Y",strtotime($thisActivityDate))."</span>";
							$display .= "</td>";
							
							$display .= "<td width='50%' style='vertical-align:top;'>";
								$display .= "<span style='display: table-cell; width: 120px;'>活動地點：</span>	
											 <span style='width:250px; display:table-cell; text-align:left;'>".$thisLocation."</span>";
							$display .= "</td>";
						$display .= "</tr>";
						
						$display .= "<tr>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
									$display .= "<span style='display: table-cell; width: 120px;'>集合時間：</span>	
												 <span style='width:250px; display:table-cell; text-align:left;'>".$thisActivityStartTime."</span>";
							$display .= "</td>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
								$display .= "<span style='display: table-cell; width: 120px;'>散隊時間：</span>	
											 <span style='width:250px; display:table-cell; text-align:left;'>".$thisActivityEndTime."</span>";
							$display .= "</td>";
						$display .= "</tr>";
						
						
						$display .= "<tr>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
									$display .= "<span style='display: table-cell; width: 120px;'>集合地點：</span>	
												 <span style='width:250px; display:table-cell;'>".$thisGatheringLocation."</span>";
							$display .= "</td>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
								$display .= "<span style='display: table-cell; width: 120px;'>散隊地點：</span>	
											 <span style='width:250px; display:table-cell;'>".$thisDismissLocation."</span>";
							$display .= "</td>";
						$display .= "</tr>";
					
						
						$display .= "<tr>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
									$display .= "<span style='display: table-cell; width: 120px;'>內　　容：</span>	
									 			 <span style='width:250px; display:table-cell; text-align:left;'>".$thisActivityContent."</span>";
							$display .= "</td>";
							$display .= "<td width='50%' style='vertical-align:top;'>";
								$display .= "<span style='display: table-cell; width: 120px;'>負責老師：</span>	
									 		 <span style='width:250px; display:table-cell; text-align:left;'>".$thisPICArrString."</span>";
							$display .= "</td>";
						$display .= "</tr>";					
						
						$display .= "<tr>";
							$display .= "<td width='100%' colspan='2'>";
								$display .= "<span style='display: table-cell; width: 120px;'>費　　用：</span>	
											 <span style='width:650px; display:table-cell; padding-left: 10px;text-align:left;'>".$thisPaymentAmount."</span>";
							$display .= "</td>";
						$display .= "</tr>";	
						
						$display .= "<tr>";
							$display .= "<td width='100%' colspan='2'>";
								$display .= "<span style='display: table-cell; width: 120px;'>備　　註：</span>	
											 <span style='width:650px; display:table-cell; padding-left: 10px;'>".$schoolRemarks."</span>";		
								$display .= "</td>";
						$display .= "</tr>";	
								
						$display .= "</table>";
				
					$display .= "</td>";
				$display .= "</tr>";
	
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell; width='100%'>上述情況若有改變，將預先函告，如有任何疑問，請致電　2457 7154　到校查詢。</span>";
					$display .= "</td>";
				$display .= "</tr>";	
					
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell;　width: 100%;'>　　此　致</span>	";
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell;　width: 100%;'>學生家長</span>	";
					$display .= "</td>";
				$display .= "</tr>";
					
				$display .= "<tr>";
					$display .= "<td align='right'>";
						$display .= "<span style='display: table-cell;　width: 100%;'>青松侯寶垣中學　　　　　　謹啟</span>	";
					$display .= "</td>";
				$display .= "</tr>";
				
//				$display .= "<tr>";
//					$display .= "<td align='right'>";
//						$display .= "<span style='display: table-cell;　width: 100px;'>謹啟</span>	";
//					$display .= "</td>";
//				$display .= "</tr>";
				
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell;　width: 100%'>日期:　20　　年　　月　　日</span>	";
					$display .= "</td>";
				$display .= "</tr>";
			
				$display .= "<tr>";
					$display .= "<td>";
					$display .= "<span style='width:1000px; display:table-cell; border-bottom: 1px solid #000;'>　</span>";
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr>";
					$display .= "<td align='center'>";
					$display .= "<span style='width:100%; text-align:center; '><u>回　　　條</u></span>";
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr>";
					$display .= "<td align='left'>";
					$display .= "逕覆者:";
					$display .= "</td>";
				$display .= "</tr>";	  
				
			$display .= "<tr><td>&nbsp;</td></tr>";	
			
			$display .= "<tr><td>";
				$display .= "<table align='left' style='" . $pageBreakCss . " border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '><tr>";
					$display .= "<td style='vertical-align: top;'><span style='border: solid 1px #000; width:14px; height: 14px;'>&nbsp;&nbsp;</span></td>";
					$display .= "<td>";
						$display .= " <span style='line-height:150%; vertical-align: middle;'>敝子弟	____________________________　(　________班 　_______號)　健康狀況良好，本人同意其參與".$thisOrganizer." 所舉辦的".$thisOrganization." ，並同意若遇上任何意外，將由校方採取正常的方法處理。</span>";
					$display .= "</td>";
				$display .= "</tr></table>";
			$display .= "</td></tr>";
			
			
			$display .= "<tr><td>&nbsp;</td></tr>";	
					
				$display .= "<tr><td>";
				$display .= "<table style='" . $pageBreakCss . " border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '><tr>";
					$display .= "<td style='vertical-align: top;'><span style='border: solid 1px #000; width:14px; height: 14px;'>&nbsp;&nbsp;</span></td>";
					$display .= "<td>";
						$display .= " <span style='line-height:150%; vertical-align: middle;'>敝子弟	___________________________　(　________班 　_______號) 因　____________________ _______________________________　之故，不宜參與".$thisOrganizer." 所舉辦的".$thisOrganization." 。</span>";
					$display .= "</td>";
				$display .= "</tr></table>";
			$display .= "</td></tr>";
					
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell;　width:8%;'>　　</span>	
									 <span style='display: table-cell;　width:85%;'>此　覆</span>";
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell;　width: 100%'>青松侯寶垣中學校長</span>";
					$display .= "</td>";
				$display .= "</tr>";
		
				$display .= "<tr><td>&nbsp;</td></tr>";	
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell; width: 120px;'>聯絡電話：</span>	
									 <span style='width:220px; display:table-cell; border-bottom: 1px solid #000;  '>&nbsp;</span>
									 <span style='display: table-cell; width: 250px;'></span>
									 <span style='display: table-cell; width: 120px;'>家長簽署：</span>	
									 <span style='width:220px; display:table-cell; border-bottom: 1px solid #000;  '>&nbsp;</span>						
									";						
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr><td>&nbsp;</td></tr>";	
				
				$display .= "<tr>";
					$display .= "<td>";
						$display .= "<span style='display: table-cell; width: 120px;'>日　期　：</span>	
									 <span style='width:220px; display:table-cell; border-bottom: 1px solid #000;  '>&nbsp;</span>
									 <span style='display: table-cell; width: 250px;'></span>
									 <span style='display: table-cell; width: 120px;'>家長姓名：</span>	
									 <span style='width:220px; display:table-cell; border-bottom: 1px solid #000;  '>&nbsp;</span>
									";
					$display .= "</td>";
				$display .= "</tr>";
				
				$display .= "<tr><td>　</td></tr>";
				$display .= "<tr><td>　</td></tr>";
				$display .= "<tr><td>　</td></tr>";
				
				$display .= "</table>";		
		/*
			}
			}
		}
		*/
	}
}

if($display==''){
	$display .= '<center>';
	$display .= "<table width='1000px' style=' border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '>";
	$display .= '<tr><td align="center">';
	$display .= '沒有活動紀錄。';
	$display .= '</td></tr></table></center>';
}

?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<!-- Display result //-->
<?=$display?>

<?
intranet_closedb();
// include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>



