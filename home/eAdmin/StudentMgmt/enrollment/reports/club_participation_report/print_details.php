<?php
## Modifying By: 
/*
 * Change Log:
 * Date:	2017-03-30 Frankie - handle User's Date Range
 * Date:	2017-01-25 Villa #B112166 allow hours can be 0
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# user access right checking
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

// debug_pr($radioPeriod);
// debug_pr($selectYear);
// debug_pr($selectSemester);
// debug_pr($textFromDate);
// debug_pr($textToDate);
// debug_pr($rankTarget);
// debug_pr($rankTargetDetail);
$rankTargetDetail_str = implode(",",(array)$rankTargetDetail);
if($rankTarget!="student")	$studentID = array();
// debug_pr($studentID);
// debug_pr($studentFlag);
// debug_pr($sel_category);
// debug_pr($activityCategory);
$selectedClub = implode(",",(array)$activityCategory);
// debug_pr($times);
// debug_pr($timesRange);
// debug_pr($hours);
// debug_pr($hoursRange);
// debug_pr($displayUnit);
// debug_pr($sortBy);
// debug_pr($sortByOrder);

# prepare report data 
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = substr(getStartDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
	$SQL_enddate = substr(getEndDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
}
if (sizeof($studentID)>0) {
	$studentID_str = implode(",",$studentID);
} else {
	$userList = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
	$studentID_str = implode(",",$userList);
}

$sortType = $sortByOrder ? "desc" : "asc";
switch($sortBy)
{
	case 0:
		$order_by = "ClassName $sortType, ClassNumber $sortType";	
		break;
	case 1:
		$order_by = "ClassName, ClassNumber";	
		break;
	case 2:
		$order_by = "hours $sortType, ClassName, ClassNumber";	
		break;
}

$name_field = getNameFieldByLang("d.");
//$title_display = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
$title_display = Get_Lang_Selection("b.TitleChinese", "b.Title");

// $sql ="
// 	select 
// 		c.UserID, d.ClassName, d.ClassNumber, $name_field, e.ActivityDateStart, $title_display, TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) as hours
// 	from 
// 		INTRANET_USERGROUP as c
// 		inner join INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
// 		inner join INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
// 		inner join INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (e.ActivityDateStart>='$SQL_startdate' and e.ActivityDateStart<='$SQL_enddate'))
// 		inner join INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
// 	where
// 		c.EnrolGroupID in ($selectedClub)
// 		and c.UserID in ($studentID_str)
// 	order by
// 		$order_by, e.ActivityDateStart
// ";
$sql ="
		SELECT
			c.UserID,
			d.ClassName,
			d.ClassNumber,
			$name_field,
			e.ActivityDateStart,
			$title_display,
			IF( f.RecordStatus=3,
				'00:00:00',
				TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)
			) as hours,
			f.RecordStatus
		from
			INTRANET_USERGROUP as c
		inner join
			INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
		inner join
			INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
		inner join
			INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID
				and (e.RecordStatus Is Null Or e.RecordStatus = 1)
				and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
		inner join
			INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID";
if ($libenroll->enableUserJoinDateRange()) {
	$sql .=" 
		AND (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%' OR EnrolAvailiableDateStart <= ActivityDateStart)
		AND (EnrolAvailiableDateEnd IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%' OR EnrolAvailiableDateEnd >= ActivityDateEnd)";
}
$sql .=") 
		WHERE
			c.EnrolGroupID in ($selectedClub)
		AND
			c.UserID in ($studentID_str)
		order by
			$order_by, e.ActivityDateStart
		";
$result = $libenroll->returnArray($sql);		

		
// For sorting
# times sql
// if($times)
// {
	$times_sym = $timesRange ? "<=" : ">=";
	$having_sql[] = " times $times_sym $times ";
// }
// if($hours)
// {
	$hours_sym = $hoursRange ? "<=" : ">=";
	$having_sql[] = " hours $hours_sym '". ($hours<10?"0":"") ."$hours:00:00' ";
// }
$having_con = !empty($having_sql) ? "having " . implode(" and " , $having_sql) : "";

$sortType = $sortByOrder ? "desc" : "asc";
switch($sortBy)
{
	case 0:
		$order_by = "d.ClassName $sortType, d.ClassNumber $sortType";	
		break;
	case 1:
		$order_by = "times $sortType";	
		break;
	case 2:
		$order_by = "hours $sortType";	
		break;
}

$name_field = getNameFieldByLang("d.");
// $sql ="
// 	select 
// 		c.UserID,  d.ClassName, d.ClassNumber, $name_field, count(c.EnrolGroupID) as times,
// 		SEC_TO_TIME(SUM(TIME_TO_SEC(IF (TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'), '00:00:00',TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart))))) as hours
// 	from 
// 		INTRANET_USERGROUP as c
// 		inner join INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
// 		inner join INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
// 		inner join INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (e.ActivityDateStart>='$SQL_startdate' and e.ActivityDateStart<='$SQL_enddate'))
// 		inner join INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
// 	where
// 		c.EnrolGroupID in ($selectedClub)
// 		and c.UserID in ($studentID_str)
// 	group by 
// 		d.UserID
// 	$having_con
// 	order by
// 		$order_by, d.ClassName, d.ClassNumber
// ";
		$sql = "
				SELECT
					c.UserID,
					d.ClassName,
					d.ClassNumber,
					$name_field,
					SUM(
						IF(f.RecordStatus=3,'0','1')
					) as times,
					SEC_TO_TIME(
						SUM(
							TIME_TO_SEC(
								IF( f.RecordStatus=3,
									'00:00:00',
									IF( TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'),
										'00:00:00',
										TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart)
									)
								)
							)
						)
					) as hours,
					f.RecordStatus
				FROM
					INTRANET_USERGROUP as c
				INNER JOIN
					INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
				INNER JOIN
					INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
				INNER JOIN
					INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID
						and (e.RecordStatus Is Null Or e.RecordStatus = 1)
						and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
				INNER JOIN
					INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID";
if ($libenroll->enableUserJoinDateRange()) {
	$sql .=" 
		AND (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%' OR EnrolAvailiableDateStart <= ActivityDateStart)
		AND (EnrolAvailiableDateEnd IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%' OR EnrolAvailiableDateEnd >= ActivityDateEnd)";
}
$sql .=") WHERE
					c.EnrolGroupID in ($selectedClub)
				AND
					c.UserID in ($studentID_str)
				GROUP BY
					d.UserID
				$having_con
				ORDER BY
					$order_by, d.ClassName, d.ClassNumber
		";
					
$result_sort = $libenroll->returnArray($sql);	

$data_ary = array();
$display = "";

$data_ary_sort = array();

if(!empty($result) && !empty($result_sort))
{		
	
	### build data array
		foreach($result as $k=>$d)
		{
			list($this_userID, $this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours) = $d;
			$data_ary[$this_ClassName][$this_name][] = array($this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours);
		}
	
	
	if($displayUnit==0)	# student
	{
		
		### build sorting array
		foreach($result_sort as $kt=>$dt)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $dt;
			$data_sorting[$this_ClassName][] = $this_student;
		}
		//2017/01/25 Villa
// 		if(count($data_sorting)>1)
// 			$class_sorted = array_merge(array_flip(array_keys($data_sorting)), $data_ary);
// 		else 
// 			$class_sorted=$data_ary;
		foreach ($data_sorting as $_class => $_data_sorting){
			foreach ($_data_sorting as $student_name){
				$class_sorted[$_class][$student_name] = $data_ary[$_class][$student_name];
			}
		}
		foreach($class_sorted as $this_ClassName=>$d)
		{	
				if (count($data_sorting[$this_ClassName])>1)
					$name_sorted = array_merge(array_flip($data_sorting[$this_ClassName]), $d);
				else 
					$name_sorted = $d;
							
			foreach($name_sorted as $this_name=>$d1)
			{	
				$temp_display="";
				
				$temp_display .= $linterface->GET_NAVIGATION2_IP25($this_ClassName);
				$temp_display .= "<br style='clear:both;'>";
				$temp_display .= $linterface->GET_NAVIGATION2_IP25($this_name);
				
				$temp_display .= "<table class='common_table_list_v30 view_table_list_v30'>";
				$temp_display .= "<tr>";
				$temp_display .= "<th width='20%'>".$i_UserName."</th>";
				$temp_display .= "<th width='15%'>".$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"]."</th>";
				$temp_display .= "<th width='15%'>".$i_ClassNumber."</th>";
				$temp_display .= "<th width='15%'>". $Lang['General']['Date'] ."</th>";
				$temp_display .= "<th width='20%'>".$i_ClubsEnrollment_GroupName."</th>";
				$temp_display .= "<th width='15%'>".$Lang['eEnrolment']['ActivityParticipationReport']['Hour(s)']."</th>";
				$temp_display .= "</tr>";
				
				$this_student_total_hours = 0;
				$this_student_total_times = 0;
				
				foreach($d1 as $k1=>$d11)
				{
					list($this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours) = $d11;		
					$this_hr = $libenroll->timeToDecimal($this_hours);
					$this_date = substr($this_date,0,10);
					$this_student_total_hours += $this_hr;
					$this_student_total_times++;
					
					$temp_display .= "<tr>";
					$temp_display .= "<td>$this_name</td>";
					$temp_display .= "<td>$this_ClassName</td>";
					$temp_display .= "<td>$this_ClassNumber</td>";
					$temp_display .= "<td>$this_date</td>";
					$temp_display .= "<td>$this_title</td>";
					$temp_display .= "<td>$this_hr</td>";
					$temp_display .= "</tr>";
				}
				$temp_display .= "<tr>";
				$temp_display .= "<td colspan='5' align='right'>$list_total</td>";
				$temp_display .= "<td>$this_student_total_hours</td>";
				$temp_display .= "</table><br>";
				// Time and Hour setting check
				if($times)
				{
					if(($timesRange && $this_student_total_times > $times) || (!$timesRange && $this_student_total_times < $times)){
						$temp_display="";
						$this_student_total_times=0;
					}
				}
				if($hours)
				{
					if(($hoursRange && $this_student_total_hours > $hours) || (!$hoursRange && $this_student_total_hours < $hours)){
						$temp_display="";
						$this_student_total_hours=0;
					}
				}
				$display .= $temp_display;
			}
		}
	}
	else 	# class
	{	
		### build sorting array
		foreach($result_sort as $k=>$d)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d;
			$this_hr = $libenroll->timeToDecimal($this_hours);
			$data_ary_sort[$this_ClassName]['t'] += $this_times;
			$data_ary_sort[$this_ClassName]['h'] += $this_hr;
		}
		
		if($sortBy!=0)
		{
			# sorting 
			foreach ($data_ary_sort as $key => $row) {
			    $t[$key]  = $row['t'];
			    $h[$key] = $row['h'];
			}

			switch($sortBy)
			{
				case 1:
					$sort_field = $t;	
					break;
				case 2:
					$sort_field = $h;	
					break;
			}
			$sort_order = $sortByOrder ? SORT_DESC : SORT_ASC;
			array_multisort($sort_field, $sort_order, $data_ary_sort);
		}
		if(count($data_ary_sort)>1)
			$class_sorted = array_merge(array_flip(array_keys($data_ary_sort)), $data_ary);
		else 
			$class_sorted = $data_ary;

		foreach($class_sorted as $this_ClassName=>$d)
		{	
			$temp_display = "";
			$temp_display .= $linterface->GET_NAVIGATION2_IP25($this_ClassName);
			$temp_display .= "<table class='common_table_list_v30 view_table_list_v30'>";
			$temp_display .= "<tr>";
			$temp_display .= "<th width='20%'>".$i_UserName."</th>";
			$temp_display .= "<th width='15%'>".$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"]."</th>";
			$temp_display .= "<th width='15%'>".$i_ClassNumber."</th>";
			$temp_display .= "<th width='15%'>". $Lang['General']['Date'] ."</th>";
			$temp_display .= "<th width='20%'>".$i_ClubsEnrollment_GroupName."</th>";
			$temp_display .= "<th width='15%'>".$Lang['eEnrolment']['ActivityParticipationReport']['Hour(s)']."</th>";
			$temp_display .= "</tr>";
			
			$this_class_total_hours = 0;
			
			foreach($d as $this_name=>$d1)
			{	
				$temp1_display="";
							
				$this_student_total_hours = 0;
				$this_student_total_times = 0;
				
				foreach($d1 as $k1=>$d11)
				{
					list($this_ClassName, $this_ClassNumber, $this_name, $this_date, $this_title, $this_hours) = $d11;		
					$this_hr = $libenroll->timeToDecimal($this_hours);
					$this_date = substr($this_date,0,10);
					$this_student_total_hours += $this_hr;
					$this_student_total_times++;
					
					$temp1_display .= "<tr>";
					$temp1_display .= "<td>$this_name</td>";
					$temp1_display .= "<td>$this_ClassName</td>";
					$temp1_display .= "<td>$this_ClassNumber</td>";
					$temp1_display .= "<td>$this_date</td>";
					$temp1_display .= "<td>$this_title</td>";
					$temp1_display .= "<td>$this_hr</td>";
					$temp1_display .= "</tr>";
				}
				
				// Time and Hour setting check
				if($times)
				{
					if(($timesRange && $this_student_total_times > $times) || (!$timesRange && $this_student_total_times < $times)){
						$temp1_display="";
						$this_student_total_hours=0;
					}
				}
				if($hours)
				{
					if(($hoursRange && $this_student_total_hours > $hours) || (!$hoursRange && $this_student_total_hours < $hours)){
						$temp1_display="";
						$this_student_total_hours=0;
					}
				}
				$temp_display .= $temp1_display;
				$this_class_total_hours += $this_student_total_hours;
			}
			
			if($this_class_total_hours!=0){
				$display .= $temp_display;
				$display .= "<tr>";
				$display .= "<td colspan='5' align='right'>$list_total</td>";
				$display .= "<td>$this_class_total_hours</td>";
				$display .= "</table><br>";
			}
		}
		
	}
	
}
?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<!-- Display result //-->
<?=$display?>
<?
intranet_closedb();
// include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>