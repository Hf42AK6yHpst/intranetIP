<?php
// using: 

############## Change Log [start] ####
#	Date: 2017-11-22	Omas
#		- remove those getJQuerySaveId for ajax - ajax do not need that -#C131627 
#
#	Date: 2015-12-05	Kenneth
#		- Add Category as filter
#
############## Change Log [End] ####

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

# user access right checking
$libclass = new libclass();
$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$libenroll->hasAccessRight($_SESSION['UserID'], 'AnyAdminOrHelperOfClubAndActivity');

$CurrentPage = "PageAttendanceStatusReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['AttendanceStatusReport'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$divName = 'reportOptionOuterDiv';
$tdId = 'tdOption';
$x = '';
$x .= '<div id="showHideOptionDiv" style="display:none;">'."\n";
	$x .= '<table style="width:100%;">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td id="tdOption" class="report_show_option">'."\n";
				$x .= '<span id="spanShowOption_'.$divName.'">'."\n";
					$x .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName', '$tdId');", '', '', 'spanShowOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<span id="spanHideOption_'.$divName.'" style="display:none">'."\n";
					$x .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('$divName', '$tdId');", '', '', 'spanHideOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<div id="'.$divName.'" style="display:none;">'."\n";
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionOuterDiv'] = $x;


$divName = 'reportOptionDiv';
$x = '';
$x .= '<div id="'.$divName.'">'."\n";
	$x .= '<div>'."\n";
		$x .= '<table id="filterOptionTable" class="form_table_v30">'."\n";
			# Target Date
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Date'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="startDateDatePicker"></div>'."\n";
					$x .= $linterface->Get_Form_Warning_Msg('inputDateWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['InputDate'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Student Source
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentSource'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_Radio_Button('studentSource_FormClass', 'studentSource', 'formClass', $isChecked=true, $Class="", $Lang['General']['Class'], 'changedStudentSource(this.value);')."\n";
					$x .= $linterface->Get_Radio_Button('studentSource_Individual', 'studentSource', 'individualStudent', $isChecked=false, $Class="", $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['IndividualStudent'], 'changedStudentSource(this.value);')."\n";
					$x .= $linterface->Get_Radio_Button('studentSource_Club', 'studentSource', 'club', $isChecked=false, $Class="", $eEnrollment['club'], 'changedStudentSource(this.value);')."\n";
					$x .= $linterface->Get_Radio_Button('studentSource_Activity', 'studentSource', 'activity', $isChecked=false, $Class="", $eEnrollment['activity'], 'changedStudentSource(this.value);')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			### Club Category Selection For Select students
			$CategorySelection = $libenroll->GET_CATEGORY_SEL($studentSourceClubTargetCategory, $ParSelected=1, 'studentSource_filterByCategory();', $ParSelLang, $noFirst=1, $CategoryTypeID=0,$IsMultiple=1, true,$DeflautName='studentSourceClubTargetCategory');
			$CategorySelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('studentSourceClubTargetCategory',1);studentSource_filterByCategory()");
			$CategorySelection .= $linterface->spacer();  	 
			$CategorySelection .= $linterface->MultiSelectionRemark(); 
			$categoryRowHTML = "<tr id='studentSource_clubSelector'' class='studentSourceTr'>";
			$categoryRowHTML .= "<td class='field_title'>".$linterface->RequiredSymbol().$eEnrollment['category']."</td>";
			$categoryRowHTML .= "<td valign='top' class='tabletext''>";
			$categoryRowHTML .= $CategorySelection;	//Club selection list
			$categoryRowHTML .= "</td>";
			$categoryRowHTML .= "</tr>";
			### Club Category Selection End
			$x .= $categoryRowHTML;
			
			
			// Club
			$studentSource_clubSel = $libenroll_ui->Get_Club_Selection('studentSource_clubSel', 'studentSource_clubIdAry[]', $TargetEnrolGroupID, $CategoryID='', $IsAll=0, $NoFirst=1, 'changedStudentSourceSelection(\'club\');', $AcademicYearID='', $IncludeEnrolGroupIdAry='', $Disabled=false, $Class='preserveValue', $isMultiple=true,'',$ParDefaultSelectedAll=true);
			$studentSource_clubSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('studentSource_clubSel', 1, 'changedStudentSourceSelection(\'club\');');");
			
			
			$x .= '<tr id="studentSource_clubTr" class="studentSourceTr">'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['club'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($studentSource_clubSel, $studentSource_clubSelectAllBtn, 'studentSource_clubSelectionBox');
					$x .= $linterface->Get_Form_Warning_Msg('studentSource_clubWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectClub'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			// Activity
			$studentSource_activitySel = $libenroll_ui->Get_Activity_Selection('studentSource_activitySel', 'studentSource_activityIdAry[]', $TargetEnrolEventID, $CategoryID='', $IsAll=0, $NoFirst=1, 'changedStudentSourceSelection(\'activity\');', $AcademicYearID='', $isMultiple=true);
			$studentSource_activitySelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('studentSource_activitySel', 1, 'changedStudentSourceSelection(\'activity\');');");
			$x .= '<tr id="studentSource_activityTr" class="studentSourceTr" style="display:none;">'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['activity'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($studentSource_activitySel, $studentSource_activitySelectAllBtn, 'studentSource_activitySelectionBox');
					$x .= $linterface->Get_Form_Warning_Msg('studentSource_activityWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectActivity'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			// Form Class
			$studentSource_classSel = $libclass->getSelectClassID('id="studentSource_classSel" name="studentSource_classIdAry[]" multiple="multiple" size="10" onchange="changedStudentSourceSelection(\'formClass\');"', $selected="", $DisplaySelect=2);
			$studentSource_classSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('studentSource_classSel', 1, 'changedStudentSourceSelection(\'formClass\');');");
			$x .= '<tr id="studentSource_formClassTr" class="studentSourceTr" style="display:none;">'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['class'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($studentSource_classSel, $studentSource_classSelectAllBtn);
					$x .= $linterface->Get_Form_Warning_Msg('studentSource_formClassWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectClass'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			// Individual Student
			$x .= '<tr id="studentSource_individualStudentTr" class="studentSourceTr" style="display:none;">'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['IndividualStudent'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= '<table class="inside_form_table_v30">'."\n";
						// Student Name
						$x .= '<tr>'."\n";
							$x .= '<td>'.$linterface->Get_Radio_Button('studentSource_individualStudent_studentNameRadio', 'studentSource_individualStudent', 'studentName', $isChecked=true, $Class="", $thisDisplay='', 'changedStudentSourceIndividualStudentRadio(this.value);').'</td>'."\n";
							$x .= '<td><label for="studentSource_individualStudent_studentNameRadio">'.$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentName'].'</label></td>'."\n";
							$x .= '<td>'."\n";
								$x .= $linterface->GET_TEXTBOX('studentSource_studentNameTb', 'studentSource_studentName', $Value='', $OtherClass='', array('onclick'=>'changedStudentSourceIndividualStudentRadio(\'studentName\');'));
								$x .= $linterface->Get_Form_Warning_Msg('studentSource_studentNameWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['InputStudentName'], $Class='warnMsgDiv')."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						// Student Login
						$x .= '<tr>'."\n";
							$x .= '<td>'.$linterface->Get_Radio_Button('studentSource_individualStudent_studentLoginRadio', 'studentSource_individualStudent', 'studentLogin', $isChecked=false, $Class="", $thisDisplay, 'changedStudentSourceIndividualStudentRadio(this.value);').'</td>'."\n";
							$x .= '<td><label for="studentSource_individualStudent_studentLoginRadio">'.$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentLogin'].'</label></td>'."\n";
							$x .= '<td>'."\n";
								$x .= $linterface->GET_TEXTBOX('studentSource_studentLoginTb', 'studentSource_studentLogin', $Value='', $OtherClass='', array('disabled'=>'disabled', 'onclick'=>'changedStudentSourceIndividualStudentRadio(\'studentLogin\');'));
								$x .= $linterface->Get_Form_Warning_Msg('studentSource_studentLoginWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['InputStudentName'], $Class='warnMsgDiv')."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						// Student Class Name and Class Number
						$x .= '<tr>'."\n";
							$x .= '<td>'.$linterface->Get_Radio_Button('studentSource_individualStudent_studentClassNameClassNumberRadio', 'studentSource_individualStudent', 'studentClassNameClassNumber', $isChecked=false, $Class="", $thisDisplay, 'changedStudentSourceIndividualStudentRadio(this.value);').'</td>'."\n";
							$x .= '<td><label for="studentSource_individualStudent_studentClassNameClassNumberRadio">'.$Lang['General']['Class'].'</lable></td>'."\n";
							$x .= '<td>'.$libclass->getSelectClassID('id="studentSource_individualStudent_classSel" name="studentSource_individualStudent_classId" onchange="changedStudentSourceIndividualStudentClassSelection();" disabled="disabled"', $selected="", $DisplaySelect=2).'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td><label for="studentSource_individualStudent_studentClassNameClassNumberRadio">'.$Lang['General']['ClassNumber'].'</lable></td>'."\n";
							$x .= '<td><span id="studentSource_individualStudent_classNumberSelSpan"></span></td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			// Student
			$studentSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('studentSel', 1);");
			$x .= '<tr id="studentTr">'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['student'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div('', $studentSelectAllBtn, 'studentSelectionSpan');
					$x .= $linterface->Get_Form_Warning_Msg('studentSelectionWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectClass'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
			
			### Club Category Selection For Attendance Source
			$CategorySelection = $libenroll->GET_CATEGORY_SEL($studentSourceClubTargetCategory, $ParSelected=1, 'attendanceSource_filterClubByCategory();', $ParSelLang, $noFirst=1, $CategoryTypeID=0,$IsMultiple=1, true,$DeflautName='attendanceSourceClubTargetCategory');
			$CategorySelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('attendanceSourceClubTargetCategory',1);attendanceSource_filterClubByCategory()");
			$CategorySelection .= $linterface->spacer();  	 
			$CategorySelection .= $linterface->MultiSelectionRemark(); 
			

			$categoryRowHTML = $CategorySelection;	//Club selection list
			### Club Category Selection End
			
			
			# Attendance Source
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceSource'].'</td>'."\n";
				$x .= '<td>'."\n";
					
					$x .= $linterface->Get_Checkbox('attendanceSourceGlobalChk', $Name, $Value, $isChecked=true, $Class="", $Lang['Btn']['SelectAll'], 'clickedAttenanceSourceCheckbox(\'global\', this.checked);');
					$x .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
					$x .= $linterface->Get_Checkbox('attendanceSourceChk_club', 'attendanceSource[]', 'club', $isChecked=true, $Class='', $eEnrollment['club'], 'clickedAttenanceSourceCheckbox(\'club\', this.checked);');
					$x .= $linterface->Get_Checkbox('attendanceSourceChk_activity', 'attendanceSource[]', 'activity', $isChecked=true, $Class="", $eEnrollment['activity'], 'clickedAttenanceSourceCheckbox(\'activity\', this.checked);');
					$x .= $linterface->Get_Form_Warning_Msg('attendanceSourceWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['ChooseSource'], $Class='warnMsgDiv')."\n";
					$x .= '<br />'."\n";
					$x .= '<br />'."\n";
					$x .= $categoryRowHTML;
					$x .= '<div id="attendanceSourceDiv_club">'."\n";
						$attendanceSource_clubSel = $libenroll_ui->Get_Club_Selection('attendanceSource_clubSel', 'attendanceSource_clubIdAry[]', $TargetEnrolGroupID, $CategoryID='', $IsAll=0, $NoFirst=1, '', $AcademicYearID='', $IncludeEnrolGroupIdAry='', $Disabled=false, $Class='', $isMultiple=true);
						$attendanceSource_clubSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('attendanceSource_clubSel', 1);");
						$x .= '<b>'.$eEnrollment['club'].'</b>'."\n";
						$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($attendanceSource_clubSel, $attendanceSource_clubSelectAllBtn, 'attendanceSource_clubSelectionBox')."\n";
						$x .= $linterface->Get_Form_Warning_Msg('attendanceSource_clubWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectClub'], $Class='warnMsgDiv')."\n";
						$x .= '<br />'."\n";
						
					$x .= '</div>'."\n";
					
					$x .= '<div id="attendanceSourceDiv_activity">'."\n";
						$attendanceSource_activitySel = $libenroll_ui->Get_Activity_Selection('attendanceSource_activitySel', 'attendanceSource_activityIdAry[]', $TargetEnrolEventID, $CategoryID='', $IsAll=0, $NoFirst=1, '', $AcademicYearID='', $isMultiple=true);
						$attendanceSource_activitySelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('attendanceSource_activitySel', 1);");
						$x .= '<b>'.$eEnrollment['activity'].'</b>'."\n";
						$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($attendanceSource_activitySel, $attendanceSource_activitySelectAllBtn, 'attendanceSource_activitySelectionBox')."\n";
						$x .= $linterface->Get_Form_Warning_Msg('attendanceSource_activityWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['SelectActivity'], $Class='warnMsgDiv')."\n";
						$x .= '<br />'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			# Attendance Status
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceStatus'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_Checkbox('attendanceStatusGlobalChk', $Name, $Value, $isChecked=false, $Class="", $Lang['Btn']['SelectAll'], 'Set_Checkbox_Value(\'attendanceStatus[]\', this.checked);');
					$x .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
					$x .= $linterface->Get_Checkbox('attendanceStatusChk_'.ENROL_ATTENDANCE_PRESENT, 'attendanceStatus[]', ENROL_ATTENDANCE_PRESENT, $isChecked=false, $Class='', $libenroll_ui->Get_Attendance_Display(ENROL_ATTENDANCE_PRESENT), 'Uncheck_SelectAll(\'attendanceStatusGlobalChk\', this.checked);');
					$x .= $linterface->Get_Checkbox('attendanceStatusChk_'.ENROL_ATTENDANCE_EXEMPT, 'attendanceStatus[]', ENROL_ATTENDANCE_EXEMPT, $isChecked=false, $Class='', $libenroll_ui->Get_Attendance_Display(ENROL_ATTENDANCE_EXEMPT), 'Uncheck_SelectAll(\'attendanceStatusGlobalChk\', this.checked);');
					$x .= $linterface->Get_Checkbox('attendanceStatusChk_'.ENROL_ATTENDANCE_ABSENT, 'attendanceStatus[]', ENROL_ATTENDANCE_ABSENT, $isChecked=true, $Class='', $libenroll_ui->Get_Attendance_Display(ENROL_ATTENDANCE_ABSENT), 'Uncheck_SelectAll(\'attendanceStatusGlobalChk\', this.checked);');
					if ($libenroll->enableAttendanceLateStatusRight()) {
						$x .= $linterface->Get_Checkbox('attendanceStatusChk_'.ENROL_ATTENDANCE_LATE, 'attendanceStatus[]', ENROL_ATTENDANCE_LATE, $isChecked=false, $Class='', $libenroll_ui->Get_Attendance_Display(ENROL_ATTENDANCE_LATE), 'Uncheck_SelectAll(\'attendanceStatusGlobalChk\', this.checked);');
					}
					if ($libenroll->enableAttendanceEarlyLeaveStatusRight()) {
						$x .= $linterface->Get_Checkbox('attendanceStatusChk_'.ENROL_ATTENDANCE_EARLY_LEAVE, 'attendanceStatus[]', ENROL_ATTENDANCE_EARLY_LEAVE, $isChecked=false, $Class='', $libenroll_ui->Get_Attendance_Display(ENROL_ATTENDANCE_EARLY_LEAVE), 'Uncheck_SelectAll(\'attendanceStatusGlobalChk\', this.checked);');
					}
					$x .= $linterface->Get_Form_Warning_Msg('attendanceStatusWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['ChooseStatus'], $Class='warnMsgDiv')."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
						
			# Show Main Guardian Information
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['ShowMainGuardianInfo'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_Radio_Button('showMainGuardianInfo_Yes', 'showMainGuardianInfo', 1, $isChecked=true, $Class="", $Lang['General']['Yes'])."\n";
					$x .= $linterface->Get_Radio_Button('showMainGuardianInfo_No', 'showMainGuardianInfo', 0, $isChecked=false, $Class="", $Lang['General']['No'])."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .= '<br style="clear:both;" />'."\n";
	
	$x .= '<div style="float:left;">'."\n";
		$x .= $linterface->MandatoryField();
	$x .= '</div>'."\n";
	$x .= '<br style="clear:both;" />'."\n";
	
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionDiv'] = $x;

$x = '';
$x .= '<div id="contentToolDiv" class="content_top_tool" style="display:none;">'."\n";
	$x .= '<div class="Conntent_tool">'."\n";
		$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
		$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['contentToolDiv'] = $x;

$htmlAry['reportResultDiv'] = '<div id="reportResultDiv"></div>';

?>
<script type="text/javascript">
$(document).ready(function () {
	Block_Document();
	
	// initialize date picker
	initDatePicker('startDateDatePicker', 'targetStartDate');
	
	// initialize the selected student selection
	changedStudentSource('formClass');
	
	// initialize the individual student class number selection  
	changedStudentSourceIndividualStudentClassSelection();
	
	js_Select_All('attendanceSource_clubSel', 1);
	js_Select_All('attendanceSource_activitySel', 1);
	
	UnBlock_Document();
});

function refreshReport() {
	var canSubmit = true;
	var isFocused = false;
	var studentSource_Club = $('input#studentSource_Club').attr('checked');
	
	var studentSource_Activity = $('input#studentSource_Activity').attr('checked');
	var studentSource_Class = $('input#studentSource_FormClass').attr('checked');
	var studentSource_Individual = $('input#studentSource_Individual').attr('checked');
	
	$('div.warnMsgDiv').hide();
	
	// input date
	if (Trim($('input#targetStartDate').val()) == '') {
		canSubmit = false;
		$('div#inputDateWarnMsgDiv').show();
		
		if (!isFocused) {
			$('input#targetStartDate').focus();
			isFocused = true;
		}
	}
	
	// choose club if select student by clubs
	if (studentSource_Club && $('select#studentSource_clubSel option:selected').length==0) {
		canSubmit = false;
		$('div#studentSource_clubWarnMsgDiv').show();
		if (!isFocused) {
			$('select#studentSource_clubSel').focus();
			isFocused = true;
		}
	}
	
	// choose activity if select student by activities
	if (studentSource_Activity && $('select#studentSource_activitySel option:selected').length==0) {
		canSubmit = false;
		$('div#studentSource_activityWarnMsgDiv').show();
		if (!isFocused) {
			$('select#studentSource_activitySel').focus();
			isFocused = true;
		}
	}
	
	// choose class if select student by class
	if (studentSource_Class && $('select#studentSource_classSel option:selected').length==0) {
		canSubmit = false;
		$('div#studentSource_formClassWarnMsgDiv').show();
		if (!isFocused) {
			$('select#studentSource_classSel').focus();
			isFocused = true;
		}
	}
	
	// choose student
	if ((studentSource_Club || studentSource_Activity || studentSource_Class) && $('select#studentSel option:selected').length==0) {
		canSubmit = false;
		$('div#studentSelectionWarnMsgDiv').show();
		if (!isFocused) {
			$('select#studentSel').focus();
			isFocused = true;
		}
	}
	
	if (studentSource_Individual) {
		// input student name
		if ($('input#studentSource_individualStudent_studentNameRadio').attr('checked') && Trim($('input#studentSource_studentNameTb').val()) == '') {
			canSubmit = false;
			$('div#studentSource_studentNameWarnMsgDiv').show();
			if (!isFocused) {
				$('input#studentSource_studentNameTb').focus();
				isFocused = true;
			}
		}
		
		// input student userlogin
		if ($('input#studentSource_individualStudent_studentLoginRadio').attr('checked') && Trim($('input#studentSource_studentLoginTb').val()) == '') {
			canSubmit = false;
			$('div#studentSource_studentLoginWarnMsgDiv').show();
			if (!isFocused) {
				$('input#studentSource_studentLoginTb').focus();
				isFocused = true;
			}
		}
	}
	
	// choose attendance source
	var attendanceSourceChkName = getJQuerySaveId('attendanceSource[]');
	if ($('input[name="' + attendanceSourceChkName + '"]:checked').length == 0) {
		canSubmit = false;
		$('div#attendanceSourceWarnMsgDiv').show();
		if (!isFocused) {
			$('input#attendanceSourceChk_club').focus();
			isFocused = true;
		}
	}
	
	// choose club attendance source
	if ($('input#attendanceSourceChk_club').attr('checked') && $('select#attendanceSource_clubSel option:selected').length==0) {
		canSubmit = false;
		$('div#attendanceSource_clubWarnMsgDiv').show();
		if (!isFocused) {
			$('input#attendanceSource_clubSel').focus();
			isFocused = true;
		}
	}
	
	// choose activity attendance source
	if ($('input#attendanceSourceChk_activity').attr('checked') && $('select#attendanceSource_activitySel option:selected').length==0) {
		canSubmit = false;
		$('div#attendanceSource_activityWarnMsgDiv').show();
		if (!isFocused) {
			$('input#attendanceSource_activitySel').focus();
			isFocused = true;
		}
	}
	
	// choose attendance status
	var attendanceStatusChkName = getJQuerySaveId('attendanceStatus[]');
	if ($('input[name="' + attendanceStatusChkName + '"]:checked').length == 0) {
		canSubmit = false;
		$('div#attendanceStatusWarnMsgDiv').show();
		if (!isFocused) {
			$('input#attendanceStatusChk_1').focus();
			isFocused = true;
		}
	}
	
	
	if (canSubmit) {
		Block_Document();
		if (Trim($('div#reportOptionOuterDiv').html()) == '') {
			saveFormValues();
			
			$('div#reportOptionDiv > div').clone(true, true).appendTo('div#reportOptionOuterDiv');
			$('div#reportOptionDiv').html('');
			$('div#showHideOptionDiv').show();
			
			initDatePicker('startDateDatePicker', 'targetStartDate', $('body').data('targetStartDate'));
			
			loadFormValues();
			$('div#contentToolDiv').show();
		}
		else {
			$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
			//$('div#reportOptionOuterDiv').hide();
			
		}
		
		$('div#reportResultDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		
		$.ajax({
			url:      	"ajax_reload.php",
			type:     	"POST",
			data:     	$("#form1").serialize() + '&RecordType=reportResult',
			success:  	function(xml) {
							$('div#reportResultDiv').html(xml);
							
							
							Scroll_To_Top();
							UnBlock_Document();
						}
		});
	}
}

function initDatePicker(divId, inputName, defaultDate) {
	defaultDate = defaultDate || '';
	$('div#' + divId).load(
		"ajax_reload.php", 
		{ 
			RecordType: 'datePicker',
			inputName: inputName,
			defaultDate: defaultDate,
			inputClass: 'preserveValue'
		},
		function(ReturnData) {
			
		}
	);
}

function changedStudentSource(targetSource) {
	$('tr.studentSourceTr').hide();
	
	if (targetSource == 'club') {
		$('tr#studentSource_clubSelector').show();
		$('tr#studentSource_clubTr').show();
		$('tr#studentTr').show();
	}
	else if (targetSource == 'activity') {
		$('tr#studentSource_clubSelector').show();
		$('tr#studentSource_activityTr').show();
		$('tr#studentTr').show();
		$('select#studentSource_activitySel option').attr("selected", "selected");
		
	}
	else if (targetSource == 'formClass') {
		$('tr#studentSource_formClassTr').show();
		$('tr#studentTr').show();
	}
	else if (targetSource == 'individualStudent') {
		$('tr#studentSource_individualStudentTr').show();
		$('tr#studentTr').hide();
	}
	
	var selectionId = getSelectionIdByStudentSource(targetSource);
	if ($('select#' + selectionId + ' option:selected').length == 0) {
		$('select#' + selectionId + ' option:first').attr('selected', 'selected');
	}
	changedStudentSourceSelection(targetSource);
}

function changedStudentSourceSelection(targetSource) {
	var selectionId = getSelectionIdByStudentSource(targetSource);
	
	var selectedIdAry = new Array();
	$('select#' + selectionId + ' option:selected').each( function() {
		selectedIdAry[selectedIdAry.length] = $(this).val();
	});
	var selectedIdList = selectedIdAry.join(',');
	
	$('span#studentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'studentSelection',
			SourceType: targetSource,
			SelectedIdList: selectedIdList,
			studentSelId: 'studentSel',
			studentSelName: 'studentIdAry[]'
		},
		function(ReturnData) {
			js_Select_All('studentSel', 1);
		}
	);
	
	if (targetSource == 'club' || targetSource == 'activity') {
		// preset the club or activity data source selection
		
		// uncheck the select all checkbox
		if ($('input#attendanceSourceGlobalChk').attr('checked')) {
			$('input#attendanceSourceGlobalChk').click();
		}
		
		// check the selected data source checkbox
		if (!$('input#attendanceSourceChk_' + targetSource).attr('checked')) {
			$('input#attendanceSourceChk_' + targetSource).click();
		}
		
		// if "club" => uncheck "activity", if "activity" => uncheck "club"
		if (targetSource == 'club') {
			if ($('input#attendanceSourceChk_activity').attr('checked')) {
				$('input#attendanceSourceChk_activity').click();
			}
		}
		else if (targetSource == 'activity') {
			if ($('input#attendanceSourceChk_club').attr('checked')) {
				$('input#attendanceSourceChk_club').click();
			}
		}
		
		// assign the student source selection value to the data source selection
		var studentSourceSelId = 'studentSource_' + targetSource + 'Sel';
		var attendanceDataSourceSelId = 'attendanceSource_' + targetSource + 'Sel';
		$('select#' + attendanceDataSourceSelId).val($('select#' + studentSourceSelId).val());
		
		
	}
}

function getSelectionIdByStudentSource(targetSource) {
	var selectionId = '';
	if (targetSource == 'club') {
		selectionId = 'studentSource_clubSel';
	}
	else if (targetSource == 'activity') {
		selectionId = 'studentSource_activitySel';
	}
	else if (targetSource == 'formClass') {
		selectionId = 'studentSource_classSel';
	}
	
	return selectionId;
}

function changedStudentSourceIndividualStudentRadio(targetSource) {
	var studentNameTbId = 'studentSource_studentNameTb';
	var studentLoginTbId = 'studentSource_studentLoginTb';
	var studentClassNameSelId = 'studentSource_individualStudent_classSel';
	var studentClassNumberSelId = 'studentSource_individualStudent_classNumberSel';
	
	$('input#' + studentNameTbId).attr('disabled', 'disabled');
	$('input#' + studentLoginTbId).attr('disabled', 'disabled');
	$('select#' + studentClassNameSelId).attr('disabled', 'disabled');
	$('select#' + studentClassNumberSelId).attr('disabled', 'disabled');
	if (targetSource == 'studentName') {
		$('input#' + studentNameTbId).attr('disabled', '').focus();
	}
	else if (targetSource == 'studentLogin') {
		$('input#' + studentLoginTbId).attr('disabled', '').focus();
	}
	else if (targetSource == 'studentClassNameClassNumber') {
		$('select#' + studentClassNameSelId).attr('disabled', '');
		$('select#' + studentClassNumberSelId).attr('disabled', '');
	}
}

function changedStudentSourceIndividualStudentClassSelection() {
	var selectedClassId = $('select#studentSource_individualStudent_classSel').val();
	var disableSelection = ($('input#studentSource_individualStudent_studentClassNameClassNumberRadio').attr('checked'))? 0 : 1;
	$('span#studentSource_individualStudent_classNumberSelSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'classNumberSelection',
			ClassID: selectedClassId,
			DisableSelection: disableSelection,
			SelectionID: 'studentSource_individualStudent_classNumberSel',
			SelectionName: 'studentSource_individualStudent_classNumber'
		},
		function(ReturnData) {
			js_Select_All('studentSel', 1);
		}
	);
}

function clickedAttenanceSourceCheckbox(targetSource, elementChecked) {
	if (targetSource == 'global') {
		Set_Checkbox_Value('attendanceSource[]', elementChecked);
		
		clickedAttenanceSourceCheckbox('club', elementChecked);
		clickedAttenanceSourceCheckbox('activity', elementChecked);
	}
	else {
		Uncheck_SelectAll('attendanceSourceGlobalChk', elementChecked);
	
		var selectionDivId = 'attendanceSourceDiv_' + targetSource;
		if (elementChecked) {
			$('div#' + selectionDivId).show();
		}
		else {
			$('div#' + selectionDivId).hide();
		}
	}
}

function goExport() {
	$('form#form1').attr('target', '_self').attr('action', 'export.php').submit();
}

function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}

function studentSource_filterByCategory(){
	var selectedCategory = $('#studentSourceClubTargetCategory').val();

	var selectedCategoryStr = selectedCategory.join(',');


	$('span#studentSource_clubSelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'clubSelection',
			CategoryIDList: selectedCategoryStr,
			SelectionID: 'studentSource_clubSel',
			//SelectionName: getJQuerySaveId('studentSource_clubIdAry[]'),
			SelectionName: 'studentSource_clubIdAry[]',
			OnChange: 'changedStudentSourceSelection(\'club\');'
			
		},
		function(ReturnData) {
			js_Select_All('studentSource_clubSel', 1);
			changedStudentSourceSelection('club');
		}
	);
	
	$('span#studentSource_activitySelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'activitySelection',
			CategoryIDList: selectedCategoryStr,
			SelectionID: 'studentSource_activitySel',
			//SelectionName: getJQuerySaveId('studentSource_activityIdAry[]'),
			SelectionName: 'studentSource_activityIdAry[]',
			OnChange: 'changedStudentSourceSelection(\'activity\');'
					
		},
		function(ReturnData) {
			js_Select_All('studentSource_activitySel', 1);
			
		}
	);
}

function attendanceSource_filterClubByCategory(){

	var selectedCategory = $('#attendanceSourceClubTargetCategory').val();
	var selectedCategoryStr =selectedCategory.join(',');
	
	$('span#attendanceSource_clubSelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'clubSelection',
			CategoryIDList: selectedCategoryStr,
			SelectionID: 'attendanceSource_clubSel',
			//SelectionName: getJQuerySaveId('attendanceSource_clubIdAry[]'),
			SelectionName: 'attendanceSource_clubIdAry[]',
			OnChange: ''
			
		},
		function(ReturnData) {
			js_Select_All('attendanceSource_clubSel', 1);
			
		}
	);
	$('span#attendanceSource_activitySelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'activitySelection',
			CategoryIDList: selectedCategoryStr,
			SelectionID: 'attendanceSource_activitySel',
			//SelectionName: getJQuerySaveId('attendanceSource_activityIdAry[]'),
			SelectionName: 'attendanceSource_activityIdAry[]',
			OnChange: ''
		},
		function(ReturnData) {
			js_Select_All('attendanceSource_activitySel', 1);
			
		}
	);
	
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['reportOptionOuterDiv']?>
	<br />
	
	<?=$htmlAry['reportOptionDiv']?>
	<?=$htmlAry['contentToolDiv']?>
	<?=$htmlAry['reportResultDiv']?>
	<br />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>