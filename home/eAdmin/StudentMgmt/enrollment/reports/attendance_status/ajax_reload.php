<?php
// using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'AnyAdminOrHelperOfClubAndActivity');
$linterface = new interface_html();
$libclass = new libclass();
$libenroll_report = new libclubsenrol_report();

$RecordType = $_POST['RecordType'];

if ($RecordType == 'datePicker') {
    $inputName= trim(stripslashes($_POST['inputName']));
	$defaultDate = trim(stripslashes($_POST['defaultDate']));
	$inputClass = trim(stripslashes($_POST['inputClass']));
		
	echo $linterface->GET_DATE_PICKER($inputName, $defaultDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum ".$inputClass);
}
else if ($RecordType == 'classNumberSelection') {
	$classId = $_POST['ClassID'];
	$disableSelection = $_POST['DisableSelection'];
	$selectionID = $_POST['SelectionID'];
	$selectionName = $_POST['SelectionName'];
	
	$studentInfoAry = $libclass->getStudentByClassId($classId);
	$classNumberAry = Get_Array_By_Key($studentInfoAry, 'ClassNumber');
	$classNumberAry = array_values(array_unique($classNumberAry));
	$numOfInfo = count($classNumberAry);
	
	$selectAry = array();
	for ($i=0; $i<$numOfInfo; $i++) {
		$_classNumber = $classNumberAry[$i];		
		$selectAry[$_classNumber] = $_classNumber;
	}
	
	if ($disableSelection) {
		$disableTag = ' disabled="disabled" ';
	}
	
	echo getSelectByAssoArray($selectAry, 'id="'.$selectionID.'" name="'.$selectionName.'"'.$disableTag, $selected="", $all=0, $noFirst=1);
}
else if ($RecordType == 'reportResult') {
	$targetStartDate = $_POST['targetStartDate'];
	$studentSource = $_POST['studentSource'];
	$studentIdAry = $_POST['studentIdAry'];
	$studentSource_individualStudent = $_POST['studentSource_individualStudent'];	// studentName, studentLogin, studentClassNameClassNumber
	$studentSource_studentName = trim(stripslashes($_POST['studentSource_studentName']));
	$studentSource_studentLogin = trim(stripslashes($_POST['studentSource_studentLogin']));
	$studentSource_individualStudent_classId = $_POST['studentSource_individualStudent_classId'];
	$studentSource_individualStudent_classNumber = $_POST['studentSource_individualStudent_classNumber'];
	$attendanceSource = $_POST['attendanceSource'];
	$attendanceSource_clubIdAry = $_POST['attendanceSource_clubIdAry'];
	$attendanceSource_activityIdAry = $_POST['attendanceSource_activityIdAry'];
	$attendanceStatus = $_POST['attendanceStatus'];
	$showMainGuardianInfo = $_POST['showMainGuardianInfo'];
		
	# Display Result	
	echo $libenroll_report->getAttendanceStatusReportResultTableHtml($targetStartDate, $targetStartDate, $studentSource, $studentIdAry, $studentSource_individualStudent, $studentSource_studentName, $studentSource_studentLogin, $studentSource_individualStudent_classId, $studentSource_individualStudent_classNumber, 
							$attendanceSource, $attendanceSource_clubIdAry, $attendanceSource_activityIdAry, $attendanceStatus, $showMainGuardianInfo);	
	
}
?>