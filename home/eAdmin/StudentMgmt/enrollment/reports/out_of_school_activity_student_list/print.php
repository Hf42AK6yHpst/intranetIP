<?php
## Modifying By:

/************************************
 * 	Date:		2014-08-11 (Bill)
 * 	Details:	Radio: Date range - Set SelectYear to null
 *	Date:		2013-04-19 (Rita) 
 * 	Details: 	Create this page
 ***********************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libuser = new libuser();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# Check User Access Right
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	# Check flag
	if(!$libenroll->enableCCHPWOutOfSchoolActivityStudentList()){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();

# Obtain Post Value
$rankTarget = $_POST['rankTarget'];
$rankTargetDetail = $_POST['rankTargetDetail'];
$studentID = $_POSR['studentID'];

$radioPeriod = $_POST['radioPeriod'];
$selectYear = $_POST['selectYear'];
$selectSemester = $_POST['selectSemester'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
$sel_category = $_POST['sel_category'];
$enrolEventID = $_POST['activityCategory'];
$sortBy = $_POST['sortBy'];
$sortByOrder = $_POST['sortByOrder'];



# prepare report data 
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester);
	$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
	
	// Not affected by School Year Selection
	$selectYear = "";
}


# Get Display Report
$display = $libenroll_ui->Get_Activity_Student_List_Report($enrolEventID,$reportType='ListReport',$SQL_startdate, $SQL_enddate, $rankTarget, $rankTargetDetail, $sel_category, $selectYear);


?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<!-- Display result //-->
<?=$display?>

<?
intranet_closedb();
// include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>



