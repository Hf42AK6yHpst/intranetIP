<?php 
/* using: 
 * **************************************************************************
 * Change Log:
 * Date: 2020-03-31 Tommy: modified permission checking
 * Date: 2020-01-30 Tommy: add permission checking
 * Date: 2017-01-11 Omas: Modified TAGS_OBJ lang
 * Date: 2017-04-11 Villa: set filter table to max-width 1600px
 * Date: 2017-02-14 Villa: add semester filter
 * Date: 2017-01-23 Villa[R99066 ]: Add printing Function, UI listing
 * Date: 2016-11-30 Villa[R99066 ]: Open the file
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
// include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libPopupInterface.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$lclass = new libclass();
$linterface = new interface_html();
// $linterface = new libPopupInterface('noHeader_template.html');

$role = array("Admin", "ClassTeacher");

if (!$libenroll->hasAccessRight($_SESSION['UserID'], $role)) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$classSelection = $lclass->getSelectClassID(" id=\"targetClassID\" name=\"targetClassID[]\" multiple size=\"10\"", $targetClassID,2);
$classSelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetClassID',0)");
$classSelection .= $linterface->spacer();  	 
$classSelection .= $linterface->MultiSelectionRemark(); 



$checkBox = "<input type='checkbox' class='targetType' id='targetType_group' name='targetType[]' value='group' onclick='OnClickGroup()' checked>"."<label for='targetType_group'>".$eEnrollment['club']."</label>";
$checkBox .= "<input type='checkbox' class='targetType' id='targetType_activity' name='targetType[]' value='activity' checked>"."<label for='targetType_activity'>".$eEnrollment['activity']."</label>";
// $radioBtn = $linterface->Get_Radio_Button("type_group",'targetType','group','1','',$eEnrollment['club']);
// $radioBtn .= $linterface->Get_Radio_Button("type_activity",'targetType','activity','0','',	$eEnrollment['activity']);
$SemesterFilter = $libenroll->Get_Term_Selection_CheckBox('Semester', $AcademicYearID, $Semester);
//$SemesterFilter = $libenroll->Get_Term_Selection_CheckBox('Semester', $AcademicYearID, $AcademicYearID);
$CurrentPage = "PageEnrolmentStatusReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($eEnrollmentMenu['enrolment_status'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:Export()','',array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

$x = "<form id='form1' name='form1' method='POST'>";
$x .= "<table class='form_table_v30' style='max-width:1600px;'>";
	$x .= "<tr>";
		$x .= "<td class='field_title'>";
			$x .= $i_EventRecordType.$linterface->RequiredSymbol();
		$x .= "</td>";
		$x .= "<td>";
// 			$x .= $radioBtn;
			$x .= $checkBox;
		$x .= "</td>";
	$x .= "</tr>";
	$x .= "<tr>";
		$x .= "<td class='field_title'>";
		$x .= $eEnrollment['Target'].$linterface->RequiredSymbol();
		$x .= "</td>";

		$x .= "<td class='tabletext'>";
		$x .= $classSelection;
		$x .= "</td>";
	$x .= "</tr>";
	$x .= "<tr id='SemesterFilter' style='display:none;'>";
		$x .= "<td class='field_title'>";
			$x .= $eEnrollment['ClubType'];
		$x .= "</td>";
		$x .= "<td>";
			$x .= $SemesterFilter;
		$x .= "</td>";
	$x .= "</tr>";
	$x .="<tr>";
		$x .="<td>";
			$x .= $linterface->MandatoryField();
		$x .="</td>";
	$x .="</tr>";
	$x .="<tr>";
		$x .="<td colspan='2'>";
			$x .="<div class='edit_bottom_v30'>";
				$x .="<p class='spacer'></p>";
				$x .= $linterface->GET_ACTION_BTN($eEnrollment['generate_or_update'], "button", "goSubmit()");
				$x .="<p class='spacer'></p>";
			$x .="</div>";
		$x .="</td>";
	$x .="</tr>";
	
$x .= "</table>";
$x .= "</form>";
$x .= $htmlAry['contentTool'];
$x .= "<br>";
$x .= "<div id='result'>"; 
$x .= "</div>";
echo $x;
?>
<script language="JavaScript" src="/templates/jquery/jquery-1.12.4.min.js"></script>
<script src="/templates/jquery/jquery.floatheader.min.js"></script>
<script src="/templates/jquery/jquery-migrate-1.4.1.min.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.PrintArea.js"></script>
<script>

var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

$('#SemesterFilter').show();
js_Select_All_With_OptGroup('targetClassID',0);
$('#termCheckBox_global').click();

function goSubmit(){

	if(formCheck()){
		$("#result").html('').append(
				$(loadingImage)
		);
		$('#PrintBtn').hide();
		$('#ExportBtn').hide();
		$.ajax({
			type: "POST",
			url: "generate.php",
			data: $('#form1').serialize(),
			success: function(msg){
				$('#PrintBtn').show();
				$('#ExportBtn').show();
				$('#result').html(msg);
			}
		});
	}
}
function Print(){
	var options = { mode : "popup", popClose : false};
// 	var options = { mode : mode, popClose : close, extraCss : extraCss, retainAttr : keepAttr, extraHead : headElements };
	$('#result').printArea(options);
};
function Export(){
	if(formCheck()){
		document.form1.action = "export.php";
		document.form1.target = "_blank";
		document.form1.method = "post";
		document.form1.submit();
	}
}
function formCheck(){
	
	var check = false;
// 	var check2 = false;
	//checking of Type
	if(targetType_group.checked){
// 		check = true;
		$('.termCheckBox').each(function(){
			if($(this).prop('checked')){
// 				check2 = true;
				check = true;
			}
		});
		if(!check){
			alert("<?=$Lang['eEnrolment']['jsWarning']['NoClubTypeIsSelected'] ?>");
			$('#targetType_group').focus();
			return false;
		}
	}
	if(targetType_activity.checked){
		check = true;
	}
	
	if(!check){
		$('#targetType_group').focus();
		return false;
	}
	//checking of selecting class
	if($('#targetClassID option:selected').val()){
		return true;
	}else{
		
		return false;
	}
}
function OnClickGroup(){
// 	$('#SemesterFilter').show();
	if($('#targetType_group').prop("checked")){
		$('#SemesterFilter').show();
	}else{
		$('#SemesterFilter').hide();
	}
}
</script>
