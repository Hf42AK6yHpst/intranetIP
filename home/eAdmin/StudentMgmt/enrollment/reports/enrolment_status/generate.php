<?php 
/* // using :anna
 *  Change Log:
 *  Date 2018-05-11 Anna Show student applied activity, ignore status
 *  Date 2018-03-21 Omas fix #K137120 
 *  Date 2017-04-20 Villa Fix php warning in  line 190
 *  Date 2017-04-11 Villa Add defind as array foreach foreach loop 
 *  Date 2017-02-14 Villa Redo the Whole file due to the change of R99066
 *  Date 2017-01-23 Villa UI Listing, support checkbox Filtering
 *  Date 2016-11-30 Villa[R99066] Open the file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$linterface = new interface_html();
$AcademicYear = Get_Current_Academic_Year_ID();
##### Class Filter START#####
$ClassIDArr = implode(",",$targetClassID);
$ClassIDArr = "(".$ClassIDArr.")";
$sql = "
		SELECT 
			UserID 
		FROM 
			YEAR_CLASS_USER 
		WHERE 
			YearClassID in $ClassIDArr;
		";
$student = $libenroll->returnResultSet($sql);
$student = Get_Array_By_Key($student, "UserID");
$studentArr = implode(",",$student);
$studentArr = "(".$studentArr.")";
##### Class Filter END #####
if($Semester){
	$Semester_sql = implode("','",$Semester);
	$Semester_cond = " AND (ieg.Semester  IN ('{$Semester_sql}') ";
}
if($Semester[0]=='0'){
	$Semester_cond .= "OR ieg.Semester IS NULL ";
}
$Semester_cond .= ") ";
##### Export Data #####
$sql = "
		SELECT 
			UserID, UserLogin, EnglishName, ChineseName, Gender, ClassName, ClassNumber
		FROM 
			INTRANET_USER
		WHERE 
			UserID in $studentArr
		Order By
			ClassName, length(ClassNumber), ClassNumber
		";
$Student_Data = $libenroll->returnResultSet($sql);

foreach ($targetType as $_targetType){
	if($_targetType=='group'){ //club
		$sql = "
				SELECT 
				 	ig.Title, ig.TitleChinese, 
				 	iegi.Semester, iegi.EnrolGroupID,
					iu.UserID, iu.UserLogin, iu.EnglishName, iu.ChineseName, iu.ClassName, iu.ClassNumber, 
					ieg.DateModified, ieg.RecordStatus, ieg.Choice, ieg.GroupID
				FROM 
					INTRANET_ENROL_GROUPSTUDENT ieg
				INNER JOIN
					INTRANET_GROUP ig ON ieg.GroupID = ig.GroupID
				INNER JOIN
					INTRANET_ENROL_GROUPINFO iegi ON ig.GroupID = iegi.GroupID and ieg.EnrolGroupID = iegi.EnrolGroupID
				INNER JOIN
					INTRANET_USER iu ON iu.UserID = ieg.StudentID
				WHERE 
					ieg.StudentID in $studentArr
				AND
					ig.AcademicYearID = {$AcademicYear}
				ORDER BY
					iu.ClassName, iu.ClassNumber, ieg.Choice
			";
		$header_sql = "
						SELECT
							Distinct
							ig.GroupID, ig.TitleChinese, ig.Title, 
							ieg.Semester, ieg.EnrolGroupID
						FROM
							INTRANET_GROUP ig
						INNER JOIN
							INTRANET_ENROL_GROUPINFO ieg ON ig.GroupID = ieg.GroupID
						WHERE 
							ig.AcademicYearID = {$AcademicYear}
						$Semester_cond
						ORDER BY
							ieg.Semester asc
			
						";
		$data = $libenroll->returnResultSet($sql);
		$temp = $libenroll->returnResultSet($header_sql);
		$header_group = BuildMultiKeyAssoc($temp, 'EnrolGroupID'); //header for group => all group in this year 
		foreach ((array)$data as $_data){
// 			$GropuID_TermID = $_data['GroupID'].'_'.$_data['Semester'];
			$groupData[$_data['UserID']][$_data['EnrolGroupID']]['Priority'] = $_data['Choice'];
			$groupData[$_data['UserID']][$_data['EnrolGroupID']]['RecordStatus'] = $_data['RecordStatus'];
		
		}
		foreach((array)$Student_Data as $_Student_Data){
			$result[$_Student_Data['UserID']] ['StudentInfo'] = $_Student_Data;
			foreach ((array)$header_group as $_header_group){
// 				$GropuID_TermID = $_header_group['GroupID'].'_'.$_header_group['Semester'];
				$result[$_Student_Data['UserID']] ['Group'] [$_header_group['EnrolGroupID']] ['Priority']= $groupData[$_Student_Data['UserID']] [$_header_group['EnrolGroupID']] ['Priority'];
				$result[$_Student_Data['UserID']] ['Group'] [$_header_group['EnrolGroupID']] ['RecordStatus']= $groupData[$_Student_Data['UserID']] [$_header_group['EnrolGroupID']] ['RecordStatus'];
			}
		}
	}else{ //activity
		$sql = "
			SELECT
				ie.EventTitle,
				iu.UserID, iu.UserLogin, iu.EnglishName, iu.ChineseName, iu.ClassName, iu.ClassNumber,
				iee.DateModified, iee.RecordStatus, iee.EnrolEventID
			FROM
				INTRANET_ENROL_EVENTSTUDENT iee
			INNER JOIN
				INTRANET_ENROL_EVENTINFO ie ON iee.EnrolEventID = ie.EnrolEventID AND ie.AcademicYearID = {$AcademicYear}
			INNER JOIN
				INTRANET_USER iu ON iu.UserID = iee.StudentID
			WHERE
				iee.StudentID in $studentArr
			ORDER BY
				iu.ClassName, iu.ClassNumber
		";
			
		$header_sql = "
		SELECT
			EnrolEventID, EventTitle
		FROM
			INTRANET_ENROL_EVENTINFO iee
		WHERE
			iee.AcademicYearID = {$AcademicYear}
			
		";
		
		$data = $libenroll->returnResultSet($sql);
		$temp = $libenroll->returnResultSet($header_sql);
		$header_activity = BuildMultiKeyAssoc($temp, 'EnrolEventID');
		foreach ((array)$data as $_data){
			$ActivityID_TermID = $_data['EnrolEventID'];
			$activityData[$_data['UserID']][$ActivityID_TermID]['RecordStatus'] = $_data['RecordStatus'];
		
		}
		foreach((array)$Student_Data as $_Student_Data){
			$result[$_Student_Data['UserID']] ['StudentInfo'] = $_Student_Data;
			foreach ((array)$header_activity as $_header_activity){
				$ActivityID_TermID = $_header_activity['EnrolEventID'];
				$result[$_Student_Data['UserID']] ['Activity'] [$ActivityID_TermID] ['RecordStatus'] = $activityData[$_Student_Data['UserID']] [$ActivityID_TermID] ['RecordStatus'];
			}
		}
	}
}
	
$display = '';
##### Print Data #####
$display .= "<table width='6000px' class=\"common_table_list_v30 view_table_list_v30 resultTable\" id=\"resultTable\">";
	$display .= "<thead><tr>";
		$display .= "<th width='5%'>".$i_UserClassName."</th>";
		$display .= "<th width='5%'>".$i_UserClassNumber."</th>";
		if(!$sys_custom['eEnrolment']['HiddenLoginName'] ){
			$display .= "<th width='5%'>".$Lang['AccountMgmt']['StudentImportFields']['0']."</th>";
		}
		$display .= "<th width='5%'>".$Lang['General']['EnglishName2']."</th>";
		$display .= "<th width='5%'>".$Lang['General']['ChineseName2']."</th>";
		$display .= "<th width='5%'>".$Lang['StudentRegistry']['Gender']."</th>";
		
// 		if($header_group||$header_activity){
			$display .= "<th width='10%'>".$Lang['eEnrolment']['ApproveClubActivity']."</th>"; //Approved Group
// 		}
		if($header_group){

			foreach ($header_group as $_header_group){
				$display .=  "<th width='10%'>".Get_Lang_Selection($_header_group['TitleChinese'], $_header_group['Title'])."</th>"; //Group Title
			}
		}
		if($header_activity){
			foreach ($header_activity as $_header_activity){
				$display .=  "<th width='10%'>".$_header_activity['EventTitle']."</th>"; //Group Title
			}
		}
	$display .= "</tr></thead>";
	$display .= "<tbody>";
	foreach ((array)$result as $student){
		$display .= "<tr>";
			$display .= "<td>";
				$display .= Get_String_Display($student['StudentInfo']['ClassName']);
			$display .= "</td>";
			$display .= "<td>";
				$display .= Get_String_Display($student['StudentInfo']['ClassNumber']);
			$display .= "</td>";
			if(!$sys_custom['eEnrolment']['HiddenLoginName'] ){
				$display .= "<td>";
					$display .= Get_String_Display($student['StudentInfo']['UserLogin']);
				$display .= "</td>";
			}
			$display .= "<td>";
				$display .= Get_String_Display($student['StudentInfo']['EnglishName']);
			$display .= "</td>";
			$display .= "<td>";
				$display .= Get_String_Display($student['StudentInfo']['ChineseName']);
			$display .= "</td>";
			$display .= "<td>";
				$display .= Get_String_Display($student['StudentInfo']['Gender']);
			$display .= "</td>";
			if($header_group||$header_activity){
				$display .= "<td>"; //Approved Group Result
				foreach ((array)$student['Group'] as $_GroupEnrolID =>$_GroupInfo){
					if($_GroupInfo['RecordStatus']=='2'){
						// 							$_GroupID = explode('_',$_GroupIDTerm);
						$display .= Get_Lang_Selection($header_group[$_GroupEnrolID] ['TitleChinese'], $header_group[$_GroupEnrolID] ['Title']);
						$display .= "</br>";
					}
				}
				foreach ((array)$student['Activity'] as $_ActivityEnrolID =>$_ActivityInfo){
					if($_ActivityInfo['RecordStatus']=='2'){
						$display .= $header_activity[$_ActivityEnrolID]['EventTitle'];
						$display .= "</br>";
					}
				}
				
				$display .= "</td>";
			}else{
				$display .= "<td>";
					$display .= "-";
				$display .= "</td>";
			}
			if($header_group){
				foreach ($student['Group'] as $_GroupInfo){ //Group Priority
					$display .= "<td>";
						$display .= $_GroupInfo['Priority'];
					$display .= "</td>";
				}
			}
			if($header_activity){
				foreach ($student['Activity'] as $_ActivityIDTerm =>$_ActivityInfo){
					$display .= "<td>";
					if($_ActivityInfo['RecordStatus'] !='' ){
						$display .= "<img src='/images/2009a/ereportcard/icon_present_bw.gif' width='10' class=''>";
					}else{
						$display .= "";
					}
					$display .= "</td>";
				}
				
			}
		$display .= "</tr>";
	}
	$display .= "</tbody>";
$display .="</table>";
?>
<script>
$( document ).ready(function() {
	$('.resultTable').floatHeader();
});
</script>
<?php
	echo $display;
	intranet_closedb();
?>