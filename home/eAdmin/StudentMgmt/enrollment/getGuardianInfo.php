<?php
// Editing by 
/*	
 * 	2017-06-20 Villa[E118935] Hide exit checking
 *  2016-11-30 Villa[R99066] Change the table class and style
 *  2015-11-27 Added by Kenneth
 */
### Show error message in red if session timeout instead of redirecting to the login page ###
session_start();
if (!isset($_SESSION['UserID'])) {
	$error = "<table border=\"0\" width=\"300\" cellpadding=\"3\" cellspacing=\"0\">";
	$error .= "<tr class=\"tablebluebottom\"><td class=\"tabletoplink\" colspan=\"8\" align=\"right\"><input class=\"formsubbutton\" type=\"button\" value=\"x\" onclick=\"hideMenu('ToolMenu2')\"></td></tr>";
	$error .= "<tr class=\"tablebluetop\">";
	$error .= "<td class=\"tabletoplink\"><span style=\"color:red;\">";
	$error .= "Error encountered, please login again<br /> 錯誤發生，請重新登入</span></td>";
	$error .= "</tr></table>";
	//$response = iconv("Big5","UTF-8",$error);
	$response = $error;
	echo $response;
	exit;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
$lidb = new libdb();

intranet_auth();
intranet_opendb();
// if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
// 	header ("Location: /");
// 	intranet_closedb();
// 	exit();
// }

$lc = new libcardstudentattend2();

$linterface = new interface_html();

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT( EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$sql = "SELECT 
				$main_content_field,
				Relation,
				Phone, 
				EmPhone,
				IsMain
		FROM
				$eclass_db.GUARDIAN_STUDENT
		WHERE 
				UserID = $targetUserID
		ORDER BY 
				IsMain DESC, Relation ASC
		";

$temp = $lidb->returnArray($sql,4);

$title = "<th  width=\"1\">#</th>";
$title .= "<th  width=\"35%\">$i_UserName</th>";
$title .= "<th  width=\"25%\">$ec_iPortfolio[relation]</th>";
$title .= "<th  width=\"20%\">$i_StudentGuardian_Phone</th>";
$title .= "<th  width=\"20%\">$i_StudentGuardian_EMPhone</th></tr>";


if (sizeof($temp)==0)
	$layer_content .= "<tr><td class=\"tablebluerow2 tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
else
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($name, $relation, $phone, $em_phone, $IsMain) = $temp[$i];
		$no = $i+1;
		if($IsMain == 1) {
			$RemarkDetailsArr[$i] .= "<td >$no</td>";
			$RemarkDetailsArr[$i] .= "<td>".$linterface->RequiredSymbol().$name."</td>";
			$RemarkDetailsArr[$i] .= "<td>$ec_guardian[$relation]</td>";
			$RemarkDetailsArr[$i] .= "<td>$phone</td>";
			$RemarkDetailsArr[$i] .= "<td>$em_phone</td>";
			
		} else {
			$RemarkDetailsArr[$i] .= "<td class=\"tablebluerow2 tabletext\">$no</td>";
			$RemarkDetailsArr[$i] .= "<td>$name</td>";
			$RemarkDetailsArr[$i] .= "<td>$ec_guardian[$relation]</td>";
			$RemarkDetailsArr[$i] .= "<td>$phone</td>";
			$RemarkDetailsArr[$i] .= "<td>$em_phone</td>";
		}
	}
}



// $RemarksLayer .= '<table width="90%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
$RemarksLayer .='<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";	
	$RemarksLayer .= '<tbody>'."\r\n";
		$RemarksLayer .= '<tr>'."\r\n";
			$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
				$RemarksLayer .= '<a href="javascript:closeLayer(\'ToolMenu2\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
			$RemarksLayer .= '</td>'."\r\n";
		$RemarksLayer .= '</tr>'."\r\n";
		$RemarksLayer .= '<tr>'."\r\n";
			$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
				$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
					$RemarksLayer .= '<thead>'."\n";
						$RemarksLayer .= '<tr>'."\n";
							$RemarksLayer .= $title."\n";
						$RemarksLayer .= '</tr>'."\n";
					$RemarksLayer .= '</thead>'."\n";
					$RemarksLayer .= '<tbody>'."\n";
						if(!empty($RemarkDetailsArr)){
							foreach ($RemarkDetailsArr as $RemarkDetail){
								$RemarksLayer .= '<tr>'."\n";
								$RemarksLayer .= $RemarkDetail."\n";
								$RemarksLayer .= '</tr>'."\n";
							}
						}else{
							$RemarksLayer .= '<tr>'."\n";
							$RemarksLayer .= '<td colspan=5>'.$i_no_record_searched_msg."</td>";
							$RemarksLayer .= '</tr>'."\n";
						}
					$RemarksLayer .= '</tbody>'."\n";
					$RemarksLayer .= '</table>'."\r\n";
			$RemarksLayer .= '</td>'."\r\n";
		$RemarksLayer .= '</tr>'."\r\n";
	$RemarksLayer .= '</tbody>'."\r\n";
$RemarksLayer .= '</table>'."\r\n";
$RemarksLayer .= $linterface->RequiredSymbol().$eEnrollment['GuardianInformationRemarks'];
/*$response = iconv("Big5","UTF-8",$layer_content);

echo $response;*/

echo $RemarksLayer;
intranet_closedb();
?>