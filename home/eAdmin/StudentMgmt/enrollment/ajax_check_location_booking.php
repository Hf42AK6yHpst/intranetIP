<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_opendb();

$linterface = new interface_html();
$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$libenroll = new libclubsenrol($AcademicYearID);

$today = date("Y-m-d");

$sql = "SELECT 
			ier.BookingID,
			CONCAT(ier.Date,',',ier.StartTime,',',ier.EndTime) as Datetime 
		FROM INTRANET_EBOOKING_RECORD as ier 
		INNER JOIN INTRANET_EBOOKING_ENROL_EVENT_RELATION as ieeer ON ieeer.BookingID=ier.BookingID 
		INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS as iebd ON iebd.BookingID=ier.BookingID 
		WHERE ieeer.EnrolEventID='$EnrolEventID' AND iebd.FacilityType='".LIBEBOOKING_FACILITY_TYPE_ROOM."' 
		ORDER BY ier.Date,ier.StartTime";
$old_booking_records = $libenroll->returnResultSet($sql);
$old_booking_datetimes = Get_Array_By_Key($old_booking_records,'Datetime');
$old_booking_count = count($old_booking_datetimes);

$new_booking_datetimes = $TargetDatetimes;
$new_booking_count = count($new_booking_datetimes);

$removed_bookingIds = array();
$removed_dates = array();
$added_dates = array();
//$changed_dates = array();

for($i=0;$i<$new_booking_count;$i++){
	$parts = explode(",",$new_booking_datetimes[$i]);
	if($parts[0] < $today) continue; // skip past dates
	if(!in_array($new_booking_datetimes[$i],$old_booking_datetimes)){
		$added_dates[] = $new_booking_datetimes[$i];
	}
}

for($i=0;$i<$old_booking_count;$i++){
	$parts = explode(",",$old_booking_datetimes[$i]);
	if($parts[0] < $today) continue; // skip past dates
	if(!in_array($old_booking_datetimes[$i],$new_booking_datetimes)){
		$removed_dates[] = $old_booking_datetimes[$i];
		$removed_booking_ids[] = $old_booking_records[$i]['BookingID'];
	}
}

if(count($removed_dates)>0){
	$libenroll->Delete_Activity_Location_Booking_Record($removed_booking_ids);
}

if(count($added_dates)>0){
	echo implode("###",$added_dates);
}

intranet_closedb();
?>