<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
				
$AcademicYearID = IntegerSafe(standardizeFormPostValue($_POST['academicYearId']));
$Semester = IntegerSafe(standardizeFormPostValue($_POST['yearTermId']));


$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol($AcademicYearID);		
$linterface = new interface_html();

$GroupInfoArr = $libenroll->Get_Club_Student_Enrollment_Info('', $AcademicYearID, $ShowUserRegOnly=1, array($Semester), /*$keyword*/intranet_htmlspecialchars(stripslashes($keyword)),$WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=false,$GroupCategory='');

$numOfClubStudent = count($GroupInfoArr);

$StudentIDArray = array();
for($i=0;$i<$numOfClubStudent;$i++){
	$StudentIDArray[] = $GroupInfoArr[$i]['StudentID'];
}
$StudentIDArray = array_unique($StudentIDArray);

####  eNotice Table	
$TableNotice .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";

// 	$TableNotice .= "<tr class=\"tablerow1\">\n";
// 		$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">".$linterface->RequiredSymbol().$Lang['eEnrolment']['SysRport']['NoticeNumber']." </td>\n";
// 			$TableNotice .= "<td valign=\"top\">
// 								<textarea name=\"TextNumber\" id=\"TextNumber\" rows=\"2\" wrap=\"virtual\" class=\"textboxtext\"></textarea>
// 								<span id='div_TextNumber_err_msg'></span>
// 							</td>\n";
// 		$TableNotice .= "</tr>\n";
// 	$TableNotice .= "</tr>\n";

	$TableNotice .= "<tr class=\"tablerow1\">\n";
		$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">".$linterface->RequiredSymbol().$Lang['eEnrolment']['SysRport']['NoticeTitle']." </td>\n";
		$TableNotice .= "<td valign=\"top\"><textarea name=\"TextTitle\" id=\"TextTitle\" rows=\"2\" wrap=\"virtual\" class=\"textboxtext\"></textarea><span id='div_TextTitle_err_msg'></span></td>\n";
		$TableNotice .= "</tr>\n";
	$TableNotice .= "</tr>\n";
	
	$TableNotice .= "<tr class=\"tablerow1\">\n";
		$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">".$linterface->RequiredSymbol().$Lang['eEnrolment']['SysRport']['NoticeContent']." </td>\n";
		$TableNotice .= "<td valign=\"top\"><textarea name=\"TextContent\" id=\"TextContent\" rows=\"5\" wrap=\"virtual\" class=\"textboxtext\"></textarea><span id='div_TextContent_err_msg'></span></td>\n";
		$TableNotice .= "</tr>\n";
	$TableNotice .= "</tr>\n";
$TableNotice .= "</table>\n";

$htmlAry['reportResultDiv'] = '<div id="reportResultDiv"></div>';
$noticeFlag = 'YES';
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "checkform(this.form);", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>

<script type="text/javascript">
function checkform(obj){
	var error_no = 0;
	var focus_field = "";

//	if(!check_text_30(obj.TextNumber, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['SysRport']['NoticeNumber']; ?>.", "div_TextNumber_err_msg"))
// 	{
// 		error_no++;
// 		if(focus_field=="")	focus_field = "TextNumber";
// 	}
	if(!check_text_30(obj.TextTitle, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['SysRport']['NoticeTitle']; ?>.", "div_TextTitle_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "TextTitle";
	}
	if(!check_text_30(obj.TextContent, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['SysRport']['NoticeContent']; ?>.", "div_TextContent_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "TextContent";
	}
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		sendeNotice();
	}

}

function sendeNotice(){
	$.ajax({
		url:      	"send_notice_check_member_status_update.php",
		type:     	"POST",
		data:     	$("#thickboxForm").serialize(),
		success:  	function(xml) {
						$('div#reportResultDiv').html(xml);
						Scroll_To_Top();
						UnBlock_Document();
		}
	});
}

</script>

	<form id="thickboxForm" name="thickboxForm" method="POST" action="send_notice_check_member_status_update.php"> 
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			
			<br />
			<?=$TableNotice?>
			<?=$htmlAry['reportResultDiv']?>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<input type="hidden" name="StudentID" value="<?=implode(",",$StudentIDArray)?>"/>
	</form>
