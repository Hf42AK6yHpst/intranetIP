<?php
# using: 
#############################################
#
#   Date    2020-01-06 Tommy
#           file created
#
#############################################
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment']) {
    include_once($PATH_WRT_ROOT."includes/libgroup.php");
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
    include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
    
    $libenroll = new libclubsenrol($AcademicYearID);
    $libenroll_ui = new libclubsenrol_ui();
    
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        if (!isset($_GET)) {
            header("Location: index.php");
            exit();
        }
        
        $ExportArr = array();
        $lexport = new libexporttext();
        
        /*
         *	$type == 2 => Club Attendance
         *	$type == 3 => Activity Attendance
         */
        
        
        if ($type == 2){	// club or activity attendance
            
            $AcademicYearID = $_GET['AcademicYearID'];
            $semester = $_GET['Semester'];
            
            # Club
            if ($type == 2)
            {
                $sel_Category=$_GET['Category'];
                $keyword = $_GET['Keyword'];
                $ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info("", $AcademicYearID, $ShowUserRegOnly=0 ,$semester,$keyword,0,1,1,true,$sel_Category);
                $recordIdArr = array_unique(array_keys($ClubInfoArr));
                $numOfRecordIdArr = count($recordIdArr);
                $filename = "club_attendance_performance.csv";          
            }
            
            $iCounter = 0;
            $jCounter = 0;
            
            $ExportArr[$iCounter][$jCounter++]  = $i_ClassName;
            $ExportArr[$iCounter][$jCounter++]  = $i_ClassNumber;
            
            if (!$forSample) {
                $ExportArr[$iCounter][$jCounter++]  = $i_UserStudentName;
            }
            
            $ExportArr[$iCounter][$jCounter++]  = $eEnrollment['club_name'];
            
            if (!$forSample) {
                $ExportArr[$iCounter][$jCounter++]  = $eEnrollment['studnet_attendence']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
                $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['AttendanceHour']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
                $ExportArr[$iCounter][$jCounter++]  = $eEnrollment['performance']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
                $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['heungto_PerformanceMark']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
                
                if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
                    $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['StudentExtraInfoArr']['EmergencyPhoneNumber'];
                    $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['StudentExtraInfoArr']['FatherName'];
                    $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['StudentExtraInfoArr']['FatherPhoneNumber'];
                    $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['StudentExtraInfoArr']['MotherName'];
                    $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['StudentExtraInfoArr']['MotherPhoneNumber'];
                }
            }
            
            $iCounter++;
            
            for($i=0;$i<$numOfRecordIdArr;$i++){
                if ($type == 2) {
                    # Club
                    $recordId = $recordIdArr[$i];
                    $dateIdFieldName = 'GroupDateID';
                    $groupId = $libenroll->GET_GROUPID($recordId);
                    $lgroup = new libgroup($groupId);
                    $title = str_replace(' ', '_', $lgroup->Title);
                    
                    $meetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($recordId);
                    $studentInfoAry = $libenroll->Get_Club_Member_Info($recordId, array(USERTYPE_STUDENT));
                    $studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
                    $memberAttendanceInfoArr = $libenroll->Get_Student_Club_Attendance_Info($studentIdAry, array($recordId));
                    $overallAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($recordId, "", $AcademicYearID);
                    
                    $recordTypeCode = $enrolConfigAry['Club'];
                }
                
                if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
                    include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");
                    $libenroll_cust = new libclubsenrol_cust();
                    $extraInfoAry = $libenroll_cust->Get_Student_Extra_Info($recordTypeCode, $studentIdAry);
                    $extraInfoAssoAry = BuildMultiKeyAssoc($extraInfoAry, 'StudentID');
                    unset($extraInfoAry);
                }
                
                $numOfMeeting = count($meetingDateInfoAry);
                $numOfStudent = count($studentIdAry);
                
                ### Header
                $jCounter = 0;
                
                if($numOfStudent==0){
                }
                else{
                    for ($j=0; $j<$numOfStudent; $j++) {
                        $_studentId = $studentInfoAry[$j]['UserID'];
                        $_studentName = Get_Lang_Selection($studentInfoAry[$j]['ChineseName'], $studentInfoAry[$j]['EnglishName']);
                        $_className = $studentInfoAry[$j]['ClassName'];
                        $_classNumber = $studentInfoAry[$j]['ClassNumber'];
                        $_attendancePercentage = $memberAttendanceInfoArr[$_studentId][$recordId]['AttendancePercentageRounded'];
                        $_attendanceHours = $memberAttendanceInfoArr[$_studentId][$recordId]['Hours'];
                        
                        $_performance = $studentInfoAry[$j]['Performance'];
                        
                        switch ($_performance){
                            case "A":
                                $_mark = 95;
                                break;
                            case "B":
                                $_mark = 85;
                                break;
                            case "C":
                                $_mark = 75;
                                break;
                            case "D":
                                $_mark = 65;
                                break;
                            case "E":
                                $_mark = 55;
                                break;
                            case "F":
                                $_mark = 45;
                                break;
                            case "U":
                                $_mark = 0;
                                break;
                        }

                        $jCounter = 0;
                        $_enrolGroupID = $studentInfoAry[$j]['EnrolGroupID'];
                        
                        $ExportArr[$iCounter][$jCounter++] = $_className;
                        $ExportArr[$iCounter][$jCounter++] = $_classNumber;
                        $ExportArr[$iCounter][$jCounter++] = $_studentName;
                        
                        if (!$forSample) {
                            
                            $ExportArr[$iCounter][$jCounter++]  = $title ;
                            
                            // attendance percentage
                            $_display = ($_attendancePercentage=='')? 0 : $_attendancePercentage;
                            $ExportArr[$iCounter][$jCounter++] = $_display.'%';
                            
                            // total attendance hour
                            $ExportArr[$iCounter][$jCounter++] = ($_attendanceHours=='')? 0 : $_attendanceHours;
                            
                            // performance
                            $ExportArr[$iCounter][$jCounter++] = ($_performance=='')? 0 : $_performance;
                            
                            // mark
                            $ExportArr[$iCounter][$jCounter++] = ($_mark=='')? 0 : $_mark;
                            
                        }
                        
                        $iCounter++;
                    }
                    
                    $jCounter = 0;
                }
            }
        }
        
        # If no Club / Activity records
        if($numOfRecordIdArr==0){
            ### Header
            $jCounter = 0;
            if ($type == 2) {
                $ExportArr[$iCounter][$jCounter++]  = $eEnrollment['club_name'] ;
            }else{
                $ExportArr[$iCounter][$jCounter++]  = $eEnrollment['add_activity']['act_name'] ;
            }
            $ExportArr[$iCounter][$jCounter++]  = $i_ClassName;
            $ExportArr[$iCounter][$jCounter++]  = $i_ClassNumber;
            
            $ExportArr[$iCounter][$jCounter++]  = $i_UserStudentName;
            
            $ExportArr[$iCounter][$jCounter++]  = $eEnrollment['studnet_attendence']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
            $ExportArr[$iCounter][$jCounter++]  = $Lang['eEnrolment']['AttendanceHour']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
            
            $iCounter++;
        }

        $export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
        
        intranet_closedb();
        
        // Output the file to user browser
        $lexport->EXPORT_FILE($filename, $export_content);
        
    }
    else
    {
        echo "You have no priviledge to access this page.";
    }
}
else
{
    echo "You have no priviledge to access this page.";
}

?>
