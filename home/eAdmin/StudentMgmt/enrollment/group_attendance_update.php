<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$GroupID = ($_POST['GroupID'] == "")? $_GET['GroupID'] : $_POST['GroupID'];
$DataArr['GroupID'] = $GroupID;
$DataArr['StaffRole'] = $_POST['StaffRole'];

//$libenroll->DEL_GROUP_ATTENDENCE($_POST['GroupID']);
## delete updated dates only
$disabledArr = unserialize(rawurldecode($disabledArr));


//get all group dates
$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
$sql = "SELECT GroupDateID FROM INTRANET_ENROL_GROUP_DATE WHERE EnrolGroupID = '$EnrolGroupID'";
$allDateArr = $libenroll->returnVector($sql);


//delete those date not DISABLED only
$notDeleteDateArr = array();
$deleteDateArr = array();
foreach ($disabledArr as $key => $value)
{
	if ($value == "DISABLED")
	{
		$notDeleteDateArr[] = $key;	
	}	
}
$deleteDateArr = array_diff($allDateArr, $notDeleteDateArr);
$deleteDateList = implode(",", $deleteDateArr);

if (isset($_POST["ActivityID"]) && $_POST["ActivityID"] > 0) {
	// delete old records
	$sql = "DELETE FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE GroupID = '".$DataArr['GroupID']."' AND GroupDateID IN ('" . $_POST["ActivityID"] . "')";
	$libenroll->db_db_query($sql);
} else {
	// delete old records
	$sql = "DELETE FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE GroupID = '".$DataArr['GroupID']."' AND GroupDateID IN ( $deleteDateList )";
	$libenroll->db_db_query($sql);
}


if (sizeof($_POST) > 0) {
	while (list($key, $value) = each($_POST)) {
	    if (is_numeric($key))
	    {
		    $DataArr['GroupDateID'] = $key;
		    
		    # add new records
		    for ($i = 0; $i < sizeof($value); $i++) {
			    $DataArr['StudentID'] = $value[$i];
			    $libenroll->ADD_STU_GROUP_ATTENDENCE($DataArr);
		    }
	    }
	}
}

intranet_closedb();
header("Location: club_attendance_mgt.php?GroupID=".$GroupID."&msg=2");
?>