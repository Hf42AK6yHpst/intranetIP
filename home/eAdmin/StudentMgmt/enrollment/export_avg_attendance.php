<?php
# using:
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

//debug_pr($_POST['exportAry']);

$avgSourceAry = $_POST['exportAry'];

$headerAry = array();
$headerAry[] = "Name";
$headerAry[] = "Semester";
$headerAry[] = "Member";
$headerAry[] = "Time(s) of Activity";
$headerAry[] = "Activity Hour(s)";
$headerAry[] = "Average Attendance";
$numRow = count($avgSourceAry);
$dataAry = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry[$i][$_col++] = $avgSourceAry[$i][0];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][1];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][2];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][3];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][4];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][5];
//debug_pr($dataAry);
}
$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "avg_attendance.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>