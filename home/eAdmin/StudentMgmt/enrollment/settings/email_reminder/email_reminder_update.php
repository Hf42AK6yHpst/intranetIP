<?php
// Editing by 
/*
 * 2014-04-14 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcrontab.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$sys_custom['eEnrolment']['TWGHCYMA'] || !$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$GeneralSetting = new libgeneralsettings();

$Settings['UseEmailReminder'] = $UseEmailReminder;
$Settings['EmailReminderSubject'] = stripslashes($EmailReminderSubject);

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($EmailReminderContent==strip_tags($EmailReminderContent))
	{
		$EmailReminderContent = nl2br($EmailReminderContent);
	}
}
$Settings['EmailReminderContent'] = stripslashes($EmailReminderContent);

$Settings['EmailReminderDaysBefore'] = $EmailReminderDaysBefore;
$Settings['EmailReminderSendTime'] = sprintf("%02d:%02d:%02d",$EmailReminderSendTime_hour,$EmailReminderSendTime_min,$EmailReminderSendTime_sec);

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting($libenroll->ModuleTitle,$Settings)) {
	$GeneralSetting->Commit_Trans();
	
	// update cron job
	$lcrontab = new libcrontab();
	$site = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	$script = $site."/schedule_task/enrolment_send_email_reminder.php";
	
	if($UseEmailReminder == 1){
		$lcrontab->setJob($EmailReminderSendTime_min, $EmailReminderSendTime_hour, '*', '*', '*', $script);
	}else{
		$lcrontab->removeJob($script);
	}
	
	$Msg = $Lang['General']['ReturnMessage']['SettingSaveSuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['General']['ReturnMessage']['SettingSaveFail'];
}

intranet_closedb();
header("Location: email_reminder.php?Msg=".urlencode($Msg));
?>