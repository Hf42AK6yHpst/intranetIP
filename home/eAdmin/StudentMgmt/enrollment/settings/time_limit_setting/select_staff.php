<?php 
/*
 *  2018-08-01 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();


$libenroll = new libclubsenrol();
$linterface = new libclubsenrol_ui();

####################################################
########## Access Right Checking [Start] ###########
####################################################
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$canAccess =  $isEnrolAdmin ? true : false;

if (!$canAccess) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
####################################################
########### Access Right Checking [End] ############
####################################################


$staffType = 1;     // default teaching staff
$staffSelection = $linterface->getStaffSelection($staffType);

### Instruction
$instruction = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eEnrolment']['settings']['timeLimitSetting']['selectStaffInstruction']);

$cancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'");

####################################################
############## Layout Display [Start] ##############
####################################################
$CurrentPage = "PageTimeLimitSetting";
$CurrentPageArr['eEnrolment'] = 1;

### top tab
$TAGS_OBJ[] = array($Lang['eEnrolment']['settings']['timeLimitSetting']['title'], "", 1);

### left-hand menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


####################################################
############### Layout Display [End] ###############
####################################################
?>
<script language="javascript">
$(document).ready(function(){
	$('#StaffType').change(function(){
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax.php',
			data : {
				'action': 'getStaffList',
				'StaffType': $('#StaffType').val()
			},		  
			success: update_staff_list,
			error: show_ajax_error
		});
	});
});

function changeStaff(obj)
{
	if ($(obj).val() != '') {
		$('#form1').attr('action', 'time_limit_setting.php?StaffID=' + $('#StaffID').val());
		$('#form1').submit();
	}
}

function update_staff_list(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#staffSelectionTd').html(ajaxReturn.html);
	}
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

</script>
<br />
<form id="form1" name="form1" action="time_limit_setting.php" method="post">
	
	<?php echo $instruction;?>
	<br style="clear:both;" />
	
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?php echo $i_identity; ?></td>
			<td>
            	<select name="StaffType" id="StaffType">
                    <option value="1" selected><?php echo $Lang['Identity']['TeachingStaff'];?></option>
                    <option value="0"><?php echo $Lang['Identity']['NonTeachingStaff'];?></option>
            	</select>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'];?></td>
			<td id="staffSelectionTd">
				<?php echo $staffSelection;?>
			</td>
		</tr>
		
	</table>
	<br />
	
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<?php echo $cancelBtn;?>
	</div>
	
</form>
<?php
$linterface->LAYOUT_STOP();
?>