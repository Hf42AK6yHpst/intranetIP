<?php
/*
 *  Using:
 * 
 *  Change Log:
 * 
 *  2018-08-01 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/json.php");


intranet_auth();
intranet_opendb();


$recordId = IntegerSafe($_POST['recordId']);    // StaffID
$recordType = $_POST['recordType'];
$dateInfoAry = $_POST['dateInfoAry'];

$action = Update;

$LibUser = new libuser($_SESSION['UserID']);
$libenroll = new libclubsenrol();
$liblog = new liblog();
$json =  new JSON_obj();


####################################################
########## Access Right Checking [Start] ###########
####################################################
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$canAccess = $isEnrolAdmin ? true : false;
	
if (!$canAccess) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$originalTimeLimitSettingAry = $libenroll->getTimeLimitSetting($recordId);
$dateIdKey = 'RecordID';
$originalTimeLimitRecordIdAry = Get_Array_By_Key($originalTimeLimitSettingAry, $dateIdKey);

####################################################
########### Access Right Checking [End] ############
####################################################


####################################################
######## Update Time Limit Setting Info [Start] #######
####################################################

$result = array();
$tableName='INTRANET_ENROL_TIME_LIMIT_SETTING';

### Inactivate deleted date
$newTimeLimitRecordIdAry = array();

foreach ((array)$dateInfoAry as $_rowNumber => $_dateInfoAry) {
	$_dateRecordId = $_dateInfoAry['dateRecordId'];
	if ($_dateRecordId != '') {
		$newTimeLimitRecordIdAry[] = $_dateRecordId;
	}
}
$deleteDateRecordIdAry = array_values(array_diff((array)$originalTimeLimitRecordIdAry, (array)$newTimeLimitRecordIdAry));
$numOfDeletedDate = count($deleteDateRecordIdAry);

// debug_pr($dateInfoAry);
// exit;
$libenroll->Start_Trans();

### add / update record 
foreach ((array)$dateInfoAry as $_rowNumber => $_dateInfoAry) {
	$_dateRecordId = $_dateInfoAry['dateRecordId'];
	$_dateText = $_dateInfoAry['dateText'];
	$_hourStart = str_pad($_dateInfoAry['hourStart'], 2, 0, STR_PAD_LEFT);
	$_minStart = str_pad($_dateInfoAry['minStart'], 2, 0, STR_PAD_LEFT);
	$_hourEnd = str_pad($_dateInfoAry['hourEnd'], 2, 0, STR_PAD_LEFT);
	$_minEnd = str_pad($_dateInfoAry['minEnd'], 2, 0, STR_PAD_LEFT);
	
	$_dateTimeStart = $_dateText.' '.$_hourStart.':'.$_minStart.':00';
	$_dateTimeEnd = $_dateText.' '.$_hourEnd.':'.$_minEnd.':00';

	$infoAry = array();
	$infoAry['ActivityDateStart'] = $_dateTimeStart;
	$infoAry['ActivityDateEnd'] = $_dateTimeEnd;
	$infoAry['LastModifiedBy'] = $_SESSION['UserID'];
	
	if ($_dateRecordId) {  // update
	    $sql = $libenroll->UPDATE2TABLE($tableName,$infoAry,array('RecordID'=>$_dateRecordId),false);
	    $res = $libenroll->db_db_query($sql);
	    $result['Update_Time_Limit_'.$_dateRecordId] = $res;
	    $action = 'Update';
	}
	else {     // add
	    $infoAry['UserID'] = $recordId;
	    $infoAry['DateInput'] = 'now()';
	    $infoAry['InputBy'] = $_SESSION['UserID'];
	    $sql = $libenroll->INSERT2TABLE($tableName,$infoAry,array(),false);
	    $res = $libenroll->db_db_query($sql);
	    $_dateRecordId = $libenroll->db_insert_id();
	    $result['Add_Time_Limit_'.$_dateRecordId] = $res;
	    $action = 'Create';
	}
	
	$timeDisplay[] = $_dateTimeStart.' to '.$_dateTimeEnd;
}
$jsonTimeDisplay = $json->encode($timeDisplay);

### Update DB record

if ($numOfDeletedDate > 0) {
    foreach ((array)$deleteDateRecordIdAry as $_dateRecordId) {
        $infoAry = array();
        $infoAry['RecordStatus'] = '0';
        $infoAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $libenroll->UPDATE2TABLE($tableName,$infoAry,array('RecordID'=>$_dateRecordId),false);
        $res = $libenroll->db_db_query($sql);
        $result['Soft_Delete_Time_Limit_'.$_dateRecordId] = $res;
    }
	$action = 'Delete';
}


####################################################
######### Update Time Limit Setting Info [End] ########
####################################################

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);


// save log
$module= 'eEnrolment';
$section= "Save_Time_Limit_Setting";
$recordTitle = $_SESSION['RecordTitle'];
$recordDetail= "$recordType Name: $recordTitle<br>Action: $action<br>Details: $jsonTimeDisplay";

$liblog->INSERT_LOG($module, $section, $recordDetail, $tableName, $recordId);

?>