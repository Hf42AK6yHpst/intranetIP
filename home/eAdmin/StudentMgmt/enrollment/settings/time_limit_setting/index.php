<?php
/*
 *  Using:
 *
 *  Purpose:  show time limit list for instructor
 *
 *
 * 	2018-08-10 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
    clearCookies($arrCookies);
}
else
{
    updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eEnrolment']['settings']['timeLimitSetting']['title']);

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;		// default ascending
$field = ($field == "") ? 0 : $field;

$li = new libdbtable2007($field, $order, $pageNo);

$cond = '';
$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if($keyword!="")
{
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (u.ChineseName LIKE '%$kw%' OR u.EnglishName LIKE '%$kw%')";
    unset($kw);
}

$instructorName = getNameFieldWithClassNumberByLang("u.", $sys_custom['hideTeacherTitle']);

$sql = "SELECT
                CONCAT('<a href=\"time_limit_setting.php?StaffID=',u.UserID,'\">',$instructorName,'</a>'),
                DATE(s.ActivityDateStart) AS ActivityDate,
                CONCAT(DATE_FORMAT(s.ActivityDateStart,'%H:%i'),' - ',DATE_FORMAT(s.ActivityDateEnd,'%H:%i')) AS TimeSlot
        FROM
                INTRANET_ENROL_TIME_LIMIT_SETTING s
        LEFT JOIN
                INTRANET_USER u ON u.UserID=s.UserID
        WHERE
                s.RecordStatus='1'".$cond;

$extra_column = 1;

$li->field_array = array("u.EnglishName", "ActivityDateStart", "ActivityDateStart");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";
$li->column_array = array(0,0,0);

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'select_staff.php')",$button_new,"","","",0);

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='40%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['General']['Date'])."</th>\n";
$li->column_list .= "<th width='25%' style='font-weight: bold;'>".$Lang['General']['Time']."</th>\n";
$pos++;

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

?>

<form name="form1" id="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%">
					<div class="content_top_tool"  style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear:both" />
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?=$li->display()?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>