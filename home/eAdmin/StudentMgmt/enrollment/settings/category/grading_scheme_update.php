<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($UserID))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$dataAry['CategoryID'] = $sel_category;
$dataAry['MinScore'] = $minScore;
$dataAry['MaxScore'] = $maxScore;
$dataAry['GradeChar'] = $grade;

$libenroll->Start_Trans();
if ($flag == "add") {
	$result = $libenroll->Add_Grading_Scheme($dataAry);
} else {
	$result = $libenroll->Edit_Grading_Scheme($dataAry, $RuleID);
}

if($result) {
	$libenroll->Commit_Trans();
	$msg = "AddSuccess";	
} else {
	$libenroll->RollBack_Trans();
	$msg = "AddUnsuccess";
}

intranet_closedb();
header("Location: grading_scheme.php?msg=$msg");
?>