<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($UserID))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");


$libenroll->Start_Trans();
$RuleIdAry = $RuleID;

$result = $libenroll->Delete_Grading_Scheme($RuleIdAry);

if($result) {
	$libenroll->Commit_Trans();
	$msg = "DeleteSuccess";	
} else {
	$libenroll->RollBack_Trans();
	$msg = "DeleteUnsuccess";
}

intranet_closedb();
header("Location: grading_scheme.php?msg=$msg");
?>