<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


if (!$plugin['eEnrollment'] && !$le->hasAccessRight($UserID) && !$le->IS_ENROL_ADMIN($UserID)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$le = new libclubsenrol();	
$CurrentPage = "PageCategorySetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $le->GET_MODULE_OBJ_ARR();

if(is_array($RuleID)) $RuleID = $RuleID[0];

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$TAGS_OBJ[] = array($eEnrollmentMenu['cat_setting'], "category.php", 0);
if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
	$TAGS_OBJ[] = array($Lang['eEnrolment']['GradingScheme'], "grading_scheme.php", 1);
}

$RuleData = $libenroll->Get_Grading_Scheme_By_RuleID($RuleID);
list($CategoryId, $minScore, $maxScore, $grade) = $RuleData;
$flag = (count($RuleData)>0) ? "edit" : "add";  

$category_selection = $libenroll->GET_CATEGORY_SEL($CategoryId, 1, "", $i_alert_pleaseselect);

$linterface->LAYOUT_START();


$CategoryList = $le->GET_CATEGORY_LIST();


if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="javascript">

function checkform(obj){
        return true;
}

function goBack() {
	self.location.href = "grading_scheme.php";	
}

function checkForm() {
	var obj = document.form1;
	if(obj.minScore.value=="" || obj.maxScore.value=="" || isNaN(obj.minScore.value) || isNaN(obj.maxScore.value)) {
		alert("<?=$i_alert_pleasefillin." ".$Lang['eEnrolment']['ScoreRange']?>");
		return false;
	}
	
	if(parseInt(obj.minScore.value) > parseInt(obj.maxScore.value)) {
		alert("<?=$Lang['eEnrolment']['AlertMsg_MinScoreNotGreaterThanMaxScore']?>");
		return false;	
	}
	
	if(obj.grade.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$Lang['eEnrolment']['Grade']?>");
		return false;	
	}
	
}
</script>


<br/>
<form name="form1" method="POST" action="grading_scheme_update.php" onSubmit="return checkForm()">

<div class='navigation_v30'>
	<a href="javascript:;" onClick="javascript:goBack()"><?=$Lang['eEnrolment']['List']?></a>
	<span><?=$Lang['Btn']['New']?></span>
</div>
<p>&nbsp;</p>
<table class="form_table_v30">
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title" width="25%"><?= $Lang['eEnrolment']['Category']?> <span class="tabletextrequire">*</span></td>
		<td>
			<?=$category_selection?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title" width="25%"><?= $Lang['eEnrolment']['ScoreRange']?> <span class="tabletextrequire">*</span></td>
		<td>
			<input type="text" id="minScore" name="minScore" value="<?=$minScore?>" class="textboxnum">
			-
			<input type="text" id="maxScore" name="maxScore" value="<?=$maxScore?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['Grade']?> <span class="tabletextrequire">*</span></td>
		<td><input type="text" id="grade" name="grade" value="<?=$grade?>" class="textboxnum"></td>
	</tr>
</table>

<div class="edit_bottom">
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit");?> 
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button", "goBack()");?>
</div>

<input type="hidden" name="flag" id="flag" value="<?=$flag?>">
<input type="hidden" name="RuleID" id="RuleID" value="<?=$RuleID?>">

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>