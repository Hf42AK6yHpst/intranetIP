<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_opendb();
$ldb = new libdb();
$libenroll = new libclubsenrol();
$lexport = new libexporttext();
// get information from INTRANET_ENROL_CATEGORY
$sql = "SELECT
				CategoryID,
                CategoryName,
				OleCategoryID,
				CategoryTypeID
		FROM
				INTRANET_ENROL_CATEGORY
		Order By
				DisplayOrder
	   ";
$categoryInfoAry = $libenroll->returnResultSet($sql);
$numOfCategory = count($categoryInfoAry);
$oleCategoryAssoAry = $libenroll->Get_Ole_Category_Array($returnAsso=true);
$categoryTypeAssoAry = $libenroll->Get_CategoryType_Array($returnAsso=true);

//build header array
$headerAry = array();
$headerAry[] = "Category Title";
$headerAry[] = "OLE Category Mapping";

//bulid header array
$dataAry = array();
for ($i=0; $i<$numOfCategory; $i++) {	
    $thisCategoryNameLink = $categoryInfoAry[$i]['CategoryName'];
    $thisOleCategoryID = $categoryInfoAry[$i]['OleCategoryID'];
    $thisOleCategoryName = $oleCategoryAssoAry[$thisOleCategoryID];
    $thisOleCategoryName = ($thisOleCategoryName=='')? $Lang['General']['EmptySymbol'] : $thisOleCategoryName;
    $thisCategoryTypeID = $categoryInfoAry[$i]['CategoryTypeID'];
    $thisCategoryType = $categoryTypeAssoAry[$thisCategoryTypeID];
    $dataAry[$i][0] = $thisCategoryNameLink;
    $dataAry[$i][1] = $thisOleCategoryName;
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'Category.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);
?>