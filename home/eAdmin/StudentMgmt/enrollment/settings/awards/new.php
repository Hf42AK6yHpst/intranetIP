<?php
/*
 * 	Log
 * 	
 *  
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();	
$libenroll_ui = new libclubsenrol_ui();


if(!$plugin['eEnrollment'] || !$sys_custom['eEnrolment']['General']['AwardMgmt']||!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;	
}
	
$linterface = new interface_html();
$CurrentPage = "PageAwardSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eEnrolment']['AwardMgmt']['Award'], "", 1);
$linterface->LAYOUT_START();

if(is_array($_POST['AwardID'])){
	$AwardID = $_POST['AwardID'][0];		
}
else{
	$AwardID = $_GET['AwardID'];
}
if($AwardID>0 || $AwardID != ''){
	$isUpdating = 1;
	$AwardInfoArr = $libenroll->GET_ENROL_AWARD($AwardID);
}
		
# page navigation (leave the array empty if no need)
if ($AwardID != "") {
	$PAGE_NAVIGATION[] = array($button_edit." ".$Lang['eEnrolment']['AwardMgmt']['Award'], "");
} else {
	$PAGE_NAVIGATION[] = array($button_new." ".$Lang['eEnrolment']['AwardMgmt']['Award'], "");
}		
?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.AwardNameEn, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['AwardMgmt']['AwardNameEn'] ?>.")) return false;
	if(!check_text(obj.AwardNameCh, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['AwardMgmt']['AwardNameCh'] ?>.")) return false;
	obj.submit();
}
</SCRIPT>
<form name="form1" action="award_update.php" method="POST" enctype="multipart/form-data">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['eEnrolment']['AwardMgmt']['AwardNameEn']?></td>
		<td><?=$linterface->GET_TEXTBOX('AwardNameEn', 'AwardNameEn', $AwardInfoArr['AwardNameEn'])?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['eEnrolment']['AwardMgmt']['AwardNameCh']?></td>
		<td><?=$linterface->GET_TEXTBOX('AwardNameCh', 'AwardNameCh', $AwardInfoArr['AwardNameCh'])?></td>
	</tr>
</table>
</td></tr>
</table>
<?if($isUpdating){
echo $linterface->GET_HIDDEN_INPUT('AwardID','AwardID',$AwardID);
echo $linterface->GET_HIDDEN_INPUT('isUpdate','isUpdate','1');
}?>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='index.php'")?>
</div>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "UpdateBtn", "style=\"display: none\"") ?>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "ResetInfo();","CancelBtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" style=\"display: none\" ") ?>
	<p class="spacer"></p>
</div>
</td></tr>
</table>
<br/>

</form>
<?= $linterface->FOCUS_ON_LOAD("form1.AwardName") ?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>