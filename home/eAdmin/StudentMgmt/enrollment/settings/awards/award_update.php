<?php
/*
 * 	Log:
 * 	
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;
}

$AwardNameEn = standardizeFormPostValue($_POST['AwardNameEn']);
$AwardNameCh = standardizeFormPostValue($_POST['AwardNameCh']);
$AwardArr['AwardNameEn'] = $AwardNameEn;
$AwardArr['AwardNameCh'] = $AwardNameCh;
$AwardID = $_POST['AwardID'];

$successAry = array();
$libenroll->Start_Trans();

if ($AwardID == "" && $AwardNameCh != '' && $AwardNameEn != '') {
	#Get Max DisplayOrder's value and increment it by 1
	$sql = "SELECT MAX(DisplayOrder) FROM INTRANET_ENROL_AWARD";
	$ReturnMaxOrderVal = $libenroll->returnVector($sql);
	$AwardArr['DisplayOrder'] = $ReturnMaxOrderVal[0] + 1;
	$successAry['add'] = $libenroll->DB_HANDLING_AWARD('insert',$AwardArr);
}
else if($_POST['isUpdate']) {
	$AwardArr['AwardID'] = $AwardID;
	$successAry['edit'] = $libenroll->DB_HANDLING_AWARD('update',$AwardArr);
}
else if($_GET['isDelete']) {
	if(is_array($AwardID)){
		foreach((array)$AwardID as $_Id){
			$AwardArr['AwardID'] = $_Id;
			$successAry['delete'] = $libenroll->DB_HANDLING_AWARD('delete',$AwardArr);
		}
	}
	else{
		$AwardArr['AwardID'] = $AwardID;
		$successAry['delete'] = $libenroll->DB_HANDLING_AWARD('delete',$AwardArr);
	}
}

if (in_array(false, $successAry)) {
	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'UpdateSuccess';
}
intranet_closedb();
if($_POST['Ajax']){
	echo $ReturnMsgKey;
	exit;
}
header("Location: index.php?ReturnMsgKey=$ReturnMsgKey");
?>