<?php
// using:
/*
* Date: 2018-06-21 Vito
* Add export function
 * 
 * 	Log
 * 	
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$sys_custom['eEnrolment']['General']['AwardMgmt']||!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;	
}

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();


$CurrentPage = "PageAwardSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eEnrolment']['AwardMgmt']['Award'], "", 1);

$awardInfoAry = $libenroll->GET_ENROL_AWARD('');
$numOfAward = count($awardInfoAry);

$ReturnMsgKey = $_GET['ReturnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];

$linterface->LAYOUT_START($ReturnMsg);
echo $linterface->Include_JS_CSS();	

## Edit/ Delete Buttons	
$ActionBtnArr = array();
$ActionBtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'AwardID[]\',\'new.php\')');
$ActionBtnArr[] = array('delete', 'javascript:checkRemove2(document.form1,\'AwardID[]\',\'goDeleteCategory();\')');

## Display Result
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="POST" action="">'."\n";
    //$x .= '<input type="hidden" id="selectedAwardInfoHidden" name="selectedAwardInfo" value="">';
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div class="content_top_tool">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Category();")."\n";
							$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:js_Go_Export();');
							$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:js_Go_Print();');
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
//					$x .= $linterface->Get_Search_Box_Div('keyword', $keyword)."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);			
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th style="width:1;">#</th>'."\n";
									$x .= '<th style="width:47%;">'.$Lang['eEnrolment']['AwardMgmt']['AwardNameEn'].'</th>'."\n";
									$x .= '<th style="width:47%;">'.$Lang['eEnrolment']['AwardMgmt']['AwardNameCh'].'</th>'."\n";
									$x .= '<th style="width:5%;">&nbsp;</th>'."\n";
									$x .= '<th style="width:1;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'AwardID[]\'):setChecked(0,this.form,\'AwardID[]\')" name="checkmaster"></th>'."\n";
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";
					
							$x .= '<tbody>'."\n";
								if ($numOfAward == 0) {
									$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else {
									for ($i=0; $i<$numOfAward; $i++) {
										$thisAwardID  = $awardInfoAry[$i]['AwardID'];
										//echo ($thisAwardID);
										$thisAwardNameLink = '<a href="new.php?AwardID='.$thisAwardID.'" class="tablelink">'.intranet_htmlspecialchars($awardInfoAry[$i]['AwardNameEn']).'</a>';
										$thisAwardNameLinkCh = '<a href="new.php?AwardID='.$thisAwardID.'" class="tablelink">'.intranet_htmlspecialchars($awardInfoAry[$i]['AwardNameCh']).'</a>';
                                        
										//for exporting
										$x .= '<input type="hidden" name="exportAwardAry['.$i.'][0]" value="'.intranet_htmlspecialchars($awardInfoAry[$i]['AwardNameEn']).'" />';
										$x .= '<input type="hidden" name="exportAwardAry['.$i.'][1]" value="'.intranet_htmlspecialchars($awardInfoAry[$i]['AwardNameCh']).'" />';
										
										$x .= '<tr id="tr_'.$thisAwardID .'">'."\n";
											$x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
											$x .= '<td>'.$thisAwardNameLink.'</td>'."\n";
											$x .= '<td>'.$thisAwardNameLinkCh.'</td>'."\n";
											$x .= '<td class="Dragable">'."\n";
												$x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move'])."\n";
											$x .= '</td>'."\n";
											$x .= '<td>'."\n";
												$x .= '<input type="checkbox" id="AwardChk" class="AwardChk" name="AwardID[]" value="'.$thisAwardID.'">'."\n";
											$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									}					
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="javascript">
$(document).ready( function() {	
	js_Init_DND_Table();
});

function js_Go_New_Category() {
	window.location = 'new.php';
}

function js_Go_Export(){
	$('form#form1').attr('target', '_self').attr('action', 'awardExport.php').submit();
}
function js_Go_Print(){
	$('form#form1').attr('target', '_blank').attr('action', 'awardPrint.php').submit();
}

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "ContentTable") {
				Block_Element(table.id);
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					{
						var thisID = rows[i].id;
						var thisIDArr = thisID.split('_');
						var thisObjectID = thisIDArr[1];
						RecordOrder += thisObjectID + ",";
						console.log(thisID);
					}
				}

				// Update DB
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_Award",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						js_Reset_Display_Range();
						
						// Get system message
						if (ReturnData=='1') {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess']?>';			
						}
						else {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
	
//Delete Category	
function goDeleteCategory() {
	var jsSelectedCategoryIDArr = new Array();
	$('input.AwardChk:checked').each( function() {
		jsSelectedCategoryIDArr[jsSelectedCategoryIDArr.length] = $(this).val();
	});
	var jsSelectedCategoryIDList = jsSelectedCategoryIDArr.join(',');

	$.post(
		"ajax_validate.php", 
		{ 
			Action: "Is_Award_Record",
			CategoryIDList: jsSelectedCategoryIDList	
		},
		
		function(ReturnData) {
			var jsCanSubmit = false;
			if (ReturnData == '1') {
				if (confirm('<?=$Lang['eEnrolment']['Settings']['WarningArr']['CategoryLinkedToDataAlready']?>')) {
					jsCanSubmit = true;
				}
				else {
					jsCanSubmit = false;
				}
			}
			else {
				jsCanSubmit = true;
			}
			if (jsCanSubmit) {
				$('form#form1').attr('action', 'award_update.php?isDelete=1').submit();
			}
		}
	);
}

//Reset the ranking display
function js_Reset_Display_Range(){
	var jsRowCounter = 0;
	$('span.rowNumSpan').each( function () {
		$(this).html(++jsRowCounter);
	});
}
</script>

<?php
echo $x;
?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>