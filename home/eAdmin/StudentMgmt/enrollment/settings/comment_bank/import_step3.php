<?php
//using  
################## Change Log [Start] #################
#
#	Date	:	2016-01-22	Kenneth
#			Add WebSAMSCode Import
#
#	Date	:	2015-06-10	Shan
# 			Create this page
#
################## Change Log [End] ###################


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

$CurrentPage = "PageCommentBankSetting";
$CurrentPageArr['eEnrolment'] = 3;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

####importing data
$result =$libenroll->Get_Achievement_Comment_Temp();
//debug_pr($result);
$successArr = array();
foreach($result as $_CommentData){
	 	
	$_code = $_CommentData['CommentCode'];	 
	$_comment = $_CommentData['Comment'];
 	$_webSAMSCode = $_CommentData['WebSAMSCode'];
 
 $successArr[] =  $libenroll->Add_Achievement_Comment($_code,$_comment,$RecordType,$_webSAMSCode);

}

if(in_array(false,$successArr)){
	$numSuccess = 0;
}
else{
	$numSuccess = count($successArr);
}


### iFrame for validation
$thisSrc = "ajax_task.php?task=validateSelfAccountImport&targetFilePath=".$targetFilePath;
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";

# Page Tag
//$RecordType = ($RecordType=='')? $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'] : $RecordType;
$TAGS_OBJ[] = array($Lang['eEnrolment']['PerformanceCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['AchievementCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['TeacherCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment']);

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
if ($RecordType=="P") $navigationAry[] =array($Lang['eEnrolment']['PerformanceCommentBank'], 'index.php?RecordType='.$RecordType);
else if ($RecordType=="A") $navigationAry[] =array($Lang['eEnrolment']['AchievementCommentBank'], 'index.php?RecordType='.$RecordType);
else if  ($RecordType=="C") $navigationAry[] =array($Lang['eEnrolment']['TeacherCommentBank'], 'index.php?RecordType='.$RecordType);
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "done();");


$linterface->LAYOUT_START();
?>

<script type="text/javascript">
$(document).ready( function() {
	
});

function done() {
	window.location = 'index.php?RecordType=<?=$RecordType?>';
}
</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<br />
	<?=$htmlAry['steps']?>
	
		<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$numSuccess?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
 <div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>