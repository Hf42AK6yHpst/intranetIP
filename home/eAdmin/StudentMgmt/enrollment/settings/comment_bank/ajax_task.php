<?php 
################## Change Log [Start] #################
#   using  
#	Date	:	2016-01-22	Kenneth
#			Add WebSAMSCode Import for RecordType=='P'
#	Date	:	2015-06-11	shan
# 			Create this page
#
################## Change Log [End] ###################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();


if($task = 'validateCommentBankImport'){
	//$webSAMSCheckingArr = array_filter(Get_Array_By_Key($data,'2'));	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	$limport = new libimporttext();
	$libuser = new libuser();
	$libenroll = new libclubsenrol();
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	
	### Get Data from the csv file
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);

	$errorCount = 0;
	$successCount = 0;
	$rowErrorRemarksAry = array();
	$insertTempDataAry = array();
	$getRecordType =$_GET["RecordType"] ;
	
	## check Record
 	$result = $libenroll->Get_Achievement_Comment_Temp_By_Record_Type($getRecordType);
		
	##verify imported data
	for ($i=0; $i<$numOfData; $i++) {
		 
		$_code = trim($csvData[$i][0]); // code
		$_comment = $csvData[$i][1]; // comment content
		$_comment = $libenroll->Get_Safe_Sql_Query(standardizeFormPostValue($_comment));
		if($getRecordType=='P'){
			$_webSAMSCode = $csvData[$i][2];
			$_webSAMSCode = $libenroll->Get_Safe_Sql_Query(standardizeFormPostValue($_webSAMSCode));
		}else{
			$_webSAMSCode='';
		}
		//debug_pr($_webSAMSCode);
		// find blank space
		 if(trim($_code) == ""){
		 	$errorArr[$i][0] = "emptyCode";	 			 	 
		 }  		
		 
		 else if ( trim($_comment) ==""){			 	  
		 	$errorArr[$i][0] = "emptyComment";				
		 }
		  
         # find duplicated data
 		  for($k=0;$k<sizeof($result);$k++){	
 		  	
	       if ( $_code ==$result[$k][CommentCode] ){ 
	       		       	
	        if ($errorArr[$i][0] == "") {
		  	$errorArr[$i][0] = "duplicateCode";
	             }		 
	             
	        else if ($errorArr[$i][0] == "emptyComment") {
	        	$errorArr[$i][1] = "duplicateCode";
	            }	
	 		}
 		}   
 		
 		 ## Insert Data in Temp Table
		//$TempValueArr[$i] = '("'.$_SESSION['UserID'].'","'.$i.'","'.$_code.'","'.$_comment.'", now() )';
		$TempValueArr[$i] = '("'.$_SESSION['UserID'].'","'.$i.'","'.$_code.'","'.$_comment.'","'.$_webSAMSCode.'", now() )';
		//debug_pr($TempValueArr);
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}		
  		   
 	#counting number of correct data
	for ($i=0;$i<sizeof($csvData);$i++){ 	 
		if ($errorArr[$i]==""){
	 		 $successCount++; 	
 		} 	 	
 	}
    # counting number of incorrect data
	$errorCount=count($errorArr);	
	
	### Display Record Error Table	//TODO
	if($errorCount == 0){
		$libenroll->Insert_Achievement_Comment_Temp($TempValueArr);
	 }
	
	$x = '';
	if($errorCount > 0) {		
	 		// get table header
			//$columnTitleDisplayAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
			$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
		 		$x .= '<tr>';
					$x .= '<th>'.$Lang['General']['Code'].'</th>';
					$x .= '<th>'.$eEnrollment['comment'].'</th>';
					if($_GET["RecordType"]=='P'){
						$x .= '<th>'.$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'].'</th>';
					}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';	
			$x .= '<tbody>';	
         
        #generating imported data error			 
         foreach($errorArr as $i => $rowError){
         	
         	$_code = trim($csvData[$i][0]);  // code
	 		$_comment = $csvData[$i][1]; // comment content	
	 		$_comment = $libenroll->Get_Safe_Sql_Query(standardizeFormPostValue($_comment));
	 		$_webSAMSCode = $csvData[$i][2];	
      		$css_1 = 'red';
		 	$css_2 = '';
		
			$x .= '<tr>';			
			 if ($rowError[0] == "emptyCode"){			  	 
			  	$x .= '<td class="tabletext '.$css_2.'">'. $_code .'</td>';
		 		$x .= '<td class="tabletext '.$css_1.'">'. $_comment .'</td>'; 
		 		if($_GET["RecordType"]=='P'){
		 			$x .= '<td class="tabletext '.$css_2.'">'. $_webSAMSCode .'</td>'; 
		 		}
		 		$x .= '<td class="tabletext">'.$Lang['General']['JS_warning']['InputCode'];	
		 		}		 			 				
			
			 if ($rowError[0] == "emptyComment"){
			  	
			  	$x .= '<td class="tabletext '.$css_1.'">'. $_code .'</td>';
		 		$x .= '<td class="tabletext '.$css_2.'">'. $_comment .'</td>'; 
		 		if($_GET["RecordType"]=='P'){
		 			$x .= '<td class="tabletext '.$css_2.'">'. $_webSAMSCode .'</td>'; 
		 		}
		 		$x .= '<td class="tabletext">'.$ec_warning['comment_content'];				 
				   
				 if ($rowError[1] == "duplicateCode"){			
			 		$x .= '</br>'.$Lang['General']['JS_warning']['CodeIsInUse'];				 	  
					}	
			 	
			    }
			     	
			 if ($rowError[0] == "duplicateCode"){	
			  	$x .= '<td class="tabletext '.$css_1.'">'. $_code .'</td>';
		 		$x .= '<td class="tabletext '.$css_2.'">'. $_comment .'</td>'; 
		 		if($_GET["RecordType"]=='P'){
		 			$x .= '<td class="tabletext '.$css_2.'">'. $_webSAMSCode .'</td>'; 
		 		}
		 		$x .= '<td class="tabletext">'.$Lang['General']['JS_warning']['CodeIsInUse'];			 
				}	
	
			$x .= '</td>';
			$x .= '</tr>';	
			
       }
		
		$x .= '</tbody>';
		$x .= '</table>';
		$htmlAry['errorTbl'] = $x;
					
	 } 
	 
  	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
	
	if ($errorCount == 0) {
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
	}
		
	$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}



intranet_closedb();
?>