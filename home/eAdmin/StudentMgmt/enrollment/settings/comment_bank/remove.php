<?php
// Using: 
/********************************************************
 *  Modification Log
 *  Date: 2014-11-13 (Omas)
 *		Passing new parameter RecordType A,C,P for redirecting
 * 
 * 
 * Date: 2013-03-26 (Rita)
 * Detail: add this page for Achievement Comment Bank
 ********************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$plugin['eEnrollment']){
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($_POST))
{
	header("Location:index.php");
}

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	header("Location:/index.php");
}

$commentID = $_POST['CommentID'];

if(count($commentID)>0){	
	$successResult =  $libenroll->Remove_Achievement_Comment($commentID);	
}

$xmsg ='';
if($successResult){
	$xmsg = 'DeleteSuccess';
}else{
	$xmsg = 'DeleteUnsuccess';
}

header("Location:index.php?RecordType=$RecordType&xmsg=$xmsg");


?>