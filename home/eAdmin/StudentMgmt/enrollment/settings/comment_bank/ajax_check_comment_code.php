<?php
// Using: 
/***********************************************
 * Date: 2013-04-11 (Rita)
 * Details: create this page to check duplication of comment code
 ***********************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_opendb();

$libenroll = new libclubsenrol();

$code = standardizeFormPostValue($_POST['code']);
$commendId = $_POST['commendId'];
$numOfResultArray = 0 ;

if($commendId==''){ #New Record	
	$sql = $libenroll->Get_Achievement_Comment_Sql('', $code);
	$resultArray = $libenroll->returnArray($sql);
	$numOfResultArray = count($resultArray);
	
}elseif($commendId!=''){
	$sql = $libenroll->Get_Achievement_Comment_Sql($commendId);
	$exisitingCommentResult = $libenroll->returnArray($sql);
	$existingCommentCode = $exisitingCommentResult[0]['CommentCode'];

	if($existingCommentCode==$code){
		// do nth
	}elseif($existingCommentCode!=$code){
		$sql = $libenroll->Get_Achievement_Comment_Sql('', $code);
		$resultArray = $libenroll->returnArray($sql);
		$numOfResultArray = count($resultArray);
	}	
}


if($numOfResultArray>0)
{
	echo 'AlreadyExist';
}
else
{
	echo 'NotExist';	
}

intranet_closedb();
?>