<?php

//using: 

#################################
#		Change Log
#	Date:	2016-01-19 Kenneth
#		- export WebSAMS Code for 'Performance Comment Bank'
#################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();	

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$filename = "achievement_comment.csv";

# Header Column
if($RecordType=='P'){
	$exportColumn = array("Code", "Comment Content","WebSAMS Code");
}else{
	$exportColumn = array("Code", "Comment Content");
}
# Data Row
$sql = $libenroll->Get_Achievement_Comment_Sql('','',$RecordType);
$result = $libenroll->returnArray($sql,4);

$exportData = array();

for($i=0;$i<count($result);$i++){
	$thisCommentCode = $result[$i]['CommentCode'];
	$thisComment = $result[$i]['Comment'];
	$thisWebSAMSCode = $result[$i]['WebSAMSCode'];
	$exportData[$i][] = $thisCommentCode;
	$exportData[$i][] = $thisComment;
	if($RecordType=='P'){
		$exportData[$i][] = $thisWebSAMSCode;
	}
}

$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);

?>