<?php
# using:
/*
 * 	Log
 *  Date:   2018-08-21 [Anna]  Added $sys_custom['eEnrolment']['EnableEditUsedRole'] 
 * 	Date:	2015-01-12 [Kenneth] Page is Created
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$lgeneralsettings = new libgeneralsettings();

$libdb = new libdb();

$countOfLoop = count($WebSAMS);
for($i=0;$i<$countOfLoop;$i++){
    
    if($sys_custom['eEnrolment']['EnableEditUsedRole']){
        $roleTitleSQL = ",Title = '".$RoleTitle[$i]."'";
    }
    
	$sql = "UPDATE INTRANET_ROLE 
		SET WebSAMSCode = '".$WebSAMS[$i]."',
            DateModified = now()
        $roleTitleSQL
		WHERE RoleID = '".$roleID[$i]."' ";

	$libdb->db_db_query($sql);
}


intranet_closedb();
header("Location: role.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);

?>