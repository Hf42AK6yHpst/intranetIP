<?php 
################## Change Log [Start] #################
#   using  
#	Date	:	2016-01-22	Kenneth
#			Add WebSAMSCode Import for RecordType=='P'
#	Date	:	2015-06-11	shan
# 			Create this page
#
################## Change Log [End] ###################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();


if($task == 'validateRoleImport'){
	//$webSAMSCheckingArr = array_filter(Get_Array_By_Key($data,'2'));	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	$limport = new libimporttext();
	$libuser = new libuser();
	$libenroll = new libclubsenrol();
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	
	### Get Data from the csv file
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);

	$errorCount = 0;
	$successCount = 0;
	$rowErrorRemarksAry = array();
	$insertTempDataAry = array();
	$getRecordType =$_GET["RecordType"] ;
	
	$sql = 'SELECT RoleID, Title, WebSAMSCode
			FROM INTRANET_ROLE
			WHERE RecordType = 5';
	## check Record
	$result = $libenroll->returnResultSet($sql);
 	
		
	##verify imported data
	for ($i=0; $i<$numOfData; $i++) {
		 
		$_role = trim($csvData[$i][0]); // Role
		$_webSAMSCode = $csvData[$i][1]; // WebSAMS

		// find blank space
		 if(trim($_role) == ""){
		 	$errorArr[$i][0] = "emptyRole";	 			 	 
		 }  		
		 
         # find duplicated data
 		  for($k=0;$k<sizeof($result);$k++){	
 		  	
	       if ( $_role ==$result[$k]['Title'] ){ 
	       		       	
	        if ($errorArr[$i][0] == "") {
		  		$errorArr[$i][0] = "duplicateRole";
	        }		 
	             
	 		}
 		}   
 		
 		 ## Insert Data in Temp Table
		//$TempValueArr[$i] = '("'.$_SESSION['UserID'].'","'.$i.'","'.$_code.'","'.$_comment.'", now() )';
		$TempValueArr[$i] = '("'.$_SESSION['UserID'].'","'.$i.'","'.$_role.'","'.$_webSAMSCode.'")';
		//debug_pr($TempValueArr);
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}		
  		   
 	#counting number of correct data
	for ($i=0;$i<sizeof($csvData);$i++){ 	 
		if ($errorArr[$i]==""){
	 		 $successCount++; 	
 		} 	 	
 	}
    # counting number of incorrect data
	$errorCount=count($errorArr);	
	
	### Display Record Error Table	//TODO
	if($errorCount == 0){
		$libenroll->Insert_Role_With_Title_Temp($TempValueArr);
	 }
	
	$x = '';
	if($errorCount > 0) {		
	 		// get table header
			//$columnTitleDisplayAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
			$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
		 		$x .= '<tr>';
					$x .= '<th>'.$eEnrollment['role'].'</th>';
					$x .= '<th>'.$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'].'</th>';
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';	
			$x .= '<tbody>';	
         
        #generating imported data error			 
         foreach($errorArr as $i => $rowError){
         	
         	$_role = trim($csvData[$i][0]);  // role
	 		$_webSAMSCode = $csvData[$i][1]; // WEBSAMS	
      		$css_1 = 'red';
		 	$css_2 = '';
		
			$x .= '<tr>';			
			 if ($rowError[0] == "emptyRole"){			  	 
			  //	$x .= '<td class="tabletext '.$css_2.'">'. $_role .'</td>';
			 	$x .= '<td class="tabletext"><font color= "'.$css_1.'">'. $_role .'</font></td>';
			 	$x .= '<td class="tabletext '.$css_2.'">'. $_webSAMSCode .'</td>'; 
		 		$x .= '<td class="tabletext">'.$Lang['General']['JS_warning']['InputRole'];	
		 		}		 			 				
			     	
			 if ($rowError[0] == "duplicateRole"){	
//			  	$x .= '<td class="tabletext '.$css_1.'">'. $_role .'</td>';
	 			$x .= '<td class="tabletext"><font color= "'.$css_1.'">'. $_role .'</font></td>';
		 		$x .= '<td class="tabletext '.$css_2.'">'. $_webSAMSCode .'</td>'; 
		 		$x .= '<td class="tabletext">'.$Lang['General']['JS_warning']['RoleIsInUse'];			 
				}	
	
			$x .= '</td>';
			$x .= '</tr>';	
			
       }
		
		$x .= '</tbody>';
		$x .= '</table>';
		$htmlAry['errorTbl'] = $x;
					
	 } 
	 
  	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
	
	if ($errorCount == 0) {
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
	}
		
	$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}



intranet_closedb();
?>