<?php

//using: 

#################################
#		Change Log
#	Date:	2016-01-19 anna
#		- export role name and WebSAMS code
#################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();	

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$filename = "role_name_WebSAMS.csv";

# Header Column

	$exportColumn = array("Role","WebSAMS Code");

# Data Row
$sql = 'SELECT RoleID, Title, WebSAMSCode FROM INTRANET_ROLE WHERE RecordType = 5';
$roleArr = $libenroll->returnResultSet($sql);


$exportData = array();

for($i=0;$i<count($roleArr);$i++){
	$thisRoleTitle = $roleArr[$i]['Title'];
	$thisWebSAMSCode = $roleArr[$i]['WebSAMSCode'];
	
	$exportData[$i][] = $thisRoleTitle;
	$exportData[$i][] = $thisWebSAMSCode;
}

$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);

?>