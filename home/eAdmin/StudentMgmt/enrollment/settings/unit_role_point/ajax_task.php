<?php 
################## Change Log [Start] #################
#   using  
#	Date	:	2017-08-24	anna
# 			Create this page
#
################## Change Log [End] ###################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe(standardizeFormGetValue($_GET['AcademicYearID']));

if($task == 'RolePointImport'){
	//$webSAMSCheckingArr = array_filter(Get_Array_By_Key($data,'2'));	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	$limport = new libimporttext();
	$libuser = new libuser();
	$libenroll = new libclubsenrol();

	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	
	### Get Data from the csv file
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);
//	debug_pr($csvData);
	$errorCount = 0;
	$successCount = 0;
	$rowErrorRemarksAry = array();
	$insertTempDataAry = array();

	## check Record
	$result = $libenroll->Get_Role_Info();
	$roleInfo = BuildMultiKeyAssoc($result, 'RoleID');
	##verify imported data
	$JSWarnig = array();
	for ($i=0; $i<$numOfData; $i++) {
		
		$_club = trim($csvData[$i][0]); //club name
		$_semester = trim($csvData[$i][1]); // semester
		$_role = trim($csvData[$i][2]);// role title
		$rolePoint = intval($csvData[$i][3]);// role point
		
		if(trim($_club) == ""){
			$errorArr[$i][] = "emptyClub";
			$JSWarnig[$i][]= $Lang['General']['JS_warning']['UnitRolePoint']['ClubEmpty'];
		} 
		
		// find blank space
		 if(trim($_role) == ""){
		 	$errorArr[$i][] = "emptyRole";	 
		 	$JSWarnig[$i][]= $Lang['General']['JS_warning']['InputRole'];
		 }  
		 
		 $roleID = "";	    
  		  for($k=0;$k<sizeof($result);$k++){
  		  	$__roleTitle =  $result[$k]['Title'];
  		  	$__RoleID = $result[$k]['RoleID'];
  		  	
  		  	if ($_role == $__roleTitle){ 
  		  		$roleID = $__RoleID;		
	       	}
	       	
  		  }
	
  		  
  		  if($roleID== ""){
  		  	$errorArr[$i][] = "RoleNotFound";
  		  	$JSWarnig[$i][]= $Lang['General']['JS_warning']['UnitRolePoint']['NotFound'];
  		  }
  		  
  		  
  		  
  		  $libTerm = new academic_year_term();
  		  
  		  $isWholeYearClub = false;
  		  if ($_semester=='' || trim(strtoupper($_semester))=='YEARBASE') {
  		      $isWholeYearClub = true;
  		  }

  		  //2012-0802-1222-23147 - support input "YearBase" in Term column
  		  if($isWholeYearClub) {
  		      $thisYearTermID = "";
  		      $termYearTermID = "0";
  		  } else {
  		      $thisYearTermID = $libTerm->Get_YearTermID_By_Name($AcademicYearID, $_semester);
  		      $termYearTermID = $thisYearTermID;
  		  }
  		  
  		  if($termYearTermID == null){
  		      $errorArr[$i][] = "SemesterNotFound";
  		      $JSWarnig[$i][]= $Lang['General']['JS_warning']['UnitRolePoint']['SemesterNotFound'];
  		  }

//   		  debug_pr($termYearTermID);
//  	  $ClubInfoAry = $libenroll->Get_GroupInfo_By_GroupTitle($_club);
//   		  $ParTitleConds = "AND (ig.Title = '$_club' or ig.TitleChinese = '$_club') ";
//   		  $ParSemesterConds = "AND iegi.Semester";
//   		  $ClubInfoAry = $libenroll->getGroupsForEnrolSet("","",$ParTitleConds,$ParSemesterConds);
  		  $ClubInfoAry = $libenroll->Get_GroupInfo_By_GroupTitle(intranet_htmlspecialchars($_club), $AcademicYearID, $termYearTermID);
  	
  		  $EnrolGroupID = '';
  		  if(empty($ClubInfoAry)){
  		  	$errorArr[$i][] = "ClubNotFound";
  		  	$JSWarnig[$i][]= $Lang['General']['JS_warning']['UnitRolePoint']['ClubNotFound'];
  		  }else{
  		  	$_GroupID = $ClubInfoAry[0]['GroupID'];
  		  	$_EnrolGroupID = $libenroll->GET_ENROLGROUPID($_GroupID);
  		//      $_EnrolGroupID = $ClubInfoAry[]['EnrolGroupID'];
  		  }
  		  
  		//  is_int($rolePoint)? '':$errorArr[$i][0]="PointNotInt";
  		  	
  		  	
//   		  var_dump($rolePoint);
//   		  debug_pr(is_int($rolePoint));
//   		  var_dump($rolePoint);
//   		  debug_pr((string)$rolePoint);
//   		  debug_Pr((string)$csvData[$i][2]);
//   		  die();
  		  if((string)$rolePoint !== (string)$csvData[$i][3] && $csvData[$i][3]!=""){
  		  	 $errorArr[$i][] = "PointNotInt";
  		  	 $JSWarnig[$i][]= $Lang['General']['JS_warning']['UnitRolePoint']['PointNotInt'] ;	
		  }
// 	  		debug_pr((string)$rolePoint == (string)$csvData[$i][2]);
//   		  var_dump($rolePoint);
//   		  debug_pr($rolePoint);
//   		  debug_pr('-----------------------------');
  		 
//   		/  die();
  		  $TempValueArr[$i] = '("'.$_SESSION["UserID"].'","'.$roleID.'","'.$_EnrolGroupID.'","'.$rolePoint.'")';

		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}		

 	#counting number of correct data
	for ($i=0;$i<sizeof($csvData);$i++){ 	 
		if ($errorArr[$i]==""){
	 		 $successCount++; 	
 		} 	 	
 	}
    # counting number of incorrect data
	$errorCount=count($errorArr);	
	
	### Display Record Error Table	//TODO
	if($errorCount == 0){
		$libenroll->Insert_Role_Point_With_Title_Temp($TempValueArr);
	 }
	
	$x = '';
	if($errorCount > 0) {		
	 		// get table header
			//$columnTitleDisplayAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
			$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
		 		$x .= '<tr>';
		 			$x .= '<th>'.$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit'].'</th>';
		 			$x .= '<th>'.$Lang['eEnrolment']['UnitRolePoint']['Semester'].'</th>';
		 			$x .= '<th>'.$Lang['eEnrolment']['SysRport']['SchoolECA']['Role'].'</th>';
				//	$x .= '<th>'.$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'].'</th>';
		 			$x .= '<th>'.$Lang['eEnrolment']['UnitRolePoint']['Point'].'</th>';
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';	
			$x .= '<tbody>';	

        #generating imported data error			 
         foreach($errorArr as $i => $rowError){
         	$clubName = trim($csvData[$i][0]); 
         	$semester = trim($csvData[$i][1]); 
         	$role = trim($csvData[$i][2]);  // role
	// 		$_webSAMSCode = $csvData[$i][1]; // WEBSAMS	
	 		$rolePoint = $csvData[$i][3]; // point
	 		$JSWarningText = implode("<br>",$JSWarnig[$i]);
      		$css_1 = 'red';
		 	$css_2 = '';
		
			$x .= '<tr>';			
 			 	$x .= '<td class="tabletext"><font color= "'.$css_2.'">'. $clubName.'</font></td>';
 			 	$x .= '<td class="tabletext"><font color= "'.$css_2.'">'. $semester .'</font></td>';
 			 	$x .= '<td class="tabletext"><font color= "'.$css_2.'">'. $role .'</font></td>';
 			 	$x .= '<td class="tabletext '.$css_2.'">'. $rolePoint.'</td>';
 			 	$x .= '<td class="tabletext"><font color= "'.$css_1.'">'.$JSWarningText.'</font>';
			$x .= '</td>';
			$x .= '</tr>';	
			
       }
		
		$x .= '</tbody>';
		$x .= '</table>';
		$htmlAry['errorTbl'] = $x;
					
	 } 
	 
  	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
	
	if ($errorCount == 0) {
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
	}
		
	$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
intranet_closedb();
?>