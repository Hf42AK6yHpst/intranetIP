<?php
# using:
/*
 * 	
 * 	Date:	2017-09-07 (Anna)
 *          Created yhis page
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$lgeneralsettings = new libgeneralsettings();
$libdb = new libdb();


$UnitRolePoint = $_POST['UnitRolePoint'];
$EnrolGroupID = IntegerSafe(standardizeFormPostValue($_POST['sel_club']));
$roleID = $_POST['roleID'];
$AcademicYearID =IntegerSafe(standardizeFormPostValue($_POST['AcademicYearID']));
$Semester = IntegerSafe(standardizeFormPostValue($_POST['Semester']));
$countOfLoop = count($roleID);

$UnitRolePointResult = $libenroll->Get_Role_Point($EnrolGroupID); 


// IF not exist this EnrolGroupID, insert  
if(empty($UnitRolePointResult)){
	for($i=0;$i<$countOfLoop;$i++){
		$_roleID = $roleID[$i];
		$_RolePoint = $UnitRolePoint[$i];
		
		
		$_RolePoint= ($_RolePoint=='')? 'null' : '\''.$_RolePoint.'\'';
		$InsertResultArr[$i] = "('".$_roleID."','".$EnrolGroupID."', ".$_RolePoint.")";
	}
	$Insert = $libenroll->Insert_Club_Role_Point($InsertResultArr);
}else{
// update already existed 
	for($i=0;$i<$countOfLoop;$i++){
		$_roleID = $roleID[$i];
		$_RolePoint = $UnitRolePoint[$i];
		
		$_RolePoint= ($_RolePoint=='')? 'null' : '\''.$_RolePoint.'\'';
		
		$Update = $libenroll->Update_Role_Point('\''.$_roleID.'\'',$_RolePoint,'\''.$EnrolGroupID.'\'');
	
	}
}



intranet_closedb();
header("Location: index.php?sel_club=$EnrolGroupID&AcademicYearID=$AcademicYearID&Semester=$Semester&xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
?>