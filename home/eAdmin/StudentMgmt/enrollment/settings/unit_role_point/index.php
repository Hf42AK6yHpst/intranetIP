<?php
/************
 * 
 * 
 * @var string $PATH_WRT_ROOT
 */


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$CurrentPage = "PageUnitRolePointSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();




if (!isset($AcademicYearID) || $AcademicYearID=="")
    $AcademicYearID = Get_Current_Academic_Year_ID();

$Semester = isset($Semester) && $Semester != ''? $Semester : 0;

$AcademicYearID = IntegerSafe($AcademicYearID);
$Semester = IntegerSafe($Semester);


$groups = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', stripslashes($keyword), (array)$Semester, $OrderBy, $OrderDesc, $ApplyUserType='', $Round);
$numOfClub = count($groups);
$EnrolGroupIDArray = array();
for ($i=0; $i<$numOfClub; $i++)
{
    $EnrolGroupIDArray[] = $groups[$i]['EnrolGroupID'];
}


// $sel_club = isset($sel_club) && $sel_club != ''? $sel_club : $groups[0]['EnrolGroupID'];

$EnrolGroupID = $EnrolGroupIDArray[0]==''? '':$EnrolGroupIDArray[0];
$sel_club = in_array($sel_club,$EnrolGroupIDArray)? $sel_club: $EnrolGroupID;

//after update 
$sel_club = standardizeFormGetValue($_GET['sel_club']) != null?  standardizeFormGetValue($_GET['sel_club']) : $sel_club;
$AcademicYearID = standardizeFormGetValue($_GET['AcademicYearID']) != null?  standardizeFormGetValue($_GET['AcademicYearID']) : $AcademicYearID;
$Semester = standardizeFormGetValue($_GET['Semester']) != null?  standardizeFormGetValue($_GET['Semester']) : $Semester;

$TAGS_OBJ[] = array($Lang['eEnrolment']['MenuTitle']['Settings']['UnitRolePoint'], "", 1);


# Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.Semester.selectedIndex=0;document.form1.action=\'index.php\';document.form1.submit();"', 1, 0, $AcademicYearID);

# Semester Selection
$SemesterFilter = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, $OnChange="document.form1.action='index.php'; document.form1.submit()", $NoFirst=1, $NoPastTerm=0, $withWholeYear=1);

# Club Selection
$clubList = '<select id="sel_club" name="sel_club" onchange="document.form1.action=\'index.php\'; document.form1.submit()">';
$numOfClub = count($groups);
for ($i=0; $i<$numOfClub; $i++)
{
	$EnrolGroupID = $groups[$i]['EnrolGroupID'];
	$this_TitleDisplay = Get_Lang_Selection($groups[$i]['TitleChinese'],$groups[$i]['Title']);
	
	$selected = "";
	if ($EnrolGroupID == $sel_club) {
		$selected = ' selected ';
	
	}
	$clubList .='<option'. $selected.' value="'.$EnrolGroupID.'">'.$this_TitleDisplay.'</option>';
}
$clubList .='</select>'; 


##Get Role data

// if(!empty($groups)){
//     if($sel_club == null){
//         $sel_club =  $groups[0]['EnrolGroupID'];
//     }
// }else{
//     $sel_club = '';
// }
// /debug_Pr($sel_club);

// DON'T HAVE RECORD BEFORE
$rolePointArr = $libenroll->Get_Role_Point($sel_club);
$rolePointAssoAry = BuildMultiKeyAssoc($rolePointArr, 'RoleID');
	
$roleArr = $libenroll->Get_Role_Info();

$thWidthArr = array(3,47,50); //define the percentages of width
///$tdHeading = array('#',$eEnrollment['role'],$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'],$Lang['eEnrolment']['UnitRolePoint']['Point']);
$tdHeading = array('#',$eEnrollment['role'],$Lang['eEnrolment']['UnitRolePoint']['Point']);


$toolbar = '';
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php?AcademicYearID=$AcademicYearID')", '', '', '', '', 0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php?AcademicYearID=$AcademicYearID')", '', '', '', '', 0);
//$toolbar .= "<a href=\"export.php?sel_club=".$sel_club."\" class=\"btn_export\"> ".$Lang['Btn']['Export']."</a>";

/////<<<<<
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post">'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					//$x .= '<div class="content_top_tool">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .=$toolbar;
							//$x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Category();")."\n";
						$x .= '</div>'."\n";
						
				//	$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
			    $x .= '<td>'."\n";
        			$x .=$yearFilter;
        			$x .=$SemesterFilter;
			        $x .=$clubList;
			    $x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x.='<div class="table_row_tool row_content_tool">';
						$x.=$linterface->GET_SMALL_BTN($button_edit, "button","javascript:editForm();","EditBtn");
					//	$x.=$linterface->GET_SMALL_BTN($button_delete, "button","checkRemove(document.form1,'RoleID[]','role_remove.php');","DeleteBtn");
					$x.='</div>';
					//$x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);			
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$countOfColumn = count($tdHeading);
									for($i=0;$i<$countOfColumn;$i++){
										$x .= '<th style="width:'.$thWidthArr[$i].'%;">'.$tdHeading[$i].'</th>'."\n";
									}
// 						/			$x .='<th class="num_check"><input type="checkbox" onClick="(this.checked)?setChecked(1,document.form1,\'RoleID[]\'):setChecked(0,document.form1,\'RoleID[]\')"></th>';
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";
					
							
							
							$x .= '<tbody>'."\n";
							if($sel_club == ''){
							      $x .= '<tr>'."\n";
							          $x .= '<td colspan ="3"  class="tabletext" text-align="align:center" >'."\n";
                                        $x .=$i_no_record_exists_msg;
							          $x .= '</td>'."\n";
							      $x .= '</tr>'."\n";
							}else{
// 							$x .= '<tbody>'."\n";
								
							
								if (empty($roleArr)) {
									$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else {
									$countOfRow = count($roleArr);
									for($i=0;$i<$countOfRow;$i++){
										$_roleId = $roleArr[$i]['RoleID'];
										$_roleTitle = $roleArr[$i]['Title'];
										$x .= '<tr id="'.$_roleId.'">';
										$x .= '<td>'.($i+1).'</td>';
										$x .= '<td>'.$_roleTitle.'<input type="hidden" name="roleID[]" value="'.$_roleId.'"></td>';
										$_rolePoint = $rolePointAssoAry[$_roleId]['Point'];
										//	$thisPoint[$i]=='0'? $thisPoint[$i]="":$thisPoint[$i];
											$x .= '<td class="UnitRolePoint_text">'.$_rolePoint.'</td>';
											$x .= '<td class="UnitRolePoint_editable" style="display:none;">';
												//$x .= '<input type="text" value="'.$_rolePoint.'" name="UnitRolePoint[]"></input>';
											$x .= $linterface->GET_TEXTBOX_NUMBER('UnitRolePoint_'.$_roleId, 'UnitRolePoint[]', $_rolePoint, $OtherClass='', $OtherPar=array('maxlength'=>2));
											$x .= '</td>';
									$x .= '</tr>';		
									}	
								}
							}	
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .='<div class="edit_bottom_v30">';
	$x .='<p class="spacer"></p>';
		$x .= $linterface->GET_ACTION_BTN($button_update, "button","CheckUpdate();","UpdateBtn", "style='display: none'");
		$x .= $linterface->GET_ACTION_BTN($button_cancel, "button", "CancelEdit();","cancelbtn", "style='display: none'");
	$x .='<p class="spacer"></p>';
	$x .='</div>';
$x .= '</form>'."\n";
$x .= '<br />'."\n";


$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();	
?>

<script language="javascript">
function editForm(){
	$('#EditBtn').hide();
	$('#DeleteBtn').hide();
	$('#UpdateBtn').show();
	$('#cancelbtn').show();
	$('.UnitRolePoint_text').hide();
	$('.UnitRolePoint_editable').show();
	
	$('.num_check').hide();
	
}

function CheckUpdate(){
	var point = document.getElementsByName("UnitRolePoint[]");
	var pointlength = point.length;

	var check;
	check = true;	
	for(var i=0;i<pointlength;i++){
	//	var inputvalue = parseFloat(point[i].value);
			var inputvalue =point[i].value;
		if(inputvalue != parseInt(inputvalue) && inputvalue !== ""){
			check = false;
		}
		if(inputvalue.length >= 3){
			check = false;
		}
		
	}
	
	if(check){
		$("#form1").attr("action", "update.php").submit();

	}else{
		alert("Please input right point(integer less than 100)");
	}
}
function CancelEdit(){
	document.form1.reset();	
	$('#EditBtn').show();
	$('#UpdateBtn').hide();
	$('#cancelbtn').hide();
	$('.UnitRolePoint_text').show();
	$('.UnitRolePoint_editable').hide();
	$('.num_check').show();
	$('#DeleteBtn').show();
	
}
</script>
<?=$x?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>