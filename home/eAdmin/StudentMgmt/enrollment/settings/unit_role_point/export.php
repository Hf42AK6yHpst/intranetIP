<?php

//using: 

#################################
#		Change Log
#	Date:	2017-08-24  Anna
#		- created this page
#################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();	

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$filename = "unit_role_point.csv";


	$exportColumn = array("Club","Role","Point");
	

	$AllEnrolGroupIDAry = $libenroll->Get_Current_AcademicYear_Club_EnrolGroupID($AcademicYearID);
	$ClubInfo = $libenroll->getGroupInfoList($AllEnrolGroupIDAry);
//	$ClubInfo = BuildMultiKeyAssoc($ClubInfo, 'EnrolGroupID');
	######### club in used 
	$ClubIDInUsed = $libenroll->Get_No_Repeat_Enrol_ClubINClubRolePoint();
	$exportDataAry= array();
	for($i=0;$i<sizeof($ClubInfo);$i++){
	
		$_EnrolGroupID = $ClubInfo[$i]['EnrolGroupID'];
 		$_ClubTitle = Get_Lang_Selection($ClubInfo[$i]['TitleChinese'],$ClubInfo[$i]['Title']);
 		
 		if(in_array($_EnrolGroupID,$ClubIDInUsed)){
 			$RoleInfo = $libenroll->Get_Role_Point($_EnrolGroupID);
 			
 			for($j=0;$j<sizeof($RoleInfo);$j++){
 				$__RoleTitle = $RoleInfo[$j]['Title'];
 				$__RolePoint = $RoleInfo[$j]['Point'];
 				$exportDataAry[$i][$j][] = $_ClubTitle;
 				$exportDataAry[$i][$j][] = $__RoleTitle;
 				$exportDataAry[$i][$j][] = $__RolePoint;
 			}
 		
 		}else{
 			$RoleInfo = $libenroll->Get_Role_Info();
 		
 			for($j=0;$j<sizeof($RoleInfo);$j++){
 				$__RoleTitle = $RoleInfo[$j]['Title'];
 				$__RolePoint = '';
 				$exportDataAry[$i][$j][] = $_ClubTitle;
 				$exportDataAry[$i][$j][] = $__RoleTitle;
 				$exportDataAry[$i][$j][] = $__RolePoint;
 			}
		}	
		
		
	//	debug_pr($RoleInfo);
	}
	
	
	$exportData = array();
	$count  = 0;
	for($i=0;$i<sizeof($exportDataAry);$i++){
		for($j=0;$j<sizeof($exportDataAry[$i]);$j++){
			$exportData[$count] = $exportDataAry[$i][$j];
			$count++;
		}
		
	}

$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportColumn, "", "\r\n", "", 0, "11",1);

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);

?>