<?php
/*
 * 	Log:
 * 	
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eEnrolment']['TWGHCYMA']){
	No_Access_Right_Pop_Up();
}

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$CategoryArr['Nature'] = $Nature;
$CategoryArr['DisplayOrder'] = $DisplayOrder;
$CategoryArr['OleCategoryID'] = ($_POST['OleCategoryID']=='')? 0 : $_POST['OleCategoryID'];
//$CategoryArr['CategoryTypeID'] = ($_POST['CategoryTypeID']=='')? 0 : $_POST['CategoryTypeID'];

$successAry = array();
$libenroll->Start_Trans();

if ($NatureID == "") {
	#Get Max DisplayOrder's value and increment it by 1
	$sql = "SELECT MAX(DisplayOrder) FROM INTRANET_ENROL_ACTIVITY_NATURE";
	$ReturnMaxOrderVal = $libenroll->returnVector($sql);
	$CategoryArr['DisplayOrder'] = $ReturnMaxOrderVal[0] + 1;
	
	$successAry['add'] = $libenroll->Add_Activity_Nature($CategoryArr);
} else {
	$CategoryArr['NatureID'] = $NatureID;
	$successAry['edit'] = $libenroll->Edit_Activity_Nature($CategoryArr);
}

if (in_array(false, $successAry)) {
	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: activity_nature.php?ReturnMsgKey=$ReturnMsgKey");
?>