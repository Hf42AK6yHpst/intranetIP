<?php
/*
 * 	Log
 * 	
 *  
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eEnrolment']['TWGHCYMA']){
	No_Access_Right_Pop_Up();
}

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
	$libenroll = new libclubsenrol();	
	$libenroll_ui = new libclubsenrol_ui();
	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
	$CurrentPage = "PageActivityNatureSetting";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID'])) {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($Lang['eEnrolment']['CategoryNature'], "", 1);
        
        $linterface->LAYOUT_START();

		if (is_array($NatureID)) {
			$NatureID = $NatureID[0];
		}
		        
		$CategoryArr = $libenroll->Get_Activity_Nature_Info($NatureID);
		$DisplayOrder = $CategoryArr['DisplayOrder'];
		$OleCategoryID = $CategoryArr['OleCategoryID']; 
		//$CategoryTypeID = $CategoryArr['CategoryTypeID'];
		
		# page navigation (leave the array empty if no need)
		if ($NatureID != "") {
			$PAGE_NAVIGATION[] = array($button_edit." ".$eEnrollment['add_activity']['act_category'], "");
			$button_title = $button_save;
		} else {
			$PAGE_NAVIGATION[] = array($button_new." ".$eEnrollment['add_activity']['act_category'], "");
			$button_title = $button_submit;
		}
		
		if($plugin['iPortfolio']) {
			$htmlAry['oleCategoryMappingRemarksTable'] = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eEnrolment']['OleCategoryRemarks']);
			
			$x = '';
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$Lang['eEnrolment']['OleCategory'].'</td>'."\r\n";
				$x .= '<td>'.$libenroll_ui->Get_Ole_Category_Selection('OleCategoryID_sel', 'OleCategoryID', $OleCategoryID).'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$htmlAry['oleCategoryMappingTr'] = $x;
		}
		/*
		if ($sys_custom['eEnrolment']['CategoryType']) {
			$x = '';
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$Lang['eEnrolment']['CategoryType'].'</td>'."\r\n";
				$x .= '<td>'.$libenroll_ui->Get_Category_Type_Selection('CategoryTypeID_sel', 'CategoryTypeID', $CategoryTypeID).'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$htmlAry['categoryTypeMappingTr'] = $x;		
		}
		*/			

?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.Nature, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) return false;
<?
//if ($sys_custom['eEnrolment']['CategoryType']) {
//	print "if(!check_text(obj.CategoryTypeID, \"". $i_alert_pleasefillin.$Lang['eEnrolment']['CategoryType']. "\")) return false;\n";
//}
?>
	obj.submit();
}
</SCRIPT>
<form name="form1" action="activity_nature_update.php" method="POST">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>
<?=$htmlAry['oleCategoryMappingRemarksTable']?>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $eEnrollmentMenu['cat_title']?></td>
		<td><input type="text" id="Nature" name="Nature" value="<?= htmlspecialchars($CategoryArr[1])?>" class="textboxtext"></td>
	</tr>
	<?=$htmlAry['oleCategoryMappingTr']?>
</table>

</td></tr>

</table>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_title, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='activity_nature.php'")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="NatureID" id="NatureID" value="<?= $NatureID?>" />
<input type="hidden" name="DisplayOrder" id="DisplayOrder" value="<?= $DisplayOrder?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />

</form>
<?= $linterface->FOCUS_ON_LOAD("form1.Nature") ?>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>