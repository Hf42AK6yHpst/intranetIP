<?php

// Using: 

/************************************
 * Date:	2017-09-04	(Anna)
 * Details: added $NoActiveReason
 * 
 * Date:	2015-06-02	(Evan)
 * Details:	Replace the old style with UI standard
 * 
 * 
 * Date: 	2013-05-30 (Rita)
 * Details:	Change $sql to call function
 ************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

if ($plugin['eEnrollment'])
{
	$AcademicYearID = IntegerSafe($AcademicYearID);
	$EnrolGroupID = IntegerSafe($EnrolGroupID);
	$EnrolEventID = IntegerSafe($EnrolEventID);

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol($AcademicYearID);	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->HAVE_CLUB_MGT() && !$libenroll->HAVE_EVENT_MGT()  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
		header("Location: {$PATH_WRT_ROOT}/home/eAdmin/StudentMgmt/enrollment/");
		
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();
        $libenroll_ui = new libclubsenrol_ui();
        
        if ($type=="club")
        {
        	$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
        	$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
        	
        	$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin', $EnrolGroupID);
        	
	    	//$li = new libgroup($GroupID);
	    	$CurrentPage = "PageMgtClub";
			$CurrentPageArr['eEnrolment'] = 1;
        	//$TAGS_OBJ[] = array($li->Title." ".$eEnrollment['active_member'], "", 1);  
        	# tags 
       		$tab_type = "club";
        	# navigation
        	$PAGE_NAVIGATION[] = array($eEnrollment['button']['active_member'], "");        	
        	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();        	
        	
        	$globalActivePer = $libenroll->activeMemberPer;
        	
        	//get active member percentage
        	//$sql = "SELECT ActiveMemberPercentage FROM INTRANET_ENROL_GROUPINFO WHERE GroupID = '$GroupID'";
        	$result = $libenroll->Get_Club_Active_Member_Percentage($EnrolGroupID);
        }
        else if ($type=="activity")
        {
        	$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID);
        	
	        $EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
	        $CurrentPage = "PageMgtActivity";
			$CurrentPageArr['eEnrolment'] = 1;
       		//$TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['active_member'], "", 1);
       		# tags 
	        $tab_type = "activity";
	        # navigation
	        $PAGE_NAVIGATION[] = array($eEnrollment['button']['active_member'], "");
       		$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
       		
       		$globalActivePer = $libenroll->Event_activeMemberPer;
       		
       		//get active member percentage
        	//$sql = "SELECT ActiveMemberPercentage FROM INTRANET_ENROL_EVENTINFO WHERE EnrolEventID = '$EnrolEventID'";
     		 $result = $libenroll->Get_Event_Active_Member_Percentage($EnrolEventID);   
        }       
         
        //get active member percentage        
       // $result = $libenroll->returnArray($sql,1);
        if ($result[0][0]==NULL)
        {
	        //not yet set percentage => use the global one
	        if ($globalActivePer == NULL || $globalActivePer == "")
	        {
		        //set to zero if no default value
		        $activeMemberPer = 0;
	        }
	        else
	        {
		       	$activeMemberPer = $globalActivePer;
        	}
        	
        }
        else
        {
	        //use the group active member percentage
	        $activeMemberPer = $result[0][0];
        }
        
        ## Construct Student Info array
        $name_field = getNameFieldByLang("b.");
		
        if ($type=="club")
        {
	        //get all studentIDs of the group
	        /*
	        $sql = "SELECT b.UserID as StudentID, 
					$name_field as StudentName, 
					b.ClassName, 
					b.ClassNumber, 
					c.Title
	        		FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_USER as b ON a.UserID = b.UserID
	        				LEFT JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
	        		WHERE a.EnrolGroupID = $EnrolGroupID AND b.RecordType = 2 and b.RecordStatus = 1 and b.ClassName Is Not Null and b.ClassNumber Is Not Null
	        		ORDER BY b.ClassName, b.ClassNumber, ".$name_field;
	        $StudentArray = $libenroll->returnArray($sql,5);
	        */
	        $StudentArray = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID, $WithIndicator=1, $IndicatorWithStyle=1);
        }
        else if ($type=="activity")
        {
	        /*
	        $sql = "SELECT a.StudentID, ".$name_field." as StudentName, b.ClassName, b.ClassNumber, a.RoleTitle
	        		FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	        		WHERE a.EnrolEventID = $EnrolEventID AND b.RecordType = 2
	        		ORDER BY b.ClassName, b.ClassNumber, ".$name_field;
	        $StudentArray = $libenroll->returnArray($sql,5);
	        */
	        $ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventID, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1);
	        $StudentArray = (array)$ActivityInfoArr[$EnrolEventID]['StatusStudentArr'][2];
        }
        
        
        $current_tab = 1;
    	include_once("management_tabs.php");
        	        
        //get name and attendance for each student
        $numOfStudent = count($StudentArray);
        for ($i=0; $i<$numOfStudent; $i++)
        {
	        	        
	   		//get attendance
	   		$DataArr['StudentID'] = $StudentArray[$i]['StudentID'];
        	$DataArr['EnrolGroupID'] = $EnrolGroupID;
        	$DataArr['EnrolEventID'] = $EnrolEventID;
        	
        	if ($type=="club")
       		{
        		$StudentArray[$i]['Attendance'] = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
    		}
    		else if ($type=="activity")
        	{
	        	$StudentArray[$i]['Attendance'] = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);
        	}
	        
        	//check if student is set to be a active member already
        	//if not, set according to the active member percentage
        	if ($type=="club")
       		{
        		//$sql = "SELECT isActiveMember FROM INTRANET_USERGROUP WHERE GroupID = $GroupID AND UserID = '".$StudentArray[$i]['StudentID']."'";
    			$result = $libenroll->Get_Club_Active_Member_Status($EnrolGroupID, $StudentArray[$i]['StudentID']);	 
    		}
        	else if ($type=="activity")
       		{
        		//$sql = "SELECT isActiveMember FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '".$StudentArray[$i]['StudentID']."'";
    			$result = $libenroll->Get_Event_Active_Member_Status($EnrolEventID, $StudentArray[$i]['StudentID']);
	    	
    		}
        	//$result = $libenroll->returnArray($sql,1);
        	if ($result[0][0]==NULL)
        	{
	        	//no setting of active member => according to active member percentage
	        	if ($StudentArray[$i]['Attendance'] >= $activeMemberPer)
	        	{
		        	$StudentArray[$i]['isActiveMember'] = 1;
	        	}
	        	else
	        	{
		        	$StudentArray[$i]['isActiveMember'] = 0;
	        	}
        	}
        	else
        	{
	        	//set according to the value of isActiveMember in the database
	        	$StudentArray[$i]['isActiveMember'] = $result[0][0];
        	}
        }
        
             
        ## Construct the table
        //construct title
        $display = "<table class=\"tablegreentop\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\">";
			$display .= "<tr>";
				$display .= "	<td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$Lang['SysMgr']['FormClassMapping']['Class']."</td>
								<td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$Lang['SysMgr']['FormClassMapping']['StudentClassNumber']."</td>
								<td class=\"tablegreentop tabletopnolink\">".$eEnrollment['student_name']."</td>
								<td class=\"tablegreentop tabletopnolink\">".$i_admintitle_role."</td>
						  		<td align=\"center\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['attendence']."</td>
				          		<td width=\"15%\" align=\"center\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['active']."</td>
						  		<td width=\"15%\" align=\"center\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['inactive']."</td>";
	   		$display .= "</tr>";
	    
	    
	    
	    $total_col = 7;
	   	$StudentSize = sizeof($StudentArray);
	    
	    //construct content
	    $display .= ($StudentSize==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
        	    
	    for ($i=0; $i<$StudentSize; $i++)
	    {
		 	$tr_css = ($i % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
		 	
		 	$radioName = "radioID[".$i."]";
		 	$radioID_isActive = "isActiveRadioID".$i;
			$radioID_notActive = "notActiveRadioID".$i;
			$radioValue_isActive = 1; 
			$radioValue_notActive = 0;   
			
			$radioText = "radioText[".$i."]";
			$radio_noActive_input = "noActiveInput".$i;
			
			$display .= '<tr class="'.$tr_css.'">';
			$display .= '<td class="tabletext" valign="top">'.$StudentArray[$i]['ClassName'].'</td>
						 <td class="tabletext" valign="top">'.$StudentArray[$i]['ClassNumber'].'</td>
						 <td class="tabletext" valign="top">'.$StudentArray[$i]['StudentName'].'</td>
						 <td class="tabletext" valign="top">'.$StudentArray[$i]['RoleTitle'].'</td>
		          	  	 <td class="tabletext" align="center" valign="top">'.$StudentArray[$i]['Attendance'].'%</td>
		          	 	 <td class="tabletext" align="center" valign="top">';
		          
		    if ($StudentArray[$i]['isActiveMember'])
		    {
			    $isActive = "checked";
			    $notActive = "";
		    }
		    else
		    {
			    $isActive = "";
			    $notActive = "checked";
		    }
		    
		    
		    $display .= "<input type=\"radio\" name=\"$radioName\" id=\"$radioID_isActive\" value=\"$radioValue_isActive\" onclick=\"changeActiveStatus($i);\" $isActive ></input>";  
		    $display .= '</td>';
		    
		    $display .= '<td class="tabletext" align="center" valign="top">';
		    $display .= '<table>';
		    	$display .= '<tr>';
		    		$display .= '<td>';
					    $display .= "<input type=\"radio\" name=\"$radioName\" id=\"$radioID_notActive\" value=\"$radioValue_notActive\"   onclick=\"changeActiveStatus($i);\" $notActive ></input>";
					 $display .= '</td>';
					 if($sys_custom['eEnrolment']['UnitRolePoint'] == true){
					 	
					 	$NoActiveReason = $StudentArray[$i]['UnActiveReason'];
						 $display .= '<td>';
						    $display .= "<div id=\"$radio_noActive_input\"><input type=\"text\" name=\"$radioText\" value=\"$NoActiveReason\"></input></div>";
						$display .= '</td>';
					 }
				$display .= '</tr>';
			$display .= '</table>';
			$display .= '</td></tr>';
		        
	    }
	    
	    
	    $display .= '</table>';
	    
	    $functionbar = $linterface->GET_SMALL_BTN($button_update, "button", "javascript:checkForm(document.form1,'active_member_update.php',1)");
	    
	    if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");

	    $linterface->LAYOUT_START();

?>

		<script language="javascript">
		$(document).ready( function() {
			var StudentSize = <?=$StudentSize?>;
			for(var i=0;i<StudentSize;i++){
				var active =document.getElementById("isActiveRadioID"+i);
				if(active.checked == true){
					$("#noActiveInput"+i).hide();
				}			
			}
		
		});

		function changeActiveStatus(id){
			var active =document.getElementById("isActiveRadioID"+id);
			if(active.checked == true){
				$("#noActiveInput"+id).hide();
			}else{
				$("#noActiveInput"+id).show();
			}
			
			
		}
		<!--
		function checkForm(obj,page,isUpdatePer){
			if (isNaN(parseInt(obj.activeMemberPer.value)) || parseInt(obj.activeMemberPer.value) < 0 || parseInt(obj.activeMemberPer.value) > 100)
			{
				alert("<?=$eEnrollment['active_member_alert']?>");
				return false;
			}
			else
			{
				if (!IsNumeric(obj.activeMemberPer.value))
				{
					alert("<?=$eEnrollment['active_member_alert']?>");
					return false;
				}
				else
				{
					objActiveMemberPer = document.getElementById("isUpdatePercentage");
					objActiveMemberPer.value = isUpdatePer;
					obj.action=page;
					
					//if update percentage, warn the user that the active member status will be changed according to the percentage
					if (isUpdatePer == 1)
					{
						if(confirm("<?=$eEnrollment['js_update_active_member_per']?>"))
						{
							obj.submit();
						}
						else
						{
							return false;	
						}
					}
					else
					{
				    	obj.submit();
					}
				}
			}
		}
		//-->
		</script>
		
		<br />
	
		<form name="form1" method="POST">
			<table width="100%" border="0" cellspacing="4" cellpadding="4">
			<tr><td>
				<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
			</td></tr>
			</table>
			
			<table class="form_table_v30">
				<tr>
					<td class="field_title"><?=$eEnrollment['active_member_per']?></td>
					<td valign="top" class="tabletext">
						<input class="textboxnum" type="text" name="activeMemberPer" maxlength="6" value="<?=$activeMemberPer?>"> %
						<? if($libenroll->AllowToEditPreviousYearData) {?>
						<?= toolBarSpacer() ?>
						<?=$functionbar?>
						<?}?>
					</td>
					<td align="right">
						<?= $SysMsg ?>
					</td>
					
				</tr>
				<br />
			</table>
			
			<br />
			
			<?= $display ?>
			
			<?= $libenroll_ui->Get_Student_Role_And_Status_Remarks() ?>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr><td align="center" colspan="6">
					<? if($libenroll->AllowToEditPreviousYearData) {?>
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checkForm(document.form1,'active_member_update.php',0)")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
					<?}?>
						<? if ($type=="club") { ?>
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID'")?>&nbsp;
						<? } else if ($type=="activity") { ?>
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID'")?>&nbsp;
						<? } ?>
						
				</td></tr>
			</table>
			
			<?
			for ($i=0; $i<sizeof($StudentArray); $i++){
			?>
				<input type="hidden" name="UID<?=$i?>" id="UID<?=$i?>" value="<?=$StudentArray[$i]['StudentID']?>">
			<?
			}	
			?>
			<input type="hidden" name="GroupID" value="<?= $GroupID ?>">
			<input type="hidden" name="EnrolGroupID" value="<?= $EnrolGroupID ?>">
			<input type="hidden" name="EnrolEventID" value="<?= $EnrolEventID ?>">
			<input type="hidden" name="type" value="<?=$type?>">
			<input type="hidden" id="isUpdatePercentage" name="isUpdatePercentage">
			<input type="hidden" name="StudentSize" value="<?= $StudentSize ?>">
			<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?= $AcademicYearID ?>">
		</form>
		<br />
<?
		echo $linterface->FOCUS_ON_LOAD("form1.activeMemberPer");
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>