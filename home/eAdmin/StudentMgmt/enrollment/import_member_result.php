<?
// using: 
#########################################################
# Modification Log:
#
#   Date:   2020-02-25 (Tommy)
#           add year term import result  (Case #Z178470)
#
#   Date    2018-09-28 (Anna)
#           Added enrollment carshClub/clashEvent push mesaage
#
#	Date	2015-04-08 (Omas)
#			added field userlogin to import (will not show in result page)
#
#	Date:	2014-12-02	(Omas) ---- (cancelled on 2015-01-02)
#			For $oneEnrol != 1 ,change font color when have time crash to orange
#			For $oneEnrol == 1 , font color keep as red
#
########################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

$lu = new libuser();
$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$linterface = new interface_html();
$lstudentprofile = new libstudentprofile();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

if (substr($type, 0, 4) == "club")
{
	$CurrentPage = "PageMgtClub";
	$tab_type = "club";
	$current_tab = 1;
	
	$PAGE_NAVIGATION[] = array($eEnrollment['import_member'], "");
	
    $oneEnrol = $libenroll->oneEnrol;
}
else if (substr($type, 0, 8)=="activity")
{
	$CurrentPage = "PageMgtActivity";
	$tab_type = "activity";
	$current_tab = 1;
	
	$PAGE_NAVIGATION[] = array($eEnrollment['import_participant'], "");
	
    $oneEnrol = $libenroll->Event_oneEnrol;
}		
include_once("management_tabs.php");
	

$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();


## intiailization
$Status = $_POST["Status"];

$importStatusArr = unserialize(rawurldecode($importStatusArr));
$importSuccessInfoArr = unserialize(rawurldecode($importSuccessInfoArr));
$importFailedInfoArr = unserialize(rawurldecode($importFailedInfoArr));
$crash_EnrolGroupID_ary = unserialize(rawurldecode($crash_EnrolGroupID_ary));
$crash_EnrolEventID_ary = unserialize(rawurldecode($crash_EnrolEventID_ary));

$successStatus = $_POST["successStatus"];
$failedStatus = $_POST["failedStatus"];
$crashedStudentArr= array();
$crashedStudentInfoArr = array();

if ($type=="clubAllMember" || $type=="activityAllParticipant")
{
	$isImportAll = 1;
}
else
{
	$isImportAll = 0;
}

if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
    //get school year term
    $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
    
    $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
    $termAry = $libenroll->returnResultSet($sqlTemp);
    $numOfTerm = count($termAry);
}

### List out the import result
$resultTable = $linterface->GET_NAVIGATION2($eEnrollment['lookup_for_clashes']);
$resultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$resultTable .= "<tr><td class=\"tabletext\">$list_total : ". count($importStatusArr) .", {$eEnrollment['succeeded']} : ". count($importSuccessInfoArr) .", ".$eEnrollment['failed'].": ". (count($importStatusArr) - count($importSuccessInfoArr)) ."</td></tr>\n";	
$resultTable .= "</table>\n";

# Result Table Title
$resultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$resultTable .= "<tr>\n";
$resultTable .= "<td width=\"10\" class=\"tablebluetop tabletopnolink\">#</td>\n";
if ($type == "clubAllMember")
{
	$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">". $eEnrollment['club_name'] ."</td>\n";
	$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\">". $Lang['General']['Term'] ."</td>\n";
}
else if ($type == "activityAllParticipant")
{
	$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">". $eEnrollment['act_name'] ."</td>\n";
}
$resultTable .= "<td width=\"4%\" class=\"tablebluetop tabletopnolink\">$i_general_class</td>\n";
$resultTable .= "<td width=\"4%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['class_no']}</td>\n";
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\">$i_UserEnglishName</td>\n";
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\">$i_UserChineseName</td>\n";
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['role']}</td>\n";
if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
    for ($i=0; $i<$numOfTerm; $i++) {
        $termName = $termAry[$i]["TermName"]." ".$eEnrollment['performance'];
        $resultTable .= "<td width=\"3%\" class=\"tablebluetop tabletopnolink\" > ".$termName."</td>\n";
    }
}
$resultTable .= "<td width=\"9%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['performance']}</td>\n";
$resultTable .= "<td width=\"9%\" class=\"tablebluetop tabletopnolink\" >{$Lang['eEnrolment']['Achievement']}</td>\n";
$resultTable .= "<td width=\"9%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['comment']}</td>\n";
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\" >".$eEnrollment['active']."/".$eEnrollment['inactive']."</td>\n";
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['AdminRole']}</td>\n";

if ($libenroll->enableClubRecordMeritInfoRight()){
	if($type == "clubAllMember" || $type == "clubMember"){
		//2014-0519-1338-56177
//		$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$Lang['eDiscipline']['sdbnsm_award']}</td>\n";
//		$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_MinorCredit_unicode}</td>\n";
//		$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_MajorCredit_unicode}</td>\n";
//		$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_SuperCredit_unicode}</td>\n";
//		$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_UltraCredit_unicode}</td>\n";
		
		if (!$lstudentprofile->is_merit_disabled) {
			$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_Merit}</td>\n";
		}
		if (!$lstudentprofile->is_min_merit_disabled) {
			$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_MinorCredit}</td>\n";
		}
		if (!$lstudentprofile->is_maj_merit_disabled) {
			$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_MajorCredit}</td>\n";
		}
		if (!$lstudentprofile->is_sup_merit_disabled) {
			$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_SuperCredit}</td>\n";
		}
		if (!$lstudentprofile->is_ult_merit_disabled) {	
			$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$i_Merit_UltraCredit}</td>\n";
		}
	}
}

if($libenroll->enableActivityFillInEnrolReasonRight() && $type == "activityAllParticipant"){
	$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$Lang['eEnrolment']['Reason'] }</td>\n";
}
	
$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">".$eEnrollment['import_status']."</td>\n";
$resultTable .= "</tr>\n";

# Time clash table title
if ((count($crash_EnrolGroupID_ary) > 0) || (count($crash_EnrolEventID_ary) > 0))
{
	$TableActionArr = array();
	$TableActionArr[] = array("javascript:submitForm(document.form1,'import_member_update.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_approve.gif", "imgApprove", $button_approve);
	
	//Time Crash Table Title
	$TimeCrashTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
	$TimeCrashTable .= "<tr><td align=\"left\">\n";
	$TimeCrashTable .= $linterface->GET_NAVIGATION2($eEnrollment['time_conflict']);
	$TimeCrashTable .= "</td>\n";
	$TimeCrashTable .= "<td align=\"right\">\n";
	$TimeCrashTable .= $libenroll->GET_TABLE_ACTION_BTN($TableActionArr);
	$TimeCrashTable .= "</td></tr></table>\n";
	
	$TimeCrashTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
	$TimeCrashTable .= "<tr>\n";
	$TimeCrashTable .= "<td width=\"10\" class=\"tabletop tabletopnolink\">#</td>\n";
	$TimeCrashTable .= "<td width=\"10%\" class=\"tabletop tabletopnolink\">$i_ClassName</td>\n";
	$TimeCrashTable .= "<td width=\"10%\" class=\"tabletop tabletopnolink\">$i_ClassNumber</td>\n";
	$TimeCrashTable .= "<td width=\"15%\" class=\"tabletop tabletopnolink\">$i_UserEnglishName</td>\n";
	$TimeCrashTable .= "<td width=\"15%\" class=\"tabletop tabletopnolink\">$i_UserChineseName</td>\n";
	$TimeCrashTable .= "<td class=\"tabletop tabletopnolink\">{$eEnrollment['club_or_act_title']}</td>\n";
	$TimeCrashTable .= "<td width=\"5%\" class=\"tabletop tabletopnolink\" align=\"center\">\n";
	if($oneEnrol == 1)
	{
		$TimeCrashTable .= "<input type=\"checkbox\" DISABLED>\n";
	}
	else
	{
		$TimeCrashTable .= "<input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,document.form1,'clash_studentID[]'):setChecked(0,document.form1,'clash_studentID[]')\">\n";
	}
	$TimeCrashTable .= "</td>\n";
	$TimeCrashTable .= "</tr>\n";
}
        

$counterTimeCrashTable = 0;
$numOfData = count($importStatusArr);

for($i=0; $i<$numOfData; $i++)
{
	$isImportSuccess = ($importStatusArr[$i]==$successStatus)? 1 : 0;
	
	if ($isImportSuccess)
	{
		$thisInfoArr = $importSuccessInfoArr[$i];
		
		
		# Get student club / activity info
		if (substr($type, 0, 4) == "club")
		{
			$thisStudentID = $thisInfoArr["StudentID"];
			$thisEventID = $thisInfoArr["EventID"];
			$thisRoleID = $thisInfoArr["RoleID"];
			$thisImportStatus = $thisInfoArr["Status"];
			$thisAdminRole = $thisInfoArr["AdminRole"];
			
			$thisYearTermID = $thisInfoArr["Semester"];
			if ($thisYearTermID=='' || $thisYearTermID==0) {
				$thisSemesterName = $eEnrollment['YearBased'];
			}
			else {
				$thisObjTerm = new academic_year_term($thisSemester);
				$thisSemesterName = $thisObjTerm->Get_Year_Term_Name();
			}
			
			$thisRoleTitle = $libenroll->Get_RoleTitle_From_RoleID($thisRoleID);
			$thisRoleTitle = ($thisRoleTitle==-1)? '&nbsp;' : $thisRoleTitle;
			$thisStudentEventInfoArr = $libenroll->Get_Student_Club_Info($thisStudentID, $thisEventID);
			$termBasedPerformanceAssoAry = BuildMultiKeyAssoc($libenroll->getClubTermBasedStudentPerformance($thisEventID, $thisStudentID), array('StudentID','YearTermID'));
		}
		else
		{
			$thisStudentID = $thisInfoArr["StudentID"];
			$thisEventID = $thisInfoArr["EventID"];
			$thisRoleTitle = $thisInfoArr["RoleTitle"];
			$thisImportStatus = $thisInfoArr["Status"];
			$thisAdminRole = $thisInfoArr["AdminRole"];
			
			$thisStudentEventInfoArr = $libenroll->Get_Student_Activity_Info($thisStudentID, $thisEventID);
		}
		
		if ($libenroll->enableClubRecordMeritInfoRight()){
			
			if($type == "clubAllMember" || $type == "clubMember"){		
				$thisMeritCount = $thisInfoArr["meritCount"];
				$thisMinorMeritCount = $thisInfoArr["minorMeritCount"];
				$thisMajorMeritCount = $thisInfoArr["majorMeritCount"];
				$thisSuperMeritCount = $thisInfoArr["superMeritCount"];
				$thisUltraMeritCount = $thisInfoArr["ultraMeritCount"];	
			}
		}
		
		# Get student info
		$libuser = new libuser($thisStudentID);
		$sql = "SELECT ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." as ClassName, ycu.ClassNumber FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID='$AcademicYearID' AND ycu.UserID='$thisStudentID')";
		$temp = $libenroll->returnArray($sql);
		
		# Consolidate Info
		$thisEventTitle = $thisInfoArr["EventTitle"];
		$thisClassName = $temp[0]['ClassName'];
		$thisClassNumber = $temp[0]['ClassNumber'];
		$thisEnglishName = $libuser->EnglishName;
		$thisChineseName = $libuser->ChineseName;
		$thisRoleTitle = $thisRoleTitle;
		$thisPerformance = $thisStudentEventInfoArr[0]["Performance"];
		if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
		    for ($term = 0; $term < sizeof($termBasedPerformanceAssoAry[$thisStudentID]); $term++){
		        ${"thisPerformance".($term+1)} = $termBasedPerformanceAssoAry[$thisStudentID][$termAry[$term]['YearTermID']]["Performance"];
		    }
		}
		$thisAchievement = $thisStudentEventInfoArr[0]["Achievement"];
		$thisComment = $thisStudentEventInfoArr[0]["CommentStudent"];
		if ($thisStudentEventInfoArr[0]["isActiveMember"] == "1")
		{
			$thisActiveMemberStatus = $eEnrollment['active'];
		}
		else if ($thisStudentEventInfoArr[0]["isActiveMember"] == "0")
		{
			$thisActiveMemberStatus = $eEnrollment['inactive'];
		}
		else
		{
			$thisActiveMemberStatus = "";
		}
		$thisImportStatusDisplay = $eEnrollment[$thisImportStatus];
		
		# CSS of result table
		$fontColorStart = "";
		$fontColorEnd = "";
	}
	else //Import not Success
	{
		$thisInfoArr = $importFailedInfoArr[$i];
		$thisImportStatus = $thisInfoArr["Status"];
		
		# Consolidate Info
		$thisEventTitle = $thisInfoArr["EventTitle"];
		$thisSemesterName = $thisInfoArr["Semester"];
		$thisClassName = $thisInfoArr["ClassName"];
		$thisClassNumber = $thisInfoArr["ClassNumber"];
		$thisAdminRole = $thisInfoArr["AdminRole"];
		
		if ($thisImportStatus != "student_not_found")
		{
			$thisStudentInfo = $libenroll->Get_StudentInfo_By_ClassName_ClassNumber($thisClassName, $thisClassNumber, $AcademicYearID);
			$thisStudentID = $thisStudentInfo[0]["UserID"];
			$thisEnglishName = $thisStudentInfo[0]["EnglishName"];
			$thisChineseName = $thisStudentInfo[0]["ChineseName"];
		}
		else
		{
			$thisEnglishName = "";
			$thisChineseName = "";
		}
		$thisRoleTitle = $thisInfoArr["RoleTitle"];
		$thisPerformance = $thisInfoArr["Performance"];
		if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
		    for ($term=1; $term<=$numOfTerm; $term++) {
		        ${"thisPerformance".$term} = $thisInfoArr["Term".$term."Performance"];
		    }
		}
		$thisAchievement = $thisInfoArr["Achievement"];
		$thisComment = $thisInfoArr["Comment"];
		$thisActiveMemberStatus = $thisInfoArr["ActiveMemberStatus"];
		
		if($libenroll->enableActivityFillInEnrolReasonRight() && $type == "activityAllParticipant"){
			$thisEnrolReason =  $thisInfoArr["Reason"];
		}
		
		$thisImportStatusDisplay = $eEnrollment[$thisImportStatus];
		
		
		# CSS of result table
		if ($oneEnrol){
			$fontColorStart = "<span class=\"tabletextrequire\">";
		}
		else{
			//$fontColorStart = "<span class=\"tabletextrequire3\">";
			$fontColorStart = "<span class=\"tabletextrequire\">";
		}
		$fontColorEnd = "</span>";
	}
	
	
	
	if ($libenroll->enableClubRecordMeritInfoRight()){
		
		if($type == "clubAllMember" || $type == "clubMember"){		
			$thisMeritCount = $thisInfoArr["meritCount"];
			$thisMinorMeritCount = $thisInfoArr["minorMeritCount"];
			$thisMajorMeritCount = $thisInfoArr["majorMeritCount"];
			$thisSuperMeritCount = $thisInfoArr["superMeritCount"];
			$thisUltraMeritCount = $thisInfoArr["ultraMeritCount"];	
		}
	}
	
	
	# Build result table
	$css = "tabletext";
	
	$resultTable .= "<tr class=\"tablebluerow".(($i % 2) + 1)."\">\n";
		$resultTable .= "<td class=\"$css\">".($i+1)."</td>\n";
		
		# display club / activity name if from import of all clubs / activities
		if ($isImportAll)
		{
			//$thisTitle = stripslashes($ImportTitleArr[$i]);
			$resultTable .= "<td class=\"$css\">". $thisEventTitle ."</td>\n";
			
			if ($type=="clubAllMember")
				$resultTable .= "<td class=\"$css\">". $thisSemesterName ."</td>\n";
		}
		
		$resultTable .= "<td class=\"$css\">". $thisClassName ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $thisClassNumber ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $thisEnglishName ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $thisChineseName ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $thisRoleTitle ."</td>\n";
		if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
		    for ($term=0; $term<$numOfTerm; $term++) {
		        $resultTable .= "<td class=\"$css\">". ${"thisPerformance".($term+1)} ."</td>\n";
		    }
		}
		$resultTable .= "<td class=\"$css\">". $thisPerformance ."</td>\n";
		$resultTable .= "<td class=\"$css\">". nl2br($thisAchievement) ."</td>\n";
		$resultTable .= "<td class=\"$css\">". nl2br($thisComment) ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $thisActiveMemberStatus ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $thisAdminRole ."</td>\n";
		
		if($libenroll->enableActivityFillInEnrolReasonRight() && $type=="activityAllParticipant"){	
			$resultTable .= "<td class=\"$css\">". $thisEnrolReason ."</td>\n";		
		}
			
	
		if ($libenroll->enableClubRecordMeritInfoRight()){
			if($type == "clubAllMember" || $type == "clubMember"){
				//2014-0519-1338-56177
//				$resultTable .= "<td class=\"$css\">". $thisMeritCount ."</td>\n";
//				$resultTable .= "<td class=\"$css\">". $thisMinorMeritCount ."</td>\n";
//				$resultTable .= "<td class=\"$css\">". $thisMajorMeritCount ."</td>\n";
//				$resultTable .= "<td class=\"$css\">". $thisSuperMeritCount ."</td>\n";
//				$resultTable .= "<td class=\"$css\">". $thisUltraMeritCount ."</td>\n";
				
				if (!$lstudentprofile->is_merit_disabled) {
					$resultTable .= "<td class=\"$css\">". $thisMeritCount ."</td>\n";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$resultTable .= "<td class=\"$css\">". $thisMinorMeritCount ."</td>\n";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$resultTable .= "<td class=\"$css\">". $thisMajorMeritCount ."</td>\n";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$resultTable .= "<td class=\"$css\">". $thisSuperMeritCount ."</td>\n";
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {	
					$resultTable .= "<td class=\"$css\">". $thisUltraMeritCount ."</td>\n";
				}		
			}
		}
	
		
		$resultTable .= "<td class=\"$css\">".$fontColorStart.$thisImportStatusDisplay.$fontColorEnd."</td>\n";
	$resultTable .= "</tr>\n";
	
	
	# Build Time clash table
	if ($thisImportStatus == "crash_group" || $thisImportStatus == "crash_activity" || $thisImportStatus == "crash_group_activity")
	{
		# Record the students so that the student can be shown after approval by teacher
		$crashedStudentInfoArr[$thisStudentID] = $importFailedInfoArr[$i];
		
		$crashedStudentArr[] = $thisStudentID;
		
		//construct time crash table content
		$TimeCrashTable .= "<tr class=\"tablerow".($counterTimeCrashTable % 2 + 1)."\">\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".($counterTimeCrashTable+1)."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$thisClassName."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$thisClassNumber."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$thisEnglishName."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$thisChineseName."</td>\n";
		
		$TimeCrashTable .= "<td valign=\"top\" class=\"$css\">".$fontColorStart."\n";
		
		//get time crash CLUBS date and time
		$numOfClashedClub = count($crash_EnrolGroupID_ary[$i]);
		for ($j=0; $j<$numOfClashedClub; $j++)
		{
			$thisEnrolGroupID = $crash_EnrolGroupID_ary[$i][$j];
			$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
			
			//get title
			$GroupInfo = $libenroll->getGroupInfo($thisGroupID);
			$TimeCrashTable .= $eEnrollment['crash_with']." ".$GroupInfo[0]['Title']."<br />\n";
			
			//get dates
			$DateArr = $libenroll->GET_ENROL_GROUP_DATE($thisEnrolGroupID);	//GroupDateID, EnrolGroupID, ActivityDateStart, ActivityDateEnd
			// construct a date array
			for ($k = 0; $k < count($DateArr); $k++) {
				$DateTitle = date("Y-m-d", strtotime($DateArr[$k][2]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][2]));
				$DateTitle .= " ".$eEnrollment['to']." ";
				$DateTitle .= date("Y-m-d", strtotime($DateArr[$k][3]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][3]));
				$TimeCrashTable .= $DateTitle."<br />\n";
				if ($k==count($DateArr)-1) $TimeCrashTable .= "<br />\n";
			}
		}
		
		//get time crash ACTIVITIES date and time
		$numOfClashActivity = count($crash_EnrolEventID_ary[$i]);
		for ($j=0; $j<$numOfClashActivity; $j++)
		{
			$thisEnrolEventID = $crash_EnrolEventID_ary[$i][$j];
			
			$ActTitle = $eEnrollment['crash_with']." ".$libenroll->GET_EVENT_TITLE($thisEnrolEventID);
			$TimeCrashTable .= $ActTitle."<br />\n";
			
			//get time crash activities date and time
			$DateArr = $libenroll->GET_ENROL_EVENT_DATE($thisEnrolEventID);
			// construct a date array
			for ($k = 0; $k < count($DateArr); $k++) {
				$DateTitle = date("Y-m-d", strtotime($DateArr[$k][2]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][2]));
				$DateTitle .= " ".$eEnrollment['to']." ";
				$DateTitle .= date("Y-m-d", strtotime($DateArr[$k][3]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][3]));
				$TimeCrashTable .= $DateTitle."<br />\n";
				if ($k==count($DateArr)-1) $TimeCrashTable .= "<br />\n";
			}
		}
		$TimeCrashTable .= $fontColorEnd."</td>\n";
		
		if($oneEnrol == 1)
		{
			$TimeCrashTable .= "<td valign=\"top\" class=\"$css\" align=\"center\"><input DISABLED type=\"checkbox\" name=\"clash_studentID[]\" value=\"".$thisStudentID."\"></td>\n";
		}
		else
		{
			$TimeCrashTable .= "<td valign=\"top\" class=\"$css\" align=\"center\"><input type=\"checkbox\" name=\"clash_studentID[]\" value=\"".$thisStudentID."\"></td>\n";
		}
		
		$TimeCrashTable .= "</tr>\n";
		$counterTimeCrashTable++;
	}
}  
        

$resultTable .= "</table>\n";

if ((count($crash_EnrolGroupID_ary) > 0) || (count($crash_EnrolEventID_ary) > 0))
{
	$TimeCrashTable .= "</table>\n";
}

//buttons at the bottom
$buttons = "";
if (substr($type, 0, 4) == "club")
{
	if ($isImportAll)
	{
		$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_club_management'], "button", "window.location = 'group.php?AcademicYearID=$AcademicYearID&Semester=$PageSemester'")."&nbsp;";
	}
	else
	{
		$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_member_list'], "button", "window.location = 'member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
	}
}
else
{
	if ($isImportAll)
	{
		$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_activity_management'], "button", "window.location = 'event.php?AcademicYearID=$AcademicYearID'")."&nbsp;";
	}
	else
	{
		$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_student_list'], "button", "window.location = 'event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
	}
}

?>

<script language="javascript">
function submitForm(obj,page)
{
	if(countChecked(obj,'clash_studentID[]')==0)
		alert(globalAlertMsg2);
	else
	{
		if(confirm(globalAlertMsg4)){
			obj.action=page;
			obj.method="POST";
			obj.submit();
			return;
		}
	}
}


</script>

<br />
<form name="form1" method="post" action="add_member_result.php">
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
	<td align="center">
    	<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                	<td colspan="2"><?=$resultTable?></td>
				</tr>
				
				<tr>
                	<td colspan="2">&nbsp;</td>
				</tr>
				
				<tr>
                	<td colspan="2"><?=$TimeCrashTable?></td>
				</tr>
				
				</table>
			</td>
		</tr>
		</table>
    </td>
</tr>
<tr>
	<td>        
    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        	<tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center" colspan="2">
				<?= $buttons ?>
				</td>
			</tr>
	    </table>                                
	</td>
</tr>
</table>
<br />
		
		<input type="hidden" name="isFromResultPage" value="<?=1?>">
		<input type="hidden" name="type" value="<?=$type?>">
		<input type="hidden" name="GroupID" value="<?=$GroupID?>">
		<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="crashedStudentInfoArr" value="<?=rawurlencode(serialize($crashedStudentInfoArr))?>">	
		<input type="hidden" name="crashedEventIDAry" value="<?=rawurlencode(serialize($crash_EnrolEventID_ary));?>">
		
		<input type="hidden" name="crashedClubEnrolGroupIDArr" value="<?=rawurlencode(serialize($crash_EnrolGroupID_ary));?>">
		<input type="hidden" name="crashedStudentArr" value="<?=rawurlencode(serialize($crashedStudentArr));?>">
		
		<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>"/>
		<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
		<input type="hidden" name="Status" value="<?=$Status?>">
		
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
