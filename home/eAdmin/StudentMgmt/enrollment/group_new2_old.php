<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();	
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);    
//(isset($_POST['column2add'])) ? $column2add = $_POST['column2add'] : $column2add = 1;
if (isset($_POST['column2add'])) {
	$column2add = $_POST['column2add'];
} else if (sizeof($DateArr) > 0) {
	$column2add = sizeof($DateArr) + 1;
} else {
	$column2add = 1;
}

if (!isset($_POST['tableDates'])) $tableDates = array();

$LibUser = new libuser($UserID);

if (!isset($thisMonth)) {
	$thisMonth = date('m');
}

if ($plugin['eEnrollment'])
{
	
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
        include_once($PATH_WRT_ROOT."includes/libgroup.php");
		
    	$libgroup = new libgroup($GroupID);
        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['add_act_event'], "", 1);
//         # tags 
// 	    $tab_type = "club";
// 	    $current_tab = 1;
// 	    include_once("management_tabs.php");
	    # navigation
        $PAGE_NAVIGATION[] = array($libgroup->Title." ".$i_menu_setting, "");

        $STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_1'], 0);
        $STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_1b'], 0);
		$STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_2'], 1);
        		
        $linterface->LAYOUT_START();
        
    
//debug_r($DateArr);

for ($i = 0; $i < sizeof($DateArr); $i++) {	
	if (!in_array(date("Y-m-d", strtotime($DateArr[$i][2])), $tableDates)) {
		$tableDates[] = date("Y-m-d", strtotime($DateArr[$i][2]));
		$StartHour[] = date("H", strtotime($DateArr[$i][2]));
		$StartMin[] = date("i", strtotime($DateArr[$i][2]));
		$EndHour[] = date("H", strtotime($DateArr[$i][3]));
		$EndMin[] = date("i", strtotime($DateArr[$i][3]));
	}
}

?>

<script language="javascript">
function CheckHorizontal(jParNo, jParCheck) {
	for (i = 0; i < 7; i++) {
		if (document.getElementById('checkboxDate[' + jParNo + '][' + i + ']') != null) {	
			if (jParCheck) {
				if (!document.getElementById('checkboxDate[' + jParNo + '][' + i + ']').checked) {
					if (document.getElementById('fieldName[' + jParNo + '][' + i + ']') != null) {
							jAddEnrollmentTime(document.FormMain, 'EnrollmentTable', 'column2add', document.getElementById('fieldName[' + jParNo + '][' + i + ']').value, document.getElementById('checkboxDate[' + jParNo + '][' + i + ']').value);
					}
				}
			} else {
				if ((document.getElementById('fieldName[' + jParNo + '][' + i + ']') != null) &&
					(document.getElementById('column2add').value > 1))
						jDeleteTableRow('EnrollmentTable', 'colNo'+document.getElementById('fieldName[' + jParNo + '][' + i + ']').value, 'column2add', 'h'+jParNo , 'v'+i);
				}			
			document.getElementById('checkboxDate[' + jParNo + '][' + i + ']').checked = jParCheck;
		}
	}
}

function CheckVertical(jParNo, jParCheck) {	
	for (i = 0; i < 5; i++) {
		if (document.getElementById('checkboxDate[' + i + '][' + jParNo + ']') != null)	{			
			if (jParCheck) {				
				if (!document.getElementById('checkboxDate[' + i + '][' + jParNo + ']').checked) {					
					if ((document.getElementById('fieldName[' + i + '][' + jParNo + ']') != null)) {
						jAddEnrollmentTime(document.FormMain, 'EnrollmentTable', 'column2add', document.getElementById('fieldName[' + i + '][' + jParNo + ']').value, document.getElementById('checkboxDate[' + i + '][' + jParNo + ']').value);						
					}
				}
			} else {
				if ((document.getElementById('fieldName[' + i + '][' + jParNo + ']') != null) &&
					(document.getElementById('column2add').value > 1))
					jDeleteTableRow('EnrollmentTable', 'colNo'+document.getElementById('fieldName[' + i + '][' + jParNo + ']').value, 'column2add', 'h'+i , 'v'+jParNo);
				}
			document.getElementById('checkboxDate[' + i + '][' + jParNo + ']').checked = jParCheck;
		}			
	}
}

function setPeriods(jObj, jParElementName) {
	len = jObj.elements.length;
	var i = 0, temp = 0;
	for(i = 0 ; i < len ; i++) {
		if (jObj.elements[i].name == jParElementName && jObj.elements[i].checked)
		{
			temp = jObj.elements[i].value;
			jObj.elements['StartHour[]'][temp].value = document.getElementById('SetStartHour').value;
			jObj.elements['StartMin[]'][temp].value = document.getElementById('SetStartMin').value;
			jObj.elements['EndHour[]'][temp].value = document.getElementById('SetEndHour').value;
			jObj.elements['EndMin[]'][temp].value = document.getElementById('SetEndMin').value;
		}
	}
}


function jInsertTableRow(jTableName, jFileNumber){
	// first argument is jTableName
	// other arguments after will be td contents
	var row = jTableName.insertRow(jFileNumber);
	
	for(var i=2; i<arguments.length; i++){
        cell = row.insertCell(i-2);
		cell.innerHTML = arguments[i];
    }
}

function jDeleteTableRow(jTableName, jRowtoDelete, jFieldName, jHorizontalName, jVerticalName)
{
  var tbl = document.getElementById(jTableName);  
  var rowNumber = document.getElementById(jRowtoDelete).value - 1;
  
  tbl.deleteRow(rowNumber);
  document.getElementById(jFieldName).value = document.getElementById(jFieldName).value - 1;
  document.getElementById(jHorizontalName).checked = 0;
  document.getElementById(jVerticalName).checked = 0;
  
  var preString = "<?= date("Ym", strtotime($thisMonth))?>";
  
  <?
  		for ($i = 1; $i <= date('t'); $i++) {
	  		($i < 10) ? $no = "0".$i : $no = $i;
  ?>  
  if (document.getElementById('colNo'+preString+'<?= $no?>') != null) {
  	if (document.getElementById('colNo'+preString+'<?= $no?>').value == (rowNumber+1)) document.getElementById('colNo'+preString+'<?= $no?>').value = "";
  	if (document.getElementById('colNo'+preString+'<?= $no?>').value > (rowNumber+1)) document.getElementById('colNo'+preString+'<?= $no?>').value = document.getElementById('colNo'+preString+'<?= $no?>').value - 1;  
  }
  <? } ?>
}



function jAddEnrollmentTime(jFormObj, jTableName, jNumberName, jFieldName, jDate){
	// prepare td contents
	// manipulate jInsertTableRow()	
	if (document.all || document.getElementById)
	{
		var table = document.all ? document.all[jTableName] : document.getElementById(jTableName);
		var col_no = parseInt(document.getElementById(jNumberName).value);
		var count;
		var temp;

		if (document.all)
		{
			var td1 = "<td>" + jDate + "<input type='hidden' name='tableDates[]' id='tableDates[]' value='" + jDate + "'></td>";
			var td2 = "<td>";
				td2 += "<select id='StartHour[]' name='StartHour[]'>";
				for (count = 0; count < 24; count++) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'>"+ temp + "</option>";
				}
				td2 += "</select> : ";
				
				td2 += "<select id='StartMin[]' name='StartMin[]'>";
				for (count = 0; count < 56; count+= 5) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'>"+ temp + "</option>";
				}
				td2 += "</select>";
				
				td2 += " <?= $eEnrollment['to']?> ";
				
				td2 += "<select id='EndHour[]' name='EndHour[]'>";
				for (count = 0; count < 24; count++) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'>"+ temp + "</option>";
				}
				td2 += "</select> : ";
				
				td2 += "<select id='EndMin[]' name='EndMin[]'>";
				for (count = 0; count < 56; count+= 5) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'>"+ temp + "</option>";
				}
				td2 += "</select>";
				td2 += "</td>";
			var td3 = "<td><input type='checkbox' id='toSet[]' name='toSet[]' value='" + col_no + "'></td>";
			jInsertTableRow(table, col_no, td1, td2, td3);
			document.getElementById('colNo'+jFieldName).value = table.rows.length - 1;
			document.getElementById(jNumberName).value = col_no + 1;
		}
	}
}

function checkSubmit(obj) {
	if (document.getElementById('column2add').value == 1) {
		alert('<?= $eEnrollment['js_sel_date']?>');
	} else {
		
		len = obj.elements.length;
		var i = 0, temp = 0;		
		for(i = 0 ; i < len ; i++) {
			if (obj.elements[i].name == 'toSet[]')
			{
				temp = obj.elements[i].value;				
				if ( 
					(obj.elements['StartHour[]'][temp].value > obj.elements['EndHour[]'][temp].value) ||
					((obj.elements['StartHour[]'][temp].value == obj.elements['EndHour[]'][temp].value) &&
					 (obj.elements['StartMin[]'][temp].value > obj.elements['EndMin[]'][temp].value)
					)
					)
				{
					alert("<?= $eEnrollment['js_time_invalid']?>");
					return false;
				}
			}
		}
		
		obj.action = 'group_new2_update.php';
		obj.submit();
	}
}
</script>

<form name="FormMain" action="" method="POST">
<input type="hidden" id="StartHour[]" name="StartHour[]" value="">
<input type="hidden" id="StartMin[]" name="StartMin[]" value="">
<input type="hidden" id="EndHour[]" name="EndHour[]" value="">
<input type="hidden" id="EndMin[]" name="EndMin[]" value="">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr><td class="tabletext" align="center">
<?= /*$eEnrollment['del_warning']*/?><br/><br/>
</td></tr>
<tr><td>
	<?= $libenroll->GET_CALENDAR($thisMonth, $_POST, $tableDates, $GroupID)?>	
	<br/>
</td></tr>

<tr><td>

<table width="65%" border="0" cellspacing="0" cellpadding="4" align="center" name="EnrollmentTable" id="EnrollmentTable">
<tr class="tablegreentop tabletoplink">
	<td><?= $eEnrollment['add_activity']['act_date']?></td><td><?= $eEnrollment['add_activity']['act_time']?></td><td><?= $eEnrollment['add_activity']['select']?><br/><input type="checkbox" onclick="(this.checked) ? setChecked(1, document.FormMain, 'toSet[]') : setChecked(0, document.FormMain, 'toSet[]');"></td>
</tr>

<?	for ($j = 0; $j < sizeof($tableDates); $j++) { ?>		

<tr>
	<td class="tabletext"><?= $tableDates[$j]?><input type='hidden' name='tableDates[]' id='tableDates[]' value='<?= $tableDates[$j]?>'></td>
	<td>
		<select id="StartHour[]" name="StartHour[]">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>" <? if ($StartHour[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="StartMin[]" name="StartMin[]">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>" <? if ($StartMin[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select>
		
		<?= $eEnrollment['to']?>
		
		<select id="EndHour[]" name="EndHour[]">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>" <? if ($EndHour[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="EndMin[]" name="EndMin[]">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>" <? if ($EndMin[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select>
	</td>
	<td>
	<!--
		<input type='checkbox' id='toSet[]' name='toSet[]' value='<?= $_POST["colNo".date("Ymd", strtotime($_POST['tableDates'][$j]))] ?>'>
	-->
		<input type='checkbox' id='toSet[]' name='toSet[]' value='<?= ($j+1) ?>'>
		<!--<input type='text' size='2' value='<?= ($j+1) ?>'>-->
	</td>
</tr>
<? } ?>
<tr class="tablegreenbottom">
	<td></td>
	<td colspan="2" class="tabletext">
		<select id="setStartHour" name="setStartHour">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="setStartMin" name="setStartMin">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select>
		
		<?= $eEnrollment['to']?>
		
		<select id="setEndHour" name="setEndHour">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="setEndMin" name="setEndMin">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select>
		<?= $linterface->GET_SMALL_BTN($eEnrollment['add_activity']['set_select_period'], "button", "javascript:setPeriods(document.FormMain, 'toSet[]');") ?>
	</td>
</tr>
</table>

</td></tr>



<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<!-- <?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp; -->
<!--
<?= $linterface->GET_ACTION_BTN($button_submit, "button", "document.FormMain.action='group_new2_update.php'; document.FormMain.submit();")?>&nbsp;
-->
<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkSubmit(this.form)")?>&nbsp;
<? if (sizeof($tableDates) > 0) print $linterface->GET_ACTION_BTN($button_back, "button", "self.location='group_new1b.php?GroupID=$GroupID'")?>&nbsp;
<? if (sizeof($tableDates) > 0) print $linterface->GET_ACTION_BTN($eEnrollment['back_to_group_index'], "button", "self.location='group.php'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>
<input type="hidden" id="column2add" name="column2add" value="<?= $column2add?>"/>
<input type="hidden" id="page_link" name="page_link" value=""/>
<input type="hidden" id="GroupID" name="GroupID" value="<?= $GroupID?>"/>

<!--
<input type="hidden" id="pic" name="pic" value="<?= $pic?>"/>
<input type="hidden" id="helper" name="helper" value="<?= $helper?>"/>
<input type="hidden" id="classlvl" name="classlvl" value="<?= $classlvl?>"/>
<input type="hidden" id="set_age" name="set_age" value="<?= $set_age?>"/>
<input type="hidden" id="LowerAge" name="LowerAge" value="<?= $LowerAge?>"/>
<input type="hidden" id="UpperAge" name="UpperAge" value="<?= $UpperAge?>"/>
<input type="hidden" id="gender" name="gender" value="<?= $gender?>"/>
<input type="hidden" id="flag" name="flag" value="<?= $flag?>"/>
<input type="hidden" id="_FILES" name="_FILES" value="<?= $_FILES?>"/>
-->
</form>
    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>