<?php 
/*
 * Using
 * Change Log:
 * Date: 2017-08-14 (Omas) #P110324
 *		create this file by copy and modify ReplySlip_ajax_ReplySlipReply_view.php
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$linterface = new interface_html("popup.html");
$MulitMC_Dimeter = '#MCDIMETER#';
if(!$sys_custom['eEnrolment']['ReplySlip'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] && !$libenroll->IS_CLUB_PIC())){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$Status = isset($_POST['Status'])? (int)$_GET['Status'] : (int)$_POST['Status'];

if($Status != -1){
	$status_conds = " AND RecordStatus = '$Status'";
}
if($isActivity){
	$table = "INTRANET_ENROL_EVENTSTUDENT";
	$key = "EnrolEventID";
}else{
	$table = "INTRANET_ENROL_GROUPSTUDENT";
	$key = "EnrolGroupID";
}
$sql = "SELECT StudentID, RecordStatus FROM $table WHERE $key = '$EnrolGroupID' $status_conds";
$resultArr = $libenroll->returnResultSet($sql);

$studentIds = Get_Array_By_Key($resultArr, 'StudentID');

$users = new libuser("","",$studentIds);
$MemberListArr = array();
foreach((array)$resultArr as $_studentStatusInfo){
	$_thisStudentId = $_studentStatusInfo['StudentID'];
	$_thisStudentStatus = $_studentStatusInfo['RecordStatus'];
	
	$users->LoadUserData($_thisStudentId);
	
	$MemberListArr[$_thisStudentId]['StudentID'] = $_thisStudentId;
	$MemberListArr[$_thisStudentId]['StudentName'] = $users->UserNameLang();
	$MemberListArr[$_thisStudentId]['ClassName'] = $users->ClassName;
	$MemberListArr[$_thisStudentId]['ClassNumber'] = $users->ClassNumber;
	$MemberListArr[$_thisStudentId]['RecordStatus'] = $_thisStudentStatus;
}
if(!empty($MemberListArr)){
	SortByColumn3($MemberListArr, 'RecordStatus','ClassName','ClassNumber');
}

$getReplySlipReply_Info = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
foreach ((array)$getReplySlipReply_Info as $_getReplySlipReply_Info){
	$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['StudentID'] = $_getReplySlipReply_Info['StudentID'];
	//Ans	
	$Answer = explode('#ANS#',$_getReplySlipReply_Info['Answer']);
	unset($Answer[0]);//0->useless
	if($Answer){
		foreach ((array)$Answer as $_Answer){
			$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['Answer'][] = $_Answer;
		}
	}
	//SignAt
	$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['SignAt'] = $_getReplySlipReply_Info['DateModified'];
	
}
$ReplySlipInfo = $libenroll->getReplySlip($ReplySlipID);
$ReplySlipInfo = $ReplySlipInfo[0];
$ReplySlip_Question = $libenroll->splitQuestion($ReplySlipInfo['Detail']);


// returnQuestionString
$ReplySlipInfoTable = '<table width="100%" class="form_table_v30">';
$ReplySlipInfoTable .= '<tr>';
	$ReplySlipInfoTable .= '<td width="30%" class="field_title">'.$eEnrollment['replySlip']['Title'].'</td>';
	$ReplySlipInfoTable .= '<td>'.$ReplySlipInfo['Title'].'</td>';
$ReplySlipInfoTable .= '</tr>';
$ReplySlipInfoTable .= '<tr>';
	$ReplySlipInfoTable .= '<td width="30%" class="field_title">'.$eEnrollment['replySlip']['Instruction'].'</td>';
	$ReplySlipInfoTable .= '<td>'.$ReplySlipInfo['Instruction'].'</td>';
$ReplySlipInfoTable .= '</tr>';
$ReplySlipInfoTable .= '</table>';

$ReplyDetailTable = '<table class="common_table_list view_table_list_v30">';
	#Table Header START
	$ReplyDetailTable .= '<thead>';
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['ClassName'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['ClassName'];
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['ClassNumber'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['ClassNumber'];
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['StudentName'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['StudentName'];
		$ReplyDetailTable .= '<th>'.$i_ClubsEnrollment_Status.'</th>';
		$ExportHeader[] = $i_ClubsEnrollment_Status;
		#Print EACH QUESTION
		foreach ((array)$ReplySlip_Question as $_NoOfQuestion => $_ReplySlip_Question){
			$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['Question'].($_NoOfQuestion+1).': '.$_ReplySlip_Question[1].'</th>';
			$ExportHeader[] = $eEnrollment['replySlip']['Question'].($_NoOfQuestion+1).': '.$_ReplySlip_Question[1];
		}
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['DateSigned'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['DateSigned'];
	$ReplyDetailTable .= '</thead>';
	#Table Header END
	
	#Table Body Start 
	$ReplyDetailTable .= '<tbody>';
	#ForEach Student
	$ExportRow = 0;
	foreach ((array)$MemberListArr as  $StudentInfo){
		$StudentID = $StudentInfo['StudentID'];
		$isSuspend = (empty($ReplySlipReply_Info[$StudentID]))? "row_suspend":"";
		$ReplyDetailTable .= '<tr class="'.$isSuspend.'">';
			//ClassName
			$ClassName = $StudentInfo['ClassName'];
			$ReplyDetailTable .= '<td>'.$ClassName.'</td>';
			$ExportBody[$ExportRow][] = $ClassName;
			//ClassNumber
			$ClassNumber = $StudentInfo['ClassNumber'];
			$ReplyDetailTable .= '<td>'.$ClassNumber.'</td>';
			$ExportBody[$ExportRow][] = $ClassNumber;
			//StudentName
			$StudentName = $StudentInfo['StudentName'];
			$ReplyDetailTable .= '<td>'.$StudentName.'</td>';
			$ExportBody[$ExportRow][] = $StudentName;
			// Enrolment Status
			if($StudentInfo['RecordStatus'] == '0'){
			$displayStatus = $Lang['eEnrolment']['EnrollmentStatus']['Waiting'];
			}else if($StudentInfo['RecordStatus'] == '1'){
				$displayStatus = $Lang['eEnrolment']['EnrollmentStatus']['Rejected'];
			}else if($StudentInfo['RecordStatus'] == '2'){
				$displayStatus = $Lang['eEnrolment']['EnrollmentStatus']['Approved'];
			}else{
				$displayStatus = Get_String_Display('');
			}
			$ReplyDetailTable .= '<td>'.$displayStatus.'</td>';
			$ExportBody[$ExportRow][] = $displayStatus;
			#ForEach Question
			foreach ((array)$ReplySlip_Question as $_NoOfQuestion => $_ReplySlip_Question){
				//handling different type of Question
				if(isset($ReplySlipReply_Info[$StudentID]['Answer'][$_NoOfQuestion])){
					/*	Type 1: YesNo Question/ MC
					 * 	save as=> #ANS#x where x is a number
					 *  Type 2: Multi-MC
					 *  save as=> #ANS#x#DIMETER#x where x is a number
					 *  Type 3: ShortQuestion/ LongQuester
					 *  save as=> #ANS#abc where abc is a string
					 */ 
					$StudentAnswer = '';
					$Comma = '';
					$StudentAnswer_temp = $ReplySlipReply_Info[$StudentID]['Answer'][$_NoOfQuestion];
					$StudentAnswer_temp = explode($MulitMC_Dimeter,$StudentAnswer_temp);
					if(count($StudentAnswer_temp)>1){
						//type 2
						foreach ((array)$StudentAnswer_temp as $_StudentAnswer_temp){
							$StudentAnswer .= $Comma.$_ReplySlip_Question[2][$_StudentAnswer_temp];
							$Comma = ',';
						}
					}else{
						//type 1 and type3
						$_StudentAnswer_temp = $_ReplySlip_Question[2][$StudentAnswer_temp[0]];
						if($_StudentAnswer_temp){
							//type 1 can match
							$StudentAnswer = $_StudentAnswer_temp;
						}else{
							$StudentAnswer = $StudentAnswer_temp[0];
						}
					}
				}else{
					$StudentAnswer = '';
				}
				$ReplyDetailTable .= '<td>'.$StudentAnswer.'</td>';
				$ExportBody[$ExportRow][] = $StudentAnswer;
			}
			//SignAt
			$SignAt = $ReplySlipReply_Info[$StudentID]['SignAt']? $ReplySlipReply_Info[$StudentID]['SignAt']:$eEnrollment['replySlip']['NotSign'] ;
			$ReplyDetailTable .= '<td>'.$SignAt.'</td>';
			$ExportBody[$ExportRow][] = $SignAt;
			
		$ReplyDetailTable .= '</tr>';
		$ExportRow++;
	}
	$ReplyDetailTable .= '</tbody>';
	#Tabble Body End
	
$ReplyDetailTable .= '</table>';
if($isExport){
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$filename = "ReplySlipReply.csv";
	$export_content = $lexport->GET_EXPORT_TXT($ExportBody, $ExportHeader);
	$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false);
}else{
	$btnAry[] = array('export', 'javascript:goExport('.$ReplySlipGroupMappingID.','.$EnrolGroupID.','.$ReplySlipID.')','',array());
	$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
	$View = $ReplySlipInfoTable;
	$View .= '</br>';
	$View .= '</br>';
	$View .= '</br>';
	$View .= $htmlAry['contentTool'];
	$View .= $ReplyDetailTable;
	$View .= '<div align="center">'.$linterface->GET_ACTION_BTN($button_close, "button", "$('#TB_window').fadeOut(); 	tb_remove(); return false;","cancelbtn").'</div>';
	echo $View;
}
?>
