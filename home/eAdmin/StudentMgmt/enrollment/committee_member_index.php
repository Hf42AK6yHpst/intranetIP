<?php
//Using: anna
/******************************************
 * Modification Log:
 * 
 * 	2018-05-02 Anna
 *  Created this page
 *  
 * ****************************************/

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]  || !($libenroll->IS_CLUB_PIC()))
if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] && !$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	debug_pr( !$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] );
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$libenroll = new libclubsenrol();
$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);

if ($_POST['AcademicYearID'] != '') {
	$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
}
else if ($_GET['AcademicYearID'] != '') {
	$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
}else{
    $AcademicYearID = $libenroll->get_Next_Academic_Year_ID();
    $AcademicYearID = $AcademicYearID ==''?Get_Current_Academic_Year_ID():$AcademicYearID;  
}
if ($_POST['Semester'] != '') {
	$Semester = IntegerSafe($_POST['Semester']);
}
else if ($_GET['Semester'] != '') {
	$Semester = IntegerSafe($_GET['Semester']);
}


$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);

// edited by Ivan(17 July 2008) set default sorting field as class(class number) in asc order
if ($field=="" || !isset($field))
{
	$field = 0;
}
if ($order=="" || !isset($order))
{
	$order = 1;
}
if($filter=="" || !isset($filter)){
	$filter = 2;
}

               
if($UID_list!="")
{
	$UID = explode(",", $UID_list);
}

if ($plugin['eEnrollment'])
{
	$linterface = new interface_html();
    $ligroup = new libgroup($GroupID);
        
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)) && (!$ligroup->hasAdminBasicInfo($UserID))  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	{
		//header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
		No_Access_Right_Pop_Up();
		exit;
	}
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        # tags 
        $tab_type = "club";
        $current_tab = 3;
        include_once("management_tabs.php");        
        # navigation
        $title_display = $intranet_session_language=="en" ? $ligroup->Title : $ligroup->TitleChinese;
        $PAGE_NAVIGATION[] = array($title_display." ".$eEnrollment['groupmates_list'], "");

// 		$ClubInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID);
		
// 		$memberCount = count($ClubInfoArr);

        $typeClub = "club";
	
        if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) || ($libenroll->IS_CLUB_PIC($EnrolGroupID))) {
            $toolbar1 .= '<div style="float:left">';
            $toolbar1 .=  "<a class='contenttool' href=\"javascript:checkRole(document.form1, 'UID[]','committee_member_email.php')\"><img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/iMail/icon_compose.gif\" border=\"0\" align=\"absmiddle\">$button_email</a>\n";
            $toolbar1 .= '</div>';
            $toolbar1 .= toolBarSpacer();
        }
        
        if ((!$libenroll->disableStatus || $libenroll->ALLOW_PAYMENT($GroupID)))
        {
            $onclick = ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitAdd(document.form1, 'add_committee_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&filter=$filter')";
            $toolbar1 .= $linterface->GET_LNK_ADD($onclick, $Lang['eEnrolment']['CommitteeRecruitment']['AddStudent'], '', '', '', 0);
        }
        
		# Construct toolbars
// 		$toolbar2 = "";
		# Export link
        $toolbar1 .= $linterface->GET_LNK_EXPORT("javascript:js_Go_Export();", '', '', '', '', 0);
        $toolbar1 .= toolBarSpacer();
		
		$toolsBar = "
				<div class=\"common_table_tool\">
				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitForm(document.form1,'committee_member_update.php?AcademicYearID=$AcademicYearID&type=approve','approve')")  ."\" class=\"tool_approve\">". $button_approve."</a>
				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitForm(document.form1,'committee_member_update.php?AcademicYearID=$AcademicYearID&type=reject','reject')")  ."\" class=\"tool_reject\">". $button_reject."</a>
				</div>
			";
		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);	
		
		$sql = $libenroll->Get_Committee_Club_Member_Sql($GroupID, $EnrolGroupID, $filter, $AcademicYearID);

	    $li->field_array = array("ClassName", "ClassNumber", "StudentName");				
	    $li->field_array[]= "RecordStatus";										
	    $li->field_array[] = "Priority";
	    $li->field_array[] = "";

		$li->sql = $sql;
		
		$li->title = "";
			
		$li->column_array = array();
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		$li->column_array[] = 0;
 		$li->IsColOff = 2;
//  		$li->IsColOff = "Enrolment_Display_Club";

		// TABLE COLUMN

			$col = 0;
			$li->column_list .= "<th width=\"1%\"><span class=\"tabletoplink\">#</span></th>\n";
			$li->column_list .= "<th width=\"25%\">".$li->column($col++, $i_general_class)."</th>\n";
			$li->column_list .= "<th width=\"15%\">".$li->column($col++, $i_ClassNumber)."</th>\n";
			$li->column_list .= "<th width=\"25%\">".$li->column($col++, $i_UserStudentName)."</th>\n";
			$li->column_list .= "<th width=\"15%\">".$li->column($col++, $Lang['General']['Status'])."</th>\n";
			$li->column_list .= "<th width=\"10%\">".$li->column($col++, $Lang['eEnrolment']['Priority'])."</td>\n";
			$li->column_list .= "<th width=\"1%\">".$li->check("UID[]")."</td>\n";

			$li->no_col = 7;
			
		################################################################
		
		$linterface->LAYOUT_START();
		
		$ClubInfoArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
		$Semester = $ClubInfoArr['Semester'];
		
		$button_back_para = "AcademicYearID=$AcademicYearID&Semester=$Semester";

		
// 		$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks();
?>

<script language="javascript">
function js_Go_Export()
{
    var jsObjForm = document.getElementById('form1');
    jsObjForm.action = 'export_committee_member.php';
    jsObjForm.submit();
}

function submitAdd(obj, page){
    obj.action=page;
    obj.submit();
}

function submitForm(obj,page,action)
{
	if(countChecked(obj,'UID[]')==0)
		alert(globalAlertMsg2);
	else{
		if(action == 'approve')
		{ 
			if(confirm('<?php echo $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmApprove'];?>')){
			    obj.action = page;
			    obj.method = "POST";
			    obj.submit();
			    return;
		     }
		}
		else if(action == 'reject')
		{ 
			 if(confirm('<?php echo $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmReject'];?>')){
			     obj.action = page;
			     obj.method = "POST";
			     obj.submit();
			     return;
		      }
		 }
	}
}
</script>
		
<br />
		
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
</td></tr>
</table>
		
<div class="table_board">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr><td><?=$quotaDisplay?></td></tr>
		<tr><td height="5"></td></tr>
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	</table>
	<br />
	
	<form id="form1" name="form1" method="post">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left">	
						<?if($libenroll->AllowToEditPreviousYearData) {echo $toolbar1;} ?>	
					</td>
					<td align="right">
						<?= $SysMsg ?>
					</td>
				</tr>
				
				<?if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) || ($libenroll->IS_CLUB_PIC($EnrolGroupID))) {?>
				<tr>
					<td align="left">	
						<?= $toolbar2 ?>	
					</td>
					<td align="right">
							
					</td>
				</tr>
				<?}?>
				<tr>
					<td align="left">				
					</td>
					<td align="right"><?if($libenroll->AllowToEditPreviousYearData) { echo $toolsBar;}?></td>
				</tr>
	
			</table>
			<?=$li->display("100%","","",$filter)?>
				
			<?=$RemarksTable?>
				
			<input type="hidden" name="GroupID" value=<?= $GroupID ?>>
			<input type="hidden" name="EnrolGroupID" value=<?= $EnrolGroupID ?>>
			<input type="hidden" name="field" value="<?=$field?>">
			<input type="hidden" name="order" value="<?=$order?>">
			<input type="hidden" name="page_size_change" value="">
			<input type="hidden" name="pageNo" value="<?=$pageNo?>">
			<input type="hidden" name="numPerPage" value="<?=$ligroup->page_size?>">
			<input type="hidden" name="typeText" value="<?=$typeClub?>">
			<input type="hidden" name="Semester" value="<?=$Semester?>">
			<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
				
				
			<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr>
					<td align="center" colspan="6">
						<div style="padding-top: 5px">
							<?=$htmlAry['meritSubmitBtn']?>	
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='group_committee_recruitment.php?".$button_back_para."'")?>
																
						</div>
					</td>
				</tr>
			</table>
	</form>
</div>

	<?

		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>