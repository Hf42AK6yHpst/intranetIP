<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$GroupArr['EnrolGroupID'] = $EnrolGroupID;
$GroupArr['FileToDel'] = $FileToDel;

$EnrolGroupID = $libenroll->DEL_GROUPINFO_FILE($GroupArr);

intranet_closedb();
header("Location: group_setting.php?EnrolGroupID=$EnrolGroupID");
?>