<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageMgtActEventStat";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $li = new libgroup($GroupID);
        $TAGS_OBJ[] = array($li->Title." ".$eEnrollment['app_stu_list'], "", 1);

	    ################################################################

$UID = (is_array($UID)? $UID[0]:$UID );
//$li = new libgroup($GroupID);

$meminfo = $li->returnMemberInfo($UID);
list($Role,$Performance) = $meminfo;

$lu = new libuser($UID);
$semesterSelect = getSelectSemester("name=semester");
$lword = new libwordtemplates();

$words = $lword->getWordList(8);                
$select_word .= $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1);
$select_word .= $linterface->GET_PRESET_LIST("jArrayWords", 0, "performance");

################################################################

$linterface->LAYOUT_START();
?>

<script language="javascript">
function checkform(obj){
         return true;
}
</script>

<form name='form1' action='member_perform_update.php' method='post' onSubmit="return checkform(this);">
<br/>
<table width='90%' border='0' cellpadding='4' cellspacing='0'>
<tr>
	<td class="tabletext formfieldtitle" <?=($intranet_session_language=="en"?"":"nowrap")?> width="30%"><?php echo $i_UserLogin; ?></td>
	<td class="tabletext"><?=$lu->UserLogin?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserEnglishName; ?></td>
	<td class="tabletext"><?=$lu->EnglishName?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserChineseName; ?></td>
	<td class="tabletext"><?=$lu->ChineseName?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserClassName; ?></td>
	<td class="tabletext"><?=$lu->ClassName?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_UserClassNumber; ?></td>
	<td class="tabletext"><?=$lu->ClassNumber?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_ActivityName; ?></td>
	<td class="tabletext"><?=$li->Title?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_ActivityRole; ?></td>
	<td class="tabletext"><?=$Role?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle"><?php echo $i_ActivityPerformance; ?></td>
	<td class="tabletext"><input type='text' class='textboxnum' name='performance' value='<?=$Performance?>'> <?= $select_word?></td>
</tr>
</table>

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
</div>
</td></tr>
</table>
<input type=hidden name='UID' value=<?=$UID?>>
<input type=hidden name='GroupID' value=<?=$GroupID?>>
<input type=hidden name='filter' value=<?=$filter?>>
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>