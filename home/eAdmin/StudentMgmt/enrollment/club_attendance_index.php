<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();
$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	$CurrentPage = "PageMgtClub";		
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrollment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        # tags 
        $tab_type = "club";
        $current_tab = 3;
        include_once("management_tabs.php");

        $linterface->LAYOUT_START();
        
        $tiebreak = $lc->tiebreak;
        
		($tiebreak) ? $tiebreak_mode = $i_ClubsEnrollment_AppTime : $tiebreak_mode = $i_ClubsEnrollment_Random;
		$defaultMin = $lc->defaultMin;
		$defaultMax = $lc->defaultMax;
		$EnrollMax = $lc->EnrollMax;
		
		$AppStart = $lc->AppStart;
		$AppEnd = $lc->AppEnd;
		$GpStart = $lc->GpStart;
		$GpEnd = $lc->GpEnd;
		
		$GroupInfoArr = $lc->getGroupInfoList();
		
		if ($msg == 15) $SysMsg = $linterface->GET_SYS_MSG("", $i_con_msg_enrollment_next)."<br/>";
		if ($msg == 16) $SysMsg = $linterface->GET_SYS_MSG("", $i_con_msg_enrollment_confirm)."<br/>";
?>

<form name="form1" action="" method="POST">
<br />
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
<tr><td>

<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tableorangetop">
		<td class="tableTitle" nowrap><span class="tabletoplink"><?= $eEnrollment['club_name']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink"><?=$eEnrollmentMenu['club_attendance_mgt']?></span></td>
	</tr>
	<?
		for ($i = 0; $i < sizeof($GroupInfoArr); $i++) {
			
			$EnrolGroupID = $lc->GET_ENROLGROUPID($GroupInfoArr[$i][0]);
			$GroupName = $GroupInfoArr[$i][1];
			if  ($GroupName == "") $GroupName = $eEnrollment['no_name'];
			
			if (
				($lc->IS_NORMAL_USER($UserID))&&($lc->IS_ENROL_PIC($UserID, $EnrolGroupID, "Group"))||
				($lc->IS_NORMAL_USER($UserID))&&($lc->IS_ENROL_HELPER($UserID, $EnrolGroupID, "Group"))||
				($lc->IS_ENROL_ADMIN($_SESSION['UserID']))||($lc->IS_ENROL_MASTER($_SESSION['UserID']))
				) {
	?>
	<tr class="tableorangerow<?= (($displayed_record % 2)+1) ?>">
		<td><span class="tabletext"><?= $GroupName ?></span></td>
		<td align="center">
			<a href="club_attendance_mgt.php?GroupID=<?=$GroupInfoArr[$i][0]?>">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/eOffice/icon_viewstat.gif" border="0"></a>
			<!--
			<span class="tabletext"><?= $linterface->GET_SMALL_BTN($eEnrollmentMenu['club_attendance_mgt'], "button", "self.location='club_attendance_mgt.php?GroupID=".$GroupInfoArr[$i][0]."'")?></span>
			-->
		</td>
	</tr>
	<?
				$displayed_record++;
			}
		}
		
		if ($displayed_record == 0) {
			print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"2\">".$eEnrollment['no_record']."</td></tr>";
		}
	?>
</table>

</td></tr>
<tr><td>

<br />
<br />
</td></tr>
</table>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>