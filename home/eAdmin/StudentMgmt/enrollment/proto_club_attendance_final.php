<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageClubAttendanceMgt";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	$TAGS_OBJ[] = array($eEnrollmentMenu['attendance_mgt'], "", 1);
	
	$lc = new libclubsenrol();
	$enrolInfo = $lc->getEnrollmentInfo();
	list($students,$received,$notSchool,$notOwn,$own) = $enrolInfo;
	if ($students != $received)
	{
	    $linkNotHandin = "<a class=functionlink href=\"javascript:openList(1)\">$i_ClubsEnrollment_NotHandinList</a>";
	}
	else
	{
	    $linkNotHandin = "";
	}
	if ($notSchool != 0)
	{
	    $linkNotSchool = "<a class=functionlink href=\"javascript:openList(2)\">$i_ClubsEnrollment_NameList</a>";
	}
	else
	{
	    $linkNotSchool = "";
	}
	if ($notOwn != 0)
	{
	    $linkNotOwn = "<a class=functionlink href=\"javascript:openList(3)\">$i_ClubsEnrollment_NameList</a>";
	}
	else
	{
	    $linkNotOwn = "";
	}
	if ($own != 0)
	{
	    $linkSat = "<a class=functionlink href=\"javascript:openList(4)\">$i_ClubsEnrollment_NameList</a>";
	}
	else
	{
	    $linkSat = "";
	}
	
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();

        $linterface->LAYOUT_START();

		//$linterface->GET_SMALL_BTN($i_Discipline_System_Approval_View, "button", "javascript:viewApproval('1');");
?>

<script language="javascript">
function viewApproval(type){
         document.location.href = '<?=$intranet_httppath?>/home/admin/discipline/approval/index.php?MeritType='+ type ;
         //window.close();
}
</script>
<form name="form1" action="" method="POST">
<br/>
<table width="35%" align="center">
<tr><td>
<table><tr><td>2006 - 2007 <?= $eEnrollment['year']?></td></tr></table>
</td><td>

<table><tr><td>
<?= $eEnrollment['activity']?> : 
</td><td>
<select id="act_id" name="act_id" onChange="this.form.submit()">
<option value="">-- <?= $button_select?> --</option>
<option value="1" selected>足球學會聚會</option>
<option value="2">藍球球學會聚會</option>
</select>
</td></tr></table>
</td></tr></table>

<?
		$ConfArr['IsHorizontal'] = 1;
		$ConfArr['ChartX'] = $eEnrollmentMenu['club_enroll_status'];
		$ConfArr['ChartY'] = $eEnrollmentMenu['head_count'];
		$ConfArr['MaxWidth'] = 500;		
		
		$DataArr[] = array("3月21日 15:30-17:00", 100);
		$DataArr[] = array("3月28日 15:30-17:00", 88);
		$DataArr[] = array("4月7日 15:30-17:00", 75);
		$DataArr[] = array("4月14日 15:30-17:00", 63);
		$DataArr[] = array("4月21日 15:30-17:00", 75);
		$DataArr[] = array("4月28日 15:30-17:00", 63);
		print $linterface->GEN_CHART($ConfArr, $DataArr);
?>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr><td>
<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tablegreentop">
		<td class="tableTitle" nowrap align="center"><span class="tabletoplink"><?= $eEnrollment['student_name']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink">3月21日 <br/>15:30-17:00</span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink">3月28日 <br/>15:30-17:00</span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink">4月7日 <br/>15:30-17:00</span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink">4月14日 <br/>15:30-17:00</span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink">4月21日 <br/>15:30-17:00</span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink">4月28日 <br/>15:30-17:00</span></td>
		<td class="tableTitle" nowrap align="center"><span class="tabletoplink"><?= $eEnrollment['studnet_attendence']?></span></td>
	</tr>
	<tr class="tablegreenrow1">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">區仲恩 (1A-01)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><span class="tabletext">66%</span></td>
	</tr>
	<tr class="tablegreenrow2">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">李明思 (1A-05)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><span class="tabletext">83%</span></td>
	</tr>
	<tr class="tablegreenrow1">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">陳卓璣 (1B-02)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><span class="tabletext">100%</span></td>
	</tr>
	<tr class="tablegreenrow2">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">陳卓韻 (1D-07)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><span class="tabletext">50%</span></td>
	</tr>
	<tr class="tablegreenrow1">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">陳子韻 (2A-05)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><span class="tabletext">83%</span></td>
	</tr>
	<tr class="tablegreenrow2">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">陳韻婷 (2C-09)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><span class="tabletext">83%</span></td>
	</tr>
	<tr class="tablegreenrow1">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">鄭正蓮 (2D-02)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><span class="tabletext">66%</span></td>
	</tr>
	<tr class="tablegreenrow2">
		<td align="center"><span class="tabletext"><a href="proto_student_comment.php" class="tablelink">張詠君 (3A-03)</a></span></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_absent.gif"></td>
		<td align="center"><img src="<?= $image_path?>/2007a/icon_present.gif"></td>
		<td align="center"><span class="tabletext">83%</span></td>
	</tr>
	<tr class="tablegreenbottom">
		<td align="center"><span class="tabletext"><?= $eEnrollment['act_attendence']?></span></td>
		<td align="center"><span class="tabletext">100%</span></td>
		<td align="center"><span class="tabletext">88%</span></td>
		<td align="center"><span class="tabletext">75%</span></td>
		<td align="center"><span class="tabletext">63%</span></td>
		<td align="center"><span class="tabletext">75%</span></td>
		<td align="center"><span class="tabletext">63%</span></td>
		<td align="center"><span class="tabletext">---</span></td>
	</tr>	
</table>

</td></tr>
<tr><td>

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>
</table>

<script language="Javascript">
top.window.moveTo(0,0);
if (document.all)
{
	top.window.resizeTo(screen.availWidth,screen.availHeight);
} else if (document.layers||document.getElementById)
{
	if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
	{
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
	}
}
</script>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>