<?php
# Modify : 

/********************************************************
 * Modification Log
 * 2020-04-29 Tommy
 *      - add Club_TargetForm setting
 * 
 * 2017-10-04 Anna
 * 		- add Club_lookupClashestoSendPushMessagetoClubPIC setting
 * 
 * 2016-07-26 Anna
 * 		- add insert log for change setting
 * 
 * 	2014-12-02 Omas
 * 		- New setting $SettingArr['Club_notCheckTimeCrash']
 *
 *  2014-10-14	Pun
 * 		- Add round report for SIS customization
 *
 * 2014-09-15  Bill
 * 		- add delete log for clear records
 * 
 * 2013-03-01  Rita
 * 		- add $disallowPICtoAddOrRemoveMembers
 * 
 * 2013-02-26  Rita
 * 		- add $DefaultAttendanceStatus for customization 
 * 
 * 2012-12-21  Ivan
 * 		- added term-based application quota settings
 * 
 * 2012-11-01  Rita
 *  	- add Club_DisableHelperModificationRightLimitation, $chkDisableHelperModificationRightLimitation for attendance helper right control
 * 
 * 	2010-08-02  Thomas
 * 		- Add 'SettingArr['Club_DisableUpdate']' to store the setting of
 *        'Update Disable' in Database
 *
 ********************************************************/
 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT.'includes/liblog.php');


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lgs = new libgeneralsettings();
$json = new JSON_obj();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$appQuotaAry = $_POST['appQuotaAry'];


$SettingArr = array();
$SettingArr['Club_EnrolmentMode'] = $basicmode;
$SettingArr['Club_EnrollDescription'] = stripslashes($Description);

$SettingArr['Club_ApplicationStart'] 		= NULL;
$SettingArr['Club_ApplicationEnd']			= NULL;
$SettingArr['Club_AnnounceEnrolmentResult'] = NULL;
//$SettingArr['Club_DefaultMin'] 			= NULL;
//$SettingArr['Club_DefaultMax'] 			= NULL;
$SettingArr['Club_TieBreak'] 				= NULL;
//$SettingArr['Club_EnrollMax'] 				= NULL;
$SettingArr['Club_EnrollPersonType'] 		= NULL;
$SettingArr['Club_DisableStatus'] 			= NULL;
$SettingArr['Club_ActiveMemberPer'] 		= NULL;
$SettingArr['Club_OneEnrol'] 				= NULL;
$SettingArr['Club_notCheckTimeCrash']		= NULL;
$SettingArr['Club_CaterPriority'] 			= NULL;
$SettingArr['Club_OnceEmail'] 				= NULL;
$SettingArr['Club_TargetEnrolSemester'] 	= NULL;
$SettingArr['Club_DisableCheckingNoOfClubStuWantToJoin'] 	= NULL;
$SettingArr['Club_DisableUpdate']          = NULL;
$SettingArr['Club_DisableHelperModificationRightLimitation']  = NULL;
$SettingArr['Club_DisallowPICtoAddOrRemoveMembers']  = NULL;
$SettingArr['Club_QuotaSettingsType']  		= NULL;
$SettingArr['Club_DefaultAttendanceStatus'] = NULL;
$SettingArr['Club_TargetEnrolRound'] 	= NULL;
if($plugin['eClassTeacherApp']){
	$SettingArr['Club_lookupClashestoSendPushMessagetoClubPIC'] = NULL;
	
}
if($sys_custom['eEnrollment']['setTargetForm']){
    $SettingArr['Club_TargetForm'] = NULL;
    
    if(sizeof($classlvl) > 0){
        $targetForm = implode(",", $classlvl);
    }
}

$AnnounceEnrolmentResultDate = trim($AnnounceEnrolmentResultDate);
if ($AnnounceEnrolmentResultDate == '') {
	$AnnounceEnrolmentResultDateTime = null;
}
else {
	$AnnounceEnrolmentResultDateTime = $AnnounceEnrolmentResultDate." ".$AnnounceEnrolmentResultHour.":".$AnnounceEnrolmentResultMin.":00";
}

if($libenroll->enableClubDefaultAttendaceSelectionControl()){
	$DefaultAttendanceStatus = $_POST['DefaultAttendanceStatus'];
}

switch ($basicmode)
{
        case 0:           # Disabled
             break;
        case 1:           # Simple
             $SettingArr['Club_ApplicationStart'] 			= $AppStart." ".$enrolStartHour.":".$enrolStartMin.":00";
             $SettingArr['Club_ApplicationEnd']				= $AppEnd." ".$enrolEndHour.":".$enrolEndMin.":00";
             $SettingArr['Club_AnnounceEnrolmentResult'] 	= $AnnounceEnrolmentResultDateTime;
//             $SettingArr['Club_DefaultMin'] 				= $defaultMin;
//             $SettingArr['Club_DefaultMax'] 				= $defaultMax;
//             $SettingArr['Club_EnrollMax'] 					= $EnrollMax;
             $SettingArr['Club_EnrollPersonType'] 			= $enrollPersontype;
             $SettingArr['Club_CaterPriority'] 				= $caterPriority;
             $SettingArr['Club_QuotaSettingsType']			= $quotaSettingsType;
             break;
        case 2:           # Advanced
             $SettingArr['Club_ApplicationStart'] 			= $AppStart." ".$enrolStartHour.":".$enrolStartMin.":00";
             $SettingArr['Club_ApplicationEnd']				= $AppEnd." ".$enrolEndHour.":".$enrolEndMin.":00";
             $SettingArr['Club_AnnounceEnrolmentResult'] 	= $AnnounceEnrolmentResultDateTime;
//             $SettingArr['Club_DefaultMin'] 				= $defaultMin;
//             $SettingArr['Club_DefaultMax'] 				= $defaultMax;
             $SettingArr['Club_TieBreak'] 					= $tiebreak;
//             $SettingArr['Club_EnrollMax'] 					= $EnrollMax;
             $SettingArr['Club_EnrollPersonType'] 			= $enrollPersontype;
             $SettingArr['Club_DisableStatus'] 				= $disableStatus;
             $SettingArr['Club_ActiveMemberPer'] 			= $activeMemberPer;
             $SettingArr['Club_OneEnrol'] 					= $oneEnrol;
             $SettingArr['Club_notCheckTimeCrash'] 			= $notCheckTimeCrash;
             $SettingArr['Club_CaterPriority'] 				= $caterPriority;
             $SettingArr['Club_OnceEmail'] 					= $onceEmail;
             $SettingArr['Club_TargetEnrolSemester'] 		= $TargetSemester;
             $SettingArr['Club_DisableCheckingNoOfClubStuWantToJoin'] 	= $disableCheckingNoOfClubStuWantToJoin;
             $SettingArr['Club_DisableUpdate']          	= $disableUpdate;
             $SettingArr['Club_DisableHelperModificationRightLimitation'] = $disableHelperModificationRightLimitation;
             $SettingArr['Club_DisallowPICtoAddOrRemoveMembers'] = $disallowPICtoAddOrRemoveMembers;
             $SettingArr['Club_QuotaSettingsType']			= $quotaSettingsType;
             $SettingArr['Club_DefaultAttendanceStatus']	= $DefaultAttendanceStatus;
             $SettingArr['Club_TargetEnrolRound'] 			= $TargetRound;
             if($plugin['eClassTeacherApp']){
             	$SettingArr['Club_lookupClashestoSendPushMessagetoClubPIC'] = $lookupClashestoSendPushMessagetoClubPIC;
             }
             $SettingArr['Club_TargetForm']                 = $targetForm;
             break;
}





$Success['EditSettings'] = $lgs->Save_General_Setting($libenroll->ModuleTitle, $SettingArr);

$Success['SaveQuotaSettings'] = $libenroll->Save_Application_Quota_Settings($enrolConfigAry['Club'], $appQuotaAry);

// debug_pr($SettingArr);
// die();
/*
$updatedcontent = implode("\n",$lines);
$lf->file_write($updatedcontent,$setting_file);
$lf->file_write($Description, $desp_file);
*/

if ($clearRecord==1)
{
	include_once($PATH_WRT_ROOT.'includes/liblog.php');
	
    intranet_opendb();
    $li = new libdb();
    
//    $sql = "DELETE FROM INTRANET_ENROL_STUDENT";
//    $li->db_db_query($sql);
	//$sql = "UPDATE INTRANET_ENROL_STUDENT Set Max = 0, Approved = 0 ";
	$sql = "UPDATE INTRANET_ENROL_STUDENT Set Max = null, Approved = 0 ";
	$li->db_db_query($sql);
	$sql = "UPDATE INTRANET_ENROL_STUDENT_CATEGORY_ENROL Set Max = 0, Approved = 0 ";
	$li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT";
    $li->db_db_query($sql);
    $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = 0, AddToPayment = 0";
    $li->db_db_query($sql);
    
    # Insert delete log
	$liblog = new liblog();
	$SuccessArr['Log_Delete'] = $liblog->INSERT_LOG('eEnrolment', 'Delete_All_Enrol_Records');
   
}



// $arr = BuildMultiKeyAssoc($tmpAry, 'SettingName', array('SettingValue'),1);

$SettingArr['ModifiedBy'] = $_SESSION['UserID'];
$jsonSettingArr = $json->encode($SettingArr);


# insert log
$liblog = new liblog();
$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Club_Setting', $jsonSettingArr, 'GENERAL_SETTING');

header("Location: basic.php?msg=2");
?>