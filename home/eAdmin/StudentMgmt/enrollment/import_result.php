<?
//using by: Pun
#############################################
#
#	Date	2018-07-25 (Pun) [121822]
#			added NCS Cust
#
#	Date	2017-11-06 (Simon) [B115479]
#			amend the preview of date format
#
#	Date	2015-05-11 (Omas)
#			modified import other records btn to new Lang
#
#	Date	2015-04-08 (Omas)
#			added field userlogin to import (will be ignore not show in result pages)
#
#############################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$GroupID = IntegerSafe($GroupID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

$data = unserialize(rawurldecode($data));
$result_ary = unserialize(rawurldecode($result_ary));
$imported_student = unserialize(rawurldecode($imported_student));
$imported_date = unserialize(rawurldecode($imported_date));

$lu = new libuser();
$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$libgroup = new libgroup($GroupID);
$linterface = new interface_html();

if ($type=="clubAttendance")
{
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrollment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	//$TAGS_OBJ[] = array($button_import." ".$libgroup->Title." ".$eEnrolment['attendance_title'], "", 1);
	# tags 
    $tab_type = "club";
    $current_tab = 1;
    # navigation
    $PAGE_NAVIGATION[] = array($button_import." ".$libgroup->Title." ".$eEnrolment['attendance_title'], "");	
    
	$parameters = "EnrolGroupID=$EnrolGroupID&GroupID=$GroupID";
	
}	
else if($type=="eventAttendance")
{
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrollment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	//$TAGS_OBJ[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrolment['attendance_title'], "", 1);
	# tags 
    $tab_type = "activity";
    $current_tab = 1;
    # navigation
    $PAGE_NAVIGATION[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrolment['attendance_title'], "");
    
	$parameters = "EnrolEventID=$EnrolEventID";
}
else if($type=="clubPerformance")
{
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
    
    //$TAGS_OBJ[] = array($button_import." ".$libgroup->Title." ".$eEnrollment['performance_title'], "", 1);
	# tags 
    $tab_type = "club";
    $current_tab = 1;
    # navigation
    $PAGE_NAVIGATION[] = array($button_import." ".$libgroup->Title." ".$eEnrolment['performance_title'], "");	
    
	$parameters = "GroupID=$GroupID";	
}
else if($type=="eventPerformance")
{
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
    
    //$TAGS_OBJ[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrollment['performance_title'], "", 1);
	# tags 
    $tab_type = "activity";
    $current_tab = 1;
    # navigation
    $PAGE_NAVIGATION[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrolment['performance_title'], "");
    
    $parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID";
}

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

include_once("management_tabs.php");

$linterface->LAYOUT_START();


### List out the import result
if ($type=="clubAttendance" || $type=="eventAttendance")
{
	$total = sizeof($data) - 1;
}
else if ($type=="clubPerformance" || $type=="eventPerformance")
{
	$total = sizeof($data);
}

$x = "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
if ($type!="clubAttendance" && $type!="eventAttendance")
	$x .= "<tr><td class=\"tabletext\">$list_total : ". $total .", ".$eEnrollment['succeeded']." : ". sizeof($imported_student) .", ". $eEnrollment['failed'] .": ". ($total - sizeof($imported_student)) ."</td></tr>";	
$x .= "</table>";
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";

$x .= "<td class=\"tablebluetop tabletopnolink\" >$i_ClassName</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\" >$i_ClassNumber</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserStudentName</td>";

if ($type=="clubAttendance" || $type=="eventAttendance")
{	
	for ($i=3; $i<sizeof($data[0]); $i++)
	{
		# amend the preview of date format
		//$meetingDate = date("Y-m-d H:i", strtotime($data[0][$i]));
		$recordDate = date("Y-m-d", strtotime(substr($data[0][$i], 0, 10)));
		$startTime = date("H:i", strtotime(substr($data[0][$i], 11, 5)));
		$endTime = date("H:i", strtotime(substr($data[0][$i], 17, 5)));
		$meetingDate = $recordDate. "<br />" . $startTime. " - " . $endTime;
		$x .= "<td class=\"tablebluetop tabletopnolink\" >$meetingDate</td>";
		
		if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
		    $x .= "<td class=\"tablebluetop tabletopnolink\" >{$Lang['General']['Remark']}</td>";
		    $i++;
		}
	}
}
else if ($type=="clubPerformance" || $type=="eventPerformance")
{
	$x .= "<td class=\"tablebluetop tabletopnolink\" >{$eEnrollment['performance']}</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" >{$eEnrollment['teachers_comment']}</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SAMS_import_error_reason</td>";
$x .= "</tr>";
      
if ($type=="clubAttendance" || $type=="eventAttendance")
{
	//title data is still here for attendance, so display one row less
	$endIndex = sizeof($data) - 1;
}
else if ($type=="clubPerformance" || $type=="eventPerformance")
{
	$endIndex = sizeof($data);
}

for($i=0;$i<$endIndex;$i++)
{
	if ($type=="clubAttendance" || $type=="eventAttendance")
	{
		/*
		[0] Class Name
		[1] Class Number
		[2] Present/Absent
		[3] Present/Absent
		...
		[?] Present/Absent
		*/
		
		$ClassName = $data[$i+1][0];
		$ClassNumber = $data[$i+1][1];
	}
	else if ($type=="clubPerformance" || $type=="eventPerformance")
	{
		/*
		[0] Class
		[1] Class Number
		[2] Performance
		[3] Comment
		*/
		list($ClassName, $ClassNumber, $Performance, $Comment) = $data[$i];
	}
	
	### checking - valid class & class number (student found)
	
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	$lu_tmp = new libuser($StudentID);
	
	if ($type=="clubAttendance" || $type=="eventAttendance")
	{
		$status = in_array($StudentID, $imported_student) ? "": $eEnrollment[$result_ary[$i+1]]; 
	}
	else if ($type=="clubPerformance" || $type=="eventPerformance")
	{
		$status = in_array($StudentID, $imported_student) ? "": $eEnrollment[$result_ary[$i]]; 
	}
	$css = "tabletext";
//	$fontColorStart = $status ? "<font color=\"red\">":"";
//	$fontColorEnd = $status ? "</font>":"";
	$this_remark = str_replace('""', '"', $Remark);
	
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">". $ClassName ."</td>";
	$x .= "<td class=\"$css\">". $ClassNumber ."</td>";
	
	if ($ClassName!=NULL && $ClassNumber!=NULL)
	{
		$x .= "<td class=\"$css\">". $lu_tmp->UserNameLang() ."</td>";
	}
	else
	{
		//have not input class and class number => don't show name
		$x .= "<td class=\"$css\"></td>";
	}
	
	if ($type=="clubAttendance" || $type=="eventAttendance")
	{
		for($j=3; $j<sizeof($data[0]); $j++)
		{
			//$css_date = ($imported_date[$i+1][$j]==1) ? "tabletext":"red";
			//$x .= "<td class=\"$css_date\">". $data[$i+1][$j] ."</td>";

		    if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
		        if($j % 2 == 0){
		            $imported_date[$i+1][$j] = 1;
		        }
		    }
			if ($imported_date[$i+1][$j]==1)
			{
				# normal	
				$x .= "<td class=\"tabletext\">". $data[$i+1][$j] ."</td>";
			}
			else
			{
				# red	
				$x .= "<td class=\"tabletext\"><font color=\"red\">". $data[$i+1][$j] ."</font></td>";
			}
		}
	}
	else if ($type=="clubPerformance" || $type=="eventPerformance")
	{
		$x .= "<td class=\"$css\">". $Performance ."</td>";
		$x .= "<td class=\"$css\">". $Comment ."</td>";
	}
	
	$fontColorStart = $status ? "<font color=\"red\">":"";
	$fontColorEnd = $status ? "</font>":"";
	
	$x .= "<td class=\"$css\">".$fontColorStart.$status.$fontColorEnd."</td>";
	$x .= "</tr>";
}  
	$x .= "<tr>";
   		$x .= "<td class=\"tabletextremark \" colspan=\"10\"><span class=\"tabletextrequire\">*</span>".$Lang['eEnrolment']['ImportFailRemarks']."</td>";
	$x .= "</tr>";             
$x .= "</table>";

?>

<br />
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
    </td>
</tr>
<tr>
	<td align="center">
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                	<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
        </td>
</tr>
<tr>
	<td>        
    	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
        	<tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center" colspan="2">
					<?
					if ($type=="clubAttendance")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($Lang['General']['ImportOtherRecords'], "button", "window.location='import.php?EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type'") ?>
						<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_attendance'], "button", "window.location = 'club_attendance_mgt.php?$parameters'")?>&nbsp;
					<?
					}
					else if ($type=="eventAttendance")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($Lang['General']['ImportOtherRecords'], "button", "window.location='import.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type'") ?>
						<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_attendance'], "button", "window.location = 'event_attendance_mgt.php?$parameters'")?>&nbsp;
					<?
					}
					else if ($type=="clubPerformance")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($Lang['General']['ImportOtherRecords'], "button", "window.location='import.php?EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type'") ?>
						<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_enrolment'], "button", "window.location = 'member_index.php?$parameters'")?>&nbsp;
					<?
					}
					else if ($type=="eventPerformance")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($Lang['General']['ImportOtherRecords'], "button", "window.location='import.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type'") ?>
						<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_enrolment'], "button", "window.location = 'event_member_index.php?$parameters'")?>&nbsp;
					<?
					}
					?>
				</td>
			</tr>
	    </table>                                
	</td>
</tr>
</table>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
