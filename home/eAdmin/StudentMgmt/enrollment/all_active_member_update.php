<?php
// Using: 

/***********************************************
 * Date: 	2013-05-28 (Rita)
 * Details: create this page 
 ***********************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->HAVE_CLUB_MGT() && !$libenroll->HAVE_EVENT_MGT()  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	header("Location: {$PATH_WRT_ROOT}home/eAdmin/StudentMgmt/enrollment/");

$activeMemberPer = $_POST['activeMemberPer'];
$radioID = $_POST['radioID'];
$type = $_POST['type'];
$AcademicYearID = $_POST['AcademicYearID'];

if($type=="club")
{
	$EnrollIDArr = $_POST['EnrolGroupIDArr'];
}else{
	$EnrollIDArr = $_POST['EnrolEventIDArr'];
}


if ($plugin['eEnrollment'])
{
	
	### Update Active Member Percentage
	if ($isUpdatePercentage == 1)
	{
		foreach ((array)$radioID as $EnrollID=>$studentInfo)
		{					
							
			if($type=="club")
			{
				$libenroll->Update_Club_Active_Member_Percentage($activeMemberPer, $EnrollID);			
			}
			elseif($type=="activity")
			{
				$libenroll->Update_Event_Active_Member_Percentage($activeMemberPer, $EnrollID);	
			}	
			
			$DataArr = array();
			$activeArr = array();
			$inactiveArr = array();
			foreach ((array)$studentInfo as $thisStudentID=>$thisIsActiveValue)
			{
				$DataArr['StudentID'] = $thisStudentID;		   		
		    			    	
				if ($type=="club")
				{
					$DataArr['EnrolGroupID'] = $EnrollID;					
					$attendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr); 					        	
				}
				elseif($type=="activity")
				{
					$DataArr['EnrolEventID'] = $EnrollID;		    		
		        	$attendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);			      	        	
				}
				
				if ($attendance>=$activeMemberPer)
	        	{
		        	$activeArr[]= $DataArr['StudentID'];
	        	}
	        	else
	        	{
		        	$inactiveArr[]= $DataArr['StudentID'];
	        	}	
				
				
			}
					
			# Update Database
        	if (sizeof($activeArr) > 0)
        	{       		
        		if ($type=="club")
        		{
        			$result = $libenroll->Update_Club_Active_Member_Status($EnrollID, $activeArr, $thisIsActiveValue=1);       		        	
        		}else
        		{
        			$result = $libenroll->Update_Event_Active_Member_Status($EnrollID, $activeArr, $thisIsActiveValue=1);
        		}
        	}
        	if (sizeof($inactiveArr) > 0)
        	{
        		if ($type=="club")
        		{
	        		$result = $libenroll->Update_Club_Active_Member_Status($EnrollID, $inactiveArr, $thisIsActiveValue=0);
        		}else
        		{
        			$result = $libenroll->Update_Event_Active_Member_Status($EnrollID, $inactiveArr, $thisIsActiveValue=0);
        		}
        	}       
        			
		}		
			
		
	}
	else
	{		
		##### Update Active Member Value					
		
		foreach ((array)$radioID as $EnrollID=>$studentInfo)
		{					
			foreach ((array)$studentInfo as $thisStudentID=>$thisIsActiveValue)
			{
				if ($type=="club")
				{
					$result = $libenroll->Update_Club_Active_Member_Status($EnrollID, $thisStudentID, $thisIsActiveValue);
				}
				elseif($type=="activity")
				{
					$result = $libenroll->Update_Event_Active_Member_Status($EnrollID, $thisStudentID, $thisIsActiveValue);
				}				
			}			
		
		}			

	}
	
	intranet_closedb();
	
	if ($type=="club")
	{		
		header("Location: all_active_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=".$EnrollIDArr."&activeMemberPer=$activeMemberPer&type=$type&msg=2");
	}
	else if ($type=="activity")
	{
		header("Location: all_active_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=".$EnrollIDArr."&activeMemberPer=$activeMemberPer&type=$type&msg=2");
	}
	
}

else
{
?>
You have no priviledge to access this page.
<?
}
?>