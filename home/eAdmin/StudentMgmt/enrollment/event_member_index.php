<?php
#using : anna

/******************************************
 * Modification Log:
 *      2018-11-14 (Philips)
 *      - Added Student Gender Column Related $sys_custom['eEnrolment']['ShowStudentGender']
 * 
 *      2018-06-14 (Philips)
 *      - Hide parent filter selection
 * 
 * 		2017-06-20 (Omas)		
 * 		- Changed $eEnrollment['comment'] to $eEnrollment['teachers_comment']
 * 
 * 		2017-05-25 (Anna)
 * 		-Improved $sys_custom['eEnrolment']['ShowStudentNickName']
 * 
 * 		2015-12-16 (Omas)
 * 		-added activity merit - enableActivityRecordMeritInfoRight()
 * 
 * 		2015-10-07 (Omas)
 * 		-Improved $sys_custom['eEnrolment']['ActivityAddRole'] , selection box for role - value using ID
 * 
 * 		2015-09-11 Omas
 * 		- add ($sys_custom['eEnrolment']['twghwfns']['AwardMgmt']) - award mgmt
 * 
 * 		2015-07-30 Evan
 * 		- Fixed bug in searching
 * 
 * 		2015-07-10 Evan
 * 		- Update search box style
 * 
 * 		2015-06-02 Evan
 * 		- Replace the old style with UI standard
 * 
 * 		2015-03-23 Omas
 * 		- Customization : hide column ECA,SS,CS - Z42562 $sys_custom['eEnrolment']['tkogss_student_enrolment_report']
 * 
 *      2014-12-08 Omas
 *      - when $sys_custom['eEnrolment']['ActivityAddRole'], provide selection box to select role
 *
 *  	2013-04-22 Rita
 * 		- modified checking of Event_DisallowPICtoAddOrRemoveMembers
 * 
 * 		2013-03-01 Rita
 * 		- add Disallow PIC to add/remove members control, $showBtnforPIC
 *
 * 		2012-03-30 Ivan
 * 		- add achivement field display
 * 
 *		2011-09-07	YatWoon
 *		- lite version implementation
 *
 * 		20110604 YatWoon
 *			- add "Print" function (case#2011-0526-1607-35071)
 *
 *		20110524 YatWoon
 *			- add column "Attendance"
 * 
 * 		20100802 Thomas
 * 			- Check the data is freezed or not
 *			  Show alert message when adding / updating the data
 * 
 ******************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();

if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "")
{
	$page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_EVENT_PIC($EnrolEventID)))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
        
        ($EventInfoArr['AddToPayment']==1)? $allowPayment=false : $allowPayment=true;

        //$TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['app_stu_list'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 1;
        include_once("management_tabs.php");
        # navigation
        $PAGE_NAVIGATION[] = array($eEnrollment['participant_list'], "");
        
        # Setting - >Disallow PIC to add/remove members Control
		$showBtnforPIC = true;
		if($libenroll->Event_DisallowPICtoAddOrRemoveMembers){
			if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])){
				if(($libenroll->IS_EVENT_PIC($EnrolEventID))){
					$showBtnforPIC = false;
				}
			}
		}
        
        
        if($filter=="" || !isset($filter)){
			$filter = 2;
//             $filter = ($filter== 1) ? 1 : 2;
		}	
        if (!isset($field))
        {
	        $field = ($filter==2)? 0 : 2;
        }

		switch ($field){
			case 0: $field = 0; break;
			case 1: $field = 1; break;
			case 2: $field = 2; break;
			case 3: $field = 3; break;
			case 4: $field = 4; break;
			case 5: $field = 5; break;
			default: $field = 0; break;
		}
		if ($order=="" || !isset($order))
		{
			$order = ($order == 0) ? 1 : 0;
		}
		
        $quotaDisplay = "<table class=\"form_table_v30\">";	
		$quotaDisplay .= "<tr><td class=\"field_title\">{$eEnrollment['act_name']}</td><td>".$EventInfoArr[3]."</td></tr>";
		if ($EventInfoArr[1] == 0)
		{
			
			$quotaDisplay .= "<tr><td class=\"field_title\">$i_ClubsEnrollment_ActivityQuota1 </td><td>".$i_ClubsEnrollment_NoLimit."</td></tr>";
		}
		else
		{
			$quotaDisplay .= "<tr><td class=\"field_title\">$i_ClubsEnrollment_ActivityQuota1 </td><td>".$EventInfoArr[1]."</td></tr>";
		}
		$quotaDisplay .= "<tr><td class=\"field_title\">$i_ClubsEnrollment_GroupApprovedCount </td><td>".$EventInfoArr[2]."</td></tr>";
		$quotaDisplay .= "</table>";
        
		//for selection of Student/Teacher/Staff/All
        $filterbar = "";
        $filter_array = array (
			   //array (3,$i_identity_parent),
               array (2,$i_identity_student),
               array (1,$i_identity_teachstaff),
               );
        $filterbar .= getSelectByArray($filter_array,"name=filter onChange=\"document.form1.target='_self'; document.form1.action='event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID'; document.form1.submit();\"",$filter, 0, 1);
		
		if ($sys_custom['eEnrolment']['ClubMemberListApprovedByUserFilter'] && $filter == 2) {
			$filterbar .= $libenroll_ui->Get_Member_Approve_By_Selection($enrolConfigAry['Activity'], 'approvedByType', 'approvedByType', $approvedByType, "changedApprovedByFilterSelection();");
		}
		$sqlkeyword = stripslashes($keyword);
		$keyword = intranet_htmlspecialchars(stripslashes($keyword));
        $searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"" . $keyword . "\" onkeyup=\"Check_Go_Search(event);\"/>";
		$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "javascript:document.form1.keyword.value = Trim(document.form1.keyword.value); document.form1.submit()");
		
		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_name']) $keyword = "";
		
		if (isset($keyword))
		{
			$conds = "AND (a.EnglishName LIKE '%{$keyword}%' OR a.ChineseName LIKE '%{$keyword}%')";
		}
		
		$type = 3;
		$add_member_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$type = "eventPerformance";
		$import_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
		//$type = 4;
		//$export_parameters = "EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$type = "activity";
		$active_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$notification_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";	
		$type = "activityParticipant";
		$importParticipant_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
		
		//tool bar
		$toolbar1 = "";
		$toolbar2 = "";
		$EventStudents = sizeof($libenroll->GET_ENROLLED_EVENTSTUDENT($EnrolEventID));
		
		if($libenroll->AllowToEditPreviousYearData) {
			//disable email function if there is no student
			$toolbar1 .= '<div style="float:left">';
			$toolbar1 .= ($EventStudents == 0) ? "<img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/iMail/icon_compose.gif\" border=\"0\" align=\"absmiddle\"><span class='tabletext'> $button_email</span>" : "<a class='contenttool' href=\"javascript:checkRole(document.form1, 'UID[]','member_email.php')\"><img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/iMail/icon_compose.gif\" border=\"0\" align=\"absmiddle\">$button_email</a>";
			$toolbar1 .= '</div>';
			$toolbar1 .= toolBarSpacer();
		}
		
		if ($libenroll->AllowToEditPreviousYearData && (!$libenroll->Event_disableStatus || $allowPayment))
		{
			//link of adding students to the event
			//$toolbar1 .= $linterface->GET_LNK_ADD("add_member.php?$add_member_parameters");
//			$toolbar1 .= "<a class=\"contenttool\" href=\"";
//			$toolbar1 .= ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "add_member.php?$add_member_parameters";
//            $toolbar1 .= "\">";
//			$toolbar1 .= "<img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_add.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
//			$toolbar1 .= $eEnrollment['button']['add&assign'];
//			$toolbar1 .= "</a>";
//			$toolbar1 .= toolBarSpacer();

			$onclick = ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "add_member.php?$add_member_parameters";
			
			if($showBtnforPIC==true){
				$toolbar1 .= $linterface->GET_LNK_ADD($onclick, $eEnrollment['button']['add&assign'], '', '', '', 0);
			}
		}
		
		//link of Print (students only)
		if ($filter == 2)
		{
			if($plugin['eEnrollmentLite'])
			{
				$toolbar1 .= "<div class='Conntent_tool'><i class='print'>".$eEnrollment['button']['notification_letter']."</i></div>";
			}
			else
			{
				$onclick = "javascript: document.form1.target='blank'; checkRole(document.form1, 'UID[]', 'notification_letter_view.php?$notification_parameters')";
				$toolbar1 .= $linterface->GET_LNK_PRINT($onclick, $eEnrollment['button']['notification_letter'], '', '', '', 0);
			}
		}
		
		//second row of toolbar
		if($libenroll->AllowToEditPreviousYearData) {
			//link of importing participant of the event
			if($showBtnforPIC==true){
				$toolbar2 .= $linterface->GET_LNK_IMPORT(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "import.php?$importParticipant_parameters", '', '', '', '', 0);
				$toolbar2 .= toolBarSpacer();
			}
		}
		
		
		# Survey Form Customization
		if($libenroll->enableActivitySurveyFormRight()){
			$exportMember = $exportMember = "<a href='javascript:js_Go_Export();' class='sub_btn'>".$Lang['eEnrolment']['Btn']['ExportMember2']."</a>";
			$exportSurveyResult = "<a href='javascript:js_Go_ExportSurveyResult();' class='sub_btn'>".	$Lang['eSurvey']['SurveyResult']."</a>";
	
			$toolbar2 .= '<div class="Conntent_tool"><div class="btn_option" id="ExportDiv">
							<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Export'].'</a>
							<br style="clear: both;">
							<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
							'.$exportMember.'
							'.$exportSurveyResult.'
							</div>
							</div></div>';
		}
		else{
			$toolbar2 .= $linterface->GET_LNK_EXPORT("javascript:js_Go_Export();", '', '', '', '', 0);
		}
		
		
		
		
		
		$toolbar2 .= toolBarSpacer();
		
		# Print member list link
		$toolbar2 .= $linterface->GET_LNK_PRINT_IP25("javascript:print_member_list();");
		$toolbar2 .= toolBarSpacer();
		
		//link of importing performance and comment of students of the event
		//$toolbar2 .= $linterface->GET_LNK_IMPORT("import.php?$import_parameters", $eEnrollment['import_performance_comment']);
		//$toolbar2 .= toolBarSpacer();
		
		//link of active member
		if($plugin['eEnrollmentLite'])
			$toolbar3 .= "<div class='Conntent_tool'><i class='import'>". $eEnrollment['button']['active_member'] ."</i></div>";
		else
			$toolbar3 .= $linterface->GET_LNK_IMPORT(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "active_member.php?$active_parameters", $eEnrollment['button']['active_member'], '', '', '', 0);
		
		if($sys_custom['eEnrolment']['twghwfns']['AwardMgmt'])
			$toolbar3 .= $linterface->GET_LNK_IMPORT(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "management/award_management/index.php?$active_parameters", $Lang['eEnrolment']['AwardMgmt']['AwardMgmt'], '', '', '', 0);
		
		# Role update
		if ($filter == 2)
		{
			//function bar
			$functionbar = "";
			if (!isset($RoleTitle) || $RoleTitle=="") $RoleTitle = $eEnrollment['enter_role'];
			//$functionbar .= "<input class='textboxnum' type='text' name='RoleTitle' value=\"".stripslashes($RoleTitle)."\" onClick=\"if (this.value == 'Enter Role') this.value=''\">\n";
			
			$functionbar .= $linterface->GET_SMALL_BTN($eEnrollment['button']['update_role'], "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkForm()")."&nbsp;";
		}
		
		if (!$libenroll->Event_disableStatus || $allowPayment)
		{
			if($showBtnforPIC==true){
				$functionbar .= ($li->RecordType == -1) ? "&nbsp;" : $linterface->GET_SMALL_BTN($Lang['Btn']['Delete'], "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRemove(document.form1,'UID[]','member_delete.php')");
			}
		}
		
		if ($filter == 2)
		{
			//update performance button
			if(!$plugin['eEnrollmentLite'])
				$groupadminfunctionbar .= $linterface->GET_SMALL_BTN($Lang['eEnrolment']['UpdatePerformanceAndPerformance'], "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRestore(document.form1, 'UID[]','member_comment_perform.php')");
			else
				$groupadminfunctionbar .= $linterface->GET_SMALL_BTN($Lang['eEnrolment']['UpdatePerformanceAndPerformance'], "button", "",""," disabled ");
		}			
		
		
		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);
		$name_field = getNameFieldByLang2("a.");
		
		
		if ($filter == 2)
		{
			//student
			/*
			$sql = "SELECT  
							IF(a.ClassName IS NULL OR a.ClassName='', '".$Lang['General']['EmptySymbol']."', a.ClassName) as ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber='', '".$Lang['General']['EmptySymbol']."', a.ClassNumber) as ClassNumber,
							Concat(
									$name_field,
									IF (iu.ClassName Is Null Or iu.ClassName='' Or iu.ClassNumber Is Null Or iu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', '')
							) as StudentName,
							IF(b.RoleTitle IS NULL, '".$Lang['General']['EmptySymbol']."', b.RoleTitle) as RoleTitle,
			                IF(b.Performance IS NULL, '".$Lang['General']['EmptySymbol']."', b.Performance) as Performance,
			                IF(b.CommentStudent IS NULL, '".$Lang['General']['EmptySymbol']."', b.CommentStudent) as Comment,
			                IF(b.isActiveMember IS NULL,
			                	'".$Lang['General']['EmptySymbol']."',
			                	IF (b.isActiveMember = '1', '".$eEnrollment['active']."', '".$eEnrollment['inactive']."')
		                	) as ActiveMemberStatus,
			              	CONCAT('<input type=checkbox name=UID[] value=', a.UserID ,'>')
			        FROM 
			        		INTRANET_USER as a 
			        		LEFT OUTER JOIN
			        		INTRANET_ENROL_EVENTSTUDENT as b ON a.UserID = b.StudentID
			        WHERE 
			        		b.EnrolEventID = '$EnrolEventID' 
			        		AND 
			        		b.RecordStatus = 2
			        		AND 
			        		a.RecordType = 2
			        		$conds    
			        ";	
			*/
			
//			if($libenroll->enableActivitySurveyFormRight()){	
//				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus","SurveyResult", "iu.UserID");
//			}else{
//				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus","iu.UserID");
//			}
			
			
//			if($libenroll->enableActivitySurveyFormRight() && !$libenroll->enableActivityFillInEnrolReasonRight()){	
//				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus","SurveyResult", "iu.UserID");
//			
//			}elseif(!$libenroll->enableActivitySurveyFormRight() && $libenroll->enableActivityFillInEnrolReasonRight()){	
//				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus","Reason", "iu.UserID");
//				
//			}elseif($libenroll->enableActivitySurveyFormRight() && $libenroll->enableActivityFillInEnrolReasonRight()){
//				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus","SurveyResult","Reason", "iu.UserID");
//			}
//			else{
//				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus","iu.UserID");
//			}
			
			if ($libenroll->enableUserJoinDateRange()) {
				$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "EnrolAvailiableDateStart", "EnrolAvailiableDateEnd", "Performance", "Achievement", "Comment","ActiveMemberStatus");
			} else {
				$li->field_array = array("ClassName", "ClassNumber", "StudentName");
				if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
					$li->field_array[] = "NickName";
				}
				$li->field_array[] = "RoleTitle";
				$li->field_array[] = "Performance";
				$li->field_array[] = "Achievement";
				$li->field_array[] = "Comment";
				$li->field_array[] = "ActiveMemberStatus";
			}
			if ($libenroll->enableActivitySurveyFormRight()) {
				$li->field_array[] = "SurveyResult";
			}
			if ($libenroll->enableActivityFillInEnrolReasonRight()) {
				$li->field_array[] = "Reason";
			}
			$li->field_array[] = "iu.UserID";
			
// Omas - case#Z42562			
//			if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
//				$li->field_array = array_merge($li->field_array, array("ECA","SS","CS"));	
//			}

			$li->fieldorder2 = ",ClassName, iu.ClassNumber, iees.EventStudentID";
		}
		else
		{
			//staff
			/*
			$sql = "SELECT  
							$name_field as StudentName,
							b.StaffType as RoleTitle,
			              	CONCAT('<input type=checkbox name=UID[] value=', a.UserID ,'>')
			        FROM 
			        		INTRANET_USER as a 
			        	LEFT OUTER JOIN 
			        		INTRANET_ENROL_EVENTSTAFF as b ON a.UserID = b.UserID
			        WHERE 
			        		b.EnrolEventID = '$EnrolEventID'
			        		AND 
			        		a.RecordType = '$filter'  and a.ClassName Is Not Null and a.ClassNumber Is Not Null
			        		$conds    
			        ";
			*/
			$li->field_array = array("StudentName", "RoleTitle", "iu.UserID");
			$li->fieldorder2 = ", StudentName";
		}
// 		debug_pr($EnrolEventID);
		$sql = $libenroll->Get_Management_Activity_Participant_Sql(array($EnrolEventID), array(2), $filter, $sqlkeyword, $ForExport=0, $AcademicYearID, $approvedByType);
		### Record the sql for export first	
//		$li->sql = $libenroll->Get_Management_Activity_Participant_Sql(array($EnrolEventID), array(2), $filter, $keyword, $ForExport=1, $AcademicYearID, $approvedByType);
//		$ExportSQLTemp = $li->built_sql();
//		$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
//		$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);
		
		// Restore to display SQL
		$li->sql = $sql;
//		$li->no_col = sizeof($li->field_array)+2;
//		$li->no_col = sizeof($li->field_array)+1;
		$li->no_col =1;
		
		$li->title = "";
		$li->column_array = array(0,0,0,0,0,0,0,18,18,0);
		$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0);
// 		$li->IsColOff = 2;
		$li->IsColOff = "Enrolment_Display_Club";
		
		//edited by Ivan (17 July 2008) deleting the loginID and showing the class number first
		// TABLE COLUMN
		if ($filter == 2)
		{
			$col = 0;
			$li->column_list .= "<th width=\"1%\"><span class=\"tabletoplink\">#</span></th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"5%\" nowrap>".$li->column($col, $i_general_class)."</th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"5%\" nowrap>".$li->column($col++, $i_ClassNumber)."</th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"10%\" nowrap>".$li->column($col++, $i_UserStudentName)."</th>\n";
			$li->no_col++;
			
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
				$li->column_list .= "<th width=\"10%\">".$li->column($col++, $i_UserNickName)."</th>\n";
				$li->no_col++;
			}
			
			if($sys_custom['eEnrolment']['ShowStudentGender'] == true){
			    $li->column_list .= "<th width=\"10%\">".$li->column($col++, $Lang['General']['Sex'])."</th>\n";
			    $li->no_col++;
			}
			
			$li->column_list .= "<th width=\"10%\">".$li->column($col++, $i_admintitle_role)."</th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"5%\" nowrap>". $eEnrollment['attendence'] ."</th>\n";
			$li->no_col++;
			
			if ($libenroll->enableUserJoinDateRange()) {
				$li->column_list .= "<th width=\"6%\">".$li->column($col++, $Lang['eEnrolment']['AvailiableDateStart'])."</th>\n";
				$li->no_col++;
				$li->column_list .= "<th width=\"6%\">".$li->column($col++, $Lang['eEnrolment']['AvailiableDateEnd'])."</th>\n";
				$li->no_col++;
			}
			$li->column_list .= "<th width=\"10%\">".$li->column($col++, $eEnrollment['performance'])."</th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"10%\">".$li->column($col++, $Lang['eEnrolment']['Achievement'])."</th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"10%\" nowrap>".$li->column($col++, $eEnrollment['teachers_comment'])."</th>\n";
			$li->no_col++;
			
			$li->column_list .= "<th width=\"9%\" nowrap>".$li->column($col++, $eEnrollment['active']."/".$eEnrollment['inactive'])."</th>\n";
			$li->no_col++;
			
			if($libenroll->enableActivitySurveyFormRight()){
				$li->column_list .= "<th width=\"9%\">".$li->column($col++, $Lang['eSurvey']['SurveyResult'])."</th>\n";
				$li->no_col++;
			}
			
			
			if($libenroll->enableActivityFillInEnrolReasonRight()){
				$li->column_list .= "<th width=\"9%\">".$li->column($col++, $Lang['eEnrolment']['Reason'])."</th>\n";
				$li->no_col++;
			}
			
		

// Omas - case#Z42562			
//			if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
//				$li->column_list .= "<th width=\"2%\">".$Lang['eEnrolment']['ECA']."</th>\n";
//				$li->column_list .= "<th width=\"2%\">".$Lang['eEnrolment']['SS']."</th>\n";
//				$li->column_list .= "<th width=\"2%\">".$Lang['eEnrolment']['CS']."</th>\n";
//			}
			
			if($libenroll->enableActivityRecordMeritInfoRight()){
				include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
				$lstudentprofile = new libstudentprofile();
				if (!$lstudentprofile->is_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_Merit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_MinorCredit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_MajorCredit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_SuperCredit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_UltraCredit)."</th>\n";
					$li->no_col++;
				}
			}
			
		

			$li->column_list .= "<th width=\"2%\">".$li->check("UID[]")."</th>\n";
		}
		else
		{
		    $col = 0;
		  
			$li->column_list .= "<th width=\"1\"><span class=\"tabletoplink\">#</span></th>\n";
			$li->no_col++;
			$li->column_list .= "<th width=\"49%\">".$li->column($col++, $i_UserName)."</th>\n";
			$li->no_col++;
			$li->column_list .= "<th width=\"50%\">".$li->column($col++, $i_admintitle_role)."</th>\n";
			$li->no_col++;
// 			$li->column_list .= "<th width=\"49%\">".$li->column(0, $i_UserName)."</th>\n";
// 			$li->column_list .= "<th width=\"50%\">".$li->column(1, $i_admintitle_role)."</th>\n";
			$li->column_list .= "<th>".$li->check("UID[]")."</th>\n";
			$li->no_col++;
		}
		
		
		if ($msg == 1)
		{
			$response_msg = $linterface->GET_SYS_MSG("add");
		}
		if ($msg == 2)
		{
		     //$response_msg = $i_Discipline_System_alert_Approved;
		     $response_msg = $linterface->GET_SYS_MSG("update");
		}
		if ($msg == 4)
		{
		     $response_msg = $linterface->GET_SYS_MSG("", $eEnrollment['approve_quota_exceeded']);
		}
						
		if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
		if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
		if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
		if ($msg == 4) $SysMsg = $linterface->GET_SYS_MSG("",$i_con_msg_email);
		
		
		$linterface->LAYOUT_START();
		
		$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
		
		
		if($sys_custom['eEnrolment']['ActivityAddRole']){
			$libgroup = new libgroup($GroupID);
			$row = $libgroup->returnGroupRoleType();
			
			$RoleAlertMessage = $Lang['eEnrolment']['selectRoleAlertMsg'];
			$RoleOptions = '';
			if ($row!=NULL){
				$RoleOptions = "<select id=\"RoleTitle\" name=\"RoleTitle\">\n";
				$RoleOptions .= "<option value='' $selected>".$eEnrollment['select_role']."</option>\n";
				for($i=0; $i<sizeof($row); $i++)
//					$RoleOptions .= (Isset($RoleTitle) && $RoleTitle!="") ? "<option value=".$row[$i][1]." ".(($row[$i][1]==$RoleTitle)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][1]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
					$RoleOptions .= (Isset($RoleTitle) && $RoleTitle!="") ? "<option value=".$row[$i][0]." ".(($row[$i][1]==$RoleTitle)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
					$RoleOptions .= "</select>\n";
					$RoleOptions .= toolBarSpacer();
			}
			//$RoleOptions .= $linterface->GET_LNK_NEW("javascript:add_role(document.form1)", '', '', '', '', 0);
		}else{
			$RoleAlertMessage = $eEnrollment['js_enter_role'];
			$RoleTitleValue = $RoleTitle==""?$eEnrollment['enter_role']:stripslashes($RoleTitle);
			$RoleOptions ='<input name="RoleTitle" type="text" class="textboxnum" value="'.$RoleTitleValue.'" onFocus="SearchTextFocus=true;if(this.value==\''.$eEnrollment['enter_role'].'\'){this.value=\'\'}" onBlur="SearchTextFocus=false;if(this.value==\'\'){this.value=\''.$eEnrollment['enter_role'].'\'}" />';
		}
?>

<script language="javascript">

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}
		
function checkForm()
{
	var obj = document.form1;
		
	if (obj.RoleTitle.value == '<?=$eEnrollment['enter_role']?>' || obj.RoleTitle.value == '')
	{
		alert("<?=$RoleAlertMessage?>");
		obj.RoleTitle.focus();
		return false;
	}
	else
	{
		checkRole(obj,'UID[]','member_role.php');
	}
}

function js_Go_Export()
{
	document.getElementById('type').value = 4;
	
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'export.php';
	jsFormObj.submit();
	
	document.getElementById('type').value = 'activity';
}

function print_member_list()
{
	newWindow('event_print_member_list.php?EnrolEventID=<?=$EnrolEventID?>&filter=<?=$filter?>',10);
}
		
<?php if($libenroll->enableActivitySurveyFormRight()){ ?>
	    function view_result(id)
		{
			newWindow('survey_result.php?RecordType=A&AnswerID='+id,1);
		}
		
		
		function js_Go_ExportSurveyResult()
		{	
			var jsObjForm = document.getElementById('form1');
			jsObjForm.action = 'export_survey_result.php';
		    jsObjForm.submit();
		    
		    jsObjForm.action = '';
		}
		

<? } ?>

function changedApprovedByFilterSelection() {
	document.form1.target = '_self';
	document.form1.action = 'event_member_index.php';
	document.form1.submit();
}

</script>


		<br />
		<table width="100%" border="0" cellspacing="4" cellpadding="4">
			<tr><td>
				<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
			</td></tr>
		</table>

		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr><td><?=$quotaDisplay?></td></tr>
			<tr><td height="5"></td></tr>
			<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		</table>
		<br />

		<form id="form1" name="form1" ACTION="" METHOD="POST">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left">	
						<?= $toolbar1?>	
					</td>
					<td align="right">
						<?= $SysMsg ?>
					</td>
				</tr>
				<tr>
					<td align="left">	
						<?= $toolbar2?>	
					</td>
					<td align="right">
					<div class="Conntent_search">
						<?=$searchTag?>
					</div>
					</td>
				</tr>
				<tr>
					<td align="left">
						<?= $toolbar3?>
					</td>
					<td align="right">
					<?if($libenroll->AllowToEditPreviousYearData){?>
						<?= $RoleOptions ?>
						<?= $functionbar?>
					<?}?>
					</td>
				</tr>
				<tr>
					<td align="left">
						<?= $filterbar ?>
						<?= toolBarSpacer() ?>
					</td>
					<td align="right">
					<?if($libenroll->AllowToEditPreviousYearData){?>
							<?= $groupadminfunctionbar?>
					<?}?>

	    			</td>
	  				
				</tr>
			</table>
		
<?=$li->display("100%","","",$filter)?>
<?=$RemarksTable?>

<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?// $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='event.php?AcademicYearID=$AcademicYearID'")?>
</div>
</td></tr>
</table>

<br />

<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" id="type" name="type" value="activity" />


</form>

<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>