<?php

## Using By :      
###########################################
##	Modification Log:
##
##  2019-09-12 Cameron
##  - unlimit time and memory for HKPF
##
##  2018-12-18 Cameron 
##  - allow Qualified Instructor in Settings > Category to view the course with those categories
##
##  2018-11-26 Anna [#H153428]
##  - Fixed no HKPF flag cannot show category 
##
##  2018-10-15 Cameron
##  - concate class name by course to show  
##
##  2018-09-07 Ivan
##  - add Intake and Class filter
##  - fix to retrieve Intake Title and ClassName column for non-admin user
##
##  2018-09-06 Cameron
##  - add Intake Title and ClassName for HKPF
##
##  2018-08-20 Cameron
##  - add more details to the warning of deleting course 
##
##  2018-08-15 Cameron
##  - fix: $nrColOffset for HKPF, one column for sequence #, one for scheduel, so it should be 2 offset columns
#3
##  2018-08-13 Cameron
##  - allow instructor (non-admin staff) to manage course schedule on himself/herself
##
##  2018-08-08 Cameron
##  - add $returnMsg to show update status for MeetingDate update
##
##  2018-08-06 Cameron
##  - hide items for HKPF
##  - change checkRemove to enrolCheckRemove so that it can check if there's booking before process delete for HKPF
##
##  2017-12-14 Pun
##  - added ncs cust
##
##  2017-08-14 (Omas) #P110324
##  - modified ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
##
##	2017-06-19 Villa
##	- Modified ReplySlip ThickBox to dynamic
##
##	2017-06-02 Anna
##	- Modified export/export_activity_info/export_attendance_record
##
##	2017-04-24 Villa #P110324 
##	- Add Reply Slip Related
##
##	2015-10-27 Omas
##	- fix missing attendace column for $sys_custom['eEnrolment']['TWGHCYMA'] - #J88130 
##
##	2015-06-16 Omas
##	- Add column for handling payment $get_payment_icon
##
##	2015-06-08 Evan
##	- Fixed the problem in searching symbols
##
##	2014-07-23 Bill
##	- Added column for group that activity belongs to
##
##	2014-04-07 Carlos
##	- Added customization $sys_custom['eEnrolment']['TWGHCYMA'] for TWGHs C Y MA MEMORIAL COLLEGE : display multiple categories
##
##	2013-05-31 Rita
##	add view and edit active member btn
##
##	2012-12-04 Rita
##	- add survey form result display for customization [Case#2012-0803-1543-49054] enableActivitySurveyFormRight()
##
##  2012-11-06 Rita 
##	- add export attendance record btn
##
##	2012-02-27	Marcus
##	- Commented UPDATE_EVENTS_APPROVED, for performance issue
## 
##	2011-09-07	YatWoon
##	- lite version implementation
## 
##	2010-12-03	YatWoon
##	- $sys_custom['YeoCheiMan_generate_performance']
##	- IP25 UI standard
##
##  20100802 Thomas
##  - Check the data is freezed or not
##    Show alert message when adding / updating the data
##
##	2010-01-28 Max (201001281139)
##	- Moved the search bar
###########################################

// ini_set('display_errors',1);
// error_reporting(E_ALL ^ E_NOTICE);
set_time_limit(0);
ini_set('memory_limit',-1);

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");

if ($sys_custom['project']['HKPF']) {
	if (isset($_GET['IntakeID'])) {
		$intakeID = IntegerSafe($_GET['IntakeID']);
	}
	if (isset($_GET['IntakeClassID'])) {
		$intakeClassID = IntegerSafe($_GET['IntakeClassID']);
	}
}
$arrCookies = array();
$arrCookies[] = array("enrol_course_list_intakeID", "intakeID");
$arrCookies[] = array("enrol_course_list_intakeClassID", "intakeClassID");
$clearCoo = IntegerSafe($clearCoo);
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "includes/liburlparahandler.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$AcademicYearID = IntegerSafe($AcademicYearID);
if (!isset ($AcademicYearID) || $AcademicYearID == "") {
	$AcademicYearID = Get_Current_Academic_Year_ID();
}

$AcademicYearID = IntegerSafe($AcademicYearID);
$sel_category = IntegerSafe($sel_category);


$libenroll = new libclubsenrol($AcademicYearID);
$lurlparahandler = new liburlparahandler('', '', $enrolConfigAry['encryptionKey']);

$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

if (!$plugin['eEnrollment']) {
	No_Access_Right_Pop_Up();
}

if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset ($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if ($field == "")
	$field = 0;

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment']) {
    $isInstructor = $libenroll->isInstructor();
    if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_EVENT_PIC()) && (!$libenroll->IS_CLUB_PIC()) && (!$libenroll->IS_EVENT_HELPER()) && (!$libenroll->IS_CLUB_HELPER()) && !$isInstructor)
		header("Location: " .
		$PATH_WRT_ROOT . "home/eService/enrollment/");
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent()) || ($LibUser->isParent())) {
		if ($helper_view)
			$CurrentPage = "PageActAttendanceMgt";
		$CurrentPageArr['eEnrollment'] = 0;
		$CurrentPageArr['eServiceeEnrollment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

	# Acadermic Year Selection        	
	$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'event.php\';document.form1.submit();"', 1, 0, $AcademicYearID);

	//	$libenroll->UPDATE_EVENTS_APPROVED();

	if ($libenroll->hasAccessRight($_SESSION['UserID'])) {
		include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

		$linterface = new interface_html();

		# tags 
		$tab_type = "activity";
		$current_tab = 1;
		include_once ("management_tabs.php");

		### Cookies handling
		$arrCookies = array ();
		$arrCookies[] = array (
			"eEnrol_management_ActivityIndex_PicView",
			"pic_view"
		);
		$arrCookies[] = array (
			"eEnrol_management_ActivityIndex_HelperView",
			"helper_view"
		);
		$arrCookies[] = array (
			"eEnrol_management_ActivityIndex_Keyword",
			"keyword"
		);
		$arrCookies[] = array (
			"eEnrol_management_ActivityIndex_CategoryID",
			"sel_category"
		);

		if (isset ($clearCoo) && $clearCoo == 1) {
			clearCookies($arrCookies);
			$pic_view = '';
			$helper_view = '';
		} else {
			updateGetCookies($arrCookies);
		}

		$pic_view = ($_GET['pic_view'] != '' && $_GET['pic_view'] != '') ? $_GET['pic_view'] : $pic_view;
		$pic_view = IntegerSafe($pic_view);
		$helper_view = ($_GET['helper_view'] != '') ? $_GET['helper_view'] : $helper_view;
		$helper_view = IntegerSafe($helper_view);
		$keyword = ($_GET['keyword'] != '') ? $_GET['keyword'] : '';
		$sel_category = ($_GET['sel_category'] != '') ? $_GET['sel_category'] : $sel_category;
		//debug_pr($keyword);
		if ($_GET['pic_view'] != '')
			$helper_view = '';
		if ($_GET['helper_view'] != '')
			$pic_view = '';

	    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
	    $linterface->LAYOUT_START($returnMsg);

		# initialize sorting order of the table - default order is ASC
		if ($order == "" || !isset ($order)) {
			$order = ($order == 0) ? 1 : 0;
		}

		# category filter
		$category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "document.form1.submit()", $eEnrollment['all_categories']);
		if (isset ($sel_category) && $sel_category != "") {
			if ($sys_custom['eEnrolment']['TWGHCYMA']) {
				$category_conds = " AND iec.CategoryID IN ('$sel_category') ";
			} else {
				$category_conds = " AND iec.CategoryID = '$sel_category' ";
			}
		}
		
		if ($sys_custom['project']['HKPF']) {
			include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
		    $libenroll_ui= new libclubsenrol_ui();
		    
			$intake_selection = '';
			$intakeClass_selection = '';
			
			// intake selection
			$firstTitle = 'All Intakes';
		    $intake_selection = $libenroll_ui->getIntakeSelection('IntakeID', $intakeID, $firstTitle);
		    
		    // intake class selection if selected intake
		    if ($intakeID!=''){
		    	$intakeList = $libenroll->getIntakeList($intakeID);
		        $currentIntake = $intakeList[0];
		        $intakeID = $intakeID ? $intakeID : $currentIntake['IntakeID'];
		        $firstTitle = $Lang['SysMgr']['FormClassMapping']['AllClass'];
		        $intakeClass_selection = $libenroll_ui->getClassSelection($intakeID, $intakeClassID, $name='IntakeClassID', $firstTitle);
		    }
		    else {
		        $intakeClass_selection = '';
		    }
		    
		    // SQL statement condition
		    if ($intakeID!='') {
		    	$conds_intakeID = " AND iei.IntakeID = '".$intakeID."' ";
		    }
		    
		    if ($intakeClassID!='') {
		    	//$conds_intakeClassID = " AND ieic.ClassID = '".$intakeClassID."' AND ieeic.IntakeClassID=ieic.ClassID ";
		    	$conds_intakeClassID = " AND ieic.ClassID = '".$intakeClassID."' AND ieeic.EnrolEventID IS NOT NULL ";
		    }
		}

		# Search box
		//		$searchbar = $libenroll->GET_STUDENT_NAME_SERACH_BOX(stripslashes($keyword), "keyword", $eEnrollment['enter_name']);
		// 		$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "javascript:document.form1.keyword.value = Trim(document.form1.keyword.value); document.form1.submit()");
		$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"" . intranet_htmlspecialchars(stripslashes($keyword)) . "\" onkeyup=\"Check_Go_Search(event);\"/>";

		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_name'])
			$keyword = "";

		if ($keyword != '')
			$name_conds = " AND ieei.EventTitle LIKE '%" . $libenroll->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
		//debug_pr($name_conds);
		# Get Current Year Club
		$CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club($AcademicYearID);

		$EnrolGroupID_conds = '';
		if (count($CurrentYearClubArr) > 0) {
			$CurrentYearClubList = implode(',', $CurrentYearClubArr);
			$EnrolGroupID_conds = " Or ieei.EnrolGroupID In ($CurrentYearClubList) ";
		}

		// 		$TableActionArr[] = array(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:enrolCheckRemove(document.form1,'EnrolEventID[]','event_remove.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $button_remove);

		if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) {
			// 			$toolsBar = $libenroll->GET_TABLE_ACTION_BTN($TableActionArr);
			$toolsBar = " <div class=\"common_table_tool\">";
			if (!$sys_custom['project']['HKPF']) {
			    $toolsBar .= " <a href=\"" . ($libenroll->disableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "javascript:Js_View_Active_Member_List(document.form1,'all_active_member.php?AcademicYearID=$AcademicYearID&type=activity')") . "\" class=\"tool_approve\">" . $eEnrollment['button']['active_member'] . "</a>";
			}			
			$toolsBar .= " <a href=\"" . ($libenroll->Event_DisableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "javascript:enrolCheckRemove(document.form1,'EnrolEventID[]','event_remove.php')") . "\" class=\"tool_delete\">" . $button_delete . "</a>
							</div>
						";

			// 			$menuBar = $linterface->GET_LNK_NEW(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "event_setting.php", '', '', '', '', 0);
			if ($libenroll->AllowToEditPreviousYearData) {
				$menuBar = "<a href=\"" . ($libenroll->Event_DisableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "event_setting.php?AcademicYearID=$AcademicYearID") . "\" class=\"new\"> " . $Lang['Btn']['New'] . "</a>";
			}

			if (!$sys_custom['project']['HKPF']) {
    			# import parameters
    			$type = "activityAllParticipant";
    			$import_parameters = "AcademicYearID=$AcademicYearID&type=$type";
    			// 			$menuBar .= $linterface->GET_LNK_IMPORT(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "import.php?".$import_parameters, $eEnrollment['import_participant'], '', '', '', 0);
    
    			# import member
    			$importMember .= "<a href=\"" . ($libenroll->disableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "import.php?" . $import_parameters) . "\" class=\"sub_btn\"> " . $Lang['eEnrolment']['Btn']['ExportMember2'] . "</a>";
    
    			# import activity info
    			$importActivityInfo .= "<a href=\"" . ($libenroll->disableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "import_activity_info.php?" . $import_parameters) . "\" class=\"sub_btn\"> " . $Lang['eEnrolment']['Btn']['ExportActivityInfo2'] . "</a>";
    
    			if ($libenroll->AllowToEditPreviousYearData) {
    				//$menuBar .= "<a href=\"". ($libenroll->Event_DisableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')": "import.php?".$import_parameters) ."\" class=\"import\"> ".$Lang['Btn']['Import']."</a>";
    
    				$menuBar .= '
    										<div class="btn_option"  id="ImportDiv"  >
    						
    											<a onclick="js_Clicked_Option_Layer(\'import_option\', \'btn_import\');" id="btn_import" class="import option_layer" href="javascript:void(0);"> ' . $Lang['Btn']['Import'] . '</a>
    											<br style="clear: both;">
    											<div onclick="js_Clicked_Option_Layer_Button(\'import_option\', \'btn_import\');" id="import_option" class="btn_option_layer" style="visibility: hidden;">
    											' . $importMember . '
    											' . $importActivityInfo . '
    											</div>
    										</div>';
    
    			}
    
    			# export parameters
    			$type = 7;
    		//	$export_parameters = "type=$type&category_conds=$category_conds&name_conds=$name_conds&AcademicYearID=$AcademicYearID;
    			$export_parameters = "type=$type&CategoryID=$sel_category&Keyword=$keyword&AcademicYearID=$AcademicYearID";
    			// 			$menuBar .= $linterface->GET_LNK_EXPORT("export.php?".$export_parameters, '', '', '', '', 0);
    			if ($sys_custom['eEnrolment']['ActivityMemberInfo_ExtraExportInfo']) {
    				$exportFile = 'export_member_info_extra.php';
    				$lurlparahandler->setParaDecrypted($export_parameters . "&recordType=" . $enrolConfigAry['Activity']);
    				$lurlparahandler->performAction(liburlparahandler :: actionEncrypt);
    				$export_parameters = 'p=' . $lurlparahandler->getParaEncrypted();
    			} else {
    				$exportFile = 'export.php';
    			}
    			$exportMember = "<a href=\"" . $exportFile . "?" . $export_parameters . "\" class=\"sub_btn\"> " . $Lang['eEnrolment']['Btn']['ExportMember2'] . "</a>";
    
    			# export activities information
    //			$exportActivityInfo = "<a href=\"export_activity_info.php?AcademicYearID=$AcademicYearID\" class=\"sub_btn\"> " . $Lang['eEnrolment']['Btn']['ExportActivityInfo2'] . "</a>";
    			$exportActivityInfo = "<a href=\"export_activity_info.php?AcademicYearID=$AcademicYearID&CategoryID=$sel_category&Keyword=$keyword\" class=\"sub_btn\"> " . $Lang['eEnrolment']['Btn']['ExportActivityInfo2'] . "</a>";
    				
    			# export attendance records
    		//	$exportAttendanceRecord = "<a href=\"export_attendance_record.php?AcademicYearID=$AcademicYearID&type=3&CategoryID=$CategoryID\" class=\"sub_btn\"> " . $i_Profile_Attendance . "</a>";
    			$exportAttendanceRecord = "<a href=\"export_attendance_record.php?AcademicYearID=$AcademicYearID&type=3&CategoryID=$sel_category&Keyword=$keyword\" class=\"sub_btn\"> " . $i_Profile_Attendance . "</a>";
    				
    			# export
    			$menuBar .= '
    							<div class="btn_option" id="ExportDiv">
    								<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> ' . $Lang['Btn']['Export'] . '</a>
    								<br style="clear: both;">
    								<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
    								' . $exportMember . '
    								' . $exportActivityInfo . '
    								' . $exportAttendanceRecord . '
    								</div>
    							</div>
    						';
			}
			
			# show Checkboxes
			$showCheckbox = true;

			if (!$plugin['eEnrollmentLite']) {
				if($sys_custom['eEnrolment']['TWGHCYMA']){
					$attendance_icon = "CONCAT('<a href=\"event_attendance_mgt.php?AcademicYearID=$AcademicYearID&EnrolEventID=', ieei.EnrolEventID ,'&sel_category=', '' ,'\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_viewstat.gif\" border=\"0\"></a>'),";
				}
				else{
					$attendance_icon = "CONCAT('<a href=\"event_attendance_mgt.php?AcademicYearID=$AcademicYearID&EnrolEventID=', ieei.EnrolEventID ,'&sel_category=', iec.CategoryID ,'\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_viewstat.gif\" border=\"0\"></a>'),";
				}
				
			} else {
				$attendance_icon = "CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_viewstat_dim.gif\" border=\"0\">'),";
			}
			
			// Omas Payment SQL
			if (!$helper_view && $plugin['payment']){
				$get_payment_icon = "IF(Count(iu.UserID) > 0,
										IF(ieei.AddToPayment = 1, 
											/*Paid*/ '".$eEnrollment['processed']."',
											/*Not Yet Paid*/
											IF( ieei.PaymentAmount = 0 OR ieei.isAmountTBC = 1,
												/*No Need to Pay*/ '".$Lang['General']['EmptySymbol']."',
												/*Need to Pay*/
												CONCAT( '<a href=\"javascript:PaymentConfirm(document.form1,' , ieei.EnrolEventID , ')\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment.gif\" border=\"0\" alt=\"".$eEnrollment['payment']."\"></a>'  )
											)
										  ),
										/* No member no need to pay*/
										'".$Lang['General']['EmptySymbol']."'
									)as PaymentDisplay,";
				if($plugin['eEnrollmentLite']){
					$get_payment_icon = "IF(Count(iu.UserID) > 0,
											IF(ieei.AddToPayment = 1, 
												/*Paid*/ '".$eEnrollment['processed']."',
												/*Not Yet Paid*/
												IF( ieei.PaymentAmount = 0 OR ieei.isAmountTBC = 1,
													/*No Need to Pay*/ '".$Lang['General']['EmptySymbol']."',
													/*Need to Pay*/
													CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment_dim.gif\" border=\"0\">')
												)
											),
											/* No member no need to pay*/
											'".$Lang['General']['EmptySymbol']."'
										  ) as PaymentDisplay,";
				}
			}else{
				$get_payment_icon = "";
			}
			if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] != ""){
				$get_payment_icon = "";
			}
			
			//			# Survey Form Customization
			//			if($libenroll->enableActivitySurveyFormRight()){
			//				$surveyForm_cond = "CONCAT('<a href=javascript:viewSurvey(', a.SurveyID, ')>', 
			//	       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>'),";
			//			}
			//			

			// To query related club name of each activity
			$name = $intranet_session_language == "en" ? "Title" : "TitleChinese";
			$targetClubSql = " IF(ig.$name='' OR ig.$name is null,'-',ig.$name) as RelatedGroup,";
			$targetClubTable = " Left Outer Join INTRANET_ENROL_GROUPINFO as iegi On (ieei.EnrolGroupID = iegi.EnrolGroupID)
											 Left Outer Join INTRANET_GROUP as ig On (iegi.GroupID = ig.GroupID)";

			$sql = "
								SELECT
										IF(ieei.ActivityCode='' OR ieei.ActivityCode is null,'-',ieei.ActivityCode) as ActivityCode,
										CONCAT('<a href=\"event_setting.php?AcademicYearID=$AcademicYearID&EnrolEventID=', ieei.EnrolEventID ,'\" class=\"tablelink\">', ieei.EventTitle, '</a>'), $targetClubSql
										/* CONCAT('<a href=\"event_new2.php?AcademicYearID=$AcademicYearID&EnrolEventID=',ieei.EnrolEventID,'\"><img height=\"18\" width=\"18\" border=\"0\" src=\"{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif\"></a>'), */
										CONCAT('<a href=\"meeting_date.php?AcademicYearID=$AcademicYearID&recordId=',ieei.EnrolEventID,'&recordType=" . $enrolConfigAry['Activity'] . "\"><img height=\"18\" width=\"18\" border=\"0\" src=\"{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif\"></a>'),";
			if ($sys_custom['eEnrolment']['TWGHCYMA']) {
				$sql .= " GROUP_CONCAT(DISTINCT iec.CategoryName SEPARATOR ', ') as CategoryName, ";
			} else {
				$sql .= "	iec.CategoryName,";
			}
			#ReplySlip #P110324
			if($sys_custom['eEnrolment']['ReplySlip']){
				$EnrolEventID_ReplySlip = 'ieei.EnrolEventID,';
			}else{
				$EnrolEventID_ReplySlip= '';
			}

			if (!$sys_custom['project']['HKPF']) {
			    $sql .= "		IF (ieei.Quota = 0, '$i_ClubsEnrollment_NoLimit', ieei.Quota),
										CONCAT('<a href=\"event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=', ieei.EnrolEventID ,'\" class=\"tablelink\">', Count(DISTINCT iu.UserID), '</a>'),
										$attendance_icon
										$get_payment_icon
										$EnrolEventID_ReplySlip
										$surveyForm_cond ";
			}
			else {
			    $sql .= " IFNULL(iei.Title,'-') AS Intake, ";
			    $sql .= " IFNULL(GROUP_CONCAT(DISTINCT IF(ieeic.IntakeClassID IS NULL, NULL, ieic.ClassName) ORDER BY ieic.ClassName SEPARATOR ', '),'-') AS ClassName, ";
			}
			
			$sql .= "				    CONCAT('<input type=checkbox name=\"EnrolEventID[]\" value=\"', ieei.EnrolEventID ,'\">')	,
										Count(DISTINCT iu.UserID) as NumMember			
								FROM
										INTRANET_ENROL_EVENTINFO as ieei ";
			if ($sys_custom['eEnrolment']['TWGHCYMA']) {
				$sql .= "	LEFT JOIN INTRANET_ENROL_EVENT_CATEGORY as ec ON ec.EnrolEventID=ieei.EnrolEventID 
											LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON ec.CategoryID=iec.CategoryID ";
			} else {
				$sql .= "	Inner Join INTRANET_ENROL_CATEGORY as iec ON (ieei.EventCategory = iec.CategoryID) ";
			}
			
			if ($sys_custom['project']['HKPF']) {
			    $sql .= " LEFT JOIN INTRANET_ENROL_GROUPINFO ieg ON ieg.EnrolGroupID=ieei.EnrolGroupID 
                          LEFT JOIN INTRANET_ENROL_GROUPINFO_MAIN iegm ON iegm.GroupID=ieg.GroupID
                          LEFT JOIN INTRANET_ENROL_INTAKE iei ON iei.IntakeID=iegm.IntakeID
			              LEFT JOIN INTRANET_ENROL_INTAKE_CLASS ieic ON ieic.IntakeID=iei.IntakeID 
                          LEFT JOIN INTRANET_ENROL_EVENT_INTAKE_CLASS ieeic ON ieeic.EnrolEventID=ieei.EnrolEventID AND ieeic.IntakeClassID=ieic.ClassID ";
			}
			
			$sql .= "		Left Outer Join INTRANET_ENROL_EVENTSTUDENT as iees On (ieei.EnrolEventID = iees.EnrolEventID And iees.RecordStatus = 2)
										Left Outer Join INTRANET_USER as iu On (iees.StudentID = iu.UserID And iu.RecordType = 2)
										$targetClubTable
								WHERE 
										(ieei.EnrolGroupID = '' Or ieei.EnrolGroupID Is Null $EnrolGroupID_conds)
										AND ieei.AcademicYearID='$AcademicYearID'
										$category_conds 
										$name_conds
										$conds_intakeID
										$conds_intakeClassID
								Group By
										ieei.EnrolEventID
							";
		//		debug_pr($sql);	
		} else {      // non-admin user and non-enrol master

			$toolsBar = "&nbsp;";
			$menuBar = "&nbsp;";
			if ($pic_view) {
				# student PIC
				$StaffType_conds = " And (iees.StaffType = 'PIC' Or iegs.StaffType = 'PIC') ";

				# PIC can create / delete activity
				// 				$toolsBar = $libenroll->GET_TABLE_ACTION_BTN($TableActionArr);
				$toolsBar = "
									<div class=\"common_table_tool\">
									<a href=\"" . ($libenroll->Event_DisableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "javascript:enrolCheckRemove(document.form1,'EnrolEventID[]','event_remove.php')") . "\" class=\"tool_delete\">" . $button_delete . "</a>
									</div>
								";

				//$menuBar = $linterface->GET_LNK_NEW(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "event_setting.php", '', '', '', '', 0);
				if ($libenroll->AllowToEditPreviousYearData) {
					$menuBar = "<a href=\"" . ($libenroll->Event_DisableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "event_setting.php?AcademicYearID=$AcademicYearID") . "\" class=\"new\"> " . $Lang['Btn']['New'] . "</a>";
				}

				# With Checkboxes
				$showCheckbox = true;
				$checkboxSQL = "CONCAT('<input type=checkbox name=\"EnrolEventID[]\" value=\"', ieei.EnrolEventID ,'\">'),";
			} else
				if ($helper_view) {
					# student Helper
					$StaffType_conds = " And (iees.StaffType = 'HELPER' Or iegs.StaffType = 'HELPER') ";

					# Without Checkboxes
					$showCheckbox = false;
					$checkboxSQL = '';
				} else {
					# staff PIC / Helper
					$StaffType_conds = " And (iees.StaffType = 'PIC' Or iegs.StaffType = 'PIC' Or iees.StaffType = 'HELPER' Or iegs.StaffType = 'HELPER') ";

					# PIC can create / delete activity
					// 				$toolsBar = $libenroll->GET_TABLE_ACTION_BTN($TableActionArr);
					if (!$isInstructor) {
    					$toolsBar = "
    										<div class=\"common_table_tool\">
    										<a href=\"" . ($libenroll->Event_DisableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "javascript:enrolCheckRemove(document.form1,'EnrolEventID[]','event_remove.php')") . "\" class=\"tool_delete\">" . $button_delete . "</a>
    										</div>
    									";
					}
					$PIC_EnrolGroupIDArr = $libenroll->Get_PIC_Club($_SESSION['UserID']);
					if (sizeof($PIC_EnrolGroupIDArr) > 0 || $libenroll->clubPICcreateNAAct) {
						//$menuBar = $linterface->GET_LNK_NEW(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "event_setting.php", '', '', '', '', 0);
						if ($libenroll->AllowToEditPreviousYearData) {
							$menuBar = "<a href=\"" . ($libenroll->Event_DisableUpdate == 1 ? "javascript:alert('" . $Lang['eEnrolment']['disableUpdateAlertMsg'] . "')" : "event_setting.php?AcademicYearID=$AcademicYearID") . "\" class=\"new\"> " . $Lang['Btn']['New'] . "</a>";
						}
					}

					# With Checkboxes
					$showCheckbox = true;
					$checkboxSQL = " 
													CONCAT(	'<input type=checkbox name=\"EnrolEventID[]\" value=\"', ieei.EnrolEventID ,'\" ',
															IF (iees.StaffType = 'PIC' Or iegs.StaffType = 'PIC',
																CONCAT(''),
																CONCAT(' disabled ')
															),
															'>'
													),";
				}

			//$CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club_EnrolGroupID();
			//$numOfCurYearClub = count($CurrentYearClubArr);
			//$CurYearClub_conds = '';
			//if ($numOfCurYearClub > 0)
			//{
			//	$CurrentYearClubList = implode(',', $CurrentYearClubArr);
			//	$CurYearClub_conds = " Or iegi.EnrolGroupID In (".$CurrentYearClubList.") ";
			//}

			if (!$plugin['eEnrollmentLite'])
				$attendance_icon = "CONCAT('<a href=\"event_attendance_mgt.php?EnrolEventID=', ieei.EnrolEventID ,'\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_viewstat.gif\" border=\"0\"></a>'),";
			else
				$attendance_icon = "CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_viewstat_dim.gif\" border=\"0\">'),";
			
			// Omas Payment SQL
			if (!$helper_view &&$plugin['payment']){
				$get_payment_icon = "IF(Count(iu.UserID) > 0,
										IF(ieei.AddToPayment = 1, 
											/*Paid*/ '".$eEnrollment['processed']."',
											/*Not Yet Paid*/
											IF( ieei.PaymentAmount = 0 OR ieei.isAmountTBC = 1,
												/*No Need to Pay*/ '".$Lang['General']['EmptySymbol']."',
												/*Need to Pay*/
												CONCAT( '<a href=\"javascript:PaymentConfirm(document.form1,' , ieei.EnrolEventID , ')\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment.gif\" border=\"0\" alt=\"".$eEnrollment['payment']."\"></a>'  )
											)
										  ),
										/* No member no need to pay*/
										'".$Lang['General']['EmptySymbol']."'
									)as PaymentDisplay,";
				if($plugin['eEnrollmentLite']){
					$get_payment_icon = "IF(Count(iu.UserID) > 0,
											IF(ieei.AddToPayment = 1, 
												/*Paid*/ '".$eEnrollment['processed']."',
												/*Not Yet Paid*/
												IF( ieei.PaymentAmount = 0 OR ieei.isAmountTBC = 1,
													/*No Need to Pay*/ '".$Lang['General']['EmptySymbol']."',
													/*Need to Pay*/
													CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment_dim.gif\" border=\"0\">')
												)
											),
											/* No member no need to pay*/
											'".$Lang['General']['EmptySymbol']."'
										  ) as PaymentDisplay,";
				}
			}else{
				$get_payment_icon = "";
			}
			if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] != ""){
				$get_payment_icon = "";
			}
			
			// To query related club name of each activity
			$name = $intranet_session_language == "en" ? "Title" : "TitleChinese";
			$targetClubSql = " IF(ig.$name='' OR ig.$name is null,'-',ig.$name) as RelatedGroup,";
			$targetClubTable = " Left Outer Join INTRANET_GROUP as ig On (iegi.GroupID = ig.GroupID)";
			
			#ReplySlip #P110324 
			if($sys_custom['eEnrolment']['ReplySlip']){
				$EnrolEventID_ReplySlip = 'ieei.EnrolEventID,';
			}else{
				$EnrolEventID_ReplySlip= '';
			}

			$sql = "Select
										IF(ieei.ActivityCode='' OR ieei.ActivityCode is null,'-',ieei.ActivityCode) as ActivityCode,";
			if ($isInstructor) {
			    $sql .= "ieei.EventTitle, $targetClubSql
			    CONCAT('<a href=\"meeting_date.php?AcademicYearID=$AcademicYearID&recordId=',ieei.EnrolEventID,'&recordType=" . $enrolConfigAry['Activity'] . "\"><img height=\"18\" width=\"18\" border=\"0\" src=\"{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif\"></a>'),";
			    
			    $instructorSql = " 
									LEFT JOIN INTRANET_ENROL_GROUPINFO ieg ON ieg.EnrolGroupID=ieei.EnrolGroupID 
			                        LEFT JOIN INTRANET_ENROL_GROUPINFO_MAIN iegm ON iegm.GroupID=ieg.GroupID
			                        LEFT JOIN INTRANET_ENROL_INTAKE iei ON iei.IntakeID=iegm.IntakeID
						            LEFT JOIN INTRANET_ENROL_INTAKE_CLASS ieic ON ieic.IntakeID=iei.IntakeID
                                    LEFT JOIN INTRANET_ENROL_EVENT_INTAKE_CLASS ieeic ON ieeic.EnrolEventID=ieei.EnrolEventID AND ieeic.IntakeClassID=ieic.ClassID 
									LEFT JOIN 
                                        	INTRANET_ENROL_EVENT_DATE ieed ON ieed.EnrolEventID=ieei.EnrolEventID
                                  	LEFT JOIN
                                        	INTRANET_ENROL_EVENT_DATE_TRAINER ieedt ON ieedt.EventDateID=ieed.EventDateID 
                                    LEFT JOIN
                                            INTRANET_ENROL_CATEGORY_INSTRUCTOR ieci ON ieci.CategoryID=ieei.EventCategory";
			    $instructorCond = " AND (ieedt.TrainerID='".$_SESSION['UserID']."' OR ieci.InstructorID='".$_SESSION['UserID']."')";
			    $StaffType_conds = '';       // No StaffType filter
			}
			else {
                $sql .= "				IF (iees.StaffType = 'PIC' Or iegs.StaffType = 'PIC',
											CONCAT('<a href=\"event_setting.php?EnrolEventID=', ieei.EnrolEventID ,'\" class=\"tablelink\">', ieei.EventTitle, '</a>'),
											ieei.EventTitle
										), $targetClubSql
                IF (iees.StaffType = 'PIC' Or iegs.StaffType = 'PIC',
                CONCAT('<a href=\"meeting_date.php?AcademicYearID=$AcademicYearID&recordId=',ieei.EnrolEventID,'&recordType=" . $enrolConfigAry['Activity'] . "\"><img height=\"18\" width=\"18\" border=\"0\" src=\"{$image_path}/{$LAYOUT_SKIN}/icon_calendar_on.gif\"></a>'),
                '" . $Lang['General']['EmptySymbol'] . "'
										),";
                $instructorSql = "";
                $instructorCond = "";
			}
			
			if ($sys_custom['eEnrolment']['TWGHCYMA']) {
				$sql .= " GROUP_CONCAT(DISTINCT iec.CategoryName SEPARATOR ', ') as CategoryName, ";
			} else {
				$sql .= " 		iec.CategoryName,";
			}
			
            if (!$sys_custom['project']['HKPF']) {
			    $sql .= " 		IF (ieei.Quota = 0, 
							 				'$i_ClubsEnrollment_NoLimit', 
							 				ieei.Quota
							 			),
							 			IF (iees.StaffType = 'PIC' Or iegs.StaffType = 'PIC',
											CONCAT('<a href=\"event_member_index.php?EnrolEventID=', ieei.EnrolEventID, '\" class=\"tablelink\">', Count(DISTINCT iu.UserID), '</a>'),
											Count(DISTINCT iu.UserID)
										),
										$attendance_icon
										$get_payment_icon
										$EnrolEventID_ReplySlip ";
            }
            else {
            	$sql .= " IFNULL(iei.Title,'-') AS Intake, ";
            	$sql .= " IFNULL(GROUP_CONCAT(DISTINCT IF(ieeic.IntakeClassID IS NULL, NULL, ieic.ClassName) ORDER BY ieic.ClassName SEPARATOR ', '),'-') AS ClassName, ";
            }
			
				$sql .= "				$checkboxSQL
										Count(DISTINCT iu.UserID) as NumMember
								From
										INTRANET_ENROL_EVENTINFO as ieei
										Left Outer Join
										INTRANET_ENROL_EVENTSTAFF as iees
										On (ieei.EnrolEventID = iees.EnrolEventID And iees.UserID = '" . $_SESSION['UserID'] . "' AND ieei.AcademicYearID='$AcademicYearID')
										Left Outer Join
										INTRANET_ENROL_GROUPINFO as iegi
										On (ieei.EnrolGroupID = iegi.EnrolGroupID)
										$targetClubTable
										Left Outer Join
										INTRANET_ENROL_GROUPSTAFF as iegs
										On (iegi.EnrolGroupID = iegs.EnrolGroupID And iegs.UserID = '" . $_SESSION['UserID'] . "' And iegs.StaffType = 'PIC') ";
			if ($sys_custom['eEnrolment']['TWGHCYMA']) {
				$sql .= "	LEFT JOIN INTRANET_ENROL_EVENT_CATEGORY as ec ON ec.EnrolEventID=ieei.EnrolEventID 
											LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON ec.CategoryID=iec.CategoryID ";
			} else {
				$sql .= "			Left Outer Join 
												INTRANET_ENROL_CATEGORY as iec
												On (ieei.EventCategory = iec.CategoryID) ";
			}
			$sql .= $instructorSql;
			$sql .= "			Left Outer Join
										INTRANET_ENROL_EVENTSTUDENT as ieest
										On (ieei.EnrolEventID = ieest.EnrolEventID And ieest.RecordStatus = 2)
										Left Outer Join
										INTRANET_USER as iu
										On (ieest.StudentID = iu.UserID And iu.RecordType = 2)
								Where
										(ieei.EnrolGroupID = '' Or ieei.EnrolGroupID Is Null $EnrolGroupID_conds)
										$category_conds
										$name_conds
										$StaffType_conds
										$instructorCond
										$conds_intakeID
										$conds_intakeClassID
								Group By
										ieei.EnrolEventID
								";
		}
//		echo $sql;

		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);

		// Change Column number due to the display of activity related group
		$field_array = array (
		    "ActivityCode",
		    "EventTitle",
		    "RelatedGroup",
		    "CategoryName");

		if (!$sys_custom['project']['HKPF']) {
		    $field_array[] = "Quota";
		    $field_array[] = "Approved";
		    $nrColOffset = 3;
		    $colWidthActivityCode = "6";
		    $colWidthActivityName = "27";
		    $colWidthClub = "13";
		    $colWidthSchedule = "12";
		    $colWidthCategory = "10";
		}
		else {
		    $field_array[] = "Intake";
		    $field_array[] = "ClassName";
		    $nrColOffset = 2;
		    $colWidthActivityCode = "10";
		    $colWidthActivityName = "25";
		    $colWidthClub = "15";
		    $colWidthSchedule = "10";
		    $colWidthCategory = "15";
		    $colWidthIntake = "12";
		    $colWidthClass = "12";
		}
		
		$li->field_array = $field_array;
		$li->sql = $sql;
		//$li->no_col = ($showCheckbox == true)? sizeof($li->field_array) + 5 : sizeof($li->field_array) + 4;
		$li->no_col = ($showCheckbox == true) ? sizeof($li->field_array) + $nrColOffset + 1: sizeof($li->field_array) + $nrColOffset;

		if($get_payment_icon != '' && !$sys_custom['project']['HKPF']){
			$li->no_col ++;
		}
		
		$li->title = "";
		$li->column_array = array (
			0,
			0
		);
		if($sys_custom['eEnrolment']['ReplySlip']){
			#Cust
			$li->IsColOff = "Enrolment_Display_Event_ReplySlip_Cust";
			echo $linterface->Include_Thickbox_JS_CSS();
		}else{
			$li->IsColOff = "IP25_table";
		}

		// TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<th class='num_check'>#</th>\n";
		$li->column_list .= "<th width=\"".$colWidthActivityCode."%\" nowrap>" . $li->column_IP25($pos++, $Lang['eEnrolment']['ActivityCode']) . "</th>\n";
		$li->column_list .= "<th width=\"".$colWidthActivityName."%\">" . $li->column_IP25($pos++, $eEnrollment['add_activity']['act_name']) . "</th>\n";
		// Add column for related group
		$li->column_list .= "<th width=\"".$colWidthClub."%\">" . $li->column_IP25($pos++, $eEnrollment['belong_to_group']) . "</th>\n";
		$li->column_list .= "<th width=\"".$colWidthSchedule."%\" nowrap>" . $eEnrollment['add_club_activity']['step_2'] . "</th>\n";
		$li->column_list .= "<th width=\"".$colWidthCategory."%\" nowrap>" . $li->column_IP25($pos++, $eEnrollment['add_activity']['act_category']) . "</th>\n";
		
        if (!$sys_custom['project']['HKPF']){
    		$li->column_list .= "<th width=5% nowrap>" . $li->column_IP25($pos++, $eEnrollment['add_activity']['act_quota']) . "</th>\n";
    		$li->column_list .= "<th width=5% nowrap>" . $li->column_IP25($pos++, $eEnrollment['participant']) . "</th>\n";
    		$li->column_list .= "<th width=5% nowrap>" . $eEnrolment['attendance_title'] . "</th>\n";
    		if (!$helper_view && $get_payment_icon != '') {
    			$li->column_list .= "<th width=5% nowrap>" . $li->column_IP25($pos++, $eEnrollment['payment']) . "</th>\n";
    		}
    		if($sys_custom['eEnrolment']['ReplySlip']){
    			$li->column_list .= "<th width=10% nowrap>" . $eEnrollment['replySlip']['Signed_Total']. "</th>\n";
    			$li->no_col ++;
    		}
    		//		# Survey form result display customization
    		//		if($libenroll->enableActivitySurveyFormRight()){
    		//			$li->column_list .= "<th width=12%>".$Lang['eSurvey']['ViewResults']."</th>\n";
    		//		}
        }
        else {
            $li->column_list .= "<th width=\"".$colWidthIntake."%\">" . $li->column_IP25($pos++, $Lang['eEnrolment']['curriculumTemplate']['intake']['title']) . "</th>\n";
            $li->column_list .= "<th width=\"".$colWidthClass."%\">" . $Lang['eEnrolment']['curriculumTemplate']['intake']['className'] . "</th>\n";
            $pos++;
        }
        
		if ($showCheckbox == true) {
			$li->column_list .= "<th width=1 class=\"num_check\">" . $li->check("EnrolEventID[]") . "</th>\n";
		}

		if ($msg == 1)
			$SysMsg = $linterface->GET_SYS_MSG("add");
		if ($msg == 2)
			$SysMsg = $linterface->GET_SYS_MSG("update");
		if ($msg == 3)
			$SysMsg = $linterface->GET_SYS_MSG("delete");

		if ($libenroll->AllowToEditPreviousYearData && $sys_custom['YeoCheiMan_generate_performance']) {
			$menuBar .= "<a href=\"javascript:CickGenPerformance();\" class=\"generate_btn\"> " . $Lang['eEnrolment']['GeneratePerformance'] . "</a>";
		}
?>

<script language="javascript">
<?php if ($sys_custom['project']['HKPF']):?>
$(document).ready( function() {
	$('select#IntakeID').change(function(){
		document.form1.submit();
	});
	
	<?php if ($intakeID=='') { ?>
		$('select#IntakeID').val('');
	<? } ?>
	<?php if ($intakeClassID=='') { ?>
		$('select#IntakeClassID').val('');
	<? } ?>
});

function changeClass() {
	document.form1.submit();
}
<?php endif ?>

function checkform(obj){
        return true;
}

function checkAll(obj,val)
{
         <?

		for ($i = 0; $i < sizeof($EnrolEvent); $i++) {
?>
         obj.Allow<?=$i?>.checked=val;
         <?

		}
?>
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function CickGenPerformance()
{
	window.location="generate_activity_performance.php";
}

<?php if($libenroll->enableActivitySurveyFormRight()){ ?>
	function viewSurvey(id)
	{
	    newWindow('view.php?SurveyID='+id,10);
	}
<? }?>


function Js_View_Active_Member_List(obj,page)
{
	if(countChecked(obj,'EnrolEventID[]')==0)
    	alert(globalAlertMsg2);
    else{
        obj.action = page;
        obj.method = "POST";
        obj.submit();
        return;
    }
}

function PaymentConfirm(obj, jParID) {
	AlertPost(obj, 'payment.php?EnrolEventID='+jParID, '<?= $eEnrollment['js_prepayment_alert']?>');
}
function onLoadThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	load_dyn_size_thickbox_ip('<?=$eEnrollment['replySlip']['ReplySlipDetail']?>', 'onLoadReplySlipThickBox('+ReplySlipGroupMappingID+','+EnrolGroupID+','+ReplySlipID+');');
}
function onLoadReplySlipThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	$.ajax({
		url: "ReplySlip_ajax_ReplySlipReply_view.php",
		type: "POST",
		data: {"ReplySlipGroupMappingID": ReplySlipGroupMappingID, "EnrolGroupID": EnrolGroupID,"ReplySlipID":ReplySlipID,"isActivity":true},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
		}
	});
}
function goExport(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
    	document.form1.action = "ReplySlip_ajax_ReplySlipReply_view.php?ReplySlipGroupMappingID="+ReplySlipGroupMappingID+"&EnrolGroupID="+EnrolGroupID+"&ReplySlipID="+ReplySlipID+"&isExport="+1+"&isActivity=true";
    	document.form1.target = "_blank";
    	document.form1.method = "post";
    	document.form1.submit();
}

function enrolCheckRemove(obj,element,page,confirmMsg){
<?php if ($sys_custom['project']['HKPF']):?>
    if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
    else{
		$.ajax({
			dataType: "json",
			type: "POST",
			url: './ajax_check_booking.php',
			data : $('#form1').serialize(),
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					if (ajaxReturn.html == '') {
				    	if (confirmMsg == null || confirmMsg == '') {
<?php if ($sys_custom['project']['HKPF']):?>
			confirmMsg = "<?php echo $Lang['eEnrolment']['course']['warning']['confirmDeleteCourse'];?>";
<?php else:?>
			confirmMsg = globalAlertMsg3;
<?php endif;?>				    		
				    	}
				        if(confirm(confirmMsg)){	            
				            obj.action=page;                
				            obj.method="POST";
				            obj.submit();				             
				        }
					}
					else {
						alert("<?php echo $Lang['eEnrolment']['booking']['notAllowToDeleteApprovedBooking'];?>");
					}
				}
			},
			error: show_ajax_error
		});
    }
<?php else:?>
	checkRemove(obj,element,page,confirmMsg);
<?php endif;?>
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

</script>

<form name="form1" id="form1" method="get" >


<div class="content_top_tool">
	<div class="Conntent_tool"><?=$menuBar?></div>
	<div class="Conntent_search"><?=$searchTag?></div>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<div class="table_filter">
<?php 
    if (!$sys_custom['project']['HKPF']) {
        echo $yearFilter."&nbsp;";
        echo $category_selection;
    }
    else {
        echo $category_selection;
        echo $intake_selection;
        echo $intakeClass_selection;
    }
?>		
		</div> 
	</td>
	<? if($libenroll->AllowToEditPreviousYearData) {?>
	<td valign="bottom"><?=$toolsBar?></td>
	<?}?>
</tr>
</table>
</div>

<?=$li->display()?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="pic_view" value="<?=$pic_view?>">
<input type="hidden" name="helper_view" value="<?=$helper_view?>">

</form>

<?

		$linterface->LAYOUT_STOP();
	} else {
?>
You have no priviledge to access this page.
    <?

	}
} else {
?>
You have no priviledge to access this page.
<?

}
?>