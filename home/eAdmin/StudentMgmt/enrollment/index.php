<?php 
/*  using: 
 *  
 *  2020-01-20 Tommy
 *      redirect to event.php for KIS
 *  
 *  2018-08-13 Cameron
 *      redirect to event.php for HKPF general staff
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();

session_register_intranet('eEnrolment_enter_from_eService', 0);

if ($libenroll->isInstructor() || $_SESSION["platform"] == "KIS") {
    header("Location: event.php");
}
else if ( (($LibUser->isStudent()) || ($LibUser->isParent())) && !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) {
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
} else {	
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))||($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))||$libenroll->HAVE_CLUB_MGT() || $libenroll->HAVE_EVENT_MGT()) {
		header("Location: group.php");
	} else {
		header("Location: club_status.php");
	}
}

?>