<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);
$UID = IntegerSafe($UID);

$memberList = $UID;

$page_breaker = "<P CLASS='breakhere'>";

//get group/activity title
if (substr($typeText, 0, 4) == "club")
{
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)) && (!$ligroup->hasAdminBasicInfo($UserID))  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	{
		//header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
		No_Access_Right_Pop_Up();
		exit;
	}
	
	$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '$GroupID'";
	$result = $libenroll->returnVector($sql);
	$title = $result[0];
	
	$LetterContent = str_replace('<!--Type-->', $Lang['eEnrolment']['Club'], $Lang['eEnrolment']['NotificationLetter']['Content']);
}
else
{
	$title = $libenroll->GET_EVENT_TITLE($EnrolEventID);
	
	$LetterContent = str_replace('<!--Type-->', $Lang['eEnrolment']['Activity'], $Lang['eEnrolment']['NotificationLetter']['Content']);
}
$LetterContent = str_replace('<!--Name-->', $title, $LetterContent);

//get student info
$namefield = getNameFieldByLang();
if(sizeof($memberList)>0){
	$id_list = implode(",",$memberList);
	$sql = "SELECT EnglishName, ChineseName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN ($id_list) ORDER BY ClassName, ClassNumber";
	$memberName = $libenroll->returnArray($sql,4);
}

//get school data
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

//construct table content
$report_content = "";
for($i=0;$i<sizeof($memberList);$i++){
	
	// School Title
	$report_content .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" align=\"center\">";
	$report_content .= "<tr><td class=\"content\" align=\"center\">$school_name</td></tr>";
	$report_content .= "</table>";
	
	// Letter Content
	$report_content .= "<table border=\"0\" width=\"600\" cellspacing=\"3\" align=\"center\">";
	
	// "To" row
	$report_content .= "<tr><td>";
	$report_content .= "<table>";
	$report_content .= "<tr><td>To: </td><td width=\"10\"><td>Parent</td></tr>";
	$report_content .= "<tr><td>致：</td><td width=\"10\"><td>家長</td></tr>";	
	$report_content .= "</table>";
	$report_content .= "</td></tr>";
	
	// "Class" row
	$report_content .= "<tr><td colspan=\"2\">";
	$report_content .= "<table>";
	$report_content .= "<tr><td>Class: <br /> 班別： </td><td width=\"10\"></td>";
	$class = $memberName[$i]['ClassName']." (".$memberName[$i]['ClassNumber'].")";
	$report_content .= "<td colspan=\"2\" rowspan=\"2\">".$class."&nbsp</td></tr>";
	$report_content .= "</table>";
	$report_content .= "</td></tr>";
	
	// "Student Name" row
	$report_content .= "<tr><td>";
	$report_content .= "<table>";
	$report_content .= "<tr><td>Name of Student </td><td width=\"10\"><td>".$memberName[$i]['EnglishName']."</td></tr>";
	$report_content .= "<tr><td>學生姓名：</td><td width=\"10\"><td>".$memberName[$i]['ChineseName']."</td></tr>";	
	$report_content .= "</table>";
	$report_content .= "</td></tr>";
	
	// "Content" row
	$report_content .= "<tr><td><table><tr><td>Message: <br>內容：</td></tr></table></td></tr>";
	
	// letter content
	$report_content .= "<tr><td align=\"center\" style=\"padding: 5 20px\">";
	$report_content .= "<div style=\"text-align: justify; border: 1px black solid; padding: 10px; line-height: 18pt\">\n";
	
	//$report_content .= "$title Enrolment process has been completed. Your child is <b>successfully</b> enrolled in $title. ";
	//$report_content .= "You may go to the eEnrolment page to find the information of the activities which your child has been approved. Should you have any enquires, please contact System Admin or the teachers responsible for ECA Enrolment.";
	
	$report_content .= $LetterContent;

	$report_content .= "</div></td></tr>";
	$report_content .= "</table>";
	

	
	# Page break
	if ($i != sizeof($memberList)-1){
	  	$report_content .= $page_breaker;
	}
}

intranet_closedb();

?>
<html
<head>
<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<title><?=$school_name?></title>
<style>
<!--
P.breakhere {page-break-before: always}
.title{
	font-size:25px;
	font-family: Times New Roman,Arial, Verdana,  Helvetica, sans-serif, 新細明體;
	font-weight: normal;
}
td {
	vertical-align:bottom;
}
.head{
	font-size:10px;
	font-family: Times New Roman,Arial, Verdana,  Helvetica, Mingliu, Sans-Serif,新細明體;; 
	font-weight: normal;
}
.content{
	font-size:18px;
	font-family: Times New Roman,Arial, Verdana,  Helvetica, Mingliu, Sans-Serif,新細明體;; 
	font-weight: normal;
}
-->
</style>
</head>

<body>

<?= $report_content ?>

</body>

</html>