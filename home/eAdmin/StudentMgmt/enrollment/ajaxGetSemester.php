<?php
##	Modifying By: 
##############################################
##	Modification Log
##	2010-02-19: Max (201002081223)
##	- check if getSemester result before loop

##	2010-02-08: Max (201002081223)
##	- deleted a line $ldiscipline = new libdisciplinev12(); line do not use below 
##############################################

#Based on IntranetIP25/StudentMgmt/disciplinev12/reports/ajaxGetSemester.php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$yearID = $year;
$result = getSemesters($yearID);

$temp = "<select name='$field' class='formtextbox'>";
$temp .= "<option value='0'";
$temp .= ($year=='0') ? " selected" : "";
$temp .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

if($year!=0 and $result) {

	foreach($result as $termID=>$termName) {
		if($field=='semester1') {
			$selected = ($termID==$term1) ? " selected" : "";
		} else if($field=='semester2') {
			$selected = ($termID==$term2) ? " selected" : "";	
		} else {
			$selected = ($termID==$term) ? " selected" : "";	
		}
		
		$temp .= "<option value='$termID' $selected>$termName</option>";
	}
}
	
$temp .= "</select>";

echo $temp;

intranet_closedb();

?>