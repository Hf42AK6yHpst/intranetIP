<?
//using: 
/*
 *  2018-08-13 Cameron
 *      - allow instructor to access course for HKPF
 *  2018-08-09 Cameron
 *      - change tab wroding for HKPF
 *  2018-08-04 Cameron
 *      - hide tabs for HKPF
 *  
 */
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

if($libenroll === NULL){
	$libenroll = new libclubsenrol($AcademicYearID);
}

# Header tags for Management of Club
if ($tab_type == "club")
{	
	$managementURL = "group.php?clearCoo=1";
	$enrolmentURL = "club_status.php?clearCoo=1";
	$attendanceURL = "club_attendance_index.php";
	if($sys_custom['eEnrolment']['CommitteeRecruitment']){
	    $committeeURL = "group_committee_recruitment.php?clearCoo=1";	    
	}
	$isPIC = $libenroll->IS_CLUB_PIC();
	$isHelper = $libenroll->IS_CLUB_HELPER();
}
else if ($tab_type == "activity")
{
	$managementURL = "event.php?clearCoo=1";
	$enrolmentURL = "event_status.php";
	$attendanceURL = "event_attendance_index.php";
	
	$isPIC = ($libenroll->IS_EVENT_PIC() || $libenroll->IS_CLUB_PIC());
	$isHelper = $libenroll->IS_EVENT_HELPER();
}

# show tags according to different permission
if ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])||$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
{
    $tabRecord = $eEnrollment['tab']['record'];
    if ($sys_custom['project']['HKPF']) {
        if ($tab_type == "club") {
            $tabRecord = $eEnrollmentMenu['club'];
        }
        else if ($tab_type == "activity") {
            $tabRecord = $eEnrollmentMenu['activity'];
        }
    }
    
    $TAGS_OBJ[] = array($tabRecord, $managementURL, ($current_tab==1));
	if (!$sys_custom['project']['HKPF']) {
    	$TAGS_OBJ[] = array($eEnrollment['tab']['process'], $enrolmentURL, ($current_tab==2));
    	if($tab_type == "club" && $sys_custom['eEnrolment']['CommitteeRecruitment']){
    	    $TAGS_OBJ[] = array($eEnrollment['tab']['CommitteeRecruitment'], $committeeURL, ($current_tab==3));	    
    	}
    	//$TAGS_OBJ[] = array($eEnrollment['tab']['attendance'], $attendanceURL, ($current_tab==3));
	}
}
else if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && $isPIC)
{
	$TAGS_OBJ[] = array($eEnrollment['tab']['record'], $managementURL."&pic_view=1", ($current_tab==1));
	
	if ($CurrentPage != "PageClubAttendanceMgt" && $CurrentPage != "PageActAttendanceMgt")
		$TAGS_OBJ[] = array($eEnrollment['tab']['process'], $enrolmentURL, ($current_tab==2));
}
else if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && $isHelper)
{
	$TAGS_OBJ[] = array($eEnrollment['tab']['record'], $managementURL."&helper_view=1", ($current_tab==1));
}
else if ($sys_custom['project']['HKPF'] && $_SESSION['UserType']==USERTYPE_STAFF && $tab_type == "activity") {     // non-admin staff
    $TAGS_OBJ[] = array($eEnrollmentMenu['activity'], $managementURL, ($current_tab==1));
}
else
{
	$TAGS_OBJ[] = array($eEnrollment['tab']['process'], $enrolmentURL, ($current_tab==2));
}

?>
