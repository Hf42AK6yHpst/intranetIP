<?php
#Modify :anna
/*
 *  2018-04-27  Anna
 *              Create this page
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol("",$sel_category);

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || !$sys_custom['eEnrolment']['CommitteeRecruitment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($plugin['eEnrollment'])
{
	$CurrentPage = "PageSysSettingBasic";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

	//$enrolInfo = $libenroll->getEnrollmentInfo(); ##Comment by Omas

    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
		include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

		$libenroll_ui = new libclubsenrol_ui();

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($Lang['eEnrolment']['GeneralSetting'], "basic.php", 0);
        if(!$plugin['SIS_eEnrollment']){
            $TAGS_OBJ[] = array($Lang['eEnrolment']['CategorySetting'], "category_setting_club.php", 0);
        }
      
        $TAGS_OBJ[] = array($Lang['eEnrolment']['CommitteeRecruitmentSetting'], "committee_recruitment_setting.php", 1);
        
        if($msg==2)
        	$ReturnMessage = $Lang['General']['ReturnMessage']['UpdateSuccess'];
        
		$linterface->LAYOUT_START($ReturnMessage);
		$AcademicYearID = $libenroll->CommitteeRecruitmentAcademicYearID;
		if (!isset($AcademicYearID) || $AcademicYearID=="")
		    $AcademicYearID = $libenroll->get_Next_Academic_Year_ID();
		
		$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);
		
		$CommitteeRecruitmentStart = $libenroll->Committee_Recruitmen_Start;
		$CommitteeRecruitmentStartHour = $libenroll->Committee_Recruitmen_Start_Hour;
		$CommitteeRecruitmentStartMin = $libenroll->Committee_Recruitmen_Start_Min;
		$CommitteeRecruitmentEnd = $libenroll->Committee_Recruitmen_End;
		$CommitteeRecruitmentEndHour = $libenroll->Committee_Recruitmen_End_Hour;
		$CommitteeRecruitmentEndMin = $libenroll->Committee_Recruitmen_End_Min;
		$SendEmail = $libenroll->CommitteeRecruitmentSendEmail;
		$CommitteeApplyMax =  $libenroll->CommitteeApplyMax;

		$chkSendEmail = ($SendEmail==1)? "checked='checked'" : "";
		$x = '<form id="form1" name="form1" method="POST" action="committee_recruitment_setting_update.php">'."\n";
		$x .= '<table cellpadding="0" cellspacing="0" border="0" class="form_table_v30"> '."\n";
			
    		$x .= '<tr >'."\n";
    		    $x .= '<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>'."\n";
    		    $x .= '<td>'.$yearFilter.'</td>'."\n";
    		$x .= '</tr>'."\n";
		
    		$x .= '<tr >'."\n";
    		$x .= '<td class="field_title">'.$Lang['eEnrolment']['CommitteeRecruitment']['Setting']['Period'].'</td>'."\n";
        		$x .= '<td>'.$linterface->GET_DATE_PICKER("CommitteeRecruitmentStart",$CommitteeRecruitmentStart).getTimeSel("CommitteeRecruitmentStart", $CommitteeRecruitmentStartHour, $CommitteeRecruitmentStartMin).
                    		'&nbsp;'.$eEnrollment['to'].'&nbsp;'.
                    		$linterface->GET_DATE_PICKER("CommitteeRecruitmentEnd",$CommitteeRecruitmentEnd).
                    		getTimeSel("CommitteeRecruitmentEnd", $CommitteeRecruitmentEndHour, $CommitteeRecruitmentEndMin).
        		      '</td>'."\n";
    		$x .= '</tr>'."\n";
    
    		$x .= '<tr>'."\n";
    		    $x .= '<td class="field_title"  style="width:45%;">'.$Lang['eEnrolment']['MaxStudentApply'].'</td>'."\n";

    		    $max_numbers = array(array(0, $i_ClubsEnrollment_NoLimit));
    		    for ($i=1; $i<10; $i++) {
    		        $max_numbers[] = array($i,$i);
    		    }
    		    
    		    $maxNumSelectionBox = getSelectByArray($max_numbers, 'id="CommitteeApplyMax" name="CommitteeApplyMax" class="ApplyMaxSel"', $CommitteeApplyMax);
        		
        		$x .= '<td class="'.$__colClass.'">'.$maxNumSelectionBox.'</td>';
        		
    		$x .= '</tr>'."\n";
    		
    		
    		$x .= '<tr>'."\n";
    		$x .= '<td class="field_title"  style="width:45%;">'.$Lang['eEnrolment']['CommitteeRecruitment']['Setting']['SendMail'].'</td>'."\n";
    		     $x .= '<td>'."\n";
    
    		     $x .= '<input type="checkbox" name="SendEmail" value="1" '.$chkSendEmail.'>';
    		     $x .= '</td>'."\n";
    		$x .= '</tr>'."\n";

		
		$x .= '</table>'."\n";
     
        $SaveBtn = $libenroll_ui->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "checkform(document.form1)");
        $x .= '<div class="edit_bottom_v30">'."\n";
            $x .= $SaveBtn;
        $x .= '</div>'."\n";
        
    $x .= '</form>'."\n";
    
    echo $x; 	

?>

<script language="javascript">
function checkform(obj)
{


	var isVaild = true;
// 	$('select.ApplyMinSel').each( function() {
// 		var _id = $(this).attr('id');
// 		var _idPieces = _id.split('_');
// 		var _termNum = _idPieces[1];
// 		var _categoryId = _idPieces[2];
		
// 		var _applyMin = $(this).val();
// 		var _applyMax = $('select#ApplyMax_' + _termNum + '_' + _categoryId + '_Sel').val();
// 		var _enrollMax = $('select#EnrollMax_' + _termNum + '_' + _categoryId + '_Sel').val();
		
// 		if (_applyMax != 0 && _applyMin > _applyMax) {
// 			isVaild = false;
			
//			alert('<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>');
// 			$(this).focus();
// 			return false; 	// break
// 		}
		
// 		if (_applyMax != 0) {
// 			if ( (_enrollMax == 0) || (_enrollMax > _applyMax) ) {
// 				isVaild = false;
				
//				alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
// 				$(this).focus();
// 				return false; 	// break
// 			}
// 		}
// 	});
	
// 	if (isVaild == false) {
// 		return false;
// 	}
	

	obj.action="committee_recruitment_setting_update.php";
	obj.submit();
}

</script>

<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>