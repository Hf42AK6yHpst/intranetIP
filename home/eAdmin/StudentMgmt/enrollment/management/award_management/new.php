<?php
//# using:  

/********************************************************
 *  Modification Log
 *		Date: 2015-11-06 (Omas)
 *			  Updated select student page to choose.php
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();

# Access Right Checking 
if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageMgtActivity";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# New Record
$RelatedToType = 'A';
$EnrolEventID = $_GET['EnrolEventID'];
if($EnrolEventID>0){
	$BelongToGroupID = $libenroll->Get_Activity_Belong_To_Club($EnrolEventID);
	$clubInfoArr = $libenroll->Get_Club_Info_By_EnrolGroupID($BelongToGroupID);
	$EventInfoArr = $libenroll->Get_Activity_Info_By_Id($EnrolEventID);
}

# Edit Record
if(!empty($_GET['EventAwardID'])){
$EventAwardID = $_GET['EventAwardID'];
}
if(!empty($_POST['EventAwardID'])){
$EventAwardID = $_POST['EventAwardID'];
}
if(is_array($EventAwardID)){
	$EventAwardID = $EventAwardID[0];
}
if($EventAwardID > 0){
	$EditMode = 1;
	$sql = 'select AwardID,RelatedToID,RelatedToType,IsGroupAward,GroupName,Division,DivisionEn from INTRANET_ENROL_EVENT_AWARD where EventAwardID = '.$EventAwardID;
	$eventAwardArr = $libenroll->returnResultSet($sql);
	$eventAwardInfoArr = $eventAwardArr[0];
	if($eventAwardInfoArr['RelatedToType'] == 'A'){
		$EnrolEventID = $eventAwardInfoArr['RelatedToID'];
		$EventInfoArr = $libenroll->Get_Activity_Info_By_Id($EnrolEventID);
	}
	$sql = 'select UserID from INTRANET_ENROL_EVENT_AWARD_STUDENT where EventAwardID = '.$EventAwardID;
	$selectedStudentArr = $libenroll->returnVector($sql);
}
# Page tag
$TAGS_OBJ[] = array($Lang['eEnrolment']['AwardMgmt']['AwardMgmt'], "");

# Page Navigation 
$PAGE_NAVIGATION[] = array($eEnrollment['activity']);
$PAGE_NAVIGATION[] = array($EventInfoArr[$EnrolEventID]['EventTitle']);
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['AwardMgmt']['AwardMgmt']);
if($EditMode){
	$PAGE_NAVIGATION[] = array($button_edit);
}
else{
	$PAGE_NAVIGATION[] = array($button_new);	
}

$HiddenField = '';
$HiddenField .= $linterface->GET_HIDDEN_INPUT('RelatedToType','RelatedToType','A');
$HiddenField .= $linterface->GET_HIDDEN_INPUT('EnrolEventID','EnrolEventID',$EnrolEventID);
if($EditMode){
	$HiddenField .= $linterface->GET_HIDDEN_INPUT('EventAwardID','EventAwardID',$EventAwardID);
	
	# Radio Btn
	if($eventAwardInfoArr['IsGroupAward'] == 1){
	// IsGroupAward = 1
	$checked_G = '1';
	$checked_I = '0';
	}
	else{
		// IsGroupAward = 0
		$checked_I = '1';
		$checked_G = '0';
	}
}

# Form Element Preparation
$awardArr = $libenroll->GET_ENROL_AWARD();
$awardAssocArr = BuildMultiKeyAssoc($awardArr,'AwardID',array(Get_Lang_Selection('AwardNameCh','AwardNameEn')),1);
$awardSelect = '<div style="float:left;">'.getSelectByAssoArray($awardAssocArr, 'class="" name="AwardID" id="AwardID"',$eventAwardInfoArr['AwardID'], $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1).'</div>';
//$newAward = $linterface->GET_LNK_NEW("javascript:add_award()", '', '', '', '', 0);

$divisionTextbox = '<input name="Division" type="text" id="Division" class="textboxtext" value="'.$eventAwardInfoArr['Division'].'"/>';
$divisionEnTextbox = '<input name="DivisionEn" type="text" id="DivisionEn" class="textboxtext" value="'.$eventAwardInfoArr['DivisionEn'].'"/>';

$radioAwardType = '<div style="float:left">' 
				.$linterface->Get_Radio_Button('AwardType_I', 'AwardType', 'I', $checked_I,'',$Lang['eEnrolment']['AwardMgmt']['IndividualAward'],'changeAwardType()')
				.$linterface->Get_Radio_Button('AwardType_G', 'AwardType', 'G', $checked_G,'',$Lang['eEnrolment']['AwardMgmt']['GroupAward'],'changeAwardType()')
				.'</div>';
if(!$EditMode){
	$defaultGroupName = $clubInfoArr['TitleChinese'];
}
else{					
	$defaultGroupName = $eventAwardInfoArr['GroupName'];
}
$groupName = '<input name="GroupName" class="textboxtext" type="text" id="GroupName" value="'.$defaultGroupName.'" disabled/>';

# Student Selection
if ($EditMode && count($selectedStudentArr) > 0){
	$StudentArr = $selectedStudentArr;	
	$luser = new libuser();
}
else{
	$StudentArr = array();
}

$studentBox = "<select id='student' name='student[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($StudentArr); $i++) {
	$studentBox .= "<option value=\"".$StudentArr[$i]."\">".$luser->getNameWithClassNumber($StudentArr[$i])."</option>";
}
$studentBox .= "</select>";
$studentBox .= "&nbsp;";

//$selectStudent = '<input type="button" id="selectStudent" class="formsmallbutton" onClick="javascript:newWindow(\'/home/common_choose/enrolment_pic_helper.php?fieldname=student[]&page_title=SelectMembers&CatID=-999&permitted_type=2&type='.$type.'&AcademicYearID='.$AcademicYearID.'&EnrolEventID='.$EnrolEventID.'&filter='.$filter.'&checkAdmin=3\', 9);"  value="'.$button_select.'"   onMouseOver="this.className=\'formsmallbuttonon\';" onMouseOut="this.className=\'formsmallbutton\';"/>';
$selectStudent = '<input type="button" id="selectStudent" class="formsmallbutton" onClick="javascript:newWindow(\'choose.php?fieldname=student[]&page_title=SelectMembers&CatID=-999&permitted_type=2&type='.$type.'&AcademicYearID='.$AcademicYearID.'&EnrolEventID='.$EnrolEventID.'&filter='.$filter.'&checkAdmin=3\', 9);"  value="'.$button_select.'"   onMouseOver="this.className=\'formsmallbuttonon\';" onMouseOut="this.className=\'formsmallbutton\';"/>';
$removeStudent = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");
$studentSection = '<table><tr><td>'.$studentBox.'</td><td>'.$selectStudent.'<br>'.$removeStudent.'</td></tr></table>';

$requireField = $linterface->RequiredSymbol();
$formElementAry = array();
// array 0 - field title , 1 - field element , 3 - cust field name
$formElementAry[] = array($Lang['eEnrolment']['AwardMgmt']['EventName'], $HiddenField.$EventInfoArr[$EnrolEventID]['EventTitle']);
$formElementAry[] = array($requireField.$Lang['eEnrolment']['AwardMgmt']['Award'], $awardSelect.$newAward);
$formElementAry[] = array($requireField.$Lang['eEnrolment']['AwardMgmt']['AwardType'], $radioAwardType);
$formElementAry[] = array($Lang['eEnrolment']['AwardMgmt']['GroupName'], $groupName ,'GroupName');
$formElementAry[] = array($Lang['eEnrolment']['AwardMgmt']['DivisionCh'], $divisionTextbox);
$formElementAry[] = array($Lang['eEnrolment']['AwardMgmt']['DivisionEn'], $divisionEnTextbox );
$formElementAry[] = array($requireField.$Lang['Identity']['Student'], $studentSection);

$html = '';
$html .= '<table class="form_table_v30">
				<tbody>';
				foreach((array)$formElementAry as $_rowElementAry){
					if($_rowElementAry[2] == 'GroupName'){
						$html .= '<tr id="GroupNameRow" style="display:none;">';
							$html .= '<td class="field_title">'.$_rowElementAry[0].'</td>';
							$html .= '<td>'.$_rowElementAry[1].'</td>';
						$html .= '</tr>';
					}
					else{						
						$html .= '<tr>';
							$html .= '<td class="field_title">'.$_rowElementAry[0].'</td>';
							$html .= '<td>'.$_rowElementAry[1].'</td>';
						$html .= '</tr>';
					}
				}	
$html .= '	</tbody>
		</table>';

$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
$(document).ready( function() {
	changeAwardType();
});

function add_award(){
	var AwardNameEn = prompt("Please Enter Award English Name (Can Not Be Empty)","");
	while (AwardNameEn == '') {
    	AwardNameEn = prompt("Please Enter Award English Name (Can Not Be Empty)","");
	}
	var AwardNameCh = prompt("Please Enter Award Chinese Name (Can Not Be Empty)","");
	while (AwardNameCh == '') {
    	AwardNameCh = prompt("Please Enter Award Chinese Name (Can Not Be Empty)","");
	}
	
	if(AwardNameCh == '' || AwardNameCh == null || AwardNameEn == '' || AwardNameEn == null){
		return false;
	}
	else{		
		$.ajax({
			type: 'POST',
			url: "../../settings/awards/award_update.php",
			data: {
					"AwardNameCh" 	:	AwardNameCh,
					"AwardNameEn" 	:	AwardNameEn,
					"Ajax" 			:	"Ajax"
				 },
			success: function(responseText){
				location.reload();
			}
		});
	}
}

function changeAwardType(){
	if($('input#AwardType_G').attr('checked')){
		$('tr#GroupNameRow').show();
		$('input#GroupName').removeAttr('disabled');		
	}
	else{
		$('tr#GroupNameRow').hide();
		$('input#GroupName').attr('disabled',true);
	}
}

function goBack(){
	window.location.href = 'index.php?EnrolEventID=<?=$EnrolEventID?>&RelatedToType=<?=$RelatedToType?>';
}


function goSubmit() {
	var canSubmit = true;
	
	// highlight all options
	checkOptionAll(document.form1.elements["student[]"]);
	
	// disable action button to prevent double submission
	$('input#SubmitBtn').attr('disabled', 'disabled');
	
	// check required fields
	if(!$('#AwardID').val()){
		canSubmit = false;
		alert('<?=$Lang['General']['PleaseSelect'].' '.$Lang['eEnrolment']['AwardMgmt']['Award']?>');
		$('#AwardID').focus();
	}
	else if($('input[name=' + 'AwardType' + ']:checked').length ==0){
		canSubmit = false;
		alert('<?=$Lang['General']['PleaseSelect'].' '.$Lang['eEnrolment']['AwardMgmt']['AwardType']?>');
		$('#AwardType_I').focus();
	}
	else if(document.form1.elements["student[]"].length == 0){
		canSubmit = false;
		alert('<?=$Lang['General']['PleaseSelect'].' '.$Lang['Identity']['Student']?>');
		$('#selectStudent').focus();
	}
		
	if (canSubmit) {
		document.form1.submit();
	}
	else {
		// enable the action buttons if not allowed to submit
		$('input#SubmitBtn').attr('disabled', '');
	}
}

</script>
<form name="form1" action="new_update.php" method="POST"= onSubmit="">
	<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result, $SpMessage);?></td>
		</tr>
		<tr>
			<td>
			<?=$html?>
			</td>	
		</tr>
	</table>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();","SubmitBtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();","CancelBtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>