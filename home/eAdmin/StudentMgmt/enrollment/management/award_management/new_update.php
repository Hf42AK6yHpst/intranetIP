<?php
/*
 * 	Log:
 * 	
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$awardInfo['Division'] = standardizeFormPostValue($_POST['Division']);
$awardInfo['DivisionEn'] = standardizeFormPostValue($_POST['DivisionEn']);
$awardInfo['IsGroupAward'] = ($_POST['AwardType'] == 'G'? 1:0);
$awardInfo['AwardID'] = $_POST['AwardID'];
$awardInfo['RelatedToType'] = $_POST['RelatedToType'];
$awardInfo['RelatedToID'] = $_POST['EnrolEventID'];
$awardInfo['GroupName'] = $_POST['GroupName'];
$awardInfo['EventAwardID'] = $_POST['EventAwardID'];
$UserIDArr = $_POST['student'];

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;
}

if($_POST['EventAwardID']>0){
	$libenroll->DB_HANDLING_EVENTAWARD('update',$awardInfo);
	$libenroll->UpdateEventAwardToStudent($EventAwardID,$UserIDArr);
}
else{
	$libenroll->DB_HANDLING_EVENTAWARD('insert',$awardInfo);
	$EventAwardID = $libenroll->db_insert_id();
	$libenroll->AddEventAwardToStudent($EventAwardID,$UserIDArr);	
}

$libenroll->syncAwardToStudentAchievement($awardInfo,$UserIDArr);

intranet_closedb();
header("Location: index.php?EnrolEventID=".$_POST['EnrolEventID']."&ReturnMsgKey=$ReturnMsgKey");
?>