<?php
# using: 
################################################

#
############ Change Log [start] #################
#	Date: 2015-11-06 (Omas)
#		  Created this page for adding awarded student
############ Change Log [End] #################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_opendb();

if ($plugin['eEnrollment'])
{
	
	$EnrolGroupID = IntegerSafe($EnrolGroupID);
	$permitted_type = IntegerSafe($permitted_type);
	
	
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$MODULE_OBJ['title'] = $button_select." ".$eEnrollment['select'][$ppl_type];
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
    
################################################################

$linterface->LAYOUT_START();

# retrieve role type
$lo = new libgroup($GroupID);
$luser = new libuser();
$row = $lo->returnGroupRoleType();
$RoleOptions .= "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$RoleOptions .= "</select>\n";

$permitted = explode(",",$permitted_type);

# retrieve group category

$li = new libgrouping();

$CatID = empty($_POST['CatID'])==''? $_POST['CatID']: $_GET['CatID'];
if($CatID < 0)
{
	 unset($ChooseGroupID);
     switch($CatID)
     {
	     case -2 : $ChooseGroupID[0]=2; $teaching=0;break;
	     case -3 : $ChooseGroupID[0]=3; $teaching=0;break;
	     
	     case -99 : $ChooseGroupID[0]=1; $teaching=1;break;
	     case -100: $ChooseGroupID[0]=1; $teaching=0;break;
	     case -999 : $ChooseGroupID[0]=2; $teaching=0;break;
	     default:$ChooseGroupID[0]=1; $teaching=1;
	 }
}

$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();


##### Identity selection
if (isset($permitted) && !empty($permitted))
{
	$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
	if($page_title != 'SelectMembers'){
		$x1 .= "<option value=\"0\"> </option>";
	}
	else{
		if (!empty($EnrolEventID)){
			$libclubsenrol = new libclubsenrol();
			$activityInfo = $libclubsenrol->Get_Activity_Info_By_Id($EnrolEventID);
			$sql = $libclubsenrol->Get_Management_Activity_Participant_Sql($EnrolEventID,2,2,''); // student only
			$memberListInfoAry = $libclubsenrol->returnResultSet($sql);
			$FromAwardMgmt = 1;
			$x1 .= "<option value=\"-999\">".$activityInfo[$EnrolEventID]['EventTitle']."</option>";
		}
	}
	$x1 .= "<optgroup label='". $Lang['CommonChoose']['IdentitySelectOptionLabel'] ."'>";
// 	$x1 .= "<option value=''>-- $button_select --</option>\n";
	if (in_array(2,$permitted))
	$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
	
	if (in_array(3,$permitted))
	$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
	    
	if (in_array(1,$permitted))
	{
		$x1 .="<option value=-99 ".(($CatID==-99)?"SELECTED":"").">$i_teachingStaff</option>\n";
		$x1 .="<option value=-100 ".(($CatID==-100)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
	}
	
}

if(isset($ChooseGroupID)) {
	if($CatID<0)
	{
	    if($CatID==-3)		# parent
	    {
	    	$sortByClass = 1;
		    # retrieve student list first
		    $StudentList = $lclass->getClassStudentList($classname, "1");
		    $StudentIDStr = implode(",",$StudentList);
		    $row = $luser->getParent($StudentIDStr, $sortByClass);
	    }
	    else if($CatID==-2 && $classname)	#student
	    {
	    	$sortByClass = 1;
		    $row = $lclass->getStudentNameListWClassNumberByClassName($classname, "1");
	    }
	    else if($CatID==-999 && $FromAwardMgmt)	#student
	    {
	    	$row = $memberListInfoAry;
	    }
	    else	# staff
	    {	
			$row = $luser->returnUsersByIdentity($ChooseGroupID[0],$teaching);
		}
	}
	else
	{
   		$row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
	}

	
	$x3  = "<select name=ChooseUserID[] size=15 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
     	
     	
 		if($CatID==-3) {
			$tmp = "";
 			list($parentID, $parentName, $studentName, $clsName, $clsNo) = $row[$i];
     		$x3 .= "<option value=".$parentID." ".$disabled.">";
			$tmp = $clsName.($clsNo!=""?"-".$clsNo:"");
			$x3 .= ($tmp=="") ? "" : "($tmp) ";
			$x3 .= $studentName." ".$Lang['General']['s']."(".$parentName.")</option>\n";
 		}
 		else if($CatID==-999){
 			
 			$displayName = $row[$i]['StudentName'].'('.$row[$i]['ClassName'].'-'.$row[$i]['ClassNumber'].')';
 			$x3 .= "<option value=".$row[$i]['UserID']." ".$disabled.">".$displayName.$role."</option>\n";
 		}
     	else {
     		$x3 .= "<option value=".$row[$i][0]." ".$disabled.">".$row[$i][1].$role."</option>\n";
     	}
     }
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
     
}
?>
<script language="JavaScript1.2">
function add_role(obj){
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          obj.role.value = role_name;
          obj.action = "add_role.php";
          obj.submit();
     }
}
function add_user(obj){
     obj.action = "add_user.php";
     obj.submit();
}

function import_update(){
        var obj = document.form1;
        checkOption(obj.elements["ChooseUserID[]"]);
        obj.action = "add_user.php";
        obj.submit();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function expandGroup()
{
        var obj = document.form1;
        checkOption(obj.elements['ChooseGroupID[]']);
        obj.submit();
}

function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     //par.form1.flag.value = 0;
     //par.generalFormSubmitCheck(par.form1);
}

</script>

<form name="form1" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table class="form_table_v30">
        <tr>
			<td class="field_title"><span><?= $i_frontpage_campusmail_select_category?></span></td>
			<td><?php echo $x1; ?></td>
		</tr>
		
		<? 
		if($CatID==-3 || $CatID==-2) 
		{ 
			$ClassSelection = $lclass->getSelectClass("name='classname' onChange='this.form.submit();'", $classname, "", $i_general_all_classes);
			?>
		<tr>
			<td class="field_title"><?=$i_UserParentLink_SelectClass?></td>
			<td><?=$ClassSelection?></td>
		</tr>
		<? } ?>

	
		<?php if(isset($ChooseGroupID))
		{ ?>
		<tr><tdclass="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td class="field_title"><?= $i_frontpage_campusmail_select_user?></span></td>
			<td width="80%" style="align: left; border-bottom:0px;">
		       	<table border="0" cellpadding="0" cellspacing="0" align="left">
					<tr> 
						<td>
		       				<?php echo $x3 ?>
		       			</td>
		       			<td style="vertical-align:bottom;">        
					        <table width="100%" border="0" cellspacing="0" cellpadding="6">
								<tr> 
									<td align="left" style="border-bottom:0px;"> 
										<?= $linterface->GET_SMALL_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])")?>
										<!--
										<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
										-->
									</td>
								</tr>
								<tr> 
									<td align="left" style="border-bottom:0px;"> 
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseUserID[]']); return false;")?>
									</td>
								</tr>
							</table>
		       			</td>
		       		</tr>
		       	</table>
			</td>
		</tr>
		<?php
		} ?>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
<input type="hidden" name="role">
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
<input type="hidden" name="ppl_type" value="<?php echo $ppl_type; ?>" />
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>