<?php
/*
##############
# Date	2017-01-09 Omas
#	-Fix C111263 
################
*/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$libenroll = new libclubsenrol();

if(!$plugin['eEnrollment'] || !$sys_custom['eEnrolment']['twghwfns']['AwardMgmt']||!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;	
}

$CurrentPage = "PageMgtActivity";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['AwardMgmt']['AwardMgmt'], "");
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Get Current Record Info
$EnrolEventID = $_GET['EnrolEventID'];
if(empty($EnrolEventID)){
	$EnrolEventID = $_POST['EnrolEventID'];
}
$BelongToGroupID = $libenroll->Get_Activity_Belong_To_Club($EnrolEventID);
$EventInfoArr = $libenroll->Get_Activity_Info_By_Id($EnrolEventID);
$RelatedToID = $EnrolEventID;
$RelatedToType = 'A';

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($eEnrollment['activity']);
$navigationAry[] = array($EventInfoArr[$EnrolEventID]['EventTitle']);
$navigationAry[] = array($Lang['eEnrolment']['AwardMgmt']['AwardMgmt']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### form table
// if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_size) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_size!=''){
// 	$PageSize = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_size;
// }
	
// if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_number) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_number!=''){
// 	$PageNumber = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_number;
// }
		
// if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_order) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_order!=''){
// 	$Order = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_order;
// }
		
// if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_field) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_field!=''){
// 	$SortField = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_field;
// }
		
# Default Table Settings
$pageNo = ($pageNo == '')? 1 : $pageNo;
$numPerPage = ($numPerPage == '')? 20 : $numPerPage;
$order = ($order == '')? 1 : $order;
$field = ($field == '')? 0 : $field;
		
		
		
# TABLE INFO
$libdbtable = new libdbtable2007($field, $order, $pageNo);
$awardNameLang = Get_Lang_Selection('AwardNameCh','AwardNameEn');
$divisionNameLang = Get_Lang_Selection('Division','DivisionEn');
$libdbtable->sql = "Select 
								Concat('<a href=\"new.php?EventAwardID=',ieea.EventAwardID,'\" class=\"tablelink\">',iea.".$awardNameLang.",'</a>') as AwardNameEn,
								ieea.".$divisionNameLang.", 
								IF(ieea.IsGroupAward=1,
									'".$Lang['eEnrolment']['AwardMgmt']['GroupAward']."',
									'".$Lang['eEnrolment']['AwardMgmt']['IndividualAward']."'
								  ) as RecordType, 
								Group_concat(".getNameFieldWithClassNumberByLang('iu.')." SEPARATOR '<br>' ),
								Concat('<input type=\"checkbox\" name=\"EventAwardID[]\" value=\"',ieea.EventAwardID,'\">'),
								iea.DisplayOrder
							from 
								INTRANET_ENROL_EVENT_AWARD as ieea
								INNER JOIN INTRANET_ENROL_AWARD as iea ON ieea.AwardID = iea.AwardID
								INNER JOIN INTRANET_ENROL_EVENT_AWARD_STUDENT  as ieeas ON ieeas.EventAwardID = ieea.EventAwardID
								INNER JOIN INTRANET_USER iu ON ieeas.UserID = iu.UserID
							where 
								RelatedToID  = '".$RelatedToID."' 
								and RelatedToType ='".$RelatedToType."'
							Group By
								ieea.EventAwardID";
$libdbtable->IsColOff =  "IP25_table";
$libdbtable->field_array = array("DisplayOrder", "Division", "RecordType" );
$libdbtable->no_col = sizeof($libdbtable->field_array) + 2;		// 2 means column #, Checkbox
$libdbtable->title = "";
$libdbtable->column_array = array(0,18,0);
$libdbtable->wrap_array = array(0,0,0);
	
$libdbtable->page_size = $numPerPage;
		
# Table Action Button
$BtnArr = array();
$BtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'EventAwardID[]\',\'new.php\');');
$BtnArr[] = array('delete', 'javascript:checkRemove(document.form1,\'EventAwardID[]\',\'remove.php\');');
$table_tool = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);
		
# Table Column
$pos = 0;
		
$libdbtable->column_list .= "<th width='1' class=''>#</td>\n";
$libdbtable->column_list .= "<th width='25%'>".$libdbtable->column_IP25($pos++, $Lang['eEnrolment']['AwardMgmt']['Award'])."</td>\n";
$libdbtable->column_list .= "<th width='25%'>".$libdbtable->column_IP25($pos++, $Lang['eEnrolment']['AwardMgmt']['Division'])."</td>\n";
$libdbtable->column_list .= "<th width='25%'>".$libdbtable->column_IP25($pos++, $Lang['eEnrolment']['AwardMgmt']['AwardType'])."</td>\n";
$libdbtable->column_list .= "<th width='25%'>".$libdbtable->column_IP25($pos++, $Lang['Identity']['Student'])."</td>\n";
$libdbtable->column_list .= "<th class='tableTitle'>".$libdbtable->check("EventAwardID[]")."</th>\n";
$libdbtable->no_col = $pos + 2;
		
$DBTable = '';
$DBTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
	$DBTable .= '<tr>'."\n";
		$DBTable .= '	<td align="right" valign="bottom">'.$table_tool.'</td>'."\n";
	$DBTable .= '</tr>'."\n";
	$DBTable .= '<tr>'."\n";
		$DBTable .= '	<td align="center" valign="top">'.$libdbtable->display().'</td>'."\n";
	$DBTable .= '</tr>'."\n";
$DBTable .= '</table>'."\n";
		
	
	
$DBTable .= '<input type="hidden" name="pageNo" value="'.$libdbtable->pageNo.'" />';
$DBTable .= '<input type="hidden" name="order" value="'.$libdbtable->order.'" />';
$DBTable .= '<input type="hidden" name="field" value="'.$libdbtable->field.'" />';
$DBTable .= '<input type="hidden" name="page_size_change" value="" />';
$DBTable .= '<input type="hidden" name="numPerPage" value="'.$libdbtable->page_size.'" />';

$htmlAry['formTable'] = $DBTable;

$newButton = '<div class="content_top_tool"><div class="Conntent_tool">'.$linterface->Get_Content_Tool_v30("new","new.php?EnrolEventID=".$EnrolEventID).'</div></div>';
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();","CancelBtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$linterface->LAYOUT_START();
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = '../../event_member_index.php?EnrolEventID=<?=$EnrolEventID?>';
}

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$newButton?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>