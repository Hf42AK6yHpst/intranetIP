<?php
// using: 

/**************************************************
 * Date:  2019-10-28 Henry
 * Details: support inactive member (ActiveMemberOnly = 2)
 * 
 * Date:  2019-10-14 Henry
 * Details: bug fix for the school year field
 * 
 * Date:  2019-05-29 Anna
 * Details: added recordtype is null consition when get role from INTRANET_USERGROUP [#160147]
 * 
 * Date : 2019-02-20 Anna
 * Details: added $TAGS_OBJ 
 * 
 * Date : 2018-11-20 Anna [#C153312]
 * Details: fix cannot get $clubPerformanceAry about active member  
 * 
 * Date	: 2017-09-19 Anna
 * Details: added $uploadMethod - directly or mappingfile
 * 
 * Date	: 2017-06-06 Anna
 * Details added $ExportIndicator	
 * 
 * Date	: 2015-07-08 Omas
 * Details updated left menu
 * 
 * Date: 2014-02-17 (Ivan)
 * Details: fixed: post data wrongly get active or inactive member records only. Cannot get all members' post. 
 * 
 * Date: 2013-06-25 (Rita)
 * Details: create this page
 **************************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$limport = new libimporttext();
$lo = new libfilesystem();

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
if ($plugin['eEnrollmentLite'] || !$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
// $TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_WebSAMS'], "", 1);
$curTab = 'to_websams';
$data_batch_process_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_sp/step1.php";
$transer_to_websams_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_websams/step1.php";
$transer_to_ole_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_ole/step1.php?clearCoo=1";
$data_deletion_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/data_deletion/data_deletion.php";

$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], $data_batch_process_link, $curTab=='to_sp');
if($plugin['iPortfolio']) {
    $TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], $transer_to_ole_link, $curTab=='to_ole');
}
$TAGS_OBJ[] = array($Lang['eEnrolment']['data_handling']['export_websams'], $transer_to_websams_link, $curTab=='to_websams');
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClearAllEnrol'], $data_deletion_link, $curTab=='data_deletion');


$STEPS_OBJ = $libenroll->GET_STEPS_OBJ_ARR($Lang['eEnrolment']['Transfer_to_SP']['StepArr'], 1);
		
$fileNameArr = array();

# Get Post/ Files
$clubCSVName = $_FILES['clubCSV']['name'];
$postCSVName = $_FILES['postCSV']['name'];
$performanceCSVName = $_FILES['performanceCSV']['name'];


$thisAcademicYearID = $_POST['AcademicYearID'];
//$thisYearTermID = $_POST['YearTermID'];
$thisYearTermIDAry = $_POST['YearTermIDAry'];
$thisActiveMemberOnly = $_POST['ActiveMemberOnly'];
$thisYearIDArr = $_POST['YearIDArr'];
$thisDataType = $_POST['DataTypeArr'];
$reportCardReadableIndicator = $_POST['reportCardReadableIndicator'];
$ExportIndicator = $_POST['ExportIndicator'];
$uploadMethod = $_POST['uploadMethod'];

// debug_pr($_POST);

if($uploadMethod == 'mappingfile'){
		

	################################## Check Each File's Name Start ##################################
	$fileNameArr[] = $clubCSVName;
	$fileNameArr[] = $postCSVName;
	$fileNameArr[] = $performanceCSVName;
	
	foreach ($fileNameArr as $filename)
	{
		$ext = strtoupper($lo->file_ext($filename));	
		# check extension of import file 
		if($ext != ".CSV" && $ext != ".TXT")
		{
			header("Location: step1.php?msg=WrongFileFormat");
			exit();
		}		
	}
	################################## Check Each File's Name End ##################################
	
	################################## Check File Format Start ##################################
	$clubMappingInfoArr = $limport->GET_IMPORT_TXT($clubCSV);
	$postMappingInfoArr = $limport->GET_IMPORT_TXT($postCSV);
	$performanceMappingInfoArr = $limport->GET_IMPORT_TXT($performanceCSV);
	
	
	##### For generate_xls_final.php page #####
	$_SESSION['toWebSAMSClubData'] = $clubMappingInfoArr;
	$_SESSION['toWebSAMSPostData'] = $postMappingInfoArr;
	$_SESSION['toWebSAMSPerformanceData'] = $performanceMappingInfoArr;
	
	
	##### Club #####
	if(is_array($clubMappingInfoArr))
	{
		$clubColumnName = array_shift($clubMappingInfoArr);
	}
	
	$clubFileFormat = array('eClass Code', 'eClass Title (Eng)', 'eClass Term Name (Eng)', 'eClass Type', 'WebSAMS School Year', 'WebSAMS Duration', 'WebSAMS STA Code', 'WebSAMS STA Type', 'WebSAMS Report Card Readable Indicator', 'WebSAMS Major Components of Other Learning Experiences Code');
	$clubFormatWrong = false;
	for($i=0; $i<sizeof($clubFileFormat); $i++)
	{
		if ($clubColumnName[$i]!=$clubFileFormat[$i])
		{
			$clubFormatWrong = true;
			break;
		}
	}
	
	##### Post #####
	if(is_array($postMappingInfoArr))
	{
		$postColumnName = array_shift($postMappingInfoArr);
	}
	$postFileFormat = array('eClass Post', 'WebSAMS STA Post Code');
	$postFormatWrong = false;
	for($i=0; $i<sizeof($postFileFormat); $i++)
	{
		if ($postColumnName[$i] != $postFileFormat[$i])
		{
			$postFormatWrong = true;
			break;
		}
	}
	
	##### Performance #####
	if(is_array($performanceMappingInfoArr))
	{
		$performanceColumnName = array_shift($performanceMappingInfoArr);
	}
	
	$performanceFileFormat = array('eClass Performance', 'WebSAMS STA Performance Code');
	$performanceFormatWrong = false;
	for($i=0; $i<sizeof($performanceFileFormat); $i++)
	{
		if ($performanceColumnName[$i] != $performanceFileFormat[$i])
		{
			$performanceFormatWrong = true;
			break;
		}
	}
	if($clubFormatWrong || $postFormatWrong || $performanceFormatWrong)
	{
		header("Location: step1.php?msg=ImportUnsuccess_IncorrectHeaderFormat");
		exit();	
	}
	
}	


	################################## Check File Format End ##################################
	$linterface->LAYOUT_START();
	
	#####  Club & Activity Mapping #####
	
	### Club and Activity Info
	if(in_array('Club', $thisDataType)){
		# Club
	//	$targetYearTermId = '';
	//	if ($thisYearTermID == '') {
	//		$targetYearTermId = array(0);
	//	}
	//	$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $targetYearTermId, $OrderBy='ClubCode');
		$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');
		
		$clubCodeArrAssoc = Get_Array_By_Key($clubInfoAry, 'GroupCode'); ;
	}
	
	if(in_array('Activity', $thisDataType)){
		# Activity
		$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID); 
		$acitvityCodeArrAssoc = Get_Array_By_Key($activityInfoAry, 'ActivityCode'); 
	}
	
	$clubActivityCode = array_merge((array)$clubCodeArrAssoc, (array)$acitvityCodeArrAssoc);



### Post 
if(in_array('Club', $thisDataType)){
	$enrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
	
	$conds_activeMember = '';
	if ($thisActiveMemberOnly == 1) {
		$conds_activeMember = " And iug.isActiveMember = '1' ";
	}
	else if ($thisActiveMemberOnly == 2) {
		$conds_activeMember = " And iug.isActiveMember <> '1' ";
	}
	
	$sql = "Select
					ir.Title as RoleTitle
			From
					INTRANET_USERGROUP as iug
					Inner Join INTRANET_ROLE as ir On (iug.RoleID = ir.RoleID) 
					Inner join INTRANET_ENROL_GROUPCLASSLEVEL ieg on iug.EnrolGroupID = ieg.EnrolGroupID	
			Where
					ir.RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'
					And iug.EnrolGroupID In ('".implode("','", (array)$enrolGroupIdAry)."')
					AND ieg.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
					$conds_activeMember
					AND iug.RecordType is NULL
			Group By 
					BINARY ir.Title
			Order By
					BINARY ir.Title
			";				
	$clubRoleAry = $libenroll->returnVector($sql);
}

# Activity 
if(in_array('Activity', $thisDataType)){
	$enrolEventIdAry = Get_Array_By_Key($activityInfoAry, 'EnrolEventID');
	
	$conds_activeMember = '';
	if ($thisActiveMemberOnly == 1) {
		$conds_activeMember = " And ies.isActiveMember = '1' ";
	}
	else if ($thisActiveMemberOnly == 2) {
		$conds_activeMember = " And ies.isActiveMember <> '1' ";
	}
	
	$sql = "Select
					ies.RoleTitle
			From
					INTRANET_ENROL_EVENTSTUDENT ies
					Inner Join INTRANET_ENROL_EVENTCLASSLEVEL iee on iee.EnrolEventID = ies.EnrolEventID
			Where
					ies.EnrolEventID In ('".implode("','", (array)$enrolEventIdAry)."')
					AND iee.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
					$conds_activeMember
			Group By
					BINARY ies.RoleTitle
			Order By
					BINARY ies.RoleTitle
			";
	$activityRoleAry = $libenroll->returnVector($sql);
}
$clubActivityRole = array_merge((array)$clubRoleAry, (array)$activityRoleAry);


### Performance
if(in_array('Club', $thisDataType)){
	$sql = "Select
					iu.Performance
			From
					INTRANET_USERGROUP iu
					Inner join INTRANET_ENROL_GROUPCLASSLEVEL ieg on iu.EnrolGroupID = ieg.EnrolGroupID			 
			Where
					iu.EnrolGroupID In ('".implode("','", (array)$enrolGroupIdAry)."')
					AND ieg.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
			Group By 
					BINARY iu.Performance
			Order By
					BINARY iu.Performance
			";
	$clubPerformanceAry = $libenroll->returnVector($sql);
}

if(in_array('Activity', $thisDataType)){
	$sql = "Select
					ies.Performance
			From
					INTRANET_ENROL_EVENTSTUDENT ies
					Inner Join INTRANET_ENROL_EVENTCLASSLEVEL iee on iee.EnrolEventID = ies.EnrolEventID
			Where
					ies.EnrolEventID In ('".implode("','", (array)$enrolEventIdAry)."')
					AND iee.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
			Group By
					BINARY ies.Performance
			Order By
					BINARY ies.Performance
			";
	$activityPerformanceAry = $libenroll->returnVector($sql);
}
$clubActivityPerformance = array_merge((array)$clubPerformanceAry, (array)$activityPerformanceAry);



$clubActivityTable = '';
$clubActivityTable .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="common_table_list_v30">';
	$clubActivityTable .= '<thead>';
		$clubActivityTable .= '<tr>';
			$clubActivityTable .= '<th width="20px">#</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['eClassCode'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['eClassTitleEng'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['eClassTermNameEng'] .'</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['eClassType'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSchoolYear'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSDuration'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTACode'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAType'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSReportCardReadableIndicator'] . '</th>';
			$clubActivityTable .= '<th>' . $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSMajorComponentsofOtherLearningExperiencesCode'] . '</th>';
			$clubActivityTable .= '<th></th>';
		$clubActivityTable .= '</tr>';
	$clubActivityTable .= '</thead>';

	$clubActivityTable .= '<tbody>';

	if($uploadMethod == 'directly'){
		$clubMappingInfoArr = array();
		$counter = 0;
		######## club activity
		if(in_array('Club',(array)$thisDataType))
		{
			$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');
			$numOfClub = count($clubInfoAry);
			for ($i=0; $i<$numOfClub; $i++) {
				$_clubCode = $clubInfoAry[$i]['GroupCode'];
				$_clubTitle = $clubInfoAry[$i]['Title'];
				//		$_clubTitle = $clubInfoAry[$i]['TitleChinese'];
				$_yearTermId = $clubInfoAry[$i]['YearTermID'];
				$_WebSAMSCode = $clubInfoAry[$i]['WebSAMSCode'];
				$_WebSAMSSTAType= $clubInfoAry[$i]['WebSAMSSTAType'];
				$_recordType = 'C';
				
				if ($_yearTermId == 0) {
					$_yearTermName = 'Whole Year';
				}
				else {
					$_yearTermName = $termAssoAry[$_yearTermId];
				}
				
				$clubMappingInfoArr[$counter][] = $_clubCode;
				$clubMappingInfoArr[$counter][] = $_clubTitle;
//				$clubMappingInfoArr[$counter][] = $_yearTermName;
				$clubMappingInfoArr[$counter][] = $result = substr(getStartDateOfAcademicYear($thisAcademicYearID), 0, 4);
				$clubMappingInfoArr[$counter][] = $_recordType;
				$clubMappingInfoArr[$counter][] = null;
				$clubMappingInfoArr[$counter][] = null;
				$clubMappingInfoArr[$counter][] = $_WebSAMSCode;
				$clubMappingInfoArr[$counter][] = $_WebSAMSSTAType;
				$clubMappingInfoArr[$counter][] =$reportCardReadableIndicator;
				
				$counter++;
			}
	
		}
		
		if(in_array('Activity',(array)$thisDataType)){
			
			$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID);
			$numOfActivity = count($activityInfoAry);
			for ($i=0; $i<$numOfActivity; $i++) {
				$_activityCode = $activityInfoAry[$i]['ActivityCode'];
				$_activityTitle = $activityInfoAry[$i]['EventTitle'];
				$_WebSAMSCode = $activityInfoAry[$i]['WebSAMSCode'];
				$_WebSAMSSTAType= $activityInfoAry[$i]['WebSAMSSTAType'];
				$_yearTermName = '';
				$_recordType = 'A';
				
				$clubMappingInfoArr[$counter][] = $_activityCode;
				$clubMappingInfoArr[$counter][] = $_activityTitle;
				$clubMappingInfoArr[$counter][] = $_yearTermName;
				$clubMappingInfoArr[$counter][] = $_recordType;
				$clubMappingInfoArr[$counter][] = null;
				$clubMappingInfoArr[$counter][] = null;
				$clubMappingInfoArr[$counter][] = $_WebSAMSCode;
				$clubMappingInfoArr[$counter][] = $_WebSAMSSTAType;
				$clubMappingInfoArr[$counter][] =$reportCardReadableIndicator;
				$counter++;
			}
		

		}
		$emptyClubMappingTitle[] = array('','','','','','','','','');
		$emptyClubMappingTitle[] =  array('','','','','','','','','');
		$SessionclubMappingInfoArr = array_merge($emptyClubMappingTitle,$clubMappingInfoArr);
		
		$_SESSION['toWebSAMSClubData'] = $SessionclubMappingInfoArr;
		

		
		######### club activity END ##########	
	}
	$numOfClubMappingInfoArr = count($clubMappingInfoArr);
	$clubActivityError = 0;
	
		if($uploadMethod == 'directly'){
		//	debug_pr($i-1);
			for($i=0;$i<$numOfClubMappingInfoArr;$i++){
				list($thiseClassCode, $thiseClassTitleEng, $thiseClassTitleChi, $thiseClassType, $thisWebSAMSchoolYear, $thisWebSAMSDuration, $thisWebSAMSSTACode, $thisWebSAMSSTAType, $thisWebSAMSRCRIndicatior, $thisWebSAMSMCOOLECode) = $clubMappingInfoArr[$i];
				
				$thisWebSAMSMCOOLECode = $thisWebSAMSMCOOLECode?$thisWebSAMSMCOOLECode:$Lang['General']['EmptySymbol'];
				
				$thisClubActivityError = '';
				
				# Check if data is null
// 				if(trim($thisWebSAMSchoolYear)=='')
// 				{
// 					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSchoolYear'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
// 					$thisWebSAMSchoolYear = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
// 				}
// 				if(trim($thisWebSAMSDuration)=='')
// 				{
// 					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSDuration'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
// 					$thisWebSAMSDuration = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
// 				}
				if(trim($thisWebSAMSSTACode)=='')
				{
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTACode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSSTACode = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				
				if(trim($thisWebSAMSSTAType)=='')
				{
					
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAType'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSSTAType = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				
				if(trim($thisWebSAMSRCRIndicatior)=='')
				{
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSReportCardReadableIndicator'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSRCRIndicatior = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				
				# Check if club/ activity is in this Academic Year
				
				if($thiseClassCode!='' && !in_array($thiseClassCode,$clubActivityCode)){
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['eClassCode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] . "</li>";
					$thiseClassCode = "<font color='red'>" . $thiseClassCode . "</font>";
				}
				
				if($thisClubActivityError)
				{
					$clubActivityError++;
					$clubActivityTable .= '<tr>';
					$clubActivityTable .= '<td>' . ($i+1) . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassCode . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassTitleEng . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassTitleChi . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassType . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSchoolYear . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSDuration . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSSTACode . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSSTAType . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSRCRIndicatior . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSMCOOLECode . '</td>';
					$clubActivityTable .= '<td><font color="red"><ul>' . $thisClubActivityError . '</ul></font></td>';
					$clubActivityTable .= '</tr>';
				}

			}
		
		}else{
			
			for($i=1;$i<$numOfClubMappingInfoArr;$i++){
				list($thiseClassCode, $thiseClassTitleEng, $thiseClassTitleChi, $thiseClassType, $thisWebSAMSchoolYear, $thisWebSAMSDuration, $thisWebSAMSSTACode, $thisWebSAMSSTAType, $thisWebSAMSRCRIndicatior, $thisWebSAMSMCOOLECode) = $clubMappingInfoArr[$i];
			
				$thisWebSAMSMCOOLECode = $thisWebSAMSMCOOLECode?$thisWebSAMSMCOOLECode:$Lang['General']['EmptySymbol'];
				
				$thisClubActivityError = '';
				
 				# Check if data is null
				if(trim($thisWebSAMSchoolYear)=='')
				{
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSchoolYear'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSchoolYear = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				if(trim($thisWebSAMSDuration)=='')
				{
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSDuration'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSDuration = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				if(trim($thisWebSAMSSTACode)=='')
				{
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTACode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSSTACode = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				
				if(trim($thisWebSAMSSTAType)=='')
				{
					
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAType'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSSTAType = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				
				if(trim($thisWebSAMSRCRIndicatior)=='')
				{
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSReportCardReadableIndicator'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSRCRIndicatior = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
				}
				
				# Check if club/ activity is in this Academic Year
				
				if($thiseClassCode!='' && !in_array($thiseClassCode,$clubActivityCode)){
					$thisClubActivityError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['eClassCode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] . "</li>";
					$thiseClassCode = "<font color='red'>" . $thiseClassCode . "</font>";
				}
				
				if($thisClubActivityError)
				{
					$clubActivityError++;
					$clubActivityTable .= '<tr>';
					$clubActivityTable .= '<td>' . $i . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassCode . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassTitleEng . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassTitleChi . '</td>';
					$clubActivityTable .= '<td>' . $thiseClassType . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSchoolYear . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSDuration . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSSTACode . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSSTAType . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSRCRIndicatior . '</td>';
					$clubActivityTable .= '<td>' . $thisWebSAMSMCOOLECode . '</td>';
					$clubActivityTable .= '<td><font color="red"><ul>' . $thisClubActivityError . '</ul></font></td>';
					$clubActivityTable .= '</tr>';
				}
			
			}
	
		}
	
	$clubActivityTable .= '</tbody>';
$clubActivityTable .= '</table>';

$clubActivityTable .= '<br />';

##### Post #####
$postTable = '';
$postTable .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="common_table_list_v30">';
$postTable .= '<thead>';
$postTable .= '<tr>';
$postTable .= '<th>#</th>';
$postTable .= '<th width="35%">'.$Lang['eEnrolment']['TransferToWebSAMS']['eClassPost'].'</th>';
$postTable .= '<th width="35%">'.$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPostCode'].'</th>';
$postTable .= '<th></th>';
$postTable .= '</tr>';
$postTable .= '</thead>';
$postTable .= '<tbody>';




if($uploadMethod == 'directly'){
	############ POST MAPPING ##############
	
	### Club
	$postMappingInfoArr= array();
	if(in_array('Club',(array)$thisDataType))
	{
// 		$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');
		if(count($clubInfoAry)>0){
// 			$enrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
			
			$conds_activeMember = '';
			if ($thisActiveMemberOnly == 1) {
				$conds_activeMember = " And iug.isActiveMember = '1' ";
			}
			else if ($thisActiveMemberOnly == 2) {
				$conds_activeMember = " And iug.isActiveMember <> '1' ";
			}
	
			$sql = "Select
							ir.Title as RoleTitle,
							ir.WebSAMSCode
					From
							INTRANET_USERGROUP as iug
							Inner Join INTRANET_ROLE as ir On (iug.RoleID = ir.RoleID)
							Inner join INTRANET_ENROL_GROUPCLASSLEVEL ieg on iug.EnrolGroupID = ieg.EnrolGroupID
					Where
							ir.RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'
							And iug.EnrolGroupID In ('".implode("','", (array)$enrolGroupIdAry)."')
							AND ieg.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
                            AND iug.RecordType is NULL
							$conds_activeMember
							Group By
							BINARY ir.Title
							Order By
							BINARY ir.Title
							";
							
			$clubRoleAry= $libenroll->returnArray($sql);
			for($j=0;$j<count($clubRoleAry);$j++){
				$clubArr[$j][] = $clubRoleAry[$j]['RoleTitle'];
				$clubArr[$j][] = $clubRoleAry[$j]['WebSAMSCode'];
			}
			
						
		}
	}
	### Activity
	$activityRoleAry = array();
	if(in_array('Activity',(array)$thisDataType))
	{
// 		$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID);
// 		$enrolEventIdAry = Get_Array_By_Key($activityInfoAry, 'EnrolEventID');
		
		$conds_activeMember = '';
		if ($thisActiveMemberOnly == 1) {
			$conds_activeMember = " And ies.isActiveMember = '1' ";
		}
		else if ($thisActiveMemberOnly == 2) {
			$conds_activeMember = " And ies.isActiveMember <> '1' ";
		}
	
		$sql = "Select
						ies.RoleTitle,
						ir.WebSAMSCode as WebSAMSCode
				From
						INTRANET_ENROL_EVENTSTUDENT ies
						LEFT OUTER JOIN INTRANET_ROLE AS ir ON (ir.Title = ies.RoleTitle)
						Inner Join INTRANET_ENROL_EVENTCLASSLEVEL iee on iee.EnrolEventID = ies.EnrolEventID
						
				Where
						ies.EnrolEventID In ('".implode("','", (array)$enrolEventIdAry)."')
						AND iee.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
						$conds_activeMember
						AND ir.RecordType= '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'
						Group By
						BINARY ies.RoleTitle
						Order By
						BINARY ies.RoleTitle
						";
					
		$activityRoleAry=$libenroll->returnArray($sql);
		for($j=0;$j<count($activityRoleAry);$j++){
			$activityArr[$j][] = $activityRoleAry[$j]['RoleTitle'];
			$activityArr[$j][] = $activityRoleAry[$j]['WebSAMSCode'];
		}
		
						//$activityRoleAry = $libenroll->returnVector($sql);
	}
	$postMappingInfoArr = array();
	
	### Combine all roles from Clubs and Activities
	if(empty($clubArr)){
		$postMappingInfoArr= $activityArr;
	}else if(empty($activityArr)){
		$postMappingInfoArr= $clubArr;
	}else{
		$postMappingInfoArr= array_merge_recursive($clubArr, $activityArr);
	}
	
	if($postMappingInfoArr == ''){
		$postMappingInfoArr = array();
	}
	
	$emptyTitle[] =  array('','');
	$emptyTitle[] =  array('','');
	$SessionpostMappingInfoArr = array_merge($emptyTitle,$postMappingInfoArr);
	$_SESSION['toWebSAMSPostData'] = $SessionpostMappingInfoArr;
	
	############ POST MAPPING END##############

}

$numOfPostMappingInfoArr = count($postMappingInfoArr);

$postError = 0;

	if($uploadMethod == 'directly'){
		for($i=0;$i<$numOfPostMappingInfoArr;$i++)
		{
			list($thiseClassPost, $thisWebSAMSSTAPostCode) = $postMappingInfoArr[$i];
			$thisPostError = '';
			
			if(trim($thisWebSAMSSTAPostCode)=='')
			{
				$thisPostError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPostCode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
				$thisWebSAMSSTAPostCode = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
			}
			
			if($thiseClassPost!='' && !in_array($thiseClassPost, $clubActivityRole)){
				$thisPostError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['eClassPost'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] . "</li>";
				$thiseClassPost = "<font color='red'>" . $thiseClassPost . "</font>";
				
			}
			
			if($thisPostError){
				$postError++;
				$postTable .= '<tr>';
				$postTable .= '<td>' . ($i+1) . '</td>';
				$postTable .= '<td>' . $thiseClassPost . '</td>';
				$postTable .= '<td><ul><font color="red">' . $thisWebSAMSSTAPostCode . '</font></ul></td>';
				$postTable .= '<td><font color="red"><ul>' . $thisPostError . '</ul></font></td>';
				$postTable .= '</tr>';
			}
		
		
		}
	}else{
		
		for($i=1;$i<$numOfPostMappingInfoArr;$i++)
		{
			list($thiseClassPost, $thisWebSAMSSTAPostCode) = $postMappingInfoArr[$i];
			$thisPostError = '';
			
			if(trim($thisWebSAMSSTAPostCode)=='')
			{
				$thisPostError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPostCode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
				$thisWebSAMSSTAPostCode = "<font color='red'>" . $Lang['General']['EmptySymbol'] . "</font>";
			}
			
			if($thiseClassPost!='' && !in_array($thiseClassPost, $clubActivityRole)){
				$thisPostError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['eClassPost'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] . "</li>";
				$thiseClassPost = "<font color='red'>" . $thiseClassPost . "</font>";
				
			}
			
			if($thisPostError){
				$postError++;
				$postTable .= '<tr>';
				$postTable .= '<td>' . ($i) . '</td>';
				$postTable .= '<td>' . $thiseClassPost . '</td>';
				$postTable .= '<td><ul><font color="red">' . $thisWebSAMSSTAPostCode . '</font></ul></td>';
				$postTable .= '<td><font color="red"><ul>' . $thisPostError . '</ul></font></td>';
				$postTable .= '</tr>';
			}
		}
			
	}


$postTable .= '</tbody>';
$postTable .= '</table>';

$postTable .= '<br />';



##### Performance Mapping #####
$performanceTable = '';
$performanceTable .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="common_table_list_v30">';
	$performanceTable .= '<thead>';
		$performanceTable .= '<tr>';
		$performanceTable .= '<th>#</th>';
		$performanceTable .= '<th>'.$Lang['eEnrolment']['TransferToWebSAMS']['eClassPerformance'].'</th>';
		$performanceTable .= '<th>'.$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPerformanceCode'].'</th>';
		$performanceTable .= '<th></th>';
		$performanceTable .= '</tr>';
	$performanceTable .= '</thead>';

	$performanceTable .= '<tbody>';
	
	#########################
	
	if($uploadMethod == 'directly'){
		
		$clubPerformanceAry = array();
		if(in_array('Club',(array)$thisDataType))
		{
// 			$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');
// 			debug_pr($clubInfoAry);
 			if(count($clubInfoAry)>0){
// 				$enrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
				
				$conds_activeMember = '';
				if ($thisActiveMemberOnly == 1) {
					$conds_activeMember = " And iu.isActiveMember = '1' ";
				}
				else if ($thisActiveMemberOnly == 2) {
					$conds_activeMember = " And iu.isActiveMember <> '1' ";
				}
				
				$sql = "Select
						iu.Performance
				From
						INTRANET_USERGROUP iu
						Inner join INTRANET_ENROL_GROUPCLASSLEVEL ieg on iu.EnrolGroupID = ieg.EnrolGroupID
				Where
						iu.EnrolGroupID In ('".implode("','", (array)$enrolGroupIdAry)."')
						AND ieg.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
						$conds_activeMember
						Group By
						BINARY iu.Performance
						Order By
						BINARY iu.Performance
						";
						
						$clubPerformanceAry = $libenroll->returnVector($sql);
						
			}
		}
		
		$activityPerformanceAry = array();
		if(in_array('Activity',(array)$thisDataType)){
			### Activity
// 			$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID);
			if(count($activityInfoAry)>0){
// 				$enrolEventIdAry = Get_Array_By_Key($activityInfoAry, 'EnrolEventID');
				
				$conds_activeMember = '';
				if ($thisActiveMemberOnly == 1) {
					$conds_activeMember = " And ies.isActiveMember = '1' ";
				}
				else if ($thisActiveMemberOnly == 2) {
					$conds_activeMember = " And ies.isActiveMember <> '1' ";
				}
	
				$sql = "Select
						ies.Performance
				From
						INTRANET_ENROL_EVENTSTUDENT ies
						Inner Join INTRANET_ENROL_EVENTCLASSLEVEL iee on iee.EnrolEventID = ies.EnrolEventID
				Where
						ies.EnrolEventID In ('".implode("','", (array)$enrolEventIdAry)."')
						AND iee.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
						$conds_activeMember
						Group By
						BINARY ies.Performance
						Order By
						BINARY ies.Performance
						";
						$activityPerformanceAry = $libenroll->returnVector($sql);
			}
		}

		### Combine all roles from Clubs and Activities
		$allPerformanceAry = array_values(array_remove_empty(array_unique(array_merge($clubPerformanceAry, $activityPerformanceAry))));
		$numOfPerformance = count($allPerformanceAry);
		
		### Obtain WebSAMS Code
		
		
		for($i=0;$i<$numOfPerformance; $i++){
			$sql="SELECT WebSAMSCode
						FROM INTRANET_ENROL_COMMENT_BANK
					WHERE
						RecordStatus = '1' AND
						Comment = '".$allPerformanceAry[$i]."'" ;
			
			$return = $libenroll->returnVector($sql);
			$webSAMSArr[$i]= $return[0];
		}
		
		
		$performanceMappingInfoArr= array();
		$counter_i = 0;
		
		for ($i=0; $i<$numOfPerformance; $i++) {
			$_performance = $allPerformanceAry[$i];
			$_webSAMSCode = $webSAMSArr[$i];
			
			$performanceMappingInfoArr[$counter_i][] = $_performance;
			$performanceMappingInfoArr[$counter_i][] = $_webSAMSCode;
			$counter_i++;
		}
		
		$emptyPerforTitle[] =  array('','');
		$emptyPerforTitle[] =  array('','');
		$SessionperformanceMappingInfoArr= array_merge($emptyPerforTitle,$performanceMappingInfoArr);
		$_SESSION['toWebSAMSPerformanceData'] = $SessionperformanceMappingInfoArr;
	}
	
	$numOfPerformanceMappingInfoArr = count($performanceMappingInfoArr);
	$performanceError = 0;
	
		if($uploadMethod == 'directly'){
			for($i=0;$i<$numOfPerformanceMappingInfoArr;$i++){
				list($thiseClassPerformance, $thisWebSAMSSTAPerformanceCode) = $performanceMappingInfoArr[$i];
				$thisPerformanceError = '';
				
				if(trim($thisWebSAMSSTAPerformanceCode)=='')
				{
					$thisPerformanceError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPerformanceCode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSSTAPerformanceCode = $Lang['General']['EmptySymbol'] ;
				}
				
				if($thiseClassPerformance!='' && !in_array($thiseClassPerformance,$clubActivityPerformance)){
					$thisPerformanceError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['eClassPerformance'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] . "</li>";
					$thiseClassPerformance = "<font color='red'>" . $thiseClassPerformance . "</font>";
				}
				
				if($thisPerformanceError){
					$performanceError++;
					$performanceTable .= '<tr>';
					$performanceTable .= '<td width="20px">' . ($i+1). '</td>';
					$performanceTable .= '<td width="33%">' . $thiseClassPerformance . '</td>';
					$performanceTable .= '<td width="33%">' . $thisWebSAMSSTAPerformanceCode . '</td>';
					$performanceTable .= '<td width="33%"><ul><font color="red">' . $thisPerformanceError . '</font></ul></td>';
					$performanceTable .= '</tr>';
				}
			
			
			}
		}else{
			for($i=1;$i<$numOfPerformanceMappingInfoArr;$i++){
				list($thiseClassPerformance, $thisWebSAMSSTAPerformanceCode) = $performanceMappingInfoArr[$i];
				$thisPerformanceError = '';
				
				if(trim($thisWebSAMSSTAPerformanceCode)=='')
				{
					$thisPerformanceError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSSTAPerformanceCode'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isMissing'] . "</li>";
					$thisWebSAMSSTAPerformanceCode = $Lang['General']['EmptySymbol'] ;
				}
				
				if($thiseClassPerformance!='' && !in_array($thiseClassPerformance,$clubActivityPerformance)){
					$thisPerformanceError .= "<li>". $Lang['eEnrolment']['TransferToWebSAMS']['eClassPerformance'] . ' ' . $Lang['eEnrolment']['TransferToWebSAMS']['errorMsg']['isInvalid'] . "</li>";
					$thiseClassPerformance = "<font color='red'>" . $thiseClassPerformance . "</font>";
				}
				
				if($thisPerformanceError){
					$performanceError++;
					$performanceTable .= '<tr>';
					$performanceTable .= '<td width="20px">' . ($i). '</td>';
					$performanceTable .= '<td width="33%">' . $thiseClassPerformance . '</td>';
					$performanceTable .= '<td width="33%">' . $thisWebSAMSSTAPerformanceCode . '</td>';
					$performanceTable .= '<td width="33%"><ul><font color="red">' . $thisPerformanceError . '</font></ul></td>';
					$performanceTable .= '</tr>';
				}
			}
		}
		
	
	
	$performanceTable .='</tbody>';
$performanceTable .= '</table>';


######################################## Display Table ########################################
$resultTable = $libenroll_ui->GET_IMPORT_STEPS($CurrStep=2, $Lang['eEnrolment']['Transfer_to_SP']['StepArr']);
$resultTable .= $libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['TransferToWebSAMS']['Club&ActivityMapping']);
$resultTable .= '<br />';

# Activity & Club Display
$resultTable .= '<table class="form_table_v30" width="100%">';
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['NoOfRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		if($uploadMethod == 'directly'){
			$resultTable .= count($clubMappingInfoArr);
		}else{
			$resultTable .= count($clubMappingInfoArr) - 1 ;
		}
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['ValidRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		if($uploadMethod == 'directly'){
			$resultTable .= count($clubMappingInfoArr) - $clubActivityError ;
			
		}else{
			$resultTable .= count($clubMappingInfoArr) - 1 - $clubActivityError ;
		}
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['InvalidRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		$resultTable .= $clubActivityError==0?$clubActivityError:"<font color='red'>" . $clubActivityError . "</font>"; 
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
$resultTable .= '</table>';

if($clubActivityError>0)
{
	$resultTable .= $clubActivityTable;
}

# Post Display 
$resultTable .= $libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['TransferToWebSAMS']['PostMapping']);
$resultTable .= '<br />';
$resultTable .= '<table class="form_table_v30" width="100%">';
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['NoOfRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		if($uploadMethod == 'directly'){
			$resultTable .= count($postMappingInfoArr);
		}else{
			$resultTable .= count($postMappingInfoArr) - 1;
		}
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['ValidRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		if($uploadMethod == 'directly'){
			$resultTable .= count($postMappingInfoArr) - $postError;
		}else{
			$resultTable .= count($postMappingInfoArr) - $postError -1;
			
		}
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['InvalidRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		$resultTable .= $postError==0? $postError: "<font color='red'>" . $postError . "</font>"; 

		$resultTable .= '</td>';
	$resultTable .= '</tr>';
$resultTable .= '</table>';
$resultTable .= '<br />';


if($postError>0)
{
	$resultTable .= $postTable;
}


# Performance Display
$resultTable .= '<form method="post" action="generate_xls_final.php">';
$resultTable .= $libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['TransferToWebSAMS']['PerformanceMapping']);

$resultTable .= '<br />';
$resultTable .= '<table class="form_table_v30" width="100%">';
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['NoOfRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		if($uploadMethod == 'directly'){
			$resultTable .= count($performanceMappingInfoArr);
		}
		else{
			$resultTable .= count($performanceMappingInfoArr) - 1;
			
		}
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['ValidRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		if($uploadMethod == 'directly'){
			$resultTable .= count($performanceMappingInfoArr) - $performanceError;
		}else{
			$resultTable .= count($performanceMappingInfoArr) - 1 - $performanceError;
		}
		$resultTable .= '</td>';
	$resultTable .= '</tr>';
	
	$resultTable .= '<tr>';
		$resultTable .= '<td class="field_title">';
		$resultTable .= $Lang['eEnrolment']['TransferToWebSAMS']['InvalidRecord'];
		$resultTable .= '</td>';
		$resultTable .= '<td>';
		$resultTable .= $performanceError==0?$performanceError: "<font color='red'>" . $performanceError . "</font>"; 

		$resultTable .= '</td>';
	$resultTable .= '</tr>';
$resultTable .= '</table>';
$resultTable .= '<br />';

if($performanceError>0)
{
	$resultTable .= $performanceTable;
}


$resultTable .= '<div class="edit_bottom_v30">'."\n";
$resultTable .= '<p class="spacer"></p>'."\n";
if($clubActivityError==0 && $postError==0 && $performanceError==0)
{	
	$resultTable .= $libenroll_ui->GET_ACTION_BTN($Lang['Btn']['Continue'], "submit");
}
$resultTable .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location = 'step1.php'");
$resultTable .= '<p class="spacer"></p>'."\n";
$resultTable .= '</div>'."\n"; 

$resultTable .= '<input type="hidden" name="reportCardReadableIndicator" value="'.$reportCardReadableIndicator.'">';
$resultTable .= '<input type="hidden" name="ExportIndicator" value="'.$ExportIndicator.'">';
$resultTable .= '<input type="hidden" name="DataTypeArr" value="'.implode(',', (array)$thisDataType).'">';
$resultTable .= '<input type="hidden" name="YearTermIDAry" value="'.implode(',', (array)$thisYearTermIDAry).'">';
$resultTable .= '<input type="hidden" name="AcademicYearID" value="'.$AcademicYearID.'">';
$resultTable .= '<input type="hidden" name="ActiveMemberOnly" value="'.$thisActiveMemberOnly.'">';
$resultTable .= '<input type="hidden" name="YearIDArr" value="'.implode(',', (array)$YearIDArr).'">';

$resultTable .= '</form>';

echo $resultTable;

?>

<script language="javascript">
<?
if($ReturnMsg != "") {
	echo "Get_Return_Message('".$ReturnMsg."')\n";	
}
?>

var jsCurAcademicYearID = '<?=$AcademicYearID?>';
var jsCurYearTermID = '<?=$YearTermID?>';

$(document).ready( function () {
	jsCurAcademicYearID = $('select#AcademicYearID').val();
	js_Reload_Term_Selection(jsCurAcademicYearID);
});

function js_Changed_Academic_Year_Selection(jsValue)
{
	jsCurAcademicYearID = jsValue;
	js_Reload_Term_Selection(jsValue);
}

function js_Reload_Term_Selection(jsAcademicYearID)
{
	$('div#YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../../ajax_reload.php", 
		{ 
			Action: 'Term_Selection',
			SelectionID: 'YearTermID',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsCurYearTermID,
			NoFirst: 0,
			AllTitle: '<?=$Lang['eEnrolment']['WholeYear']?>',
			OnChange: 'js_Relaod_SP_Last_Transfer_Date('+jsAcademicYearID+', this.value)'
		},
		function(ReturnData)
		{
			if ($('select#YearTermID').val() != jsCurYearTermID)
			{
				$('select#YearTermID').val($('select#YearTermID :first-child').val());
				jsCurYearTermID = $('select#YearTermID').val();
			}
			js_Relaod_SP_Last_Transfer_Date(jsAcademicYearID, jsCurYearTermID);
		}
	);
}

function js_Relaod_SP_Last_Transfer_Date(jsAcademicYearID, jsYearTermID) {
	$('div#LastTransferDiv').html('<?/*=$linterface->Get_Ajax_Loading_Image()*/?>').load(
		"../../../ajax_reload.php", 
		{ 
			Action: 'Reload_SP_Last_Transfer_Date',
			SelectionID: 'YearTermID',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			NoFirst: 0,
			AllTitle: '<?=$Lang['eEnrolment']['WholeYear']?>',
			WithLastTransferDate: 1,
			OnChange: 'js_Relaod_SP_Last_Transfer_Date('+jsAcademicYearID+')'
		},
		function(ReturnData)
		{
			$('#LastTransferDiv').val(ReturnData);
		}
	);
}


function js_Check_Form()
{
	// Club and Activity
	var jsHasCheckDataType = false;
	$('.DataTypeChk').each( function() {
		if ($(this).attr('checked') == true)
		{
			jsHasCheckDataType = true;
			return false;
		}
	})
	if (jsHasCheckDataType == false)
	{
		alert('<?=$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['SelectDataSource']?>');
		$('input#SelectAll_DataTypeChk').focus();
		return false;
	}
	
	// Form
	var jsHasCheckForm = false;
	$('.FormChk').each( function() {
		if ($(this).attr('checked') == true)
		{
			jsHasCheckForm = true;
			return false;
		}
	})
	if (jsHasCheckForm == false)
	{
		alert('<?=$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['SelectForm']?>');
		$('input#SelectAll_TargetFormChk').focus();
		return false;
	}
	
	$('form#form1').attr('action', 'step2.php').submit();
}


function js_export_sample(formAction){
			
	$('#form1').attr('action', formAction);
	
	$('#form1').submit();
	
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>