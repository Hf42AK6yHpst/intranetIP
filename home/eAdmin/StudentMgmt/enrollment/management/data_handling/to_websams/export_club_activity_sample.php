<?php

// using: 

/****************************************************
 * Date:	2017-09-15 (Anna)
 * Details: added $_WebSAMSCode and $_WebSAMSSTAType in club and activity
 * 
 * Date:	2015-01-21 (Omas)
 * Details: Fix - Club name english display chinese name problem
 * 
 * Date: 	2013-06-25 (Rita) 
 * Details: Create this page
 ****************************************************/
 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$lfcm = new form_class_manage();

# Get Post Value
$thisAcademicYearID = $_POST['AcademicYearID'];
//$thisYearTermID =  $_POST['YearTermID'];
$thisYearTermIDAry =  $_POST['YearTermIDAry'];
$thisDataTypeArr = $_POST['DataTypeArr'];
$thisReportCardReadableIndicator = $_POST['reportCardReadableIndicator'];

# Header 
$exportColumnLang = array();
$exportColumnLang['En'][] = 'eClass Code';
$exportColumnLang['En'][] = 'eClass Title (Eng)';
$exportColumnLang['En'][] = 'eClass Term Name (Eng)';
$exportColumnLang['En'][] = 'eClass Type';
$exportColumnLang['En'][] = 'WebSAMS School Year';
$exportColumnLang['En'][] = 'WebSAMS Duration';
$exportColumnLang['En'][] = 'WebSAMS STA Code';
$exportColumnLang['En'][] = 'WebSAMS STA Type';
$exportColumnLang['En'][] = 'WebSAMS Report Card Readable Indicator';
$exportColumnLang['En'][] = 'WebSAMS Major Components of Other Learning Experiences Code';
$exportColumnLang['Ch'][] = 'eClass 編號';
$exportColumnLang['Ch'][] = 'eClass 名稱 (英文)';
$exportColumnLang['Ch'][] = 'eClass 學期名稱 (英文)';
$exportColumnLang['Ch'][] = 'eClass 類型';
$exportColumnLang['Ch'][] = 'WebSAMS 學年';
$exportColumnLang['Ch'][] = 'WebSAMS 時段';
$exportColumnLang['Ch'][] = 'WebSAMS 課外活動代碼';
$exportColumnLang['Ch'][] = 'WebSAMS 課外活動類型';
$exportColumnLang['Ch'][] = 'WebSAMS 成績表可讀取示標';
$exportColumnLang['Ch'][] = 'WebSAMS 其他學習經歷的主要種類代碼';

$ColumnPropertyArr = array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1);


$exportColumn = $lexport->GET_EXPORT_HEADER_COLUMN($exportColumnLang, $ColumnPropertyArr);

//$termAry = $lfcm->Get_Academic_Year_Term_List(Get_Current_Academic_Year_ID());

$termAry = $lfcm->Get_Academic_Year_Term_List($thisAcademicYearID);
$termAssoAry = BuildMultiKeyAssoc($termAry, 'YearTermID', 'YearTermNameEN', $SingleValue=1);

$exportDataAry = array();

###### Club ###### 
$counter = 0;
if(in_array('Club',(array)$thisDataTypeArr))
{
//	$targetYearTermId = '';
//	if ($thisYearTermID == '') {
//		$targetYearTermId = array(0);
//	}
	//$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $targetYearTermId, $OrderBy='ClubCode');
	$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');
	$numOfClub = count($clubInfoAry);
	for ($i=0; $i<$numOfClub; $i++) {
		$_clubCode = $clubInfoAry[$i]['GroupCode'];
		$_clubTitle = $clubInfoAry[$i]['Title'];
//		$_clubTitle = $clubInfoAry[$i]['TitleChinese'];
		$_yearTermId = $clubInfoAry[$i]['YearTermID'];
		$_WebSAMSCode = $clubInfoAry[$i]['WebSAMSCode'];
		$_WebSAMSSTAType= $clubInfoAry[$i]['WebSAMSSTAType'];
		$_recordType = 'C';
		
		if ($_yearTermId == 0) {
			$_yearTermName = 'Whole Year';
		}
		else {
			$_yearTermName = $termAssoAry[$_yearTermId];
		}
		
		$exportDataAry[$counter][] = $_clubCode;
		$exportDataAry[$counter][] = $_clubTitle;
		$exportDataAry[$counter][] = $_yearTermName;
		$exportDataAry[$counter][] = $_recordType;
		$exportDataAry[$counter][] = null;
		$exportDataAry[$counter][] = null;
		$exportDataAry[$counter][] = $_WebSAMSCode;
		$exportDataAry[$counter][] = $_WebSAMSSTAType;
		$exportDataAry[$counter][] =$thisReportCardReadableIndicator;
		
		$counter++;
	}
}

###### Activity ###### 
if(in_array('Activity',(array)$thisDataTypeArr)){
	
	$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID); 
	$numOfActivity = count($activityInfoAry);
	for ($i=0; $i<$numOfActivity; $i++) {
		$_activityCode = $activityInfoAry[$i]['ActivityCode'];
		$_activityTitle = $activityInfoAry[$i]['EventTitle'];
		$_WebSAMSCode = $activityInfoAry[$i]['WebSAMSCode'];
		$_WebSAMSSTAType= $activityInfoAry[$i]['WebSAMSSTAType'];
		$_yearTermName = '';
		$_recordType = 'A';
		
		$exportDataAry[$counter][] = $_activityCode;
		$exportDataAry[$counter][] = $_activityTitle;
		$exportDataAry[$counter][] = $_yearTermName;
		$exportDataAry[$counter][] = $_recordType;
		$exportDataAry[$counter][] = null;
		$exportDataAry[$counter][] = null;
		$exportDataAry[$counter][] = $_WebSAMSCode;
		$exportDataAry[$counter][] = $_WebSAMSSTAType;
		$exportDataAry[$counter][] =$thisReportCardReadableIndicator;
		$counter++;
	}
}

$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportDataAry, $exportColumn,"", "\r\n", "", 0, "11");

intranet_closedb();

$filename = 'club_activity_mapping_'.date('Ymd_His').'.csv';
$lexport->EXPORT_FILE($filename, $export_content);
?>