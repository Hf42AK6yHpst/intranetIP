<?php
// using: 

/******************************************
 * Date：	2019-10-28 Henry
 * 			- support inactive member (ActiveMemberOnly = 2)
 * 	
 *   Date:	2016-01-12	Kenneth
 * 			- Export WebSAMS Code
 * 
 * Date: 	2013-06-26 (Rita)
 * Details: Create this page
 ******************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$lfcm = new form_class_manage();

//debug_pr($_POST);

# Get Post Value
$thisAcademicYearID = $_POST['AcademicYearID'];
//$thisYearTermID =  $_POST['YearTermID'];
$thisYearTermIDAry =  $_POST['YearTermIDAry'];
$thisDataTypeArr = $_POST['DataTypeArr'];
$thisActiveMemberOnly = $_POST['ActiveMemberOnly'];
$thisYearIDArr = $_POST['YearIDArr'];

# Header
$exportColumnLang = array();
$exportColumnLang['En'][] = 'eClass Performance';
$exportColumnLang['En'][] = 'WebSAMS STA Performance Code';
$exportColumnLang['Ch'][] = 'eClass 表現';
$exportColumnLang['Ch'][] = 'WebSAMS 活動表現代碼';

$ColumnPropertyArr = array(1, 1);

$exportColumn = $lexport->GET_EXPORT_HEADER_COLUMN($exportColumnLang, $ColumnPropertyArr);

$clubPerformanceAry = array();
if(in_array('Club',(array)$thisDataTypeArr))
{
	### Club
//	$targetYearTermId = '';
//	if ($thisYearTermID == '') {
//		$targetYearTermId = array(0);
//	}
	//$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $targetYearTermId, $OrderBy='ClubCode');
	$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');

	if(count($clubInfoAry)>0){
		$enrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
		
		$conds_activeMember = '';
		if ($thisActiveMemberOnly == 1) {
			$conds_activeMember = " And iug.isActiveMember = '1' ";
		}
		else if ($thisActiveMemberOnly == 2) {
			$conds_activeMember = " And iug.isActiveMember <> '1' ";
		}
		
		$sql = "Select
						iu.Performance
				From
						INTRANET_USERGROUP iu
						Inner join INTRANET_ENROL_GROUPCLASSLEVEL ieg on iu.EnrolGroupID = ieg.EnrolGroupID			 
				Where
						iu.EnrolGroupID In ('".implode("','", (array)$enrolGroupIdAry)."')
						AND ieg.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
						$conds_activeMember
				Group By 
						BINARY iu.Performance
				Order By
						BINARY iu.Performance
				";
					
		$clubPerformanceAry = $libenroll->returnVector($sql);
	}
}

$activityPerformanceAry = array();
if(in_array('Activity',(array)$thisDataTypeArr)){
	### Activity
	$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID);
	if(count($activityInfoAry)>0){
		$enrolEventIdAry = Get_Array_By_Key($activityInfoAry, 'EnrolEventID');
		
		$conds_activeMember = '';
		if ($thisActiveMemberOnly == 1) {
			$conds_activeMember = " And ies.isActiveMember = '1' ";
		}
		else if ($thisActiveMemberOnly == 2) {
			$conds_activeMember = " And ies.isActiveMember <> '1' ";
		}

		$sql = "Select
						ies.Performance
				From
						INTRANET_ENROL_EVENTSTUDENT ies
						Inner Join INTRANET_ENROL_EVENTCLASSLEVEL iee on iee.EnrolEventID = ies.EnrolEventID
				Where
						ies.EnrolEventID In ('".implode("','", (array)$enrolEventIdAry)."')
						AND iee.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
						$conds_activeMember
				Group By
						BINARY ies.Performance
				Order By
						BINARY ies.Performance
				";
		$activityPerformanceAry = $libenroll->returnVector($sql);
	}
}

### Combine all roles from Clubs and Activities
$allPerformanceAry = array_values(array_remove_empty(array_unique(array_merge($clubPerformanceAry, $activityPerformanceAry))));
$numOfPerformance = count($allPerformanceAry);

### Obtain WebSAMS Code


for($i=0;$i<$numOfPerformance; $i++){
	$sql="SELECT WebSAMSCode 
			FROM INTRANET_ENROL_COMMENT_BANK 
			WHERE 
				RecordStatus = '1' AND
				Comment = '".$allPerformanceAry[$i]."'" ;

	$return = $libenroll->returnVector($sql);
	$webSAMSArr[$i]= $return[0];
}


$exportDataAry = array();
$counter_i = 0;
for ($i=0; $i<$numOfPerformance; $i++) {
	$_performance = $allPerformanceAry[$i];
	$_webSAMSCode = $webSAMSArr[$i];
	
	$exportDataAry[$counter_i][] = $_performance;
	$exportDataAry[$counter_i][] = $_webSAMSCode;
	$counter_i++;
}

//$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportDataAry, $exportColumn);
$export_content = $lexport->GET_EXPORT_TXT($exportDataAry, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

intranet_closedb();

$filename = 'performance_mapping_'.date('Ymd_His').'.csv';
$lexport->EXPORT_FILE($filename, $export_content);
?>