<?php
// Using:

/********************************************************
 * Date 2020-11-06 Tommy
 *      sql get current year student class, not selected year student class
 *
 * Date 2020-06-10 Henry
 *      bug fix for the wrong error msg [Case#W186984]
 *
 * Date 2019-03-21 Anna
 *      fixed 158201, $clubInfoArr title have &
 *
 * Date 2017-09-25 Anna
 * 		add event mapping
 *
 * Date	2017-06-09 Omas
 * 		fix 0 is being omitted problem - #H118276
 *
 * Date 2017-06-06 Anna
 * 		added $ExportIndicator to generate single file
 *
 * Date	2015-02-02 Omas
 * 		fix club name with single quote cannot get mapping problem - #P92347
 * Date 2015-06-05 Omas
 *		Remove temp file when temp from last session is found
 *
 * Date: 2013-07-05 (Rita)
 * Details: Create and modified this page
 ********************************************************/

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

/** PHPExcel_IOFactory */
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");

intranet_auth();
intranet_opendb();

$libfs = new libfilesystem();
$libimport = new libimporttext();
$libenroll = new libclubsenrol();
$libgs = new libgeneralsettings();


# some parameters may need to change below
$IsChecking = false;
$GetStudentRegNoFrom = "MAPPING"; //"MAPPING" or "DB";

# [END] some parameters may need to change below
function showChecking($echoText)
{
	global $IsChecking;

	if ($IsChecking)
	{
		debug($echoText);
	}
}

############################################################################
##################### Temp hard-coded settings [Start] #####################
############################################################################

# Check if from $_POST
if(!$_POST)
{
	header("Location: step1.php");
	exit();
}

# Get POST Value
//$targetAcademicYearID = Get_Current_Academic_Year_ID();
$targetAcademicYearID = trim($_POST['AcademicYearID']);
//$thisYearTermID =  $_POST['YearTermID'];
$thisYearTermIDAry =  explode(',', IntegerSafe($_POST['YearTermIDAry']));
$ActiveMemberOnly = $_POST['ActiveMemberOnly'];
$reportCardReadableIndicator = $_POST['reportCardReadableIndicator'];
$ExportIndicator = $_POST['ExportIndicator'];
$YearIDArr = explode(',', IntegerSafe($_POST['YearIDArr']));
$DataTypeAry = explode(',',$_POST['DataTypeArr']);

if($ExportIndicator == 'Single'){
	$sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls'] = true;
}
//debug_pr($reportCardReadableIndicator);

# default not to show the record for Report Card
//$reportCardReadableIndicator = 'N';
showChecking("school academic year ID: ".$targetAcademicYearID);

# $YearTermIDArr (should set for the term and/or whole year)
############################################################################
###################### Temp hard-coded settings [End] ######################
############################################################################


############################################################################
################## Retrieve data from JUPAS files [Start] ##################
############################################################################

### Get Club Mapping Info
$clubMappingInfoArr = $_SESSION['toWebSAMSClubData'];
array_shift($clubMappingInfoArr);
array_shift($clubMappingInfoArr);


$numOfClubFromCsv = count($clubMappingInfoArr);

//debug_pr($clubMappingInfoArr);
$clubMappingAssoArr = array();
for ($i=0; $i<$numOfClubFromCsv; $i++) {
	$_code = $clubMappingInfoArr[$i][6];
	$_title = $clubMappingInfoArr[$i][1];
	$_duration = $clubMappingInfoArr[$i][5];
	$_type = $clubMappingInfoArr[$i][7];
	$_ercdisplay = strtoupper(trim($clubMappingInfoArr[$i][8]));
	if ($_ercdisplay=="YES" || $_ercdisplay=="1" || $_ercdisplay=="Y")
	{
		$_ercdisplay = "Y";
	}
	else {
		$_ercdisplay = "N";
	}
	$_OLEcomponents = trim($clubMappingInfoArr[$i][9]);
// 	$targetAcademicYear = $clubMappingInfoArr[$i][4];
	$targetAcademicYear = $clubMappingInfoArr[$i][2];


	# find duplicated codes
	if (!$checked[$_code])
	{
		for ($j=$i+1; $j<$numOfClubFromCsv; $j++)
		{
			$jj_code = $clubMappingInfoArr[$j][0];
			$jj_title = $clubMappingInfoArr[$j][1];
			$p = 0;
			if ($_code==$jj_code)
			{
				similar_text($_title, $jj_title, $p);
				$color_used = ($p<50) ? "red" : "grey";
				$duplicated_str .= "<br /><font color='$color_used'>  [{$i}] ".$_code.":".$_title." vs ".$jj_code.":".$jj_title." [".$j."] ({$p} % like)</font>";
			}
		}
	}
	// #P92347
	$_title = intranet_htmlspecialchars($_title);
	$clubMappingAssoArr[$_title]['code'] = $_code;
	$clubMappingAssoArr[$_title]['duration'] = $_duration;
	$clubMappingAssoArr[$_title]['type'] = $_type;
	//$clubMappingAssoArr[$_title]['ercdisplay'] = $_ercdisplay;
	$clubMappingAssoArr[$_title]['academic_year'] = $targetAcademicYear;
	//$clubMappingAssoArr[$_title]['reportCardIndicator'] = $_ercdisplay;
	$clubMappingAssoArr[$_title]['reportCardIndicator'] = $_ercdisplay;

	$checked[$_code] = true;
}

unset($clubMappingInfoArr);

### Get Post Mapping Info
$postMappingInfoArr = $_SESSION['toWebSAMSPostData'];
array_shift($postMappingInfoArr);
array_shift($postMappingInfoArr);
$postMappingAssoArr = build_assoc_array($postMappingInfoArr);
unset($postMappingInfoArr);
### Get Performance Mapping Info
$performanceMappingInfoArr = $_SESSION['toWebSAMSPerformanceData'];
array_shift($performanceMappingInfoArr);
array_shift($performanceMappingInfoArr);
$performanceMappingAssoArr = build_assoc_array($performanceMappingInfoArr);

unset($performanceMappingInfoArr);

# get from database
// $sql = "SELECT ClassName, ClassNumber, WebSAMSRegNo FROM INTRANET_USER WHERE RecordType=2 ORDER BY ClassName, ClassNumber";
$sql = "Select yc.ClassTitleEN as ClassName, ycu.ClassNumber, iu.WebSAMSRegNo FROM YEAR_CLASS yc 
        LEFT JOIN YEAR_CLASS_USER ycu ON (yc.YearClassID= ycu.YearClassID) 
        LEFT JOIN INTRANET_USER iu ON (ycu.UserID = iu.UserID) 
        WHERE iu.RecordType=2 AND yc.AcademicYearID ='".$targetAcademicYearID."' ORDER BY ClassName, ClassNumber";
$std_rows = $libenroll->returnArray($sql);

for ($i=0; $i<sizeof($std_rows); $i++)
{
	$data_i = $std_rows[$i];
	// STID, REGNO, STRN, CLASS, CLASSNO;
	if ($data_i["ClassName"]!="" && $data_i["ClassNumber"]!="" && $data_i["WebSAMSRegNo"]!="")
	{
		$RegNoArrs[$data_i["ClassName"]][(int) $data_i["ClassNumber"]] = (string) (str_replace("#", "", $data_i["WebSAMSRegNo"]));
	}
}
############################################################################
################### Retrieve data from JUPAS files [End] ###################
############################################################################


############################################################################
################## Retrieve data from eEnrolment [Start] ###################
############################################################################
### Get Club of the selected Academic Year
//$targetYearTermId = '';
//if ($thisYearTermID == '') {
//	$targetYearTermId = array(0);
//}
//$clubInfoArr = $libenroll->GET_GROUPINFO($ParEnrolGroupID='', $targetAcademicYearID, $ClubType='', $targetYearTermId);
$clubInfoArr = $libenroll->Get_All_Club_Info('', $targetAcademicYearID, '', '', $thisYearTermIDAry, $OrderBy='ClubCode');
$numOfClub = count($clubInfoArr);


$CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club($targetAcademicYearID);

$CategoryID_conds = '';

$EnrolGroupID_conds = '';
if (count($CurrentYearClubArr) > 0) {
	$CurrentYearClubList = implode(',', $CurrentYearClubArr);
	$EnrolGroupID_conds = "AND (ieei.EnrolGroupID = '' Or ieei.EnrolGroupID Is Null Or ieei.EnrolGroupID In ($CurrentYearClubList)) ";
}
$ActivityInfoArr = $libenroll->Get_Activity_Info($EnrolGroupID_conds,$targetAcademicYearID);
$numOfActivity = count($ActivityInfoArr);


### Get Club Member info of the specific Academic Year
//debug($targetAcademicYearID);
$clubMemberInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupIDArr='', $PersonTypeArr=array(USERTYPE_STUDENT), $targetAcademicYearID, $IndicatorArr=0, $IndicatorWithStyle=1, $WithEmptySymbol=0, $ReturnAsso=1,$thisYearTermIDAry, $ClubKeyword='', $ClubCategoryArr='', $ActiveMemberOnly, $YearIDArr);


$EventMemberInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr='', $ActivityKeyword='', $targetAcademicYearID, $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1,$ActiveMemberOnly=0, $FormIDArr='', $enrollStatusAry='2', $ReturnAsso=false,$studentIDArr='', $sel_categoryIDArr='');
$AssocKeyAry = array();
$AssocKeyAry = array('EnrolEventID','StudentID');
$EventMemberInfoArr = BuildMultiKeyAssoc($EventMemberInfoArr,$AssocKeyAry);

// debug_pr($EventMemberInfoArr);

############################################################################
################### Retrieve data from eEnrolment [End] ####################
############################################################################

############################################################################
############### Prepare zip and xls files directory [Start] ################
############################################################################
### Prepare csv and zip files Folder
$TempFolder = $intranet_root."/file/temp/enrol_to_websams";
$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
$TempCsvFolder = $TempUserFolder.'/to_websams';

$ZipFileName = "to_websams.zip";
$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;

if (!file_exists($TempCsvFolder)) {
	$SuccessArr['CreateTempCsvFolder'] = $libfs->folder_new($TempCsvFolder);
}
else{
	// Omas Clear Temp for last session
	$fileList = array_diff(scandir($TempCsvFolder), array('..','.'));

	if(count($fileList) > 0){
		foreach((array)$fileList as $filename){
			$libfs->file_remove($TempCsvFolder.'/'.$filename);
		}
	}
}
############################################################################
################ Prepare zip and xls files directory [End] #################
############################################################################


############################################################################
######################### Build xls files [Start] ##########################
############################################################################
### Define xls Header
$xlsHeaderArr = array();
$xlsHeaderArr[] = 'Registration Number';
$xlsHeaderArr[] = 'School Year';
$xlsHeaderArr[] = 'Duration';
$xlsHeaderArr[] = 'STA Code';
$xlsHeaderArr[] = 'STA Type';
$xlsHeaderArr[] = 'STA Post Code';
$xlsHeaderArr[] = 'STA Performance Code';
$xlsHeaderArr[] = 'Report Card Readable Indicator';
$xlsHeaderArr[] = 'Major Components of Other Learning Experiences Code';
$xlsHeaderArr[] = 'Awards / Certifications / Achievements Code';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 1) in English';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 1) in Chinese';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 2) in English';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 2) in Chinese';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 3) in English';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 3) in Chinese';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 4) in English';
$xlsHeaderArr[] = 'Awards / Certifications /Achievements (by text 4) in Chinese';
$numOfHeader = count($xlsHeaderArr);

### Build xls
$debugInfoArr = array();
$successArr = array();
$_rowCount = 0;

if(in_array('Club',(array)$DataTypeAry))
{
	for ($i=0; $i<$numOfClub; $i++) {

		$_enrolGroupId = $clubInfoArr[$i]['EnrolGroupID'];

// 		$_clubTitle = str_replace("&amp;", "&", $clubInfoArr[$i]['Title']); //$clubInfoArr[$i]['TitleChinese'];
		$_clubTitle = $clubInfoArr[$i]['Title'];
		// #P92347
		$_clubTitle = intranet_htmlspecialchars($_clubTitle);

		if (!isset($clubMappingAssoArr[$_clubTitle])) {
			$_clubTitle = str_replace("&amp;", "&", $clubInfoArr[$i]['TitleChinese']); //$clubInfoArr[$i]['TitleChinese'];
		}

		$_clubCode = $clubMappingAssoArr[$_clubTitle]['code'];
		$_clubDuration = $clubMappingAssoArr[$_clubTitle]['duration'];
		$_clubType = $clubMappingAssoArr[$_clubTitle]['type'];
		$targetAcademicYear = $clubMappingAssoArr[$_clubTitle]['academic_year'];
		$ERC_display = $clubMappingAssoArr[$_clubTitle]['reportCardIndicator'];
		if ($ERC_display == '') {
			$ERC_display = $reportCardReadableIndicator;
		}
// 		debug_pr($_clubTitle);
// 		debug_Pr($clubMappingAssoArr);
		# Reportcard Indicator Display
	//	if($reportCardReadableIndicator=="Y"){
	//		$ERC_display =	($clubMappingAssoArr[$_clubTitle]['ercdisplay']!="") ? $clubMappingAssoArr[$_clubTitle]['ercdisplay'] : $reportCardReadableIndicator;
	//	}else{
	//		$ERC_display = $reportCardReadableIndicator;
	//	}



		$_xlsFileName = $_clubCode.'_'.$_enrolGroupId.'.xls';

		// Get club member info
		$_memberInfoArr = array();
		if (trim($_clubCode)!="")
		{
			$_memberInfoArr = $clubMemberInfoArr[$_enrolGroupId];
		}

		$_numOfMember = count($_memberInfoArr);
		if (!$sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls'] || $i==0) {
			// flag disabled => one club one xls => do initiation for each clubs
			// flag enabled => all data one xls => only $i==0 do initiation

				$objPHPExcel = new PHPExcel();
				// Set properties
				$objPHPExcel->getProperties()->setCreator("eClass")
											 ->setLastModifiedBy("eClass")
											 ->setTitle("eClass enrolment records")
											 ->setSubject("eClass enrolment records")
											 ->setDescription("to websams")
											 ->setKeywords("eClass enrolment records")
											 ->setCategory("eClass");


				// Create a first sheet, representing sales data
				$objPHPExcel->setActiveSheetIndex(0);

				$ActiveSheet = $objPHPExcel->getActiveSheet();

				// Build xls header
				for ($j=0; $j<$numOfHeader; $j++)
				{
					$ActiveSheet->setCellValue(chr(ord("A")+$j).'1', $xlsHeaderArr[$j]);
				}
				$_rowCount=0;


		}
		// Build xls content
		for ($j=0; $j<$_numOfMember; $j++)
		{

			$__studentWebSAMSRegNo = $RegNoArrs[$_memberInfoArr[$j]['ClassName']][(int) $_memberInfoArr[$j]['ClassNumber']];
	//		debug_pr($_memberInfoArr[$j]['EnglishName']);
			if (trim($__studentWebSAMSRegNo)=="" || strlen($__studentWebSAMSRegNo)<4)
			{
				$errors[] = $_memberInfoArr[$j]['ClassName']."-". $_memberInfoArr[$j]['ClassNumber'];
			}

			$__role = $_memberInfoArr[$j]['RoleTitle'];
			$__roleCode = $postMappingAssoArr[$__role];
			$__performance = $_memberInfoArr[$j]['Performance'];
			$__performanceCode = $performanceMappingAssoArr[$__performance];

			$ActiveSheet->getCell('A'.($_rowCount+2))->setValueExplicit($__studentWebSAMSRegNo, PHPExcel_Cell_DataType::TYPE_STRING);
			$ActiveSheet->getCell('B'.($_rowCount+2))->setValueExplicit($targetAcademicYear, PHPExcel_Cell_DataType::TYPE_STRING);
			$ActiveSheet->getCell('C'.($_rowCount+2))->setValueExplicit($_clubDuration, PHPExcel_Cell_DataType::TYPE_STRING);
			$ActiveSheet->getCell('D'.($_rowCount+2))->setValueExplicit($_clubCode, PHPExcel_Cell_DataType::TYPE_STRING);
			//#H118276
			//$ActiveSheet->setCellValue('E'.($_rowCount+2), $_clubType);
			$ActiveSheet->getCell('E'.($_rowCount+2))->setValueExplicit($_clubType, PHPExcel_Cell_DataType::TYPE_STRING);
			$ActiveSheet->getCell('F'.($_rowCount+2))->setValueExplicit($__roleCode, PHPExcel_Cell_DataType::TYPE_STRING);

			//#H118276
	 		//$ActiveSheet->setCellValue('G'.($_rowCount+2), $__performanceCode);
			$ActiveSheet->getCell('G'.($_rowCount+2))->setValueExplicit($__performanceCode, PHPExcel_Cell_DataType::TYPE_STRING);

			//#H118276
			//$ActiveSheet->setCellValue('H'.($_rowCount+2), $ERC_display);
			$ActiveSheet->getCell('H'.($_rowCount+2))->setValueExplicit($ERC_display, PHPExcel_Cell_DataType::TYPE_STRING);

			$_rowCount++;

		}


		$path2write = ($_rowCount<=0) ? $TempCsvFolder.'/ZZ_NoData_'.$_xlsFileName : $TempCsvFolder.'/'.$_xlsFileName;


		if (!$sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls']) {

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$successArr[$_enrolGroupId]['GenerateXls'] = $objWriter->save($path2write);
		}
	}
}
if(in_array('Activity',(array)$DataTypeAry)){
	for ($i=0; $i<$numOfActivity; $i++) {

		$_enrolEventId = $ActivityInfoArr[$i]['EnrolEventID'];

		$_eventTitle = str_replace("&amp;", "&", $ActivityInfoArr[$i]['EventTitle']); //$clubInfoArr[$i]['TitleChinese'];
		// #P92347
		$_eventTitle= intranet_htmlspecialchars($_eventTitle);

// 		if (!isset($clubMappingAssoArr[$_clubTitle])) {
// 			$_clubTitle = str_replace("&amp;", "&", $clubInfoArr[$i]['TitleChinese']); //$clubInfoArr[$i]['TitleChinese'];
// 		}

		$_eventCode = $clubMappingAssoArr[$_eventTitle]['code'];
		$_eventDuration = $clubMappingAssoArr[$_eventTitle]['duration'];
		$_eventType = $clubMappingAssoArr[$_eventTitle]['type'];
		$targetAcademicYear = $clubMappingAssoArr[$_eventTitle]['academic_year'];
		$ERC_display = $clubMappingAssoArr[$_eventTitle]['reportCardIndicator'];
		if ($ERC_display == '') {
			$ERC_display = $reportCardReadableIndicator;
		}


		$_xlsFileName = $_eventCode.'_'.$_enrolEventId.'.xls';

				// Get club member info
		$_memberInfoArr = array();
		if (trim($_eventCode)!="")
		{
			$_memberInfoArr = $EventMemberInfoArr[$_enrolEventId];
		}

				$_numOfMember = count($_memberInfoArr);

				if (!$sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls'] || (!in_array('Club',(array)$DataTypeAry) && $i==0 ) ) {
					// flag disabled => one club one xls => do initiation for each clubs
					// flag enabled => all data one xls => only $i==0 do initiation

					$objPHPExcel = new PHPExcel();
					// Set properties
					$objPHPExcel->getProperties()->setCreator("eClass")
					->setLastModifiedBy("eClass")
					->setTitle("eClass enrolment records")
					->setSubject("eClass enrolment records")
					->setDescription("to websams")
					->setKeywords("eClass enrolment records")
					->setCategory("eClass");


					// Create a first sheet, representing sales data
					$objPHPExcel->setActiveSheetIndex(0);

					$ActiveSheet = $objPHPExcel->getActiveSheet();

					// Build xls header
					for ($j=0; $j<$numOfHeader; $j++)
					{
						$ActiveSheet->setCellValue(chr(ord("A")+$j).'1', $xlsHeaderArr[$j]);
					}

					$_rowCount = 0;

				}
		//		debug_pr($_memberInfoArr);

	//			debug_pr($_memberInfoArr);
				// Build xls content
// 				for ($j=0; $j<$_numOfMember; $j++)
// 				{

			if(!empty($_memberInfoArr)){
				//debug_pr($_memberInfoArr);
				foreach ($_memberInfoArr as $StudentID => $value){
				//	debug_pr($StudentID);
					list($EnrolEventID,$RecordStatus,$StudentName,$ClassName,$ClassNumber,$UserLogin,$UserID,$StudentID,$isActiveMember,$ActiveMemberStatus,$RoleTitle,$WebSAMSRegNo,$Performance)=$value;


					$__studentWebSAMSRegNo = $RegNoArrs[$_memberInfoArr[$StudentID]['ClassName']][(int) $_memberInfoArr[$StudentID]['ClassNumber']];



						if (trim($__studentWebSAMSRegNo)=="" || strlen($__studentWebSAMSRegNo)<4)
						{
							$errors[] = $_memberInfoArr[$StudentID]['ClassName']."-". $_memberInfoArr[$StudentID]['ClassNumber'];
						}

						$__role = $_memberInfoArr[$StudentID]['RoleTitle'];
						$__roleCode = $postMappingAssoArr[$__role];
						$__performance = $_memberInfoArr[$StudentID]['Performance'];
						$__performanceCode = $performanceMappingAssoArr[$__performance];


						$ActiveSheet->getCell('A'.($_rowCount+2))->setValueExplicit($__studentWebSAMSRegNo, PHPExcel_Cell_DataType::TYPE_STRING);
						$ActiveSheet->getCell('B'.($_rowCount+2))->setValueExplicit($targetAcademicYear, PHPExcel_Cell_DataType::TYPE_STRING);
						$ActiveSheet->getCell('C'.($_rowCount+2))->setValueExplicit($_eventDuration, PHPExcel_Cell_DataType::TYPE_STRING);
						$ActiveSheet->getCell('D'.($_rowCount+2))->setValueExplicit($_eventCode, PHPExcel_Cell_DataType::TYPE_STRING);
						//#H118276
						//$ActiveSheet->setCellValue('E'.($_rowCount+2), $_clubType);
						$ActiveSheet->getCell('E'.($_rowCount+2))->setValueExplicit($_eventType, PHPExcel_Cell_DataType::TYPE_STRING);
						$ActiveSheet->getCell('F'.($_rowCount+2))->setValueExplicit($__roleCode, PHPExcel_Cell_DataType::TYPE_STRING);

						//#H118276
						//$ActiveSheet->setCellValue('G'.($_rowCount+2), $__performanceCode);
						$ActiveSheet->getCell('G'.($_rowCount+2))->setValueExplicit($__performanceCode, PHPExcel_Cell_DataType::TYPE_STRING);

						//#H118276
						//$ActiveSheet->setCellValue('H'.($_rowCount+2), $ERC_display);
						$ActiveSheet->getCell('H'.($_rowCount+2))->setValueExplicit($ERC_display, PHPExcel_Cell_DataType::TYPE_STRING);

						$_rowCount++;

					}
			}


				$path2write = ($_rowCount<=0) ? $TempCsvFolder.'/ZZ_NoData_'.$_xlsFileName : $TempCsvFolder.'/'.$_xlsFileName;

				if (!$sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls']) {

					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$successArr[$_enrolEventId]['GenerateXls'] = $objWriter->save($path2write);
				}
	}
}


if ($sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls']) {
	$path2write = $TempCsvFolder.'/toWebSAMS.xls';
	$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($path2write);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$successArr['all club&act']['GenerateXls'] = $objWriter->save($path2write);
}

// debug_pr($successArr);
if (sizeof($errors)>0)
{
	echo "The following students have no WebSAMSRegNo in eClass!<br />";
	echo implode(",",$errors);
	intranet_closedb();
	die();
}


$SettingAry = array();
$SettingAry['Generate_WebSAMS_Excel'] = date('Y-m-d H:i:s');
$libgs->Save_General_Setting($libenroll->ModuleTitle, $SettingAry);

intranet_closedb();
############################################################################
########################## Build xls files [End] ###########################
############################################################################


############################################################################
######################## Generate zip file [Start] #########################
############################################################################
### Delete old zip file and zip the current csv files
if ($sys_custom['eEnrolment']['ExportToWebSAMS']['singleXls']) {
	output2browser(get_file_content($path2write), 'toWebSAMS.xls');
}
else {
	$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
	$SuccessArr['ZipCsvFiles'] = $libfs->file_zip('to_websams', $ZipFilePath, $TempUserFolder);

	### remove the folder
	$SuccessArr['DeleteTempCsvFiles'] = $libfs->folder_remove_recursive($TempCsvFolder);

	### output the zip file to browser and let the user download it.
	output2browser(get_file_content($ZipFilePath), $ZipFileName);
}

############################################################################
######################### Generate zip file [End] ##########################
############################################################################
?>