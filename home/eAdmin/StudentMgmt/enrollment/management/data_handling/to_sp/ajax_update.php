<?php
//using: 
/*******************
 * Date: 	2013-01-04 (Rita)
 * Details:	add DataNameLang Variables for Club Name Option
 * 
 * 
 ********************/

@set_time_limit(21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();

$Action = trim(urldecode(stripslashes($_REQUEST['Action'])));

if ($Action == 'Transfer_Data_To_SP')
{
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$DataTypeArr = explode(',', $_REQUEST['DataTypeArr']);
	$ActiveMemberOnly = $_REQUEST['ActiveMemberOnly'];
	$YearIDArr = explode(',', $_REQUEST['YearIDArr']);
	$DataNameLang = $_POST['DataNameLang'];
	
	### Get Academic Year Info
	$AcademicYearObj = new academic_year($AcademicYearID);
	$AcademicYearName = $AcademicYearObj->YearNameEN;
	
	### Get Academic Year Term
	$YearTermInfoArr = $AcademicYearObj->Get_Term_List($returnAsso=0, $ParLang='en');
	$YearTermInfoAssoArr = BuildMultiKeyAssoc($YearTermInfoArr, 'YearTermID');
	$YearTermIDArr = Get_Array_By_Key($YearTermInfoArr, 'YearTermID');
	$numOfYearTerm = count($YearTermIDArr);
	$FirstYearTermID = $YearTermIDArr[0];
	
	
	### Get Student of the Required Form
	$libFCM = new form_class_manage();
	$FormStudentInfoArr = $libFCM->Get_Student_By_Form($AcademicYearID, $YearIDArr);
	$FormStudentIDArr = Get_Array_By_Key($FormStudentInfoArr, 'UserID');
	
	$SuccessArr = array();
	$libenroll->Start_Trans();
	
	
	### Club Handling ###
	if (in_array('Club', $DataTypeArr))
	{
		### Get Club and Member Info
		$ClubType = ($YearTermID == '' || $YearTermID == 0)? 'Y' : 'S';
		$ClubInfoArr = $libenroll->GET_GROUPINFO($ParEnrolGroupID='', $AcademicYearID, $ClubType, $YearTermID);
		$EnrolGroupIDArr = Get_Array_By_Key($ClubInfoArr, 'EnrolGroupID');
		$numOfClub = count($ClubInfoArr);
		
		$ClubMemberInfoArr = $libenroll->Get_Club_Member_Info('', $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('GroupAdmin'), $IndicatorWithStyle=1, $WithEmptySymbol=0, $ReturnAsso=1,
																$YearTermID, $ClubKeyword='', $ClubCategoryArr='', $ActiveMemberOnly, $YearIDArr);
		
		$TotalNumOfClubRecord = 0;
		foreach((array)$ClubMemberInfoArr as $thisEnrolGroupID => $thisMemberInfoArr)
		{
			$TotalNumOfClubRecord += count($thisMemberInfoArr);
		}
		
		# Update Progress Display
		$thisJSMsg = str_replace('<!--NumOfRecord-->', '<span id="NumOfTransferredSpan">0</span> / '.$TotalNumOfClubRecord, $Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataPreparationArr']['Club']);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.js_Update_Span_Info(\'ProgressSpan\', \''.$thisJSMsg.'\');';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		
		
		### Delete the old records of the selected Clubs of the selected Form's Student of the selected Academic Year
		$sql = "Delete From
					PROFILE_STUDENT_ACTIVITY
				Where
					AcademicYearID = '".$AcademicYearID."'
					And FromModule = 'eEnrol'
					And eEnrolRecordType = 'C'
					And eEnrolRecordID In ('".implode("','", $EnrolGroupIDArr)."')
					And UserID In ('".implode("','", $FormStudentIDArr)."')
				";
		$SuccessArr['Club']['DeleteOldRecords'] = $libenroll->db_db_query($sql);
		
		### Prepare to insert new records
		$InsertArr = array();
		$UpdateArr = array();
		$ClubMemberProcessCounter = 0;
		for ($i=0; $i<$numOfClub; $i++)
		{
			$thisEnrolGroupID = $ClubInfoArr[$i]['EnrolGroupID'];
			
			//2012-0628-1115-06071
			//$thisClubName = $ClubInfoArr[$i]['Title'];
			
			//$thisClubName = Get_Lang_Selection($ClubInfoArr[$i]['TitleChinese'], $ClubInfoArr[$i]['Title']);
			# Check Club Name Lang Display Option 
			if($DataNameLang=='1'){
				$thisClubName = $ClubInfoArr[$i]['Title'];
			}else{
				$thisClubName = $ClubInfoArr[$i]['TitleChinese'];
			}
			
			$thisClubName = $libenroll->Get_Safe_Sql_Query($thisClubName);
			
			$thisClubYearTermID = $ClubInfoArr[$i]['Semester'];
			$thisClubType = ($thisClubYearTermID=='' || $thisClubYearTermID==0)? 'Y' : 'S';
			
			$thisMemberInfoArr = $ClubMemberInfoArr[$thisEnrolGroupID];
			$thisNumOfMember = count($thisMemberInfoArr);
			
			for ($j=0; $j<$thisNumOfMember; $j++)
			{
				$thisStudentID = $thisMemberInfoArr[$j]['StudentID'];
				$thisYearClassID = $thisMemberInfoArr[$j]['YearClassID'];
				$thisClassName = $thisMemberInfoArr[$j]['ClassName'];
				$thisClassNumber = $thisMemberInfoArr[$j]['ClassNumber'];
				$thisStudentName = $thisMemberInfoArr[$j]['StudentName'];
				$thisRoleTitle = ($thisMemberInfoArr[$j]['RoleTitle']=='')? '' : $thisMemberInfoArr[$j]['RoleTitle'];
				$thisPerformance = ($thisMemberInfoArr[$j]['Performance']=='')? '' : $thisMemberInfoArr[$j]['Performance'];
				$thisComment = ($thisMemberInfoArr[$j]['Comment']=='')? '' : $thisMemberInfoArr[$j]['Comment'];
				$thisActiveMemberStatus = $thisMemberInfoArr[$j]['ActiveMemberStatus'];
				$thisEnrolYearTermID = $thisMemberInfoArr[$j]['EnrolYearTermID'];
				
				$thisClassName = $libenroll->Get_Safe_Sql_Query($thisClassName);
				$thisStudentName = $libenroll->Get_Safe_Sql_Query($thisStudentName);
				$thisRoleTitle = $libenroll->Get_Safe_Sql_Query($thisRoleTitle);
				$thisPerformance = $libenroll->Get_Safe_Sql_Query($thisPerformance);
				$thisComment = $libenroll->Get_Safe_Sql_Query($thisComment);
				
				//if ($thisClubType=='Y' && $thisEnrolYearTermID==$FirstYearTermID)
				if($thisClubType=='Y' && $YearTermID==0)
				{
					# One Whole Year Record for Year-based Club if the Student has joined the Club in the 1st Term (if Semester equals to empty string, SP will display as "Whole Year")
					$InsertArr[] = "(
										'$thisStudentID', '$AcademicYearName', '', '$thisClubName', '$thisRoleTitle', '$thisPerformance', '$thisClassName', '$thisClassNumber', now(), now(),
										'$AcademicYearID', null, '$thisYearClassID', 'eEnrol', 'C', '$thisEnrolGroupID'
									)";
				}
				else if ($thisClubType=='Y' && $thisEnrolYearTermID!=$FirstYearTermID)
				{
					# One Record for each Terms for Year-based Club if the Student did NOT join the Club in the 1st Term
					$StartTransferTerm = false;		
					for ($k=0; $k<$numOfYearTerm ; $k++)
					{
						$thisYearTermID = $YearTermIDArr[$k];
						
						if ($thisEnrolYearTermID == $thisYearTermID)
							$StartTransferTerm = true;
							
						if ($StartTransferTerm == true)
						{
							$thisYearTermName = $libenroll->Get_Safe_Sql_Query($YearTermInfoAssoArr[$thisYearTermID]['TermName']);
							$InsertArr[] = "(
												'$thisStudentID', '$AcademicYearName', '$thisYearTermName', '$thisClubName', '$thisRoleTitle', '$thisPerformance', '$thisClassName', '$thisClassNumber', now(), now(),
												'$AcademicYearID', '$thisYearTermID', '$thisYearClassID', 'eEnrol', 'C', '$thisEnrolGroupID'
											)";
						}
					}
				}
				else if ($thisClubType=='S')
				{
					# Transfer the Term same as the Term defined for the Club
					$thisYearTermName = $libenroll->Get_Safe_Sql_Query($YearTermInfoAssoArr[$thisClubYearTermID]['TermName']);
					$InsertArr[] = "(
										'$thisStudentID', '$AcademicYearName', '$thisYearTermName', '$thisClubName', '$thisRoleTitle', '$thisPerformance', '$thisClassName', '$thisClassNumber', now(), now(),
										'$AcademicYearID', '$thisClubYearTermID', '$thisYearClassID', 'eEnrol', 'C', '$thisEnrolGroupID'
									)";
				}
				
				# Update Progress Display
				$ClubMemberProcessCounter++;
				$thisJSUpdate = '';
				$thisJSUpdate .= '<script language="javascript">'."\n";
					$thisJSUpdate .= 'window.parent.js_Update_Span_Info(\'NumOfTransferredSpan\', \''.$ClubMemberProcessCounter.'\');';
				$thisJSUpdate .= '</script>'."\n";
				echo $thisJSUpdate;
			}
		}
		
		# Update Progress Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.js_Update_Span_Info(\'ProgressSpan\', \''.$Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataTransferringArr']['Club'].'\');';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		# Insert Student Subject Scores
		$numOfInsertData = count($InsertArr);
		if ($numOfInsertData > 0) {
			$numOfDataPerChunck = 1000;
			$numOfChunk = ceil($numOfInsertData / $numOfDataPerChunck);
			$insertDataSplitedArr = array_chunk($InsertArr, $numOfChunk);
			
			foreach((array)$insertDataSplitedArr as $_insertDateArr) {
				$sql = "Insert Into PROFILE_STUDENT_ACTIVITY
						(	
							UserID, Year, Semester, ActivityName, Role, Performance, ClassName, ClassNumber, DateInput, DateModified, 
							AcademicYearID, YearTermID, YearClassID, FromModule, eEnrolRecordType, eEnrolRecordID
						)
					Values
						".implode(',', (array)$_insertDateArr);
				$SuccessArr['Club']['Insert_Records'][] = $libenroll->db_db_query($sql);
			}
		}
	}	// End of Club Handling
	
	
	### Activity Handling ###
	if (in_array('Activity', $DataTypeArr) && ($YearTermID == '' || $YearTermID == 0))
	{
		### Get Activity Info
		$ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr='', $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=0, $ActiveMemberOnly, $YearIDArr);
		$EnrolEventIDArr = array_keys($ActivityInfoArr);
		$numOfActivity = count($ActivityInfoArr);
		
		$TotalNumOfActivityRecord = 0;
		foreach((array)$ActivityInfoArr as $thisEnrolEventID => $thisActivityInfoArr)
		{
			$thisMemberInfoArr = $thisActivityInfoArr['StatusStudentArr'][2];
			$TotalNumOfActivityRecord += count((array)$thisMemberInfoArr);
		}
		
		# Update Progress Display
		$thisJSMsg = str_replace('<!--NumOfRecord-->', '<span id="NumOfTransferredSpan">0</span> / '.$TotalNumOfActivityRecord, $Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataPreparationArr']['Activity']);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.js_Update_Span_Info(\'ProgressSpan\', \''.$thisJSMsg.'\');';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		
		
		### Delete the old records of the selected Activities of the selected Form's Student of the selected Academic Year
		$sql = "Delete From
					PROFILE_STUDENT_ACTIVITY
				Where
					AcademicYearID = '".$AcademicYearID."'
					And FromModule = 'eEnrol'
					And eEnrolRecordType = 'A'
					And eEnrolRecordID In ('".implode("','", $EnrolEventIDArr)."')
					And UserID In ('".implode("','", $FormStudentIDArr)."')
				";
		$SuccessArr['Activity']['DeleteOldRecords'] = $libenroll->db_db_query($sql);
		
		### Prepare to insert new records
		$InsertArr = array();
		$UpdateArr = array();
		$ActivityParticipantProcessCounter = 0;
		foreach((array)$ActivityInfoArr as $thisEnrolEventID => $thisActivityInfoArr)
		{
			$thisActivityName = $thisActivityInfoArr['EventTitle'];
			$thisActivityName = $libenroll->Get_Safe_Sql_Query($thisActivityName);
			
			$thisMemberInfoArr = $thisActivityInfoArr['StatusStudentArr'][2];
			$thisNumOfMember = count($thisMemberInfoArr);
			
			if ($thisNumOfMember == 0)
				continue;
			
			for ($j=0; $j<$thisNumOfMember; $j++)
			{
				$thisStudentID = $thisMemberInfoArr[$j]['StudentID'];
				$thisYearClassID = $thisMemberInfoArr[$j]['YearClassID'];
				$thisClassName = $thisMemberInfoArr[$j]['ClassName'];
				$thisClassNumber = $thisMemberInfoArr[$j]['ClassNumber'];
				$thisStudentName = $thisMemberInfoArr[$j]['StudentName'];
				$thisRoleTitle = ($thisMemberInfoArr[$j]['RoleTitle']=='')? '' : $thisMemberInfoArr[$j]['RoleTitle'];
				$thisPerformance = ($thisMemberInfoArr[$j]['Performance']=='')? '' : $thisMemberInfoArr[$j]['Performance'];
				$thisComment = ($thisMemberInfoArr[$j]['Comment']=='')? '' : $thisMemberInfoArr[$j]['Comment'];
				$thisActiveMemberStatus = $thisMemberInfoArr[$j]['ActiveMemberStatus'];
				
				$thisClassName = $libenroll->Get_Safe_Sql_Query($thisClassName);
				$thisStudentName = $libenroll->Get_Safe_Sql_Query($thisStudentName);
				$thisRoleTitle = $libenroll->Get_Safe_Sql_Query($thisRoleTitle);
				$thisPerformance = $libenroll->Get_Safe_Sql_Query($thisPerformance);
				$thisComment = $libenroll->Get_Safe_Sql_Query($thisComment);
				
				### Assuming all Activities are Year-based Activity at this time
				# One Whole Year Record for Year-based Activity (if Semester equals to empty string, SP will display as "Whole Year")
				$InsertArr[] = "(
									'$thisStudentID', '$AcademicYearName', '', '$thisActivityName', '$thisRoleTitle', '$thisPerformance', '$thisClassName', '$thisClassNumber', now(), now(),
									'$AcademicYearID', null, '$thisYearClassID', 'eEnrol', 'A', '$thisEnrolEventID'
								)";
								
				
				# Update Progress Display
				$ActivityParticipantProcessCounter++;
				$thisJSUpdate = '';
				$thisJSUpdate .= '<script language="javascript">'."\n";
					$thisJSUpdate .= 'window.parent.js_Update_Span_Info(\'NumOfTransferredSpan\', \''.$ActivityParticipantProcessCounter.'\');';
				$thisJSUpdate .= '</script>'."\n";
				echo $thisJSUpdate;
			}
		}
		
		
		# Update Progress Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.js_Update_Span_Info(\'ProgressSpan\', \''.$Lang['eEnrolment']['Transfer_to_SP']['ProgressDisplayArr']['DataTransferringArr']['Activity'].'\');';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		# Insert Student Subject Scores
		$numOfInsertData = count($InsertArr);
		if ($numOfInsertData > 0) {
			$numOfDataPerChunck = 1000;
			$numOfChunk = ceil($numOfInsertData / $numOfDataPerChunck);
			$insertDataSplitedArr = array_chunk($InsertArr, $numOfChunk);
			
			foreach((array)$insertDataSplitedArr as $_insertDateArr) {
				$sql = "Insert Into PROFILE_STUDENT_ACTIVITY
							(	
								UserID, Year, Semester, ActivityName, Role, Performance, ClassName, ClassNumber, DateInput, DateModified, 
								AcademicYearID, YearTermID, YearClassID, FromModule, eEnrolRecordType, eEnrolRecordID
							)
						Values
							".implode(',', (array)$_insertDateArr);
				$SuccessArr['Activity']['Insert_Records'][] = $libenroll->db_db_query($sql);
			}
		}
	}	// End of Activity Handling
	
	
	
	### Log the Action
	$TransferDetailsAssoArr = array();
	$TransferDetailsAssoArr['AcademicYearID'] = $AcademicYearID;
	$TransferDetailsAssoArr['YearTermID'] = $YearTermID;
	$TransferDetailsAssoArr['DataTypeArr'] = implode(',', $DataTypeArr);
	$TransferDetailsAssoArr['ActiveMemberOnly'] = $ActiveMemberOnly;
	$TransferDetailsAssoArr['YearIDArr'] = implode(',', $YearIDArr);
	
	$TransferDetailsArr = array();
	foreach ((array)$TransferDetailsAssoArr as $thisField => $thisValue)
	{
		$TransferDetailsArr[] = $thisField.'='.$thisValue;
	}
	$TransferDetailsText = implode('|||', $TransferDetailsArr);
	
	$sql = "Insert Into MODULE_RECORD_TRANSFER_LOG
				(FromModule, ToModule, RecordDetail, LogDate, LogBy)
			Values
				('eEnrol', 'StudentProfile', '".$libenroll->Get_Safe_Sql_Query($TransferDetailsText)."', now(), '".$_SESSION['UserID']."')
			";
	$SuccessArr['Add_Log'] = $libenroll->db_db_query($sql);
	
	if (in_multi_array(false, $SuccessArr))
	{
		$libenroll->RollBack_Trans();
		$ReturnMsgKey = 'failed';
	}
	else
	{
		$libenroll->Commit_Trans();
		$ReturnMsgKey = 'success';
	}
	
	# store last transfer time
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$libgs = new libgeneralsettings();
	$yearTermText = ($YearTermID==0) ? "wholeyear" : $YearTermID;
	$name = "TransferToSP_".$AcademicYearID."_".$yearTermText;
	$ary[$name] = date('Y-m-d H:i:s')."::".$UserID;
	$libgs->Save_General_Setting('eEnrolment', $ary);
	
	
	
	### Go to Step 3
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.js_Go_Step3(\''.$ReturnMsgKey.'\');';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();
?>