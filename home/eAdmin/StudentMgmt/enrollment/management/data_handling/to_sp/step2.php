<?php
//using: 
/*******************
 * Date	: 2015-07-08 Omas
 * Details: updated left menu
 * 
 * Date: 	2013-01-04 (Rita)
 * Details:	add DataNameLang Variables for Club Name Option
 * 
 * 
 *******************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

### Cookies handling
$arrCookies = array();
$arrCookies[] = array("eEnrol_management_transfer2SP_AcademicYearID", "AcademicYearID");
$arrCookies[] = array("eEnrol_management_transfer2SP_YearTermID", "YearTermID");
$arrCookies[] = array("eEnrol_management_transfer2SP_DataTypeArr", "DataTypeText");
$arrCookies[] = array("eEnrol_management_transfer2SP_ActiveMemberOnly", "ActiveMemberOnly");
$arrCookies[] = array("eEnrol_management_transfer2SP_YearIDArr", "YearIDText");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$AcademicYearID = '';
	$YearTermID = 0;
	$DataTypeArr = '';
	$ActiveMemberOnly = '';
	$YearIDArr = '';
}
else 
{
	$DataTypeText = implode(',', $DataTypeArr);
	$YearIDText = implode(',', $YearIDArr);
	updateGetCookies($arrCookies);
}


intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
if (!$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = ($_REQUEST['YearTermID']=='')? 0 : $_REQUEST['YearTermID'];
$DataTypeArr = $_REQUEST['DataTypeArr'];
$ActiveMemberOnly = $_REQUEST['ActiveMemberOnly'];
$YearIDArr = $_REQUEST['YearIDArr'];
$DataNameLang = $_POST['DataNameLang'];


# check any existing records which created in Admin Console (if yes, terminate data transfer)
$anyRecordFromAdminConsole = $libenroll->Check_Any_Record_From_AdminConsole($AcademicYearID, $YearTermID, $DataTypeArr, $YearIDArr);

if($anyRecordFromAdminConsole) {
	header("Location: step1.php?ReturnMsg=".$Lang['eEnrolment']['ReturnMsg_DataComeFromAdminConsole']);
	exit;	
}


# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], "", 1);
		
$linterface->LAYOUT_START();

echo $libenroll_ui->Get_Management_TransferToSP_Step2_UI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr, $DataNameLang);

?>

<script language="javascript">
$(document).ready( function () {

});

function js_Go_Back()
{
	window.location = 'step1.php';
}

function js_Print_Details()
{
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}

function js_Continue()
{
	if (confirm('<?=$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['TransferDataToSP']?>'))
	{
		Block_Document('<span id="ProgressSpan">' + '<?=$Lang['eEnrolment']['Transfer_to_SP']['PreparingData']?>' + '</span>', '400px');
	
		$('form#form1').attr('target', 'TransferIFrame').attr('action', 'ajax_update.php?Action=Transfer_Data_To_SP').submit();
	}
}

function js_Update_Span_Info(jsSpanID, jsSpanHTML)
{
	$('span#' + jsSpanID).html(jsSpanHTML);
}

function js_Go_Step3(jsReturnMsgKey)
{
	window.location = 'step3.php?ReturnMsgKey=' + jsReturnMsgKey;
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>