<?php

# using: 

##### Change Log [Start] #####
#
#   Date    :   2020-01-16  Tommy
#               fouce KIS client to to_sp page since KIS client not using to_ole
#
#	Date	:	2015-07-07	Omas
#				Fixed: Club / Activity disappear issue #P80762 
#
#	Date	:	2015-06-01	Omas
#				Merged to Data Handling , added Tab
#
#	Date	:	2014-12-31	Omas
#				Event SQL - not join Category since no use and will affect $sys_custom['eEnrolment']['TWGHCYMA']
#
#	Date	:	2014-10-15	Omas
#				Add filtering for select clubs/activity that transferred/not transferrent to SLP
#
#	Date	:	2014-10-14	Omas
#				Improved checkbox will not disable if no date/period
#
#	Date	:	2012-01-04	YatWoon
#				Fixed: Display activities which un-match with any categories. [Case#2012-0103-1210-01066]
#
#	Date	:	2011-09-27	Henry Chow
# 				add default value to $RecordType if it is NULL
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
###### Change Log [End] ######

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

if($_SESSION["platform"] == "KIS"){
    header("Location: /home/eAdmin/StudentMgmt/enrollment/management/data_handling/index.php");
    exit();
}

### Cookies handling
$arrCookies = array();
$arrCookies[] = array("eEnrol_management_transfer2OLE_AcademicYearID", "AcademicYearID");
$arrCookies[] = array("eEnrol_management_transfer2OLE_RecordType", "RecordType");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	//$AcademicYearID = Get_Current_Academic_Year_ID();
	//$RecordType = 'club';
}
else 
{
	updateGetCookies($arrCookies);
}


intranet_auth();
intranet_opendb();

if(!isset($AcademicYearID) || $AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();

$libenroll = new libclubsenrol();	
$linterface = new interface_html();

//$LibUser = new libuser($UserID);

# Check access right
// $libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
if ($plugin['eEnrollmentLite'] || !$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Tab
$curTab = 'to_ole';
$data_batch_process_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_sp/step1.php";
$transer_to_websams_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_websams/step1.php";
$transer_to_ole_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_ole/step1.php?clearCoo=1";
$data_deletion_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/data_deletion/data_deletion.php";
$transer_to_cees_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_cees/step1.php";
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], $data_batch_process_link, $curTab=='to_sp');
if($plugin['iPortfolio']) {
	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], $transer_to_ole_link, $curTab=='to_ole');
}
$TAGS_OBJ[] = array($Lang['eEnrolment']['data_handling']['export_websams'], $transer_to_websams_link, $curTab=='to_websams');
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClearAllEnrol'], $data_deletion_link, $curTab=='data_deletion');

if($plugin['SDAS_module']['KISMode']){
	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_CEES'], $transer_to_cees_link, $curTab=='to_cees');
}
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step1'], 1);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step2'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step3'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step4'], 0);
		
$linterface->LAYOUT_START();


### Academic Year Selection
$thisTag = 'onchange="js_Changed_Academic_Year_Selection(this.value);"';
$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $thisTag, $noFirst=0, $noPastYear=0, $AcademicYearID);

####Type Selection
$RecordType = $RecordType ? $RecordType : "club";

$TypeSelect = '';
$TypeSelect .= "<SELECT name='RecordType' onChange='this.form.submit()'>\n";
$TypeSelect.= "<OPTION value='club'  ". ($RecordType=="club"? "selected":"") .">". $eEnrollment['Club_Records'] ."</OPTION>\n";
$TypeSelect.= "<OPTION value='activity' ". ($RecordType=="activity"? "selected":"") .">". $eEnrollment['Activity_Records'] ."</OPTION>\n";
$TypeSelect.= "</SELECT>\n";

###Transfer Status Filtering
$TransferStatus = $TransferStatus ? $TransferStatus : "no";
$TransferStatusSelect = '';
$TransferStatusSelect .= "<SELECT name='TransferStatus' onChange='this.form.submit()'>\n";
$TransferStatusSelect.= "<OPTION value='all'" . ($TransferStatus=="all"? "selected":"") .">". $eEnrollment['all'] ."</OPTION>\n";
$TransferStatusSelect.= "<OPTION value='no'" . ($TransferStatus=="no"? "selected":"") .">". $Lang['eEnrolment']['Transfer_to_SLP']['Filter']['NotYetTransfer'] ."</OPTION>\n";
$TransferStatusSelect.= "<OPTION value='yes'" . ($TransferStatus=="yes"? "selected":"") .">". $eEnrollment['Already_Transfered'] ."</OPTION>\n";
$TransferStatusSelect.= "</SELECT>\n";


# Transfer button
$transferBtn = $linterface->GET_SMALL_BTN($eEnrollment['proceed_to_step2'], "button", "ClickTransfer();");

#select 50 items button
$select50 = $linterface->GET_SMALL_BTN($Lang['eEnrolment']['Btn']['Select50Items'], "button", "selectNext50();")."&nbsp;";

#Filtering Transfer Status to Select data
$SelectTransferStatus = "";
if($TransferStatus == "yes"){
	$SelectTransferStatus = "And a.OLE_ProgramID IS NOT NULL";
}
else if($TransferStatus =="no"){
	$SelectTransferStatus = "And a.OLE_ProgramID IS NULL";
}
else{
	$SelectTransferStatus = "";
}


# Table setting
if($RecordType=="club")
{
	$titleField = Get_Lang_Selection('b.TitleChinese', 'b.Title');
	
	$sql = "SELECT 
				$titleField as title,  
				if(left(min(c.ActivityDateStart),10) = left(max(c.ActivityDateEnd),10), left(min(c.ActivityDateStart),10), concat(left(min(c.ActivityDateStart),10) , ' - ' , left(max(c.ActivityDateEnd),10))) as dateperiod,
				a.OLE_ProgramID,
				a.EnrolGroupID,
				a.Semester
			FROM 
				INTRANET_ENROL_GROUPINFO as a
				Inner Join INTRANET_GROUP as b ON a.GroupID = b.GroupID
				LEFT OUTER JOIN INTRANET_ENROL_GROUP_DATE as c ON c.EnrolGroupID  = a.EnrolGroupID AND (c.RecordStatus IS NULL OR c.RecordStatus = 1)
			WHERE
				/*(c.RecordStatus IS NULL OR c.RecordStatus = 1)
				AND*/
				b.AcademicYearID = '".$AcademicYearID."'
				And
				b.RecordType = 5
				$SelectTransferStatus
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				b.Title
			";
	$infoArr = $libenroll->returnArray($sql, 4);
	
	# Build GroupIDArr
	$EnrolGroupIDArr = array();
	$infoSize = count($infoArr);
	for ($i=0; $i<$infoSize; $i++)
	{
		$EnrolGroupIDArr[] = $infoArr[$i]['EnrolGroupID'];
	}
	
	# Get number of members of each group
	$numOfMemberArr = $libenroll->GET_ARR_NUMBER_OF_MEMBER($EnrolGroupIDArr, "club");
	
	# Build Associate Array
	// $numOfMemberMap[#GroupID] = #numOfMember
	$numOfMemberMap = array();
	$infoMember = count($numOfMemberArr);
	for ($i=0; $i<$infoMember; $i++)
	{
		$thisGroupID = $numOfMemberArr[$i]['EnrolGroupID'];
		$thisNumOfMember = $numOfMemberArr[$i]['COUNT(*)'];
		$numOfMemberMap[$thisGroupID] = $thisNumOfMember;
	}
	
}
else
{
	# Get Current Year Club
	$CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club($AcademicYearID);
	
	$EnrolGroupID_conds = '';
	if (count($CurrentYearClubArr) > 0)
	{
		$CurrentYearClubList = implode(',', $CurrentYearClubArr);
		$EnrolGroupID_conds = " Or a.EnrolGroupID In ($CurrentYearClubList) ";
	}
	
	$sql ="
		SELECT
			a.EventTitle as title,
			if(left(min(b.ActivityDateStart),10) = left(max(b.ActivityDateEnd),10), left(min(b.ActivityDateStart),10), concat(left(min(b.ActivityDateStart),10) , ' - ' , left(max(b.ActivityDateEnd),10))) as dateperiod,
			a.OLE_ProgramID,
			a.EnrolEventID
		FROM
			INTRANET_ENROL_EVENTINFO as a
			LEFT JOIN INTRANET_ENROL_EVENT_DATE as b on b.EnrolEventID = a.EnrolEventID And (b.RecordStatus IS NULL OR b.RecordStatus = 1)
			/*inner join INTRANET_ENROL_CATEGORY as c on (a.EventCategory = c.CategoryID)*/	
		WHERE
			(a.EnrolGroupID = '' Or a.EnrolGroupID Is Null Or a.EnrolGroupID = 0 $EnrolGroupID_conds)
			/*And (b.RecordStatus IS NULL OR b.RecordStatus = 1)*/ AND a.AcademicYearID='$AcademicYearID' $SelectTransferStatus
		GROUP BY
			a.EnrolEventID, a.EventTitle
	";
	
	$infoArr = $libenroll->returnArray($sql, 4);
	
	# Build GroupIDArr
	$EnrolGroupIDArr = array();
	$infoSize = count($infoArr);
	for ($i=0; $i<$infoSize; $i++)
	{
		$EnrolGroupIDArr[] = $infoArr[$i]['EnrolEventID'];
	}
	
	# Get number of members of each group
	$numOfMemberArr = $libenroll->GET_ARR_NUMBER_OF_MEMBER($EnrolGroupIDArr, "activity");
	
	# Build Associate Array
	// $numOfMemberMap[#GroupID] = #numOfMember
	$numOfMemberMap = array();
	$infoMember = count($numOfMemberArr);
	for ($i=0; $i<$infoMember; $i++)
	{
		$thisGroupID = $numOfMemberArr[$i]['EnrolEventID'];
		$thisNumOfMember = $numOfMemberArr[$i]['COUNT(*)'];
		$numOfMemberMap[$thisGroupID] = $thisNumOfMember;
	}
}

# Build Table
$display .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' bgcolor='#CCCCCC'>\n";

# Title Row
$display .= "<tr>\n";
	$display .= "<td class='tabletop tabletopnolink'>#</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>".$ec_iPortfolio['title']."</td>\n";
	if ($RecordType=="club")
	{
		$display .= "<td class='tabletop tabletopnolink'>".$Lang['General']['Term']."</td>\n";
	}
	$display .= "<td class='tabletop tabletopnolink'>".$eEnrollment['Date'] ." / " . $eEnrollment['Period']."</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>".$eEnrollment['No_of_Student']."</td>\n";
	$display .= "<td class='tabletop tabletopnolink'>". $eEnrollment['Already_Transfered'] ."</td>\n";
	$display .= "<td class='tabletop tabletopnolink' style='text-align:center;'>";
	$display .= "<input type='checkbox' onClick=\"(this.checked)?setChecked(1,this.form,'ID[]'):setChecked(0,this.form,'ID[]')\"><br>";
if ($sys_custom['eEnrolment']['Select50Button']){
	$display .= $select50;
}
	$display .= "</td>\n";
$display .= "</tr>\n";

# Contents
for ($i=0; $i<$infoSize; $i++)
{
	list($thisTitle, $thisDatePeriod, $thisOLE_ProgramID, $thisID, $Semester) = $infoArr[$i];
	$count = $i+1;
	$css = ($i % 2) ? "tablerow1" : "tablerow2";
	
	$display .= "<tr>\n";
		# count
		$display .= "<td class='$css tabletext'>$count</td>\n";
		# Title
		$display .= "<td class='$css tabletext'>$thisTitle</td>\n";
		# Semester
		if ($RecordType=="club")
		{
			if ($Semester == '' || $Semester == 0)
				$Semester = $i_ClubsEnrollment_WholeYear;
			else
			{
				$objTerm = new academic_year_term($Semester);
				$Semester = $objTerm->Get_Year_Term_Name();
			}
			$display .= "<td class='$css tabletext'>$Semester</td>\n";
		}
		# Date / Period
		$display .= "<td class='$css tabletext'>$thisDatePeriod</td>\n";
		# Number of Students
		if ($numOfMemberMap[$thisID] > 0)
		{
			# add link to the number if members > 0
			$thisDisplay = "<a href='javascript:void(0)' class='tablelink' onClick='javascript:newWindow(\"student_list_popup.php?RecordType=$RecordType&FromStep=1&ParID=$thisID&AcademicYearID=$AcademicYearID\", 10)'>";
			$thisDisplay .= $numOfMemberMap[$thisID];
			$thisDisplay .= "</a>";
		}
		else
		{
			$thisDisplay = 0;
		}
		$display .= "<td class='$css tabletext'>".$thisDisplay."</td>\n";		
		# Already Transfer
		$display .= "<td class='$css tabletext'>";
		$display .= ($thisOLE_ProgramID != NULL)? $i_general_yes : $i_general_no;
		$display .= "</td>\n";
		# Checkbox
		$display .= "<td class='$css tabletext' style='text-align:center;'>";
		if ( $numOfMemberMap[$thisID] == NULL || $numOfMemberMap[$thisID] == 0)
		{
			# disable checkbox if no student
			$display .= "<input type='checkbox' DISABLED>";
		}
		else
		{
			$display .= "<input type='checkbox' class='itemChk' name=ID[] value='$thisID'>";
		}
		$display .= "</td>\n";
	$display .= "</tr>\n";
}

$display .= "</table>\n";

?>
<script language="javascript">
<!--
function ClickTransfer()
{
	var obj = document.form1;
	var element = "ID[]";
	
	if(countChecked(obj,element)>0) 
	{
 		obj.action='step2.php';
 		obj.submit();
    } else {
		alert(globalAlertMsg2);
    }
}

function js_Changed_Academic_Year_Selection(jsAcademciYearID)
{
	var obj = document.form1;
 	obj.submit();
}

function selectNext50() {
	var counter = 0;
	$('input.itemChk:not(:checked)').each(function() {
		$(this).attr('checked', 'checked');
		counter++;
		
		if (counter >= 50) {
			return false;	// break
		}
	});
}

//-->
</script>

<br />
<form name="form1" method="post" action="step1.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr>
    <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    	<td align="right" valign="bottom" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                    </tr>

                    <tr>
                    	<td align="left" valign="bottom"><?=$AcademicYearSelection?>&nbsp;<?=$TypeSelect?>&nbsp;<?=$TransferStatusSelect?></td>
                    	<td align="right" valign="bottom" height="28"><?=$transferBtn?></td>
                    </tr>
                    <tr>
                    	<td align="center" colspan='2'>
                    		<?=$display?>
                    	</td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
    </td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="order" value="<?=$li->order?>" />

</form>
</br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>