<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();	
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html("popup.html");


# Check access right
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');


//$AcademicYearID = $_POST['AcademicYearID'];


# Student List
$condsActive = '';
if ($FromStep == 3 && $studentSetting == "activeOnly")
	$condsActive = " AND a.isActiveMember = 1 ";

$FormIDList = $_REQUEST['FormIDList'];
$IntExt = $_REQUEST['IntExt'];

$condsFormID = '';
if ($FormIDList != '')
	$condsFormID = " And yc.YearID In ($FormIDList) ";
	

$isInt = false;
if ($IntExt == $enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['IntExt']['Internal']) {
	$isInt = true;
}


if ($RecordType == "club")
{
	/*
	$sql = "SELECT
					IF(c.ClassNumber IS NULL OR c.ClassNumber = '', CONCAT(c.ClassName,' (-)'), CONCAT(c.ClassName,' (',c.ClassNumber,')')) as StuClass,
				    c.EnglishName, 
				    c.ChineseName,
				    b.Title as Role,
					a.CommentStudent,
					a.UserID,
					d.Title as title
			FROM 
					INTRANET_USERGROUP as a
					LEFT OUTER JOIN INTRANET_ROLE as b ON a.RoleID = b.RoleID
					LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
					LEFT OUTER JOIN INTRANET_GROUP as d ON a.GroupID = d.GroupID
					Left Outer Join YEAR_CLASS_USER as ycu On (c.UserID = ycu.UserID)
					Left Outer Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
			WHERE 
					a.EnrolGroupID = $ParID 
					AND
					c.RecordType = 2
					And
					yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					$condsActive
					$condsFormID
			ORDER BY
					c.ClassName, c.ClassNumber
		";
	*/
	//$stuResult = $libenroll->Get_Club_Member_Info($ParID, array(2), $AcademicYearID, $IndicatorArr=1);
	$stuResult = $libenroll->Get_Club_Member_Info($ParID, array(2), $AcademicYearID, $IndicatorArr=1, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=1,
													$YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='', $ActiveMemberOnly, $FormIDArr);
	$Title = $stuResult[0]['GroupTitle'];
	$TitleEn = $stuResult[0]['TitleEn'];
	$TitleCh = $stuResult[0]['TitleCh'];
}
else
{
	/*
	$sql = "SELECT
					IF(b.ClassNumber IS NULL OR b.ClassNumber = '', CONCAT(b.ClassName,' (-)'), CONCAT(b.ClassName,' (',b.ClassNumber,')')) as StuClass,
				    b.EnglishName, 
				    b.ChineseName,
				    a.RoleTitle as Role,
					a.CommentStudent,
					a.StudentID as UserID,
					c.EventTitle as title
			FROM 
					INTRANET_ENROL_EVENTSTUDENT as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN INTRANET_ENROL_EVENTINFO as c ON a.EnrolEventID = c.EnrolEventID
					Left Outer Join YEAR_CLASS_USER as ycu On (b.UserID = ycu.UserID)
					Left Outer Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
			WHERE 
					a.EnrolEventID = $ParID 
					AND
					a.RecordStatus = 2
					And
					yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					$condsActive
					$condsFormID
			ORDER BY
					b.ClassName, b.ClassNumber
		";
		*/
		//$StudentInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($ParID, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1);
		$StudentInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($ParID, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1,
																				$ActiveMemberOnly, $FormIDArr);
		$stuResult = $StudentInfoArr[$ParID]['StatusStudentArr'][2];
		$Title = $StudentInfoArr[$ParID]['EventTitle'];
		$TitleEn = $StudentInfoArr[$ParID]['TitleEn'];
		$TitleCh = $StudentInfoArr[$ParID]['TitleCh'];
}
//$stuResult = $libenroll->returnArray($sql,7);

if ($FromStep == 3)
{
	# calculate OLE hours of each student
	# settings
	$ParData['enrolmentID'] = $ParID;
	$ParData['studentSetting'] = ${"studentSetting_".$ParID};
	$ParData['zeroHour'] = ${"zeroHour_".$ParID};	
	$ParData['hourSetting'] = ${"hours_setting_".$ParID};
	$ParData['leastAttendance'] = ${"leastAttendance_".$ParID};
	$ParData['manualHours'] = ${"manualHours_".$ParID};
	$OLE_HoursArr = $libenroll->GET_STUDENT_OLE_HOURS($ParData, $RecordType, $returnAssociativeArr=1, '', $AcademicYearID);
		
	$organization = stripslashes(stripslashes(${"organization_".$ParID}));
	$details = stripslashes(stripslashes(${"details_".$ParID}));
	$SchoolRemarks = stripslashes(stripslashes(${"SchoolRemarks_".$ParID}));
	
	$titleLang = ${"titleLang_".$ParID};
	$category = ${"category_".$ParID};
	$ele = ${"ele_".$ParID};
	$hours_setting = ${"hours_setting_".$ParID};
	$zeroHour = ${"zeroHour_".$ParID};
	$studentSetting = ${"studentSetting_".$ParID};
	$leastAttendance = ${"leastAttendance_".$ParID};
	$manualHours = ${"manualHours_".$ParID};
	
	$Title = ($titleLang==$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['English'])? $TitleEn : $TitleCh;
}


# construct table
$studentTable = "";

# table title
$studentTable .= "<table class=\"tablebluetop\" align=\"center\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\">";
$studentTable .= "<tr>";
$studentTable .= "<td width=1 class='tablebluetop tabletopnolink'><span class='tabletoplink'>#</span></td>\n";
$studentTable .= "<td class='tablebluetop tabletopnolink'>". $i_ClassName ."</td>\n";
$studentTable .= "<td class='tablebluetop tabletopnolink'>". $i_ClassNumber ."</td>\n";
$studentTable .= "<td class='tablebluetop tabletopnolink'>". $i_UserStudentName ."</td>\n";
$studentTable .= "<td class='tablebluetop tabletopnolink'>". $ec_iPortfolio['ole_role'] ."</td>\n";
$studentTable .= "<td class='tablebluetop tabletopnolink'>".$Lang['eEnrolment']['Achievement'] ."</td>\n";


if ($FromStep == 3 && $isInt) {
	$studentTable .= "<td class='tablebluetop tabletopnolink'>".$ec_iPortfolio['hours'] ."</td>\n";
}
$studentTable .= "<td class='tablebluetop tabletopnolink'>".$ec_iPortfolio['comment'] ."</td>\n";
$studentTable .= "</tr>";



# table contents
$counter = 0;
for ($i=0; $i<sizeof($stuResult); $i++)
{
	$studentID = $stuResult[$i]['UserID'];
	
	# skip zero record if the setting is enabled
	if ($zeroHour == "notTransfer" && $OLE_HoursArr[$studentID] == 0) continue;
	
	$tr_css = ($counter % 2 == 1) ? "tablebluerow2" : "tablebluerow1";	
	$row = $counter+1;	
		 	
	$studentTable .= "<tr class=\"".$tr_css."\">";
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".$row."</td>";
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".$stuResult[$i]['ClassName']."</td>";
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".$stuResult[$i]['ClassNumber']."</td>";
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".$stuResult[$i]['StudentName']."</td>";
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".$stuResult[$i]['RoleTitle']."</td>";
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".nl2br($stuResult[$i]['Achievement'])."</td>";
	if ($FromStep == 3 && $isInt) {
		$studentTable .= "<td class=\"tabletext\" valign=\"top\">".$OLE_HoursArr[$studentID]."</td>";
	}
	$studentTable .= "<td class=\"tabletext\" valign=\"top\">".nl2br($stuResult[$i]['Comment'])."</td>";
	$studentTable .= "</tr>";
	
	$counter++;
}

$studentTable .= "</table>";


$MODULE_OBJ['title'] = $eEnrollment['app_stu_list']; 
$linterface->LAYOUT_START();

$LibPortfolio = new libpf_slp();
$Cats = $LibPortfolio->GET_OLR_Category();	

# ELE display
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	$ELEArr = $ele;
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
	$ele_display = "--";
	
# hour setting display
$hourSettingDisplay = "";
if ($hours_setting == "studentHours")
{
	$hourSettingDisplay = $eEnrollment['total_hours_of_student'];
}
else if ($hours_setting == "abovePercentage")
{
	$hourSettingDisplay = $eEnrollment['total_hours_if_above_percentage'].$leastAttendance."%";
}
else if ($hours_setting == "manualInput")
{
	$hourSettingDisplay = $eEnrollment['manual_input_hours']." ".$manualHours." ".$eEnrollment['unit_hour'];
}

# zero hour display
$zeroHourDisplay = "";
if ($zeroHour == "transfer")
{
	$zeroHourDisplay = $eEnrollment['Transfer'];	
}
else
{
	$zeroHourDisplay = $eEnrollment['Not_Transfer'];	
}

# student setting
$studentSettingDisplay = "";
if ($studentSetting == "allMember")
{
	$studentSettingDisplay = $eEnrollment['all_member'];	
}
else
{
	$studentSettingDisplay = $eEnrollment['active_member_only'];	
}


$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks();
?>

<br />
	<table align="center" width="96%" border="0" cellpadding="4" cellspacing="0" style="border:dashed 1px #666666">
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['title']; ?></span></td>
				<td width="80%" valign="top"><?=$Title?></td>
			</tr>
			
			<? if ($FromStep == 3) { ?>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['category']; ?></span></td>
					<td><?=$Cats[$category]?></td>
				</tr>
				<? if ($isInt) { ?>
					<tr valign="top">
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['ele']; ?></span></td>
						<td><?=$ele_display?></td>
					</tr>	
				<? } ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['organization']?></span></td>
					<td><?=($organization==""?"--":nl2br($organization))?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['details']?></span></td>
					<td><?=($details==""?"--":nl2br($details))?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remark']?></span></td>
					<td><?=($SchoolRemarks==""?"--":nl2br($SchoolRemarks))?></td>
				</tr>
				<? if ($isInt) { ?>
					<tr valign="top">
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eEnrollment['hours_setting']?></span></td>
						<td><?=$hourSettingDisplay?></td>
					</tr>
				<? } ?>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eEnrollment['transfer_if_zero_hour']?></span></td>
					<td><?=$zeroHourDisplay?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eEnrollment['transfer_student_setting']?></span></td>
					<td><?=$studentSettingDisplay?></td>
				</tr>
			<? } ?>
			</table>
			</td>
		</tr>
		</table>
		
<br />	

<table align="center" width="96%" border="0" cellpadding="0" cellspacing="0">
	<tr><td><?=$studentTable?></td></tr>
	<tr><td><?=$RemarksTable?></td></tr>
</table>
<br />

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?>
</div>
</td></tr>
</table>

<?		
$linterface->LAYOUT_STOP();
intranet_closedb();
?>