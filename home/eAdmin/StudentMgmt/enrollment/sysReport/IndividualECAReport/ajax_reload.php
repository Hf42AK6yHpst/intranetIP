<?php 


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$libclass = new libclass();


$RecordType = $_POST['RecordType'];

if ($RecordType == 'reportResult') {

	$studentIdAry = $_POST['studentIdAry'];
	$classIdAry = $_POST['classIdAry'];
	$selectSemesterAry = $_POST['Semester'];
	$StudentType = $_POST['StudentType'];
	
	
	if(isset($selectSemesterAry))
	{
		$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $selectSemesterAry);
		
		$term_group_list = array();
		if(!empty($term_group_list_temp))
		{
			foreach($term_group_list_temp as $k=>$d)
				$term_group_list[] = $d['EnrolGroupID'];
		}
	}
	
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$fcm = new form_class_manage();
	$FormList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);	
	$FormListAry = BuildMultiKeyAssoc($FormList,'YearClassID');	
	
	$ClassSize = sizeof($classIdAry);
	for($k=0;$k<$ClassSize;$k++){
		$YearClassID = $classIdAry[$k];
		$WebSAMSCode = $FormListAry[$YearClassID]['Form_WebSAMSCode'];
		$FormNumber = preg_replace('/\D/s', '', $WebSAMSCode);
		
		
		$SuggestUpper = $libenroll->GetFormExceedPoint($AcademicYearID,$WebSAMSCode);
		$studentInfoAry = $libclass->getStudentByClassId($YearClassID);
		$thisClassStudentIDAry = Get_Array_By_Key($studentInfoAry,'UserID');

// 		$SuggestUpperAry[1] = 24;
// 		$SuggestUpperAry[2] = 24;
// 		$SuggestUpperAry[3] = 30;
// 		$SuggestUpperAry[4] = 36;
// 		$SuggestUpperAry[5] = 36;
// 		$SuggestUpperAry[6] = 20;
// 		$SuggestUpper = $SuggestUpperAry[$FormNumber];
		$SuggestUpper = $libenroll->GetFormExceedPoint($AcademicYearID,$WebSAMSCode);

		
		$studentClubArr= $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIdAry,$targetCategory);
		
		$studentInfoArr = array();
	
		
		foreach ($studentClubArr as $key => $clubDataArr)
		{
			
			list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
			$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
			//	$thisTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
			$roleTitle = ($roleTitle!="") ? $roleTitle : "--";
			
			//						if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
			if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
			{
				continue;
			}
			
			if(in_array($thisUserID,$thisClassStudentIDAry)){
			    if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
			    {
			        $studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
			    }
			    $rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($thisEnrolGroupID,$roleTitle);
			    $Point = "";
			    if($rolePointArr[0]['Point'] != null){
			        $Point = $rolePointArr[0]['Point'];
			    }			    
			    $studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle,$Point);
	    
			}
		}
	
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$AcademicYearObj = new academic_year($AcademicYearID);
		$AcademicYearName = $AcademicYearObj->Get_Academic_Year_Name();
		# Display Result
		$display = '';
		for($i=0;$i<sizeof($studentIdAry);$i++){
		    $thisStudentID = $studentIdAry[$i];    		    
		    if (in_array($thisStudentID,$thisClassStudentIDAry))
    		  {  			
    			$libuser =new libuser($thisStudentID);
    			$StudentEnglishName = $libuser->EnglishName;
    			$StudentChineseName = $libuser->ChineseName;
    			$ClassName = $libuser->ClassName;
    			$ClassNumber = $libuser->ClassNumber;
    			
    		
    			$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
    			
    			
    			$sizeClubArr = sizeof($thisClubArr);
    			if($sizeClubArr > 0 ){
    			    
    			
        			$thisClubLink = '';
        			$RoleTitle = '';
        			$RolePoint='';
        			$PointTotal = '';
        			for ($j=0; $j<$sizeClubArr; $j++)
        			{
        				list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
        				
        				$thisClubLink .= $thisTitle."\n";
        				
        				if ($j < $sizeClubArr-1 )
        				{
        					$thisClubLink .= "<br />\n";
        				}
        				$RoleTitle .=$thisRoleTitle."\n";
        				if ($j < $sizeClubArr-1 )
        				{
        					$RoleTitle.= "<br />\n";
        				}
        				$RolePoint .=$thisRolePoint;
        				if ($j < $sizeClubArr-1 )
        				{
        					$RolePoint.= "<br />\n";
        				}
        				$PointTotal += $thisRolePoint;
        	
        			}
        			
        			
        			if($StudentType=='UpperLimit' && $PointTotal <= $SuggestUpper){
        			//    $display .="<tr><td align='center' colspan='3' class='tabletext tablerow2' ><br>$i_no_record_exists_msg<br><br></td></tr>\n";
        			}else{
        			    
        			
            			##### Header
            			$display .="<br>";
            			$display .= "<center>";
            			$display .= "<table width='1000px'  border:0px; margin: 0 auto; font-size: 22px; '>";
            				# Table Name
            				$display .= "<tr>";
            					$display .= "<td colspan='2' style='line-height:30px;' align='center'>";
            						$display .= GET_SCHOOL_NAME()." Individual Student ECA Record ". $AcademicYearName."<br/>";
            					$display .= "</td>";
            				$display .= "</tr>";
            				# Student Information
            				$display .= "<tr>";
            					$display .= "<td colspan='2' align='left' style='line-height:25px;float: left; padding-right: 30px;'>";
            						$display .= "<span style='display: table-cell; width:800px; '>".$ClassName.$ClassNumber." ".$StudentEnglishName." ".$StudentChineseName."</span>";
            					$display .= "</td>";
            				$display .= "</tr>";
            			
            			#### Header End ########
            				##### Content #####
            				$display .= "<tr>";
            					$display .= "<td align='center'>";
            						$display .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
            							$display .= "<thead><tr>";
            								$display .= "
            										<th width='20%'>".$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit']."</th>
            										<th width='20%'>".$Lang['eEnrolment']['SysRport']['SchoolECA']['Role']."</th>
            										<th width='20%'>".$Lang['eEnrolment']['UnitRolePoint']['Point']."</th>";
            								$display .= "</tr></thead>";
            								

//             								else{   									
            									###Club Role Point
            									$display .= '<tr>';
            										$display .= '<td class="tabletext" valign="top">'.$thisClubLink.'</td>
            													<td class="tabletext"  valign="top">'.$RoleTitle.'</td>
            													<td class="tabletext"  valign="top">'.$RolePoint.'</td>
            													';
            									
            									$display .= '</tr>';
            									### Points Total
            									$display .='<tr>';
            										$display .= '<td class="tabletext" valign="top"></td>
            													<td class="tabletext"  valign="right">'.$Lang['eEnrolment']['SysRport']['IndividualECA']['TotalPoint'].'</td>
            													<td class="tabletext"  valign="top">'.$PointTotal.'</td>';
            									
            									$display .= '</tr>';
            									###Suggest upper limit
            									$display .='<tr>';
            										$display .= '<td class="tabletext" valign="top"></td>
            														<td class="tabletext"  valign="right">'.$Lang['eEnrolment']['SysRport']['IndividualECA']['SuggestPoint'].'</td>
            														<td class="tabletext"  valign="top">'.$SuggestUpper.'</td>';
            									
            									$display .= '</tr>';
//             								}
            						$display .= '</table>';
            					$display .= "</td>";
            				$display .= "</tr>";
            			$display .= '</table></center>';	
        			}
        		}
    		  }
		}
		echo $display;
	}
	
}


?>