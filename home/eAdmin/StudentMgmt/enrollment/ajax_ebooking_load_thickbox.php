<?php 
/*
 * Using:
 * 
 * Change Log:
 *
 * Date:    2019-02-22  Cameron
 *          - pass $meetingDateType to GetEnrolEbookingRelation()
 * Date:    2019-01-14  Cameron
 *          - show booking datetime column in red vs activity column if they are inconsistent
 * Date:    2019-01-11  Cameron
 *          - add column title name for status
 * Date:	2017-06-19	Villa
 * 			-update the available room checking with the change of api returned array
 * Date:	2017-05-11	Villa #K108095 
 * 			-update UI
 * Date:	2017-05-04	Villa
 * 			-Open the File
 */
##Modify Form Serlize to Arr
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");

intranet_auth();
intranet_opendb();

$libebooking_api = new libebooking_api();
$linterface = new interface_html();
$libenroll = new libclubsenrol($AcademicYearID);

$recordType = $_POST['recordType'];
$recordId = IntegerSafe($_POST['recordId']);
$meetingDateType = $recordType == $enrolConfigAry['Club'] ? ENROL_TYPE_CLUB : ENROL_TYPE_ACTIVITY;

$checkMismatch = $libenroll->checkeEnrolmenteBookingTimeMismatch($recordType, $recordId, $dateInfoAry);
if ($checkMismatch['success']) {
    $tableWidth = '50%';
    $dateTimeColWidth= '35%';
    $colWidth['location'] = '21.4%';
    $colWidth['status'] = '21.2%';
    $colWidth['remove'] = '22.0%';
    $bookingTimeTitle = '';
    $totalCol = 4;
}
else {
    $tableWidth = '85%';
    $dateTimeColWidth= '20%';
    $colWidth['location'] = '19.2%';
    $colWidth['status'] = '19.0%';
    $colWidth['remove'] = '19.4%';
    $bookingTimeTitle= '<th style="width:'.$dateTimeColWidth.';">'.$Lang['eEnrolment']['eBooking']['BookingTime'].'</th>';
    $totalCol = 5;
}

##Data Setting Related Start
foreach ((array)$dateInfoAry as $key=>$_dateInfoAry){
	$BookingDetail[$key]['Date'] = $_dateInfoAry['dateText'];
	$BookingDetail[$key]['StartTime'] = str_pad($_dateInfoAry['hourStart'], 2, "0", STR_PAD_LEFT).':'.str_pad($_dateInfoAry['minStart'], 2, "0", STR_PAD_LEFT).':00';
	$BookingDetail[$key]['EndTime'] =  str_pad($_dateInfoAry['hourEnd'], 2, "0", STR_PAD_LEFT).':'.str_pad($_dateInfoAry['minEnd'], 2, "0", STR_PAD_LEFT).':00';
	$BookingDetail[$key]['ItemID'] = '';
	
	$BookingID = $_dateInfoAry['BookingID'];
	if($_dateInfoAry['dateRecordId'] && !$BookingID){
	    $BookingID = $libenroll->GetEnrolEbookingRelation($_dateInfoAry['dateRecordId'], $meetingDateType);
		$BookingID = $BookingID['BookingRecordID'];
	}
	if($BookingID){
		$BookingDetail[$key]['RoomID'] = $libebooking_api->ReturnLocationIdFromBookingID($BookingID);
	}
}
$AllRoom = $libebooking_api->ReturnAllLocation();
foreach ((array)$BookingDetail as $key=>$_BookingDetail){
	
	$check=$libebooking_api->GetAvailableRoom($_BookingDetail);
	
	foreach ((array)$AllRoom as $RoomID=>$_AllRoom){
		if($check[$_BookingDetail['Date']][$RoomID]['BookAvailable']){
			$temptemp[$_AllRoom['BuildingID']]['Level'][$_AllRoom['LocationLevelID']]['Room'][$RoomID] = $_AllRoom;
// 			$temptemp[$_AllRoom['BuildingID']]['isPrint'] = true;
			$temptemp[$_AllRoom['BuildingID']]['BuildingName'] = $_AllRoom['BuildingNameChi'];
// 			$temptemp[$_AllRoom['BuildingID']][$_AllRoom['LocationLevelID']]['isPrint'] = true;
			$temptemp[$_AllRoom['BuildingID']]['Level'][$_AllRoom['LocationLevelID']]['LevelName'] = $_AllRoom['LevelNameChi'];
		}
	}
	if($_BookingDetail['RoomID']==''){
		if($temptemp){
		$SelectionBox[$key] = '<select name="BookingDetail['.$key.'][RoomID]" style="width:30%;">';
			
				$SelectionBox[$key] .= '<option value="">'.$Lang['General']['PleaseSelect'].'</option>';
			foreach ((array)$temptemp as $BuildingID => $_Building){
				$SelectionBox[$key] .= '<optgroup label="'.$_Building['BuildingName'].'">';
				foreach((array)$_Building['Level']as $LevelID => $_Level){
					$SelectionBox[$key] .= '<optgroup label="'.'&nbsp;&nbsp;'.$_Level['LevelName'].'">';
					foreach ((array)$_Level['Room']as $_RoomID => $_Room){
						$SelectionBox[$key] .= '<option value="'.$_RoomID.'">';
						$SelectionBox[$key] .= $_Room['NameChi'];
						$SelectionBox[$key] .= '</option>';
					}
					$SelectionBox[$key] .= '</optgroup>';
				}
				$SelectionBox[$key] .= '</optgroup>';
			}
			$SelectionBox[$key] .= '</select>';
		}else{
			$SelectionBox[$key]  = $Lang['eEnrolment']['eBooking']['NoAvailableRoom'];
		}
	}else{
		$SelectionBox[$key] = '<table style="width:100%;">';
			$SelectionBox[$key] .= '<tr>';
				$SelectionBox[$key] .= '<td style="width:33%;">'.Get_String_Display(Get_Lang_Selection($_BookingDetail['RoomID']['NameChi'], $_BookingDetail['RoomID']['NameEng'])).'</td>';
				$SelectionBox[$key] .= '<td style="width:33%;">'.GetBooingStatusDisplay($_BookingDetail['RoomID']['BookingStatus']).'</td>';
				$SelectionBox[$key] .= '<td style="width:33%;">'.'<a href="javascript: void(0);" OnClick="DeleteBookingRecord('.$_BookingDetail['RoomID']['BookingID'].','.$key.');">'.'x'.'</a>'.'</td>';
			$SelectionBox[$key] .= '</tr>';
		$SelectionBox[$key] .= '</table>';
	}
	unset($temptemp);
}
###Print Table Start
$x = '<form id="eBookingForm" name="eBookingForm">';
	$x .= '<table style="width:'.$tableWidth.'" class="common_table_list_v30">';
		$x .= '<thead>';
		$x .= '<th style="width:'.$dateTimeColWidth.';">'.$Lang['eEnrolment']['eBooking']['Time'] .'</th>';
		$x .= $bookingTimeTitle;
//		$x .= '<th>'.$Lang['eEnrolment']['eBooking']['Location'] .'</th>';
		$x .= '<th style="width:'.$colWidth['location'].';">'.$Lang['eEnrolment']['eBooking']['Location'] .'</th>';
		$x .= '<th style="width:'.$colWidth['status'].';">'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'].'</th>';
		$x .= '<th style="width:'.$colWidth['remove'].';">&nbsp;</th>';
		$x .= '</thead>';
		$x .= '<tbody>';
		if($BookingDetail){
			foreach ((array)$BookingDetail as $key=>$_BookingDetail){
				$x .= '<tr>';
					$x .= '<td style="white-space:nowrap;">'.$_BookingDetail['Date'].'<br>'.$_BookingDetail['StartTime'].'-'.$_BookingDetail['EndTime'].'</td>';
					if (!$checkMismatch['success']) {
					    $bookedInfo = $checkMismatch['error'][$key];
					    if ($bookedInfo['Date']) {
					        if ($bookedInfo['Date'] == $_BookingDetail['Date'] && $bookedInfo['StartTime'] == $_BookingDetail['StartTime'] && $bookedInfo['EndTime'] == $_BookingDetail['EndTime']) {
					            $color = '';
					        }
					        else {
					            $color = ' color:red;';
					        }
					        $x .= '<td style="white-space:nowrap;'.$color.'">'.$bookedInfo['Date'].'<br>'.$bookedInfo['StartTime'].'-'.$bookedInfo['EndTime'].'</td>';
					    }
					    else {
					        $x .= '<td style="white-space:nowrap;">-</td>';
					    }
					}
					$x .= '<td colspan="3">'.$SelectionBox[$key].'</td>';
					
					$x .= '<input type="hidden" name="BookingDetail['.$key.'][Date]" value="'.$_BookingDetail['Date'].'">';
					$x .= '<input type="hidden" name="BookingDetail['.$key.'][StartTime]" value="'.$_BookingDetail['StartTime'].'">';
					$x .= '<input type="hidden" name="BookingDetail['.$key.'][EndTime]" value="'.$_BookingDetail['EndTime'].'">';
				$x .= '</tr>';
			}
		}else{
			$x .= '<tr>';
				$x .= '<td colspan="'.$totalCol.'" style="text-align: center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
			$x .= '</tr>';
		}
		$x .= '</tbody>';
	$x .= '</table>';
$x .= '</form>';
$x .= '<div align="center">';
$x .= $linterface->GET_ACTION_BTN($button_save, "button", "RequestBooking('copyback');", "submit2");
$x .= '&nbsp;';
$x .= $linterface->GET_ACTION_BTN($button_close, "button", "self.parent.tb_remove();", "cancelbtn");
$x .= '</div>';
###Print Table End
function GetBooingStatusDisplay($bookingStatus){
	global $Lang, $sys_custom, $image_path, $LAYOUT_SKIN;
	switch($bookingStatus) {
		case 0:
			$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval']."'>";
			
			break;
		case 1:
			$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."'>".$infoAry[$id][$bookingPicFieldName]."<br><span class='tabletextremark'>".$infoAry[$id][$processDayBeforeFieldName]."</span>";
			
			break;
		case -1:
			$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."'>".$infoAry[$id][$bookingPicFieldName]."<br><span class='tabletextremark'>".$infoAry[$id][$processDayBeforeFieldName]."</span>";
			
			break;
		case 999:
			$img_status = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title='".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory']."'>";
			break;
	}
	return $img_status;
}

##JavaScript
echo $x;
intranet_closedb();
// return $data;
?>