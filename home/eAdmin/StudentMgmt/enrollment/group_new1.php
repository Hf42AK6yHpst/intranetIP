<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageSysSettingGroSetting";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['add_act_event'], "", 1);

        $STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_1'], 1);
		$STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_2'], 0);
        
		$tiebreakerSelection = "<SELECT name=tiebreak>\n";
		$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
		$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
		$tiebreakerSelection.= "</SELECT>\n";
		
        $linterface->LAYOUT_START();

        
$EnrollGroupArr = $lc->GET_GROUPINFO($GroupID);
//debug_r($EnrollGroupArr);
# Last selection - PIC
###############################################################################################
$picArr = $lc->GET_GROUPSTAFF($EnrollGroupArr[0], "PIC");
$PICSel = "<select name='pic[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($picArr); $i++) {
	/*
	$username_field = getNameFieldByLang("a.");
	$Sql = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a WHERE a.UserID = '".$picArr[$i][0]."'";
	$TempArr = $lc->returnArray($Sql);
	debug_r($TempArr);
	*/
	$PICSel .= "<option value=\"".$picArr[$i][0]."\">".$LibUser->getNameWithClassNumber($picArr[$i][0])."</option>";
}
$PICSel .= "</select>";

//if (sizeof($picArr)>0)
//{
	$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");
//}
###############################################################################################


# Last selection - helper
###############################################################################################
$helperArr = $lc->GET_GROUPSTAFF($EnrollGroupArr[0], "HELPER");
$HELPERSel = "<select name='helper[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($helperArr); $i++) {
	/*
	$username_field = getNameFieldByLang("a.");
	$Sql = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a WHERE a.UserID = '".$picArr[$i][0]."'";
	$TempArr = $lc->returnArray($Sql);
	debug_r($TempArr);
	*/
	$HELPERSel .= "<option value=\"".$helperArr[$i][0]."\">".$LibUser->getNameWithClassNumber($helperArr[$i][0])."</option>";
}
$HELPERSel .= "</select>";

//if (sizeof($helperArr)>0)
//{
	$button_remove_html_helper = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['helper[]'])");
//}
###############################################################################################

$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$GroupArr = $lc->GET_GROUPCLASSLEVEL($EnrollGroupArr[0]);
$ClassLvlChk = "<input type=\"checkbox\" id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]');\"><label for=\"classlvlall\">".$eEnrollment['all']."</label>";
$ClassLvlChk .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = "";
	for ($j = 0; $j < sizeof($GroupArr); $j++) {	
		if (($ClassLvlArr[$i][0] == $GroupArr[$j][0])&&(in_array($GroupArr[$j], $ClassLvlArr))) {	
			 $checked = " checked ";
			 break;
		}
	}
	
	if (($i % 10) == 0) $ClassLvlChk .= "<tr>";
	$ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$i}\" name=\"classlvl[]\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label></td>";
	if (($i % 10) == 9) $ClassLvlChk .= "</tr>";
}
$ClassLvlChk .= "</table>";

if ($EnrollGroupArr[8] != 0) $LowerAge = $EnrollGroupArr[8];
$LowerAgeSel = "<select name=\"LowerAge\" id=\"LowerAge\">";
for ($i = 11; $i < 21; $i++) {
	$LowerAgeSel .= "<option value=\"$i\"";
	if ($LowerAge == $i) $LowerAgeSel .= " selected ";
	$LowerAgeSel .= ">".$i."</option>";
}
$LowerAgeSel .= "</select>";

if ($EnrollGroupArr[7] != 0) $UpperAge = $EnrollGroupArr[7];
$UpperAgeSel = "<select name=\"UpperAge\" id=\"UpperAge\">";
for ($i = 11; $i < 21; $i++) {
	$UpperAgeSel .= "<option value=\"$i\"";
	if (($UpperAge == $i)||(($i == 20)&&(!$UpperAgeSelected))) {
		$UpperAgeSelected = true;
		$UpperAgeSel .= " selected ";
	}
	$UpperAgeSel .= ">".$i."</option>";
}
$UpperAgeSel .= "</select>";


$filecount = 0;
for ($i = 10; $i < 15; $i++) {
	if ($EnrollGroupArr[$i] != "") $filecount++;
}

?>
<script language="javascript">
function generalFormSubmitCheck(obj)
{	
	checkOptionAll(obj.elements["pic[]"]);
	checkOptionAll(obj.elements["helper[]"]);
}

function FormSubmitCheck(obj)
{
	if(!check_text(obj.Discription, "<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_content']; ?>.")) return false;
	if (obj.sel_category.value == '') {
		alert("<?php echo $i_alert_pleaseselect.$eEnrollment['add_activity']['act_category']; ?>.");
		return false;
	}	
	if (document.getElementById('pic[]').length == 0) {
		alert('<?= $eEnrollment['js_sel_pic']?>');
		return false;
	}	
	if(!check_text(obj.PaymentAmount, "<?php echo $i_alert_pleasefillin.$eEnrollment['PaymentAmount']; ?>.")) return false;
	generalFormSubmitCheck(obj);
	obj.submit();
}
</SCRIPT>
<form name="form1" action="group_new1_update.php" method="POST" enctype="multipart/form-data">
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr><td class="tabletext" align="center">
<?= $eEnrollment['del_warning']?><br/><br/>
</td></tr>
<tr><td>
<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_content']?> <span class="tabletextrequire">*</span></td>
		<td><?= $linterface->GET_TEXTAREA("Discription", $EnrollGroupArr[4])?></td>
	</tr>
		<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_category']?> <span class="tabletextrequire">*</span></td>
		<td>
		<?= $lc->GET_CATEGORY_SEL($EnrollGroupArr[15], 1, "", $button_select)?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_pic']?> <span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $PICSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('choose/index_all.php?fieldname=pic[]&ppl_type=pic', 9)")?><br />
				&nbsp;<?=$button_remove_html?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_assistant']?></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $HELPERSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('choose/index_all.php?fieldname=helper[]&ppl_type=helper', 9)")?><br />
				&nbsp;<?=$button_remove_html_helper?>
				</td>
			</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_target']?> </td>
		<td class="tabletext"><?= $ClassLvlChk?>
			
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_age']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" id="set_age_limit" name="set_age" value="0" checked><label for="set_age_limit"> <?= $eEnrollment['no_limit']?></label><br/>
			<input type="radio" id="set_age_range" name="set_age" value="1" <? if (($EnrollGroupArr[7] > 0)||($EnrollGroupArr[8] > 0)) print "checked"; ?>>
			<label for="set_age_range">
			<?= $LowerAgeSel?>
			&nbsp;-&nbsp;
			<?= $UpperAgeSel?>&nbsp;<?= $eEnrollment['add_activity']['age']?>
			</label>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_gender']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" name="gender" id="gender_all" value="A" checked> <label for="gender_all"><?= $eEnrollment['all']?></label>
			<input type="radio" name="gender" id="gender_m" value="M" <? if ($EnrollGroupArr[9] == "M") print "checked"?>> <label for="gender_m"><?= $eEnrollment['male']?></label>
			<input type="radio" name="gender" id="gender_f" value="F" <? if ($EnrollGroupArr[9] == "F") print "checked"?>> <label for="gender_f"><?= $eEnrollment['female']?></label>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['PaymentAmount']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="PaymentAmount" name="PaymentAmount" value="<?= (0+$EnrollGroupArr[16])?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['attachment']?></td>
		<td class="tabletext">
		<? if ($EnrollGroupArr[10] == "")  { ?>
			<input type="file" id="attach[]" name="attach[]" class="textboxtext">
		<? } else {?>
			<a href="<?= "/file/".$EnrollGroupArr[10]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[10])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='1'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
		<? } ?>
			<? if ($filecount == 0) { ?> <a href="#more_file_a" onClick="javascript:document.getElementById('more_file').style.display = 'block';document.getElementById('tohide').style.display = 'none'"><span id="tohide"><?= $eEnrollment['add_activity']['more_attachment']?></span></a> <? } ?>
			<div id="more_file" style="display : <? ($filecount > 0) ? print "block": print "none"; ?>"><a name="more_file_a"></a>
			<? if ($EnrollGroupArr[11] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr[11]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[11])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='2'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr[12] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr[12]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[12])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='3'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr[13] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr[13]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[13])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='4'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr[14] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr[14]?>" target="_blank" class="tablelink"><?= basename($EnrollGroupArr[14])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='5'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			</div>
		</td>
	</tr>


</table>

</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_save_continue, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_next, "button", "self.location='group_new2.php?GroupID=$GroupID'")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='group.php'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="GroupID" id="GroupID" value="<?= $GroupID?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />

</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>