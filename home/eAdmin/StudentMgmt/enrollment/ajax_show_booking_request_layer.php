<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();

$content .= "<form>\n";
	$content .= "<center>\n";
	
	$content .= "<div class='edit_pop_board' style='height:370px'>\n";
		$content .= "<br>\n";
		$content .= "<table width='100%'>\n";
		
			$content .= "<tr><td>\n";
			$content .= $lebooking_ui->Get_Booking_Location_Selection_iCal('FacilityID', $FacilityID, ' onchange="ShowBookingDetails(this.value);" ', $UserID)."\n";
			$content .= "</td></tr>\n";
			
			$content .= "<tr><td>\n";
			$content .= "<div id='div_booking_details' style='display:none;'>\n";
			$content .= "</div>";
			$content .= "</td></tr>\n";
			
			## Hidden Input Fields
			$content .= "<tr><td>\n";
			$content .= "<input type='hidden' name='hiddenFacilityID' id='hiddenFacilityID' value=''>\n";
			$content .= "</td></tr>\n";
		
		$content .= "</table>\n";
	$content .= "</div>\n";
	
	$content .= "</center>\n";
$content .= "</form>\n";

echo $content;

intranet_closedb();
?>