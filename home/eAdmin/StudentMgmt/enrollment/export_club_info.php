<?php
# using: 
/**********************************
 * 
 * Date: 2020-05-20 Tommy
 *      added columns for $sys_custom['eEnrollment']['exportExtraInfor']
 * 
 * Date: 2018-06-28 Anna
 *      Added $sys_custom['eEnrolment']['exportOLEPartnerOrganizations']
 * 
 * ********************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


if ($plugin['eEnrollment']) {

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	
	$libenroll = new libclubsenrol();
		
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    if (!isset($_GET)) {
			header("Location: index.php");
			exit();
		}

		$ExportArr = array();
		$lexport = new libexporttext();
		$luser = new libuser();
		
		 $Semester = $_GET['Semester'];
		 $sel_Category=$_GET['Category'];
		 $keyword = $_GET['Keyword'];
		
		//debug_pr($_GET);
		# EnrolGroupIDArr
		//$ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info("", $AcademicYearID);
		$ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info("", $AcademicYearID,"",$Semester,$keyword,0,1,1,true,$sel_Category);
		$EnrolGroupIDArr = array_keys($ClubInfoArr);

		# Target Form Arr
		$TargetFormArr = $libenroll->GET_GROUPCLASSLEVEL($EnrolGroupIDArr);
		$TargetFormAssoc = BuildMultiKeyAssoc($TargetFormArr, array("EnrolGroupID","ClassLevelID"),"LevelName",1);
		
		# Club Category Arr
		$CategoryArr = $libenroll->GET_CATEGORY_LIST();
	 	$CategoryAssoc = BuildMultiKeyAssoc($CategoryArr,"CategoryID");
	 	
	 	#PIC Name Arr
	 	$PICArr = $libenroll->GET_GROUPSTAFF($EnrolGroupIDArr,'PIC');
	 	$PICIDArr = Get_Array_By_Key($PICArr,"UserID");
	 	$PICAssoc = BuildMultiKeyAssoc($PICArr,"EnrolGroupID","UserID",1,1);
		$PICNameArr = $luser->getNameWithClassNumber($PICIDArr);
		
		#Meeting Date Arr
		$MeetingDate = $libenroll->Get_Club_Meeting_Date();
		$MeetingDateAssoc = BuildMultiKeyAssoc($MeetingDate,"EnrolGroupID","",0,1);
		
		# Column Title
		$exportColumn = array("Club Code","Club Name (English)","Club Name (Chinese)","Content","Category","Term","Target Form","Target Age Group"," Target Gender","Tentative Fee","Member Quota", "Minimum Member Quota","In Charge","Schedule");
		if($sys_custom['eEnrollment']['exportExtraInfor']){
		  $exportColumn[] = 'Record Type';
		  $exportColumn[] = 'Language';
		  $exportColumn[] = 'OLE Category';
		  $exportColumn[] = 'OLE Components';
		}
		if($sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
		    $exportColumn[] = "Partner Organizations";
		}
		
		if($sys_custom['eEnrollment']['exportExtraInfor']){
		    if(!$sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
		      $exportColumn[] = 'Partner Organizations';
		    }
		    $exportColumn[] = 'Details';
		    $exportColumn[] = 'School Remarks';
		}
		$exportColumn[] = 'WebSAMS STA Code';
		$exportColumn[] = 'WebSAMS STA Type';
		
		$ExportArr = array();
		foreach((array)$ClubInfoArr as $thisClubID => $thisClubInfo)
		{
			# Gender
			switch($thisClubInfo['Gender'])
			{
				case "M" : $Gender = $eEnrollment['male']; break;
				case "F" : $Gender = $eEnrollment['female']; break;
				case "A" : $Gender = $eEnrollment['all']; break;
			}
			
			# Date
			$DateDisplayArr = array();
			foreach((array)$MeetingDateAssoc[$thisClubID] as $DateArr)
			{
				$DateDisplayArr[] = $libenroll->Format_Meeting_Period($DateArr['ActivityDateStart'],$DateArr['ActivityDateEnd'],'ParStartDate ParStartTime - ParEndTime');
			}
			$DateDisplay = implode("\n",array_remove_empty((array)$DateDisplayArr));
			
			# PIC
			$ClubPICNameArr = array();
			foreach((array)$PICAssoc[$thisClubID] as $PIC)
			{
				$ClubPICNameArr[] = $PICNameArr[$PIC];
			}
			$ClubPICName = implode("\n",array_remove_empty((array)$ClubPICNameArr));
			
			#WebSAMS
			$webSamsArr = $libenroll->getWebSAMS('club', (array)$thisClubInfo['EnrolGroupID']);
			
			$ExportCol = array();
			$ExportCol[] = $thisClubInfo['GroupCode'];
			$ExportCol[] = $thisClubInfo['GroupTitle'];
			$ExportCol[] = $thisClubInfo['TitleChinese'];
			$ExportCol[] = removeHTMLtags($thisClubInfo['Description']);
			$ExportCol[] = $CategoryAssoc[$thisClubInfo['GroupCategory']]['CategoryName'];
			$ExportCol[] = $thisClubInfo['YearTermName']; 
			$ExportCol[] = implode("\n",array_remove_empty((array)$TargetFormAssoc[$thisClubID]));
			$ExportCol[] = ($thisClubInfo['UpperAge']==0 && $thisClubInfo['LowerAge']==0)?$Lang['eEnrolment']['NoLimit']:$thisClubInfo['LowerAge']." - ".$thisClubInfo['UpperAge'];
			$ExportCol[] = $Gender;
			$ExportCol[] = $thisClubInfo['PaymentAmount'].($thisClubInfo['isAmountTBC']?"(".$eEnrollment['ToBeConfirmed'].")":"");
			$ExportCol[] = $thisClubInfo['Quota']==0?$Lang['eEnrolment']['NoLimit']:$thisClubInfo['Quota'];
			$ExportCol[] = $thisClubInfo['MinQuota']==0?$Lang['eEnrolment']['NoLimit']:$thisClubInfo['MinQuota'];
			$ExportCol[] = $ClubPICName;
			$ExportCol[] = $DateDisplay;
			if($sys_custom['eEnrollment']['exportExtraInfor']){
			    include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
			    $OLESettingAry = $libenroll->Get_Enrolment_Default_OLE_Setting('club',(array)$thisClubInfo['EnrolGroupID']);

// 			    $ExportCol[] = ($OLESettingAry[0]['IntExt'] == "INT")?$iPort["internal_record"]:$iPort["external_record"];
// 			    $ExportCol[] = ($OLESettingAry[0]['TitleLang'] == "E")?$Lang['General']['English']:$Lang['General']['Chinese'];
			    $ExportCol[] = $OLESettingAry[0]['IntExt'];
			    $ExportCol[] = $OLESettingAry[0]['TitleLang'];
			    $ExportCol[] = $OLESettingAry[0]['CategoryID'];
			    $ExportCol[] = $OLESettingAry[0]['OLE_Component'];
// 			    $file_array = $libenroll->Get_Ole_Category_Array();
// 			    for($k = 0; $k < sizeof($file_array); $k++){
// 			        if($OLESettingAry[0]['CategoryID'] == $file_array[$k][0]){
// 			            $ExportCol[] = $file_array[$k][1];
// 			        }
// 			    }
			    
// 			    include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
// 			    $LibPortfolio = new libpf_slp();
// 			    $DefaultELEArray = $LibPortfolio->GET_ELE();
// 			    $ole_component = explode(",", $OLESettingAry[0]['OLE_Component']);
// 			    $component = array();
// 			    foreach($DefaultELEArray as $ELE_ID => $ELE_Name){
// 			        if(in_array($ELE_ID, $ole_component)){
// 			            $component[] = $ELE_Name;
// 			        }
// 			    }
// 			    $ExportCol[] = implode("\n", $component);
			}
			
			if($sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
			    $OLESettingAry = $libenroll->Get_Enrolment_Default_OLE_Setting('club',(array)$thisClubInfo['EnrolGroupID']);
			    $ExportCol[] = $OLESettingAry[0]['Organization'];
			}
			
			if($sys_custom['eEnrollment']['exportExtraInfor']){
			    if(!$sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
			        $ExportCol[] = $OLESettingAry[0]['Organization'];
			    }
			        $ExportCol[] = $OLESettingAry[0]['Details'];
			        $ExportCol[] = $OLESettingAry[0]['SchoolRemark'];
			}
			
			$ExportCol[] = $webSamsArr[0]["WebSAMSCode"];
// 			if($webSamsArr[0]["WebSAMSSTAType"] == "E"){
// 			    $STAType = $eEnrollment['WebSAMSSTA']['ECA'];
// 			}elseif($webSamsArr[0]["WebSAMSSTAType"] == "S"){
// 			    $STAType = $eEnrollment['WebSAMSSTA']['ServiceDuty'];
// 			}elseif($webSamsArr[0]["WebSAMSSTAType"] == "I"){
// 			    $STAType = $eEnrollment['WebSAMSSTA']['InterSchoolActivities'];
// 			}
			$ExportCol[] = $webSamsArr[0]["WebSAMSSTAType"];
	
			$ExportArr[] = $ExportCol;
			
		}

		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = "export_club_info.csv";
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}

?>
