<?php
#using :  

/******************************************
 * Modification Log:
 *  2020-06-12 (Tommy)
 *  - added radio button to output target form / all form record
 * 
 *  2018-10-03 (Philips)
 *  - Added Student Gender Column Related $sys_custom['eEnrolment']['ShowStudentGender']
 * 
 *  2017-08-14 (Omas) #P110324 
 *  - Added ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
 *  
 *  2017-03-09 (Anna)
 * 	- replace ALLOW_PAYMENT() by AllowPayment 
 * 
 *  2015-09-14 (Omas)
 *  - fixed back btn go to event_status.php
 * 
 *  2015-07-10 (Shan)
 *  - Update serachbox style
 * 
 *  2015-06-12 (Evan)
 * 	- Fix problem of changing page size 
 * 
 * 	2015-06-11 (Evan)
 * 	- Fix problem of changing page size 
 * 
 *  2015-06-02 (Shan)
 * 	- UI style updated        
 * 
 * 	2014-09-19	Bill
 *  - fix: cannot export normal mode (once priority mode is used)
 * 
 * 	2014-07-28	Bill
 * 	- add priority export option
 *
 *	2011-10-31	YatWoon
 *	- add customization access right checking Customization_HeungToChecking() # For Heung To only: only admin can add/edit/delete member (for club only)
 *
 *	2010-12-14 YatWoon
 *	- club name eng/chinese
 *	
 * 		20100802 Thomas
 * 			- Check the data is freezed or not
 *			  Show alert message when adding / updating the data
 *
 ******************************************/

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

global $sys_custom;

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}else{
	$page_size = "50";
}

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$libenroll = new libclubsenrol($AcademicYearID);

if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] && (!$libenroll->IS_CLUB_PIC()))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


if ($plugin['eEnrollment'])
{
	$LibUser = new libuser($UserID);
	$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
	$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		if ($helper_view)
			$CurrentPage = "PageClubAttendanceMgt";
			
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

	$libenroll = new libclubsenrol();
	$linterface = new interface_html();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        //$TAGS_OBJ[] = array($i_adminmenu_adm_enrollment_group, "", 1);        
        # tags 
        $tab_type = "club";
        $current_tab = 2;
        include_once("management_tabs.php");
        # navigation
        $PAGE_NAVIGATION[] = array($i_adminmenu_adm_enrollment_group, "");

        $linterface->LAYOUT_START();

		$lg = new libgroup($GroupID);
		$groupQuota = $libenroll->getQuota($EnrolGroupID);
		$libenroll->synApprovedCount($EnrolGroupID);
		$approvedCount = $libenroll->getApprovedCount($EnrolGroupID, $showAll);
		$memberCount = $libenroll->GET_NUMBER_OF_MEMBER($EnrolGroupID, "club", $showAll);
		
		if($sys_custom['eEnrollment']['setTargetForm']){
		    if($showAll_tmp == '')
		        $showAll_tmp = 0;
		    $showAll = $showAll_tmp ? $showAll_tmp : 0;
		    $targetForm = $libenroll->settingTargetForm;
		    
		    $showAllRD = "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">". $Lang['eEnrolment']['ShowForm'] ."</td><td>";
		    $showAllRD .= "<form name=\"showAllForm\" method=\"POST\">
                  <label><input type=\"radio\" id=\"targetForm\" name=\"showAll\" value=\"0\" onClick='click_All();'". ($showAll_tmp ? "" : "checked").">". str_replace('([=TargetForm=])', '', $Lang['eEnrolment']['ShowRoundForm']) ."</label>
                  <label><input type=\"radio\" id=\"allForm\" name=\"showAll\" value=\"1\" onClick='click_All();'". ($showAll_tmp ? "checked" : "").">". $Lang['eEnrolment']['ShowAllForm'] ."</label>
                  <input type='hidden' name='showAll_tmp' value='" . $showAll_tmp . "'>
                        </form>";
		    $showAllRD .= "</td></tr>";
		}

		$quotaDisplay = "<table cellspacing=\"0\" cellpadding=\"4\" border=\"0\" width=\"100%\" class =\"form_table_v30\">";
		$quotaDisplay .= $showAllRD;
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">";
		$quotaDisplay .= "$i_ClubsEnrollment_GroupName </td><td>". ($intranet_session_language=="en"?$lg->Title:$lg->TitleChinese) ."</td></tr>";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_GroupQuota1 </td><td>$groupQuota</td></tr>";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_GroupApprovedCount </td><td>$approvedCount</td></tr>";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_CurrentSize </td><td>$memberCount</td></tr>";
		$quotaDisplay .= "</table>";
		
		//$searchbar = $libenroll->GET_STUDENT_NAME_SERACH_BOX($keyword, "keyword", $eEnrollment['enter_student_name']);
		//$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "javascript:document.form1.keyword.value = Trim(document.form1.keyword.value); document.form1.submit()");
		
		$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
		
		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_student_name']) $keyword = "";

if ($field=="" || !isset($field))
{
	$field = 0;
}
switch ($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	case 4: $field = 4; break;
	//case 5: $field = 5; break;
}

$order = ($order == "") ? 1 : $order;
if (!isset($filter) || $filter == "") $filter = -1;
//$conds_status = "AND iegs.RecordStatus = $filter";

/*
$sql = "SELECT
				iu.ClassName,
				iu.ClassNumber,
				CONCAT(	'<a href=\'javascript:void(0)\' class=\'tablelink\' onClick=\'javascript:newWindow(\"group_member_enrolment.php?StudentID=', iu.UserID, '\", 10)\'>', 
						iu.EnglishName,
						IF(iug.RecordType = 'A', '".$IndicatorPrefix."*".$IndicatorSuffix."', ''),
						IF(iu.ClassName Is Null Or iu.ClassName='' Or iu.ClassNumber Is Null Or iu.ClassNumber='' Or iu.RecordStatus != 1, '".$IndicatorPrefix."^".$IndicatorSuffix."', ''),
						'</a>'),
				CONCAT('<a href=\'javascript:void(0)\' class=\'tablelink\' onClick=\'javascript:newWindow(\"group_member_enrolment.php?StudentID=', iu.userID, '\", 10)\'>', iu.ChineseName, '</a>'),
				CASE iegs.RecordStatus
					WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
					WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
					WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
				END,
				iegs.DateInput,
				CONCAT('<input type=checkbox name=uid[] value=', iu.UserID ,'>'),
				iegs.StatusChangedBy
        FROM
        		INTRANET_USER as iu
        		Inner Join
        		INTRANET_ENROL_GROUPSTUDENT as iegs
        		On iu.UserID = iegs.StudentID
        WHERE
        		iegs.EnrolGroupID = '$EnrolGroupID'
				And
				iu.RecordType = 2
        		AND
        		(iu.EnglishName LIKE '%".$keyword."%' OR iu.ChineseName LIKE '%".$keyword."%')
        		$conds_status
        ";
*/

$sql = $libenroll->Get_Management_Club_Enrollment_Student_Sql(array($EnrolGroupID), $AcademicYearID, $keyword, $filter, $ForExport=0, $showAll);

//debug_pr($sql);die();
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("iu.ClassName", "iu.ClassNumber", "iu.EnglishName", "iu.ChineseName");
if($sys_custom['eEnrolment']['ShowStudentGender']){
    $li->field_array[] = "iu.Gender";
}
if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
	$li->field_array[]="iu.NickName";	
}
$li->field_array[]= "iegs.RecordStatus";
$li->field_array[]= "iegs.DateInput";
$li->field_array[]= "iu.UserID";


//### Get the export Sql first
//$li->sql = $libenroll->Get_Management_Club_Enrollment_Student_Sql(array($EnrolGroupID), $AcademicYearID, $keyword, $filter, $ForExport=1);
//$ExportSQLTemp = $li->built_sql();
//$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
//$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);

$AllowPayment = $libenroll->ALLOW_PAYMENT($EnrolGroupID);

### Restore to display Sql
$li->sql = $sql;
if (!$libenroll->disableStatus ||$AllowPayment)
{
	$li->no_col = sizeof($li->field_array)+1;
}
else
{
	$li->no_col = sizeof($li->field_array);
}
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = "eEnrolmentGroupEnrolList";

// TABLE COLUMN
$col = 0;
$li->column_list .= "<td width=1 class=tableTitle><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($col++, $i_ClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($col++, $i_ClassNumber)."</td>\n";
$li->column_list .= "<td width=23% class=tableTitle>".$li->column($col++, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width=18% class=tableTitle>".$li->column($col++, $i_UserChineseName)."</td>\n";
if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
	$li->column_list .= "<td width=18% class=tableTitle>".$li->column($col++, $i_UserNickName)."</td>\n";
}
if($sys_custom['eEnrolment']['ShowStudentGender']){
    $li->column_list .= "<td width=8% class=tableTitle>".$li->column($col++, $Lang['General']['Sex'])."</td>\n";
}
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($col++, $i_ClubsEnrollment_Status)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($col++, $i_ClubsEnrollment_LastSubmissionTime)."</td>\n";
if (!$libenroll->disableStatus || $AllowPayment)
{
	$li->column_list .= "<td width='1'>".$li->check("uid[]")."</td>\n";
}


$filter_array = array (
                       array (-1,$i_status_all),
                       array (0,$i_ClubsEnrollment_StatusWaiting),
                       array (1,$i_ClubsEnrollment_StatusRejected),
                       array (2,$i_ClubsEnrollment_StatusApproved)
                       );
$toolbar = getSelectByArray($filter_array,'name=filter id="filter" onchange="js_Refresh();"',$filter, 0, 1);

if ($msg == 2)
{
     //$response_msg = $i_Discipline_System_alert_Approved;
     $response_msg = $linterface->GET_SYS_MSG("update");
}
if ($msg == 4)
{
     $response_msg = $linterface->GET_SYS_MSG("", $eEnrollment['approve_quota_exceeded']);
}
if ($msg == 5)
{
     $response_msg = $linterface->GET_SYS_MSG("", $eEnrollment['crash_group']);
}
if ($msg == 6)
{
     $response_msg = $linterface->GET_SYS_MSG("", $eEnrollment['crash_activity']);
}

//paramemters for adding student in enrolment list
$type = 2;
$add_member_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";

# Setting - >Disallow PIC to add/remove members Control
$showBtnforPIC = true;
if($libenroll->disallowPICtoAddOrRemoveMembers){
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])){
		if(($libenroll->IS_CLUB_PIC($EnrolGroupID))){
			$showBtnforPIC = false;
		}
	}
}

#	Get toolbar
if (!$libenroll->disableStatus || $AllowPayment){
	
	# Initiate toolbar
	$x = '';
	
	# Export options
	$optionAry = array();
	$optionAry[] = array("javascript:clickedExport('normal');", $Lang['Btn']['Export'], 'normalExportBtn');
	$optionAry[] = array("javascript:clickedExport('priority');", $Lang['Btn']['ExportPrioity'], 'priorityExportBtn');
	
	$x .= "<div class=\"content_top_tool\"><div class=\"Conntent_tool\">";
	
	// Get add button
	if($showBtnforPIC){
	if($libenroll->AllowToEditPreviousYearData && $libenroll->Customization_HeungToChecking()) {
		$x .= toolBarSpacer();
		$ParHref = ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "add_member.php?$add_member_parameters";
		if($filter==1){
		
		}else{
			$x .= '<a href="'. $ParHref .'"> '. $Lang['Btn']['Add'] .'</a>';
		}
		
	}
	}
	
	// Get export content
	$buttonHref = "javascript:void(0);";
	
	$x .= $linterface->Get_Content_Tool_v30('export', $buttonHref, $text="", $optionAry, $other="", $divID='', $extra_class='', $hideDivAfterClicked=false);
	
	if($sys_custom['eEnrolment']['ReplySlip']){
		$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolGroupID);
		$ReplySlipRelation = $ReplySlipRelation[0];
		$ReplySlipGroupMappingID = $ReplySlipRelation['ReplySlipGroupMappingID'];
		$ReplySlipID = $ReplySlipRelation['ReplySlipID'];
		
		$ReplySlipReply = '';
		if($ReplySlipGroupMappingID){
			$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
			$link = '<a href="javascript:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$EnrolGroupID.','.$ReplySlipID.')" style="background:url(\'/images/2009a/eOffice/icon_resultview.gif\') no-repeat;">'.$eEnrollment['replySlip']['replySlip'].'</a>';
		}
		$x .= $link;
	}
	$x .= "</div></div>";
	
	$htmlAry['toolbar'] = $x;
}

if($sys_custom['eEnrolment']['ReplySlip']){
	echo $linterface->Include_Thickbox_JS_CSS();
}
?>
<script language="javascript">
function onLoadThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	load_dyn_size_thickbox_ip('<?=$eEnrollment['replySlip']['ReplySlipDetail']?>', 'onLoadReplySlipThickBox('+ReplySlipGroupMappingID+','+EnrolGroupID+','+ReplySlipID+');');
}
function onLoadReplySlipThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	$.ajax({
		url: "ReplySlip_ajax_ReplySlipReply_view2.php",
		type: "POST",
		data: {
			"ReplySlipGroupMappingID": ReplySlipGroupMappingID, 
			"EnrolGroupID": EnrolGroupID,
			"ReplySlipID":ReplySlipID,
			"Status" : $("#filter").val()
			},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
		}
	});
}
function goExport(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	document.form1.action = "ReplySlip_ajax_ReplySlipReply_view2.php?ReplySlipGroupMappingID="+ReplySlipGroupMappingID+"&EnrolGroupID="+EnrolGroupID+"&ReplySlipID="+ReplySlipID+"&Status="+$("#filter").val()+"&isExport="+1;
	document.form1.target = "_blank";
	document.form1.method = "post";
	document.form1.submit();
}

function clickedExport(types)
{	
	// Type: club applicants list
	document.getElementById('type').value = 5;
	
	// Add parameter for export priority report
	if(types == 'priority'){
		document.getElementById('priority').value = 1;
	} else {
		document.getElementById('priority').value = 0;
	}
	var jsObjForm = document.getElementById('form1');
	var showAll = document.getElementById('showAll_tmp');
	<?if($sys_custom['eEnrollment']['setTargetForm']){?>
	jsObjForm.action = 'export.php?showAll='+<?=$showAll?>;
	<?}else{?>
	jsObjForm.action = 'export.php';
	<?}?>
    jsObjForm.submit();
    // Prevent always export when submit
    jsObjForm.action = '';
}

function js_Go_Export()
{
	document.getElementById('type').value = 5;
	
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = 'export.php';
    jsObjForm.submit();
}

function js_Refresh()
{
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = 'group_member.php';
    jsObjForm.submit();
}

function click_All()
{
	var obj=document.showAllForm;
	
	if(obj.targetForm.checked==true)
		obj.showAll_tmp.value=0;
	else
		obj.showAll_tmp.value=1;

	obj.action = "group_member.php?AcademicYearID="+<?=$AcademicYearID?>+"&EnrolGroupID="+<?=$EnrolGroupID?>+"&filter="+<?=$filter?>;
	obj.submit();	
}

</script>
		
<br />
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td><?=$quotaDisplay?></td></tr>
	<tr><td height="5"></td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
</table>
<br/>

<form id="form1" name="form1" ACTION="" METHOD="POST">
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td align="left"></td>
	<td align="right"><?=$response_msg?></td>
</tr>
<tr>
	<td align="left">
		<?=$htmlAry['toolbar']?>
	<!--	
	<?
		if (!$libenroll->disableStatus ||$AllowPayment)
		{
		?>
			<? if($libenroll->AllowToEditPreviousYearData && $libenroll->Customization_HeungToChecking()) {?>
			<?= toolBarSpacer()?>
			<?= $linterface->GET_LNK_ADD(($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "add_member.php?$add_member_parameters", '', '', '', '', 0) ?>
			<?}?>
			<?= toolBarSpacer() ?>
			<?= $linterface->GET_LNK_EXPORT("javascript:js_More_Export();", '', '', '', '', 0) ?>
			<div class="content_top_tool">
				<div class="Conntent_tool">
					<?=$htmlAry['toolbar']?>
				</div>
			</div>

		<?
		}
		?> 
	-->
	</td>
	<td align="right"><?=$htmlAry['searchBox']?></td>
</tr>
<tr class="table-action-bar">
	<td align="left">
		<?= $toolbar ?>
		<?= toolBarSpacer()?>
	 
		<!-- <?= $searchbar ?> -->
		
	</td>
	<td align="right" valign="bottom">
	
	<? 
	if ($libenroll->AllowToEditPreviousYearData && (!$libenroll->disableStatus || $AllowPayment) && $libenroll->Customization_HeungToChecking())
	{	
	?>
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
		  <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
		  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
		  <table border="0" cellspacing="0" cellpadding="2">
		      <tr>
		        <td nowrap="nowrap"><a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRejectNoDel(document.form1,'uid[]','group_stu_remove_update.php')"?>" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_reject.gif" width="12" height="12" border="0" align="absmiddle">
		          <?=$button_reject?></a></td>
		        <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
		        <td nowrap="nowrap"><a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkApprove(document.form1,'uid[]','group_stu_approve_update.php')"?>" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle">
		          <?=$button_approve?></a></td>
		        <!--
		        <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
		        <td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'RecordID[]','edit.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle">
		          <?=$button_edit?></a></td>
		        -->
		      </tr>
		  </table></td>
		  <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
		</tr>
		</table>
	<? 
	}
	?>

	</td>
</tr>
<tr><td colspan="2"><?=$li->display("100%")?></td></tr>
</table><br />



<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='club_status.php?AcademicYearID=$AcademicYearID'")?>
</div>
</td></tr>
</table>

<br />

<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="EventEnrolID" value="<?=$EnrolEventID?>">
<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
<input type="hidden" id="type" name="type" value="">
<input type="hidden" id="priority" name="priority" value="">
<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type='hidden' name='showAll_tmp' value="<?=$showAll_tmp?>">
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
<?php
//include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>