<?php
## Using By: 
######################################
##	Modification Log:
##  2018-08-21 Anna
##  added $sys_custom['eEnrolment']['DeleteMemberFromList']
##
##	2016-12-20 Carlos 
##	$sys_custom['StudentAttendance']['SyncDataToPortfolio'] - sync student activity profile and portfolio.
##
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition
######################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
intranet_opendb();

$li = new libdb();
$libenroll = new libclubsenrol();
$liblog = new liblog();

$access2portfolio = $_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"];


$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);
$UID = IntegerSafe($UID);

if (is_array($UID) && sizeof($UID)!=0)
{
	$SuccessArr = array();
	$li->Start_Trans();
	
	if ($type=="activity")
	{
		$GroupEventId = $EnrolEventID;
		if ($filter == 2)
		{
			//student
			$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET RecordStatus = 1 WHERE EnrolEventID = '$EnrolEventID' AND StudentID IN ('".implode("','", $UID)."')";
		    $SuccessArr['Update_INTRANET_ENROL_EVENTSTUDENT'] = $li->db_db_query($sql);
		    
		    $numDeleted = sizeof($UID);
		    
		    $sql = "UPDATE INTRANET_ENROL_EVENTINFO SET Approved = Approved - $numDeleted WHERE EnrolEventID = '$EnrolEventID'";
		    $SuccessArr['Update_INTRANET_ENROL_EVENTINFO'] = $li->db_db_query($sql);
		    
		    # insert delete log
		    $tmpAry = array();
		    $tmpAry['EnrolEventID'] = $EnrolEventID;
		    $tmpAry['DeletedUserID'] = implode(",", $UID);
		    $SuccessArr['Log_Delete_User'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Delete_Activity_Participant', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_ENROL_EVENTINFO');
		    
		    /* do not delete OLE record automatically
		    # OLE related, added on 02/09/2008 Yat Woon
		    # check OLE right
		    if((($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))||($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))))
		    {
			    $sql = "select StudentID, OLE_STUDENT_RecordID from INTRANET_ENROL_EVENTSTUDENT WHERE  EnrolEventID = $EnrolEventID AND StudentID IN (".implode(",", $UID).")";
			    $result = $li->returnArray($sql);
			    foreach($result as $key=>$data)
			    {
				    list($tempSID, $tempOLE) = $data;
					$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT set OLE_STUDENT_RecordID is NULL WHERE  EnrolEventID = $EnrolEventID AND StudentID = $tempSID";
			    	$li->db_db_query($sql);    
			    	
			    	$sql = "delete from {$eclass_db}.OLE_STUDENT where RecordID=$tempOLE";
			    	$li->db_db_query($sql);    
			    }
		    }
		    */
		}
		else
		{
			//staff
			$sql = "DELETE FROM INTRANET_ENROL_EVENTSTAFF WHERE EnrolEventID = '$EnrolEventID' AND UserID IN ('".implode("','", $UID)."')";
		    $SuccessArr['Delete_INTRANET_ENROL_EVENTSTAFF'] = $li->db_db_query($sql);
		    
		    # insert delete log
		    $tmpAry = array();
		    $tmpAry['EnrolEventID'] = $EnrolEventID;
		    $tmpAry['DeletedUserID'] = implode(",", $UID);
		    $SuccessArr['Log_Delete_User'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Delete_Activity_Staff', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_ENROL_EVENTSTAFF');
		}
		
		if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
			$libenroll->SyncProfileRecordsForActivity($AcademicYearID, $YearTermID, $EnrolEventID);
			$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
		}
		
	}
	else
	{
		$GroupEventId = $EnrolGroupID;
		if ($filter == 2)
		{
			/* do not delete OLE record automatically
			# OLE related, added on 02/09/2008 Yat Woon
			# check OLE right
			if((($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))||($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))))
			{
			    $sql = "select UserID, OLE_STUDENT_RecordID from INTRANET_USERGROUP WHERE GroupID = $GroupID and UserID IN (".implode(",", $UID).")";
			    $result = $li->returnArray($sql);
			    
			    foreach($result as $key=>$data)
			    {
				    list($tempSID, $tempOLE) = $data;
			    	$sql = "delete from {$eclass_db}.OLE_STUDENT where RecordID=$tempOLE";
			    	$li->db_db_query($sql);    
			    }
		    }
		    */
		    
			//student
			# check if student was imported in the system
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE EnrolGroupID = '$EnrolGroupID'";
			$memberArr = $li->returnVector($sql);			
			$sql = "SELECT StudentID FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$EnrolGroupID'";
			$enrolArr = $li->returnVector($sql);
			$importedStu = array();		
			$nonImportedStu = array();		
			$importedStu = array_diff($memberArr, $enrolArr);
			$nonImportedStu = array_diff($memberArr, $importedStu);	
			
			$selectedAndNonImportedStuArr = array_intersect($UID, $nonImportedStu);
			$selectedAndImportedStuArr = array_intersect($UID, $importedStu);
			
			$selectedToDeleteList = implode(",", $UID);
			$selectedAndNonImportedStuList = implode(",", $selectedAndNonImportedStuArr);
			$numOfSelectedAndNonImportedStu = sizeof($selectedAndNonImportedStuArr);
			
			# delete selected students in INTRANET_USERGROUP
			$sql = "DELETE FROM INTRANET_USERGROUP WHERE EnrolGroupID = '$EnrolGroupID' AND UserID IN ($selectedToDeleteList)";
		    $SuccessArr['Delete_INTRANET_USERGROUP'] = $li->db_db_query($sql);
		    
		    # insert delete log
		    $tmpAry = array();
		    $tmpAry['EnrolGroupID'] = $EnrolGroupID;
		    $tmpAry['DeletedUserID'] = $selectedToDeleteList;
		    $SuccessArr['Log_Delete_User'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Delete_Club_Member', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_USERGROUP');
		    
		    # delete select students in the staff list
		    $sql = "DELETE FROM INTRANET_ENROL_GROUPSTAFF WHERE EnrolGroupID = '$EnrolGroupID' AND UserID IN ($selectedToDeleteList)";
		    $SuccessArr['Delete_INTRANET_ENROL_GROUPSTAFF'] = $li->db_db_query($sql);
		    
		    # update the enrol status
		    $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 1, StatusChangedBy = '$UserID' WHERE EnrolGroupID = '$EnrolGroupID' AND StudentID IN ($selectedToDeleteList)";
		    if($sys_custom['eEnrolment']['DeleteMemberFromList']){
		        $sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$EnrolGroupID' AND StudentID IN ($selectedToDeleteList)";
		    }
		    $SuccessArr['Update_INTRANET_ENROL_GROUPSTUDENT'] = $li->db_db_query($sql);
		    
		    # if not imported student, update the enrol club number
		    if ($numOfSelectedAndNonImportedStu > 0)
		    {
		    	$sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved = Approved - 1 WHERE StudentID IN ($selectedAndNonImportedStuList)";
		    	$SuccessArr['Update_INTRANET_ENROL_STUDENT'] = $li->db_db_query($sql);
		    }
		    
		    # update the approved non-import student number
		    if ($numOfSelectedAndNonImportedStu > 0)
		    {
			    $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = Approved - $numOfSelectedAndNonImportedStu WHERE EnrolGroupID = '$EnrolGroupID'";
			    $SuccessArr['Update_INTRANET_ENROL_GROUPINFO'] = $li->db_db_query($sql);
		    }
		}
		else
		{
			//staff
			$sql = "DELETE FROM INTRANET_USERGROUP WHERE EnrolGroupID = '$EnrolGroupID' AND UserID IN ('".implode("','", $UID)."')";
		    $SuccessArr['Delete_INTRANET_USERGROUP'] = $li->db_db_query($sql);
		    
		    $sql = "DELETE FROM INTRANET_ENROL_GROUPSTAFF WHERE EnrolGroupID = '$EnrolGroupID' AND UserID IN ('".implode("','", $UID)."')";
		    $SuccessArr['Delete_INTRANET_ENROL_GROUPSTAFF'] = $li->db_db_query($sql);
		    
		    # insert delete log
		    $tmpAry = array();
		    $tmpAry['EnrolGroupID'] = $EnrolGroupID;
		    $tmpAry['DeletedUserID'] = implode(",", $UID);
		    $SuccessArr['Log_Delete_User'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Delete_Club_Staff', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_USERGROUP');
		}
		
		if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
			$libenroll->SyncProfileRecordsForClub($AcademicYearID, $YearTermID, $EnrolGroupID);
			$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
		}
	}
	
    if (in_array(false, $SuccessArr))
    {
    	$li->RollBack_Trans();
    }
    else
    {
    	$li->Commit_Trans();
    	
    	// send email
	    if ($filter==2) {
			if (empty($type)) {
				$type="club";
			} else {
				// do nothing (when processing activity data, $type will never empty)
			}		
	    	$successResult = false;
	    	$libenroll->Send_Enrolment_Result_Email($UID, $GroupEventId, $type, $successResult, 1);
	    }
    }
}

intranet_closedb();

if ($type=="activity")
{
	header("Location: event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&msg=3");
}
else
{
	header("Location: member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&filter=$filter&msg=3");
}

?>