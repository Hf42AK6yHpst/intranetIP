<?php
## Using By : yat
###########################################
##	Modification Log:
##
##	2012-01-18	YatWoon
##	Improved: display student name in notification email
##	Fixed:	- remove "Period"
##			- crashed UI
##
##	2010-02-01 Max (201001271658) 
##	Add radio box to send email together with parent

##	2010-01-08 Max (201001081039)
##	Send Email for Enrollment Result
###########################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();	
	
	// Page redirection if the user is not admin/master/normal user
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID']))) {
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	}
	
	$CurrentPage = "PageMgtActivity";
	
	// Define the selection of [Record][Process] tabs
	$current_tab = 2;
	
    include_once("management_tabs.php");
        
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
	/*
	 * S: This is used to check the login status of a user
	 * If a user is valid
	 *  load the page
	 * Else
	 *  shows "You have no priviledge to access this page."
	 */
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();

	    
	    
	    /* Start Used by Steps */
//	    $STEPS_OBJ[] = array("Select student(s)", 1);
//        $STEPS_OBJ[] = array("Compose email", 0);
	    /* End Used by Steps */
	     
	$ldiscipline = new libdisciplinev12();
	
	$lclass = new libclass();
	$linterface = new interface_html();
	
	# School year
	$currentAcademicYearID = Get_Current_Academic_Year_ID();
	$yearStart = getStartDateOfAcademicYear($currentAcademicYearID);
	$yearEnd = getEndDateOfAcademicYear($currentAcademicYearID);


$linterface->LAYOUT_START();
?>
<!-- TODO: A javascript of checking form maybe needed -->
<!-- S:copy from IntranetIP25/StudentMgmt/disciplinev12/reports/goodconduct_misconduct/index.php -->
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str, choice)
{
	//alert("showResult :\nstr - "+str+"\nchoice - "+choice+"\nYearID - <?=$currentAcademicYearID?>"+"\nStudentFlag - "+document.getElementById("studentFlag").value);
    if (str != "student2ndLayer") {
        document.getElementById('spanRankTarget').style.display = 'none';
        document.getElementById('spanRankTargetDefault').style.display = 'inline-block';
    }
	if (str != "student") {
		$("#studentFlag").val(0);
	}
	if (str.length==0)
		{ 
			document.getElementById("spanRankTarget").innerHTML = "";
			document.getElementById("spanRankTarget").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
	url = "get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+<?=$currentAcademicYearID?>;
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "spanRankTarget";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
        if (xmlHttp.responseText.length > 0)
        {
            document.getElementById(showIn).style.display = 'inline-block';
            document.getElementById(showIn + 'Default').style.display = 'none';
        }
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
</script>

<script language="javascript">

/*S:show or hide the students' list after selected classes*/
function showSpan(span) {
	if(span == 'spanStudentContainer') {
        document.getElementById(span).style.display="inline-block";
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} else {
        document.getElementById(span).style.display="inline";
    }
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudentContainer') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}
/*E:show or hide the students' list after selected classes*/
function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function checkForm(){
	try {
		if ($("#rankTarget").val() == "#") {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");
			return false;
		} else if ($("#rankTarget").val() == "form" && $("#rankTargetDetail\\[\\] option:selected").length<1) {
			alert("<?=$i_alert_pleaseselect.$i_Discipline_Form?>");
			return false;
		} else if ($("#rankTarget").val() == "class" && $("#rankTargetDetail\\[\\] option:selected").length<1){
			alert("<?=$i_alert_pleaseselect.$i_Discipline_Class?>");
			return false;
		} else if ($("#rankTarget").val() == "student" && $("#studentID\\[\\] option:selected").length<1) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");
			return false;
		}
	} catch (e) {
		alert(e.description());
	}
	return true;
}
</script>
<!-- E:copy from IntranetIP25/StudentMgmt/disciplinev12/reports/goodconduct_misconduct/index.php -->

<form name="form1" action='event_email_send.php' method='post' onSubmit="return checkForm();"><br/>
	<!-- Start Navigation -->
	<table width="100%" border="0" cellspacing="4" cellpadding="4">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
	</table>
	<!-- End Navigation -->
	
	<!-- Start Steps -->
	<?/*<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/></td>
		</tr>
	</table>*/?>
	<!-- End Steps -->
	
	<!-- S:Choose students -->
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr align="left">
						<td>
							<?/*<!-- S:-option- -->
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
								<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
									<?=$optionString?>
								</tr>
							</table>
							<!-- E:-option- -->*/?>
							
							<!-- S:Option Panel -->
							<span id="spanOptionContent">
							<table width="100%" border="0" cellpadding="5" cellspacing="0" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
							<tr><td colspan="2" class="tabletext" align="right"><? if ($msg == 2) print $linterface->GET_SYS_MSG("email"); ?></td></tr>
								<? /* ?>
								<!-- S:Period -->
								<tr valign="top">
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
										<?=$iDiscipline['Period']?>
									</td>
									<td width="70%">
										<table border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td height="30" colspan="6">
													Year <?=date("Y", strtotime($yearStart))?>-<?=date("Y", strtotime($yearEnd))?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- E:Period -->
								<? */ ?>
								
								
								<!-- S:Target -->
								<tr valign="top">
									<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
										<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
									</td>
									<td width="70%">
									<? /* ?>
										<table width="0%" border="0" cellspacing="2" cellpadding="0">
											<tr>
												<td valign="top">
													<table border="0">
														<tr>
															<td valign="top">
																<select name="rankTarget" id="rankTarget" onChange="showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
																	<option value="#">-- <?=$i_general_please_select?> --</option>
																	<option value="form" <? if($rankTarget=="form") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
																	<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
																	<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
																</select>
															</td>
															<td>
																<div id='spanRankTarget' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
																<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
																<br><?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
															</td>
															<td>
																<div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:1;<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
																	<select name="studentID[]" multiple size="5" id="studentID[]"></select>
																	<br><?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
																</div>
															</td>
														</tr>
														<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
													</table>
												</td>
												<td valign="top"><br /></td>
											</tr>
										</table>
										<? */ ?>
										
										<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<table border="0">
													<tr>
														<td valign="top">
															<select name="rankTarget" id="rankTarget" onChange="showResult(this.value,'');if(this.value=='student') {showSpan('spanStudentContainer');} else {hideSpan('spanStudentContainer');}">
																<option value="#">-- <?=$i_general_please_select?> --</option>
																<option value="form" <? if($rankTarget=="form") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
																<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
																<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
															</select>
														</td>
														<td>
<!--															<div id='spanRankTarget' style='position:absolute; width:280px; height:0px; z-index:0;'></div>-->
<!--															<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5" id="rankTargetDetail[]"></select>-->
<!--															<br><br><br>--><?//= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
<!--															<br> -->
<!--															<div id='spanStudent' style='position:relative; width:280px; height:80px; z-index:1;--><?// if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?><!--'>-->
<!--																<select name="studentID[]" multiple size="5" id="studentID[]"></select>-->
<!--																<br>--><?//= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
<!--															</div>-->


                                                            <div style="padding-bottom: 15px">
                                                                <div id='spanRankTarget' style="min-width:150px; display: none;">

                                                                </div>
                                                                <div id='spanRankTargetDefault' style="min-width:150px; display: inline-block;">
                                                                    <select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
                                                                </div>
                                                                <span style="vertical-align: top;"><?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?></span>
                                                            </div>

                                                            <div id='spanStudentContainer' style="padding-bottom: 15px; <? if($rankTarget=="student") { echo "display:inline-block;";} else { echo "display:none;";} ?>">
                                                                <div id='spanStudent' style="min-width:150px; display: none;">

                                                                </div>
                                                                <div id='spanStudentDefault' style="min-width:150px; display: inline-block;">
                                                                    <select name="studentID[]" multiple size="5" id="studentID[]"></select>
                                                                </div>
                                                                <span style="vertical-align: top;"><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?></span>
                                                            </div>
														</td>
													</tr>
													<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
												</table>
											</td>
											<td valign="top"><br /></td>
										</tr>
									</table>
									 
									
									</td>
								</tr>
								<!-- E:Target -->
								
								<!-- Email Sample -->
								<tr>
									<td valign="top"><?= $Lang['eEnrolment']['EmailSample'] ?></td>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
											<tr><td><?= implode('<br />', str_replace("__STUDENT_NAME__", "[".$Lang['StudentAttendance']['StudentName']."]",$Lang['eEnrolment']['ActivityEmailSampleArr'])) ?></td></tr>
										</table>
									</td>
								</tr>
								
								<!-- Send Email With Parent -->
								<tr>
									<td valign="top"><?= $Lang['eEnrolment']['SendEmailWithParent'] ?></td>
									<td>
										<input type="radio" name="sendWithParent" id="sendWithParentYes" value="1" checked="checked"/><label for="sendWithParentYes"><?=$Lang['General']['Yes']?></label>
										<input type="radio" name="sendWithParent" id="sendWithParentNo" value="0" /><label for="sendWithParentNo"><?=$Lang['General']['No']?></label>
									</td>
								</tr>
								
								<!-- S:Action Button(s) -->
								<tr>
									<td colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
											<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
											<tr><td align="left" colspan="6"><span class="tabletextremark"><?=$i_general_required_field?></span></td></tr>
											<tr><td align="center" colspan="6">
												<div style="padding-top: 5px">
													<?= $linterface->GET_ACTION_BTN($Lang['eEnrolment']['SendEmail'], "submit", "", "submitBtn01")?>
												</div>
											</td></tr>
										</table>
									</td>
								</tr>
								<!-- S:Action Button(s) -->
							</table>
							</span>
							<!-- E:Option Panel -->
						</td>
					</tr>
				</table>				
			</td>
		</tr>
	</table>
	<input type="hidden" name="currentYear" value="<?=$currentAcademicYearID?>" />
	<input type="hidden" name="id" id="id" value="">
	<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">
	<input type="hidden" name="showStatButton" id="showStatButton" value="0" />
	<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
	<!-- E:Storing parameters to next page -->
</form>
<br />
<!-- E:Choose students -->
<?=$initialString?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>

<script language="javascript">
/*S:Initially set [Form] as the preset selection*/
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form","");
}
<?
if($submitBtn01 == "")
	echo "init();\n";
?>
<?
if($submitBtn01 != "") {
	echo "showResult(\"$rankTarget\", \"$jsRankTargetDetail\");";
}
?>
/*E:Initially set [Form] as the preset selection*/


//-->
</script>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
    /*E: This is used to check the login status of a user*/
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>