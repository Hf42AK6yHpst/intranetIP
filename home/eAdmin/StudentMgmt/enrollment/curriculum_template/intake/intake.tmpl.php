<?php
/*
 * 	Log
 *
 *  2018-09-04 [Cameron]
 *      add * to indicate left instructor
 *      
 * 	2018-07-12 [Cameron]
 * 		create this file
 */
?>
<script type="text/javascript">
var isLoading = true;
var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
	if($('#Title').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'Title';
 		$('#ErrTitle').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if ( ($('#StartDate').val() != '' ) && ($('#EndDate').val() != '' ) && (compareDate($('#EndDate').val(), $('#StartDate').val()) < 0) ) {
 		error++;
 		if (focusField == '') focusField = 'StartDate';
 		$('#ErrStartDate').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if ($(':input[name="ClassName\[\]"]').length == 0 ) {
 		error++;
 		if (focusField == '') focusField = 'ClassAddBtn';
 		$('#ErrClassAddBtn').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	
 	$(':input[name="ClassName\[\]"]').each(function(){
 		if ($.trim($(this).val()) == '') {
 	 		error++; 	 		
 	 		if (focusField == '') focusField = $(this).attr('id');
 	 		$('#Err' + $(this).attr('id')).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});

 	$(':select[name^="Instructor"]').each(function(){
 	 	var id = $(this).attr('id');
 		if ($('#' + id + ' option:selected').length == 0) {
 	 		error++; 	 		
 	 		if (focusField == '') focusField = $(this).attr('id');
 	 		$('#Err' + $(this).attr('id')).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});

 	$(':select[name="Location\[\]"]').each(function(){
 		if ($.trim($(this).val()) == '') {
 	 		error++; 	 		
 	 		if (focusField == '') focusField = $(this).attr('id');
 	 		$('#Err' + $(this).attr('id')).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});
 	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

$(document).ready(function(){
		
	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

	    $(':select[name^="Instructor"]').each(function(){
		    var thisID = $(this).attr('id');
// 		    thisID = thisID.replace(/\[/g,"\\\[");
// 		    thisID = thisID.replace(/\]/g,"\\\]");
			$('#' + thisID + ' option').attr('selected',true);
	    });

		if (checkForm(document.form1)) {
   			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

});

function addClass() 
{
	var row = parseInt($('#rowCounter').val());
	$.ajax({
		dataType: "json",
		async: false,		// to safeguard rowCounter	
		type: "POST",
		url: '../ajax.php?&action=addClass&Row='+row,
		data : $('#form1').serialize(),		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#classListArea').append(ajaxReturn.html);
				$('#rowCounter').val(row+1);
			}
		},
		error: show_ajax_error
	});
}

function removeClass(obj)
{
	var $this = $(obj);
	var classID = $this.attr('data-ClassID');
	
	var conf = confirm('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['confirmDeleteClass'];?>');
	if (!conf) {
		return;
	}
	
	if (classID != '') {	// old record
		$("#classesToDelete").val($("#classesToDelete").val()+','+classID);
	}
	$this.parent().parent().parent().parent().parent().remove();

}

function changeNumberOfStudent(obj)
{
	var $this = $(obj);
	var thisID = $this.attr('id');
	var str = thisID.split('_');
	var rowID = str[1];

	isLoading = true;
	$('#LocationCol_'+rowID).html(loadingImg);
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '../ajax.php',
		data : {
			'action': 'getAvailableLocationListInIntake',
			'capacity': $this.val(),
			'row': rowID
		},
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#LocationCol_'+rowID).html(ajaxReturn.html);
				isLoading = false;
			}
		},
		error: show_ajax_error
	});
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

</script>

<style>
.hide {display:none};
.show {display:''};
 </style>

<form name="form1" id="form1" method="post" action="<?php echo $form_action;?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="navigation"><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION); ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['title'];?></td>
								<td class="tabletext">
									<input type="text" name="Title" id="Title" class="textboxtext" value="<?php echo intranet_htmlspecialchars($intake['Title']);?>">
									<span class="error_msg_hide" id="ErrTitle"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputIntakeTitle'];?></span>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['General']['StartDate'];?></td>
								<td class="tabletext">
									<?php echo $linterface->GET_DATE_PICKER("StartDate",(empty($intake['StartDate']) ? date('Y-m-d') : $intake['StartDate']));?> 
									<span class="error_msg_hide" id="ErrStartDate"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['startDateGtEndDate'];?></span>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['General']['EndDate'];?></td>
								<td class="tabletext"><?php echo $linterface->GET_DATE_PICKER("EndDate",(empty($intake['EndDate']) ? $defaultEndDate : $intake['EndDate']));?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['class'];?></td>
								<td class="tabletext">
									<div id="classListArea">
										<?php echo $classListLayout;?>
									</div>
									<div>
										<span class="table_row_tool"><a id="ClassAddBtn" class="newBtn add" onclick="javascript:addClass()" title="<?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['addClass'];?>"></a><br></span>
										<span class="error_msg_hide" id="ErrClassAddBtn"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['addClassAtLeastOneClass'];?></span>
									</div>
								</td>
							</tr>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?php echo $i_general_required_field;?>&nbsp;&nbsp; <?php echo $Lang['eEnrolment']['instructor']['left'];?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}"; ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
    									<?php echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
    									<?php echo $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
    									<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>

<input type=hidden id="IntakeID" name="IntakeID" value="<?php echo $intakeID;?>">
<input type=hidden id="classesToDelete" name="classesToDelete" value="">
<input type=hidden id="rowCounter" name="rowCounter" value="<?php echo $_rowCounter?>">
</form>
