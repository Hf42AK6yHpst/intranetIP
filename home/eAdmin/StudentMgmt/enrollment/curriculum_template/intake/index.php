<?php
/*
 *  Using:
 *  
 *  Purpose:    show intake list
 *  
 *  
 * 	2018-07-12 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtIntake";

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['intake']['intake']);

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;		// default ascending
$field = ($field == "") ? 0 : $field;

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT
				CONCAT('<a href=\"edit.php?IntakeID=',i.IntakeID,'\">',i.Title,'</a>'),
				i.StartDate, i.EndDate,
                c.NumberOfClass, c.NumberOfStudent,
				CONCAT('<input type=\'checkbox\' name=\'IntakeID[]\' id=\'IntakeID[]\' value=\'', i.IntakeID,'\'>')
        FROM 
				INTRANET_ENROL_INTAKE i
		LEFT JOIN (SELECT 
                        IntakeID,
                        COUNT(*) AS NumberOfClass,
                        SUM(NumberOfStudent) AS NumberOfStudent
                    FROM 
                        INTRANET_ENROL_INTAKE_CLASS 
                    GROUP BY IntakeID ) AS c ON c.IntakeID=i.IntakeID 
		";

$extra_column = 2;
$checkbox_col = "<th width='1'>".$li->check("IntakeID[]")."</th>\n";
$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')",$button_new,"","","",0);
//$manage_record_bar  = '<a href="javascript:checkEdit(document.form1,\'IntakeID[]\',\'edit.php\')" class="tool_edit">'.$button_edit.'</a>';
$manage_record_bar = '<a href="javascript:checkRemove(document.form1,\'IntakeID[]\',\'remove.php\')" class="tool_delete">'.$button_delete.'</a>';

$li->field_array = array("Title", "StartDate", "EndDate","NumberOfClass", "NumberOfStudent");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='40%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['intake']['title'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['General']['StartDate'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['General']['EndDate'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfClass'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfStudent'])."</th>\n";
$li->column_list .= $checkbox_col;

?>

<form name="form1" id="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td valign="bottom">
				<div class="table_filter">&nbsp;</div> 
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
					<?=$manage_record_bar?>					
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?=$li->display()?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>