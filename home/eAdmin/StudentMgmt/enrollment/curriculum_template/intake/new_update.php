<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-13 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$result = array();
$dataAry = array();
$dataAry['Title'] = standardizeFormPostValue($_POST['Title']);
$dataAry['StartDate'] = $_POST['StartDate'];
$dataAry['EndDate'] = $_POST['EndDate'];
$dataAry['DateInput'] = 'now()'; 
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_INTAKE',$dataAry,array(),false);

$libenroll->Start_Trans();

$res = $libenroll->db_db_query($sql);
$result[] = $res;
if ($res) {
    $intakeID = $libenroll->db_insert_id();
    $classNameAry = $_POST['ClassName'];
    
    $instructors = $_POST['Instructor'];
    foreach((array)$instructors as $_instructor) {
        $instructorAry[] = IntegerSafe($_instructor);
    }
    
    $numberOfStudentAry = IntegerSafe($_POST['NumberOfStudent']);
    $locationAry = IntegerSafe($_POST['Location']);

    $numberOfClass = count($classNameAry);
    for($i=0;$i<$numberOfClass;$i++) {
        unset($dataAry);
        $dataAry['IntakeID'] = $intakeID;
        $dataAry['ClassName'] = standardizeFormPostValue($classNameAry[$i]);
        $dataAry['LocationID'] = $locationAry[$i];
        $dataAry['NumberOfStudent'] = $numberOfStudentAry[$i];
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_INTAKE_CLASS',$dataAry,array(),false);
        $res = $libenroll->db_db_query($sql);
        $result[] = $res;
        $classID = $libenroll->db_insert_id();
        
        foreach((array)$instructorAry[$i] as $_instructorID) {
            if ($_instructorID) {
                unset($dataAry);
                $dataAry['ClassID'] = $classID;
                $dataAry['InstructorID'] = $_instructorID;
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_INTAKE_INSTRUCTOR',$dataAry,array(),false, false, false);
                $result[] = $libenroll->db_db_query($sql);
            }
        }
    }
}

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>