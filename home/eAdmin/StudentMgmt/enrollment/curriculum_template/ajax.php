<?
//Using:  
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 *  2019-02-22 Cameron
 *  - add parameter ENROL_TYPE_ACTIVITY to GetEnrolEbookingRelation()
 *   
 *  2018-11-21 Cameron
 *  - disable isIntakeTimeConflict checking in checkeBooking (when generate from template for combined course) 
 *  
 *  2018-11-07 Cameron
 *  - disable checking class conflict in case checkeBooking4LessonUpdate and checkeBooking4OneLessonUpdate
 *  
 *  2018-10-26 Cameron
 *  - show time conflict classes in case checkeBooking4LessonUpdate and checkeBooking4OneLessonUpdate
 *  
 *  2018-10-24 Cameron
 *  - fix: add combined lesson layout in getCoursesByIntake
 *  - disable time conflict checking for instructor that need to monitor several classes at a time in different locations (e.g. different sections in the same playground)
 *  - apply checkIntakeTimeConflict() to checkeBooking case
 *  
 *  2018-10-18 Cameron
 *  - add case checkeBooking4OneLessonUpdate
 *  
 *  2018-10-12 Cameron
 *  - fix: add new lesson checking (available room and instructor conflict) in checkeBooking4LessonUpdate
 *  - add class conflict checking in checkeBooking4LessonUpdate
 *  
 *  2018-10-10 Cameron
 *  - add parameter $intakeID to case addLesson
 *  
 *  2018-10-05 Cameron
 *  - add case addCombinedLessonClassInstructor
 *  
 *  2018-09-20 Cameron
 *  - add case checkIsAllowedInstructor
 *  
 *  2018-09-14 Ivan
 *  - added getInstructorApplyToAllShortCutLayout table in "getCoursesByIntake" action
 *  
 *  2018-08-20 Cameron
 *  - fix:  $startTime and $endTime should not include date part in getAvailableLocationList and getAvailableLocationListInSchedule
 *  
 *  2018-08-13 Cameron
 *  - allow instructor (non-admin staff) to manage course schedule on himself/herself
 *   
 *  2018-07-13 [Cameron]
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->isInstructor()))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$y = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
//$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash

$linterface = new libclubsenrol_ui();

switch($action) {
    
    case 'addClass':
        $row = IntegerSafe($_GET['Row']);
        $x = $linterface->getIntakeClassTable(array(),$row);
        $json['success'] = true;
        break;
        
    case 'getCoursesByIntake':
        $PATH_WRT_ROOT = "../../../../../../";      // for date picker
        $intakeID = IntegerSafe($_POST['IntakeID']);
        $templateID = IntegerSafe($_POST['TemplateID']);
        
        $x = '';
        $x .= $linterface->getInstructorApplyToAllShortCutLayout($intakeID);
        
        $x .= $linterface->getCombinedCourseLayout($templateID, $intakeID);
        
        $x .= $linterface->getCombinedLessonLayout($templateID, $intakeID);
        
        $x .= $linterface->getNonCombinedCourseLayout($templateID, $intakeID);

        $intakeAry = $libenroll->getIntake($intakeID);
        $intakeStartDate = $intakeAry[$intakeID]['StartDate'];
        $ret = array();
        $ret[] = $intakeStartDate;
        
//         $capacityAry = $libenroll->getNumberOfIntakeStudents($intakeID);
//         $ret[] = $capacityAry[$intakeID];       // capacity
        $y = $ret;
        $json['success'] = true;
        break;

    case 'getAvailableLocationList':        // from generate
        $startDate = $_POST['startDate'];
        $startHour = IntegerSafe($_POST['startHour']);
        $startMinute = IntegerSafe($_POST['startMinute']);
        $endHour = IntegerSafe($_POST['endHour']);
        $endMinute = IntegerSafe($_POST['endMinute']);
        $courseID = IntegerSafe($_POST['courseID']);
        $row = IntegerSafe($_POST['row']);
        $intakeID = IntegerSafe($_POST['intakeID']);
        $startTime = str_pad($startHour, 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinute, 2, "0", STR_PAD_LEFT) . ':00';
        $endTime = str_pad($endHour, 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinute, 2, "0", STR_PAD_LEFT) . ':00';
        $prefixLocation = 'Location';
        $selectedLocationID = '';
        $bookingInfo = array('Date'=>$startDate, 'StartTime'=>$startTime, 'EndTime'=>$endTime);
        
        if ($intakeID) {
            $intakeAssoc = $libenroll->getNumberOfIntakeStudents($intakeID);
            $capacity = $intakeAssoc[$intakeID];
        }
        else {
            $capacity = 0;
        }
        
        $categoryID = '';
        if ($courseID) {
            $templateCourseAry = $libenroll->getCourseTemplate($courseID);
            if (count($templateCourseAry)) {
                $categoryID = $templateCourseAry[0]['CategoryID']; 
            }
        }
        
        $x = $linterface->getLocationSelection($prefixLocation, $selectedLocationID, $row, $courseID, $showCapacity=true, $capacity, $bookingInfo, $categoryID);
        
        $json['success'] = true;

        break;

    case 'getAvailableLocationListInIntake':        // from intake

        $prefixLocation = 'Location';
        $selectedLocationID = '';
        $row = IntegerSafe($_POST['row']);
        $courseID = null;
        $capacity = IntegerSafe($_POST['capacity']);
        
        $x = $linterface->getLocationSelection($prefixLocation, $selectedLocationID, $row, $courseID, $showCapacity=true, $capacity);
        
        $json['success'] = true;
        
        break;
        
    case 'getAvailableLocationListInSchedule':
        $lessonDate = $_POST['lessonDate'];
        $startHour = IntegerSafe($_POST['startHour']);
        $startMinute = IntegerSafe($_POST['startMinute']);
        $endHour = IntegerSafe($_POST['endHour']);
        $endMinute = IntegerSafe($_POST['endMinute']);
        $eventDateID = IntegerSafe($_POST['eventDateID']);
        $row = IntegerSafe($_POST['row']);
        $capacity = IntegerSafe($_POST['capacity']);
        $selectedLocationID = IntegerSafe($_POST['originalLocationID']);
        $classLocationID = IntegerSafe($_POST['classLocationID']);      // top priority location ID
        $categoryID = IntegerSafe($_POST['categoryID']);
        $startTime = str_pad($startHour, 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinute, 2, "0", STR_PAD_LEFT) . ':00';
        $endTime = str_pad($endHour, 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinute, 2, "0", STR_PAD_LEFT) . ':00';
        $prefixLocation = 'Location';
        
        if ($eventDateID) {
            $categoryID = $libenroll->getCategoryIDByEventDateID($eventDateID);
            $enrolBookingRelationAry = $libenroll->GetEnrolEbookingRelation($eventDateID, ENROL_TYPE_ACTIVITY);
            $bookingID = count($enrolBookingRelationAry) ? $enrolBookingRelationAry['BookingRecordID'] : '';
        }
        else {
            $bookingID = '';
        }
        
        $bookingInfo = array('Date'=>$lessonDate, 'StartTime'=>$startTime, 'EndTime'=>$endTime, 'BookingID'=>$bookingID);
// print("selectedLocationID=$selectedLocationID\n");        
// print("row=$row\n");
// print("eventDateID=$eventDateID\n");
// print("capacity=$capacity\n");
// print("bookingInfo\n");
// print_r($bookingInfo);
// print("\ncategoryID=$categoryID\n");
// print("classLocationID=$classLocationID\n");

        $x = $linterface->getLocationSelection($prefixLocation, $selectedLocationID, $row, $eventDateID, $showCapacity=true, $capacity, $bookingInfo, $categoryID, $classLocationID);
        
        $json['success'] = true;
        
        break;
        
    case 'checkeBooking':
        $courseTitleAry = $_POST['CourseTitle'];
        $locationAry = $_POST['Location'];
        $startHourAry = $_POST['StartHour'];
        $startMinuteAry = $_POST['StartMinute'];
        $endHourAry = $_POST['EndHour'];
        $endMinuteAry = $_POST['EndMinute'];
        $instructorAry = $_POST['Instructor'];
        $intakeID = $_POST['Intake'];
        $isRoomBookedAry = array();
        $isTimeConflictAry = array();
        $isIntakeTimeConflictAry = array();
        
        foreach((array)$courseTitleAry as $_courseID => $_courseTitle) {
            foreach ((array)$startHourAry[$_courseID] as $__row => $__startHour ) {
                $lessonDate = $_POST["LessonDate_{$_courseID}_{$__row}"];
                $startTime = str_pad($startHourAry[$_courseID][$__row], 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinuteAry[$_courseID][$__row], 2, "0", STR_PAD_LEFT) . ':00';
                $endTime = str_pad($endHourAry[$_courseID][$__row], 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinuteAry[$_courseID][$__row], 2, "0", STR_PAD_LEFT) . ':00';
                
                if (!empty($lessonDate) && $locationAry[$_courseID][$__row]) {
                    $bookingDetails = array();
                    $bookingDetails['Date'] = $lessonDate;
                    $bookingDetails['StartTime'] = $startTime;
                    $bookingDetails['EndTime'] = $endTime;
                    $bookingDetails['BookingType'] = 'room';
                    $bookingDetails['FacilityID'] = $locationAry[$_courseID][$__row];
                    $isRoomBooked = $libenroll->isRoomBooked($bookingDetails);  // don't set BookingID or set it to empty
                    $isRoomBookedAry[$_courseID][$__row] = $isRoomBooked;
                }
                
                if (!empty($lessonDate) && $instructorAry[$_courseID][$__row]) {
                    $startDate = $lessonDate . ' ' . $startTime;
                    $endDate = $lessonDate . ' ' . $endTime;
                    $isThisTimeConflictAry = array();
                    $instructorAry[$_courseID][$__row] = array_unique((array)$instructorAry[$_courseID][$__row]);      // remove duplicate instructor
                    foreach((array)$instructorAry[$_courseID][$__row] as $___instructor) {
                        if ($___instructor) {       // filter out empty record
                            $isThisTimeConflictAry[] = $libenroll->checkTimeLimitSetting($___instructor, $startDate, $endDate);
                        }
                    }
                    
                    $isTimeConflictAry[$_courseID][$__row] = in_array(true, $isThisTimeConflictAry) ? true : false;
                }
                
                // 2018-11-21: disable this
//                 if (!empty($lessonDate) && $intakeID) {
//                     $startDate = $lessonDate . ' ' . $startTime;
//                     $endDate = $lessonDate . ' ' . $endTime;
//                     $isIntakeTimeConflict = $libenroll->checkIntakeTimeConflict($intakeID, $startDate, $endDate);
//                     $isIntakeTimeConflictAry[$_courseID][$__row] = $isIntakeTimeConflict; 
//                 }
            }            
        }
        $x = $isRoomBookedAry;
        $y = $isTimeConflictAry;
        $json['isIntakeTimeConflict'] = $isIntakeTimeConflictAry;
        $json['success'] = true;
        
        break;
        
    // from course schedule        
    case 'checkeBooking4LessonUpdate':
        
        $bookingIDAry = $_POST['BookingID'];
        $locationAry = $_POST['Location'];
        $startHourAry = $_POST['StartHour'];
        $startMinuteAry = $_POST['StartMinute'];
        $endHourAry = $_POST['EndHour'];
        $endMinuteAry = $_POST['EndMinute'];
        $instructorAry = $_POST['Instructor'];
        $lessonClassAry = $_POST['LessonClass'];
        $newLessonClassAry = $_POST['NewLessonClass'];
        $bookingType = 'room';
        $isRoomBookedAry = array();     // for old
        $errorAry = array();        
        $timeConflictAry = array();
        $isNewRoomBookedAry = array();  // for new
        $newErrorAry = array();
        $newTimeConflictAry = array();
        
        $libebooking_api = new libebooking_api();
        
        foreach ((array)$startHourAry as $_eventDateID => $_startHourAry ) {
            if ($_eventDateID > 0) {
                $lessonDate = $_POST["LessonDate_{$_eventDateID}_0"];
                $startTime = str_pad($startHourAry[$_eventDateID][0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinuteAry[$_eventDateID][0], 2, "0", STR_PAD_LEFT) . ':00';
                $endTime = str_pad($endHourAry[$_eventDateID][0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinuteAry[$_eventDateID][0], 2, "0", STR_PAD_LEFT) . ':00';
                $locationID = $locationAry[$_eventDateID][0];
                
                $bookingDetails = array();
                
                $enrolBookingRelationAry = $libenroll->GetEnrolEbookingRelation($_eventDateID, ENROL_TYPE_ACTIVITY);
                if (count($enrolBookingRelationAry)) {  // booking record exist
                    $bookingID= $enrolBookingRelationAry['BookingRecordID'];
                    
                    $bookingRecordAry = $libebooking_api->Get_Booking_Record_By_BookingID($bookingID);
                    if (count($bookingRecordAry)) {
                        $bookingRecordAry = current($bookingRecordAry);
                        $bookingDate = $bookingRecordAry['Date'];
                        $bookingStartTime = $bookingRecordAry['StartTime'];
                        $bookingEndTime = $bookingRecordAry['EndTime'];
                        
                        if ($bookingDate != $lessonDate) {  // date is changed, like new booking checking
                            if (!empty($lessonDate) && $locationID) {
                                $bookingDetails['Date'] = $lessonDate;
                                $bookingDetails['StartTime'] = $startTime;
                                $bookingDetails['EndTime'] = $endTime;
                                $bookingDetails['BookingType'] = $bookingType;
                                $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                                //debug_pr($availableRoomAssoc);
                                $isRoomBookedAry[$_eventDateID] = !$availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                                $errorAry[$_eventDateID] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
                            }
                        }
                        else {  // same date, check time slot is changed
                            $outRangeTimeAry = $libenroll->getOutRangeTime($bookingStartTime, $bookingEndTime, $startTime, $endTime);
                            //debug_pr($outRangeTimeAry);
                            if (!empty($lessonDate) && $locationID) {
                                if (count($outRangeTimeAry)) {
                                    $timeSlotAvailableAry = array();
                                    $thisErrorAry = array();
                                    foreach((array)$outRangeTimeAry as $timeSlot) {
                                        unset($bookingDetails);
                                        $bookingDetails['Date'] = $lessonDate;
                                        $bookingDetails['StartTime'] = $timeSlot['StartTime'];
                                        $bookingDetails['EndTime'] = $timeSlot['EndTime'];
                                        $bookingDetails['BookingType'] = $bookingType;
                                        // debug_pr($bookingDetails);
                                        $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                                        $timeSlotAvailableAry[] = $availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                                        $thisErrorAry[] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
                                        // debug_pr($availableRoomAssoc);
                                        // debug_pr($timeSlotAvailableAry);
                                    }
                                    $isRoomBookedAry[$_eventDateID] = in_array(false,$timeSlotAvailableAry) ? true : false;
                                    $errorAry[$_eventDateID] = array_unique($thisErrorAry);
                                }
                                else {      // time is not changed or change to inbound timeslot
                                    $eventDateAry = $libenroll->GET_ENROL_EVENT_DATE('', '', '', $_eventDateID);
                                    //debug_pr($eventDateAry);
                                    if (count($eventDateAry)) {
                                        $originalLocationID = $eventDateAry[0]['LocationID'];
                                        // print "orgLoc=$originalLocationID\n";
                                        // print "loc=$locationID\n";
                                        if ($locationID != $originalLocationID) {       // location has changed
                                            unset($bookingDetails);
                                            $bookingDetails['Date'] = $lessonDate;
                                            $bookingDetails['StartTime'] = $startTime;
                                            $bookingDetails['EndTime'] = $endTime;
                                            $bookingDetails['BookingType'] = $bookingType;
                                            // debug_pr($bookingDetails);
                                            $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                                            $isRoomBookedAry[$_eventDateID] = !$availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                                            $errorAry[$_eventDateID] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
                                            // debug_pr($isRoomBookedAry);
                                            // debug_pr($errorAry);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }           // end booking record exist
                else {      // booking record not exist
                    //debug_pr('no booking'.$_eventDateID);
                    if (!empty($lessonDate) && $locationID) {
                        $bookingDetails['Date'] = $lessonDate;
                        $bookingDetails['StartTime'] = $startTime;
                        $bookingDetails['EndTime'] = $endTime;
                        $bookingDetails['BookingType'] = $bookingType;
                        $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                        $isRoomBookedAry[$_eventDateID] = !$availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                        $errorAry[$_eventDateID] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
                    }
                }
                
                // by pass if startTime == endTime == midnight
                if (!empty($lessonDate) && !($startTime == '00:00:00' && $endTime == '00:00:00')) {
                    $startDate = $lessonDate . ' ' . $startTime;
                    $endDate = $lessonDate . ' ' . $endTime;
                    $isThisTimeConflictAry = array();
                    
                    // check time conflict of instructor
                    if ($instructorAry[$_eventDateID][0]) {
                        $instructorAry[$_eventDateID][0] = array_unique((array)$instructorAry[$_eventDateID][0]);      // remove duplicate instructor
                        foreach((array)$instructorAry[$_eventDateID][0] as $__instructor) {
                            if ($__instructor) {       // filter out empty record
                                // check if it conflict with applied leave record
                                $isThisTimeConflictAry[] = $libenroll->checkTimeLimitSetting($__instructor, $startDate, $endDate);

                                // 2018-10-24: on client request: disable time conflict checking for the same instructor that need to monitor the lesson carrying on 
                                // for different classes in different locations (different sections on the same playground)
//                                 // check if it conflict with other lesson schedule record of himeself/herself
//                                 $isThisTimeConflictAry[] = $libenroll->checkSelfTimeConflict($__instructor, $startDate, $endDate, $_eventDateID);
                            }
                        }
                        
                        if (in_array(true, $isThisTimeConflictAry)) {
                            $timeConflictAry[$_eventDateID][] = array('102','');        // instructor time conflict
                        }
                        
                    }
                
//                     // check class conflict  <- disable after 2018-11-07
//                     $classIDAry = array_keys((array)$lessonClassAry[$_eventDateID]);
//                     if (count($classIDAry)) {
//                         $_timeConflictInfoAry = $libenroll->checkClassTimeConflict($startDate, $endDate, $classIDAry, $_eventDateID);
//                         if (count($_timeConflictInfoAry)) {
//                             $conflictClass = array();
//                             foreach((array)$_timeConflictInfoAry as $__timeConflictInfoAry) {
//                                 $conflictClass[] = $__timeConflictInfoAry['ClassName'];
//                             }
//                             $code = '202';
//                             $classes = implode(",",$conflictClass);
//                             $timeConflictAry[$_eventDateID][] = array($code, $classes);        // class time conflict
//                         }
//                     }
                    
                }   // not empty LessonDate
                
            }       // existing lesson
            else {  // new lesson
                foreach((array)$_startHourAry as $__rowID=>$__startHourAry) {
                    $__lessonDate = $_POST["LessonDate_0_{$__rowID}"];
                    $__startTime = str_pad($startHourAry[0][$__rowID], 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinuteAry[0][$__rowID], 2, "0", STR_PAD_LEFT) . ':00';
                    $__endTime = str_pad($endHourAry[0][$__rowID], 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinuteAry[0][$__rowID], 2, "0", STR_PAD_LEFT) . ':00';
                    $__locationID = $locationAry[0][$__rowID];
                    
                    if (!empty($__lessonDate) && $__locationID) {
                        unset($bookingDetails);
                        $bookingDetails['Date'] = $__lessonDate;
                        $bookingDetails['StartTime'] = $__startTime;
                        $bookingDetails['EndTime'] = $__endTime;
                        $bookingDetails['BookingType'] = $bookingType;
                        $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                        $isNewRoomBookedAry[$__rowID] = !$availableRoomAssoc[$__lessonDate][$__locationID]['BookAvailable'];
                        $newErrorAry[$__rowID] = $availableRoomAssoc[$__lessonDate][$__locationID]['ErrorCode'];
                    }
                    
                    // by pass if startTime == endTime == midnight
                    if (!empty($__lessonDate) && !($__startTime== '00:00:00' && $__endTime== '00:00:00')) {
                        $__startDate = $__lessonDate . ' ' . $__startTime;
                        $__endDate = $__lessonDate . ' ' . $__endTime;
                        $isThisTimeConflictAry = array();
                    
                        // check time conflict of instructor
                        if ($instructorAry[0][$__rowID]) {
                            $instructorAry[0][$__rowID] = array_unique((array)$instructorAry[0][$__rowID]);      // remove duplicate instructor
                            foreach((array)$instructorAry[0][$__rowID] as $___instructor) {
                                if ($___instructor) {       // filter out empty record
                                    // check if it conflict with applied leave record
                                    $isThisTimeConflictAry[] = $libenroll->checkTimeLimitSetting($___instructor, $__startDate, $__endDate);
    
                                    // 2018-10-24: on client request: disable time conflict checking for the same instructor that need to monitor the lesson carrying on
                                    // for different classes in different locations (different sections on the same playground)
//                                     // check if it conflict with other lesson schedule record of himeself/herself
//                                     $isThisTimeConflictAry[] = $libenroll->checkSelfTimeConflict($___instructor, $__startDate, $__endDate, $_eventDateID);
                                }
                            }
                            
                            if (in_array(true, $isThisTimeConflictAry)) {
                                $newTimeConflictAry[$__rowID][] = array('102','');        // instructor time conflict
                            }
                        }
                        
//                         // check class conflict <- disable after 2018-11-07
//                         $__classIDAry = array_keys((array)$newLessonClassAry[$__rowID]);
//                         if (count($__classIDAry)) {
//                             $__timeConflictInfoAry = $libenroll->checkClassTimeConflict($__startDate, $__endDate, $__classIDAry);
//                             if (count($__timeConflictInfoAry)) {
//                                 $conflictClass = array();
//                                 foreach((array)$__timeConflictInfoAry as $___timeConflictInfoAry) {
//                                     $conflictClass[] = $___timeConflictInfoAry['ClassName'];
//                                 }
//                                 $code = '202';
//                                 $classes = implode(",",$conflictClass);
//                                 $newTimeConflictAry[$__rowID][] = array($code, $classes);        // class time conflict
//                             }
//                         }
                        
                    }
                    
                }   // end loop for each new StartHour
                
            }       // end new lesson
        }           // end loop for each StartHour
        $x = $isRoomBookedAry;
        $y = $timeConflictAry;
// debug_pr($x);        
// debug_pr($errorAry);
        $json['isNewRoomBooked'] = $isNewRoomBookedAry;
        $json['newError'] = $newErrorAry;
        $json['newTimeConflict'] = $newTimeConflictAry;
        $json['success'] = true;
        
        break;
        
    case 'addLesson':
        $rowCounter = $_POST['rowCounter'];
        $rowNum = $_POST['rowNum'];
        $categoryID = $_POST['categoryID'];
        $capacity = $_POST['capacity'];
        $intakeStartDate = $_POST['intakeStartDate'];
        $intakeID = $_POST['intakeID'];
        $x = $linterface->getCourseScheduleRow($rowCounter, $rowNum, $categoryID, $capacity, $intakeStartDate, $intakeID);
        $json['success'] = true;
        break;
        
    // check if instructor(s) are allowed for the course category before add lesson schedule
    case 'checkIsAllowedInstructor':
        $courseID = $_POST['Course'];
        $instructorIDAry = $_POST['Instructor'];
        if (count($instructorIDAry)) {
            $instructorIDAry = array_remove_empty(array_unique((array)$instructorIDAry));      // remove duplicate instructor
            $x = $libenroll->checkIsAllowedInstructor($courseID,$instructorIDAry);
        }
        else {
            $x = true;
        }
        $json['success'] = true;
        break;
        
    case 'addCombinedLessonClassInstructor':
        $intakeID = IntegerSafe($_POST['IntakeID']);
        $courseID = IntegerSafe($_POST['CourseID']);
        $lessonID = IntegerSafe($_POST['LessonID']);
        $rowID = IntegerSafe($_POST['RowID']);
        $templateID = IntegerSafe($_POST['TemplateID']);
        
        $classAssoc = $libenroll->getIntakeClass($intakeID);
        $courseAry = $libenroll->getCourseTemplate($courseID, $templateID);
        if (count($courseAry)) {
            $categoryID = $courseAry[0]['CategoryID'];
        }
        
        $x = $linterface->getCombinedLessonTemplateInstructor($courseID, $lessonID, $rowID, $categoryID, $classAssoc);
        $json['success'] = true;
        break;

        
    // from weekly schedule        
    case 'checkeBooking4OneLessonUpdate':
        
        $eventDateID = $_POST['EventDateID'];
        $bookingIDAry = $_POST['BookingID'];
        $locationAry = $_POST['Location'];
        $startHourAry = $_POST['StartHour'];
        $startMinuteAry = $_POST['StartMinute'];
        $endHourAry = $_POST['EndHour'];
        $endMinuteAry = $_POST['EndMinute'];
        $instructorAry = $_POST['Instructor'];
        $lessonClassAry = $_POST['LessonClass'];
        
        $bookingType = 'room';
        $isRoomBooked = false;
        $errorAry = array();
        $timeConflictAry = array();
        
        $libebooking_api = new libebooking_api();
        
        if ($eventDateID > 0) {     // edit
            $lessonDate = $_POST["LessonDate_0_0"];
            $startTime = str_pad($startHourAry[0][0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinuteAry[0][0], 2, "0", STR_PAD_LEFT) . ':00';
            $endTime = str_pad($endHourAry[0][0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinuteAry[0][0], 2, "0", STR_PAD_LEFT) . ':00';
            $locationID = $locationAry[0][0];
            
            $bookingDetails = array();
            
            $enrolBookingRelationAry = $libenroll->GetEnrolEbookingRelation($eventDateID, ENROL_TYPE_ACTIVITY);
//debug_pr($enrolBookingRelationAry);            
            if (count($enrolBookingRelationAry)) {  // booking record exist
                $bookingID= $enrolBookingRelationAry['BookingRecordID'];
                
                $bookingRecordAry = $libebooking_api->Get_Booking_Record_By_BookingID($bookingID);
//debug_pr($bookingRecordAry);                
                if (count($bookingRecordAry)) {
                    $bookingRecordAry = current($bookingRecordAry);
                    $bookingDate = $bookingRecordAry['Date'];
                    $bookingStartTime = $bookingRecordAry['StartTime'];
                    $bookingEndTime = $bookingRecordAry['EndTime'];
                    
                    if ($bookingDate != $lessonDate) {  // date is changed, like new booking checking
                        if (!empty($lessonDate) && $locationID) {
                            $bookingDetails['Date'] = $lessonDate;
                            $bookingDetails['StartTime'] = $startTime;
                            $bookingDetails['EndTime'] = $endTime;
                            $bookingDetails['BookingType'] = $bookingType;
                            $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
//debug_pr($availableRoomAssoc);
                            $isRoomBooked = !$availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                            $errorAry[] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
                        }
                    }
                    else {  // same date, check time slot is changed
                        $outRangeTimeAry = $libenroll->getOutRangeTime($bookingStartTime, $bookingEndTime, $startTime, $endTime);
//debug_pr($outRangeTimeAry);
                        if (!empty($lessonDate) && $locationID) {
                            if (count($outRangeTimeAry)) {
                                $timeSlotAvailableAry = array();
                                $thisErrorAry = array();
                                foreach((array)$outRangeTimeAry as $timeSlot) {
                                    unset($bookingDetails);
                                    $bookingDetails['Date'] = $lessonDate;
                                    $bookingDetails['StartTime'] = $timeSlot['StartTime'];
                                    $bookingDetails['EndTime'] = $timeSlot['EndTime'];
                                    $bookingDetails['BookingType'] = $bookingType;
                                    // debug_pr($bookingDetails);
                                    $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                                    $timeSlotAvailableAry[] = $availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                                    $thisErrorAry[] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
//debug_pr($availableRoomAssoc);
//debug_pr($timeSlotAvailableAry);
                                }
                                $isRoomBooked = in_array(false,$timeSlotAvailableAry) ? true : false;
                                $errorAry[] = array_unique($thisErrorAry);
                            }
                            else {      // time is not changed or change to inbound timeslot
                                $eventDateAry = $libenroll->GET_ENROL_EVENT_DATE('', '', '', $eventDateID);
//debug_pr($eventDateAry);                                
                                if (count($eventDateAry)) {
                                    $originalLocationID = $eventDateAry[0]['LocationID'];
// print "orgLoc=$originalLocationID\n";
// print "loc=$locationID\n";
                                    if ($locationID != $originalLocationID) {       // location has changed
                                        unset($bookingDetails);
                                        $bookingDetails['Date'] = $lessonDate;
                                        $bookingDetails['StartTime'] = $startTime;
                                        $bookingDetails['EndTime'] = $endTime;
                                        $bookingDetails['BookingType'] = $bookingType;
// debug_pr($bookingDetails);
                                        $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                                        $isRoomBooked = !$availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                                        $errorAry[] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
// print "isRoomBooked=$isRoomBooked\n"; 
// debug_pr($errorAry);
                                    }
                                }                                
                            }
                        }
                    }
                }
                
            }           // end booking record exist
            else {      // booking record not exist
                //debug_pr('no booking'.$_eventDateID);
                if (!empty($lessonDate) && $locationID) {
                    $bookingDetails['Date'] = $lessonDate;
                    $bookingDetails['StartTime'] = $startTime;
                    $bookingDetails['EndTime'] = $endTime;
                    $bookingDetails['BookingType'] = $bookingType;
                    $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                    $isRoomBooked = !$availableRoomAssoc[$lessonDate][$locationID]['BookAvailable'];
                    $errorAry[] = $availableRoomAssoc[$lessonDate][$locationID]['ErrorCode'];
                }
            }
            
            // by pass if startTime == endTime == midnight
            if (!empty($lessonDate) && !($startTime == '00:00:00' && $endTime == '00:00:00')) {
                $startDate = $lessonDate . ' ' . $startTime;
                $endDate = $lessonDate . ' ' . $endTime;
                $isThisTimeConflictAry = array();
                
                // check time conflict of instructor
                if ($instructorAry[0][0]) {
                    $instructorAry[0][0] = array_unique((array)$instructorAry[0][0]);      // remove duplicate instructor
                    foreach((array)$instructorAry[0][0] as $__instructor) {
                        if ($__instructor) {       // filter out empty record
                            // check if it conflict with applied leave record
                            $isThisTimeConflictAry[] = $libenroll->checkTimeLimitSetting($__instructor, $startDate, $endDate);

                            // 2018-10-24: on client request: disable time conflict checking for the same instructor that need to monitor the lesson carrying on
                            // for different classes in different locations (different sections on the same playground)
//                             // check if it conflict with other lesson schedule record of himeself/herself
//                             $isThisTimeConflictAry[] = $libenroll->checkSelfTimeConflict($__instructor, $startDate, $endDate, $eventDateID);
// print ("instructor=$__instructor\n");                            
// print ("startDate=$startDate\n");
// print ("endDate=$endDate\n");
// debug_pr($isThisTimeConflictAry);                            
                        }
                    }
                    
                    if (in_array(true, $isThisTimeConflictAry)) {
                        $timeConflictAry[] = array('102','');        // instructor time conflict
                    }
                    
                }
                
//                 // check class conflict <- void after 2018-11-07
//                 $classIDAry = array_keys((array)$lessonClassAry[0]);
//                 if (count($classIDAry)) {
//                     $timeConflictInfoAry = $libenroll->checkClassTimeConflict($startDate, $endDate, $classIDAry, $eventDateID);
//                     if (count($timeConflictInfoAry)) {
//                         $conflictClass = array();
//                         foreach((array)$timeConflictInfoAry as $_timeConflictInfoAry) {
//                             $conflictClass[] = $_timeConflictInfoAry['ClassName'];
//                         }
//                         $code = '202';
//                         $classes = implode(",",$conflictClass);
//                         $timeConflictAry[] = array($code, $classes);        // class time conflict
//                     }
//                 }
                
            }   // not empty LessonDate
            
        }       // existing lesson
        else {  // new lesson
            $__lessonDate = $_POST["LessonDate_0_0"];
            $__startTime = str_pad($startHourAry[0][0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($startMinuteAry[0][0], 2, "0", STR_PAD_LEFT) . ':00';
            $__endTime = str_pad($endHourAry[0][0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($endMinuteAry[0][0], 2, "0", STR_PAD_LEFT) . ':00';
            $__locationID = $locationAry[0][0];

            if (!empty($__lessonDate) && $__locationID) {
                unset($bookingDetails);
                $bookingDetails['Date'] = $__lessonDate;
                $bookingDetails['StartTime'] = $__startTime;
                $bookingDetails['EndTime'] = $__endTime;
                $bookingDetails['BookingType'] = $bookingType;
                $availableRoomAssoc = $libebooking_api->GetAvailableRoom($bookingDetails);
                $isRoomBooked = !$availableRoomAssoc[$__lessonDate][$__locationID]['BookAvailable'];
                $errorAry[] = $availableRoomAssoc[$__lessonDate][$__locationID]['ErrorCode'];
            }
            
            // by pass if startTime == endTime == midnight
            if (!empty($__lessonDate) && !($__startTime== '00:00:00' && $__endTime== '00:00:00')) {
                $__startDate = $__lessonDate . ' ' . $__startTime;
                $__endDate = $__lessonDate . ' ' . $__endTime;
                $isThisTimeConflictAry = array();
                
                // check time conflict of instructor
                if ($instructorAry[0][0]) {
                    $instructorAry[0][0] = array_unique((array)$instructorAry[0][0]);      // remove duplicate instructor
                    foreach((array)$instructorAry[0][0] as $___instructor) {
                        if ($___instructor) {       // filter out empty record
                            // check if it conflict with applied leave record
                            $isThisTimeConflictAry[] = $libenroll->checkTimeLimitSetting($___instructor, $__startDate, $__endDate);
                            
                            // 2018-10-24: on client request: disable time conflict checking for the same instructor that need to monitor the lesson carrying on
                            // for different classes in different locations (different sections on the same playground)
//                             // check if it conflict with other lesson schedule record of himeself/herself
//                             $isThisTimeConflictAry[] = $libenroll->checkSelfTimeConflict($___instructor, $__startDate, $__endDate, 0);
                        }
                    }
                    
                    if (in_array(true, $isThisTimeConflictAry)) {
                        $timeConflictAry[] = array('102','');        // instructor time conflict
                    }
                }
                
//                 // check class conflict <- void after 2018-11-07
//                 $__classIDAry = array_keys((array)$lessonClassAry[0]);
//                 if (count($__classIDAry)) {
//                     $__timeConflictInfoAry = $libenroll->checkClassTimeConflict($__startDate, $__endDate, $__classIDAry);
//                     if (count($__timeConflictInfoAry)) {
//                         $conflictClass = array();
//                         foreach((array)$__timeConflictInfoAry as $___timeConflictInfoAry) {
//                             $conflictClass[] = $___timeConflictInfoAry['ClassName'];
//                         }
//                         $code = '202';
//                         $classes = implode(",",$conflictClass);
//                         $timeConflictAry[] = array($code, $classes);        // class time conflict
//                     }
//                 }
                
            }
                
        }       // end new lesson

        $x = $isRoomBooked;
        $y = $timeConflictAry;
        // debug_pr($x);
        // debug_pr($errorAry);
        $json['success'] = true;
        
        break;
        
}

//if ($remove_dummy_chars) {
//	$x = remove_dummy_chars_for_json($x);
//  $y = remove_dummy_chars_for_json($y);
//}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
}

$json['html'] = $x;
$json['html2'] = $y;
$json['error'] = $errorAry;
echo $ljson->encode($json);

intranet_closedb();
?>