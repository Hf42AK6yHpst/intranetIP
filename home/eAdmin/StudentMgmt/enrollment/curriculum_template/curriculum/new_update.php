<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-06 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
 
$result = array();
$dataAry = array();
$dataAry['TitleEnglish'] = standardizeFormPostValue($_POST['TitleEnglish']);
$dataAry['TitleChinese'] = standardizeFormPostValue($_POST['TitleChinese']);
$dataAry['Description'] = standardizeFormPostValue($_POST['Description']);
$dataAry['DateInput'] = 'now()'; 
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_CURRICULUM_TEMPLATE',$dataAry,array(),false);

$libenroll->Start_Trans();

$res = $libenroll->db_db_query($sql);
$result[] = $res;
if ($res) {
    $templateID = $libenroll->db_insert_id();
}

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>