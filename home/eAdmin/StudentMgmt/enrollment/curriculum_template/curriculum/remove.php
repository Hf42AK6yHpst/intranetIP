<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-06 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
 
$result = array();
$TemplateID = IntegerSafe($TemplateID);

if (count($TemplateID) > 0) {
    $sql = "SELECT CourseID FROM INTRANET_ENROL_CURRICULUM_TEMPLATE_COURSE WHERE TemplateID IN ('".implode("','",(array)$TemplateID)."')";
    $courseIDAry = $libenroll->returnVector($sql);
    
    $libenroll->Start_Trans();

    $sql = "DELETE FROM INTRANET_ENROL_CURRICULUM_TEMPLATE WHERE TemplateID IN ('".implode("','",(array)$TemplateID)."')";
    $result[] = $libenroll->db_db_query($sql);
    
    $sql = "DELETE FROM INTRANET_ENROL_CURRICULUM_TEMPLATE_COURSE WHERE TemplateID IN ('".implode("','",(array)$TemplateID)."')";
    $result[] = $libenroll->db_db_query($sql);

    if (count($courseIDAry)) {
        $sql = "DELETE FROM INTRANET_ENROL_CURRICULUM_TEMPLATE_LESSON WHERE CourseID IN ('".implode("','",(array)$courseIDAry)."')";
        $result[] = $libenroll->db_db_query($sql);
    }
}

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'DeleteSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'DeleteUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>