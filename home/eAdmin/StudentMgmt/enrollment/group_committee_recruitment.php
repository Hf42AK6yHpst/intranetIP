<?php
// Using: anna

/******************************************
 * Modification Log:
 * 
 * 2018-04-30 Anna
 * Create this page
 * 
 ******************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}

intranet_auth();
intranet_opendb();


if(!$plugin['eEnrollment'] || !$sys_custom['eEnrolment']['CommitteeRecruitment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_sel_category", "sel_category");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Semester", "Semester");
$arrCookies[] = array("ck_eEnrol_Management_Club_Record_Round", "Round"); 

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$pic_view = '';
}
else 
	updateGetCookies($arrCookies);

$libenroll = new libclubsenrol();
if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = $libenroll->get_Next_Academic_Year_ID();
	$AcademicYearID = $AcademicYearID ==''?Get_Current_Academic_Year_ID():$AcademicYearID;  
//	$AcademicYearID = Get_Current_Academic_Year_ID();

$sel_category = isset($sel_category)? $sel_category : '';
$Semester = isset($Semester) && $Semester != ''? $Semester : 0;
$Round = isset($Round) && $Round != ''? $Round : '';

$AcademicYearID = IntegerSafe($AcademicYearID);
$Semester = IntegerSafe($Semester);
$sel_category = IntegerSafe($sel_category);


if ($plugin['eEnrollment'])
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

	$haveClubMgmt = $libenroll->HAVE_CLUB_MGT();
	
	$isClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID']);

// 	if ( ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT) && !($isEnrolAdmin) && !($isEnrolMaster))
// 		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
		
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;

	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	

	if ($libenroll->hasAccessRight($_SESSION['UserID'],'Club_Admin'))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        # tags 
        $tab_type = "club";
        $current_tab = 3;
        include_once("management_tabs.php");
        
        $yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.Semester.selectedIndex=0;document.form1.action=\'group_committee_recruitment.php\';document.form1.submit();"', 1, 0, $AcademicYearID);
        
        # category filter
        $category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "document.form1.action='group_committee_recruitment.php'; document.form1.submit()", $eEnrollment['all_categories']);

		$SemesterFilter = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, $OnChange="document.form1.action='group_committee_recruitment.php'; document.form1.submit()", $NoFirst=1, $NoPastTerm=0, $withWholeYear=1);
	

        $linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
        
		# Order
		$OrderBy = $OrderBy ? $OrderBy : "ClubCode";
		$OrderDesc = $OrderDesc ? $OrderDesc : "asc";
		$OrderIcon = "<image src='{$image_path}/{$LAYOUT_SKIN}/icon_sort_". substr($OrderDesc,0,1) ."_off.gif' border=0 align='absmiddle'>";

        $groups = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category, stripslashes($keyword), array($Semester), $OrderBy, $OrderDesc, $ApplyUserType='');
       
        if ($isEnrolAdmin || $isEnrolMaster || $isClubPIC)
        {
			$toolsBar = "
				<div class=\"common_table_tool\">
				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitForm(document.form1,'group_update.php?AcademicYearID=$AcademicYearID&type=enable','enable')")  ."\" class=\"tool_approve\">". $eEnrollment['enable_online_registration'] ."</a>
				<a href=\"". ($libenroll->disableUpdate==1 ? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitForm(document.form1,'group_update.php?AcademicYearID=$AcademicYearID&type=disable','disable')")  ."\" class=\"tool_reject\">". $eEnrollment['disable_online_registration'] ."</a>
				</div>
			";
			$menuBar = $linterface->GET_LNK_EXPORT("javascript:checkNew('export_club_committee_recruitment_member.php?AcademicYearID=$AcademicYearID&Semester=$Semester')", '', '', '', '', 0);

        }
      
//         # System message		
// 		if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
// 		if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
// 		if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="javascript">
function submitForm(obj,page,action)
{
	if(countChecked(obj,'EnrolGroupID[]')==0)
    	alert(globalAlertMsg2);
    else{
	    if(action == 'enable')
	    { 
	    	if(confirm(globalAlertMsgEnable)){
	            obj.action = page;
	            obj.method = "POST";
	            obj.submit();
	            return;
            }
    	}
    	else if(action == 'disable')
	    { 
	    	if(confirm(globalAlertMsgDisable)){
	            obj.action = page;
	            obj.method = "POST";
	            obj.submit();
	            return;
            }
    	}
    }
}
function click_order(o, d)
{
	document.form1.OrderBy.value = o;
	document.form1.OrderDesc.value = d;	
	document.form1.submit();
}
</script>

<form name="form1" id="form1" method="post">
    
    <div class="content_top_tool">
    	<div class="Conntent_tool"><?=$menuBar?></div>
    	<br style="clear:both" />
    </div>

    <div class="table_board">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            	<td valign="bottom">
            		<div class="table_filter">
            			<?=$yearFilter?>
            			<?= $SemesterFilter ?>
            			<?= $category_selection ?>
            		</div> 
            	</td>
            	<?if($libenroll->AllowToEditPreviousYearData) {?>
            	<td valign="bottom"><?=$toolsBar?></td>
            	<?}?>
            </tr>
        </table>
    </div>

    <table class="common_table_list">
    <thead>
    	<tr>
    		<th class="num_check">#</th>
    		<th><b><a href="javascript:click_order('ClubCode','<?= $OrderBy=="ClubCode" ? ($OrderDesc=="desc"?"asc":"desc") : "asc" ?>')"><?=$Lang['eEnrolment']['ClubCode']?><?= $OrderBy=="ClubCode" ? $OrderIcon : "" ?></a></b></th>
    		<th><b><a href="javascript:click_order('ClubName','<?= $OrderBy=="ClubName" ? ($OrderDesc=="desc"?"asc":"desc") : "asc" ?>')"><?=$i_ClubsEnrollment_ClubName?><?= $OrderBy=="ClubName" ? $OrderIcon : "" ?></a></b></th>
    		<th><?php echo $Lang['General']['Term'];?></th>
    
    		<th><?=$eEnrollment['add_activity']['act_category']?></th>
    		<th><?=$i_ClubsEnrollment_OnlineRegistration?></th>
    		<th><?=$eEnrollment['member']?></th>
    
    		<? if($isEnrolAdmin||$isEnrolMaster){ ?>
    			<th class="num_check"><input type="checkbox" onClick="(this.checked)?setChecked(1,document.form1,'EnrolGroupID[]'):setChecked(0,document.form1,'EnrolGroupID[]')"></th>
    		<?}?>
    	</tr>
    </thead>
    
    	<?php
    	$displayedRecord = 0;
    	$numOfClub = count($groups);
    	
    	for ($i=0; $i<$numOfClub; $i++)
    	{
    		 $thisClubInfoArr = $groups[$i];
    		 $EnrolGroupID = $groups[$i]['EnrolGroupID'];
    
    	     $this_TitleDisplay = Get_Lang_Selection($groups[$i]['TitleChinese'],$groups[$i]['Title']);
    	     
    	     $CommitteeRecrutmentStudentNum=  $libenroll->Get_Committee_Club_MemberCount($EnrolGroupID);
    	 
    	     # Allow Online Registration
    	     $thisRecordStatus = $groups[$i]['UseOnlineRegistration'];
    	     $isAllowOnlineRegistration = ($thisRecordStatus == 1)? true : false;
    	     $chkAllow = ($isAllowOnlineRegistration)? 'CHECKED' : '';
    	     
    	     # Semester Display
    	     $SemesterDisplay = $groups[$i]['YearTermName'];
    
    	     # Category Display
    	     $CategoryDisplay = ($groups[$i]['CategoryName']=="") ? "-" : $groups[$i]['CategoryName'];   	 
    	     
    	     if (($isClubPIC)||($isEnrolAdmin)||($isEnrolMaster)){
    				
    			$displayedRecord++;
    	  ?>
    		     <tr class="tablerow<?= ((($displayedRecord-1) % 2) + 1)?>">
    		     	 <td class="tabletext"> <?=$displayedRecord?> </td>
    		     	 <td class="tabletext"> <?=($groups[$i]['GroupCode']?$groups[$i]['GroupCode']:'-')?> </td>
    		     	 <!--Club Name -->
    			     <td class="tabletext"><?=$this_TitleDisplay?></td>
    	     
    			     <!--Semester -->
    			     <td class="tabletext"><?=$SemesterDisplay?></td>
    
    			     
    			     <!--Category -->
    			     <td class="tabletext"><?=$CategoryDisplay?></td>			     
    			     
    			     <!--Online Registration -->
    			     	<td align="center">
    				     <? if ($chkAllow == "CHECKED") { ?>
    				     	<?/*
    					     <a class="tablelink" href="group_new1a.php?GroupID=<?=$id?>">
    					     <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icons_manage.gif" alt="<?= $eEnrollment['club_setting']?>" border="0">
    					     </a>
    					     */
    					    ?>
    					     <img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_tick_green.gif" width="18" height="18" border="0" align="absmiddle">
    				     <? } else { ?>
    				     	<!-- <?= $i_general_disabled ?> -->
    				     	<img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_delete_b.gif" width="18" height="18" border="0" align="absmiddle">
    				     <? } ?>
    				     </td>
    
    			     
     
    			     
    			     <!--Member -->
    			     <td align="center" class="tabletext">
    			     		<a class="tablelink" href="committee_member_index.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$EnrolGroupID?>&Semester=<?=$Semester?>">		     	
    			     		<?= $CommitteeRecrutmentStudentNum?>		
    			     		</a>
    			     </td>
    
    			     <!--Checkbox -->
    			     <td align="center" valign="top">
    				     <input type="checkbox" name="EnrolGroupID[]" value="<?=$EnrolGroupID?>">
    			     </td>
    			     
    
    			     
    		     </tr>
    	     <?
         	}
    	}
    	if ($displayedRecord == 0)
    	{
    		print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"10\">".$i_no_record_exists_msg."</td></tr>";
    	}
    	?>

    </table>
    
    <input type="hidden" name="FromPage" value="committee">
    <input type="hidden" name="OrderBy" value="<?=$OrderBy?>">
    <input type="hidden" name="OrderDesc" value="<?=$OrderDesc?>">
	
</form>

<?
        $linterface->LAYOUT_STOP();
       
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>