<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$le = new libclubsenrol();	
	$CurrentPage = "PageCategorySetting";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $le->GET_MODULE_OBJ_ARR();
		
	$lc = new libclubsenrol();
	if (!$lc->IS_ENROL_ADMIN($_SESSION['UserID']))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
    if ($le->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['cat_setting'], "", 1);

        $linterface->LAYOUT_START();


$CategoryList = $le->GET_CATEGORY_LIST();

//debug_r($CategoryList);
$sql = "
		SELECT
				CONCAT('<a href=\"category_new.php?CategoryID=', CategoryID ,'\" class=\"tablelink\">', CategoryName, '</a>'),
				DisplayOrder, 
				CONCAT('<input type=checkbox name=\"CategoryID[]\" value=\"', CategoryID ,'\">')
		FROM
				INTRANET_ENROL_CATEGORY
		";


               
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CategoryName", "DisplayOrder");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width=50% class='tabletoplink'>".$li->column($pos++, $eEnrollmentMenu['cat_title'])."</td>\n";
$li->column_list .= "<td width=50% class='tabletoplink'>".$li->column($pos++, $i_general_DisplayOrder)."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("CategoryID[]")."</td>\n";

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="javascript">

function checkform(obj){
        return true;
}

function checkAll(obj,val)
{
         <?
         for ($i=0; $i<sizeof($CategoryList); $i++)
         {
         ?>
         obj.Allow<?=$i?>.checked=val;
         <?
         }
         ?>
}
</script>


<br/>
<form name="form1" method="get">

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td align="left"> </td>
	<td align="right"><?=$SysMsg?></td>
</tr>
<tr>
	<td align="left"><img src="<?= $image_path?>/2009a/icon_new.gif" align="absmiddle"> <a href="category_new.php" class="contenttool"><?= $button_new?></a></span></td>
	<td align="right">
	<?// $functionbar; ?>
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
	  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif"><table border="0" cellspacing="0" cellpadding="2">
	      <tr>
	        <td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'CategoryID[]','category_remove.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle">
	          <?=$Lang['Btn']['Delete']?></a></td>
	      </tr>
	    </table></td>
	  <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
	</tr>
	</table>

	</td>
</tr>
</table>
<?=$li->display("96%")?>

<br />
<br />








<!--
<table width="96%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td class="tablegreentop tabletoplink"><?=$eEnrollmentMenu['cat_title']?></td>
<td class="tablegreentop tabletoplink">&nbsp;</td>
</tr>
<?php
for ($i=0; $i<sizeof($CategoryList); $i++)
{
     $id = $CategoryList[$i][0];
     ?>
     <tr class="tablegreenrow<?= (($i % 2) + 1)?>">
     <td class="tabletext"><a href="category_new.php?CategoryID=<?=$CategoryList[$i][0]?>" class="tablelink"><?=$CategoryList[$i][1]?></a></td>  
     <td align="right"><?= $linterface->GET_ACTION_BTN($button_remove, "button", "document.getElementById('CategoryID').value='".$CategoryList[$i][0]."'; AlertPost (this.form, 'category_remove.php' , '".$eEnrollment['js_confirm_del']."')"); ?></td>
     </tr>
     <?
}

if (sizeof($CategoryList) == 0) {
?>
	 <tr class="tablegreenrow2">
     <td class="tabletext" colspan="2" align="center"><?= $eEnrollment['no_record']?></td>
     </tr>
<?	
}
?>
</table>
-->
<!--<input type="hidden" id="CategoryID" name="CategoryID" value="" />-->
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>