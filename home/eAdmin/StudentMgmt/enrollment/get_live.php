<?php
#Modify : 
#Based on IntranetIP25/StudentMgmt/disciplinev12/reports/goodconduct_misconduct/get_live.php
/*
 * 2014-04-17 (Carlos): Modified $target=="student2ndLayer", support multiple YearClassID and order by ClassName,ClassNumber
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

function getLevelArray($ParCurrentAcademicYearId) {
	$libdb = new libdb();
	$sql = "
			SELECT DISTINCT
				Y.YEARID AS CLASSLEVELID,
				Y.YEARNAME AS LEVELNAME
			FROM YEAR Y
			INNER JOIN YEAR_CLASS YC ON Y.YEARID = YC.YEARID
			INNER JOIN ACADEMIC_YEAR AY ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
			WHERE AY.ACADEMICYEARID = '$ParCurrentAcademicYearId'
			ORDER BY Y.SEQUENCE, YC.SEQUENCE
		";
		$returnArray = $libdb->returnArray($sql) or die(mysql_error());
	return $returnArray;
}

$RTDetail = explode(",", $rankTargetDetail);

if($ownClassOnly) {
	$libenroll = new libclubsenrol();
	$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);	
	for($i=0; $i<count($classInfo); $i++) {
		list($classId, $classNameEn, $classNameB5, $yearLevelId) = $classInfo[$i];
		$OwnYearLevelAry[] = $yearLevelId;
		$OwnClassAry[] = $classId;	
	}
}


if($target=='form') {
	$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='7' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;} \">";
	$ClassLvlArr = getLevelArray($year);

	$SelectedFormTextArr = explode(",", $SelectedFormText);

	$isEmpty = $ldiscipline->checkIsEmptyArray($RTDetail);
	
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		if($ownClassOnly) {
			if(!in_array($ClassLvlArr[$i][0], $OwnYearLevelAry)) 
				continue;
		}
		$temp .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
		$temp .= (in_array($ClassLvlArr[$i][0], $RTDetail) || $isEmpty) ? " selected" : "";
		$temp .= ">".$ClassLvlArr[$i][1]."</option>\n";
	}
	$temp .= "</select>\n";
}

if($target=="class") {
	$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='7' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;}\">";
	$classResult = $ldiscipline->getRankClassList("",$year);
	$isEmpty = $ldiscipline->checkIsEmptyArray($RTDetail);
	
	for($k=0;$k<sizeof($classResult);$k++) {
		if($ownClassOnly) {
			if(!in_array($classResult[$k][1], $OwnClassAry)) 
				continue;
		}
		$temp .= "<option value='{$classResult[$k][1]}'";
		for($j=0;$j<sizeof($RTDetail);$j++) {
			$temp .= ($RTDetail[$j]==$classResult[$k][1] || $isEmpty) ? " selected" : "";	
		}
		$temp .= ">{$classResult[$k][0]}</option>";
	}
	$temp .= "</select>";
	
}

if($target=="student") {
	
	$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='7' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;} \">";
	$classResult = $ldiscipline->getRankClassList("", $year);
			
	for($k=0;$k<sizeof($classResult);$k++) {
		if($ownClassOnly) {
			if(!in_array($classResult[$k][1], $OwnClassAry)) 
				continue;
		}
		$temp .= "<option value='{$classResult[$k][1]}'";
		for($j=0;$j<sizeof($RTDetail);$j++) {
			$temp .= ($RTDetail[$j]==$classResult[$k][1]) ? " selected" : "";	
		}
		$temp .= ">{$classResult[$k][0]}</option>";
	}
	$temp .= "</select>";
}

$name_field = getNameFieldByLang("USR.");
if($target=="student2ndLayer") {
	
	$studentIDAry = explode(',',$studentid);
	$temp .= "<select name='studentID[]' id='studentID[]' class='formtextbox' multiple size='7'>";
	if($value != "") {
		$valueAry = explode(",",$value);
		$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
		//$sql = "SELECT ClassName, ClassNumber, $name_field, UserID FROM INTRANET_USER WHERE ClassName='$value' AND RecordType=2 ORDER BY ClassNumber";
		$sql = "SELECT $clsName, ycu.ClassNumber, $name_field, USR.UserID FROM INTRANET_USER USR
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) 
				LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
				WHERE ".(count($valueAry)>0?" yc.YearClassID IN ($value) " : " yc.YearClassID='$value' ")." AND USR.RecordType=2 
				ORDER BY USR.ClassName,ycu.ClassNumber+0
				";
		
		$studentResult = $ldiscipline->returnArray($sql, 3);

		for($k=0;$k<sizeof($studentResult);$k++) {
			$temp .= "<option value={$studentResult[$k][3]}";
			for($j=0;$j<sizeof($RTDetail);$j++) {
				$temp .= (in_array($studentResult[$k][3], $studentIDAry)) ? " selected" : "";
			}
			//$temp .= ">{$studentResult[$k][0]}-{$studentResult[$k][1]} {$studentResult[$k][2]}</option>";
			$temp .= ">{$studentResult[$k][1]} {$studentResult[$k][2]}</option>";
		}
	}
	$temp .= "</select>";
	
	//$temp .= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(document.getElementById('studentID[]'), true);return false;");
}	




echo $temp;

intranet_closedb();

?>