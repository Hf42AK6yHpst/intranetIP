<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$libenroll = new libclubsenrol();
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");

if ($type=="club")
{
	//get all groups and members of that group
	$groups = $libenroll->returnAllGroupMemberList();

	//get all groups and members of that group
	$header .= "<table width='90%' border='0' cellpadding='2' cellspacing='0' class='eSporttableborder'>\n";
	$header .= "<tr class='eSporttdborder eSportprinttabletitle'>
	<td width=10 class='eSporttdborder eSportprinttabletitle'>#</td>
	<td class='eSporttdborder eSportprinttabletitle'>$i_UserClassName</td>
	<td class='eSporttdborder eSportprinttabletitle'>$i_UserClassNumber </td>
	<td class='eSporttdborder eSportprinttabletitle'>$i_UserStudentName </td>
	<td class='eSporttdborder eSportprinttabletitle'>$i_UserLogin </td>
	</tr>\n";
	
	for ($i=0; $i<sizeof($groups); $i++)
	{
	     list($gid,$name,$quota,$members) = $groups[$i];
	     if ($quota==0) $quota = $i_ClubsEnrollment_NoQuota;
	     $size = sizeof($members);
		 
		 $x .= "<table border='0' cellpadding='0' cellspacing='0' align='center' width='90%'>\n";
		 $x .= "<tr><td>";
		 $x .= "<table border='0' cellpadding='0' cellspacing='0' align='left'>\n";
	     $x .= "<tr class='tabletext'align='left'><td>$i_ClubsEnrollment_GroupName</td><td width='10'></td><td>$name</td></tr>\n";
	     $x .= "<tr class='tabletext'align='left'><td>$i_ClubsEnrollment_Quota</td><td width='10'></td><td>$quota</td></tr>\n";
	     $x .= "<tr class='tabletext'align='left'><td>$i_ClubsEnrollment_CurrentSize</td><td width='10'></td><td>$size</td></tr>\n";
	     $x .= "</table>\n";
	     $x .= "</td></tr>";
		 $x .= "</table>\n";
		 
	     $x .= "$header";
	     for ($j=0; $j<sizeof($members); $j++)
	     {
	          $count = $j+1;
	          $css = (($j%2)+1);
	          list($gid,$class,$classnumber,$name,$login) = $members[$j];
	          $x .= "<tr class='eSporttdborder eSportprinttext'>
	          			<td width=10 class='eSporttdborder eSportprinttext'>{$count}&nbsp</td>
	          			<td class='eSporttdborder eSportprinttext'>{$class}&nbsp</td>
	          			<td class='eSporttdborder eSportprinttext'>{$classnumber}&nbsp</td>
	          			<td class='eSporttdborder eSportprinttext'>{$name}&nbsp</td>
	          			<td class='eSporttdborder eSportprinttext'>{$login}&nbsp</td>
	          		</tr>\n";
	     }
	     if (sizeof($members)==0)
	     {
	         $x .= "<tr class='eSporttdborder eSportprinttext'><td colspan='5' align='center' class='eSporttdborder eSportprinttext'>$i_ClubsEnrollment_NoMember</td></tr>\n";
	     }
	     $x .= "</table>\n<br>\n";
	
	}
}
	


################################################################

?>
<table width='100%' align='center' class='print_hide' border=0>
<tr>
	<td align='right'><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<?=$x?>
<br>

<?
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();

?>