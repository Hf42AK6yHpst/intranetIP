<?php
# using: 

################ Change Log [Start] 
#
#	Date:	2014-10-14 Pun
#			Add round report for SIS customization
#
#	Date:	2014-07-22 Bill
#			add DirectToMeeting to continue the process
#
#	Date:	2010-12-14 YatWoon
#			add "Club name (Chinese)"
#
###################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/liborganization.php");

$li = new libdb();

$Title = intranet_htmlspecialchars(trim($Title));
$TitleChinese = intranet_htmlspecialchars(trim($TitleChinese));
$Description = intranet_htmlspecialchars(trim($Description));
$Quota = intranet_htmlspecialchars(trim($Quota));
if (!is_numeric($Quota) || $Quota < 0) $Quota = 5;

/* 20100316 Ivan: Default allow all access right
if ($alltools == 1)
{
    $functionAccess = 'NULL';
}
else
{
    # Change the tool rights to integer
    $sum = 0;
    for ($i=0; $i<sizeof($grouptools); $i++)
    {
         $target = intval($grouptools[$i]);
         if ($target == 5)        # Question bank is for academic only
         {
             if ($RecordType == 2)
             {
                 $sum += pow(2,$target);
             }
         }
         else
         {
             $sum += pow(2,$target);
         }
    }
    $functionAccess = $sum;
}
*/
$functionAccess = 'NULL';

$fieldname = "Title, TitleChinese, Description, RecordType, DateInput, DateModified, StorageQuota, FunctionAccess, AcademicYearID, GroupCode";
$fieldvalue = "'$Title', '$TitleChinese', '$Description', '$RecordType', now(), now(), $Quota, $functionAccess, '$AcademicYearID', '$ClubCode'";
$AnnounceAllowed = IntegerSafe($AnnounceAllowed);
if ($AnnounceAllowed == 1)
{
    $fieldname .= ",AnnounceAllowed";
    $fieldvalue .= ",1";
}
$sql = "INSERT INTO INTRANET_GROUP ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);
$GroupID = $li->db_insert_id();

# Match with INTRANET_CLASS
if ($RecordType == 3)
{
    $sql = "UPDATE INTRANET_CLASS SET GroupID = '$GroupID' WHERE ClassName = '$Title'";
    $li->db_db_query($sql);
}

# INTRANET_ORPAGE_GROUP
$lorg = new liborganization();
if ($hide==1)
{
    $lorg->setGroupHidden($GroupID);
}

### Insert Enrolment Club Main Info
switch($ClubType){
	case 'YearBase':
		$ClubTypeForDB = 'Y';
		break;
	case 'SemesterBase':
		$ClubTypeForDB = 'S';
		break;
	case 'RoundBase':
		$ClubTypeForDB = 'Y';
		break;
}
$ApplyOnceOnly = ($ApplyOnceOnly == 1)? 1 : 0;			// For Semester-Based Club
$FirstSemEnrolOnly = ($FirstSemEnrolOnly == 1)? 1 : 0;	// For Year-Based Club
$ApplyUserTypeForDB = $ApplyUserType;	// S - Student, P - Parent
 

$DataArr = array();
$DataArr['GroupID'] = $GroupID;
$DataArr['ClubType'] = $ClubTypeForDB;
$DataArr['ApplyOnceOnly'] = $ApplyOnceOnly;
$DataArr['FirstSemEnrolOnly'] = $FirstSemEnrolOnly;
$SuccessArr['Insert_Group_Main_Info'] = $libenroll->Insert_Group_Main_Info($DataArr);

		
### Create Enrolment Club Record
if ($ClubType == 'YearBase')
{
	$SuccessArr['Insert_Year_Based_Club'] = $libenroll->Insert_Year_Based_Club($GroupID);
	$EnrolGroupID =  mysql_insert_id();
}
else if ($ClubType == 'SemesterBase')
{
	$SemesterArr = $_REQUEST['Sem'];
	$numOfSemester = count($SemesterArr);
	
	for ($i=0; $i<$numOfSemester; $i++)
	{
		$thisSemester = $SemesterArr[$i];
		$SuccessArr['Insert_Semester_Based_Club'] = $libenroll->Insert_Semester_Based_Club($GroupID, $thisSemester);
		$EnrolGroupIDArr[$thisSemester] = mysql_insert_id();
	}
	$EnrolGroupID = $EnrolGroupIDArr[$SemesterArr[0]]; // go to the first sem
}
else if ( ($plugin['SIS_eEnrollment']) && ($ClubType == 'RoundBase') )
{
	$Round = IntegerSafe($Round);
	$SuccessArr['Insert_Round_Based_Club'] = $libenroll->Insert_Round_Based_Club($GroupID, $Round);
	$EnrolGroupID = mysql_insert_id();
}


intranet_closedb();
if($DirectToSetting)
	header("Location: group_setting.php?AcademicYearID=$AcademicYearID&Semester=$Semester&Round=$Round&EnrolGroupID=$EnrolGroupID&DirectToMeeting=1");
else
	header("Location: group.php?AcademicYearID=$AcademicYearID&Semester=$Semester&Round=$Round&xmsg=AddSuccess");
?>