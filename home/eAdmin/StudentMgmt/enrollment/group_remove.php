<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$libenroll = new libclubsenrol();

if (is_array($EnrolGroupID))
{
	$EnrolGroupIDArr = $EnrolGroupID;
}
else
{
	$EnrolGroupIDArr = array($EnrolGroupID);
}
$EnrolGroupIDList = implode(',', $EnrolGroupIDArr);
$numEnrolGroupID = count($EnrolGroupIDArr);

$DeleteGroupIDArr = array();
for ($i=0; $i<$numEnrolGroupID; $i++)
{
	$thisEnrolGroupID = $EnrolGroupIDArr[$i];
	$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
	
	$SuccessArr['Delete_' + $thisEnrolGroupID] = $libenroll->Delete_Club_Data($thisEnrolGroupID);
	
	if ($SuccessArr['Delete_' + $thisEnrolGroupID])
	{
		# if no club is linked to the group anymore, add the group to the delete list
		$GroupClubInfoArr = $libenroll->Get_Club_Info_By_GroupID($thisGroupID);
		
		if (!is_array($GroupClubInfoArr) || count($GroupClubInfoArr)==0)
			$DeleteGroupIDArr[] = $thisGroupID;
	}
	
	if($plugin['iPortfolio']) {
		# Delete Default OLE Setting
		$libenroll->Delete_Enrolment_Default_OLE_Setting('club', $thisEnrolGroupID);
	}
}

if (count($DeleteGroupIDArr) > 0)
{
	$DeleteGroupIDList = implode(',', $DeleteGroupIDArr);
	$sql = "DELETE FROM INTRANET_GROUP WHERE GroupID IN (".$DeleteGroupIDList.")";
	
	$SuccessArr['DeleteGroup' + $DeleteGroupIDList] = $li->db_db_query($sql);
}




#$sql = "DELETE FROM INTRANET_GROUPTIMETABLE WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPRESOURCE WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPPOLLING WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);

/*

if(isset($gid))
{
	$GroupID = $gid;
}
$list = implode(",",$GroupID);


for ($i = 0; $i < sizeof($GroupID); $i++) {
	# update student approved enrolment number
	$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$GroupID[$i]."'";
	$memberArr = $li->returnVector($sql);			
	$sql = "SELECT StudentID FROM INTRANET_ENROL_GROUPSTUDENT WHERE GroupID = '".$GroupID[$i]."'";
	$enrolArr = $li->returnVector($sql);
	$importedStu = array();		
	$nonImportedStu = array();		
	$importedStu = array_diff($memberArr, $enrolArr);
	$nonImportedStu = array_diff($memberArr, $importedStu);	
	
	# if not imported student, decrease the approved number by 1
	$sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved = Approved - 1 WHERE StudentID IN (".implode(",", $nonImportedStu).")";
	$li->db_db_query($sql);		
	
	$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID[$i]);
	$sql = "DELETE FROM INTRANET_ENROL_GROUPCLASSLEVEL WHERE EnrolGroupID = '$EnrolGroupID'";
	$li->db_db_query($sql);
	$sql = "DELETE FROM INTRANET_ENROL_GROUPSTAFF WHERE EnrolGroupID = '$EnrolGroupID'";
	$li->db_db_query($sql);
	$sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT WHERE GroupID = '".$GroupID[$i]."'";
	$li->db_db_query($sql);
	$sql = "DELETE FROM INTRANET_ENROL_GROUP_DATE WHERE EnrolGroupID = '$EnrolGroupID'";
	$li->db_db_query($sql);
}

# delete the club from INTRANET_USERGROUP
$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID IN (".implode(",", $GroupID).")";
$li->db_db_query($sql);
# delete the group from INTRANET_GROUP
$sql = "DELETE FROM INTRANET_GROUP WHERE GroupID IN (".implode(",", $GroupID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_ENROL_GROUPINFO WHERE GroupID IN (".implode(",", $GroupID).")";
$li->db_db_query($sql);
//Remove from archieve table
$sql = "DELETE FROM INTRANET_ARCHIVE_ENROL_GROUPINFO WHERE GroupID IN (".implode(",", $GroupID).")";
$li->db_db_query($sql);
$sql = "DELETE FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE GroupID IN (".implode(",", $GroupID).")";
$li->db_db_query($sql);

*/




# Set Class's GroupID to NULL
#$sql = "UPDATE INTRANET_CLASS SET GroupID = NULL WHERE GroupID IN ($list)";
#$li->db_db_query($sql);
$libclass = new libclass();
$libclass->fixClassGroupRelation();



intranet_closedb();
header("Location: group.php?AcademicYearID=$AcademicYearID&Semester=$Semester&xmsg=DeleteSuccess");

?>