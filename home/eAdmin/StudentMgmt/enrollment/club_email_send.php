<?php
## Using By : yat
###########################################
##	Modification Log:
##
##	2012-01-18	YatWoon
##	- display student name in the notification email
##
##	2011-05-06	YatWoon
##	- update Email noticiation wordings (not hardcode!)
##
##	2010-02-25 Max (201002251050)
##	- no email send for no club enrolled

##	2010-01-27 Max (201001261722)
##	Move functions to libclubsenrol.php
##	-[getUser()]
##	-[getUserByYearId()]
##	-[getUserByYearClassId()]
##	-[getUserByUserId()]
##	-[getTargetUsers()]
##	-[AddClubsDetailToTargetUsers()]
##	-[getUserClub()]
##	-[composeEmailContent()]
##	-[composeEmailList()]

##	2010-01-08 Max (201001081039)
##	Send Email for Enrollment Result
###########################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT.'includes/libwebmail.php');
			

intranet_auth();
intranet_opendb();

$rankTarget = $_POST['rankTarget'];
$currentYear = $_POST['currentYear'];
$sendWithParent = $_POST['sendWithParent'];

$libclubsenrol = new libclubsenrol();
$lwebmail = new libwebmail();
	// CONSTRUCT EMAILS ELEMENT FOR USERS
	$targetIdentifier = $rankTarget == "student" ? $_POST['studentID']:$_POST['rankTargetDetail'];
	$targetUsersArray = $libclubsenrol->getTargetUsers($rankTarget, $targetIdentifier);
	$libclubsenrol->AddClubsDetailToTargetUsers($targetUsersArray);
	
	if (is_array($targetUsersArray)) {
		foreach($targetUsersArray as $key => $elements) {
			
			// COMPOSE RECEIPIANT LIST
			$studentIdArr = array($elements["ID"]);
			if ($sendWithParent) {
				$emailList = $libclubsenrol->Get_Email_List_Including_Parent($studentIdArr);	
			} else {
				$emailList = $studentIdArr;
			}
			
			// COMPOSE MAIL SUBJECT
			$mailSubject = $Lang['EmailNotification']['eEnrolment']['Club']['EnrolmentResult']['Subject'];
			
			// COMPOSE MAIL BODY
			$mailBody = str_replace("__STUDENT_NAME__", $elements['NAME'], $Lang['EmailNotification']['eEnrolment']['EnrolmentResult']['Content']) ."<br />";
			
			if (is_array($elements["CLUBSDETAIL"]) && count($elements["CLUBSDETAIL"])>0) {
				$mailBody .= $libclubsenrol->composeEmailContent($elements["CLUBSDETAIL"]);
			
				// SEND EMAIL
				$lwebmail->sendModuleMail($emailList, $mailSubject, $mailBody);
			} else {
				// do nothing
			}
		}
	} else {
		// do nothing
	}
//}

header("Location: club_email.php?msg=2");

?>
