<?php
# using: 
#############################################
#
#   Date    2020-01-09 Tommy
#           file created
#
#############################################
$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment']) {
    include_once ($PATH_WRT_ROOT . "includes/libgroup.php");
    include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
    include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
    include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_ui.php");

    $libenroll = new libclubsenrol($AcademicYearID);
    $libenroll_ui = new libclubsenrol_ui();

    if ($libenroll->hasAccessRight($_SESSION['UserID'])) {
        if (! isset($_GET)) {
            header("Location: index.php");
            exit();
        }

        /*
         * $type == 2 => Club Attendance
         */
        $type = $_POST["type"];
        $success = array();

        if($sys_custom['eEnrolment']['Refresh_Performance']){
            if ($type == 2) { // club or activity attendance
                
//                 $sql = "SELECT COUNT(*) FROM information_schema.COLUMNS WHERE TABLE_NAME = 'INTRANET_USERGROUP' AND  COLUMN_NAME = 'AdjustReason';";
//                 $result = $libenroll->returnVector($sql);

                if(!$libenroll->checkColumnIsExist("INTRANET_USERGROUP", "AdjustReason")){
                    $sql = "ALTER TABLE INTRANET_USERGROUP ADD AdjustReason varchar(255)";
                    $libenroll->db_db_query($sql);
                }
    
                $AcademicYearID = $_POST['AcademicYearID'];
                $semester = $_POST['Semester'];
    
                // Club
                if ($type == 2) {
//                     $sel_Category = $_GET['Category'];
//                     $keyword = $_GET['Keyword'];
                    $ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info("", $AcademicYearID, $ShowUserRegOnly = 0, $semester, '', 0, 1, 1, true, '');
                    $recordIdArr = array_unique(array_keys($ClubInfoArr));
                    $numOfRecordIdArr = count($recordIdArr);
                }
    
                for ($i = 0; $i < $numOfRecordIdArr; $i ++) {
                    if ($type == 2) {
                        // Club
                        $recordId = $recordIdArr[$i];
                        $dateIdFieldName = 'GroupDateID';
                        $groupId = $libenroll->GET_GROUPID($recordId);
                        $lgroup = new libgroup($groupId);
                        $title = str_replace(' ', '_', $lgroup->Title);
    
                        $meetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($recordId);
                        $studentInfoAry = $libenroll->Get_Club_Member_Info($recordId, array(
                            USERTYPE_STUDENT
                        ));
                        $studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
                        $memberAttendanceInfoArr = $libenroll->Get_Student_Club_Attendance_Info($studentIdAry, array(
                            $recordId
                        ));    
                    }
    
                    $numOfMeeting = count($meetingDateInfoAry);
                    $numOfStudent = count($studentIdAry);
    
                    for ($j = 0; $j < $numOfStudent; $j ++) {
                        $_studentId = $studentInfoAry[$j]['UserID'];
                        $_userGroupId = $studentInfoAry[$j]['UserGroupID'];
                        $_attendancePercentage = $memberAttendanceInfoArr[$_studentId][$recordId]['AttendancePercentageRounded'];
                        $_attendanceHours = $memberAttendanceInfoArr[$_studentId][$recordId]['Hours'];
                        $clubID = 0;
                        $hasRecord = array();
                        for($k = 0; $k < sizeof($meetingDateInfoAry); $k++){
                            $clubID = $meetingDateInfoAry[$k]['GroupDateID'];
                            $hasRecord[] = ($memberAttendanceInfoArr[$_studentId][$recordId][$clubID] == '')? 0 : 1;
                        }
                        
                        $_performance = "";
                        if(!in_array(1, $hasRecord)){
                            $_performance = "";
                        }else{
                            if ($_attendanceHours < 10) {
                                if ($_attendancePercentage == 100) {
                                    $_performance = "C";
                                } elseif ($_attendancePercentage >= 75 && $_attendancePercentage < 100) {
                                    $_performance = "D";
                                } elseif ($_attendancePercentage >= 50 && $_attendancePercentage < 75) {
                                    $_performance = "E";
                                } elseif ($_attendancePercentage >= 25 && $_attendancePercentage < 50) {
                                    $_performance = "F";
                                } elseif ($_attendancePercentage >= 0 && $_attendancePercentage < 25) {
                                    $_performance = "U";
                                }else{
                                    $_performance = "error";
                                }
                            } else {
                                if ($_attendancePercentage >= 90) {
                                    $_performance = "A";
                                } elseif ($_attendancePercentage >= 80 && $_attendancePercentage < 90) {
                                    $_performance = "B";
                                } elseif ($_attendancePercentage >= 70 && $_attendancePercentage < 80) {
                                    $_performance = "C";
                                } elseif ($_attendancePercentage >= 60 && $_attendancePercentage < 70) {
                                    $_performance = "D";
                                } elseif ($_attendancePercentage >= 50 && $_attendancePercentage < 60) {
                                    $_performance = "E";
                                } elseif ($_attendancePercentage >= 40 && $_attendancePercentage < 50) {
                                    $_performance = "F";
                                } elseif ($_attendancePercentage >= 0 && $_attendancePercentage < 40) {
                                    $_performance = "U";
                                }else{
                                    $_performance = "error";
                                }
                            }
                        }
    
                        $_enrolGroupID = $studentInfoAry[$j]['EnrolGroupID'];

                        $success[] = $libenroll->Update_Enrollment_Performance($_userGroupId, $_performance);
                    }
                }
            }

            if(!in_array(false, $success)){
                $libenroll->Update_Last_Generated_Time($AcademicYearID, $semester);
            }
            header("Location: refresh_performance.php?AcademicYearID=".$AcademicYearID."&type=".$type."&Semester=".$semester);
        }else {
            echo "You have no priviledge to access this page.";
        }
    } else {
        echo "You have no priviledge to access this page.";
    }
} else {
    echo "You have no priviledge to access this page.";
}

?>