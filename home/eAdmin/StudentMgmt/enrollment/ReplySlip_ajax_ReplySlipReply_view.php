<?php 
/*
 * Using
 * Change Log:
 * Date: 2017-08-14 (Omas) #P110324 
 *  		Modified ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
 *  		fix export became big5 problem
 * Date: 2017-04-24 Villa #P110324 
 * 		- open the file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$linterface = new interface_html("popup.html");
$MulitMC_Dimeter = '#MCDIMETER#';
if(!$sys_custom['eEnrolment']['ReplySlip']){
	echo "YOU HAVE NO ACCESS RIGHT";
	return false;
}
#Data Getting START
if($isActivity){
	$MemberList = $libenroll->getEventStudentInfo($EnrolGroupID);
// 	$MemberList = "";
// 	debug_pr($MemberList);
	foreach ((array)$MemberList as $_MemberList){
		$MemberListArr[$_MemberList['StudentID']]['StudentID'] = $_MemberList['StudentID'];
		$MemberListArr[$_MemberList['StudentID']]['StudentName'] = Get_Lang_Selection($_MemberList['ChineseName'],$_MemberList['EnglishName']);
		$MemberListArr[$_MemberList['StudentID']]['ClassName'] = $_MemberList['ClassName'];
		$MemberListArr[$_MemberList['StudentID']]['ClassNumber'] = $_MemberList['ClassNumber'];
		if(!$_MemberList['ClassName']){
			unset($MemberListArr[$_MemberList['StudentID']]);
		}
	}
}else{
	$MemberList = $libenroll->Get_Club_Member_Info($EnrolGroupID);
	foreach ((array)$MemberList as $_MemberList){
		$MemberListArr[$_MemberList['StudentID']]['StudentID'] = $_MemberList['StudentID'];
		$MemberListArr[$_MemberList['StudentID']]['StudentName'] = $_MemberList['StudentName'];
		$MemberListArr[$_MemberList['StudentID']]['ClassName'] = $_MemberList['ClassName'];
		$MemberListArr[$_MemberList['StudentID']]['ClassNumber'] = $_MemberList['ClassNumber'];
		if(!$_MemberList['YearClassID']){
			unset($MemberListArr[$_MemberList['StudentID']]);
		}
	}
}
//if(!$ReplySlipGroupMappingID){
// 	$rs = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolGroupID,2);
// 	$rs = $rs[0];
// 	$ReplySlipGroupMappingID = $rs['ReplySlipGroupMappingID'];
// 	$ReplySlipID = $rs['ReplySlipID'];
// }
// if(!$ReplySlipGroupMappingID){
// 	return false;
// }
$getReplySlipReply_Info = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
$repliedStudentId = Get_Array_By_Key( $getReplySlipReply_Info, 'StudentID');
$memberStudentId = Get_Array_By_Key( $MemberList, 'StudentID');
$repliedStudentIdArr = array_intersect($repliedStudentId, $memberStudentId);
foreach ((array)$getReplySlipReply_Info as $_getReplySlipReply_Info){
	$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['StudentID'] = $_getReplySlipReply_Info['StudentID'];
	//Ans	
	$Answer = explode('#ANS#',$_getReplySlipReply_Info['Answer']);
	unset($Answer[0]);//0->useless
	if($Answer){
		foreach ((array)$Answer as $_Answer){
			$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['Answer'][] = $_Answer;
		}
	}
// 	//SignBy
// 	$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['SignBy'] = $_getReplySlipReply_Info['ModifiedBy'];
	//SignAt
	$ReplySlipReply_Info[ $_getReplySlipReply_Info['StudentID']]['SignAt'] = $_getReplySlipReply_Info['DateModified'];
	
}
$ReplySlipInfo = $libenroll->getReplySlip($ReplySlipID);
$ReplySlipInfo = $ReplySlipInfo[0];
$ReplySlip_Question = $libenroll->splitQuestion($ReplySlipInfo['Detail']);
#Data Getting END
// returnQuestionString

$ReplySlipInfoTable = '<table width="100%" class="form_table_v30">';
$ReplySlipInfoTable .= '<tr>';
	$ReplySlipInfoTable .= '<td width="30%" class="field_title">'.$eEnrollment['replySlip']['Title'].'</td>';
	$ReplySlipInfoTable .= '<td>'.$ReplySlipInfo['Title'].'</td>';
$ReplySlipInfoTable .= '</tr>';
$ReplySlipInfoTable .= '<tr>';
	$ReplySlipInfoTable .= '<td width="30%" class="field_title">'.$eEnrollment['replySlip']['Instruction'].'</td>';
	$ReplySlipInfoTable .= '<td>'.$ReplySlipInfo['Instruction'].'</td>';
$ReplySlipInfoTable .= '</tr>';
$ReplySlipInfoTable .= '<tr>';
$ReplySlipInfoTable .= '<td class="field_title">'.$eEnrollment['replySlip']['Signed_Total'].'</td>';
	//$ReplySlipInfoTable .= '<td>'.count($ReplySlipReply_Info).'/'.count($MemberListArr).'</td>';
	$ReplySlipInfoTable .= '<td>'.count($repliedStudentIdArr).'/'.count($MemberListArr).'</td>';
$ReplySlipInfoTable .= '</tr>';
$ReplySlipInfoTable .= '</table>';

$ReplyDetailTable = '<table class="common_table_list view_table_list_v30">';
	#Table Header START
	$ReplyDetailTable .= '<thead>';
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['ClassName'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['ClassName'];
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['ClassNumber'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['ClassNumber'];
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['StudentName'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['StudentName'];
		#Print EACH QUESTION
		foreach ((array)$ReplySlip_Question as $_NoOfQuestion => $_ReplySlip_Question){
			$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['Question'].($_NoOfQuestion+1).': '.$_ReplySlip_Question[1].'</th>';
			$ExportHeader[] = $eEnrollment['replySlip']['Question'].($_NoOfQuestion+1).': '.$_ReplySlip_Question[1];
		}
// 		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['SignedBy'].'</th>';
// 		$ExportHeader[] = $eEnrollment['replySlip']['SignedBy'];
		$ReplyDetailTable .= '<th>'.$eEnrollment['replySlip']['DateSigned'].'</th>';
		$ExportHeader[] = $eEnrollment['replySlip']['DateSigned'];
	$ReplyDetailTable .= '</thead>';
	#Table Header END
	
	#Table Body Start 
	$ReplyDetailTable .= '<tbody>';
	#ForEach Student
	$ExportRow = 0;
	foreach ((array)$MemberListArr as $StudentID => $StudentInfo){
		$isSuspend = (empty($ReplySlipReply_Info[$StudentID]))? "row_suspend":"";
		$ReplyDetailTable .= '<tr class="'.$isSuspend.'">';
			//ClassName
			$ClassName = $StudentInfo['ClassName'];
			$ReplyDetailTable .= '<td>'.$ClassName.'</td>';
			$ExportBody[$ExportRow][] = $ClassName;
			//ClassNumber
			$ClassNumber = $StudentInfo['ClassNumber'];
			$ReplyDetailTable .= '<td>'.$ClassNumber.'</td>';
			$ExportBody[$ExportRow][] = $ClassNumber;
			//StudentName
			$StudentName = $StudentInfo['StudentName'];
			$ReplyDetailTable .= '<td>'.$StudentName.'</td>';
			$ExportBody[$ExportRow][] = $StudentName;
			#ForEach Question
			foreach ((array)$ReplySlip_Question as $_NoOfQuestion => $_ReplySlip_Question){
				//handling different type of Question
				if(isset($ReplySlipReply_Info[$StudentID]['Answer'][$_NoOfQuestion])){
					/*	Type 1: YesNo Question/ MC
					 * 	save as=> #ANS#x where x is a number
					 *  Type 2: Multi-MC
					 *  save as=> #ANS#x#DIMETER#x where x is a number
					 *  Type 3: ShortQuestion/ LongQuester
					 *  save as=> #ANS#abc where abc is a string
					 */ 
					$StudentAnswer = '';
					$Comma = '';
					$StudentAnswer_temp = $ReplySlipReply_Info[$StudentID]['Answer'][$_NoOfQuestion];
					$StudentAnswer_temp = explode($MulitMC_Dimeter,$StudentAnswer_temp);
					if(count($StudentAnswer_temp)>1){
						//type 2
						foreach ((array)$StudentAnswer_temp as $_StudentAnswer_temp){
							$StudentAnswer .= $Comma.$_ReplySlip_Question[2][$_StudentAnswer_temp];
							$Comma = ',';
						}
					}else{
						//type 1 and type3
						$_StudentAnswer_temp = $_ReplySlip_Question[2][$StudentAnswer_temp[0]];
						if($_StudentAnswer_temp){
							//type 1 can match
							$StudentAnswer = $_StudentAnswer_temp;
						}else{
							$StudentAnswer = $StudentAnswer_temp[0];
						}
					}
				}else{
					$StudentAnswer = '';
				}
				$ReplyDetailTable .= '<td>'.$StudentAnswer.'</td>';
				$ExportBody[$ExportRow][] = $StudentAnswer;
			}
			//SignBy
// 			$SignBy = $MemberListArr[$ReplySlipReply_Info[$StudentID]['SignBy']]['StudentName'];
// 			$ReplyDetailTable .= '<td>'.$SignBy.'</td>';
// 			$ExportBody[$ExportRow][] = $SignBy;
			//SignAt
			$SignAt = $ReplySlipReply_Info[$StudentID]['SignAt']? $ReplySlipReply_Info[$StudentID]['SignAt']:$eEnrollment['replySlip']['NotSign'] ;
			$ReplyDetailTable .= '<td>'.$SignAt.'</td>';
			$ExportBody[$ExportRow][] = $SignAt;
			
		$ReplyDetailTable .= '</tr>';
		$ExportRow++;
	}
	$ReplyDetailTable .= '</tbody>';
	#Tabble Body End
	
$ReplyDetailTable .= '</table>';
if($isExport){
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$filename = "ReplySlipReply.csv";
	$export_content = $lexport->GET_EXPORT_TXT($ExportBody, $ExportHeader);
	$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false);
}else{
	$btnAry[] = array('export', 'javascript:goExport('.$ReplySlipGroupMappingID.','.$EnrolGroupID.','.$ReplySlipID.')','',array());
	$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
// 	$View = '<div align="center">'.$linterface->GET_ACTION_BTN($button_close, "export", "goExport($ReplySlipGroupMappingID,$EnrolGroupID,$ReplySlipID)","cancelbtn").'</div>';
	$View = $ReplySlipInfoTable;
	$View .= '</br>';
	$View .= '</br>';
	$View .= '</br>';
	$View .= $htmlAry['contentTool'];
	$View .= $ReplyDetailTable;
	$View .= '<div align="center">'.$linterface->GET_ACTION_BTN($button_close, "button", "$('#TB_window').fadeOut(); 	tb_remove(); return false;","cancelbtn").'</div>';
	echo $View;
}
?>
