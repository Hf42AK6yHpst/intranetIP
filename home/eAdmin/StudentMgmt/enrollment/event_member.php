<?php
#using : 
/******************************************
 * Modification Log:
 * Date: 2020-04-27 (Tommy)
 * removed 'ExportSQL' in hidden value since it cases "select.+(from|limit)"
 * 
 * Date: 2018-10-03 (Philips)
 *  - Added Student Gender Column Related $sys_custom['eEnrolment']['ShowStudentGender']
 *  
 * Date: 2017-08-14 (Omas) #P110324 
 * Modified Added ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
 * 
 * Date:	2017-05-24 Anna
 * Details: Added $sys_custom['eEnrolment']['ShowStudentNickName']
 * 
 * Date:	2015-07-30 Evan
 * Details: Fixed thebug in listing with status criterion
 * 
 * Date:	2015-07-10 Evan
 * Details:	Update search box style
 * 
 * Date:	2015-06-11 Evan
 * Details: Set if $EventInfoArr[2]== null will show 0 instead of nothing
 * 
 * Date:	2015-06-02 Evan
 * Details:	Replace the old style with UI standard
 * 
 * Date: 	2012-12-20 Rita
 * Details:	change approval btn's link to page
 * 
 * Date: 	2010-08-02 Thomas
 * Details	- Check the data is freezed or not
 *			  Show alert message when adding / updating the data
 *
 ******************************************/
 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolEventID = IntegerSafe($EnrolEventID);

if ($plugin['eEnrollment'])
{
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_EVENT_PIC($EventEnrolID)))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
//	$lc = new libclubsenrol();
//	$enrolInfo = $lc->getEnrollmentInfo();// 305----->42
	
    if ($libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
        
        $linterface = new interface_html();
        
        if (isset($EventEnrolID) && $EventEnrolID != ''){
        	$EnrolEventID = $EventEnrolID;
        }else{
        	$EventEnrolID = $EventEnrolID;
        }
        	
        $EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		
        
        ($EventInfoArr['AddToPayment']==1)? $allowPayment=false : $allowPayment=true;
        
        //$TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['app_stu_list'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 2;
        include_once("management_tabs.php");
        # navigation
        $PAGE_NAVIGATION[] = array($EventInfoArr[3]." ".$eEnrollment['app_stu_list'], "");

        $quotaDisplay = "<table class=\"form_table_v30\">";
		$quotaDisplay .= "<tr><td class=\"field_title\">{$eEnrollment['act_name']}</td><td>".$EventInfoArr[3]."</td></tr>";
		if ($EventInfoArr[1] == 0)
		{
			
			$quotaDisplay .= "<tr><td class=\"field_title\">$i_ClubsEnrollment_ActivityQuota1 </td><td>".$i_ClubsEnrollment_NoLimit."</td></tr>";
		}
		else
		{
			$quotaDisplay .= "<tr><td class=\"field_title\">$i_ClubsEnrollment_ActivityQuota1 </td><td>".$EventInfoArr[1]."</td></tr>";
		}
		$quotaDisplay .= "<tr><td class=\"field_title\">$i_ClubsEnrollment_GroupApprovedCount </td><td>".($EventInfoArr[2] == null? 0 :$EventInfoArr[2])."</td></tr>";
		$quotaDisplay .= "</table>";
        
        $linterface->LAYOUT_START();
        
       	$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"" . intranet_htmlspecialchars(stripslashes($keyword)) . "\" onkeyup=\"Check_Go_Search(event);\"/>";
		
		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_student_name']) $keyword = "";

if ($field=="" || !isset($field))
{
	$field = 0;
}

switch ($field){
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	case 3: $field = 3; break;
	case 4: $field = 4; break;
	case 5: $field = 5; break;
	default: $field = 0; break;
}
if ($order=="" && !isset($order))
{
	$order = ($order == 0) ? 1 : 0;
}

/*
if (!isset($filter) || $filter=="") $filter = -1;
if ($filter != -1)
{
    $conds = "AND b.RecordStatus = $filter";
}
else
{
    $conds = "";
}

$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
$sql = "SELECT
				IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber = '', '-', CONCAT($ClassNameField,' (',ycu.ClassNumber,')')) as Class,
				a.EnglishName,
				a.ChineseName,
				CASE b.RecordStatus
				     WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
				     WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
				     WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
				END,
				b.DateInput,
				CONCAT('<input type=checkbox name=uid[] value=', a.UserID ,'>'),
				b.ApprovedBy
		FROM 
				INTRANET_USER as a
				Inner Join
				INTRANET_ENROL_EVENTSTUDENT as b
				On (a.UserID = b.StudentID)
				Inner Join
				YEAR_CLASS_USER as ycu
				On (a.UserID = ycu.UserID)
				Inner Join
				YEAR_CLASS as yc
				On (ycu.YearClassID = yc.YearClassID)
		WHERE 
				b.EnrolEventID = '$EnrolEventID'
				AND
				(a.EnglishName LIKE '%".$keyword."%' OR a.ChineseName LIKE '%".$keyword."%')
				And
				a.RecordType = 2
				And
				a.RecordStatus = 1
				And
				ycu.ClassNumber is not null
				And
				yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				$conds
        ";		
*/
$filter = $_POST['filter'];
if($filter== '')
	$filter = $_GET['filter'];
if (!isset($filter) || $filter=="") 
	$filter = 2;
	
$sql = $libenroll->Get_Management_Activity_Participant_Enrollment_Sql(array($EnrolEventID), array($filter), $keyword, $ForExport=0);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);


//if($libenroll->enableActivitySurveyFormRight() && !$libenroll->enableActivityFillInEnrolReasonRight()){
//	$li->field_array = array("Class","EnglishName","ChineseName","iees.RecordStatus","iees.DateInput", "SurveyResult", "iu.UserID");
//}elseif(!$libenroll->enableActivitySurveyFormRight() && $libenroll->enableActivityFillInEnrolReasonRight()){
//	$li->field_array = array("Class","EnglishName","ChineseName","iees.RecordStatus","iees.DateInput", "Reason", "iu.UserID");
//}elseif($libenroll->enableActivitySurveyFormRight() && $libenroll->enableActivityFillInEnrolReasonRight()){
//	$li->field_array = array("Class","EnglishName","ChineseName","iees.RecordStatus","iees.DateInput", "SurveyResult", "Reason", "iu.UserID");
//}else{
//	$li->field_array = array("Class","EnglishName","ChineseName","iees.RecordStatus","iees.DateInput","iu.UserID");
//}
$li->field_array = array();
$li->field_array[] = "ClassName";
$li->field_array[] ="ClassNumber";
$li->field_array[] = "EnglishName";
$li->field_array[] = "ChineseName";
if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
	$li->field_array[] = "NickName";
}
if($sys_custom['eEnrolment']['ShowStudentGender']){
    $li->field_array[] = "Gender";
}
$li->field_array[] = "iees.RecordStatus";
$li->field_array[] = "iees.DateInput";
if ($libenroll->enableActivitySurveyFormRight()) {
	$li->field_array[] = "SurveyResult";
}
if ($libenroll->enableActivityFillInEnrolReasonRight()) {
	$li->field_array[] = "Reason";
}
$li->field_array[] = "iu.UserID";


$li->fieldorder2 = ", iees.EventStudentID";

### Record the sql for export first	
$li->sql = $libenroll->Get_Management_Activity_Participant_Enrollment_Sql(array($EnrolEventID), array($filter), $keyword, $ForExport=1);
$ExportSQLTemp = $li->built_sql();
$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);
		
### Switch back to display sql	
$li->sql = $sql;

if (!$libenroll->Event_disableStatus || $allowPayment)
{
	$li->no_col = sizeof($li->field_array)+1;
}
else
{
	$li->no_col = sizeof($li->field_array);
}

$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = "eEnrolmentGroupEnrolList";

// TABLE COLUMN
$col = 0;
$li->column_list .= "<td width=1 class=tableTitle><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($col++, $i_ClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($col++, $i_ClassNumber)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($col++, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($col++, $i_UserChineseName)."</td>\n";
if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
	$li->column_list .= "<td width=15% class=tableTitle>".$li->column($col++, $i_UserNickName)."</td>\n";
}
if($sys_custom['eEnrolment']['ShowStudentGender']){
    $li->column_list .= "<td width=8% class=tableTitle>".$li->column($col++, $Lang['General']['Sex'])."</td>\n";
}
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($col++, $i_ClubsEnrollment_Status)."</td>\n";
$li->column_list .= "<td width=22% class=tableTitle>".$li->column($col++, $i_ClubsEnrollment_LastSubmissionTime)."</td>\n";

# Survey Form Customization
if($libenroll->enableActivitySurveyFormRight()){
	# Survey Form Result Column	
	$li->column_list .= "<td width=5% class=tableTitle>".$li->column($col++, $Lang['eSurvey']['SurveyResult'])."</td>\n";

	//$exportMember = "<a href=\"".$exportFile."?".$export_parameters."\" class=\"sub_btn\"> ".$Lang['eEnrolment']['Btn']['ExportMember2']."</a>";
	
	$exportMember = "<a href='javascript:js_Go_Export();' class='sub_btn'>".$Lang['eEnrolment']['Btn']['ExportMember2']."</a>";
	$exportSurveyResult = "<a href='javascript:js_Go_ExportSurveyResult();' class='sub_btn'>".	$Lang['eSurvey']['SurveyResult']."</a>";
	
	$exportLayer = '<div class="Conntent_tool"><div class="btn_option" id="ExportDiv">
					<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Export'].'</a>
					<br style="clear: both;">
					<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
					'.$exportMember.'
					'.$exportSurveyResult.'
					</div>
					</div></div>';
}

if($libenroll->enableActivityFillInEnrolReasonRight()){
	$li->column_list .= "<th width=\"9%\">".$li->column($col++, $Lang['eEnrolment']['Reason'])."</th>\n";
}

if (!$libenroll->Event_disableStatus || $allowPayment)
{
	$li->column_list .= "<td width='1'>".$li->check("uid[]")."</td>\n";
}

$filter_array = array (
                       array (-1,$i_status_all),
                       array (0,$i_ClubsEnrollment_StatusWaiting),
                       array (1,$i_ClubsEnrollment_StatusRejected),
                       array (2,$i_ClubsEnrollment_StatusApproved)
                       );
$toolbar = getSelectByArray($filter_array,"name='filter' id='filter' onChange='this.form.submit()'",$filter, 0, 1);

if ($msg == 1)
{
	$response_msg = $linterface->GET_SYS_MSG("add");
}
if ($msg == 2)
{
     //$response_msg = $i_Discipline_System_alert_Approved;
     $response_msg = $linterface->GET_SYS_MSG("update");
}
if ($msg == 4)
{
     $response_msg = $linterface->GET_SYS_MSG("", $eEnrollment['approve_quota_exceeded']);
}

if($libenroll->enableActivityFillInEnrolReasonRight()){
	if($msg == 5){
		$response_msg = $linterface->GET_SYS_MSG("update_failed");	
	}
}


$type = 1;
//$export_parameters = "EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
$add_member_parameters = "EnrolEventID=$EnrolEventID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";

$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));

if($filter==1){
	$addLink = '';
}else{
	$addLink = $linterface->GET_LNK_ADD(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "add_member.php?$add_member_parameters", '', '', '', '', 0); 
	
}

if($sys_custom['eEnrolment']['ReplySlip']){
	echo $linterface->Include_Thickbox_JS_CSS();
}
?>
<br />

<script language="javascript">
	
function js_Go_Export()
{
	document.getElementById('type').value = 1;
	
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = 'export.php';
    jsObjForm.submit();
    
    jsObjForm.action = '';
}

<?php if($libenroll->enableActivitySurveyFormRight()){ ?>
	
	    function view_result(id)
		{
			newWindow('survey_result.php?RecordType=A&AnswerID='+id,1);
		}
		
		function js_Go_ExportSurveyResult()
		{	
			var jsObjForm = document.getElementById('form1');
			jsObjForm.action = 'export_survey_result.php';
		    jsObjForm.submit();
		    
		    jsObjForm.action = '';
		}
		
	
<? } ?>

<?php if($sys_custom['eEnrolment']['ReplySlip']):?>
function onLoadThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	load_dyn_size_thickbox_ip('<?=$eEnrollment['replySlip']['ReplySlipDetail']?>', 'onLoadReplySlipThickBox('+ReplySlipGroupMappingID+','+EnrolGroupID+','+ReplySlipID+');');
}
function onLoadReplySlipThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	$.ajax({
		url: "ReplySlip_ajax_ReplySlipReply_view2.php",
		type: "POST",
		data: {
			"ReplySlipGroupMappingID": ReplySlipGroupMappingID, 
			"EnrolGroupID": EnrolGroupID,
			"ReplySlipID":ReplySlipID,
			"Status" : $("#filter").val(),
			"isActivity":true
			},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
		}
	});
}
function goExport(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
    	document.form1.action = "ReplySlip_ajax_ReplySlipReply_view2.php?ReplySlipGroupMappingID="+ReplySlipGroupMappingID+"&EnrolGroupID="+EnrolGroupID+"&ReplySlipID="+ReplySlipID+"&isExport="+1+"&isActivity=true";
    	document.form1.target = "_blank";
    	document.form1.method = "post";
    	document.form1.submit();
}
<?php endif?>
</script>
<form id="form1" name="form1" ACTION="" METHOD="POST">
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td><?=$quotaDisplay?></td></tr>
	<tr><td height="5"></td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
</table>
<br/>

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td align="left"></td>
	<td align="right"><?=$response_msg?></td>
</tr>
<tr>
	<td align="left">
		<?
		if ($libenroll->AllowToEditPreviousYearData && (!$libenroll->Event_disableStatus || $allowPayment))
		{
		?>
			<?=$addLink?>
			<?= toolBarSpacer();?>
		<?
		}
		?>
		
		<?php if($libenroll->enableActivitySurveyFormRight()){
			
			echo $exportLayer;
		
		}else{
			?>
			<div  class="Conntent_tool" >
			<a href="javascript:js_Go_Export();"  style="background-image: none;padding-left:0px; line-height:20px" ><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a>
			</div>
		<?}?>
		<?php 
		if($sys_custom['eEnrolment']['ReplySlip']){
			$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolEventID,2);
			$ReplySlipRelation = $ReplySlipRelation[0];
			$ReplySlipGroupMappingID = $ReplySlipRelation['ReplySlipGroupMappingID'];
			$ReplySlipID = $ReplySlipRelation['ReplySlipID'];
			
			$link = '';
			$ReplySlipReply = '';
			if($ReplySlipGroupMappingID){
				$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
				$link = '<div class="Conntent_tool"><a href="javascript:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$EnrolEventID.','.$ReplySlipID.')" style="background:url(\'/images/2009a/eOffice/icon_resultview.gif\') no-repeat;">'.$eEnrollment['replySlip']['replySlip'].'</a></div>';
			}
		}
		echo $link;
		?>
	</td>
	<td align="right">
		<div class="Conntent_search"><?=$searchTag?></div>
	</td>
</tr>
<tr>
	<td align="left">
		<?= $toolbar ?>
		<?= toolBarSpacer() ?>
	</td>
	<td align="right" valign='bottom'>
	<?
	if ($libenroll->AllowToEditPreviousYearData && (!$libenroll->Event_disableStatus || $allowPayment))
	{
	?>
		<?/* $functionbar; */?>
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
		  <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
		  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif"><table border="0" cellspacing="0" cellpadding="2">
		      <tr>
		        
		       <?php if($libenroll->enableActivityFillInEnrolReasonRight() && $EnrolEventID!=''){ ?>		
	        		<td nowrap="nowrap"><a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRejectNoDel(document.form1,'uid[]','add_event_stu_enrol_rejected_reason.php')"?>" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_reject.gif" width="12" height="12" border="0" align="absmiddle">
		        
	           <?}else{?>
		        	<td nowrap="nowrap"><a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRejectNoDel(document.form1,'uid[]','event_stu_remove_update.php')"?>" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_reject.gif" width="12" height="12" border="0" align="absmiddle">
		        
		        <?}?>
		         
		          
		          <?=$button_reject?></a></td>
		        <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
		 
		    	<td nowrap="nowrap"><a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkApprove(document.form1,'uid[]','event_stu_approve_update.php')" ?>" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle">
		  
		          <?=$button_approve?></a></td>
		      </tr>
		    </table></td>
		  <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
		</tr>
		</table>
	<?
	}
	?>

	</td>
</tr>
</table>
<?=$li->display("95%")?>
<br />
<?=$RemarksTable?>

<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='event_status.php?AcademicYearID=$AcademicYearID'")?>
</div>
</td></tr>
</table>

<br />

<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="EventEnrolID" value="<?=$EnrolEventID?>">
<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" id="type" name="type" value="<?=$type?>">

</form>

<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>