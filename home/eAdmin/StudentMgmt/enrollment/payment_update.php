<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_opendb();

$libenroll = new libclubsenrol();

# update AddToPayment
if ($EnrolGroupID != "") {
	$Sql = "
				UPDATE
						INTRANET_ENROL_GROUPINFO
				SET
						AddToPayment = 1
				WHERE
						EnrolGroupID = '$EnrolGroupID'
			";
} else if ($EnrolEventID != "") {
	$Sql = "
				UPDATE
						INTRANET_ENROL_EVENTINFO
				SET
						AddToPayment = 1
				WHERE
						EnrolEventID = '$EnrolEventID'
			";
}
$libenroll->db_db_query($Sql);

$fieldname = '';
$values = '';
$lpayment = new libpayment();
$use_merchant_account =  $lpayment->useEWalletMerchantAccount();
$multi_marchant_account_ids = array();
if($use_merchant_account){
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$last_MerchantAccountID_value = '';
		$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, true);
		foreach ($service_provider_list as $k => $temp) {
			$MerchantAccountID_value = ${'MerchantAccountID_' . $k};
			if ($MerchantAccountID_value == '') {
				continue;
			}
			$last_MerchantAccountID_value = $MerchantAccountID_value;
			$multi_marchant_account_ids[] = $MerchantAccountID_value;
		}
		$fieldname .= ",MerchantAccountID";
		$values .= (($last_MerchantAccountID_value == '') ? ",NULL" : ",'$last_MerchantAccountID_value'");
	} else {
		if ($_POST['MerchantAccountID'] != '') {
			$fieldname .= ",MerchantAccountID";
			$values .= ",'" . $_POST['MerchantAccountID'] . "'";
		}
	}
}

# add PAYMENT_PAYMENT_ITEM ################################################################################
$Sql = "
			INSERT INTO
						PAYMENT_PAYMENT_ITEM
						(CatID, Name, DisplayOrder, PayPriority, Description, RecordStatus, 
						 ProcessingTerminalUser, ProcessingTerminalIP, ProcessingAdminUser,
						 ProcessingUserID,
						 StartDate, EndDate, DefaultAmount, DateInput, DateModified $fieldname)
			VALUES
						(
						'".$_POST['CatID']."',
						'".$_POST['ItemName']."',
						'".$_POST['DisplayOrder']."',
						'".$_POST['PayPriority']."',
						'".$_POST['Description']."',
						'0', 						
						NULL, NULL, NULL,
						'$UserID',
						'".$_POST['StartDate']."',
						'".$_POST['EndDate']."',
						'".$OriItemAmount."',
						NOW(), NOW() $values
						)
		";
$libenroll->db_db_query($Sql);
$ItemID = $libenroll->db_insert_id($Sql);
# add PAYMENT_PAYMENT_ITEM ################################################################################

if($use_merchant_account) {
	if ($sys_custom['ePayment']['MultiPaymentGateway']) {
		foreach ($multi_marchant_account_ids as $MerchantAccountID_value) {
			$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY
						(ItemID, MerchantAccountID, DateInput, InputBy)
						VALUES
						($ItemID, '$MerchantAccountID_value', now(), '" . $_SESSION['UserID'] . "')
						";
			$Result['CreateItemPaymentGateway' . $MerchantAccountID_value] = $libenroll->db_db_query($sql);
		}
	}
}


// get enrol student list
if ($EnrolGroupID != "") {
	// get group enrol student list	
	$StudentArr = $libenroll->GET_ENROLLED_GROUPSTUDENT($EnrolGroupID);
} else if ($EnrolEventID != "") {
	// get event enrol student list
	$StudentArr = $libenroll->GET_ENROLLED_EVENTSTUDENT($EnrolEventID);
}

for ($i = 0; $i < sizeof($StudentArr); $i++) {
	$thisStudentID = $StudentArr[$i][1];
	$Sql = "
				INSERT INTO
							PAYMENT_PAYMENT_ITEMSTUDENT
							(ItemID, StudentID, Amount, ProcessingUserID, RecordType, RecordStatus, 
							 DateInput, DateModified)
				VALUES
							(
							'".$ItemID."',
							'".$thisStudentID."',
							'".$_POST['ItemAmount'][$thisStudentID]."',
							'".$UserID."',
							0, 0, 
							NOW(), NOW()
							)
			";
	$libenroll->db_db_query($Sql);
	$PaymentID = $libenroll->db_insert_id($Sql);

	if ($debit_type == "debit_directly") {
		
		# get current balance
		$Sql = "
					SELECT
								Balance
					FROM
								PAYMENT_ACCOUNT
					WHERE
								StudentID = '".$thisStudentID."'
				";
	
		$Temp = $libenroll->returnVector($Sql);
		$Balance = $Temp[0];
		$Payment_Amount = intval($_POST['ItemAmount'][$thisStudentID]);
		$BalanceAfter = $Balance - $Payment_Amount;
		
		# Check Current Balance amount enough to settle the payment or not #
		if ($BalanceAfter >= 0) {			// enough current balance to settle payment
		
			# add PAYMENT_OVERALL_TRANSACTION_LOG
			/*
			$Sql = "
						INSERT INTO
									PAYMENT_OVERALL_TRANSACTION_LOG
									(StudentID, TransactionType, Amount, RelatedTransactionID,
									 BalanceAfter, TransactionTime, Details  )
						VALUES
									(
									'".$StudentArr[$i][1]."',
									'2',
									'".$_POST['ItemAmount']."',
									'".$PaymentID."',
									'".$BalanceAfter."',
									NOW(),
									'�ҥ~���ʶO / ECA Fee'
									)
					";
			*/
			$Sql = "
						INSERT INTO
									PAYMENT_OVERALL_TRANSACTION_LOG
									(StudentID, TransactionType, Amount, RelatedTransactionID,
									 BalanceAfter, TransactionTime, Details  )
						VALUES
									(
									'".$thisStudentID."',
									'2',
									'".$_POST['ItemAmount'][$thisStudentID]."',
									'".$PaymentID."',
									'".$BalanceAfter."',
									NOW(),
									'".$_POST['ItemName']."'
									)
					";
			$Temp = $libenroll->db_db_query($Sql);
			$logID = $libenroll->db_insert_id($Sql);
			
			$Sql = "
						UPDATE
									PAYMENT_OVERALL_TRANSACTION_LOG
						SET
									RefCode = CONCAT('PAY',LogID)
						WHERE
									LogID = '$logID'
					";
			$libenroll->db_db_query($Sql);
	
			# update PAYMENT_ACCOUNT
			$Sql = "
						UPDATE
									PAYMENT_ACCOUNT
						SET
									Balance = '".$BalanceAfter."',
									LastUpdateByAdmin = NULL,
									LastUpdateByTerminal = NULL,
									LastUpdateByUser = NOW(),
									LastUpdated = NOW()
						WHERE
									StudentID = '".$thisStudentID."'
					";				
			$libenroll->db_db_query($Sql);
			
			# update PAYMENT_PAYMENT_ITEMSTUDENT
			$Sql = "
					UPDATE				
								PAYMENT_PAYMENT_ITEMSTUDENT
					SET
								ProcessingTerminalUser = NULL,
								ProcessingTerminalIP = NULL,
								ProcessingAdminUser = NULL,
								RecordStatus = 1,
								PaidTime = NOW(),
								DateModified = NOW()
					WHERE
								ItemID = '".$ItemID."'
							AND
								StudentID = '".$thisStudentID."'
				";
			$libenroll->db_db_query($Sql);		
			
			$arr_payment_success_list[] = $thisStudentID;
			
		} else {			// NOT enough current balance to settle payment
		
			$arr_payment_fail_list[] = $thisStudentID;
			
		}
	}
}

if(sizeof($arr_payment_success_list)>0) {
	$success_list = implode(",",$arr_payment_success_list);
}
if(sizeof($arr_payment_fail_list)>0) {
	$fail_list = implode(",",$arr_payment_fail_list);
}

if ($debit_type == "debit_directly")
{
	//if(($success_list != "") || ($fail_list != "")) {
		$URL = "payment_summary.php?success_list=$success_list&fail_list=$fail_list&EnrolGroupID=$EnrolGroupID&EnrolEventID=$EnrolEventID";
	//}

}
else
{
	if ($EnrolGroupID != "")
	{
		$URL = "club_status.php?msg=2";
	}
	
	if ($EnrolEventID != "")
	{
		$URL = "event_status.php?msg=2";
	}
	
}


/*
if ($GroupID != "") {
	$URL = "club_status.php?msg=2";
}
if ($EnrolEventID != "") {
	$URL = "event_status.php?msg=2";
}
*/
intranet_closedb();
header("Location: $URL");

?>