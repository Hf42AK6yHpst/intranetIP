<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$linterface = new interface_html();
$lurlparahandler = new liburlparahandler(liburlparahandler::actionDecrypt, $_GET['p'], $enrolConfigAry['encryptionKey']);


### Check Access Right
$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin');


### Get Parameters
$paraAry = $lurlparahandler->getParaAry();
$recordType = $paraAry['recordType'];
$exportTypeCode = $paraAry['type'];
$AcademicYearID = $paraAry['AcademicYearID'];
$category_conds = stripslashes($paraAry['category_conds']);
$name_conds = stripslashes($paraAry['name_conds']);
$semester_conds = stripslashes($paraAry['semester_conds']);


### Retrieve settings and data for Clubs / Activities
if ($recordType == $enrolConfigAry['Club']) {
	$CurrentPage = "PageMgtClub";
	$tab_type = "club";
	$idFieldName = 'EnrolGroupID';
	$recordTypeTitle = $Lang['eEnrolment']['Club'];
	$backFile = 'group.php';
}
else if ($recordType == $enrolConfigAry['Activity']) {
	$CurrentPage = "PageMgtActivity";
	$tab_type = "activity";
	$idFieldName = 'EnrolEventID';
	$recordTypeTitle = $Lang['eEnrolment']['Activity'];
	$backFile = 'event.php';	
}


### Student extra data multiple selection
$optionAry = array();
$optionAry["UserLogin"] = array("UserLogin",$i_UserLogin);
$optionAry["UserEmail"] = array("UserEmail",$i_UserEmail);
$optionAry["EnglishName"] = array("EnglishName",$i_UserEnglishName);
$optionAry["ChineseName"] = array("ChineseName",$i_UserChineseName);
$optionAry["Gender"] = array("Gender",$i_UserGender);
$optionAry["MobileTelNo"] = array("MobileTelNo",$i_UserMobileTelNo);
$optionAry["DateOfBirth"] = array("DateOfBirth",$i_UserDateOfBirth);
$optionAry["HomeTelNo"] = array("HomeTelNo",$i_UserHomeTelNo);
$optionAry["OfficeTelNo"] = array("OfficeTelNo",$i_UserOfficeTelNo);
$optionAry["FaxNo"] = array("FaxNo",$i_UserFaxNo);
$optionAry["ICQNo"] = array("ICQNo",$i_UserICQNo);
$optionAry["Address"] = array("Address",$i_UserAddress);
$optionAry["Country"] = array("Country",$i_UserCountry);
$optionAry["CardID"] = array("CardID",$i_SmartCard_CardID);
$optionAry["WebSamsRegNo"] = array("WebSamsRegNo",$i_WebSAMS_Registration_No);
$optionAry["HKJApplNo"] = array("HKJApplNo",$Lang['AccountMgmt']['HKJApplNo']);
//$optionAry["WebSAMSStaffCode"] = array("StaffCode",$Lang['AccountMgmt']['StaffCode']);
if($special_feature['ava_hkid']) {
	$optionAry["HKID"] = array("HKID",$i_HKID);
} 
if($special_feature['ava_strn']) {
	$optionAry["STRN"] = array("STRN",$i_STRN);
}
$extraInfoSelection = getSelectByArray(array_values($optionAry)," name='studentExtraInfoFieldAry[]' id='extraInfoFieldSel' multiple size=10 style='width:400px;'", $default_selected ,0,1);
$selectAllExtraInfoBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('extraInfoFieldSel', 1);");


### Main Table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['eEnrolment']['ExportMemberExtraData'].'</td>'."\r\n";
		$x .= '<td>'.$linterface->Get_MultipleSelection_And_SelectAll_Div($extraInfoSelection, $selectAllExtraInfoBtn).'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['mainTable'] = $x;


### Buttons
$htmlAry['exportBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "goExport();", "submitBtn");
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();", "backBtn");


### UI Menu Display
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($recordTypeTitle, "javascript:goBack()"); 
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['ExportMember'], ""); 
$htmlAry['pageNavigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$current_tab = 1;
include_once("management_tabs.php");

$linterface->LAYOUT_START();
?>
<script language="javascript">
function goBack() {
	window.location = '<?=$backFile?>';
}
function goExport() {
	$('#form1').attr('action', 'export.php').submit();
}
</script>
<form id="form1" name="form1" method="post">
	<?=$htmlAry['pageNavigation']?>
	<br />
	
	<?=$htmlAry['mainTable']?>
	<br />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['exportBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="type" name="type" value="<?=$exportTypeCode?>" />
	<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="<?=$AcademicYearID?>" />
	<input type="hidden" id="category_conds" name="category_conds" value="<?=intranet_htmlspecialchars($category_conds)?>" />
	<input type="hidden" id="name_conds" name="name_conds" value="<?=intranet_htmlspecialchars($name_conds)?>" />
	<input type="hidden" id="semester_conds" name="semester_conds" value="<?=intranet_htmlspecialchars($semester_conds)?>" />
	<input type="hidden" id="fromExtraInfoCust" name="fromExtraInfoCust" value="1" />
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>