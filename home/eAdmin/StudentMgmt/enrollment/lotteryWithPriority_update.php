<?php
## Using By: ivan
######################################
##	Modification Log:
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition
######################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
global $debug_sql;
if ($plugin['eEnrollment'])
{

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$MODULE_OBJ['title'] = $eEnrollment['drawing'];
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

$libenroll->Start_Trans();

# Grab min requirement to temp table
$minmax = $libenroll->getFullMinMax();

$lclass = new libclass();

if($libenroll->UseCategorySetting)
{
	$CategoryIDArr = $libenroll->GET_CATEGORY_LIST();
	$CategoryIDArr = Get_Array_By_Key($CategoryIDArr, "CategoryID");
}
else
	$CategoryIDArr = array(0); // means do not use cateory setting

$CategorySetting = $libenroll->Get_Category_Enrol_Setting($CategoryIDArr);

$sql = "CREATE TEMPORARY TABLE TEMP_ENROL_REQUIRE(
         StudentID int(11),
         Min int(11)
         )";//20110823
$libenroll->db_db_query($sql);

$sql = "DELETE FROM TEMP_ENROL_REQUIRE";
$libenroll->db_db_query($sql);

foreach($CategoryIDArr as $thisCategoryID)
{
		//$overallMax = $libenroll->defaultMax;
	$overallMax = $CategorySetting[$thisCategoryID]['Club']['DefaultClub'];
	for ($i=0; $i<count($minmax); $i++)
	{
	     list($id,$min,$max) = $minmax[$i];
	     if ($max == "") continue;
	     if ($max > $overallMax) $overallMax = $max;
	     // - if ($max < $overallMax) $overallMax = $max;
	     
	     # Grab classname for this form
	     $classes = $lclass->returnClassListByLevel($id);
	     if(count($classes))
	     {
		     $delimiter = "";
		     $list = "";
		     for ($j=0; $j<count($classes); $j++)
		     {
		          list($id,$name) = $classes[$j];
		          $list .= "$delimiter '$name'";
		          $delimiter = ",";
		     }
		     $sql = "INSERT IGNORE INTO TEMP_ENROL_REQUIRE (StudentID,Min)
		             SELECT UserID, $min FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($list) AND ClassName != ''";
		     $libenroll->db_db_query($sql);//20110823
	     }
	}
	
	if ($overallMax == 0)
	{
		if($thisCategoryID==0)
	    	$sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT"; 
	    else
	    	$sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT_CATEGORY_ENROL WHERE CategoryID = '$thisCategoryID' ";
	    $result = $libenroll->returnVector($sql);
	    if ($result[0]!=0)
	    {
	        $overallMax = $result[0];
	    }
	}
	
	# Grab unsatisfied school requirement 
	$unsatSchool = $libenroll->Get_Student_Unsatisfy_School_Requirement($thisCategoryID);
	
	
	$i = 0;
//	if ($libenroll->tiebreak == 0)         # Tiebreaker
//	{
//	    $tiebreak_ord = "RAND()";
//	}
//	else  # 1
//	{
//	    $tiebreak_ord = "DateInput";
//	}
	
	while (count($unsatSchool)>0 && $i<$overallMax)
	{	   
		
//			$unsatList = implode(",",$unsatSchool);
			$i++;

	       if($WithPriority)
	       {
	       		$m = 1; 
	       		$Loop = $overallMax;
	       }
	       else
	       {
	       		$m = 0;
	       		$Loop = 0;
	       }
			
			for($m; $m<=$Loop; $m++) {	# check priority first
				
				# Retrieve Groups with spaces
				$groups = $libenroll->Retrieve_Groups_With_Spaces_For_Lottery($thisCategoryID, $m);
				
				$libenroll->Lottery_Arrange_Students_To_Groups($groups, $unsatSchool, $overallMax, $i, $SuccessArr);
				
//				for ($j=0; $j<count($groups); $j++)
//				{
//					$approvedStudents = array();
//					list ($EnrolGroupID, $groupQuota, $groupApproved) = $groups[$j];
//					$GroupQuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($EnrolGroupID);
//					$limit_str = "";
//					$sql = "SELECT EnrolGroupID, StudentID FROM INTRANET_ENROL_GROUPSTUDENT
//							WHERE StudentID IN ($unsatList) AND
//								EnrolGroupID = $EnrolGroupID AND Choice = $i AND RecordStatus = 0
//							ORDER BY $tiebreak_ord $limit_str";
//					
//					$TempStudents = $libenroll->returnArray($sql,2);
//					$numOfStudents = count($TempStudents);
//					#### filter student, check for time crash to a new array ####
//					for ($checkCrashCount = 0; $checkCrashCount < $numOfStudents; $checkCrashCount++) {	    
//						$thisStudentID = $TempStudents[$checkCrashCount][1];
//		            
//			            // check student will of joining how many clubs
//			            $studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($thisStudentID);
//						if ($lclub->disableCheckingNoOfClubStuWantToJoin == false && $studentQuotaLeft <= 0)
//						{
//							// student does not want to join so many clubs
//							continue;
//						}
//						        
//						$crash = 0;     	
//						$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($TempStudents[$checkCrashCount][1]);
//						$numOfStudentClub = count($StudentClub);
//						for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
//							if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash][0])) &&
//								(count($StudentClub) < $overallMax)) {
//								$crash = 1;
//								continue;
//							}
//						}
//						
//						// check for event group crash
//						// get event list
//						$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($TempStudents[$checkCrashCount][1]);
//						$numOfStudentActivity = count($StudentEvent);
//						for ($CountEvent = 0; $CountEvent < $numOfStudentActivity; $CountEvent++) {
//					    	if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent][0])) {
//								$crash = true;
//								continue;
//							}
//						}
//						
//						if ( (!$crash) && (!in_array($TempStudents[$checkCrashCount], $approvedStudents)) && ($GroupQuotaLeft > count($approvedStudents))) {
//							$approvedStudents[] = $TempStudents[$checkCrashCount];
//						}
//					}
//					#### filter student, check for time crash to a new array ####
//					
//					$delimiter = "";
//					$enrolIDList = "";
//					$studentIDList = "";
//					$numApproved = count($approvedStudents);
//					
//					for ($k=0; $k<$numApproved; $k++)
//					{
//						list($enrolID,$studentID) = $approvedStudents[$k];
//						
//						$SuccessArr[$enrolID][$studentID] = $libenroll->Add_Student_To_Club_Member_List($studentID, $enrolID);
//					}
//					
//				}
				
			
				
				
	   
			}
			# Grab unsatisfied school requirement again
			$unsatSchool = $libenroll->Get_Student_Unsatisfy_School_Requirement($thisCategoryID);
	}
	
	
	# Now have to match students' own requirement
	$unsatOwn = $libenroll->Get_Student_Unsatisfy_Max_Want($thisCategoryID);
	$i = 0;
	
	while (count($unsatOwn)>0 && $i<$overallMax)
	{
//	       $unsatOwnList = implode(",",$unsatOwn);
	       $i++;
	       
	       if($WithPriority)
	       {
	       		$m = 1; 
	       		$Loop = $overallMax;
	       }
	       else // 
	       {
	       		$m = 0;
	       		$Loop = 0;
	       }
	       
	       for($m; $m<=$Loop; $m++) {	# check priority first
		       
				# Retrieve Groups with spaces
				//$sql = "SELECT GroupID,Quota,Approved FROM INTRANET_ENROL_GROUPINFO WHERE (Quota > Approved OR Quota = 0 OR Quota IS NULL)";
//				$sql = "SELECT a.EnrolGroupID, a.Quota, a.Approved FROM INTRANET_ENROL_GROUPINFO a LEFT OUTER JOIN INTRANET_ENROL_GROUPSTUDENT b ON (a.EnrolGroupID=b.EnrolGroupID) WHERE (a.Quota > a.Approved OR a.Quota = 0 OR a.Quota IS NULL) AND b.Choice=$m";
//				$groups = $libenroll->returnArray($sql,3);
				$groups = $libenroll->Retrieve_Groups_With_Spaces_For_Lottery($thisCategoryID, $m);
				
				$libenroll->Lottery_Arrange_Students_To_Groups($groups, $unsatOwn, $overallMax, $i, $SuccessArr);
	
//				for ($j=0; $j<count($groups); $j++)
//				{
//					$approvedStudents = array();
//					list ($EnrolGroupID, $groupQuota, $groupApproved) = $groups[$j];
//					$GroupQuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($EnrolGroupID);
//					$limit_str = "";
//					$sql = "SELECT EnrolGroupID, StudentID FROM INTRANET_ENROL_GROUPSTUDENT
//							WHERE StudentID IN ($unsatOwnList) AND
//								EnrolGroupID = $EnrolGroupID AND Choice = $i AND RecordStatus = 0
//							ORDER BY $tiebreak_ord $limit_str";
//					$TempStudents = $libenroll->returnArray($sql,2);
//					$numOfStudent = count($TempStudents);
//							     
//					#### filter student, check for time crash to a new array ####
//					for ($checkCrashCount = 0; $checkCrashCount < $numOfStudent; $checkCrashCount++) {	  
//						$thisStudentID = $TempStudents[$checkCrashCount][1];
//		            
//			            // check student will of joining how many clubs
//			            $studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($thisStudentID);
//						if ($lclub->disableCheckingNoOfClubStuWantToJoin == false && $studentQuotaLeft <= 0)
//						{
//							// student does not want to join so many clubs
//							continue;
//						}
//					          
//						$crash = 0;     	
//						$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($TempStudents[$checkCrashCount][1]);
//						$numOfStudentClub = count($StudentClub);
//						for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
//							if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash][0])) &&
//					  			(count($StudentClub) < $overallMax)) {
//								$crash = 1;
//								continue;
//					    	}
//						}
//						# check for event group crash
//						# get event list
//						$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($TempStudents[$checkCrashCount][1]);
//						$numOfStudentActivity = count($StudentEvent);
//						for ($CountEvent = 0; $CountEvent < $numOfStudentActivity; $CountEvent++) {
//							if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent][0])) {
//								$crash = true;
//								continue;
//							}
//						}
//						
//						if ( (!$crash) && (!in_array($TempStudents[$checkCrashCount], $approvedStudents)) && ($GroupQuotaLeft > count($approvedStudents))) {
//							$approvedStudents[] = $TempStudents[$checkCrashCount];
//						}
//					}
//					#### filter student, check for time crash to a new array ####
//			
//					$delimiter = "";
//					$enrolIDList = "";
//					$studentIDList = "";
//					$numApproved = count($approvedStudents);
//					for ($k=0; $k<$numApproved; $k++)
//					{
//						list($enrolID,$studentID) = $approvedStudents[$k];
//						
//						$SuccessArr[$enrolID][$studentID] = $libenroll->Add_Student_To_Club_Member_List($studentID, $enrolID);
//					}
//	
//				}
	
				# Grab unsatisfied own requirement again
				$unsatOwn = $libenroll->Get_Student_Unsatisfy_Max_Want($thisCategoryID);
			}
	}
}
//debug_pr($libenroll->returnArray("SELECT * FROM INTRANET_ENROL_STUDENT_CATEGORY_ENROL")); 


///////////////////////////////////////////////////////////////////////////////////////////////////

////$overallMax = $libenroll->defaultMax;
//$overallMax = $libenroll->EnrollMax;
//for ($i=0; $i<count($minmax); $i++)
//{
//     list($id,$min,$max) = $minmax[$i];
//     if ($max == "") continue;
//     if ($max > $overallMax) $overallMax = $max;
//     // - if ($max < $overallMax) $overallMax = $max;
//     
//     # Grab classname for this form
//     $classes = $lclass->returnClassListByLevel($id);
//     $delimiter = "";
//     $list = "";
//     for ($j=0; $j<count($classes); $j++)
//     {
//          list($id,$name) = $classes[$j];
//          $list .= "$delimiter '$name'";
//          $delimiter = ",";
//     }
//     $sql = "INSERT IGNORE INTO TEMP_ENROL_REQUIRE (StudentID,Min)
//             SELECT UserID, $min FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($list) AND ClassName != ''";
//     $libenroll->db_db_query($sql);//20110823
//}
//
//if ($overallMax == 0)
//{
//    $sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT"; //20110823
//    $result = $libenroll->returnVector($sql);
//    if ($result[0]!=0)
//    {
//        $overallMax = $result[0];
//    }
//}
//
//$sql = "SELECT a.StudentID FROM INTRANET_ENROL_STUDENT as a, TEMP_ENROL_REQUIRE as b
//        WHERE a.StudentID = b.StudentID AND a.Approved < b.Min";
//$unsatSchool = $libenroll->returnVector($sql); //20110823
//
//$i = 0;
//if ($libenroll->tiebreak == 0)         # Tiebreaker
//{
//    $tiebreak_ord = "RAND()";
//}
//else  # 1
//{
//    $tiebreak_ord = "DateInput";
//}
//
//
//while (count($unsatSchool)>0 && $i<$overallMax)
//{	   
//	
//		$unsatList = implode(",",$unsatSchool);
//		$i++;
//		
//		# 
//	
//		for($m=1; $m<=$overallMax; $m++) {	# check priority first
//	
//			# Retrieve Groups with spaces
//			$sql = "SELECT a.EnrolGroupID, a.Quota, a.Approved FROM INTRANET_ENROL_GROUPINFO a LEFT OUTER JOIN INTRANET_ENROL_GROUPSTUDENT b ON (a.EnrolGroupID=b.EnrolGroupID) WHERE (a.Quota > a.Approved OR a.Quota = 0 OR a.Quota IS NULL) AND b.Choice=$m";
//			$groups = $libenroll->returnArray($sql,3);
//			
//			
//			for ($j=0; $j<count($groups); $j++)
//			{
//				$approvedStudents = array();
//				list ($EnrolGroupID, $groupQuota, $groupApproved) = $groups[$j];
//				$GroupQuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($EnrolGroupID);
//				$limit_str = "";
//				$sql = "SELECT EnrolGroupID, StudentID FROM INTRANET_ENROL_GROUPSTUDENT
//						WHERE StudentID IN ($unsatList) AND
//							EnrolGroupID = $EnrolGroupID AND Choice = $i AND RecordStatus = 0
//						ORDER BY $tiebreak_ord $limit_str";
//				
//				$TempStudents = $libenroll->returnArray($sql,2);
//				$numOfStudents = count($TempStudents);
//				#### filter student, check for time crash to a new array ####
//				for ($checkCrashCount = 0; $checkCrashCount < $numOfStudents; $checkCrashCount++) {	    
//					$thisStudentID = $TempStudents[$checkCrashCount][1];
//	            
//		            // check student will of joining how many clubs
//		            $studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($thisStudentID);
//					if ($lclub->disableCheckingNoOfClubStuWantToJoin == false && $studentQuotaLeft <= 0)
//					{
//						// student does not want to join so many clubs
//						continue;
//					}
//					        
//					$crash = 0;     	
//					$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($TempStudents[$checkCrashCount][1]);
//					$numOfStudentClub = count($StudentClub);
//					for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
//						if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash][0])) &&
//							(count($StudentClub) < $overallMax)) {
//							$crash = 1;
//							continue;
//						}
//					}
//					
//					// check for event group crash
//					// get event list
//					$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($TempStudents[$checkCrashCount][1]);
//					$numOfStudentActivity = count($StudentEvent);
//					for ($CountEvent = 0; $CountEvent < $numOfStudentActivity; $CountEvent++) {
//				    	if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent][0])) {
//							$crash = true;
//							continue;
//						}
//					}
//					
//					if ( (!$crash) && (!in_array($TempStudents[$checkCrashCount], $approvedStudents)) && ($GroupQuotaLeft > count($approvedStudents))) {
//						$approvedStudents[] = $TempStudents[$checkCrashCount];
//					}
//				}
//				#### filter student, check for time crash to a new array ####
//				
//				$delimiter = "";
//				$enrolIDList = "";
//				$studentIDList = "";
//				$numApproved = count($approvedStudents);
//				
//				for ($k=0; $k<$numApproved; $k++)
//				{
//					list($enrolID,$studentID) = $approvedStudents[$k];
//					
//					$SuccessArr[$enrolID][$studentID] = $libenroll->Add_Student_To_Club_Member_List($studentID, $enrolID);
//				}
//				
//			}
//			
//		
//			# Grab unsatisfied school requirement again
//			$sql = "SELECT a.StudentID FROM INTRANET_ENROL_STUDENT as a, TEMP_ENROL_REQUIRE as b
//					WHERE a.StudentID = b.StudentID AND a.Approved < b.Min";
//			$unsatSchool = $libenroll->returnVector($sql);
//   
//		}
//}
//
//
//# Now have to match students' own requirement
//$unsatOwn = $libenroll->Get_Student_Unsatisfy_Max_Want();
//$i = 0;
//
//while (count($unsatOwn)>0 && $i<$overallMax)
//{
//       $unsatOwnList = implode(",",$unsatOwn);
//       $i++;
//       
//       for($m=1; $m<=$overallMax; $m++) {	# check priority first
//	       
//			# Retrieve Groups with spaces
//			//$sql = "SELECT GroupID,Quota,Approved FROM INTRANET_ENROL_GROUPINFO WHERE (Quota > Approved OR Quota = 0 OR Quota IS NULL)";
//			$sql = "SELECT a.EnrolGroupID, a.Quota, a.Approved FROM INTRANET_ENROL_GROUPINFO a LEFT OUTER JOIN INTRANET_ENROL_GROUPSTUDENT b ON (a.EnrolGroupID=b.EnrolGroupID) WHERE (a.Quota > a.Approved OR a.Quota = 0 OR a.Quota IS NULL) AND b.Choice=$m";
//			$groups = $libenroll->returnArray($sql,3);
//
//			for ($j=0; $j<count($groups); $j++)
//			{
//				$approvedStudents = array();
//				list ($EnrolGroupID, $groupQuota, $groupApproved) = $groups[$j];
//				$GroupQuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($EnrolGroupID);
//				$limit_str = "";
//				$sql = "SELECT EnrolGroupID, StudentID FROM INTRANET_ENROL_GROUPSTUDENT
//						WHERE StudentID IN ($unsatOwnList) AND
//							EnrolGroupID = $EnrolGroupID AND Choice = $i AND RecordStatus = 0
//						ORDER BY $tiebreak_ord $limit_str";
//				$TempStudents = $libenroll->returnArray($sql,2);
//				$numOfStudent = count($TempStudents);
//						     
//				#### filter student, check for time crash to a new array ####
//				for ($checkCrashCount = 0; $checkCrashCount < $numOfStudent; $checkCrashCount++) {	  
//					$thisStudentID = $TempStudents[$checkCrashCount][1];
//	            
//		            // check student will of joining how many clubs
//		            $studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($thisStudentID);
//					if ($lclub->disableCheckingNoOfClubStuWantToJoin == false && $studentQuotaLeft <= 0)
//					{
//						// student does not want to join so many clubs
//						continue;
//					}
//				          
//					$crash = 0;     	
//					$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($TempStudents[$checkCrashCount][1]);
//					$numOfStudentClub = count($StudentClub);
//					for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
//						if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash][0])) &&
//				  			(count($StudentClub) < $overallMax)) {
//							$crash = 1;
//							continue;
//				    	}
//					}
//					# check for event group crash
//					# get event list
//					$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($TempStudents[$checkCrashCount][1]);
//					$numOfStudentActivity = count($StudentEvent);
//					for ($CountEvent = 0; $CountEvent < $numOfStudentActivity; $CountEvent++) {
//						if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent][0])) {
//							$crash = true;
//							continue;
//						}
//					}
//					
//					if ( (!$crash) && (!in_array($TempStudents[$checkCrashCount], $approvedStudents)) && ($GroupQuotaLeft > count($approvedStudents))) {
//						$approvedStudents[] = $TempStudents[$checkCrashCount];
//					}
//				}
//				#### filter student, check for time crash to a new array ####
//		
//				$delimiter = "";
//				$enrolIDList = "";
//				$studentIDList = "";
//				$numApproved = count($approvedStudents);
//				for ($k=0; $k<$numApproved; $k++)
//				{
//					list($enrolID,$studentID) = $approvedStudents[$k];
//					
//					$SuccessArr[$enrolID][$studentID] = $libenroll->Add_Student_To_Club_Member_List($studentID, $enrolID);
//				}
//
//			}
//
//			# Grab unsatisfied own requirement again
//			$unsatOwn = $libenroll->Get_Student_Unsatisfy_Max_Want();
//		}
//}


$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 1 WHERE RecordStatus = 0";
$libenroll->db_db_query($sql);

// send email
$laccess = new libaccess();
if ($laccess->retrieveAccessCampusmailForType(2) && $libenroll->onceEmail == 1)     # if can access campusmail
{	
	//get all groups
	$groupArr = $libenroll->getGroupInfoList($isAll=1, $Semester='', $EnrolGroupIDArr='', $getRegOnly=1);
	$numOfClub = count($groupArr);
	
	// loop through all groups
	for ($j=0; $j<$numOfClub; $j++)
	{
		$thisEnrolGroupID = $groupArr[$j]['EnrolGroupID'];
		$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
						
		//check if the club is using the enrolment system
		/* checked in function getGroupInfoList() already
		$sql = "SELECT Quota FROM INTRANET_ENROL_GROUPINFO WHERE GroupID = '$thisGroupID'";
		$result = $libenroll->returnArray($sql,1);
		if ($result[0][0] == "")
		{
			//ignore clubs that are nor using enrolment system
			continue;
		}
		*/
				
		//send email to all students who have applied for this club
		$sql = "SELECT StudentID, RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$thisEnrolGroupID'";
		$students = $libenroll->returnArray($sql,1);
		$numOfClubMember = count($students);	
	
		for ($i=0; $i<$numOfClubMember; $i++)
		{
			$thisStudentID = $students[$i]['StudentID'];
			$thisRecordStatus = $students[$i]['RecordStatus'];
			
			# define record type
			$RecordType = "club";
			
			# get student list
			$userArray = array();			
			$userArray[] = $students[$i][0];
			
			# get success result
			$successResult = false;
			
			// commented by Ivan 20100226 - Select the RecordStatus in previous SQL so that less SQL is run
			//$sql = "SELECT RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE GroupID = '$GroupID' AND StudentID = '".$students[$i][0]."'";
			//$choices = $libenroll->returnArray($sql,1);
			
			if ($thisRecordStatus == 2) {
				$successResult = true;
			} else if ($thisRecordStatus == 1){
				$successResult = false;
			} else {
				// do nothing
			}
			
			$libenroll->Send_Enrolment_Result_Email($userArray, $thisGroupID, $RecordType, $successResult);
		}
		//debug_r($recipient);
		
	}
}

$libenroll->RollBack_Trans();
intranet_closedb();


################################################################

$linterface->LAYOUT_START();

?>

<div style="height: 400px;">
<br/><br/><br/><br/>
<?=$i_ClubsEnrollment_LotteryFinished?><br/><br/>
<?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
</div>
<script type="text/javascript">
	window.opener.location.reload();
</script>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>