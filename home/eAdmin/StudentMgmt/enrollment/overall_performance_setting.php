<?php

# using: 

##### Change Log [Start] #####
#
#	Date	:	2015-06-02	Evan
#				Update the style according to UI standard
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'] && !$plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	$CurrentPage = "PagePerformanceSetting";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['overall_perform'], "", 1);

        $linterface->LAYOUT_START();
        
        ($libenroll->overallPerformance == 1)? $chkEnable = "checked" : $chkDisabled = "checked";
        
        
    ?>
    
          
    <form name="form1" action="overall_performance_setting_update.php" method="post" enctype="multipart/form-data">
		<table width="96%" border="0" cellpadding="4" cellspacing="0" align="center">
			<tr>
				<td width="30%" class="tabletext" align="left">&nbsp;</td>
				<td width="70%" class="tabletext" align="right">
					<? if ($msg == 2) print $linterface->GET_SYS_MSG("update"); ?>
				</td>
			</tr>
		</table>
		
		<!-- Instruction -->
		<?= $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $eEnrollment['overall_performance_instruction']); ?>
					
		<!-- Mode Selection -->
		<table width="90%" align="center">
			<tr>
				<td>
					<table class="form_table_v30" width="85%">
						<tr>
							<td class="field_title">
								<?= $eEnrollmentMenu['overall_perform_report'] ?>
							</td>
							<td class="tablerow tabletext">
								<input type="radio" id="mode0" name="overallSetting" value="0" <?=$chkDisabled?>><label for="mode0"><?=$i_general_disabled?></label>
								<?= toolBarSpacer() ?>
								<input type="radio" id="mode1" name="overallSetting" value="1" <?=$chkEnable?>><label for="mode1"><?=$i_general_enabled?></label>
							</td>
						</tr>
					</table>	
				</td>
			</tr>
		</table>
		<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
			<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr><td align="center" colspan="6">
				<div style="padding-top: 5px">
					<?= $linterface->GET_ACTION_BTN($button_save, "button", "javascript:document.form1.submit();")?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
				</div>
			</td></tr>
		</table>
	</form>
			

        
    <?  
        $linterface->LAYOUT_STOP();
	}
	else
	{
	?>
		You have no priviledge to access this page.
	<?
	}
}
else
{
	intranet_closedb();
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
?>