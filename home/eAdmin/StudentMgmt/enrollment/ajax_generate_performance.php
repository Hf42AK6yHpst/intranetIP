<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$linterface = new interface_html();
$lg = new libgeneralsettings();
$lc = new libclubsenrol_cust();

$AcademicYearID = Get_Current_Academic_Year_ID();
$attendance_mark = 10;

$msg = "";

$UserID_str = "";
if($type=="club")
{
	## clear all club performance
	$lc->clearClubPerformance();
	
	$result = $lc->countClubAttendance_AllClubs();
	if(isset($result) && !empty($result))
	{
		foreach($result	 as $k=>$d)
		{
			list($tmpUserID, $tmpEnrolGroupID, $tmpCount) = $d;
			$newPerformance = $tmpCount * $attendance_mark;
			$sql = "update INTRANET_USERGROUP set Performance='$newPerformance' where EnrolGroupID='$tmpEnrolGroupID' AND UserID='$tmpUserID'";
			$libenroll->db_db_query($sql);
			
			$UserID_str .= $tmpUserID.",";
		}
	}
	
	$datetime = date("Y-m-d H:i:s");
	# store generate datetime
	$dataAry["Club_Generate_Performance_DateTime"] = $datetime;
	$dataAry["Club_Generate_Performance_By"] = $UserID;
	$lg->Save_General_Setting("eEnrolment", $dataAry);

	$msg = $Lang['eEnrolment']['GeneratePerformanceCompleted'] ."<br><br>";
}
else if($type=="activity")
{
	## clear all activity performance
	$lc->clearActivityPerformance();
	
	$result = $lc->countActivityAttendance_AllActivities();
	if(isset($result) && !empty($result))
	{
		foreach($result	 as $k=>$d)
		{
			list($tmpUserID, $tmpEnrolEventID, $tmpCount) = $d;
			$newPerformance = $tmpCount * $attendance_mark;
			$sql = "update INTRANET_ENROL_EVENTSTUDENT set Performance='$newPerformance' where EnrolEventID='$tmpEnrolEventID' AND StudentID='$tmpUserID'";
			$libenroll->db_db_query($sql);
			
			$UserID_str .= $tmpUserID.",";
		}
	}
	
	$datetime = date("Y-m-d H:i:s");
	# store generate datetime
	$dataAry["Activity_Generate_Performance_DateTime"] = $datetime;
	$dataAry["Activity_Generate_Performance_By"] = $UserID;
	$lg->Save_General_Setting("eEnrolment", $dataAry);
	
	$msg = $Lang['eEnrolment']['GeneratePerformanceCompleted'] ."<br><br>";
}

if($msg)
{
	$UserID_str = substr($UserID_str, 0 , strlen($UserID_str)-1);
	
	##### update total performance
	# retrieve club perform 
 	$result = $lc->returnClubPerformanceByUserIDs($UserID_str);
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['UserID']] = $d['performance'] ? $d['performance'] : 0;
	}
			
 	# retrieve activity perform 
	$result = $lc->returnActivityPerformanceByUserIDs($UserID_str);
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['StudentID']] += $d['performance'] ? $d['performance'] : 0;
	}
	
	# retrieve adjuestment
	$result = $lc->returnAdjustmentByUserIDs($UserID_str);
	$adj_ary = array();
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
 		{
			$adj_ary[$d['UserID']] = $d['Adjustment'];
		}
	}
	$UserID_Ary = explode(",",$UserID_str);
	foreach($UserID_Ary as $k=>$d)
	{
		$this_UserID = $k;
		$this_performance = $performance_ary[$this_UserID] ? $performance_ary[$this_UserID] : 0;
		$this_Adjustment = $adj_ary[$this_UserID] ;
		$this_total = $this_performance + $this_Adjustment;
		
		$dataAry = array();
		$dataAry['UserID'] = $this_UserID;
		$dataAry['this_Adjustment'] = $this_Adjustment;
		$dataAry['this_total'] = $this_total;
		
		$lc->InsertUpdateStudentTotalPerformance($dataAry);
	}
	
	$lastGenBy_tmp = $libenroll->returnUserName($UserID);
	$lastGenBy = $lastGenBy_tmp[0];
	
	$x .= "<span>". $Lang['eEnrolment']['LastGenerated'] .": ". $datetime ." (". $lastGenBy .")</span>";
}
else
{
	$msg = $Lang['eEnrolment']['GeneratePerformanceFailed'] ."<br><br>";
}

$x .= "<p class=\"spacer\"></p>";
$x .= $msg;
$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "click_back();");
$x .= "<p class=\"spacer\"></p>";
	
echo $x;

intranet_closedb();
?>