<?php

// Using: 

/************************************
 * Date:	2017-09-01 (Anna)
 * Detaols: added $thisNoActiveReason
 * 
 * Date: 	2013-05-30 (Rita)
 * Details:	Change $sql to call function
 ************************************/
 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->HAVE_CLUB_MGT() && !$libenroll->HAVE_EVENT_MGT()  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	header("Location: {$PATH_WRT_ROOT}home/eAdmin/StudentMgmt/enrollment/");

if ($plugin['eEnrollment'])
{
	//check if update active member percentage only
	if ($isUpdatePercentage == 1)
	{
		if ($type=="club")
		{
			
			# update group's active member percentage
//			$sql = "UPDATE INTRANET_ENROL_GROUPINFO SET ActiveMemberPercentage = '$activeMemberPer' WHERE EnrolGroupID = '$EnrolGroupID'";
//			$libenroll->db_db_query($sql);
			
			# Update group's active member percentage
			$libenroll->Update_Club_Active_Member_Percentage($activeMemberPer, $EnrolGroupID);
			
			# update student active member status
			//get all studentIDs of the group
	        $sql = "SELECT b.UserID
	        		FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_USER as b ON a.UserID = b.UserID
	        		WHERE a.EnrolGroupID = '$EnrolGroupID' AND b.RecordType = 2";
	        $StudentArray = $libenroll->returnVector($sql);
	        
	        //get student attendance to check if one is an active member or not
	        $activeArr = array();
	        $inactiveArr = array();
	        for ($i=0; $i<sizeof($StudentArray); $i++)
        	{
		   		$DataArr['StudentID'] = $StudentArray[$i];
		   		//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
		    	$DataArr['EnrolGroupID'] = $EnrolGroupID;
		    	
	        	$attendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
	        		        	
	        	if ($attendance >= $activeMemberPer)
	        	{
		        	$activeArr[] = $DataArr['StudentID'];
	        	}
	        	else
	        	{
		        	$inactiveArr[] = $DataArr['StudentID'];
	        	}
        	}
        	
        	# update database
        	if (sizeof($activeArr) > 0)
        	{
//	        	$activeList = implode(",", $activeArr);
//	        	$sql = "UPDATE INTRANET_USERGROUP SET isActiveMember = 1, DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."'  WHERE EnrolGroupID = ".$EnrolGroupID." AND UserID IN (".$activeList.")";
//	        	$libenroll->db_db_query($sql);
				$result = $libenroll->Update_Club_Active_Member_Status($EnrolGroupID, $activeArr, $thisIsActiveValue=1);     
        	}
        	if (sizeof($inactiveArr) > 0)
        	{
//	        	$inactiveList = implode(",", $inactiveArr);
//	        	$sql = "UPDATE INTRANET_USERGROUP SET isActiveMember = 0, DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."'  WHERE EnrolGroupID = ".$EnrolGroupID." AND UserID IN (".$inactiveList.")";
//	        	$libenroll->db_db_query($sql);
				$result = $libenroll->Update_Club_Active_Member_Status($EnrolGroupID, $inactiveArr, $thisIsActiveValue=0);			
        	}      

			
						
			# Update student active member status
      		//$libenroll->Update_Club_Student_Active_Member_Status($EnrolGroupID, $activeMemberPer); 
  	
		}
		else if ($type=="activity")
		{
//			# update activity's active member percentage
//			$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET ActiveMemberPercentage = $activeMemberPer WHERE EnrolEventID = $EnrolEventID";
//			$libenroll->db_db_query($sql);

			# Update Activity Active Member Percentage
			$libenroll->Update_Event_Active_Member_Percentage($activeMemberPer,$EnrolEventID);	
		
			# update student active member status
			//get all studentIDs of the activity
	        $sql = "SELECT StudentID
	        		FROM INTRANET_ENROL_EVENTSTUDENT
	        		WHERE EnrolEventID = '$EnrolEventID'";
	        $StudentArray = $libenroll->returnVector($sql);
	        
	        //get student attendance to check if one is an active member or not
	        $activeArr = array();
	        $inactiveArr = array();
	        for ($i=0; $i<sizeof($StudentArray); $i++)
        	{
		   		$DataArr['StudentID'] = $StudentArray[$i];
		    	$DataArr['EnrolEventID'] = $EnrolEventID;
		    	
	        	$attendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);	        	
	        	if ($attendance >= $activeMemberPer)
	        	{
		        	$activeArr[] = $DataArr['StudentID'];
	        	}
	        	else
	        	{
		        	$inactiveArr[] = $DataArr['StudentID'];
	        	}
        	}
        	
        	# update database
        	if (sizeof($activeArr) > 0)
        	{
        		
//	        	$activeList = implode(",", $activeArr);
//	        	$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET isActiveMember = 1, DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."'  WHERE EnrolEventID = ".$EnrolEventID." AND StudentID IN (".$activeList.")";
//	        	$libenroll->db_db_query($sql);
				
				$result = $libenroll->Update_Event_Active_Member_Status($EnrolEventID, $activeArr, $thisIsActiveValue=1);    
        		
        	}
        	if (sizeof($inactiveArr) > 0)
        	{
//	        	$inactiveList = implode(",", $inactiveArr);
//	        	$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET isActiveMember = 0, DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."'  WHERE EnrolEventID = ".$EnrolEventID." AND StudentID IN (".$inactiveList.")";
//	        	$libenroll->db_db_query($sql);

				$result = $libenroll->Update_Event_Active_Member_Status($EnrolEventID, $inactiveArr, $thisIsActiveValue=0);	 
        	}    

				
       		
       		# Update Student Active Member Status
       	//	$libenroll->Update_Event_Student_Active_Member_Status($EnrolEventID, $activeMemberPer);
       	
       		
       		
   
		}
		
	}
	else
	{
		if ($type=="club")
		{			
			
			//update active member status for each student
			for ($i=0; $i<$StudentSize; $i++)
			{
//				$sql = "UPDATE INTRANET_USERGROUP SET isActiveMember = ".$radioID[$i].", DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."'  WHERE EnrolGroupID = $EnrolGroupID AND UserID = ".${"UID".$i};
//				$libenroll->db_db_query($sql);
				$thisStudentID = ${"UID".$i} ;
				
				if($sys_custom['eEnrolment']['UnitRolePoint']==true){
					$thisNoActiveReason = $radioText[$i];
					if($radioID[$i] == '1'){
						$thisNoActiveReason = '';
					}
	
					$result =  $libenroll->Update_Club_Active_Member_Status($EnrolGroupID, $thisStudentID, $radioID[$i],$thisNoActiveReason);
				}else{
					$result = $libenroll->Update_Club_Active_Member_Status($EnrolGroupID, $thisStudentID, $radioID[$i]);
				}
			
			}
		}
		else if ($type=="activity")
		{
			//update active member status for each student
			for ($i=0; $i<$StudentSize; $i++)
			{
//				$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET isActiveMember = ".$radioID[$i].", DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."'  WHERE EnrolEventID = $EnrolEventID AND StudentID = ".${"UID".$i};
//				$libenroll->db_db_query($sql);
				$thisStudentID = ${"UID".$i} ;
				$result = $libenroll->Update_Event_Active_Member_Status($EnrolEventID, $thisStudentID, $radioID[$i]);
			

			}
		}
		
	}
	
	
	intranet_closedb();
	
	if ($type=="club")
	{
		header("Location: active_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type&msg=2");
	}
	else if ($type=="activity")
	{
		header("Location: active_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type&msg=2");
	}
	
}

else
{
?>
You have no priviledge to access this page.
<?
}
?>