<?php
// Using: Rita

/***********************************
 * Date: 2012-12-20 Rita
 * Details: create this page for fill in approval reason
 * 
 * 
 ***********************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$linterface = new interface_html();
$libdb = new libdb();

$libuser = new libuser();

# Check If Has Access Right 
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID) && !$libgroup->hasAdminBasicInfo($UserID) && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
{
	//header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
	
# Obtain POST Value
$EnrolEventID = $_POST['EnrolEventID'];
$type = $_POST['type'];
$uid = $_POST['uid'];

/*
 *	type=0 => add club member with Role selection (from member_index.php)
 *  type=1 => add activity student with RecordStatus selection (from event_member.php)
 *  type=2 => add club member with RecordStatus selection (from group_member.php)
 *  type=3 => add activity student with Role selection (from event_member_index.php)
 */

if ($type==0 || $type==2 || $from=="ClubEnrolList"){	//from club enrolment
	$CurrentPage = "PageMgtClub";
	$tab_type = "club";
	if ($type==0)
    {
        $current_tab = 1;
    }
    else
    {
        $current_tab = 2;
    }
    $oneEnrol = $libenroll->oneEnrol;
}
else if ($type==1 || $type==3 || $from=="EventEnrolList"){	//from activity enrolment
	$CurrentPage = "PageMgtActivity";
	$tab_type = "activity";
    if ($type==3)
    {
        $current_tab = 1;
    }
    else
    {
        $current_tab = 2;
    }
    $oneEnrol = $libenroll->Event_oneEnrol;
}		
include_once("management_tabs.php");

# Module
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Page Navigation
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['EnrolReason']['FillInReason'], "");

# Step 
$STEPS_OBJ[] = array($eEnrollment['select_student'], 0);
$STEPS_OBJ[] = array($Lang['eEnrolment']['EnrolReason']['FillInReason'], 1);

$linterface->LAYOUT_START();

# Display Table Header
$fillInReasonTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$fillInReasonTable .= "<tr>";
	$fillInReasonTable .= "<td width=\"10\" class=\"tablebluetop tabletopnolink\">#</td>\n";
	$fillInReasonTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">$i_general_class</td>\n";
	$fillInReasonTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['class_no']}</td>\n";
	$fillInReasonTable .= "<td width=\"15%\" class=\"tablebluetop tabletopnolink\">$i_UserEnglishName</td>\n";
	$fillInReasonTable .= "<td width=\"15%\" class=\"tablebluetop tabletopnolink\">$i_UserChineseName</td>\n";
	$fillInReasonTable .= "<td width\"15%\" class=\"tablebluetop tabletopnolink\">" . $Lang['eEnrolment']['Reason']. "</td>\n";
	$fillInReasonTable .= "<td width\"15%\" class=\"tablebluetop tabletopnolink\">$i_SAMS_import_error_reason</td>\n";
$fillInReasonTable .= "</tr>";

# Obtain User Info
$UserInfoArray = $libuser->returnUser($userID="",$userLogin="", $uid);
$numOfUserArr = count($uid);
$errorMsgArr = array();

# Loop Each User
for ($i = 0; $i<$numOfUserArr; $i++) {
	$errorMsg = '';
	$eventStudentEnrolInfo = array();
	$displayNo =  $i + 1;
	$thisUserID = $UserInfoArray[$i]['UserID'];
	$thisClassNumber = $UserInfoArray[$i]['ClassNumber'];
	$thisClassName = $UserInfoArray[$i]['ClassName'];
	$thisEngName =  $UserInfoArray[$i]['EnglishName'];
	$thisChiName =  $UserInfoArray[$i]['ChineseName'];

	# Check If The Student Is Rejected Already
	$eventStudentEnrolInfo = $libenroll->GET_EVENTSTUDENT($EnrolEventID , $thisUserID);
	$recordStatus = $eventStudentEnrolInfo[0]['RecordStatus'];
	$rejectReasonValue = $eventStudentEnrolInfo[0]['Reason'];
	
//	if($recordStatus == '1'){
//		$errorMsg = $Lang['eEnrolment']['RejectError']['ApplicationHasBeenRejectedAlready'];
//		$errorMsgArr[]= $errorMsg;
//		$rejectReasonTextBoxDisplay = $rejectReasonValue;
//	}else{	
//		$rejectReasonTextBoxDisplay = $linterface->GET_TEXTBOX('rejectReason_'.$StudentID, 'rejectReason[]'.$StudentID, $rejectReasonValue, $OtherClass='', $OtherPar=array()) ; 
//	}

	if($recordStatus == '1'){
		$errorMsg = $Lang['eEnrolment']['RejectError']['ApplicationHasBeenRejectedAlready'];
		$errorMsgArr[]= $errorMsg;
	}
		$rejectReasonTextBoxDisplay = $linterface->GET_TEXTBOX('rejectReason_'.$StudentID, 'rejectReason[]'.$StudentID, $rejectReasonValue, $OtherClass='', $OtherPar=array()) ; 
	

	# Display Each Record
	$fillInReasonTable .= "<tr>";
		$fillInReasonTable .= "<td>" . $displayNo . "</td>";
		$fillInReasonTable .= "<td>" . $thisClassName . "</td>";
		$fillInReasonTable .= "<td>" . $thisClassNumber . "</td>";
		$fillInReasonTable .= "<td>" . $thisEngName . "</td>";
		$fillInReasonTable .= "<td>" . $thisChiName . "</td>";	
				
		$fillInReasonTable .= "<td>";
			$fillInReasonTable .= $rejectReasonTextBoxDisplay;
		$fillInReasonTable .= "</td>";	
		
		$fillInReasonTable .= "<td><font color='red'>" . $errorMsg . "</td>";
	$fillInReasonTable .= "</tr>";
}

$fillInReasonTable .= "</table>";


# Set Target Submission Page
$TargetFile = 'event_stu_remove_update.php';


# Action Button 
$buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript:submitEnrolReason(document.form1, '".$TargetFile."')")."&nbsp;";
//$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_applicant_list'], "button", "window.location = 'event_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";

?>


<script>
function submitEnrolReason(obj,page)
{   
   	obj.action=page;
    obj.method="POST";
    obj.submit();
 	return;   
}
</script>

<br />


<form name="form1" method="post" action="event_stu_remove_update.php">
	<table width="100%" border="0" cellspacing="4" cellpadding="4">
		<tr>
			<td>
				<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
			</td>
		</tr>
	</table>
	
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
		    </td>
		</tr>
		<tr>
		    <td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center">
		    	<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
				                	<td colspan="2"><?=$fillInReasonTable?></td>
								</tr>							
								<tr>
				                	<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
		    </td>
		</tr>
		<tr>
			<td>        
		    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		        	<tr>
		            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		            </tr>
		            <tr>
						<td align="center" colspan="2">
						<?= $buttons ?>
						</td>
					</tr>
			    </table>                                
			</td>
		</tr>
	</table>
	
	    <input type="hidden" name="uid" value="<?=rawurlencode(serialize($uid));?>">
		<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="filter" value="<?=$filter?>">
		
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
