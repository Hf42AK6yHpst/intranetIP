<?php

## Using By :

###########################################
## Modification Log:
## 
## 2017-10-13 (Simon) [B115479]
##  Send email to class teacher and PIC
##
## 2015-09-24 (Omas)
##	add column for seperating parent and student selection and add attendance rate
##
## 2013-11-01 (Henry)
##			File created
##
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();
$libuser = new libuser();

//$academicYearId = $_POST['academicYearId'];
//$yearTermId = $_POST['yearTermId'];
$groupDateID = IntegerSafe($_POST['groupDateID']);
$enrolGroupID = IntegerSafe($_POST['enrolGroupID']);
$queryStr = $_POST['queryStr'];
$eventDateID = IntegerSafe($_POST['eventDateID']);
$enrolEventID = IntegerSafe($_POST['enrolEventID']);

if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false/* || ($action!="view" && $canEdit==false)*/)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

### get the absent student list

if($groupDateID){
	$sql = "Select
				iega.StudentID, iug.ActivityDateStart, iega.GroupID
			From
				INTRANET_ENROL_GROUP_ATTENDANCE as iega
				Inner Join INTRANET_ENROL_GROUP_DATE as iug On (iega.GroupDateID = iug.GroupDateID And iega.EnrolGroupID = iug.EnrolGroupID)
			Where
				iega.GroupDateID = '".$groupDateID."'
				AND iega.EnrolGroupID = '".$enrolGroupID."'
				AND iega.RecordStatus = 3
			";
}
else{
	$sql = "Select
				iega.StudentID, iegd.ActivityDateStart, iega.EnrolEventID
			From
				INTRANET_ENROL_EVENT_DATE as iegd
				Inner Join
				INTRANET_ENROL_EVENT_ATTENDANCE as iega On (iegd.EventDateID = iega.EventDateID And iegd.EnrolEventID = iega.EnrolEventID)
			Where
				iega.RecordStatus = 3
				And iega.EventDateID = '".$eventDateID."' 
				And iega.EnrolEventID = '".$enrolEventID."'
			";
}



$AttendanceDataArr = $libenroll->returnArray($sql);

# Get HTML editor
$msg_box_width = 600;
$msg_box_height = 200;
$obj_name = "emailContent";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$oFCKeditor = new FCKeditor($obj_name,$editor_width,$editor_height);
//$oFCKeditor->Value =  $EnrollGroupArr['Description'];
$HTMLEditor = $oFCKeditor->Create2();

### table of absent student list
$diaplay = "";
//$display .=$libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['AbsentStudent']);
$display .= "<table width=\"100%\" cellpadding=\"5\" cellspacing=\"0\">";
$display .= "<tr>";
$display .= "<td width=\"1\" warp=\"nowrap\" class=\"tablebluetop tabletopnolink\">#</td>
			<td width=\"20%\" warp=\"nowrap\" class=\"tablebluetop tabletopnolink\">".$i_UserClassName."</td>
			<td width=\"20%\" warp=\"nowrap\" class=\"tablebluetop tabletopnolink\">".$i_UserClassNumber."</td>
			<td width=\"30%\" class=\"tablebluetop tabletopnolink\">".$i_UserStudentName."</td>
			<td width=\"1\" class=\"tablebluetop tabletopnolink\"><input type=\"checkbox\" onclick=\"(this.checked)?setChecked(1,this.form,'userIdAry[]'):setChecked(0,this.form,'userIdAry[]')\" name=\"checkmaster\" checked></input></td>
			<td width=\"30%\" class=\"tablebluetop tabletopnolink\">".$Lang['eEnrolment']['ParentName']."</td>
			<td width=\"1\" class=\"tablebluetop tabletopnolink\"><input type=\"checkbox\" onclick=\"(this.checked)?setChecked(1,this.form,'parentIdAry[]'):setChecked(0,this.form,'parentIdAry[]')\" name=\"checkmaster\" checked></input></td>
			";
$display .= "</tr>";

$studentArr = array();
$x ="";
$j = 0;
$hasAtLeastPG = 0;
foreach($AttendanceDataArr as $AttendanceData){
	$parentInfo = $libuser->getParent($AttendanceData['StudentID']);
	if($parentInfo){
		for($i=0; $i<count($parentInfo); $i++){
			$tr_css = ($j % 2 == 1) ? "tablebluerow2" : "tablebluerow1";
			$j++;
			$x .= '<tr class="'.$tr_css.'">';
			$x .= "		<td class='tabletext' valign='top'>".$j."</td>
						<td class='tabletext' valign='top'>".$parentInfo[$i]['ClassName']."</td>
						<td class='tabletext' valign='top'>".$parentInfo[$i]['ClassNumber']."</td>
						<td class='tabletext' valign='top'>".$parentInfo[$i]['StudentName']."</td>
						<td><input type=\"checkbox\" name=\"userIdAry[]\" id=\"userIdChk_Global\" value=\"".$AttendanceData['StudentID']."\" onclick=\"unset_checkall(this, document.getElementById('thickboxForm'))\" checked/></td>
						<td class='tabletext' valign='top'>".$parentInfo[$i]['OutputName']."</td>
						<td><input type=\"checkbox\" name=\"parentIdAry[]\" id=\"parentIdChk_Global\" value=\"".$AttendanceData['StudentID']."\" onclick=\"unset_checkall(this, document.getElementById('thickboxForm'))\" checked/></td>
						";
			$x .= "</tr>";
		}
		$hasAtLeastPG = 1;
	}
	else{
		$lu = new libuser($AttendanceData['StudentID']);

		$tr_css = ($j % 2 == 1) ? "tablebluerow2" : "tablebluerow1";
			$j++;
			$x .= '<tr class="'.$tr_css.'">';
			$x .= "		<td class='tabletext' valign='top'>".$j."</td>
						<td class='tabletext' valign='top'>".$lu->ClassName."</td>
						<td class='tabletext' valign='top'>".$lu->ClassNumber."</td>
						<td class='tabletext' valign='top'>".Get_Lang_Selection($lu->ChineseName,$lu->EnglishName)."</td>
						<td><input type=\"checkbox\" name=\"userIdAry[]\" id=\"userIdChk_Global\" value=\"".$AttendanceData['StudentID']."\" onclick=\"unset_checkall(this, document.getElementById('thickboxForm'))\" checked/></td>
						<td class='tabletext' valign='top'>---</td>
						<td class='tabletext' valign='top'>---</td>
						";
			$x .= "</tr>";
		# Omas 2015-09-29 
		$hasAtLeastPG = 1;
	}
	$studentArr[] = $AttendanceData['StudentID'];
}

if(!$AttendanceDataArr){
	$display .= "<tr class='tablerow2'><td align='center' colspan='7' class='tabletext' height='50'><br>$i_no_record_exists_msg<br><br></td></tr>\n";
}
else
	$display .= $x;

$display .= "</table>";

$display .="<br/>";

#send email to class teacher and PIC
$display .="<input type='checkbox' name='classTeacher' id='classTeacher' value='yes' /><label for='classTeacher'>".$Lang['eEnrolment']['InformedToClassTeacher']."</label>&nbsp;&nbsp;";
$display .="<input type='checkbox' name='pic' id='pic' value='yes' /><label for='pic'>".$Lang['eEnrolment']['InformedToPIC']."</label>&nbsp;";

//$display .=$libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['SendEmail']);
//$display .= '<table class="form_table_v30">';
//$display .= '<tr>
//				<td class="field_title" nowrap="nowrap" valign="top" width="30%">'.$Lang['eEnrolment']['MailTitle'].' <span class="tabletextrequire">*</span></td>
//				<td><input type="text" class="textboxtext" name="emailTitle" id="emailTitle" maxlength="256"></td>
//			</tr>';
//$display .= '<tr>
//				<td class="field_title" nowrap="nowrap" valign="top" width="30%">'.$Lang['eEnrolment']['MailContent'].'  <span class="tabletextrequire">*</span></td>
//				<td>'.$HTMLEditor.'</td>
//			</tr>';	
//$display .= '</table>';
//$display .= '<span class="tabletextremark"><span class="tabletextrequire">*</span> Mandatory field(s)</span>';

//--- should modify
//$display .=$libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['SendEmail']);
//$display .= '<table class="form_table_v30">';
//$display .= '<tr>
//				<td class="field_title" nowrap="nowrap" valign="top" width="30%">'.$Lang['eEnrolment']['MailTitle'].' <span class="tabletextrequire">*</span></td>
//				<td><!--clubName--> absent alert</td>
//			</tr>';
//$display .= '<tr>
//				<td class="field_title" nowrap="nowrap" valign="top" width="30%">'.$Lang['eEnrolment']['MailContent'].'  <span class="tabletextrequire">*</span></td>
//				<td><!--studentName--> (<!--className--> - <!--classNumber-->) was absent for the meeting of <!--clubName--> on <!--datetime-->.</td>
//			</tr>';	
//$display .= '</table>';

if($groupDateID){
	$groupInfo = $libenroll->retrieveGroupEventTitle($AttendanceDataArr[0]['GroupID'], "club");
}
else{
	$groupInfo = $libenroll->retrieveGroupEventTitle($AttendanceDataArr[0]['EnrolEventID'], "activity");
}
$groupTitle = $groupInfo;

$instructionInfo = '<table>';
$instructionInfo .= '<tr valign="top">
				<td>'.$Lang['eEnrolment']['MailTitle'].': </td>
				<td>'.str_replace("<!--clubName-->",($groupDateID?$Lang['eEnrolment']['Club']:$Lang['eEnrolment']['Activity']),$Lang['eEnrolment']['AttenanceMailTitleFixed']).' ('.$groupTitle.')'.'</td>
			</tr>';
	
$emailContent = str_replace("<!--studentName-->"," [".$Lang['StudentAttendance']['StudentName']."] ( [".$Lang['StudentAttendance']['Class']."] - [".$Lang['StudentAttendance']['ClassNumber']."] )",$Lang['eEnrolment']['AttenanceMailContentFixed']);
$emailContent = str_replace("<!--clubName-->",$groupTitle,$emailContent);
$emailContent = str_replace("<!--datetime-->",$AttendanceDataArr[0]['ActivityDateStart'],$emailContent);
$emailContent = str_replace("<!--attendanceRate-->",'['.$eEnrollment['attendance_rate'].']',$emailContent);

$instructionInfo .= '<tr valign="top">
				<td>'.$Lang['eEnrolment']['MailContent'].': </td>
				<td>'.$emailContent.'</td>
			</tr>';	
$instructionInfo .= '</table>';
### instruction box
$htmlAry['instructionBox'] = $libenroll_ui->Get_Warning_Message_Box($Lang['eEnrolment']['SendEmail'], $instructionInfo);
$htmlAry['Remarks'] = $libenroll_ui->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eEnrolment']['AbsentNotificationRemarks']);

### action buttons
$htmlAry['submitBtn'] = $libenroll_ui->Get_Action_Btn($Lang['Btn']['Submit'], "button", "FormSubmitCheck(document.thickboxForm)", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $libenroll_ui->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<script language="javascript">
$(document).ready(function(){
	$('#TB_window').css("z-index", 10000);
});

function getEditorValue(instanceName) {
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	
	// Get the editor contents as XHTML.
	return oEditor.GetXHTML(true) ; // "true" means you want it formatted.
}

function FormSubmitCheck(obj)
{
	if(!$("input[name='userIdAry[]']").is(':checked')){
		alert("<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>");
		return false;
	}
//	if($('#emailTitle').val() ==""){
//		alert("<?=$i_Form_pls_fill_in_title?>");
//		$('#emailTitle').focus();
//		return false;
//	}
//	if(getEditorValue("emailContent") =='' || getEditorValue("emailContent") =='<br />' || getEditorValue("emailContent") =='&nbsp;'){
//		alert("<?=$Lang['SysMgr']['Homework']['SearchAlert']?>!");
//		FCKeditorAPI.GetInstance("emailContent").Focus();
//		return false;
//	}
	document.getElementById('submitBtn_tb').disabled = true;
	document.getElementById('cancelBtn_tb').disabled = true;
	obj.submit();
}
</script>

<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm" action="get_mail_to_absence_stu_tb_update.php" method="POST" enctype="multipart/form-data">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
		<?=$libenroll_ui->GET_NAVIGATION2_IP25($Lang['eEnrolment']['AbsentStudent'].($AttendanceDataArr?" (".$AttendanceDataArr[0]['ActivityDateStart'].")":""))?>
			<?= $display?>
			<?=($AttendanceDataArr?$htmlAry['instructionBox']:"")?>
			<?=($AttendanceDataArr?$htmlAry['Remarks']:"")?>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<?=(($AttendanceDataArr && $hasAtLeastPG)?$htmlAry['submitBtn']:'')?>&nbsp;
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
		<input type="hidden" name="hid_query" value="<?=$queryStr?>" />
		<input type="hidden" name="hid_studentList" value="<?=implode(',',$studentArr)?>" />
		<input type="hidden" name="hid_fromLink" value="<?=($groupDateID?"0":"1")?>" />
		<input type="hidden" name="hid_activityDateStart" value="<?=$AttendanceDataArr[0]['ActivityDateStart']?>" />
		<input type="hidden" name="hid_activityGroupID" value="<?=$AttendanceDataArr[0]['GroupID']?>" />
		<input type="hidden" name="hid_activityEventID" value="<?=$AttendanceDataArr[0]['EnrolEventID']?>" />
	</form>
</div>