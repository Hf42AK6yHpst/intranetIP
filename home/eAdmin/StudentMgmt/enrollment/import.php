<?php 
//using: 
/*
 *  Modification:
 *  Date:   2020-02-25 (Tommy)
 *      - add year term import (Case #Z178470)
 *  2016-05-31 Henry HM
 *      - Added merit fields for type={activityAllParticipant,activityParticipant}
 * 	2015-10-07 Omas
 *	fix UI problem
 *  2015-06-02 Shan
 *  UI style updated
 *  2015-04-08 Omas:
 * 		- changed import column discription, added field userlogin
 *  2013-01-31 Rita: 
 * 		- add Term remark
 *  2013-01-02 Rita:
 * 		- add merit import 
 *  20121227 Rita:
 *  	- add import field "Reason" for Customization 
 * 	20120215 Marcus:
 * 		- Cater status selection
*/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$lstudentprofile = new libstudentprofile();

if ($plugin['eEnrollment']){
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	
	$AcademicYearID = IntegerSafe($AcademicYearID);
	$EnrolGroupID = IntegerSafe($EnrolGroupID);
	$EnrolEventID = IntegerSafe($EnrolEventID);
	
	$libenroll = new libclubsenrol();
	$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
	$libgroup = new libgroup($GroupID);
	$linterface = new interface_html();
	$keyword = 	str_replace(
	    array('<', '>','"','\'','/',';'),
	    array('','','','','',''),
	    $keyword);
	$filter = IntegerSafe($filter);
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
		
	if ($type=="clubAttendance")
	{
		$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Attendance', $EnrolGroupID);
	
		$CurrentPage = "PageMgtClub";
		$CurrentPageArr['eEnrolment'] = 1;
		if (($LibUser->isStudent())||($LibUser->isParent())) {
			$CurrentPageArr['eEnrolment'] = 0;
			$CurrentPageArr['eServiceeEnrollment'] = 1;
		}
		$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		//$TAGS_OBJ[] = array($button_import." ".$libgroup->Title." ".$eEnrolment['attendance_title'], "", 1);
		# tags 
        $tab_type = "club";
        $current_tab = 1;
        # navigation
        $PAGE_NAVIGATION[] = array($button_import." ".$libgroup->Title." ".$eEnrolment['attendance_title'], "");
        
		$cancel_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&field=$field&order=$order&filter=$filter&keyword=$keyword";
		
		//$format_str = "<a class=\"tablelink\" href=\"get_sample_csv.php?GroupID=$GroupID&type=clubAttendance\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>";
		$sampleLink = "<a class=\"tablelink\" href=\"export.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=2&forSample=1\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>";
	}	
	else if($type=="eventAttendance")
	{
		$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Attendance', $EnrolEventID);
		
		$CurrentPage = "PageMgtActivity";
		$CurrentPageArr['eEnrolment'] = 1;
		if (($LibUser->isStudent())||($LibUser->isParent())) {
			$CurrentPageArr['eEnrolment'] = 0;
			$CurrentPageArr['eServiceeEnrollment'] = 1;
		}
		$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		//$TAGS_OBJ[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrolment['attendance_title'], "", 1);
		# tags 
        $tab_type = "activity";
        $current_tab = 1;
        # navigation
        $PAGE_NAVIGATION[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrolment['attendance_title'], "");
        
		$cancel_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword";
		
		//$format_str = "<a class=\"tablelink\" href=\"get_sample_csv.php?EnrolEventID=$EnrolEventID&type=activityAttendance\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>";
		$sampleLink = "<a class=\"tablelink\" href=\"export.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=3&forSample=1\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>";
	}
	else if(substr($type, 0, 4) == "club")
	{
		$CurrentPage = "PageMgtClub";
		$CurrentPageArr['eEnrolment'] = 1;
		$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
        
        //$TAGS_OBJ[] = array($button_import." ".$libgroup->Title." ".$eEnrollment['performance_title'], "", 1);
		# tags 
        $tab_type = "club";
        $current_tab = 1;
        
        $cancel_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&field=$field&order=$order&filter=$filter&keyword=$keyword";
		
        if ($type=="clubMember")
		{
			$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin', $EnrolGroupID);
		
			# navigation
        	$PAGE_NAVIGATION[] = array($button_import." ".$libgroup->Title." ".$eEnrollment['member'], "");
        	
//        	if($libenroll->enableClubRecordMeritInfoRight()){
//        		$FileDescription = $eEnrollment['MemberWithMerit_Import_FileDescription'];
//        		$CsvFileName = 'import_member_with_merit_sample.csv';
//        	}else{
//        		$FileDescription = $eEnrollment['Member_Import_FileDescription'];
//        		$CsvFileName = 'import_member_sample.csv';
//        	}
//
//			$FileDescription = $eEnrollment['Member_Import_FileDescription'];
//			if($libenroll->enableClubRecordMeritInfoRight()){
//				if (!$lstudentprofile->is_merit_disabled) {
//					$FileDescription .= ",\"Merit\"";
//				}
//				if (!$lstudentprofile->is_min_merit_disabled) {
//					$FileDescription .= ",\"Minor Merit\"";
//				}
//				if (!$lstudentprofile->is_maj_merit_disabled) {
//					$FileDescription .= ",\"Major Merit\"";
//				}
//				if (!$lstudentprofile->is_sup_merit_disabled) {
//					$FileDescription .= ",\"Super Merit\"";
//				}
//				if (!$lstudentprofile->is_ult_merit_disabled) {
//					$FileDescription .= ",\"Ultra Merit\"";
//				}
//			}
			$ColumnTitleArr = $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription'];
			if($libenroll->enableClubRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][0];
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][1];
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][2];
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][3];
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][4];
				}
			}
			$ColumnPropertyArr = array(2,2,2,3,3,3,3,3,3,3);
			
			if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
			    $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
			    
			    //get school year term
			    $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
			    $termAry = $libenroll->returnResultSet($sqlTemp);
			    $numOfTerm = count($termAry);
			    $result = array_search($eEnrollment['performance'],$ColumnTitleArr);
			    
			    for ($i=0; $i<$numOfTerm; $i++) {
			        $termName = $termAry[$i]["TermName"]." ".$eEnrollment['performance'];
			        //insert termName between role and performance 
			        array_splice( $ColumnTitleArr, $result, 0, $termName );
			        array_splice( $ColumnPropertyArr, $result, 0, 3);
			        $result++;
			    }
			}
			
			$RemarksArr = array();	
		
			$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
			$FileDescription = $ImportPageColumn;

			if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
			     $csvFileExportPath = "export.php?type=12&AcademicYearID=$AcademicYearID";
			}else{
			     $csvFileExportPath = "export.php?type=8";
			}
		}
		else if ($type=="clubAllMember")
		{	//Omas
			$ColumnTitleArr = $Lang['eEnrolment']['AllClubMember_Import_FileDescription'];
			if($libenroll->enableClubRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][0];
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][1];
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][2];
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][3];
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][4];
				}
			}
			$ColumnPropertyArr = array(1,3,2,2,2,3,3,3,3,3);
			
			if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
			    $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
			    
			    //get school year term
			    $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
			    $termAry = $libenroll->returnResultSet($sqlTemp);
			    $numOfTerm = count($termAry);
			    $result = array_search($eEnrollment['performance'],$ColumnTitleArr);
			    
			    for ($i=0; $i<$numOfTerm; $i++) {
			        $termName = $termAry[$i]["TermName"]." ".$eEnrollment['performance'];
			        //insert termName between role and performance
			        array_splice( $ColumnTitleArr, $result, 0, $termName );
			        array_splice( $ColumnPropertyArr, $result, 0, 3);
			        $result++;
			    }
			}
			
			$RemarksArr = array("" , "<span class='tabletextremark'>".$eEnrollment['All_Member_Import_FileDescription_TermRemarks']."</span>");	
		
			$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
			
			# navigation
        	$PAGE_NAVIGATION[] = array($eEnrollment['import_member'], "");
//        	if($libenroll->enableClubRecordMeritInfoRight()){
//        		 $FileDescription = $eEnrollment['All_MemberWithMerit_Import_FileDescription'];
//	        	$CsvFileName = 'import_all_member_with_merit_sample.csv';
//        	}else{
//	        	$FileDescription = $eEnrollment['All_Member_Import_FileDescription'];
//	        	$CsvFileName = 'import_all_member_sample.csv';
//        	}
//			$FileDescription = $eEnrollment['All_Member_Import_FileDescription'];
//			if($libenroll->enableClubRecordMeritInfoRight()){
//				if (!$lstudentprofile->is_merit_disabled) {
//					$FileDescription .= ",\"Merit\"";
//				}
//				if (!$lstudentprofile->is_min_merit_disabled) {
//					$FileDescription .= ",\"Minor Merit\"";
//				}
//				if (!$lstudentprofile->is_maj_merit_disabled) {
//					$FileDescription .= ",\"Major Merit\"";
//				}
//				if (!$lstudentprofile->is_sup_merit_disabled) {
//					$FileDescription .= ",\"Super Merit\"";
//				}
//				if (!$lstudentprofile->is_ult_merit_disabled) {
//					$FileDescription .= ",\"Ultra Merit\"";
//				}
//			}
			$FileDescription .= $ImportPageColumn;
			
			if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
			    $csvFileExportPath = "export.php?type=12&AcademicYearID=$AcademicYearID&importType=clubAllMember";
			}else{
			 $csvFileExportPath = "export.php?type=8&importType=clubAllMember";
			}
		}
		
	}	
	else if(substr($type, 0, 8) == "activity")
	{
		$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID);
		
		$CurrentPage = "PageMgtActivity";
		$CurrentPageArr['eEnrolment'] = 1;
		$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
        
        //$TAGS_OBJ[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrollment['performance_title'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 1;
        
		$cancel_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword";
		
		if ($type=="activityParticipant")
		{
			# navigation
        	$PAGE_NAVIGATION[] = array($button_import." ".$libenroll->GET_EVENT_TITLE($EnrolEventID)." ".$eEnrollment['participant2'], "");
        	
        	//$FileDescription = $eEnrollment['Member_Import_FileDescription'];
        	
        	$ColumnTitleArr = $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription'];
			if($libenroll->enableActivityRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][0];
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][1];
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][2];
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][3];
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][4];
				}
			}
			$ColumnPropertyArr = array(2,2,2,3,3,3,3,3,3);
			$RemarksArr = array();	

			$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
        	$FileDescription = $ImportPageColumn;
        	
        	$csvFileExportPath = "export.php?type=11&importType=activityParticipant";
		}
		else if ($type=="activityAllParticipant")
		{
			# navigation
        	$PAGE_NAVIGATION[] = array($eEnrollment['import_participant'], "");
        	
        	$ColumnTitleArr = $Lang['eEnrolment']['AllActivityMember_Import_FileDescription'];
        	if($libenroll->enableActivityFillInEnrolReasonRight()){
        	//	$FileDescription = $eEnrollment['All_Participant_Import_FileDescription_WithReason'];
        		$ColumnTitleArr[] = $Lang['General']['Reason'];
//         		$CsvFileName = 'import_all_participant_sample_with_reason.csv';
        		$csvFileExportPath = "export.php?type=11&importType=activityAllParticipant";
        	}else{
	        //	$FileDescription = $eEnrollment['All_Participant_Import_FileDescription'];
// 	        	$CsvFileName = 'import_all_participant_sample.csv';
        		$csvFileExportPath = "export.php?type=11&importType=activityAllParticipant";
        	}
			if($libenroll->enableActivityRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][0];
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][1];
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][2];
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][3];
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ColumnTitleArr[] .= $Lang['eEnrolment']['ClubActivityMember_Import_FileDescription_array_merit'][4];
				}
			}
        	$ColumnPropertyArr = array(1,2,2,2,3,3,3,3,3,3);
        	$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
        	$FileDescription = $ImportPageColumn;
		}
	}	
	
	if ($type=="clubAttendance" || $type=="eventAttendance") {
		$format_str .= $sampleLink;
		$format_str .= "<br>";
		$format_str .= '<b>'.$Lang['eEnrolment']['AttendanceDataInputRemarks'].'</b>';
		$format_str .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
			$format_str .= '<thead>'."\n";
				$format_str .= '<tr>'."\n";
					$format_str .= '<th>'.$Lang['eEnrolment']['Code'].'</th>'."\n";
					$format_str .= '<th>'.$Lang['eEnrolment']['AttendanceStatus'].'</th>'."\n";
				$format_str .= '</tr>'."\n";
			$format_str .= '</thead>'."\n";
			$format_str .= '<tbody>'."\n";
				$format_str .= '<tr>'."\n";
					$format_str .= '<td>'.$enrolConfigAry['AttendanceImportCode']['Present'].'</td>'."\n";
					$format_str .= '<td>'.$Lang['eEnrolment']['Attendance']['Present'].'</td>'."\n";
				$format_str .= '</tr>'."\n";
				$format_str .= '<tr>'."\n";
					$format_str .= '<td>'.$enrolConfigAry['AttendanceImportCode']['Absent'].'</td>'."\n";
					$format_str .= '<td>'.$Lang['eEnrolment']['Attendance']['Absent'].'</td>'."\n";
				$format_str .= '</tr>'."\n";
				$format_str .= '<tr>'."\n";
					$format_str .= '<td>'.$enrolConfigAry['AttendanceImportCode']['Exempt'].'</td>'."\n";
					$format_str .= '<td>'.$Lang['eEnrolment']['Attendance']['Exempt'].'</td>'."\n";
				$format_str .= '</tr>'."\n";
			$format_str .= '</tbody>'."\n";
		$format_str .= '</table>'."\n";
		$format_str .= '<span class="tabletextremark">'.$Lang['eEnrolment']['ImportAttendanceRemarks'].'</span>'."\n";
		$format_str .= '<br />'."\n";
		$format_str .= '<br />'."\n";
	}
	else if ($type=="clubMember" || $type=="clubAllMember" || $type=="activityParticipant" || $type=="activityAllParticipant")
	{
		$format_str = $FileDescription;
		$format_str .= "<br><br>";
		
		if($type=="clubAllMember" || $type=="activityAllParticipant"){
		# Term Remarks
//		$format_str .= "<b>".$eEnrollment['All_Member_Import_FileDescription_TermRemarksTitle']."</b>";	
//		$format_str .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
//			$format_str .= "<tr class='tabletext'>";
//				$format_str .= "<td width='55'>".$eEnrollment['All_Member_Import_FileDescription_TermTile']."</td>";
//				$format_str .= "<td width='10'> : </td>";
//				$format_str .= "<td>".$eEnrollment['All_Member_Import_FileDescription_TermRemarks']."</td>";
//			$format_str .= "</tr>";
//		$format_str .= "</table>";
//		$format_str .= "<br>";


		}
		
		# Admin Role Remarks
		$format_str .= "<b>".$eEnrollment['All_Member_Import_FileDescription_AdminRoleRemarks']."</b>";
		$format_str .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			$format_str .= "<tr class='tabletext'>";
				$format_str .= "<td width='55'>".$eEnrollment['All_Member_Import_FileDescription_PICTitle']."</td>";
				$format_str .= "<td width='10'> : </td>";
				$format_str .= "<td>".$eEnrollment['All_Member_Import_FileDescription_PICRemarks']."</td>";
			$format_str .= "</tr>";
			$format_str .= "<tr class='tabletext'>";
				$format_str .= "<td>".$eEnrollment['All_Member_Import_FileDescription_HelperTitle']."</td>";
				$format_str .= "<td>: </td>";
				$format_str .= "<td>".$eEnrollment['All_Member_Import_FileDescription_HelperRemarks']."</td>";
			$format_str .= "</tr>";
			$format_str .= "<tr class='tabletext'>";
				$format_str .= "<td>".$eEnrollment['All_Member_Import_FileDescription_NATitle']."</td>";
				$format_str .= "<td>: </td>";
				$format_str .= "<td>".$eEnrollment['All_Member_Import_FileDescription_NARemarks']."</td>";
			$format_str .= "</tr>";
		$format_str .= "</table>";
		$format_str .= $eEnrollment['All_Member_Import_FileDescription_UnchangeRemarks'];
		$format_str .= "<br><br>";
		
			
		if ($csvFileExportPath == '') {
			$href = GET_CSV($CsvFileName);
		}
		else {
			$href = $csvFileExportPath;
		}
		
		$format_str .= "<a class=\"tablelink\" href=\"". $href ."\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>";
		
		$StatusSelectRadio .= '<tr>'."\n";
			$StatusSelectRadio .= '<td valign="top" nowrap="nowrap" class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Status'].'</td>'."\n";
			$StatusSelectRadio .= '<td class="tabletext">'."\n";
				$StatusSelectRadio .= $linterface->Get_Radio_Button("StatusApproved", "Status", 1, 1, "", $Lang['eEnrolment']['EnrollmentStatus']['Approved'])."\n";
				$StatusSelectRadio .= $linterface->Get_Radio_Button("StatusWaiting", "Status", 2, 0, "", $Lang['eEnrolment']['EnrollmentStatus']['Waiting'])." (".$Lang['eEnrolment']['ImportMember']['WaitingRemarks'].")"."\n";
			$StatusSelectRadio .= '</td>'."\n";
		$StatusSelectRadio .= '</tr>'."\n";
		
	}
	
	$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
	
    include_once("management_tabs.php");

	$linterface->LAYOUT_START();
		
?>

<script type="text/JavaScript" language="JavaScript">
var jType = "<?=$type?>";

function trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eEnrolment['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}

	if (jType=="clubMember" || jType=="activityParticipant" || jType=="clubAllMember" || jType=="activityAllParticipant")
	{
		obj.action = "import_member_update.php";
	}
	else
	{
		obj.action = "import_update.php";
	}
	
	obj.submit();
}
</script>
<br />

<form name="form1" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
	</table>
	<br /><br />
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" >
		
		<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
		
		<tr>
	    	<td colspan="2">
	    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" >
				   	<br />
	    			 <tr>
	    				<?
	    				if ($type=="clubAttendance" || $type=="clubPerformance" || $type=="clubMember")
	    				{
		    			?>
							<td width="35%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><span class="tabletext"><?= $eEnrollment['club_name']?></span></td>
							<td class="tabletext"><?=$libgroup->Title?></td>
						<?
						}
						else if ($type=="eventAttendance" || $type=="activityPerformance" || $type=="activityParticipant")
						{
						?>
							<td width="35%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><span class="tabletext"><?= $eEnrollment['activity_title']?></span></td>
							<td class="tabletext"><?=$libenroll->GET_EVENT_TITLE($EnrolEventID)?></td>
						<?
						}
						?>
					 </tr>
					
				     <tr>
				     <td colspan="2">
					     <table width="100%" class="form_table_v30">
					       <tr>
					   		<td width="35%" valign="top" nowrap="nowrap" class="field_title" >
								<?= $linterface->RequiredSymbol() . $i_select_file ?>
						    </td>
							<td class="tabletext"><input class="file" type="file" name="userfile"><br>
							 <?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
							</td>
						   </tr>
						   <?=$StatusSelectRadio?> 
					   	   <tr>					   	   				 
							<td width="35%" valign="top" nowrap="nowrap" class="field_title" ><?= $i_general_Format ?></td>
							<td class="tabletext"><?=$format_str?></td>					
				 		  </tr>	
					     <tr>
				  			<td colspan="2"><?=$Lang['eEnrolment']['ImportRemarks_ClassnameClassNumber_Userlogin']?></td>
						 </tr>
						<tr>
							<td colspan="2"><?=$Lang['General']['RequiredField']?></td>
					   </tr>
					   
					 </table>
					 
				  </td>
				   </tr>	
					 
		            <tr>
    		 			<td colspan="2">
		   			 	  <table width="99%" border="0" cellpadding="0" cellspacing="0" align="center">
		   			 		<tr>
                			<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                			</tr>
		    			  </table>
	    				</td>
					</tr>	
		
					<tr>
					<td colspan="2" align="center">
			  
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
					<?
					if ($type == "clubAttendance")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'club_attendance_mgt.php?$cancel_parameters'")?>&nbsp;
					<?
						}
						else if ($type == "eventAttendance")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'event_attendance_mgt.php?$cancel_parameters'")?>&nbsp;
					<?
					}
					else if ($type == "clubPerformance" || $type == "clubMember")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'member_index.php?$cancel_parameters'")?>&nbsp;
					<?
					}
					else if ($type == "eventPerformance" || $type=="activityParticipant")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'event_member_index.php?$cancel_parameters'")?>&nbsp;
					<?
					}
					else if ($type=="clubAllMember")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'group.php?AcademicYearID=$AcademicYearID&Semester=$Semester'")?>&nbsp;
					<?
					}
					else if ($type=="activityAllParticipant")
					{
					?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'event.php?AcademicYearID=$AcademicYearID'")?>&nbsp;
					<? 
					}
					?>					 
				  	</td>
					</tr>		
						
		      </table>
		    
		</td>
		</tr>
					
	</table> 
	
	<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>"/>
	<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?=$EnrolGroupID?>"/>
	<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?=$EnrolEventID?>"/>
	<input type="hidden" name="type" id="type" value="<?=$type?>"/>
	<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>"/>
	<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>"/>
	
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
}
?>