<?php
# using: Rita

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'];
$EnrolGroupID = $_POST['EnrolGroupID'];
$filter = $_POST['filter'];

$meritCount = $_POST['meritCount'];
$minorMeritCount = $_POST['minorMeritCount'];
$majorMeritCount = $_POST['majorMeritCount'];
$superMeritCount = $_POST['superMeritCount'];
$ultraMeritCount = $_POST['ultraMeritCount'];

$successArr = array();

# Merit Record
foreach ($meritCount as $studentID=>$meritCountInfoArr){
	foreach ($meritCountInfoArr as $_meritType=>$_meritCountInfo){
		foreach ($_meritCountInfo as $__meritRecordID=>$__meritCount){	
			if($__meritRecordID!='0' && $__meritCount!=''){
				$successArr[]= $libenroll->Update_Merit_Record($__meritRecordID,$__meritCount,$_meritType);
			}elseif($__meritRecordID=='0' && $__meritCount!=''){
				$successArr[]= $libenroll->Add_Merit_Record($studentID,$EnrolGroupID,$__meritCount,$_meritType);
			}elseif($__meritRecordID!='0' && $__meritCount==''){
				$successArr[]= $libenroll->Remove_Merit_Record($__meritRecordID);
			}
		}
	}
}


# Minor Merit Record
foreach ($minorMeritCount as $studentID=>$minorMeritCountInfoArr){
	foreach ($minorMeritCountInfoArr as $_minorMeritType=>$_minorMeritCountInfo){
		foreach ($_minorMeritCountInfo as $__minorMeritRecordID=>$__minorMeritCount){	
			if($__minorMeritRecordID!='0' && $__minorMeritCount!=''){
				$successArr[]= $libenroll->Update_Merit_Record($__minorMeritRecordID,$__minorMeritCount,$_minorMeritType);
			}elseif($__minorMeritRecordID=='0' && $__minorMeritCount!=''){
				$successArr[]= $libenroll->Add_Merit_Record($studentID,$EnrolGroupID,$__minorMeritCount,$_minorMeritType);
			}elseif($__minorMeritRecordID!='0' && $__minorMeritCount==''){
				$successArr[]= $libenroll->Remove_Merit_Record($__minorMeritRecordID);
			}
		}
	}
}

# Major Merit Record
foreach ($majorMeritCount as $studentID=>$majorMeritCountInfoArr){
	foreach ($majorMeritCountInfoArr as $_majorMeritType=>$_majorMeritCountInfo){
		foreach ($_majorMeritCountInfo as $__majorMeritRecordID=>$__majorMeritCount){	
			if($__majorMeritRecordID!='0' && $__majorMeritCount!=''){
				$successArr[]= $libenroll->Update_Merit_Record($__majorMeritRecordID,$__majorMeritCount,$_majorMeritType);
			}elseif($__majorMeritRecordID=='0' && $__majorMeritCount!=''){
				$successArr[]= $libenroll->Add_Merit_Record($studentID,$EnrolGroupID,$__majorMeritCount,$_majorMeritType);
			}elseif($__majorMeritRecordID!='0' && $__majorMeritCount==''){
				$successArr[]= $libenroll->Remove_Merit_Record($__majorMeritRecordID);
			}
		}
	}
}


# Super Merit Record
foreach ($superMeritCount as $studentID=>$superMeritCountInfoArr){
	foreach ($superMeritCountInfoArr as $_superMeritType=>$_superMeritCountInfo){
		foreach ($_superMeritCountInfo as $__superMeritRecordID=>$__superMeritCount){	
			if($__superMeritRecordID!='0' && $__superMeritCount!=''){
				$successArr[]= $libenroll->Update_Merit_Record($__superMeritRecordID,$__superMeritCount,$_superMeritType);
			}elseif($__superMeritRecordID=='0' && $__superMeritCount!=''){
				$successArr[]= $libenroll->Add_Merit_Record($studentID,$EnrolGroupID,$__superMeritCount,$_superMeritType);
			}elseif($__superMeritRecordID!='0' && $__superMeritCount==''){
				$successArr[]= $libenroll->Remove_Merit_Record($__superMeritRecordID);
			}
		}
	}
}


# Ultra Merit Record
foreach ($ultraMeritCount as $studentID=>$ultraMeritCountInfoArr){
	foreach ($ultraMeritCountInfoArr as $_ultraMeritType=>$_ultraMeritCountInfo){
		foreach ($_ultraMeritCountInfo as $__ultraMeritRecordID=>$__ultraMeritCount){	
			if($__ultraMeritRecordID!='0' && $__ultraMeritCount!=''){
				$successArr[]= $libenroll->Update_Merit_Record($__ultraMeritRecordID,$__ultraMeritCount,$_ultraMeritType);
			}elseif($__ultraMeritRecordID=='0' && $__ultraMeritCount!=''){
				$successArr[]= $libenroll->Add_Merit_Record($studentID,$EnrolGroupID,$__ultraMeritCount,$_ultraMeritType);
			}elseif($__ultraMeritRecordID!='0' && $__ultraMeritCount==''){
				$successArr[]= $libenroll->Remove_Merit_Record($__ultraMeritRecordID);
			}
		}
	}
}
		
if (in_array(false,$successArr)) 
{
	$libenroll->RollBack_Trans();
	$msg='5';

}
else
{
	$libenroll->Commit_Trans();	
	$msg='2';
}



intranet_closedb();

	header("Location: member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&filter=$filter&msg=$msg");
	
?>