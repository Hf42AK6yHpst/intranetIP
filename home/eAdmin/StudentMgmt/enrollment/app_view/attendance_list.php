<?php 
//using: 
#############################################
#
#   Date:   2020-06-24  Tommy
#           - added $allClubMember for checking total club student to count attendance is taken or not
#
##############################################

$PATH_WRT_ROOT = "../../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

$uid = IntegerSafe($_GET['uid']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

$today = date('Y-m-d');
if(!isset($date) || $date=="")
{
	$date=date('Y-m-d');
}else{
	$date_record = strtotime($date);
	$date = date("Y-m-d", $date_record);
}

if(!isset($msg) || $msg=="")
{
	$msg=0;
}

$libdb = new libdb();
$linterface = new interface_html();
$show_main_table = "";
if($charactor=="club"){
	$sql = "select 
			     IF(ig.GroupCode='' OR ig.GroupCode is null,'-',ig.GroupCode),
			     ig.Title, 
			     ig.TitleChinese, 
			     iegd.ActivityDateStart,
			     iegd.ActivityDateEnd,
			     iegd.GroupDateID,
			     iegd.EnrolGroupID
			from 
			     INTRANET_ENROL_GROUP_DATE as iegd
			     inner join INTRANET_ENROL_GROUPINFO as iegi on iegi.EnrolGroupID=iegd.EnrolGroupID
				 inner join INTRANET_ENROL_GROUPSTAFF as iegs on (iegs.UserID = '".$uid."' and iegs.EnrolGroupID = iegi.EnrolGroupID)
				 inner join INTRANET_GROUP as ig on ig.GroupID = iegi.GroupID
			where 
			     iegd.ActivityDateStart like '".$date."%' and iegd.RecordStatus=1
			";
	$result = $libdb->returnArray($sql);
	
	if(sizeof($result)==0){
		$show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	for($i=0;$i<sizeof($result);$i++){
		list($GroupCode, $TitleEN,$TitleChinese,$ActivityDateStart,$ActivityDateEnd,$GroupDateID,$EnrolGroupID) = $result[$i];
		$starttime_record = strtotime($ActivityDateStart);
	    $starttime = date("H:i", $starttime_record);
	    $endtime_record = strtotime($ActivityDateEnd);
	    $endtime = date("H:i", $endtime_record);
	    $Title = Get_Lang_Selection($TitleChinese, $TitleEN)."<br>(".$starttime." - ".$endtime.")";
		$sql1="select 
			     Count(iega.GroupAttendanceID)
			from 
	             INTRANET_ENROL_GROUP_ATTENDANCE as iega
			where 
			     iega.GroupDateID = '".$GroupDateID."'
     		Group By
				iega.EnrolGroupID";
		$result1 = $libdb->returnVector($sql1);
		$clubSql = "SELECT count(UserGroupID) FROM INTRANET_USERGROUP WHERE EnrolGroupID = '".$EnrolGroupID."' AND RecordType IS NULL";
		$allClubMember = $libdb->returnVector($clubSql);
		$sql2="select Count(iu.UserID) 
			   from 
                 INTRANET_USERGROUP as iug
                 Left Join INTRANET_USER as iu On (iug.UserID = iu.UserID And iu.RecordType = 2)
               where iug.EnrolGroupID = '".$EnrolGroupID."'
               group by 
                 iug.EnrolGroupID
				 ";
		$result2 = $libdb->returnVector($sql2);
		//$show_main_table .= "<td  width='20%' class='line'>".$GroupCode."</td>";
        if($date>$today) {
            $show_main_table .= "<tr><td  width='50%' class='line' style='text-align:left'>".$Title."</td>";
        }else{
            $show_main_table .= "<tr><td  width='50%' class='line' style='text-align:left'><a href='javascript:handle_attendance(".$GroupDateID.");' data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$Title."</a></td>";
        }
		$show_main_table .= "<td  width='20%' class='line'>".$result2[0]."</td>";
// 		if($result1[0]==0){
		if($result1[0]!=$allClubMember[0]){
			$show_main_table .= "<td style='color:red;' width='20%'  class='line'>".$Lang['eClassApp']['eEnrollment']['NotTake']."</td>";
		}
		else{
			$show_main_table .= "<td style='color:#3a960e;' width='20%'  class='line'>".$Lang['eClassApp']['eEnrollment']['HasTaken']."</td>";
		}
		if($date>$today){
			$show_main_table.="<td width='10%'  class='line' style='text-align:center'></td><tr>";
		}else{
			$show_main_table .="<td width='10%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$GroupDateID.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";			
		}
		
	}
}
if($charactor=="activity"){
	$sql = "select 
			     IF(ieei.ActivityCode='' OR ieei.ActivityCode is null,'-',ieei.ActivityCode) as ActivityCode,
			     ieei.EventTitle,  
			     ieed.ActivityDateStart,
			     ieed.ActivityDateEnd,
			     ieed.EventDateID,
			     ieed.EnrolEventID
			from 
			     INTRANET_ENROL_EVENT_DATE as ieed
			     inner join INTRANET_ENROL_EVENTINFO as ieei on ieei.EnrolEventID=ieed.EnrolEventID
				 inner join INTRANET_ENROL_EVENTSTAFF as ieest on (ieest.UserID = '".$uid."' and ieest.EnrolEventID = ieei.EnrolEventID)
			where 
			     ieed.ActivityDateStart like '".$date."%' and ieed.RecordStatus=1
			";
	$result = $libdb->returnArray($sql);

	if(sizeof($result)==0){
		$show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	for($i=0;$i<sizeof($result);$i++){
		list($ActivityCode, $Title, $ActivityDateStart,$ActivityDateEnd,$EventDateID,$EnrolEventID) = $result[$i];
		
		$starttime_record = strtotime($ActivityDateStart);
	    $starttime = date("H:i", $starttime_record);
	    $endtime_record = strtotime($ActivityDateEnd);
	    $endtime = date("H:i", $endtime_record);
	    $Title = $Title."<br>(".$starttime." - ".$endtime.")";
		$sql1="select 
			     Count(ieea.EventAttendanceID)
			from 
	             INTRANET_ENROL_EVENT_ATTENDANCE as ieea
			where 
			     ieea.EventDateID = '".$EventDateID."'
     		Group By
				ieea.EnrolEventID";
		$result1 = $libdb->returnVector($sql1);
		$sql2="select Count(iu.UserID) 
			   from 
                 INTRANET_ENROL_EVENTSTUDENT as iees
				 Left Join INTRANET_USER as iu On (iees.StudentID = iu.UserID And iu.RecordType = 2)
               where iees.EnrolEventID = '".$EnrolEventID."' and iees.RecordStatus = 2
               group by 
                 iees.EnrolEventID";
		$result2 = $libdb->returnVector($sql2);
		//$show_main_table .= "<td  width='20%' class='line'>".$ActivityCode."</td>";
        if($date>$today) {
            $show_main_table .= "<tr><td  width='50%' class='line' style='text-align:left'>".$Title."</td>";
        }else{
            $show_main_table .= "<tr><td  width='50%' class='line' style='text-align:left'><a href='javascript:handle_attendance(".$EventDateID.");' data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$Title."</a></td>";
        }
		$show_main_table .= "<td  width='20%' class='line'>".$result2[0]."</td>";
		if($result1[0]==0){
			$show_main_table .= "<td style='color:red;' width='20%'  class='line'>".$Lang['eClassApp']['eEnrollment']['NotTake']."</td>";
		}
		else{
			$show_main_table .= "<td style='color:#3a960e;' width='20%'  class='line'>".$Lang['eClassApp']['eEnrollment']['HasTaken']."</td>";
		}
		
		if($date>$today){
			$show_main_table .="<td width='10%'  class='line' style='text-align:center'></td><tr>";
			
		}else{
			$show_main_table .="<td width='10%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$EventDateID.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";			
		}
		
	}
}

echo $libeClassApp->getAppWebPageInitStart();
?>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<script>

	jQuery(document).ready( function() {
		 var msg = <?=$msg?>;
		 if(msg=="1") {
			 alert("<?=$Lang['eClassApp']['eEnrollment']['RecordHasUpdated']?>");
			 window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&parLang=<?=$parLang?>";			 			 
	     }
		 
	});
	
	//handle the datepicker onchange function
	function onSelectedDateOfJqueryMobileDatePicker() {
		   refresh();	
	}
	
	function refresh(){
	   document.form1.action="attendance_list.php";
	   document.form1.submit();
    }

    function handle_attendance(ID){
       var div = document.getElementById("div1");
       var input = document.createElement("input");
       input.type = "hidden";
       input.name = "id";
       input.value = ID;
       div.appendChild(input);

       document.form1.action = "take_attendance.php";
       document.form1.submit();
    }

    function back(){
       alert("back");
    }
    
</script>
<style type="text/css">
    #body{
            
         background:#ffffff;
    }
      
    .form_table_attendence{ width:100%; margin:0;}
 	.form_table_attendence tr td.line{vertical-align:center;	text-align:center; line-height:18px; padding-top:5px; padding-bottom:5px;	border-bottom:2px solid #EFEFEF; padding-left:2px; padding-right:2px}
    .form_table_attendence tr td {vertical-align:center;	text-align:center; line-height:18px; padding-top:5px; padding-bottom:5px;padding-left:2px; padding-right:2px} 
		
	.form_table_attendence tr td.field_title{width:15%;  padding-left:2px; padding-right:2px;border-width: 5px;}
	.form_table_attendence tr td.field_title_short{	border-bottom:2px solid #FFFFFF ; width:14%; padding-left:2px; padding-right:2px}
	.form_table_attendence col.field_title_short{width:14%; }
	.form_table_attendence col.field_content_short{width:20%; }
	.form_table_attendence tr td a{ color: #9966CC;	}
	.form_table_attendence tr th a{ color: #FFFFFF;	}
	.form_table_attendence tr td a:hover{ color: #FF0000;	}
	.form_table_attendence col.field_c {width:10px;}
	.form_table_attendence tr td .textbox{	width:98%}
	
	.header a:link { text-decoration: none; color: white;}
    .header a:active {text-decoration: none; color: white !important;} 
    .header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;} 
</style>
</head>
	<body>
	
		<div data-role="page" id="body">			
   	        <div data-role="content">
				
				<form id="form1" name="form1" method="get" >
				    <?php if(!isset($msg)||$msg=="") { ?>
				      <table class="form_table_attendence" width="100%" >
				        <tr>
				          <td class="field_title">
				            <label class="label" for="date" align="left"><?=$Lang['eClassApp']['eEnrollment']['RecordDate']?></label>
				          </td>
				          <td>
				            <input type="text" data-role="date" id="date" name="date" value="<?=$date?>" onchange="javascript:refresh();">
				          </td>
				        </tr>
				      </table>				   
                      <table class="form_table_attendence" width="100%" >
                         <tr style="background-color:#E6E6E6;">

                            <td width="50%">
                               <?=$Lang['eClassApp']['eEnrollment']['Name']?>                           
                            </td>
                            <td width="20%">
                               <?=$Lang['eClassApp']['eEnrollment']['Amount']?>
                            </td>
                            <td width="20%">
                               <?=$Lang['General']['Status']?>
                            </td>
                            <td width="10%">
                            </td>
                         </tr> 
                         <?=$show_main_table?>                                              
                      </table>
                      <div id="div1">
                      </div>
                                          
                      <input type="hidden" value=<?=$token?> name="token">
                      <input type="hidden" value=<?=$uid?> name="uid">
                      <input type="hidden" value=<?=$ul?> name="ul">                
                      <input type="hidden" value=<?=$parLang?> name="parLang">        
                      <input type="hidden" value=<?=$charactor?> name="charactor">               
                             
                      <?php } ?>                                                         
				</form>
				 
			</div>
	    </div>
	   
	</body>
</html>

<?php

intranet_closedb();

?>