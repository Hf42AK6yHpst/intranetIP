<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$lc = new libclubsenrol();
if (!$lc->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$lf = new libfilesystem();
$minmax_file = "$intranet_root/file/clubenroll_minmax.txt";
$content = "";
for ($i=0; $i<$num; $i++)
{
     if (isset(${"Def$i"}) && ${"Def$i"}==1)
     {
         # Default
     }
     else
     {
         $id = ${"LevelID$i"};
         $min = ${"min$i"};
         $max = ${"max$i"};
         $disabled = ${"NotUse$i"};
         $enrollmax = ${"enrollmax$i"};
         if ($disabled != 1 && ($min=="" || $max=="" || $enrollmax=="") )
         {
             continue;
         }
         $content .= "$id:::$min:::$max:::$enrollmax:::$disabled";
         if ($i!=$num-1) $content .= "\n";
     }
}
$lf->file_write($content,$minmax_file);

/* need not delete enrolment records
# Remove applications
$lc->removeNotUseEnrollmentApplication();
*/

intranet_closedb();
header("Location: student.php?msg=2");
?>