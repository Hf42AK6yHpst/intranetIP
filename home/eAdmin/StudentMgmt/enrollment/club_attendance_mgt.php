<?php
// Using: 

/********************************************************
 *	Modification Log
 *	2018-07-03 Pun [121822] [ip.2.5.9.7.1]
 *  - Added NCS cust
 *
 *	2016-11-30 Villa [R99066]
 *  - sycn the Gradian Information Floating box style to import
 *
 *	2015-11-27 Kenneth
 *	- Added Gradian Information Floating box
 *
 *	2013-11-05 Henry
 *	- Added email absent student buttons
 * 
 *	2011-09-08	YatWoon
 *	- lite version should not access this page 
 *
 *	2011-08-24	Yuen
 *	- tried to handle display issue for iPad but don't applied at this moment 
 *		* required fixSuperTableWidth() in script.js
 *
 *	2010-12-10	YatWoon
 *	- add js function Reload_Club_Attendance_Info_Table2()
 * 
 *  2010-12-03  Ivan
 *	- Added "Absent" Option in taking attendance
 *	- Restructured the coding of the page (the old one in /bak/club_attendance_mgt.php.old.20101203)
 * 
 * 	2010-08-02  Thomas
 * 	- Check the data is freezed or not
 *	  Show alert message when adding / updating the data
 *
 * 	2010-04-30	YatWoon
 * 	- apply jquery GridView for display attendance table (right / bottom scroll bar)
 *
 * 	20100310 Marcus:
 * 	- add Attendance Hours, modify checkbox to selection box to cater exemption in attendance
 * ******************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();

$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
$Semester = $_REQUEST['Semester'];
$action = $_REQUEST['action'];
$applicantId = IntegerSafe($applicantId);

$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isClubPIC = $libenroll->IS_CLUB_PIC($EnrolGroupID);
$isClubHelper = $libenroll->IS_CLUB_HELPER($EnrolGroupID);

## temp
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
if (!$isEnrolAdmin && !$isEnrolMaster && !$isClubPIC && !$isClubHelper)
	$canEdit = 0;
else
	$canEdit = 1;

if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
{
	No_Access_Right_Pop_Up();
}



### Menu Settings
$CurrentPage = "PageMgtClub";
$CurrentPageArr['eEnrolment'] = 1;
	
	
if ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT) {
	if ($action == "view")
	{
		$CurrentPage = "PageClubEnroll";
		
		$joinedEnrolGroupIdAry = Get_Array_By_Key($libenroll->Get_Student_Club_Info($applicantId), 'EnrolGroupID');
		if (!in_array($EnrolGroupID, $joinedEnrolGroupIdAry)) {
			No_Access_Right_Pop_Up();
		}
	}
	else
	{
		if ($libenroll->IS_ENROL_PIC($UserID, $EnrolGroupID, 'Club'))
		{
			$CurrentPage = "PageMgtClub";
		}
		else
		{
			$CurrentPage = "PageClubAttendanceMgt";
		}
	}
	$CurrentPageArr['eEnrollment'] = 0;
	$CurrentPageArr['eServiceeEnrollment'] = 1;
}
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# tags 
$tab_type = "club";
$current_tab = 1;
# navigation
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "javascript:js_Cancel();",);
$PAGE_NAVIGATION[] = array($eEnrollmentMenu['attendance_mgt'], "");
include_once("management_tabs.php");

$linterface = new interface_html();
$linterface->LAYOUT_START();
echo $linterface->Include_Thickbox_JS_CSS(); //Henry added 20131101

### System Msg
if ($msg == 2)
	$SysMsg = $linterface->GET_SYS_MSG("update");
//Henry added 20131104 [start]
if($msg == 3)
	$SysMsg = $linterface->GET_SYS_MSG("email");
//Henry added 20131104 [end]
	
$x = '';
$x .= '<form id="form1" name="form1" method="post" action="club_attendance_update.php">'."\n";
	$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td colspan="2">'."\n";
				$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		if ($isEnrolAdmin || $isEnrolMaster)
		{
			### Admin / Master: EnrolGroupID depends on the Selection
			$CategorySelection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "js_Changed_Category_Selection(this.value);", $Lang['eEnrolment']['AllCategories']);
			
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $CategorySelection;
					$x .= '&nbsp;';
					$x .= '<span id="ClubSelectionSpan"></span>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td align="right">'.$SysMsg.'</td>'."\n";
			$x .= '</tr>'."\n";
		}
		else
		{
			### Normal User: EnrolGroupID depends on the selected club in the last page
			$x .= '<input type="hidden" id="EnrolGroupID" name="EnrolGroupID" value="'.$EnrolGroupID.'" />'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td align="right" colspan="2">'.$SysMsg.'</td>'."\n";
			$x .= '</tr>'."\n";
		}
		
		### Content Table
		$x .= '<tr>'."\n";
			$x .= '<td colspan="2">'."\n";
				$x .= '<div id="ContentTableDiv" style="width:100%;"></div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
	$x .= '<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="'.$AcademicYearID.'">'."\n";
$x .= '</form>'."\n";

echo $x;


//$FixTableSize = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod") ? "30000" : "500";
$FixTableSize = "500";
?>

<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/superTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.superTable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/superTables.js"></script>
<script language="Javascript" src='/templates/tooltip.js'></script>		
			
<script language="javascript">
var jsCurCategoryID = '';
var jsCurEnrolGroupID = '<?=$EnrolGroupID?>';
var jsCanEdit = '<?=$canEdit?>';
var jsPageAction = '<?=$action?>';

$(document).ready( function () {
	<? if ($isEnrolAdmin || $isEnrolMaster) { ?>
		jsCurCategoryID = $('select#sel_category').val();
		js_Reload_Club_Selection(jsCurEnrolGroupID);
	<? } else { ?>
		Reload_Club_Attendance_Info_Table();
	<? } ?>
});

function js_Changed_Category_Selection(jsCategoryID)
{
	jsCurCategoryID = jsCategoryID;
	js_Reload_Club_Selection('');
}

function js_Reload_Club_Selection(jsEnrolGroupID)
{
	$('span#ClubSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Club_Selection',
			ID: 'EnrolGroupID',
			Name: 'EnrolGroupID',
			TargetEnrolGroupID: jsEnrolGroupID,
			CategoryID: jsCurCategoryID,
			IsAll: 0,
			NoFirst: 1,
			OnChange: 'js_Changed_Club_Selection(this.value);',
			AcademicYearID: <?=$AcademicYearID?>			
		},
		function(ReturnData)
		{
			jsCurEnrolGroupID = $('select#EnrolGroupID').val();
			Reload_Club_Attendance_Info_Table();
		}
	);
}

function js_Changed_Club_Selection(jsEnrolGroupID)
{
	jsCurEnrolGroupID = jsEnrolGroupID;
	Reload_Club_Attendance_Info_Table();
}

function Reload_Club_Attendance_Info_Table2(jsEnrolGroupID,jsCanEdit,jsPageAction, jsDisplayPhoto)
{
	$('div#div_Club_AttendanceTable').html('');
	$('div#div_Club_AttendanceTable').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Reload_Club_Attendance_Table',
			EnrolGroupID: jsEnrolGroupID,
			CanEdit: jsCanEdit,
			PageAction: jsPageAction,
			DisplayPhoto : jsDisplayPhoto
		},
		function(ReturnData)
		{
			var jsScreenWidth = parseInt(Get_Screen_Width());
        	var jsTableWidth = jsScreenWidth - 250;
        	
        		$("table#AttendanceTable").toSuperTable({
			    	width: jsTableWidth + "px", 
			    	height: "500px", 
			    	fixedCols: 3,
                    cssSkin: 'common_table_list',
			    	onFinish: 	function () 
			    				{
			    					fixSuperTableHeight('AttendanceTable', <?=$FixTableSize?>);
			    				}
				});
		}
	);
}

function changeActivity(obj) {
	var activityID = obj.value;
	if (typeof activityID != "undefined") {
		Reload_CLUB_Attendance_Info_Schedule_Detail_Table(activityID)
	}
}

function Reload_CLUB_Attendance_Info_Schedule_Detail_Table(ActivityID) {
	$('div#ContentTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Club_Attendance_Info_Table_Schedule_Detail',
			EnrolGroupID: jsCurEnrolGroupID,
			ActivityID: ActivityID,
			CanEdit: jsCanEdit,
			PageAction: jsPageAction,
			AcademicYearID: <?=$AcademicYearID?>
			},
			function(ReturnData)
			{
				var jsScreenWidth = parseInt(Get_Screen_Width());
        		var jsTableWidth = jsScreenWidth - 250;
        		$("table#AttendanceTable").toSuperTable({
			    	width: jsTableWidth + "px", 
			    	height: "500px", 
			    	fixedCols: 3,
			    	onFinish: 	function () 
			    				{
			    					fixSuperTableHeight('AttendanceTable', <?=$FixTableSize?>);
<?php
if (false && $userBrowser->platform=="iPad" && $userBrowser->platform=="Andriod")
{
?>
		    					fixSuperTableWidth('AttendanceTable', 300000);
<?php
}
?>
		    					//$(".sSky").css("font-size","0.9em");
		    				}
			});
		}
	);
}

function Reload_Club_Attendance_Info_Table()
{
	if (jsCurEnrolGroupID == '')
	{
		$('div#ContentTableDiv').html('');
	}
	else
	{
		$('div#ContentTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{
				Action: 'Club_Attendance_Info_Table',
				EnrolGroupID: jsCurEnrolGroupID,
				CanEdit: jsCanEdit,
				PageAction: jsPageAction,
				AcademicYearID: <?=$AcademicYearID?>
			},
			function(ReturnData)
			{
				var jsScreenWidth = parseInt(Get_Screen_Width());
        		var jsTableWidth = jsScreenWidth - 250;
        		$("table#AttendanceTable").toSuperTable({
			    	width: jsTableWidth + "px", 
			    	height: "500px", 
			    	fixedCols: 3,
                    cssSkin: 'common_table_list',
			    	onFinish: 	function () 
			    				{
			    					fixSuperTableHeight('AttendanceTable', <?=$FixTableSize?>);
<?php
if (false && $userBrowser->platform=="iPad" && $userBrowser->platform=="Andriod")
{
?>
			    					fixSuperTableWidth('AttendanceTable', 300000);
<?php
}
?>
			    					//$(".sSky").css("font-size","0.9em");
			    				}
				});
			}
		);
	}
}

function CheckAll(ColNo)
{
	var ApplyToAllVal = '';
	$("select.Header_Col"+ColNo).each(function(){
		if($(this).val() != ''){
			ApplyToAllVal = $(this).val(); 
		}
	});
	$("select.Col"+ColNo).val(ApplyToAllVal);

	ApplyToAllVal = '';
	$("input.Header_Col"+ColNo).each(function(){
		if($(this).val() != ''){
			ApplyToAllVal = $(this).val(); 
		}
	});
	$("input.Col"+ColNo).val(ApplyToAllVal);
}

function js_Cancel()
{
	window.location = "group.php?AcademicYearID=<?=$AcademicYearID?>";
}

function js_Scroll_Right() {
	$(".sData").scrollLeft($(".sData").scrollLeft() + 10);
}

//Henry added 20131101 [start]
function onloadThickBox(GroupDateID, EnrolGroupID) {
	$('div#TB_ajaxContent').load(
		"get_mail_to_absence_stu_tb.php", 
		{ 
			groupDateID: GroupDateID,
			enrolGroupID: EnrolGroupID,
			queryStr: "<?=$_SERVER['QUERY_STRING']?>"
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
//Henry added 20131101 [end]

//Grardian Info Added by Kenneth 2015-11-27
// var callback = {
//     success: function ( o )
//     {
//     	jChangeContent( "ToolMenu2", o.responseText );
//     }
// }
function retrieveGuardianInfo(UserID,event)
{   
    obj = document.form2;
    var myElement = document.getElementById("ToolMenu2");
//     showMenu("ToolMenu2","<table border='0' width='300' cellpadding='3' cellspacing='0'><tr class='tablebluetop'><td class='tabletoplink'>Loading</td></tr></table>");
	myElement.style.display = 'block';
	myElement.style.visibility = 'visible';
	 $.ajax({
	        url: "getGuardianInfo.php",
	        data:{
	        	targetUserID : UserID
	        },
	        success: function(msg){
	        	$('#ToolMenu2').html(msg);
	        }
	    	
	    });
// 	YAHOO.util.Connect.setForm(obj);
//     var path = "getGuardianInfo.php?targetUserID=" + UserID;
//     var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
    moveObject('ToolMenu2', event);
    
}

function moveObject( obj, e, moveLeft ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 8;
	var objHolder = obj;
	var moveDistance = 0;
	
	obj = document.getElementById(obj);
	if (obj==null) {return;}
	
	if (document.all) {
		var ScrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
		var ScrollHeight = document.documentElement.scrollTop || document.body.scrollTop;
		tempX = event.clientX + ScrollLeft;
		tempY = event.clientY + ScrollHeight;
	}
	else {
		tempX = e.pageX
		tempY = e.pageY
	}

	if(moveLeft == undefined) {
		moveDistance = 300;		
	} else {
		moveDistance = moveLeft;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + 300) + 'px';
}

function closeLayer(LayerID) {
	var obj = document.getElementById(LayerID);
// 	obj.style.display = 'none';
	obj.style.visibility = 'hidden';
}

</script>
<style type="text/css">
#ToolMenu2 {
	position:absolute;
	top: 0px;
	left: 0px;
	z-index:10;
}
</style>
<div id="ToolMenu2" class="selectbox_layer" style="width:550px; visibility:hidden;"></div>
<form id="form2" name="form2" method="post" action="" >
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>