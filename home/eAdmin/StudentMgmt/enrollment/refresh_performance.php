<?php
# using: 
#############################################
#
#   Date    2020-01-07 Tommy
#           file created
#
#############################################
$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgroup.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$LibUser = new libuser($UserID);

if (!isset($AcademicYearID) || $AcademicYearID=="")
    $AcademicYearID = Get_Current_Academic_Year_ID();
else if($AcademicYearID != Get_Current_Academic_Year_ID() || $type != 2)
    header("Location: group.php?clearCoo=1");
    
$libenroll = new libclubsenrol($AcademicYearID);

$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "group.php",);
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['Refresh_Performance']['Name']);

$yearOption = getAcademicYearByAcademicYearID($AcademicYearID);
$semSelection = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, $OnChange="change_field(this);", $NoFirst=1, $NoPastTerm=0, $withWholeYear=1);

$formAction = "refresh_performance_update.php";

$CurrentPage = "PageMgtClub";
$CurrentPageArr['eEnrolment'] = 1;
if (($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)) {
    if ($helper_view)
        $CurrentPage = "PageClubAttendanceMgt";
        
        $CurrentPageArr['eEnrolment'] = 0;
        $CurrentPageArr['eServiceeEnrolment'] = 1;
}
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">

function change_field(selected)
{
	var semester = 0;

	if(Number.isInteger(selected)){
		semester = selected;
	}else{
		semester = selected.value;
	}

	$.ajax({
   		url: "ajax_change_semester.php",
   		method: "GET",
   		data: {settingId:semester, AcademicYearID:"<?=$AcademicYearID?>"},
    	success : function(data){
    		document.getElementById('LastGenDiv').innerHTML = data;
    	}
	});
}

// function js_Reload_Term_Selection(jsAcademicYearID)
// {
// 	$('#termSelectionDiv').load(
// 		"ajax_conduct.php", 
// 		{ 
// 			task: 'reload_term_selection',
// 			AcademicYearID: jsAcademicYearID
// 		},
// 		function(ReturnData)
// 		{
// 			change_field('ajaxSemester', 0);
// 		}
// 	);
// }
</script>
<br />
<body onload="change_field(<?=$Semester?>)">
<form name="form1" method="post" action="<?=$formAction?>">

			<table width="98%" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
						<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
						</table>
					</td>
				</tr>
				
				
				<tr>
					<td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td align="center">
								<fieldset class="instruction_box">
										<legend class="instruction_title"><?=$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction1?></legend>
										<?=$Lang['eEnrolment']['Refresh_Performance']['Role']?><br>
										<span class="tabletextrequire"><?=$Lang['eEnrolment']['Refresh_Performance']['extraInfo']?></span>
								</fieldset>
					</td></tr>
					</table>
					</td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr>
					<td align="center">
						<table width="80%" border="0" cellspacing="5" cellpadding="5" align="center">
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Profile_Year?></td>
								<td width="80%"><?=$yearOption?></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Profile_Semester?></td>
								<td width="80%" class="tabletextremark"><div id="termSelectionDiv"><?=$semSelection?></div></td>
							</tr>
							
							<tr valign="top">
								<td>&nbsp;</td>
								<td width="80%" class="tabletextremark"><?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <span id="LastGenDiv"><?=$LastGen?></div></td>
							</tr>
							<tr><td colspan="2"><span class="tabletextremark"><?=$i_general_required_field?></td></tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr><td colspan="2" align="center">
					<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
					<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
					<? echo $linterface->GET_ACTION_BTN($button_back, "button", "","back"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"history.back()\""); ?>
				</td></tr>
				</table><br>
				<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="<?=$AcademicYearID?>">
				<input type="hidden" id="type" name="type" value="<?=$_GET["type"]?>">

</form>
</body>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>