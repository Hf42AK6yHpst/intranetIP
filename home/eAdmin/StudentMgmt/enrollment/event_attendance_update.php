<?php
// Using: Pun
/******************************************************
 *  modification log
 *  2019-08-09 Ray
 *  - Added SubmittedAbsentLateRecord
 *	2018-07-03 Pun [121822] [ip.2.5.9.7.1]
 *  - Added save remarks
 *  20160725 Anna :
 *  	- loop POST change to loop attendanceArr
 *  	- Add function CHANGE_ACTIVITY_ATTENDENCE_STATUS 
 * *****************************************************/


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$json = new JSON_obj();

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();

$DataArr['EnrolEventID'] = IntegerSafe($EnrolEventID);
$DataArr['StaffRole'] = $_POST['StaffRole'];

//$libenroll->DEL_EVENT_ATTENDENCE($EnrolEventID);
## delete updated dates only
$disabledArr = unserialize(rawurldecode($disabledArr));

//get all group dates
$sql = "SELECT EventDateID FROM INTRANET_ENROL_EVENT_DATE WHERE EnrolEventID = '$EnrolEventID'";
$allDateArr = $libenroll->returnVector($sql);

// construct a list for deletion
$notDeleteDateArr = array();
$deleteDateArr = array();
foreach ($disabledArr as $key => $value)
{
	if ($value == "DISABLED")
	{
		$notDeleteDateArr[] = $key;	
	}	
}
$deleteDateArr = array_diff($allDateArr, $notDeleteDateArr);
$deleteDateList = implode(",", $deleteDateArr);

if (isset($_POST["ActivityID"]) && $_POST["ActivityID"] > 0) {
	// delete those date which is not DISABLED only
	$sql = "DELETE FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE EnrolEventID = '".$DataArr['EnrolEventID']."' AND EventDateID IN ('" . $_POST["ActivityID"] . "')";
	$libenroll->db_db_query($sql);
} else {
	// delete those date which is not DISABLED only
	$sql = "DELETE FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE EnrolEventID = '".$DataArr['EnrolEventID']."' AND EventDateID IN ( $deleteDateList )";
	$libenroll->db_db_query($sql);
}

$postArr = $_POST['attendanceArr'];
$jsonAttendance = $json->encode($postArr);
$postRemarkArr = $_POST['attendanceRemarkArr'];
$jsonAttendanceRemark = $json->encode($postRemarkArr);
// debug_pr($postArr);
//  die();
if (sizeof($_POST) > 0) {
// 	while (list($key, $value) = each($_POST)) {
	foreach((array)$postArr as $key => $value){
	    if (is_numeric($key))
	    {
	    	$DataArr['EventDateID'] = $key;
// 	    	while (list($StudentID, $Status) = each($value))
			foreach((array)$value as $StudentID => $Status)
			{
	    		if($Status)
	    		{
	    			$DataArr['StudentID'] = $StudentID;
	    			$DataArr['Status'] = $Status;
	    			$DataArr['Remark'] = $postRemarkArr[$key][$StudentID];
				    $libenroll->ADD_STU_EVENT_ATTENDENCE($DataArr);
// 				    debug_pr($DataArr);
// 				    die();

					if($sys_custom['eEnrolment']['SubmitAbsentLateRecordsToExternalSystem']) {
						$TargetStatus = false;
						if ($Status == ENROL_ATTENDANCE_ABSENT) {
							$TargetStatus = CARD_STATUS_ABSENT;
						} else if ($Status == ENROL_ATTENDANCE_LATE) {
							$TargetStatus = CARD_STATUS_LATE;
						}

						if($TargetStatus !== false) {
							$EventDateArr = $libenroll->GET_ENROL_EVENT_DATE($DataArr['EnrolEventID'], '', '', $DataArr['EventDateID']);
							if ($EventDateArr) {
								$RecordDate = date("Y-m-d", strtotime($EventDateArr[0]['ActivityDateStart']));
								$lcardattend->InsertSubmittedAbsentLateRecord($StudentID, $RecordDate, '', '', $DataArr['EnrolEventID'], $TargetStatus, '', '');

							}
						}
					}
	    		}
	    	}
	    }
	}
}


// CHANGE_ACTIVITY_ATTENDENCE_STATUS($EnrolEventID, $jsonAttendance);

// function CHANGE_ACTIVITY_ATTENDENCE_STATUS($EnrolEventID, $jsonAttendance) {
// 	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT.'includes/liblog.php');

//	global $libenroll;

	// 	$sql = "Select
	// 				EventTitle
	// 			From
	// 				INTRANET_ENROL_EVENTINFO
	// 			Where
	// 				EnrolEventID = '".$ParEnrolEventID."'";

// $sql = "SELECT
// 				eventattendance.EnrolEventID,
// 				eventattendance.StudentID,
// 				eventattendance.DateModified,
// 				eventattendance.RecordStatus,
// 				eventattendance.LastModifiedBy
				
				
// 			FROM
// 				INTRANET_ENROL_EVENT_ATTENDANCE as eventattendance
// 				INNER JOIN INTRANET_ENROL_EVENTINFO as eventinfo on (eventattendance.EnrolEventID = eventinfo.EnrolEventID)
// 			WHERE
// 				eventattendance.EnrolEventID = '$EnrolEventID'
	
// 	";


// 	$allDateArr = $libenroll->returnArray($sql);



	# insert log
	$liblog = new liblog();
// 	$tmpAry = array();
// 	$tmpAry['EnrolEventID'] = $EnrolEventID;
// 	$tmpAry['StudentID'] = $StudentID;
	$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Student_Activity_Attendance_Status', $jsonAttendance, ' INTRANET_ENROL_EVENT_ATTENDANCE', $EnrolEventID);
	$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Student_Activity_Attendance_Remark', $jsonAttendanceRemark, ' INTRANET_ENROL_EVENT_ATTENDANCE', $EnrolEventID);
// 	return $ReturnVal;
// }



intranet_closedb();
header("Location: event_attendance_mgt.php?AcademicYearID=$AcademicYearID&EnrolEventID=".$EnrolEventID."&msg=2");





?>