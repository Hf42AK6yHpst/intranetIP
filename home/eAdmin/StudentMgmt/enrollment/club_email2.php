<?php
## Using By : Max
###########################################
##	Modification Log:
##	2010-01-08 Max (201001081039)
##	Send Email for Enrollment Result
###########################################
#Based on member_email.php
/* Debug using parameters */
$BR = "<br />";
$HR = "<hr />";

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li = new libgroup($GroupID);


if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();	
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	if ($type == "activity")
	{
		$CurrentPage = "PageMgtActivity";
		$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		$CurrentPageArr['eEnrolment'] = 1;

	    # tags 
        $tab_type = "activity";
        # navigation
        $PAGE_NAVIGATION[] = array($EventInfoArr[3]." ".$button_email, "");
	}
	else
	{
		$CurrentPage = "PageMgtClub";
		$CurrentPageArr['eEnrolment'] = 1;
		
		# tags 
        $tab_type = "club";
        # navigation
        $PAGE_NAVIGATION[] = array($li->Title." ".$button_email, "");
	}	
	
	$current_tab = 1;
    include_once("management_tabs.php");
        
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
	    
	    # get student name, class and class number for display
	    $name_field = getNameFieldWithClassNumberByLang();
	    $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($studentList) ORDER BY ClassName,ClassNumber";
	    $studentDisplayArr = $libenroll->returnVector($sql);
	    
	    $toRowsHTML = "";
	    for ($i=0; $i<sizeof($studentDisplayArr); $i++)
	    {
		    # show "To" in the first row
			if ($i==0)
			{
				$toRowsHTML .= "<tr><td class=\"tabletext\" width=\"30%\" valign=\"top\">";
				$toRowsHTML .= $i_email_to;
			}
			else if ($i==sizeof($studentDisplayArr)-1)
			{
				$toRowsHTML .= "<tr><td class=\"tabletext formfieldtitle\" width=\"30%\" valign=\"top\">";
				$toRowsHTML .= "&nbsp;";
			}
			else
			{
				$toRowsHTML .= "<tr><td class=\"tabletext\" width=\"30%\" valign=\"top\">";
				$toRowsHTML .= "&nbsp;";
			}
			$toRowsHTML .= "</td>";
			
			$toRowsHTML .= "<td class=\"tabletext\">{$studentDisplayArr[$i]}</td>";
			$toRowsHTML .= "</tr>";
	    }
	    
	    
	    /* Start Used by Steps */
	    $STEPS_OBJ[] = array("Select student(s)", 0);
        $STEPS_OBJ[] = array("Compose email", 1);
	    /* End Used by Steps */
	    
	    /* S: Get the list of to */
		$rankTarget = $_POST['rankTarget'];
		$detailIdentifier = implode(",", $_POST['rankTargetDetail']);
		$sql = "";
		$conn = "";
		$result = "";
		if (isset($rankTarget)) {
			$conn = new libdb();
			
			switch ($rankTarget) {
				case "form":
					$sql = "SELECT DISTINCT Y.YEARID, Y.YEARNAME AS DISPLAY
							FROM YEAR Y LEFT JOIN YEAR_CLASS YC ON Y.YEARID = YC.YEARID
							WHERE Y.YEARID IN (".$detailIdentifier.")";
					break;
				case "class":
					$sql = "SELECT DISTINCT YC.YEARCLASSID, YC.CLASSTITLE" . strtoupper($intranet_session_language) . " AS DISPLAY
							FROM YEAR_CLASS YC WHERE YC.YEARCLASSID IN (".$detailIdentifier.")";
					break;
				case "student":
					$name = $intranet_session_language=="en"?"ENGLISHNAME":"CHINESENAME";
					$studentID = implode(",", $_POST['studentID']);
					$sql = "
						SELECT
							DISTINCT IU.USERID, IU.$name AS DISPLAY
						FROM
						  YEAR Y LEFT JOIN YEAR_CLASS YC ON Y.YEARID = YC.YEARID
						  LEFT JOIN YEAR_CLASS_USER YCU ON YC.YEARCLASSID = YCU.YEARCLASSID
						  LEFT JOIN INTRANET_USER IU ON YCU.USERID = IU.USERID
						WHERE 
						  YCU.USERID IN (".$studentID.")
										";
					break;
			}
			//if (!empty($sql)) $sql .= " AND YC.ACADEMICYEARID = 1 ";
			if (!empty($sql)) $sql .= " ORDER BY DISPLAY ";
			
			$result = $conn->returnArray($sql);
			$sizeofResult = count($result);
			$toArray = array();
			for($i=0;$i<$sizeofResult;$i++) {
				$toArray[] = $result[$i]["DISPLAY"];
			}
			$displayToField = implode(", ", $toArray);
		}
		/* E: Get the list of to */
	    
	    $linterface->LAYOUT_START();
?>

<script language="javascript">
function checkform(obj){
	if(!check_text(obj.subject, "<?php echo $i_alert_pleasefillin.$i_email_subject; ?>.")) return false;
	if(!check_text(obj.message, "<?php echo $i_alert_pleasefillin.$i_email_message; ?>.")) return false;
	return (confirm("<?php echo $i_email_sendemail; ?>?")) ? true : false;
}
</script>

<form name="form1" action='club_email_update.php' method='post' onSubmit="return checkform(this);">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>
<!-- Start Steps -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
		</td>
	</tr>
</table>
<!-- End Steps -->

<table width='90%' border='0' cellpadding='4' cellspacing='0'>
<?= $toRowsHTML ?>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><?=$i_email_to?></td>
	<td class="tabletext"><?=$displayToField?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><?php echo $i_email_subject; ?></td>
	<td class="tabletext"><input class='textboxtext' type='text' name='subject'></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><?=$eEnrollment['Content']?></td>
	<td><?= $linterface->GET_TEXTAREA("message", "", $taCols=55, $taRows=15) ?></td>
</tr>
</table>

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_send, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
</div>
</td></tr>
</table>

</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>