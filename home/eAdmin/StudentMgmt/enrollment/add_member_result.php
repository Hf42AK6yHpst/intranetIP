<?
// using: 
#########################################################
#   Date:   2019-07-23 (Anna)
#           changed enrol admin can ignore time clash only one club checking
#
#   Date:   2019-02-28 (Anna) [K149026]
#           Fixed $crashedStudentArr and $crashedEnrolGroupIDArr order 
#
#	Date:	2017-10-06 (Anna)
#			Added crashedClubEnrolGroupIDArr, crashedEventIDArr
#			Added PIC name list after club/activity title
#
#	Date:	2015-10-12	(Omas)
#			Fix : add $filter for type 2 button - add other student 
#
#	Date:	2015-10-07 (Omas)
#	 		-Improved $sys_custom['eEnrolment']['ActivityAddRole'] , Get Role title from ID
#
#	Date:	2015-06-29	(Omas)
#			Improvement - when RoleID = '' do not run that sql 
#
#	Date:	2015-06-12	(Evan)
#			Update table style
#
#	Date:	2014-12-02	(Omas)   ---- (cancelled on 2015-01-02)
#			For $oneEnrol != 1 ,change font color when have time crash to orange
#			For $oneEnrol == 1 , font color keep as red
#
#	Date:	2014-10-14	(Omas)
#			type0, 3
#			Added function goUpdatePerformance() for button route to Update Performance & Achievement
#			
# 	Date: 	2012-12-20 (Rita)
# Details:	add submit reason btn for customization - enableActivityFillInEnrolReasonRight() [#2012-0803-1543-49054]
# 
##########################################################

$PATH_WRT_ROOT="../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

# all selected students for adding
$data = unserialize(rawurldecode($data));
# error code for rejection
$result_ary = unserialize(rawurldecode($result_ary));
# students who are approved
$imported_student = unserialize(rawurldecode($imported_student));
# 2D array - index: studentID ; value:all time-crashed groups 
$crash_EnrolGroupID_ary = unserialize(rawurldecode($crash_EnrolGroupID_ary));
# 2D array - index: studentID ; value:all time-crashed activities 
$crash_EnrolEventID_ary = unserialize(rawurldecode($crash_EnrolEventID_ary));
# all time-crashed students (used after approving the time-crashed students) 
$crashedStudentArr = unserialize(rawurldecode($crashedStudentArr));

# import-related array
if ($fromImport)
{
	if (isset($RoleIDArr))
	{
		$RoleIDArr = unserialize(rawurldecode($RoleIDArr));
	}
	if (isset($RoleTitleArr))
	{
		$RoleTitleArr = unserialize(rawurldecode($RoleTitleArr));
	}
	if (isset($NotFoundClassNameArr))
	{
		$NotFoundClassNameArr = unserialize(rawurldecode($NotFoundClassNameArr));
	}
	if (isset($NotFoundClassNumberArr))
	{
		$NotFoundClassNumberArr = unserialize(rawurldecode($NotFoundClassNumberArr));
	}
	if (isset($ImportIDArr))
	{
		$ImportIDArr = unserialize(rawurldecode($ImportIDArr));
	}
	if (isset($ImportTitleArr))
	{
		$ImportTitleArr = unserialize(rawurldecode($ImportTitleArr));
	}
}

$lu = new libuser();
$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$libgroup = new libgroup($GroupID);
$linterface = new interface_html();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID) && !$libgroup->hasAdminBasicInfo($UserID) && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
{
	//header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$enrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);

/*
 *	type=0 => add club member with Role selection (from member_index.php)
 *  type=1 => add activity student with RecordStatus selection (from event_member.php)
 *  type=2 => add club member with RecordStatus selection (from group_member.php)
 *  type=3 => add activity student with Role selection (from event_member_index.php)
 */
if($sys_custom['eEnrolment']['ActivityAddRole']){
	// using RoleID to get the Role Title value
	$sql = "SELECT RoleID ,Title FROM INTRANET_ROLE WHERE RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'";
	$roleArr = BuildMultiKeyAssoc( $libenroll->returnResultSet($sql) , 'RoleID', array('Title'),1);
}

if ($type==0 || $type==2 || $from=="ClubEnrolList"){	//from club enrolment
	$CurrentPage = "PageMgtClub";
	$tab_type = "club";
	if ($type==0)
    {
        $current_tab = 1;
    }
    else
    {
        $current_tab = 2;
    }
    $oneEnrol = $libenroll->oneEnrol;
}
else if ($type==1 || $type==3 || $from=="EventEnrolList"){	//from activity enrolment
	$CurrentPage = "PageMgtActivity";
	$tab_type = "activity";
    if ($type==3)
    {
        $current_tab = 1;
    }
    else
    {
        $current_tab = 2;
    }
    $oneEnrol = $libenroll->Event_oneEnrol;
}		
include_once("management_tabs.php");
	
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);

if ($type==0){
	# navigation
	if ($fromImport == "1")
	{
		$PAGE_NAVIGATION[] = array($eEnrollment['import_member'], "");
	}
	else
	{
		$PAGE_NAVIGATION[] = array($eEnrollment['add_member_and_assign_role'], "");
	}
	//$TAGS_OBJ[] = array($li->Title." ".$eEnrollment['assigning_member'], "", 1);
}
else if ($type==1 || $type==3 || $from=="EventEnrolList"){
	# navigation
	if ($fromImport == "1")
	{
		$PAGE_NAVIGATION[] = array($eEnrollment['import_participant'], "");
	}
	else
	{
		$PAGE_NAVIGATION[] = array($eEnrollment['add_participant_and_assign_role'], "");
	}
	//$TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['assigning_student'], "", 1);
}
else if ($type==2 || $from=="ClubEnrolList"){
	# navigation
    $PAGE_NAVIGATION[] = array($eEnrollment['add_member_and_assign_role'], "");
	//$TAGS_OBJ[] = array($li->Title." ".$eEnrollment['assigning_enrolment'], "", 1);
}

if ($fromImport == "1")
{
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
}
else
{
	$STEPS_OBJ[] = array($eEnrollment['select_student'], 0);
	$STEPS_OBJ[] = array($eEnrollment['lookup_for_clashes'], 1);
}

$linterface->LAYOUT_START();

### List out the import result
$resultTable = $linterface->GET_NAVIGATION2($eEnrollment['lookup_for_clashes']);
$resultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$resultTable .= "<tr><td class=\"tabletext\">$list_total : ". count($data) .", {$eEnrollment['succeeded']} : ". count($imported_student) .", ".$eEnrollment['failed'].": ". (count($data) - count($imported_student)) ."</td></tr>\n";	
$resultTable .= "</table>\n";

//Result Table Title
$resultTable .= "<table class=\"common_table_list_v30 view_table_list_v30\">\n";
$resultTable .= "<thead><tr>\n";
$resultTable .= "<th>#</td>\n";
if ($importType == "clubAllMember")
{
	$resultTable .= "<th>". $eEnrollment['club_name'] ."</th>\n";
}
else if ($importType == "eventAllParticipant")
{
	$resultTable .= "<th>". $eEnrollment['act_name'] ."</th>\n";
}
$resultTable .= "<th>$i_general_class</td>\n";
$resultTable .= "<th>{$eEnrollment['class_no']}</th>\n";
$resultTable .= "<th>$i_UserEnglishName</th>\n";
$resultTable .= "<th>$i_UserChineseName</th>\n";

if ($type==0 || $type==3)
{	
	$resultTable .= "<th>{$eEnrollment['role']}</td>\n";
}
elseif ($type==1 || $type==2 || $from=="ClubEnrolList" || $from=="EventEnrolList")
{
	$resultTable .= "<th>{$i_ClubsEnrollment_Status}</th>\n";
}

if($libenroll->enableActivityFillInEnrolReasonRight()){
	if ($type==1 || $type==3 || $from=="EventEnrolList"){
		if($fromImport != "1"){
			$resultTable .= "<th>".$Lang['eEnrolment']['Reason']."</th>\n";
		}
	}
}

$resultTable .= "<th>$i_SAMS_import_error_reason</th>\n";

$resultTable .= "</tr></thead>\n";


if ((count($crash_EnrolGroupID_ary) > 0) || (count($crash_EnrolEventID_ary) > 0))
{
    
	$TableActionArr = array();
	$TargetFile = '';
	if ($from == "ManualAdd")
		$TargetFile = 'add_member_update.php';
	elseif ($from == "ClubEnrolList")
		$TargetFile = 'group_stu_approve_update.php';
	elseif ($from == "EventEnrolList")
		$TargetFile = 'event_stu_approve_update.php';
	$TableActionArr[] = array("javascript:submitForm(document.form1, '".$TargetFile."')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_approve.gif", "imgApprove", $button_approve);
	
	
	//Time Crash Table Title
	$TimeCrashTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
	$TimeCrashTable .= "<tr><td align=\"left\">\n";
	$TimeCrashTable .= $linterface->GET_NAVIGATION2($eEnrollment['time_conflict']);
	$TimeCrashTable .= "</td>\n";
	$TimeCrashTable .= "<td align=\"right\">\n";
	$TimeCrashTable .= $libenroll->GET_TABLE_ACTION_BTN($TableActionArr);
	$TimeCrashTable .= "</td></tr></table>\n";
	
	$TimeCrashTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
	$TimeCrashTable .= "<tr>\n";
	$TimeCrashTable .= "<td width=\"10\" class=\"tabletop tabletopnolink\">#</td>\n";
	$TimeCrashTable .= "<td width=\"10%\" class=\"tabletop tabletopnolink\">$i_ClassName</td>\n";
	$TimeCrashTable .= "<td width=\"10%\" class=\"tabletop tabletopnolink\">$i_ClassNumber</td>\n";
	$TimeCrashTable .= "<td width=\"15%\" class=\"tabletop tabletopnolink\">$i_UserEnglishName</td>\n";
	$TimeCrashTable .= "<td width=\"15%\" class=\"tabletop tabletopnolink\">$i_UserChineseName</td>\n";
	$TimeCrashTable .= "<td class=\"tabletop tabletopnolink\">{$eEnrollment['club_or_act_title']}</td>\n";
	$TimeCrashTable .= "<td width=\"5%\" class=\"tabletop tabletopnolink\" align=\"center\">\n";
	if($oneEnrol == 1 && !$enrolAdmin)
	{
		$TimeCrashTable .= "<input type=\"checkbox\" DISABLED>\n";
	}
	else
	{
		$TimeCrashTable .= "<input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,document.form1,'uid[]'):setChecked(0,document.form1,'uid[]')\">\n";
	}
	$TimeCrashTable .= "</td>\n";
	$TimeCrashTable .= "</tr>\n";
}
        


$counterTimeCrashTable = 0;
$crashedStudentArr = array();
$crashedRoleIDArr = array();
$crashedRoleTitleArr = array();

$crashedImportTitleArr = array();
$crashedImportIDArr = array();
$crashedClubEnrolGroupIDArr= array();
$crashedEventIDAry=array();

$notFoundStudentCounter = 0;

for($i=0;$i<count($data);$i++)
{
	$StudentID = $data[$i];
	
	if ($fromImport)
	{
		$RoleID = $RoleIDArr[$i];
	}
	
	//get student class, class nubmer, and student name
	$StudentInfoArr = array();
	if ($StudentID == -1)
	{
		$StudentInfoArr[0]['ClassName'] = $NotFoundClassNameArr[$notFoundStudentCounter];
		$StudentInfoArr[0]['ClassNumber'] = $NotFoundClassNumberArr[$notFoundStudentCounter];
		$notFoundStudentCounter++;
	}
	else
	{
		$sql = "SELECT ClassName, ClassNumber, EnglishName, ChineseName FROM INTRANET_USER WHERE UserID = '$StudentID'";
		$StudentInfoArr = $libenroll->returnArray($sql,4);
	}
	
	if($RoleID>0){
		//get role title
		$sql = "SELECT Title FROM INTRANET_ROLE WHERE RoleID = '$RoleID'";
		$result = $libenroll->returnArray($sql,1);
		$roleTitle = $result[0][0];
	}
	else{
		$roleTitle = '';
	}
	
	//get application status
	/*
	switch ($enrolStatus)
	{
		case 0: $statusText = $i_ClubsEnrollment_StatusWaiting;
				break;
		case 1: $statusText = $i_ClubsEnrollment_StatusRejected;
				break;
		case 2: $statusText = $i_ClubsEnrollment_StatusApproved;
				break;
	}
	*/
	
	if ($GroupID != '')
	{
		$sql = "	Select
							CASE RecordStatus
								WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
								WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
								WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
							END
					From
							INTRANET_ENROL_GROUPSTUDENT
					Where
							StudentID = '".$StudentID."'
							AND
							GroupID = '".$GroupID."'
				";
	}
	else
	{
		$sql = "	Select
							CASE RecordStatus
								WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
								WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
								WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
							END
					From
							INTRANET_ENROL_EVENTSTUDENT
					Where
							StudentID = '".$StudentID."'
							AND
							EnrolEventID = '".$EnrolEventID."'
				";
	}
	$thisRecordStatusArr = $libenroll->returnVector($sql);
	$statusText = $thisRecordStatusArr[0];
		
	$status = in_array($StudentID, (array)$imported_student) ? "": $eEnrollment[$result_ary[$i]]; 
	$css = "tabletext";
	if ($oneEnrol && !$enrolAdmin){
		$fontColorStart = $status ? "<span class=\"tabletextrequire\">":"";
	}
	else{
		//$fontColorStart = $status ? "<span class=\"tabletextrequire3\">":"";
		$fontColorStart = $status ? "<span class=\"tabletextrequire\">":"";
	}
	$fontColorEnd = $status ? "</span>":"";
	$this_remark = str_replace('""', '"', $Remark);
	
	$resultTable .= "<tr class=\"tablebluerow".($i%2+1)."\">\n";
	$resultTable .= "<td class=\"$css\">".($i+1)."</td>\n";
	# display club / activity name if from import of all clubs / activities
	if ($importAll)
	{
		$thisTitle = stripslashes($ImportTitleArr[$i]);
		$resultTable .= "<td class=\"$css\">". $thisTitle ."</td>\n";
	}
	$resultTable .= "<td class=\"$css\">". $StudentInfoArr[0]['ClassName'] ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $StudentInfoArr[0]['ClassNumber'] ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $StudentInfoArr[0]['EnglishName'] ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $StudentInfoArr[0]['ChineseName'] ."</td>\n";
	
	if ($type==0)
	{	
		$resultTable .= "<td class=\"$css\">". $roleTitle ."</td>\n";
	}
	elseif ($type==3)
	{
		if ($fromImport == "1")
		{
			$thisRoleTitle = stripslashes($RoleTitleArr[$i]);
		}
		else
		{
			$thisRoleTitle = stripslashes(stripslashes($_POST['RoleTitle']));
		}

		if( $sys_custom['eEnrolment']['ActivityAddRole'] ){
			$resultTable .= "<td class=\"$css\">". $roleArr[$thisRoleTitle]."</td>\n";
		}
		else{
			$resultTable .= "<td class=\"$css\">". $thisRoleTitle ."</td>\n";
		}	
	}
	elseif ($type==1 || $type==2 || $from=="ClubEnrolList" || $from=="EventEnrolList")
	{
		$resultTable .= "<td class=\"$css\" $fontColor>". $statusText ."</td>\n";
	}
	
	if($libenroll->enableActivityFillInEnrolReasonRight()){
		if (($type==1 || $type==3 || $from=="EventEnrolList")&& ($fromImport != "1")){
			$eventStudentEnrolInfo = $libenroll->GET_EVENTSTUDENT($EnrolEventID , $StudentID);
			$reasonValue = $eventStudentEnrolInfo[0]['Reason'];
				
			$resultTable .= "<td width\"15%\">" . $linterface->GET_TEXTBOX('approvalReason_'.$StudentID, 'approvalReason[]'.$StudentID, $reasonValue, $OtherClass='', $OtherPar=array()) ; 
			$resultTable .= "<input type='hidden' name='UserIDWithEnrolReason[]' value='".$StudentID."'>";
			$resultTable .= "</td>\n";
		
		}
	}
	
	
	$resultTable .= "<td class=\"$css\">".$fontColorStart.$status.$fontColorEnd."</td>\n";
	$resultTable .= "</tr>\n";
	
	if ($result_ary[$i] == "crash_group" || $result_ary[$i] == "crash_activity" || $result_ary[$i] == "crash_group_activity")
	{
		# Record the students so that the student can be shown after approval by teacher
	    $crashedStudentArr[$i] = $StudentID;
		
		if ($fromImport)
		{
			$crashedRoleIDArr[] = $RoleIDArr[$i];
			$crashedRoleTitleArr[] = $RoleTitleArr[$i];
		}
		
		if ($importAll)
		{
			$crashedImportTitleArr[] = $ImportTitleArr[$i];
			$crashedImportIDArr[] = $ImportIDArr[$i];
		}
		
		//construct time crash table content
		$TimeCrashTable .= "<tr class=\"tablerow".($counterTimeCrashTable % 2 + 1)."\">\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".($counterTimeCrashTable+1)."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$StudentInfoArr[0]['ClassName']."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$StudentInfoArr[0]['ClassNumber']."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$StudentInfoArr[0]['EnglishName']."</td>\n";
		$TimeCrashTable .= "<td valign=\"top\" class=\"tabletext\">".$StudentInfoArr[0]['ChineseName']."</td>\n";
		
		$TimeCrashTable .= "<td valign=\"top\" class=\"$css\">".$fontColorStart."\n";
		
		//get time crash CLUBS date and time
		for ($j=0; $j<count($crash_EnrolGroupID_ary[$i]); $j++)
		{
			//get title
			$thisEnrolGroupID = $crash_EnrolGroupID_ary[$i][$j];
			$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
			
			$ClubPICArr = $libenroll->GET_GROUPSTAFF($thisEnrolGroupID,'PIC');
			$ClubPICName= array();
			for($k=0;$k<sizeof($ClubPICArr);$k++){
				$ClubPICID =$ClubPICArr[$k]['UserID'];
				$ClubPICNameAry = $libenroll->Get_User_Info_By_UseID($ClubPICID);
				$ClubPICName[] = Get_Lang_Selection($ClubPICNameAry[0]['ChineseName'], $ClubPICNameAry[0]['EnglishName']);
				
			}
		
			$ClubPICNameList = implode(',',$ClubPICName); 

			
			$ClubInfo = $libenroll->getGroupInfo($thisGroupID);
			$TimeCrashTable .= $eEnrollment['crash_with']." ".$ClubInfo[0]['Title']." (".$Lang['eEnrolment']['ClubPIC'].":".$ClubPICNameList.")"."<br />\n";
			
			$crashedClubEnrolGroupIDArr[$i][] = $thisEnrolGroupID;
			//get dates
			//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($crash_EnrolGroupID_ary[$i][$j]);
			$DateArr = $libenroll->GET_ENROL_GROUP_DATE($thisEnrolGroupID);	//GroupDateID, EnrolGroupID, ActivityDateStart, ActivityDateEnd
			$numOfDate = count($DateArr);
			
			// construct a date array
			for ($k = 0; $k < $numOfDate; $k++) {
				$DateTitle = date("Y-m-d", strtotime($DateArr[$k][2]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][2]));
				$DateTitle .= " ".$eEnrollment['to']." ";
				$DateTitle .= date("Y-m-d", strtotime($DateArr[$k][3]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][3]));
				$TimeCrashTable .= $DateTitle."<br />\n";
				if ($k==count($DateArr)-1) $TimeCrashTable .= "<br />\n";
			}
		}
	
		//get time crash ACTIVITIES date and time
		for ($j=0; $j<count($crash_EnrolEventID_ary[$i]); $j++)
		{
			
			$EventPICArr = $libenroll->GET_EVENTSTAFF($crash_EnrolEventID_ary[$i][$j],'PIC');
			$EventPICName = array();
			for($k=0;$k<sizeof($EventPICArr);$k++){
				$EventPICID = $EventPICArr[$k]['UserID'];
				$EventPICNameAry = $libenroll->Get_User_Info_By_UseID($EventPICID);
				$EventPICName[] = Get_Lang_Selection($EventPICNameAry[0]['ChineseName'],$EventPICNameAry[0]['EnglishName']);
			}
			$EventPICList = implode(',',$EventPICName);
			
			
			
			$ActTitle = $eEnrollment['crash_with']." ".$libenroll->GET_EVENT_TITLE($crash_EnrolEventID_ary[$i][$j]);
			$TimeCrashTable .= $ActTitle." (".$Lang['eEnrolment']['ActivityPIC'].":".$EventPICList.")"."<br />\n";
			
			$crashedEventIDAry[$i][] = $crash_EnrolEventID_ary[$i][$j];
		
			
			//get time crash activities date and time
			$DateArr = $libenroll->GET_ENROL_EVENT_DATE($crash_EnrolEventID_ary[$i][$j]);
			// construct a date array
			for ($k = 0; $k < count($DateArr); $k++) {
				$DateTitle = date("Y-m-d", strtotime($DateArr[$k][2]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][2]));
				$DateTitle .= " ".$eEnrollment['to']." ";
				$DateTitle .= date("Y-m-d", strtotime($DateArr[$k][3]));
				$DateTitle .= " ";
				$DateTitle .= date("H:i", strtotime($DateArr[$k][3]));
				$TimeCrashTable .= $DateTitle."<br />\n";
				if ($k==count($DateArr)-1) $TimeCrashTable .= "<br />\n";
			}
			
		}
		
		$TimeCrashTable .= $fontColorEnd."</td>\n";
		
		if($oneEnrol == 1 && !$enrolAdmin)
		{
			$TimeCrashTable .= "<td valign=\"top\" class=\"$css\" align=\"center\"><input DISABLED type=\"checkbox\" name=\"uid[]\" value=\"".$StudentID."\"></td>\n";
		}
		else
		{
			$TimeCrashTable .= "<td valign=\"top\" class=\"$css\" align=\"center\"><input type=\"checkbox\" name=\"uid[]\" value=\"".$StudentID."\"></td>\n";
		}
		
		$TimeCrashTable .= "</tr>\n";
		$counterTimeCrashTable++;
	}
	
	
}  

// debug_Pr($crashedClubEnrolGroupIDArr);
$resultTable .= "</table>\n";
if ((count($crash_EnrolGroupID_ary) > 0) || (count($crash_EnrolEventID_ary) > 0))
{
	$TimeCrashTable .= "</table>\n";
}
$keyword = str_replace(
    array('<', '>','"','\'','/',';'),
    array('','','','','',''),
    $keyword);
//buttons at the bottom
$buttons = "";
$filter = IntegerSafe($filter);
if ($from == "ManualAdd")
{
	if ($type==0)
	{
		if ($fromImport != "1")
		{
			$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['add_others_student'], "button", "window.location='add_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type'")."&nbsp;";
			if (!empty ($imported_student)){
				$buttons .= $linterface->GET_ACTION_BTN($Lang['eEnrolment']['UpdatePerformanceAndPerformance'], "button", "goUpdatePerformance();")."&nbsp;";
			}
		}
		if ($importAll)
		{
			$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_club_management'], "button", "window.location = 'group.php?AcademicYearID=$AcademicYearID'")."&nbsp;";
		}
		else
		{
			$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_member_list'], "button", "window.location = 'member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
		}
	}
	elseif ($type==1)
	{
		
		if($libenroll->enableActivityFillInEnrolReasonRight() && $fromImport != "1"){
			
			$TargetFile = 'event_stu_enrol_approved_reason_update.php';
			$buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript:submitEnrolReason(document.form1, '".$TargetFile."')")."&nbsp;";
					
		}	
		else{
			if ($fromImport != "1")
			{
				$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['add_others_student'], "button", "window.location='add_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type'")."&nbsp;";
			}
			$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_applicant_list'], "button", "window.location = 'event_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
		}
	}
	elseif ($type==2)
	{
		if ($fromImport != "1")
		{
			$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['add_others_student'], "button", "window.location='add_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type&filter=$filter'")."&nbsp;";
		}
		$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_applicant_list'], "button", "window.location = 'group_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
	}
	elseif ($type==3)
	{
		if($libenroll->enableActivityFillInEnrolReasonRight() && $fromImport != "1"){
			
			$TargetFile = 'event_stu_enrol_approved_reason_update.php';
			$buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript:submitEnrolReason(document.form1, '".$TargetFile."')")."&nbsp;";
		
		}else{
			if ($fromImport != "1")
			{
				$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['add_others_student'], "button", "window.location='add_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type'")."&nbsp;";
				if (!empty ($imported_student)){
					$buttons .= $linterface->GET_ACTION_BTN($Lang['eEnrolment']['UpdatePerformanceAndPerformance'], "button", "goUpdatePerformance('activity');")."&nbsp;";
				}
			}
			if ($importAll)
			{
				$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_activity_management'], "button", "window.location = 'event.php?AcademicYearID=$AcademicYearID'")."&nbsp;";
			}
			else
			{
				$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_student_list'], "button", "window.location = 'event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
			}
		}
	}	

}
elseif ($from == "ClubEnrolList")
{
	
	$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_applicant_list'], "button", "window.location = 'group_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";

}
elseif ($from == "EventEnrolList")
{
	# Submit Reason Btn
	if($libenroll->enableActivityFillInEnrolReasonRight()){
			
		$TargetFile = 'event_stu_enrol_approved_reason_update.php';
		$buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript:submitEnrolReason(document.form1, '".$TargetFile."')")."&nbsp;";
	
	}
	else{
		$buttons .= $linterface->GET_ACTION_BTN($eEnrollment['back_to_applicant_list'], "button", "window.location = 'event_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&field=$field&order=$order&filter=$filter&keyword=$keyword'")."&nbsp;";
	}
}



?>

<script language="javascript">
function submitForm(obj,page)
{
	if(countChecked(obj,'uid[]')==0)
    	alert(globalAlertMsg2);
    else{
            if(confirm(globalAlertMsg4)){
            obj.action=page;
            obj.method="POST";
            obj.submit();
            return;
            }
    }

}

<?php if($libenroll->enableActivityFillInEnrolReasonRight()) {?>
	function submitEnrolReason(obj,page)
	{	   
	   	obj.action=page;
	    obj.method="POST";
	    obj.submit();
	 return;    
	}
<? } ?>

function goUpdatePerformance(parType) {
	parType = parType || 0;		// '0' means club
	
	$('input#type').val(parType);	
	$('form#form1').attr('action', 'member_comment_perform.php').submit();
}

</script>

<br />
<form name="form1" id="form1" method="post" action="add_member_result.php">
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
	<td align="center">
    	<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                	<td colspan="2"><?=$resultTable?></td>
				</tr>
				
				<tr>
                	<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
                	<td colspan="2"><?=$TimeCrashTable?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
    </td>
</tr>
<tr>
	<td>        
    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        	<tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center" colspan="2">
				<?= $buttons ?>
				</td>
			</tr>
	    </table>                                
	</td>
</tr>
</table>
<br />

		<input type="hidden" name="from" value="addMemberResult">
		<input type="hidden" name="type" id="type" value="<?=$type?>">
		<input type="hidden" name="GroupID" value="<?=$GroupID?>">
		<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="enrolStatus" value="<?=$enrolStatus?>">
		<input type="hidden" name="RoleID" value="<?=$RoleID?>">
		<input type="hidden" name="RoleTitle" value="<?=$_POST['RoleTitle']?>">
		<input type="hidden" name="crashedStudentArr" value="<?=rawurlencode(serialize($crashedStudentArr));?>">
		
		<input type="hidden" name="fromImport" value="<?=$fromImport?>">
		<input type="hidden" name="crashedRoleIDArr" value="<?=rawurlencode(serialize($crashedRoleIDArr));?>">
		<input type="hidden" name="crashedRoleTitleArr" value="<?=rawurlencode(serialize($crashedRoleTitleArr));?>">
		<input type="hidden" name="crashedImportTitleArr" value="<?=rawurlencode(serialize($crashedImportTitleArr));?>">
		<input type="hidden" name="crashedImportIDArr" value="<?=rawurlencode(serialize($crashedImportIDArr));?>">
		<input type="hidden" name="crashedClubEnrolGroupIDArr" value="<?=rawurlencode(serialize($crashedClubEnrolGroupIDArr));?>">
		<input type="hidden" name="crashedEventIDAry" value="<?=rawurlencode(serialize($crashedEventIDAry));?>">

		<input type="hidden" name="importAll" value="<?=$importAll?>">
		<input type="hidden" name="importType" value="<?=$importType?>">
		<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
		<input type="hidden" name="filter" value="<?=$filter?>">
		
		<input type="hidden" name="data" value="<?=rawurlencode(serialize($data))?>">
		<input type="hidden" name="ImportedStudent" value="<?=rawurlencode(serialize($imported_student))?>">
		<input type="hidden" name="activeStatus" id="activeStatus" value="<?=$_POST['activeStatus']?>">
</form>

<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>
