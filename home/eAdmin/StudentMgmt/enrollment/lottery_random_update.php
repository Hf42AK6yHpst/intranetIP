<?php
## Using By: 

############# Change Log [Start] #################
#
#   Date:   2020-04-29 (Tommy)
#           - update record status if it is the target class level #R171077
#
#	Date:	2017-09-14	(OMas) [N123444] - migrate from EJ
#			- added $sys_custom['eEnrolment']['RejectEnrollmentRecordIfReachedEnrollMax']
#
#	Date:	2017-08-29	(Anna)
#			- Added $applySpecificClubLottery 
#
#	Date:	2017-08-18	(Anna)
#			- Added EnrolGroupIDAry to lottery for specific group
#
#	Date:	2015-02-10	(Omas)
#			- fix: check different category club time clash 
#
#	Date:	2014-12-23	[Omas]
#			- Fixed later term(i.e. term >1) can not lottery correctly according to school quota
#			- Fixed when using Year-base, term 1/2 club followed term-based setting
#
#	Date:	2014-10-14 [Pun] 
#			- Add round report for SIS customization
#
#	Date:	2012-06-27 [Ivan] [2012-0626-1104-33073 Point 8]
#			- added category settings checking logic
#
#	Date:	2012-02-07 [Henry Chow]
#			- revise the lottery logic
#
#	2010-02-01: Max (201001271658)
#	- adjust the send email condition
#
############## Change Log [End] ##################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);

$lclass = new libclass();
$lfs = new libfilesystem();
$libenroll = new libclubsenrol();
$libfcm = new form_class_manage();
$libenroll->hasAccessRight($_SESSION['UserID']);

$MODULE_OBJ['title'] = $eEnrollment['drawing'];
$linterface = new interface_html("popup.html");

################################################################

function returnLogMessage($enrolGroupId, $studentId, $status, $remarks='') {
	return $enrolGroupId."\t".$studentId."\t".$status."\t".$remarks;
}

$libenroll->Start_Trans();
// $EnrolGroupIDList = $_GET['EnrolGroupID'];
$AcademicYearID = $_GET['AcademicYearID'];

$EnrolGroupIDList= $_GET['EnrolGroupID'];
if ($EnrolGroupIDList== null) {
	$applySpecificClubLottery = false;
	$EnrolGroupIDAry = '';
}
else {
	$applySpecificClubLottery = true;
	$EnrolGroupIDAry = explode(',', $EnrolGroupIDList);
}

//$EnrolGroupIDAry = explode(',',$EnrolGroupIDList);

if ($AcademicYearID == '') {
	$AcademicYearID = Get_Current_Academic_Year_ID();
}


### Retrieve drawing-related settings
if ($libenroll->tiebreak == 1) {
	$isFCFS = true;
	$isRandom = false;
}
else {
	$isFCFS = false;
	$isRandom = true;
}
$isCaterPriority = ($libenroll->caterPriority)? true : false;
$isUseCategorySettings = ($libenroll->UseCategorySetting)? true : false;
$checkStudentMaxwant = ($libenroll->disableCheckingNoOfClubStuWantToJoin)? false : true;
$enrolOneClubOnlyIfTimeClash = ($libenroll->oneEnrol)? true : false;


# Grab min / max of quota for each level
$minmax = $libenroll->getFullMinMax();


if($isUseCategorySettings) {
	$CategoryIDArr = $libenroll->GET_CATEGORY_LIST();
	$CategoryIDArr = Get_Array_By_Key($CategoryIDArr, "CategoryID");
}
else {
	$CategoryIDArr = array(0); // means do not use cateory setting
}
// $CategorySetting['CategoryID']['TermNumber']['RecordType']['SettingName'] = data
$CategorySetting = $libenroll->Get_Category_Enrol_Setting($CategoryIDArr);

//$sql = "CREATE TEMPORARY TABLE TEMP_ENROL_REQUIRE(
//         StudentID int(11),
//         Min int(11)
//         )";
//$libenroll->db_db_query($sql);


### Get Club Info
// if(empty($EnrolGroupIDAry)){
	$clubInfoAry = $libenroll->Get_All_Club_Info('', $AcademicYearID);
// }else{
// 	$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDAry, $AcademicYearID);
// }
//$clubInfoAry = $libenroll->Get_All_Club_Info('', $AcademicYearID);
$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, 'EnrolGroupID');
unset($clubInfoAry);


$pendingRecordAry = $libenroll->Get_Pending_Student_With_Choice($AcademicYearID, $returnAsso=false,$EnrolGroupIDAry);

### get pending student info
$numOfPendingRecord = count($pendingRecordAry);


### only run the lottery process ONLY IF there is pending student(s)
$SuccessAry = array();
$logMessageAry = array();
$rejectStudentArr = array();
if($numOfPendingRecord > 0) {	// no. of pending students in groups
	$overallMaxOfCategory = array();	
	$enrollMaxOfCategory = array();
	
	$termAry = $libfcm->Get_Academic_Year_Term_List(Get_Current_Academic_Year_ID());
	$numOfTerm = count($termAry);
	$termAry[$numOfTerm]['YearTermID'] = 0;
	$numOfTerm = count($termAry);
//	debug_pr($termAry);
//	debug_pr($numOfTerm);
	for ($i=0; $i<$numOfTerm; $i++) {
		$_yearTermId = $termAry[$i]['YearTermID'];
		
		if ($_yearTermId == 0) {
			$_termNumber = 0;
		}
		else {
			//$_termNumber = i + 1;
			$_termNumber = $i + 1; //2014-12-23
		}
		//debug_pr('when $i = '.$i.',$_termNumber = '.$_termNumber);
		foreach($CategoryIDArr as $thisCategoryID) {
			$enrollMaxOfCategory[$thisCategoryID][$_termNumber] = $CategorySetting[$thisCategoryID][$_termNumber]['Club']['EnrollMax'];
			
			for ($j=0; $j<count($minmax); $j++)		// quota setting (min & max) of each "Form"
			{
			     list($id,$min,$max,$enrollMax) = $minmax[$j];
			     if ($max == "") continue;
			     
			     if ($isUseCategorySettings) {
			     	// do nth
			     }
			     else {
			     	if ($enrollMax > $enrollMaxOfCategory[$thisCategoryID][$_termNumber]) {
			     		$enrollMaxOfCategory[$thisCategoryID][$_termNumber] = $enrollMax;
			     	}
			     }
			}
		}
	}
	
//	foreach($CategoryIDArr as $thisCategoryID) {
//		// clear for each category from temp table
////		$sql = "DELETE FROM TEMP_ENROL_REQUIRE";
////		$libenroll->db_db_query($sql);
//		
//		$overallMaxOfCategory[$thisCategoryID] = $CategorySetting[$thisCategoryID]['Club']['DefaultMax'];
//		$enrollMaxOfCategory[$thisCategoryID] = $CategorySetting[$thisCategoryID]['Club']['EnrollMax'];
//		
//		for ($i=0; $i<count($minmax); $i++)		// quota setting (min & max) of each "Form"
//		{
//		     list($id,$min,$max,$enrollMax) = $minmax[$i];
//		     if ($max == "") continue;
//		     if ($max > $overallMaxOfCategory[$thisCategoryID]) $overallMaxOfCategory[$thisCategoryID] = $max;
//		     
//		     if ($isUseCategorySettings) {
//		     	// do nth
//		     }
//		     else {
//		     	if ($enrollMax > $enrollMaxOfCategory[$thisCategoryID]) {
//		     		$enrollMaxOfCategory[$thisCategoryID] = $enrollMax;
//		     	}
//		     }
//		     
////		     # Grab classname for this form
////		     $classes = $lclass->returnClassListByLevel($id);
////		     if(count($classes))
////		     {
////			     $delimiter = "";
////			     $list = "";
////			     for ($j=0; $j<count($classes); $j++)
////			     {
////			          list($id,$name) = $classes[$j];
////			          $list .= "$delimiter '$name'";
////			          $delimiter = ",";
////			     }
////			     $sql = "INSERT IGNORE INTO TEMP_ENROL_REQUIRE (StudentID,Min)
////			             SELECT UserID, $min FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($list) AND ClassName != ''";
////			     $libenroll->db_db_query($sql);
////		     }
//		}
//		
////		if ($overallMaxOfCategory[$thisCategoryID] == 0) {
////			if($thisCategoryID==0)
////		    	$sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT"; 
////		    else
////		    	$sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT_CATEGORY_ENROL WHERE CategoryID = '$thisCategoryID' ";
////		    $result = $libenroll->returnVector($sql);
////		    if ($result[0]!=0) {
////		        $overallMaxOfCategory[$thisCategoryID]= $result[0];
////		    }
////		}
//	}
	
//	if(empty($EnrolGroupIDAry)){
		$randomizedAllRecordEnrolGroupIdAry = Get_Array_By_Key($pendingRecordAry, 'EnrolGroupID');
		
// 	}else{
// 		$randomizedAllRecordEnrolGroupIdAry = $EnrolGroupIDAry;
// 	}

	### Random the processing order of Clubs
// 	$randomizedAllRecordEnrolGroupIdAry = Get_Array_By_Key($pendingRecordAry, 'EnrolGroupID');
	if (!$isCaterPriority) {
		shuffle($randomizedAllRecordEnrolGroupIdAry);
	}
	$numOfProcessRecord = count($randomizedAllRecordEnrolGroupIdAry);
	
	
	### Construct the student processing order of each clubs
	$pendingRecordWithPriorityAssoAry = array();
	for ($i=0; $i<$numOfPendingRecord; $i++) {
		$_enrolGroupId = $pendingRecordAry[$i]['EnrolGroupID'];
		$_studentId = $pendingRecordAry[$i]['StudentID'];
		$_choice = ($isCaterPriority)? $pendingRecordAry[$i]['Choice'] : 0;
		
		$pendingRecordWithPriorityAssoAry[$_enrolGroupId][$_choice][] = $_studentId;
	}
	
	if ($isCaterPriority) {
		// $pendingRecordAry is ordered by DateInput only, so, priority 2 maybe in front of priority 1 as the array index
		// not cater priority => all priority is zero => not re-ordering to have better performance
		foreach ((array)$pendingRecordWithPriorityAssoAry as $_enrolGroupId => $_clubChoiceInfoAry) {
			ksort($pendingRecordWithPriorityAssoAry[$_enrolGroupId], SORT_NUMERIC);
		}
	}
	
	if ($isRandom) {
		foreach ((array)$pendingRecordWithPriorityAssoAry as $_enrolGroupId => $_clubChoiceInfoAry) {
			foreach ((array)$_clubChoiceInfoAry as $__choice => $__clubChoiceStudentIdAry) {
				if (is_array($pendingRecordWithPriorityAssoAry[$_enrolGroupId][$__choice])) {
					shuffle($pendingRecordWithPriorityAssoAry[$_enrolGroupId][$__choice]);
				}
			}
		}
	}
	
	
	### Remove the priority index from the array and merge the student array of different priorities for processing
	$pendingRecordAssoAry = array();
	foreach ((array)$pendingRecordWithPriorityAssoAry as $_enrolGroupId => $_clubChoiceInfoAry) {
		foreach ((array)$_clubChoiceInfoAry as $__choice => $__clubChoiceStudentIdAry) {
			$pendingRecordAssoAry[$_enrolGroupId] = array_merge((array)$pendingRecordAssoAry[$_enrolGroupId], (array)$__clubChoiceStudentIdAry);
		}
	}
	
	
	### Initialize the counting array
	$processedCountAssoAry = array();	// $processedCountAry[$enrolGroupId] = $count;
	$clubQuotaLeftAssoAry = array();
	foreach ((array)$pendingRecordAssoAry as $_enrolGroupId => $_studentIdAry) {
		$processedCountAssoAry[$_enrolGroupId] = 0;
		$clubQuotaLeftAssoAry[$_enrolGroupId] = $libenroll->GET_GROUP_QUOTA_LEFT($_enrolGroupId);
	}

	### Process the drawing data now
	for ($i=0; $i<$numOfProcessRecord; $i++) {
		$_enrolGroupId = $randomizedAllRecordEnrolGroupIdAry[$i];
		$_clubQuotaLeft = $clubQuotaLeftAssoAry[$_enrolGroupId];
		
		$_categoryId = $clubInfoAssoAry[$_enrolGroupId]['GroupCategory'];
		$_categoryIdNum = ($isUseCategorySettings) ? $_categoryId : 0;
		$_categoryIdText = ($isUseCategorySettings) ? $_categoryId : '';
		
		$_yearTermId = $clubInfoAssoAry[$_enrolGroupId]['YearTermID'];
		$_termNumber = $libenroll->Get_Term_Number($_yearTermId);
		
		
		$_processedCount = $processedCountAssoAry[$_enrolGroupId]++;
		$_studentId = $pendingRecordAssoAry[$_enrolGroupId][$_processedCount];
		
		if ($_clubQuotaLeft <= 0) {
			// Skip if the club has no quota left
		    if($sys_custom['eEnrollment']['setTargetForm']){
		        if(!in_array($_studentId, $rejectStudentArr))
		          $rejectStudentArr[] = $_studentId;
		    }
			$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'rejected', 'club has no quota left.');
			continue;
		}
		
		// Check school quota settings
		//$_studentEnrolledClubAry = $libenroll->Get_Student_Club_Info($_studentId, '', $AcademicYearID, '', $_categoryIdText);
		//$_libenroll_temp = new libclubsenrol($_categoryId);
		$_libenroll_temp = new libclubsenrol($AcademicYearID, $_categoryId); //2014-12-23
		
		if ($_libenroll_temp->quotaSettingsType == 'YearBase') {
			$_yearTermId_temp = '';
			$_termNumber_temp = 0;
		}
		else if ($_libenroll_temp->quotaSettingsType == 'TermBase') {
			$_yearTermId_temp = $_yearTermId;
			$_termNumber_temp = $_termNumber;
		}
		$_studentEnrolledClubAry = $libenroll->Get_Student_Club_Info($_studentId, '', $AcademicYearID, $_yearTermId_temp, $_categoryIdText);
		$_numOfEnrolledClub = count($_studentEnrolledClubAry);
		
//			debug_pr('------------------------------------------------Record Start------------------------------------------------');
//			debug_pr('$enrollMaxOfCategory');
//			debug_pr($enrollMaxOfCategory);
//			debug_pr('$_termNumber = '.$_termNumber);
//			debug_pr('$_categoryIdNum = '.$_categoryIdNum);
//			debug_pr('$enrollMaxOfCategory[$_categoryIdNum][$_termNumber] = '.$enrollMaxOfCategory[$_categoryIdNum][$_termNumber]);
//			debug_pr('$_numOfEnrolledClub = '. $_numOfEnrolledClub);
//			debug_pr($_studentEnrolledClubAry);
//			debug_pr('------------------------------------------------Record End------------------------------------------------');
		
		if ($enrollMaxOfCategory[$_categoryIdNum][$_termNumber_temp] != 0 && $_numOfEnrolledClub >= $enrollMaxOfCategory[$_categoryIdNum][$_termNumber_temp]) {
			// school does not allow student to join so many club
		    if($sys_custom['eEnrollment']['setTargetForm']){
		        if(!in_array($_studentId, $rejectStudentArr))
		            $rejectStudentArr[] = $_studentId;
		    }
			$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'rejected', 'school does not allow student to join so many clubs. $_numOfEnrolledClub = '.$_numOfEnrolledClub.'; $enrollMaxOfCategory[$_categoryIdNum][$_termNumber] = '.$enrollMaxOfCategory[$_categoryIdNum][$_termNumber]);
			continue;
		}
		
		// Check student max want to join number of club 
		$_studentMaxwantQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($_studentId, $_categoryIdNum);
		if ($checkStudentMaxwant && $_studentMaxwantQuotaLeft <= 0) {
			// student does not want to join so many clubs
		    if($sys_custom['eEnrollment']['setTargetForm']){
		        if(!in_array($_studentId, $rejectStudentArr))
		            $rejectStudentArr[] = $_studentId;
		    }
			$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'rejected', 'student does not want to join so many clubs.');
			continue;
		}
		
		// Check time clash for enrolled clubs
		if ($enrolOneClubOnlyIfTimeClash) {
			$_clashClub = false;
			$_clashClubEnrolGroupIdAry = array();
			
			//check from all enrolled club time clash
	    	$_studentAllEnrolledClubAry = $libenroll->Get_Student_Club_Info($_studentId);
	    	$_numOfAllEnrolledClub = count($_studentAllEnrolledClubAry);
			
	    	for ($j=0; $j<$_numOfAllEnrolledClub; $j++) {
	    		$__enrolGroupId = $_studentAllEnrolledClubAry[$j]['EnrolGroupID'];
	    		
	    		if ($_enrolGroupId == $__enrolGroupId) {
	    			// skip checking for the same club
	    			continue;
	    		}
	    		
	        	if ($libenroll->IS_CLUB_CRASH($_enrolGroupId, $__enrolGroupId)) {
	            	$_clashClub = true;
	            	$_clashClubEnrolGroupIdAry[] = $__enrolGroupId;
	            	continue;
	        	}
	    	}
	    	if ($_clashClub) {
	    		// time-clash with other enrolled club
	    	    if($sys_custom['eEnrollment']['setTargetForm']){
	    	        if(!in_array($_studentId, $rejectStudentArr))
	    	            $rejectStudentArr[] = $_studentId;
	    	    }
	    		$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'rejected', 'time clash with other enrolled clubs (EnrolGroupID = '.implode(', ', (array)$_clashClubEnrolGroupIdAry).').');
	    		continue;
	    	}
		}
		
    	
    	// Check time clash for enrolled activities
    	$_studentEnrolledActivityAry = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($_studentId, $AcademicYearID);
    	$_numOfStudentEnrolledActivity = count($_studentEnrolledActivityAry);
    	$_clashActivity = false;
    	$_clashClubEnrolEventIdAry = array();
    	for ($j=0; $j<$_numOfStudentEnrolledActivity; $j++) {
    		$__enrolEventId = $_studentEnrolledActivityAry[$j]['EnrolEventID'];
    		
        	if ($libenroll->IS_EVENT_GROUP_CRASH($_enrolGroupId, $__enrolEventId)) {
				$_clashActivity = true;
				$_clashClubEnrolEventIdAry[] = $__enrolEventId;
				continue;
			}
    	}
    	if ($_clashActivity) {
    		// time-clash with other enrolled activity
    	    if($sys_custom['eEnrollment']['setTargetForm']){
    	        if(!in_array($_studentId, $rejectStudentArr))
    	            $rejectStudentArr[] = $_studentId;
    	    }
    		$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'rejected', 'time clash with other enrolled activities (EnrolEventID = '.implode(', ', (array)$_clashClubEnrolEventIdAry).').');
	    	continue;
    	}
    	
    	$SuccessAry[$_enrolGroupId][$_studentId]['addStudentToMemberList'] = $libenroll->Add_Student_To_Club_Member_List($_studentId, $_enrolGroupId);
		$clubQuotaLeftAssoAry[$_enrolGroupId]--;
		$_numOfEnrolledClub++;
		
		if($sys_custom['eEnrollment']['setTargetForm']){
		    include_once($PATH_WRT_ROOT."includes/libuser.php");
		    $libuser = new libuser();
		    $form = $libuser->Get_User_Studying_Form($_studentId, $AcademicYearID);
		    $year = explode(",",$libenroll->settingTargetForm);

// 		    if(in_array($form[0]["YearID"], $year)){
// 		      $sql = "SELECT * FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID IN (".implode(',', (array)$EnrolGroupIDAry).")";
		        
// 		      $rejectStudentArr[] = $_studentId;
// 		    }
		}
		
		if ($sys_custom['eEnrolment']['RejectEnrollmentRecordIfReachedEnrollMax'] && $enrollMaxOfCategory[$_categoryIdNum][$_termNumber] != 0 && $_numOfEnrolledClub >= $enrollMaxOfCategory[$_categoryIdNum][$_termNumber]) {
			### student reached enroll limit already => change all pending records to reject
				
			// get pending records of the student with the same category and term
			$_studentEnrolAry = $libenroll->Get_Club_Student_Enrollment_Info('', $AcademicYearID, $ShowUserRegOnly=0, $_termNumber, $ClubKeyword='', $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=false, $_categoryIdNum, $_studentId, $parRecordStatus=0);
				
			// set the remaining pending records to "Rejected"
			$_remainingPendingEnrolGroupIdAry = Get_Array_By_Key($_studentEnrolAry, 'EnrolGroupID');
			
			$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 1, DateModified = now(), StatusChangedBy = '".$_SESSION['UserID']."' WHERE RecordStatus = 0 And StudentID = '".$_studentId."' And EnrolGroupID IN ('".implode("' ,'", (array)$_remainingPendingEnrolGroupIdAry)."')";
			$libenroll->db_db_query($sql);
		}
		
		if ($SuccessAry[$_enrolGroupId][$_studentId]['addStudentToMemberList']) {
			$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'approved');
		}
		else {
			$logMessageAry[] = returnLogMessage($_enrolGroupId, $_studentId, 'failed', 'cannot add student to club member list in database.');
		}
	}
}
if ($applySpecificClubLottery){
	$EnrolGroupIDconds = "AND EnrolGroupID IN (".implode(',', (array)$EnrolGroupIDAry).")";
}else{
	$EnrolGroupIDconds = "";
}

if($sys_custom['eEnrollment']['setTargetForm']){
    $rejectStdconds = " AND StudentID IN ('".implode("','", $rejectStudentArr)."') ";
}else{
    $rejectStdconds = "";
}

## Set the unsuccess application to "Rejected"
$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 1, DateModified = now(), StatusChangedBy = '".$_SESSION['UserID']."' WHERE RecordStatus = 0 $EnrolGroupIDconds $rejectStdconds";
$SuccessAry['setWaitingRecordToRejected'] = $libenroll->db_db_query($sql);


### Log the Processs in a file
$logFolder = $intranet_root."/file/enroll_info/drawing_log";
if(!file_exists($logFolder)) {
	$SuccessAry['log']['createLogFolder'] = $lfs->folder_new($logFolder);
}
$logFile = $logFolder.'/'.date('Ymd_His').'_'.$_SESSION['UserID'].'.txt';

$logContent = '';
$logContent .= '$isFCFS = '.$isFCFS."\r\n";
$logContent .= '$isRandom = '.$isRandom."\r\n";
$logContent .= '$isCaterPriority = '.$isCaterPriority."\r\n";
$logContent .= '$isUseCategorySettings = '.$isUseCategorySettings."\r\n";
$logContent .= '$checkStudentMaxwant = '.$checkStudentMaxwant."\r\n";
$logContent .= '$enrolOneClubOnlyIfTimeClash = '.$enrolOneClubOnlyIfTimeClash."\r\n";
$logContent .= "\r\n";
$logContent .= "\r\n";
$logContent .= 'EnrolGroupID'."\t".'StudentID'."\t".'Status'."\t".'Remarks'."\r\n";
$logContent .= implode("\r\n", (array)$logMessageAry);
$SuccessAry['log']['writeLog'] = $lfs->file_write($logContent, $logFile);


if (in_multi_array(false, (array)$SuccessAry)) {
	$libenroll->RollBack_Trans();
}
else {
	$libenroll->Commit_Trans();
}




// send email
$laccess = new libaccess();
if ($laccess->retrieveAccessCampusmailForType(2) && $libenroll->onceEmail == 1)     # if can access campusmail
{	
	//get all groups
	$groupArr = $libenroll->getGroupInfoList($isAll=1, $Semester='', $EnrolGroupIDArr='', $getRegOnly=1);
	$numOfClub = count($groupArr);
	//echo "Group Array : <br>";
	//debug_pr($groupArr); 
	// loop through all groups
	for ($j=0; $j<$numOfClub; $j++)
	{
		$thisEnrolGroupID = $groupArr[$j]['EnrolGroupID'];
		$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
						
		//check if the club is using the enrolment system
		/* checked in function getGroupInfoList() already
		$sql = "SELECT Quota FROM INTRANET_ENROL_GROUPINFO WHERE GroupID = '$thisGroupID'";
		$result = $libenroll->returnArray($sql,1);
		if ($result[0][0] == "")
		{
			//ignore clubs that are nor using enrolment system
			continue;
		}
		*/
				
		//send email to all students who have applied for this club
		$sql = "SELECT StudentID, RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$thisEnrolGroupID'";
		$students = $libenroll->returnArray($sql,1);
		$numOfClubMember = count($students);	
		//echo "Student Array : <br>";
		//debug_pr($students); 
		for ($i=0; $i<$numOfClubMember; $i++)
		{
			$thisStudentID = $students[$i]['StudentID'];
			$thisRecordStatus = $students[$i]['RecordStatus'];
			
			# define record type
			$RecordType = "club";
			
			# get student list
			$userArray = array();			
			$userArray[] = $students[$i][0];
			
			# get success result
			$successResult = false;
			
			// commented by Ivan 20100226 - Select the RecordStatus in previous SQL so that less SQL is run
			//$sql = "SELECT RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE GroupID = '$GroupID' AND StudentID = '".$students[$i][0]."'";
			//$choices = $libenroll->returnArray($sql,1);
			
			if ($thisRecordStatus == 2) {
				$successResult = true;
			} else if ($thisRecordStatus == 1){
				$successResult = false;
			} else {
				// do nothing
			}
			
			$libenroll->Send_Enrolment_Result_Email($userArray, $thisGroupID, $RecordType, $successResult);
		}
		//debug_r($recipient);
		
	}
}


intranet_closedb();


################################################################

$linterface->LAYOUT_START();

?>

<div style="height: 400px;">
<br/><br/><br/><br/>
<?=$i_ClubsEnrollment_LotteryFinished?><br/><br/>
<?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
</div>
<script type="text/javascript">
	window.opener.location.reload();
</script>

<?
$linterface->LAYOUT_STOP();  
?>