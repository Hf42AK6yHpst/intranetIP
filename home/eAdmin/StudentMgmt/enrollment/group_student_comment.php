<?php
#using : 

/********************************************************
 *  Modification Log
 *  2015-05-11	Omas
 * 		- fixed : percentage round to 2 d.p and exempted will not count in $total and $attend  
 * 
 *	2010-12-14	YatWoon
 *		- club name eng/chinese
 *
 * 	2010-08-16  Thomas
 * 		- Check the data is freezed or not
 *		  Show alert message when adding / updating the data
 *
 ********************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();


$LibUser = new libuser($UserID);

$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
$EnrolGroupID = IntegerSafe($_GET['EnrolGroupID']);
$StudentID = IntegerSafe($_GET['StudentID']);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
	$libenroll = new libclubsenrol($AcademicYearID);	
	$libenroll_ui = new libclubsenrol_ui();	
	
	if ($libenroll->enableAttendanceScheduleDetail()) {
		include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui_cust.php");
		$libenroll_ui_cust = new libclubsenrol_ui_cust();
	}
	
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
	//	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)) && (!$libenroll->IS_CLUB_HELPER($EnrolGroupID)))
	{
		//header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		$canEdit = false;
	}
	else
	{
		$canEdit = true;
	}
	
	if ($libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	//$TAGS_OBJ[] = array($eEnrollmentMenu['attendance_mgt'], "", 1);
	# tags 
    $tab_type = "club";
    $current_tab = 1;
    # navigation
    $PAGE_NAVIGATION[] = array($eEnrollmentMenu['attendance_mgt'], "");
    include_once("management_tabs.php");

    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();

        $linterface->LAYOUT_START();
        
        $GroupEnrollArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
        $GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
        $GroupArr = $libenroll->getGroupInfo($GroupID);
        $title_display = $intranet_session_language=="en" ? $GroupArr[0]['Title'] : $GroupArr[0]['TitleChinese'];
        $GroupDateArr = $libenroll->GET_ENROL_GROUP_DATE($GroupEnrollArr[0]);
        
        if ($libenroll->enableAttendanceScheduleDetail()) {
        	if (count($GroupDateArr) > 0) {
        		
        		foreach ($GroupDateArr as $i => $GroupDateRec) {
        			$tmp = $libenroll_ui_cust->Get_Attendance_Time_Log_Records($EnrolGroupID, $GroupDateRec["GroupDateID"], array($StudentID=>$StudentID), "Club");
        			$GroupDateArr[$i]["time_attend"] = $tmp[$GroupDateRec["GroupDateID"]][$StudentID]["time_attend"];
        			$GroupDateArr[$i]["time_leave"] = $tmp[$GroupDateRec["GroupDateID"]][$StudentID]["time_leave"];
        			if (empty($GroupDateArr[$i]["time_attend"])) $GroupDateArr[$i]["time_attend"] = "-";
        			if (empty($GroupDateArr[$i]["time_leave"])) $GroupDateArr[$i]["time_leave"] = "-";
        		}
        		unset($tmp);
        	}
        }
        if ($libenroll->enableUserJoinDateRange()) {
        	$enrolAvailDateArr = $libenroll->Get_Enrol_AvailDate($EnrolGroupID, array($StudentID), "Club");
        	if (count($GroupDateArr) > 0) {
        		if (empty($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateStart"])) $AvailStart = "";
        		else $AvailStart = strtotime(date("Y-m-d", strtotime($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateStart"])));
        
        		if (empty($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateEnd"])) $AvailEnd = "";
        		else $AvailEnd = strtotime(date("Y-m-d", strtotime($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateEnd"])));
        
        		foreach ($GroupDateArr as $kk => $vv) {
        			$checkStart = strtotime(date("Y-m-d", strtotime($vv["ActivityDateStart"])));
        			$notAvailable = true;
        			if (
        					(empty($AvailStart) || $checkStart >= $AvailStart)
        					&& (empty($AvailEnd) || $checkStart <= $AvailEnd)
        					) {
        						$notAvailable = false;
        					}
        					if ($notAvailable) {
        						unset($GroupDateArr[$kk]);
        					}
        		}
        		$GroupDateArr = array_values($GroupDateArr);
        	}
        }
        
        $DataArr['StudentID'] = $StudentID;
        $DataArr['EnrolGroupID'] = $EnrolGroupID;
        $CommentStudent = $libenroll->GET_GROUP_STUDENT_COMMENT($DataArr);
        $Performance = $libenroll->GET_GROUP_STUDENT_PERFORMANCE($DataArr);
        //debug_r($GroupEnrollArr);
        $total = 0;
        $attend = 0;
        
		$lword = new libwordtemplates();
		$performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");
        
        if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
        
        $words = $lword->getWordList(8);                
        $select_word .= $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1);
		$select_word .= $linterface->GET_PRESET_LIST("jArrayWords", 0, "performance");
		
		$libuser_student = new libuser($StudentID);
		$thisWebSAMSRegNo = $libuser_student->WebSamsRegNo;
		# get offical photos
		list($thisPhotoFilePath, $thisPhotoURL) = $libuser_student->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
		$fs = new libfilesystem();
		if (file_exists($thisPhotoFilePath))
		{
			list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
			$thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
			$thisPhotoSpan = "<br /><span class=\"tabletext\">";
				$thisPhotoSpan .= $thisImgTag;
			$thisPhotoSpan .= "</span>\n";
		}
		else
		{
			$thisPhotoSpan = "";
		}
		
		$AttendanceIconRemarks = '';
//		$AttendanceIconRemarks .= '<div>'."\n";
//			$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
//			$AttendanceIconRemarks .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//			$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
//			$AttendanceIconRemarks .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//			$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
//			if ($libenroll->enableAttendanceLateStatusRight()) {
//				$AttendanceIconRemarks .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//				$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_LATE).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Late'].'</span>'."\n";
//			}
//		$AttendanceIconRemarks .= '</div>'."\n";
		$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon_Remarks();
        
?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	//if(!check_text(obj.CommentStudent, "<?php echo $i_alert_pleasefillin.$eEnrollment['teachers_comment']; ?>.")) return false;
	obj.submit();
}
</script>
<form name="form1" action="add_student_comment.php" method="POST">
<br/>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
</table>

<table align="center">
<tr><td>
<table><tr><td class="tabletext"><?= GET_ACADEMIC_YEAR(date("Y-m-d"))?> <?= $eEnrollment['year']?></td></tr></table>
</td><td>

<table><tr><td class="tabletext">
<?= $eEnrollment['activity']?> : 
</td><td class="tabletext">
<?=$title_display?>
</td></tr></table>
</td></tr></table>

<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td class="tabletext" align="center"><?= $LibUser->getNameWithClassNumber($StudentID) . $thisPhotoSpan ?></td>
</tr> 
<tr height="10"><td></td></tr>
<tr><td>
<table id="html_body_frame" width="60%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tablegreentop">
		<td class="tableTitle" width="35%" nowrap align="center"><span class="tabletopnolink"><?= $eEnrollment['add_activity']['act_date']?></span></td>
<?php if ($libenroll->enableAttendanceScheduleDetail()) { ?>
		<td class="tableTitle" width="25%" nowrap align="center"><span class="tabletopnolink"><?php echo $Lang['eEnrolment']['TimeofArrival']; ?></span></td>
		<td class="tableTitle" width="25%" nowrap align="center"><span class="tabletopnolink"><?php echo $Lang['eEnrolment']['TimeofDeparture']; ?></span></td>
<?php } ?>
		<td><span class="tabletopnolink"> </span></td>
	</tr>	
	<? for ($i = 0; $i < sizeof($GroupDateArr); $i++) { ?>
	<tr class="tablegreenrow<?= (($i % 2) + 1)?>">
		<td class="tabletext" align="center"><?= date("Y-m-d H:i", strtotime($GroupDateArr[$i][2]))?>-<?= date("H:i", strtotime($GroupDateArr[$i][3]))?></td>
<?php if ($libenroll->enableAttendanceScheduleDetail()) { ?>
		<td class="tabletext" align="center"><?php echo $GroupDateArr[$i]["time_attend"]?></td>
		<td class="tabletext" align="center"><?php echo $GroupDateArr[$i]["time_leave"]?></td>
<?php } ?>
		<td align="center">		
		<?
			
			if (date("Y-m-d", strtotime($GroupDateArr[$i][2])) > date("Y-m-d"))
			{
				$temp = "future";
			}
			else
			{
				//$total++;
				/*
				if ($status = $libenroll->Get_Group_Attendance_Status($GroupDateArr[$i][0],$StudentID,$EnrolGroupID)) {
					$temp = $status==1?"present":"exempt";
					$attend++;
				} else {
					$temp = "absent";
				}
				*/	
				
				$status = $libenroll->Get_Group_Attendance_Status($GroupDateArr[$i][0],$StudentID,$EnrolGroupID);
				//if (in_array($status, array(ENROL_ATTENDANCE_PRESENT, ENROL_ATTENDANCE_EXEMPT)))
				if (in_array($status, array(ENROL_ATTENDANCE_PRESENT)))
					$attend++;
				else if (in_array($status, array(ENROL_ATTENDANCE_LATE)) && $libenroll->enableAttendanceLateStatusRight())
					$attend++;
				else if (in_array($status, array(ENROL_ATTENDANCE_EARLY_LEAVE)) && $libenroll->enableAttendanceEarlyLeaveStatusRight())
					$attend++;
					
				if (!in_array($status, array(ENROL_ATTENDANCE_EXEMPT)))
					$total++;
				
				$thisIcon = $libenroll_ui->Get_Attendance_Icon($status);
			}
		?>
		
		<? if ($temp == "future") { ?>
			<?=$Lang['General']['EmptySymbol']?>
		<? } else { ?>
			<?=$thisIcon?>
		<? } ?>
		
		</td>
	</tr>
	<? } ?>
	<tr class="tablegreenbottom">
		<td class="tabletext" nowrap align="center"><?= $eEnrollment['attendence']?></td>
<?php if ($libenroll->enableAttendanceScheduleDetail()) { ?>
		<td class="tabletext">&nbsp;</td>
		<td class="tabletext">&nbsp;</td>
<?php } ?>
		<?($total != 0) ? $attendPercentage = round(($attend / $total * 100),2) : $attendPercentage = "0"; ?>
		<td class="tabletext" align="center"><?= $attendPercentage ?>%</td>
	</tr>
</table>
<table id="html_body_frame" width="60%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr><td align="right"><?= $AttendanceIconRemarks ?></td></tr>
</table>


</td></tr>
<tr><td>
<br/>
<table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
<tr>
	<td class="tabletext formfieldtitle" width="30%" valign="top" nowrap="nowrap">
		<?php echo $i_ActivityPerformance; ?>
	</td>
	<td class="tabletext">
		<input type='text' class='textboxnum' id='performance' name='performance' value="<?= htmlspecialchars($Performance)?>"> <?= $select_word?><?//$performancelist?>
	</td>
</tr>
<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
<?= $eEnrollment['teachers_comment']?>
</td><td>
<?= $linterface->GET_TEXTAREA("CommentStudent", $CommentStudent, "40")?>
</td></tr>
</table>

</td></tr>
<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?if($libenroll->AllowToEditPreviousYearData){?>
<?= $linterface->GET_ACTION_BTN($button_save, "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "FormSubmitCheck(this.form)")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?}?>
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='club_attendance_mgt.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>
</table>
<input type="hidden" id="EnrolGroupID" name="EnrolGroupID" value="<?= $EnrolGroupID?>" />
<input type="hidden" id="StudentID" name="StudentID" value="<?= $StudentID?>" />
</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>