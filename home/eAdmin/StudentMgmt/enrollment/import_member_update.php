<?php
// # Modifying By : 

// #######################################
// Modification Log:
//
// Date:   2020-02-25 (Tommy)
// add year term import (Case #Z178470)
//
// Date: 2019-10-14 Henry
// pass data into function Get_GroupInfo_By_GroupTitle() without intranet_htmlspecialchars
//
// Date: 2019-04-01 Anna [#158292]
// Added move import csv to temp folder as log
//
// Date 2018-09-28 Anna
// Added enrollment carshClub/clashEvent push mesaage
//
// Date 2016-12-20 Carlos
// $sys_custom['StudentAttendance']['SyncDataToPortfolio'] - sync student activity profile and portfolio.
//
// Date 2016-05-31 Henry HM
// Added merit fields for type={activityAllParticipant,activityParticipant}
//
// Date 2015-10-13 (Omas)
// modified returnFailedInfoArr() - add pram $UserLogin fix#P87168
//
// Date 2015-04-08 (Omas)
// added field userlogin to import
//
// Date: 2014-12-02 Omas
// New setting $libenroll->notCheckTimeCrash, $libenroll->Event_notCheckTimeCrash bypass time crash checking
//
// Date: 2013-01-02 Rita
// Add merit customization
//
// Date: 2012-03-30 Ivan
// - Cater "Achievement" field
//
// Date: 2012-02-15 Marcus
// - Cater status selection
//
// Date: 2011-11-22 YatWoon
// fixed: pass data into function Get_GroupInfo_By_GroupTitle() with intranet_htmlspecialchars to cater special character (e.g. &)
//
// Date: 2011-03-04 YatWoon
// add Gender checking
//
// # 2010-01-28: Max (201001271658)
// # - Append parents email to email recepient if $libenroll->Event_onceEmail == 1
// #######################################
$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");
include_once ($PATH_WRT_ROOT . "includes/libcampusmail.php");
include_once ($PATH_WRT_ROOT . "includes/libaccess.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lstudentprofile = new libstudentprofile();
$lfs = new libfilesystem();

function returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $UpdateStatus, $EventTitle = "", $Semester = '', $AdminRole = '', $Achievement = '', $Reason = '', $meritCount = '', $minorMeritCount = '', $majorMeritCount = '', $superMeritCount = '', $ultraMeritCount = '', $Term1Performance = '', $Term2Performance = '', $Term3Performance = '')
{
    global $PATH_WRT_ROOT, $sys_custom;
    include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
    $libenroll = new libclubsenrol();

    $returnArr = array();
    $returnArr["ClassName"] = $ClassName;
    $returnArr["ClassNumber"] = $ClassNumber;
    $returnArr["UserLogin"] = $UserLogin;
    $returnArr["RoleTitle"] = $RoleTitle;
    $returnArr["Performance"] = $Performance;
    $returnArr["Achievement"] = $Achievement;
    $returnArr["Comment"] = $Comment;
    $returnArr["ActiveMemberStatus"] = $ActiveMemberStatus;
    $returnArr["Status"] = $UpdateStatus;
    $returnArr["EventTitle"] = $EventTitle;
    $returnArr["Semester"] = $Semester;
    $returnArr["AdminRole"] = $AdminRole;

    if ($libenroll->enableActivityFillInEnrolReasonRight()) {
        $returnArr["Reason"] = $Reason;
    }

    if ($libenroll->enableClubRecordMeritInfoRight()) {
        $returnArr["meritCount"] = $meritCount;
        $returnArr["minorMeritCount"] = $minorMeritCount;
        $returnArr["majorMeritCount"] = $majorMeritCount;
        $returnArr["superMeritCount"] = $superMeritCount;
        $returnArr["ultraMeritCount"] = $ultraMeritCount;
    }

    if ($libenroll->enableActivityRecordMeritInfoRight()) {
        $returnArr["meritCount"] = $meritCount;
        $returnArr["minorMeritCount"] = $minorMeritCount;
        $returnArr["majorMeritCount"] = $majorMeritCount;
        $returnArr["superMeritCount"] = $superMeritCount;
        $returnArr["ultraMeritCount"] = $ultraMeritCount;
    }

    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
        $returnArr["Term1Performance"] = $Term1Performance;
        $returnArr["Term2Performance"] = $Term2Performance;
        $returnArr["Term3Performance"] = $Term3Performance;
    }

    return $returnArr;
}

function returnSuccessInfoArr($StudentID, $EventID, $RoleID, $UpdateStatus, $EventTitle = "", $Semester = '', $AdminRole = '', $Reason = '', $meritCount = '', $minorMeritCount = '', $majorMeritCount = '', $superMeritCount = '', $ultraMeritCount = '')
{
    global $PATH_WRT_ROOT;
    include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
    $libenroll = new libclubsenrol();

    $returnArr = array();
    $returnArr["StudentID"] = $StudentID;
    $returnArr["EventID"] = $EventID;
    $returnArr["RoleID"] = $RoleID;
    $returnArr["RoleTitle"] = $RoleID;
    $returnArr["Status"] = $UpdateStatus;
    $returnArr["EventTitle"] = $EventTitle;
    $returnArr["Semester"] = $Semester;
    $returnArr["AdminRole"] = $AdminRole;

    // Enrol Reason Customization
    if ($libenroll->enableActivityFillInEnrolReasonRight()) {
        $returnArr["Reason"] = $Reason;
    }

    // Merit Customization
    if ($libenroll->enableClubRecordMeritInfoRight()) {
        $returnArr["meritCount"] = $meritCount;
        $returnArr["minorMeritCount"] = $minorMeritCount;
        $returnArr["majorMeritCount"] = $majorMeritCount;
        $returnArr["superMeritCount"] = $superMeritCount;
        $returnArr["ultraMeritCount"] = $ultraMeritCount;
    }

    return $returnArr;
}

// initialize libraries
$libenroll = new libclubsenrol();
$limport = new libimporttext();
$libTerm = new academic_year_term();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

if (! $plugin['eEnrollment']) {
    echo "You have no priviledge to access this page.";
    exit();
}

if (! $libenroll->hasAccessRight($_SESSION['UserID'])) {
    echo "You have no priviledge to access this page.";
    exit();
}

// Data from the Result page

$PageSemster = $Semester;
$lineBreakSymbol = '<!--lineBreakHere-->';

$crashedStudentArr = unserialize(rawurldecode($crashedStudentArr));
$crashedClubEnrolGroupIDArr = unserialize(rawurldecode($crashedClubEnrolGroupIDArr));
$crashedEventIDArr = unserialize(rawurldecode($crashedEventIDAry));

if ($isFromResultPage) {

    // get the StudentID array
    if (is_array($clash_studentID))
        $clash_studentIDArr = $clash_studentID;
    else
        $clash_studentIDArr = array(
            $clash_studentID
        );

    // get the Student info array
    $crashedStudentInfoArr = unserialize(rawurldecode($crashedStudentInfoArr));

    $data = array();
    for ($i = 0; $i < count($clash_studentIDArr); $i ++) {
        $thisStudentID = $clash_studentIDArr[$i];

        if ($type == "clubAllMember" || $type == "activityAllParticipant") {
            $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["EventTitle"];
        }

        if ($type == "clubAllMember") {
            $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["Semester"];
        }

        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["ClassName"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["ClassNumber"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["UserLogin"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["RoleTitle"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["Performance"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["Achievement"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["Comment"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["ActiveMemberStatus"];
        $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["AdminRole"];

        // Enrol Reason Customization
        if ($libenroll->enableActivityFillInEnrolReasonRight() && $type == "activityAllParticipant") {
            $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["Reason"];
        }

        // Merit Customization
        if ($libenroll->enableClubRecordMeritInfoRight()) {
            if ($type == "clubAllMember" || $type == "clubMember") {
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["meritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["minorMeritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["majorMeritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["superMeritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["ultraMeritCount"];
            }
        }

        if ($libenroll->enableActivityRecordMeritInfoRight()) {
            if ($type == "activityParticipant" || $type == "activityAllParticipant") {
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["meritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["minorMeritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["majorMeritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["superMeritCount"];
                $data[$i][] = $crashedStudentInfoArr[$thisStudentID]["ultraMeritCount"];
            }
        }
    }
} else {
    // Data from the first Import page

    $filepath = $userfile;
    $filename = $userfile_name;

    $back_parmeters = "EnrolGroupID=$EnrolGroupID&type=$type";

    if ($filepath == "none" || $filepath == "" || ! isset($GroupID)) { // import failed
        header("Location: import.php?$back_parmeters&Result=import_failed2");
        exit();
    } else {

        if ($limport->CHECK_FILE_EXT($filename)) {
            $name = $_FILES['userfile']['name'];
            $ext = strtoupper($lfs->file_ext($filename));

            // ## move to temp folder first for others validation
            $folderPrefix = $intranet_root . "/file/import_temp/eEnrolment/club_member";
            if (! file_exists($folderPrefix)) {
                $lfs->folder_new($folderPrefix);
            }

            $targetFileName = date('Ymd_His') . '_' . $_SESSION['UserID'] . '_' . $type . $ext;
            $targetFilePath = stripslashes($folderPrefix . "/" . $targetFileName);
            $lfs->lfs_copy($userfile, $targetFilePath);

            // read file into array
            // return 0 if fail, return csv array if success
            $data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow = 0, $lineBreakSymbol);
            $TitleData = $data[0];

            $format_array = array();
            if ($type == "clubAllMember") {
                // $format_array = array("ClubName(EN)", "Term", "ClassName", "ClassNumber", "Role", "Performance", "Achievement", "Comment", "Active/Inactive", "AdminRole");
                $format_array[] = "ClubName(EN)";
                $format_array[] = "Term";
                $format_array[] = "ClassName";
                $format_array[] = "ClassNumber";
                $format_array[] = "UserLogin";
                $format_array[] = "Role";
                $format_array[] = "Performance";
                $format_array[] = "Achievement";
                $format_array[] = "Comment";
                $format_array[] = "Active/Inactive";
                $format_array[] = "AdminRole";
            } else if ($type == "activityAllParticipant") {
                $format_array = array();
                $format_array[] = "ActivityTitle";
                $format_array[] = "ClassName";
                $format_array[] = "ClassNumber";
                $format_array[] = "UserLogin";
                $format_array[] = "Role";
                $format_array[] = "Performance";
                $format_array[] = "Achievement";
                $format_array[] = "Comment";
                $format_array[] = "Active/Inactive";
                $format_array[] = "AdminRole";

                if ($libenroll->enableActivityFillInEnrolReasonRight()) {
                    $format_array[] = "Reason";
                }
            } else {

                // $format_array = array("ClassName", "ClassNumber", "Role", "Performance", "Achievement", "Comment", "Active/Inactive", "AdminRole");

                $format_array[] = "ClassName";
                $format_array[] = "ClassNumber";
                $format_array[] = "UserLogin";
                $format_array[] = "Role";
                $format_array[] = "Performance";
                $format_array[] = "Achievement";
                $format_array[] = "Comment";
                $format_array[] = "Active/Inactive";
                $format_array[] = "AdminRole";
            }

            if ($libenroll->enableClubRecordMeritInfoRight()) {

                if ($type == "clubAllMember" || $type == "clubMember") {
                    // 2014-0519-1338-56177
                    // $format_array[] = "Merit";
                    // $format_array[] = "Minor Merit";
                    // $format_array[] = "Major Merit";
                    // $format_array[] = "Super Merit";
                    // $format_array[] = "Ultra Merit";

                    if (! $lstudentprofile->is_merit_disabled) {
                        $format_array[] = "Merit";
                    }
                    if (! $lstudentprofile->is_min_merit_disabled) {
                        $format_array[] = "MinorMerit";
                    }
                    if (! $lstudentprofile->is_maj_merit_disabled) {
                        $format_array[] = "MajorMerit";
                    }
                    if (! $lstudentprofile->is_sup_merit_disabled) {
                        $format_array[] = "SuperMerit";
                    }
                    if (! $lstudentprofile->is_ult_merit_disabled) {
                        $format_array[] = "UltraMerit";
                    }
                }
            }

            if ($libenroll->enableActivityRecordMeritInfoRight()) {
                if ($type == "activityParticipant" || $type == "activityAllParticipant") {
                    if (! $lstudentprofile->is_merit_disabled) {
                        $format_array[] = "Merit";
                    }
                    if (! $lstudentprofile->is_min_merit_disabled) {
                        $format_array[] = "MinorMerit";
                    }
                    if (! $lstudentprofile->is_maj_merit_disabled) {
                        $format_array[] = "MajorMerit";
                    }
                    if (! $lstudentprofile->is_sup_merit_disabled) {
                        $format_array[] = "SuperMerit";
                    }
                    if (! $lstudentprofile->is_ult_merit_disabled) {
                        $format_array[] = "UltraMerit";
                    }
                }
            }
            
            if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                $sqlTemp = "SELECT YearTermID, YearTermNameEN as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
                $termAry = $libenroll->returnResultSet($sqlTemp);
                $numOfTerm = count($termAry);
                $result = array_search('Performance',$format_array);
                
                for ($i = 0; $i < $numOfTerm; $i ++) {
                    $termName = str_replace(' ', '', $termAry[$i]["TermName"] . "Performance");
                    // insert termName between role and performance
                    array_splice($format_array, $result, 0, $termName);
                    $result ++;
                }
            }

            $limport->SET_CORE_HEADER($format_array);
            if (! $limport->VALIDATE_HEADER($data, true) || ! $limport->VALIDATE_DATA($data)) {
                header("Location: import.php?$back_parmeters&Result=import_failed");
                exit();
            }

            array_shift($data); // ignore the title row
        } else {
            header("Location: import.php?$back_parmeters&Result=import_failed");
            exit();
        }
    }
}

// intialization
$importStatusArr = array();
$importSuccessInfoArr = array();
$importFailedInfoArr = array();
$insertSuccessArr = array();
$updateSuccessArr = array();
$crash_EnrolGroupID_ary = array();
$crash_EnrolEventID_ary = array();

$successStatus = "success";
$failedStatus = "failed";

$email_student = array();
$email_student_EventID = array();

// new setting for not checking time crash 2014-12-02
if ($type == 0 || $type == 2) {
    $notCheckTimeCrashSetting = $libenroll->notCheckTimeCrash;
} else if ($type == 1 || $type == 3) {
    $notCheckTimeCrashSetting = $libenroll->Event_notCheckTimeCrash;
}

if ($type == "clubMember") {
    $thisEnrolGroupInfo = $libenroll->GET_GROUPINFO($EnrolGroupID, $AcademicYearID);
    $thisTargetGender = $thisEnrolGroupInfo['Gender'];
    $EnrolGroupEngName = $thisEnrolGroupInfo['Title'];
    $EnrolGroupChiName = $thisEnrolGroupInfo['TitleChinese'];
} else if ($type == "activityParticipant") {
    $thisEnrolEventInfo = $libenroll->GET_EVENTINFO($EnrolEventID, "", $AcademicYearID);
    $thisTargetGender = $thisEnrolEventInfo['Gender'];
    $CurrentEventName = $thisEnrolEventInfo['EventTitle'];
}

// loop through all records
$sizeOfDataArr = count($data);

for ($i = 0; $i < $sizeOfDataArr; $i ++) // $i=1 means ignore the title row
{
    if ($type == "clubAllMember") {
        if ($libenroll->enableClubRecordMeritInfoRight()) {
            list ($EventTitle, $Semester, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount) = $data[$i];
        } else {
            list ($EventTitle, $Semester, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
            
            if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                $termAry = $libenroll->getTermColumn($AcademicYearID);
                $numOfTerm = count($termAry);
                
                if (sizeof($data[$i]) == 13) {
                    list ($EventTitle, $Semester, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Term1Performance, $Term2Performance, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                } else if (sizeof($data[$i]) == 14) {
                    list ($EventTitle, $Semester, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Term1Performance, $Term2Performance, $Term3Performance, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                }
            }
        }

        $isWholeYearClub = false;
        if ($Semester == '' || trim(strtoupper($Semester)) == 'YEARBASE') {
            $isWholeYearClub = true;
        }

        // 2012-0802-1222-23147 - support input "YearBase" in Term column
        if ($isWholeYearClub) {
            $thisYearTermID = "";
            $termYearTermID = "0";
        } else {
            $thisYearTermID = $libTerm->Get_YearTermID_By_Name($AcademicYearID, $Semester);
            $termYearTermID = $thisYearTermID;
        }
        $thisGroupInfo = $libenroll->Get_GroupInfo_By_GroupTitle($EventTitle, $AcademicYearID, $termYearTermID);

        $thisGroupID = $thisGroupInfo[0]["GroupID"];
        // $thisYearTermID = $libTerm->Get_YearTermID_By_Name(Get_Current_Academic_Year_ID(), $Semester);
        $matchedEnrolGroupID = $libenroll->Get_Club_Of_Semester($thisGroupID, $thisYearTermID);
        // echo $thisGroupID.'/'.$thisYearTermID.'/'.$matchedEnrolGroupID;exit;
        if ($matchedEnrolGroupID > 0) {
            $thisEnrolGroupID = $matchedEnrolGroupID;
        }
        $thisEnrolGroupInfo = $libenroll->GET_GROUPINFO($thisEnrolGroupID);
        $thisTargetGender = $thisEnrolGroupInfo['Gender'];
    } else if ($type == "activityAllParticipant") {
        if ($libenroll->enableActivityFillInEnrolReasonRight()) {
            list ($EventTitle, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $Reason) = $data[$i];
            if ($libenroll->enableActivityRecordMeritInfoRight()) {
                list ($EventTitle, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount) = $data[$i];
            }
        } else {
            list ($EventTitle, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
            if ($libenroll->enableActivityRecordMeritInfoRight()) {
                list ($EventTitle, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount) = $data[$i];
            }
            
            if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                $termAry = $libenroll->getTermColumn($AcademicYearID);
                $numOfTerm = count($termAry);
                
                if (sizeof($data[$i]) == 12) {
                    list ($EventTitle, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Term1Performance, $Term2Performance, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                } else if (sizeof($data[$i]) == 13) {
                    list ($EventTitle, $ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Term1Performance, $Term2Performance, $Term3Performance, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                }
            }
        }

        $thisActivityInfo = $libenroll->Get_EventInfo_By_EventTitle($libenroll->Get_Safe_Sql_Query($EventTitle), $AcademicYearID);

        $thisTargetGender = $thisActivityInfo[0]['Gender'];
    } else {

        if ($libenroll->enableClubRecordMeritInfoRight() && $type == "clubMember") {
            list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount) = $data[$i];
        } else {
            list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
            if ($libenroll->enableActivityRecordMeritInfoRight()) {
                list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount) = $data[$i];
            }
            if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                $termAry = $libenroll->getTermColumn($AcademicYearID);
                $numOfTerm = count($termAry);

                if (sizeof($data[$i]) == 11) {
                    list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount) = $data[$i];
                    
                    list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Term1Performance, $Term2Performance, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                } else if (sizeof($data[$i]) == 12) {
                    list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Term1Performance, $Term2Performance, $Term3Performance, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                } else {
                    list ($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Achievement, $Comment, $ActiveMemberStatus, $AdminRole) = $data[$i];
                }
            }
        }

        $EventTitle = "";
    }

    // should not addslashes here. Otherwise, cannot search the tilte in SQL for special characters
    $ClassName = trim(str_replace($lineBreakSymbol, ' ', $ClassName));
    $ClassNumber = trim(str_replace($lineBreakSymbol, ' ', $ClassNumber));
    $UserLogin = trim(str_replace($lineBreakSymbol, ' ', $UserLogin));
    $RoleTitle = trim(str_replace($lineBreakSymbol, ' ', $RoleTitle));
    $Performance = trim(str_replace($lineBreakSymbol, ' ', $Performance));
    $Achievement = trim(str_replace($lineBreakSymbol, "\n", $Achievement));
    $Comment = trim(str_replace($lineBreakSymbol, "\n", $Comment));
    $ActiveMemberStatus = strtolower(trim(str_replace($lineBreakSymbol, ' ', $ActiveMemberStatus)));
    $EventTitle = trim(str_replace($lineBreakSymbol, ' ', $EventTitle));
    $Semester = trim(str_replace($lineBreakSymbol, ' ', $Semester));
    $AdminRole = strtoupper(trim(str_replace($lineBreakSymbol, ' ', $AdminRole)));
    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
        if (isset($Term1Performance))
            $Term1Performance = trim(str_replace($lineBreakSymbol, ' ', $Term1Performance));
        if (isset($Term2Performance))
            $Term2Performance = trim(str_replace($lineBreakSymbol, ' ', $Term2Performance));
        if (isset($Term3Performance))
            $Term3Performance = trim(str_replace($lineBreakSymbol, ' ', $Term3Performance));
    }

    // Enrol Reason Customization
    if ($libenroll->enableActivityFillInEnrolReasonRight() && $type == "activityAllParticipant") {
        $Reason = trim(str_replace($lineBreakSymbol, ' ', $Reason));
    }

    // Merit Customization
    if ($libenroll->enableClubRecordMeritInfoRight()) {
        if ($type == "clubAllMember" || $type == "clubMember") {
            $meritCount = trim(str_replace($lineBreakSymbol, ' ', $meritCount));
            $minorMeritCount = trim(str_replace($lineBreakSymbol, ' ', $minorMeritCount));
            $majorMeritCount = trim(str_replace($lineBreakSymbol, ' ', $majorMeritCount));
            $superMeritCount = trim(str_replace($lineBreakSymbol, ' ', $superMeritCount));
            $ultraMeritCount = trim(str_replace($lineBreakSymbol, ' ', $ultraMeritCount));
        }
    }

    if ($libenroll->enableActivityRecordMeritInfoRight()) {
        if ($type == "activityParticipant" || $type == "activityAllParticipant") {
            $meritCount = trim(str_replace($lineBreakSymbol, ' ', $meritCount));
            $minorMeritCount = trim(str_replace($lineBreakSymbol, ' ', $minorMeritCount));
            $majorMeritCount = trim(str_replace($lineBreakSymbol, ' ', $majorMeritCount));
            $superMeritCount = trim(str_replace($lineBreakSymbol, ' ', $superMeritCount));
            $ultraMeritCount = trim(str_replace($lineBreakSymbol, ' ', $ultraMeritCount));
        }
    }

    // #### Check if student exist #####
    if ($UserLogin == '') {
        $thisStudentInfoArr = $libenroll->Get_StudentInfo_By_ClassName_ClassNumber($ClassName, $ClassNumber, $AcademicYearID);
    } else {
        $thisStudentInfoArr = $libenroll->Get_StudentInfo_By_UserLogin($UserLogin, $AcademicYearID);
        $ClassName = $thisStudentInfoArr[0]['ClassName'];
        $ClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
    }

    // If not a student, return error and process the next student
    if ($thisStudentInfoArr[0]['UserID'] == NULL || $thisStudentInfoArr[0]['UserID'] == "") {
        $importStatusArr[] = $failedStatus;
        // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
        // $ActiveMemberStatus, "student_not_found", $EventTitle, $Semester, $AdminRole, $Achievement);
        if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "student_not_found", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
        } else {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "student_not_found", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
        }
        continue;
    } else {
        $thisStudentID = $thisStudentInfoArr[0]['UserID'];
    }

    // #### Check Gender #####
    if ($thisTargetGender != "A" && $thisTargetGender != "" && $thisTargetGender != $thisStudentInfoArr[0]['Gender']) {
        $importStatusArr[] = $failedStatus;

        // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
        // $ActiveMemberStatus, "target_gendar_not_match", $EventTitle, $Semester, $AdminRole, $Achievement);

        if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "target_gendar_not_match", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
        } else {

            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "target_gendar_not_match", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
        }
        continue;
    }

    // #### Check if Active Member Status Valid #####
    if ($ActiveMemberStatus != "active" && $ActiveMemberStatus != "inactive" && $ActiveMemberStatus != "") {
        $importStatusArr[] = $failedStatus;

        // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
        // $ActiveMemberStatus, "invalid_active_member_status", $EventTitle, $Semester, $AdminRole, $Achievement);
        if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "invalid_active_member_status", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
        } else {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "invalid_active_member_status", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
        }
        continue;
    }

    // #### Check if Admin Role Valid #####
    if ($AdminRole != "PIC" && $AdminRole != "HELPER" && $AdminRole != "N.A." && $AdminRole != "") {
        $importStatusArr[] = $failedStatus;
        // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
        // $ActiveMemberStatus, "invalid_admin_role", $EventTitle, $Semester, $AdminRole, $Achievement);
        if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "invalid_admin_role", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
        } else {
            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "invalid_admin_role", $EventTitle, $Semester, $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
        }
        continue;
    }

    // #### Club Handling #####
    if (substr($type, 0, 4) == "club") {
        if ($type == "clubAllMember") {
            // #### Check if Group exist #####
            // Get the GroupID if import all clubs
            // $thisGroupInfo = $libenroll->Get_GroupInfo_By_GroupTitle($EventTitle);

            // -1 means club not found, -2 means title same for more than 1 club
            // otherwise, store the GroupID of the club
            if ($thisGroupInfo == NULL) {
                $thisReason = "club_not_found";
                $importStatusArr[] = $failedStatus;
                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                } else {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
                }
                continue;
            } else if ($isWholeYearClub && count($thisGroupInfo) > 1) {
                $thisReason = "clubs_with_same_name";
                $importStatusArr[] = $failedStatus;
                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                } else {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
                }
                continue;
            } else {
                // $thisGroupID = $thisGroupInfo[0]["GroupID"];

                // #### Check if Semester exist #####
                if (! $isWholeYearClub) {
                    $thisYearTermID = $libTerm->Get_YearTermID_By_Name($AcademicYearID, $Semester);
                    if ($thisYearTermID == '') {
                        $thisReason = "semester_not_found";
                        $importStatusArr[] = $failedStatus;
                        if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                        } else {
                            $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
                        }
                        continue;
                    }
                }

                // #### Check if the semester match the club #####
                // $matchedEnrolGroupID = $libenroll->Get_Club_Of_Semester($thisGroupID, $thisYearTermID);
                if ($matchedEnrolGroupID > 0) {
                    $thisEnrolGroupID = $matchedEnrolGroupID;
                } else {
                    $thisReason = "semester_cannot_be_match";
                    $importStatusArr[] = $failedStatus;
                    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                        $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                    } else {
                        $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
                    }
                    continue;
                }
            }
        } else {
            $thisEnrolGroupID = $EnrolGroupID;
            $thisGroupID = $GroupID;
        }

        // Get RoleID of the input role
        if ($RoleTitle != '') // skip if empty
        {
            // if the role is not existed, create one
            $RoleTitle = addslashes($RoleTitle);
            $thisRoleID = $libenroll->Get_RoleID_From_RoleTitle($RoleTitle);
            if ($thisRoleID != - 1) {
                $thisRoleID = $thisRoleID;
            } else {
                $newRoleID = $libenroll->ADD_ROLE($RoleTitle);
                $thisRoleID = $newRoleID;
            }
        } else {
            // skip if empty
            $thisRoleID = '';
        }

        $thisClubInfoArr = array();
        $thisClubInfoArr["RoleID"] = $thisRoleID;
        // $thisClubInfoArr["Performance"] = addslashes($Performance);
        // $thisClubInfoArr["Achievement"] = addslashes($Achievement);
        // $thisClubInfoArr["Comment"] = addslashes($Comment);
        $thisClubInfoArr["Performance"] = $libenroll->Get_Safe_Sql_Query($Performance);
        $thisClubInfoArr["Achievement"] = $libenroll->Get_Safe_Sql_Query($Achievement);
        $thisClubInfoArr["Comment"] = $libenroll->Get_Safe_Sql_Query($Comment);

        if ($ActiveMemberStatus == "active") {
            $thisClubInfoArr["isActiveMember"] = 1;
        } else if ($ActiveMemberStatus == "inactive") {
            $thisClubInfoArr["isActiveMember"] = 0;
        } else if ($ActiveMemberStatus == "") {
            $thisClubInfoArr["isActiveMember"] = - 1;
        }

        // #### check if the student is already a member #####
        if ($libenroll->Is_Club_Member($thisStudentID, $thisEnrolGroupID)) {
            if ($Status == 1) // Update member info only if user choose approved
            {
                // update
                $updateSuccessArr["ClubMemberInfo"][$thisStudentID] = $libenroll->Update_Club_Member_Info($thisStudentID, $thisEnrolGroupID, $thisClubInfoArr);

                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                    $termBasedPerformanceAssoAry = BuildMultiKeyAssoc($libenroll->getClubTermBasedStudentPerformance($thisEnrolGroupID, $thisStudentID), array('StudentID','YearTermID'));

                    $termPerformanceArr = array();
                    $studentTermPerformanceArr = array();

                    for ($term = 0; $term < $numOfTerm; $term ++) {
                        // ## Get Term Info
                        $ObjAcademicYear = new academic_year(Get_Current_Academic_Year_ID());
                        $TermArr = $ObjAcademicYear->Get_Term_List($returnAsso = 0);
                        $YearTermID = $TermArr[$term]['YearTermID'];

                        $termPerformanceArr['RecordID'] = $termBasedPerformanceAssoAry[$thisStudentID][$YearTermID]['RecordID'];
                        $termPerformanceArr['StudentID'] = $thisStudentID;
                        $termPerformanceArr['EnrolGroupID'] = $thisEnrolGroupID;
                        $termPerformanceArr['YearTermID'] = $YearTermID;
                        $termPerformanceArr['Performance'] = ${"Term" . ($term + 1) . "Performance"};

                        $studentTermPerformanceArr[] = $termPerformanceArr;
                    }
                    $insertSuccessArr["TermPerformance"][$thisStudentID] = $libenroll->saveClubTermBasedStudentPerformance($studentTermPerformanceArr);
                }

                // Save the result and pass to the result page
                $importStatusArr[] = $successStatus;

                $importSuccessInfoArr[$i] = returnSuccessInfoArr($thisStudentID, $thisEnrolGroupID, $thisRoleID, "member_updated", $EventTitle, $thisYearTermID, $AdminRole, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
            } else {
                $importStatusArr[] = $failedStatus;
                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "already_member", $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                } else {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "already_member", $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
                }
            }
        } else {
            // insert
            // #### Check time clash if it is first import from the csv file #####
            // No need to check time clash if it is selected by the user in the result page

            if (! $isFromResultPage && $notCheckTimeCrashSetting != 1) {
                // check for time crash of clubs
                $crashClub = false;
                // get all clubs that the student has joined
                // $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$thisStudentID."'";
                // $StudentClub = $libenroll->returnArray($sql,1);
                $StudentClub = $libenroll->Get_Student_Club_Info($thisStudentID);
                $numOfStudentClub = count($StudentClub);

                for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash ++) {
                    if (($libenroll->IS_CLUB_CRASH($thisEnrolGroupID, $StudentClub[$CountCrash]['EnrolGroupID'], $StudentClub))) {
                        $crashClub = true;
                        $crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
                        continue;
                    }
                }

                // check for time crash of activities
                $crashActivity = false;
                // get student's enrolled event list, check for any time crash
                $StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($thisStudentID);
                $numOfStudentEvent = count($StudentEvent);

                for ($CountEvent = 0; $CountEvent < $numOfStudentEvent; $CountEvent ++) {
                    if ($libenroll->IS_EVENT_GROUP_CRASH($thisEnrolGroupID, $StudentEvent[$CountEvent]['EnrolEventID'])) {
                        $crashActivity = true;
                        $crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent]['EnrolEventID'];
                        continue;
                    }
                }

                if ($crashActivity && $crashClub) {
                    $thisReason = "crash_group_activity";
                } else if ($crashClub) {
                    $thisReason = "crash_group";
                } else if ($crashActivity) {
                    $thisReason = "crash_activity";
                }

                if ($crashActivity || $crashClub) {
                    $importStatusArr[] = $failedStatus;
                    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                        $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                    } else {
                        $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, $Semester, $AdminRole, $Achievement, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
                    }
                    continue;
                }
            }

            // #### add member #####
            $insertSuccessArr["EnrolList"][$thisStudentID] = $libenroll->Add_Student_To_Club_Enrol_List($thisStudentID, $thisEnrolGroupID);

            // if($libenroll->enableClubRecordMeritInfoRight()){
            //
            // # Import Merit
            // if($meritCount){
            // $libenroll->Add_Merit_Record($thisStudentID,$thisEnrolGroupID,$meritCount,'1');
            // }
            // if($minorMeritCount){
            // $libenroll->Add_Merit_Record($thisStudentID,$thisEnrolGroupID,$minorMeritCount,'2');
            // }
            // if($majorMeritCount){
            // $libenroll->Add_Merit_Record($thisStudentID,$thisEnrolGroupID,$majorMeritCount,'3');
            // }
            // if($superMeritCount){
            // $libenroll->Add_Merit_Record($thisStudentID,$thisEnrolGroupID,$superMeritCount,'4');
            // }
            // if($ultraMeritCount){
            // $libenroll->Add_Merit_Record($thisStudentID,$thisEnrolGroupID,$ultraMeritCount,'5');
            // }
            // }

            if ($Status == 1) {
                $insertSuccessArr["MemberList"][$thisStudentID] = $libenroll->Add_Student_To_Club_Member_List($thisStudentID, $thisEnrolGroupID, $thisRoleID);
                $insertSuccessArr["ClubMemberInfo"][$thisStudentID] = $libenroll->Update_Club_Member_Info($thisStudentID, $thisEnrolGroupID, $thisClubInfoArr);

                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {

                    for ($term = 0; $term < $numOfTerm; $term ++) {
                        $termPerformanceArr['StudentID'] = $thisStudentID;
                        $termPerformanceArr['EnrolGroupID'] = $thisEnrolGroupID;
                        $termPerformanceArr['Performance'] = ${"Term" . ($term + 1) . "Performance"};
                        
                        $studentTermPerformanceArr[] = $termPerformanceArr;
                    }
                    $insertSuccessArr["TermPerformance"][$thisStudentID] = $libenroll->saveClubTermBasedStudentPerformance($studentTermPerformanceArr);
                }
                $email_student[] = $thisStudentID;
                $email_student_EventID[] = $thisGroupID;
            }

            // Save the result and pass to the result page
            $importStatusArr[] = $successStatus;

            $importSuccessInfoArr[$i] = returnSuccessInfoArr($thisStudentID, $thisEnrolGroupID, $thisRoleID, "member_added", $EventTitle, $thisYearTermID, $AdminRole, '', $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount);
        }

        // Merit Customization
        if ($libenroll->enableClubRecordMeritInfoRight()) {
            if ($type == "clubAllMember" || $type == "clubMember") {
                $existingMeritRecordArr = $libenroll->Get_Merit_Record($thisStudentID, $thisEnrolGroupID);
                $existingMeritRecordAssoArr = array();
                $existingMeritRecordAssoArr = BuildMultiKeyAssoc($existingMeritRecordArr, 'MeritType');
                $existingMeritRecordTypeArr = array_keys($existingMeritRecordAssoArr);
                // Import Merit
                if ($meritCount != '') {
                    if (in_array('1', $existingMeritRecordTypeArr)) {
                        $libenroll->Update_Merit_Record('', $meritCount, '1', $thisStudentID, $thisEnrolGroupID);
                    } else {
                        $libenroll->Add_Merit_Record($thisStudentID, $thisEnrolGroupID, $meritCount, '1');
                    }
                } else {
                    $libenroll->Remove_Merit_Record("", $thisStudentID, $thisEnrolGroupID, '1');
                }

                if ($minorMeritCount != '') {
                    if (in_array('2', $existingMeritRecordTypeArr)) {
                        $libenroll->Update_Merit_Record('', $minorMeritCount, '2', $thisStudentID, $thisEnrolGroupID);
                    } else {
                        $libenroll->Add_Merit_Record($thisStudentID, $thisEnrolGroupID, $minorMeritCount, '2');
                    }
                } else {
                    $libenroll->Remove_Merit_Record("", $thisStudentID, $thisEnrolGroupID, '2');
                }

                if ($majorMeritCount != '') {
                    if (in_array('3', $existingMeritRecordTypeArr)) {
                        $libenroll->Update_Merit_Record('', $majorMeritCount, '3', $thisStudentID, $thisEnrolGroupID);
                    } else {
                        $libenroll->Add_Merit_Record($thisStudentID, $thisEnrolGroupID, $majorMeritCount, '3');
                    }
                } else {
                    $libenroll->Remove_Merit_Record("", $thisStudentID, $thisEnrolGroupID, '3');
                }

                if ($superMeritCount != '') {
                    if (in_array('4', $existingMeritRecordTypeArr)) {
                        $libenroll->Update_Merit_Record('', $superMeritCount, '4', $thisStudentID, $thisEnrolGroupID);
                    } else {
                        $libenroll->Add_Merit_Record($thisStudentID, $thisEnrolGroupID, $superMeritCount, '4');
                    }
                } else {
                    $libenroll->Remove_Merit_Record("", $thisStudentID, $thisEnrolGroupID, '4');
                }

                if ($ultraMeritCount != '') {
                    if (in_array('5', $existingMeritRecordTypeArr)) {
                        $libenroll->Update_Merit_Record('', $ultraMeritCount, '5', $thisStudentID, $thisEnrolGroupID);
                    } else {
                        $libenroll->Add_Merit_Record($thisStudentID, $thisEnrolGroupID, $ultraMeritCount, '5');
                    }
                } else {
                    $libenroll->Remove_Merit_Record("", $thisStudentID, $thisEnrolGroupID, '5');
                }
            }
        }

        // #### Add Staff #####
        // $thisEnrolGroupID = $libenroll->GET_ENROLGROUPID($thisGroupID);
        $AddStaffInfoArr['EnrolGroupID'] = $thisEnrolGroupID;
        $AddStaffInfoArr['UserID'] = $thisStudentID;

        if ($AdminRole == 'PIC' || $AdminRole == 'HELPER') {
            $AddStaffInfoArr['StaffType'] = $AdminRole;
            $successArr[$thisStudentID]['AddStaff'] = $libenroll->ADD_GROUPSTAFF($AddStaffInfoArr);
        } else if ($AdminRole == 'N.A.') {
            $successArr[$thisStudentID]['DeleteStaff'] = $libenroll->DEL_GROUPSTAFF($thisEnrolGroupID, $thisStudentID);
        }
    } else if (substr($type, 0, 8) == "activity") {
        // Get the EnrolEventID if import all activities
        if ($type == "activityAllParticipant") {
            // Check if Activity exist
            // Get the EnrolEventID if import all activities
            $thisActivityInfo = $libenroll->Get_EventInfo_By_EventTitle($libenroll->Get_Safe_Sql_Query($EventTitle), $AcademicYearID);

            // -1 means activity not found, -2 means title same for more than 1 activity
            // otherwise, store the EnrolEvent of the activity
            if ($thisActivityInfo == NULL) {
                $thisReason = "activity_not_found";
                $importStatusArr[] = $failedStatus;

                // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
                // $ActiveMemberStatus, $thisReason, $EventTitle, '',$AdminRole, $Achievement);
                //
                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, '', $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                } else {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, '', $AdminRole, $Achievement, $Reason);
                }

                continue;
            } // else if (count($thisActivityInfo) > 1)
              // {
              // $thisReason = "activity_with_same_name";
              // $importStatusArr[] = $failedStatus;
              //
              // // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
              // // $ActiveMemberStatus, $thisReason, $EventTitle, '',$AdminRole, $Achievement);
              // //
              //
              // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber,$UserLogin, $RoleTitle, $Performance, $Comment,
              // $ActiveMemberStatus, $thisReason, $EventTitle, '',$AdminRole, $Achievement, $Reason);
              //
              //
              // continue;
              // }
            else {
                $thisEnrolEventID = $thisActivityInfo[0]["EnrolEventID"];
            }
        } else {
            $thisEnrolEventID = $EnrolEventID;
        }

        $thisActivityInfoArr = array();
        $thisActivityInfoArr["StudentID"] = $thisStudentID;
        $thisActivityInfoArr["EnrolEventID"] = $thisEnrolEventID;
        // $thisActivityInfoArr["RoleTitle"] = addslashes($RoleTitle);
        // $thisActivityInfoArr["Performance"] = addslashes($Performance);
        // $thisActivityInfoArr["Achievement"] = addslashes($Achievement);
        // $thisActivityInfoArr["Comment"] = addslashes($Comment);
        $thisActivityInfoArr["RoleTitle"] = $libenroll->Get_Safe_Sql_Query($RoleTitle);
        $thisActivityInfoArr["Performance"] = $libenroll->Get_Safe_Sql_Query($Performance);
        $thisActivityInfoArr["Achievement"] = $libenroll->Get_Safe_Sql_Query($Achievement);
        $thisActivityInfoArr["Comment"] = $libenroll->Get_Safe_Sql_Query($Comment);

        if ($Status == 1)
            $thisActivityInfoArr["RecordStatus"] = 2; // set to approved
        else
            $thisActivityInfoArr["RecordStatus"] = 0; // set to waiting

        if ($ActiveMemberStatus == "active") {
            $thisActivityInfoArr["isActiveMember"] = 1;
        } else if ($ActiveMemberStatus == "inactive") {
            $thisActivityInfoArr["isActiveMember"] = 0;
        } else if ($ActiveMemberStatus == "") {
            $thisActivityInfoArr["isActiveMember"] = - 1;
        }

        // check if the student is already a member
        if ($libenroll->Is_Activity_Participant($thisStudentID, $thisEnrolEventID)) {
            if ($Status == 1) // Update member info only if user choose approved
            {
                // update
                $updateSuccessArr["ActivityParticipantInfo"][$thisStudentID] = $libenroll->Update_Activity_Participant_Info($thisStudentID, $thisEnrolEventID, $thisActivityInfoArr);

                // Save the result and pass to the result page
                $importStatusArr[] = $successStatus;
                // $importSuccessInfoArr[$i] = returnSuccessInfoArr($thisStudentID, $thisEnrolEventID, $RoleTitle, "participant_updated", $EventTitle, $Semester, $AdminRole);

                $importSuccessInfoArr[$i] = returnSuccessInfoArr($thisStudentID, $thisEnrolEventID, $RoleTitle, "participant_updated", $EventTitle, $Semester, $AdminRole, $Reason);
            } else {
                $importStatusArr[] = $failedStatus;
                // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
                // $ActiveMemberStatus, "already_member", $EventTitle, '', $AdminRole, $Achievement);

                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "already_member", $EventTitle, '', $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                } else {
                    $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, "already_member", $EventTitle, '', $AdminRole, $Achievement, $Reason);
                }
            }
        } else {
            // insert

            // Check time clash if it is first import from the csv file
            // No need to check time clash if it is selected by the user in the result page
            if (! $isFromResultPage && $notCheckTimeCrashSetting != 1) {
                // check for time crash of clubs
                $crashClub = false;
                // get all clubs that the student has joined -> check time crash
                // $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$thisStudentID."'";
                // $StudentClub = $libenroll->returnArray($sql,1);
                $StudentClub = $libenroll->Get_Student_Club_Info($thisStudentID);
                $numOfStudentClub = count($StudentClub);

                for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash ++) {
                    if (($libenroll->IS_EVENT_GROUP_CRASH($StudentClub[$CountCrash]['EnrolGroupID'], $thisEnrolEventID))) {
                        $crashClub = true;
                        $crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
                        continue;
                    }
                }

                // check for time crash of activities
                $crashActivity = false;
                // get student's enrolled event list, check for any time crash
                $StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($thisStudentID);
                $numOfStudentEvent = count($StudentEvent);

                for ($CountEvent = 0; $CountEvent < $numOfStudentEvent; $CountEvent ++) {
                    if ($libenroll->IS_EVENT_CRASH($thisEnrolEventID, $StudentEvent[$CountEvent]['EnrolEventID'], $StudentEvent)) {
                        $crashActivity = true;
                        $crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent]['EnrolEventID'];
                        continue;
                    }
                }

                if ($crashActivity && $crashClub) {
                    $thisReason = "crash_group_activity";
                } else if ($crashClub) {
                    $thisReason = "crash_group";
                } else if ($crashActivity) {
                    $thisReason = "crash_activity";
                }

                if ($crashActivity || $crashClub) {
                    $importStatusArr[] = $failedStatus;
                    // $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $RoleTitle, $Performance, $Comment,
                    // $ActiveMemberStatus, $thisReason, $EventTitle, '',$AdminRole, $Achievement);

                    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
                        $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, '', $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount, $superMeritCount, $ultraMeritCount, $Term1Performance, $Term2Performance, $Term3Performance);
                    } else {
                        $importFailedInfoArr[$i] = returnFailedInfoArr($ClassName, $ClassNumber, $UserLogin, $RoleTitle, $Performance, $Comment, $ActiveMemberStatus, $thisReason, $EventTitle, '', $AdminRole, $Achievement, $Reason, $meritCount, $minorMeritCount, $majorMeritCount);
                    }
                    continue;
                }
            }

            // add member
            if (! $libenroll->Is_Activity_Participant_List($thisStudentID, $thisEnrolEventID)) {
                $libenroll->ADD_EVENTSTUDENT($thisActivityInfoArr);
            }

            if ($Status == 1) {
                $libenroll->APPROVE_EVENT_STU($thisActivityInfoArr);
                $email_student[] = $thisStudentID;
                $email_student_EventID[] = $thisEnrolEventID;
            }

            // Save the result and pass to the result page
            $importStatusArr[] = $successStatus;
            // $importSuccessInfoArr[$i] = returnSuccessInfoArr($thisStudentID, $thisEnrolEventID, $RoleTitle, "participant_added", $EventTitle, $Semester, $AdminRole);
            $importSuccessInfoArr[$i] = returnSuccessInfoArr($thisStudentID, $thisEnrolEventID, $RoleTitle, "participant_added", $EventTitle, $Semester, $AdminRole, $Reason);
        }

        // Merit Customization
        if ($libenroll->enableActivityRecordMeritInfoRight()) {
            if ($type == "activityParticipant" || $type == "activityAllParticipant") {
                $existingMeritRecordArr = $libenroll->Get_Merit_Record($thisStudentID, $thisEnrolEventID, '', $isActivity = true);
                $existingMeritRecordAssoArr = array();
                $existingMeritRecordAssoArr = BuildMultiKeyAssoc($existingMeritRecordArr, 'MeritType');
                $existingMeritRecordTypeArr = array_keys($existingMeritRecordAssoArr);
                // Import Merit
                $array_merit = array(
                    'NO_INDEX_ZERO',
                    $meritCount,
                    $minorMeritCount,
                    $majorMeritCount
                );

                for ($i_merit = 1; $i_merit <= 5; $i_merit ++) {
                    if ($array_merit[$i_merit] != '') {
                        if (in_array('' . $i_merit, $existingMeritRecordTypeArr)) {
                            $libenroll->Update_Merit_Record('', $array_merit[$i_merit], '' . $i_merit, $thisStudentID, $thisEnrolEventID, $isActivity = true);
                        } else {
                            $libenroll->Add_Merit_Record($thisStudentID, $thisEnrolEventID, $array_merit[$i_merit], '' . $i_merit, $isActivity = true);
                        }
                    } else {
                        $libenroll->Remove_Merit_Record("", $thisStudentID, $thisEnrolEventID, '' . $i_merit, $isActivity = true);
                    }
                }
            }
        }

        // ## Add Staff
        $AddStaffInfoArr['EnrolEventID'] = $thisEnrolEventID;
        $AddStaffInfoArr['UserID'] = $thisStudentID;

        if ($AdminRole == 'PIC' || $AdminRole == 'HELPER') {
            $AddStaffInfoArr['StaffType'] = $AdminRole;
            $successArr[$thisStudentID]['AddStaff'] = $libenroll->ADD_EVENTSTAFF($AddStaffInfoArr);
        } else if ($AdminRole == 'N.A.') {
            $successArr[$thisStudentID]['DeleteStaff'] = $libenroll->DEL_EVENTSTAFF($thisEnrolEventID, $thisStudentID);
        }
    }
}

if ($sys_custom['eEnrolment']['SyncDataToPortfolio']) {
    if (substr($type, 0, 4) == "club") {
        $libenroll->SyncProfileRecordsForClub($AcademicYearID, $YearTermID, $EnrolGroupID);
    } else if (substr($type, 0, 8) == "activity") {
        $libenroll->SyncProfileRecordsForActivity($AcademicYearID, $YearTermID, $EnrolEventID);
    }
    $libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
}

if ($plugin['eClassTeacherApp'] && $isFromResultPage) {

    include_once ($PATH_WRT_ROOT . "includes/eClassApp/libeClassApp.php");
    $libeClassApp = new libeClassApp();
    $messageTitle = $Lang['eEnrolment']['PushMessage']['ClubCrashTitle'];
    $appType = $eclassAppConfig['appType']['Teacher'];
    $lookupClashestoSendPushMessagetoClubPIC = $libenroll->lookupClashestoSendPushMessagetoClubPIC;
    if ($lookupClashestoSendPushMessagetoClubPIC) {
        for ($k = 0; $k < sizeof($crashedClubEnrolGroupIDArr); $k ++) {

            $crashedClubEnrolGroupID = $crashedClubEnrolGroupIDArr[$k];
            $thiscrasedClubStudentID = $crashedStudentArr[$k];

            // $StudentInfoArr = $libenroll->Get_User_Info_By_UseID($thiscrasedClubStudentID);
            // $StudentEnglishName = $StudentInfoArr[0]['EnglishName'];
            // $StudentChineseName = $StudentInfoArr[0]['ChineseName'];
            // $StudentName =Get_Lang_Selection($StudentChineseName,$StudentEnglishName)==''?$StudentEnglishName:Get_Lang_Selection($StudentChineseName,$StudentEnglishName);
            if (in_array($thiscrasedClubStudentID, $clash_studentIDArr)) {

                $StudentNameArr = $libenroll->Get_User_Info_By_UseID($thiscrasedClubStudentID);

                $individualMessageInfoAry = array();
                for ($i = 0; $i < sizeof($crashedClubEnrolGroupID); $i ++) {

                    $thisEnrolGroupID = $crashedClubEnrolGroupID[$i];
                    $crashedClubPICArr = $libenroll->GET_GROUPSTAFF($thisEnrolGroupID, 'PIC');

                    $crashedClubGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
                    $crashedClubGroupNameArray = $libenroll->getGroupInfo($crashedClubGroupID);

                    // $crashedClubName =Get_Lang_Selection($crashedClubGroupNameArray[0]['TitleChinese'],$crashedClubGroupNameArray[0]['Title']) ==''?$crashedClubGroupNameArray[0]['Title']:Get_Lang_Selection($crashedClubGroupNameArray[0]['TitleChinese'],$crashedClubGroupNameArray[0]['Title']);
                    $PICStudentAssoAry = array();
                    for ($j = 0; $j < sizeof($crashedClubPICArr); $j ++) {

                        $crashedClubPICid = $crashedClubPICArr[$j]['UserID'];

                        $PICStudentAssoAry[$crashedClubPICid] = array(
                            $thiscrasedClubStudentID
                        );
                    }

                    $messageContent = str_replace("__StudentEngName__", $StudentNameArr[0]['EnglishName'], $Lang['eEnrolment']['PushMessage']['ClubCrash']);
                    $messageContent = str_replace("__StudentChiName__", $StudentNameArr[0]['ChineseName'], $messageContent);

                    $messageContent = str_replace("__CurrentPICEng__", $CurrentPICEngName, $messageContent);
                    $messageContent = str_replace("__CurrentPICChi__", $CurrentPICChiName, $messageContent);

                    $messageContent = str_replace("__CurrentClubEngName__", $EnrolGroupEngName, $messageContent);
                    $messageContent = str_replace("__CurrentClubChiName__", $EnrolGroupChiName, $messageContent);

                    $messageContent = str_replace("__ClubChiName__", $crashedClubGroupNameArray[0]['TitleChinese'], $messageContent);
                    $messageContent = str_replace("__ClubEngName__", $crashedClubGroupNameArray[0]['Title'], $messageContent);

                    $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $PICStudentAssoAry;

                    if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
                        $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString);
                    } else {
                        $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString);
                    }
                }
            }
        }
    }
    if ($libenroll->lookupClashestoSendPushMessagetoEventPIC) {
        for ($k = 0; $k < sizeof($crashedEventIDArr); $k ++) {

            $crashedEventID = $crashedEventIDArr[$k];
            $thiscrasedEventStudentID = $crashedStudentArr[$k];

            // $StudentInfoArr = $libenroll->Get_User_Info_By_UseID($thiscrasedEventStudentID);

            $StudentNameArr = $libenroll->Get_User_Info_By_UseID($thiscrasedEventStudentID);

            if (in_array($thiscrasedEventStudentID, $clash_studentIDArr)) {

                $individualMessageInfoAry = array();
                for ($i = 0; $i < sizeof($crashedEventID); $i ++) {
                    $thisEventID = $crashedEventID[$i];
                    $crashedEventPICArr = $libenroll->GET_EVENTSTAFF($thisEventID, 'PIC');

                    $crashedEventName = $libenroll->GET_EVENT_TITLE($thisEventID);

                    $PICStudentAssoAry = array();
                    for ($j = 0; $j < sizeof($crashedEventPICArr); $j ++) {
                        $crashedEventPICid = $crashedEventPICArr[$j]['UserID'];
                        $PICStudentAssoAry[$crashedEventPICid] = array(
                            $thiscrasedEventStudentID
                        );
                    }

                    $messageContent = str_replace("__StudentEngName__", $StudentNameArr[0]['EnglishName'], $Lang['eEnrolment']['PushMessage']['ActivityCrash']);
                    $messageContent = str_replace("__StudentChiName__", $StudentNameArr[0]['ChineseName'], $messageContent);

                    $messageContent = str_replace("__CurrentPICEng__", $CurrentPICEngName, $messageContent);
                    $messageContent = str_replace("__CurrentPICChi__", $CurrentPICChiName, $messageContent);

                    $messageContent = str_replace("__CurrentActivityChiName__", $CurrentEventName, $messageContent);
                    $messageContent = str_replace("__CurrentActivityEngName__", $CurrentEventName, $messageContent);

                    $messageContent = str_replace("__ActivityName__", $crashedEventName, $messageContent);
                    // $messageContent= str_replace("__ActivityEngName__", $crashedEventName,$messageContent);

                    $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $PICStudentAssoAry;

                    if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
                        $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString);
                    } else {
                        $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString);
                    }
                }
            }
        }
    }
}

// send email
$numEmailStudent = count($email_student);
$successResult = true;
if (substr($type, 0, 4) == "club") { // if type 0 or type 2, send club
    $RecordType = "club";
    if ($type == "clubAllMember") {
        // get group title and send to students one by one
        for ($i = 0; $i < $numEmailStudent; $i ++) {
            $userArray = array(
                $email_student[$i]
            );
            $GroupEventId = $email_student_EventID[$i];
            $libenroll->Send_Enrolment_Result_Email($userArray, $GroupEventId, $RecordType, $successResult);
        }
    } else {
        $GroupEventId = $GroupID;
        $libenroll->Send_Enrolment_Result_Email($email_student, $GroupEventId, $RecordType, $successResult);
    }
} else if (substr($type, 0, 8) == "activity") { // if type 1 or type 3, send activity
    $RecordType = "activity";
    if ($type == "activityAllParticipant") {
        // get activity title and send to students one by one
        $numEmailStudent = count($email_student);
        for ($i = 0; $i < $numEmailStudent; $i ++) {
            $userArray = array(
                $email_student[$i]
            );
            $GroupEventId = $email_student_EventID[$i];
            $libenroll->Send_Enrolment_Result_Email($userArray, $GroupEventId, $RecordType, $successResult);
        }
    } else {
        $GroupEventId = $EnrolEventID;
        $libenroll->Send_Enrolment_Result_Email($email_student, $GroupEventId, $RecordType, $successResult);
    }
} else {
    // do nothing
}

intranet_closedb();

?>

<body>
	<form name="form1" method="post" action="import_member_result.php">
		<input type="hidden" name="type" value="<?=$type?>"> <input
			type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>"> <input
			type="hidden" name="GroupID" value="<?=$GroupID?>"> <input
			type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>"> <input
			type="hidden" name="importStatusArr"
			value="<?=rawurlencode(serialize($importStatusArr))?>"> <input
			type="hidden" name="importSuccessInfoArr"
			value="<?=rawurlencode(serialize($importSuccessInfoArr))?>"> <input
			type="hidden" name="importFailedInfoArr"
			value="<?=rawurlencode(serialize($importFailedInfoArr))?>"> <input
			type="hidden" name="crash_EnrolGroupID_ary"
			value="<?=rawurlencode(serialize($crash_EnrolGroupID_ary))?>"> <input
			type="hidden" name="crash_EnrolEventID_ary"
			value="<?=rawurlencode(serialize($crash_EnrolEventID_ary))?>"> <input
			type="hidden" name="successStatus" value="<?=$successStatus?>"> <input
			type="hidden" name="failedStatus" value="<?=$failedStatus?>"> <input
			type="hidden" name="PageSemester" id="PageSemester"
			value="<?=$PageSemester?>" /> <input type="hidden"
			name="AcademicYearID" id="AcademicYearID"
			value="<?=$AcademicYearID?>" /> <input type="hidden" name="Status"
			value="<?=$Status?>">
	</form>


	<script language="Javascript">
		document.form1.submit();
	</script>


</body>


