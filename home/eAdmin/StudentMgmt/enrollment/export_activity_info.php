<?php
# using: 
/*
 * 2020-05-21 (Tommy): Added columns for $sys_custom['eEnrollment']['exportExtraInfor']
 * 2018-06-28 (Anna): Added $sys_custom['eEnrolment']['exportActivityOLEPartnerOrganizations']
 * 2017-05-31 (Anna) : Added keyword and categoryID when export
 * 2014-04-10 (Carlos): Added customization $sys_custom['eEnrolment']['TWGHCYMA'] for TWGHs C Y MA MEMORIAL COLLEGE
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


if ($plugin['eEnrollment']) {

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	
	$libenroll = new libclubsenrol();
		
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    if (!isset($_GET)) {
			header("Location: index.php");
			exit();
		}
		
		$ExportArr = array();
		$lexport = new libexporttext();
		$luser = new libuser();
		
		
		
		$CategoryID=$_GET['CategoryID'];
		$keyword = $_GET['Keyword'];
		# EnrolGroupIDArr
	//	$ClubInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info('', '', $AcademicYearID);
		$ClubInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info('', $keyword, $AcademicYearID,0,1,1,0,'','',true,'',$CategoryID);
		$EnrolGroupIDArr = array_keys($ClubInfoArr);
	
		# Target Form Arr
		$TargetFormArr = $libenroll->GET_EVENTCLASSLEVEL($EnrolGroupIDArr);
		$TargetFormAssoc = BuildMultiKeyAssoc($TargetFormArr, array("EnrolEventID","ClassLevelID"),"LevelName",1);
		
		# Club Category Arr
		$CategoryArr = $libenroll->GET_CATEGORY_LIST();
	 	$CategoryAssoc = BuildMultiKeyAssoc($CategoryArr,"CategoryID");
	 	
	 	#PIC Name Arr
	 	$PICArr = $libenroll->GET_EVENTSTAFF($EnrolGroupIDArr,'PIC');
	 	$PICIDArr = Get_Array_By_Key($PICArr,"UserID");
	 	$PICAssoc = BuildMultiKeyAssoc($PICArr,"EnrolEventID","UserID",1,1);
	 	
		$PICNameArr = $luser->getNameWithClassNumber($PICIDArr);
		
		#Meeting Date Arr
		$MeetingDate = $libenroll->Get_Activity_Meeting_Date();
		$MeetingDateAssoc = BuildMultiKeyAssoc($MeetingDate,"EnrolEventID","",0,1);
		
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$ActivityNatureArr = $libenroll->Get_Activity_Nature_List();
			$ActivityNatureAssoc = BuildMultiKeyAssoc($ActivityNatureArr,"NatureID");
		}
		
		# Column Title
		//$exportColumn = array("Activity Code","Event Name","Content","Category","Target Form","Target Age Group","Target Gender","Tentative Fee","Member Quota","In Charge","Enrolment Method","Enrol By","Enrolment Period","Schedule");
		$exportColumn = array("Activity Code");
		
		if($sys_custom['eEnrollment']['exportExtraInfor']){
		    $exportColumn[] = "Belong to club";
		}
		
		$exportColumn[] = "Event Name";
		$exportColumn[] = "Content";
		$exportColumn[] = "Category";
		
		if($sys_custom['eEnrollment']['exportExtraInfor']){
		    $exportColumn[] = "Nature";
		}

		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$exportColumn = array_merge($exportColumn,array("Volunteer Service","Volunteer Service Hour","Activity Location","School Location","External Location","Nature"));
		}
		$exportColumn = array_merge($exportColumn,array("Target Form","Target Age Group","Target Gender","Tentative Fee","Member Quota","In Charge","Enrolment Method","Enrol By","Enrolment Period","Schedule"));
		
		if($sys_custom['eEnrollment']['exportExtraInfor']){
		    $exportColumn[] = 'Record Type';
		    $exportColumn[] = 'Language';
		    $exportColumn[] = 'OLE Category';
		    $exportColumn[] = 'OLE Components';
		}
		if($sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
		    $exportColumn[] = "Partner Organizations";
		}
		
		if($sys_custom['eEnrollment']['exportExtraInfor']){
		    if(!$sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
		        $exportColumn[] = 'Partner Organizations';
		    }
		    $exportColumn[] = 'Details';
		    $exportColumn[] = 'School Remarks';
		}
		$exportColumn[] = 'WebSAMS STA Code';
		$exportColumn[] = 'WebSAMS STA Type';
		
		$ExportArr = array();
		foreach((array)$ClubInfoArr as $thisClubID => $thisClubInfo)
		{
			# Gender
			switch($thisClubInfo['Gender'])
			{
				case "M" : $Gender = $eEnrollment['male']; break;
				case "F" : $Gender = $eEnrollment['female']; break;
				case "A" : $Gender = $eEnrollment['all']; break;
			}
			
			# Date
			$DateDisplayArr = array();
			foreach((array)$MeetingDateAssoc[$thisClubID] as $DateArr)
			{
				$DateDisplayArr[] = $libenroll->Format_Meeting_Period($DateArr['ActivityDateStart'],$DateArr['ActivityDateEnd'],'ParStartDate ParStartTime - ParEndTime');
			}
			$DateDisplay = implode("\n",array_remove_empty((array)$DateDisplayArr));
			
			# PIC
			$ClubPICNameArr = array();
			foreach((array)$PICAssoc[$thisClubID] as $PIC)
			{
				$ClubPICNameArr[] = $PICNameArr[$PIC];
			}
			$ClubPICName = implode("\n",array_remove_empty((array)$ClubPICNameArr));
			
			#WebSAMS
			$webSamsArr = $libenroll->getWebSAMS('activity', (array)$thisClubInfo['EnrolGroupID']);
			
			$ExportCol = array();
			$ExportCol[] = $thisClubInfo['ActivityCode'];
			
			if($sys_custom['eEnrollment']['exportExtraInfor']){
// 			    $title = "";
// 			    $title = ($intranet_session_language=="en")?"Title":"TitleChinese";
// 			    $sql = "SELECT ".$title." FROM INTRANET_GROUP WHERE GroupID = (SELECT GroupID FROM INTRANET_ENROL_GROUPINFO WHERE EnrolGroupID = ".$thisClubInfo['EnrolGroupID'].")";
// 			    $title = $libenroll->returnVector($sql);
			    
// 			    $ExportCol[] = $title[0];
			    $ExportCol[] = $thisClubInfo['EnrolGroupID'];
			}
			
			$ExportCol[] = $thisClubInfo['EventTitle'];
			$ExportCol[] = removeHTMLtags($thisClubInfo['Description']);
			if($sys_custom['eEnrolment']['TWGHCYMA']){
				$EventCategoryIdList = $libenroll->Get_Activity_Event_Categories($thisClubInfo['EnrolEventID']);
				$CategoryCsvStr = "";
				if(count($EventCategoryIdList)>0){
					$delim = "";
					foreach($EventCategoryIdList as $cat_id){
						$CategoryCsvStr .= $delim.$CategoryAssoc[$cat_id]['CategoryName'];
						$delim = ",";
					}
				}
				$ExportCol[] = $CategoryCsvStr;
				
				$ExportCol[] = $thisClubInfo['IsVolunteerService'] == '1' ? $Lang['General']['Yes'] : $Lang['General']['No'];
				$ExportCol[] = $thisClubInfo['VolunteerServiceHour'];
				if($thisClubInfo['SchoolActivity'] == 2){
					$ExportCol[] = $Lang['eEnrolment']['MixedActivity'];
				}else if($thisClubInfo['SchoolActivity'] == 1){
					$ExportCol[] = $Lang['eEnrolment']['SchoolActivity'];
				}else{
					$ExportCol[] = $Lang['eEnrolment']['NonSchoolActivity'];
				}
				$ExportCol[] = $thisClubInfo['ActivityInLocation'];
				$ExportCol[] = $thisClubInfo['ActivityExtLocation'];
				
				$EventActivityNatureIdList = $libenroll->Get_Activity_Event_Natures($thisClubInfo['EnrolEventID']);
				$NatureCsvStr = "";
				if(count($EventActivityNatureIdList)>0){
					$delim = "";
					foreach($EventActivityNatureIdList as $nature_id){
						$NatureCsvStr .= $delim.$ActivityNatureAssoc[$nature_id]['Nature'];
						$delim = ",";
					}
				}	
				$ExportCol[] = $NatureCsvStr;
				
			}else{
				$ExportCol[] = $CategoryAssoc[$thisClubInfo['EventCategory']]['CategoryName'];
			}
			if($sys_custom['eEnrollment']['exportExtraInfor']){
// 			    if($thisClubInfo["SchoolActivity"] == 0){
// 			        $activity_nature = $Lang['eEnrolment']['NonSchoolActivity'];
// 			    }elseif ($thisClubInfo["SchoolActivity"] == 1){
// 			        $activity_nature = $Lang['eEnrolment']['SchoolActivity'];
// 			    }elseif ($thisClubInfo["SchoolActivity"] == 2){
// 			        $activity_nature = $Lang['eEnrolment']['MixedActivity'];
// 			    }
			    $ExportCol[] = $thisClubInfo["SchoolActivity"];
			}
			$ExportCol[] = implode("\n",array_remove_empty((array)$TargetFormAssoc[$thisClubID]));
			$ExportCol[] = ($thisClubInfo['UpperAge']==0 && $thisClubInfo['LowerAge']==0)?$Lang['eEnrolment']['NoLimit']:$thisClubInfo['LowerAge']." - ".$thisClubInfo['UpperAge'];
			$ExportCol[] = $Gender;
			$ExportCol[] = $thisClubInfo['PaymentAmount'].($thisClubInfo['isAmountTBC']?"(".$eEnrollment['ToBeConfirmed'].")":"");
			$ExportCol[] = $thisClubInfo['Quota']==0?$Lang['eEnrolment']['NoLimit']:$thisClubInfo['Quota'];
			$ExportCol[] = $ClubPICName;
			$ExportCol[] = $thisClubInfo['ApplyMethod']==1?$i_ClubsEnrollment_AppTime:$i_ClubsEnrollment_Random;
			$ExportCol[] = $thisClubInfo['ApplyUserType']=='S'?$Lang['Identity']['Student']:$Lang['Identity']['Parent'];
			$ExportCol[] = $thisClubInfo['ApplyStartTime']." - ".$thisClubInfo['ApplyEndTime'];
			$ExportCol[] = $DateDisplay;
			if($sys_custom['eEnrollment']['exportExtraInfor']){
			    include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
			    $OLESettingAry = $libenroll->Get_Enrolment_Default_OLE_Setting('club',(array)$thisClubInfo['EnrolGroupID']);
			    $ExportCol[] = $OLESettingAry[0]['IntExt'];
			    $ExportCol[] = $OLESettingAry[0]['TitleLang'];
			    $ExportCol[] = $OLESettingAry[0]['CategoryID'];
			    $ExportCol[] = $OLESettingAry[0]['OLE_Component'];
			    
// 			    $ExportCol[] = ($OLESettingAry[0]['IntExt'] == "INT")?$iPort["internal_record"]:$iPort["external_record"];
// 			    $ExportCol[] = ($OLESettingAry[0]['TitleLang'] == "E")?$Lang['General']['English']:$Lang['General']['Chinese'];
// 			    $file_array = $libenroll->Get_Ole_Category_Array();
// 			    for($k = 0; $k < sizeof($file_array); $k++){
// 			        if($OLESettingAry[0]['CategoryID'] == $file_array[$k][0]){
// 			            $ExportCol[] = $file_array[$k][1];
// 			        }
// 			    }
			    
// 			    include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
// 			    $LibPortfolio = new libpf_slp();
// 			    $DefaultELEArray = $LibPortfolio->GET_ELE();
// 			    $ole_component = explode(",", $OLESettingAry[0]['OLE_Component']);
// 			    $component = array();
// 			    foreach($DefaultELEArray as $ELE_ID => $ELE_Name){
// 			        if(in_array($ELE_ID, $ole_component)){
// 			            $component[] = $ELE_Name;
// 			        }
// 			    }
// 			    $ExportCol[] = implode("\n", $component);
			}
			
			if($sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
    			$OLESettingAry = $libenroll->Get_Enrolment_Default_OLE_Setting('activity',(array)$thisClubInfo['EnrolEventID']);
    			$ExportCol[] = $OLESettingAry[0]['Organization'];
			}
			
			if($sys_custom['eEnrollment']['exportExtraInfor']){
			    if(!$sys_custom['eEnrolment']['exportOLEPartnerOrganizations']){
			        $ExportCol[] = $OLESettingAry[0]['Organization'];
			    }
			    $ExportCol[] = $OLESettingAry[0]['Details'];
			    $ExportCol[] = $OLESettingAry[0]['SchoolRemark'];
			}
			$ExportCol[] = $webSamsArr[0]["WebSAMSCode"];
// 			if($webSamsArr[0]["WebSAMSSTAType"] == "E"){
// 			    $STAType = $eEnrollment['WebSAMSSTA']['ECA'];
// 			}elseif($webSamsArr[0]["WebSAMSSTAType"] == "S"){
// 			    $STAType = $eEnrollment['WebSAMSSTA']['ServiceDuty'];
// 			}elseif($webSamsArr[0]["WebSAMSSTAType"] == "I"){
// 			    $STAType = $eEnrollment['WebSAMSSTA']['InterSchoolActivities'];
// 			}
			$ExportCol[] = $webSamsArr[0]["WebSAMSSTAType"];
			
			$ExportArr[] = $ExportCol;
		//	debug_pr($ExportCol);
		}
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = "export_activity_info.csv";
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}

?>
