<?php
/*
 *  Using:
 *  
 *  2019-02-22 Cameron
 *  - add parameter ENROL_TYPE_ACTIVITY to UpdateEnrolEbookingRelation() and GetEnrolEbookingRelation()
 *   
 *  2018-10-11 Cameron
 *  - support multiple lesson-class combination
 *  
 *  2018-08-18 Cameron
 *  - allow booking if end time = start time and Location is selected
 *   
 *  2018-08-13 Cameron
 *  - allow instructor (non-admin staff) to manage course schedule on himself/herself
 *   
 * 	2018-07-30 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->isInstructor()))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
 
$result = array();
$dataAry = array();

$recordId = IntegerSafe($_POST['recordId']);        // EnrolEventID
$enrolEventID = $recordId;                          // CourseID
$academicYearID = IntegerSafe($_POST['AcademicYear']);
$eventDateIDAry = IntegerSafe($_POST['EventDateID']);
$lessonTitleAry = $_POST['LessonTitle'];
$locationAry = $_POST['Location'];
$startHourAry = $_POST['StartHour'];
$startMinuteAry = $_POST['StartMinute'];
$endHourAry = $_POST['EndHour'];
$endMinuteAry = $_POST['EndMinute'];
$instructorAry = $_POST['Instructor'];
$lessonClassAry = $_POST['LessonClass'];
$newLessonClassAry = $_POST['NewLessonClass'];

$eventDateIDToDelete = $_POST['eventDateIDToDelete'];

// debug_pr($_POST);
// exit;

$libenroll->Start_Trans();

    // Step 1: DELETE EventDate (lesson) and related booking record [has logged deleted record]
    if ($eventDateIDToDelete) {
        $eventDateIDToDeleteAry = explode(',',$eventDateIDToDelete);
//         debug_pr($eventDateIDToDelete);
//         debug_pr($eventDateIDToDeleteAry);
        
        $result['DeleteLesson'] = $libenroll->DEL_ENROL_EVENT_DATE('', $eventDateIDToDeleteAry);
    }
    
    // Step 2: Add new lesson to INTRANET_ENROL_EVENT_DATE
    $nrNewLesson = count($startHourAry[0]);
    for ($i = 0; $i < $nrNewLesson; $i++) {
        $lessonTitleName = 'NewLessonTitle_'.$i;
        $lessonTitle = standardizeFormPostValue($_POST[$lessonTitleName]);
        $lessonDateName = 'LessonDate_0_'.$i;
        $lessonDate = $_POST[$lessonDateName];
        $startHour = str_pad(IntegerSafe($startHourAry[0][$i]), 2, "0", STR_PAD_LEFT);
        $startMinute = str_pad(IntegerSafe($startMinuteAry[0][$i]), 2, "0", STR_PAD_LEFT);
        $activityDateStart = $lessonDate.' '.$startHour.':'.$startMinute.':00';
        $endHour = str_pad(IntegerSafe($endHourAry[0][$i]), 2, "0", STR_PAD_LEFT);
        $endMinute = str_pad(IntegerSafe($endMinuteAry[0][$i]), 2, "0", STR_PAD_LEFT);
        $activityDateEnd = $lessonDate.' '.$endHour.':'.$endMinute.':00';
        $locationID = $locationAry[0][$i];
        
        unset($dataAry);
        $dataAry['EnrolEventID'] = $enrolEventID;
        $dataAry['ActivityDateStart'] = $activityDateStart;
        $dataAry['ActivityDateEnd'] = $activityDateEnd;
        $dataAry['LocationID'] = $locationID;     // store locationID instead of location name for HKPF
        $dataAry['Lesson'] = $lessonTitle;
        $dataAry['RecordStatus'] = $enrolConfigAry['MeetingDate_RecordStatus']['Active'];
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,array(),false);
        $res = $libenroll->db_db_query($sql);
        $result["AddEventDate_0_{$i}"] = $res;
        if ($res) {
            $eventDateID = $libenroll->db_insert_id();
            
            $instructorAry[0][$i] = array_unique((array)$instructorAry[0][$i]);      // remove duplicate instructor
            
            // Step 3: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
            foreach((array)$instructorAry[0][$i] as $__instructor) {
                if ($__instructor) {       // filter out empty record
                    unset($dataAry);
                    $dataAry['EventDateID'] = $eventDateID;
                    $dataAry['TrainerID'] = $__instructor;
                    $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
                    $result["AddTrainer_0_{$i}"] = $libenroll->db_db_query($sql);
                }
            }
            
            // Step 4: add record to lesson-class
            foreach((array)$newLessonClassAry[$i] as $__intakeClassID => $__checkedVal) {
                unset($dataAry);
                $dataAry['EventDateID'] = $eventDateID;
                $dataAry['IntakeClassID'] = $__intakeClassID;
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, false, false);
                $result["AddNewLessonClass_{$eventDateID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
                
                // Step 5: add course-class
                unset($dataAry);
                $dataAry['EnrolEventID'] = $enrolEventID;
                $dataAry['IntakeClassID'] = $__intakeClassID;
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
                $result["AddNewCourseClass_{$enrolEventID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
            }
            
            // Step 6: prepare adding booking records:
            $requestAry = array();
            $bookingAry = array();
            $startTime = $startHour.':'.$startMinute.':00';
            $endTime = $endHour.':'.$endMinute.':00';
            $bookingAry['BookingType'] = 'room';
            $bookingAry['Date'] = $lessonDate;
            $bookingAry['StartTime'] = $startTime;
            $bookingAry['EndTime'] = $endTime;
            $bookingAry['RoomID'] = $locationID;
            $requestAry['BookingDetails'][$i] = $bookingAry;
            
            if (($endTime > $startTime) && $locationID) {
                // Step 7: add booking record
                $requestAry['BookingFrom'] = 'eEnrol';
                $libebooking_api = new libebooking_api();
                $bookingResultAry = $libebooking_api->RequestBooking($requestAry);
                if (count($bookingResultAry)) {
                    $bookingResult = array_pop($bookingResultAry);  // one element
                    $result["AddBooking_0_{$i}"] = $bookingResult['Status'];
                    $bookingID = $bookingResult['BookingID'];
                    $result["AddBookingRelation_0_{$i}"] = $libenroll->UpdateEnrolEbookingRelation($eventDateID, $bookingID, ENROL_TYPE_ACTIVITY);
                }
            }
        }
    }       // end loop for each new lesson
    
    
    // Step 8: update lesson record to INTRANET_ENROL_EVENT_DATE
    $_row = 0;
    foreach((array)$eventDateIDAry as $_eventDateID) {
        $_eventDateID = IntegerSafe($_eventDateID);
        $lessonTitle = standardizeFormPostValue($lessonTitleAry[$_eventDateID]);
        $lessonDateName = 'LessonDate_'.$_eventDateID.'_0';
        $lessonDate = $_POST[$lessonDateName];
        $startHour = str_pad(IntegerSafe($startHourAry[$_eventDateID][0]), 2, "0", STR_PAD_LEFT);
        $startMinute = str_pad(IntegerSafe($startMinuteAry[$_eventDateID][0]), 2, "0", STR_PAD_LEFT);
        $activityDateStart = $lessonDate.' '.$startHour.':'.$startMinute.':00';
        $endHour = str_pad(IntegerSafe($endHourAry[$_eventDateID][0]), 2, "0", STR_PAD_LEFT);
        $endMinute = str_pad(IntegerSafe($endMinuteAry[$_eventDateID][0]), 2, "0", STR_PAD_LEFT);
        $activityDateEnd = $lessonDate.' '.$endHour.':'.$endMinute.':00';
        $locationID = $locationAry[$_eventDateID][0];

        unset($dataAry);
        $dataAry['ActivityDateStart'] = $activityDateStart;
        $dataAry['ActivityDateEnd'] = $activityDateEnd;                
        $dataAry['LocationID'] = $locationID;     // store locationID instead of location name for HKPF 
        $dataAry['Lesson'] = $lessonTitle;
        $dataAry['RecordStatus'] = $enrolConfigAry['MeetingDate_RecordStatus']['Active'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        $condAry = array('EventDateID'=>$_eventDateID);
        $sql = $libenroll->UPDATE2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,$condAry,false);
        $result["UpdateEventDate_{$_eventDateID}"] = $libenroll->db_db_query($sql);
//debug_pr($sql);

        // Step 9. delete event date trainer
        $sql = "DELETE FROM INTRANET_ENROL_EVENT_DATE_TRAINER WHERE EventDateID='".$_eventDateID."'";
        $result["DeleteEventDateTrainer_{$_eventDateID}"] = $libenroll->db_db_query($sql);


        $instructorAry[$_eventDateID][0] = array_unique((array)$instructorAry[$_eventDateID][0]);      // remove duplicate instructor
        
        // Step 10: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
        foreach((array)$instructorAry[$_eventDateID][0] as $__instructor) {
            if ($__instructor) {       // filter out empty record
                unset($dataAry);
                $dataAry['EventDateID'] = $_eventDateID;
                $dataAry['TrainerID'] = $__instructor;
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
                $result["AddTrainer_{$_eventDateID}_{$__instructor}"] = $libenroll->db_db_query($sql);
                
            }
        }

        
        // Step 11. delete lesson-class
        $sql = "DELETE FROM INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS WHERE EventDateID='".$_eventDateID."'";
        $result["DeleteEventDateIntakeClass_{$_eventDateID}"] = $libenroll->db_db_query($sql);
        
        // Step 12: add record to lesson-class
        foreach((array)$lessonClassAry[$_eventDateID] as $__intakeClassID => $__checkedVal) {
            unset($dataAry);
            $dataAry['EventDateID'] = $_eventDateID;
            $dataAry['IntakeClassID'] = $__intakeClassID;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, false, false);
            $result["AddNewLessonClass_{$_eventDateID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
            
            // Step 13: add course-class
            unset($dataAry);
            $dataAry['EnrolEventID'] = $enrolEventID;
            $dataAry['IntakeClassID'] = $__intakeClassID;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
            $result["AddNewCourseClass_{$enrolEventID}_{$__intakeClassID}"] = $libenroll->db_db_query($sql);
        }
        
        // Step 14: check if booking exist
        $enrolBookingRelationAry = $libenroll->GetEnrolEbookingRelation($_eventDateID, ENROL_TYPE_ACTIVITY);
        $startTime = $startHour.':'.$startMinute.':00';
        $endTime = $endHour.':'.$endMinute.':00';
//debug_pr($enrolBookingRelationAry);

        // don't process booking if start time < end time or location is not selected 
        if (($endTime >= $startTime) && $locationID) {
            // Step 15: add / update booking records:
            if (count($enrolBookingRelationAry)) {  // booking record exist
                $bookingID= $enrolBookingRelationAry['BookingRecordID'];
    
                unset($dataAry);
                $dataAry['Date'] = $lessonDate;
                $dataAry['StartTime'] = $startTime;
                $dataAry['EndTime'] = $endTime;
                $dataAry['ModifiedBy'] = $_SESSION['UserID'];
                $condAry = array('BookingID'=>$bookingID);
                $sql = $libenroll->UPDATE2TABLE('INTRANET_EBOOKING_RECORD',$dataAry,$condAry,false);
                $result["UpdateEBookingRecord_{$bookingID}"] = $libenroll->db_db_query($sql);
    
                
                unset($dataAry);
                $dataAry['FacilityID'] = $locationID;
                $dataAry['ModifiedBy'] = $_SESSION['UserID'];
                $condAry = array('BookingID'=>$bookingID);
                $sql = $libenroll->UPDATE2TABLE('INTRANET_EBOOKING_BOOKING_DETAILS',$dataAry,$condAry,false);
                $result["UpdateEBookingDetails_{$bookingID}"] = $libenroll->db_db_query($sql);
    
            }
            else {      // new booking
                $requestAry = array();
                $bookingAry = array();
                $bookingAry['BookingType'] = 'room';
                $bookingAry['Date'] = $lessonDate;
                $bookingAry['StartTime'] = $startTime;
                $bookingAry['EndTime'] = $endTime;
                $bookingAry['RoomID'] = $locationID;
                $requestAry['BookingDetails'][$_row] = $bookingAry;
             
                // Step 16: add booking record
                $requestAry['BookingFrom'] = 'eEnrol';
                $libebooking_api = new libebooking_api();
                $bookingResultAry = $libebooking_api->RequestBooking($requestAry);
//debug_pr($bookingResultAry);
                if (count($bookingResultAry)) {
                    $bookingResult = array_pop($bookingResultAry);  // one element
                    $result["AddBooking_{$_eventDateID}"] = $bookingResult['Status'];
                    $bookingID = $bookingResult['BookingID'];
                    $result["AddBookingRelation_{$_eventDateID}"] = $libenroll->UpdateEnrolEbookingRelation($_eventDateID, $bookingID, ENROL_TYPE_ACTIVITY);
    
                }
            }
        }
        $_row++;
    }   // end loop for each event date
    
    $intakeClassIDToRemoveAry = $libenroll->getIntakeClassIDToRemove($enrolEventID);
    if (count($intakeClassIDToRemoveAry)) {
        $sql = "DELETE FROM INTRANET_ENROL_EVENT_INTAKE_CLASS WHERE EnrolEventID='".$enrolEventID."' AND IntakeClassID IN ('".implode("','",$intakeClassIDToRemoveAry)."')";
        $result["DeleteCourseClass_{$enrolEventID}"] = $libenroll->db_db_query($sql);
    }
    
if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}

//  debug_pr($result);
//  debug_pr($returnMsgKey);
header("location: event.php?AcademicYear=".$academicYearID."&returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>