<?php
// using: 

#################################
#   Date:   2020-01-17 (Tommy)
#       added adjustedReason for $sys_custom['eEnrolment']['Refresh_Performance']
#
#   Date:   2018-12-10 (Anna)
#       fix $PAGE_NAVIGATION show strange code [#E152829]
#
#	Date:	2017-03-16	(Villa)
#		update UI - add textbox for achievement/ commment
#		add CheckAll() - add apply all function
#
#	Date:	2015-12-16	(Omas)
#		added activity merit
#
#	Date:	2015-09-11	(Omas)
#		added $sys_custom['eEnrolment']['twghwfns']['AwardMgmt'] 
#
#	Date:	2015-06.02	(Evan)
#		Replace the old style with UI standard
#
# 	Date:	2015-03-23  (Omas)
# 		Customization : hide column ECA,SS,CS - Z42562 $sys_custom['eEnrolment']['tkogss_student_enrolment_report']
#
#	Date:	2014-11-13	(Omas)
#		Add select Teachers comment hidden layer
#		Modified logic will not generate hidden layer for each student
#		All the comments are get from comment bank Get_Achievement_Comment_Sql(), No need to get words from includes/libwordtemplates 
#
#	Date:	2014-10-14	(Omas)
#	Added $ImportedStudentAry from "add and assign"
#
#  2013-05-22 (Rita)
#  add merit
# 
#  2013-05-13 (Rita)
# 	Amend comment bank
# 
#  2013-04-17 (Rita)
# 	Add Comment Bank js
# 
#	2013-03-26 (Rita) 
#  Add Comment Bank
#################################
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
//include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);
$GroupID = IntegerSafe($GroupID);

$ImportedStudentAry = unserialize(rawurldecode($ImportedStudent));

$LibUser = new libuser($UserID);
$li = new libgroup($GroupID);
$lstudentprofile = new libstudentprofile();

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
		
	if ($type=="activity")
	{
		$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		
		//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_EVENT_PIC($EventEnrolID)))
		if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) 
			 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) 
			 && (!$libenroll->IS_ENROL_PIC($UserID, $EnrolEventID, 'Activity'))
			 && (!$libenroll->IS_ENROL_PIC($UserID, $EventInfoArr['EnrolGroupID'], 'Club'))
			)
			header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
			
		$CurrentPage = "PageMgtActivity";	
		$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		# tags 
        $tab_type = "activity";
      
        # navigation
        $PAGE_NAVIGATION[] = array($EventInfoArr['EventTitle']." ".$eEnrollment['student_perform_comment_list'], "");
	}
	else
	{
		if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_CLUB_PIC($EnrolGroupID)))
			header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
		$CurrentPage = "PageMgtClub";
		# tags 
        $tab_type = "club";
        # navigation
        $PAGE_NAVIGATION[] = array($li->Title." ".$eEnrollment['member_perform_comment_list'], "");
		
	}
	$current_tab = 1;
    include_once("management_tabs.php");
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();      
        $showRole = false;
        if ($type=="activity")
        {
       		//$TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['student_perform_comment_list'], "", 1);
       		
       		//check if show role column
       		//set roles to students in the event
			$sql = "SELECT GroupID FROM INTRANET_ENROL_EVENTINFO WHERE EnrolEventID = '$EnrolEventID'";
			$result = $libenroll->returnArray($sql,1);
			$GroupID = $result[0][0];		
			if ($GroupID!=0)	//event belongs to a group => show role selection bar
			{
				$showRole = true;
			}
			
			//generate the activity info at the top of the table
			$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
			$quotaDisplay = "<table class=\"form_table_v30\">";
			$quotaDisplay .= "<tr ><td class=\"field_title\">{$eEnrollment['act_name']}</td><td>".$EventInfoArr[3]."</td></tr>";
			if ($EventInfoArr[1] == 0)
			{
				
				$quotaDisplay .= "<tr ><td class=\"field_title\">$i_ClubsEnrollment_GroupQuota1 </td><td>".$i_ClubsEnrollment_NoLimit."</td></tr>";
			}
			else
			{
				$quotaDisplay .= "<tr ><td class=\"field_title\">$i_ClubsEnrollment_GroupQuota1 </td><td>".$EventInfoArr[1]."</td></tr>";
			}
			$quotaDisplay .= "<tr ><td class=\"field_title\">$i_ClubsEnrollment_GroupApprovedCount </td><td>".$EventInfoArr[2]."</td></tr>";
			$quotaDisplay .= "</table>";
			
			$navigationBar =  $linterface->GET_NAVIGATION2($eEnrollment['student_perform_comment_list']);
        }
        else	//club performance
        {
	        $showRole = true;
	        
	        $quotaDisplay = "<table class=\"form_table_v30\">";
			$quotaDisplay .= "<tr ><td class=\"field_title\" width=\"30%\">";
			$quotaDisplay .= "$i_ClubsEnrollment_GroupName </td><td>".$li->Title."</td></tr>";
	
			$groupQuota = $libenroll->getQuota($EnrolGroupID);
			$approvedCount = $libenroll->GET_NUMBER_OF_MEMBER($EnrolGroupID, "club");
			$quotaDisplay .= "<tr ><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_GroupQuota1 </td><td>$groupQuota</td></tr>";
			$quotaDisplay .= "<tr ><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_CurrentSize </td><td>$approvedCount</td></tr>";
			$quotaDisplay .= "</table>";
	        
	        $navigationBar =  $linterface->GET_NAVIGATION2($eEnrollment['member_perform_comment_list']);
        }
                        
        $display = "<table class=\"yui-skin-sam\" width=\"98%\" cellpadding=\"5\" cellspacing=\"0\">";
		$display .= "<tr><td colspan=\"5\">$navigationBar</td></tr>";
		
		$display .= "<tr>";		
		if ($showRole)
		{
			$display .= "<td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['role']."</td>";
		}
		
		$display .= "
			  <td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['class_name']." (".$eEnrollment['class_number'].")</td>
			  <td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['student_name']."</td>
			  <td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['attendence']."</td>";
			  		
		if ($libenroll->enableUserJoinDateRange()) {
			$display .= "<td width=\"10%\" class=\"tablegreentop tabletopnolink\">";
			$display .= $Lang['eEnrolment']['AvailiableDate'];
			$fromName = "DataFrom";
			$fromID = "DataFrom";
			$display .= "<div>";
			$display .= $Lang['eEnrolment']['AvailiableDateFrom'] . " " . $linterface->GET_DATE_PICKER($fromName, date("Y-m-d"), "", "yy-mm-dd", "", "", "", "", $fromID, 0, 1);
			$display .= "</div>";
			$toName = "DataTo";
			$toID = "DataTo";
			$display .= "<div>";
			$display .= $Lang['eEnrolment']['AvailiableDateTo'] . " " . $linterface->GET_DATE_PICKER($toName, date("Y-m-d"), "", "yy-mm-dd", "", "", "", "", $toID, 0, 1);
			$display .= "</div>";
			$display .= $linterface->Get_Apply_All_Icon("javascript:AssignAll('Master_AvailDate');");
			$display .= "</td>";
		}
// 	          <td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['performance']."</td>";
// 			  <td width=\"20%\" class=\"tablegreentop tabletopnolink\">".$Lang['eEnrolment']['Achievement']."</td>
// 			  <td width=\"20%\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['teachers_comment']."</td>";
		$display .= " <td width=\"10%\" class=\"tablegreentop tabletopnolink\">";
			$display .= $eEnrollment['performance']."</br>";
			$display .= "<div><input type=\"text\" class=\"textboxnum\"  id=\"Master_Performance\"></input></div>";
			# Performance Comment Selection Btn  
	   		$peformanceSelectionBtn = '';
			$peformanceSelectionBtn .= '<a  id="remarkBtn_PerformanceComment_Master_Performance" onclick=\'javascript:js_Select_Comment("PerformanceComment", "Master_Performance")\' href="javascript:void(0)">';
			$peformanceSelectionBtn .= '<img name="posimg1" align="absMiddle" onmouseover="MM_swapImage(\'posimg1\',\'\',\'/images/2009a/icon_pre-set_on.gif\',1)" onmouseout="MM_swapImgRestore()" src="/images/2009a/icon_pre-set_off.gif" border="0"/></a></div>';
			$peformanceSelectionBtn .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";
			$peformanceSelectionBtn .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";
// 	    	$display .= $peformanceSelectionBtn;
	    	$display .= $peformanceSelectionBtn;
	    	$display .= $linterface->Get_Apply_All_Icon("javascript:CheckAll('Master_Performance');");
		$display .= "</td>";
			
		if($sys_custom['eEnrolment']['Refresh_Performance']){
		    $display .= "<td width=\"10%\" class=\"tablegreentop tabletopnolink\">";
		    $display .= $Lang['eEnrolment']['Refresh_Performance']['AdjustedReason'];
		    $display .= "</td>";
		}
		
		$display .= "<td width=\"20%\" class=\"tablegreentop tabletopnolink\">";
			$display .= $Lang['eEnrolment']['Achievement'];
			$display .= "<textarea class=\"textboxtext\" id=\"Master_Achievement\"  cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\" $limitTextAreaEvent></textarea>";
			$achievementSelectionBtn = '';
			$achievementSelectionBtn .= '<div><a  id="remarkBtn_AchievementComment_Master_Achievement" onclick=\'javascript:js_Select_Comment("AchievementComment", "Master_Achievement")\' href="javascript:void(0)">';
			$achievementSelectionBtn .= '<img name="posimg1" align="absMiddle" onmouseover="MM_swapImage(\'posimg1\',\'\',\'/images/2009a/icon_pre-set_on.gif\',1)" onmouseout="MM_swapImgRestore()" src="/images/2009a/icon_pre-set_off.gif" border="0"/></a></div>';
			$achievementSelectionBtn .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";
			$achievementSelectionBtn .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";
			$display .= '<span id="achievementCommentLayer" style="float:right;">' . $achievementSelectionBtn.' </span>';
			$display .= $linterface->Get_Apply_All_Icon("javascript:CheckAll('Master_Achievement');");
		$display .= "</td>";
		
		$display .= "<td width=\"20%\" class=\"tablegreentop tabletopnolink\">";
			$display .= $eEnrollment['teachers_comment'];
			$display .= "<textarea class=\"textboxtext\" id=\"Master_Comment\"  cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\" $limitTextAreaEvent></textarea>";
			$commentSelectionBtn = '';
			$commentSelectionBtn .= '<div><a  id="remarkBtn_commentComment_Master_Comment" onclick=\'javascript:js_Select_Comment("commentComment", "Master_Comment")\' href="javascript:void(0)">';
			$commentSelectionBtn .= '<img name="posimg1" align="absMiddle" onmouseover="MM_swapImage(\'posimg1\',\'\',\'/images/2009a/icon_pre-set_on.gif\',1)" onmouseout="MM_swapImgRestore()" src="/images/2009a/icon_pre-set_off.gif" border="0"/></a></div>';
			$commentSelectionBtn .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";
			$commentSelectionBtn .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";
			$display .= '<span id="commentCommentLayer" style="float:right;">' . $commentSelectionBtn.' </span>';
			$display .= $linterface->Get_Apply_All_Icon("javascript:CheckAll('Master_Comment');");
			if ($type!="activity" && $sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
			    $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
			    
			    $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
			    $termAry = $libenroll->returnResultSet($sqlTemp);
			    $numOfTerm = count($termAry);
			    
			    // get term-based performance data
			    $termBasedPerformanceAssoAry = BuildMultiKeyAssoc($libenroll->getClubTermBasedStudentPerformance($EnrolGroupID), array('StudentID', 'YearTermID'));
			    
			    // build the table header with term name
			    for ($i=0; $i<$numOfTerm; $i++) {
			        $_yearTermId = $termAry[$i]['YearTermID'];
			        $_yearTermName = $termAry[$i]['TermName'];
			        
			        $display .= "<td width=\"10%\" class=\"tablegreentop tabletopnolink\">";
			        $display .= $_yearTermName.' '.$eEnrollment['performance'];
			        $display .= "</td>";
			    }
			}
		$display .= '</td>';
			##### Merit Record #####
			if($libenroll->enableClubRecordMeritInfoRight() && $type!="activity"){
				//2014-0519-1338-56177
//				$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$Lang['eDiscipline']['sdbnsm_award']."</td>";
//				$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_MinorCredit_unicode."</td>";
//				$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_MajorCredit_unicode."</td>";
//				$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_SuperCredit_unicode."</td>";
//				$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_UltraCredit_unicode."</td>";

				if (!$lstudentprofile->is_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_Merit."</td>";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_MinorCredit."</td>";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_MajorCredit."</td>";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_SuperCredit."</td>";
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_UltraCredit."</td>";
				}
			}
			else if($libenroll->enableActivityRecordMeritInfoRight() && $type=="activity"){
				if (!$lstudentprofile->is_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_Merit."</td>";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_MinorCredit."</td>";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_MajorCredit."</td>";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_SuperCredit."</td>";
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$display .= "<td width=\"40px\" nowrap class=\"tablegreentop tabletopnolink\">" .$i_Merit_UltraCredit."</td>";
				}
			}
			
// Omas - case#Z42562 	  
//		  if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
//		  	$display .= "<td width=\"10%\" class=\"tablegreentop tabletopnolink\">";
//		  		$display .= "<table border='0' cellpadding='3' cellspacing='0'>
//								<tr><td colspan='3' align='center'>".$Lang['eEnrolment']['PerformanceMark']."</td></tr>
//								<tr><td>".$Lang['eEnrolment']['ECA']."</td><td>".$Lang['eEnrolment']['SS']."</td><td>".$Lang['eEnrolment']['CS']."</td></tr></table>
//							";
//			$display .= "</td>";
//		  }
	    $display .= "</tr>";
	    
	    //Construct Student Info Array	    
	    //$name_field = getNameFieldByLang();
	    
	    // $ImportedStudentAry from "add & assign" page
	    if (sizeof($UID)==0){
	    	$UID = $ImportedStudentAry;
	    }
	    
	    // get student info
	    $objUser = new libuser('', '', $UID);
	    
	    // get student club or activity info
	    if ($type=="activity") {
	    	$activityParticipantInfoAry = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventID, '', $AcademicYearID, $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=0);
	    	$studentInfoAssoAry = BuildMultiKeyAssoc($activityParticipantInfoAry[$EnrolEventID]['StatusStudentArr'][ENROL_RECORDSTATUS_APPROVED], array('StudentID'));
	    }
	    else {
	    	$studentInfoAssoAry = BuildMultiKeyAssoc($libenroll->Get_Club_Member_Info($EnrolGroupID, '', '', $IndicatorArr=0, $IndicatorWithStyle=1, $WithEmptySymbol=0), array('StudentID'));

	    }

	    if ($libenroll->enableUserJoinDateRange()) {
			####Get Role title array
			if ($type=="activity") {
				$sql = "SELECT 
							StudentID as UserID,
							RoleTitle,
							EnrolAvailiableDateStart,
							EnrolAvailiableDateEnd
						FROM 
							INTRANET_ENROL_EVENTSTUDENT 
						WHERE
							EnrolEventID = '".$EnrolEventID."' ";
			}
			else {	
			 	$sql = "SELECT 
							ug.UserID,
							ir.title as RoleTitle,
			 				EnrolAvailiableDateStart,
							EnrolAvailiableDateEnd
					FROM 
						INTRANET_USERGROUP ug 
						LEFT OUTER JOIN  INTRANET_ROLE ir ON (ug.RoleID=ir.RoleID) 
					WHERE 
						ug.EnrolGroupID = ".$EnrolGroupID."";
			 }
			 
			 $roleArr = $li->ReturnResultSet($sql);
			 $roleAssoArr = BuildMultiKeyAssoc($roleArr, 'UserID', array('RoleTitle', 'EnrolAvailiableDateStart', 'EnrolAvailiableDateEnd'),1);
			 
			 
	    } else {
	    	####Get Role title array
	    	if ($type=="activity") {
	    		$sql = "SELECT
							StudentID as UserID,
							RoleTitle
						FROM
							INTRANET_ENROL_EVENTSTUDENT
						WHERE
							EnrolEventID = '".$EnrolEventID."' ";
	    	}
	    	else {
	    		$sql = "SELECT
							ug.UserID,
							ir.title as RoleTitle
					FROM
						INTRANET_USERGROUP ug
						LEFT OUTER JOIN  INTRANET_ROLE ir ON (ug.RoleID=ir.RoleID)
					WHERE
						ug.EnrolGroupID = ".$EnrolGroupID."";
	    	}
	    	$roleArr = $li->ReturnResultSet($sql);
	    	$roleAssoArr = BuildMultiKeyAssoc($roleArr, 'UserID', array('RoleTitle'),1);
	    }
	    
		for ($i=0; $i<sizeof($UID); $i++){
	    	$_studentId = $UID[$i];
	    	$objUser->LoadUserData($_studentId);
	    	
	    	
		    //$StudentArray[$i]['StudentID'] = $UID[$i];
		    $StudentArray[$i]['StudentID'] = $_studentId;
		    if ($showRole)
		    {
			    //get student role
			    if ($type=="activity")
			    {
//				    $sql = "SELECT RoleTitle FROM INTRANET_ENROL_EVENTSTUDENT WHERE StudentID = '".$_studentId."' AND EnrolEventID = '$EnrolEventID'";
//					$result = $li->returnArray($sql,1);
//				    $StudentArray[$i]['Role'] = $result[0][0];
			    	if ($libenroll->enableUserJoinDateRange()) {
						$StudentArray[$i]['Role'] = Get_String_Display($roleAssoArr[$_studentId]["RoleTitle"]);
			    	} else {
			    		$StudentArray[$i]['Role'] = Get_String_Display($roleAssoArr[$_studentId]);
			    	}
			    }
			    else
			    {
//				    $sql = "SELECT RoleID FROM INTRANET_USERGROUP WHERE UserID = '".$_studentId."' AND EnrolGroupID = '$EnrolGroupID'";
//				    $result = $li->returnArray($sql,1);
//				    $sql = "SELECT Title FROM INTRANET_ROLE WHERE RoleID = '".$result[0][0]."'";
//				    $result2 = $li->returnArray($sql,1);
//				    $StudentArray[$i]['Role'] = $result2[0][0];
			    	if ($libenroll->enableUserJoinDateRange()) {
			    		$StudentArray[$i]['Role'] = Get_String_Display($roleAssoArr[$_studentId]["RoleTitle"]);
			    	} else {
			    		$StudentArray[$i]['Role'] = Get_String_Display($roleAssoArr[$_studentId]);
			    	}
					
			    }
			   
		    }
		    
		    
		    //get student ClassName, ClassNumber, Name
//		    $sql = "SELECT ClassName, ClassNumber, ".$name_field." FROM INTRANET_USER WHERE UserID = '".$_studentId."'";
//		    $result = $li->returnArray($sql,3);
//
//		    if (($result[0][0]==NULL || $result[0][0]=="" || $result[0][1]==NULL || $result[0][1]=="")){
//			    $StudentArray[$i]['Class'] = "-";
//		    }
//		    else{
//			    $StudentArray[$i]['Class'] = $result[0][0]."(".$result[0][1].")";
//		    }
//		    $StudentArray[$i]['StudentName'] = $result[0][2];
			
			if ($objUser->ClassName == '' || $objUser->ClassNumber == '') {
				$StudentArray[$i]['Class'] = $Lang['General']['EmptySymbol'];
			}
			else {
				$StudentArray[$i]['Class'] = $objUser->ClassName.' ('.$objUser->ClassNumber.')';
			}
			$StudentArray[$i]['StudentName'] = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
			
		    
		    //get attendance, performace and comment
		    $DataArr['StudentID'] = $_studentId;
		    
		    if ($type=="activity")
		    {
			    $DataArr['EnrolEventID'] = $EnrolEventID;
			    
			    $StudentArray[$i]['Attendance'] = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);
			    //$StudentArray[$i]['Performance'] = $libenroll->GET_EVENT_STUDENT_PERFORMANCE($DataArr);
			    //$StudentArray[$i]['Comment'] = $libenroll->GET_EVENT_STUDENT_COMMENT($DataArr);

// Omas - case#Z42562
//			    if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
//				    $StudentArray[$i]['PerformanceMark'] = $libenroll->GET_EVENT_STUDENT_PERFORMANCE_MARK($DataArr);
//				    $StudentArray[$i]['ECA'] = $StudentArray[$i]['PerformanceMark']['ECA'];
//				    $StudentArray[$i]['SS'] = $StudentArray[$i]['PerformanceMark']['SS'];
//				    $StudentArray[$i]['CS'] = $StudentArray[$i]['PerformanceMark']['CS'];
//			    }
		    }
		    else
		    {
	        	//$DataArr['GroupID'] = $GroupID;
				$DataArr['EnrolGroupID'] = $EnrolGroupID;
				
				$StudentArray[$i]['Attendance'] = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
				//$StudentArray[$i]['Performance'] = $libenroll->GET_GROUP_STUDENT_PERFORMANCE($DataArr);
	        	//$StudentArray[$i]['Comment'] = $libenroll->GET_GROUP_STUDENT_COMMENT($DataArr);
		    }
		    if ($libenroll->enableUserJoinDateRange()) {
		    	$StudentArray[$i]['EnrolAvailiableDateStart'] = $roleAssoArr[$_studentId]['EnrolAvailiableDateStart'];
		    	$StudentArray[$i]['EnrolAvailiableDateEnd'] = $roleAssoArr[$_studentId]['EnrolAvailiableDateEnd'];
		    	if ($StudentArray[$i]['EnrolAvailiableDateStart'] == "0000-00-00 00:00:00") $StudentArray[$i]['EnrolAvailiableDateStart'] = ""; 
		    	if ($StudentArray[$i]['EnrolAvailiableDateEnd'] == "0000-00-00 00:00:00") $StudentArray[$i]['EnrolAvailiableDateEnd'] = "";
		    	if (!empty($StudentArray[$i]['EnrolAvailiableDateStart'])) {
		    		$StudentArray[$i]['EnrolAvailiableDateStart'] = date("Y-m-d", strtotime($StudentArray[$i]['EnrolAvailiableDateStart']));
		    	}
		    	if (!empty($StudentArray[$i]['EnrolAvailiableDateEnd'])) {
		    		$StudentArray[$i]['EnrolAvailiableDateEnd'] = date("Y-m-d", strtotime($StudentArray[$i]['EnrolAvailiableDateEnd']));
		    	}
		    }
		    $StudentArray[$i]['Performance'] = $studentInfoAssoAry[$_studentId]['Performance'];
		    if($sys_custom['eEnrolment']['Refresh_Performance']){
		        $StudentArray[$i]['AdjustedReason'] = $studentInfoAssoAry[$_studentId]['AdjustedReason'];
		    }
	        $StudentArray[$i]['Comment'] = $studentInfoAssoAry[$_studentId]['Comment'];
	        $StudentArray[$i]['Achievement'] = $studentInfoAssoAry[$_studentId]['Achievement'];
	    }
	    $StudentSize = sizeof($UID);	 
	    
	    /* No limit of text length
	    $textAreaMaxChar = 255;
    	*/
    	
//		$lword = new libwordtemplates();
//		$words = $lword->getWordList(8);
		
		//$select_word = $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1);
			    
	    # Student Content
		if (isset($StudentSize) && $StudentSize > 0) {
		    for($j=0; $j<$StudentSize; $j++){
			    $tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
			    
			    $_studentId = $StudentArray[$j]['StudentID'];
			    
			    $performTextareaName = "performID[".$j."]";
				$performTextareaID = "performID".$j;
				$performTextareaValue = intranet_htmlspecialchars($StudentArray[$j]['Performance']);
				
				if($sys_custom['eEnrolment']['Refresh_Performance']){
				    $adjustedReasonTextareaName = "adjustedReason[".$j."]";
				    $adjustedReasonTextareaID = "adjustedReason".$j;
				    $adjustedReasonTextareaValue = intranet_htmlspecialchars($StudentArray[$j]['AdjustedReason']);
				}
				
				$achievementTextareaName = "achievementID[".$j."]";
				$achievementTextareaID = "achievementID".$j;
				$achievementTextareaValue = $StudentArray[$j]['Achievement'];
				
			    $commentTextareaName = "commentID[".$j."]";
				$commentTextareaID = "commentID".$j;
				$commentTextareaValue = $StudentArray[$j]['Comment'];

// Omas - case#Z42562				
//				if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
//					# ECA
//					$ecaName = "eca[".$j."]";
//					$ecaID = "eca".$j;
//					$ecaValue = $StudentArray[$j]['ECA'];
//					# SS
//					$ssName = "ss[".$j."]";
//					$ssID = "ss".$j;
//					$ssValue = $StudentArray[$j]['SS'];
//					# CS
//					$csName = "cs[".$j."]";
//					$csID = "cs".$j;
//					$csValue = $StudentArray[$j]['CS'];
//				}
	
				$display .= '<tr class="'.$tr_css.'">';
				if ($showRole)
				{
					$display .= '<td class="tabletext" valign="top">'.$StudentArray[$j]['Role'].'</td>';
				}
				$display .= '<td class="tabletext" valign="top">'.$StudentArray[$j]['Class'].'</td>
		          	  <td class="tabletext" valign="top">'.$StudentArray[$j]['StudentName'].'</td>
		          	  <td class="tabletext" valign="top">'.$StudentArray[$j]['Attendance'].'%</td>';
				
				if ($libenroll->enableUserJoinDateRange()) {
					
					$AvailiableDateStartName = "AvailiableDateStart[".$j."]";
					$AvailiableDateStartID = "AvailiableDateStart".$j;
					$AvailiableDateEndName = "AvailiableDateEnd[".$j."]";
					$AvailiableDateEndID = "AvailiableDateEnd".$j;
					
					$display .= '<td class="tabletext" valign="top">';
					$display .= $Lang['eEnrolment']['AvailiableDateFrom'] . " " . $linterface->GET_DATE_PICKER($AvailiableDateStartName, $StudentArray[$j]["EnrolAvailiableDateStart"], "", "yy-mm-dd", "", "", "", "", $AvailiableDateStartID, 0, 1, false, "Master_AvailDate_From") . "<br />";
					$display .= $Lang['eEnrolment']['AvailiableDateTo'] . " " . $linterface->GET_DATE_PICKER($AvailiableDateEndName, $StudentArray[$j]["EnrolAvailiableDateEnd"], "", "yy-mm-dd", "", "", "", "", $AvailiableDateEndID, 0, 1, false, "Master_AvailDate_To");
					$display .= '</td>';
				}
				
				$display .= '<td class="tabletext" valign="top">';
		        
		        //performace row  
		        /* No limit of text length	  
		        $limitTextAreaEvent = "onKeyDown='limitText(document.getElementById(\"$performTextareaID\"),".$textAreaMaxChar.");'";
				$limitTextAreaEvent .= "onKeyUp='limitText(document.getElementById(\"$performTextareaID\"),".$textAreaMaxChar.");'"; 
		        */
		        
		        $display .= "<input type=\"text\" class=\"textboxnum Master_Performance\" name=\"$performTextareaName\" id=\"$performTextareaID\" value=\"".$performTextareaValue."\"></input>";  
		       
		     //   $select_word2 = $linterface->GET_PRESET_LIST("jArrayWords", $j,  $performTextareaID);
		    //    $select_wordList = $select_word.$select_word2;
		    //    $display .= $select_wordList;
		    	
		    	# Performance Comment Selection Btn  
		   		$peformanceSelectionBtn = '';
				$peformanceSelectionBtn .= '<div><a  id="remarkBtn_PerformanceComment_'.$performTextareaID.'" onclick=\'javascript:js_Select_Comment("PerformanceComment", "'.$performTextareaID.'")\' href="javascript:void(0)">';
				$peformanceSelectionBtn .= '<img name="posimg1" align="absMiddle" onmouseover="MM_swapImage(\'posimg1\',\'\',\'/images/2009a/icon_pre-set_on.gif\',1)" onmouseout="MM_swapImgRestore()" src="/images/2009a/icon_pre-set_off.gif" border="0"/></a></div>';
				$peformanceSelectionBtn .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";
				$peformanceSelectionBtn .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";
		    	
		    	$display .= $peformanceSelectionBtn;

		        $display .= '</td>';
		        
		        if($sys_custom['eEnrolment']['Refresh_Performance']){
		            $display .= '<td class="tabletext" valign="top">';
		            $display .= "<textarea type=\"text\" class=\"textboxnum Master_Achievement\" name=\"$adjustedReasonTextareaName\" id=\"$adjustedReasonTextareaID\" cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\">$adjustedReasonTextareaValue</textarea>";
		            $display .= '</td>';
		        }
		        		       
		        # Archievement Comment Textarea
		        $display .= '<td class="tabletext" valign="top">';
		        if($sys_custom['eEnrolment']['twghwfns']['AwardMgmt']){
		        	$display .= "$achievementTextareaValue";
		        }
		        else{
					$display .= "<textarea class=\"textboxtext Master_Achievement\" id=\"$achievementTextareaID\" name=\"$achievementTextareaName\" cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\" $limitTextAreaEvent>$achievementTextareaValue</textarea>";
		        }
					
				###########     achievement Comment Bank Customization      ###########
		        if($libenroll->enableAchievementCommentBank() && !$sys_custom['eEnrolment']['twghwfns']['AwardMgmt']){
		        	# Get Achievement Comment Data
//		        	$achievementDataSql = $libenroll->Get_Achievement_Comment_Sql('','',$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
//		        	$achievementCommentDataArray = $libenroll->returnArray($achievementDataSql);
//		        	$displayAchievementArr = array();
//		        	$numOfachievementCommentDataArray = count($achievementCommentDataArray);
    				        	
					$achievementSelectionBtn = '';
					$achievementSelectionBtn .= '<div><a id="remarkBtn_AchievementComment_'.$achievementTextareaID.'" onclick=\'javascript:js_Select_Comment("AchievementComment", "'.$achievementTextareaID.'")\' href="javascript:void(0)">';
					$achievementSelectionBtn .= '<img name="posimg1" align="absMiddle" onmouseover="MM_swapImage(\'posimg1\',\'\',\'/images/2009a/icon_pre-set_on.gif\',1)" onmouseout="MM_swapImgRestore()" src="/images/2009a/icon_pre-set_off.gif" border="0"/></a></div>';
					$achievementSelectionBtn .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";
					$achievementSelectionBtn .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";

					$display .= '<span id="achievementCommentLayer" style="float:right;">' . $achievementSelectionBtn.' </span>';
					
				}
				$display .= '</td>';
		        
		        //comment 

				$display .= '<td class="tabletext" valign="top">';
					$display .= "<textarea class=\"textboxtext Master_Comment\" id=\"$commentTextareaID\" name=\"$commentTextareaName\" cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\" $limitTextAreaEvent>$commentTextareaValue</textarea>";
															
					$commentSelectionBtn = '';
					$commentSelectionBtn .= '<div><a id="remarkBtn_commentComment_'.$commentTextareaID.'" onclick=\'javascript:js_Select_Comment("commentComment", "'.$commentTextareaID.'")\' href="javascript:void(0)">';
					$commentSelectionBtn .= '<img name="posimg1" align="absMiddle" onmouseover="MM_swapImage(\'posimg1\',\'\',\'/images/2009a/icon_pre-set_on.gif\',1)" onmouseout="MM_swapImgRestore()" src="/images/2009a/icon_pre-set_off.gif" border="0"/></a></div>';
					$commentSelectionBtn .=  "<script language='javascript'>jImagePath=\"{$image_path}/{$LAYOUT_SKIN}\";\n</script>\n";
					$commentSelectionBtn .=  "<div id=\"listContent\" class='calendarlayer' style='display:none;z-index:100;'></div>";

					$display .= '<span id="commentCommentLayer" style="float:right;">' . $commentSelectionBtn.' </span>';
				
				$display .= '</td>';
				
				if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
				    for ($k=0; $k<$numOfTerm; $k++) {
				        $__yearTermId = $termAry[$k]['YearTermID'];
				        
				        $__performance = $termBasedPerformanceAssoAry[$_studentId][$__yearTermId]['Performance'];
				        $display .= "<td class=\"tabletext\" valign=\"top\">";
				        $display .= $linterface->GET_TEXTBOX('termBasedPerformanceTb_'.$_studentId.'_'.$__yearTermId, 'termBasedPerformanceAry['.$_studentId.']['.$__yearTermId.']', $__performance, $OtherClass='', $OtherPar=array());
				        $display .= "</td>";
				    }
				}
				
				
// Omas - case#Z42562
//				#####tkogss_student_enrolment_report cust#####	
//				if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
//					$display .= '<td>';
//						$display .= '<table border="0" cellpadding="3" cellspacing="0"><tr>';
//						$display .= '<td><input type="text" name="'.$ecaID.'" id="'.$ecaName.'" size="2" maxlength="10" value="'.$ecaValue.'"></td>';
//							$display .= '<td><input type="text" name="'.$ssID.'" id="'.$ssName.'" size="2" maxlength="10" value="'.$ssValue.'"></td>';
//							$display .= '<td><input type="text" name="'.$csID.'" id="'.$csName.'" size="2" maxlength="10" value="'.$csValue.'"></td>';
//						$display .= '</td>';
//						$display .= '<tr></table>';
//					$display .= '</td>';	
//				}								
								
				##### Merit Record #####
				if($libenroll->enableClubRecordMeritInfoRight() && $type!="activity" || $libenroll->enableActivityRecordMeritInfoRight() && $type=="activity"){		
					
					$thisStudentIdArr = $StudentArray[$j]['StudentID'];		
							
					if($type == 'activity'){
						$meritTypeInfoArr = $libenroll->Get_Merit_Record($thisStudentIdArr,$DataArr['EnrolEventID'],'',true);
					}
					else{
						$meritTypeInfoArr = $libenroll->Get_Merit_Record($thisStudentIdArr,$DataArr['EnrolGroupID']);						
					}
								
					$meritTypeAssocArr = BuildMultiKeyAssoc($meritTypeInfoArr,'MeritType');
					
													
					$meritType1Count =  $meritTypeAssocArr[1]['MeritCount'];
					$meritType1RecordID = $meritTypeAssocArr[1]['RecordID'];
					
					$meritType2Count =  $meritTypeAssocArr[2]['MeritCount'];
					$meritType2RecordID = $meritTypeAssocArr[2]['RecordID'];
					
					$meritType3Count =  $meritTypeAssocArr[3]['MeritCount'];
					$meritType3RecordID = $meritTypeAssocArr[3]['RecordID'];
					
					$meritType4Count =  $meritTypeAssocArr[4]['MeritCount'];
					$meritType4RecordID = $meritTypeAssocArr[4]['RecordID'];
					
					$meritType5Count =  $meritTypeAssocArr[5]['MeritCount'];
					$meritType5RecordID = $meritTypeAssocArr[5]['RecordID'];
					
					if (!$lstudentprofile->is_merit_disabled) {
						$display .= '<td>';	
							$display .= $linterface->GET_TEXTBOX("meritCount","meritCount[$thisStudentIdArr][1][CountValue]", $meritType1Count, '', array('style'=>'width:30px;'));	
							$display .= $linterface->GET_HIDDEN_INPUT("meritRecordID", "meritCount[$thisStudentIdArr][1][MeritRecordID]", $meritType1RecordID);				
						$display .= '</td>';
					}
					
					if (!$lstudentprofile->is_min_merit_disabled) {
						$display .= '<td>';	
							$display .= $linterface->GET_TEXTBOX("meritCount","meritCount[$thisStudentIdArr][2][CountValue]", $meritType2Count, '', array('style'=>'width:30px;'));								
							$display .= $linterface->GET_HIDDEN_INPUT("meritRecordID", "meritCount[$thisStudentIdArr][2][MeritRecordID]", $meritType2RecordID);
						$display .= '</td>';
					}
					
					if (!$lstudentprofile->is_maj_merit_disabled) {
						$display .= '<td>';	
							$display .= $linterface->GET_TEXTBOX('meritCount','meritCount['.$thisStudentIdArr.'][3][CountValue]', $meritType3Count, '', array('style'=>'width:30px;'));									
							$display .= $linterface->GET_HIDDEN_INPUT("meritRecordID", "meritCount[$thisStudentIdArr][3][MeritRecordID]", $meritType3RecordID);
						$display .= '</td>';
					}
					
					if (!$lstudentprofile->is_sup_merit_disabled) {
						$display .= '<td>';
							$display .= $linterface->GET_TEXTBOX('meritCount','meritCount['.$thisStudentIdArr.'][4][CountValue]', $meritType4Count, '', array('style'=>'width:30px;'));												
							$display .= $linterface->GET_HIDDEN_INPUT("meritRecordID", "meritCount[$thisStudentIdArr][4][MeritRecordID]", $meritType4RecordID);
						$display .= '</td>';
					}
					
					if (!$lstudentprofile->is_ult_merit_disabled) {
						$display .= '<td>';
							$display .= $linterface->GET_TEXTBOX('meritCount','meritCount['.$thisStudentIdArr.'][5][CountValue]', $meritType5Count, '', array('style'=>'width:30px;'));												
							$display .= $linterface->GET_HIDDEN_INPUT("meritRecordID", "meritCount[$thisStudentIdArr][5][MeritRecordID]", $meritType5RecordID);
						$display .= '</td>';
					}
				}
				
				
				$display .= '<tr>';

			} 	
		    $display .= '</table>';
			$display .= '<script type="text/javascript">';
			$display .= $script;
			$display .= '</script>';
			
			$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "document.form1.submit()")."&nbsp";
	        $display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'");
		} else {
			//$td_colspan = "6";
			$td_colspan = "100%";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		

################################Start of comment bank layer########################################
				
				#####Get comment preset words############
				$getPerformanceSQL= $libenroll->Get_Achievement_Comment_Sql('','',$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance']);
				$getPerformanceSQL.= "ORDER BY Comment";
				$performanceDataArr = $libenroll->ReturnResultSet($getPerformanceSQL);
				$displayPerformanceArr = BuildMultiKeyAssoc($performanceDataArr, 'CommentID', array('Comment'),1);
					
				$achievementDataSql = $libenroll->Get_Achievement_Comment_Sql('','',$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
				$achievementDataSql .= "ORDER BY Comment";
				$achievementCommentDataArray = $libenroll->ReturnResultSet($achievementDataSql);
				$displayAchievementArr = BuildMultiKeyAssoc($achievementCommentDataArray, 'CommentID', array('Comment'),1);
						
				$commentDataSql = $libenroll->Get_Achievement_Comment_Sql('','',$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment']);
				$commentDataSql .= "ORDER BY Comment";
				$commentCommentDataArray = $libenroll->ReturnResultSet($commentDataSql);
				$displayCommentArr = BuildMultiKeyAssoc($commentCommentDataArray, 'CommentID', array('Comment'),1);

				$remarksWidth = 200;

				$commentBankLayer = '';
				
				# 2013-04-17 Rita - Performance Comment Layer
				$thisRemarksType = 'PerformanceComment';
					$performanceCommentLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:'.$remarksWidth.'px;">'."\r\n";
						$performanceCommentLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
							$performanceCommentLayer .= '<tbody>'."\r\n";
								$performanceCommentLayer .= '<tr>'."\r\n";
									$performanceCommentLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
										$performanceCommentLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
									$performanceCommentLayer .= '</td>'."\r\n";
								$performanceCommentLayer .= '</tr>'."\r\n";
								$performanceCommentLayer .= '<tr>'."\r\n";
									$performanceCommentLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
										$performanceCommentLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										
											$performanceCommentLayer .= '<tbody>'."\n";
		
											if (empty($displayPerformanceArr)){
												$performanceCommentLayer .= '<tr>'."\n";
													$performanceCommentLayer .= '<td align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td>'."\n";
												$performanceCommentLayer .= '</tr>'."\n";
												
											}
											else{
												foreach ($displayPerformanceArr as $thisSelectionCommentID => $thisSelectionComment){
													$performanceCommentLayer .= '<tr>'."\n";
														$performanceCommentLayer .= '<td id="PerformanceComment_'.$thisSelectionCommentID.'" class="PerformanceComment" ><a onclick=\'js_Assign_Comment("PerformanceComment","'.$thisSelectionCommentID.'", "'.$performTextareaID.'")\' href="javascript:void(0)">'.nl2br($thisSelectionComment).'</a></td>'."\n";
													$performanceCommentLayer .= '</tr>'."\n";
												}
											}
											
											$performanceCommentLayer .= '</tbody>'."\n";
										$performanceCommentLayer .= '</table>'."\r\n";
									$performanceCommentLayer .= '</td>'."\r\n";
								$performanceCommentLayer .= '</tr>'."\r\n";
							$performanceCommentLayer .= '</tbody>'."\r\n";
						$performanceCommentLayer .= '</table>'."\r\n";
					$performanceCommentLayer .= '</div>'."\r\n";
					
					$commentBankLayer.= $performanceCommentLayer;
				
				
				# 2013-04-15 Rita	Achievement Comment Layer
					$thisRemarksType = 'AchievementComment';
					$achievementCommentLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:'.$remarksWidth.'px;">'."\r\n";
						$achievementCommentLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
							$achievementCommentLayer .= '<tbody>'."\r\n";
								$achievementCommentLayer .= '<tr>'."\r\n";
									$achievementCommentLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
										$achievementCommentLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
									$achievementCommentLayer .= '</td>'."\r\n";
								$achievementCommentLayer .= '</tr>'."\r\n";
								$achievementCommentLayer .= '<tr>'."\r\n";
									$achievementCommentLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
										$achievementCommentLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										
											$achievementCommentLayer .= '<tbody>'."\n";
											
											
											if (empty($displayAchievementArr)){
												$achievementCommentLayer .= '<tr>'."\n";
													$achievementCommentLayer .= '<td align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td>'."\n";
												$achievementCommentLayer .= '</tr>'."\n";
												
											}
											else{
												foreach ($displayAchievementArr as $thisSelectionCommentID => $thisSelectionComment){
													$achievementCommentLayer .= '<tr>'."\n";
														$achievementCommentLayer .= '<td id="AchievementComment_'.$thisSelectionCommentID.'" class="AchievementComment"><a onclick=\'js_Assign_Comment("AchievementComment","'.$thisSelectionCommentID.'", "'.$achievementTextareaID.'")\' href="javascript:void(0)">'.nl2br($thisSelectionComment).'</a></td>'."\n";
													$achievementCommentLayer .= '</tr>'."\n";
						
												}
											}
											$achievementCommentLayer .= '</tbody>'."\n";
										$achievementCommentLayer .= '</table>'."\r\n";
									$achievementCommentLayer .= '</td>'."\r\n";
								$achievementCommentLayer .= '</tr>'."\r\n";
							$achievementCommentLayer .= '</tbody>'."\r\n";
						$achievementCommentLayer .= '</table>'."\r\n";
					$achievementCommentLayer .= '</div>'."\r\n";
										
					$commentBankLayer.= $achievementCommentLayer;
					
					
					
					# Comment Layer
				$thisRemarksType = 'commentComment';
					$commentCommentLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:'.$remarksWidth.'px;">'."\r\n";
						$commentCommentLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
							$commentCommentLayer .= '<tbody>'."\r\n";
								$commentCommentLayer .= '<tr>'."\r\n";
									$commentCommentLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
										$commentCommentLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
									$commentCommentLayer .= '</td>'."\r\n";
								$commentCommentLayer .= '</tr>'."\r\n";
								$commentCommentLayer .= '<tr>'."\r\n";
									$commentCommentLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
										$commentCommentLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										
											$commentCommentLayer .= '<tbody>'."\n";
											if (empty($displayCommentArr)){
												$commentCommentLayer .= '<tr>'."\n";
													$commentCommentLayer .= '<td align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td>'."\n";
												$commentCommentLayer .= '</tr>'."\n";
												
											}
											else{
												foreach ($displayCommentArr as $thisSelectionCommentID => $thisSelectionComment){
													
													$commentCommentLayer .= '<tr>'."\n";
														$commentCommentLayer .= '<td id="commentComment_'.$thisSelectionCommentID.'" class="commentComment" ><a onclick=\'js_Assign_Comment("commentComment","'.$thisSelectionCommentID.'", "'.$commentTextareaID.'")\' href="javascript:void(0)">'.nl2br($thisSelectionComment).'</a></td>'."\n";
													$commentCommentLayer .= '</tr>'."\n";
						
												}
											}
											$commentCommentLayer .= '</tbody>'."\n";
										$commentCommentLayer .= '</table>'."\r\n";
									$commentCommentLayer .= '</td>'."\r\n";
								$commentCommentLayer .= '</tr>'."\r\n";
							$commentCommentLayer .= '</tbody>'."\r\n";
						$commentCommentLayer .= '</table>'."\r\n";
					$commentCommentLayer .= '</div>'."\r\n";
					
					$commentBankLayer.= $commentCommentLayer;		
		
################################End of comment bank layer########################################

//		if (isset($StudentSize) && $StudentSize > 0) {
//		    for($j=0; $j<$StudentSize; $j++){
//		
//				$performTextareaID = "performID".$j;
//				$achievementTextareaID = "achievementID".$j;
//				$commentTextareaID = "commentID".$j;
//				
//				# 2013-04-17 Rita - Performance Comment Layer
//				$thisRemarksType = 'PerformanceComment_'.$performTextareaID;
//					$performanceCommentLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:'.$remarksWidth.'px;">'."\r\n";
//						$performanceCommentLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
//							$performanceCommentLayer .= '<tbody>'."\r\n";
//								$performanceCommentLayer .= '<tr>'."\r\n";
//									$performanceCommentLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
//										$performanceCommentLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
//									$performanceCommentLayer .= '</td>'."\r\n";
//								$performanceCommentLayer .= '</tr>'."\r\n";
//								$performanceCommentLayer .= '<tr>'."\r\n";
//									$performanceCommentLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
//										$performanceCommentLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
//										
//											$performanceCommentLayer .= '<tbody>'."\n";
//		
//											foreach ($displayPerformanceArr as $thisSelectionCommentID => $thisSelectionComment){
//												
//												$performanceCommentLayer .= '<tr>'."\n";
//												$performanceCommentLayer .= '<td id="PerformanceComment_'.$thisSelectionCommentID.'" class="PerformanceComment" ><a onclick=\'js_Assign_Comment("PerformanceComment","'.$thisSelectionCommentID.'", "'.$performTextareaID.'")\' href="javascript:void(0)">'.nl2br($thisSelectionComment).'</a></td>'."\n";
//												$performanceCommentLayer .= '</tr>'."\n";
//					
//											}
//											
//											$performanceCommentLayer .= '</tbody>'."\n";
//										$performanceCommentLayer .= '</table>'."\r\n";
//									$performanceCommentLayer .= '</td>'."\r\n";
//								$performanceCommentLayer .= '</tr>'."\r\n";
//							$performanceCommentLayer .= '</tbody>'."\r\n";
//						$performanceCommentLayer .= '</table>'."\r\n";
//					$performanceCommentLayer .= '</div>'."\r\n";
//					
//					$commentBankLayer.= $performanceCommentLayer;
//				
//				
//				# 2013-04-15 Rita	Achievement Comment Layer
//				if($libenroll->enableAchievementCommentBank()){
//					$thisRemarksType = 'AchievementComment_'.$achievementTextareaID;
//					$achievementCommentLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:'.$remarksWidth.'px;">'."\r\n";
//						$achievementCommentLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
//							$achievementCommentLayer .= '<tbody>'."\r\n";
//								$achievementCommentLayer .= '<tr>'."\r\n";
//									$achievementCommentLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
//										$achievementCommentLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
//									$achievementCommentLayer .= '</td>'."\r\n";
//								$achievementCommentLayer .= '</tr>'."\r\n";
//								$achievementCommentLayer .= '<tr>'."\r\n";
//									$achievementCommentLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
//										$achievementCommentLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
//										
//											$achievementCommentLayer .= '<tbody>'."\n";
//		
//											foreach ($displayAchievementArr as $thisSelectionCommentID => $thisSelectionComment){
//												$achievementCommentLayer .= '<tr>'."\n";
//												$achievementCommentLayer .= '<td id="AchievementComment_'.$thisSelectionCommentID.'" class="AchievementComment"><a onclick=\'js_Assign_Comment("AchievementComment","'.$thisSelectionCommentID.'", "'.$achievementTextareaID.'")\' href="javascript:void(0)">'.nl2br($thisSelectionComment).'</a></td>'."\n";
//												$achievementCommentLayer .= '</tr>'."\n";
//					
//											}
//											
//											$achievementCommentLayer .= '</tbody>'."\n";
//										$achievementCommentLayer .= '</table>'."\r\n";
//									$achievementCommentLayer .= '</td>'."\r\n";
//								$achievementCommentLayer .= '</tr>'."\r\n";
//							$achievementCommentLayer .= '</tbody>'."\r\n";
//						$achievementCommentLayer .= '</table>'."\r\n";
//					$achievementCommentLayer .= '</div>'."\r\n";
//					
//					$commentBankLayer.= $achievementCommentLayer;
//					
//					
//					
//					
//					# Comment Layer
//				$thisRemarksType = 'commentComment_'.$commentTextareaID;
//					$commentCommentLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:'.$remarksWidth.'px;">'."\r\n";
//						$commentCommentLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
//							$commentCommentLayer .= '<tbody>'."\r\n";
//								$commentCommentLayer .= '<tr>'."\r\n";
//									$commentCommentLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
//										$commentCommentLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
//									$commentCommentLayer .= '</td>'."\r\n";
//								$commentCommentLayer .= '</tr>'."\r\n";
//								$commentCommentLayer .= '<tr>'."\r\n";
//									$commentCommentLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
//										$commentCommentLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
//										
//											$commentCommentLayer .= '<tbody>'."\n";
//		
//											foreach ($displayCommentArr as $thisSelectionCommentID => $thisSelectionComment){
//												
//												$commentCommentLayer .= '<tr>'."\n";
//												$commentCommentLayer .= '<td id="commentComment_'.$thisSelectionCommentID.'" class="commentComment" ><a onclick=\'js_Assign_Comment("commentComment","'.$thisSelectionCommentID.'", "'.$commentTextareaID.'")\' href="javascript:void(0)">'.nl2br($thisSelectionComment).'</a></td>'."\n";
//												$commentCommentLayer .= '</tr>'."\n";
//					
//											}
//											
//											$commentCommentLayer .= '</tbody>'."\n";
//										$commentCommentLayer .= '</table>'."\r\n";
//									$commentCommentLayer .= '</td>'."\r\n";
//								$commentCommentLayer .= '</tr>'."\r\n";
//							$commentCommentLayer .= '</tbody>'."\r\n";
//						$commentCommentLayer .= '</table>'."\r\n";
//					$commentCommentLayer .= '</div>'."\r\n";
//					
//					$commentBankLayer.= $commentCommentLayer;
//				}
//			}
//		}	
			

		$linterface->LAYOUT_START();
		
		?>
		
		<script type="text/javascript">	
			function limitText(limitField, limitNum) {
				if (limitField.value.length > limitNum) {
					limitField.value = limitField.value.substring(0, limitNum);
				}
			}
						
			// 2013-04-15 Rita
			function getRemarkDivIdByRemarkType(remarkType) {
				return 'remarkDiv_' + remarkType;
			}
			
			function js_Select_Comment(remarkType, targetId){
									
				var remarkBtnId = 'remarkBtn_' + remarkType + '_' + targetId;
				var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
				//var remarkDivId = getRemarkDivIdByRemarkType(remarkType + '_' + targetId );
		
				$('input#lastClickedElementId').val(targetId);
				
				if(remarkType == 'commentComment'){
					var leftAdjustment = -<?=$remarksWidth?> - $('#' + remarkBtnId).width();
				}
				else{
					var leftAdjustment = $('#' + remarkBtnId).width();
				}
				
				var topAdjustment = 0;
				
				changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
				hideAllRemarkLayer();
				MM_showHideLayers(remarkDivId, '', 'show');
				
				
				// Check If There Are Any Option Selected
				
			
			}
			function hideRemarkLayer(remarkType) {
				var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
				MM_showHideLayers(remarkDivId, '', 'hide');
			}
			
			function hideAllRemarkLayer() {
				$('div.selectbox_layer').each( function() {
					MM_showHideLayers($(this).attr('id'), '', 'hide');
				});
			}
			
			function js_Assign_Comment(type, commentId, textAreaId){							
				$.post(
					'ajax_reload_comment.php', 
					{
						commentId: commentId,
						type: type
					},									
					function(data) {		
						
						textAreaId = $('input#lastClickedElementId').val();
						
						//$('#' +textAreaId).val(data);					 
						$('#' +textAreaId).val($('#' +textAreaId).val() + data);
						
						$('#' +textAreaId).focus();
						$('#' +textAreaId).setCursorToTextEnd();
					}
				);				
			}
			
			function CheckAll(onClickID){
				$('.'+onClickID).each(function(){
					$(this).val($('#'+onClickID).val());
				});
			}


			function AssignAll(onClickID) {
				switch (onClickID) {
					case "Master_AvailDate":
						var availFrom = $("input[name='DataFrom']").val();
						var availTo = $("input[name='DataTo']").val();
						$('input.Master_AvailDate_From').val(availFrom);
						$('input.Master_AvailDate_To').val(availTo);
					break
				}
			}
			
			(function($){
			    $.fn.setCursorToTextEnd = function() {
			        $initialVal = this.val();
			        this.val($initialVal + ' ');
			        this.val($initialVal);
			    };
			})(jQuery);
				
		</script>

		<form name="form1" method="post" action="member_comment_perform_update.php">
			<br />
			<table width="100%" border="0" cellspacing="4" cellpadding="4">
				<tr><td>
					<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
				</td></tr>
			</table>

			<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr><td><?=$quotaDisplay?></td></tr>
				<tr><td height="5"></td></tr>
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			</table>
			
			<br />			
			
			<?=$display?>
			<?=$commentBankLayer?>
			
			<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr><td align="center" colspan="6">
					<div style="padding-top: 5px">
						<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
					</div>
				</td></tr>
			</table>
			
			<?
			for ($i=0; $i<sizeof($UID); $i++){
			?>
				<input type="hidden" name="UID<?=$i?>" id="UID<?=$i?>" value="<?=$UID[$i]?>">
			<?
			}	
			?>
			<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
			<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?=$EnrolGroupID?>">
			<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?=$EnrolEventID?>">
			<input type="hidden" name="type" id="type" value="<?=$type?>">
			<input type="hidden" name="StudentSize" id="StudentSize" value="<?=$StudentSize?>">
			<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
			
			<input type="hidden" name="lastClickedElementId" id="lastClickedElementId" value="" />

		</form>
		
		<?
		intranet_closedb();
		//$libenroll->db_show_debug_log_by_query_number(1);
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
