<?php
# using: rita

################## Change Log ##
#	Date: 	2012-12-19 Rita
#			Copy from eSurvey and use for activity survey form customization	
#
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#
#	Date:	2011-02-08	YatWoon
#			Wish List Item: Display "Unfill User List" even the survery is "Anonymous"
#
################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eSurvey']['ViewSurveyResult'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$answerID = $_GET['AnswerID'];
$RecordType = $_GET['RecordType'];

$libenroll = new libclubsenrol($AcademicYearID);
$userAnswerInfoArray = $libenroll->returnUserSurveyAnswerInfo($viewUserID="", $eEnrolRecordID="", $answerID);

if($RecordType=="A"){
	$eEnrolRecordID = $userAnswerInfoArray[0]['eEnrolRecordID'];
	//$answerID =  $userAnswerInfoArray[0]['AnswerID'];
	$enrollEventInfoArr = $libenroll->GET_EVENTINFO($eEnrolRecordID);	
}

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);
$lsurvey = new libsurvey($SurveyID);

$queString = $enrollEventInfoArr['Question'];

$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);

$poster = $lsurvey->returnPosterName();
$targetGroups = $lsurvey->returnTargetGroups();

if (sizeof($targetGroups)==0)
{
	$target = "$i_general_WholeSchool";
}
else
{
	$target = implode(", ",$targetGroups);
}

$answerResult = array();
$answerResult = $libenroll->returnUserSurveyAnswerInfo("", "", $answerID);
$answer =  $libenroll->returnAnswerInJS($answerResult);

$survey_description = nl2br(intranet_convertAllLinks($lsurvey->Description));
$survey_description = $survey_description ? $survey_description : "---";

?>

<br />

<table width=95% cellspacing=0 cellpadding=5 border=0>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$eEnrollment['add_activity']['act_name']?></td>
	<td><?=$enrollEventInfoArr['EventTitle']?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$eEnrollment['add_activity']['app_period']?></td>
	<td><?=$enrollEventInfoArr['ApplyStartTime'] . '<br />'. $Lang['General']['To'] . '<br />' . $enrollEventInfoArr['ApplyEndTime'] ?></td>
</tr>

<tr>
<td colspan=2>
	<? if ($answer!="") { ?>
		<hr>
		<B><?=$i_Survey_Overall?></B><br>
		
		<script language="javascript" src="/templates/forms/form_view.js"></script>
		
		<form name="ansForm" method="post" action="update.php">
		        <input type=hidden name="qStr" value="">
		        <input type=hidden name="aStr" value="">
		</form>
		
		<script language="Javascript">
		<?=returnFormStatWords()?>
				
		var myQue = "<?=$queString?>";
		var myAns = new Array();
		<?=$answer?>
		var DisplayQuestionNumber = '<?=$enrollEventInfoArr['DisplayQuestionNumber']?>';
		
		var stats= new Statistics();
		stats.qString = myQue;
		stats.answer = myAns;
		stats.analysisData();
		document.write(stats.getStats());
		</SCRIPT>
	<? }
	else
	{
		echo $i_Survey_NoSurveyForCriteria;
	}
	?>
	<hr>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_BTN($button_print, "button", "window.print();") ?>
		<?=$linterface->GET_BTN($button_close, "button", "window.close();") ?>
	</td>
</tr>
</table>


<?php

intranet_closedb();
$linterface->LAYOUT_STOP();
?>