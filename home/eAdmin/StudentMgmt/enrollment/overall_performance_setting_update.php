<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();	
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

/*
$lf = new libfilesystem();

$setting_file = "$intranet_root/file/clubenroll_setting_performance.txt";

$lines[0] = $overallSetting;

$updatedcontent = implode("\n",$lines);
$lf->file_write($updatedcontent,$setting_file);
*/

$lgs = new libgeneralsettings();
$SettingArr = array();
$SettingArr['EnableOverallPerformanceReport'] = $overallSetting;
$Success['EditSettings'] = $lgs->Save_General_Setting($libenroll->ModuleTitle, $SettingArr);



intranet_closedb();

header("Location: overall_performance_setting.php?msg=2");
?>