<?php
# using: 

/* 
 * Change Log
 *
 * 2019-10-28 Henry 
 *  - bug fix for Club PICs fail to set schedule of activities [Case#E174088]
 * 
 * 2019-02-15 Cameron 
 *  - show loading icon before get eBooking result in onloadThickBox() [case #H156591]
 * 
 * 2019-01-14 Cameron
 *  - add function checkIsDateValid(), call it before calling eBooking thickbox linking to avoid changing date time after processing booking
 *  - add $Lang['eEnrolment']['eBooking']['TimeChangedError'] to show error if activity time has changed after booking
 *  
 * 2018-07-26 Cameron
 *  - redirect to course_schedule page for HKPF
 * 
 * 2017-05-11 Villa #K108095 
 * -	add enrol-eBooking Related
 * 2017-04-27 (Villa):	
 * -	modified convertRowNumOfTr - add Location Related
 * 2015-09-16 (Omas):
 * -	Add js logic to exclude holiday - search jsHolidayArr 
 * 2014-07-22 (Bill):
 * - 	DirectToMember for new group/activity setting
 * 2014-04-09 (Carlos):
 * -	 $sys_custom['eEnrolment']['TWGHCYMA'] for TWGHs C Y MA MEMORIAL COLLEGE customization - added location booking function
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
$Semester = trim(stripslashes($_GET['Semester']));
$recordId = IntegerSafe($_GET['recordId']);
$recordType = $_GET['recordType'];

if ($sys_custom['project']['HKPF'] && $recordType == 'Activity') {
    header("location: course_schedule.php?AcademicYearID={$AcademicYearID}&recordId={$recordId}&recordType={$recordType}");
}

// for new group / activity setting
$NewGroup = $_GET['DirectToMemberGroup'];
$NewActivity = $_GET['DirectToMemberActivity'];

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$libenroll = new libclubsenrol($AcademicYearID);

if (isset($_GET['p'])) {
	// By $_GET method
	$lurlparahandler = new liburlparahandler(liburlparahandler::actionDecrypt, $_GET['p'], $enrolConfigAry['encryptionKey']);	
	$AcademicYearID = $lurlparahandler->returnParaValue('AcademicYearID');
	$Semester = $lurlparahandler->returnParaValue('Semester');
	$recordId = IntegerSafe($lurlparahandler->returnParaValue('recordId'));
	$recordType = $lurlparahandler->returnParaValue('recordType');
	$pic_view = $lurlparahandler->returnParaValue('pic_view');
	$returnMsgKey = $lurlparahandler->returnParaValue('returnMsgKey');
	$NewGroup = $lurlparahandler->returnParaValue('DirectToMemberGroup');

}

####################################################
########## Access Right Checking [Start] ###########
####################################################
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isPic = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $recordId, $recordType);

$canAccess = true;
if ($recordType == $enrolConfigAry['Club']) {
	if (!$plugin['eEnrollment'] && !$isEnrolAdmin && !$isEnrolMaster && !$isPic) {
		$canAccess = false;
	}
}
else if ($recordType == $enrolConfigAry['Activity']) {
	$activityInfoAry = $libenroll->GET_EVENTINFO($recordId);
	$activityEnrolGroupID = $activityInfoAry['EnrolGroupID'];
	$isActivityClubPic = $libenroll->IS_CLUB_PIC($activityEnrolGroupID);
	if (!$isEnrolAdmin && !$isEnrolMaster && !$isActivityClubPic && !$isPic) {
		$canAccess = false;
	}
	
	if($sys_custom['eEnrolment']['TWGHCYMA'] && $plugin['eBooking'] && in_array($activityInfoAry['SchoolActivity'],array(1,2))){
		$has_location_booking = true;
		$eventDatesAry = $libenroll->GET_ENROL_EVENT_DATE($recordId);
		$js_global_datetime_ary = 'var globalBookingTimesAry = [';
		$delim = '';
		$today = date("Y-m-d");
		for($i=0;$i<count($eventDatesAry);$i++){
			if(date("Y-m-d",strtotime($eventDatesAry[$i]['ActivityDateStart'])) >= $today){
				$js_global_datetime_ary.= $delim.'"'.date("Y-m-d,H:i:s",strtotime($eventDatesAry[$i]['ActivityDateStart'])).date(",H:i:s",strtotime($eventDatesAry[$i]['ActivityDateEnd'])).'"';
				$delim = ',';
			}
		}
		$js_global_datetime_ary .= '];'."\n"; 
	}
}

if (!$canAccess) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
####################################################
########### Access Right Checking [End] ############
####################################################


####################################################
############# Get General Data [Start] #############
####################################################
$AcademicStart = date("Y-m-d", getStartOfAcademicYear('', $AcademicYearID));
$AcademicEnd   = date("Y-m-d", getEndOfAcademicYear('', $AcademicYearID));
####################################################
############## Get General Data [End] ##############
####################################################


####################################################
######### Get Club / Activity Info [Start] #########
####################################################
if ($recordType == $enrolConfigAry['Club']) {
	$recordInfoAry = $libenroll->GET_GROUPINFO($recordId);
	$h_recordTitle = $recordInfoAry['Title'];
	$h_recordTerm = $recordInfoAry['SemesterTitle'];
	
	$disableUpdateSettings = $libenroll->disableUpdate;
}
else if ($recordType == $enrolConfigAry['Activity']) {
	$recordInfoAry = $activityInfoAry;
	$h_recordTitle = $recordInfoAry['EventTitle'];
	
	$disableUpdateSettings = $libenroll->Event_DisableUpdate;
}




$timeDescription = $recordInfoAry['TimeDescription'];
if ($sys_custom['eEnrollment']['DisplayDefaultTimeDescription']) {
	$timeDescription = ($timeDescription == '')? $Lang['eEnrolment']['DefaultTimeDescription'] : $timeDescription;
}
####################################################
########## Get Club / Activity Info [End] ##########
####################################################



####################################################
############## Layout Display [Start] ##############
####################################################
if ($recordType == $enrolConfigAry['Club']) {
	$CurrentPage = "PageMgtClub";
	$backPage = "group.php";
}
else if ($recordType == $enrolConfigAry['Activity']) {
	$CurrentPage = "PageMgtActivity";
	$backPage = "event.php";
}
$CurrentPageArr['eEnrolment'] = 1;

$cancelBtnPara = "AcademicYearID=$AcademicYearID&Semester=$Semester";
$picView_HiddenField = '';
	
if ($LibUser->isStudent() || $LibUser->isParent())  {
	if ($isPic) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
		
		$cancelBtnPara = "&pic_view=1";
		$picView_HiddenField = "<input type='hidden' name='pic_view' value='1' />";
	}
}

### top menu
$tab_type = strtolower($recordType);
$current_tab = 1;
include_once("management_tabs.php");

### left-hand menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### Navigation
$PAGE_NAVIGATION[] = array($eEnrollment['enrolment_settings'], "");
$PAGE_NAVIGATION[] = array($EnrollGroupArr['Title'], "");
$h_pageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$h_schedulingDatesNavigation = $linterface->GET_NAVIGATION2($eEnrollment['schedule_settings']['scheduling_dates']);
$h_schedulingTimesNavigation = $linterface->GET_NAVIGATION2($eEnrollment['schedule_settings']['scheduling_times']);
$h_timesDescriptionNavigation = $linterface->GET_NAVIGATION2($Lang['eEnrolment']['TimeDescription']);

### Instruction
$h_instruction = '';
if ($libenroll->AllowToEditPreviousYearData) {
	$h_instruction .= $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $eEnrollment['schedule_settings']['instruction']);
}

### Add Meeting Date Type
$h_dateTypeSelection = '';
$h_dateTypeSelection .= $linterface->Get_Radio_Button("dateType_".$enrolConfigAry['MeetingDateType']['Single'], "dateType", $enrolConfigAry['MeetingDateType']['Single'], true, $Class="dateType", $Lang['eEnrolment']['DateTypeArr']['Single'], "changedDateType()");
$h_dateTypeSelection .= '&nbsp;';
$h_dateTypeSelection .= $linterface->Get_Radio_Button("dateType_".$enrolConfigAry['MeetingDateType']['Periodic'], "dateType", $enrolConfigAry['MeetingDateType']['Periodic'], false, $Class="dateType", $Lang['eEnrolment']['DateTypeArr']['Periodic'], "changedDateType()");

### Time Selection for "Add by Single Date"
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalHourStart', 'hour', 0);
$h_singleDate_globalTimeSeletion .= ' : '; 
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalMinStart', 'min', 0, $others_tab='', $interval=5);
$h_singleDate_globalTimeSeletion .= ' '.$eEnrollment['to'].' ';
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalHourEnd', 'hour', 0);
$h_singleDate_globalTimeSeletion .= ' : ';
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalMinEnd', 'min', 0, $others_tab='', $interval=5);
$h_singleDate_globalTimeSeletion .= $linterface->Get_Form_Warning_Msg('singleDate_global_invalidTimeRangeWarningDiv', $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');

### Date Time Action Button
$h_actionBtn = '';
if ($libenroll->AllowToEditPreviousYearData) {
	$BtnArr = array();
	$BtnArr[] = array('delete', 'javascript:deleteDate();');
	$h_actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);
}

### Submit & Cancel Button
if ($libenroll->AllowToEditPreviousYearData) {
	$h_submitBtn = $linterface->GET_ACTION_BTN($button_save, "button", ($disableUpdateSettings)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkForm();");
}
if($libenroll->CheckEnableEnroleBooking()){
	$h_cancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "ClearBookingRecord()");
}else{
	$h_cancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='".$backPage."?".$cancelBtnPara."'");
}



### School & Public Holiday
$StartDate = getStartDateOfAcademicYear($AcademicYearID);
$EndDate = getEndDateOfAcademicYear($AcademicYearID);
$sql = "SELECT 
			DATE_FORMAT(EventDate,'%Y-%m-%d')
		FROM 
	        INTRANET_EVENT
		where 
			EventDate BETWEEN '$StartDate' AND '$EndDate' and RecordType IN (3,4)";
$holidayArr = $libenroll->returnVector($sql);

### eBooking Related
$eBookingBtn = '<a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);">'.$Lang['eEnrolment']['eBooking']['ClickThickBox'].'</a>';
echo $linterface->Include_Thickbox_JS_CSS();
	
	
####################################################
############### Layout Display [End] ###############
####################################################
?>
<?php if($has_location_booking){ ?>
<link href="/templates/jquery/thickbox.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript"  language="JavaScript" src="/templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" language="JavaScript" src="/templates/jquery/thickbox.js"></script>
<? } ?>
<script language="javascript">
var jsHolidayArr = new Array('<?=implode("','",$holidayArr)?>');

var date = new Date();
var curYear;
var curMonth;

$(document).ready( function() {
	changedDateType();
	reloadDateTimeTable();
	
	//eBooking
	<?php if($libenroll->CheckEnableEnroleBooking()):?>
		$('a#dynSizeThickboxLink').click( function() {
			if (checkIsDateValid()) {
				$('td#bookingError').css('display','none');
				load_dyn_size_thickbox_ip('<?=$Lang['eEnrolment']['eBooking']['eBookingRecordDetail']?>', 'onloadThickBox();');
			}
		});
	<?php endif; ?>
});
//eBooking Start
<?php if($libenroll->CheckEnableEnroleBooking()){?>
function onloadThickBox() {
	var numOfRow = getMaxRowNum();
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	$('#TB_ajaxContent').html(loadingImg);
		
	$.ajax({
		url: "ajax_ebooking_load_thickbox.php/",
		data: $('#form1').serialize(),
		type: "POST",
		success:
			function(ReturnData) {
			$('#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			isLoading = false;
		}
	});
}

function RequestBooking(){
	$('#submit2').attr('disabled','disabled');
	$.ajax({
		url: "ajax_ebooking_update_record.php/",
		data:  $('#eBookingForm').serialize(),
		type: "POST",
		async:false,
		success:
			function(ReturnData){
				self.parent.tb_remove();
				$('#HiiddenDiv').append(ReturnData);
			}
	});
}
function ClearBookingRecord(){
	$.ajax({
		url: "ajax_ebooking_delete_record.php/",
		data:  $('#form1').serialize(),
		type: "POST",
		async:false,
		success:
			function(ReturnData){
				self.location=<?="'".$backPage."?".$cancelBtnPara."'"?>;
			}
	});
}
function DeleteBookingRecord(bookingID, BookingDetailKey){
	var reomveHiddenInput = document.getElementById('dateInfoAry_BookingID_'+BookingDetailKey);
	if(reomveHiddenInput){
		reomveHiddenInput.parentNode.removeChild(reomveHiddenInput);
	}
	if(confirm("<?=$Lang['eEnrolment']['eBooking']['DeleBookingRecord']?>")){
		$.ajax({
			url: "ajax_ebooking_delete_record.php/",
			data:  { "BookingID" : bookingID},
			type: "POST",
			async:false,
			success:
				function(ReturnData){
					$('a#dynSizeThickboxLink').click();
				}
		});
	}else{
		//Do nth
	}
}
<?php } ?>
//eBooking End

function PasteToAllLocation(){
	var value = $('#globalLocation').val();
	var numOfRow = getMaxRowNum()+1;
	for (var i=1; i<numOfRow ; i++){
		$('#Location_'+i).val(value);
	}
}

function getRecordType() {
	return $('input#recordType').val();
}

function getRecordId() {
	return $('input#recordId').val();
}

function getMaxRowNum() {
	var maxRowNum = 0;
	$('tr.dateTimeTr').each( function() {
		var _rowNum = parseInt($(this).attr('id').replace('dateTimeTr_', ''));
		if (_rowNum > maxRowNum) {
			maxRowNum = _rowNum;
		}
	});
	
	return maxRowNum;
}

function getStartDateWanringMsg() {
	return '<?=str_replace('<!--targetDate-->', $AcademicStart, $Lang['eEnrolment']['Warning']['StartDateInvalid'])?>';
}

function getEndDateWanringMsg() {
	return '<?=str_replace('<!--targetDate-->', $AcademicEnd, $Lang['eEnrolment']['Warning']['EndDateInvalid'])?>';
}

function resetCurYearMonth() {
	curYear = date.getFullYear();
	curMonth = date.getMonth() + 1;
}

function changedDateType() {
	resetCurYearMonth();
	
	if ($('input#dateType_' + '<?=$enrolConfigAry['MeetingDateType']['Single']?>').attr('checked')) {
		$('div#singleDateGlobalTimeSelDiv').show();
		reloadDateTable();
	}
	else if ($('input#dateType_' + '<?=$enrolConfigAry['MeetingDateType']['Periodic']?>').attr('checked')) {
		$('div#singleDateGlobalTimeSelDiv').hide();
		reloadPeriodicDateSelectionTable();
	}
}

function changeMonth(prevnext) {
	var newMonth = parseInt(curMonth) + prevnext;
	
	if(newMonth==13) {
		curMonth = 1;
		curYear += 1;
	}
	else if(newMonth==0) {
		curMonth = 12;
		curYear -= 1;
	}
	else {
		curMonth = newMonth;
	}
	reloadDateTable();
}

function addSingleDate(dateText) {
	var canAddDate = true;
	if (!compareTimeByObjId('', 'singleDate_globalHourStart', 'singleDate_globalMinStart', '', '', 'singleDate_globalHourEnd', 'singleDate_globalMinEnd', '')) {
		$('div#singleDate_global_invalidTimeRangeWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#singleDate_global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canAddDate) {
		var defaultStartHour = $('select#singleDate_globalHourStart').val();
		var defaultStartMin = $('select#singleDate_globalMinStart').val();
		var defaultEndHour = $('select#singleDate_globalHourEnd').val();
		var defaultEndMin = $('select#singleDate_globalMinEnd').val();
		
		addDateRowToTable(dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
	}
}

function addPeriodicDate() {
	var startDate = $('input#StartDate').val();
	var endDate = $('input#EndDate').val();
	var canAddDate = true;
	
	// check if the date range is valid
	if (startDate=='' || endDate=='' || startDate > endDate) {
		$('div#invalidDateRangeWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#invalidDateRangeWarningDiv').hide();
	}
	
	// check if the date is within academic year
	if (startDate < '<?=$AcademicStart?>') {
		$('div#invalidStartDateWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#invalidStartDateWarningDiv').hide();
	}
	if (endDate > '<?=$AcademicEnd?>') {
		$('div#invalidEndDateWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#invalidEndDateWarningDiv').hide();
	}
	
	// check if selected any weekday
	if($('#rcChoice_p').attr('checked')){
		if ($('input.weekdayChk:checked').length==0) {
			$('div#selectWeekdayWarningDiv').show();
			canAddDate = false;
		}
		else {
			$('div#selectWeekdayWarningDiv').hide();
		}
	} else {
		var selected = 0;
		$('input[name="cycleday_text[]"]').each(function(){
			if($(this).attr('checked')) selected++;
		});
		if(selected==0) canAddDate = false;
	}
	
	if (!compareTimeByObjId('', 'globalPeriodicHourStart', 'globalPeriodicMinStart', '', '', 'globalPeriodicHourEnd', 'globalPeriodicMinEnd', '')) {
		$('div#periodicDate_global_invalidTimeRangeWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#periodicDate_global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canAddDate) {
		var defaultStartHour = $('select#globalPeriodicHourStart').val();
		var defaultStartMin = $('select#globalPeriodicMinStart').val();
		var defaultEndHour = $('select#globalPeriodicHourEnd').val();
		var defaultEndMin = $('select#globalPeriodicMinEnd').val();
		
		var startDatePiece = startDate.split('-');
		var startYear = startDatePiece[0];
		var startMonth = startDatePiece[1] - 1;
		var startDay = startDatePiece[2];
		
		// Get the date which fulfill the requirements
		var dateObj = new Date(startYear, startMonth, startDay);
		var currentDate = getDateTextByDateObject(dateObj);
		var isExcludeHoiday = $('#excludeHoliday').attr('checked');
		var targetDateAry = new Array();

		if($('#rcChoice_p').attr('checked')){
    		while (currentDate <= endDate) {
    			var _day = dateObj.getDay();
    // 			console.log(_day);
    			if ($('input#WeekDay' + _day).attr('checked')) {
    				var thisdate = getDateTextByDateObject(dateObj);
    				if(isExcludeHoiday ){
    					if($.inArray(thisdate,jsHolidayArr)>-1){
    						// do nth
    					}
    					else{
    						targetDateAry.push(thisdate);	
    					}
    				}
    				else{
    					targetDateAry.push(thisdate);
    				}
    			}
    			
    			dateObj.setDate(dateObj.getDate() + 1);
    			currentDate = getDateTextByDateObject(dateObj);
    		}
    		var numOfDate = targetDateAry.length;
    		var i;
    		for (i=0; i<numOfDate; i++) {
    			var _dateText = targetDateAry[i];
    			addDateRowToTable(_dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
    		}
		} else {
			var targetCycleDay = "";
			$('input[name="cycleday_text[]"]').each(function(){
				if($(this).attr('checked')){
					targetCycleDay += $(this).val() + ",";
				}
			});
			$.ajax({
				method: 'POST',
				url: 'ajax_retriveCycleDayTextByDateRange.php',
				data: {
					action: "dummy",
					StartDate: $('#StartDate').val(),
					EndDate: $('#EndDate').val(),
					targetCycleDays: targetCycleDay
				},
				success: function(responseText){
					//console.log(responseText);
					var spAry = responseText.split(',');
					//console.log(spAry);
					for(var x in spAry){
						if(x!='in_array'){
							var _dateText = spAry[x];
							if(isExcludeHoiday){
		    					if($.inArray(_dateText,jsHolidayArr)>-1){
		    						// do nth
		    					}
		    					else{
		    						addDateRowToTable(_dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
		    					}
		    				} else {
		    					addDateRowToTable(_dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
		    				}
						}
					}
				}
			});
		}
	}
}

function addDateRowToTable(dateText, startHour, startMin, endHour, endMin) {
	// find the corresponding row to be inserted
	var insertRowNum = 0;
	$('tr.dateTimeTr').each( function() {
		var _rowNum = parseInt($(this).attr('id').replace('dateTimeTr_', ''));
		var _dateText = $('input#dateTextHidden_' + _rowNum).val();
		
		if (insertRowNum == 0 && dateText < _dateText) { 
			insertRowNum = _rowNum;
			return false;	// break
		}
	});
	// if not inserted in the middle of the table => insert at the bottom of the table 
	var maxRowNum = getMaxRowNum();
	if (insertRowNum == 0) {
		insertRowNum = maxRowNum + 1;
	}
	
	// shift the rowNum of the affected row first
	var i;
	for (i=maxRowNum; i>=insertRowNum; i--) {
		var _oldRowNum = i;
		var _newRowNum = i + 1;
		
		convertRowNumOfTr(_oldRowNum, _newRowNum);
	}
	
	// insert the new row to the table
	var trTemplate = htmlspecialchars_decode($('textarea#dateTimeTrTemplate').html());
	trTemplate = trTemplate.replace(/<?=$enrolConfigAry['MeetingDateNewRowNumTempCode']?>/g, insertRowNum).replace(/<?=$enrolConfigAry['MeetingDateNewRowNumTempDateStart']?>/g, dateText);
	
	if (insertRowNum == 1) {
		$('table#dateTimeSettingTable > tbody').prepend(trTemplate);
	}
	else {
		var targetRowNum = insertRowNum - 1;
		$('tr#dateTimeTr_' + targetRowNum).after(trTemplate);
	}
	
	// preset the value of the inserted row
	$('select#hourStart_' + insertRowNum).val(startHour);
	$('select#minStart_' + insertRowNum).val(startMin);
	$('select#hourEnd_' + insertRowNum).val(endHour);
	$('select#minEnd_' + insertRowNum).val(endMin);
	
	// hide the no data display row
	$('tr#noRecordTr').hide();
}

function getDateTextByDateObject(dateObj) {
	return dateObj.getFullYear() + '-' + str_pad((dateObj.getMonth() + 1), 2) + '-' + str_pad(dateObj.getDate(), 2);;
}

function convertRowNumOfTr(fromRowNum, toRowNum) {
	$('input#dateRecordId_' + fromRowNum).attr('id', 'dateRecordId_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][dateRecordId]');
	$('tr#dateTimeTr_' + fromRowNum).attr('id', 'dateTimeTr_' + toRowNum);
	$('div#rowNumDiv_' + fromRowNum).attr('id', 'rowNumDiv_' + toRowNum).html(toRowNum);
	$('input#dateTextHidden_' + fromRowNum).attr('id', 'dateTextHidden_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][dateText]');
	$('select#hourStart_' + fromRowNum).attr('id', 'hourStart_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][hourStart]');
	$('select#minStart_' + fromRowNum).attr('id', 'minStart_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][minStart]');
	$('select#hourEnd_' + fromRowNum).attr('id', 'hourEnd_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][hourEnd]');
	$('select#minEnd_' + fromRowNum).attr('id', 'minEnd_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][minEnd]');
	$('input#dateChk_' + fromRowNum).attr('id', 'dateChk_' + toRowNum).attr('name', 'dateChk_' + toRowNum);
	<? if($sys_custom['eEnrolment']['EnableLocation_tlgcCust']){ ?>
		$('input#Location_' + fromRowNum).attr('id', 'Location_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][Location]');
	<? } ?>
	//ebooking
	$('#dateInfoAry_BookingID_'+fromRowNum).attr('id','dateInfoAry_BookingID_'+fromRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][BookingID]');
}

function deleteDate() {
	if ($('input.dateChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else if (confirm('<?=$Lang['eEnrolment']['jsWarning']['DeleteMeetingDate']?>')) {
		$('input.dateChk:checked').each( function() {
			var _rowNum = parseInt($(this).attr('id').replace('dateChk_', ''));
			$('tr#dateTimeTr_' + _rowNum).remove();
			$('input#dateRecordId_' + _rowNum).remove();
		});
		
		var rowCounter = 1;
		$('tr.dateTimeTr').each( function() {
			var _rowNum = parseInt($(this).attr('id').replace('dateTimeTr_', ''));
			convertRowNumOfTr(_rowNum, rowCounter++);
		});
	}
	
	// show the no data display row if deleted all items
	if ($('tr.dateTimeTr').length == 0) {
		$('tr#noRecordTr').show();
	}
}

function applyTimeToDate(selectedDateOnly) {
	var canApply = true;
	
	if (!compareTimeByObjId('', 'globalHourStart', 'globalMinStart', '', '', 'globalHourEnd', 'globalMinEnd', '')) {
		$('div#global_invalidTimeRangeWarningDiv').show();
		canApply = false;
	}
	else {
		$('div#global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canApply) {
		var selectedPara = '';
		if (selectedDateOnly == 1) {
			selectedPara = ':checked';
		}
		
		var targetStartHour = $('select#globalHourStart').val();
		var targetStartMin = $('select#globalMinStart').val();
		var targetEndHour = $('select#globalHourEnd').val();
		var targetEndMin = $('select#globalMinEnd').val();
		
		$('input.dateChk' + selectedPara).each( function() {
			var _rowNum = parseInt($(this).attr('id').replace('dateChk_', ''));
			
			$('select#hourStart_' + _rowNum).val(targetStartHour);
			$('select#minStart_' + _rowNum).val(targetStartMin);
			$('select#hourEnd_' + _rowNum).val(targetEndHour);
			$('select#minEnd_' + _rowNum).val(targetEndMin);
		});
	}
}

function reloadDateTable() {
	//$("div#meetingDateCalendarDiv").html(getAjaxLoadingMsg());
	Block_Element('meetingDateCalendarDiv', getAjaxLoadingMsg(false));
	
	$.post(
		"ajax_reload.php",
		{
			Action: 'getMeetingDate_dateSelectionCalendar',
			recordType: getRecordType(),
			recordId: getRecordId(),
			year: curYear,
			month: curMonth
		},
		function (data) {
			$("div#meetingDateCalendarDiv").html(data);
			
			UnBlock_Element('meetingDateCalendarDiv');
		}
	);
}

function reloadPeriodicDateSelectionTable() {
	Block_Element('meetingDateCalendarDiv', getAjaxLoadingMsg(false));
	
	$.post(
		"ajax_reload.php",
		{
			Action: 'getMeetingDate_periodicDateSelectionTable'
		},
		function (data) {
			$('div#meetingDateCalendarDiv').html(data);
			$('input#StartDate').val('<?=$AcademicStart?>');
			$('input#EndDate').val('<?=$AcademicEnd?>');
			$('span#periodicDate_academicYearStartDateSpan').html(getStartDateWanringMsg());
			$('span#periodicDate_academicYearEndDateSpan').html(getEndDateWanringMsg());
			$('div#invalidStartDateWarningDiv').html($('div#invalidStartDateWarningDiv').html().replace('<!--targetDate-->', '<?=$AcademicStart?>'));
			$('div#invalidEndDateWarningDiv').html($('div#invalidEndDateWarningDiv').html().replace('<!--targetDate-->', '<?=$AcademicEnd?>'));
			
			UnBlock_Element('meetingDateCalendarDiv');
		}
	);
}

function reloadDateTimeTable() {
	$.post(
		"ajax_reload.php",
		{
			Action: 'getMeetingDate_dateTimeMgmtTable',
			recordType: getRecordType(),
			recordId: getRecordId()
		},
		function (data) {
			$("div#meetingDateTimeDiv").html(data);
		}
	);
}

function checkIsDateValid()
{
	var isValid = true;
	
	// hide all warning message
	$('div.warningDiv').hide();
	
	// check time range valid and overlapping
	var maxRowNum = getMaxRowNum();
	var i, j;
	
	for (i=1; i<=maxRowNum; i++) {
		var _rowNum1 = i;
		var _dateText1 = $('input#dateTextHidden_' + _rowNum1).val();
		
		var _hourStart1 = 'hourStart_' + _rowNum1;
		var _minStart1 = 'minStart_' + _rowNum1;
		var _hourEnd1 = 'hourEnd_' + _rowNum1;
		var _minEnd1 = 'minEnd_' + _rowNum1;
		
		if (!compareTimeByObjId(_dateText1, _hourStart1, _minStart1, '', _dateText1, _hourEnd1, _minEnd1, '')) {
			$('div#invalidTimeRangeWarningDiv_' + _rowNum1).show();
			isValid = false;
		}
		
		for (j=i+1; j<=maxRowNum; j++) {
			var __rowNum2 = j;
			var __dateText2 = $('input#dateTextHidden_' + __rowNum2).val();
				
			if (_dateText1 == __dateText2) {
				var __hourStart2 = 'hourStart_' + __rowNum2;
				var __minStart2 = 'minStart_' + __rowNum2;
				var __hourEnd2 = 'hourEnd_' + __rowNum2;
				var __minEnd2 = 'minEnd_' + __rowNum2;
				
				if (checkTimeRangeOverlappedByObjId(_dateText1, _hourStart1, _minStart1, '', _dateText1, _hourEnd1, _minEnd1, '', __dateText2, __hourStart2, __minStart2, '', __dateText2, __hourEnd2, __minEnd2, '')) {
					$('div#timeOverlapWarningDiv_' + _rowNum1).show();
					$('div#timeOverlapWarningDiv_' + __rowNum2).show();
					isValid = false;
				}
			}
		}
	}
	return isValid;
}

function checkForm() {
	var canSubmit = checkIsDateValid();

<?php if($has_location_booking){ ?>
	var booking_datetime_modified = false;
	if(canSubmit && $("#hiddenBookingIDs").val()!='' && globalBookingTimesAry.length > 0){
		var tmpDatetimeAry = GetBookingDateTimeAry(true);
		if(tmpDatetimeAry.length == 0){
			canSubmit = false;
			booking_datetime_modified = true;
		}
		if(globalBookingTimesAry.length != tmpDatetimeAry.length){
			canSubmit = false;
			booking_datetime_modified = true;
		}else{
			for(var i=0;i<globalBookingTimesAry.length;i++){
				if(globalBookingTimesAry[i] != tmpDatetimeAry[i]){
					canSubmit = false;
					booking_datetime_modified = true;
					break;
				}
			}
		}
	}
	if(booking_datetime_modified){
		js_OnSubmit_Check_Booking();
	}
<?php } ?>
	
	<? if($NewGroup){?>
		if(confirm("<?=$Lang['eEnrolment']['DirectToMemberSetting']?>"))
		{
			$('#DirectToMemberSettingGroup').val(1);
		} 
	<? } else if($NewActivity){?>
		if(confirm("<?=$Lang['eEnrolment']['DirectToMemberSetting']?>"))
		{
			$('#DirectToMemberSettingActivity').val(1);
		} 
	<? } ?>
	if (canSubmit) {
		<?php if($libenroll->CheckEnableEnroleBooking()):?>		
		$.ajax({			
			type: 'POST',
			url: 'ajax_check_eenrolment_ebooking_time_mismatch.php',
			data: $('form#form1').serialize(),
			dataType: 'json',
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('form#form1').attr('action', 'meeting_date_update.php').submit();			
				}
				else {
					$('td#bookingError').css('display','');	
				}
			}
		});
<?php else:?>
		$('form#form1').attr('action', 'meeting_date_update.php').submit();
<?php endif;?>		
	}
}

function checkRCChoice(){
	if($('#rcChoice_p').attr('checked')){
		$('#periodTr').show();
		$('#cycleTr').hide();
	} else {
		//$('#rcChoice_c').attr('checked')
		$.ajax({
			method: 'POST',
			url: 'ajax_retriveCycleDayTextByDateRange.php',
			data: {
				StartDate: $('#StartDate').val(),
				EndDate: $('#EndDate').val()
			},
			success: function(responseText){
				$('#cycleTr td:nth-child(2)').html(responseText);
			}
		});
		$('#cycleTr').show();
		$('#periodTr').hide();
	}
}

<?php if($has_location_booking){ ?>
//var globalBookingTimesAry = []; // YYYY-MM-DD,hh:mm:ss,hh:mm:ss 
<?=$js_global_datetime_ary?>

function js_OnSubmit_Check_Booking()
{
	var datetimeAry = GetBookingDateTimeAry(true);
	
	$.post(
		"ajax_check_location_booking.php",
		{
			"FacilityID" : $("#hiddenBookingLocationID").val(),
			"TargetDatetimes[]" : datetimeAry,
			"strBookingIDs" : $("#hiddenBookingIDs").val(),
			"AcademicYearID" : $("#AcademicYearID").val(),
			"EnrolEventID" : $("#recordId").val() 
		},
		function(responseText){
			if(responseText != ""){
				$.post(
					"ajax_delete_booking_record.php",
					{
						"strBookingIDs" : $("#hiddenBookingIDs").val(),
						"deleteAll" : 1
					},
					function(returnResult)
					{
						js_Show_BookingRequest_Layer();
						//initThickBox();
						alert('<?=$Lang['eEnrolment']['ReserveBookingTimeChangedWarning']?>');
					}
				);
			}else{
				$('form#form1').attr('action', 'meeting_date_update.php').submit();
			}
		}
	);
}

function js_Show_BookingRequest_Layer()
{
	var tr_objs = GetBookingDateTimeAry(true);
	
	if(tr_objs.length == 0){
		alert('<?=$Lang['eEnrolment']['ReserveLocationNoDateWarning']?>');
		return;
	}
	
	js_Show_ThickBox('<?=$Lang['eEnrolment']['ReserveActivityLocation']?>', 500, 800, '');
	var location_id = $('#hiddenBookingLocationID').val();
	$.post(
		"ajax_show_booking_request_layer.php",
		{
			 "FacilityID": location_id  
		},
		function(responseText){
			$('#TB_ajaxContent').html(responseText);
			if(location_id != '' && location_id > 0){
				ShowBookingDetails(location_id);
			}
		}
	);
}

function ShowBookingDetails(val)
{
	var datetimeAry = GetBookingDateTimeAry(true);
	
	$.post(
		"ajax_check_location_available.php",
		{
			"FacilityID" : val,
			"TargetDatetimes[]" : datetimeAry 
		},
		function(responseText){
			$("#div_booking_details").html(responseText);
			$("#div_booking_details").show();
		}
	);
}

function GetBookingDateTimeAry(skipPastDays)
{
	var today = getToday();
	var tr_objs = $('.dateTimeTr');
	var datetimeAry = [];
	
	for(var i=0;i<tr_objs.length;i++){
		var tr_obj = tr_objs.get(i);
		var id_parts = tr_obj.id.split('_');
		var id_num = id_parts[1];
		
		var date = $('input#dateTextHidden_'+id_num).val();
		//if(skipPastDays && date < today) continue;
		var start_hour = $('select#hourStart_'+id_num).val();
		var start_min = $('select#minStart_'+id_num).val();
		var end_hour = $('select#hourEnd_'+id_num).val();
		var end_min = $('select#minEnd_'+id_num).val();
		var start_time = (start_hour<10? '0'+start_hour : ''+start_hour) + ':' + (start_min<10?'0'+start_min:''+start_min) + ':00';
		var end_time = (end_hour<10? '0'+end_hour : ''+end_hour) + ':' + (end_min<10?'0'+end_min:''+end_min) + ':00';
		
		datetimeAry.push(date + ',' + start_time + ',' + end_time);
	}
	
	return datetimeAry;
}

function js_Show_Item_Booking_Details(jsItemID, jsClickedObjID)
{
	MM_showHideLayers('ItemBookingDetailsLayer'+jsItemID,'','show');
	if($('#ItemBookingDetailsLayer'+jsItemID).is(':visible')){
		$('#ItemBookingDetailsLayer'+jsItemID).hide();
	}else{
		$('#ItemBookingDetailsLayer'+jsItemID).show();
	}
}

function js_Submit_Booking() 
{
	globalBookingTimesAry = GetBookingDateTimeAry(true);
	var cancel_booking_item = $('input[name="CancelDayBookingIfOneItemIsNA"]:checked').val();
	var item_id_ary = [];
	if($('input[name="SingleItemID\\[\\]"]').length>0){
		var checked_items = $('input[name="SingleItemID\\[\\]"]:checked');
		for(var i=0;i<checked_items.length;i++){
			item_id_ary.push(checked_items.get(i).value);
		}
	}
	$.post(
		"ajax_submit_booking_request.php",
		{
			"FacilityID" : $("#FacilityID").val(),
			"TargetDatetimes[]": globalBookingTimesAry,
			"CancelDayBookingIfOneItemIsNA" : cancel_booking_item,
			"SingleItemID[]" : item_id_ary 
		},
		function(responseText)
		{
			if(responseText != "")
			{
				var arrResult = responseText.split("||");
				
				$("#hiddenBookingLocationID").val($("#FacilityID").val());
				$("#hiddenBookingIDs").val(arrResult[1]);
				$("#div_location").hide();
				$("#div_selected_location").html(arrResult[0]);
				$("#div_selected_location").show();
				window.top.tb_remove();
				//initThickBox();
			}
		}
	);
}

function js_Change_Selected_Location()
{
	var current_BookingIDs = $("#hiddenBookingIDs").val();
	
	$.post(
		"ajax_delete_booking_record.php",
		{
			"strBookingIDs" : current_BookingIDs,
			"deleteAll" : 1  
		},
		function(responseText)
		{
			js_Show_BookingRequest_Layer();
			//initThickBox();
		}
	);
}

function js_Cancel_Selected_Location()
{
	if (confirm('<?=$Lang['eEnrolment']['ConfirmClearBookingMsg'] ?>')) {
		globalBookingTimesAry.splice(0,globalBookingTimesAry.length);
		var current_BookingIDs = $("#hiddenBookingIDs").val();
		
		$.post(
			"ajax_delete_booking_record.php",
			{
				"strBookingIDs" : current_BookingIDs,
				"deleteAll" : 1
			},
			function(responseText)
			{
				globalBookingTimesAry = GetBookingDateTimeAry(true);
				$("#hiddenBookingLocationID").val("");
				$("#hiddenBookingIDs").val("");
				$("#location").val("");
				$("#div_location").show();
				$("#div_selected_location").html("");
				$("#div_selected_location").hide();
				//initThickBox();
			}
		);
	}
}
<?php } ?>
</script>
<br />
<form id="form1" name="form1" action="" method="post">
	<table width="98%" border="0" cellpadding"0" cellspacing="0" align="center">
		<tr>
			<td align="left" class="navigation"><?=$h_pageNavigation?></td>
		</tr>
	</table>
	
	<?=$h_instruction?>
	<br style="clear:both;" />
	
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$eEnrollment['club_name']?></td>
			<td><?=$h_recordTitle?></td>
		</tr>
		<? if ($recordType == $enrolConfigAry['Club']) { ?>
			<tr>
				<td class="field_title"><?=$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term']?></td>
				<td><?=$h_recordTerm?></td>
			</tr>
		<? } ?>
	</table>
	<br />
	
	<? if ($libenroll->AllowToEditPreviousYearData) { ?>
	<div style="float:left"><?=$h_schedulingDatesNavigation?></div>
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['eEnrolment']['DateType']?></td>
			<td><?=$h_dateTypeSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['General']['Date']?></td>
			<td>
				<div id="singleDateGlobalTimeSelDiv"><?=$h_singleDate_globalTimeSeletion?></div>
				<div>
					<table border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td style="padding:5px; background-color:#dbedff" valign="top">
								<div id='meetingDateCalendarDiv'></div>											
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<br style="clear:both;" />
	<? } ?>
	
	<div style="float:left"><?=$h_schedulingTimesNavigation?></div>
	<br style="clear:both;" />
	<?=$h_actionBtn?>
	<div id="meetingDateTimeDiv"></div>
	<br style="clear:both;" />
	
<?php
if($has_location_booking){
	//$activityLocationBookingIds = $libenroll->Get_Activity_Location_Booking_Records($recordId);
	include_once($PATH_WRT_ROOT."includes/libinventory.php");
	include_once($PATH_WRT_ROOT."includes/libebooking.php");	// for loading of constant
	$linventory = new libinventory();
	
	$cnt_rejected = 0;
	$cnt_pending = 0;
	$cnt_approved = 0;
	
	$libenroll->Remove_Non_Existing_Activity_Location_Booking_Record($recordId);
	/*
	$sql = "SELECT e.BookingID FROM INTRANET_EBOOKING_ENROL_EVENT_RELATION as r LEFT JOIN INTRANET_EBOOKING_RECORD as e ON e.BookingID=r.BookingID AND e.BookingStatus <> 0 WHERE r.EnrolEventID='$recordId' AND e.BookingStatus <> 0";
	$existingBookingIdAry = $libenroll->returnVector($sql);
	
	if(count($existingBookingIdAry)==0){
		// the booking records are deleted at eBooking
		$sql = "DELETE FROM INTRANET_EBOOKING_ENROL_EVENT_RELATION WHERE EnrolEventID='$recordId'";
		$libenroll->db_db_query($sql);
		$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET BookingLocationID=NULL, ActivityInLocation=NULL WHERE EnrolEventID='$recordId'";
		$libenroll->db_db_query($sql);
	}
	*/
	$sql = "SELECT 
				event_booking.EnrolEventID,
				CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.")."), 
				room_booking.BookingStatus  
			FROM 
				INVENTORY_LOCATION_BUILDING AS building 
				INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
				LEFT OUTER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
				LEFT OUTER JOIN INTRANET_EBOOKING_ENROL_EVENT_RELATION AS event_booking ON (room_booking.BookingID = event_booking.BookingID)
			WHERE 
				event_booking.EnrolEventID IN ($recordId)";
	$arrBookingResult = $libenroll->returnArray($sql);
	
	if(sizeof($arrBookingResult)>0)
	{
		list($temp_event_id, $location_name, $booking_result) = $arrBookingResult[0];
		if($booking_result == -1)
		{
			//$cnt_rejected++;
			global $image_path, $LAYOUT_SKIN;
			$result_img = "<img border='0' src='".$image_path."/".$LAYOUT_SKIN."/icon_reject_l'>".$Lang['eBooking']['iCal']['FieldTitle']['Rejected'];
		}
		else if($booking_result == 0 || $booking_result == 999)
		{
			//$cnt_pending++;
			global $image_path, $LAYOUT_SKIN;
			$result_img = "<img border='0' src='".$image_path."/".$LAYOUT_SKIN."/icon_waiting.gif'>".$Lang['eBooking']['iCal']['FieldTitle']['Pending'];
		}
		else if($booking_result == 1)
		{
			//$cnt_approved++;
			global $image_path, $LAYOUT_SKIN;
			$result_img = "<img border='0' src='".$image_path."/".$LAYOUT_SKIN."/icon_approve.gif'>".$Lang['eBooking']['iCal']['FieldTitle']['Approved'];
		}
		
		//$edit_link = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
		//$edit_link .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
		//$edit_link .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
		$edit_link = "<span id='DIV_eBookingLocation'>".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Change_Selected_Location(); return false;", "FakeLayer", $Lang['Btn']['Edit'])."</span>";
		
		$return_string = $location_name." [ ".$edit_link." | <a href='#' onClick='javascript:js_Cancel_Selected_Location()'>".$Lang['Btn']['Clear']."</a> ]<br>".$result_img;
		
		$return_content = "<span id='div_location' style='display:none;'>";
		//$return_content .= $lui->Get_Input_Text("location", $location, " class=\"textbox\" maxlength=\"255\" style=\"width:60%\" ");
		$return_content .= Get_String_Display('');
		
		//$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
		//$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
		//$return_content .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
		//$return_content .= " <span id='DIV_eBookingLocation'> [ ".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Show_BookingRequest_Layer(); return false;", "FakeLayer", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'])." ]</span>";
		$return_content .= " <span id='DIV_eBookingLocation'> [ <a href=\"javascript:void(0);\" title=\"".$Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking']."\" onclick=\"js_Show_BookingRequest_Layer(); return false;\">".$Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking']."</a> ]</span>";
		
		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_ENROL_EVENT_RELATION WHERE EnrolEventID = '$recordId'";
		$arrHiddenBookingIDs = $libenroll->returnVector($sql);
		$hiddenBookingIDs = implode(",",$arrHiddenBookingIDs);
		
		$return_content .= "<input type='hidden' id='hiddenBookingIDs' name='hiddenBookingIDs' value='$hiddenBookingIDs'>";
		$return_content .= "<input type='hidden' id='hiddenBookingLocationID' name='hiddenBookingLocationID' value='".$activityInfoAry['BookingLocationID']."'>";
		$return_content .= "</span>";

		$return_content .= "<span id='div_selected_location'>";
		$return_content .= $return_string;
		$return_content .= "</span>";
	}
	else
	{
		$return_content = "<span id='div_location'>";
		//$return_content .= $lui->Get_Input_Text("location", $location, " class=\"textbox\" maxlength=\"255\" style=\"width:60%\" ");
		$return_content .= Get_String_Display('');
		
		//$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
		//$return_content .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
		//$return_content .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
		//$return_content .= " <span id='DIV_eBookingLocation'> [ ".$linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Show_BookingRequest_Layer(); return false;", "FakeLayer", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'])." ]</span>";
		$return_content .= " <span id='DIV_eBookingLocation'> [ <a href=\"javascript:void(0);\" title=\"".$Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking']."\" onclick=\"js_Show_BookingRequest_Layer(); return false;\">".$Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking']."</a> ]</span>";
		$return_content .= "<input type='hidden' id='hiddenBookingIDs' name='hiddenBookingIDs' value=''>";
		$return_content .= "<input type='hidden' id='hiddenBookingLocationID' name='hiddenBookingLocationID' value='".$activityInfoAry['BookingLocationID']."'>";
		$return_content .= "</span>";

		$return_content .= "<span id='div_selected_location' style='display:none'>";
		$return_content .= "</span>";
	}
	$return_content;
	
	echo '<div style="float:left">'.$linterface->GET_NAVIGATION2($Lang['eEnrolment']['ReserveActivityLocation']).'</div>'."\n";
	echo '<br style="clear:both;" /><br>'."\n";
	echo '<table class="form_table_v30">
			<tr>
				<td class="field_title">'.$Lang['eEnrolment']['SchoolLocation'].'</td>
				<td>';
		echo $return_content;				
		echo '</td>
			</tr>
		</table>'."\n";
	echo '<br style="clear:both;" /><br>'."\n";
}
?>
	<div style="float:left"><?=$h_timesDescriptionNavigation?></div>
	<br style="clear:both;" />
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['AccountMgmt']['display']?></td>
			<td><?=$linterface->GET_TEXTAREA("TimeDescription", $timeDescription)?></td>
		</tr>
	</table>
	<?php if($libenroll->CheckEnableEnroleBooking()):?>
	<table>
		<tr>
			<td colspan="2"><?=$linterface->GET_NAVIGATION2($Lang['eEnrolment']['eBooking']['eBooking'])?></td>
		</tr>
		<tr>
			<td><?=$eBookingBtn?></td>
			<td id="bookingError" class="systemmsg" style="display:none;"><?php echo $Lang['eEnrolment']['eBooking']['TimeChangedError'];?></td>
		</tr>
	</table>
	<?php endif;?>
	
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<?=$h_submitBtn?>
		<?=$h_cancelBtn?>
	</div>
	<div id="HiiddenDiv"></div>
	<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="<?=$AcademicYearID?>" />
	<input type="hidden" id="Semester" name="Semester" value="<?=intranet_htmlspecialchars($Semester)?>" />
	<input type="hidden" id="recordType" name="recordType" value="<?=$recordType?>" />
	<input type="hidden" id="recordId" name="recordId" value="<?=$recordId?>" />
	<?=$picView_HiddenField?>
	<?if($NewGroup){?>
		<input type="hidden" name="DirectToMemberSettingGroup" id="DirectToMemberSettingGroup" />
	<? } else if($NewActivity){?>
		<input type="hidden" name="DirectToMemberSettingActivity" id="DirectToMemberSettingActivity" />
	<? } ?>
</form>
<?php
$linterface->LAYOUT_STOP();
?>