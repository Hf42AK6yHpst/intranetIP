<?php 
$PATH_WRT_ROOT = "../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol($AcademicYearID);
$lgs = new libgeneralsettings();


$settingName = $libenroll->Performance_Generated_Setting_Name($AcademicYearID, $settingId);
$SettingsArr = $lgs->Get_General_Setting("eEnrolment");

echo $SettingsArr["$settingName"];
?>