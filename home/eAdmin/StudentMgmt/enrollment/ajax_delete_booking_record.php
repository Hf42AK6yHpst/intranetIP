<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_opendb();

$li = new libclubsenrol($AcademicYearID);
//$li = new libdb();

$deleteAll = $_REQUEST['deleteAll'];

if ($deleteAll == 1) {
	$strBookingIDs = $_REQUEST['strBookingIDs'];
}
else {
	// delete iCal temp records only
	$sql = "Select BookingID From INTRANET_EBOOKING_RECORD Where BookingStatus = '".LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY."' And BookingID IN ($strBookingIDs)";
	$bookingAry = $li->returnResultSet($sql);
	$targetBookingIdAry = Get_Array_By_Key($bookingAry, 'BookingID');
	$strBookingIDs = implode(',', $targetBookingIdAry);
}

$result = $li->Delete_Activity_Location_Booking_Record(explode(",",$strBookingIDs));

echo $result? "1":"0";
/*

$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($strBookingIDs)";
echo $sql."<BR>";
$result['DeleteFromIntranet_Ebooking_Record'] = $li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($strBookingIDs)";
echo $sql."<BR>";
$result['DeleteFromIntranet_Ebooking_Room_Booking_Details'] = $li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_EBOOKING_ENROL_EVENT_RELATION WHERE BookingID IN ($strBookingIDs)";
echo $sql."<BR>";
$result['DeleteFromIntranet_Ebooking_eEnrolment_Event_Relation'] = $li->db_db_query($sql);
*/

intranet_closedb();
?>