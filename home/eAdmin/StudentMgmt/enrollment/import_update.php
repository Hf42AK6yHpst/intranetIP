<?php
//using by:Pun
#############################################
#
#	Date	2018-07-25 (Pun) [121822]
#			added NCS Cust
#
#	Date	2017-11-02 (Simon) [B115479]
#			add flag to import future event status
#			$sys_custom['eEnrolment']['EnableTakeAttendanceFutureDate']
#			add IS_ENROL_PIC checking
#
#	Date	2015-04-08 (Omas)
#			added field userlogin to import
#
#############################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment']) {
	
	$AcademicYearID = IntegerSafe($AcademicYearID);
	$GroupID = IntegerSafe($GroupID);
	$EnrolGroupID = IntegerSafe($EnrolGroupID);
	$EnrolEventID = IntegerSafe($EnrolEventID);
	
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	
	if ($libenroll->hasAccessRight($_SESSION['UserID'])) {
		
		include_once($PATH_WRT_ROOT."includes/libimporttext.php");
		include_once($PATH_WRT_ROOT."includes/libgroup.php");		
		$libgroup = new libgroup($GroupID);
		$limport = new libimporttext();
		
		$li = new libdb();
		$lu = new libuser();
		$filepath = $userfile;
		$filename = $userfile_name;
		
		if ($type=="clubAttendance")
			$parmeters = "EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type";
		else if ($type=="eventAttendance")
			$parmeters = "EnrolEventID=$EnrolEventID&type=$type";
			
		if($filepath=="none" || $filepath == "")
		{   # import failed
		    header("Location: import.php?$parmeters&Result=import_failed2");
		    exit();
		}
		else
		{
			if($limport->CHECK_FILE_EXT($filename)) {
				# read file into array
				# return 0 if fail, return csv array if success
				$data = $limport->GET_IMPORT_TXT($filepath);
				
				$result_ary = array();
				$imported_student = array();
				$imported_date = array();
				
				if ($type=="clubAttendance")
				{	
					### Attendance helper cannot import the date which has been saved by the admin already
					$CannotModifyGroupDateIDArr = array();
					# add IS_ENROL_PIC checking
					if (
							($libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolGroupID, "Club")) || 
							($libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club") && 
								$libenroll->enableTakeAttendanceFutureDate() &&
									$libenroll->disabledPicTakeAttendanceIfAdminTaken()
							)
						)
					{
						// check for each Date
						$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);
						$numOfDate = count($DateArr);
						for ($i=0; $i<$numOfDate; $i++)
						{
							$tempGroupDateID = $DateArr[$i]['GroupDateID'];
							
							// check if there are any attendance records in DB
							$sql = "SELECT DateModified, LastModifiedRole FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE EnrolGroupID = '$EnrolGroupID' AND GroupDateID = '$tempGroupDateID' ";
							$result = $libenroll->returnArray($sql,2);
							
							# no attendance record yet => enable checkboxes for edit
							# otherwise, do checking
							if (count($result) > 0)
							{
								if ($result[0]['LastModifiedRole'] == "A")
								{
									$CannotModifyGroupDateIDArr[] = $tempGroupDateID;
									
								}
								else
								{
									# disable checkboxes if last modified date is not today
									if (date("Y-m-d") != date("Y-m-d", strtotime($result[0]['DateModified'])))
									{
										$CannotModifyGroupDateIDArr[] = $tempGroupDateID;
									}
								} 
							}
						}
					}
					
					//loop through all students		
					$numOfData = count($data);					
					for ($i=1; $i<$numOfData; $i++)	//$i=1 means ignore the title row
					{
						
						/*
						 * $data[$i][0] = ClassName
						 * $data[$i][1] = ClassNumber
						 * $data[$i][2] = UserLogin
						 * others => "Absent" / "Present" / "Exempt"
						 */
						 
						if (empty($data[$i]))
						{
							$result_ary[$i] = "empty_row";
							continue;
						}
						
						$className = $data[$i][0];
						$classNumber = $data[$i][1];
						$userLogin = $data[$i][2];
						
						
						//check if the student is existing
						if($userLogin == ''){
							$studentID = $lu->returnUserID($className, $classNumber, 2, '0,1,2');
						}
						else{
							$thisStudentInfoArr = 	$libenroll->Get_StudentInfo_By_UserLogin($userLogin, $AcademicYearID);
							if(!empty($thisStudentInfoArr)){
								$className = $thisStudentInfoArr[0]['ClassName'];
								$classNumber = $thisStudentInfoArr[0]['ClassNumber'];
								$data[$i][0] = $className;
								$data[$i][1] = $classNumber;
								$studentID = $thisStudentInfoArr[0]['UserID'];
							}
						}						
						
						if(!$studentID)
						{
							$result_ary[$i] = "student_not_found";
							continue;
						}
											
						//check if the student is in the club
						$sql = "SELECT UserGroupID FROM INTRANET_USERGROUP WHERE EnrolGroupID = '$EnrolGroupID' AND UserID = '$studentID'";
						$result = $libgroup->returnArray($sql,1);
						
						if ($result[0][0] == 0)
						{
							//ignore the student if one is not a member of the club
							$result_ary[$i] = "student_not_member";
							continue;
						}
						
						// check attendance of each date
						$invalid_date = false;
						for ($j=3; $j<sizeof($data[0]); $j++)	//date starts at index=2 //=>Omas added field userLogin changed to 3 
						{
							//check if the input date is in the future
							$meetingDate = date("Y-m-d H:i:s", strtotime(substr($data[0][$j], 0 , 16)));
							$now = date("Y-m-d H:i:s");
							$meetingDateTime = strtotime($meetingDate);
							$nowDateTime = strtotime($now);

							# add flag to import future event status [B115479]
							if(!$libenroll->enableTakeAttendanceFutureDate()){
								if ($meetingDateTime > $nowDateTime)
								{
									//ignore the data of future event
									$imported_date[$i][$j] = false;
									continue;
								}
							}
							
							$DataArr['GroupID'] = $libenroll->GET_GROUPID($EnrolGroupID);
							
							//check if the student is present, if not, ignore the that date
							$inputData = strtolower($data[$i][$j]);
							$PresentString = "present";
							$AbsentString = "absent";
							$ExemptString = "exempt";
							if ($inputData == $PresentString || $inputData == $ExemptString ||$inputData == $AbsentString)
							{
								//get EnrolGroupID
								//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
		
								//check if there is activity on that date					 
								$sql = "SELECT GroupDateID FROM INTRANET_ENROL_GROUP_DATE WHERE EnrolGroupID = '$EnrolGroupID' AND ActivityDateStart = '".$meetingDate."' AND (RecordStatus IS NULL OR RecordStatus = 1)";
								$result = $libgroup->returnArray($sql,1);
								$thisGroupDateID = $result[0][0];
								
								if ($result==NULL)
								{
									//ignore the wrong date time
									$imported_date[$i][$j] = false;
									continue;
								}
								
								if (in_array($thisGroupDateID, $CannotModifyGroupDateIDArr))
								{
									//attendance has been saved by admin already / attendance taking time is over
									$imported_date[$i][$j] = false;
									continue;
								}
// 								debug_pr($thisGroupDateID);
// 								debug_pr($CannotModifyGroupDateIDArr);
								$DataArr['GroupDateID'] = $thisGroupDateID;
								$DataArr['StudentID'] = $studentID;
								$DataArr['EnrolGroupID'] = $EnrolGroupID;
								# add $DataArr['LastModifiedRole'] = 'A';
								if($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'], $EnrolGroupID, "Club") &&
										$libenroll->enableTakeAttendanceFutureDate() &&
										$libenroll->disabledPicTakeAttendanceIfAdminTaken())
								{
									$DataArr['StaffRole'] = 'A';
								}
								//debug_pr($DataArr['StaffRole']);
								//$DataArr['Status'] = $inputData == $PresentString?1:2;
								
								switch ($inputData) 
								{
								    case $PresentString:
								        $DataArr['Status'] = 1;
								        break;
								    case $ExemptString:
								        $DataArr['Status'] = 2;
								        break;
								    case $AbsentString:
								        $DataArr['Status'] = 3;
								        break;
								}
									
								//check if the record is existing or not
								$sql = "DELETE FROM INTRANET_ENROL_GROUP_ATTENDANCE
										WHERE GroupDateID = '".$DataArr['GroupDateID']."' AND StudentID = '".$DataArr['StudentID']."' AND EnrolGroupID = '".$DataArr['EnrolGroupID']."'";
								$result = $libgroup->db_db_query($sql,1);
	
//								if ($result!=NULL)	
//								{
//									//ignore existing record
//									## Remark: in frontend, the present record is unchanged if the record is existed
//									## So, in order not to confuse the user, set "true" here but ignore this record so that one less query is run
//									$imported_date[$i][$j] = true;
//									continue;
//								}

								if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
								    $DataArr['Remark'] = $data[$i][$j+1];
								}
								
								//update attendance
								//if($inputData != $AbsentString)
								//{
								//debug_pr($DataArr);
									$libenroll->ADD_STU_GROUP_ATTENDENCE($DataArr);
								//}
								
								$imported_date[$i][$j] = true;
							}
//							else if ($inputData == $AbsentString)
//							{
//								//get EnrolGroupID
//								//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
//		
//								//check if there is activity on that date					 
//								$sql = "SELECT GroupDateID FROM INTRANET_ENROL_GROUP_DATE WHERE EnrolGroupID = $EnrolGroupID AND ActivityDateStart = '".$meetingDate."' AND (RecordStatus IS NULL OR RecordStatus = 1)";
//								$result = $libgroup->returnArray($sql,1);
//
//								if ($result==NULL)
//								{
//									//ignore the wrong date time
//									$imported_date[$i][$j] = false;
//									continue;
//								}
//
//								//update attendance
//								$DataArr['GroupDateID'] = $result[0][0];
//								$DataArr['StudentID'] = $studentID;
//								$DataArr['EnrolGroupID'] = $EnrolGroupID;
//								
//								//check if the record is existing or not
//								$sql = "SELECT GroupAttendanceID FROM INTRANET_ENROL_GROUP_ATTENDANCE
//										WHERE GroupDateID = ".$DataArr['GroupDateID']." AND StudentID = ".$DataArr['StudentID']." AND EnrolGroupID = ".$DataArr['EnrolGroupID'];
//								$result = $libgroup->returnArray($sql,1);
//	
//								if ($result==NULL)	
//								{
//									//ignore if there is no record yet
//									## Remark: in frontend, the record is absent if there is no record
//									## So, in order not to confuse the user, set "true" here but ignore this record so that one less query is run
//									$imported_date[$i][$j] = true;	
//									continue;
//								}
//								
//								//delete the attendance record
//								$GroupAttendanceID = $result[0][0];
//								$sql = "DELETE FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE GroupAttendanceID = $GroupAttendanceID";
//								$libgroup->db_db_query($sql);
//								$imported_date[$i][$j] = true;
//							}
							else
							{
								$imported_date[$i][$j] = false;	
							}
						}
						
						array_push($imported_student, $studentID);
					}
// 					die();
				}
				else if ($type=="eventAttendance")
				{
					### Attendance helper cannot import the date which has been saved by the admin already
					$CannotModifyEventDateIDArr = array();
					if ($libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolEventID, "Event"))
					{
						$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID);
						$numOfData = count($DateArr);
						
						// check for each Date
						for ($i=0; $i<$numOfData; $i++)
						{
							$tempEventDateID = $DateArr[$i]['EventDateID'];
							
							// check if there are any attendance records in DB
							$sql = "SELECT DateModified, LastModifiedRole FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE EnrolEventID = '$EnrolEventID' AND EventDateID = '$tempEventDateID'";
							$result = $libenroll->returnArray($sql,2);
							
							# no attendance record yet => enable checkboxes for edit
							# otherwise, do checking
							if (count($result) > 0)
							{
								if ($result[0]['LastModifiedRole'] == "A")
								{
									$CannotModifyEventDateIDArr[] = $tempEventDateID;
								}
								else
								{
									# disable checkboxes if last modified date is not today
									if (date("Y-m-d") != date("Y-m-d", strtotime($result[0]['DateModified'])))
									{
										$CannotModifyEventDateIDArr[] = $tempEventDateID;
									}
								} 
							}
						}
					}
					
					//loop through all students
					$numOfData = count($data);								
					for ($i=1; $i<$numOfData; $i++)	//$i=1 means ignore the title row but not dropping it
					{
						
						/*
						 * $data[$i][0] = ClassName
						 * $data[$i][1] = ClassNumber
						 * $data[$i][2] = UserLogin
						 * others => "Absent" / "Present"
						 */
						 
						if (empty($data[$i]))
						{
							$result_ary[$i] = "empty_row";
							continue;
						}
						
						$className = $data[$i][0];
						$classNumber = $data[$i][1];
						$userLogin = $data[$i][2];
						
						//check if the student is existing
						if($userLogin == ''){
							$studentID = $lu->returnUserID($className, $classNumber, 2, '0,1,2');
						}
						else{
							$thisStudentInfoArr = 	$libenroll->Get_StudentInfo_By_UserLogin($userLogin, $AcademicYearID);
							if(!empty($thisStudentInfoArr)){
								$className = $thisStudentInfoArr[0]['ClassName'];
								$classNumber = $thisStudentInfoArr[0]['ClassNumber'];
								$data[$i][0] = $className;
								$data[$i][1] = $classNumber;
								$studentID = $thisStudentInfoArr[0]['UserID'];
							}
						}
												
						if($studentID==0)
						{
							$result_ary[$i] = "student_not_found";
							continue;
						}
											
						//check if the student has joined the activity
						$sql = "SELECT EventStudentID FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '$studentID'";
						$result = $libgroup->returnArray($sql,1);
						
						if ($result==NULL)
						{
							//ignore the student if one is not a member of the club
							$result_ary[$i] = "student_not_member";
							continue;
						}
						
						// check attendance of each date
						$invalid_date = false;
						for ($j=3; $j<sizeof($data[0]); $j++)	//date starts at index=2//=>Omas added userLogin changed to =3 
						{
							//check if the input date is in the future
							$meetingDate = date("Y-m-d H:i:s", strtotime(substr($data[0][$j], 0 , 16)));
							$now = date("Y-m-d H:i:s");
							$meetingDateTime = strtotime($meetingDate);
							$nowDateTime = strtotime($now);
							if ($meetingDateTime > $nowDateTime)
							{
								//ignore the data of future event
								$imported_date[$i][$j] = false;
								continue;
							}

							$inputData = strtolower($data[$i][$j]);
							$PresentString = "present";
							$AbsentString = "absent";
							$ExemptString = "exempt";
							//check if the student is present, if not, ignore the that date
							if ($inputData == $PresentString ||$inputData == $ExemptString ||$inputData == $AbsentString)
							{
								//check if there is activity on that date					 
								$sql = "SELECT EventDateID FROM INTRANET_ENROL_EVENT_DATE WHERE ActivityDateStart = '$meetingDate' AND EnrolEventID = '$EnrolEventID' AND (RecordStatus IS NULL OR RecordStatus = 1)";
								$result = $libgroup->returnArray($sql,1);
								$thisEventDateID = $result[0][0];

								if ($result==NULL)
								{
									//ignore the wrong date time
									$imported_date[$i][$j] = false;
									continue;
								}
								
								if (in_array($thisEventDateID, $CannotModifyEventDateIDArr))
								{
									//ignore the wrong date time
									$imported_date[$i][$j] = false;
									continue;
								}

								$DataArr['EventDateID'] = $thisEventDateID;
								$DataArr['StudentID'] = $studentID;
								$DataArr['EnrolEventID'] = $EnrolEventID;
								//$DataArr['Status'] = $inputData == $PresentString?1:2;
								
								switch ($inputData) 
								{
								    case $PresentString:
								        $DataArr['Status'] = 1;
								        break;
								    case $ExemptString:
								        $DataArr['Status'] = 2;
								        break;
								    case $AbsentString:
								        $DataArr['Status'] = 3;
								        break;
								}

								//delete existing
								$sql = "DELETE FROM INTRANET_ENROL_EVENT_ATTENDANCE
										WHERE EventDateID = '".$DataArr['EventDateID']."' AND StudentID = '".$DataArr['StudentID']."' AND EnrolEventID = '".$DataArr['EnrolEventID']."'";
								
								$result = $libgroup->db_db_query($sql);
	
//								if ($result!=NULL)	
//								{
//									//ignore existing record
//									## Remark: in frontend, the present record is unchanged if the record is existed
//									## So, in order not to confuse the user, set "true" here but ignore this record so that one less query is run
//									$imported_date[$i][$j] = true;
//									continue;
//								}

								if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
								    $DataArr['Remark'] = $data[$i][$j+1];
								}
								//update attendance
								//if($inputData != $AbsentString)
								//{
									$libenroll->ADD_STU_EVENT_ATTENDENCE($DataArr);
								//}
																
								$imported_date[$i][$j] = true;
							}
//							else if ($inputData == $AbsentString)
//							{
//								//check if there is activity on that date					 
//								$sql = "SELECT EventDateID FROM INTRANET_ENROL_EVENT_DATE WHERE ActivityDateStart = '$meetingDate' AND EnrolEventID = $EnrolEventID AND (RecordStatus IS NULL OR RecordStatus = 1)";
//								$result = $libgroup->returnArray($sql,1);
//								if ($result==NULL)
//								{
//									//ignore the wrong date time
//									$imported_date[$i][$j] = false;
//									continue;
//								}
//
//								//update attendance
//								$DataArr['EventDateID'] = $result[0][0];
//								$DataArr['StudentID'] = $studentID;
//								$DataArr['EnrolEventID'] = $EnrolEventID;
//								
//								//check if the record is existing or not
//								$sql = "SELECT EventAttendanceID FROM INTRANET_ENROL_EVENT_ATTENDANCE
//										WHERE EventDateID = ".$DataArr['EventDateID']." AND StudentID = ".$DataArr['StudentID']." AND EnrolEventID = ".$DataArr['EnrolEventID'];
//								$result = $libgroup->returnArray($sql,1);
//								if ($result==NULL)	
//								{
//									//ignore if there is no such record
//									## Remark: in frontend, the record is absent if there is no record => same as update to absent
//									## So, in order not to confuse the user, set "true" here but ignore this record so that one less query is run
//									$imported_date[$i][$j] = true;
//									continue;
//								}
//								
//								//delete the attendance record
//								$EventAttendanceID = $result[0][0];
//								$sql = "DELETE FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE EventAttendanceID = $EventAttendanceID";
//								$libgroup->db_db_query($sql);
//								$imported_date[$i][$j] = true;
//							}
							else
							{
								$imported_date[$i][$j] = false;
							}
						}
						array_push($imported_student, $studentID);
					}
				}
				else if($type=="clubPerformance")
				{			
					$toprow = array_shift($data);                   # drop the title bar
					
					for ($i=0; $i<sizeof($data); $i++) {
						
						if (empty($data[$i]))
						{
							$result_ary[$i] = "empty_row";
							continue;
						}
						
						list($className,$classNumber,$performance,$comment) = $data[$i];
						# type cast the ClassNumber to integer so any leading zerio is trimmed
						$classNumber = (int)$classNumber;
						$performance = addslashes(intranet_htmlspecialchars($performance));
						$comment = addslashes(intranet_htmlspecialchars($comment));
					
						//check if the student is existing first and get the UserID
						$studentID = $lu->returnUserID($className, $classNumber, 2, '0,1,2');
	
						if(!$studentID)
						{
							$result_ary[$i] = "student_not_found";
							continue;
						}
						
						//check if the student is in the club
						$sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID' AND UserID = '$studentID'";
						$result = $libgroup->returnArray($sql,1);
												
						if ($result[0][0] == 0)
						{
							//ignore the student if one is not a member of the club
							$result_ary[$i] = "student_not_member";
							continue;
						}
						
						//update performance and comment
						$sql = "UPDATE INTRANET_USERGROUP SET Performance = '$performance', CommentStudent = '$comment' WHERE UserID = '$studentID' AND GroupID = '$GroupID'";
						$li->db_db_query($sql);
						array_push($imported_student, $studentID);
					}			
				}
				else if($type=="eventPerformance")
				{			
					$toprow = array_shift($data);                   # drop the title bar
					
					for ($i=0; $i<sizeof($data); $i++) {
						
						if (empty($data[$i]))
						{
							$result_ary[$i] = "empty_row";
							continue;
						}
						
						list($className,$classNumber,$performance,$comment) = $data[$i];
						# type cast the ClassNumber to integer so any leading zerio is trimmed
						$classNumber = (int)$classNumber;
						$performance = addslashes(intranet_htmlspecialchars($performance));
						$comment = addslashes(intranet_htmlspecialchars($comment));
						
						//check if the student is existing first and get the UserID
						$studentID = $lu->returnUserID($className, $classNumber, 2, '0,1,2');
	
						if(!$studentID)
						{
							$result_ary[$i] = "student_not_found";
							continue;
						}
						
						//check if the student is approved in this activity
						$sql = "SELECT COUNT(EventStudentID) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '$studentID' AND RecordStatus = 2";
						$result = $libgroup->returnArray($sql,1);
												
						if ($result[0][0] == 0)
						{
							//ignore the student if one has not joined the activity
							$result_ary[$i] = "student_not_event_student";
							continue;
						}
						
						//update performance and comment
						$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET Performance = '$performance', CommentStudent = '$comment' WHERE StudentID = '$studentID' AND EnrolEventID = '$EnrolEventID'";
						$li->db_db_query($sql);
						array_push($imported_student, $studentID);
					}			
				}
		    }
		    else
		    {
		    	header("Location: import.php?$parmeters&Result=import_failed");
				exit();
			}
		}	
		intranet_closedb();
		
		?>
				
		<body>
		
		<form name="form1" method="post" action="import_result.php">
			<input type="hidden" name="type" value="<?=$type?>">
			<input type="hidden" name="GroupID" value="<?=$GroupID?>">
			<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
			<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
			<input type="hidden" name="data" value="<?=rawurlencode(serialize($data));?>">
			<input type="hidden" name="result_ary" value="<?=rawurlencode(serialize($result_ary));?>">
			<input type="hidden" name="imported_student" value="<?=rawurlencode(serialize($imported_student));?>">
			<input type="hidden" name="imported_date" value="<?=rawurlencode(serialize($imported_date));?>">
			<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
		</form>
		
		<script language="Javascript">
			document.form1.submit();
		</script>
		
		</body>
		
	<?		
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>