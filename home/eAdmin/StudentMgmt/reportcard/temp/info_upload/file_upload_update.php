<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once("../default_header.php");

$fs = new libfilesystem();
intranet_opendb();
$lreportcard = new libreportcard();

$TeachingClassName = $lreportcard->GET_TEACHING_CLASS();

//if (!(($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".str_replace('/', '', $TeachingClassName)."/", $file)))) {
$FileClassName = str_replace('.', '', str_replace('/', '', $TeachingClassName));
$URL = "index.php";


if (
	(($ck_ReportCard_UserType=="TEACHER")&&(preg_match("/".$FileClassName."/", $userfile_name)))
	|| ($ck_ReportCard_UserType=="ADMIN"))
	{
	if(!empty($userfile))
	{
		switch ($UploadType) {
				case 0:
					$folder_prefix = $intranet_root."/file/reportcard/summary";
					break;
				case 1:
					$folder_prefix = $intranet_root."/file/reportcard/award";
					break;
				case 2:
					$folder_prefix = $intranet_root."/file/reportcard/merit";
					break;
				case 3:
					$folder_prefix = $intranet_root."/file/reportcard/eca";
					break;
				case 4:
					$folder_prefix = $intranet_root."/file/reportcard/remark";
					break;
				case 5:
					$folder_prefix = $intranet_root."/file/reportcard/interschool";
					break;
				case 6:
					$folder_prefix = $intranet_root."/file/reportcard/schoolservice";
					break;
		}
		$fs->folder_new($folder_prefix);	
	
		$loc = $userfile;
		$filename = $userfile_name;
		$success = 0;
		
		
		if($filename!="")
		{			
			$ext = $fs->file_ext($filename);
			if($ext==".csv")
			{
				if ($loc!="none" && file_exists($loc))
				{
      		$data = $fs->file_read_csv($loc);
      
          $x = $UploadType;
          if($ReportCardTemplate == "")
            $y = 1;
          else
            $y = $ReportCardTemplate; 
      
          if(!empty($data))
          {
            $header_row = array_shift($data);
            $wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $DefaultHeaderArray[$x][$y]);
          }
          if($wrong_format)
          {
            $success = 0;
            $URL = "file_upload.php";
          }
					else if(strpos($filename, ".")!=0)
					{
					  $Class = str_replace(".", "", str_replace("/", "", $Class));
					  $filename = $Year."_".$Semester."_".$Class.substr($filename, strpos($filename, "."));
						$success = $fs->file_copy($loc, stripslashes($folder_prefix."/".$filename));
					}
				}
			}
		}
	}
	
	$Result = ($success==1) ? "upload" : ($wrong_format?"wrong_csv_header":"upload_failed");

	
} else {
	$Result = "upload_failed";
}

$URL .= "?Result=$Result&UploadType=$UploadType";

header("Location: $URL");
?>
