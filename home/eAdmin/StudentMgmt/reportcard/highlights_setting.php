<?php
$PATH_WRT_ROOT = "../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_Highlights";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        
        # Retrieve file content
		list($failed_style, $distinction_style, $FailColor, $DistinctionColor) = $lreportcard->GET_HIGHLIGHTS();
		
        # tag information
		$TAGS_OBJ[] = array($eReportCard['SchemeSettings_Highlights'], "", 0);
		
		$linterface->LAYOUT_START();
		
		?>
		
		<form name="form1" action="highlights_setting_update.php" method="post">
		<br />
		
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>

		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Fail']; ?> </td>
							<td width="75%">
								<table width="50%">
									<tr>
										<td class="tabletext"><input type="checkbox" name="failed_bold" value="1" id="failed_bold" <?=(is_array($failed_style) && in_array('bold', $failed_style) ? "CHECKED" : "")?> /><label for="failed_bold"><b><?=$eReportCard['Bold']?></b></label></td>
										<td class="tabletext"><input type="checkbox" name="failed_italic" value="1" id="failed_italic" <?=(is_array($failed_style) && in_array('italic', $failed_style) ? "CHECKED" : "")?> /><label for="failed_italic"><i><?=$eReportCard['Italic']?></i></label></td>
										<td class="tabletext"><input type="checkbox" name="failed_underline" value="1" id="failed_underline" <?=(is_array($failed_style) && in_array('underline', $failed_style) ? "CHECKED" : "")?> /><label for="failed_underline"><u><?=$eReportCard['Underline']?></u></label></td>
									</tr>
									<tr>
										<td class="tabletext" colspan="3">
											<input type="checkbox" name="failed_color" value="1" id="failed_color" <?=(is_array($failed_style) && in_array('color', $failed_style) ? "CHECKED" : "")?> />
											<label for="failed_color"><?=$eReportCard['Color']?></label>
											<select name="FailColor" onclick="js_Select_Checkbox('failed_color');">
												<option value="Red" <?=($FailColor == "Red" ? "SELECTED" : "")?>><?=$eReportCard['Red']?></option>
												<option value="Green" <?=($FailColor == "Green" ? "SELECTED" : "")?>><?=$eReportCard['Green']?></option>
												<option value="Blue" <?=($FailColor == "Blue" ? "SELECTED" : "")?>><?=$eReportCard['Blue']?></option>
											</select>
										</td>
									</tr>
								</table>
							</td>
							<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Distinction']; ?> </td>
							<td width="75%">
								<table width="50%">
									<tr>
										<td class="tabletext"><input type="checkbox" name="distinction_bold" value="1" id="distinction_bold" <?=(is_array($distinction_style) && in_array('bold', $distinction_style) ? "CHECKED" : "")?> /><label for="distinction_bold"><b><?=$eReportCard['Bold']?></b></label></td>
										<td class="tabletext"><input type="checkbox" name="distinction_italic" value="1" id="distinction_italic" <?=(is_array($distinction_style) && in_array('italic', $distinction_style) ? "CHECKED" : "")?> /><label for="distinction_italic"><i><?=$eReportCard['Italic']?></i></label></td>
										<td class="tabletext"><input type="checkbox" name="distinction_underline" value="1" id="distinction_underline" <?=(is_array($distinction_style) && in_array('underline', $distinction_style) ? "CHECKED" : "")?> /><label for="distinction_underline"><u><?=$eReportCard['Underline']?></u></label></td>
									</tr>
									<tr>
										<td class="tabletext" colspan="3">
											<input type="checkbox" name="distinction_color" value="1" id="distinction_color" <?=(is_array($distinction_style) && in_array('color', $distinction_style) ? "CHECKED" : "")?> />
											<label for="distinction_color"><?=$eReportCard['Color']?></label>
											<select name="DistinctionColor" onclick="js_Select_Checkbox('distinction_color');">
												<option value="Red" <?=($DistinctionColor == "Red" ? "SELECTED" : "")?>><?=$eReportCard['Red']?></option>
												<option value="Green" <?=($DistinctionColor == "Green" ? "SELECTED" : "")?>><?=$eReportCard['Green']?></option>
												<option value="Blue" <?=($DistinctionColor == "Blue" ? "SELECTED" : "")?>><?=$eReportCard['Blue']?></option>
											</select>
										</td>
									</tr>
								</table>
							</td>
							</tr>
							<!--
							<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['HighlightOverall']; ?> </td>
							<td width="75%">
								<input type="checkbox" name="HighlightOverall" id="HighlightOverall_1" value="1" /> <label for="HighlightOverall_1">$eReportCard['OverallResult']</label></td>
							</td>
						</tr>
						-->
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		</form>
		
		<script language="javascript">
			function js_Select_Checkbox(jsID)
			{
				$('input#' + jsID).attr('checked', 'checked');
			}
		</script>
	<?= $linterface->FOCUS_ON_LOAD("form1.failed_bold") ?>

	<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
