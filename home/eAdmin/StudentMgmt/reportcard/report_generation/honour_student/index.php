<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lf = new libfilesystem();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "PageReportGenerationHonourStudent";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		# Get collection period
		$Year = ($libreportcard->Year=="") ? date('Y') : $libreportcard->Year;
		
		# get report list
		$sql = "SELECT
					a.ReportID, 
					REPLACE(a.ReportTitle, '::', '<br />'),
					a.Description,
					date_format(a.LastGenerated, '%Y-%m-%d %H:%i')
				FROM 
					RC_REPORT_TEMPLATE as a
				WHERE 
					a.RecordStatus = 1
				";
		$ReportArray = $libreportcard->returnArray($sql, 4);

		if(!empty($ReportArray))
		{
			$ReportSelect = "<SELECT name='ReportID'>";
			for($i=0; $i<sizeof($ReportArray); $i++)
			{
				$ReportSelect .= "<OPTION value='".$ReportArray[$i][0]."' $Selected>".$ReportArray[$i][1]."</OPTION>";
			}
			$ReportSelect .= "</SELECT>";
		}

		# get semester list
		$semester_data = getSemesters();
		if(!empty($semester_data))
		{
			$SemesterSelect = "<SELECT name='Semester'>";
			for($i=0; $i<sizeof($semester_data); $i++)
			{
				$TmpArr = explode("::", $semester_data[$i]);
				$t_semester = $TmpArr[0];
				$Selected = (($libreportcard->Semester==$t_semester) || ($libreportcard->Semester=="" && $TmpArr[1]==1)) ? "SELECTED='SELECTED'" : "";
				$SemesterSelect .= "<OPTION value='".$t_semester."' $Selected>".$t_semester."</OPTION>";
			}
			$SemesterSelect .= "<OPTION value='FULL' ".($libreportcard->Semester=="FULL"?"SELECTED='SELECTED'" : "").">".$eReportCard['FullYear']."</OPTION>";
			$SemesterSelect .= "</SELECT>";
		}
		
		$MeritSelect = "<SELECT name='MeritSelect'>";
		for($i=0; $i<5; $i++)
		{
			$MeritSelect .= "<OPTION value='$i'>$i</OPTION>";
		}
		$MeritSelect .= "</SELECT>";
		
		$DemeritSelect = "<SELECT name='DemeritSelect'>";
		for($i=1; $i<5; $i++)
		{
			$DemeritSelect .= "<OPTION value='$i'>$i</OPTION>";
		}
		$DemeritSelect .= "</SELECT>";
		
		$AbsentSelect = "<SELECT name='AbsentSelect'>";
		for($i=1; $i<5; $i++)
		{
			$AbsentSelect .= "<OPTION value='$i'>$i</OPTION>";
		}
		$AbsentSelect .= "</SELECT>";
		
		$LateSelect = "<SELECT name='LateSelect'>";
		for($i=1; $i<5; $i++)
		{
			$LateSelect .= "<OPTION value='$i'>$i</OPTION>";
		}
		$LateSelect .= "</SELECT>";
				
		# ConductArray
		##################################################################################################		
		# get all level
		$SelectedFormArr = $libreportcard->GET_REPORT_FORMS_WITHOUT_REPORTID();
		
		# get student reg no
		$ClassLevels = implode(",", $SelectedFormArr);
		$StudentRegNoArray = $libreportcard->GET_STUDENT_REGNO_ARRAY($ClassLevels);
		
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		
		function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray)
		{
			global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
			
			$ClassArr = $libreportcard->GET_CLASSES();
			$TargetArray = $SemesterArray;
			$TargetArray[] = "FULL";
			for($i=0; $i<sizeof($TargetArray); $i++)
			{
				$t_semester = trim($TargetArray[$i]);
				$sem_number = GET_SEMESTER_NUMBER($t_semester);
				if($sem_number>=0)
				{
					for ($j = 0; $j < sizeof($ClassArr); $j++)
					{
						$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$ClassArr[$j].".csv";
						if(file_exists($TargetFile))
						{
							$data = $lf->file_read_csv($TargetFile);
							if(!empty($data))
							{
								$header_row = array_shift($data);
								$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
								if(!$wrong_format)
								{
									for($k=0; $k<sizeof($data); $k++)
									{
										$reg_no = array_shift($data[$k]);								
										$reg_no = trim(str_replace("#", "", $reg_no));	
										$student_id = trim($StudentRegNoArray[$reg_no]);
										if($student_id!="")
										{
											if(sizeof($data[$k])>1) {
												$ReturnArray[$student_id][$t_semester] = $data[$k];
											}
											else {
												$ReturnArray[$student_id][$t_semester] .= "<li>".$data[$k][0]."</li>";
											}
										}
									}
								}
							}
						}
					}
				}
			}		
			return $ReturnArray;
		}
		
		function GET_SEMESTER_NUMBER($ParSemester)
		{
			global $libreportcard, $SemesterArray;
		
			$sem_number = "-1";
			if(trim($ParSemester)=="FULL")
			{
				$sem_number = 0;
			} else
			{
				for($i=0; $i<sizeof($SemesterArray); $i++)
				{
					if(trim($ParSemester)==trim($SemesterArray[$i]))
					{
						$sem_number = $i+1;
						break;
					}
				}
			}
			return $sem_number;
		}
		
		function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
		{
			# Check Title Row
			$format_wrong = false;
		
			for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
			{
				if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
				{
					$format_wrong = true;
					break;
				}
			}
		
			return $format_wrong;
		}
		
		#####################################################################
		# get summary data from csv file
		
		$SummaryHeader = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
		$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
		$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);
		#####################################################################
		
		##################################################################################################

		if (is_array($StudentSummaryArray)) {
			while (list ($StudentID, $SemesterRecord) = each ($StudentSummaryArray))
			{
				if (is_array($SemesterRecord))
				{
					while (list ($SemesterName, $GradeRecord) = each ($SemesterRecord))
					{
						if ($GradeRecord[3] != "") $GradeArr[] = $GradeRecord[3];
					}
				}
			}
		}
		
		if (is_array($GradeArr)) $GradeArr = array_unique($GradeArr);
		
		$ConductCheck = "";
		if (is_array($GradeArr)) {
			while (list ($key, $value) = each ($GradeArr))
			{
				$ConductCheck .= "<input type='checkbox' name='Grade[]' id='Grade{$key}' value='{$value}'><label for='Grade{$key}'>".$value."</label>&nbsp;";
			}
		} else {
			$ConductCheck = " N/A";
		}
		
		
			
		
		
		//"<input type='radio' name='JudgeBy' id='ByTotal' value='total' checked><label for='ByTotal'>".$eReportCard['ByTotal']."</label>&nbsp;";

		
		
        # tag information
		$TAGS_OBJ[] = array($eReportCard['HonourStudent'], "", 0);
		
		$linterface->LAYOUT_START();
		
		?>
		<script language="javascript">
		function checkform(obj){
			if(!check_positive_nonzero_int (obj.ResultOver, "<?= $eReportCard['jsNonzeroAlert']?>")) return false;
			return true;
		}
		</script>

		<form name="form1" action="report.php" method="post"onSubmit="return checkform(this);">
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr><td></td></tr>
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Report'] ?> </td>
							<td><?=$ReportSelect?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_Semester ?> </td>
							<td><?=$SemesterSelect?></td>
						</tr>						
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['JudgeBy'] ?> </td>
							<td class="tabletext">
								<input type="radio" name="JudgeBy" id="ByTotal" value="total" checked><label for="ByTotal"><?= $eReportCard['ByTotal']?></label>&nbsp;
								<input type="radio" name="JudgeBy" id="ByGPA" value="gpa"><label for="ByGPA"><?= $eReportCard['ByGPA']?></label>&nbsp;
								<input type="radio" name="JudgeBy" id="AverageMark" value="average"><label for="AverageMark"><?= $eReportCard['AverageMark']?></label>
								&nbsp;&nbsp;&nbsp;<?= $eReportCard['Over'] ?>
								<input type="text" class="textboxnum" name="ResultOver" id="ResultOver" value="1">&nbsp;<?= $eReportCard['OverMark']?>
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Conduct'] ?> </td>
							<td class="tabletext">
								<?= $ConductCheck ?>
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= str_replace("<!--Item-->", $eReportCard['Merit'], $eReportCard['NoOf'])?></td>
							<td class="tabletext"><?= $eReportCard['morethanOrEqual']?> <?=$MeritSelect?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= str_replace("<!--Item-->", $eReportCard['Demerit'], $eReportCard['NoOf'])?></td>
							<td class="tabletext"><?= $eReportCard['lessthan']?> <?=$DemeritSelect?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= str_replace("<!--Item-->", $eReportCard['DaysAbsent'], $eReportCard['NoOf'])?></td>
							<td class="tabletext"><?= $eReportCard['lessthan']?> <?=$AbsentSelect?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= str_replace("<!--Item-->", $eReportCard['TimesLate'], $eReportCard['NoOf'])?></td>
							<td class="tabletext"><?= $eReportCard['lessthan']?> <?=$LateSelect?></td>
						</tr>
						<!--
						Conduct Grade > XXXXXXXX
						-->

			
						<!--
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['StartDate']; ?> </td>
							<td><input type="text" class="textboxnum" name="StartDate" maxlength="10" value="<?=$libreportcard->StartDate?>"> <?=$linterface->GET_CALENDAR("form1", "StartDate")?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['EndDate']; ?> </td>
							<td><input type="text" class="textboxnum" name="EndDate" maxlength="10" value="<?=$libreportcard->EndDate?>"> <?=$linterface->GET_CALENDAR("form1", "EndDate")?></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowClassTeacherComment" id="AllowClassTeacherComment" value=1 <?=($libreportcard->AllowClassTeacherComment==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherComment"><?=$eReportCard['AllowClassTeacherComment']?></label></span></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowSubjectTeacherComment" id="AllowSubjectTeacherComment" value=1 <?=($libreportcard->AllowSubjectTeacherComment==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowSubjectTeacherComment"><?=$eReportCard['AllowSubjectTeacherComment']?></label></span></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowClassTeacherUploadCSV" id="AllowClassTeacherUploadCSV" value=1 <?=($libreportcard->AllowClassTeacherUploadCSV==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherUploadCSV"><?=$eReportCard['AllowClassTeacherUploadCSV']?></label></span></td>
						</tr>
						-->
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		</form>
	<?= $linterface->FOCUS_ON_LOAD("form1.StartDate") ?>

	<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
