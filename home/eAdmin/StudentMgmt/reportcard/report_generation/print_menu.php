<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "ReportGeneration";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['ReportPrinting'];

	if ($libreportcard->hasAccessRight())
    {
		$linterface = new interface_html("popup.html");
#######################################################################################################
		$FormArray = $libreportcard->GET_REPORT_FORMS($ReportID);
		$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange='jFILTER_CHANGE(1)'", $ClassLevelID, 1, 0, "");
		
		$PrintReportFileName = (file_exists($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/print_report".$ReportCardTemplate.".php")) ? "print_report".$ReportCardTemplate.".php" : "print_report.php";

		if($ClassLevelID!="")
		{
			$ClassArray = $libreportcard->GET_REPORT_CLASSES($ClassLevelID, $ReportID);
			$ClassSelection = getSelectByArray($ClassArray, "name='ClassID' onChange='jFILTER_CHANGE(0)'", $ClassID, 1, 0, "");
			$ClassRow = "<tr>
						<td nowrap='nowrap' class='formfieldtitle tabletext'>".$i_ClassName."</td>
						<td class='tabletext' width='75%'>".$ClassSelection."</td>
						<tr>";

			if($ClassID!="")
			{
				$StudentSelection = $libreportcard->GET_REPORT_STUDENT_SELECTION($ClassID);
				$StudentRow = "<tr>
								<td valign='top' nowrap='nowrap' ><span class='tabletext'>".$i_identity_student.": </span></td>
								<td width='80%' style='align: left'>
									<table border='0' cellpadding='0' cellspacing='0' align='left'>
										<tr> 
											<td class='tabletext'>".$StudentSelection."</td>
											<td style='vertical-align:bottom'>        
												<table width='100%' border='0' cellspacing='0' cellpadding='6'>
													<tr> 
														<td align='left'> 
															".$linterface->GET_SMALL_BTN($button_select_all, 'button', "SelectAll(this.form.elements['TargetStudentID[]']); return false;")."
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>";
			}
		}
		
		
		####### Tung Nam only ###############################################################
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		
		if ($ReportCardTemplate == "4")
		{
			$SemesterSel = "<select id='commentSemester' name='commentSemester'>";
			$SemesterSel .= "<option value='0'>".$eReportCard['FullYear']."</option>";
			for ($i = 0; $i < sizeof($ColumnArray); $i++) {
				if (!$ColumnArray[$i]['IsColumnSum']) 
				{
					$Selected = ($commentSemester == ($i+1)) ? "selected" : "";
					$SemesterSel .= "<option value='".($i+1)."' $Selected>".$ColumnArray[$i]['ColumnTitle']."</option>";
				}
			}
			$SemesterSel .= "</select>";
		}
		####### Tung Nam only ###############################################################
		
##########################################################################################

$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function jFILTER_CHANGE(jChangeType)
{
	obj = document.form1;

	if(jChangeType==1 && typeof(obj.ClassID)!="undefined")
		obj.ClassID.value = "";

	obj.action = "print_menu.php";
	obj.submit();
}

function jDO_PRINT()
{
	obj = document.form1;
	obj.action = "<?=$PrintReportFileName?>";
	obj.target = "_blank";
	obj.submit();
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}
//-->
</script>

<form name="form1" method="get">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">
	<table width="95%" border="0" cellpadding="5" cellspacing="1">
	<? if ($ReportCardTemplate == "4") { ?>
	<tr>
		<td nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Semester'] ?></td>
		<td class="tabletext" width="75%"><?=$SemesterSel?></td>
	<tr>
	<? } ?>
	<tr>
		<td nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_ClassLevel?></td>
		<td class="tabletext" width="75%"><?=$FormSelection?></td>
	<tr>
	<?=$ClassRow?>
	<?=$StudentRow?>
	</table>
</td>
</tr>

<tr><td>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center">
	<?= $linterface->GET_ACTION_BTN($button_continue, "button", "javascript:jDO_PRINT()")?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.close()")?>
	</td>
</tr>
</table>
<br/>
</td></tr>

</table>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>