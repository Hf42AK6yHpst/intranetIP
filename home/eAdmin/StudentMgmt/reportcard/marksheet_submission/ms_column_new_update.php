<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList";
if(!empty($ColumnID))
{
	$sql = "UPDATE RC_SUB_MARKSHEET_COLUMN SET ColumnTitle='$ColumnTitle', Weight='$Weight', DateModified=now() WHERE ColumnID = '$ColumnID'";
	$success = $li->db_db_query($sql);
}
else if(!empty($ReportColumnID))
{
	$sql = "INSERT INTO RC_SUB_MARKSHEET_COLUMN (SubjectID, ReportColumnID, ColumnTitle, Weight, DateInput, DateModified) VALUES ('$SubjectID', '$ReportColumnID', '$ColumnTitle', '$Weight', now(), now())";
	$success = $li->db_db_query($sql);
}

if(!empty($ColumnID))
	$Result = ($success==1) ? "update" : "update_failed";
else
	$Result = ($success==1) ? "add" : "add_failed";

intranet_closedb();
header("Location:sub_marksheet.php?$param&Result=$Result");
?>

