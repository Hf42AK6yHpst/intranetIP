<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	include_once("../default_header.php");
	
	$libreportcard = new libreportcard();
	$limport = new libimporttext();
	
	$CurrentPage = "MarksheetSubmission";

	if ($libreportcard->hasAccessRight())
    {
		$linterface = new interface_html();
		
		global $eReportCard;
	
		# Add Tidiness - Offence to header for display 
	  	$DefaultHeaderArray[4][2][] = "TARDINESS_OFFENCE";
		$HeaderLangMap[4]["TARDINESS_OFFENCE"] = $eReportCard['TardinessOffence']."(".$eReportCard['Offence'].")";
		
###############################################################

		# get class object
		$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
		list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

		# Get Student of Class
		$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);
		
		# Get Results
		if ($ReportColumnID != "") {
			$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_COLUMN_RESULT($ClassID, $ReportColumnID, $IsWeighted);			
		} else {
			$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID);
		}
		
	    # Get RegNo map
	    $StudentRegNoMap = $libreportcard->GET_STUDENT_REGNO_ARRAY($ClassLevelID, $ClassID);
		
		# Get ReportID Class
		$ReportID = $libreportcard->GET_REPORTID_BY_CLASS($ClassID);

		# Get Report Subject
		$SubjectArray = $libreportcard->GET_REPORT_SUBJECTS($ReportID);
		
		
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		for ($temp = 0; $temp < sizeof($ColumnArray); $temp++) {
		  if($ReportColumnID == $ColumnArray[$temp][0])
		  {
		    $SelectedColumn = $ColumnArray[$temp];
		    break;
		  }
		}

		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		
		
	### 20091126 Ivan
    ### Scan the score list -> if the subject score of all student is empty => hide the subject
    $DisplaySubjectIDAssoArr = array();
    for($i=0; $i<sizeof($StudentArray); $i++)
    {
	    list($r_student_id, $RegNo, $class_number, $student_name) = $StudentArray[$i];
	    
	    if (is_array($SubjectArray) && count($SubjectArray) > 0)
	    {
		    foreach($SubjectArray as $r_subject_id => $subject_name)
			{
		    	$s_mark = $ResultArray[$r_student_id][$r_subject_id]["Mark"];
				$s_grade = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
				
				if ($s_mark != '' || $s_grade != '')
					$DisplaySubjectIDAssoArr[$r_subject_id] = true;
	    	}
	    }
	}
		
	### aki
    ### GET GT, AM START ##################################################################################
    # Get Class Level(s) of the report
	if($ClassLevelID!="")
		$SelectedFormArr = array($ClassLevelID);
	else
		$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);

	# Get Class Student(s)
	$LevelList = implode(",", $SelectedFormArr);
    list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID);
    ### GET GT, AM END ####################################################################################

		
    if($ReportColumnID=="")
      $ReportPeriod = 0;
    else
    {
	  if (is_array($SemesterArray))
      foreach($SemesterArray as $idx => $Semester)
      {
        if ((preg_match("/".$Semester."/", $SelectedColumn['ColumnTitle']))||(preg_match("/".$SelectedColumn['ColumnTitle']."/", $Semester)))
        {
          $ReportPeriod = $idx + 1;
          break;
        }
      }
    }
		
		$thisYearTermID = $libreportcard->Get_Semester_By_SemesterName($SelectedColumn['ColumnTitle']);
		if ($SelectedColumn['ColumnTitle'] == '' || $thisYearTermID == '')
			$thisYearTermID = 0;
			
		for($k=0; $k<sizeof($ReportTypeArray); $k++)
		{
		  
//			$ReportPeriod = $ReportColumnID==""?0: ($ReportColumnID=="58"?1:($ReportColumnID=="61"?2:""));
      //$TargetFile = $intranet_root."/file/reportcard/".$ReportTypeArray[$k]."/".$libreportcard->Year."_".$ReportPeriod."_".str_replace("/", "", $ClassName).".csv";
      $TargetFile = $intranet_root."/file/reportcard/".$ReportTypeArray[$k]."/".$libreportcard->YearName."_".$thisYearTermID."_".str_replace("/", "", $ClassName).".csv";

      if(file_exists($TargetFile))
      {
        $stu = 0;
        //$handle = fopen($TargetFile, "r");
        //$CSVHeader = fgetcsv($handle, 1000, ",");
        
        $data = $limport->GET_IMPORT_TXT($TargetFile, 0, '<br />');
        $CSVHeader = array_shift($data);
        
        //while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
         for ($i=0; $i<count($data); $i++) {
          for($j=0; $j<count($CSVHeader); $j++)
          {
            $StudentRegNo = str_replace("#", "", $data[$i][0]);
            $Student_ID = $StudentRegNoMap[$StudentRegNo];
            if($Student_ID != "")
              $StudentExRecord[$Student_ID][$ReportTypeArray[$k]][$CSVHeader[$j]] = $data[$i][$j];
          }
          $stu++;
        }
//        fclose($handle);                
      }
    }

		# Get Report Display Settings
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		$Settings = $ReportInfo[4];
		$SettingArray = unserialize($Settings);
		$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];

//		# Get Results
//		if ($ReportColumnID != "") {
//			$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_COLUMN_RESULT($ClassID, $ReportColumnID, $IsWeighted);
//		} else {
//			$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID);
//		}

		$TableContent = "<table width=\"100%\" bgcolor=\"#000000\" cellpadding=\"5\" cellspacing=\"1\" align=\"center\"  >";
		if(is_array($SubjectArray))
		{
			$HeaderRow = "<tr>";
			$HeaderRow .= "<td bgcolor=\"#FFFFFF\">&nbsp;</td>";
			foreach($SubjectArray as $subject_id => $subject_name)
			{
				### 20091126 Ivan
    			### if the subject score of all student is empty => hide the subject
				if (!$DisplaySubjectIDAssoArr[$subject_id])
					continue;
					
				$HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$subject_name."</font></td>";
				$SubjectIDArray[] = $subject_id;
			}
		
			$HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$eReportCard['ByTotal']."</font></td>";
			$HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$eReportCard['AverageMark']."</font></td>";
			$HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$eReportCard['ClassPosition']."</font></td>";
			$HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$eReportCard['FormPosition']."</font></td>";
			$HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$eReportCard['SubjectTaken']."</font></td>";
			
  		$i = 0;
  		while($DefaultHeaderArray[$i][$ReportCardTemplate] != ""){
  		  if(!in_array($i, $ExcludeInReport))
  		  {
	  		  if (is_array($DefaultHeaderArray[$i][$ReportCardTemplate]))
    		  foreach($DefaultHeaderArray[$i][$ReportCardTemplate] as $key=>$value){
    		    if(strcmp($value, "REGNO") != 0)
              $HeaderRow .= "<td class=\"report_contenttitle\" bgcolor=\"#FFFFFF\"><font size='-2'>".$HeaderLangMap[$i][$value]."</font></td>";
          }
        }
        $i++;
      }

			$HeaderRow .= "</tr>";
		}

		### aki
		if ($ReportColumnID=="") {
			$SummaryColumnTitle = $libreportcard->Semester;
		} else {
			//$SummaryColumnTitle = $SelectedColumn['ColumnTitle'];
			$SummaryColumnTitle = $thisYearTermID;
		}
		
		
		$CmpArray = $libreportcard->GET_REPORT_SELECTED_SUBJECTS($ReportID);
		for ($CmpCount = 0; $CmpCount < sizeof($CmpArray); $CmpCount++) {
			$IsCmpArray[$CmpArray[$CmpCount][3]] = $CmpArray[$CmpCount][1];
		}
		
		# Get Grademark scheme of subjects
		$SubjectIDList = (is_array($SubjectIDArray)) ? implode(",", $SubjectIDArray) : "";
		list($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray) = $libreportcard->GET_SUBJECT_GRADEMARK($ClassID, $SubjectIDList);

		##############################################	Get Attendance and Tardiness	##############################################
		
		include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/print_report_functions2.php");
		$lf = new libfilesystem();
		$StudentRegNoArray = $StudentRegNoMap;
		
		# Get Summary Array from CSV file
		$SummaryHeader = array("REGNO","CONDUCT","TIDINESS","APPLICATION","DAYS_ABSENT","TIMES_LATE","DAYS_ABSENT_WO_REASON","POLITENESS","BEHAVIOUR");
		$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
		$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);
		
		##############################################	Get Attendance and Tardiness	##############################################
		
		$TableContent .= $HeaderRow;
		for($i=0; $i<sizeof($StudentArray); $i++)
		{
		  $TotalMark = 0;
		  $AvgMark = 0;
		  $SubjectTaken = 0;
		
			$css = ($i%2?"2":"");
			list($r_student_id, $RegNo, $class_number, $student_name) = $StudentArray[$i];
			### aki
			list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ClassStudentArray[$ClassID][$r_student_id]["Result"][$SummaryColumnTitle];
			
			//$class_number_arr = split("-", $class_number);
			$class_number_arr = explode("-", $class_number);
			$class_number = trim($class_number_arr[1]);
			$TableContent .= "<tr>";
			$TableContent .= "<td bgcolor=\"#FFFFFF\"><span class=\"tabletext\"><font size='-2'>".$class_number." " .$student_name."</font></span></td>";
			for($k=0; $k<sizeof($SubjectIDArray); $k++)
			{
				$r_subject_id = $SubjectIDArray[$k];
				$SubjectSetting = $SubjectScaleArray[$r_subject_id];
				$s_mark = "";
				
				### 20091126 Ivan
    			### if the subject score of all student is empty => hide the subject
				if (!$DisplaySubjectIDAssoArr[$r_subject_id])
					continue;
					
				$StylePrefix = "";
				$StyleSuffix = "";
				if($SubjectSetting=="S")
				{
					$s_mark = $ResultArray[$r_student_id][$r_subject_id]["Mark"];
					$s_grade = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
					
					if ((!$IsCmpArray[$r_subject_id])&&($s_mark>=-1)) {
						$SubjectTaken++;
					}
					
					
					if($s_mark>=0) {
						$Result = ($MarkTypeDisplay==2) ? $s_grade : $s_mark;
						$TempSetting = ($MarkTypeDisplay==2) ? "G" : $SubjectSetting;
						//if (($ReportColumnID=="" && $libreportcard->HighlightOverall) || $ReportColumnID>0)
						if (true)
						{
							list($StylePrefix, $StyleSuffix) = $libreportcard->GET_STYLE($TempSetting, $Result, $FailedArray[$r_subject_id], $DistinctionArray[$r_subject_id], $PFArray[$r_subject_id]);
						}
						
						if($s_mark != "")
						{
	  						$TotalMark += $s_mark;
	  						//if (!$IsCmpArray[$r_subject_id]) $SubjectTaken++;
	  					}
					}
					else
						$Result = $libreportcard->SpecialMarkArray[trim($s_mark)];
				}
				else {
					$Result = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
					if($Result!="/" && $Result!="abs")
					{
						//if (($ReportColumnID=="" && $libreportcard->HighlightOverall) || $ReportColumnID>0)
						if (true)
						{
							list($StylePrefix, $StyleSuffix) = $libreportcard->GET_STYLE($SubjectSetting, $Result, $FailedArray[$r_subject_id], $DistinctionArray[$r_subject_id], $PFArray[$r_subject_id]);
						}
					}
				}
				
				if(trim($Result) != "")
				  $Result = $StylePrefix.$Result.$StyleSuffix;
				$TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext \">".($Result==""?"--":$Result)."</span></td>";
			}
			
  			if($SubjectTaken > 0)
  			{
          		$TotalMark = number_format($TotalMark, 2, '.', '');
          		$AvgMark = number_format(($TotalMark/$SubjectTaken), 2, '.', '');
  			}
			
			$TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">".(($GT=="")?"--":$GT)."</span></td>";
			$TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">".(($AM=="")?"--":$AM)."</span></td>";
			$TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">".(($OMC=="")?"--":$OMC)."</span></td>";
			$TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">".(($OMF=="")?"--":$OMF)."</span></td>";
			$TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">".$SubjectTaken."</span></td>";
		
		# Get calculated attendance and tardiness
		$Summary_Details_Ary = GET_REPORT_ATTENDANCE_AND_TARDINESS($StudentSummaryArray[$r_student_id]);
		$Summary_Details_Ary = $Summary_Details_Ary[$SelectedColumn['ColumnTitle']];

  		for($k=0; $k<sizeof($ReportTypeArray); $k++)
  		{
  		  if(!in_array($k, $ExcludeInReport))
  		  {
    		  $Header = $DefaultHeaderArray[$k][$ReportCardTemplate];
    		  $Content = $StudentExRecord[$r_student_id][$ReportTypeArray[$k]];	  
    		  
    		  # Display calculated value
    		  if(in_array('ATTENDANCE', $Header)){
			  	$Content['ATTENDANCE'] = $Summary_Details_Ary['Attendance']? $Summary_Details_Ary['Attendance'] : "";
			  }
			  if(in_array('TIMES_LATE', $Header)){
			  	$Content['TIMES_LATE'] = $Summary_Details_Ary['TimesLate']? $Summary_Details_Ary['TimesLate'] : "";
			  }
			  if(in_array('TARDINESS', $Header)){
			  	$Content['TARDINESS'] = $Summary_Details_Ary['Tardiness']? $Summary_Details_Ary['Tardiness'] : "";
			  	$Content['TARDINESS_OFFENCE'] = $Summary_Details_Ary['TardinessOffence']? $Summary_Details_Ary['TardinessOffence'] : "";
			  }

    		  if(is_array($Content))
    		  {
	    		if (is_array($Header))
    		    foreach($Header as $idx => $fieldname)
    		    {	
    		      if($fieldname != "REGNO")
    		        $TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">".$Content[$fieldname]."</span></td>";
   		     	}
    		  }
    		  else
    		  {
    		    for($a=1; $a<count($Header); $a++)
              $TableContent .= "<td nowrap='nowrap' bgcolor=\"#FFFFFF\"><span class=\"tabletext\">--</span></td>";
    		  }
    		}
		}
			
			$TableContent .= "</tr>";
		}
		$TableContent .= "</table>";
################################################################

?>
<style type='text/css' media='print'>
.print_hide {display:none;}
</style>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr><td class="print_hide" align='right' height='50'><input name="print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
<tr>
	<td nowrap="nowrap" class="report_contenttitle" ><?= $eReportCard['ClassName']; ?>&nbsp;:&nbsp;<?=$ClassName?></td><td class="tabletext"></td>
</tr>
<tr><td><?=$TableContent?></td></tr>
<tr><td class="print_hide" align='right' height='50'><input name="print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<?
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
