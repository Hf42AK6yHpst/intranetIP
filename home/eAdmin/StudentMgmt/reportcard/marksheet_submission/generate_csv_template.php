<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();

# Get Subject Name
$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);

$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
$ColumnSize = sizeof($ColumnArray);
$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);
$StudentSize = sizeof($StudentArray);

# Get ReportSubjectID
$ReportSubject = $libreportcard->GET_REPORT_SUBJECT_ID($ReportID, $SubjectID);
$ReportSubjectID = $ReportSubject[0];

# Get Report Type
$ReportType = $libreportcard->GET_REPORT_TYPE($ReportID);
if($ReportType=="F")	# Export result of previous semester
{
	if(!empty($TargetStudentIDList))
	{
		$StudentIDList = $TargetStudentIDList;
	}
	else
	{
		for($i=0; $i<$StudentSize; $i++)
		{
			$StudentIDArray[] = $StudentArray[$i][0];
		}
		if(!empty($StudentIDArray))
			$StudentIDList = implode(",", $StudentIDArray);
	}

	$PrevResultArray = $libreportcard->GET_PREVIOUS_MARKSHEET_SCORE("", $ReportID, 0, $SubjectID, $StudentIDList);
}

//$Content .= "ClassName,ClassNumber";

$exportColumn = array();
$exportColumn[] = 'ClassName';
$exportColumn[] = 'ClassNumber';

for($i=0; $i<$ColumnSize; $i++)
{
	list($RCID, $ColumnTitle, $Weight) = $ColumnArray[$i];
	$ReportCell = $libreportcard->GET_REPORT_CELL_BY_SUBJECT_COLUMN($ReportSubjectID, $RCID);
	if($ReportCell[$RCID]!="N/A")
	{
		//$Content .= ",".$ColumnTitle;
		$exportColumn[] = $ColumnTitle;
	}

	$RCIDArray[] = $RCID;
}
//$Content .= ($DefaultSetting!="S" && $ColumnSize>1) ? ",".$eReportCard['OverallResult']."\n" : "\n";
if ($DefaultSetting!="S" && $ColumnSize>1)
	$exportColumn[] = $eReportCard['OverallResult'];

$Counter = 0;
$ExportArr = array();
for($j=0; $j<$StudentSize; $j++)
{
	$StudentID = $StudentArray[$j][0];
	$Class = $StudentArray[$j]['ClassTitleEn'];
	if(!empty($Class))
	{
		//list($ClassName, $ClassNumber) = explode("-", $Class);
		//$Content .= trim($ClassName).",".trim($ClassNumber);
		
		$ClassName = $StudentArray[$j]['ClassTitleEn'];
		$ClassNumber = $StudentArray[$j]['ClassNumber'];
		(array)$ExportArr[$Counter][] = $ClassName;
		(array)$ExportArr[$Counter][] = $ClassNumber;

		# Export result of previous semester
		if($ReportType=="F")
		{
			for($k=0; $k<sizeof($RCIDArray); $k++)
			{
				$rc_id = $RCIDArray[$k];
				$value = $PrevResultArray[$StudentID][$rc_id];
				if($value<0)
					$value = $libreportcard->SpecialMarkArray[$value];
				
				(array)$ExportArr[$Counter][] = $value;
				//$Content .= ",".$value;
			}
		}
		//$Content .= "\n";
		
		$Counter++;
	}
}

intranet_closedb();

// Output the file to user browser
$filename = str_replace("&nbsp;", " ", "Marksheet_Template_".trim($SubjectName)."_".trim($ClassName).".csv");
//output2browser($Content, $filename);

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>