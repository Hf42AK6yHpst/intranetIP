<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "MarksheetSubmission";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();

	if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html("popup.html");

################################################################
	
		$param = "StudentID=$StudentID&StudentName=$StudentName&ClassNumber=$ClassNumber&NotificationID=$NotificationID";
		
		# Get Subject Name
		$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
		$FeedbackArray = $libreportcard->GET_STUDENT_FEEDBACK($NotificationID, $StudentID);

		$x = "";
		for($i=0; $i<sizeof($FeedbackArray); $i++)
		{
			$css = ($i%2==0) ? 1 : 2;
			list($InputDate, $Comment, $IsRead) = $FeedbackArray[$i];
			
			$x .= "<tr>";
			$x .= "<td class='tablebluerow$css tabletext'>".($i+1)."</td>";
			$x .= ($IsRead==0) ? "<td class='tablebluerow$css tabletext'><img align='absmiddle' src='{$image_path}/{$LAYOUT_SKIN}/icon_newfeedback.gif' title='Unread'>".$Comment."</td>" : "<td class='tablebluerow$css tabletext'>".$Comment."</td>";
			$x .= "<td class='tablebluerow$css tabletext'>".$InputDate."</td>";
			$x .= "</tr>";
		}

################################################################
	
# tag information
$TAGS_OBJ[] = array($eReportCard['MarksheetSubmission'], "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['StudentFeedback'], "");

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>
function jMARK_READ()
{
	if(confirm("<?=$eReportCard['MarkReadAlert']?>"))
	{
		var jNID = "<?=$NotificationID?>";
		self.location.href = "mark_read.php?<?=$param?>&NotificationID="+jNID;
	}
}
</SCRIPT>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1" align="center">
	<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
	<tr>
		<td colspan="2">
			<table width='100%' border='0' cellpadding="3" cellspacing="1">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="120"><?= $i_ClassNumber; ?></td><td class="tabletext tablerow2" nowrap="nowrap"><?=$ClassNumber?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="120"><?= $i_identity_student; ?></td><td class="tabletext tablerow2" nowrap="nowrap"><?=$StudentName?></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
			<tr>
				<td class='tabletopnolink tablebluetop'>#</td>
				<td class='tabletopnolink tablebluetop'><?=$eReportCard['Feedback']?></td>
				<td class='tabletopnolink tablebluetop'><?=$eReportCard['SendingDate']?></td>
			</tr>
			<?=$x?>
			</table>
		</td>
	</tr>
	</table>
</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp;<img align='absmiddle' src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_newfeedback.gif' title='Unread'>&nbsp;<?=$eReportCard['UnreadRecordRemind'] ?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($eReportCard['MarkAllRead'], "button", "javascript:jMARK_READ();")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<p></p>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
