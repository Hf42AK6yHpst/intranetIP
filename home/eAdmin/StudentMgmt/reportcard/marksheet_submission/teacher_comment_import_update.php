<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();

$limport = new libimporttext();

$param = "ClassID=$ClassID&SubjectID=$SubjectID";

if($SubjectID!="")
{
	$delete_cond = " AND SubjectID = '$SubjectID'";
	$ReturnPage = "subject_teacher_comment.php?".$param;
}
else
{
	$delete_cond = " AND SubjectID IS NULL";
	$ReturnPage = "class_teacher_comment.php?".$param;
}


# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename))
{
	$Result = "import_failed2";
	header("Location: class_teacher_comment.php?$param&Result=$Result");
}
else
{
	intranet_opendb();

	$li = new libdb();
	$lo = new libfilesystem();
	$libreportcard = new libreportcard();
	
	# get class object
	$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
	list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

	/*
	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV")
	{
		//$data = $lo->file_read_csv($filepath);
		$data = $lo->file_read_csv($filepath);
		$HeaderRow = array_shift($data);	# drop the title bar
	}
	*/

	$data = $limport->GET_IMPORT_TXT($filepath);
	$HeaderRow = array_shift($data);		# drop the title bar
	
	$ColumnTitle = array("ClassName", "ClassNumber", "Comment");
	for($j=0; $j<sizeof($HeaderRow); $j++)
	{
		if($HeaderRow[$j]!=$ColumnTitle[$j])
		{
			$FormatWrong = 1;
			break;
		}
	}
	
	if($FormatWong!=1)
	{
		$Year = $libreportcard->Year;
		$Semester = $libreportcard->Semester;
		for($i=0; $i<sizeof($data); $i++)
		{
			$data_obj = $data[$i];
			$cname = trim(array_shift($data_obj));
			$cnum = trim(array_shift($data_obj));

			if($cname!=$ClassName)
				continue;

			$sql = "SELECT 
							iu.UserID, yc.ClassTitleEN, ycu.ClassNumber 
					FROM 
							INTRANET_USER as iu
							Inner Join
							YEAR_CLASS_USER as ycu On (iu.UserID = ycu.UserID)
							Inner Join
							YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
					WHERE 
							yc.AcademicYearID = '".$libreportcard->Year."'
							And
							yc.ClassTitleEN = '$cname' 
							AND 
							ycu.ClassNumber = '$cnum'
					";
			$row = $li->returnArray($sql, 3);
			list($StudentID, $ClassName, $ClassNumber) = $row[0];

			if(!empty($StudentID) && !empty($data_obj))
			{
				$comment = array_shift($data_obj);
				$comment = intranet_htmlspecialchars(addslashes($comment));
				
				# Remove comment reocrd of student
				$sql = "DELETE FROM RC_MARKSHEET_COMMENT WHERE StudentID = '$StudentID' AND Year = '$Year' AND Semester = '$Semester' $delete_cond";
				$li->db_db_query($sql);

				# Insert comment
				if($SubjectID=="")
				{
					$sql = "INSERT INTO RC_MARKSHEET_COMMENT(StudentID, Comment, Year, Semester, ClassName, ClassNumber, TeacherID, DateInput, DateModified) VALUES('".$StudentID."', '".$comment."', '".$Year."', '".$Semester."', '".$ClassName."', '".$ClassNumber."', '".$UserID."', now(), now())";
				}
				else
				{
					$sql = "INSERT INTO RC_MARKSHEET_COMMENT(StudentID, SubjectID, Comment, Year, Semester, ClassName, ClassNumber, TeacherID, DateInput, DateModified) VALUES('".$StudentID."', '".$SubjectID."', '".$comment."', '".$Year."', '".$Semester."', '".$ClassName."', '".$ClassNumber."', '".$UserID."', now(), now())";
				}
				$success = $li->db_db_query($sql);
			}				
		}
	}
}

$Result = ($success==1) ? "import_success" : "import_failed2";
intranet_closedb();

header("Location:".$ReturnPage."&Result=$Result");
?>

