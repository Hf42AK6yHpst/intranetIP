<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "MarksheetRevision";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

		$HeaderRow = "<tr class='tablegreentop'>
						<td class=\"tabletoplink\" nowrap=\"nowrap\">".$eReportCard['Subject']."</td>
						<td class=\"tabletoplink\" nowrap=\"nowrap\">".$eReportCard['Class']."</td>
					";
		if (!$ReportCardConfirmDisabled)
		{
				$HeaderRow .= "<td class=\"tabletoplink\" nowrap=\"nowrap\">".$i_general_status."</td>";

		}
		$HeaderRow .= "<td class=\"tabletoplink\" nowrap=\"nowrap\">".$eReportCard['Feedback']."</td>
						<td class=\"tabletoplink\" nowrap=\"nowrap\">".$eReportCard['LastModifiedDate']."</td>
						<td class=\"tabletoplink\" nowrap=\"nowrap\">".$eReportCard['LastModifiedBy']."</td>";
		$HeaderRow .= ($libreportcard->AllowSubjectTeacherComment==1) ? "<td class=\"tabletoplink\" width='55'>".$eReportCard['TeacherComment']."</td>" : "";
		$HeaderRow .= "</tr>";

		# Get All Subjects
		$FormArray = $libreportcard->GET_ALL_REPORT_FORMS();
		if($ClassLevelID=="")
			$ClassLevelID = $FormArray[0][0];

		$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange='document.form1.form_change.value=1;document.form1.submit()'", $ClassLevelID, 1, 1, "");

		$FilterRow = "<tr><td colspan='6' class='tabletext'>";
		$FilterRow .= $eReportCard['Form'].": ".$FormSelection;

		# Get All Subjects
		$SubjectArray = $libreportcard->GET_SUBJECT_BY_FORM($ClassLevelID);

		$TableContent = "";
		if(!empty($SubjectArray))
		{
			if($SubjectID=="" || $form_change==1)
				$SubjectID = $SubjectArray[0][0];

			$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
			$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $libreportcard->Year);
			$ClassSize = sizeof($ClassArray);

			$SubjectSelection = getSelectByArray($SubjectArray, "name='SubjectID' onChange='document.form1.submit()'", $SubjectID, 1, 1, "");
			$FilterRow .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$eReportCard['Subject'].": ".$SubjectSelection."</td>";

			# Get Class Notification
			$NotificationArray = $libreportcard->GET_CLASS_NOTIFICATION($SubjectID, $ClassLevelID);
			
			# Get Feedback Status
			$FeedbackCountArray = $libreportcard->GET_FEEDBACK_COUNT($SubjectID);

			# Get related subject list for last modified counting purpose
			$RelatedSubject = $libreportcard->GET_RELATED_SUBJECT($SubjectID);
			$SubjectList = (sizeof($RelatedSubject)>1) ? implode(",", $RelatedSubject) : $SubjectID;
			$LastModifiedArray = $libreportcard->GET_CLASS_MARKSHEET_LAST_MODIFIED($SubjectList, $ClassLevelID);
			
			$ClassCount = 0;
			for($i=0; $i<sizeof($ClassArray); $i++)
			{
				list($ClassID, $ClassName) = $ClassArray[$i];

				$param = "SubjectID=$SubjectID&ClassID=$ClassID&ClassLevelID=$ClassLevelID";

				# generate notification button
				if($NotificationArray[$ClassID]==1)
				{
					$NotifyBtn = "<td class=\"tabletext\"><font color=blue>".$eReportCard['Confirmed']."</font></td>";

					$FeedbackCount = ($FeedbackCountArray[$ClassID]=="") ? 0 : $FeedbackCountArray[$ClassID];
					$Feedback = $FeedbackCount." ".$eReportCard['Appeals'];
					$FeedbackBtn = "<td>".$linterface->GET_BTN($button_view, "button", "javascript:self.location.href='feedback.php?$param'", "", "title='".$Feedback."'")."</td>";
				}
				else
				{
					if ($ck_ReportCard_UserType=="ADMIN") {
						$NotifyBtn = "<td class=\"tabletext\">--</td>";
					} else {
						$NotifyBtn = "<td class=\"tabletext\">".$linterface->GET_BTN($eReportCard['ConfirmMarksheet'], "button", "javascript:jCONFIRM_MARKSHEET('$SubjectID', '$ClassID', '$ClassLevelID')")."</td>";
					}

					$FeedbackBtn = "<td class=\"tabletext\">--</td>";
				}
				list($LastModifiedDate, $LastModifiedBy) = $LastModifiedArray[$ClassID];

				$css = ($i%2?"2":"");
				$TableContent .= "<tr class='tablegreenrow".$css."'>";
				$TableContent .= ($ClassCount==0) ? "<td rowspan='$ClassSize' valign='top'><span class=\"tabletext\">".$SubjectName."</span></td>" : "";
				$TableContent .= "<td><a href='marksheet_edit1.php?$param' class='tablegreenlink'>".$ClassName."</a></td>";
				if (!$ReportCardConfirmDisabled)
				{
					$TableContent .= $NotifyBtn;
				}
				//$TableContent .= $ConfirmedBtn;
				$TableContent .= $FeedbackBtn;
				$TableContent .= "<td><span class=\"tabletext\">".($LastModifiedDate==""?"--":$LastModifiedDate)."</span></td>";
				$TableContent .= "<td><span class=\"tabletext\">".($LastModifiedBy==""?"--":$LastModifiedBy)."</span></td>";
				$TableContent .= ($libreportcard->AllowSubjectTeacherComment==1) ? "<td><span class=\"tabletext\"><a href='subject_teacher_comment.php?$param' class='tablegreenlink'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" border=\"0\" align=\"absmiddle\">".$button_edit."</a></td>" : "";
				$TableContent .= "</tr>";

				$ClassCount++;
			}
		}
		else
		{
			$TableContent = "<tr><td colspan=\"6\" align=\"center\" class='tabletext tablegreenrow1'>".$i_no_record_exists_msg."</td></tr>";
		}
		$FilterRow .= "</tr>";

		
		$TeacherCommentRow = "<tr><td align='right' nowrap='nowrap' colspan='7'>";
		# Teacher comment / score list row
		if($libreportcard->AllowClassTeacherComment==1)
		{
			//$TeacherCommentRow = "<tr><td align='right' nowrap='nowrap' colspan='7'>";
			$TeacherCommentRow .= "<a class='tablegreenlink' href=\"javascript:jGO_TEACHER_COMMENT()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_comment.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ClassTeacherComment']."</a>";
			//$TeacherCommentRow .= "&nbsp;&nbsp;<a class='tablegreenlink' href=\"javascript:jGO_SCORE_LIST()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ViewScoreList']."</a>";
			//$TeacherCommentRow .= "&nbsp;&nbsp;<a class='tablegreenlink' href=\"javascript:jGO_SCORE_SUMMARY()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ViewScoreSummary']."</a>";		
		}
		$TeacherCommentRow .= "&nbsp;&nbsp;<a class='tablegreenlink' href=\"javascript:jGO_SCORE_LIST()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ViewScoreList']."</a>";
		$TeacherCommentRow .= "&nbsp;&nbsp;<a class='tablegreenlink' href=\"javascript:jGO_SCORE_SUMMARY()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ViewScoreSummary']."</a>";
		$TeacherCommentRow .= "</td></tr>";

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['MarksheetRevision'], "", 0);

$linterface->LAYOUT_START();


?>
<SCRIPT LANGUAGE="Javascript">

function jCONFIRM_MARKSHEET(jSubjectID, jClassID, jClassLevelID)
{
	if(confirm("<?=$eReportCard['ConfirmMarksheetAlert']?>"))
	{
		self.location.href = "confirm_marksheet.php?SubjectID="+jSubjectID+"&ClassID="+jClassID+"&ClassLevelID="+jClassLevelID;
	}
}

function jGO_SCORE_LIST()
{
	obj = document.form1;
	var action_cur = obj.action;
	var target_cur = obj.target;
	obj.action = "score_list.php";
	obj.target = "scorelist";
	obj.submit();
	obj.target = target_cur;
	obj.action = action_cur;
}

function jGO_SCORE_SUMMARY()
{
	obj = document.form1;
	var action_cur = obj.action;
	var target_cur = obj.target;
	obj.action = "score_summary.php";
	obj.target = "scoresummary";
	obj.submit();
	obj.target = target_cur;
	obj.action = action_cur;
}

function jGO_TEACHER_COMMENT()
{
	obj = document.form1;
	obj.action = "class_teacher_comment.php";
	obj.submit();
}

</SCRIPT>

<br/>
<form name="form1" action="" method="POST">
<table width="90%" border="0" cellpadding="2" cellspacing="0">
<tr><td align="right" colspan="6"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
<?=$FilterRow?>
<?=$TeacherCommentRow?>
<?=$HeaderRow?>
<?=$TableContent?>
</table>
<br/>
<input type="hidden" name="form_change" value=0 />
<!--<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />-->
</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
