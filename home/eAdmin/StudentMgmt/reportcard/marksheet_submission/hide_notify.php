<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

if(!empty($NotificationID))
{
	$sql = "UPDATE RC_MARKSHEET_NOTIFICATION SET Deadline = NULL WHERE NotificationID = '$NotificationID'";
	$success = $li->db_db_query($sql);
}

$Result = ($success==1) ? "hide_notify" : "hide_notify_failed";
intranet_closedb();
header("Location:index.php?Result=$Result");
?>

