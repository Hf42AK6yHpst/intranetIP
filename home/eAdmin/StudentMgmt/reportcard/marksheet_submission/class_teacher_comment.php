<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	if($ck_ReportCard_UserType=="ADMIN")
	{
		$CurrentPage = "MarksheetRevision";
		$BackPage = "admin_index.php?SubjectID=$SubjectID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();

	
	##### Word Template Setting #####################################################
	$lf = new libwordtemplates(1);
	$base_dir = "$intranet_root/file/templates/";
	if (!is_dir($base_dir))
	{
	     $lf->folder_new($base_dir);
	}
	
	$file_array = $lf->file_array;
	$word_array = $lf->word_array;
	$data = get_file_content($base_dir.$file_array[0]); // 0 - class teacher
	$CommentArr = explode("\n", $data);
	
	function GEN_COMMENT_SEL($ParStuID) {
		global $CommentArr, $button_select;
		
		$ReturnStr = "<select onchange='document.getElementById(\"Comment_".$ParStuID."\").innerText+=\" \"+this.value'>\n";		
		$ReturnStr .= "<option value=''>-- {$button_select} --</option>";
		for ($i = 0; $i < sizeof($CommentArr); $i++) {
			$ReturnStr .= "<option value='".str_replace("'", "&#39;", str_replace("\"", "&quot;", $CommentArr[$i]))."'>".$CommentArr[$i]."</option>\n";
		}
		$ReturnStr .= "<select>\n";
		return $ReturnStr;
	}
	##### Word Template Setting #####################################################
	
	if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		if($ck_ReportCard_UserType=="ADMIN")
		{
			$FormArray = $libreportcard->GET_ALL_REPORT_FORMS();
			if($ClassLevelID=="")
				$ClassLevelID = $FormArray[0][0];

			$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange=\"document.form1.form_change.value=1;jFilterChange()\"", $ClassLevelID, 1, 1, "");

			$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $libreportcard->Year);
			$ClassSelection = getSelectByArray($ClassArray, "name='ClassID' onChange=\"jFilterChange()\"", $ClassID, 1, 1, "");
			
			if($ClassID=="" || $form_change==1)
				$ClassID = $ClassArray[0][0];

			$FormRow = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['Form']."</td><td class=\"tabletext\" width=\"70%\">".$FormSelection."</td></tr>";
		}
		else
			$HiddenClassIDRow = "<input type=\"hidden\" name=\"ClassID\" value=\"".$ClassID."\" />";

		# get class object
		$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
		list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;
		
		$ClassDisplay = ($ck_ReportCard_UserType=="ADMIN") ? $ClassSelection : $ClassName;
###############################################################
		
		# Get Results
		$CommentArray = $libreportcard->GET_TEACHER_COMMENT($ClassID);
		
		# Get Student of Class
		$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);
		
		$TableContent = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$HeaderRow = "<tr class='tablegreentop'>";
		$HeaderRow .= "<td class=\"tabletoplink\">".$eReportCard['ClassNumber']."</td>";
		$HeaderRow .= "<td class=\"tabletoplink\">".$i_identity_student."</td>";
		$HeaderRow .= "<td class=\"tabletoplink\">".$eReportCard['Comment']."</td>";
		$HeaderRow .= "</tr>";
		
		# Get Grademark scheme of subjects
		$SubjectIDList = (is_array($SubjectIDArray)) ? implode(",", $SubjectIDArray) : "";
		list($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray) = $libreportcard->GET_SUBJECT_GRADEMARK($ClassID, $SubjectIDList);
		
		# generate buttons
		$ImportButton = $linterface->GET_SMALL_BTN($button_import, "button", "javascript:self.location.href='teacher_comment_import.php?ClassID=$ClassID'");
		$PrintButton = $linterface->GET_SMALL_BTN($button_print, "button", "javascript:newWindow('class_teacher_comment_print.php?ClassID=$ClassID', 8)");

		$GoToBottomRow = "<tr><td colspan=2>".$ImportButton." ".$PrintButton."</td><td align='right'><a class='tablegreenlink' href='#bottom'>".$eReportCard['GoToBottom']."</a></td></tr>";
		$GoToTopRow = "<tr><td align='right' colspan='3'><a class='tablegreenlink' href='#top'>".$eReportCard['GoToTop']."</a></td></tr>";

		$TableContent .= $GoToBottomRow;
		$TableContent .= $HeaderRow;
		for($i=0; $i<sizeof($StudentArray); $i++)
		{
			$css = ($i%2?"2":"");
			list($r_student_id, $class_number, $student_name) = $StudentArray[$i];
			$r_comment = $CommentArray[$r_student_id];

			$TableContent .= "<tr class='tablegreenrow".$css."'>";
			$TableContent .= "<td nowrap='nowrap' valign='top'><span class=\"tabletext\">".$class_number."</span></td>";
			$TableContent .= "<td nowrap='nowrap' valign='top'><span class=\"tabletext\"> ".$student_name."</span></td>";
			$TableContent .= "<td valign='top'>".GEN_COMMENT_SEL($r_student_id)."<span class=\"tabletext\">".$linterface->GET_TEXTAREA("Comment_".$r_student_id, $r_comment)."</span></td>";
			$TableContent .= "</tr>";
		}
		$TableContent .= $GoToTopRow;
		$TableContent .= "</table>";
		
################################################################

# tag information
$TAGS_OBJ[] = array($TagName, "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['ClassTeacherComment'], "");

$linterface->LAYOUT_START();
?>
<style type='text/css' media='print'>
.print_hide {display:none;}
</style>
<script language="javascript">
function jFilterChange()
{
	obj = document.form1;
	obj.action = "class_teacher_comment.php";
	obj.submit();
}
</script>
<br />
<form name="form1" action="class_teacher_comment_update.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="4" cellspacing="0" align="center">
			<tr><td align="right" colspan=3><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
			<?=$FormRow?>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['ClassName']; ?></td><td class="tabletext" width="70%"><?=$ClassDisplay?></td></tr>
			<tr><td class="dotline" colspan=2><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width=10 height=1></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$TableContent?>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"" id="bottom">
			<?= $linterface->GET_ACTION_BTN($button_update, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location.href='".$BackPage."'")?>&nbsp;
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<?=$HiddenClassIDRow?>
<input type="hidden" name="form_change" />
</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
