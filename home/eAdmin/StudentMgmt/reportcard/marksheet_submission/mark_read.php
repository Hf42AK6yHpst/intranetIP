<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$param = "StudentID=$StudentID&StudentName=$StudentName&ClassNumber=$ClassNumber&NotificationID=$NotificationID";

$li = new libdb();

$sql = "UPDATE RC_MARKSHEET_FEEDBACK SET IsTeacherRead = 1, DateModified = now() WHERE NotificationID = '$NotificationID'";
$success = $li->db_db_query($sql);

$Result = ($success==1) ? "update" : "update_failed";
intranet_closedb();
header("Location:student_feedback.php?$param&Result=$Result");
?>

