<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList";	

if(!empty($ColumnID))
{
	# remove column
	$sql = "DELETE FROM RC_SUB_MARKSHEET_COLUMN WHERE ColumnID = '$ColumnID'";
	$success = $li->db_db_query($sql);
	
	# remove column result
	if($success==1)
	{
		if($ColumnSize>1)	# only remove the column results
		{
			$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ColumnID = '$ColumnID'";
			$li->db_db_query($sql);
		}
		else	# remove overall results too if this is the last column
		{
			$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID'";
			$li->db_db_query($sql);
		}
	}
}

$Result = ($success==1) ? "delete" : "delete_failed";
intranet_closedb();
header("Location:sub_marksheet.php?$param&Result=$Result");
?>

