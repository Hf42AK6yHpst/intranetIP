<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();

	# get class object
	$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
	list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

	if($ck_ReportCard_UserType=="ADMIN")
	{
		$CurrentPage = "MarksheetRevision";
		$BackPage = "admin_index.php?SubjectID=$SubjectID&ClassID=$ClassID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	
	if ($libreportcard->hasAccessRight())
    {
         $linterface = new interface_html();

################################################################
	
		$param = "SubjectID=$SubjectID&ClassID=$ClassID";
		
		# Get Subject Name
		$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);

		# Generate Displa Mode filter
		$DisplayModeFilter = "<SELECT name='DisplayMode' onChange=\"document.form1.submit();\">";
		$DisplayModeFilter .= "<OPTION value=0 ".($DisplayMode==0?"SELECTED='SELECTED'":"").">".$eReportCard['ShowAppealStudentOnly']."</OPTION>";
		$DisplayModeFilter .= "<OPTION value=1 ".($DisplayMode==1?"SELECTED='SELECTED'":"").">".$eReportCard['ShowClassStudentStatus']."</OPTION>";
		$DisplayModeFilter .= "</SELECT>";
		
		if($DisplayMode==1)
		{
			$StatusHeadingRow = "<td class='tabletopnolink tablegreentop'>".$i_general_status."</td>";

			$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);
			list($FeedbackArray, $ConfirmedArray) = $libreportcard->GET_ALL_STUDENT_FEEDBACK_STATUS($SubjectID, $ClassID);
					
			$x = "";
			for($i=0; $i<sizeof($StudentArray); $i++)
			{
				$css = ($i%2==0) ? 1 : 2;
				list($StudentID, $ClassNumber, $StudentName) = $StudentArray[$i];
				
				$confirmed_status = (is_array($ConfirmedArray) && in_array($StudentID, $ConfirmedArray)) ? $eReportCard['Confirmed'] : "--";
				$Feedback = (!empty($FeedbackArray[$StudentID])) ? $FeedbackArray[$StudentID] : "--";

				$x .= "<tr>";
				$x .= "<td class='tablegreenrow$css tabletext'>".$ClassNumber."</td>";
				$x .= "<td class='tablegreenrow$css tabletext'>".$StudentName."</td>";
				$x .= "<td class='tablegreenrow$css tabletext'>".$Feedback."</td>";
				$x .= "<td class='tablegreenrow$css tabletext'>".$confirmed_status."</td>";
				$x .= "</tr>";
			}

			$EditButton = $linterface->GET_ACTION_BTN($eReportCard['EditMarksheet'], "button", "javascript:self.location.href='marksheet_edit1.php?$param'")."&nbsp;";
		}
		else
		{
			$StudentFeedbackArray = $libreportcard->GET_ALL_STUDENT_FEEDBACK($SubjectID, $ClassID);
			
			if(sizeof($StudentFeedbackArray)>0)
			{
				for($i=0; $i<sizeof($StudentFeedbackArray); $i++)
				{
					list($StudentID, $ClassNumber, $StudentName, $Feedback) = $StudentFeedbackArray[$i];
					
					$x .= "<tr>";
					$x .= "<td class='tablegreenrow$css tabletext'>".$ClassNumber."</td>";
					$x .= "<td class='tablegreenrow$css tabletext'>".$StudentName."</td>";
					$x .= "<td class='tablegreenrow$css tabletext'>".$Feedback."</td>";
					$x .= "</tr>";
					$StudentIDArray[] = $StudentID;
				}
				$TargetStudentIDList = implode(",", $StudentIDArray);

				$EditButton = $linterface->GET_ACTION_BTN($eReportCard['EditMarksheet'], "button", "javascript:self.location.href='marksheet_edit1.php?$param&TargetStudentIDList=$TargetStudentIDList'")."&nbsp;";
			}
			else
			{
				$x .= "<tr><td colspan=3 class='tabletext' align='center'>".$i_no_record_exists_msg."</td></tr>";
			}
		}

################################################################

# tag information
$TAGS_OBJ[] = array($TagName, "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['StudentFeedback'], "");

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" action="feedback.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td id='top'><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1" align="center">
	<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
	<tr>
		<td colspan="2">
			<table width='100%' border='0' cellpadding="3" cellspacing="1">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $eReportCard['Class']; ?></td>
				<td class="tabletext" nowrap="nowrap"><?=$ClassName?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $eReportCard['Subject']; ?></td><td class="tabletext" nowrap="nowrap"><?=$SubjectName?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width=10 height=1></td></tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td colspan=4>
				<table border=0 cellpadding=0 cellspacing=0 width="100%">
				<td align='left' class="tabletext"><?=$DisplayModeFilter?></td>
				<td align='right' class="tabletext"><a class='tablegreenlink' href='#bottom'><?=$eReportCard['GoToBottom']?></a></td>
				</table>
			</td></tr>
			<tr>
				<td class='tabletopnolink tablegreentop'><?=$i_ClassNumber?></td>
				<td class='tabletopnolink tablegreentop'><?=$i_identity_student?></td>
				<td class='tabletopnolink tablegreentop'><?=$eReportCard['Appeal']?></td>
				<?=$StatusHeadingRow?>
			</tr>
			<?=$x?>
			</table>
		</td>
	</tr>
	<tr><td align='right'><a class='tablegreenlink' href='#top'><?=$eReportCard['GoToTop']?></a></td></tr>
	</table>
</td>
</tr>
<tr>
	<td id='bottom'><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
			<?=$EditButton?>
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='$BackPage'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
</form>
<p></p>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
