<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SubMarksheet'];

	if ($libreportcard->hasAccessRight())
    {
         $linterface = new interface_html("popup.html");
		
################################################################

		if(!empty($ColumnID))
		{
			list($ColumnTitle, $Weight) = $libreportcard->GET_SUB_MARKSHEET_COLUMN($ColumnID);
		}
		$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID";
################################################################
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['SubMarksheet'], "sub_marksheet.php?$param");
		$PAGE_NAVIGATION[] = (empty($ColumnID)) ? array($button_new, "") : array($button_edit, "");

		$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkform(obj){
	if(!check_text(obj.ColumnTitle, "<?php echo $i_alert_pleasefillin.$eReportCard['ColumnTitle']; ?>.")) return false;
    return true;
}

</script>
<form name="form1" action="ms_column_new_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="1" align="center">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['ColumnTitle']?> <span class="tabletextrequire">*</span></td><td class="tabletext" width="75%"><input type='text' name='ColumnTitle' class='tabletext' value="<?=$ColumnTitle?>" /></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Weight']?></td><td width="75%"><input type='text' name='Weight' value="<?=$Weight?>" class="textboxnum" />&nbsp;<span class="textboxnum">%</span></td></tr>
			</table>
		</td>
	</tr>
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
	</table>
</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="ColumnID" value="<?=$ColumnID?>" />
<input type="hidden" name="ReportColumnID" value="<?=$ReportColumnID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="TargetStudentIDList" value="<?=$TargetStudentIDList?>" />

<p></p>
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>