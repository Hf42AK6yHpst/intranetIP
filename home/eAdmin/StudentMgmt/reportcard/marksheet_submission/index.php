<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "MarksheetSubmission";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

		# check whether now is in marksheet submission period
		$StartDate = $libreportcard->StartDate;
		$EndDate = $libreportcard->EndDate;
		//$ValidPeriod = ($StartDate=="" || $EndDate=="") ? 0 : validatePeriod($StartDate, $EndDate);
		$ValidPeriod = $libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD();

		if($ValidPeriod)
		{
			$IsClassTeacher = $libreportcard->IS_CLASS_TEACHER();
			if($IsClassTeacher==1)
			{
				$ClassTeacherRow = "<tr><td align='right'>";
				$ClassTeacherRow .= ($libreportcard->AllowClassTeacherComment==1) ? "<a class='tablegreenlink' href=\"javascript:jGO_TEACHER_COMMENT()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_comment.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ClassTeacherComment']."</a>&nbsp;&nbsp;" : "";
				$ClassTeacherRow .= ($libreportcard->AllowClassTeacherUploadCSV==1) ? "<a class='tablegreenlink' href=\"javascript:jGO_TEACHER_MISC_CSV()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_upload.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['InfoUpload']."</a>&nbsp;&nbsp;" : "";
				$ClassTeacherRow .= "<a class='tablegreenlink' href=\"javascript:jGO_SCORE_LIST()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" border=\"0\" align=\"absmiddle\">".$eReportCard['ViewScoreList']."</a>";
				$ClassTeacherRow .= "</td></tr>";
			}

			$ReminderRow = "<tr><td class='tabletext' height='30' valign='top'>".$eReportCard['MarksheetConfirmationRemind']."</td></tr>";

			$PeriodTable = "<tr><td>";
			$PeriodTable .= "<table border='0' width='100%' cellpadding='5' cellspacing='0'>";
			$PeriodTable .= "<tr><td class=\"tabletext formfieldtitle\" nowrap='nowrap'>".$eReportCard['MarksheetSubmissionPeriod'].":</td></tr>";
			$PeriodTable .= "<tr><td class=\"tabletext formfieldtitle\" nowrap='nowrap'>".$eReportCard['StartDate']."</td><td class=\"tabletext\" nowrap='nowrap' width='70%'>".$StartDate."</td></tr>";
			$PeriodTable .= "<tr><td class=\"tabletext formfieldtitle\" nowrap='nowrap'>".$eReportCard['EndDate']."</td><td class=\"tabletext\" nowrap='nowrap' width='70%'>".$EndDate."</td></tr>";
			$PeriodTable .= "</table>";
			$PeriodTable .= "</td></tr>";
			$PeriodTable .= "<tr><td class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=10 height=1></td></tr>";

			$HeaderRow = "<tr class='tablegreentop'>
						<td class=\"tabletoplink\">".$eReportCard['Subject']."</td>
						<td class=\"tabletoplink\">".$eReportCard['Class']."</td>
						";
			if (!$ReportCardConfirmDisabled)
			{
				$HeaderRow .= "<td class=\"tabletoplink\">".$i_general_status."</td>";
			}
			$HeaderRow .= "<td class=\"tabletoplink\">".$eReportCard['Feedback']."</td>
						<td class=\"tabletoplink\">".$eReportCard['LastModifiedDate']."</td>
						<td class=\"tabletoplink\">".$eReportCard['LastModifiedBy']."</td>";
			$HeaderRow .= ($libreportcard->AllowSubjectTeacherComment==1) ? "<td class=\"tabletoplink\" width='55'>".$eReportCard['TeacherComment']."</td>" : "";
			$HeaderRow .= "</tr>";

			$Content = "<tr><td>";
			$Content .= "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
			$Content .= $HeaderRow;

			$SubjectClassArr = $libreportcard->GET_TEACHER_SUBJECT_CLASS();
			if(!empty($SubjectClassArr))
			{
				$SubjectCount = 0;
				foreach($SubjectClassArr as $SubjectID => $Data)
				{
					$css = ($SubjectCount%2?"2":"");
					$SubjectName = $Data["name"];
					$ClassArray = $Data["class"];
					$ClassSize = sizeof($ClassArray);

					if(!empty($ClassArray))
					{
						# Get Feedback Array
						$FeedbackCountArray = $libreportcard->GET_FEEDBACK_COUNT($SubjectID);

						# Get Notification Array
						$NotificationArray = $libreportcard->GET_CLASS_NOTIFICATION($SubjectID);

						# Get Last Modiifed Array
						$RelatedSubject = $libreportcard->GET_RELATED_SUBJECT($SubjectID);
						$SubjectList = (sizeof($RelatedSubject)>1) ? implode(",", $RelatedSubject) : $SubjectID;
						$LastModifiedArray = $libreportcard->GET_CLASS_MARKSHEET_LAST_MODIFIED($SubjectList);

						$ClassCount = 0;
						foreach($ClassArray as $ClassID => $ClassName)
						{
							$param = "SubjectID=$SubjectID&ClassID=$ClassID";

							# generate notification button
							if($NotificationArray[$ClassID]==1)
							{
								$NotifyBtn = "<td class=\"tabletext\"><font color=blue>".$eReportCard['Confirmed']."</font></td>";

								$FeedbackCount = ($FeedbackCountArray[$ClassID]=="") ? 0 : $FeedbackCountArray[$ClassID];
								$Feedback = $FeedbackCount." ".$eReportCard['Appeals'];
								$FeedbackBtn = "<td>".$linterface->GET_BTN($button_view, "button", "javascript:self.location.href='feedback.php?$param'", "", "title='".$Feedback."'")."</td>";
							}
							else
							{
								$NotifyBtn = "<td class=\"tabletext\">".$linterface->GET_BTN($eReportCard['ConfirmMarksheet'], "button", "javascript:jCONFIRM_MARKSHEET('$SubjectID', '$ClassID')")."</td>";

								$FeedbackBtn = "<td class=\"tabletext\">--</td>";
							}
							list($LastModifiedDate, $LastModifiedBy) = $LastModifiedArray[$ClassID];

							$Content .= "<tr class='tablegreenrow".$css."'>";
							$Content .= ($ClassCount==0) ? "<td rowspan='$ClassSize' valign='top'><span class=\"tabletext\">".$SubjectName."</span></td>" : "";
							$Content .= "<td><a href='marksheet_edit1.php?$param' class='tablegreenlink'>".$ClassName."</a></td>";
							if (!$ReportCardConfirmDisabled)
							{
								$Content .= $NotifyBtn;
							}
							$Content .= $FeedbackBtn;
							$Content .= "<td><span class=\"tabletext\">".($LastModifiedDate==""?"--":$LastModifiedDate)."</span></td>";
							$Content .= "<td><span class=\"tabletext\">".($LastModifiedBy==""?"--":$LastModifiedBy)."</span></td>";
							$Content .= ($libreportcard->AllowSubjectTeacherComment==1) ? "<td><span class=\"tabletext\"><a href='subject_teacher_comment.php?$param' class='tablegreenlink'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" border=\"0\" align=\"absmiddle\">".$button_edit."</a></td>" : "";
							$Content .= "</tr>";

							$ClassCount++;
						}
					}
					$SubjectCount++;
				}
			}
			else
			{
				$Content .= "<tr><td align=\"center\" class='tabletext tablegreenrow1' colspan=7>".$i_no_record_exists_msg."</td></tr>";
			}
			$Content .= "</table>";
			$Content .= "</td></tr>";
		}
		else
		{
			$Content = "<tr><td align=\"center\" class='tabletext tablegreenrow1'>".$eReportCard['MarksheetCollectionNotInPeriodMsg']."</td></tr>";
		}

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['MarksheetSubmission'], "", 0);

$linterface->LAYOUT_START();


?>
<SCRIPT LANGUAGE="Javascript">

function jCONFIRM_MARKSHEET(jSubjectID, jClassID)
{
	if(confirm("<?=$eReportCard['ConfirmMarksheetAlert']?>"))
	{
		self.location.href = "confirm_marksheet.php?SubjectID="+jSubjectID+"&ClassID="+jClassID;
	}
}

function jGO_SCORE_LIST()
{
	obj = document.form1;
	obj.action = "score_list.php";
	obj.target = "_blank";
	obj.submit();
}

function jGO_TEACHER_COMMENT()
{
	obj = document.form1;
	obj.action = "class_teacher_comment.php";
	obj.submit();
}

function jGO_TEACHER_MISC_CSV()
{
	obj = document.form1;
	obj.action = "../info_upload/index.php";
	obj.submit();
}

</SCRIPT>
<form name="form1" action="" method="GET">
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr><td align="right"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
		<?=$ReminderRow?>
		<?=$PeriodTable?>
		<?=$ClassTeacherRow?>
		<?=$Content?>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="ClassID" value="<?=$libreportcard->TeachingClassID?>" />
<input type="hidden" name="ClassName" value="<?=$libreportcard->TeachingClassName?>" />
</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>