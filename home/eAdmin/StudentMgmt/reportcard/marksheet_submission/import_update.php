<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_auth();

$limport = new libimporttext();

$param = "SubjectID=$SubjectID&ClassID=$ClassID&&ReportID=$ReportID&MarkType=$MarkType&HaveComponent=$HaveComponent&IsParent=$IsParent&ParentSubjectID=$ParentSubjectID&TargetStudentIDList=$TargetStudentIDList";

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename))
{
	header("Location: import.php?$param");
}
else
{
	intranet_opendb();

	$li = new libdb();
	$lo = new libfilesystem();
	$libreportcard = new libreportcard();
	
	# get class object
	$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
	list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

	/*
	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV")
	{
		# read file into array
		$data = $lo->file_read_csv($filepath);
		$HeaderRow = array_shift($data);	# drop the title bar
	}
	*/
	
	$data = $limport->GET_IMPORT_TXT($filepath);
	$HeaderRow = array_shift($data);		# drop the title bar
	
	# Get ReportSubjectID
	$ReportSubject = $libreportcard->GET_REPORT_SUBJECT_ID($ReportID, $SubjectID);
	$ReportSubjectID = $ReportSubject[0];

	$ColumnTitle = array("ClassName", "ClassNumber");
	$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
	$ColumnSize = sizeof($ColumnArray);
	for($i=0; $i<$ColumnSize; $i++)
	{
		$RCID = $ColumnArray[$i][0];
		$ReportCell = $libreportcard->GET_REPORT_CELL_BY_SUBJECT_COLUMN($ReportSubjectID, $RCID);
		if($ReportCell[$RCID]!="N/A")
		{
			$ColumnTitle[] = $ColumnArray[$i][1];
			$NewColumnArray[] = $ColumnArray[$i];
		}
	}
	$ColumnArray = $NewColumnArray;
	if($DefaultSetting!="S" && $ColumnSize>1)
		$ColumnTitle[] = $eReportCard['OverallResult'];

	for($j=0; $j<sizeof($HeaderRow); $j++)
	{
		if($HeaderRow[$j]!=$ColumnTitle[$j])
		{
			$FormatWrong = 1;
			break;
		}
	}
	
	if($FormatWong!=1)
	{
		$Year = $libreportcard->Year;
		$Semester = $libreportcard->Semester;
		for($i=0; $i<sizeof($data); $i++)
		{
			$data_obj = $data[$i];
			$cname = trim(array_shift($data_obj));
			$cnum = trim(array_shift($data_obj));

			if($cname!=$ClassName)
				continue;

			$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER WHERE ClassName = '$cname' AND (ClassNumber = '$cnum' OR CONCAT('0',ClassNumber) = '$cnum' or ClassNumber = '0".$cnum."')";
			$row = $li->returnArray($sql, 3);
			list($StudentID, $ClassName, $ClassNumber) = $row[0];

			if(!empty($StudentID) && !empty($data_obj))
			{
				$TotalPercent = 0;
				$WeightedMarkArray = array();
				$ResultNameField = ($DefaultSetting=="S") ? "Mark" : "Grade";
				
				for($j=0; $j<sizeof($ColumnArray); $j++)
				{
					list($ReportColumnID, $ColumnTitle, $Weight) = $ColumnArray[$j];

					$value = $data_obj[$j];
					if(strtolower($value)=="abs")
						$value = strtolower($value);

					# Remove all reocrds of student in the subject
					$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '$StudentID' AND SubjectID = '$SubjectID' AND ReportColumnID = '$ReportColumnID'";
					$li->db_db_query($sql);
					
					# Insert Raw Data
					if($DefaultSetting=="S")
					{
						# "-1" stand for "abs"; "-2" stand for "/"
						if($value=="abs")
							$value = "-1";
						else if(strcmp($value, "/")==0 || $value==="" || empty($value))
							$value = "-2";
					}
					else
						$value = ($value==="" || empty($value)) ? "/" : $value;

					
					# Insert Raw Result
					$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, ReportColumnID, $ResultNameField, TeacherID, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$StudentID', '$SubjectID', '$ReportColumnID', '$value', '$UserID', '".$Year."', '".$Semester."', '".$ClassName."', '".$ClassNumber."', now(), now())";
					$success = $li->db_db_query($sql);

					if($DefaultSetting=="S")
					{
						$WeightedMark = ($value>=0) ? ($value*$Weight/100) : $value;
						if($value!="-2")
						{
							$TotalPercent = $TotalPercent + $Weight;
							if($WeightedMark>=0)
								$WeightedMarkArray[] = $WeightedMark;
						}
						# Insert Weighted Result
						$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, ReportColumnID, $ResultNameField, TeacherID, IsWeighted, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$StudentID', '$SubjectID', '$ReportColumnID', '$WeightedMark', '$UserID', '1', '".$Year."', '".$Semester."', '".$ClassName."', '".$ClassNumber."', now(), now())";
						$success = $li->db_db_query($sql);
					}
				}
				
				if($DefaultSetting=="S")
				{
					if($ColumnSize>1)
					{
						$OverallResult = (!empty($WeightedMarkArray)) ? (array_sum($WeightedMarkArray)*100/$TotalPercent) : "-2";
					}
					else
					{
						$OverallResult = $WeightedMark;
					}
					$OverallFieldName = "Mark";
				}
				else
				{
					$OverallResult = ($ColumnSize>1) ? array_pop($data_obj) : $value;
					$OverallResult = ($OverallResult==="" || empty($OverallResult)) ? "/" : $OverallResult;
					$OverallFieldName = "Grade";
				}
	
				# Insert Overall Reocrds
				$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, $OverallFieldName, TeacherID, IsOverall, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$StudentID', '$SubjectID', '$OverallResult', '$UserID', '1', '".$Year."', '".$Semester."', '".$ClassName."', '".$ClassNumber."', now(), now())";
				$success = $li->db_db_query($sql);
			}				
		}
	}
}

$Result = ($success==1) ? "import_success" : "import_failed2";
intranet_closedb();

header("Location:marksheet_edit2.php?$param&Result=$Result");
?>

