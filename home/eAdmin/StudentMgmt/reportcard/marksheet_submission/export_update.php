<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$lexport = new libexporttext();

# Get SubjectName
$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);

# Get ReportSubjectName
$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);

if(!empty($TargetStudentIDList))
{
	$StudentIDList = $TargetStudentIDList;
}
else
{
	for($i=0; $i<sizeof($StudentArray); $i++)
	{
		$StudentIDArray[] = $StudentArray[$i][0];
	}
	if(!empty($StudentIDArray))
		$StudentIDList = implode(",", $StudentIDArray);
}

# Get ReportSubjectID
$ReportSubject = $libreportcard->GET_REPORT_SUBJECT_ID($ReportID, $SubjectID);
$ReportSubjectID = $ReportSubject[0];

$Content = "ClassName,ClassNumber";
$exportColumn[] = "ClassName";
$exportColumn[] = "ClassNumber";
$ColumnSize = sizeof($ColumnArray);
list($GradingSchemeID, $DefaultSetting) = $libreportcard->GET_GRADING_SCHEME_BY_SUBJECT_CLASS($SubjectID, $ClassID);
for($i=0; $i<$ColumnSize; $i++)
{
	list($RCID, $ColumnTitle, $Weight) = $ColumnArray[$i];
	$ReportCell = $libreportcard->GET_REPORT_CELL_BY_SUBJECT_COLUMN($ReportSubjectID, $RCID);
	if($ReportCell[$RCID]!="N/A")
	{
		$Content .= ",".$ColumnTitle;
		$exportColumn[] = $ColumnTitle;
		$NewColumnArray[] = $ColumnArray[$i];
	}
}
$Content .= (($DefaultSetting!="S" && $ColumnSize>1) || ($DefaultSetting=="S" && $MarkType==1)) ? ",".$eReportCard['OverallResult'] : "";
if (($DefaultSetting!="S" && $ColumnSize>1) || ($DefaultSetting=="S" && $MarkType==1)) {
	$exportColumn[] = $eReportCard['OverallResult'];
} else {
	$exportColumn[] = "";
}

$Content .= ($DefaultSetting=="S" && $MarkType==1) ? ",".$eReportCard['Grade'] : "";
/*
if ($DefaultSetting=="S" && $MarkType==1) {
	$exportColumn[] = $eReportCard['Grade'];
} else {
	$exportColumn[] = "";
}
*/

$Content .= "\n";
$ColumnArray = $NewColumnArray;

# Get Marksheet Result
$ReportType = $libreportcard->GET_REPORT_TYPE($ReportID);
$IsFullReport = ($ReportType=="F") ? 1 : 0;
$MarksheetResult = $libreportcard->GET_MARKSHEET_SCORE($ReportID, $SubjectID, $StudentIDList, $DefaultSetting, $MarkType, $IsFullReport);

$RoundDP = $libreportcard->GET_DECIMAL_POINT($ReportID);

for($j=0; $j<sizeof($StudentArray); $j++)
{
	list($StudentID, $WebSAMSRegNo, $Class, $StudentName) = $StudentArray[$j];
	if(!empty($StudentID))
	{
		$ClassName = $StudentArray[$j]['ClassTitleEn'];
		$ClassNumber = $StudentArray[$j]['ClassNumber'];
		//list($ClassName, $ClassNumber) = explode("-", $Class);
		$Content .= trim($ClassName).",".trim($ClassNumber);
		$row[] = trim($ClassName);
		$row[] = trim($ClassNumber);
		if(!empty($MarksheetResult))
		{
			for($k=0; $k<sizeof($ColumnArray); $k++)
			{
				$RCID = trim($ColumnArray[$k][0]);
				$Mark = trim($MarksheetResult[$StudentID][$RCID]);
				
				if($MarkType!=2)
				{
					if($DefaultSetting=="S")
						$Mark = round($Mark, $RoundDP);
					if($Mark<0)
						$Mark = $libreportcard->SpecialMarkArray[$Mark];
				}
				$Content .= ",".$Mark;
				$row[] = $Mark;
			}

			if(($DefaultSetting!="S" && $ColumnSize>1) || ($DefaultSetting=="S" && $MarkType==1))
			{
				$OverallMark = $MarksheetResult[$StudentID]["Overall"];
				
				if($MarkType!=2)
				{
					if($DefaultSetting=="S")
						$OverallMark = round($OverallMark, $RoundDP);
					if($OverallMark<0)
						$OverallMark = $libreportcard->SpecialMarkArray[trim($OverallMark)];
				}
				$Content .= ",".$OverallMark;
				$row[] = $OverallMark;
			}
			
			/*
			if($DefaultSetting=="S" && $MarkType==1)
			{
				$OverallGrade = $MarksheetResult[$StudentID]["OverallGrade"];
				$Content .= ",".$OverallGrade;
				$row[] = $OverallGrade;
			}
			*/
		}
		$Content .= "\n";
		$rows[] = $row;
		unset($row);
	}
}

intranet_closedb();

// Output the file to user browser
if($DefaultSetting=="S")
	$Prefix = ($MarkType==1) ? "Weighted_" : "Raw_";
$filename = str_replace("&nbsp;", "", $Prefix."Marksheet_".trim(str_replace("-", "_", $SubjectName))."_".trim($ClassName).".csv");

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($Content, $filename);

?>
