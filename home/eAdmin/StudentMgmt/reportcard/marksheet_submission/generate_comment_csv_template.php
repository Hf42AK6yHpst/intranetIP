<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();

$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);
$StudentSize = sizeof($StudentArray);

$exportColumn = array();
$exportColumn[] = 'ClassName';
$exportColumn[] = 'ClassNumber';
$exportColumn[] = 'Comment';
//$Content .= "ClassName,ClassNumber,Comment\n";

$ExportArr = array();
for($j=0; $j<$StudentSize; $j++)
{
	$StudentID = $StudentArray[$j][0];
	$RegNo = $StudentArray[$j]['ClassTitleEn'];
	$Class = $StudentArray[$j]['ClassNumber'];
	//$Content .= trim($RegNo).",".trim($Class)."\n";
	/*
	if(!empty($Class))
	{
		list($ClassName, $ClassNumber) = explode("-", $Class);
		debug($ClassName, $ClassNumber);
		$Content .= trim($ClassName).",".trim($ClassNumber)."\n";
	}
	*/
	
	$ExportArr[$j][] = $RegNo;
	$ExportArr[$j][] = $Class;
	$ExportArr[$j][] = '';
}

if($SubjectID!="")
{
	# Get Subject Name
	$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
	$SubjectName = str_replace("&nbsp;", "_", $SubjectName);
	$filename = "Comment_Template_".trim($SubjectName)."_".trim($ClassName).".csv";
}
else
{
	$filename = "Comment_Template_".trim($ClassName).".csv";
}

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

// Output the file to user browser
//output2browser($Content, $filename);

?>