<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$libreportcard = new libreportcard();
$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList";

# Get student with ClassName and ClassNumber
$StudentClassArray = $libreportcard->GET_CLASS_BY_USERID($StudentIDList);

$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
$ColumnSize = sizeof($ColumnArray);

if(!empty($StudentClassArray) && !empty($ColumnArray))
{
	$Year = $libreportcard->Year;
	$Semester = $libreportcard->Semester;

	# Remove Old record
	$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' AND StudentID IN ($StudentIDList)";
	$libreportcard->db_db_query($sql);
	
	if($SubmitType==2)	# Remove Main Marksheet Report Column Results
	{
		$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' AND StudentID IN ($StudentIDList)";
		$li->db_db_query($sql);
	}

	foreach($StudentClassArray as $StudentID => $ClassInfo)
	{
		list($ClassName, $ClassNumber) = $ClassInfo;

		$TotalMark = 0;
		$TotalWeight = 0;
		for($j=0; $j<$ColumnSize; $j++)
		{
			list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$j];

			//$mark = ${"mark_".$StudentID."_".$ColumnID};
			$long_value = ${"mark_long_".$StudentID."_".$ColumnID};
			$short_value = ${"mark_".$StudentID."_".$ColumnID};
			$mark = ($long_value==$short_value || ($long_value>0 && $short_value>0 && round($long_value, 1)==$short_value)) ? $long_value : $short_value;

			if($mark=="abs")
				$mark = "-1";
			else if(strcmp($mark, "/")==0 || $mark==="")
				$mark = "-2";
			else
				$TotalMark = $TotalMark + ($mark*$Weight/100);
			
			if($mark!="-2")
				$TotalWeight = $TotalWeight + $Weight;

			# Insert result
			$sql = "INSERT INTO RC_SUB_MARKSHEET_SCORE (ColumnID, SubjectID, ReportColumnID, StudentID, Mark, TeacherID, Year, Semester, DateInput, DateModified) VALUES ('$ColumnID', '$SubjectID', '$ReportColumnID', '$StudentID', '$mark', '$UserID', '$Year', '$Semester', now(), now())";
			$libreportcard->db_db_query($sql);
		}

		# Insert overall result
		if($SubmitType==0) {
			$OverallResult = ($TotalWeight>0) ? ($TotalMark*100/$TotalWeight) : "-2";
		}
		else {
			$overall_long_value = ${"overall_long_".$StudentID};
			$overall_short_value = ${"overall_".$StudentID};
			$OverallResult = ($overall_long_value==$overall_short_value || ($overall_long_value>0 && $overall_short_value>0 && round($overall_long_value, 1)==$overall_short_value)) ? $overall_long_value : $overall_short_value;

			if($OverallResult=="abs")
				$OverallResult = "-1";
			else if(strcmp($OverallResult, "/")==0 || $OverallResult==="")
				$OverallResult = "-2";
		}
		$sql = "INSERT INTO RC_SUB_MARKSHEET_SCORE (SubjectID, ReportColumnID, StudentID, Mark, TeacherID, Year, Semester, IsOverall, DateInput, DateModified) VALUES ('$SubjectID', '$ReportColumnID', '$StudentID', '$OverallResult', '$UserID', '$Year', '$Semester', 1, now(), now())";
		$success = $libreportcard->db_db_query($sql);

		# Insert overall weighted result
		if($SubmitType==0) {
			$OverallWeightedResult = ($OverallResult>0) ? ($OverallResult*$ColumnWeight/100) : "-2";
		}
		else {
			$overall_weighted_long_value = ${"overall_weighted_long_".$StudentID};
			$overall_weighted_short_value = ${"overall_weighted_".$StudentID};
			$OverallWeightedResult = ($overall_weighted_long_value==$overall_weighted_short_value || ($overall_weighted_long_value>0 && $overall_weighted_short_value>0 && round($overall_weighted_long_value, 1)==$overall_weighted_short_value)) ? $overall_weighted_long_value : $overall_weighted_short_value;
			
			if($OverallWeightedResult=="abs")
				$OverallWeightedResult = "-1";
			else if(strcmp($OverallWeightedResult, "/")==0 || $OverallWeightedResult==="")
				$OverallWeightedResult = "-2";
		}
		$sql = "INSERT INTO RC_SUB_MARKSHEET_SCORE (SubjectID, ReportColumnID, StudentID, Mark, TeacherID, Year, Semester, IsOverallWeighted, DateInput, DateModified) VALUES ('$SubjectID', '$ReportColumnID', '$StudentID', '$OverallWeightedResult', '$UserID', '$Year', '$Semester', 1, now(), now())";
		$success = $libreportcard->db_db_query($sql);
		
		if($SubmitType==2)	# Insert Main Marksheet Report Column Results
		{
			# Raw Mark
			$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, ReportColumnID, Mark, TeacherID, IsWeighted, IsOverall, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$StudentID', '$SubjectID', '$ReportColumnID', '$OverallResult', '$UserID', 0, 0, '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now())";
			//debug($sql);
			$li->db_db_query($sql);

			# Weighted Mark
			$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, ReportColumnID, Mark, TeacherID, IsWeighted, IsOverall, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$StudentID', '$SubjectID', '$ReportColumnID', '$OverallWeightedResult', '$UserID', 1, 0, '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now())";
			//debug($sql);
			$li->db_db_query($sql);
		}
	}
}

intranet_closedb();
/*
if($SubmitType==1)
{
	echo "<script language=javascript>self.close();</script>";
}
*/
if($SubmitType==2) {
	echo "<script language='javascript'>opener.location.reload();self.close();</script>";
}
else {
	$Result = ($success==1) ? "update" : "update_failed";
	header("Location:sub_marksheet.php?$param&Result=$Result");
}
?>

