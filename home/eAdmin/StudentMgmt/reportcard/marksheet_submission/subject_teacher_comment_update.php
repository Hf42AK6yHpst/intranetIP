<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$libreportcard = new libreportcard();

# Get Student of Class
$StudentArray = $libreportcard->GET_STUDENTID_BY_CLASS($ClassID);

if(is_array($StudentArray))
{
	$StudentList = implode(",", $StudentArray);
	$Year = $libreportcard->Year;
	$Semester = $libreportcard->Semester;
	
	# Get student with ClassName and ClassNumber
	$StudentClassArray = $libreportcard->GET_CLASS_BY_USERID($StudentList);

	# Remove old record
	$sql = "DELETE FROM RC_MARKSHEET_COMMENT WHERE StudentID IN ($StudentList) AND Year = '$Year' AND Semester = '$Semester' AND SubjectID = '$SubjectID'";
	$success = $li->db_db_query($sql);

	# Insert Record
	for($i=0; $i<sizeof($StudentArray); $i++)
	{
		$t_student_id = $StudentArray[$i];
		$t_comment = ${"Comment_".$t_student_id};
		$t_comment = intranet_htmlspecialchars($t_comment);
		list($ClassName, $ClassNumber) = $StudentClassArray[$t_student_id];
		
		# Insert Comment
		$sql = "INSERT INTO RC_MARKSHEET_COMMENT (StudentID, SubjectID, Comment, Year, Semester, ClassName, ClassNumber, TeacherID, DateInput, DateModified) VALUES ('$t_student_id', '$SubjectID', '$t_comment', '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', '$UserID', now(), now())";
		$success = $li->db_db_query($sql);
	}
}

$Result = ($success==1) ? "update" : "update_failed";
intranet_closedb();
header("Location:subject_teacher_comment.php?ClassID=$ClassID&SubjectID=$SubjectID&Result=$Result");
?>

