<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_auth();
intranet_opendb();

$limport = new libimporttext();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	//$CurrentPage = "MarksheetSubmission";
	if($ck_ReportCard_UserType=="ADMIN")
	{
		$CurrentPage = "MarksheetRevision";
		//$BackPage = "admin_index.php?SubjectID=$SubjectID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		//$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();

	if ($libreportcard->hasAccessRight())
    {
         $linterface = new interface_html();
		
		# get class object
		$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
		list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;
################################################################
		$param = "ClassID=$ClassID&SubjectID=$SubjectID";
		$ReturnPage = ($SubjectID!="") ? "subject_teacher_comment.php?".$param : "class_teacher_comment.php?".$param;
################################################################
	
		$TAGS_OBJ[] = array($TagName, "", 0);

		# page navigation (leave the array empty if no need)
		if($SubjectID!="")
		{
			$NavTagName = $eReportCard['ImportSubjectTeacherComment'];

			# Get Subject Name
			$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
			$SubjectRow = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['Subject']."</td><td class=\"tabletext\" width=\"70%\" nowrap=\"nowrap\">".$SubjectName."</td></tr>";
		}
		else
		{
			$NavTagName = $eReportCard['ImportClassTeacherComment'];
		}
		$PAGE_NAVIGATION[] = array($NavTagName, "");

		$linterface->LAYOUT_START();
?>
<form name="form1" action="teacher_comment_import_update.php" method="post" enctype="multipart/form-data">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1" align="center">
	<tr>
		<td colspan="2">
			<table width='100%' border='0' cellpadding="5" cellspacing="0">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Class']; ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$ClassName?></td></tr>
			<?=$SubjectRow?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['File']?></td>
			<td><input type="file" name="userfile" size=25><br />
			<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td>
			<td valign="top"><?=$linterface->GET_SMALL_BTN($eReportCard['DownloadCSVFile'], "button", "javascript:self.location.href='generate_comment_csv_template.php?$param'")?></td></tr>
			</table>
		</td>
	</tr>
	</table>
</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='$ReturnPage'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="HaveComponent" value="<?=$HaveComponent?>" />
<input type="hidden" name="DefaultSetting" value="<?=$DefaultSetting?>" />
<input type="hidden" name="TargetStudentIDList" value="<?=$TargetStudentIDList?>" />
<p></p>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.userfile") ?>

<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
