<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

$SubjectArray = $lreportcard->GET_ALL_SUBJECTS();
if(is_array($SubjectArray))
{
	foreach($SubjectArray as $SubjectID => $Data)
	{
		if(is_array($Data))
		{
			foreach($Data as $CmpSubjectID => $SubjectName)
			{
				$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
				$Scale = ${"Scale_".$SubjectID};
				$Scheme = ${"Scheme_".$SubjectID};
				$TmpArr = explode("_", $Scheme);
				$SchemeID = $TmpArr[0];
				
				# Step 1: retrieve and remove default subject grading ID
				$sql = "SELECT a.SubjectGradingID FROM RC_SUBJECT_GRADING_SCHEME as a LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID WHERE SubjectID = '$SubjectID' AND b.SubjectGradingID IS NULL";
				$SubjectGradingArr = $li->returnVector($sql);
				if(!empty($SubjectGradingArr))
				{
					$SubjectGradingIDs = implode(",", $SubjectGradingArr);

					# Step 2: remove old record
					$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME WHERE SubjectGradingID IN ($SubjectGradingIDs)";
					$li->db_db_query($sql);
				}

				# Step 3: add new record
				$sql = "INSERT INTO RC_SUBJECT_GRADING_SCHEME (SubjectID, SchemeID, Scale, DateInput, DateModified) Values('$SubjectID', '$SchemeID', '$Scale', now(), now()) ";
				$success = $li->db_db_query($sql);
				$lreportcard->UPDATE_CELL_SETTING($SubjectID, $Scale);
			}
		}
	}
}

intranet_closedb();

$Result = ($success==1) ? "update" : "update_failed";
header("Location: index.php?Result=$Result");
?>