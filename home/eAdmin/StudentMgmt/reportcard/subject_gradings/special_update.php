<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

if(empty($SubjectGradingID))
{
	if($SubjectID!="" && $Scheme!="" && !empty($Form))
	{
		$TmpArr = explode("_", $Scheme);
		$SchemeID = $TmpArr[0];
		
		# Step 1: Get related subject(s)
		$SubjectArray = $lreportcard->GET_RELATED_SUBJECT($SubjectID);
		
		# Insert Main Subject Record
		# Step 2.1: Insert Record RC_SUBJECT_GRADING_SCHEME
		$sql = "INSERT INTO RC_SUBJECT_GRADING_SCHEME (SubjectID, SchemeID, Scale, DateInput, DateModified) Values('".$SubjectArray[0]."', '$SchemeID', '$Scale', now(), now()) ";
		$li->db_db_query($sql);
		$ParentSubjectGradingID = $li->db_insert_id();

		# Step 2.2: Insert record to INSERT_SUBJECT_GRADING_FORM
		$success = $lreportcard->INSERT_SUBJECT_GRADING_FORM($ParentSubjectGradingID, $Form);
		
		# Insert Component Subject Record(s) if any
		for($i=1; $i<sizeof($SubjectArray); $i++)
		{
			$sid = $SubjectArray[$i];

			# Step 3.1: Insert record to RC_SUBJECT_GRADING_SCHEME
			$sql = "INSERT INTO RC_SUBJECT_GRADING_SCHEME (ParentSubjectGradingID, SubjectID, SchemeID, Scale, DateInput, DateModified) Values('$ParentSubjectGradingID', '$sid', '$SchemeID', '$Scale', now(), now()) ";
			$li->db_db_query($sql);
			$SubjectGradingID = $li->db_insert_id();
			
			# Step 3.2: Insert record to INSERT_SUBJECT_GRADING_FORM
			$success = $lreportcard->INSERT_SUBJECT_GRADING_FORM($SubjectGradingID, $Form);
			$lreportcard->UPDATE_CELL_SETTING($sid, $Scale);
		}
	}
	$Result = ($success==1) ? "add" : "add_failed";
}
else
{
	for($i=0; $i<sizeof($SubjectGradingID); $i++)
	{
		$sg_id = $SubjectGradingID[$i];
		$Scheme = ${"Scheme_".$sg_id};
		$TmpArr = explode("_", $Scheme);
		$SchemeID = $TmpArr[0];
		$Scale = ${"Scale_".$sg_id};
		
		$TempSql = "
						SELECT
								SubjectID
						FROM
								RC_SUBJECT_GRADING_SCHEME
						WHERE
								SubjectGradingID = '$sg_id'
					";
		$Temp = $li->returnVector($TempSql);
		$SubjectID = $Temp[0];

		# Update reocrd in RC_SUBJECT_GRADING_SCHEME
		$sql = "UPDATE RC_SUBJECT_GRADING_SCHEME SET SchemeID = '$SchemeID', Scale = '$Scale' WHERE SubjectGradingID = '$sg_id'";
		$success = $li->db_db_query($sql);
		$lreportcard->UPDATE_CELL_SETTING($SubjectID, $Scale);
	}
	$Result = ($success==1) ? "update" : "update_failed";
}
intranet_closedb();

header("Location: index.php?GradingType=1&Result=$Result");
?>