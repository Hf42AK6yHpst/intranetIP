<?php
// Using: 
/*****************************************
 *  Modification log
 * 
 * 	20160711 Bill	[2016-0203-1700-19207]
 * 		- display system msg to remind client to re-save marksheet and re-generate report after grading scheme updated
 * 
 * ***************************************/
 
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_SubjectGradings";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

############################################################################################################
// function to set the checked field and disabled field of scale and scheme settings
function SET_FIELDS($ParScheme, $ParScale)
{
	//$TempArr = split("_", $ParScheme);
    $TempArr = explode("_", $ParScheme);
	if($TempArr[1]=="H")
	{
		$PFDisabled = "DISABLED='DISABLED'";
		$ScoreDisabled = "";
		$GradeDisabled = "";
						
		$PFChecked = "";
		if($ParScale=="G")
		{
			$GradeChecked = "CHECKED='CHECKED'";
			$ScoreChecked = "";
		}
		else
		{
			$ScoreChecked = "CHECKED='CHECKED'";
			$GradeChecked = "";
		}
	}
	else
	{
		$ScoreDisabled = "DISABLED='DISABLED'";
		$GradeDisabled = "DISABLED='DISABLED'";
		$PFDisabled = "";

		$GradeChecked = "";
		$ScoreChecked = "";
		$PFChecked = "CHECKED='CHECKED'";
	}

	return array($ScoreChecked, $GradeChecked, $PFChecked, $ScoreDisabled, $GradeDisabled, $PFDisabled);
}

# Generate the scheme selection
$SchemeArr = $lreportcard->GET_ALL_SCHEMES();

$SchemeDetailsSelection = getSelectByArray($SchemeArr, "name='SchemeDetails' onChange='jVIEW_SCHEME_DETAILS(this.value)'", "", 0, 0, "-- ".$eReportCard['ViewGradingSchemeDetails']." --");

//id='SchemeSelection' style='display: none'
$SchemeSelection = getSelectByArray($SchemeArr, "name='SchemeID' onChange='jSELECT_ALL(this.value)'", "", 0, 0, "-- ".$eReportCard['SelectAllAs']." --");

# tag information
$TAGS_OBJ[] = array($eReportCard['DefaultSubjectGradings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/subject_gradings/?GradingType=0", $link0);
$TAGS_OBJ[] = array($eReportCard['SpecialSubjectGradings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/subject_gradings/?GradingType=1", $link1);


$linterface->LAYOUT_START();
echo "<div id='top'></div>";

if($GradingType!=1)
{
	# prepare javascript array and buttons for "select all" function
	echo "<script language=\"javascript\">jSubjectArray = new Array(); var i = 0;</script>";
	$ScoreScaleCheckAllBtn = "<br />".$linterface->GET_SMALL_BTN($button_check_all, "button", "jCHECK_ALL('S')");
	$GradeScaleCheckAllBtn = "<br />".$linterface->GET_SMALL_BTN($button_check_all, "button", "jCHECK_ALL('G')");
	$SchemeSelectAllBtn .= "<br />".$SchemeSelection;

	$SubjectArray = $lreportcard->GET_ALL_SUBJECTS();
	$ResultArray = $lreportcard->GET_SUBJECT_GRADINGS_RECORDS();
	$colspan = 6;
	$UpdatePage = "default_update.php";
	
	$rx = "";
	$count = 0;
	if(!empty($SubjectArray))
	{
		foreach($SubjectArray as $SubjectID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpSubjectID => $SubjectName)
				{	
					$css = ($count%2?"2":"");
					$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
					$SchemeFieldName = "Scheme_".$SubjectID;
					$ScaleFieldName = "Scale_".$SubjectID;
				
					if(!empty($ResultArray[$SubjectID]))
					{
						list($Scheme, $Scale) = $ResultArray[$SubjectID];
						if($Scale!="PF")
							echo "<script language=\"javascript\">jSubjectArray[i] = '".$SubjectID."'; i++</script>";
					}
					$Scheme = ($Scheme=="") ? $SchemeArr[0][0] : $Scheme;
					$SchemeSelection = getSelectByArray($SchemeArr, "name='".$SchemeFieldName."' class=\"SchemeSelection\" onChange='jCHANGE_SCHEME(this.value, this.name)'", $Scheme, 0, 1);
					
					# Set selected and disabled fields
					list($ScoreChecked, $GradeChecked, $PFChecked, $ScoreDisabled, $GradeDisabled, $PFDisabled) = SET_FIELDS($Scheme, $Scale);

					$rx .= "<tr class='tablegreenrow".$css."'>";
					$rx .= ($CmpSubjectID>0) ? "<td>&nbsp</td><td><span class=\"tabletext\">".$SubjectName."</span></td>" : "<td colspan=\"2\"><span class=\"tabletext\">".$SubjectName."</span></td>";
					$rx .= "<td><span class=\"tabletext\">".$SchemeSelection."</span></td>";
					$rx .= "<td align='center'><span class=\"tabletext\"><input type=\"radio\" name=\"".$ScaleFieldName."\" id='".$ScaleFieldName."_S' class=\"ScaleRadio_S\" value=\"S\" $ScoreChecked $ScoreDisabled /></span></td>";
					$rx .= "<td align='center'><span class=\"tabletext\"><input type=\"radio\" name=\"".$ScaleFieldName."\" id='".$ScaleFieldName."_G' class=\"ScaleRadio_G\" value=\"G\" $GradeChecked $GradeDisabled /></span></td>";
					$rx .= "<td align='center'><span class=\"tabletext\"><input type=\"radio\" name=\"".$ScaleFieldName."\" id='".$ScaleFieldName."_PF' class=\"ScaleRadio_PF\" value=\"PF\" $PFChecked $PFDisabled /></span></td>";
					$rx .= "</tr>";

					$flag = 1;
					$count++;
				}
			}
		}
	}
	else
	{
		$rx .= "<tr><td colspan=\"6\" align=\"center\" class=\"tabletext\">".$i_no_record_exists_msg."</td></tr>";
	}
}
else
{
	$ResultArray = $lreportcard->GET_SPECIAL_SUBJECT_GRADINGS_RECORDS();
	$SubjectGradingFormArray = $lreportcard->GET_SUBJECT_GRADING_FORM(1);
	$colspan = 8;
	$UpdatePage = "special_update.php";
	
	if(!empty($ResultArray))
	{
		$rx = "";
		$count = 0;
		foreach($ResultArray as $ParentSubjectGradingID => $Data)
		{
			if(!empty($Data))
			{
				foreach($Data as $SubjectGradingID => $Info)
				{
					list($SubjectID, $SubjectName, $Scheme, $Scale) = $Info;

					$css = ($count%2?"2":"");
					$SchemeFieldName = "Scheme_".$SubjectGradingID;
					$ScaleFieldName = "Scale_".$SubjectGradingID;

					$SchemeSelection = getSelectByArray($SchemeArr, "name='".$SchemeFieldName."' onChange='jCHANGE_SCHEME(this.value, this.name)'", $Scheme, 0, 1);
							
					# Set selected and disabled fields
					list($ScoreChecked, $GradeChecked, $PFChecked, $ScoreDisabled, $GradeDisabled, $PFDisabled) = SET_FIELDS($Scheme, $Scale);

					$rx .= "<tr class='tablegreenrow".$css."'>";
					$rx .= ($ParentSubjectGradingID!=$SubjectGradingID) ? "<td>&nbsp</td><td><span class=\"tabletext\">".$SubjectName."</span></td>" : "<td colspan=\"2\"><span class=\"tabletext\">".$SubjectName."</span></td>";
					$rx .= "<td><span class=\"tabletext\"><a href=\"javascript:newWindow('form_edit.php?SubjectGradingID=$SubjectGradingID&SubjectID=$SubjectID', 1)\" class='tablegreenlink'>".implode(", ", $SubjectGradingFormArray[trim($SubjectGradingID)])."</a></span></td>";
					$rx .= "<td><span class=\"tabletext\">".$SchemeSelection."</span></td>";
					$rx .= "<td align='center'><span class=\"tabletext\"><input type=\"radio\" name=\"".$ScaleFieldName."\" id='".$ScaleFieldName."_S' value=\"S\" $ScoreChecked $ScoreDisabled /></span></td>";
					$rx .= "<td align='center'><span class=\"tabletext\"><input type=\"radio\" name=\"".$ScaleFieldName."\" id='".$ScaleFieldName."_G' value=\"G\" $GradeChecked $GradeDisabled /></span></td>";
					$rx .= "<td align='center'><span class=\"tabletext\"><input type=\"radio\" name=\"".$ScaleFieldName."\" id='".$ScaleFieldName."_PF' value=\"PF\" $PFChecked $PFDisabled /></span></td>";
					$rx .= "<td class=\"tabletext\">".$linterface->GET_SMALL_BTN($button_remove, "button", "javascript:jREMOVE($SubjectGradingID)")."</td>";
					$rx .= "</tr>";
					$rx .= "<input type='hidden' name='SubjectGradingID[]' value='".$SubjectGradingID."' />";
					
					$count++;
				}
			}
		}
	}
	else
	{
		$rx .= "<tr><td colspan=\"".$colspan."\" align=\"center\" class=\"tabletext\">".$i_no_record_exists_msg."</td></tr>";
	}
	
	$FormColumn = "<td class='tableTitle'><span class='tabletoplink'>".$eReportCard['Forms']."</span></td>";
	$RemoveColumn = "<td class='tableTitle'>&nbsp;</td>";

	$rx .= "<tr><td class='tabletext' colspan='".$colspan."'>".$linterface->GET_SMALL_BTN($eReportCard['AddNewRecord'], "button", "javascript:self.location='special_add.php'")."</td></tr>";
}
############################################################################################################

if($GradingType==1)
{
	$link0 = 0;
	$link1 = 1;
}
else
{
	$link0 = 1;
	$link1 = 0;
}

// [2016-0203-1700-19207] Message after update
$Result2 = "";
if(trim($Result)=="update"){
	$Result = "";
	$Result2 = $eReportCard["NeedToReGenAfterUpdate"];
}

?>

<script language="javascript">
<!--
//var jSubjectArray = new Array();

function jCHANGE_SCHEME(jScheme, jElementName){

	var jSchemeArr = jScheme.split("_");
	var jSchemeType = jSchemeArr[1];

	var jElementArr = jElementName.split("_");
	var jItemID = jElementArr[1];
	var jScaleName = "Scale_"+jItemID;

	var jScalseScore = document.getElementById(jScaleName+"_S");
	var jScalseGrade = document.getElementById(jScaleName+"_G");
	var jScalsePF = document.getElementById(jScaleName+"_PF");
	
	if(jSchemeType=="PF")
	{
		jScalsePF.disabled = false;
		jScalsePF.checked = true;
		
		jScalseScore.checked = false;
		jScalseScore.disabled = true;

		jScalseGrade.checked = false;
		jScalseGrade.disabled = true;
	}
	else
	{
		jScalsePF.checked = false;
		jScalsePF.disabled = true;

		jScalseScore.checked = true;
		jScalseScore.disabled = false;

		jScalseGrade.checked = false;
		jScalseGrade.disabled = false;
	}
}

function jVIEW_SCHEME_DETAILS(jScheme)
{
	if(jScheme!="")
	{
		var jSchemeArr = jScheme.split("_");
		var jSchemeID = jSchemeArr[0];
		newWindow("scheme_details.php?SchemeID="+jSchemeID, 5);
		document.form1.SchemeDetails.value = "";
	}
}

function jREMOVE(jSubjectGradingID)
{
	var alertConfirmRemove = (jSubjectGradingID=="ALL") ? "<?=$eReportCard['RemoveAllItemAlert']?>" : "<?=$eReportCard['RemoveItemAlert']?>";
	if(confirm(alertConfirmRemove)){
		self.location.href='special_remove.php?SubjectGradingID=' + jSubjectGradingID;
	}
}

function jCHECK_ALL(jType)
{	
//	var jValue, jValueObj;
//	for(var j=0; j<jSubjectArray.length; j++)
//	{
//		jSubjectID = jSubjectArray[j];
//		jValueObj = eval("document.getElementById('Scale_"+jSubjectID+"_"+jType+"')");
//		jValueObj.checked = true;
//	}

	$('input.ScaleRadio_' + jType + ':enabled').attr('checked', 'checked');
}

function jSELECT_ALL(jSchemeID)
{
//	var jValue, jValueObj;
//	for(var j=0; j<jSubjectArray.length; j++)
//	{
//		jSubjectID = jSubjectArray[j];
//		jValueObj = eval("document.getElementById('Scheme_"+jSubjectID+"')");
//		jValueObj.value = jSchemeID;
//	}
//	document.form1.SchemeID.value = "";

	$('select.SchemeSelection').val(jSchemeID);
}
//-->
</script>
<br/>
<form name="form1" method="POST" action="<?=$UpdatePage?>">
<table width="90%" border="0" cellpadding="4" cellspacing="0">
<tr><td align="right" colspan="<?=$colspan?>"><?= $linterface->GET_SYS_MSG($Result, $Result2); ?></td></tr>
<tr>
	<td colspan="<?=($colspan-1)?>"><?=$SchemeDetailsSelection?></td>
	<td align='right' class='tabletext' nowrap='nowrap'><a class='tablegreenlink' href='#bottom'><?=$eReportCard['GoToBottom']?></a></td>
</tr>
<tr class='tablegreentop'>
<td class="tableTitle" colspan="2"><span class="tabletoplink"><?=$i_Homework_subject?></span></td>
<?=$FormColumn?>
<td class="tableTitle"><span class="tabletoplink"><?=$eReportCard['GradingScheme']?><?=$SchemeSelectAllBtn?></span></td>
<td class="tableTitle" align='center'><span class="tabletoplink"><?=$eReportCard['ScoreScale']?><?=$ScoreScaleCheckAllBtn?></span></td>
<td class="tableTitle" align='center'><span class="tabletoplink"><?=$eReportCard['GradeScale']?><?=$GradeScaleCheckAllBtn?></span></td>
<td class="tableTitle" align='center'><span class="tabletoplink"><?=$eReportCard['PassFail']?></span></td>
<?=$RemoveColumn?>
</tr>
<?=$rx?>
<tr>
	<td align='right' class='tabletext' nowrap='nowrap' colspan="<?=$colspan?>"><a class='tablegreenlink' href='#top'><?=$eReportCard['GoToTop']?></a></td>
</tr>
<tr><td class="dotline" colspan="<?=$colspan?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="<?=$colspan?>">
<div style="padding-top: 5px"  id='bottom'>
<?= $linterface->GET_ACTION_BTN($button_update, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "button", "self.location.reload()")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="flag" value="1">
<input type="hidden" name="GradingType" value="<?=$GradingType?>">

</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>