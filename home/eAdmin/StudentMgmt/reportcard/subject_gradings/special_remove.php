<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

if($SubjectGradingID=="ALL")
{
	# To remove all records, retrieve all SubjectGradingID from RC_SUBJECT_GRADING_SCHEME_FORM
	$sql = "SELECT DISTINCT SubjectGradingID FROM RC_SUBJECT_GRADING_SCHEME_FORM";
	$SubjectGradingIDArray = $li->returnVector($sql);
}
else
{
	# Get component subject if any
	$sql = "SELECT SubjectGradingID FROM RC_SUBJECT_GRADING_SCHEME WHERE ParentSubjectGradingID = '".$SubjectGradingID."'";
	$CmpArray = $li->returnVector($sql);
	$SubjectGradingIDArray = (!empty($CmpArray)) ? array_merge(array($SubjectGradingID), $CmpArray) : array($SubjectGradingID);
}

for($i=0; $i<sizeof($SubjectGradingIDArray); $i++)
{
	$sg_id = $SubjectGradingIDArray[$i];

	# Step1: remove record in RC_SUBJECT_GRADING_SCHEME
	$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME WHERE SubjectGradingID = '$sg_id'";
	$success = $li->db_db_query($sql);
		
	if($success==1)
	{
		# Step2: remove record(s) in RC_SUBJECT_GRADING_SCHEME_FORM
		$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME_FORM WHERE SubjectGradingID = '$sg_id'";
		$li->db_db_query($sql);
	}
}

intranet_closedb();

$Result = ($success==1) ? "delete" : "delete_failed";
header("Location: index.php?GradingType=1&Result=$Result");
?>