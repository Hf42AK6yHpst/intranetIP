<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

# Insert record to RC_MARKSHEET_FEEDBACK
if(!empty($SubjectID))
{
	$libreportcard = new libreportcard();

	$sql = "INSERT INTO RC_MARKSHEET_FEEDBACK (SubjectID, StudentID, IsComment, Year, Semester, DateInput, DateModified) VALUES ('$SubjectID', '$UserID', 0, '".$libreportcard->Year."', '".$libreportcard->Semester."', now(), now())";
	$success = $li->db_db_query($sql);
}

intranet_closedb();
$Result = ($success==1) ? "update" : "update_failed";
header("Location: index.php?Result=$Result");
?>
