<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

if($ReportID!="" && $SubjectID!="")
{
	############################### STEP 1: Check the type of the Subject ################################
	### 1) Non-component subject without component subject
	### 2) Non-Component subject with component subject
	### 3) Component subject that parent subject without seleted
	### 4) Component subject that parent subject seleted
	
	# Retreive related subjects of the target subjects and all selected subjects
	$SubjectArray = $lreportcard->GET_RELATED_SUBJECT($SubjectID);
	$SelectedSubjectIDArray = $lreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);

	# Check subject types
	if($SubjectArray[0]!=$SubjectID)	// Component Subject
	{
  		$ParentSubjectID = $SubjectArray[0];	
			$ParentExist = ($ParentSubjectID>0 && is_array($SelectedSubjectIDArray) && in_array($ParentSubjectID, $SelectedSubjectIDArray)) ? 1 : 0;		// Parent subject selected: 1/0
	}
	else if(sizeof($SubjectArray)>1 && is_array($SelectedSubjectIDArray) && in_array($SubjectID, $SelectedSubjectIDArray))
	{
			$ParentSubjectID = $SubjectArray[0];
			$ParentExist = 1;
	}
	else
	{
			$ParentSubjectID = 0;
			$ParentExist = 0;  	  
	}

	############################ STEP 2: Set the Display Order ##############################
	# Step Retreive Last Display Order
	$sql = "SELECT MAX(DisplayOrder) FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportID = '$ReportID';";
	$row = $li->returnVector($sql);
	$LastOrder = $row[0];
	
	if($LastOrder=="")	// No selected subject
	{
		$DisplayOrder = 1;
	}
	else
	{
		# Step Get DisplayOrder List
		$sql = "SELECT 
					a.ReportSubjectID,
					a.SubjectID,
					c.RecordID,
					a.DisplayOrder
				FROM 
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
					LEFT JOIN ASSESSMENT_SUBJECT as c ON b.CODEID = c.CODEID AND (c.CMP_CODEID IS NULL Or c.CMP_CODEID = '')
				WHERE 
					a.ReportID = '$ReportID' 
					And
					b.RecordStatus = 1
					And
					c.RecordStatus = 1
				ORDER BY 
					a.DisplayOrder
				";
		$row = $li->returnArray($sql, 4);
	
		for($i=0; $i<sizeof($row); $i++)
		{
			list($RSID, $SID, $ParentSID, $DisplayOrder) = $row[$i];
			if($InsertAfterID==$RSID)
			{
				$AfterOrder = $DisplayOrder;  // New order should be after this order
			}
			$RSOrderArr[$DisplayOrder] = array($RSID, $SID, $ParentSID);
			
			# Array for handling component subject that parent subject exist
			if($ParentSubjectID>0 && $ParentExist==1 && $ParentSubjectID==$ParentSID)
			{
				$RSArr[$DisplayOrder] = $RSID;
			}
		}
		if(is_array($RSOrderArr))
			$OrderArr = array_keys($RSOrderArr);
		
		# Handle the InsertAfterID of the component subject that parent subject exist.
		if($ParentSubjectID>0 && $ParentExist==1 && is_array($RSArr) && !in_array($InsertAfterID, $RSArr))
		{
			$AfterOrder = max(array_keys($RSArr));
			$InsertAfterID = $RSArr[$AfterOrder];
		}

		# Get DisplayOrder
		$Reorder = 1;
		if($InsertAfterID!="" && is_array($OrderArr))
		{
			$DisplayOrder = -1;
			$TempOrderArray = array_slice($OrderArr, array_search($AfterOrder, $OrderArr));
			array_shift($TempOrderArray);		// Remove the first element which is the AfterOrder
		
			while($AfterOrder<$LastOrder)
			{
				$AfterOrder = array_shift($TempOrderArray);
				list($RSID, $SID, $ParentSID) = $RSOrderArr[$AfterOrder];
				
				if($ParentSID==$SID || ($ParentSID!=$SID && $ParentSID==$ParentSubjectID))
				{	
					$DisplayOrder = $AfterOrder;
					break;				
				}
			}
		}
		else	// The new order is in the beginning
		{
			$DisplayOrder = 1;
		}

		if($DisplayOrder<0)		 // The new order is in the end
		{
			$DisplayOrder = $LastOrder+1;
			$Reorder = 0;
		}
	}

	####################### STEP 3: Insert Record  ###############################
	# Insert record into RC_REPORT_TEMPLATE_SUBJECT
	if(is_array($SubjectArray) && is_array($SelectedSubjectIDArray))
		$SubjectArray = array_diff($SubjectArray, $SelectedSubjectIDArray);
	
	# Postion options
	/*
	$ShowPosClassColumn = ($ShowPosClassColumn==="") ? 0 : $ShowPosClassColumn;
	$ShowPosFormColumn = ($ShowPosFormColumn==="") ? 0 : $ShowPosFormColumn;
	$ClassPosRange = ($ShowPosClassColumn==1) ? $ClassPosFrom.",".$ClassPosTo : "";
	$FormPosRange = ($ShowPosFormColumn==1) ? $FormPosFrom.",".$FormPosTo : "";
	*/
	$HidePosition = ($HidePosition==="") ? 0 : $HidePosition;
	
	$FinalDisplayOrder = $DisplayOrder;
	foreach($SubjectArray as $key => $sid)
	{
		$sql = "INSERT INTO RC_REPORT_TEMPLATE_SUBJECT (ReportID, SubjectID, DisplayOrder, HidePosition, DateInput, DateModified) VALUES ('$ReportID', '$sid', '$FinalDisplayOrder', '$HidePosition', now(), now())";
		$success = $li->db_db_query($sql);
		$FinalDisplayOrder++;
	}
	
	####################### STEP 4: Reordering  ###############################
	if($Reorder!=0 && is_array($OrderArr))
	{
		$NewOrder = $FinalDisplayOrder;
		$StartPos = ($DisplayOrder==1) ? 0 : array_search($DisplayOrder, $OrderArr);
		$SortingOrderArray = array_slice($OrderArr, $StartPos);

		for($i=0; $i<sizeof($SortingOrderArray); $i++)
		{
			$Order = $SortingOrderArray[$i];
			$rsid = $RSOrderArr[$Order][0];
			$sql = "UPDATE RC_REPORT_TEMPLATE_SUBJECT SET DisplayOrder = '".$NewOrder."' WHERE ReportSubjectID = '".$rsid."'";
			$li->db_db_query($sql);
			$NewOrder++;
		}
	}
	######################################################################################
}
intranet_closedb();

$Result = ($success==1) ? "add" : "add_failed";
header("Location:subject_manage.php?Result=$Result&ReportID=$ReportID");
?>
