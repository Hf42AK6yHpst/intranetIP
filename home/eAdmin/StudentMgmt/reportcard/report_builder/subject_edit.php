<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_Template";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SubjectManage'];

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html("popup.html");

################################################################
		
		$ReportSubjectID = (is_array($ReportSubjectID)) ? $ReportSubjectID[0] : $ReportSubjectID;
		
		# Get the subject name
		$sql = "SELECT
					a.SubjectID,
					CONCAT(b.EN_DES, ' (', b.CH_DES, ')'),
					CONCAT(c.EN_DES, ' (', c.CH_DES, ')'),
					a.DisplayOrder,
					a.HidePosition 
				FROM 
					RC_REPORT_TEMPLATE_SUBJECT as a 
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
					LEFT JOIN ASSESSMENT_SUBJECT as c ON b.CODEID = c.CODEID AND (c.CMP_CODEID IS NULL Or c.CMP_CODEID = '')
				WHERE
					a.ReportSubjectID = '$ReportSubjectID'
					And
					b.RecordStatus = 1
					And
					c.RecordStatus = 1
				";
		$row = $lreportcard->returnArray($sql, 5);

		list($SubjectID, $SubjectName, $ParentSubjectName, $DisplayOrder, $HidePosition) = $row[0];

		$SubjectName = ($SubjectName==$ParentSubjectName) ? $SubjectName : $ParentSubjectName."&nbsp;&nbsp;-&nbsp;&nbsp;".$SubjectName;
		
		# Get the insert subject seletion
		$InsertSelect = $lreportcard->GET_REPORT_SUBJECT_INSERT_SELECTION($ReportID, $ReportSubjectID);
		
		/*
		if($ClassPosRange!="")
		{
			$TmpArr = explode(",", $ClassPosRange);
			$ClassPosFrom = $TmpArr[0];
			$ClassPosTo = $TmpArr[1];
		}
		if($FormPosRange!="")
		{
			$TmpArr = explode(",", $FormPosRange);
			$FormPosFrom = $TmpArr[0];
			$FormPosTo = $TmpArr[1];
		}
		
		# Position in class Column
		$PosClassColumn = $eReportCard['ShowOverallPosFrom']."&nbsp;<input type='text' name='ClassPosFrom' class='textboxnum' value='".$ClassPosFrom."'/>&nbsp;".$i_To."&nbsp;<input type='text' name='ClassPosTo' class='textboxnum' value='".$ClassPosTo."'/>";
		
		# Position in form Column
		$PosFormColumn = $eReportCard['ShowOverallPosFrom']."&nbsp;<input type='text' name='FormPosFrom' class='textboxnum' value='".$FormPosFrom."'/>&nbsp;".$i_To."&nbsp;<input type='text' name='FormPosTo' class='textboxnum' value='".$FormPosTo."'/>";
		*/

################################################################
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['SubjectManage'], "subject_manage.php?ReportID=$ReportID");
		$PAGE_NAVIGATION[] = array($button_edit, "");

		$linterface->LAYOUT_START();
?>
<form name="form1" action="subject_edit_update.php" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Homework_subject?></td><td class="tabletext" width="75%"><?=$SubjectName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['InsertAfter']?></td><td class="tabletext" width="75%"><?=$InsertSelect?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="tabletext" colspan=2><input type="checkbox" name="HidePosition" id="HidePosition" value="1" <?=($HidePosition==1?"CHECKED='CHECKED'":"")?> />&nbsp;<label for="HidePosition"><?=$eReportCard['HidePosition']?></label></td></tr>
			<!--
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['ClassPosition']?></td><td class="tabletext" width="75%"><input type="checkbox" name="ShowPosClassColumn" id="ShowPosClassColumn" value="1" <?=($ShowPosClassColumn==1?"CHECKED='CHECKED'":"")?> /><label for="ShowPosClassColumn"><?=$PosClassColumn?></label></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['FormPosition']?></td><td class="tabletext" width="75%"><input type="checkbox" name="ShowPosFormColumn" id="ShowPosFormColumn" value="1" <?=($ShowPosFormColumn==1?"CHECKED='CHECKED'":"")?> /><label for="ShowPosFormColumn"><?=$PosFormColumn?></label></td></tr>
			-->
			</table>
		</td>
	</tr>
	</table>

</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='subject_manage.php?ReportID=$ReportID'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ReportSubjectID" value="<?=$ReportSubjectID?>" />
<input type="hidden" name="OldDisplayOrder" value="<?=$DisplayOrder?>" />

<p></p>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.InsertAfterID") ?>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>