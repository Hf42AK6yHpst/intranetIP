<?php
/*
 * Subject Unit Setting Array - defined in Get_CLASS_LEVEL_SUBJECT_UNIT()
 * Need to update /report_generation/print_report_functions2.php
 */

$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

############################ Functions Start #############################

/*
* Generate signature table
*/
function GENERATE_SIGNATURE_TABLE()
{
	global $SignatureTitleArray, $SettingSignatureArray, $LangArray;
	global $SignatureWidth;

	$CellNumber = 0;
	for($k=0; $k<sizeof($SignatureTitleArray); $k++)
	{
		$bottom_border = $k == (sizeof($SignatureTitleArray) - 1)? " " : "report_formfieldtitle";
		$SettingID = trim($SignatureTitleArray[$k]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			if ($Title=="Class Teacher")
			{
				$Title .= "'s Signature";
			}
			$SignatureRow .= "<tr>";
			//$SignatureRow .= "<td class='small_title ".$bottom_border."' valign='top' height='75'><span style='width:{$SignatureWidth}px'>".$Title."</span></td>";
			$SignatureRow .= "<td class='small_title report_formfieldtitle' valign='top' height='150'><span style='width:{$SignatureWidth}px'>".$Title."</span></td>";
			$SignatureRow .= "</tr>";

			$CellNumber++;
		}
	}
	
	if($CellNumber>0)
	{
		$SignatureRow .= "<tr>";
		$SignatureRow .= "<td class='small_title' valign='top' height='5'>";
			$SignatureRow .= "<span style='width:{$SignatureWidth}px'>";
				$SignatureRow .= "*Maximum = 100 <br>";
				$SignatureRow .= "<span style='visibility: hidden;'>*</span>Pass = 50 <br>";
				$SignatureRow .= "<span style='visibility: hidden;'>*</span>Fail : Below 50";
			$SignatureRow .= "</span>";
		$SignatureRow .= "</td>";
		$SignatureRow .= "</tr>";
		
		$SignatureTable = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0' align='center' valign='top' style='min-height:100%;'>";
		$SignatureTable .= $SignatureRow;
		$SignatureTable .= "</table>";
	}

	return $SignatureTable;
}

/*
* Generate attendance table
*/
function GENERATE_ATTENDANCE_AND_MERIT_TABLE()
{
	global $SummaryTitleArray, $SettingSummaryArray, $SettingMiscArray, $ColumnArray, $LangArray, $SemesterArray;
	global $DaysAbsentTitle, $TimesLateTitle, $AbsentWOLeaveTitle;

	$AttendanceTableShow = ($SettingSummaryArray["DaysAbsent"]==1 || $SettingSummaryArray["TimesLate"] || $SettingSummaryArray["AbsentWOLeave"]);
	$MeritTableShow = ($SettingMiscArray["MeritsAndDemerits"]==1);

	$SemColumnCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$sem_title = trim($ColumnArray[$i]["ColumnTitle"]);
		if ($sem_title!="") {
			if(in_array($sem_title, $SemesterArray))
			{
				$SemesterHeaderRow .= ($AttendanceTableShow==1 || $MeritTableShow==1) ? "<td class='reportcard_text border_top border_left' align='center'>".$sem_title."</td>\n" : "";

				$DaysAbsentCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				
				$LateCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				
				$AbsentWOLeaveCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				
				$MeritCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				
				$DemeritCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				
				$SemColumnCount++;
			}
		}
	}

	if($SemColumnCount>0 && ($AttendanceTableShow==1 || $MeritTableShow==1))
	{
		$ReturnTable = "<table width='100%' valign='bottom' border='0' cellspacing='0' cellpadding='1'>\n";
		if($AttendanceTableShow==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Attendance']."</td>\n";
			$ReturnTable .= $SemesterHeaderRow;
		}

		# Rows of Attendance records
		if($SettingSummaryArray["DaysAbsent"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$DaysAbsentTitle."</td>\n";
			$ReturnTable .= $DaysAbsentCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["TimesLate"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$TimesLateTitle."</td>\n";
			$ReturnTable .= $LateCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["AbsentWOLeave"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$AbsentWOLeaveTitle."</td>";
			$ReturnTable .= $AbsentWOLeaveCell;
			$ReturnTable .= "</tr>";
		}
		
		# Row of Merit records
		if($MeritTableShow==1)
		{
			if($AttendanceTableShow==1) {
				$ReturnTable .= "<tr><td class='reportcard_text border_top' colspan='".($SemColumnCount+1)."'>".$LangArray['MeritsAndDemerits']."</td></tr>\n";
			}
			else {
				$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['MeritsAndDemerits']."</td>\n";
				$ReturnTable .= $SemesterHeaderRow;
			}

			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Merit']."</td>\n";
			$ReturnTable .= $MeritCell;
			$ReturnTable .= "</tr>";
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Demerit']."</td>\n";
			$ReturnTable .= $DemeritCell;
			$ReturnTable .= "</tr>";
		}
		
		$ReturnTable .= "</table>";
	}
	return $ReturnTable;
}

/*
* Get summary array
*/
function GET_SUMMARY_ARRAY()
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $HideOverallResult;
	global $libreportcard;

	$ReturnArray = "";

	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			if($SettingSummaryArray[$SettingID]==1 &&
			  	$SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave" && $SettingID!="AbsentWOReason" &&
				$SettingID!="Merit" && $SettingID!="Demerit" && $SettingID!="Conduct" &&
				$SettingID!="Politeness" && $SettingID!="Behaviour" && $SettingID!="Application" &&
				$SettingID!="Tidiness" &&

				$SettingID!="Motivation" &&$SettingID!="SelfConfidence" &&$SettingID!="SelfDiscipline" &&
				$SettingID!="Courtesy" &&$SettingID!="Honesty" &&$SettingID!="Responsibility" &&
				$SettingID!="Cooperation" &&
				
				$SettingID!="Merits" &&$SettingID!="Attendance" &&$SettingID!="Competitions" &&
				$SettingID!="Performances" &&$SettingID!="Services" &&
				
				$SettingID!="Demerits" &&$SettingID!="Forgetfulness" &&$SettingID!="Tardiness" &&
				$SettingID!="Misbehaviour" &&
				
				$SettingID!="Offence" && $SettingID!="TardinessOffence"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				switch ($SettingID) {
					case "GrandTotal":
					case "GPA":
					case "AverageMark":
					case "ClassHighestAverage":
						$Display = "S";
						break;
					case "ClassPupilNumber":
					case "ClassPosition":
					case "FormPosition":
					
					//case "Attendance":
					case "DaysAbsent":
					case "TimesLate":
					case "AbsentWOLeave":
					//case "MeritsAndDemerits":
					case "Merit":
					case "Demerit":
						$Display = "#";
						break;
					case "Conduct":
					case "Politeness":
					case "Behaviour":
					case "Application":
					case "Tidiness":
						$Display = "G";
						break;
					default:
						$Display = "#";
						break;
				}
				
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					$ReturnArray[$count][$column] = $Display;
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
					$ReturnArray[$count][$Semester] = $Display;

				$count++;
			}
		}
	}
	return $ReturnArray;
}


/*
* Get summary array
*/
function GET_CONDUCT_ARRAY()
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $HideOverallResult;

	$ReturnArray = "";

	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		# Row - Settings Type
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if($SettingSummaryArray[$SettingID]==1 &&
				$SettingID!="GrandTotal" && $SettingID!="GPA" && $SettingID!="AverageMark" &&
				$SettingID!="ClassHighestAverage" && $SettingID!="ClassPupilNumber" &&
				$SettingID!="ClassPosition" && $SettingID!="FormPosition" && $SettingID!="FormPupilNumber" && 
				$SettingID!="PFRemarks"
				)
			//if($SettingSummaryArray[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				switch ($SettingID) {
					case "GrandTotal":
					case "GPA":
					case "AverageMark":
					case "ClassHighestAverage":
						$Display = "S";
						break;
					case "ClassPupilNumber":
					case "ClassPosition":
					case "FormPosition":
					
					//case "Attendance":
					case "DaysAbsent":
					case "TimesLate":
					case "AbsentWOLeave":
					//case "MeritsAndDemerits":
					case "Merit":
					case "Demerit":
						$Display = "#";
						break;
					case "Conduct":
					case "Politeness":
					case "Behaviour":
					case "Application":
					case "Tidiness":
						$Display = "G";
						break;
					default:
						$Display = "#";
						break;
				}
				
				# Column - Term
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					$ReturnArray[$count][$column] = $Display;
				}
				
				# Overall Result
				if(!($Semester=="FULL" && $HideOverallResult==1))
					$ReturnArray[$count][$Semester] = $Display;

				$count++;
			}
		}
	}
	return $ReturnArray;
}

/*
* generate MISC Table
*/
function GENERATE_MISC_TABLE()
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	
	//2014-0509-1103-37071
	$MiscTitleArray = array('ECA', 'ClassTeacherComment');

	if(!empty($SettingMiscArray))
	{
		$IsFirst = 1;
		$MiscTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='report_border'>";
		for($k=0; $k<sizeof($MiscTitleArray); $k++)
		{
			$SettingID = trim($MiscTitleArray[$k]);
			switch($SettingID)
			{
				case "ECA":
					$Display = "1.############<br>2.############";
					break;
				case "ClassTeacherComment":
					$Display = "############";
					break;
			}
			
			//2014-0509-1103-37071
			if ($SettingID != 'ClassTeacherComment' && $SettingID != 'ECA') {
				// show class teacher comment only
				continue;
			}
			$Title = $LangArray[$SettingID];
			if ($SettingID == 'ECA') {
				$Title = 'ECA';
			}
			
			if ($SettingID == 'ClassTeacherComment') {
				$MiscTable .= "<tr>";
				if($IsFirst==1) {
					$IsFirst = 0;
				}
				else {
					$top_border_style = "class='border_top'";
				}
				
				//2014-0509-1103-37071
				//$MiscTable .= "<td class='small_title' valign='top'>".$Title."</td></tr>";
				$MiscTable .= "<td class='small_title' valign='top'>".$Title."</td></tr>";
				# Add ECA below "Comments"
				$MiscTable .= $ECARowContent;
				$_top_border_style = "border_top";
				$MiscTable .= "<tr><td class='small_text ".$_top_border_style."' valign='top'>".$Display."</td></tr>";
			}
			else if($SettingID == 'ECA'){
				$ECARowContent = "<tr><td class='border_top' valign='top'><span class='small_text'>".$Title.": ".$Display."</span></td></tr>";
			}
			else{
				$MiscTable .= "<tr>";
				if($IsFirst==1) {
					$IsFirst = 0;
				}
				else {
					$top_border_style = "class='border_top'";
				}
				$MiscTable .= "<td valign='top'><span class='small_title'>".$Title.": </span><span class='small_text'>".$Display."</span></td></tr>";
			}
		}
		$MiscTable .= "</table>";
	}

	return $MiscTable;
}

function GET_SUBJECT_UNIT($subjectCode, $SubjectUnitArr) {
	$subjectUnit = 0;
	$relatedSubjectCodeAry = array();
	$numOfUnit = count($SubjectUnitArr['subjectUnitAry']);
	
	for ($i=0; $i<$numOfUnit; $i++) {
		$_relatedSubjectCodeAry = $SubjectUnitArr['subjectUnitAry'][$i]['relatedSubjectCodeAry'];
		$_unit = $SubjectUnitArr['subjectUnitAry'][$i]['unit'];
		
		if (in_array($subjectCode, (array)$_relatedSubjectCodeAry)) {
			$subjectUnit = $_unit;
			$relatedSubjectCodeAry = $_relatedSubjectCodeAry;
			break;
		}
	}
	
	return array($subjectUnit, $relatedSubjectCodeAry);
}

# Map Subject Code and Subject ID
function GET_SUBJECT_SUBJECTCODE_MAPPING(){
	
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	$li = new libdb();
	$sql = "SELECT 
				CODEID,
				RecordID
			FROM
				ASSESSMENT_SUBJECT 
			WHERE
				EN_DES IS NOT NULL
				AND RecordStatus = 1
				AND (CMP_CODEID IS NULL || CMP_CODEID = '')
			ORDER BY
				DisplayOrder
			";
	$SubjectArr = $li->returnArray($sql, 2);
	
	$ReturnArr = array();
	if (sizeof($SubjectArr) > 0) {
		for($i=0; $i<sizeof($SubjectArr); $i++) {
			$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
		}
	}
	return $ReturnArr;
}

# Santa Rosa - return Hard Code Subject Unit Array
function Get_CLASS_LEVEL_SUBJECT_UNIT($ParClassLevelReg, $ClassID){
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	
	$unitCount = 0;
	$unitAry = array();
	$levelReg = trim($ParClassLevelReg);
	
	# Check Arts / Science
	if($ParClassLevelReg == "S4" || $ParClassLevelReg == "S5" || $ParClassLevelReg == "S6"){
		$liclass = new libclass();
		$ClassName = $liclass->getClassName($ClassID);
		if(strpos($ClassName, 'Science') !== false){
			$Elective = "Sci";
		} else {
			$Elective = "Arts";
		}
	}
	
	# Primary
	# P1
	if($levelReg == 'P1'){
		$unitAry['borderPassUnit'] = 2;
		$unitAry['failUnit'] = 3;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('114');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// [2017-1013-1811-15235] added Drama
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('148');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;	
	} 
	# P2
	else if($levelReg == 'P2'){
		$unitAry['borderPassUnit'] = 2;
		$unitAry['failUnit'] = 3;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('114');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// [2017-1013-1811-15235] added Drama
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('148');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	} 
	# P3
	else if($levelReg == 'P3'){
		$unitAry['borderPassUnit'] = 2;
		$unitAry['failUnit'] = 3;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('114');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;	
	}	
	# P4
	else if($levelReg == 'P4'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	}
	# P5
	else if($levelReg == 'P5'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;	
	} 
	# P6
	else if($levelReg == 'P6'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;		
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;		
	} 
	# Secondary
	# F1
	else if($levelReg == 'S1'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;	
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('176');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	}
	# F2
	else if($levelReg == 'S2'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;	
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;	
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	}
	# F3
	else if($levelReg == 'S3'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;		
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('163', '126');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
	}
	# F4
	else if($levelReg == 'S4'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('166');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		if ($Elective == "Sci"){	
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('159');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitCount++;
		}
		if ($Elective == "Sci"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('129');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('126');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('128');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('158');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('162');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// F81303 - 為兩個新增學科加上相關的 "Units"
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('171');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('172');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
	}
	# F5
	else if($levelReg == 'S5'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('166');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('161');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		if ($Elective == "Sci"){	
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('126');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('128');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('129');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('159');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		
		// [2016-1214-1048-47235] removed Chinese History, cancelled Chinese and Chinese History grouping
//		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('158');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('162');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// [2016-1214-1048-47235] added Music and Integrated Science
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('172');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
	}
	# F6
	else if($levelReg == 'S6'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
			
			// [2016-1214-1048-47235] removed Business English, replaced by Business Studies
			//$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('161');
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('166');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('159');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		
		// [2017-0103-1001-49207] added Accounting for F.6 Science
		//if ($Elective == "Arts"){
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
		$unitCount++;
		//}
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		
		// [2017-1013-1811-15235] added Additional Mathematics
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('175');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0.5;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('158');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		if ($Elective == "Sci"){	
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('129');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('128');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('126');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('162');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('177');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1; // 2019-12-30 (Philips) [2019-1212-1809-07235] - Add Geo Literacy
//		$unitCount++;
//		if ($Elective = "Arts"){
//			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
//			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
//			$unitCount++;
//		}
	}
	# S7 (146 - Testing)
	else if($levelReg == '146-S7'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('80', '203');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('201');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('280');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('606');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('coc');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('104');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('901');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('401');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('101', '102');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
	}
	# dummy 2 (149 - Testing)
	else if($levelReg == '149-S1'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('P008');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('S02', '052');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('21B');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('099');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('0009');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('053');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('054');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('061', '063');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('333');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('036');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('082');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('009');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
	}
	
	
	$returnAry = $unitAry;
	unset($unitAry);
	return $returnAry;
}

############################ Functions End ################################
if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");

	$libreportcard = new libreportcard();
	$CurrentPage = "ReportGeneration";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $button_preview;
	if ($libreportcard->hasAccessRight())
    {
		$linterface = new interface_html("popup.html");
#######################################################################################################
		$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		$SubjectGradingIDArray = (is_array($SubjectGradingArray)) ? array_values($SubjectGradingArray) : "";
		
		# get current Year and Semester
		$Year = trim($libreportcard->Year);
		$Semester = trim($libreportcard->Semester);

		# Get Semester Array
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();

		# Get Column Array of the report
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		$SubjectArr = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID, $SubjectGradingIDArray);
		$ColumnArr = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);

		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();
		
		# Get Subject Code Mapping
		$SubjectCodeMappingArr = GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		# Get Subject Units
		$libformclass = new Year($SelectedFormArr[0]);
		$SubjectUnitArr = Get_CLASS_LEVEL_SUBJECT_UNIT($libformclass->WEBSAMSCode, "");

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader, $LineHeight, $SignatureWidth) = $ReportInfo;

		$SettingArray = unserialize($Settings);
		$ResultDisplayType = $SettingArray["ResultDisplayType"];
		$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
		$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];
		$HideOverallResult = $SettingArray["HideOverallResult"];

		# define the name of the variable array by checking the bilingual setting
		if($BilingualReportTitle==1)
		{
			$LangArray = $eReportCardBilingual;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$pos = strpos($DaysAbsentTitle, " ");
			$DaysAbsentTitle = substr_replace($DaysAbsentTitle, "<br />", $pos, 1);

			$TimesLateTitle = $LangArray["TimesLate"];
			$pos = strpos($TimesLateTitle, " ");
			$TimesLateTitle = substr_replace($TimesLateTitle, "<br />", $pos, 1);

			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
			$pos = strpos($AbsentWOLeaveTitle, " ");
			$AbsentWOLeaveTitle = substr_replace($AbsentWOLeaveTitle, "<br />", $pos, 1);
		}
		else {
			$LangArray = $eReportCard;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$TimesLateTitle = $LangArray["TimesLate"];
			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
		}

		# get school badge
		$SchoolLogo = GET_SCHOOL_BADGE();

		# get school name
		$SchoolName = GET_SCHOOL_NAME();
// 		$SchoolName = "Santa Rosa de Lima English Secondary School Macau"; // 2019-12-27 Philips - Use hardcode school name

		$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
		if ($NoHeader != -1) $TempLogo = "&nbsp;";

		$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
		if(!empty($ReportTitle) || !empty($SchoolName))
		{
			list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);
			$TitleTable .= "<td>";
			if ($NoHeader == -1) {
				$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
				if(!empty($SchoolName))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
				if(!empty($ReportTitle1))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle1."</td></tr>\n";
				if(!empty($ReportTitle2))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' colspan='2'>".$ReportTitle2."</td></tr>\n";
				$TitleTable .= "</table>\n";
			} else {
				for ($i = 0; $i < $NoHeader; $i++) {
					$TitleTable .= "<br/>";
				}
			}
			$TitleTable .= "</td>";
		}
		$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
		$TitleTable .= "</table>";

		################################################################
		# Generate Student Info Table
		$StudentTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		$SettingStudentInfo = $SettingArray["StudentInfo"];
		if(!empty($SettingStudentInfo))
		{
			$count = 0;
			$StudentInfoTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber")
				{
					$Title = $LangArray[$SettingID];
					$Title = str_replace("<br />", " ", $Title);
					if($count%2==0) {
						$StudentInfoTable .= "<tr>";
					}
					$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top'>".$Title." : XXX</td>";
					if($count%2==1) {
						$StudentInfoTable .= "</tr>";
					}
					$count++;
				}
			}
			$StudentInfoTable .= "</table>";
		}

		$StudentInfoRow = (!empty($StudentInfoTable)) ? "<tr><td>".$StudentInfoTable."</td></tr>" : "";
		################################################################
		# Generate Summary Table
		$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		$SettingSummaryArray = $SettingArray["Summary"];
		
		################################################################
		# Generate Misc Table
		$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$SettingMiscArray = $SettingArray["Misc"];
		
		### add merit to display detail (aki)
		$SettingSummaryArray = $SettingArray["Summary"];		
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$SettingSummaryArray['Merit'] = 1;
			$SettingSummaryArray['Demerit'] = 1;
			
			$SummaryTitleArray[] = "Merit";
			$SummaryTitleArray[] = "Demerit";			
		}
		
		$SummaryResultArray = GET_SUMMARY_ARRAY();
		$ConductResultArray = GET_CONDUCT_ARRAY();
		
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];
		$MiscTable = GENERATE_MISC_TABLE();
		$MiscTableRow = (!empty($MiscTable)) ? "<tr><td>".$MiscTable."</td></tr>" : "";

		################################################################
		# Generate Signature Table
		$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
		$SettingSignatureArray = $SettingArray["Signature"];
		$SignatureTable = GENERATE_SIGNATURE_TABLE();

		# Generate Attendance and Merit Table
		$AttendanceMeritTable = GENERATE_ATTENDANCE_AND_MERIT_TABLE();

		################################################################
		# Generate Details Table
		$SubjectTitleType = $SettingArray["SubjectTitleType"];
		if($BilingualReportTitle==1 || $SubjectTitleType==0) {
			$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
		}
		else {
			$SubjectTitleCell = "<td class='small_title'>".$eReportCard['Subject']."</td>";
		}
		
		$ExtraColumn = 0;
		$ColumnSize = sizeof($ColumnArr);
		$subjectUnitColWidth = 6;
		$signatureColWidth = 22;
		
		$ColumnWidth = 11;
		$totalMarkColWidth = $ColumnWidth * ($ColumnSize+1);
		
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		
		$DetailsTable .= "<tr>
							<td class='report_formfieldtitle' height='100%'>
								<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr valign='top'><td align='right' class='small_title' colspan='2'>&nbsp;</td></tr>
								<tr valign='bottom'>".$SubjectTitleCell."</tr>
								</table>
							</td>";
							
		# Suject Unit Column
		$DetailsTable .= "<td width='".$subjectUnitColWidth."%' class='border_left small_title report_formfieldtitle'>
							<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr valign='top'><td align='right' class='small_title'>".$LangArray['Marks']."</td></tr>
							<tr valign='bottom'><td align='left' class='small_title'>".$LangArray['SubjectUnit']."</td></tr>
						</table></td>";
		$ExtraColumn++;
						
//		if($ShowFullMark==1)
//		{
//			$DetailsTable .= "<td width='5%' class='report_formfieldtitle border_left small_title' align='center' >".$LangArray['FullMark']."</td>";
//			$ExtraColumn++;
//		}

		for($i=0; $i<sizeof($ColumnArr); $i++)
		{
			list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArr[$i];
			if($Weight!="")
			{
				$ColumnTitle = ($ShowColumnPercentage==1) ? $ColumnTitle."&nbsp;".$Weight."%" : $ColumnTitle;
				$WeightArray[$ReportColumnID] = $Weight;
			}
		
			$DetailsTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
			$DetailsTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3' width='100%' style=''>".$ColumnTitle."</td></tr>";
			if($ShowPosition==1 || $ShowPosition==2)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
				$DetailsTable .= "<td width='1'>&nbsp;</td>";
				$DetailsTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
				$DetailsTable .= "</tr>";
			}
			$DetailsTable .= "</table></td>";
		}
		
		if($ColumnSize>1 && $HideOverallResult!=1)
		{
			// width='10%'
			$DetailsTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
			$DetailsTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
			if(($ShowOverallPosition==1 || $ShowOverallPosition==2) && !$eRC_custom['HideOverallSubjectPosition'])
			{
				$ResultTable .= "<tr>";
				$ResultTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
				$ResultTable .= "<td width='1'>&nbsp;</td>";
				$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
				$ResultTable .= "</tr>";
			}
			$DetailsTable .= "</table></td>";
			$ExtraColumn++;
		}
		
		if($ShowSubjectTeacherComment==1) {
			$DetailsTable .= "<td class='report_formfieldtitle border_left small_title' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
		}
		
		if ($SignatureTable!="")
		{
			$DetailsTable .= "<td height='100%' rowspan='50' class='border_left' width='2'><img src='/images/spacer.gif' width='2' height='1' border='0' /></td>";
			$DetailsTable .= "<td width='".$signatureColWidth."%' height='100%' rowspan='50' class='border_left'>";
			$DetailsTable .= "<table border='0' height='100%' width='100%' cellpadding='0' cellspacing='0'>";
			$DetailsTable .= ($SignatureTable!="") ? "<tr><td valign='top' width='100%' height='100%'>".$SignatureTable."</td></tr>" : "";
			$DetailsTable .= "</table>";
			$DetailsTable .= "</td>";
		}
		$DetailsTable .= "</tr>";

		$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
		$IsFirst = 1;
	
		# Subject Table
		if(is_array($SubjectArr))
		{
			$count = 0;
			foreach($SubjectArr as $CodeID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpCodeID => $InfoArr)
					{
						list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
						
						$IsComponent = ($CmpCodeID != '') ? 1 : 0;
						$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
						$IsFirst = 0;
						$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";

						# Highest Grade
						$MaxGrade = $DistinctionArray[$SubjectID]["G"];
						$FirstRowSetting = $ReportCellSetting[$ColumnArr[0][0]][$ReportSubjectID];
						$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

						# check whether is parent/component subject
						$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
						$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

						$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
						$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

						# get subject display cell
						if($BilingualReportTitle==1 || $SubjectTitleType==0)
						{
							//$SubjectCell = "<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td><td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>";
							$SubjectCell = "<td>
									<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
									<tr>
										<td width='50%' class='tabletext'>".$Prefix.$EngSubjectName."</td>
										<td width='50%' class='tabletext'>".$Prefix.$ChiSubjectName."</td>
									</tr>
									</table>
									</td>";
						}
						else
						{
							$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
							$SubjectCell = "<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>";
						}
						
						$DetailsTable .= "<tr>";
						$DetailsTable .= "<td class='$top_style'>
											<table width='100%' border='0' cellpadding='2' cellspacing='0' height='$LineHeight'><tr>".$SubjectCell."</tr></table>
										</td>";
						//$DetailsTable .= ($ShowFullMark==1) ? "<td class='border_left tabletext $top_style' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";
						$ValidCount = 0;
					
						# Display Subject Unit
						$currentSubjectCode = trim($SubjectCodeMappingArr[$SubjectID]);
						list($CurrentSubjectUnit, $RelatedSubjectCodeAry) = GET_SUBJECT_UNIT($currentSubjectCode, $SubjectUnitArr);
						
						# Subject with Combined Unit
	//					if(count($SubjectUnitArr['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry']) > 1){
						if(count($RelatedSubjectCodeAry) > 1){
							$firstSubjectCode = trim($RelatedSubjectCodeAry[0]);
							# Display Combined Subject Unit - First subject
							if($currentSubjectCode == $firstSubjectCode){
								$DetailsTable .= "<td class='tabletext border_left $top_style' align='center' rowspan=2>".$CurrentSubjectUnit."</td>";
							}
						} 
						# Normal Subject
						else {
							$DetailsTable .= "<td class='tabletext border_left $top_style' align='center'>".$CurrentSubjectUnit."</td>";
						}
					
						for($i=0; $i<sizeof($ColumnArr); $i++)
						{
							list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArr[$i];
							$Setting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];

							if($Setting!="N/A")
								$ValidCount++;
							else
								$Setting = "--";

							$Setting = ($MarkTypeDisplay==2 && $Setting=="S") ? "G" : $Setting;
							$DetailsTable .= "<td class='border_left $top_style'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$DetailsTable .= "<tr>";
							if(!$ResultShow) {
								$DetailsTable .= "<td align='center' class='tabletext'>&nbsp;</td>";
							}
							else
							{
								if($ShowPosition==1 || $ShowPosition==2) {
									$DetailsTable .= "<td align='center' class='tabletext' width='50%'>".$Setting."</td>";
									$DetailsTable .= "<td>&nbsp;</td>";
									$DetailsTable .= ($HidePosition==0 && $Setting=="S") ? "<td align='center' class='tabletext' width='50%'>(#)</td>" : "<td align='center' class='tabletext' width='50%'></td>";
								}
								else {
									$DetailsTable .= "<td align='center' class='tabletext'>".$Setting."</td>";
								}
							}
							$DetailsTable .= "</tr>";
							$DetailsTable .= "</table></td>";
						}

						if($ColumnSize>1 && $HideOverallResult!=1)
						{
							$Scale = ($MarkTypeDisplay==2 && $Scale=="S") ? "G" : $Scale;
							$DetailsTable .= "<td class='border_left $top_style'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$DetailsTable .= "<tr>";
							if(!$OverallShow)
							{
								$DetailsTable .= "<td class='tabletext' align='center'>&nbsp;</td>";
							}
							else
							{
								if($ShowOverallPosition==1 || $ShowOverallPosition==2)
								{
									$DetailsTable .= "<td align='center' class='tabletext'>".($ValidCount>0?$Scale:'--')."</td>";
//									$DetailsTable .= "<td>&nbsp;</td>";
//									$DetailsTable .= ($HidePosition==0 && $Setting=="S") ? "<td align='center' class='tabletext' width='50%'>(#)</td>" : "<td align='center' class='tabletext' width='50%'></td>";
								}
								else
								{
									$DetailsTable .= "<td align='center' class='tabletext'>".($ValidCount>0?$Scale:'--')."</td>";
								}
							}
							$DetailsTable .= "</tr>";
							$DetailsTable .= "</table></td>";
						}
						if($ShowSubjectTeacherComment==1)
						{
							$DetailsTable .= "<td class='tabletext border_left $top_style'>&nbsp;</td>";
						}
						$DetailsTable .= "</tr>";

						$count++;
					}
				}
			}
		}
		else
		{
			$DetailsTable .= "<tr><td align='center' class='tabletext' colspan='".$ColumnSpan."'>&nbsp;</td></tr>";
		}

		# Summary Result Table
		if(!empty($SummaryResultArray))
		{
			for($k=0; $k<sizeof($SummaryResultArray); $k++)
			{
				$DetailsTable .= "<tr>";
				
				$top_border_style = $SummaryResultArray[$k]["Title"] == 'Grand Total'? 'border_top_bold' : 'border_top';
				$colspan = "colspan='2'";
				
				$DetailsTable .= "<td class='{$top_border_style}' {$colspan}>
							<table border='0' cellpadding='2' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
							</td>";
							
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$SummaryResultArray[$k][$ColumnTitle]."</td>";
				}
				
				### Overall Grand Result
				if(!(($Semester=='FULL' || $Semester==0) && $HideOverallResult==1))
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$SummaryResultArray[$k][$Semester]."</td>";
					
				if($ShowSubjectTeacherComment==1) {
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>&nbsp;</td>";
				}
				$DetailsTable .= "</tr>";
			}
		}	
		
		# Conduct Result Table
		if(!empty($ConductResultArray))
		{
			for($k=0; $k<sizeof($ConductResultArray); $k++)
			{
				$DetailsTable .= "<tr>";
				
				//$top_border_style = ($k==0) ? "border_top" : "";
				//$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
				$_title = $ConductResultArray[$k]['Title'];
				$top_border_style = ($_title=='Conduct' || $_title=='Merits' || $_title=='Demerits' || $_title=='Offence') ? "border_top_bold" : "border_top";
			
				if ($_title=='Merits' || $_title=='Demerits' || $_title=='Offence') {
//					$colspan = ($ShowFullMark==1) ? "colspan='".(sizeof($ColumnArray) + 3)."'" : "colspan='".(sizeof($ColumnArray) + 2)."'";
					$colspan = "colspan='".(sizeof($ColumnArray) + 3)."'";
					$stylePrefix = '<b>';
					$styleSuffix = '</b>';
				}
				else {
					$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
					$colspan = "colspan='2'";
					$stylePrefix = '';
					$styleSuffix = '';
				}
				
				$DetailsTable .= "<td class='{$top_border_style}' {$colspan}>
							<table border='0' cellpadding='2' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$ConductResultArray[$k]["Title"]."</td></tr></table>
							</td>";
						
				if ($_title=='Merits' || $_title=='Demerits' || $_title=='Offence') {
					// do nth
				}
				else {
					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
						$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$ConductResultArray[$k][$ColumnTitle]."</td>";
					}
					if(!($Semester=='FULL' && $HideOverallResult==1))
						$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$ConductResultArray[$k][$Semester]."</td>";
					if($ShowSubjectTeacherComment==1) {
						$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>&nbsp;</td>";
					}
				}
				$DetailsTable .= "</tr>";
			}
		}
		
		$DetailsTable .= "</table>";

		if($Footer!="") {
			$FooterRow = "<tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n";
		}
##########################################################################################

$linterface->LAYOUT_START();
?>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/print.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.border_top_bold {
	border-top-width: 2px;
	border-top-style: solid;
	border-top-color: #000000;
}
</style>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">
	<table width="95%" border="0" cellpadding="2" cellspacing="1">
	<tr><td><?=$TitleTable?></td><tr>
	<?=$StudentInfoRow?>
	<tr><td align="center" width="100%"><?=$DetailsTable?></td><tr>
	<?=$MiscTableRow?>
	<?=$FooterRow?>
	</table>
</td>
</tr>

<tr><td>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>
</td></tr>

</table>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>