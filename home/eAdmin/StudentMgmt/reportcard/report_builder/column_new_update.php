<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

if($ReportID!="")
{
	################################# STEP 1: GET DisplayOrder #####################################
	# Get Column Display Order list
	$sql = "SELECT ReportColumnID, DisplayOrder FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID = '".$ReportID."' ORDER BY DisplayOrder";
	$row = $li->returnArray($sql, 2);
	for($i=0; $i<sizeof($row); $i++)
	{
		list($rc_id, $order) = $row[$i];
		$RCOrderArr[$order] = $rc_id;
	}

	if(is_array($RCOrderArr))
	{
		$OrderArr = array_keys($RCOrderArr);	
		if($InsertAfterID!="")
		{
			for($i=0; $i<sizeof($OrderArr); $i++)
			{
				$order = $OrderArr[$i];
				$rc_id = $RCOrderArr[$order];
				if($rc_id==$InsertAfterID)
				{
					$DisplayOrder = ($OrderArr[$i+1]!="") ? $OrderArr[$i+1] : "";
					$Reorder = 1;
					break;
				}
			}
		}
		else
		{
			$DisplayOrder = 1;
			$Reorder = 1;
		}

	}
	
	if($DisplayOrder=="")
	{
		# Get Max Display Order
		$sql = "SELECT MAX(DisplayOrder) FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID = '".$ReportID."'";
		$row = $li->returnVector($sql);
		$LastOrder = $row[0];	
			
		$DisplayOrder = ($LastOrder=="") ? 1 : $LastOrder+1;
		$Reorder = 0;
	}	

	################################# STEP 2: Insert Record #####################################
	
	# Insert record to RC_REPORT_TEMPLATE_COLUMN
	$ColumnTitle = ($ColumnTitle=="") ? intranet_htmlspecialchars($CustomTitle) : $ColumnTitle;
	$IsColumnSum = ($ColumnTitle == "SYS_SUM_COLUMN") ? 1 : 0;
	if ($IsColumnSum) $ColumnTitle = $WeightCustomTitle;
	
	$ShowPosition = ($ShowPosClassColumn==="") ? 0 : $ShowPosition;
	$PositionRange = ($ShowPosition==1 || $ShowPosition==2) ? $PositionFrom.",".$PositionTo : "";
	$NotAllowToInput = ($NotAllowToInput==="") ? 0 : $NotAllowToInput;
		
	$Fields = "(ReportID, ColumnTitle, Weight, DisplayOrder, ShowPosition, PositionRange, DateInput, DateModified, IsColumnSum, MarkSummaryWeight, NotForInput)";
	$Values = "('$ReportID', '$ColumnTitle', '$Weight', '$DisplayOrder', '$ShowPosition', '$PositionRange', now(), now(), '$IsColumnSum', '$MarkSummaryWeight', '$NotAllowToInput')";
	$sql = "INSERT INTO RC_REPORT_TEMPLATE_COLUMN $Fields VALUES $Values";
	$success = $li->db_db_query($sql);

	$Result = ($success==1) ? "add" : "add_failed";

	################################# STEP 3: Reordering #####################################
	# Reordering
	if($success==1 && $Reorder==1 && is_array($OrderArr))
	{
		$NewOrder = $DisplayOrder+1;
		$StartPos = ($DisplayOrder==1) ? 0 : array_search($DisplayOrder, $OrderArr);
		$SortingOrderArray = array_slice($OrderArr, $StartPos);
		for($i=0; $i<sizeof($SortingOrderArray); $i++)
		{
			$Order = $SortingOrderArray[$i];
			$rcid = $RCOrderArr[$Order];
			$sql = "UPDATE RC_REPORT_TEMPLATE_COLUMN SET DisplayOrder = '".$NewOrder."' WHERE ReportColumnID = '".$rcid."'";
			$li->db_db_query($sql);

			$NewOrder++;
		}
	}
}

intranet_closedb();
header("Location:column_manage.php?Result=$Result&ReportID=$ReportID&ReportType=$ReportType");
?>

