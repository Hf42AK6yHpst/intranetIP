<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	
	$libreportcard = new libreportcard();
	$CurrentPage = "ReportGeneration";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $button_preview;

	if ($libreportcard->hasAccessRight())
    {
    ####### New Appraoch Start (Read config CSV) ########
    include_once("../default_header.php");
    ################## New Appraoch End #################
		$linterface = new interface_html("popup.html");
#######################################################################################################
		$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		$SubjectGradingIDArray = (is_array($SubjectGradingArray)) ? array_values($SubjectGradingArray) : "";
		
		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		$SubjectArr = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID, $SubjectGradingIDArray);
		$ColumnArr = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		$ColumnSize = sizeof($ColumnArr);
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);
		
		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader) = $ReportInfo;

		$SettingArray = unserialize($Settings);
		$ResultDisplayType = $SettingArray["ResultDisplayType"];
		$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
		$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];
		$HideOverallResult = $SettingArray["HideOverallResult"];

		# define the name of the variable array by checking the bilingual setting
		$LangArray = ($BilingualReportTitle==1) ? $eReportCardBilingual : $eReportCard;

		# get school badge
		$SchoolLogo = GET_SCHOOL_BADGE();
			
		# get school name
		$SchoolName = GET_SCHOOL_NAME();	
		
		$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
		if ($NoHeader != -1) $TempLogo = "&nbsp;";

		$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
		if(!empty($ReportTitle) || !empty($SchoolName))
		{
			list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);

			$TitleTable .= "<td>";
			if ($NoHeader == -1) {
				$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
				if(!empty($SchoolName))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
				if(!empty($ReportTitle1))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle1."</td></tr>\n";
				if(!empty($ReportTitle2))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' colspan='2'>".$ReportTitle2."</td></tr>\n";
				$TitleTable .= "</table>\n";
			} else {
				for ($i = 0; $i < $NoHeader; $i++) {
					$TitleTable .= "<br/>";
				}
			}
			$TitleTable .= "</td>";
		}
		$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
		$TitleTable .= "</table>";
				
		################################################################
		# Generate Student Info Table
		$StudentTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		
		$SettingStudentInfo = $SettingArray["StudentInfo"];
		if(!empty($SettingStudentInfo))
		{
			$count = 0;
			$StudentInfoTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber")
				{
					$Title = $LangArray[$SettingID];
					$Title = str_replace("<br />", " ", $Title);
					if($count%2==0) {
						$StudentInfoTable .= "<tr>";
					}
					$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top'>".$Title." : XXX</td>";
					if($count%2==1) {
						$StudentInfoTable .= "</tr>";
					}
					$count++;
				}
			}
			$StudentInfoTable .= "</table>";
		}
		
		################################################################
		# Generate Summary Table
		$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		$SettingSummaryArray = $SettingArray["Summary"];
		
		################# New Appraoch Start ################
		for($i=0; $i<sizeof($DefaultHeaderArray["summary"]); $i++) {
			$tmpSettingTitle = $HeaderSettingMap["summary"][$DefaultHeaderArray["summary"][$i]];
			$SettingSummaryArray[$tmpSettingTitle] = 1;
		}
    ################## New Appraoch End #################
    
		if(!empty($SettingSummaryArray))
		{
			$count = 0;
			$SummaryTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			for($k=0; $k<sizeof($SummaryTitleArray); $k++)
			{
				$SettingID = trim($SummaryTitleArray[$k]);
				if($SettingSummaryArray[$SettingID]==1)
				{
					$Title = $LangArray[$SettingID];
					if($count%2==0) {
						$SummaryTable .= "<tr valign='top'>";
					}
					switch ($SettingID) {
						case "GrandTotal":
						case "GPA":
						case "AverageMark":
						case "ClassHighestAverage":
							$SampleType = "S";
							break;
						case "Conduct":
						case "Politeness":
						case "Behaviour":
						case "Application":
						case "Tidiness":
							$SampleType = "G";
							break;
						default:
							$SampleType = "#";
							break;						
					}
					$SummaryTable .= "<td class='small_title' width='30%' nowrap='nowrap'>".$Title."</td><td class='report_contenttitle' width='5'>:</td><td class='tabletext' width='20%'>".$SampleType."</td>";
					if($count%2==1) {
						$SummaryTable .= "</tr>";
					}
					$count++;
				}
			}
			$SummaryTable .= "</table>";
		}
		
		################################################################
		# Generate Misc Table
		$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$SettingMiscArray = $SettingArray["Misc"];
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];
		
		################# New Appraoch Start ################
		// Different from "Summary" because they are not 
		// directly mapped to one field only
		if (isset($DefaultHeaderArray["award"]))
			$SettingMiscArray["Awards"] = 1;
		if (isset($DefaultHeaderArray["merit"]))
			$SettingMiscArray["MeritsAndDemerits"] = 1;
		if (isset($DefaultHeaderArray["eca"]))
			$SettingMiscArray["ECA"] = 1;
		if (isset($DefaultHeaderArray["remark"]))
			$SettingMiscArray["Remark"] = 1;
		if (isset($DefaultHeaderArray["interschool"]))
			$SettingMiscArray["InterSchoolCompetition"] = 1;
		if (isset($DefaultHeaderArray["schoolservice"]))
			$SettingMiscArray["SchoolService"] = 1;
    ################## New Appraoch End #################
    
		if(!empty($SettingMiscArray))
		{
			$count = 0;
			$MiscTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			for($k=0; $k<sizeof($MiscTitleArray); $k++)
			{
				$SettingID = trim($MiscTitleArray[$k]);
				if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment")
				{
					$Title = $LangArray[$SettingID];
					$Title = str_replace("<br />", " ", $Title);
					$MiscTable .= ($count%2==0) ? "<tr>" : "";
					$MiscTable .= "<td width='50%' class='report_border'>";
					$MiscTable .= "<table width='100%' cellpadding='4' cellspacing='0' align='center'>
									<tr><td class='small_title'>".$Title."</td></tr>
									<tr><td class='small_title' height='80' nowrap='nowrap'>&nbsp;</td></tr>
									</table>";
					$MiscTable .= "</td>";
					$MiscTable .= ($count%2==0) ? "<td>&nbsp;</td>" : "</tr><tr><td colspan='3'><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width='10' height='5'></td></tr>";

					$count++;
				}
			}
			$MiscTable .= "</table>";
		}

		################################################################
		# Generate Details Table
		$SubjectTitleType = $SettingArray["SubjectTitleType"];
		if($BilingualReportTitle==1 || $SubjectTitleType==0) {
			$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
		}
		else {
			$SubjectTitleCell = "<td class='small_title'>".$eReportCard['Subject']."</td>";
		}

		$ExtraColumn = 0;
		$DetailsTable = "<table width='100%' border=0 cellspacing='0' cellpadding='4' class='report_border'>";
		$DetailsTable .= "<tr>
							<td width='30%' class='report_formfieldtitle'>
								<table width='100%' border='0' cellpadding='4' cellspacing='0'><tr>".$SubjectTitleCell."</tr></table>
							</td>";
		if($ShowFullMark==1)
		{
			$DetailsTable .= "<td width='10%' class='report_formfieldtitle border_left small_title' align='center' >".$LangArray['FullMark']."</td>";
			$ExtraColumn++;
		}

		for($i=0; $i<sizeof($ColumnArr); $i++)
		{
			list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArr[$i];
			if($Weight!="" && $ShowColumnPercentage==1)
			{
				$ColumnTitle = $ColumnTitle."&nbsp;".$Weight."%";
			}
			$DetailsTable .= "<td class='border_left report_formfieldtitle'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
			$DetailsTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";
			if($ShowPosition==1 || $ShowPosition==2)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
				$DetailsTable .= "<td>&nbsp;</td>";
				$DetailsTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
				$DetailsTable .= "</tr>";
			}
			$DetailsTable .= "</table></td>";
		}
		if($ColumnSize>1 && $HideOverallResult!=1)
		{
			$DetailsTable .= "<td class='border_left report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
			$DetailsTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
			if($ShowOverallPosition==1 || $ShowOverallPosition==2)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
				$DetailsTable .= "<td>&nbsp;</td>";
				$DetailsTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
				$DetailsTable .= "</tr>";
			}
			$DetailsTable .= "</table></td>";
			$ExtraColumn++;
		}
		if($ShowSubjectTeacherComment==1)
		{
			$DetailsTable .= "<td width='10%' class='report_formfieldtitle border_left small_title' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
		}
		$DetailsTable .= "</tr>";

		$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
		if(is_array($SubjectArr))
		{
			$count = 0;
			$IsFirst = 1;
			foreach($SubjectArr as $CodeID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpCodeID => $InfoArr)
					{
						$IsComponent = ($CmpCodeID>0) ? 1 : 0;
						$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
						$IsFirst = 0;

						$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";
						list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
						
						# Highest Grade
						$MaxGrade = $DistinctionArray[$SubjectID]["G"];
						$FirstRowSetting = $ReportCellSetting[$ColumnArr[0][0]][$ReportSubjectID];
						$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

						# check whether is parent/component subject
						$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
						$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

						$ResultShow = (!($ResultDisplayType==2 && $IsParent==1)) ? 1 : 0;
						$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;
						
						if($BilingualReportTitle==1 || $SubjectTitleType==0)
						{
							$SubjectCell = "<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td><td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>";
						}
						else
						{
							$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
							$SubjectCell = "<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>";
						}
						$DetailsTable .= "<tr>";
						$DetailsTable .= "<td class='tabletext $top_style'>
											<table width='100%' border='0' cellpadding='4' cellspacing='0'><tr>".$SubjectCell."</tr></table>
										</td>";
						$DetailsTable .= ($ShowFullMark==1) ? "<td class='border_left tabletext $top_style' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";
						$ValidCount = 0;
						for($i=0; $i<sizeof($ColumnArr); $i++)
						{
							list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArr[$i];
							$Setting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];

							if($Setting!="N/A")
								$ValidCount++;
							else
								$Setting = "--";
							
							$Setting = ($MarkTypeDisplay==2 && $Setting=="S") ? "G" : $Setting;
							$DetailsTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$DetailsTable .= "<tr>";
							if(!$ResultShow)
							{
								$DetailsTable .= "<td align='center' class='tabletext'>&nbsp;</td>";
							}
							else 
							{
								if($ShowPosition==1 || $ShowPosition==2)
								{
									$DetailsTable .= "<td align='center' class='tabletext' width='50%'>".$Setting."</td>";
									$DetailsTable .= "<td>&nbsp;</td>";
									$DetailsTable .= ($HidePosition==0 && $Setting=="S") ? "<td align='center' class='tabletext' width='50%'>(#)</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
								}
								else
								{
									$DetailsTable .= "<td align='center' class='tabletext'>".$Setting."</td>";
								}
							}
							$DetailsTable .= "</tr>";
							$DetailsTable .= "</table></td>";
						}
						if($ColumnSize>1 && $HideOverallResult!=1) 
						{
							$Scale = ($MarkTypeDisplay==2 && $Scale=="S") ? "G" : $Scale;
							$DetailsTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$DetailsTable .= "<tr>";
							if(!$OverallShow)
							{
								$DetailsTable .= "<td class='tabletext' align='center'>&nbsp;</td>";
							}
							else
							{
								if($ShowOverallPosition==1 || $ShowOverallPosition==2)
								{
									$DetailsTable .= "<td align='center' class='tabletext' width='50%'>".($ValidCount>0?$Scale:'--')."</td>";
									$DetailsTable .= "<td>&nbsp;</td>";
									$DetailsTable .= ($HidePosition==0 && $Setting=="S") ? "<td align='center' class='tabletext' width='50%'>(#)</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
								}
								else
								{
									$DetailsTable .= "<td align='center' class='tabletext'>".($ValidCount>0?$Scale:'--')."</td>";
								}
							}
							$DetailsTable .= "</tr>";
							$DetailsTable .= "</table></td>";
						}
						if($ShowSubjectTeacherComment==1)
						{
							$DetailsTable .= "<td class='tabletext border_left $top_style'>&nbsp;</td>";
						}
						$DetailsTable .= "</tr>";

						$count++;
					}
				}
			}
		}
		else
		{
			$DetailsTable .= "<tr><td align='center' class='tabletext report_formfieldtitle' colspan='".$ColumnSpan."'>&nbsp;</td></tr>";
		}
		$DetailsTable .= "</table>";
		if($SummaryTable!="")
		{
			$DetailsTable .= "<table width='100%' cellspacing='0' cellpadding='4' class='summary_table'>";
			$DetailsTable .= "<tr><td colspan='".$ColumnSpan."' height='100' valign='top'>".$SummaryTable."</td></tr>";
			$DetailsTable .= "</table>";
		}

		################################################################
		# Generate Signature Table
		$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
		$SettingMiscArray = $SettingArray["Signature"];
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='report_border'>";
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			if($SettingMiscArray[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$SignatureTable .= "<td valign='bottom' align='center'>";
				$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
				$SignatureTable .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
				$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
				$SignatureTable .= "</table>";
				$SignatureTable .= "</td>";
			}
		}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		if($Footer!="") {
			$FooterRow = "<tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n";
		}
##########################################################################################

$linterface->LAYOUT_START();
?>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/print.css" rel="stylesheet" type="text/css" />

<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">
	<table width="95%" border="0" cellpadding="2" cellspacing="1">
	<tr><td><?=$TitleTable?></td><tr>
	<tr><td><?=$StudentInfoTable?></td><tr>
	<tr><td><?=$DetailsTable?></td><tr>
	<tr><td><?=$MiscTable?></td><tr>
	<tr><td><?=$SignatureTable?></td><tr>
	<?=$FooterRow?>
	</table>
</td>
</tr>

<tr><td>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>
</td></tr>

</table>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>