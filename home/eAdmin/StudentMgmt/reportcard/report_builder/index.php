<?php
// Using: 
/*****************************************
 *  Modification log
 * 
 * 	20160711 Bill	[2016-0203-1700-19207]
 * 		- display system msg to remind client to re-save marksheet and re-generate report after grading scheme updated
 * 
 * ***************************************/
 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_report_builder_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_report_builder_page_number", $pageNo, 0, "", "", 0);
	$ck_report_builder_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_report_builder_page_number!="")
{
	$pageNo = $ck_report_builder_page_number;
}

if ($ck_report_builder_page_order!=$order && $order!="")
{
	setcookie("ck_report_builder_page_order", $order, 0, "", "", 0);
	$ck_report_builder_page_order = $order;
} else if (!isset($order) && $ck_report_builder_page_order!="")
{
	$order = $ck_report_builder_page_order;
}

if ($ck_report_builder_page_field!=$field && $field!="")
{
	setcookie("ck_report_builder_page_field", $field, 0, "", "", 0);
	$ck_report_builder_page_field = $field;
} else if (!isset($field) && $ck_report_builder_page_field!="")
{
	$field = $ck_report_builder_page_field;
}

$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_Template";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
############################################################################################################

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 0;
if (!isset($order)) $order = 1;
$order = ($order == 1) ? 1 : 0;


$PreviewFileName = (file_exists($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_builder/preview".$ReportCardTemplate.".php")) ? "preview".$ReportCardTemplate.".php" : "preview.php";
$YearNameField = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');
$TermNameField = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');

$sql  = "SELECT
			CONCAT('<a class=\'tablelink\' href=\'edit.php?ReportID=', rt.ReportID, '\'>', REPLACE(rt.ReportTitle, '::', '<br />'), '</a>') as ReportTitle,			
			CONCAT('<a class=\'tablelink\' href=\"javascript:newWindow(\'".$PreviewFileName."?ReportID=', rt.ReportID, '\', \'10\')\" >
			<img src=\'$image_path/$LAYOUT_SKIN/icon_view.gif\' border=0 align=\'absmiddle\'>$button_preview</a>') as Preview,
			if(rt.Description IS NULL OR rt.Description='', '".$Lang['General']['EmptySymbol']."', rt.Description) as Description,
			if(rt.AcademicYear IS NULL OR rt.AcademicYear='', '".$Lang['General']['EmptySymbol']."', ".$YearNameField.") as AcademicYear,
			if(rt.ReportType='F', '".$eReportCard['FullYear']."', ".$TermNameField.") as ReportType,
			if(rt.RecordStatus=1, '".$eReportCard['Public']."', '".$eReportCard['Private']."') as RecordStatus,
			rt.DateModified,
			CONCAT('<input type=checkbox name=ReportID[] value=', rt.ReportID ,'>')
         FROM
            RC_REPORT_TEMPLATE as rt
			Left Outer Join
			ACADEMIC_YEAR as ay On (rt.AcademicYear = ay.AcademicYearID)
			Left Outer Join
			ACADEMIC_YEAR_TERM as ayt On (rt.ReportType = ayt.YearTermID)
         ";
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("ReportTitle", "Preview", "Description", "AcademicYear", "ReportType", "RecordStatus", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,1,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%' >".$li->column($pos++, $i_general_title)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $button_preview)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_general_description)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['General']['SchoolYear'])."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_Type)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_status)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("ReportID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')", '', '', '', '', 0);

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['ReportBuilder_Template'], "", 0);

// [2016-0203-1700-19207] Message after update
$Result2 = "";
if(trim($Result)=="update"){
	$Result = "";
	$Result2 = $eReportCard["NeedToReGenAfterUpdate"];
}

$linterface->LAYOUT_START();

?>


<br/>
<script language="javascript">
<!--
function jREMOVE_REPORT(obj,element,page){
		var alertConfirmRemove = "<?=$eReportCard['RemoveItemAlert']?>";
        if(countChecked(obj,element)==0)
			alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}

function jUPDATE_STATUS(obj,element,page){
		obj.action=page;
		obj.method="POST";
		obj.submit();
}
//-->
</script>

<form name="form1" method="get">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr><td><?= $toolbar ?></td><td align="right"><?= $linterface->GET_SYS_MSG($Result, $Result2); ?></td></tr>
<tr><td>&nbsp;</td><td align="right">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
  <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif"><table border="0" cellspacing="0" cellpadding="2">
      <tr>
		<td nowrap="nowrap"><a href="javascript:jUPDATE_STATUS(document.form1,'ReportID[]','status_update.php?RecordStatus=1')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_public.gif" width="12" height="12" border="0" align="absmiddle">         <?=$eReportCard['Public']?></a></td>
		<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
		<td nowrap="nowrap"><a href="javascript:jUPDATE_STATUS(document.form1,'ReportID[]','status_update.php?RecordStatus=0')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_private.gif" width="12" height="12" border="0" align="absmiddle">
          <?=$eReportCard['Private']?></a></td>
		<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
        <td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'ReportID[]','edit.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle">
          <?=$button_edit?></a></td>
        <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
        <td nowrap="nowrap"><a href="javascript:jREMOVE_REPORT(document.form1,'ReportID[]','remove.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle">
          <?=$button_remove?></a></td>
      </tr>
    </table></td>
  <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
</tr>
</table>
<tr><td colspan="2"><?= $li->display() ?></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>