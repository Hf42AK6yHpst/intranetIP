<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

if($SubjectGradingID!="")
{
	if(!empty($Form))
	{
		# Step 1: Remove old record in RC_SUBJECT_GRADING_SCHEME_FORM
		$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME_FORM WHERE SubjectGradingID = '$SubjectGradingID'";
		$li->db_db_query($sql);
		for($i=0; $i<sizeof($Form); $i++)
		{
			$ClassLevelID = $Form[$i];

			# Step 2: Insert reocrd in RC_SUBJECT_GRADING_SCHEME_FORM
			$sql = "INSERT INTO RC_SUBJECT_GRADING_SCHEME_FORM (SubjectGradingID, ClassLevelID, DateInput, DateModified) VALUES ('$SubjectGradingID', '$ClassLevelID', now(), now())";
			$li->db_db_query($sql);
		}
	}
}
intranet_closedb();
?>
<script language='javascript'>
opener.location.reload();
self.close();
</script>
