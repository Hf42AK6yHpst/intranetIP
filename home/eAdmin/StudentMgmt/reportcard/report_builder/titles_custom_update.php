<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$lf = new libfilesystem();

# custom lang paths
$EngCustomFile = $intranet_root."/file/reportcard/lang.en.php";
$ChiCustomFile = $intranet_root."/file/reportcard/lang.b5.php";

# get and save customized lang values
$EngFileContent = "<?php \n";
$ChiFileContent = "<?php \n";
$ChiTitleArray = $eReportCard_ReportTitleArray["b5"];
$EngTitleArray = $eReportCard_ReportTitleArray["en"];

$BilingualSpecialArray = array("ClassTeacher", "FullMark", "OverallResult", "Principal", "ParentGuardian", "SchoolChop", "Attendance", "MeritsAndDemerits", "Merit", "Demerit");
foreach($EngTitleArray as $ID => $DefaultTitle)
{
	$ID = trim($ID);
	$EngTitle = trim(${$ID."_eng"});
	$ChiTitle = trim(${$ID."_chi"});
	$VariableName = "eReportCard['".$ID."']";
	$BilingualVariableName = "eReportCardBilingual['".$ID."']";
	$BilingualTitle = "";
	$BliingualSpace = (in_array($ID, $BilingualSpecialArray)) ? "<br />" : " ";
	if(!empty($ChiTitle))
	{
		$ChiTitle = str_replace("\'", "'", $ChiTitle);
		if($ID=="Subject") {
			$ChiVariableName = "eReportCard['ChiSubject']";
			$ChiVariable = "$".$ChiVariableName." = \"".$ChiTitle."\";\n";
			$ChiFileContent .= $ChiVariable;
			$EngFileContent .= $ChiVariable;
		}
		$ChiVariable = "$".$VariableName." = \"".$ChiTitle."\";\n";
		$ChiFileContent .= $ChiVariable;
		$BilingualTitle .= $ChiTitle;
	}
	else 
		$BilingualTitle .= $ChiTitleArray[$ID];

	if(!empty($EngTitle))
	{
		$EngTitle = str_replace("\'", "'", $EngTitle);
		if($ID=="Subject") {
			$EngVariableName = "eReportCard['EngSubject']";
			$EngVariable = "$".$EngVariableName." = \"".$EngTitle."\";\n";
			$EngFileContent .= $EngVariable;
			$ChiFileContent .= $EngVariable;
		}
		$EngVariable = "$".$VariableName." = \"".$EngTitle."\";\n";
		$EngFileContent .= $EngVariable;
		$BilingualTitle .= $BliingualSpace.$EngTitle;
	}
	else 
		$BilingualTitle .= $BliingualSpace.$EngTitleArray[$ID];

	$BilingualVariable = "$".$BilingualVariableName." = \"".$BilingualTitle."\";\n";
	$ChiFileContent .= $BilingualVariable;
	$EngFileContent .= $BilingualVariable;
}

$EngFileContent .= "?>";
$ChiFileContent .= "?>";

$success = $lf->file_write($EngFileContent, $EngCustomFile);
$success = $lf->file_write($ChiFileContent, $ChiCustomFile);

$Result = ($success==1) ? "update" : "update_failed";
intranet_closedb();
header("Location:titles_custom.php?Result=$Result");
?>

