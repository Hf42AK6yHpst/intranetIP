<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

$ReportSubjectArr = $lreportcard->GET_REPORT_SUBJECT_ID($ReportID);
$ReportColumnArr = $lreportcard->GET_REPORT_COLUMN_ID($ReportID);

$success = 1;
for($i=0; $i<sizeof($ReportColumnArr); $i++)
{
	$rc_id = $ReportColumnArr[$i];
	for($j=0; $j<sizeof($ReportSubjectArr); $j++)
	{
		$rs_id = $ReportSubjectArr[$j];
		$Scale = ${"Scale_".$rc_id."_".$rs_id};

		# remove the old record
		$sql = "DELETE FROM RC_REPORT_TEMPLATE_CELL WHERE ReportColumnID = '$rc_id' AND ReportSubjectID = '$rs_id'";
		$success = $li->db_db_query($sql);

		# insert new record
		$sql = "INSERT INTO RC_REPORT_TEMPLATE_CELL (ReportColumnID, ReportSubjectID, Setting, DateInput, DateModified) VALUES ('$rc_id', '$rs_id', '$Scale', now(), now())";
		$success = $li->db_db_query($sql);
	}
}

$Result = ($success) ? "update" : "update_failed";
intranet_closedb();
header("Location:index.php?Result=$Result");
?>

