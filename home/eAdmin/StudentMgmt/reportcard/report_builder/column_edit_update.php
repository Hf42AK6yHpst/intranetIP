<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

if($ReportColumnID!="")
{
	################################# STEP 1: GET DisplayOrder #####################################
	# Get Column Display Order list
	$sql = "SELECT ReportColumnID, DisplayOrder FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID = '".$ReportID."' ORDER BY DisplayOrder";
	$row = $li->returnArray($sql, 2);
	for($i=0; $i<sizeof($row); $i++)
	{
		list($rc_id, $order) = $row[$i];
		$RCOrderArr[$order] = $rc_id;
	}

	$DisplayOrder = "";
	if(is_array($RCOrderArr))
	{
		$OrderArr = array_keys($RCOrderArr);
		if($InsertAfterID!="")
		{
			for($i=0; $i<sizeof($OrderArr); $i++)
			{
				$order = $OrderArr[$i];
				$rc_id = $RCOrderArr[$order];
				if($rc_id==$InsertAfterID)
				{
					if($RCOrderArr[$OrderArr[$i+1]]==$ReportColumnID)
					{
						$NoOrderChange = 1;
					}
					else
					{
						$DisplayOrder = ($OrderArr[$i+1]!="") ? $OrderArr[$i+1] : "";
						$Reorder = 1;
					}
					break;
				}
			}
		}
		else
		{
			if($OrderArr[0]==$OldDisplayOrder)
			{
				$NoOrderChange = 1;
			}
			else
			{			
				$DisplayOrder = 1;
				$Reorder = 1;
			}
		}
		
		if($DisplayOrder=="")
		{
			# Get Max Display Order
			$sql = "SELECT MAX(DisplayOrder) FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID = '".$ReportID."'";
			$row = $li->returnVector($sql);
			$LastOrder = $row[0];	
				
			$DisplayOrder = ($LastOrder=="") ? 1 : $LastOrder+1;
			$Reorder = 0;
		}	
	}

	################################# STEP 2: Update / Insert Record #####################################
	# Update record to RC_REPORT_TEMPLATE_COLUMN
	$ColumnTitle = ($ColumnTitle=="") ? intranet_htmlspecialchars($CustomTitle) : $ColumnTitle;
	$IsColumnSum = ($ColumnTitle == "SYS_SUM_COLUMN") ? 1 : 0;
	if ($IsColumnSum) $ColumnTitle = $WeightCustomTitle;
	
	$ShowPosition = ($ShowPosClassColumn==="") ? 0 : $ShowPosition;
	$PositionRange = ($ShowPosition==1 || $ShowPosition==2) ? $PositionFrom.",".$PositionTo : "";
	$NotAllowToInput = ($NotAllowToInput==="") ? 0 : $NotAllowToInput;	
			
	$FieldValue = "ColumnTitle='$ColumnTitle', ";
	$FieldValue .= "Weight='$Weight', ";
	$FieldValue .= ($NoOrderChange!=1) ? "DisplayOrder='$DisplayOrder', " : "";
	$FieldValue .= "ShowPosition='$ShowPosition', ";
	$FieldValue .= "PositionRange='$PositionRange', ";
	$FieldValue .= "DateModified=now(), ";
	$FieldValue .= "IsColumnSum='$IsColumnSum', ";
	$FieldValue .= "MarkSummaryWeight='$MarkSummaryWeight', ";
	$FieldValue .= "NotForInput='$NotAllowToInput'";

	$sql = "UPDATE RC_REPORT_TEMPLATE_COLUMN SET $FieldValue WHERE ReportColumnID = '$ReportColumnID'";
	$success = $li->db_db_query($sql);
		
	################################# STEP 3: Reordering #####################################
	if($NoOrderChange!=1)
	{
		# remove the RecordColumnID from the array
		unset($RCOrderArr[$OldDisplayOrder]);
		unset($OrderArr);
		$OrderArr = array_keys($RCOrderArr);
		
		# Reordering
		if($success==1 && $Reorder==1 && is_array($OrderArr))
		{
			$NewOrder = $DisplayOrder+1;
			$StartPos = ($DisplayOrder==1) ? 0 : array_search($DisplayOrder, $OrderArr);
			$SortingOrderArray = array_slice($OrderArr, $StartPos);

			for($i=0; $i<sizeof($SortingOrderArray); $i++)
			{
				$Order = $SortingOrderArray[$i];
				$rcid = $RCOrderArr[$Order];
				$sql = "UPDATE RC_REPORT_TEMPLATE_COLUMN SET DisplayOrder = '".$NewOrder."' WHERE ReportColumnID = '".$rcid."'";
				$li->db_db_query($sql);

				$NewOrder++;
			}
		}
	}
}

intranet_closedb();
$Result = ($success==1) ? "update" : "update_failed";
header("Location:column_manage.php?Result=$Result&ReportID=$ReportID&ReportType=$ReportType");
?>

