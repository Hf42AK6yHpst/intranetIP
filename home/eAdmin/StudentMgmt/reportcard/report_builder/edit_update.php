<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$ReportType = ($YearTermID == 0)? 'F' : $YearTermID;

$SerializedSettings = serialize($Settings);
$ReportTitle = $ReportTitle1."::".$ReportTitle2;
$Description = intranet_htmlspecialchars($Description);
$Footer = intranet_htmlspecialchars($Footer);
$ReportTitle = intranet_htmlspecialchars($ReportTitle);

$ShowPosition = ($ShowPosClassColumn==="") ? 0 : $ShowPosition;
$PositionRange = ($ShowPosition==1 || $ShowPosition==2) ? $PositionFrom.",".$PositionTo : "";
if ($NoHeader == 1) $NoHeader = $NoHeaderBr;
if ($LineHeight < 20) $LineHeight = 20;
if ($SignatureWidth < 120) $SignatureWidth = 120;

# Step 1: Insert record to RC_SUBJECT_GRADING_SCHEME
$sql = "UPDATE 
			RC_REPORT_TEMPLATE 
		SET 
			ReportTitle='$ReportTitle', 
			ReportType='$ReportType', 
			Description='$Description',
			Footer='$Footer', 
			ShowFullMark='$ShowFullMark', 
			DisplaySettings='$SerializedSettings', 
			RecordStatus='$RecordStatus', 
			ShowPosition='$ShowPosition', 
			PositionRange='$PositionRange',
			HideNotEnrolled='$HideNotEnrolled',
			NoHeader='$NoHeader',
			LineHeight='$LineHeight',
			SignatureWidth='$SignatureWidth',
			AcademicYear='$AcademicYearID',
			DecimalPoint='$DecimalPoint',
			DateModified=now() 
		WHERE 
			ReportID = '$ReportID' 
		";
$li->db_db_query($sql);

# Step 2.1: Remove the old records 
$sql = "DELETE FROM RC_REPORT_TEMPLATE_FORM WHERE ReportID = '$ReportID'";
$li->db_db_query($sql);

# Step 2.2: Insert reocrd in RC_SUBJECT_GRADING_SCHEME_FORM
for($i=0; $i<sizeof($Form); $i++)
{
	$ClassLevelID = $Form[$i];
	$sql = "INSERT INTO RC_REPORT_TEMPLATE_FORM (ReportID, ClassLevelID, DateInput, DateModified) VALUES ('$ReportID', '$ClassLevelID', now(), now())";
	$li->db_db_query($sql);
}

intranet_closedb();
header("Location:report_subject.php?ReportID=$ReportID&ReportType=$ReportType");
?>

