<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$ReportIDList = (is_array($ReportID)) ? implode(",", $ReportID) : $ReportID;

# Step 1: Remove record(s) in RC_REPORT_TEMPLATE
$sql = "DELETE FROM RC_REPORT_TEMPLATE WHERE ReportID IN ($ReportIDList)";
$success = $li->db_db_query($sql);

if($success==1)
{
	# Step 2:  Remove record(s) in RC_REPORT_TEMPLATE_FORM
	$sql = "DELETE FROM RC_REPORT_TEMPLATE_FORM WHERE ReportID IN ($ReportIDList)";
	$li->db_db_query($sql);

	# Step 3: Retrieve ReportSubjectID and ReportColumnID for record(s) removal in RC_REPORT_TEMPLATE_CELL
	$sql = "SELECT ReportSubjectID FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportID IN ($ReportIDList)";
	$ReportSubjectIDArr = $li->returnVector($sql);
	$ReportSubjectIDList = implode(",", $ReportSubjectIDArr);

	$sql = "SELECT ReportColumnID FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID IN ($ReportIDList)";
	$ReportColumnIDArr = $li->returnVector($sql);
	$ReportColumnIDList = implode(",", $ReportColumnIDArr);

	$sql = "DELETE FROM RC_REPORT_TEMPLATE_CELL WHERE ReportSubjectID IN ($ReportSubjectIDList) AND ReportColumnID IN ($ReportColumnIDList)";
	$li->db_db_query($sql);

	# Step 4: Remove record(s) in RC_REPORT_TEMPLATE_SUBJECT and RC_MARKSHEET_SCORE 
	$sql = "DELETE FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportID IN ($ReportIDList)";
	$li->db_db_query($sql);

	$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE ReportColumnID IN (".$ReportColumnIDList.")";
	$li->db_db_query($sql);

	# Step 5:  Remove record(s) in RC_REPORT_TEMPLATE_COLUMN, RC_SUB_MARKSHEET_COLUMN and RC_SUB_MARKSHEET_SCORE 
	$sql = "DELETE FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID IN ($ReportIDList)";
	$li->db_db_query($sql);

	$sql = "DELETE FROM RC_SUB_MARKSHEET_COLUMN WHERE ReportColumnID IN (".$ReportColumnIDList.")";
	$li->db_db_query($sql);

	$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ReportColumnID IN (".$ReportColumnIDList.")";
	$li->db_db_query($sql);

}

intranet_closedb();

$Result = ($success==1) ? "delete" : "delete_failed";
header("Location:index.php?Result=$Result");
?>

