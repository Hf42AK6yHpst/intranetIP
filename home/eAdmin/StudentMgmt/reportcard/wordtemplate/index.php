<?php

$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "PageMarksheetRevisionPresetComment";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

$lf = new libwordtemplates(1);
$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$file_array = $lf->file_array;
$word_array = $lf->word_array;


$type +=0;

$type_select = "<SELECT name=type onChange=\"location='index.php?type='+this.value\">";
for ($i=0; $i<sizeof($file_array); $i++)
{
     $selected_str = ($type==$i? "SELECTED":"");
     $type_select .= "<OPTION value=$i $selected_str>".$word_array[$i]."</OPTION>\n";
}
$type_select .= "</SELECT>\n";

$disable = false;
$data = get_file_content($base_dir.$file_array[$type]);

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['PresetComment'], "", 0);

$linterface->LAYOUT_START();


?>

<script language="javascript">
function checkform(obj){
     return true;
}
</script>

<form action="update.php" name="form1" method="post">

<table width="96%" border="0" cellpadding="2" cellspacing="0" align="center">
<tr>
	<td class="tabletext" align="center">
		<table width="60%" border="0" cellpadding="2" cellspacing="0" align="center">
		<tr><td class="tabletext"><br>
		<?=$i_wordtemplates_select?> <?=$type_select?><br>
			<textarea name=data COLS=60 ROWS=20><?=$data?></textarea>
			<br><span class="extraInfo"><?=$i_wordtemplates_instruction?></span>
		</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
</form>

<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>