<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

#intranet_opendb();

$lf = new libwordtemplates(1);
$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$file_array = $lf->file_array;
if ($type >= sizeof($file_array) || $type < 0)
{
    header ("Location: index.php");
}
else
{
    $file_target = $file_array[$type];
    $data = stripslashes($data);
    $lf->file_write($data,"$base_dir$file_target");
    header("Location: index.php?type=$type&msg=2");
}

?>