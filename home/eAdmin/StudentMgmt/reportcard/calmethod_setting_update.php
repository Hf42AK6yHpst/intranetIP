<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$file_content = "";

$Temp = ($CalculationMethodRule == 1) ? 1:0; 

$file_content .= $Temp."\n";
$file_content .= $CalculationOrder."\n";

# Write to file
$li = new libfilesystem();
$location = $intranet_root."/file/reportcard";
$li->folder_new($location);
$file = $location."/calmethod_setting.txt";

$success = $li->file_write($file_content, $file);

$Result = ($success==1) ? "update" : "update_failed";
header("Location: calmethod_setting.php?Result=$Result");
?>