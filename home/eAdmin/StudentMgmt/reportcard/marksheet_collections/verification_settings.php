<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "MarksheetCollection_VerificationPeriodSetting";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		# Get collection period
		$VerificationPeriod = $lreportcard->GET_MARKSHEET_VERIFICATION();
		list($StartDate, $EndDate) = $VerificationPeriod[0];
		
		$StartDatePicker = $linterface->GET_DATE_PICKER("StartDate", $StartDate);
		$EndDatePicker = $linterface->GET_DATE_PICKER("EndDate", $EndDate);
				
        # tag information
		$TAGS_OBJ[] = array($eReportCard['ResultVerificationPeriodSetting'], "", 0);
		
		$linterface->LAYOUT_START();

		?>
		<script language="javascript">
		function checkform(obj){
			if(!check_text(obj.StartDate, "<?php echo $i_alert_pleasefillin.$eReportCard['Period']; ?>.")) return false;
			if(!check_date(obj.StartDate, "<?php echo $eReportCard['InvalidDateFormatAlert']; ?>")) return false;
			if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$eReportCard['Period']; ?>.")) return false;
			if(!check_date(obj.EndDate, "<?php echo $eReportCard['InvalidDateFormatAlert']; ?>")) return false;
			if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
					obj.StartDate.focus();
					alert("<?= $eReportCard['InvalidPeriod'] ?>");
					return false;
			}
			return true;
		}
		
		function jGO_PRINT_MENU()
		{
			newWindow('print_menu.php', 15);
			//obj = document.form1;
			//obj.action = "print_menu.php";
			//obj.target = "_blank";
			//obj.submit();
		}
		</script>

		<form name="form1" action="verification_settings_update.php" method="post"onSubmit="return checkform(this);">
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="4" cellspacing="0" align="center">
						<tr><td colspan="2" align="left" class="tabletextremark"><?=$eReportCard['MarksheetVerificationPeriodRemind']?></td></tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['StartDate']; ?> </td>
							<td width="70%"><?=$StartDatePicker?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['EndDate']; ?> </td>
							<td width="70%"><?=$EndDatePicker?></td>
						</tr>
						<tr>							
							<td class="tabletext" colspan="2"><?=$PrintBtn = $linterface->GET_SMALL_BTN($eReportCard['VerificationReportPriting'], "button", "javascript:jGO_PRINT_MENU()") ?>&nbsp;</td>							
						</tr>	
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		</form>
	<?= $linterface->FOCUS_ON_LOAD("form1.StartDate") ?>

	<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
