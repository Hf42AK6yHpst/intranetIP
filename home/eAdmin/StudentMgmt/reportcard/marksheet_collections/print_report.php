<?php
//echo "<script>if(typeof(opener)!='undefined') { opener.close(); }</script>";
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

############################ Function Start ######################################

		
# function to get Title Table
function GENERATE_TITLE_TABLE($ParReportTitle)
{
	global $SchoolName;

	if(!empty($ParReportTitle) || !empty($SchoolName))
	{
		$TitleTable = "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";

		list($ReportTitle1, $ReportTitle2) = explode("::", $ParReportTitle);
		if(!empty($SchoolName))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
		if(!empty($ReportTitle1))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle1."</td></tr>\n";
		if(!empty($ReportTitle2))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle2."</td></tr>\n";

		$TitleTable .= "</table>\n";
	}

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $today, $eReportCard;

	list($ClassName, $ClassNumber, $ChiName, $EngName) = $ParInfoArray;
	
	$StudentInfoTable .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";
	$StudentInfoTable .= "<tr>
							<td class='report_contenttitle'  nowrap='nowrap' width=\"50%\">".$eReportCard['Name']."�G".$EngName."</td>
							<td class='report_contenttitle nowrap='nowrap' width=\"50%\">".$eReportCard['ClassName']."�G".$ClassName." (".$ClassNumber.")</td>
						</tr>";
	$StudentInfoTable .= "</table>";
	
	
	return $StudentInfoTable;
}

# function to get result table
function GENERATE_DETAILS_RESULT_TABLE($ParStudentResultArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard, $SettingArray, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray;

	$MarkTypeDisplay = $SettingArray['MarkTypeDisplay'];
	$ColumnSize = sizeof($ColumnArray);
	$IsFirst = 1;
	if(is_array($SubjectArray))
	{
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;

					# Do not show subject if there is no results
					if(empty($ParStudentResultArray[$SubjectID]))
						continue;
					
					if($CmpCodeID>0) {
						$SubjectName = $SubjectArray[$CodeID][0][2]." - ".$EngSubjectName."&nbsp;&nbsp;".$SubjectArray[$CodeID][0][3]." - ".$ChiSubjectName;
					} else {
						$SubjectName = $EngSubjectName."&nbsp;&nbsp;".$ChiSubjectName;
					}

					# Full Mark / Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

					$FullMarkDisplay = (!empty($FullMark)) ? " (".$eReportCard['FullMark']." ".$FullMark.")" : "";
					if($IsFirst==1) {
						$IsFirst = 0;
					} else {
						$ReturnContent .= "<br />";
					}
					$ReturnContent .= "<span class=\"report_contenttitle\">".$SubjectName.$FullMarkDisplay."</span>";
					$ReturnContent .= "<table width=\"100%\" class=\"report_border\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						$ColumnTitle = (!empty($Weight)) ? $ColumnTitle." ".$Weight."%" : $ColumnTitle;
						
						if($CellSetting=="S") {
							$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
							$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
							$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
							if($s_raw_mark>=0) {
								if($MarkTypeDisplay==1)
									$DisplayResult = $s_raw_mark;
								else if($MarkTypeDisplay==2)
									$DisplayResult = $s_grade;
								else
									$DisplayResult = $s_weighted_mark;
							}
							else 
								$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
						}
						else
							$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

						$dotline_style = (($i+1)==sizeof($ColumnArray))? "dotline" : "";
						$ReturnContent .= "<tr>
										<td class='tabletext {$dotline_style}' valign='top'>".$ColumnTitle.": </td>
										<td class='tabletext {$dotline_style}' align='right' valign='top'>".($DisplayResult==""?"--":$DisplayResult)."</td>
										</tr>
										";
					}				
					if($ColumnSize>1)
					{
						if($CellSetting=="S") {
							$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
							$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
									
							if($s_overall_mark>=0)
								$OverallResult = ($MarkTypeDisplay==2) ? $s_overall_grade : $s_overall_mark;
							else
								$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
						}
						else
							$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];

						$ReturnContent .= "<tr>
										<td class='tabletext' valign='top'>".$eReportCard['OverallResult'].": </td>
										<td class='tabletext' align='right' valign='top'>".($OverallResult==""?"--":$OverallResult)."</td>
										</tr>
										";
					}
					$ReturnContent .= "</table>";
				}
			}
		}
	}

	return $ReturnContent;
}
################################ Function End #####################################

//StartTimer();
if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "ReportGeneration";

	if ($libreportcard->hasAccessRight())
    {
		$linterface = new interface_html();
#######################################################################################################
		# Get ReportTitle
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		$ReportTitle = $ReportInfo[0];
		$Settings = $ReportInfo[4];
		$SettingArray = unserialize($Settings);
		
		# get school name
		$SchoolName = GET_SCHOOL_NAME();

		# Get Class Level(s) of the report
		if($ClassLevelID!="")
			$SelectedFormArr = array($ClassLevelID);
		else
			$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
		
		# Get Class Student(s)
		$LevelList = implode(",", $SelectedFormArr);

		$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";
		
		list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID, $TargetStudentList, 1);

		# Get Report Subject and Column
		$SubjectArray = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID);
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		# Get Report Cell Setting
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);

		# Get Report Marksheet result
		$SubjectIDArray = $libreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);
		if(is_array($SubjectIDArray) && is_array($StudentIDArray))
		{
			$SubjectIDList = implode(",", $SubjectIDArray);
			$StudentIDList = implode(",", $StudentIDArray);
			$ReportMarksheetArray = $libreportcard->GET_REPORT_MARKSHEET_SCORES($SubjectIDList, $StudentIDList);
		}
#######################################################################################################

		# Generate Report
		# Generate Title Table
		$TitleTable = GENERATE_TITLE_TABLE($ReportTitle);
		
		$rx = "";
		$PageBreakStyle = "style='page-break-after:always'";
		if(is_array($ClassStudentArray))
		{
			foreach($ClassStudentArray as $ClassID => $StudentArray)
			{
				if(is_array($StudentArray))
				{
					foreach($StudentArray as $StudentID => $InfoArray)
					{
						$UserInfoArray = $InfoArray["UserInfo"];
						
						# Generate Student Info Table
						$StudentInfoTable = GENERATE_STUDENT_INFO_TABLE($UserInfoArray);
						
						$Content .= "<tr ><td align='center' {$PageBreakStyle}>";
						
						$Content .= "<table width='90%' border='0' cellspacing='0' cellpadding='2'>";
						$Content .= "<tr height='80' width='55%'>";
						$Content .= "<td colspan=\"2\" valign='top'>".$TitleTable."</td></tr></table>";
						$Content .= $StudentInfoTable;
						
						$Content .= "<table width='100%' class=\"report_border\" align=\"left\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" >";
						$Content .= "<tr><td>";
						# Generate Details Table
						$Content .= GENERATE_DETAILS_RESULT_TABLE($ReportMarksheetArray[$StudentID]);
						$Content .= "</td></tr>";
						$Content .= "<tr><td align='left' class='tabletext'>".$eReportCard['MarkRemind']."</td></tr>";
						$Content .= "</table>";

						$Content .= "</td></tr>";
					}
				}
			}
		}

##########################################################################################
//debug(StopTimer());

?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" valign="top">
<tr><td class="print_hide" align='right' height='50' ><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
<?=$Content?>
<tr><td class="print_hide" align='right' height='50'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>