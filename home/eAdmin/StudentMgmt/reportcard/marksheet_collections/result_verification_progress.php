<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "MarksheetCollection_VerificationProgress";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

		# Get Report Template Selection
		$ReportArray = $libreportcard->GET_ALL_TEMPLATES();

		if(!empty($ReportArray))
		{
			$ReportID = ($ReportID=="") ? $ReportArray[0][0] : $ReportID;
			$ReportSelection = getSelectByArray($ReportArray, "name='ReportID' onChange='document.form1.report_change.value=1;document.form1.submit()'", $ReportID, 1, 1, "");
			
			# Get Report Form Selection
			$FormArray = $libreportcard->GET_REPORT_FORMS($ReportID);
			$ClassLevelID = ($ClassLevelID=="" || $report_change==1) ? $FormArray[0][0] : $ClassLevelID;
			$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange='document.form1.submit()'", $ClassLevelID, 1, 1, "");		
			# Get report subject class
			$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
			$SubjectGradingArr = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
			$SubjectArray = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID, $SubjectGradingArr);
			
			$col_size = sizeof($SubjectArray) + 1;
			$FilterRow = "<tr><td colspan='$col_size' class='tabletext'>";
			$FilterRow .= $eReportCard['Report'].": ".$ReportSelection."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$FilterRow .= $eReportCard['Form'].": ".$FormSelection."</td></tr>";

			$TableContent = "";
			if(is_array($SubjectArray))
			{
				$TableContent .= "<tr >";
				$TableContent .= "<td  width='10%'>&nbsp;</td>";
				foreach($SubjectArray as $CodeID => $Data)
				{
					if(is_array($Data))
					{
						foreach($Data as $CmpCodeID => $InfoArr)
						{
							if($CmpCodeID==0)
							{
								list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale) = $InfoArr;
								$TableContent .= "<td class=\"tabletoplink tablebluetop\" align=\"center\">".($intranet_session_language=="en"?$EngSubjectName:$ChiSubjectName)."</td>";
								$SubjectIDArray[] = $SubjectID;
							}
						}
					}
				}
				$TableContent .= "</tr>";
			}
			
			# Get Classes by ClassLevelID
			$ReportInfoArr = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
			$ReportAcademicYearID = $ReportInfoArr['AcademicYear'];
			$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ReportAcademicYearID);
			
			# Get marksheet submission status
			$ClassCountingArray = $libreportcard->GET_CLASS_CONFIRMATION_COUNT($ReportID, $ClassLevelID, $ReportAcademicYearID);
			for($i=0; $i<sizeof($ClassArray); $i++)
			{
				list($ClassID, $ClassName) = $ClassArray[$i];
				
				//$StudentTotal = ($ClassCountingArray[$ClassID]["Total"]=="") ? 0 : $ClassCountingArray[$ClassID]["Total"];

				$css = ($i%2?"2":"1");
				$TableContent .= "<tr class='tablebluerow$css'>";
				$TableContent .= "<td class=\"tablebluelist \" nowrap=\"nowrap\">".$ClassName."</td>";
				for($k=0; $k<sizeof($SubjectIDArray); $k++)
				{
					$SubjectID = $SubjectIDArray[$k];
					
					$StudentTotal = ($ClassCountingArray[$ClassID][$SubjectID]['TotalNumOfStudent']=="") ? 0 : $ClassCountingArray[$ClassID][$SubjectID]['TotalNumOfStudent'];
					$ConfirmedCount = $ClassCountingArray[$ClassID][$SubjectID]['NumOfConfirmed'];
					$ConfirmedCount = ($ConfirmedCount=="") ? 0 : $ConfirmedCount;
					$TextColor = ($ConfirmedCount==$StudentTotal) ? "blue" : "red";
					
					$TableContent .= "<td class=\"tabletext\" align=\"center\"><font color='".$TextColor."'>".$ConfirmedCount." / ".$StudentTotal."</font></td>";
				}			
			}
		}
		else
		{
			$TableContent .= "<tr class='tabletext'><td align='center'>".$eReportCard['NoReport']."</td></tr>";
		}
############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['MonitorResultVerificationProgress'], "", 0);

$linterface->LAYOUT_START();

?>
<script language="javascript">
function jHIDE_NOTIFICATION(jNID){
	var alertConfirmHide = "<?=$eReportCard['HideNotifyAlert']?>";
	if(confirm(alertConfirmHide))
	{
		self.location.href='hide_notify.php?NotificationID='+jNID;
	}
}
</script>
<br/>
<form name="form1" action="" method="POST">
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr><td align="right" colspan="<?=$col_size?>"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
<?=$FilterRow?>
<tr><td colspan="<?=$col_size?>"><br/></td></tr>
<?=$TableContent?>
</table>
<br/>
<input type="hidden" name="report_change" value=0 />
</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>