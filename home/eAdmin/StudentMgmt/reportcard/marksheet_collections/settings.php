<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "MarksheetCollection_PeriodSetting";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		# Get collection period
		//$Year = ($libreportcard->Year=="") ? date('Y') : $libreportcard->Year;
		$Year = ($libreportcard->Year=="") ? Get_Current_Academic_Year_ID() : $libreportcard->Year;
		$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $tag='onchange="js_Changed_Academic_Year_Selection(this.value);"', $noFirst=1, $noPastYear=0, $Year);
		
		# display duplicated classlevel
		$DuplicatedTable = $libreportcard->GET_DUPLICATED_CLASS_REPORT_TABLE();
		
		# get semester list
		$jsYearTermID = ($ReportType == 'F')? 0 : $libreportcard->Semester;
		
		/*
		$semester_data = getSemesters();
		if(!empty($semester_data))
		{
			$SemesterSelect = "<SELECT name='Semester'>";
			for($i=0; $i<sizeof($semester_data); $i++)
			{
				$TmpArr = explode("::", $semester_data[$i]);
				$t_semester = $TmpArr[0];
				$Selected = (($libreportcard->Semester==$t_semester) || ($libreportcard->Semester=="" && $TmpArr[1]==1)) ? "SELECTED='SELECTED'" : "";
				$SemesterSelect .= "<OPTION value='".$t_semester."' $Selected>".$t_semester."</OPTION>";
			}
			$SemesterSelect .= "<OPTION value='FULL' ".($libreportcard->Semester=="FULL"?"SELECTED='SELECTED'" : "").">".$eReportCard['FullYear']."</OPTION>";
			$SemesterSelect .= "</SELECT>";
		}
		*/
		
		
		### Start Date & End Date
		$StartDatePicker = $linterface->GET_DATE_PICKER("StartDate", $libreportcard->StartDate);
		$EndDatePicker = $linterface->GET_DATE_PICKER("EndDate", $libreportcard->EndDate);
		
		
        # tag information
		$TAGS_OBJ[] = array($eReportCard['MarksheetCollectionPeriodSetting'], "", 0);
		
		$linterface->LAYOUT_START();
		
		?>
		<script language="javascript">
		
		$(document).ready(function () {
			js_Reload_Term_Selection($('select#AcademicYearID').val(), '<?=$jsYearTermID?>');
		});
		
		function js_Changed_Academic_Year_Selection(jsAcademicYearID)
		{
			js_Reload_Term_Selection(jsAcademicYearID);
		}
		
		function js_Reload_Term_Selection(jsAcademicYearID, jsYearTermID)
		{
			$('div#TermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
				"../ajax_reload.php", 
				{ 
					Action: 'Term_Selection',
					SelectionID: 'YearTermID',
					AcademicYearID: jsAcademicYearID,
					YearTermID: jsYearTermID,
					NoFirst: 1,
					WithWholeYear: 1
				},
				function(ReturnData)
				{
					
				}
			);
		}

		function checkform(obj){
			if(!check_text(obj.StartDate, "<?php echo $i_alert_pleasefillin.$eReportCard['Period']; ?>.")) return false;
			if(!check_date(obj.StartDate, "<?php echo $eReportCard['InvalidDateFormatAlert']; ?>")) return false;
			if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$eReportCard['Period']; ?>.")) return false;
			if(!check_date(obj.EndDate, "<?php echo $eReportCard['InvalidDateFormatAlert']; ?>")) return false;
			if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
					obj.StartDate.focus();
					alert("<?= $eReportCard['InvalidPeriod'] ?>");
					return false;
			}
			return true;
		}
		</script>

		<form name="form1" action="settings_update.php" method="post"onSubmit="return checkform(this);">
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr><td><?=$DuplicatedTable?></td></tr>
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr><td colspan="2" align="left" class="tabletextremark"><?=$eReportCard['MarksheetCollectionPeriodRemind']?></td></tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_Year; ?> </td>
							<td><?=$AcademicYearSelection?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_Semester; ?> </td>
							<td><div id="TermSelectionDiv"></div></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['StartDate']; ?> </td>
							<td><?=$StartDatePicker?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['EndDate']; ?> </td>
							<td><?=$EndDatePicker?></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowClassTeacherComment" id="AllowClassTeacherComment" value=1 <?=($libreportcard->AllowClassTeacherComment==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherComment"><?=$eReportCard['AllowClassTeacherComment']?></label></span></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowSubjectTeacherComment" id="AllowSubjectTeacherComment" value=1 <?=($libreportcard->AllowSubjectTeacherComment==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowSubjectTeacherComment"><?=$eReportCard['AllowSubjectTeacherComment']?></label></span></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowClassTeacherUploadCSV" id="AllowClassTeacherUploadCSV" value=1 <?=($libreportcard->AllowClassTeacherUploadCSV==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherUploadCSV"><?=$eReportCard['AllowClassTeacherUploadCSV']?></label></span></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		</form>
	<?= $linterface->FOCUS_ON_LOAD("form1.StartDate") ?>

	<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
