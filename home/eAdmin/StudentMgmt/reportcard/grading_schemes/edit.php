<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

############################################################################################################

$SchemeID = (is_array($SchemeID)) ? $SchemeID[0] : $SchemeID;
$CurrentPage = "SchemeSettings_GradingSchemes";

list($MainInfo, $GradeMarkArr) = $lreportcard->GET_SCHEME_INFO($SchemeID);
list($SchemeTitle, $Description, $SchemeType, $FullMark, $Pass, $Fail, $Weighting) = $MainInfo;
if($SchemeType=="PF")
{
	$FormTable = "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Pass']." <span class='tabletextrequire'>*</span></td><td width='75%'><input class='textboxnum' type='text' name='Pass' maxlength='10' value='".$Pass."' /></td></tr>";
	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Fail']." <span class='tabletextrequire'>*</span></td><td width='75%'><input class='textboxnum' type='text' name='Fail' maxlength='10' value='".$Fail."' /></td></tr>";
}
else
{
	$FormTable = "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['FullMark']." <span class='tabletextrequire'>*</span></td><td width='75%'><input class='textboxnum' type='text' name='FullMark' maxlength='10' value='".$FullMark."' /></td></tr>";

	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Pass']."</td><td width='75%' valign='top'>
			<table id='tblPass'>
			<tr>
				<td class='tabletext'>".$eReportCard['LowerLimit']."</td>
				<td class='tabletext'>".$eReportCard['Grade']."</td>
				<td class='tabletext'>".$eReportCard['GradePoint']."</td>
			</tr>
			".($lreportcard->GENERATE_GRADING_FORM_CELL(1, 'Pass', $GradeMarkArr['P']))."
			</table>
			<span class='tabletext'><a href=\"javascript:jADD_ROW('tblPass', 'Pass');\" class='tablelink'>[".$eReportCard['AddMore']."]</a></span>
		</td></tr>";

	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Fail']."</td><td width='75%' valign='top'>
			<table id='tblFail'>
			<tr>
				<td class='tabletext'>".$eReportCard['LowerLimit']."</td>
				<td class='tabletext'>".$eReportCard['Grade']."</td>
				<td class='tabletext'>".$eReportCard['GradePoint']."</td>
			</tr>
			".($lreportcard->GENERATE_GRADING_FORM_CELL(1, 'Fail', $GradeMarkArr['F']))."
			</table>
			<span class='tabletext'><a href=\"javascript:jADD_ROW('tblFail', 'Fail');\" class='tablelink'>[".$eReportCard['AddMore']."]</a></span>
		</td></tr>";

	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Distinction']."</td><td width='75%' valign='top'>
			<table>
			<tr>
				<td class='tabletext'><label for='DistLowerLimit'>".$eReportCard['LowerLimit']."</label></td>
				<td class='tabletext'><label for='DistGrade'>".$eReportCard['Grade']."</td>
				<td class='tabletext'><label for='DistGradePoint'>".$eReportCard['GradePoint']."</td>
			</tr>
			<tr>
				<td class='tabletext'><input class='textboxnum' maxlength='10' type='text' name='DistLowerLimit' id='DistLowerLimit' value='".$GradeMarkArr['D'][0]."' /></td>
				<td class='tabletext'><input class='textboxnum' maxlength='10' type='text' name='DistGrade' id='DistGrade' value='".$GradeMarkArr['D'][1]."' /></td>
				<td class='tabletext'><input class='textboxnum' maxlength='10' type='text' name='DistGradePoint' id='DistGradePoint' value='".$GradeMarkArr['D'][2]."' /></td>
			</tr>
			</table>
		</td></tr>";
}
############################################################################################################

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['SchemeSettings_GradingSchemes'], "", 0);

$linterface->LAYOUT_START();

?>
<script language="javascript">
function checkform(obj){
	if(!check_text(obj.SchemeTitle, "<?php echo $i_alert_pleasefillin.$i_general_title; ?>.")) return false;
	if(obj.SchemeType.value=="H")
	{	
		if(!check_text(obj.FullMark, "<?php echo $i_alert_pleasefillin.$eReportCard['Schemes_FullMark']; ?>.")) return false;
	}
	else
	{
		if(!check_text(obj.Pass, "<?php echo $i_alert_pleasefillin.$eReportCard['Pass']; ?>.")) return false;
		if(!check_text(obj.Fail, "<?php echo $i_alert_pleasefillin.$eReportCard['Fail']; ?>.")) return false;
	}
	if(!check_text(obj.Weighting, "<?php echo $i_alert_pleasefillin.$eReportCard['WeightingGrandTotal']; ?>.")) return false;
   
     return true;
}

// reference
//http://www.mredkj.com/tutorials/tablebasics3.html 

function jADD_ROW(jTableID, jPrefix)
{
  var jTableBody = document.getElementById(jTableID).tBodies[0];
  var jNewRow = jTableBody.insertRow(-1);
  var jNewCell0 = jNewRow.insertCell(0);
  jNewCell0.innerHTML = "<input class='textboxnum' maxlength='10' type='text' name='"+jPrefix+"LowerLimit[]' id='PassLowerLimit[]' />";
  var jNewCell1 = jNewRow.insertCell(1);
  jNewCell1.innerHTML = "<input class='textboxnum' maxlength='10' type='text' name='"+jPrefix+"Grade[]' id='PassGrade[]' />";
  var jNewCell2 = jNewRow.insertCell(2);
  jNewCell2.innerHTML = "<input class='textboxnum' maxlength='10' type='text' name='"+jPrefix+"GradePoint[]' id='PassGradePoint[]' /></td>";
}
</script>


<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_title; ?> <span class="tabletextrequire">*</span></td><td width="75%"><input class="textboxtext" type="text" name="SchemeTitle" maxlength="255" value="<?=$SchemeTitle?>" /></td></tr>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['GradingType']; ?></td><td class="textboxnum tablerow2" width="75%"><?=($SchemeType=="H"?$eReportCard['HonorBased']:$eReportCard['PassFailBased'])?></td></tr>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_description; ?></td><td width="75%">
		<?= $linterface->GET_TEXTAREA("Description", $Description, 40) ?>
		</td></tr>
		<?=$FormTable?>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['WeightingGrandTotal']; ?> <span class="tabletextrequire">*</span></td><td width="75%"><input class="textboxnum" type="text" name="Weighting" value="<?=$Weighting?>" maxlength="10" /></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>


<p></p>
<input type="hidden" name="SchemeID" value="<?=$SchemeID?>" />
<input type="hidden" name="SchemeType" value="<?=$SchemeType?>" />
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.SchemeTitle") ?>

<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
