<?php
// Using: 
/*****************************************
 *  Modification log
 * 
 * 	20160421 Bill	[2016-0203-1700-19207]
 * 		- pass parms to display msg to remind client to re-save marksheet and re-generate report after grading scheme updated
 * 
 * ***************************************/
 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$SchemeTitle = intranet_htmlspecialchars($_POST['SchemeTitle']);
$Description = intranet_htmlspecialchars($_POST['Description']);

$FieldsValues = "SchemeTitle='$SchemeTitle'";
$FieldsValues .= ", Description='$Description'";
$FieldsValues .= ($SchemeType=="H") ? ", FullMark='$FullMark'" : ", Pass='$Pass', Fail='$Fail'";
$FieldsValues .= ", Weighting='$Weighting'";
$FieldsValues .= ", DateModified=now()";
$sql = "UPDATE RC_GRADING_SCHEME SET $FieldsValues WHERE SchemeID = '$SchemeID'";
$success = $li->db_db_query($sql);

# Remove old GradeMark reocrds
$sql = "DELETE FROM RC_GRADING_SCHEME_GRADEMARK WHERE SchemeID = '$SchemeID'";
$li->db_db_query($sql);
if(!empty($PassLowerLimit))
{
	for($i=0; $i<sizeof($PassLowerLimit); $i++)
	{
		$p_lower_limit = $PassLowerLimit[$i];
		if($p_lower_limit!="")
		{
			$p_grade = $PassGrade[$i];
			$p_grade_point = $PassGradePoint[$i];
			$sql = "INSERT INTO RC_GRADING_SCHEME_GRADEMARK (SchemeID, Nature, LowerLimit, Grade, GradePoint, DateInput, DateModified)
				   VALUES ('$SchemeID', 'P', '$p_lower_limit', '$p_grade', '$p_grade_point' , now() , now())";
			$li->db_db_query($sql);
		}
	}
}
if(!empty($FailLowerLimit))
{
	for($i=0; $i<sizeof($FailLowerLimit); $i++)
	{
		$f_lower_limit = $FailLowerLimit[$i];
		if($f_lower_limit!="")
		{
			$f_grade = $FailGrade[$i];
			$f_grade_point = $FailGradePoint[$i];
			$sql = "INSERT INTO RC_GRADING_SCHEME_GRADEMARK (SchemeID, Nature, LowerLimit, Grade, GradePoint, DateInput, DateModified)
				   VALUES ('$SchemeID', 'F', '$f_lower_limit', '$f_grade', '$f_grade_point' , now() , now())";
			$li->db_db_query($sql);
		}
	}
}
if($DistLowerLimit!="")
{
	$sql = "INSERT INTO RC_GRADING_SCHEME_GRADEMARK (SchemeID, Nature, LowerLimit, Grade, GradePoint, DateInput, DateModified)
               VALUES ('$SchemeID', 'D', '$DistLowerLimit', '$DistGrade', '$DistGradePoint' , now() , now())";
	$li->db_db_query($sql);
}

intranet_closedb();

// [2016-0203-1700-19207] set parms for system msg display after update
//$Result = ($success==1) ? "update" : "update_failed";
//header("Location: index.php?Result=$Result");
$Result = ($success==1) ? "" : "update_failed";
$Result2 = ($success==1) ? "NeedToReGenAfterUpdate" : "";
header("Location: index.php?Result=$Result&Result2=$Result2");
?>