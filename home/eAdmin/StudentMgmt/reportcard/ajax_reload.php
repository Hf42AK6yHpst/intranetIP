<?php
// using :
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
$lreportcard = new libreportcard();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Term_Selection")
{
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$SelectionID = $_REQUEST['SelectionID'];
	$NoFirst = $_REQUEST['NoFirst'];
	$WithWholeYear = $_REQUEST['WithWholeYear'];
	
	echo $lreportcard->Get_Term_Selection($SelectionID, $AcademicYearID, $YearTermID, $OnChange='', $NoFirst, $WithWholeYear);
}


intranet_closedb();
?>