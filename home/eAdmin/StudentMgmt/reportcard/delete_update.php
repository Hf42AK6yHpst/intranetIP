<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$cond = ($IsAnnual==0 && $Semester!="") ? " AND Semester = '".$Semester."'" : "";

# Delete record in RC_MARKSHEET_SCORE
$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE Year = '".$Year."' {$cond}";
$success = $li->db_db_query($sql);

# Delete record in RC_REPORT_RESULT
$sql = "DELETE FROM RC_REPORT_RESULT WHERE Year = '".$Year."' {$cond}";
$success = $li->db_db_query($sql);

# Delete record in RC_MARKSHEET_COMMENT
$sql = "DELETE FROM RC_MARKSHEET_COMMENT WHERE Year = '".$Year."' {$cond}";
$success = $li->db_db_query($sql);

# Delete record in RC_MARKSHEET_FEEDBACK
$sql = "DELETE FROM RC_MARKSHEET_FEEDBACK WHERE Year = '".$Year."' {$cond}";
$success = $li->db_db_query($sql);

# Delete record in RC_SUB_MARKSHEET_SCORE
$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE Year = '".$Year."' {$cond}";
$success = $li->db_db_query($sql);

intranet_closedb();

$Result = ($success==1) ? "delete" : "delete_failed";
header("Location:delete.php?Result=$Result");
?>

