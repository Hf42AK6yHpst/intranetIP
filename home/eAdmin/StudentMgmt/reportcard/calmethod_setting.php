<?php
$PATH_WRT_ROOT = "../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_CalMethod";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        
        # Retrieve file content
		//list($CalculationMethodRule, $CalculationOrder) = $lreportcard->GET_CALCULATION_METHOD();
		
		$CalculationMethodRule = $lreportcard->CalculationMethodRule;
		$CalculationOrder = $lreportcard->CalculationOrder;
		
        # tag information
		$TAGS_OBJ[] = array($eReportCard['SchemeSettings_CalMethod'], "", 0);
		
		$linterface->LAYOUT_START();
		
		?>
		
		<form name="form1" action="calmethod_setting_update.php" method="post">
		<br />
		
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>

		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['CalculationMethod']; ?> </td>
							<td width="75%" class='tabletext'>
								<input type="checkbox" name="CalculationMethodRule" value="1" id="CalculationMethodRule" <? if ($CalculationMethodRule == 1) print "checked" ?> />
								<label for="CalculationMethodRule"><?=$eReportCard['CalculationMethodRule']?></label>
							</td>
							<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['CalculationOrder']; ?> </td>
							<td width="75%">
								<table width="100%" cellspacing='0' cellpadding='0' border='0'>
									<tr>
										<td class="tabletext">
											<input type="radio" name="CalculationOrder" value="Horizontal" id="CalculationOrderHorizontal" <? if ($CalculationOrder == "Horizontal") print "checked" ?> />
											<label for="CalculationOrderHorizontal"><?=$eReportCard['CalculationOrderHorizontal']?></label>
										</td>
									</tr>
									<tr>
										<td class="tabletext">
											<input type="radio" name="CalculationOrder" value="Vertical" id="CalculationOrderVertical" <? if ($CalculationOrder == "Vertical") print "checked" ?> />
											<label for="CalculationOrderVertical"><?=$eReportCard['CalculationOrderVertical']?></label>
										</td>
									</tr>
								</table>
							</td>
							</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		</form>
	<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
