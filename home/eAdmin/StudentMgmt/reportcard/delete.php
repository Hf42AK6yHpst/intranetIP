<?php
$PATH_WRT_ROOT = "../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "DataDeletion";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight())
    {
		$linterface = new interface_html();
		
###############################################################################
		
		# Year Selection
		$YearSelection = $libreportcard->GET_ASSESSMENT_YEAR_SELECTION($Year);

		# Semester Selection
		$SemesterSelection = $libreportcard->GET_ASSESSMENT_Semester_SELECTION($Year, $Semester);
		
		$IsAnnual = (empty($IsAnnual) || $IsAnnual==="") ? 1 : 0;
		$Disabled = ($SemesterSelection=="<i>".$eReportCard['NoRecord']."</i>") ? "DISABLED": "";
###############################################################################

        # tag information
		$TAGS_OBJ[] = array($eReportCard['DataDeletion'], "", 0);
		
		$linterface->LAYOUT_START();
		?>
		<script language="javascript">
		function checkform(obj){
			if(confirm("<?=$eReportCard['DataRemovalMsg']?>")) {
					obj.action = "delete_update.php";
					return true;
			}
			return false;
		}
		</script>
		
		<form name="form1" action="" method="post"onSubmit="return checkform(this);">
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="tabletext" colspan=2 height='35'><?= $eReportCard['DataDeletionRemind']; ?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Profile_Year?></td>
							<td class="tabletext" width="75%"><?=$YearSelection?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Period']?></td>
							<td class="tabletext" width="75%">
							  <input type="radio" name="IsAnnual" id="annual_1" value=1 <?=($IsAnnual==1?"CHECKED":"")?> /><label for="annual_1"><?=$eReportCard['FullYear']?></label>
							  <br>
							  <input type="radio" name="IsAnnual" id="annual_0" value=0 <?=($IsAnnual==0?"CHECKED":"")?> <?=$Disabled?>><label for="annual_0"><?=$i_Profile_Semester?></label>: <?= $SemesterSelection ?>
							</td>
						</tr>
						<!--
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Class']?></td>
							<td class="tabletext" width="75%"><?= $class_selection_html ?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['NumberOfRecord']?></td>
							<td class="tabletext" width="75%"><?= $record_num?></td>
						</tr>	
						-->
					</table>
				</td>
			</tr>
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center"><?= $linterface->GET_ACTION_BTN($button_remove, "submit", "", "btnDelete")?>
				</td>
			</tr>
		</table>
		<input type="hidden" name="year_change" />
		</form>
	<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
