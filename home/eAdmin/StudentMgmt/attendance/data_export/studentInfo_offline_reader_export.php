<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "SELECT CardID, UserID, UserLogin 
        FROM INTRANET_USER 
					WHERE 
						RecordType = 2 AND RecordStatus = 1 
							AND CardID IS NOT NULL 
							AND UserID IS NOT NULL 
							AND UserLogin IS NOT NULL
						ORDER BY ClassName, ClassNumber, EnglishName 
		";

//$x = "Card ID,User ID,User Login\n";

$result = $li->returnArray($sql,3);
for ($i=0; $i<sizeof($result); $i++)
{
     list($cardid,$userid,$userlogin) = $result[$i];

	 $userlogin = substr($userlogin, 0, 8);

	 if( $cardid<>"" AND $userid<>"" AND $userlogin<>"" )
	     $x .= "$cardid,$userid,$userlogin\n";
}

// Output the file to user browser
$filename = "studentinfo.csv";
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
intranet_closedb();
?>
