<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();

$sql = "SELECT SUBSTRING(CardID,3) , UserID,UserLogin
        	FROM INTRANET_USER
            WHERE
                RecordType = 2 AND RecordStatus IN(0,1,2) 
                        AND CardID IS NOT NULL
                        AND UserID IS NOT NULL
                        AND UserLogin IS NOT NULL
                        AND CardID != ''
                ORDER BY ClassName, ClassNumber, EnglishName
                ";


$result = $li->returnArray($sql,3);

for ($i=0; $i<sizeof($result); $i++)
{
     $data[$i][0] = ($i+1);
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
	      $data[$i][$j+1] = $result[$i][$j];
     }
     $data[$i][sizeof($result[$i])+1] .= "\r\n";
}

$lexport = new libexporttext();

$export_content = $lexport->GET_EXPORT_TXT($data, "", ",", "\r\n", "\t", 4);


// Output the file to user browser
$filename = "userDetail.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>