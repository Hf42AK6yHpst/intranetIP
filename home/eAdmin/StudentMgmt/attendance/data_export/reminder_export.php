<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();
$namefield = getNameFieldForRecord("c.");

$sql = "SELECT b.CardID, a.DateOfReminder, $namefield, a.Reason
        FROM CARD_STUDENT_REMINDER as a
             LEFT OUTER JOIN INTRANET_USER as b
                  ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN(0,1,2)
             LEFT OUTER JOIN INTRANET_USER as c
                  ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
               a.DateOfReminder >= '$startdate' AND a.DateOfReminder <= '$enddate'
                  ";

$result = $li->returnArray($sql,5);

$lexport = new libexporttext();

$exportColumn = array("CardID", "Date", "Teacher", "Reason");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn, "###", "%%%", ",");

/*
$x = "CardID,Date,Teacher,Reason%%%";
for ($i=0; $i<sizeof($result); $i++)
{
     list($cardid,$remindDate,$teacher,$reason) = $result[$i];
     $x .= "$cardid###$remindDate###$teacher###$reason%%%";
}
*/

// Output the file to user browser
$filename = "reminder_pc.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>
