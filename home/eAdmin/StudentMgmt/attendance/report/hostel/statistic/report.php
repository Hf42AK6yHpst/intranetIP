<?php
// Editing by 
/*
 * 2017-08-10 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HostelAttendance']
	|| count($GroupID)==0 || $StartDate=='' || $EndDate=='' || $ReportType=='' || $StayType=='') {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if($lc->HostelAttendanceGroupCategory == ''){
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$in_status_ary = array(
	CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['HostelAttend'],
	CARD_STATUS_ABSENT=>$Lang['StudentAttendance']['HostelAbsent']
);

$out_status_ary = array(
	''=>'N.A.',
	(string)CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['HostelNormalLeave'],
	(string)PROFILE_TYPE_EARLY=>$Lang['StudentAttendance']['HostelEarlyLeave']
);

$report_type_ary = array('m'=>$Lang['StudentAttendance']['Monthly'],'w'=>$Lang['StudentAttendance']['Weekly'], 'd'=>$Lang['StudentAttendance']['Daily']);

$raw_start_ts = strtotime($StartDate);
$raw_end_ts = strtotime($EndDate);
$columns = array();
$year_month_ary = array();

if($ReportType == 'd') // daily
{
	for($cur_ts=$raw_start_ts;$cur_ts<=$raw_end_ts;$cur_ts+=86400)
	{
		$date = date("Y-m-d",$cur_ts);
		$columns[] = array($date,$date);
	}
}else if($ReportType == 'w') // weekly
{
	$firstday_weekday = intval(date("w",$raw_start_ts));
	$lastday_weekday = intval(date("w",$raw_end_ts));
	$startday_ts = $raw_start_ts - $firstday_weekday * 86399;
	$endday_ts = $raw_end_ts + $lastday_weekday * 86399;
	for($cur_ts=$startday_ts;$cur_ts<=$endday_ts && $cur_ts<=$raw_end_ts;$cur_ts += 86400*7)
	{
		$start_date = date("Y-m-d",$cur_ts);
		$end_date = date("Y-m-d",$cur_ts + 86399*7);
		$columns[] = array($start_date, $end_date);
	}
}

$start_date = date("Y-m-01",$raw_start_ts);
$start_ts = strtotime($start_date);
$days_last_month = intval(date("t",$raw_end_ts));
$end_ts = strtotime(date("Y-m-01",$raw_end_ts)) + $days_last_month * 86399;
for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,1,intval(date("m",$cur_ts))+1,date("d",$cur_ts),date("Y",$cur_ts)))
{
	$days_this_month = intval(date("t", $cur_ts));
	$begin = max($cur_ts,$raw_start_ts);
	$end = min($cur_ts+$days_this_month*86399,$raw_end_ts);
	$range = array(date("Y-m-d",$begin), date("Y-m-d,",$end));
	if($ReportType == 'm'){ // monthly
		$columns[] = $range;
	}
	$year_month_ary[] = $range;
}

//debug_pr($year_month_ary);
$dateStudentIdToRecords = array();
for($i=0;$i<count($year_month_ary);$i++){
	$start_date = $year_month_ary[$i][0];
	$end_date = $year_month_ary[$i][1];
	$ts = strtotime($start_date);
	$year = date("Y",$ts);
	$month = date("m",$ts);
	$params = array('StartDate'=>$start_date,'EndDate'=>$end_date,'GroupID'=>$GroupID,'StayType'=>$StayType);
	$tmp_records = $lc->getHostelAttendanceRecords($year,$month,$params);
	$tmp_record_size = count($tmp_records);
	for($j=0;$j<$tmp_record_size;$j++){
		$record_date = $tmp_records[$j]['RecordDate'];
		if(!isset($dateStudentIdToRecords[$record_date])){
			$dateStudentIdToRecords[$record_date] = array();
		}
		$dateStudentIdToRecords[$record_date][$tmp_records[$j]['UserID']] = $tmp_records[$j];
	}
}

$students = $lc->getHostelAttendanceRecords($year,$month,array('GroupID'=>$GroupID,'RecordDate'=>$year_month_ary[count($year_month_ary)-1][1]));

if(in_array($Format,array("","web")))
{
	$is_web = true;
	$present_icon = '/images/2009a/attendance/icon_present_s.png';
	$space = '&nbsp;';
	
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
}
if($Format == "print")
{
	$is_print = true;
	$present_icon = '/images/2009a/attendance/icon_present_bw.gif';
	$space = '&nbsp;';
	
	$x = '';
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>';
	$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
			 <tr>
				<td align="center"><h2>'.$Lang['StudentAttendance']['HostelStatistic'].'</h2></td>
			 </tr>
		   </table>';
	
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}
if($Format == "csv")
{
	$is_csv = true;
	$csv_rows = array();
	$space = ' ';
}

for($i=0;$i<count($columns);$i++){
	$start_date = $columns[$i][0];
	$end_date = $columns[$i][1];
	$start_ts = strtotime($start_date);
	$end_ts = strtotime($end_date);
	$num_of_day = ($end_ts - $start_ts + 86400)/86400.0;
	$col_width = sprintf("%.2f", 85.0 / $num_of_day);
	
	$total_count = 0;
	$student_to_count = array();
	$date_to_count = array();
	
	$table = '';
	$date_title = '';
	if($ReportType == 'm'){
		$date_title = Get_Lang_Selection(date("Y",$start_ts).$space.date('n',$start_ts).$Lang['General']['Month'] ,$Lang['General']['month'][date('n',$start_ts)].$space.date("Y",$start_ts));
	}else if($ReportType == 'w'){
		$date_title = date("Y-m-d",$start_ts).$space.$Lang['General']['To'].$space.date("Y-m-d",$end_ts);
	}else if($ReportType == 'd'){
		$date_title = date("Y-m-d",$start_ts);
	}
	$table.= '<h3>'.$date_title.'</h3>'."\n";
	$table.= '<table class="common_table_list_v30">'."\n";
	$table.= '<thead>'."\n";
	if($is_csv){
		$csv_row = array($date_title);
		$csv_row = array_merge($csv_row, array_fill(0,$num_of_day+2,' '));
		$csv_rows[] = $csv_row;
		
		$csv_row = array($Lang['StudentAttendance']['HostelGroup'],$Lang['Identity']['Student']);
	}
		$header_row = '<tr>';
			$header_row.= '<th width="6%">'.$Lang['StudentAttendance']['HostelGroup'].'</th>';
			$header_row.= '<th width="6%">'.$Lang['Identity']['Student'].'</th>';
		for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts+=86400){
			$day_title = ($ReportType=='d'?date("Y-m-d",$cur_ts) : ($ReportType == 'w'? date("j/n",$cur_ts) : date("j",$cur_ts)));
			$header_row.= '<th width="'.$col_width.'%" style="text-align:center">'.($day_title).'<br />('.$Lang['General']['DayType4'][date("w",$cur_ts)].')</th>';
			
			if($is_csv){
				$csv_row[] = $day_title.$space.'('.$Lang['General']['DayType4'][date("w",$cur_ts)].')';
			}
		}
			$header_row.='<th '.($is_web?'class="sub_row_top"':'').' width="3%" style="text-align:center">'.$Lang['General']['Total'].'</th>';
		$header_row.= '</tr>'."\n";
		
		if($is_csv){
			$csv_row[] = $Lang['General']['Total'];
			$csv_rows[] = $csv_row;
		}
		
		$table.=$header_row;
	$table.= '</thead>'."\n";
	$table.= '<tbody>'."\n";
		
		for($j=0;$j<count($students);$j++){
			$student_to_count[$students[$j]['UserID']] = 0;
			$row = '<tr>';
			$row.='<td>'.$students[$j]['GroupName'].'</td>';
			$row.='<td>'.$students[$j]['StudentName'].'</td>';
			if($is_csv){
				$csv_row = array($students[$j]['GroupName'],$students[$j]['StudentName']);
			}
			for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts+=86400){
				$cur_date = date("Y-m-d",$cur_ts);
				if(!isset($date_to_count[$cur_date])){
					$date_to_count[$cur_date] = 0;
				}
				$cell = '&nbsp;';
				$csv_cell = ' ';
				if(isset($dateStudentIdToRecords[$cur_date]) && isset($dateStudentIdToRecords[$cur_date][$students[$j]['UserID']])){
					$cell = '<img src="'.$present_icon.'" align="absmiddle" width="24" height="20">';
					$total_count += 1;
					$student_to_count[$students[$j]['UserID']] += 1;
					$date_to_count[$cur_date] += 1;
					$csv_cell = '1';
				}
				$row.='<td align="center">'.$cell.'</td>';
				if($is_csv) $csv_row[] = $csv_cell;
			}
			$row.='<td align="center">'.($student_to_count[$students[$j]['UserID']]).'</td>';
			$row.='</tr>';
			$table.=$row;
			if($is_csv){
				$csv_row[] = $student_to_count[$students[$j]['UserID']];
				$csv_rows[] = $csv_row;
			}
		}
		$row = '<tr class="total_row"><th colspan="2">'.$Lang['General']['Total'].'</th>';
		if($is_csv) $csv_row=array('',$Lang['General']['Total']);
		for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts+=86400){
			$cur_date = date("Y-m-d",$cur_ts);
			$cell = '&nbsp;';
			$csv_cell = ' ';
			if(isset($date_to_count[$cur_date])){
				$cell = $date_to_count[$cur_date];
				$csv_cell = $date_to_count[$cur_date];
			}
			$row.='<td align="center">'.$cell.'</td>';
			if($is_csv) $csv_row[] = $csv_cell;
		}
		if($is_csv){
			$csv_row[] = $total_count;
			$csv_rows[] = $csv_row;
			$csv_rows[] = array_fill(0,$num_of_day+3,' ');
		}
		$row.= '<td align="center">'.$total_count.'</td>';
		$row.= '</tr>';
		$table.=$row;
	$table.= '</tbody>'."\n";
	$table.= '</table><br />'."\n";
	
	$x.= $table;
}

if(in_array($Format,array("","web","print")))
{
	echo $x;
}

if($Format == "print")
{
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

if($Format == "csv")
{
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$headers = array_shift($csv_rows);	
	
	$csv_filename = $Lang['StudentAttendance']['HostelStatistic'].'('.$report_type_ary[$ReportType].')'.".csv";
	$exportContent = $lexport->GET_EXPORT_TXT($csv_rows, $headers);
	$lexport->EXPORT_FILE($csv_filename, $exportContent);
}

intranet_closedb();
?>