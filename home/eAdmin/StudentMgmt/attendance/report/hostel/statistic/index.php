<?php
// Editing by 
/*
 * 2017-08-10 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HostelAttendance']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

//if($lc->HostelAttendanceGroupCategory == ''){
//	intranet_closedb();
//	header ("Location: ../../");
//	exit();
//}

$academic_year_startdate = date("Y-m-d", getStartOfAcademicYear());
$today = date("Y-m-d");
if(!isset($StartDate)) $StartDate = $academic_year_startdate;
if(!isset($EndDate)) $EndDate = $today;

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_HostelAttendanceStatistic";

$linterface = new interface_html();
$libgrouping = new libgrouping();

$groups = $libgrouping->returnCategoryGroups($lc->HostelAttendanceGroupCategory);
$group_selection = $linterface->GET_SELECTION_BOX($groups, ' id="GroupID" name="GroupID[]" multiple="multiple" size="8" style="min-width:10em;" ', '', '', false);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HostelStatistic'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table_v30" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['StudentAttendance']['ReportType']?><span class="tabletextrequire">*</span></td>
	    <td width="70%" class="tabletext">
			<?=$linterface->Get_Radio_Button("ReportType3", "ReportType", "m", 1, "report_type", $Lang['StudentAttendance']['Monthly'], "",0)?>
			<?=$linterface->Get_Radio_Button("ReportType2", "ReportType", "w", 0, "report_type", $Lang['StudentAttendance']['Weekly'], "",0)?>
			<?=$linterface->Get_Radio_Button("ReportType1", "ReportType", "d", 0, "report_type", $Lang['StudentAttendance']['Daily'], "",0)?>
	    </td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StartDate"><?=$Lang['General']['Date']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $StartDate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $EndDate)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Date_Error WarnMsg')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="GroupID"><?=$Lang['StudentAttendance']['HostelGroup']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<?=$group_selection.$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('GroupID', true);return false;")?>
			<?=$linterface->Get_Form_Warning_Msg('GroupID_Error', $Lang['General']['PleaseSelect'].$word_space.$Lang['StudentAttendance']['HostelGroup'], 'GroupID_Error WarnMsg', false);?>
			<div class="tabletextremark" ><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		</td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['StudentAttendance']['CountFor']?><span class="tabletextrequire">*</span></td>
	    <td width="70%" class="tabletext">
			<?=$linterface->Get_Radio_Button("StayType1", "StayType", "1", 1, "stay_type", $Lang['StudentAttendance']['StayOvernight'], "",0)?>
			<?=$linterface->Get_Radio_Button("StayType0", "StayType", "0", 0, "stay_type", $Lang['StudentAttendance']['DoNotStayOvernight'], "",0)?>
	    </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "submitForm('');","submitBtn") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="Format" id="Format" value="" />
</form>
<br />
<div id="ReportLayer">
</div>
<br />
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';

function submitForm(format)
{
	$('#submitBtn').attr('disabled',true);
	$('.WarnMsg').hide();
	var is_valid = true;
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');
	var target_obj = document.getElementById('GroupID');
	var target_values = [];

	if(!check_date_without_return_msg(startdateObj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		is_valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		is_valid = false;
		$('#Date_Error').show();
	}

	if(target_obj)
	{
		for(var i=0;i<target_obj.options.length;i++)
		{
			if(target_obj.options[i].selected) target_values.push(target_obj.options[i].value);
		}
	}

	if(target_values.length <= 0)
	{
	 	is_valid = false;
		$('#GroupID_Error').show();
	}
	
	if(!is_valid) $('#submitBtn').attr('disabled',false);
	
	if(is_valid)
	{
		$('#Format').val(format);
		if(format == '' || format == 'web'){
			$('#ReportLayer').html(loadingImg);
			$.post(
				'report.php',
				$('#form1').serialize(),
				function(returnHtml)
				{
					$('#ReportLayer').html(returnHtml);
					$('#submitBtn').attr('disabled',false);
				}
			);
		}else if(format == 'print' || format == 'pdf')
		{
			var url = '';
			var winType = '10';
			var win_name = 'intranet_popup'+winType;
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = win_name;
			
			newWindow(url, winType);
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}else if(format == 'csv'){
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = '_blank';
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}
	}
}

$(document).ready(function(){
	Select_All_Options('GroupID',true);
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>