<?
//using by 
/*
 * 2019-10-31 (Ray)   : Added date range select
 * 2017-02-02 (Carlos): Added display options [Login ID], [Reason], [Teacher's remarks], [Office Remark].
 * 2013-11-06 (Carlos): Modified to allow select classs or group
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_ClassDaily";

$linterface = new interface_html();

if(!isset($ShowAllColumns) || $ShowAllColumns=="") $ShowAllColumns = "0"; 

# Get Classes List
$classes = $lc->getClassList();
$select_class = '<select name="ClassID[]" id="ClassID" class="inputfield" size="5" multiple>';
for ($i=0; $i<sizeof($classes); $i++)
{
	if($i==0) $select_class .= "<option value=\"".$classes[$i][0]."\" selected>".intranet_htmlspecialchars($classes[$i][1])."</option>\n";
	else
		$select_class .= "<option value=\"".$classes[$i][0]."\">".intranet_htmlspecialchars($classes[$i][1])."</option>\n";
}
$select_class.= '</select>';

$groups = $lc->getGroupListMode(3);
$select_group = '<select name="GroupID[]" id="GroupID" size="5" multiple>';
for($i=0;$i<sizeof($groups);$i++){
	
	$select_group .= '<option value="'.$groups[$i][0].'" '.($i==0?'selected':'').'>'.$groups[$i][1].'</option>';
}
$select_group.= '</select>';

#$select_class = getSelectByArray($classes,"name=ClassID","",0,1);

$TAGS_OBJ[] = array($i_StudentAttendance_Report_ClassDaily, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script language="JavaScript" type="text/javascript">
function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}
  
function submitForm()
{
  var warningMsg="";
  var targetDate = document.getElementById("TargetDate");
  var dateMatch = targetDate.value.match(/\d\d\d\d\-\d\d\-\d\d/);
  var endDate = document.getElementById("EndDate");
  var enddateMatch = endDate.value.match(/\d\d\d\d\-\d\d\-\d\d/);
  var targetType = $('input[name="TargetType"]:checked').val();
  var countSelectedClass = 0;
  var countSelectedGroup = 0;
  var classids=document.getElementById("ClassID");
  var groupids=document.getElementById("GroupID");
  for(var i=0;i<classids.length;i++)
  {
  	if(classids.options[i].selected)
  	{
  		countSelectedClass += 1;
  		break;
  	}
  }
  for(var i=0;i<groupids.length;i++)
  {
  	if(groupids.options[i].selected)
  	{
  		countSelectedGroup += 1;
  		break;
  	}
  }
  
  if(targetType=='CLASS' && countSelectedClass == 0)
  {
  	  warningMsg += '<?=$i_Discipline_System_alert_PleaseSelectClass?>'+'\n';
  }
  if(targetType=='GROUP' && countSelectedGroup == 0)
  {
  	 warningMsg += '<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>\n';
  }
  
  if(dateMatch == null || enddateMatch == null)
  {
  	  warningMsg += '<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>';
  }
  
  if(warningMsg != "")
  {
  	  alert(warningMsg);
  }else
  {
  	  document.getElementById("form1").submit();
  }
}

function targetTypeChanged(type)
{
	if(type == 'CLASS'){
		$('tr#ClassTr').show();
		$('tr#GroupTr').hide();
	}else{
		$('tr#ClassTr').hide();
		$('tr#GroupTr').show();
	}
}
</script>
<br />
<form name="form1" id="form1" action="class_day_report.php" target="_blank" method="POST">
<?php
if (isset($ClassID) && isset($TargetDate)) {
	$lclass = new libclass();
	$ClassName = $lclass->getClassName($ClassID);
	
	$i_title = "$ClassName ($TargetDate)";
	$PAGE_NAVIGATION[] = array($i_title);
?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<?php } ?>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['CommonChoose']['SelectTarget']?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->Get_Radio_Button("TargetTypeClass", "TargetType", "CLASS", 1, "", $Lang['SysMgr']['FormClassMapping']['Class'], "targetTypeChanged(this.value)",0)?>
			<?=$linterface->Get_Radio_Button("TargetTypeGroup", "TargetType", "GROUP", 0, "", $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Group'], "targetTypeChanged(this.value)",0)?>
		</td>
	</tr>
	<tr id="GroupTr" style="display:none;">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_GroupName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_group?>&nbsp;
			<span><?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(document.form1.elements['GroupID[]']);return false;")?></span>
		</td>
	</tr>
	<tr id="ClassTr">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_ClassName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>&nbsp;
			<span><?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(document.form1.elements['ClassID[]']);return false;")?></span>
		</td>
	</tr>
	<tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
        <td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("TargetDate",date('Y-m-d'))?>
        </td>
    </tr>
    <tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
        <td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate",date('Y-m-d'))?>
        </td>
    </tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<input type="radio" name="format" id="web" value="web" checked><label for="web">Web</label>
			&nbsp;&nbsp; 
			<input type="radio" name="format" id="csv" value="csv"><label for="csv">CSV</label>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SmartCard']['StaffAttendence']['ShowAllColumns']?></td>
		<td width="70%"class="tabletext">
			<input type="radio" id="ShowAllColumnsYes" name="ShowAllColumns" value="1" onclick="document.getElementById('OptionsLayer').style.display='none';this.checked=true;" <?if($ShowAllColumns=="1"){echo "checked";}else{echo "";}?>><label for="ShowAllColumnsYes"><?=$i_general_yes?></label>
			&nbsp;&nbsp;
			<input type="radio" id="ShowAllColumnsNo" name="ShowAllColumns" value="0" onclick="document.getElementById('OptionsLayer').style.display='block';this.checked=true;" <?if($ShowAllColumns=="0"){echo "checked";}else{echo "";}?>><label for="ShowAllColumnsNo"><?=$i_general_no?></label>
		</td>
	</tr>
	<?
	if($ShowAllColumns == "0")
		$OptionsLayerVisibility = "style='display:block;'";
	else
		$OptionsLayerVisibility = "style='display:none;'";
	?>
	<tr>
		<td width="30%">&nbsp;</td>
		<td width="70%"class="tabletext">
		<span id="OptionsLayer" <?=$OptionsLayerVisibility?>>
			<input type="checkbox" id="ColumnLoginID" name="ColumnLoginID" value="1" /><label for="ColumnLoginID"><?=$i_UserLogin?></label><br />
			<input type="checkbox" id="ColumnClassNo" name="ColumnClassNo" value="1" checked="checked" /><label for="ColumnClassNo"><?=$i_UserClassNumber?></label><br />
			<input type="checkbox" id="ColumnStudentName" name="ColumnStudentName" value="1" checked="checked" /><label for="ColumnStudentName"><?=$i_UserStudentName?></label><br />
			<?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
			<input type="checkbox" id="ColumnSession" name="ColumnSession" value="1" /><label for="ColumnSession"><?=$Lang['StudentAttendance']['SessionsStat']?></label><br />
			<?}?>
			<input type="checkbox" id="ColumnReason" name="ColumnReason" value="1" /><label for="ColumnReason"><?=$i_Attendance_Reason?></label><br />
			<input type="checkbox" id="ColumnRemark" name="ColumnRemark" value="1" /><label for="ColumnRemark"><?=$Lang['StudentAttendance']['iSmartCardRemark']?></label><br />
			<input type="checkbox" id="ColumnOfficeRemark" name="ColumnOfficeRemark" value="1" /><label for="ColumnOfficeRemark"><?=$Lang['StudentAttendance']['OfficeRemark']?></label><br />
		</span>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm()") ?>
		</td>
	</tr>
</table>
</form>
<?php
if (isset($ClassID) && isset($TargetDate)) {
	
	$lc->retrieveSettings();

	$ts = strtotime($TargetDate);
	if ($ts==-1 || $TargetDate =="")
	{
	    $TargetDate = date('Y-m-d');
	    $year = date('Y');
	    $month = date('m');
	    $day = date('d');
	}
	else
	{
	    $year = date('Y',$ts);
	    $month = date('m',$ts);
	    $day = date('d',$ts);
	}
	
	$need_to_take_attendance = $lc->isRequiredToTakeAttendanceByDate($ClassName,$TargetDate);
		
	if($need_to_take_attendance) {
		$result = $lc->retrieveClassDayData($ClassName, $year, $month, $day);
		
		
		$space = $intranet_session_language=="en"?" ":"";
		
		$display = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$display .= "<tr class=\"tablebluetop\">";
		$display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
		$display .= "<td class=\"tabletoplink\">$i_ClassNameNumber</td>";
		$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_InSchoolTime</td>";
		$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>";
		
		if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode==3)
		{
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_AM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
		}
		else
		{
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
		}
		
		if ($lc->attendance_mode==2)      # With Lunch Out
		{
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_LunchOutTime</td>";
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>\n";
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_LunchBackTime</td>";
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>\n";
		}
		if ($lc->attendance_mode == 2 || $lc->attendance_mode==3)
		{
		    $display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
		}
		$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_LeaveTime</td>";
		$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>\n";
		$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Type_LeaveSchool$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
		$display .= "</tr>\n";
		
		for ($i=0; $i<sizeof($result); $i++)
		{
		     list($studentid, $student_name, $student_class, $classnum, $inTime, $inStation,
		          $am, $lunchOutTime, $lunchOutStation, $lunchBackTime, $lunchBackStation,
		          $pm, $leaveSchoolTime, $leaveSchoolStation, $leave) = $result[$i];
		          
	     switch($am){
		     case "0" : $am = $i_StudentAttendance_Status_OnTime; break;
		     case "1" : $am = $i_StudentAttendance_Status_Absent; break;
		     case "2" : $am = $i_StudentAttendance_Status_Late; break;
		     case "3" : $am = $i_StudentAttendance_Status_Outing; break;
		     default : $am = $i_StudentAttendance_Status_Absent;
		 }     
		 
		 switch($pm){
			 case "0" : $pm = $i_StudentAttendance_Status_OnTime; break;
			 case "1" : $pm = $i_StudentAttendance_Status_Absent; break;
			 case "2" : $pm = $i_StudentAttendance_Status_Late; break;
			 case "3" : $pm = $i_StudentAttendance_Status_Outing; break;
			 default : $pm = $i_StudentAttendance_Status_Absent;
		 }
		 switch($leave){
			 case "0" : $leave = $i_StudentAttendance_Status_Present; break;
			 case "1": $leave = $i_StudentAttendance_Status_EarlyLeave; break;
			 case "2": $leave = $i_StudentAttendance_Status_EarlyLeave; break;
		 }		          
		          
		     $css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
		     $display .= "<tr class=\"$css\">";
		     $display .= "<td class=\"tabletext\">$student_name</td>";
		     $display .= "<td class=\"tabletext\">$classnum</td>\n";
		
		     $display .= "<td class=\"tabletext\">$inTime</td>";
		     $display .= "<td class=\"tabletext\">$inStation</td>\n";
		     if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode==3)
		     {
		         $display .= "<td class=\"tabletext\">$am</td>\n";
		     }
		     else
		     {
		         $display .= "<td class=\"tabletext\">$pm</td>\n";
		     }
		
		     if ($lc->attendance_mode==2)      # With Lunch Out
		     {
		         $display .= "<td class=\"tabletext\">$lunchOutTime</td>";
		         $display .= "<td class=\"tabletext\">$lunchOutStation</td>\n";
		         $display .= "<td class=\"tabletext\">$lunchBackTime</td>";
		         $display .= "<td class=\"tabletext\">$lunchBackStation</td>\n";
		     }
		     if ($lc->attendance_mode == 2 || $lc->attendance_mode==3)
		     {
		         $display .= "<td class=\"tabletext\">$pm</td>\n";
		     }
		     $display .= "<td class=\"tabletext\">$leaveSchoolTime</td>";
		     $display .= "<td class=\"tabletext\">$leaveSchoolStation</td>\n";
		     $display .= "<td class=\"tabletext\">$leave</td>\n";
		     $display .= "</tr>\n";
		}
		$display .= "</table>\n";
	}else{
		$display = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$display.= "<tr class=\"tablebluerow2\">";
		$display.= "<td class=\"tabletext\" align=\"center\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>";
		$display.= "</tr>";
		$display.= "</table>\n";
	}
	
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);

?>
<script language="JavaScript" type="text/javascript">
function openPrintPage()
{
    newWindow("class_day_print.php?ClassID=<?=$ClassID?>&TargetDate=<?=$TargetDate?>",10);
}
</script>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $toolbar ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?=$display?>
<?php } ?>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>