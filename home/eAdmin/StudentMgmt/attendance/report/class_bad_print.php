<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if (!isset($BadType) || !isset($StartDate) || !isset($EndDate)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

switch ($BadType)
{
    case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
    case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
    case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
    case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
    case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
}

$i_title = "<div class=\"eSportprinttext\">$i_StudentAttendance_Report_ClassBadRecords<br />$nav_title ($StartDate to $EndDate)</div>";

$lc->retrieveSettings();

$ts = strtotime($StartDate);
if ($ts==-1 || $StartDate =="")
{
    $StartDate = date('Y-m-d');
}
$ts = strtotime($EndDate);
if ($ts==-1 || $EndDate =="")
{
    $EndDate = date('Y-m-d');
}

$result = $lc->retrieveBadRecordsCountByClass($StartDate, $EndDate, $BadType);

$display = "<table class=\"eSporttableborder\" width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$display .= "<tr>";
$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_SmartCard_ClassName</td>\n";
$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_ReportHeader_NumStudents</td>\n";
$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
$display .= "</tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list ($class_name, $count_student, $count_record) = $result[$i];
     $display .= "<tr>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$class_name</td>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$count_student</td>\n";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$count_record</td></tr>\n";
}
$display .= "</table>\n";

?>
<table width='100%' align='center' class='print_hide' border=0>
<tr>
	<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td class="eSportprinttext"><?=$i_title?></td>
</tr>
</table>
<?=$display?>
<br />
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>