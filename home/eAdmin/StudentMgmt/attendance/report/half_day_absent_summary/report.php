<?php
// Editing by 
/*
 * 2016-06-08 (Carlos): Created.
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$GroupBy = IntegerSafe($_POST['GroupBy']);
$WithReason = IntegerSafe($_POST['WithReason']);
$Format = $_POST['Format']; // ""/"web", "csv", "pdf", "print"

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"] || !$sys_custom['LessonAttendance_LaSalleCollege'] 
	 || !in_array($GroupBy,array(1,2)) || !in_array($Format,array("","web","csv","pdf","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$Settings = $LessonAttendUI->Get_General_Settings('LessonAttendance', $LessonAttendUI->GetLessonAttendanceSettingsList());
$am_session_start = $Settings['StartSessionToCountAsAM'];
$am_session_end = $Settings['EndSessionToCountAsAM'];
$pm_session_start = $Settings['StartSessionToCountAsPM'];
$pm_session_end = $Settings['EndSessionToCountAsPM'];

function isAMPresent($oneDayRecords, $presentStatusAry)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	// 1-6 session are present, then count as AM present
	$am_lesson_index = $am_session_end; 
	$record_size = min($am_lesson_index, count($oneDayRecords));
	$is_am_present = true;
	for($i=0;$i<$record_size;$i++)
	{
		if(!in_array($oneDayRecords[$i]['AttendStatus'],$presentStatusAry)){
			$is_am_present = false;
		}
	}
	return $is_am_present;
}

function isPMPresent($oneDayRecords, $presentStatusAry)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	// if 7-[last lesson] are all present then treated as PM present
	$pm_lesson_index = $pm_session_start;
	$record_size = count($oneDayRecords);
	if($record_size < $pm_lesson_index) return false;
	$is_pm_present = true;
	for($i=$pm_lesson_index-1;$i<$record_size;$i++)
	{
		if(!in_array($oneDayRecords[$i]['AttendStatus'],$presentStatusAry)){
			$is_pm_present = false;
		}
	}
	return $is_pm_present;
}

function countNumberOfPresentPeriod($oneDayRecords, $presentStatusAry)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	$numberPresentPeriod = 0;
	for($i=0;$i<count($oneDayRecords);$i++)
	{
		if(in_array($oneDayRecords[$i]['AttendStatus'],$presentStatusAry)){
			$numberPresentPeriod += 1;
		}
	}
	return $numberPresentPeriod;
}

$groupByMap = array(1=>$Lang['StudentAttendance']['Class'],2=>$Lang['LessonAttendance']['Subject']);
$groupBySubTitle = array(1=>$Lang['LessonAttendance']['ByClass'],2=>$Lang['LessonAttendance']['BySubject']);



$academic_year_id = $LessonAttendUI->CurAcademicYearID;
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();

$presentStatusAry = array($statusMap['Present']['code'], $statusMap['Late']['code']);
if(isset($statusMap['LateForSplitClass'])){
	$presentStatusAry[] = $statusMap['LateForSplitClass']['code'];
}

$report_title = $Lang['LessonAttendance']['HalfDayAbsentSummaryReport'];

$student_name_field = getNameFieldByLang2("u1.");
//$teacher_name_field = getNameFieldByLang("u2.");
$subject_name = Get_Lang_Selection("s.CH_DES","s.EN_DES");

$cond = "";
if($WithReason == '1')
{
	$cond .= " AND sgsa.Reason IS NOT NULL AND TRIM(sgsa.Reason)<>'' ";
}else if($WithReason == '-1')
{
	$cond .= " AND (sgsa.Reason IS NULL OR TRIM(sgsa.Reason)='') ";
}

$sql = "SELECT 
			sgao.LessonDate,
			itt.TimeSlotName,
			sgao.StartTime,
			sgsa.StudentID,
			sgsa.AttendStatus,
			sgsa.Reason,
			st.SubjectID,
			$subject_name as Subject,
			u1.ClassName,
			u1.ClassNumber,
			$student_name_field as StudentName 
		FROM 
		SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
		INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
		INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
		INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao.SubjectGroupID=stc.SubjectGroupID 
		INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
		INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
		LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao.RoomAllocationID 
		LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
		WHERE (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate') $cond ";
$sql .= " GROUP BY sgao.AttendOverviewID,sgao.LessonDate,sgsa.StudentID ";
$sql.= " ORDER BY ";

if($GroupBy == 1) // group by class
{
	$key_name = 'class_name';
	$sql .= "u1.ClassName,sgao.LessonDate,sgao.StartTime ";
}

if($GroupBy == 2) // group by subject
{
	$key_name = 'subject';
	$sql.= "Subject,sgao.LessonDate,sgao.StartTime ";
}

$records = $LessonAttendUI->returnResultSet($sql);
$record_size = count($records);
$keyToRecords = array();
$keyToStats = array();
$total_school_days = 0;

for($i=0;$i<$record_size;$i++)
{
	$teacher_name = $records[$i]['TeacherName'];
	$subject = $records[$i]['Subject'];
	$class_name = $records[$i]['ClassName'];
	$lesson_date = $records[$i]['LessonDate'];
	$lesson_session = $records[$i]['LessonSession'];
	$student_name = $records[$i]['StudentName'].(' ('.$records[$i]['ClassName'].'#'.$records[$i]['ClassNumber'].')');
	$period_time = $records[$i]['StartTime'];
	$time_slot_name = $records[$i]['TimeSlotName'];
	$key = $$key_name;
	if(!isset($keyToRecords[$key])){
		$keyToRecords[$key] = array();
	}
	if(!isset($keyToRecords[$key][$student_name]))
	{
		$keyToRecords[$key][$student_name] = array();
	}
	if(!isset($keyToRecords[$key][$student_name][$lesson_date]))
	{
		$keyToRecords[$key][$student_name][$lesson_date] = array();
	}
	$keyToRecords[$key][$student_name][$lesson_date][] = $records[$i];
	
	if(!isset($keyToStats[$key])){
		$keyToStats[$key] = array();
		$keyToStats[$key]['NumberOfStudent'] = 0;
		$keyToStats[$key]['NumberOfFullDay'] = 0;
		$keyToStats[$key]['NumberOfHalfDay'] = 0;
		$keyToStats[$key]['PresentFullDay'] = 0;
		$keyToStats[$key]['PresentHalfDay'] = 0;
		
		$keyToStats[$key]['NumberOfPeriod'] = 0;
		$keyToStats[$key]['Periods'] = array();
		$keyToStats[$key]['PresentPeriod'] = 0;
	}
	$keyToStats[$key]['Periods'][$lesson_date.$time_slot_name] = $time_slot_name;
}

if(count($keyToRecords)>0)
{
	foreach($keyToRecords as $key1 => $ary1)
	{
		$keyToStats[$key1]['NumberOfStudent'] = count($ary1);
		foreach($ary1 as $student_name => $ary2)
		{
			$keyToStats[$key1]['NumberOfFullDay'] = count($ary2);
			$keyToStats[$key1]['NumberOfHalfDay'] = $keyToStats[$key1]['NumberOfFullDay'] * 2;
			$keyToStats[$key1]['NumberOfPeriod'] += count($ary2);
			foreach($ary2 as $date => $ary3)
			{
				$is_am_present = isAMPresent($ary3, $presentStatusAry);
				$is_pm_present = isPMPresent($ary3, $presentStatusAry);
				$keyToStats[$key1]['PresentPeriod'] += countNumberOfPresentPeriod($ary3, $presentStatusAry);
				$is_fullday_present = $is_am_present && $is_pm_present;
				
				if($is_fullday_present){
					$keyToStats[$key1]['PresentFullDay'] += 1;
				}
				if($is_am_present){
					$keyToStats[$key1]['PresentHalfDay'] += 1;
				}
				if($is_pm_present){
					$keyToStats[$key1]['PresentHalfDay'] += 1;
				}
			}
		}
		$total_school_days = max($total_school_days, $keyToStats[$key1]['NumberOfFullDay']);
	}
}

//debug_pr($keyToStats);

if(in_array($Format,array("","web","print","pdf")))
{
	if(in_array($Format,array("","web")))
	{
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
				//$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	}
	
	if($Format == "print")
	{
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>';
	}
	
	$x .= '<table width="99%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr style="background:#f3f3f3;">' . "\n";
			$x .= '<td>'."\n";
				$x .= $Lang['LessonAttendance']['Period'].': <span style="font-weight:bold;">'.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</span>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x .= '<tr>';
			$x .= '<td style="padding-top:16px;" valign="top">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['NumberOfStudentDays'].'</div>';
				$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="left" >' . "\n";
					$x .= '<tr><td width="15%">'.$Lang['LessonAttendance']['FullDay'].'</td><td width="85%">'.$total_school_days.'</td></tr>';
					$x .= '<tr><td width="15%">'.$Lang['LessonAttendance']['HalfDay'].'</td><td width="85%">'.($total_school_days*2).'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr style="padding-top:16px;">';
			$x .= '<td colspan="4" style="padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['PresentRate'].' - '.$groupBySubTitle[$GroupBy].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>'.$groupByMap[$GroupBy].'</th>';
					if($GroupBy == 1) // group by class
					{
						  $x .='<th style="text-align:center;">'.$Lang['LessonAttendance']['NumberOfStudent'].'</th>
								<th style="text-align:center;">'.$Lang['Identity']['Student'].' x '.$Lang['LessonAttendance']['FullDay'].'</th>
								<th style="text-align:center;">'.$Lang['Identity']['Student'].' x '.$Lang['LessonAttendance']['HalfDay'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['Present'].' - '.$Lang['LessonAttendance']['FullDay'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['Present'].' - '.$Lang['LessonAttendance']['HalfDay'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['FullDay'].' %</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['HalfDay'].' %</th>';
					}else{ // group by subject
						$x .='<th style="text-align:center;">'.$Lang['LessonAttendance']['NumberOfStudent'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['NumberOfPeriod'].'</th>
								<th style="text-align:center;">'.$Lang['Identity']['Student'].' x '.$Lang['LessonAttendance']['Period'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['Present'].'</th>
								<th style="text-align:center;">%</th>';
					}
					$x .= '</tr>';
					if(count($keyToStats)>0)
					{
						$i = 0;
						foreach($keyToStats as $key => $ary)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class="'.$row_css.'">';
							if($GroupBy == 1){ // group by class
								$x .= '<td>'.$key.'</td><td style="text-align:center;">'.$ary['NumberOfStudent'].'</td>
										<td style="text-align:center;">'.($ary['NumberOfStudent'] * $ary['NumberOfFullDay']).'</td>
										<td style="text-align:center;">'.($ary['NumberOfStudent'] * $ary['NumberOfHalfDay']).'</td>
										<td style="text-align:center;">'.($ary['PresentFullDay']).'</td>
										<td style="text-align:center;">'.($ary['PresentHalfDay']).'</td>
										<td style="text-align:center;">'.(sprintf("%.0f%%", $ary['PresentFullDay'] / ($ary['NumberOfStudent'] * $ary['NumberOfFullDay']) * 100)).'</td>
										<td style="text-align:center;">'.(sprintf("%.0f%%", $ary['PresentHalfDay'] / ($ary['NumberOfStudent'] * $ary['NumberOfHalfDay']) * 100)).'</td>';
							}else{
								$num_period = count($ary['Periods']);
								$x .= '<td>'.$key.'</td><td style="text-align:center;">'.$ary['NumberOfStudent'].'</td>
										<td style="text-align:center;">'.($num_period).'</td>
										<td style="text-align:center;">'.($num_period * $ary['NumberOfStudent']).'</td>
										<td style="text-align:center;">'.($ary['PresentPeriod']).'</td>
										<td style="text-align:center;">'.(sprintf("%.0f%%", $ary['PresentPeriod'] / ($ary['NumberOfStudent'] * $num_period) * 100)).'</td>';
							}
							$x .= '</tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
	$x .= '</table>'."\n";
	
	if($Format == 'print')
	{
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		echo $x;
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}
	
	if(in_array($Format,array("","web")))
	{
		echo $x;
	}
	
	if($Format == 'pdf')
	{
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		
		$css_file_path = $intranet_root.'/templates/2009a/css/content_30.css';
		$css_content = "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>\n";
		$css_file_path = $intranet_root.'/templates/2009a/css/print_25.css';
		$css_content .= "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>\n";
		
		$x = $css_content.$x;
		
		$margin= 12.7; // mm
		$margin_top = $margin;
		$margin_bottom = $margin;
		$margin_left = $margin;
		$margin_right = $margin;
		$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
		
		//pdf metadata setting
		$pdf->SetTitle($report_title);
		$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
		$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
		$pdf->SetSubject($report_title);
		$pdf->SetKeywords($report_title);
		
		// Chinese use mingliu 
		$pdf->backupSubsFont = array('mingliu');
		$pdf->useSubstitutions = true;
		
		$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">{PAGENO} / {nbpg}</div>');
		$pdf->SetHTMLFooterByName('htmlFooter');
		$pos = 0;
		$len = mb_strlen($x);
		$perlen = 100000;
		for($p=$pos;$p<$len;$p+=$perlen)
		{
			$y = mb_substr($x, $p, $perlen);
			$pdf->WriteHTML($y);
		}
		$pdf->Output('Absent_Summary_Report(Half_Day).pdf', 'I');
	}
}



intranet_closedb();
?>