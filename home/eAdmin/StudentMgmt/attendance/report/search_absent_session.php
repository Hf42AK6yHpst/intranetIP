<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['SmartCardAttendance_StudentAbsentSession']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_SearchAbsentSession";


$linterface = new interface_html();

$TAGS_OBJ[] = array($button_find." (".$Lang['StudentAttendance']['AbsentSessions'].")", "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

echo $StudentAttendUI->Get_Search_Sessions_Report_Form();
?>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<script type="text/javascript">
<!--
  function SelectAll(obj)
  {
		for (i=0; i<obj.length; i++)
		{
			obj.options[i].selected = true;
		}
  }

  function isValidDate(obj){
			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
  }
	
  function checkform(formObj){
		if(formObj==null)return false;
      	if(formObj.startStr==null || formObj.endStr==null) return false;
      	if(!isValidDate(formObj.startStr) || !isValidDate(formObj.endStr)) return false;
      	if(formObj.startStr.value>formObj.endStr.value){
          		alert('<?=$i_Booking_EndDateWrong?>');
          		 return false;
         }
      	var ClassSelected = false;
      	var ClassSelectObj = document.getElementById('class_name[]');
      	for(i=0;i<ClassSelectObj.options.length;i++){
          	if(ClassSelectObj.options[i].selected) {
          		ClassSelected = true;
          		break;
          	}
        	}
        	
      	if(!ClassSelected){
      		alert('<?=$i_Discipline_System_alert_PleaseSelectClass?>');
          		return false;
          	}
        
        var cbLate = document.getElementById('se_late');
        var cbRequestLeave = document.getElementById('se_request_leave');
        var cbPlayTruant = document.getElementById('se_play_truant');
        var cbOfficialLeave = document.getElementById('se_official_leave');
        if(!cbLate.checked && !cbRequestLeave.checked && !cbPlayTruant.checked && !cbOfficialLeave.checked){
        	alert('<?=$Lang['StudentAttendance']['SelectAbsentSessionTypeWarning']?>');
        	return false;
        }else{
        	return true;
        }
      }
  function submitForm(formObj)
  {
      if(checkform(formObj)){ 
	 	  selected_format = formObj.format.options[formObj.format.selectedIndex].value;
	      if(selected_format=='2'){
		      formObj.target="_blank";
		  } else {
		 	 formObj.target='intranet_popup10';
		 	 newWindow('about:blank', 10);
	 	  }
	 	  
		  formObj.submit();
	  }
  }
// -->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>