<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['SmartCardAttendance_StudentAbsentSession']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentUI = new libstudentattendance_ui();

$StudentUI->Get_Search_Sessions_Report($class_name,$startStr,$endStr,$day_type,$report_type,$se_late,$se_request_leave,$se_play_truant,$se_official_leave,$format,$DisplayAllStudent);

intranet_closedb();
?>