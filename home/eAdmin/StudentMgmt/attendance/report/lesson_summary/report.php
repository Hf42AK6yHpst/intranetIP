<?php
// Editing by 
/*
 * 2016-06-07 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$Type = IntegerSafe($_POST['Type']);
$GroupBy = IntegerSafe($_POST['GroupBy']);
$WithReason = IntegerSafe($_POST['WithReason']);
$TargetType = $_POST['TargetType']; // "form", "subject" 
$TargetID = $_POST['TargetID'];
$ReportType = $_POST['ReportType']; // "absent", "skip_lesson", "late"
$Format = $_POST['Format']; // ""/"web", "csv", "pdf", "print"

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]
	 || !in_array($Type,array(1,2,3)) || !in_array($GroupBy,array(1,2,3,4,5)) || !in_array($ReportType,array("absent","skip_lesson","late")) || !in_array($Format,array("","web","csv","pdf","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$groupByMap = array(1=>$Lang['General']['Teacher'],2=>$Lang['LessonAttendance']['Subject'],3=>$Lang['StudentAttendance']['Class'],4=>$Lang['Identity']['Student'],5=>$Lang['LessonAttendance']['Period']);

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$academic_year_id = $LessonAttendUI->CurAcademicYearID;
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();

// prepare date columns
$start_ts = strtotime($StartDate);
$end_ts = strtotime($EndDate);
$columns = array(); //  [0] is start date, [1] is end date, [2] is the total count, [3] is the formatted display date string
if($Type == '1') // daily
{
	for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts+=86400)
	{
		$date = date("Y-m-d",$cur_ts);
		$columns[] = array($date,$date,0,date("d/m/Y",$cur_ts));
	}
}else if($Type == '2') // $weekly
{
	$firstday_weekday = intval(date("w",$start_ts));
	$lastday_weekday = intval(date("w",$end_ts));
	$startday_ts = $start_ts - $firstday_weekday * 86399;
	$endday_ts = $end_ts + $lastday_weekday * 86399;
	for($cur_ts=$startday_ts;$cur_ts<=$endday_ts && $cur_ts<=$end_ts;$cur_ts += 86400*7)
	{
		$start_date = date("Y-m-d",$cur_ts);
		$end_date = date("Y-m-d",$cur_ts + 86399*7);
		$columns[] = array($start_date, $end_date,0,date("d/m/Y",$cur_ts).' - '.date("d/m/Y",$cur_ts + 86399*7));
	}
}else if($Type == '3') // $monthly
{
	$start_date = date("Y-m-01",$start_ts);
	$start_ts = strtotime($start_date);
	$days_last_month = intval(date("t",$end_ts));
	$end_ts = strtotime(date("Y-m-01",$end_ts)) + $days_last_month * 86399;
	for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,1,intval(date("m",$cur_ts))+1,date("d",$cur_ts),date("Y",$cur_ts)))
	{
		$days_this_month = intval(date("t", $cur_ts));
		$columns[] = array(date("Y-m-d",$cur_ts), date("Y-m-d,",$cur_ts+$days_this_month*86399),0,date("Y M",$cur_ts));
	}
}

//debug_pr($columns);

if($ReportType == 'absent')
{
	$report_title = $Lang['LessonAttendance']['AbsentSummary'];
	$csv_filename = 'lesson_attendance_absent_summary.csv';
	$student_name_field = getNameFieldByLang2("u1.");
	$cond = "";
	if($WithReason == '1')
	{
		$cond .= " AND sgsa.Reason IS NOT NULL AND TRIM(sgsa.Reason)<>'' ";
	}else if($WithReason == '-1')
	{
		$cond .= " AND (sgsa.Reason IS NULL OR TRIM(sgsa.Reason)='') ";
	}
	
	$sql = "SELECT 
				sgao.LessonDate,
				sgsa.StudentID,
				u1.ClassName,
				u1.ClassNumber,
				$student_name_field as StudentName, 
				COUNT(*) as LessonSession 
			FROM 
			SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
			INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
			INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
			WHERE sgsa.AttendStatus='".$statusMap['Absent']['code']."' AND (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate') $cond 
			GROUP BY sgao.LessonDate,sgsa.StudentID 
			ORDER BY u1.ClassName,u1.ClassNumber+0,sgao.LessonDate";
	$records = $LessonAttendUI->returnResultSet($sql);
	$record_size = count($records);
	$keyToRecords = array();
	
	for($i=0;$i<$record_size;$i++)
	{
		$class_name = $records[$i]['ClassName'];
		$lesson_date = $records[$i]['LessonDate'];
		$lesson_session = $records[$i]['LessonSession'];
		$student_name = $records[$i]['StudentName'].(' ('.$records[$i]['ClassName'].'#'.$records[$i]['ClassNumber'].')');
		$key = $GroupBy == 3 ? $class_name : $student_name;
		if(!isset($keyToRecords[$key])){
			$keyToRecords[$key] = array();
			foreach($columns as $column)
			{
				$keyToRecords[$key][] = $column;
			}
		}
		
		for($j=0;$j<count($keyToRecords[$key]);$j++)
		{
			if($lesson_date >= $keyToRecords[$key][$j][0] && $lesson_date <= $keyToRecords[$key][$j][1])
			{
				$keyToRecords[$key][$j][2] += $lesson_session;
			}
		}
	}
}


if($ReportType == 'skip_lesson')
{
	$report_title = $Lang['LessonAttendance']['SkipLessonSummary'];
	$csv_filename = 'lesson_attendance_skip_lesson_summary.csv';
	
	$student_name_field = getNameFieldByLang2("u1.");
	$teacher_name_field = getNameFieldByLang("u2.");
	$subject_name = Get_Lang_Selection("s.CH_DES","s.EN_DES");
	$absent_code = $statusMap['Absent']['code'];
	$cond = "";
	if($WithReason == '1')
	{
		$cond .= " AND sgsa.Reason IS NOT NULL AND TRIM(sgsa.Reason)<>'' ";
	}else if($WithReason == '-1')
	{
		$cond .= " AND (sgsa.Reason IS NULL OR TRIM(sgsa.Reason)='') ";
	}
	
	if($TargetType != '')
	{
		if(count($TargetID) > 0)
		{
			if($TargetType == 'form')
			{
				$target_result = $LessonAttendUI->GetTargetSelectionResult($TargetType, $TargetID);
				$targetStudentIdAry = $target_result[0];
				$cond .= " AND sgsa.StudentID IN ('".implode("','",(array)$targetStudentIdAry)."') ";
			}else if($TargetType == 'subject'){
				$cond .= " AND st.SubjectID IN ('".implode("','",(array)$TargetID)."') ";
			}
		}
	}
	
	$sql = "SELECT 
				sgao.LessonDate,
				itt.TimeSlotName,
				sgao.StartTime,
				sgsa.StudentID,
				sgsa.AttendStatus,
				st.SubjectID,
				$subject_name as Subject,
				u1.ClassName,
				u1.ClassNumber,
				$student_name_field as StudentName, 
				$teacher_name_field as TeacherName 
			FROM 
			SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
			INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
			INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
			INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
			INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON stct.SubjectGroupID=st.SubjectGroupID 
			INNER JOIN INTRANET_USER as u2 ON u2.UserID=stct.UserID 
			LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao.RoomAllocationID 
			LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
			WHERE (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate') $cond ";
	$sql .= " GROUP BY sgao.AttendOverviewID,sgao.LessonDate,sgsa.StudentID ";
	if($GroupBy == 1) // teacher
	{
		$sql.= " ORDER BY TeacherName,sgao.LessonDate ";
		$key_name = "teacher_name";
	}else if($GroupBy == 2) // subject
	{
		$sql.= " ORDER BY Subject,sgao.LessonDate ";
		$key_name = "subject";
	}else if($GroupBy == 3) // class
	{
		$sql.= " ORDER BY u1.ClassName,sgao.LessonDate ";
		$key_name = "class_name";
	}else if($GroupBy == 4) // student
	{
		$sql.= " ORDER BY u1.ClassName,u1.ClassNumber+0,sgao.LessonDate ";
		$key_name = "student_name";
	}else if($GroupBy == 5) // period 
	{
		$sql.= " ORDER BY sgao.StartTime,sgao.LessonDate ";
		$key_name = "time_slot_name";
	}
	
	$tmp_records = $LessonAttendUI->returnResultSet($sql);
	$tmp_record_size = count($tmp_records);
	$tmpKeyToRecords = array();
	$records = array();
	
	for($i=0;$i<$tmp_record_size;$i++)
	{
		$date = $tmp_records[$i]['LessonDate'];
		$student_id = $tmp_records[$i]['StudentID'];
		$attend_status  = $tmp_records[$i]['AttendStatus'];
		$subject_id = $tmp_records[$i]['SubjectID'];
		if(!isset($tmpKeyToRecords[$date])){
			$tmpKeyToRecords[$date] = array();
		}
		if(!isset($tmpKeyToRecords[$date][$student_id])){
			$tmpKeyToRecords[$date][$student_id] = array();
		}
		if(!isset($tmpKeyToRecords[$date][$student_id][$subject_id])){
			$tmpKeyToRecords[$date][$student_id][$subject_id] = array();
			$tmpKeyToRecords[$date][$student_id][$subject_id]['Records'] = array();
			$tmpKeyToRecords[$date][$student_id][$subject_id]['AbsentSessionCount'] = 0;
		}
		
		$tmpKeyToRecords[$date][$student_id][$subject_id]['Records'][] = $tmp_records[$i];
		if($attend_status == $absent_code)
		{
			$tmpKeyToRecords[$date][$student_id][$subject_id]['AbsentSessionCount'] += 1;
		}
	}
	//debug_pr($tmpKeyToRecords);
	foreach($tmpKeyToRecords as $key1 => $ary1)
	{
		foreach($ary1 as $key2 => $ary2)
		{
			foreach($ary2 as $key3 => $ary3)
			{
				// skip lesson means for the same subject, some lessons are absent, but not all lessons are absent 
				if($ary3['AbsentSessionCount'] > 0 && $ary3['AbsentSessionCount'] < count($ary3['Records']))
				{
					for($j=0;$j<count($ary3['Records']);$j++){
						if($ary3['Records'][$j]['AttendStatus'] == $absent_code)
						{
							$records[] = $ary3['Records'][$j];
						}
					}
				}
			}
		}
	}
	//debug_pr($records);
	$record_size = count($records);
	$keyToRecords = array();
	for($i=0;$i<$record_size;$i++)
	{
		$teacher_name = $records[$i]['TeacherName'];
		$subject = $records[$i]['Subject'];
		$class_name = $records[$i]['ClassName'];
		$lesson_date = $records[$i]['LessonDate'];
		$lesson_session = $records[$i]['LessonSession'];
		$student_name = $records[$i]['StudentName'].(' ('.$records[$i]['ClassName'].'#'.$records[$i]['ClassNumber'].')');
		$period_time = $records[$i]['StartTime'];
		$time_slot_name = $records[$i]['TimeSlotName'];
		$key = $$key_name;
		if(!isset($keyToRecords[$key])){
			$keyToRecords[$key] = array();
			foreach($columns as $column)
			{
				$keyToRecords[$key][] = $column;
			}
		}
		
		for($j=0;$j<count($keyToRecords[$key]);$j++)
		{
			if($lesson_date >= $keyToRecords[$key][$j][0] && $lesson_date <= $keyToRecords[$key][$j][1])
			{
				$keyToRecords[$key][$j][2] += 1;
			}
		}
	}
	
}

if($ReportType == 'late')
{
	$report_title = $Lang['LessonAttendance']['LateSummary'];
	$csv_filename = 'lesson_attendance_late_summary.csv';
	$student_name_field = getNameFieldByLang2("u1.");
	$teacher_name_field = getNameFieldByLang("u2.");
	$subject_name = Get_Lang_Selection("s.CH_DES","s.EN_DES");
	$cond = "";
	if($WithReason == '1')
	{
		$cond .= " AND sgsa.Reason IS NOT NULL AND TRIM(sgsa.Reason)<>'' ";
	}else if($WithReason == '-1')
	{
		$cond .= " AND (sgsa.Reason IS NULL OR TRIM(sgsa.Reason)='') ";
	}
	
	if($TargetType != '')
	{
		if(count($TargetID) > 0)
		{
			if($TargetType == 'form')
			{
				$target_result = $LessonAttendUI->GetTargetSelectionResult($TargetType, $TargetID);
				$targetStudentIdAry = $target_result[0];
				$cond .= " AND sgsa.StudentID IN ('".implode("','",(array)$targetStudentIdAry)."') ";
			}else if($TargetType == 'subject'){
				$cond .= " AND st.SubjectID IN ('".implode("','",(array)$TargetID)."') ";
			}
		}
	}
	
	$sql = "SELECT 
				sgao.LessonDate,
				itt.TimeSlotName,
				sgao.StartTime,
				sgsa.StudentID,
				st.SubjectID,
				$subject_name as Subject,
				u1.ClassName,
				u1.ClassNumber,
				$student_name_field as StudentName, 
				$teacher_name_field as TeacherName, 
				COUNT(*) as LessonSession 
			FROM 
			SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
			INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
			INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
			INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
			INNER JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON stct.SubjectGroupID=st.SubjectGroupID 
			INNER JOIN INTRANET_USER as u2 ON u2.UserID=stct.UserID 
			LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao.RoomAllocationID 
			LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
			WHERE sgsa.AttendStatus IN ('".$statusMap['Late']['code']."' ".(isset($statusMap['LateForSplitClass']['code'])?",'".$statusMap['LateForSplitClass']['code']."'":"").") AND (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate') $cond ";
	if($GroupBy == 1) // teacher
	{
		$sql.= " GROUP BY sgao.LessonDate,TeacherName 
				ORDER BY TeacherName,sgao.LessonDate ";
		$key_name = "teacher_name";
	}else if($GroupBy == 2) // subject
	{
		$sql.= " GROUP BY sgao.LessonDate,Subject 
				ORDER BY Subject,sgao.LessonDate ";
		$key_name = "subject";
	}else if($GroupBy == 3) // class
	{
		$sql.= " GROUP BY sgao.LessonDate,u1.ClassName 
				ORDER BY u1.ClassName,sgao.LessonDate ";
		$key_name = "class_name";
	}else if($GroupBy == 4) // student
	{
		$sql.= " GROUP BY sgao.LessonDate,sgsa.StudentID 
				ORDER BY u1.ClassName,u1.ClassNumber+0,sgao.LessonDate";
		$key_name = "student_name";
	}else if($GroupBy == 5) // period 
	{
		$sql.= " GROUP BY sgao.LessonDate,sgao.StartTime 
				ORDER BY sgao.StartTime,sgao.LessonDate ";
		$key_name = "time_slot_name";
	}
	
	$records = $LessonAttendUI->returnResultSet($sql);
	$record_size = count($records);
	$keyToRecords = array();
	
	for($i=0;$i<$record_size;$i++)
	{
		$teacher_name = $records[$i]['TeacherName'];
		$subject = $records[$i]['Subject'];
		$class_name = $records[$i]['ClassName'];
		$lesson_date = $records[$i]['LessonDate'];
		$lesson_session = $records[$i]['LessonSession'];
		$student_name = $records[$i]['StudentName'].(' ('.$records[$i]['ClassName'].'#'.$records[$i]['ClassNumber'].')');
		$period_time = $records[$i]['StartTime'];
		$time_slot_name = $records[$i]['TimeSlotName'];
		$key = $$key_name;
		if(!isset($keyToRecords[$key])){
			$keyToRecords[$key] = array();
			foreach($columns as $column)
			{
				$keyToRecords[$key][] = $column;
			}
		}
		
		for($j=0;$j<count($keyToRecords[$key]);$j++)
		{
			if($lesson_date >= $keyToRecords[$key][$j][0] && $lesson_date <= $keyToRecords[$key][$j][1])
			{
				$keyToRecords[$key][$j][2] += $lesson_session;
			}
		}
	}
	
	//debug_pr($keyToRecords);
}

ksort($keyToRecords);

if(in_array($Format,array("","web")))
{
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
				$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	
	$column_percent = sprintf("%d", 100.0 / (count($columns)+2));
	
	$x.= '<table class="common_table_list_v30">'."\n";
			$x.='<thead>';
			$x.= '<tr>';
				$x.='<th style="width:'.$column_percent.'%;">'.$groupByMap[$GroupBy].'</th>';
				for($i=0;$i<count($columns);$i++)
				{
					$x .= '<th style="width:'.$column_percent.'%;text-align:center;">'.$columns[$i][3].'</th>';
				}
				$x.='<th style="width:'.$column_percent.'%;text-align:center;font-weight:bold;">'.$Lang['General']['Total'].'</th>';
			$x.= '</tr>'."\n";
			$x.='</thead>';
			$x.='<tbody>';
			
		if(count($keyToRecords) == 0){
			$x.='<tr>';
				$x.='<td colspan="'.(count($columns)+2).'" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
			$x.='</tr>'."\n";
		}else{
			$column_total = array();
			$all_total = 0;
			foreach($keyToRecords as $key => $ary)
			{
				$row_total = 0;
				$x .= '<tr>';
					$x.='<td>'.$key.'</td>';
					for($j=0;$j<count($ary);$j++)
					{
						$x .= '<td style="text-align:right;">'.$ary[$j][2].'</td>';
						$row_total += $ary[$j][2];
						$column_total[$j] += $ary[$j][2];
					}
					$x.= '<td style="text-align:right;font-weight:bold;">'.$row_total.'</td>';
				$x .= '</tr>'."\n";
				$all_total += $row_total;
			}
			$x .= '<tr>';
				$x .= '<td style="text-align:right;font-weight:bold;">'.$Lang['General']['Total'].'</td>';
					for($j=0;$j<count($column_total);$j++)
					{
						$x .= '<td style="text-align:right;font-weight:bold;">'.$column_total[$j].'</td>';
					}
				$x .= '<td style="text-align:right;font-weight:bold;">'.$all_total.'</td>';
			$x .= '</tr>';
		}
		$x .= '</tbody>';
	$x .= '</table>'."\n";
	
	echo $x;
}

if($Format == "print" || $Format == "pdf")
{	
	$x = '';
	if($Format == "print")
	{
		$column_percent = sprintf("%d", 100.0 / (count($columns)+2));
		
		$x .= '<table width="98%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
					</tr>
				</table>';
	}
		$x .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" align="center">
				 <tr>
					<td align="center"><h2>'.$report_title.'</h2></td>
				 </tr>
			   </table>';	   
		$x .= '<table align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="98%">
			  	<tbody>
			  	<tr class="tabletop">';
				    $x.='<th class="eSporttdborder eSportprinttabletitle" style="width:'.$column_percent.'%;">'.$groupByMap[$GroupBy].'</th>';
				for($i=0;$i<count($columns);$i++)
				{
					$x .= '<th class="eSporttdborder eSportprinttabletitle" style="width:'.$column_percent.'%;text-align:center;">'.$columns[$i][3].'</th>';
				}
				$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:'.$column_percent.'%;text-align:center;font-weight:bold;">'.$Lang['General']['Total'].'</th>';
		$x .= '</tr>';
			
			if(count($keyToRecords) == 0){
				$x.='<tr>';
					$x.='<td class="eSporttdborder eSportprinttext" colspan="'.(count($columns)+2).'" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
				$x.='</tr>'."\n";
			}else{
				$column_total = array();
				$all_total = 0;
				foreach($keyToRecords as $key => $ary)
				{
					$row_total = 0;
					$x .= '<tr>';
						$x.='<td class="eSporttdborder eSportprinttext">'.$key.'</td>';
						for($j=0;$j<count($ary);$j++)
						{
							$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:right">'.$ary[$j][2].'</td>';
							$row_total += $ary[$j][2];
							$column_total[$j] += $ary[$j][2];
						}
						$x.= '<td class="eSporttdborder eSportprinttext" style="text-align:right;font-weight:bold;">'.$row_total.'</td>';
					$x .= '</tr>'."\n";
					$all_total += $row_total;
				}
				$x .= '<tr>';
					$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:right;font-weight:bold;">'.$Lang['General']['Total'].'</td>';
						for($j=0;$j<count($column_total);$j++)
						{
							$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:right;font-weight:bold;">'.$column_total[$j].'</td>';
						}
					$x .= '<td class="eSporttdborder eSportprinttext" style="text-align:right;font-weight:bold;">'.$all_total.'</td>';
				$x .= '</tr>';
			}
			
			$x .= '</tbody>';
		$x .= '</table>';
		$x .= '<br />';
		
		if($Format == 'print')
		{
			include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
			echo $x;
			include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
		}
		
		if($Format == 'pdf')
		{
			require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
			
			$css_file_path = $intranet_root.'/templates/2009a/css/print_25.css';
			$css_content = "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>";
			
			$x = $css_content.$x;
			
			$margin= 12.7; // mm
			$margin_top = $margin;
			$margin_bottom = $margin;
			$margin_left = $margin;
			$margin_right = $margin;
			$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
			
			//pdf metadata setting
			$pdf->SetTitle($report_title);
			$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
			$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
			$pdf->SetSubject($report_title);
			$pdf->SetKeywords($report_title);
			
			// Chinese use mingliu 
			$pdf->backupSubsFont = array('mingliu');
			$pdf->useSubstitutions = true;
			
			$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">{PAGENO} / {nbpg}</div>');
			$pdf->SetHTMLFooterByName('htmlFooter');
			$pos = 0;
			$len = mb_strlen($x);
			$perlen = 100000;
			for($p=$pos;$p<$len;$p+=$perlen)
			{
				$y = mb_substr($x, $p, $perlen);
				$pdf->WriteHTML($y);
			}
			$pdf->Output('lesson_attendance_absent_summary.pdf', 'I');
		}
}

if($Format == "csv")
{
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$rows = array();
	$row = array();
	$headers = array();
	
	$headers[] = $groupByMap[$GroupBy];
	for($i=0;$i<count($columns);$i++)
	{
		$headers[] = $columns[$i][3];
	}
	$headers[] = $Lang['General']['Total'];
	
	$column_total = array();
	$all_total = 0;
	foreach($keyToRecords as $key => $ary)
	{
		$row_total = 0;
		unset($row);
		
		$row[] = $key;
		for($j=0;$j<count($ary);$j++)
		{
			$row[] = $ary[$j][2];
			$row_total += $ary[$j][2];
			$column_total[$j] += $ary[$j][2];
		}
		$row[] = $row_total;
		$all_total += $row_total;
		$rows[] = $row;
	}
	unset($row);
	$row[] = $Lang['General']['Total'];
	for($j=0;$j<count($column_total);$j++)
	{
		$row[] = $column_total[$j];
	}
	$row[] = $all_total;
	$rows[] = $row;
	
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $headers);
	$lexport->EXPORT_FILE($csv_filename, $exportContent);
}

intranet_closedb();
?>