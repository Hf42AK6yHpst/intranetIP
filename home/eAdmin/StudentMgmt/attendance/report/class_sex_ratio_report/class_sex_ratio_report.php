<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttendUI = new libstudentattendance_ui();

$TargetDate = $_REQUEST['TargetDate'];
$DayType = $_REQUEST['DayType'];
$Format = $_REQUEST['Format'];
$ClassName = $_REQUEST['class_name'];

//debug_r($_REQUEST);
$StudentAttendUI->Get_Absent_Present_Sex_Distribution_By_Class($TargetDate,$DayType,$ClassName,$Format);

intranet_closedb();
?>