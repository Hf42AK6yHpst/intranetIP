<?php
// Editing by 
/*
 * 2020-09-29 (Ray): add reason
 * 2016-06-16 (Carlos): Modified query to only get consolidated lesson attendance records. 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancelesson']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$LessonAttendUI = new libstudentattendance_ui();

$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
$target_date = $target_date==""?$today:$target_date;
$date_cond = " a.RecordDate = '$target_date' ";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

switch($filter){
	case 4: $cond_filter = " and sgsa1.AttendStatus = 1 "; break;
	case 2: $cond_filter = " and sgsa1.AttendStatus = 2 "; break;
	case 3: $cond_filter = " and sgsa1.AttendStatus = 3 "; break;
	case 1: $cond_filter = " and (sgsa1.AttendStatus = 2 or sgsa1.AttendStatus = 3) "; break;
	default: $cond_filter =" and (sgsa1.AttendStatus = 2 or sgsa1.AttendStatus = 3) ";
}

$namefield = getNameFieldByLang("b.");

$field_array = array("$namefield","b.ClassName","b.ClassNumber","a.DayPeriod","a.Reason","Waive","a.Remark");
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";
/*
if($target_date!=""){
	$lb = new libdb();
	$sql=" SELECT $namefield,
				b.ClassName,b.ClassNumber,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				 a.Remark
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";
	$temp = $lb->returnArray($sql,6);
	
	$lexport = new libexporttext();
	
	$exportColumn = array("$i_UserStudentName", "$i_ClassName", "$i_ClassNumber", 
						"$i_Attendance_DayType", "$i_Attendance_Reason",$Lang['StudentAttendance']['Waived'], "$i_Attendance_Remark");
	$export_content = $lexport->GET_EXPORT_TXT($temp, $exportColumn);
	
	
	$export_content_final = "$i_SmartCard_DailyOperation_Preset_Absence ($target_date)\n\n";
	if(sizeof($temp)<=0){
		$export_content_final .= "$i_no_record_exists_msg\n";
	} else {
		$export_content_final .= $export_content;
	}
	
}
*/

$studentID = implode(",",$StudentList);
if($studentID!=""){
	$lb = new libdb();
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	/*
	$sql=" SELECT CONCAT('<a class=\'tablelink\' href=\'edit.php?RecordID=',a.RecordID,'\'>',a.RecordDate,'</a>'),
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				 a.Remark,
				 CONCAT('<input type=''checkbox'' name=''RecordID[]'' value=', a.RecordID ,'>')
				FROM CARD_STUDENT_PRESET_LEAVE AS a WHERE a.StudentID IN ($studentID)  $cond_filter AND 
				(a.RecordDate LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				";
				*/
	//new sql statemant [Start]
	$cond ='';
	  	if($studentID != '')
	  		$cond = " AND a1.StudentID IN (".$studentID.") ";
	  	
	  	$StudentList = explode(",",$studentID);
	  	$NameField = getNameFieldWithClassNumberByLang('u.');
		$NameField1 = getNameFieldWithClassNumberByLang('u1.');
	  	$TermInfo = getAcademicYearInfoAndTermInfoByDate(date('Y-m-d'));
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	  	//debug_pr($AcademicYearID." ".$studentID);
	  	/*
	  	$sql = 'select distinct
                a1.StudentID
              from
                SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as o1
                inner join
                SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' as a1
                on o1.AttendOverviewID = a1.AttendOverviewID '.$cond;
//                o1.LessonDate = \''.$TargetDate.'\''.$cond.'
//                and o1.AttendOverviewID = a1.AttendOverviewID
//                and a1.AttendStatus in ('.implode(',',$AttendStatus).')';
      $StudentList = $LessonAttendUI->returnVector($sql);
      */
      //debug_pr($sql);
      $ClassTitle = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
      $SubjectName = Get_Lang_Selection("s.CH_DES", "s.EN_DES");
      
      if (sizeof($StudentList) > 0) {
      	$tempField = "ORDER BY ";
if($field == 1){
	$tempField .= "ClassName";
}else if($field == 2){
	$tempField .= "ClassNumber";
}else if($field == 3){
	$tempField .= "LessonDate1";
}else if($field == 4){
	$tempField .= "DayPeriod1";
}else if($field == 5){
	$tempField .= "SubjectGroupTitle";
}else if($field == 6){
	$tempField .= "AttendStatus1";
}else if($field == 7){
	$tempField .= "Reason";
}else if($field == 0){
	$tempField .= "StudentName";
}else
	$tempField ='';

$tempOrder = "";
if($order == 0){
	$tempOrder = " DESC";
}
		  	$StudenCondition = implode(',',$StudentList);
		  	
		  		$NameField3 = getNameFieldByLang('u.');
		  		//debug_pr($NameField);
				$sql = "select DISTINCT ".$NameField3." as StudentName,
						IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassName) as ClassName,
								IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassNumber) as ClassNumber,
								  IF(sgao1.LessonDate IS NULL, '', sgao1.LessonDate) as LessonDate1,
								  CONCAT(itt.TimeSlotName,' (',itt.StartTime,' - ',itt.EndTime,')') as DayPeriod1,
								  CONCAT($SubjectName,' - ',$ClassTitle) as SubjectGroupTitle,
								  (IF(sgsa1.AttendStatus = 2, '".$Lang['LessonAttendance']['Late']."', IF(sgsa1.AttendStatus = 3, '".$Lang['LessonAttendance']['Absent']."', IF(sgsa1.AttendStatus = 1, '".$Lang['LessonAttendance']['Outing']."', '')))) as AttendStatus1
								from
								  INTRANET_CYCLE_GENERATION_PERIOD as icgp
									inner join
									INTRANET_PERIOD_TIMETABLE_RELATION as iptr
									on
									  
										icgp.PeriodID = iptr.PeriodID
								  inner join
								  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
								  on
								    itra.TimeTableID = iptr.TimeTableID
								   
								  inner join
								  SUBJECT_TERM_CLASS as stc
								  on stc.SubjectGroupID = itra.SubjectGroupID
								  inner join 
								  SUBJECT_TERM as st
								  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = '$YearTermID' 
								  inner join 
								  ASSESSMENT_SUBJECT as s 
								  on st.SubjectID = s.RecordID 
								  inner join
								  SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID as sgao1
								  on
								    sgao1.SubjectGroupID = itra.SubjectGroupID
								    and sgao1.LessonDate between '$FromDate' and '$ToDate'
								    and sgao1.RoomAllocationID = itra.RoomAllocationID
								  inner join
								  SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID as sgsa1
								  on
								    sgsa1.AttendOverviewID = sgao1.AttendOverviewID
								    and
								    sgsa1.StudentID in ($StudenCondition)
								  inner JOIN
								  INTRANET_USER as u
								  on u.UserID = sgsa1.StudentID
								  inner JOIN 
								  SUBJECT_TERM_CLASS_USER as stcu
								  on 
								  	stcu.SubjectGroupID = stc.SubjectGroupID
								  	and 
								  	stcu.UserID in ($StudenCondition)
								  inner JOIN 
								  INTRANET_USER as u1 
								  on u1.UserID = stcu.UserID
								  inner join
								  INTRANET_TIMETABLE_TIMESLOT as itt
								  on itra.TimeSlotID = itt.TimeSlotID
								where 
									true $cond_filter
											AND 
				(sgao1.LessonDate LIKE '%$keyword%' OR $SubjectName LIKE '%$keyword%' OR $ClassTitle LIKE '%$keyword%' )
								 ".$tempField.$tempOrder;
								 
			$sql = "select DISTINCT ".$NameField3." as StudentName,
						IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassName) as ClassName,
								IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassNumber) as ClassNumber,
								  IF(sgao1.LessonDate IS NULL, '', sgao1.LessonDate) as LessonDate1,
								  CONCAT(itt.TimeSlotName,' (',itt.StartTime,' - ',itt.EndTime,')') as DayPeriod1,
								  CONCAT($SubjectName,' - ',$ClassTitle) as SubjectGroupTitle,
								  (IF(sgsa1.AttendStatus = 2, '".$Lang['LessonAttendance']['Late']."', IF(sgsa1.AttendStatus = 3, '".$Lang['LessonAttendance']['Absent']."', IF(sgsa1.AttendStatus = 1, '".$Lang['LessonAttendance']['Outing']."', '')))) as AttendStatus1,
								  sgsa1.Reason 
								from 
								SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID as sgao1 
								inner join SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID as sgsa1 
								  on sgsa1.AttendOverviewID = sgao1.AttendOverviewID 
								INNER JOIN INTRANET_USER as u ON u.UserID=sgsa1.StudentID 
								INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao1.SubjectGroupID=stc.SubjectGroupID 
								INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
								INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
								LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao1.RoomAllocationID 
								LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
							where (sgao1.LessonDate between '$FromDate' and '$ToDate') and sgsa1.StudentID in ($StudenCondition) $cond_filter
								AND (sgao1.LessonDate LIKE '%$keyword%' OR $SubjectName LIKE '%$keyword%' OR $ClassTitle LIKE '%$keyword%' ) ".$tempField.$tempOrder;
      //debug_pr($sql);
      $temp = $lb->returnArray($sql,6);
      
	
	$lexport = new libexporttext();
	
	$exportColumn = array("".$Lang['StudentAttendance']['StudentName'], "$i_ClassName", "$i_ClassNumber","$i_Attendance_Date", "".$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period'],"".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'], "".$Lang['General']['Status2'], $Lang['LessonAttendance']['Reason']);
	$export_content = $lexport->GET_EXPORT_TXT($temp, $exportColumn);
	
	//$lu = new libuser($studentID);
	//$studentName = Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);
	$studentName ="";
	foreach($StudentList as $aStudentID){
		$lu = new libuser($aStudentID);
		$studentName .= Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);
		$studentName .= "_";
		}
	
	$export_content_final = $Lang['LessonAttendance']['LessonSummaryReport']."\n\n";//." (".$Lang['StudentAttendance']['StudentName'].": $studentName | ".$Lang['StudentAttendance']['Class'].": ".$lu->ClassName." | ".$Lang['StudentAttendance']['ClassNumber'].": ".$lu->ClassNumber.")\n\n";
	if(sizeof($temp)<=0){
		$export_content_final .= "$i_no_record_exists_msg\n";
	} else {
		$export_content_final .= $export_content;
	}
      }
}

intranet_closedb();

//$filename = "lesson_summary_report_".$lu->ClassName."#".$lu->ClassNumber.".csv";
$filename = "lesson_summary_report.csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>