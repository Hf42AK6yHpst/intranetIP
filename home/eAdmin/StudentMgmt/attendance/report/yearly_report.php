<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if (!$sys_custom['QualiEd_StudentAttendance'])
{
	die();
}

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

$flag = 1;

if ($Year == "")
{
	$flag = 0;
}

$Year = ($Year == "") ? date('Y') : $Year;

# Get Year List
$years = $lcard->getRecordYear();
$select_year = getSelectByValue($years, "name=Year",$Year,0,1);

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=Class", $Class, 0);

# Month List
$months = $i_general_MonthShortForm;
$select_month = "<SELECT name=Month>\n";
$currMon = date('n')-1;
for ($i=0; $i<sizeof($months); $i++)
{
     $month_name = $months[$i];
     $string_selected = ($currMon==$i? "SELECTED":"");
     $select_month .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month .= "</SELECT>\n";


?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj) {
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_TwoMonths,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method=GET>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr>
	<td align=center><?=$i_general_Year?></td>
	<td align=left><?=$select_year?></td>
</tr>
<tr>
	<td align=center><?=$i_ClassName?></td>
	<td align=left><?=$select_class?></td>
</tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
	# Display result	
	if ($flag == 1)
	{	
		$count = 0;		
		$Month = 9;		

		# Retrieve Class List					
		if ($Class == "")
		{
			$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
			$classes = $lcard->returnVector($sql);
		}			
		else
		{
			$classes[0] = $Class;
		}
		
		$x = "";
		while ($count < 11)
		{			
			$CurrMonth = date("m", mktime(0, 0, 0, $Month, 1, $Year));			
			$CurrYear = date("Y", mktime(0, 0, 0, $Month, 1, $Year));	
					
			# Header
			$x .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
			$x .= "<tr>";			
				$x .= "<td class=tableTitle colspan=4 align=center>$i_general_Year : $CurrYear $i_general_Month : $CurrMonth</td>";
			$x .= "</tr>";
			$x .= "<tr>";
				$x .= "<td class=tableTitle>$i_ClassName</td>";
				$x .= "<td class=tableTitle>$i_StudentAttendance_Status_Present</td>";
				$x .= "<td class=tableTitle>$i_StudentAttendance_Status_Absent</td>";
				$x .= "<td class=tableTitle>$i_StudentAttendance_Status_Late</td>";						
			$x .= "</tr>";	
			# End of Table Header		
			
			for ($i=0; $i<sizeof($classes); $i++)
			{
				$ClassName = $classes[$i];
				$result = $lcard->retrieveMonthAttendanceByClass($ClassName, $CurrYear, $CurrMonth);
							
				$total = $result[0] + $result[1] + $result[2];
				$present = ($total == 0) ? 0 : round(($result[0]/$total*100), 2);
				$absent = ($total == 0) ? 0 : round(($result[1]/$total*100), 2);
				$late = ($total == 0) ? 0 : round(($result[2]/$total*100), 2);				
				
				$TotalPresent[$ClassName] += $result[0];
				$TotalAbsent[$ClassName] += $result[1];
				$TotalLate[$ClassName] += $result[2];
				
				$x .= "<tr>";
					$x .= "<td>$ClassName</td>";				
					$x .= "<td>".($result[0]+0)." $present%</td>";
					$x .= "<td>".($result[1]+0)." $absent%</td>";
					$x .= "<td>".($result[2]+0)." $late%</td>";
				$x .= "</tr>";
			}
			
			$x .= "</table>";
			$count++;
			if ($Month++ >= 12)
			{
				$Month = 1;
				$Year++;
			}			
		}
		
		# Year Statistic
		$display = "";
		$display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
		$display .= "<tr>";
		$display .= "<td class=tableTitle colspan=4 align=center>$i_general_Year : ".($Year-1)."</td>";
		$display .= "</tr>";
		
		$display .= "<tr>";
		$display .= "<td class=tableTitle>$i_ClassName</td>";
		$display .= "<td class=tableTitle>$i_StudentAttendance_Status_Present</td>";
		$display .= "<td class=tableTitle>$i_StudentAttendance_Status_Absent</td>";
		$display .= "<td class=tableTitle>$i_StudentAttendance_Status_Late</td>";
		$display .= "</tr>";

		for ($i=0; $i<sizeof($classes); $i++)
		{
			$ClassName = $classes[$i];
			$Total = $TotalPresent[$ClassName] + $TotalAbsent[$ClassName] + $TotalLate[$ClassName];
			$PresentPercentage = ($Total == 0) ? 0 : round(($TotalPresent[$ClassName]/$Total*100), 2);
			$AbsentPercentage = ($Total == 0) ? 0 : round(($TotalAbsent[$ClassName]/$Total*100), 2);
			$LatePercentage = ($Total == 0) ? 0 : round(($TotalLate[$ClassName]/$Total*100), 2);				
			
			$display .= "<tr>";
			$display .= "<td>$ClassName</td>";
			$display .= "<td>".($TotalPresent[$ClassName]+0)." $PresentPercentage% </td>";
			$display .= "<td>".($TotalAbsent[$ClassName]+0)." $AbsentPercentage% </td>";
			$display .= "<td>".($TotalLate[$ClassName]+0)." $LatePercentage% </td>";
			$display .= "</tr>";
		}			
		$display .= "</table>";		
	}
	echo $display;
	echo $x;

?>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>