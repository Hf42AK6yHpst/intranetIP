<?php
//editing by 
/**************************************************** Change Log ********************************************************************
 * 2015-09-23 (Carlos)[ip2.5.6.10.1]: Added option [Include Tap Card Location].
 * 2014-10-07 (Carlos)[ip.2.5.5.10.1]: created
 ************************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_EntryLog";


$linterface = new interface_html();

$lclass= new libclass();

# class list
$select_class = $lc->getSelectClass('name="ClassName[]" id="ClassName[]" multiple class="class_list" size="10" onchange="Get_Student_List()" style="min-width:150px; width:200px;"',$class_name,1);

# date range
$current_month=date('n');
$current_year =date('Y');
if($current_month>=9){
	$start_date = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
}else{
	$start_date = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
}
$end_date=date('Y-m-d');

$TAGS_OBJ[] = array($Lang['StudentAttendance']['EntryLog'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<script type="text/javascript" language="JavaScript">
function isValidDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function Get_Student_List() {
	var PersonalSelect = document.getElementById('PersonalSelection').checked;
	
	if (PersonalSelect) {
		document.getElementById('StudentListRow').style.display = '';
		var ClassSelected = Get_Selection_Value("ClassName[]","Array");
		
		var PostVar = {
			"ClassList[]":ClassSelected
		};
		$('#StudentListLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$('#StudentListLayer').load("../ajax_get_student_selection.php",PostVar,
			function(data) {
				if (data == "die") {
					window.top.location = "/";
				}
			}
		);
	}
	else {
		document.getElementById('StudentListRow').style.display = 'none';
		$('#StudentListLayer').html('');
	}
}

function submitForm()
{
	var isValid = true;
	var StartDate = $.trim($('#StartDate').val());
	var EndDate = $.trim($('#EndDate').val());
	
	$('#DateWarning').hide();
	if($('#DPWL-StartDate').html()!='' || $('#DPWL-EndDate').html()!=''){
		$('#DateWarning > span').html('<?=$Lang['General']['InvalidDateFormat']?>');
		$('#DateWarning').show();
		isValid = false;
	}else if(StartDate > EndDate){
		$('#DateWarning > span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
		$('#DateWarning').show();
		isValid = false;
	}
	
	var ClassSelected = false;
	var ClassSelectObj = document.getElementById('ClassName[]');
	for(i=0;i<ClassSelectObj.options.length;i++){
		if(ClassSelectObj.options[i].selected) {
			ClassSelected = true;
			break;
		}
	}
	$('#ClassWarning').hide();
	if(!ClassSelected){
		$('#ClassWarning').show();
		isValid = false;
	}
	
	var PersonalSelection = $('#PersonalSelection').is(':checked');
	$('#StudentWarning').hide();
	if(PersonalSelection){
		var StudentSelected = false;
		var StudentSelectObj = document.getElementById('StudentList[]');
		for(i=0;i<StudentSelectObj.options.length;i++){
			if(StudentSelectObj.options[i].selected) {
				StudentSelected = true;
				break;
			}
		}
		if(!StudentSelected){
			$('#StudentWarning').show();
			isValid = false;
		}
	}
	
	if(isValid){
		$('#ReportLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$.post(
			'report.php',
			$('form#form1').serialize(),
			function(data){
				$('#ReportLayer').html(data);
			}
		);
	}
}

function submitReport(formId, format)
{
	var formObj = $('form#'+formId);
	$('#'+formId+' #Format').remove();
	formObj.append('<input type="hidden" name="Format" value="'+format+'" />');
	formObj.submit();
}

function PrintPage(formObj)
{
	submitReport(formObj.name, 'print');
}

function ExportPage(formObj)
{
	submitReport(formObj.name, 'csv');
}

$(document).ready(function(){
	Get_Student_List();
});
</script>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="StartDate"><?=$Lang['General']['Date']?></label></td>
		<td width="70%"class="tabletext">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $start_date).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $end_date)?>
			<?=$linterface->Get_Form_Warning_Msg('DateWarning', $Lang['General']['JS_warning']['InvalidDateRange'], 'DateWarning')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="ClassName[]"><?="$button_select $i_ClassName"?></label></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>
			&nbsp;<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassName[]',true);Get_Student_List();return false;")?>
			<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
			<?=$linterface->Get_Form_Warning_Msg('ClassWarning', $Lang['StudentAttendance']['PleaseSelectAtLeastOnClass'], 'ClassWarning')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="PersonalSelection"><?=$Lang['StudentAttendance']['PersonalSelection']?></label></td>
		<td width="70%"class="tabletext">
			<input type="checkbox" id="PersonalSelection" name="PersonalSelection" value="1" onclick="Get_Student_List();">
		</td>
	</tr>
	<tr id="StudentListRow" style="display:none;">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="Studentlist[]"><?=$Lang['Identity']['Student']?></label></td>
		<td width="70%"class="tabletext">
			<div>
				<span id="StudentListLayer"></span>
				&nbsp;<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('StudentList[]',true);return false;")?>
				<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
			</div>
			<?=$linterface->Get_Form_Warning_Msg('StudentWarning', $Lang['StudentAttendance']['PleaseSelectAtLeastOneStudent'], 'StudentWarning')?>
		</td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="GroupBy"><?=$Lang['StaffAttendance']['GroupBy']?></label></td>
	    <td width="70%"class="tabletext">
			<select name="GroupBy" id="GroupBy"> 
				<option value="Date"><?=$Lang['General']['Date']?></option>
				<option value="Class"><?=$Lang['StudentAttendance']['Class']?></option>
			</select>
	    </td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="IncludeLocation"><?=$Lang['StudentAttendance']['IncludeTapCardLocation']?></label></td>
	    <td width="70%"class="tabletext">
			<input type="checkbox" name="IncludeLocation" id="IncludeLocation" value="1" />
	    </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm();") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "","reset_button"," class='formbutton' onclick=\"window.location.reload();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<div id="ReportLayer"></div>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>