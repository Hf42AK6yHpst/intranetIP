<?php
// Editing by 
/**************************************************** Change Log ********************************************************************
 * 2015-09-23 (Carlos)[ip2.5.6.10.1]: Added tap card location. 
 * 2014-10-07 (Carlos)[ip.2.5.5.10.1]: created
 ************************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

/*
 * @params
 * $StartDate (string)
 * $EndDate (string)
 * $ClassName (array)
 * $PersonalSelection (int or bool)
 * $StudentList (array)
 * $GroupBy (string)
 * $IncludeLocation (1)
 * 
 * $Format (string): web(default), csv, print
 */
 
/*
 * Group by Date
 * +-------------------------------------------------------------------------------------+
 * | Date                                                                                |
 * +-------------------------------------------------------------------------------------+
 * | Class Name | ClassNumber | Student Name | Record Time                               |
 * +-------------------------------------------------------------------------------------+
 * | 1A         | 1           | Student 1    | 08:15:00 | 12:30:00 | 13:30:00 | 17:00:00 |
 * +-------------------------------------------------------------------------------------+
 *
 * 
 * Group by Class
 * +---------------------------------------------------------------------------------------------------+
 * | Class Name                                                                                        |
 * +---------------------------------------------------------------------------------------------------+
 * | Date       | Class Name | Class Number | Student Name | Record Time                               |
 * +---------------------------------------------------------------------------------------------------+
 * | 2014-09-01 | 1A         | 1            | Student 1    | 08:15:00 | 12:30:00 | 13:30:00 | 17:00:00 |
 * +---------------------------------------------------------------------------------------------------+
 * 
 */
$lc = new libcardstudentattend2();
$linterface = new interface_html();
$lexport = new libexporttext();

if($PersonalSelection == '1'){
	$UserIdAry = (array)$_POST['StudentList'];
}else{
	$ClassNameAry = (array)$_POST['ClassName'];
	
	$sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName IN ('".implode("','",$lc->Get_Safe_Sql_Query($ClassNameAry))."')";
	$UserIdAry = $lc->returnVector($sql);
}

$records = $lc->getRawLogRecords($StartDate,$EndDate,$UserIdAry,1);
$record_count = count($records);
/*
 * UserID
 * StudentName
 * ClassName
 * ClassNumber
 * RecordDate
 * RecordTime
 * RecordStation
 */

if($record_count == 0){
	$x = '<div class="no_record_find_v30">'.$Lang['General']['NoRecordFound'].'</div>';
	echo $x;
	intranet_closedb();
	exit;
}

$with_location = $IncludeLocation == 1;
$is_csv = $Format == 'csv';

//debug_r($records);
$data = array();
if($GroupBy == 'Class'){
	
	for($i=0;$i<$record_count;$i++){
		$user_id = $records[$i]['UserID'];
		$student_name = $records[$i]['StudentName'];
		$class_name = $records[$i]['ClassName'];
		$class_number = $records[$i]['ClassNumber'];
		$date = $records[$i]['RecordDate'];
		$time = $records[$i]['RecordTime'];
		$location = trim($records[$i]['RecordStation']);
		
		if(!isset($data[$class_name])){
			$data[$class_name] = array();
		}
		if(!isset($data[$class_name][$date])){
			$data[$class_name][$date] = array();
		}
		if(!isset($data[$class_name][$date][$user_id])){
			$data[$class_name][$date][$user_id] = array();
			$data[$class_name][$date][$user_id]['Time'] = array();
		}
		
		$data[$class_name][$date][$user_id]['StudentName'] = $student_name;
		$data[$class_name][$date][$user_id]['ClassName'] = $class_name;
		$data[$class_name][$date][$user_id]['ClassNumber'] = $class_number;
		$data[$class_name][$date][$user_id]['Time'][] = $time.($with_location && $location !=''? ' '.($is_csv? $location:intranet_htmlspecialchars($location)):'');
	}
	
}else if($GroupBy == 'Date'){
	
	for($i=0;$i<$record_count;$i++){
		$user_id = $records[$i]['UserID'];
		$student_name = $records[$i]['StudentName'];
		$class_name = $records[$i]['ClassName'];
		$class_number = $records[$i]['ClassNumber'];
		$date = $records[$i]['RecordDate'];
		$time = $records[$i]['RecordTime'];
		$location = trim($records[$i]['RecordStation']);
		
		if(!isset($data[$date])){
			$data[$date] = array();
		}
		if(!isset($data[$date][$user_id])){
			$data[$date][$user_id] = array();
			$data[$date][$user_id]['Time'] = array();
		}
		
		$data[$date][$user_id]['StudentName'] = $student_name;
		$data[$date][$user_id]['ClassName'] = $class_name;
		$data[$date][$user_id]['ClassNumber'] = $class_number;
		$data[$date][$user_id]['Time'][] = $time.($with_location && $location!=''? ' '.($is_csv? $location:intranet_htmlspecialchars($location)):'');
	}
}

if($Format == 'print'){
	$table_attr = ' align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="96%" ';
	$th_attr = ' class="eSporttdborder eSportprinttabletitle" ';
	$td_attr = ' class="eSporttdborder eSportprinttext" ';
}else{
	$table_attr = ' class="common_table_list" ';
	$th_attr = '';
	$td_attr = '';
}

//debug_r($data);
$x = '';
// Web fomat [Print] and [Export] buttons 
if($Format != 'csv' && $Format != 'print')
{
	$x .='<form id="ReportForm" name="ReportForm" method="POST" action="report.php" target="_blank">';
	$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
				<td align="left">';
	$x .= $linterface->GET_LNK_PRINT("#","","PrintPage(document.ReportForm); return false;","","",0);
	$x .= $linterface->GET_LNK_EXPORT("#","","ExportPage(document.ReportForm); return false;","","",0);
	$x .= '		</td>
			</tr>
		  </table>'."\n";
		  
	$x .= '<input type="hidden" name="StartDate" value="'.escape_double_quotes($StartDate).'" />'."\n";
	$x .= '<input type="hidden" name="EndDate" value="'.escape_double_quotes($EndDate).'" />'."\n";
	for($i=0;$i<count($ClassName);$i++){
		$x .= '<input type="hidden" name="ClassName[]" value="'.escape_double_quotes($ClassName[$i]).'" />'."\n";
	}
	$x .= '<input type="hidden" name="PersonalSelection" value="'.escape_double_quotes($PersonalSelection).'" />'."\n";
	for($i=0;$i<count($StudentList);$i++){
		$x .= '<input type="hidden" name="StudentList[]" value="'.escape_double_quotes($StudentList[$i]).'" />'."\n";
	}
	$x .= '<input type="hidden" name="GroupBy" value="'.escape_double_quotes($GroupBy).'" />'."\n";
	$x .= '<input type="hidden" name="IncludeLocation" value="'.escape_double_quotes($IncludeLocation).'" />'."\n";
}

// Print format [Print] button
// Report title
if($Format == 'print'){
	$x .= '<table width="96%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","print_button").'</td>
				</tr>
			</table>'."\n";
	$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['Header']['Menu']['eAttednance'].' - '.$Lang['StudentAttendance']['EntryLog'].'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['General']['From'].' '.intranet_htmlspecialchars($StartDate).' '.$Lang['General']['To'].' '.intranet_htmlspecialchars($EndDate).'</span></td>
			 </tr>
		   </table>
		   <p>&nbsp;</p>'."\n";
}

if($GroupBy == 'Date')
{
	if($Format == 'csv'){
		$header = array($Lang['Header']['Menu']['eAttednance'].' - '.$Lang['StudentAttendance']['EntryLog'],'','','');
		$rows = array();
		$row = array($Lang['General']['From'].' '.$StartDate.' '.$Lang['General']['To'].' '.$EndDate,'','','');
		$rows[] = $row;
	}
	
	foreach($data as $date => $user_ary){
		
		if($Format == 'csv'){
			$row = array('','','','');
			$rows[] = $row;
			$row = array($date,'','','');
			$rows[] = $row;
			
			$row = array($Lang['StudentAttendance']['ClassName'],$Lang['StudentAttendance']['ClassNumber'],$Lang['StudentAttendance']['StudentName'],$Lang['StudentAttendance']['RecordTime']);
			$rows[] = $row;
		}
		
		$x .= '<table '.$table_attr.'>
				  	<tbody>
				  	<tr>
				  		<td colspan="4" '.$td_attr.'>'.Get_String_Display($date).'</td>
				  	</tr>
				  	<tr class="tabletop">
					    <th width="10%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassName'].'</th>
					    <th width="5%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassNumber'].'</th>
						<th width="15%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['StudentName'].'</th>
						<th width="70%" '.$th_attr.'>'.$Lang['StudentAttendance']['RecordTime'].'</th>
				    </tr>'."\n";
		
		foreach($user_ary as $user_id => $info_ary){
			
			if($Format == 'csv'){
				$row = array($info_ary['ClassName'],$info_ary['ClassNumber'],$info_ary['StudentName'],implode(' | ',$info_ary['Time']));
				$rows[] = $row;
			}
			
			$x .= '<tr>';
				$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassName']).'</td>';
				$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassNumber']).'</td>';
				$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['StudentName']).'</td>';
				$x .= '<td '.$td_attr.'>';
					for($i=0;$i<count($info_ary['Time']);$i++){
						$color = $i % 2 == 0? 'green' : 'red';
						if($i>0){
							$x .= ' | ';
						}
						$x .= '<span style="color:'.$color.'">'.$info_ary['Time'][$i].'</span>';
					}
				$x .= '</td>';
			$x .= '</tr>'."\n";
			
		}
		
			$x .= '</tbody>';
		$x .= '</table><br />'."\n";
	}
	
	if($Format == 'csv'){
		$filename = "eAttendance_EntryLog.csv";
		$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
		$lexport->EXPORT_FILE($filename, $export_content);
	}
}else{
	
	if($Format == 'csv'){
		$header = array($Lang['Header']['Menu']['eAttednance'].' - '.$Lang['StudentAttendance']['EntryLog'],'','','','');
		$rows = array();
		$row = array($Lang['General']['From'].' '.$StartDate.' '.$Lang['General']['To'].' '.$EndDate,'','','','');
		$rows[] = $row;
	}
	
	foreach($data as $class => $date_ary){
		
		if($Format == 'csv'){
			$row = array('','','','','');
			$rows[] = $row;
			$row = array($class,'','','','');
			$rows[] = $row;
			
			$row = array($Lang['General']['Date'],$Lang['StudentAttendance']['ClassName'],$Lang['StudentAttendance']['ClassNumber'],$Lang['StudentAttendance']['StudentName'],$Lang['StudentAttendance']['RecordTime']);
			$rows[] = $row;
		}
		
		$x .= '<table '.$table_attr.'>
				  	<tbody>
				  	<tr>
				  		<td colspan="5" '.$td_attr.'>'.Get_String_Display($class).'</td>
				  	</tr>
				  	<tr class="tabletop">
						<th width="10%" nowrap="nowrap" '.$th_attr.'>'.$Lang['General']['Date'].'</th>
					    <th width="10%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassName'].'</th>
					    <th width="5%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassNumber'].'</th>
						<th width="15%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['StudentName'].'</th>
						<th width="60%" '.$th_attr.'>'.$Lang['StudentAttendance']['RecordTime'].'</th>
				    </tr>'."\n";
		foreach($date_ary as $date => $user_ary){
			
			foreach($user_ary as $user_id => $info_ary){
				
				if($Format == 'csv'){
					$row = array($date,$info_ary['ClassName'],$info_ary['ClassNumber'],$info_ary['StudentName'],implode(' | ',$info_ary['Time']));
					$rows[] = $row;
				}
				
				$x .= '<tr>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($date).'</td>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassName']).'</td>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassNumber']).'</td>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['StudentName']).'</td>';
					$x .= '<td '.$td_attr.'>';
						for($i=0;$i<count($info_ary['Time']);$i++){
							$color = $i % 2 == 0? 'green' : 'red';
							if($i>0){
								$x .= ' | ';
							}
							$x .= '<span style="color:'.$color.'">'.$info_ary['Time'][$i].'</span>';
						}
					$x .= '</td>';
				$x .= '</tr>'."\n";
				
			}
		}
			$x .= '</tbody>';
		$x .= '</table><br />'."\n";
	}
	
	if($Format == 'csv'){
		$filename = "eAttendance_EntryLog.csv";
		$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
		$lexport->EXPORT_FILE($filename, $export_content);
	}
}

if($Format != 'csv' && $Format != 'print')
{
	$x .= '</form>'."\n";
}

if($Format=='print'){
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
}
if($Format != 'csv'){
	echo $x;
}
if($Format=='print'){
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
}

intranet_closedb();
?>