<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_LessonNonconfirmedReport";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonNonConfirmedReport'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 

echo $LessonAttendUI->Get_Lesson_Non_Confirmed_Report_Form();

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script type="text/javascript" language="JavaScript">
function Get_Lesson_Non_Confirmed_Report_Table()
{
	var fromDateObj = document.getElementById('FromDate');
	var toDateObj = document.getElementById('ToDate');
	var fromDate = $.trim(fromDateObj.value);
	var toDate = $.trim(toDateObj.value);
	var isValid = true;
	
	if($('#DPWL-FromDate').html()!='') {
		isValid = false;
	}
	
	if($('#DPWL-ToDate').html()!='') {
		isValid = false;
	}
	
	if(isValid && fromDate > toDate){
		$('#ToDateWarningLayer').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
		isValid = false;
	}else{
		$('#ToDateWarningLayer').html('').hide();
	}
	
	if(isValid){
		Block_Element('TableDiv');
		$('#TableDiv').load(
			'ajax_get_lesson_non_confirmed_report.php',
			{
				'FromDate':fromDate,
				'ToDate':toDate 
			},
			function(data){
				UnBlock_Element('TableDiv');
			}
		);
	}
}

$(document).ready(function(){
	Get_Lesson_Non_Confirmed_Report_Table();
});
</script>