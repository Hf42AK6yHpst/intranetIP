<?php
// Editing by 
/*
 * 2016-06-08 (Carlos): Created.
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$TargetType = $_POST['TargetType'];
$TargetID = $_POST['TargetID'];
$Format = $_POST['Format']; // ""/"web", "pdf", "print"

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"] || count($TargetID)==0 || !in_array($Format,array("","web","pdf","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$Settings = $LessonAttendUI->Get_General_Settings('LessonAttendance', $LessonAttendUI->GetLessonAttendanceSettingsList());
$am_session_start = $Settings['StartSessionToCountAsAM'];
$am_session_end = $Settings['EndSessionToCountAsAM'];
$pm_session_start = $Settings['StartSessionToCountAsPM'];
$pm_session_end = $Settings['EndSessionToCountAsPM'];

function isAMAbsent($oneDayRecords, $absentCode)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	// if either 1-3, 1-4, 1-5, 1-6 are absent then treated as AM absent
	//$am_lesson_lower_index = 3; 
	//$am_lesson_upper_index = 6; 
	$record_size = count($oneDayRecords);
	$flags = array_fill(1, $record_size, 0);
	for($i=0;$i<$record_size;$i++)
	{
		if($oneDayRecords[$i]['AttendStatus'] == $absentCode){
			$flags[$i+1] = 1;
		}
	}
	return ($flags[1] && $flags[2] && $flags[3]);
}

function isPMAbsent($oneDayRecords, $absentCode)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	// if 7-[last lesson] are all absent then treated as PM absent
	$pm_lesson_index = $pm_session_start;
	$record_size = count($oneDayRecords);
	$flags = array_fill(1, $record_size, 0);
	for($i=0;$i<$record_size;$i++)
	{
		if($oneDayRecords[$i]['AttendStatus'] == $absentCode){
			$flags[$i+1] = 1;
		}
	}
	$is_pm_absent = true;
	for($i=$pm_lesson_index;$i<=$record_size;$i++)
	{
		$is_pm_absent = $is_pm_absent && $flags[$i]==1;
	}
	return $is_pm_absent;
}

function findSkipLessonRecords($records, $absentCode)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	$subjectIdToRecords = array();
	$record_size = count($records);
	$returnAry = array();
	// group records by subject
	for($i=0;$i<$record_size;$i++)
	{
		$subject_id = $records[$i]['SubjectID'];
		if(!isset($subjectIdToRecords[$subject_id]))
		{
			$subjectIdToRecords[$subject_id] = array();
		}
		$subjectIdToRecords[$subject_id][] = $records[$i];
	}
	
	if(count($subjectIdToRecords)>0)
	{
		foreach($subjectIdToRecords as $subject_id => $ary)
		{
			$absent_count = 0;
			$session_count = count($ary);
			$absent_records = array();
			for($j=0;$j<$session_count;$j++)
			{
				if($ary[$j]['AttendStatus'] == $absentCode){
					$absent_count++;
					$absent_records[] = $ary[$j];
				}
			}
			// skip lesson means for the same subject lessons, some lessions are absent, but not all lessons are absent, if all lessons are absent, then it means absent
			if($absent_count > 0 && $absent_count < $session_count)
			{
				$returnAry = $absent_records;
			}
		}
	}
	
	return $returnAry;
}

function isAMSkipLesson($skipRecords, $records)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	if(count($records)<=$am_session_end && count($skipRecords)>0){
		return true;
	}
	$pm_lesson_time = $records[$pm_session_start-1]['StartTime'];
	
	for($i=0;$i<count($skipRecords);$i++){
		if($skipRecords[$i]['StartTime'] < $pm_lesson_time)
		{
			return true;
		}
	}
	
	return false;
}

function isPMSkipLesson($skipRecords, $records)
{
	global $am_session_start, $am_session_end, $pm_session_start, $pm_session_end;
	if(count($records)<$pm_session_start) return false;
	$pm_lesson_time = $records[$pm_session_start-1]['StartTime'];
	
	for($i=0;$i<count($skipRecords);$i++){
		if($skipRecords[$i]['StartTime'] >= $pm_lesson_time)
		{
			return true;
		}
	}
	
	return false;
}

$academic_year_id = $LessonAttendUI->CurAcademicYearID;
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();

//$student_id = IntegerSafe($TargetID[0]);
//$luser = new libuser($student_id);

$report_title = $Lang['LessonAttendance']['SingleStudentSummaryReport'];

function getReportContent($student_id)
{
	global $Lang, $linterface, $LessonAttendUI, $scm, $academic_year_id, $statusMap, $StartDate, $EndDate, $Format;
	
	$x = '';
	
	$luser = new libuser($student_id);

	$student_name_field = getNameFieldByLang2("u1.");
	$teacher_name_field = getNameFieldByLang("u2.");
	$subject_name = Get_Lang_Selection("s.CH_DES","s.EN_DES");
	
	$sql = "SELECT 
				sgao.LessonDate,
				itt.TimeSlotName,
				sgao.StartTime,
				sgsa.StudentID,
				sgsa.AttendStatus,
				sgsa.Reason,
				st.SubjectID,
				$subject_name as Subject,
				u1.ClassName,
				u1.ClassNumber,
				$student_name_field as StudentName, 
				$teacher_name_field as TeacherName 
			FROM 
			SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
			INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
			INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
			INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
			LEFT JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON stct.SubjectGroupID=st.SubjectGroupID 
			LEFT JOIN INTRANET_USER as u2 ON u2.UserID=stct.UserID 
			LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao.RoomAllocationID 
			LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
			WHERE sgsa.StudentID='$student_id' AND (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate')  ";
	$sql .= " GROUP BY sgao.AttendOverviewID,sgao.LessonDate,sgsa.StudentID ";
	$sql.= " ORDER BY sgao.LessonDate,sgao.StartTime ";
	
	$tmp_records = $LessonAttendUI->returnResultSet($sql);
	//debug_pr($tmp_records);
	
	$tmp_record_size = count($tmp_records);
	$dateToRecords = array();
	$reasonToRecords = array();
	$subjectToRecords = array();
	$periodDateToRecords = array();
	$records = array();
	
	for($i=0;$i<$tmp_record_size;$i++)
	{
		$date = $tmp_records[$i]['LessonDate'];
		$student_id = $tmp_records[$i]['StudentID'];
		$attend_status  = $tmp_records[$i]['AttendStatus'];
		$subject_id = $tmp_records[$i]['SubjectID'];
		$subject = $tmp_records[$i]['Subject'];
		$time_slot_name = $tmp_records[$i]['TimeSlotName'];
		$reason = trim($tmp_records[$i]['Reason']);
		
		// date to records
		if(!isset($dateToRecords[$date])){
			$dateToRecords[$date] = array();
			$dateToRecords[$date]['Records'] = array();
			$dateToRecords[$date]['PresentRecords'] = array();
			$dateToRecords[$date]['AbsentRecords'] = array();
			$dateToRecords[$date]['LateRecords'] = array();
			$dateToRecords[$date]['SkipLessonRecords'] = array();
			$dateToRecords[$date]['AMHalfDayAbsent'] = 0;
			$dateToRecords[$date]['PMHalfDayAbsent'] = 0;
			$dateToRecords[$date]['AMHalfDaySkipLesson'] = 0;
			$dateToRecords[$date]['PMHalfDaySkipLesson'] = 0;
			$dateToRecords[$date]['AMSession'] = 0;
			$dateToRecords[$date]['PMSession'] = 0;
		}
		$dateToRecords[$date]['Records'][] = $tmp_records[$i];
		if($attend_status == $statusMap['Present']['code'])
		{
			$dateToRecords[$date]['PresentRecords'][] = $tmp_records[$i];
		}
		if($attend_status == $statusMap['Absent']['code'])
		{
			$dateToRecords[$date]['AbsentRecords'][] = $tmp_records[$i];
		}
		if($attend_status == $statusMap['Late']['code'] || (isset($statusMap['LateForSplitClass']) && $attend_status == $statusMap['LateForSplitClass']))
		{
			$dateToRecords[$date]['LateRecords'][] = $tmp_records[$i];
		}
		
		// reason to records
		if($reason != '' && !isset($reasonToRecords[$reason])){
			$reasonToRecords[$reason] = array();
			$reasonToRecords[$reason]['HalfDayAbsent'] = 0;
			$reasonToRecords[$reason]['FullDayAbsent'] = 0;
		}
		
		
		// subject to records
		if(!isset($subjectToRecords[$subject])){
			$subjectToRecords[$subject] = array();
			$subjectToRecords[$subject]['TotalSession'] = 0;
			$subjectToRecords[$subject]['PresentSession'] = 0;
			//$subjectToRecords[$subject]['AbsentSession'] = 0;
			$subjectToRecords[$subject]['SkipLessonSession'] = 0;
		}
		
		// period,date to records
		if(!isset($periodDateToRecords[$time_slot_name])){
			$periodDateToRecords[$time_slot_name] = array();
			$weekday = date("w",strtotime($date));
			if(!isset($periodDateToRecords[$time_slot_name][$weekday])){
				$periodDateToRecords[$time_slot_name][$weekday] = array();
				$periodDateToRecords[$time_slot_name][$weekday]['AbsentSession'] = 0;
				$periodDateToRecords[$time_slot_name][$weekday]['LateSession'] = 0;
				$periodDateToRecords[$time_slot_name][$weekday]['SkipLessonSession'] = 0;
			}
			
		}
		
	}
	
	$total_days = count(array_unique(array_keys($dateToRecords)));
	$total_days_fullday_absent = 0;
	$total_days_halfday_absent = 0;
	$total_days_late = 0;
	$total_days_skip_lesson = 0;
	$total_days_halfday_skip_lesson = 0;
	$total_days_fullday_skip_lesson = 0;
	$total_days_am_skip_lesson = 0;
	$total_days_pm_skip_lesson = 0;
	$total_session = 0;
	$total_am_session = 0;
	$total_pm_session = 0;
	$total_attend_session = 0;
	$total_punctual_session = 0;
	$total_skip_lesson_session = 0;
	
	if(count($dateToRecords)>0)
	{
		foreach($dateToRecords as $date => $ary)
		{	
			$skip_lesson_records = findSkipLessonRecords($ary['Records'], $statusMap['Absent']['code']);
			$dateToRecords[$date]['SkipLessonRecords'] = $skip_lesson_records;
			
			$is_am_half_day_absent = isAMAbsent($ary['Records'], $statusMap['Absent']['code']);
			$is_pm_half_day_absent = isPMAbsent($ary['Records'], $statusMap['Absent']['code']);
			$is_fullday_absent = count($dateToRecords[$date]['AbsentRecords']) > 0 && count($dateToRecords[$date]['AbsentRecords']) == count($dateToRecords[$date]['Records']);
			
			$dateToRecords[$date]['AMHalfDayAbsent'] = !$is_fullday_absent && $is_am_half_day_absent? 1 : 0;
			$dateToRecords[$date]['PMHalfDayAbsent'] = !$is_fullday_absent && $is_pm_half_day_absent? 1 : 0;
			
			if($is_fullday_absent){
				$total_days_fullday_absent += 1;
			}
			$total_days_halfday_absent += $dateToRecords[$date]['AMHalfDayAbsent'] + $dateToRecords[$date]['PMHalfDayAbsent'];
			
			if($is_fullday_absent){
				$reasonAdded = array();
				for($j=0;$j<count($ary['Records']);$j++){
					if($ary['Records'][$j]['AttendStatus'] == $statusMap['Absent']['code'] && $ary['Records'][$j]['Reason'] != '' && !in_array($ary['Records'][$j]['Reason'],$reasonAdded))
					{
						$reasonToRecords[$ary['Records'][$j]['Reason']]['FullDayAbsent'] += 1;
						$reasonAdded[] = $ary['Records'][$j]['Reason'];
					}
				}
			}else if($is_am_half_day_absent || $is_pm_half_day_absent){
				$reasonAdded = array();
				for($j=0;$j<count($ary['Records']);$j++){
					if($ary['Records'][$j]['AttendStatus'] == $statusMap['Absent']['code'] && $ary['Records'][$j]['Reason'] != '' && !in_array($ary['Records'][$j]['Reason'],$reasonAdded))
					{
						$reasonToRecords[$ary['Records'][$j]['Reason']]['HalfDayAbsent'] += 1;
						$reasonAdded[] = $ary['Records'][$j]['Reason'];
					}
				}
			}
			
			for($j=0;$j<count($dateToRecords[$date]['Records']);$j++){
				$subjectToRecords[$dateToRecords[$date]['Records'][$j]['Subject']]['TotalSession'] += 1;
			}
			
			for($j=0;$j<count($dateToRecords[$date]['PresentRecords']);$j++){
				$subjectToRecords[$dateToRecords[$date]['PresentRecords'][$j]['Subject']]['PresentSession'] += 1;
			}
			
			for($j=0;$j<count($dateToRecords[$date]['SkipLessonRecords']);$j++){
				$subjectToRecords[$dateToRecords[$date]['SkipLessonRecords'][$j]['Subject']]['SkipLessonSession'] += 1;
				
				$weekday = date("w",strtotime($date));
				$periodDateToRecords[$dateToRecords[$date]['SkipLessonRecords'][$j]['TimeSlotName']][$weekday]['SkipLessonSession'] += 1;
			}
			
			for($j=0;$j<count($dateToRecords[$date]['AbsentRecords']);$j++){
				$weekday = date("w",strtotime($date));
				$periodDateToRecords[$dateToRecords[$date]['AbsentRecords'][$j]['TimeSlotName']][$weekday]['AbsentSession'] += 1;
			}
			
			for($j=0;$j<count($dateToRecords[$date]['LateRecords']);$j++){
				$weekday = date("w",strtotime($date));
				$periodDateToRecords[$dateToRecords[$date]['LateRecords'][$j]['TimeSlotName']][$weekday]['LateSession'] += 1;
			}
			
			$is_am_skip_lesson = isAMSkipLesson($ary['SkipLessonRecords'], $ary['Records']);
			$is_pm_skip_lesson = isPMSkipLesson($ary['SkipLessonRecords'], $ary['Records']);
			
			$total_days_late += count($dateToRecords[$date]['LateRecords']) > 0 ? 1 : 0;
			$total_days_skip_lesson += count($dateToRecords[$date]['SkipLessonRecords']) > 0? 1: 0;
			$total_days_halfday_skip_lesson += $is_am_skip_lesson || $is_pm_skip_lesson? 1:0;
			$total_days_fullday_skip_lesson += $is_am_skip_lesson && $is_pm_skip_lesson? 1:0;
			
			$total_days_am_skip_lesson += $is_am_skip_lesson ? 1:0;
			$total_days_pm_skip_lesson += $is_pm_skip_lesson ? 1:0;
			
			$total_session += count($dateToRecords[$date]['Records']);
			$dateToRecords[$date]['PMSession'] = count($dateToRecords[$date]['Records']) > 6? count($dateToRecords[$date]['Records']) - 6 : 0;
			$dateToRecords[$date]['AMSession'] = count($dateToRecords[$date]['Records']) - $dateToRecords[$date]['PMSession'];
			$total_am_session += $dateToRecords[$date]['AMSession'];
			$total_pm_session += $dateToRecords[$date]['PMSession'];
			
			$total_attend_session += count($dateToRecords[$date]['PresentRecords']) + count($dateToRecords[$date]['LateRecords']);
			$total_punctual_session += count($dateToRecords[$date]['PresentRecords']);
			$total_skip_lesson_session += count($dateToRecords[$date]['SkipLessonRecords']);
		}
	}
	
	
	$x .= '<table width="99%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr style="background:#f3f3f3;">' . "\n";
			$x .= '<td colspan="2" style="width:50%;">' . "\n";
				$x .= $Lang['Identity']['Student'].': <span style="font-weight:bold;">'.$luser->StandardDisplayName.' '.$luser->ClassName.'-'.$luser->ClassNumber.'</span>';
			$x .= '</td>'."\n";
			//$x .= '<td style="width:2%">&nbsp;</td>'."\n";
			$x .= '<td colspan="2" style="text-align:right;width:50%;">'."\n";
				$x .= $Lang['LessonAttendance']['Period'].': <span style="font-weight:bold;">'.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</span>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	
		$x .= '<tr>';
			$x .= '<td style="width:45%;padding-top:16px;" valign="top">';
				$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="left" >' . "\n";
					$attendance_rate = sprintf("%.0f%%", $total_session == 0 ? 0: ($total_attend_session / $total_session * 100) );
					$punctual_rate = sprintf("%.0f%%", $total_attend_session == 0? 0 : ($total_punctual_session / $total_attend_session * 100) );
					$skip_lesson_rate = sprintf("%.0f%%", $total_attend_session == 0? 0 : ($total_skip_lesson_session / $total_attend_session * 100) );
					$x .= '<tr><td>'.$Lang['LessonAttendance']['AttendanceRate'].':</td><td>'.$attendance_rate.'</td></tr>';
					$x .= '<tr><td>'.$Lang['LessonAttendance']['PunctualRate'].':</td><td>'.$punctual_rate.'</td></tr>';
					$x .= '<tr><td>'.$Lang['LessonAttendance']['SkipLessonRate'].':</td><td>'.$skip_lesson_rate.'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:45%;padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['AbsencesByReason'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>&nbsp;</th><th style="text-align:center;">'.$Lang['LessonAttendance']['FullDay'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['HalfDay'].'</th></tr>';
					$i = 0;
					if(count($reasonToRecords)>0)
					{
						foreach($reasonToRecords as $reason => $ary)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class="'.$row_css.'"><td>'.$reason.'</td><td style="text-align:center;">'.$ary['FullDayAbsent'].'</td><td style="text-align:center;">'.$ary['HalfDayAbsent'].'</td></tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr>';
			$x .= '<td style="width:45%;padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['DaysAbsentLate'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>&nbsp;</th><th style="text-align:center;">'.$Lang['LessonAttendance']['TotalSchoolDays'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['Count'].'</th><th style="text-align:center;">%</th></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['LessonAttendance']['FullDay'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_fullday_absent.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days == 0 ? 0 : ($total_days_fullday_absent/$total_days * 100) )).'</td></tr>';
					$x .= '<tr class="row_off"><td>'.$Lang['LessonAttendance']['HalfDay'].'</td><td style="text-align:center;">'.($total_days*2).'</td><td style="text-align:center;">'.$total_days_halfday_absent.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days == 0? 0 : ($total_days_halfday_absent/($total_days*2) * 100) )).'</td></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['LessonAttendance']['Late'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_late.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days == 0 ? 0 : ($total_days_late/$total_days * 100) )).'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:45%;padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['SkipLesson'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>&nbsp;</th><th style="text-align:center;">'.$Lang['LessonAttendance']['TotalSchoolDays'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['Count'].'</th><th style="text-align:center;">%</th></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['StudentAttendance']['AM'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_am_skip_lesson.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days == 0? 0:($total_days_am_skip_lesson/$total_days * 100) )).'</td></tr>';
					$x .= '<tr class="row_off"><td>'.$Lang['StudentAttendance']['PM'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_pm_skip_lesson.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days == 0? 0 : ($total_days_pm_skip_lesson/$total_days * 100))).'</td></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['LessonAttendance']['Overall'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.($total_days_am_skip_lesson+$total_days_pm_skip_lesson).'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days == 0? 0 : (($total_days_am_skip_lesson+$total_days_pm_skip_lesson)/$total_days * 100) )).'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr style="padding-top:16px;">';
			$x .= '<td colspan="4" style="padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['AttendanceBySubject'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>'.$Lang['LessonAttendance']['Subject'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['NumberOfPeriod'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['Present'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['SkipLesson'].'</th><th style="text-align:center;">%</th></tr>';
					if(count($subjectToRecords)>0)
					{
						$i = 0;
						foreach($subjectToRecords as $subject => $ary)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class="'.$row_css.'">';
								$x .= '<td>'.$subject.'</td><td style="text-align:center;">'.$ary['TotalSession'].'</td><td style="text-align:center;">'.$ary['PresentSession'].'</td><td style="text-align:center;">'.$ary['SkipLessonSession'].'</td><td style="text-align:center;">'.sprintf("%.2f%%", $ary['TotalSession'] == 0? 0 : (($ary['PresentSession']) / $ary['TotalSession'] * 100) ).'</td>';
							$x .= '</tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr>';
			$x .= '<td colspan="4" style="padding-top:16px;">';
				$x .= '<span style="font-weight:bold;padding-bottom:8px;text-decoration:underline;width:50%;display:inline-block;">'.$Lang['LessonAttendance']['OverviewByPeriod'].'</span>';
				$x .= '<span style="width:50%;padding-bottom:8px;display:inline-block;text-align:right;">['.$Lang['LessonAttendance']['Absent'].'] ['.$Lang['LessonAttendance']['Late'].'] ['.$Lang['LessonAttendance']['SkipLesson'].']</span>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>'.$Lang['LessonAttendance']['Period'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][1].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][2].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][3].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][4].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][5].'</th>
							</tr>';
					if(count($periodDateToRecords)>0)
					{
						$i = 0;
						foreach($periodDateToRecords as $period => $ary1)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class='.$row_css.'>';
							$x .= '<td>'.$period.'&nbsp;</td>';
							for($w=1;$w<=5;$w++)
							{
								$x .= '<td style="text-align:center;">';
								$map = array('AbsentSession'=>0,'LateSession'=>0,'SkipLessonSession'=>0);
								foreach($ary1 as $weekday => $ary2)
								{
									//$weekday = date("w",strtotime($date));
									if($weekday == $w)
									{
										$map['AbsentSession'] += $ary2['AbsentSession'];
										$map['LateSession'] += $ary2['LateSession'];
										$map['SkipLessonSession'] += $ary2['SkipLessonSession'];
									}
								}
									$x .= $map['AbsentSession'].','.$map['LateSession'].','.$map['SkipLessonSession'];
								$x .= '</td>';
							}
							$x .= '</tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x.= '</tr>';
		
	$x .= '</table>'."\n";
	$x .= '<br /><br />'."\n";
	if(in_array($Format,array('print'))) {
		$x .= '<div class="page_break"></div>';
	}
	
	return $x;
}

if(in_array($Format,array("","web","print","pdf")))
{
	
	if(in_array($Format,array("","web")))
	{
	/*
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
				//$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	*/
		$MODULE_OBJ['title'] = $Lang['LessonAttendance']['SingleStudentSummaryReport'];
		$linterface = new interface_html("popup.html");
	}
	
	if(in_array($Format,array("print","pdf"))){
		$x .= '<style type="text/css">'."\n";
		$x .= '@media print { .page_break {display:block; page-break-after: always !important; } }'."\n";
		$x .= '</style>'."\n";
	}
	
	if($Format == "print")
	{
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>'."\n";
	}
	
	if($Format != "pdf")
	{
		for($i=0;$i<count($TargetID);$i++){
			
			$x .= getReportContent($TargetID[$i]);
			
		}
	}
	/*
	$x .= '<table width="99%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr style="background:#f3f3f3;">' . "\n";
			$x .= '<td colspan="2" style="width:50%;">' . "\n";
				$x .= $Lang['Identity']['Student'].': <span style="font-weight:bold;">'.$luser->StandardDisplayName.' '.$luser->ClassName.'-'.$luser->ClassNumber.'</span>';
			$x .= '</td>'."\n";
			//$x .= '<td style="width:2%">&nbsp;</td>'."\n";
			$x .= '<td colspan="2" style="text-align:right;width:50%;">'."\n";
				$x .= $Lang['LessonAttendance']['Period'].': <span style="font-weight:bold;">'.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</span>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	
		$x .= '<tr>';
			$x .= '<td style="width:45%;padding-top:16px;" valign="top">';
				$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="left" >' . "\n";
					$attendance_rate = sprintf("%.0f%%",  $total_attend_session / $total_session * 100);
					$punctual_rate = sprintf("%.0f%%", $total_punctual_session / $total_attend_session * 100);
					$skip_lesson_rate = sprintf("%.0f%%", $total_skip_lesson_session / $total_attend_session * 100);
					$x .= '<tr><td>'.$Lang['LessonAttendance']['AttendanceRate'].':</td><td>'.$attendance_rate.'</td></tr>';
					$x .= '<tr><td>'.$Lang['LessonAttendance']['PunctualRate'].':</td><td>'.$punctual_rate.'</td></tr>';
					$x .= '<tr><td>'.$Lang['LessonAttendance']['SkipLessonRate'].':</td><td>'.$skip_lesson_rate.'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:45%;padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['AbsencesByReason'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>&nbsp;</th><th style="text-align:center;">'.$Lang['LessonAttendance']['FullDay'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['HalfDay'].'</th></tr>';
					$i = 0;
					if(count($reasonToRecords)>0)
					{
						foreach($reasonToRecords as $reason => $ary)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class="'.$row_css.'"><td>'.$reason.'</td><td style="text-align:center;">'.$ary['FullDayAbsent'].'</td><td style="text-align:center;">'.$ary['HalfDayAbsent'].'</td></tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr>';
			$x .= '<td style="width:45%;padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['DaysAbsentLate'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>&nbsp;</th><th style="text-align:center;">'.$Lang['LessonAttendance']['TotalSchoolDays'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['Count'].'</th><th style="text-align:center;">%</th></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['LessonAttendance']['FullDay'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_fullday_absent.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days_fullday_absent/$total_days * 100)).'</td></tr>';
					$x .= '<tr class="row_off"><td>'.$Lang['LessonAttendance']['HalfDay'].'</td><td style="text-align:center;">'.($total_days*2).'</td><td style="text-align:center;">'.$total_days_halfday_absent.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days_halfday_absent/$total_days * 100)).'</td></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['LessonAttendance']['Late'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_late.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days_late/$total_days * 100)).'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:5%">&nbsp;</td>';
			$x .= '<td style="width:45%;padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['SkipLesson'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>&nbsp;</th><th style="text-align:center;">'.$Lang['LessonAttendance']['TotalSchoolDays'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['Count'].'</th><th style="text-align:center;">%</th></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['StudentAttendance']['AM'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_am_skip_lesson.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days_am_skip_lesson/$total_days * 100)).'</td></tr>';
					$x .= '<tr class="row_off"><td>'.$Lang['StudentAttendance']['PM'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.$total_days_pm_skip_lesson.'</td><td style="text-align:center;">'.(sprintf("%.2f%%", $total_days_pm_skip_lesson/$total_days * 100)).'</td></tr>';
					$x .= '<tr class="row_on"><td>'.$Lang['LessonAttendance']['Overall'].'</td><td style="text-align:center;">'.$total_days.'</td><td style="text-align:center;">'.($total_days_am_skip_lesson+$total_days_pm_skip_lesson).'</td><td style="text-align:center;">'.(sprintf("%.2f%%", ($total_days_am_skip_lesson+$total_days_pm_skip_lesson)/$total_days * 100)).'</td></tr>';
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr style="padding-top:16px;">';
			$x .= '<td colspan="4" style="padding-top:16px;">';
				$x .= '<div style="font-weight:bold;padding-bottom:8px;text-decoration:underline;">'.$Lang['LessonAttendance']['AttendanceBySubject'].'</div>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>'.$Lang['LessonAttendance']['Subject'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['NumberOfPeriod'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['Present'].'</th><th style="text-align:center;">'.$Lang['LessonAttendance']['SkipLesson'].'</th><th style="text-align:center;">%</th></tr>';
					if(count($subjectToRecords)>0)
					{
						$i = 0;
						foreach($subjectToRecords as $subject => $ary)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class="'.$row_css.'">';
								$x .= '<td>'.$subject.'</td><td style="text-align:center;">'.$ary['TotalSession'].'</td><td style="text-align:center;">'.$ary['PresentSession'].'</td><td style="text-align:center;">'.$ary['SkipLessonSession'].'</td><td style="text-align:center;">'.sprintf("%.2f%%",($ary['PresentSession']) / $ary['TotalSession'] * 100).'</td>';
							$x .= '</tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x .= '</tr>';
		
		$x .= '<tr>';
			$x .= '<td colspan="4" style="padding-top:16px;">';
				$x .= '<span style="font-weight:bold;padding-bottom:8px;text-decoration:underline;width:50%;display:inline-block;">'.$Lang['LessonAttendance']['OverviewByPeriod'].'</span>';
				$x .= '<span style="width:50%;padding-bottom:8px;display:inline-block;text-align:right;">['.$Lang['LessonAttendance']['Absent'].'] ['.$Lang['LessonAttendance']['Late'].'] ['.$Lang['LessonAttendance']['SkipLesson'].']</span>';
				$x .= '<table class="common_table_list_v30">';
					$x .= '<tr><th>'.$Lang['LessonAttendance']['Period'].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][1].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][2].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][3].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][4].'</th>
								<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][5].'</th>
							</tr>';
					if(count($periodDateToRecords)>0)
					{
						$i = 0;
						foreach($periodDateToRecords as $period => $ary1)
						{
							$row_css = ($i % 2 == 0)? 'row_on':'row_off';
							$x .= '<tr class='.$row_css.'>';
							$x .= '<td>'.$period.'</td>';
							for($w=1;$w<=5;$w++)
							{
								$x .= '<td style="text-align:center;">';
								$map = array('AbsentSession'=>0,'LateSession'=>0,'SkipLessonSession'=>0);
								foreach($ary1 as $date => $ary2)
								{
									$weekday = date("w",strtotime($date));
									if($weekday == $w)
									{
										$map['AbsentSession'] += $ary2['AbsentSession'];
										$map['LateSession'] += $ary2['LateSession'];
										$map['SkipLessonSession'] += $ary2['SkipLessonSession'];
									}
								}
									$x .= $map['AbsentSession'].','.$map['LateSession'].','.$map['SkipLessonSession'];
								$x .= '</td>';
							}
							$x .= '</tr>';
							$i++;
						}
					}
				$x .= '</table>';
			$x .= '</td>';
		$x.= '</tr>';
		
	$x .= '</table>'."\n";
	*/
	
	if($Format == 'print')
	{
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		echo $x;
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}
	
	if(in_array($Format,array("","web")))
	{
		$linterface->LAYOUT_START();
		echo $x;
		$linterface->LAYOUT_STOP();
	}
	
	if($Format == 'pdf')
	{
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		
		$css_file_path = $intranet_root.'/templates/2009a/css/content_30.css';
		$css_content = "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>\n";
		$css_file_path = $intranet_root.'/templates/2009a/css/print_25.css';
		$css_content .= "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>\n";
		
		//$x = $css_content.$x;
		
		$margin= 12.7; // mm
		$margin_top = $margin;
		$margin_bottom = $margin;
		$margin_left = $margin;
		$margin_right = $margin;
		$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
		
		//pdf metadata setting
		$pdf->SetTitle($report_title);
		$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
		$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
		$pdf->SetSubject($report_title);
		$pdf->SetKeywords($report_title);
		
		// Chinese use mingliu 
		$pdf->backupSubsFont = array('mingliu');
		$pdf->useSubstitutions = true;
		
		$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">{PAGENO} / {nbpg}</div>');
		$pdf->SetHTMLFooterByName('htmlFooter');
		
		for($i=0;$i<count($TargetID);$i++){
			
			$x = $css_content.getReportContent($TargetID[$i]);
			
			$pos = 0;
			$len = mb_strlen($x);
			$perlen = 100000;
			for($p=$pos;$p<$len;$p+=$perlen)
			{
				$y = mb_substr($x, $p, $perlen);
				$pdf->WriteHTML($y);
			}
			
			$pdf->WriteHTML('<pagebreak resetpagenum="1" pagenumstyle="1" suppress="off" />');
		}
		//$pdf->Output('single_student_summary_report('.$luser->StandardDisplayName.' '.$luser->ClassName.'-'.$luser->ClassNumber.').pdf', 'I');
		$pdf->Output('single_student_summary_report.pdf', 'I');
	}
}



intranet_closedb();
?>