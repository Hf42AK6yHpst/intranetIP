<?php
// using 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = $_REQUEST['TargetDate'];
$TargetEndDate = $_REQUEST['TargetEndDate'];
$AttendStatus = $_REQUEST['AttendStatus'];

$LessonAttendUI = new libstudentattendance_ui();

if($TargetEndDate > $TargetDate) {
	$LessonAttendUI->Class_Lesson_Daily_Report_Export($TargetDate,$TargetEndDate,$AttendStatus);
}else{
	echo $LessonAttendUI->Class_Lesson_Daily_Report($TargetDate,$AttendStatus);
}

intranet_closedb();
?>