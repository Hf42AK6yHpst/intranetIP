<?php

// Editing by 
/*
 * 2016-08-29 (Henry HM): Create file.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$start_date = $_POST['StartDate'];
$end_date = $_POST['EndDate'];
$type = $_POST['TargetType'];
$array_type_id = $_POST['TargetID'];
$format = $_POST['Format']; // ""/"web", "csv", "pdf", "print"

if (!$sys_custom['eClassApp']['HKUSPH'] || !in_array($Format,array("","web","csv","pdf","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$db = new libHKUFlu_db();
$ui = new interface_html();
$hkuFlu = new HKUFlu();

$rows_full_sick_leave = $db->getFullSickLeaveBySickLeave($db->getSickLeaveReportByTypeID($type, $array_type_id, $start_date, $end_date));
$record_size = count($rows_full_sick_leave);

$array_html = array();
	

$html = '';

$html.= '<table class="eSporttableborder common_table_list_v30">'."\n";
		$html.='<thead>';
		$html.= '<tr>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:5%;" class="num_check" width="1">#</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:10%;">'.$Lang['HKUSPH']['LeaveDate'].'</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:10%;">'.$Lang['HKUSPH']['SickDate'].'</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:10%;">'.$Lang['StudentAttendance']['Class'].'</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:5%;">'.$Lang['StudentAttendance']['ClassNumber'].'</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:14%;">'.$Lang['Identity']['Student'].'</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">'.$Lang['HKUSPH']['Sick'] .'</th>';
		$html.='<th class="eSporttdborder eSportprinttabletitle" style="width:26%;">'.$Lang['HKUSPH']['Symptom'].'</th>';
		$html.= '</tr>'."\n";
		$html.='</thead>';
		$html.='<tbody>';
		array_push($array_html, $html);
		$html = '';
		
	if($record_size == 0){
		$html.='<tr>';
			$html.='<td colspan="8" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$html.='</tr>'."\n";
		array_push($array_html, $html);
		$html = '';
	}
	
	$i = 0;
	foreach($rows_full_sick_leave as $row_full_sick_leave){
		$row_sick_leave = $row_full_sick_leave['sick_leave'];
		
		$sick_string = '';
		$symptom_string = '';
		
		foreach((array)$row_full_sick_leave['sick_leave_sick'] as $row_sick){
			$sick_string .= ($sick_string==''?'':', ') . $Lang['HKUSPH']['array_sick'][$hkuFlu->getIndexFromSickCode($row_sick['SickCode'])] . $row_sick['Other'];
		}
		
		foreach((array)$row_full_sick_leave['sick_leave_symptom'] as $row_symptom){
			$symptom_string .= ($symptom_string==''?'':', ') . $Lang['HKUSPH']['array_symptom'][$hkuFlu->getIndexFromSymptomCode($row_symptom['SymptomCode'])] . $row_symptom['Other'];
		}
		
		$html .= '<tr>';
			$html.='<td class="eSporttdborder eSportprinttext">'.($i+1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display(substr($row_sick_leave['LeaveDate'],0,10),1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display(substr($row_sick_leave['SickDate'],0,10),1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($row_sick_leave['ClassName'],1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($row_sick_leave['ClassNumber'],1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display(Get_Lang_Selection($row_sick_leave['ChineseName'],$row_sick_leave['EnglishName']),1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($sick_string,1).'</td>';
			$html.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($symptom_string,1).'</td>';
		$html .= '</tr>'."\n";
		array_push($array_html, $html);
		$html = '';
		
		$i++;
	}
	
	$html .= '</tbody>';
$html .= '</table>'."\n";

array_push($array_html, $html);
$html = '';
	
if(in_array($format,array("","web"))){
	$html = '';
	$html .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
	$html .= '<tr>' . "\n";
	$html .= '<td>' . "\n";
	$html .= '<div class="content_top_tool">' . "\n";
	$html .= '<div class="Conntent_tool">' . "\n";
	$html .= $ui->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
	$html .= $ui->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
	$html .= $ui->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
	$html .= '</div>' . "\n";
	$html .= '<br style="clear: both;">' . "\n";
	$html .= '</div>' . "\n";
	$html .= '</td>' . "\n";
	$html .= '</tr>' . "\n";
	$html .= '</table>' . "\n";
	echo $html;
	
	foreach((array)$array_html as $html){
		echo $html;
	}
	
}elseif($format == "print"){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	$html .= '<table width="98%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$ui->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
			</tr>
		</table>';
	echo $html;
	
	foreach((array)$array_html as $html){
		echo $html;
	}
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	
}elseif($format == 'pdf'){
	require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	
	$css_file_path = $intranet_root.'/templates/'.$LAYOUT_SKIN.'/css/print_25.css';
	$css_content = "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>";
	
	$margin= 12.7; // mm
	$margin_top = $margin;
	$margin_bottom = $margin;
	$margin_left = $margin;
	$margin_right = $margin;
	$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
	
	//pdf metadata setting
	$pdf->SetTitle($Lang['HKUSPH']['HKUSPH Report']);
	$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
	$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
	$pdf->SetSubject($Lang['HKUSPH']['HKUSPH Report']);
	$pdf->SetKeywords($Lang['HKUSPH']['HKUSPH Report']);
	
	// Chinese use mingliu 
	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = true;
	
	$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">{PAGENO} / {nbpg}</div>');
	$pdf->SetHTMLFooterByName('htmlFooter');
	
	$pdf->WriteHTML($css_content);
	foreach((array)$array_html as $html){
		$pdf->WriteHTML($html);
	}
	$pdf->Output('late_skip_lesson_records.pdf', 'I');
	
}elseif($format == "csv"){
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$rows = array();
	$row = array();
	$headers = array();

	$headers[] = $Lang['StudentAttendance']['RecordDate'];
	$headers[] = $Lang['StudentAttendance']['Class'];
	$headers[] = $Lang['StudentAttendance']['ClassNumber'];
	$headers[] = $Lang['Identity']['Student'];
	$headers[] = $Lang['HKUSPH']['Sick'];
	$headers[] = $Lang['HKUSPH']['Symptom'];
	
	for($i=0;$i<$record_size;$i++){
	}
	foreach($rows_full_sick_leave as $row_full_sick_leave){
		$row_sick_leave = $row_full_sick_leave['sick_leave'];
		
		$sick_string = '';
		$symptom_string = '';
		
		foreach((array)$row_full_sick_leave['sick_leave_sick'] as $row_sick){
			$sick_string .= ($sick_string==''?'':', ') . $Lang['HKUSPH']['array_sick'][$hkuFlu->getIndexFromSickCode($row_sick['SickCode'])] . $row_sick['Other'];
		}
		
		foreach((array)$row_full_sick_leave['sick_leave_symptom'] as $row_symptom){
			$symptom_string .= ($symptom_string==''?'':', ') . $Lang['HKUSPH']['array_symptom'][$hkuFlu->getIndexFromSymptomCode($row_symptom['SymptomCode'])] . $row_symptom['Other'];
		}
		
		
		unset($row);
		$row[] = substr($row_sick_leave['LeaveDate'],0,10);
		$row[] = $row_sick_leave['ClassName'];
		$row[] = $row_sick_leave['ClassNumber'];
		$row[] = Get_Lang_Selection($row_sick_leave['ChineseName'],$row_sick_leave['EnglishName']);
		$row[] = $sick_string;
		$row[] = $symptom_string;
		
		$rows[] = $row;
	}
	
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $headers);
	$lexport->EXPORT_FILE('hkusph.csv', $exportContent);
}

intranet_closedb();
?>