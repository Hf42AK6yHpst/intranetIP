<?php
// Editing by 
/*
 * 2016-06-06 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$TargetType = $_POST['TargetType'];
$TargetID = $_POST['TargetID'];
$TeacherID = $_POST['TeacherID'];
$SubjectID = $_POST['SubjectID'];
$ViewType = $_POST['ViewType']; // 1:Late, 2:Skip Lesson
$Format = $_POST['Format']; // ""/"web", "csv", "pdf", "print"

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"] || !in_array($ViewType,array(1,2)) || !in_array($Format,array("","web","csv","pdf","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$academic_year_id = $LessonAttendUI->CurAcademicYearID;

$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();

$target_result = $LessonAttendUI->GetTargetSelectionResult($TargetType, $TargetID);
$targetStudentIdAry = $target_result[0];

$student_name_field = getNameFieldByLang2("u1.");
$teacher_name_field = getNameFieldByLang("u2.");

$subject_name = Get_Lang_Selection("s.CH_DES","s.EN_DES");

$sql = "SELECT 
			sgsa.AttendOverviewID,
			sgao.LessonDate,
			sgao.StartTime,
			sgsa.StudentID,
			sgsa.AttendStatus,
			st.SubjectID,
			$subject_name as Subject,
			u1.ClassName,
			u1.ClassNumber,
			$student_name_field as StudentName,
			$teacher_name_field as TeacherName ";
//if($ViewType == 1)
//{
//	$sql .= ",COUNT(*) as LessonSession ";
//}
$sql.=" FROM 
		SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
		INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
		INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
		INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao.SubjectGroupID=stc.SubjectGroupID 
		INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
		INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
		LEFT JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON stct.SubjectGroupID=st.SubjectGroupID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=stct.UserID 
		WHERE (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate') AND sgsa.StudentID IN (".implode(",",$targetStudentIdAry).") 
		";

if(trim($TeacherID) != '')
{
	$sql .= " AND stct.UserID='$TeacherID' ";
}

if(trim($SubjectID) != '')
{
	$sql .= " AND s.RecordID='$SubjectID' ";
}
if($ViewType == 1){
	$lateStatusAry = array();
	if(isset($statusMap['Late'])){
		$lateStatusAry[] = $statusMap['Late']['code'];
	}
	if(isset($statusMap['LateForSplitClass']))
	{
		$lateStatusAry[] = $statusMap['LateForSplitClass']['code'];
	}
	$sql .= " AND sgsa.AttendStatus IN (".implode(",",$lateStatusAry).") ";
	//$sql .= " GROUP BY sgsa.AttendOverviewID,sgao.LessonDate,sgsa.StudentID,st.SubjectID ";
	//$sql .= " ORDER BY sgao.LessonDate,sgsa.StudentID,sgao.StartTime ";
	$sql .= " GROUP BY sgsa.AttendOverviewID,sgsa.StudentID ";
	$sql .= " ORDER BY sgao.LessonDate,sgsa.StudentID,sgao.StartTime ";
	
	$tmp_records = $LessonAttendUI->returnResultSet($sql);
	$tmp_record_size = count($tmp_records);
	
	$records = array();
	$keyToRecords = array();
	for($i=0;$i<$tmp_record_size;$i++)
	{
		$date = $tmp_records[$i]['LessonDate'];
		$student_id = $tmp_records[$i]['StudentID'];
		$attend_status  = $tmp_records[$i]['AttendStatus'];
		$subject_id = $tmp_records[$i]['SubjectID'];
		if(!isset($keyToRecords[$date])){
			$keyToRecords[$date] = array();
		}
		if(!isset($keyToRecords[$date][$student_id])){
			$keyToRecords[$date][$student_id] = array();
			$keyToRecords[$date][$student_id]['Lessons'] = array();
		}
		$keyToRecords[$date][$student_id]['Lessons'][] = $tmp_records[$i];
		if(in_array($attend_status,$lateStatusAry))
		{
			$lesson_index = count($keyToRecords[$date][$student_id]['Lessons']);
			$keyToRecords[$date][$student_id]['Lessons'][$lesson_index-1]['LessonSession'] = $lesson_index;
		}
	}
	
	foreach($keyToRecords as $date => $ary1)
	{
		foreach($ary1 as $student_id => $lesson_ary)
		{
			for($j=0;$j<count($lesson_ary['Lessons']);$j++)
			{
				$records[] = $lesson_ary['Lessons'][$j];
			}
		}
	}
	
	$record_size = count($records);
	
}else if($ViewType == 2){
	
	$sql .= " GROUP BY sgsa.AttendOverviewID,sgsa.StudentID ";
	$sql .= " ORDER BY sgao.LessonDate,sgsa.StudentID,sgao.StartTime ";

	$tmp_records = $LessonAttendUI->returnResultSet($sql);
	$absent_code = $statusMap['Absent']['code'];
	
	$records = array();
	$keyToRecords = array();
	for($i=0;$i<count($tmp_records);$i++)
	{
		$date = $tmp_records[$i]['LessonDate'];
		$student_id = $tmp_records[$i]['StudentID'];
		$attend_status  = $tmp_records[$i]['AttendStatus'];
		$subject_id = $tmp_records[$i]['SubjectID'];
		if(!isset($keyToRecords[$date])){
			$keyToRecords[$date] = array();
		}
		if(!isset($keyToRecords[$date][$student_id])){
			$keyToRecords[$date][$student_id] = array();
			$keyToRecords[$date][$student_id]['Lessons'] = array();
		}
		$keyToRecords[$date][$student_id]['Lessons'][] = $tmp_records[$i];
		if(!isset($keyToRecords[$date][$student_id][$subject_id])){
			$keyToRecords[$date][$student_id][$subject_id] = array();
			$keyToRecords[$date][$student_id][$subject_id]['Records'] = array();
		}
		
		$keyToRecords[$date][$student_id][$subject_id]['Records'][] = $tmp_records[$i];
		$keyToRecords[$date][$student_id][$subject_id]['Records'][count($keyToRecords[$date][$student_id][$subject_id]['Records'])-1]['LessonIndex'] = count($keyToRecords[$date][$student_id]['Lessons']);
	}
	foreach($keyToRecords as $date => $ary1)
	{
		foreach($ary1 as $student_id => $ary2)
		{
			foreach($ary2 as $subject_id => $ary3)
			{
				// skip lesson means for the same subject lessons, some lessons are absent, but not all lessons are absent 
				/*
				if($ary3['LessonSession'] > 0 && $ary3['LessonSession'] < count($ary3['Records']))
				{
					$record = $ary3['Records'][0];
					$record['LessonSession'] = $ary3['LessonSession'];
					$records[] = $record;
				}
				*/
				$status_ary = Get_Array_By_Key($ary3['Records'],'AttendStatus');
				$status_ary_size = count($status_ary);
				$num_of_absent = 0;
				for($i=0;$i<$status_ary_size;$i++){
					if($status_ary[$i] == $absent_code){
						$num_of_absent += 1;
					}
				}
				if($status_ary_size>0 && $num_of_absent > 1 && $num_of_absent < $status_ary_size){
					for($i=0;$i<count($ary3['Records']);$i++)
					{
						if($ary3['Records'][$i]['AttendStatus'] == $absent_code)
						{
							$record = $ary3['Records'][$i];
							$record['LessonSession'] = $keyToRecords[$date][$student_id][$subject_id]['Records'][$i]['LessonIndex'];
							$records[] = $record;
						}
					}
				}
			}
		}
	}
	$record_size = count($records);
}

$x = '';

if(in_array($Format,array("","web")))
{
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
				$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	
	$x.= '<table class="common_table_list_v30">'."\n";
			$x.='<thead>';
			$x.= '<tr>';
				$x.='<th class="num_check" width="1">#</th>';
				$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['RecordDate'].'</th>';
				$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['Class'].'</th>';
				$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['ClassNumber'].'</th>';
				$x.='<th style="width:15%;">'.$Lang['Identity']['Student'].'</th>';
				$x.='<th style="width:10%;">'.$Lang['LessonAttendance']['LessonSession'] .'</th>';
				$x.='<th style="width:15%;">'.$Lang['LessonAttendance']['Subject'].'</th>';
				$x.='<th style="width:15%;">'.$Lang['General']['Teacher'].'</th>';
			$x.= '</tr>'."\n";
			$x.='</thead>';
			$x.='<tbody>';
			
		if($record_size==0){
			$x.='<tr>';
				$x.='<td colspan="8" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
			$x.='</tr>'."\n";
		}else{
			for($i=0;$i<$record_size;$i++)
			{
				$x .= '<tr>';
					$x.='<td>'.($i+1).'</td>';
					$x.='<td>'.Get_String_Display($records[$i]['LessonDate'],1).'</td>';
					$x.='<td>'.Get_String_Display($records[$i]['ClassName'],1).'</td>';
					$x.='<td>'.Get_String_Display($records[$i]['ClassNumber'],1).'</td>';
					$x.='<td>'.Get_String_Display($records[$i]['StudentName'],1).'</td>';
					$x.='<td>'.$records[$i]['LessonSession'].'</td>';
					$x.='<td>'.Get_String_Display($records[$i]['Subject'],1).'</td>';
					$x.='<td>'.Get_String_Display($records[$i]['TeacherName'],1).'</td>';
				$x .= '</tr>'."\n";
			}
		}
		$x .= '</tbody>';
	$x .= '</table>'."\n";
	
	echo $x;
}

if($Format == "print" || $Format == "pdf")
{	
	$x = '';
	if($Format == "print")
	{
	$x .= '<table width="98%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>';
	}
	$x .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" align="center">
			 <tr>
				<td align="center"><h2>'.$Lang['LessonAttendance']['LateSkipLessonRecords'].'</h2></td>
			 </tr>
		   </table>';	   
	$x .= '<table align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="98%">
		  	<tbody>
		  	<tr class="tabletop">
				<th class="eSporttdborder eSportprinttabletitle" style="width:1;">#</th>
			    <th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['RecordDate'].'</th>
			    <th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['Class'].'</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['ClassNumber'].'</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['Identity']['Student'].'</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:10%;">'.$Lang['LessonAttendance']['LessonSession'].'</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['LessonAttendance']['Subject'].'</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['General']['Teacher'].'</th>
		    </tr>';
		
		if($record_size == 0){
			$x.='<tr>';
				$x.='<td class="eSporttdborder eSportprinttext" colspan="8" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
			$x.='</tr>'."\n";
		}
		for($i=0;$i<$record_size;$i++){
		
			$x .= '<tr>';
				$x.='<td class="eSporttdborder eSportprinttext">'.($i+1).'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['LessonDate'],1).'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['ClassName'],1).'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['ClassNumber'],1).'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['StudentName'],1).'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.$records[$i]['LessonSession'].'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['Subject'],1).'</td>';
				$x.='<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['TeacherName'],1).'</td>';
			$x .= '</tr>'."\n";
		}
		
		$x .= '</tbody>';
	$x .= '</table>';
	$x .= '<br />';
	
	if($Format == 'print')
	{
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		echo $x;
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}
	
	if($Format == 'pdf')
	{
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		
		$css_file_path = $intranet_root.'/templates/2009a/css/print_25.css';
		$css_content = "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>";
		
		$x = $css_content.$x;
		
		$margin= 12.7; // mm
		$margin_top = $margin;
		$margin_bottom = $margin;
		$margin_left = $margin;
		$margin_right = $margin;
		$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
		
		//pdf metadata setting
		$pdf->SetTitle($Lang['LessonAttendance']['LateSkipLessonRecords']);
		$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
		$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
		$pdf->SetSubject($Lang['LessonAttendance']['LateSkipLessonRecords']);
		$pdf->SetKeywords($Lang['LessonAttendance']['LateSkipLessonRecords']);
		
		// Chinese use mingliu 
		$pdf->backupSubsFont = array('mingliu');
		$pdf->useSubstitutions = true;
		
		$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">{PAGENO} / {nbpg}</div>');
		$pdf->SetHTMLFooterByName('htmlFooter');
		
		$pos = 0;
		$len = mb_strlen($x);
		$perlen = 100000;
		for($p=$pos;$p<$len;$p+=$perlen)
		{
			$y = mb_substr($x, $p, $perlen);
			$pdf->WriteHTML($y);
		}
		$pdf->Output('late_skip_lesson_records.pdf', 'I');
	}
}

if($Format == "csv")
{
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$rows = array();
	$row = array();
	$headers = array();
	
	$headers[] = $Lang['StudentAttendance']['RecordDate'];
	$headers[] = $Lang['StudentAttendance']['Class'];
	$headers[] = $Lang['StudentAttendance']['ClassNumber'];
	$headers[] = $Lang['Identity']['Student'];
	$headers[] = $Lang['LessonAttendance']['LessonSession'];
	$headers[] = $Lang['LessonAttendance']['Subject'];
	$headers[] = $Lang['General']['Teacher'];
	
	for($i=0;$i<$record_size;$i++){
		
		unset($row);
		$row[] = $records[$i]['LessonDate'];
		$row[] = $records[$i]['ClassName'];
		$row[] = $records[$i]['ClassNumber'];
		$row[] = $records[$i]['StudentName'];
		$row[] = $records[$i]['LessonSession'];
		$row[] = $records[$i]['Subject'];
		$row[] = $records[$i]['TeacherName'];
		
		$rows[] = $row;
	}
	
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $headers);
	$lexport->EXPORT_FILE('late_skip_lesson_records.csv', $exportContent);
}

intranet_closedb();
?>