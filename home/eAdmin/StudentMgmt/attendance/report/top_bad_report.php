<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();

if (!$lc->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

switch ($BadType)
{
        case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
        case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
        case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
        case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
        case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
}


$i_title = $i_StudentAttendance_Report_StudentBadRecords." ($nav_title - $i_StudentAttendance_Top $TopNumber $i_StudentAttendance_Top_student_suffix)";

$lc->retrieveSettings();

$ts = strtotime($StartDate);
if ($ts==-1 || $StartDate =="")
{
    $StartDate = date('Y-m-d');
}
$ts = strtotime($EndDate);
if ($ts==-1 || $EndDate =="")
{
    $EndDate = date('Y-m-d');
}

$result = $lc->retrieveTopBadRecordsCountByStudent($StartDate, $EndDate, $BadType, $TopNumber);

$display = "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$display .= "<tr class=\"tablebluetop\">";
$display .= "<td class=\"tabletoplink\">#</td>";
$display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>\n";
$display .= "<td class=\"tabletoplink\">$i_SmartCard_ClassName</td>\n";
$display .= "<td class=\"tabletoplink\">$i_ClassNameNumber</td>\n";
$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
$display .= "</tr>\n";

$curr_pos = 1;
$last_count = 0;

if (sizeof($result)==0) {
	$display .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
} else {
	for ($i=0; $i<sizeof($result); $i++)
	{
	     list($studentid,$student_name,$student_class, $student_classnum, $count) = $result[$i];
	     if ($count != $last_count)
	     {
	         if ($i==0)
	         {
	         }
	         else
	         {
	             $curr_pos = $i+1;
	         }
	     }
	     else
	     {
	         $curr_pos = "&nbsp;";
	     }
	     $last_count = $count;
	     $css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
	     $display .= "<tr class=\"$css\">";
	     $display .= "<td class=\"tabletext\">$curr_pos</td>";
	     $display .= "<td class=\"tabletext\">$student_name</td>";
	     $display .= "<td class=\"tabletext\">$student_class</td>";
	     $display .= "<td class=\"tabletext\">$student_classnum</td>";
	     $display .= "<td class=\"tabletext\">$count</td></tr>\n";
	}
}
$display .= "</table>\n";

$MODULE_OBJ['title'] = $i_title;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<br />
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_startdate ?></td>
		<td width="70%" class="tabletext"><?= $StartDate ?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_enddate ?></td>
		<td width="70%" class="tabletext"><?= $EndDate ?></td>
	</tr>
</table>
<br />
<?=$display?>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
