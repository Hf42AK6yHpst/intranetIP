<?php
// editing by : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();
/*
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
*/
//$debug = true;
$lcardattend = new libcardstudentattend2();

$lcardattend->retrieveSettings();
//set_time_limit(0);

$lcardattend->Start_Trans();
$Result = array();
if ($plugin['Disciplinev12'])
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
  	$ldisciplinev12 = new libdisciplinev12();
  	$LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
  
  	## Only For IP25 eDis1.2 Only ##
  	if($sys_custom['Discipline_SeriousLate'])
  	{	
	    $sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType = -1 AND LateMinutes IS NOT NULL";
	    $targetCatID = $ldisciplinev12->returnVector($sql);
	    if(sizeof($targetCatID)>0)
	    {
	    	for($i=0; $i<sizeof($targetCatID); $i++)
	    	{
	    		$result = $ldisciplinev12->CategoryInUsed($targetCatID[$i]);
	    		if($result){
	    			$cnt++;
	    		}
	    	}
	    }
	    if($cnt > 0){
	    	$SeriousLateUsed = 1;
	    }
	}
}

$confirmed_user_id = $_SESSION['UserID'];

# Get Student data
# ClassName, ClassNumber -> UserID
$sql = "SELECT UserID, ClassName, ClassNumber 
		FROM INTRANET_USER
        WHERE RecordType = 2 AND RecordStatus IN (0,1)
        	  AND ClassName != '' AND ClassNumber != '' ";
$Students = $lcardattend->returnArray($sql,3);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($targetUserID, $targetClass, $targetNum) = $Students[$i];
     $students_id[$targetClass][$targetNum] = $targetUserID;
}
#Get Classes data
$Classes = $lcardattend->getClassList();


$tsStartDate = strtotime($StartDate);
$tsEndDate = strtotime($EndDate);
$tsOneDay = 24*60*60;
/*
// Get Years and Months in the current semester
//$TermStartDate = date('Y-m-d',getStartOfAcademicYear(""));
//$TermEndDate = date('Y-m-d',getEndOfAcademicYear(""));
$TodayDate = date('Y-m-d');
//$TermEndDate = ($TodayDate>$TermEndDate)?$TermEndDate:$TodayDate;
$TermStartDateArr = explode("-", $StartDate);
$TermEndDateArr = explode("-", $EndDate);
$StartYear = $TermStartDateArr[0];
$StartMonth = $TermStartDateArr[1];
$StartDay = $TermStartDateArr[2];
$EndYear = $TermEndDateArr[0];
$EndMonth = $TermEndDateArr[1];
$EndDay = $TermEndDateArr[2];
$YearArr = array();// start year to end year
$MonthArr = array();// months of start year to months of end year [assoc array using numeric year as key]
for($y = intval($StartYear);$y<=intval($EndYear);$y++)
{
	$YearArr[] = $y;
	$MonthArr[$y] = array();
}
for($i=0;$i<sizeof($YearArr);$i++)
{
	if($i==0)//first year
	{
		for($m=intval($StartMonth);$m<=12;$m++)
		{
			$MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
		}
	}else if($i==(sizeof($YearArr)-1))//last year
	{
		for($m=1;$m<=$EndMonth;$m++)
		{
			$MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
		}
	}else// middle years
	{
		for($m=1;$m<=12;$m++)
		{
			$MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
		}
	}
}
*/
# Get non-School days
$sql = "SELECT DISTINCT SUBSTRING(CONCAT(EventDate),1,10) as EventDate 
		FROM INTRANET_EVENT WHERE RecordType IN (3,4) AND EventDate BETWEEN '$StartDate' AND '$EndDate' ";
$SchoolHolidaysResult = $lcardattend->returnArray($sql);
$SchoolHolidays = array();
for($i=0;$i<sizeof($SchoolHolidaysResult);$i++)
{
	$SchoolHolidays[$SchoolHolidaysResult[$i]['EventDate']] = $SchoolHolidaysResult[$i]['EventDate'];
}

// Insert all students daily log records in this semester day by day
/*
for($i=0;$i<sizeof($YearArr);$i++)
{
	$txt_year = $YearArr[$i];
	for($j=0;$j<sizeof($MonthArr[$txt_year]);$j++)
	{
		$txt_month = $MonthArr[$txt_year][$j];
		$ts = mktime(0,0,0,intval($txt_month), 1, intval($txt_year));
		$totaldays = date('t',$ts);
		//$lcardattend->createTable_Card_Student_Daily_Log($txt_year, $txt_month);
		//$lcardattend->createTable_Card_Student_Daily_Class_Confirm($txt_year, $txt_month);
		$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
		$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
		for($d=1;$d<=$totaldays;$d++)
		{
			$txt_day = ($d)<10?'0'.$d:$d;
			$datestr = $txt_year.'-'.$txt_month.'-'.$txt_day;
			$weekday = date('w',strtotime($datestr));
			if($weekday == 0) // Skip Sundays
				continue;
			// Check other non-school days here to skip
			if($SchoolHolidays[$datestr])
				continue;
			if($txt_year == $StartYear && $txt_month == $StartMonth && $txt_day<$StartDay)
				continue;
			if($txt_year == $EndYear && $txt_month == $EndMonth && $txt_day>$EndDay)
				continue;
			for($s=0;$s<sizeof($Students);$s++)
			{
				list($TargetUserID, $Class, $ClassNumber) = $Students[$s];
				$sql = "INSERT INTO $card_log_table_name
				           	(UserID,DayNumber,AMStatus,PMStatus,ConfirmedUserID,IsConfirmed,DateInput,DateModified)
							VALUES ('$TargetUserID','$d','".CARD_STATUS_PRESENT."','".CARD_STATUS_PRESENT."','$confirmed_user_id','1',NOW(),NOW())
						ON DUPLICATE KEY UPDATE
							AMStatus='".CARD_STATUS_PRESENT."',PMStatus='".CARD_STATUS_PRESENT."',ConfirmedUserID='$confirmed_user_id',IsConfirmed='1',DateModified=NOW()";
				$Result[$i.'-'.$j.'-'.$d.'-'.$s.'-001'] = $lcardattend->db_db_query($sql);
			}
			
			for($c=0;$c<sizeof($Classes);$c++)
			{
				// Process Class Confirm
			    $class_id = $Classes[$c]['ClassID'];
				// Confirm AM
			    $sql = "INSERT INTO $card_student_daily_class_confirm
			            (ClassID,ConfirmedUserID,DayNumber,DayType,DateInput,DateModified) 
						VALUES ('$class_id','$confirmed_user_id','$d','".PROFILE_DAY_TYPE_AM."',NOW(), NOW() )
			            ON DUPLICATE KEY UPDATE 
			            	ConfirmedUserID=$confirmed_user_id, DateModified = NOW() ";
			    $Result[$i.'-'.$j.'-'.$d.'-'.$c.'-002'] = $lcardattend->db_db_query($sql);
			    // Confirm PM
			    $sql = "INSERT INTO $card_student_daily_class_confirm
			            (ClassID,ConfirmedUserID,DayNumber,DayType,DateInput,DateModified)
	  					VALUES ('$class_id','$confirmed_user_id','$d','".PROFILE_DAY_TYPE_PM."',NOW(),NOW() )
			            ON DUPLICATE KEY UPDATE 
			            	ConfirmedUserID=$confirmed_user_id, DateModified = NOW() ";
			    $Result[$i.'-'.$j.'-'.$d.'-'.$c.'-003'] = $lcardattend->db_db_query($sql);
			}
		}
	}
}
*/
for($tsCurrent = $tsStartDate;$tsCurrent<=$tsEndDate;$tsCurrent+=$tsOneDay)
{
	$datestr = date("Y-m-d", $tsCurrent);
	$CurrentDateArr = explode("-",$datestr);
	$txt_year = $CurrentDateArr[0];
	$txt_month = $CurrentDateArr[1];
	$txt_day = $CurrentDateArr[2];
	
	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
	$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
	
	$weekday = date('w',$tsCurrent);
	if($weekday == 0) // Skip Sundays
		continue;
	// Check other non-school days here to skip
	if($SchoolHolidays[$datestr])
		continue;
	
	$d = intval($txt_day);
	for($s=0;$s<sizeof($Students);$s++)
	{
		list($TargetUserID, $Class, $ClassNumber) = $Students[$s];
		$sql = "INSERT INTO $card_log_table_name
		           	(UserID,DayNumber,AMStatus,PMStatus,ConfirmedUserID,IsConfirmed,DateInput,DateModified)
					VALUES ('$TargetUserID','$d','".CARD_STATUS_PRESENT."','".CARD_STATUS_PRESENT."','$confirmed_user_id','1',NOW(),NOW())
				ON DUPLICATE KEY UPDATE
					AMStatus='".CARD_STATUS_PRESENT."',PMStatus='".CARD_STATUS_PRESENT."',ConfirmedUserID='$confirmed_user_id',IsConfirmed='1',DateModified=NOW()";
		$Result[$datestr.'-'.$s.'-001'] = $lcardattend->db_db_query($sql);
	}
	
	for($c=0;$c<sizeof($Classes);$c++)
	{
		// Process Class Confirm
	    $class_id = $Classes[$c]['ClassID'];
		// Confirm AM
	    $sql = "INSERT INTO $card_student_daily_class_confirm
	            (ClassID,ConfirmedUserID,DayNumber,DayType,DateInput,DateModified) 
				VALUES ('$class_id','$confirmed_user_id','$d','".PROFILE_DAY_TYPE_AM."',NOW(), NOW() )
	            ON DUPLICATE KEY UPDATE 
	            	ConfirmedUserID=$confirmed_user_id, DateModified = NOW() ";
	    $Result[$datestr.'-'.$c.'-002'] = $lcardattend->db_db_query($sql);
	    // Confirm PM
	    $sql = "INSERT INTO $card_student_daily_class_confirm
	            (ClassID,ConfirmedUserID,DayNumber,DayType,DateInput,DateModified)
				VALUES ('$class_id','$confirmed_user_id','$d','".PROFILE_DAY_TYPE_PM."',NOW(),NOW() )
	            ON DUPLICATE KEY UPDATE 
	            	ConfirmedUserID=$confirmed_user_id, DateModified = NOW() ";
	    $Result[$datestr.'-'.$c.'-003'] = $lcardattend->db_db_query($sql);
	}
}

// Pre-create tables CARD_STUDENT_DAILY_LOG_YYYY_MM and CARD_STUDENT_DAILY_CLASS_CONFIRM_YYYY_MM for insertion data later
$sql = "SELECT SUBSTRING(CONCAT(RecordDate),1,4) as Year, SUBSTRING(CONCAT(RecordDate),6,2) as Month 
		FROM TEMP_CARD_STUDENT_DAILY_LOG 
		GROUP BY Year, Month ";
$InvolvedYearsMonths = $lcardattend->returnArray($sql, 2);
for($i=0;$i<sizeof($InvolvedYearsMonths);$i++)
{
	$txt_year = $InvolvedYearsMonths[$i]['Year'];
	$txt_month = $InvolvedYearsMonths[$i]['Month'];
	//$lcardattend->createTable_Card_Student_Daily_Log($txt_year, $txt_month);
	//$lcardattend->createTable_Card_Student_Daily_Class_Confirm($txt_year, $txt_month);
}
// End pre-create CARD_STUDENT_DAILY_LOG_YYYY_MM and CARD_STUDENT_DAILY_CLASS_CONFIRM_YYYY_MM

// Get WebSAMS Reasons
$sql = "SELECT ReasonType, ReasonText FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE";
$WebSAMSReasonsResult = $lcardattend->returnArray($sql);
$WebSAMSReasons = array();
$WebSAMSReasons[PROFILE_TYPE_ABSENT] = array();
$WebSAMSReasons[PROFILE_TYPE_LATE] = array();
$WebSAMSReasons[PROFILE_TYPE_EARLY] = array();
$WebSAMSAddedReasons[PROFILE_TYPE_ABSENT] = array();
$WebSAMSAddedReasons[PROFILE_TYPE_LATE] = array();
$WebSAMSAddedReasons[PROFILE_TYPE_EARLY] = array();
for($i=0;$i<sizeof($WebSAMSReasonsResult);$i++)
{
	$WebSAMSReasons[$WebSAMSReasonsResult[$i]['ReasonType']][]=$WebSAMSReasonsResult[$i]['ReasonText'];
}
// End getting WebSAMS Reasons


// Process Late, Absence, Early Leave records
$sql = "SELECT 
			UserID,ClassName,ClassNumber,DayNumber,RecordDate,
			AMInStatus,AMInTime,AMInWaive,AMInReason,AMOutStatus,AMOutWaive,AMOutReason,
			PMInStatus,PMInTime,PMInWaive,PMInReason,PMOutStatus,PMOutWaive,PMOutReason
        FROM TEMP_CARD_STUDENT_DAILY_LOG";
$data = $lcardattend->returnArray($sql, 19);

for($i=0;$i<sizeof($data);$i++)
{
	list($TargetUserID,$Class,$ClassNumber,$DayNumber,$TargetDate,
          $AMInStatus,$AMInTime,$AMInWaive,$AMInReason,$AMOutStatus,$AMOutWaive,$AMOutReason,
		  $PMInStatus,$PMInTime,$PMInWaive,$PMInReason,$PMOutStatus,$PMOutWaive,$PMOutReason) = $data[$i];
	
	$ts_record = strtotime($TargetDate);
	$txt_year = date('Y',$ts_record);
	$txt_month = date('m',$ts_record);
	$txt_day = date('d',$ts_record);
	
	list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);
	
	### for student confirm
	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
	
	/*
	### Bug tracing
	if($bug_tracing['smartcard_student_attend_status']){
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		if(!is_dir($file_path."/file/log_student_attend_status")){
			$lf = new libfilesystem();
			$lf->folder_new($file_path."/file/log_student_attend_status");
		}
		$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($txt_year.$txt_month.$txt_day).".txt";
	}
	*/
	//Determine AM Status
	if($AMInStatus > 0)// Late or Absent
	{
		$AMStatus = $AMInStatus;
		$AMReason = $AMInReason;
		$AMWaive = $AMInWaive;
	}
	else // Present
	{
		$AMStatus = CARD_STATUS_PRESENT;
		$AMReason = $AMInReason;
		$AMWaive = $AMInWaive;
	}
	
	if($AMOutStatus==PROFILE_TYPE_EARLY)// Early Leave
	{
		$AMLeaveStatus = CARD_LEAVE_AM;
		$AMLeaveReason = $AMOutReason;
		$AMLeaveWaive = $AMOutWaive;
	}else
	{
		$AMLeaveStatus = CARD_LEAVE_NORMAL;
		$AMLeaveReason = $AMOutReason;
		$AMLeaveWaive = $AMOutWaive;
	}
	
	//Determine PM Status
	if($PMInStatus > 0)// Late or Absent
	{
		$PMStatus = $PMInStatus;
		$PMReason = $PMInReason;
		$PMWaive = $PMInWaive;
	}
	else // Present
	{
		$PMStatus = CARD_STATUS_PRESENT;
		$PMReason = $PMInReason;
		$PMWaive = $PMInWaive;
	}
	
	if($PMOutStatus==PROFILE_TYPE_EARLY)// Early Leave
	{
		$PMLeaveStatus = CARD_LEAVE_PM;
		$PMLeaveReason = $PMOutReason;
		$PMLeaveWaive = $PMOutWaive;
	}else
	{
		$PMLeaveStatus = CARD_LEAVE_NORMAL;
		$PMLeaveReason = $PMOutReason;
		$PMLeaveWaive = $PMOutWaive;
	}
	
	if(trim($AMInTime)=="")
	{
		$AMInTimeVal = "NULL";
	}else
	{
		$AMInTimeVal = "'$AMInTime'";
	}
	if(trim($PMInTime)=="")
	{
		$PMInTimeVal = "NULL";
	}else
	{
		$PMInTimeVal = "'$PMInTime'";
	}
	
	if($AMStatus > CARD_STATUS_PRESENT)// Need update AMStatus
	{
		if($PMStatus > CARD_STATUS_PRESENT)// And need update PMStatus
		{
			$fields = "UserID,DayNumber,InSchoolTime,AMStatus,LunchBackTime,PMStatus,ConfirmedUserID,IsConfirmed,DateInput,DateModified";
			$values = "'$TargetUserID','$DayNumber',$AMInTimeVal,'$AMStatus',$PMInTimeVal,'$PMStatus','$confirmed_user_id','1',NOW(),NOW()";
			$update_values = "InSchoolTime=$AMInTimeVal,AMStatus='$AMStatus',LunchBackTime=$PMInTimeVal,PMStatus='$PMStatus',ConfirmedUserID='$confirmed_user_id',IsConfirmed='1',DateModified=NOW()";
		}else // Only need update AMStatus
		{
			$fields = "UserID,DayNumber,InSchoolTime,AMStatus,ConfirmedUserID,IsConfirmed,DateInput,DateModified";
			$values = "'$TargetUserID','$DayNumber',$AMInTimeVal,'$AMStatus','$confirmed_user_id','1',NOW(),NOW()";
			$update_values = "InSchoolTime=$AMInTimeVal,AMStatus='$AMStatus',ConfirmedUserID='$confirmed_user_id',IsConfirmed='1',DateModified=NOW()";
		}
	}else // No need update AMStatus
	{
		if($PMStatus > CARD_STATUS_PRESENT)//Only need update PMStatus
		{
			$fields = "UserID,DayNumber,LunchBackTime,PMStatus,ConfirmedUserID,IsConfirmed,DateInput,DateModified";
			$values = "'$TargetUserID','$DayNumber',$PMInTimeVal,'$PMStatus','$confirmed_user_id','1',NOW(),NOW()";
			$update_values = "LunchBackTime=$PMInTimeVal,PMStatus='$PMStatus',ConfirmedUserID='$confirmed_user_id',IsConfirmed='1',DateModified=NOW()";
		}else // Set both AMStatus and PMStatus to Present
		{
			$fields = "UserID,DayNumber,InSchoolTime,AMStatus,LunchBackTime,PMStatus,ConfirmedUserID,IsConfirmed,DateInput,DateModified";
			$values = "'$TargetUserID','$DayNumber',$AMInTimeVal,'$AMStatus',$PMInTimeVal,'$PMStatus','$confirmed_user_id','1',NOW(),NOW()";
			$update_values = "InSchoolTime=$AMInTimeVal,AMStatus='$AMStatus',LunchBackTime=$PMInTimeVal,PMStatus='$PMStatus',ConfirmedUserID='$confirmed_user_id',IsConfirmed='1',DateModified=NOW()";
		}
	}
	
	if($AMLeaveStatus==CARD_LEAVE_AM)// Update LeaveStatus to AM LEAVE
	{
		$fields .= ",LeaveStatus";
		$values .= ",'$AMLeaveStatus'";
		$update_values .= ",LeaveStatus = '$AMLeaveStatus'";
	}else if($PMLeaveStatus==CARD_LEAVE_PM)// Update LeaveStatus to PM LEAVE
	{
		$fields .= ",LeaveStatus";
		$values .= ",'$PMLeaveStatus'";
		//if LeaveStatus is already set to AM Leave, this will override it with PM Leave
		$update_values .= ",LeaveStatus = '$PMLeaveStatus'";
	}else
	{
		$fields .= ",LeaveStatus";
		$values .= ",'".CARD_LEAVE_NORMAL."'";
		$update_values .= ",LeaveStatus = '".CARD_LEAVE_NORMAL."'";
	}
	
	$sql = "INSERT INTO $card_log_table_name
	           	($fields) VALUES ($values)
			ON DUPLICATE KEY UPDATE 
				$update_values ";
	//if($debug) echo "<PRE>";echo $sql;echo "</PRE><br>";
	$Result[$i.'-001'] = $lcardattend->db_db_query($sql);
	
	$insert_cardlog_id = $lcardattend->db_insert_id();
	/*
	## Bug Tracing
    if($bug_tracing['smartcard_student_attend_status'])
    {
      	$log_date = date('Y-m-d H:i:s');
      	$log_target_date = $TargetDate;
      	$log_student_id = $TargetUserID;
      	$log_old_status = "";
      	$log_sql = $sql;
      	$log_admin_user = $PHP_AUTH_USER;
		$log_page = 'import_update_confirm.php';
    	$log_content = get_file_content($log_filepath);
    	
    	$log_new_status = $AMStatus;
    	$log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
    	$log_new_status = $PMStatus;
    	$log_entry .= "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
    	
    	$log_content .= $log_entry;
    	write_file_content($log_content, $log_filepath);
    }
    */
    
	// Process AM Status
	# Check Bad actions
    if ($AMStatus==CARD_STATUS_PRESENT) # 0 = Present
    {
        $lcardattend->removeBadActionFakedCardAM($TargetUserID,$TargetDate);
        # Forgot to bring card
        $lcardattend->addBadActionNoCardEntrance($TargetUserID,$TargetDate,"");
    }
	else if ($AMStatus==CARD_STATUS_LATE) # 2 = Late
	{
	    $lcardattend->removeBadActionFakedCardAM($TargetUserID,$TargetDate);
	    # Forgot to bring card
	    $lcardattend->addBadActionNoCardEntrance($TargetUserID,$TargetDate,"");
	    
	    #########################################################
	    if(intval($AMWaive) == 1) // Late record is waived, no PROFILE_STUDENT_ATTENDANCE record
	    {
	    	$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE 
					WHERE UserID = '$TargetUserID' AND AttendanceDate='$TargetDate' 
						  AND RecordType='".PROFILE_TYPE_LATE."' AND DayType='".PROFILE_DAY_TYPE_AM."' ";
			$Result[$i.'-028'] = $lcardattend->db_db_query($sql);
	    	$insert_id = "NULL";
	    }else
	    {	
		    # Add late to student profile
		    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, RecordStatus, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
		    $fieldvalue = "'$TargetUserID','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','$AMWaive','".PROFILE_DAY_TYPE_AM."','".$lcardattend->Get_Safe_Sql_Query($AMReason)."',now(),now(),'$Class','$ClassNumber'";
		    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
		    				on duplicate key update 
		    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		    					DateModified = NOW() ";
		    $Result[$i.'-002'] = $lcardattend->db_db_query($sql);
		    $insert_id = $lcardattend->db_insert_id();
		    
			# Calculate upgrade items
			if ($plugin['Disciplinev12'] && $LateCtgInUsed)
			{
				$dataAry = array();
				$dataAry['StudentID'] = $TargetUserID;
				$dataAry['RecordDate'] = $TargetDate;
				$dataAry['Year'] = $school_year;
				$dataAry['YearID'] = $school_year_id;
				$dataAry['Semester'] = $semester;
				$dataAry['SemesterID'] = $semester_id;
				$dataAry['StudentAttendanceID'] = $insert_id;
				$dataAry['Remark'] = $AMInTime;
				
				if($SeriousLateUsed == 1)
				{
					if($AMInTime != "")
					{
						$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$TargetUserID);
						$school_start_time = $arr_time_table['MorningTime'];
						
						$str_late_minutes = strtotime($AMInTime) - strtotime($school_start_time);
						//$late_minutes = date("i",$str_late_minutes);
						$late_minutes = ceil($str_late_minutes/60);
						//$late_secode = date("s",$str_late_minutes);
						//debug_r($TargetDate.':'.$late_minutes.'<br>');
						$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
						$min_late_minutes = $lcardattend->returnVector($sql);		
						
						if(sizeof($min_late_minutes)>0)
						{
							/*if($late_minutes >= $min_late_minutes[0])
							{*/
								/*if($late_minutes == $min_late_minutes[0])
								{
									if($late_secode > 0)
									{
										$late_minutes = $late_minutes + 1;
										$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes = '$late_minutes'";
										
										$isValid = $lcardattend->returnVector($sql);
										if($isValid[0] == 0)
										{
											$late_minutes = $late_minutes - 1;
										}
									}
								}*/
								$dataAry['SeriousLate'] = 1;
								$dataAry['LateMinutes'] = $late_minutes;
							/*}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}*/
						}
						else
						{
							$dataAry['SeriousLate'] = 0;
						}
					}
					else
					{
						$dataAry['SeriousLate'] = 0;
					}
				}
				$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
			}
			
		    if (!$insert_id)
		    {
				$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
				          WHERE UserID = '$TargetUserID' AND AttendanceDate = '$TargetDate'
				          AND RecordType = '".PROFILE_TYPE_LATE."'
				          AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				$temp = $lcardattend->returnVector($sql);
				$insert_id = $temp[0];
		    }
	    }
		
		$sql = "SELECT RecordID FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate' AND RecordType='".PROFILE_DAY_TYPE_AM."' ";
		$ConfirmRecord = $lcardattend->returnArray($sql);
		if($ConfirmRecord[0]['RecordID'])
		{
			// Update
			$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM SET LateConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_AM."', LateConfirmTime = NOW(), DateModified = NOW() ";
		}else
		{
			// Insert New
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,LateConfirmed,RecordType,LateConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_AM."',NOW(),NOW(),NOW())";
		}
		/*
		$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,LateConfirmed,RecordType,LateConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_AM."',NOW(),NOW(),NOW())
					ON DUPLICATE KEY UPDATE
						LateConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_AM."', LateConfirmTime = NOW(), DateModified = NOW()";
    	*/
    	$Result[$i.'-003'] = $lcardattend->db_db_query($sql);
		
		if(trim($AMReason)!="")
		{
		    # Update to reason table
		    $fieldname = "RecordDate, StudentID, Reason, ProfileRecordID, RecordType, DayType, RecordStatus, DateInput, DateModified";
		    $fieldsvalues = "'$TargetDate', '$TargetUserID', '".$lcardattend->Get_Safe_Sql_Query($AMReason)."', $insert_id, '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', '$AMWaive', now(), now() ";
		    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                   VALUES ($fieldsvalues) 
		                   on duplicate key update 
		                   	RecordID = LAST_INSERT_ID(RecordID), 
		                   	DateModified = NOW() ";
		    $Result[$i.'-004'] = $lcardattend->db_db_query($sql);
		    
		    if(sizeof($WebSAMSReasons[PROFILE_TYPE_LATE])==0 || 
		       (sizeof($WebSAMSReasons[PROFILE_TYPE_LATE])>0 && !in_array($AMReason, $WebSAMSReasons[PROFILE_TYPE_LATE])))
		    {
		    	// check if the same reason was already
		    	if(sizeof($WebSAMSAddedReasons[PROFILE_TYPE_LATE])==0 || 
		       	   (sizeof($WebSAMSAddedReasons[PROFILE_TYPE_LATE])>0 && !in_array($AMReason, $WebSAMSAddedReasons[PROFILE_TYPE_LATE])))
		       	{
			    	$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (ReasonText,ReasonType,RecordType,DateInput,DateModified) 
						  VALUES ('".$lcardattend->Get_Safe_Sql_Query($AMReason)."','".PROFILE_TYPE_LATE."','0',NOW(),NOW())";
					$Result[$i.'-005'] = $lcardattend->db_db_query($sql);
					$WebSAMSAddedReasons[PROFILE_TYPE_LATE][] = $AMReason;
		       	}
		    }
		}
		
	    #########################################################
	}else if ($AMStatus==CARD_STATUS_ABSENT)// 1 = Absent
	{
	    $lcardattend->removeBadActionFakedCardAM($TargetUserID,$TargetDate); 
	    $lcardattend->removeBadActionNoCardEntrance($TargetUserID,$TargetDate);
	    // Need insert profile record and insert profile record reason if has reason
	    if(intval($AMWaive) == 1) // Absent record is waived, no PROFILE_STUDENT_ATTENDANCE record
	    {
	    	$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE 
					WHERE UserID = '$TargetUserID' AND AttendanceDate='$TargetDate' 
						  AND RecordType='".PROFILE_TYPE_ABSENT."' AND DayType='".PROFILE_DAY_TYPE_AM."' ";
			$Result[$i.'-029'] = $lcardattend->db_db_query($sql);
	    	$insert_id = "NULL";
	    }else
	    {
	    	# Add absent student profile
		    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, RecordStatus, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
		    $fieldvalue = "'$TargetUserID','$TargetDate','$school_year','$semester','".PROFILE_TYPE_ABSENT."','$AMWaive','".PROFILE_DAY_TYPE_AM."','".$lcardattend->Get_Safe_Sql_Query($AMReason)."',now(),now(),'$Class','$ClassNumber'";
		    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
		    				on duplicate key update 
		    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		    					DateModified = NOW() ";
		    $Result[$i.'-006'] = $lcardattend->db_db_query($sql);
		    $insert_id = $lcardattend->db_insert_id();
		    
		    if (!$insert_id)
		    {
				$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
				          WHERE UserID = '$TargetUserID' AND AttendanceDate = '$TargetDate'
				          AND RecordType = '".PROFILE_TYPE_ABSENT."'
				          AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				$temp = $lcardattend->returnVector($sql);
				$insert_id = $temp[0];
		    }
	    }
	    
	    $sql = "SELECT RecordID FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate' AND RecordType='".PROFILE_DAY_TYPE_AM."' ";
		$ConfirmRecord = $lcardattend->returnArray($sql);
		if($ConfirmRecord[0]['RecordID'])
		{
			// Update
			$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM SET AbsenceConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_AM."', AbsenceConfirmTime = NOW(), DateModified = NOW() ";
		}else
		{
			// Insert New
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,AbsenceConfirmed,RecordType,AbsenceConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_AM."',NOW(),NOW(),NOW())";
		}
	    /*
	    $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,AbsenceConfirmed,RecordType,AbsenceConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_AM."',NOW(),NOW(),NOW())
					ON DUPLICATE KEY UPDATE
						AbsenceConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_AM."', AbsenceConfirmTime = NOW(), DateModified = NOW()";
    	*/
    	$Result[$i.'-007'] = $lcardattend->db_db_query($sql);
	    
	    if(trim($AMReason)!="")
		{
		    # Update to reason table
		    $fieldname = "RecordDate, StudentID, Reason, ProfileRecordID, RecordType, DayType, RecordStatus, DateInput, DateModified";
		    $fieldsvalues = "'$TargetDate', '$TargetUserID', '".$lcardattend->Get_Safe_Sql_Query($AMReason)."', $insert_id, '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_AM."','$AMWaive', now(), now() ";
		    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                   VALUES ($fieldsvalues) 
		                   on duplicate key update 
		                   	RecordID = LAST_INSERT_ID(RecordID), 
		                   	DateModified = NOW() ";
		    $Result[$i.'-008'] = $lcardattend->db_db_query($sql);
		    
		    if(sizeof($WebSAMSReasons[PROFILE_TYPE_ABSENT])==0 ||
		       (sizeof($WebSAMSReasons[PROFILE_TYPE_ABSENT])>0 && !in_array($AMReason, $WebSAMSReasons[PROFILE_TYPE_ABSENT])))
		    {
		    	
		    	if(sizeof($WebSAMSAddedReasons[PROFILE_TYPE_ABSENT])==0 ||
		       	   (sizeof($WebSAMSAddedReasons[PROFILE_TYPE_ABSENT])>0 && !in_array($AMReason, $WebSAMSAddedReasons[PROFILE_TYPE_ABSENT])))
		        {
			    	$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (ReasonText,ReasonType,RecordType,DateInput,DateModified) 
						  VALUES ('".$lcardattend->Get_Safe_Sql_Query($AMReason)."','".PROFILE_TYPE_ABSENT."','0',NOW(),NOW())";
					$Result[$i.'-009'] = $lcardattend->db_db_query($sql);
					$WebSAMSAddedReasons[PROFILE_TYPE_ABSENT][] = $AMReason;
		        }
		    }
		}
	}
	
	if($AMOutStatus==PROFILE_TYPE_EARLY)// 3 = Early Leave
	{
		// Need insert profile record and insert profile record reason if has reason
		
	    if(intval($AMLeaveWaive) == 1) // Early leave record is waived, no PROFILE_STUDENT_ATTENDANCE record
	    {
	    	$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE 
					WHERE UserID = '$TargetUserID' AND AttendanceDate='$TargetDate' 
						  AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".PROFILE_DAY_TYPE_AM."' ";
			$Result[$i.'-030'] = $lcardattend->db_db_query($sql);
	    	$insert_id = "NULL";
	    }else
	    {
	    	# Add absent student profile
		    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, RecordStatus, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
		    $fieldvalue = "'$TargetUserID','$TargetDate','$school_year','$semester','".PROFILE_TYPE_EARLY."','$AMLeaveWaive','".PROFILE_DAY_TYPE_AM."','".$lcardattend->Get_Safe_Sql_Query($AMLeaveReason)."',now(),now(),'$Class','$ClassNumber'";
		    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
		    				on duplicate key update 
		    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		    					DateModified = NOW() ";
		    $Result[$i.'-010'] = $lcardattend->db_db_query($sql);
		    $insert_id = $lcardattend->db_insert_id();
		    
		    if (!$insert_id)
		    {
				$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
				          WHERE UserID = '$TargetUserID' AND AttendanceDate = '$TargetDate'
				          AND RecordType = '".PROFILE_TYPE_EARLY."'
				          AND DayType = '".PROFILE_DAY_TYPE_AM."'";
				$temp = $lcardattend->returnVector($sql);
				$insert_id = $temp[0];
		    }
	    }
	    
	    $sql = "SELECT RecordID FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate' AND RecordType='".PROFILE_DAY_TYPE_AM."' ";
		$ConfirmRecord = $lcardattend->returnArray($sql);
		if($ConfirmRecord[0]['RecordID'])
		{
			// Update
			$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM SET EarlyConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_AM."', EarlyConfirmTime = NOW(), DateModified = NOW() ";
		}else
		{
			// Insert New
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,EarlyConfirmed,RecordType,EarlyConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_AM."',NOW(),NOW(),NOW())";
		}
	    /*
	    $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,EarlyConfirmed,RecordType,EarlyConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_AM."',NOW(),NOW(),NOW())
					ON DUPLICATE KEY UPDATE
						EarlyConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_AM."', EarlyConfirmTime = NOW(), DateModified = NOW()";
    	*/
    	$Result[$i.'-011'] = $lcardattend->db_db_query($sql);
	    
	    if(trim($AMLeaveReason)!="")
		{
		    # Update to reason table
		    $fieldname = "RecordDate, StudentID, Reason, ProfileRecordID, RecordType, DayType, RecordStatus, DateInput, DateModified";
		    $fieldsvalues = "'$TargetDate', '$TargetUserID', '".$lcardattend->Get_Safe_Sql_Query($AMLeaveReason)."', $insert_id, '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."','$AMLeaveWaive', now(), now() ";
		    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                   VALUES ($fieldsvalues) 
		                   on duplicate key update 
		                   	RecordID = LAST_INSERT_ID(RecordID), 
		                   	DateModified = NOW() ";
		    $Result[$i.'-012'] = $lcardattend->db_db_query($sql);
		    
		    if(sizeof($WebSAMSReasons[PROFILE_TYPE_EARLY])==0 || 
		       (sizeof($WebSAMSReasons[PROFILE_TYPE_EARLY])>0 && !in_array($AMLeaveReason, $WebSAMSReasons[PROFILE_TYPE_EARLY])))
		    {
		    	if(sizeof($WebSAMSAddedReasons[PROFILE_TYPE_EARLY])==0 || 
		           (sizeof($WebSAMSAddedReasons[PROFILE_TYPE_EARLY])>0 && !in_array($AMLeaveReason, $WebSAMSAddedReasons[PROFILE_TYPE_EARLY])))
		        {
			    	$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (ReasonText,ReasonType,RecordType,DateInput,DateModified) 
						  VALUES ('".$lcardattend->Get_Safe_Sql_Query($AMLeaveReason)."','".PROFILE_TYPE_EARLY."','0',NOW(),NOW())";
					$Result[$i.'-013'] = $lcardattend->db_db_query($sql);
					$WebSAMSAddedReasons[PROFILE_TYPE_EARLY][] = $AMLeaveReason;
		        }
		    }
		}
	}
	// End processing AM Status
	
	
	// Process PM Status
	# Check Bad actions
    if ($PMStatus==CARD_STATUS_PRESENT) # 0 = Present
    {
        $lcardattend->removeBadActionFakedCardPM($TargetUserID,$TargetDate);
        # Forgot to bring card
        $lcardattend->addBadActionNoCardEntrance($TargetUserID,$TargetDate,"");
    }
	else if ($PMStatus==CARD_STATUS_LATE) # 2 = Late
	{
	    $lcardattend->removeBadActionFakedCardPM($TargetUserID,$TargetDate);
	    # Forgot to bring card
	    $lcardattend->addBadActionNoCardEntrance($TargetUserID,$TargetDate,"");
	    
	    #########################################################
	    
		if(intval($PMWaive) == 1) // Late record is waived, no PROFILE_STUDENT_ATTENDANCE record
	    {
	    	$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE 
					WHERE UserID = '$TargetUserID' AND AttendanceDate='$TargetDate' 
						  AND RecordType='".PROFILE_TYPE_LATE."' AND DayType='".PROFILE_DAY_TYPE_PM."' ";
			$Result[$i.'-031'] = $lcardattend->db_db_query($sql);
	    	$insert_id = "NULL";
	    }else
	    {
		    # Add late to student profile
		    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, RecordStatus, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
		    $fieldvalue = "'$TargetUserID','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','$PMWaive','".PROFILE_DAY_TYPE_PM."','".$lcardattend->Get_Safe_Sql_Query($PMReason)."',now(),now(),'$Class','$ClassNumber'";
		    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
		    				on duplicate key update 
		    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		    					DateModified = NOW() ";
		    $Result[$i.'-014'] = $lcardattend->db_db_query($sql);
		    $insert_id = $lcardattend->db_insert_id();
		    
			# Calculate upgrade items
			if ($plugin['Disciplinev12'] && $LateCtgInUsed)
			{
				$dataAry = array();
				$dataAry['StudentID'] = $TargetUserID;
				$dataAry['RecordDate'] = $TargetDate;
				$dataAry['Year'] = $school_year;
				$dataAry['YearID'] = $school_year_id;
				$dataAry['Semester'] = $semester;
				$dataAry['SemesterID'] = $semester_id;
				$dataAry['StudentAttendanceID'] = $insert_id;
				$dataAry['Remark'] = $PMInTime;
				
				if($SeriousLateUsed == 1)
				{
					if($PMInTime != "")
					{
						$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$TargetUserID);
						
						//$school_start_time = $arr_time_table['MorningTime'];
						if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
						{
							##$field = "InSchoolTime";
							$school_start_time = $arr_time_table['MorningTime'];
						}
						else # retrieve LunchBackTime
						{
							##$field = "LunchBackTime";
							$school_start_time = $arr_time_table['LunchEnd'];
						}
						
						$str_late_minutes = strtotime($PMInTime) - strtotime($school_start_time);
						//$late_minutes = date("i",$str_late_minutes);
						$late_minutes = ceil($str_late_minutes/60);
						//$late_secode = date("s",$str_late_minutes);
						
						$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
						$min_late_minutes = $lcardattend->returnVector($sql);		
						
						if(sizeof($min_late_minutes)>0)
						{
							/*if($late_minutes >= $min_late_minutes[0])
							{*/
								/*if($late_minutes == $min_late_minutes[0])
								{
									if($late_secode > 0)
									{
										$late_minutes = $late_minutes + 1;
										$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes = '$late_minutes'";
										
										$isValid = $lcardattend->returnVector($sql);
										if($isValid[0] == 0)
										{
											$late_minutes = $late_minutes - 1;
										}
									}
								}*/
								$dataAry['SeriousLate'] = 1;
								$dataAry['LateMinutes'] = $late_minutes;
							/*}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}*/
						}
						else
						{
							$dataAry['SeriousLate'] = 0;
						}
					}
					else
					{
						$dataAry['SeriousLate'] = 0;
					}
				}
				$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
			}
			
		    if (!$insert_id)
		    {
				$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
				          WHERE UserID = '$TargetUserID' AND AttendanceDate = '$TargetDate'
				          AND RecordType = '".PROFILE_TYPE_LATE."'
				          AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$temp = $lcardattend->returnVector($sql);
				$insert_id = $temp[0];
		    }
	    }
	    
	    $sql = "SELECT RecordID FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate' AND RecordType='".PROFILE_DAY_TYPE_PM."' ";
		$ConfirmRecord = $lcardattend->returnArray($sql);
		if($ConfirmRecord[0]['RecordID'])
		{
			// Update
			$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM SET LateConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_PM."', LateConfirmTime = NOW(), DateModified = NOW() ";
		}else
		{
			// Insert New
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,LateConfirmed,RecordType,LateConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_PM."',NOW(),NOW(),NOW())";
		}
	    /*
		$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,LateConfirmed,RecordType,LateConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_PM."',NOW(),NOW(),NOW())
					ON DUPLICATE KEY UPDATE
						LateConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_PM."', LateConfirmTime = NOW(), DateModified = NOW()";
    	*/
    	$Result[$i.'-015'] = $lcardattend->db_db_query($sql);
		
		if(trim($PMReason)!="")
		{
		    # Update to reason table
		    $fieldname = "RecordDate, StudentID, Reason, ProfileRecordID, RecordType, DayType, RecordStatus, DateInput, DateModified";
		    $fieldsvalues = "'$TargetDate', '$TargetUserID', '".$lcardattend->Get_Safe_Sql_Query($PMReason)."', $insert_id, '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', '$PMWaive', now(), now() ";
		    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                   VALUES ($fieldsvalues) 
		                   on duplicate key update 
		                   	RecordID = LAST_INSERT_ID(RecordID), 
		                   	DateModified = NOW() ";
		   	
		    $Result[$i.'-016'] = $lcardattend->db_db_query($sql);
		    
		    if(sizeof($WebSAMSReasons[PROFILE_TYPE_LATE])==0 || 
		       (sizeof($WebSAMSReasons[PROFILE_TYPE_LATE])>0 && !in_array($PMReason, $WebSAMSReasons[PROFILE_TYPE_LATE])))
		    {
		    	if(sizeof($WebSAMSAddedReasons[PROFILE_TYPE_LATE])==0 || 
		       	   (sizeof($WebSAMSAddedReasons[PROFILE_TYPE_LATE])>0 && !in_array($PMReason, $WebSAMSAddedReasons[PROFILE_TYPE_LATE])))
		        {
			    	$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (ReasonText,ReasonType,RecordType,DateInput,DateModified) 
						  VALUES ('".$lcardattend->Get_Safe_Sql_Query($PMReason)."','".PROFILE_TYPE_LATE."','0',NOW(),NOW())";
					$Result[$i.'-017'] = $lcardattend->db_db_query($sql);
					$WebSAMSAddedReasons[PROFILE_TYPE_LATE][] = $PMReason;
		        }
		    }
		}
	    #########################################################
	}else if ($PMStatus==CARD_STATUS_ABSENT)// 1 = Absent
	{
	    $lcardattend->removeBadActionFakedCardPM($TargetUserID,$TargetDate); 
	    $lcardattend->removeBadActionNoCardEntrance($TargetUserID,$TargetDate);
	    // Need insert profile record and insert profile record reason if has reason
	   	if(intval($PMWaive) == 1) // Absent record is waived, no PROFILE_STUDENT_ATTENDANCE record
	    {
	    	$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE 
					WHERE UserID = '$TargetUserID' AND AttendanceDate='$TargetDate' 
						  AND RecordType='".PROFILE_TYPE_ABSENT."' AND DayType='".PROFILE_DAY_TYPE_PM."' ";
			$Result[$i.'-032'] = $lcardattend->db_db_query($sql);
	    	$insert_id = "NULL";
	    }else
	    {
	    	# Add absent student profile
		    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, RecordStatus, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
		    $fieldvalue = "'$TargetUserID','$TargetDate','$school_year','$semester','".PROFILE_TYPE_ABSENT."','$PMWaive','".PROFILE_DAY_TYPE_PM."','".$lcardattend->Get_Safe_Sql_Query($PMReason)."',now(),now(),'$Class','$ClassNumber'";
		    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
		    				on duplicate key update 
		    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		    					DateModified = NOW() ";
		    $Result[$i.'-018'] = $lcardattend->db_db_query($sql);
		    $insert_id = $lcardattend->db_insert_id();
		    
		    if (!$insert_id)
		    {
				$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
				          WHERE UserID = '$TargetUserID' AND AttendanceDate = '$TargetDate'
				          AND RecordType = '".PROFILE_TYPE_ABSENT."'
				          AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$temp = $lcardattend->returnVector($sql);
				$insert_id = $temp[0];
		    }   
	    }
	    
	    $sql = "SELECT RecordID FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate' AND RecordType='".PROFILE_DAY_TYPE_PM."' ";
		$ConfirmRecord = $lcardattend->returnArray($sql);
		if($ConfirmRecord[0]['RecordID'])
		{
			// Update
			$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM SET AbsenceConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_PM."', AbsenceConfirmTime = NOW(), DateModified = NOW() ";
		}else
		{
			// Insert New
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,AbsenceConfirmed,RecordType,AbsenceConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_PM."',NOW(),NOW(),NOW())";
		}
	    /*
	    $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,AbsenceConfirmed,RecordType,AbsenceConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_PM."',NOW(),NOW(),NOW())
					ON DUPLICATE KEY UPDATE
						AbsenceConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_PM."', AbsenceConfirmTime = NOW(), DateModified = NOW()";
    	*/
    	$Result[$i.'-019'] = $lcardattend->db_db_query($sql);
	    
	    if(trim($PMReason)!="")
		{
		    # Update to reason table
		    $fieldname = "RecordDate, StudentID, Reason, ProfileRecordID, RecordType, DayType, RecordStatus, DateInput, DateModified";
		    $fieldsvalues = "'$TargetDate', '$TargetUserID', '".$lcardattend->Get_Safe_Sql_Query($PMReason)."', $insert_id, '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_PM."', '$PMWaive', now(), now() ";
		    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                   VALUES ($fieldsvalues) 
		                   on duplicate key update 
		                   	RecordID = LAST_INSERT_ID(RecordID), 
		                   	DateModified = NOW() ";
		    $Result[$i.'-020'] = $lcardattend->db_db_query($sql);
		    
		    if(sizeof($WebSAMSReasons[PROFILE_TYPE_ABSENT])==0 || 
		       (sizeof($WebSAMSReasons[PROFILE_TYPE_ABSENT])>0 && !in_array($PMReason, $WebSAMSReasons[PROFILE_TYPE_ABSENT])))
		    {
		    	if(sizeof($WebSAMSAddedReasons[PROFILE_TYPE_ABSENT])==0 || 
		           (sizeof($WebSAMSAddedReasons[PROFILE_TYPE_ABSENT])>0 && !in_array($PMReason, $WebSAMSAddedReasons[PROFILE_TYPE_ABSENT])))
		        {
			    	$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (ReasonText,ReasonType,RecordType,DateInput,DateModified) 
						  VALUES ('".$lcardattend->Get_Safe_Sql_Query($PMReason)."','".PROFILE_TYPE_ABSENT."','0',NOW(),NOW())";
					$Result[$i.'-021'] = $lcardattend->db_db_query($sql);
					$WebSAMSAddedReasons[PROFILE_TYPE_ABSENT][] = $PMReason;
		        }
		    }
		}
	}
	
	if($PMOutStatus==PROFILE_TYPE_EARLY)// 3 = Early Leave
	{
		// Need insert profile record and insert profile record reason if has reason
	    if(intval($PMLeaveWaive) == 1) // Early leave record is waived, no PROFILE_STUDENT_ATTENDANCE record
	    {
	    	$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE 
					WHERE UserID = '$TargetUserID' AND AttendanceDate='$TargetDate' 
						  AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".PROFILE_DAY_TYPE_PM."' ";
			$Result[$i.'-033'] = $lcardattend->db_db_query($sql);
	    	$insert_id = "NULL";
	    }else
	    {
	    	# Add absent student profile
		    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, RecordStatus, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
		    $fieldvalue = "'$TargetUserID','$TargetDate','$school_year','$semester','".PROFILE_TYPE_EARLY."','$PMLeaveWaive','".PROFILE_DAY_TYPE_PM."','".$lcardattend->Get_Safe_Sql_Query($PMLeaveReason)."',now(),now(),'$Class','$ClassNumber'";
		    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
		    				on duplicate key update 
		    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		    					DateModified = NOW() ";
		    $Result[$i.'-022'] = $lcardattend->db_db_query($sql);
		    $insert_id = $lcardattend->db_insert_id();
		    
		    if (!$insert_id)
		    {
				$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
				          WHERE UserID = '$TargetUserID' AND AttendanceDate = '$TargetDate'
				          AND RecordType = '".PROFILE_TYPE_EARLY."'
				          AND DayType = '".PROFILE_DAY_TYPE_PM."'";
				$temp = $lcardattend->returnVector($sql);
				$insert_id = $temp[0];
		    }
	    }
	    
	    $sql = "SELECT RecordID FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate' AND RecordType='".PROFILE_DAY_TYPE_PM."' ";
		$ConfirmRecord = $lcardattend->returnArray($sql);
		if($ConfirmRecord[0]['RecordID'])
		{
			// Update
			$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM SET EarlyConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_PM."', EarlyConfirmTime = NOW(), DateModified = NOW() ";
		}else
		{
			// Insert New
			$sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,EarlyConfirmed,RecordType,EarlyConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_PM."',NOW(),NOW(),NOW())";
		}
	    /*
	    $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate,EarlyConfirmed,RecordType,EarlyConfirmTime,DateInput,DateModified)
                	VALUES ('$TargetDate','1','".PROFILE_DAY_TYPE_PM."',NOW(),NOW(),NOW())
					ON DUPLICATE KEY UPDATE
						EarlyConfirmed = '1', RecordType='".PROFILE_DAY_TYPE_PM."', EarlyConfirmTime = NOW(), DateModified = NOW()";
    	*/
    	$Result[$i.'-023'] = $lcardattend->db_db_query($sql);
	    
	    if(trim($PMLeaveReason)!="")
		{
		    # Update to reason table
		    $fieldname = "RecordDate, StudentID, Reason, ProfileRecordID, RecordType, DayType, RecordStatus, DateInput, DateModified";
		    $fieldsvalues = "'$TargetDate', '$TargetUserID', '".$lcardattend->Get_Safe_Sql_Query($PMLeaveReason)."', $insert_id, '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', '$PMLeaveWaive', now(), now() ";
		    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                   VALUES ($fieldsvalues) 
		                   on duplicate key update 
		                   	RecordID = LAST_INSERT_ID(RecordID), 
		                   	DateModified = NOW() ";
		    $Result[$i.'-024'] = $lcardattend->db_db_query($sql);
		    
		    if(sizeof($WebSAMSReasons[PROFILE_TYPE_EARLY])==0 || 
		       (sizeof($WebSAMSReasons[PROFILE_TYPE_EARLY])>0 && !in_array($PMLeaveReason, $WebSAMSReasons[PROFILE_TYPE_EARLY])))
		    {
		    	if(sizeof($WebSAMSAddedReasons[PROFILE_TYPE_EARLY])==0 || 
		           (sizeof($WebSAMSAddedReasons[PROFILE_TYPE_EARLY])>0 && !in_array($PMLeaveReason, $WebSAMSAddedReasons[PROFILE_TYPE_EARLY])))
		    	{
			    	$sql="INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE (ReasonText,ReasonType,RecordType,DateInput,DateModified) 
						  VALUES ('".$lcardattend->Get_Safe_Sql_Query($PMLeaveReason)."','".PROFILE_TYPE_EARLY."','0',NOW(),NOW())";
					$Result[$i.'-025'] = $lcardattend->db_db_query($sql);
					$WebSAMSAddedReasons[PROFILE_TYPE_EARLY][] = $PMLeaveReason;
		    	}
		    }
		}
	}
	// End processing PM Status
	
	// Process Class Confirm
	$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
    $class_id = $lcardattend->getClassID($Class);
	// Confirm AM
    $sql = "INSERT INTO $card_student_daily_class_confirm
            (
                ClassID,
                ConfirmedUserID,
                DayNumber,
                DayType,
                DateInput,
                DateModified
            ) VALUES
            (
                '$class_id',
                '$confirmed_user_id',
                '$DayNumber',
                '".PROFILE_DAY_TYPE_AM."',
                NOW(),
                NOW()
            )
            ON DUPLICATE KEY UPDATE 
            	ConfirmedUserID=$confirmed_user_id, DateModified = NOW() ";
    $Result[$i.'-026'] = $lcardattend->db_db_query($sql);
    
    // Confirm PM
    $sql = "INSERT INTO $card_student_daily_class_confirm
            (
                ClassID,
                ConfirmedUserID,
                DayNumber,
                DayType,
                DateInput,
                DateModified
            ) VALUES
            (
                '$class_id',
                '$confirmed_user_id',
                '$DayNumber',
                '".PROFILE_DAY_TYPE_PM."',
                NOW(),
                NOW()
            )
            ON DUPLICATE KEY UPDATE 
            	ConfirmedUserID=$confirmed_user_id, DateModified = NOW() ";
    $Result[$i.'-027'] = $lcardattend->db_db_query($sql);
}

if (!in_array(false,$Result))
{
	$Msg = $Lang['StudentAttendance']['DailyStatusConfirmSuccess'];
	$lcardattend->Commit_Trans();
}
else
{
	$Msg = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
	$lcardattend->RollBack_Trans();
}

//set_time_limit(30);
intranet_closedb();
//if($debug) echo "$Msg<PRE>";print_r($Result);echo "</PRE>";
//$Msg = $Lang['StudentAttendance']['OfflineRecordImportSuccess'];
header("Location: import.php?Msg=".urlencode($Msg));
?>