<?php
// editing by kenenth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$lcardattend = new libcardstudentattend2();

if(!isset($_REQUEST["start"]) && !isset($_REQUEST["end"]))
{
	echo "Please input start date and end date in this way: clear.php?start=2009-09-01&end=2009-12-31";
	intranet_closedb();
	exit();
}

$lcardattend->Start_Trans();
$Result = array();

$StartDate = $_REQUEST["start"];
$EndDate = $_REQUEST["end"];
$tsStartDate = strtotime($StartDate);
$tsEndDate = strtotime($EndDate);
$tsOneDay = 24*60*60;

if($plugin['Disciplinev12'])
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldisciplinev12 = new libdisciplinev12();
	$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE WHERE RecordType = '2' and AttendanceDate BETWEEN '$StartDate 00:00:00' AND '$EndDate 23:59:59' ";
	$ProfileID = $lcardattend->returnArray($sql);
	for($i=0;$i<sizeof($ProfileID);$i++)
	{
		$attendance_id = $ProfileID[$i]['StudentAttendanceID'];
		$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
	}
}

for($tsCurrent = $tsStartDate;$tsCurrent<=$tsEndDate;$tsCurrent+=$tsOneDay)
{
	$datestr = date("Y-m-d", $tsCurrent);
	$CurrentDateArr = explode("-",$datestr);
	$txt_year = $CurrentDateArr[0];
	$txt_month = $CurrentDateArr[1];
	$txt_day = $CurrentDateArr[2];
	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
	$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
	$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;
	$d = intval($txt_day);
	
	$sql = "SHOW TABLES LIKE '".$card_log_table_name."'";
	$Temp = $lcardattend->returnArray($sql);
	if (sizeof($Temp) > 0) {
		$sql = "DELETE FROM $card_log_table_name WHERE DayNumber = '$d' ";
		$Result[$datestr.'-DAILYLOG'] = $lcardattend->db_db_query($sql);
	}
	
	$sql = "SHOW TABLES LIKE '".$card_student_daily_class_confirm."'";
	$Temp = $lcardattend->returnArray($sql);
	if (sizeof($Temp) > 0) {
    $sql = "DELETE FROM $card_student_daily_class_confirm WHERE DayNumber = '$d' ";
    $Result[$datestr.'-CLASS_CONFIRM'] = $lcardattend->db_db_query($sql);
  }
  
  $sql = "SHOW TABLES LIKE '".$card_student_daily_group_confirm."'";
	$Temp = $lcardattend->returnArray($sql);
	if (sizeof($Temp) > 0) {  
    $sql = "DELETE FROM $card_student_daily_group_confirm WHERE DayNumber = '$d' ";
    $Result[$datestr.'-GROUP_CONFIRM'] = $lcardattend->db_db_query($sql);
  }
    
  $sql = "DELETE FROM CARD_STUDENT_BAD_ACTION WHERE RecordDate = '$datestr' ";
  $Result[$datestr.'-BAD_ACTION'] = $lcardattend->db_db_query($sql);
  
  $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate = '$datestr' ";
  $Result[$datestr.'-PROFILE_REASON'] = $lcardattend->db_db_query($sql);
  
  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate LIKE '$datestr%' ";
  $Result[$datestr.'-PROFILE'] = $lcardattend->db_db_query($sql);
}

if (!in_array(false,$Result))
{
	echo "Data cleaned successfully!";
	$lcardattend->Commit_Trans();
}
else
{
	echo "Failed to clean data! Deletion has been rolled back!";
	$lcardattend->RollBack_Trans();
}

intranet_closedb();
?>