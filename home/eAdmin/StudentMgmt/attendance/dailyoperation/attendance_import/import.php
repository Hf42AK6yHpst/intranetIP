<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
/*
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
*/
$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportAttendanceData";

$linterface = new interface_html();

$lc->retrieveSettings();

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ImportAttendenceData'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
if ($msg == 4) $SysMsg = $linterface->GET_SYS_MSG("import_failed2");
if ($msg == 5) $SysMsg = $linterface->GET_SYS_MSG("import_failed");


$TermStartDate = date('Y-m-d',getStartOfAcademicYear(""));
$TermEndDate = date('Y-m-d',getEndOfAcademicYear(""));
$TermStartDateArr = explode("-", $TermStartDate);
$TermEndDateArr = explode("-", $TermEndDate);
$StartYear = $TermStartDateArr[0];
$StartMonth = $TermStartDateArr[1];
$StartDay = $TermStartDateArr[2];
$EndYear = $TermEndDateArr[0];
$EndMonth = $TermEndDateArr[1];
$EndDay = $TermEndDateArr[2];
$YearArr = array();// start year to end year
$MonthArr = array();// months of start year to months of end year [assoc array using numeric year as key]
for($y = intval($StartYear);$y<=intval($EndYear);$y++)
{
	$YearArr[] = $y;
	$MonthArr[$y] = array();
}
for($i=0;$i<sizeof($YearArr);$i++)
{
	if($i==0)//first year
	{
		for($m=intval($StartMonth);$m<=12;$m++)
		{
			$MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
		}
	}else if($i==(sizeof($YearArr)-1))//last year
	{
		for($m=1;$m<=$EndMonth;$m++)
		{
			$MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
		}
	}else// middle years
	{
		for($m=1;$m<=12;$m++)
		{
			$MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
		}
	}
}
//debug_r($YearArr);
//debug_r($MonthArr);

?>
<br />
<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this)">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>	
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_select_file?>
		</td>
		<td width="70%"class="tabletext">
			<input type="file" class="file" name="userfile"><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_general_Format?>
		</td>
		<td width="70%"class="tabletext">
			<br />
			<span class="tabletextremark">
			<?=$Lang['StudentAttendance']['ImportAttendDataFormatDesc']?>
			</span>
			<br /><a class="tablelink" href="<?=GET_CSV("sample.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a><br />
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "",""," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>