<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StudAttendUI = new libstudentattendance_ui();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_PresetAbsence";
$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/new.php", 1);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_student.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php", 0);

$MODULE_OBJ = $StudAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

echo $StudAttendUI->Get_Preset_Absence_Form();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>