<?php // editing by 
##################################### Change Log #####################################################
# 2020-06-03 Ray: Add TW
# 2019-10-15 Ray: Added session From/To
# 2017-12-04 Carlos: Changed $HTTP_SERVER_VARS['HTTP_REFERER'] to $_SERVER['HTTP_REFERER']
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$lword = new libwordtemplates();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_PresetAbsence";

$linterface = new interface_html();

$return_url = $return_url==""?$_SERVER['HTTP_REFERER']:$return_url;

if(sizeof($RecordID)!=1){
	header("Location: $return_url");
}

if(is_array($RecordID))
	$RecordID=$RecordID[0];

$namefield = getNameFieldWithClassNumberByLang("b.");

if(get_client_region() == 'zh_TW') {
	$EmptySymbol = $Lang['General']['EmptySymbol'];
	$lc->createCountSchoolDayFunction();
	$sql = " SELECT
                $namefield,
				CASE 
				    WHEN a.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
				    WHEN a.AttendanceType = 1 THEN '$i_DayTypeAM'
				    WHEN a.AttendanceType = 2 THEN '$i_DayTypePM'
				    WHEN a.AttendanceType = 3 THEN '$i_DayTypeSession'
				END as AttendanceType,
			    a.RecordDate,
			    IF(p.RecordDate IS NULL, '$EmptySymbol', p.RecordDate) as EndDate,
		        IF(p.RecordDate IS NULL,'$EmptySymbol', count_schooldays(a.RecordDate, p.RecordDate)) as NumberOfDays,
				IF(a.AttendanceType = 3, a.SessionFrom, '$EmptySymbol') as SessionFrom,
				IF(a.AttendanceType = 3, a.SessionTo, '$EmptySymbol') as SessionTo, 
				IF(a.AttendanceType = 3, a.SessionTo - a.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
				lt.TypeName as LeaveTypeName, 
				 a.Reason,
				 a.Remark
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				LEFT JOIN CARD_STUDENT_PRESET_LEAVE p ON p.RecordID = (SELECT RecordID FROM CARD_STUDENT_PRESET_LEAVE WHERE RelatedRecordID=a.RecordID ORDER BY RecordDate DESC LIMIT 1)
				LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID
				WHERE a.RecordID='$RecordID'
				";

} else {
$session_fields = "";
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	$session_fields .= " a.SessionFrom, a.SessionTo, ";
}
	$sql = "SELECT $namefield,a.RecordDate,a.DayPeriod, $session_fields
 lt.TypeName as LeaveTypeName,
 a.Reason,a.Waive,a.DocumentStatus,a.Remark
 FROM CARD_STUDENT_PRESET_LEAVE AS a
  LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
   LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID
   WHERE RecordID='$RecordID'";
}

$lc = new libcardstudentattend2();

$temp = $lc->returnArray($sql,5);

if(get_client_region() == 'zh_TW') {
	list($studentname, $attendancetype, $recorddate, $enddate, $no_of_days, $session_from, $session_to, $session_diff, $leave_type_name, $reason, $remark) = $temp[0];
} else {
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		list($studentname, $recorddate, $datetype, $session_from, $session_to, $leave_type_name, $reason, $Waive, $Handin_Prove, $remark) = $temp[0];
} else {
		list($studentname, $recorddate, $datetype, $leave_type_name, $reason, $Waive, $Handin_Prove, $remark) = $temp[0];
	}
}

if($datetype==PROFILE_DAY_TYPE_AM)
	$datetype = $i_DayTypeAM;
else if($datetype==PROFILE_DAY_TYPE_PM)
	$datetype = $i_DayTypePM;
	
// prepare preset WEBSAMS reason list
$words_absence = $lword->getWordListAttendance(1);
$AbsentHasWord = sizeof($words_absence)!=0;
foreach($words_absence as $key=>$word)
	$words_absence[$key]= htmlspecialchars($word);

$refer_url = $_SERVER['HTTP_REFERER'];
$refer_url = parse_url($refer_url);
if ($refer_url["path"] == "/home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php")
	$refer_page = "date";
else
	$refer_page = "student";

$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/new.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_student.php",($refer_page=="student")?1:0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php",($refer_page=="date")?1:0);

$PAGE_NAVIGATION[] = array($button_edit);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");	

$hide_waive_style= '';
if(get_client_region() == 'zh_TW') {
	$hide_waive_style = ' style="display: none;"';
}

?>
<script language='javascript'>
function submitForm(){
	document.form1.submit();
}
</script>
<br />
<form name="form1" method="POST" action='edit_update.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_UserStudentName ?></td>
		<td valign="top" class="tabletext" width="70%"><?=$studentname?></td>
	</tr>
    <?php if(get_client_region() == 'zh_TW') { ?>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Attendance_Type ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$attendancetype?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_general_startdate ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$recorddate?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_general_enddate ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$enddate?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_StaffAttendance_Report_Number_Of_Date ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$no_of_days?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['SessionFrom'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$session_from?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['SessionTo'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$session_to?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['TotalSession'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$session_diff?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['LeaveType'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$leave_type_name?></td>
    </tr>
	<?php } else { ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Attendance_Date ?></td>
		<td valign="top" class="tabletext" width="70%"><?=$recorddate?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Attendance_DayType ?></td>
		<td valign="top" class="tabletext" width="70%"><?=$datetype?></td>
	</tr>
    <?php if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) { ?>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['SessionFrom'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$session_from?></td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['SessionTo'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$session_to?></td>
    </tr>
    <?php } ?>
    <?php if($sys_custom['StudentAttendance']['LeaveType']) { ?>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['StudentAttendance']['LeaveType'] ?></td>
        <td valign="top" class="tabletext" width="70%"><?=$leave_type_name?></td>
    </tr>
    <?php } ?>
    <?php } // end TW?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Attendance_Reason ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$linterface->CONVERT_TO_JS_ARRAY($words_absence, "AbsentArrayWords", 1, 1)?>
			<input class="textboxnum" type="text" name="reason" id="reason" maxlength="255" value="<?=htmlspecialchars($reason,ENT_QUOTES)?>">
			<?=$linterface->GET_PRESET_LIST("AbsentArrayWords", $i, "reason")?>
		</td>
	</tr>
	<?php if(get_client_region() != 'zh_TW') { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['Waived']?>
		</td>
		<td width="70%"class="tabletext">
			<input type="checkbox" name="Waive" id="Waive" value="1" <?=(($Waive == "1")? "Checked":"")?>>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['ProveDocument']?>
		</td>
		<td width="70%"class="tabletext">
			<input type="checkbox" name="Handin_Prove" id="Handin_Prove" value="1" <?=(($Handin_Prove == "1")? "Checked":"")?>>
		</td>
	</tr>
    <?php } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Attendance_Remark ?></td>
		<td valign="top" class="tabletext" width="70%"><?=$linterface->GET_TEXTAREA("remark", $remark)?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "submitForm()", "submit2") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='".$return_url."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="RecordID" value="<?=$RecordID?>">
<input type="hidden" name="return_url" value="<?=$return_url?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>