<?php
// Editing by 
##################################### Change Log #####################################################
# 2020-06-10 Ray: Add TW
# 2019-10-15 Ray: Added session From/To
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");


$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
switch($filter){
	case 1: $cond_filter = " AND a.RecordDate <'$today' "; break;
	case 2: $cond_filter = " AND a.RecordDate >='$today' "; break;
	case 3: $cond_filter = ""; break;
	default: $cond_filter =" AND a.RecordDate >= '$today' ";
}

$has_apply_leave = $plugin['eClassApp']? true : false;

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
if(get_client_region() == 'zh_TW') {
	$field_array = array("a.RecordDate", "EndDate","NumberOfDays","AttendanceType","SessionFrom","SessionTo","SessionDiff","LeaveTypeName","a.Reason","a.Remark", "a.DateModified");
} else {
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$field_array = array("a.RecordDate", "a.DayPeriod", "a.SessionFrom", "a.SessionTo");
		if ($sys_custom['StudentAttendance']['LeaveType']) {
			$field_array[] = 'LeaveTypeName';
		}
		$field_array = array_merge($field_array, array("a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified"));
} else {
		$field_array = array("a.RecordDate", "a.DayPeriod");
		if ($sys_custom['StudentAttendance']['LeaveType']) {
			$field_array[] = 'LeaveTypeName';
		}
		$field_array = array_merge($field_array, array("a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified"));
	}
}
if($has_apply_leave){
	$field_array[] = "a.ApprovedAt";
	$field_array[] = "ApprovedBy";
}
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($studentID!=""){
	$lb = new libdb();
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT $namefield FROM INTRANET_USER WHERE UserID='$studentID' AND RecordType=2 AND RecordStatus IN (0,1,2)";
	$temp = $lb->returnVector($sql);
	$student_name = $temp[0];
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " ,a.ApprovedAt,$approved_by_namefield as ApprovedBy ";
	}

	if(get_client_region() == 'zh_TW') {
		$cond_keyword = '';
		if($keyword != '') {
			$cond_keyword = " AND (a.RecordDate LIKE '%" . $lb->Get_Safe_Sql_Like_Query($keyword) . "%' OR a.Reason LIKE '%" . $lb->Get_Safe_Sql_Like_Query($keyword) . "%' OR a.Remark LIKE '%" . $lb->Get_Safe_Sql_Like_Query($keyword) . "%' )";
		}

		$sql = "SELECT RelatedRecordID FROM CARD_STUDENT_PRESET_LEAVE a
                LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
                WHERE a.StudentID='" . $lb->Get_Safe_Sql_Query($studentID) . "' $cond_filter
				$cond_keyword
				AND a.RelatedRecordID IS NOT NULL
				$cond GROUP BY RelatedRecordID";
		$rs = $lb->returnArray($sql);
		$relatedRecordIdAry = Get_Array_By_Key($rs, 'RelatedRecordID');
		$conds_recordId = '';
		if(!empty($relatedRecordIdAry)) {
			$conds_recordId = " OR a.RecordID IN (" . implode(',', $relatedRecordIdAry) . ")  ";
		}

		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$lc->createCountSchoolDayFunction();
		$sql = " SELECT a.RecordDate,
                IF(p.RecordDate IS NULL, '$EmptySymbol', p.RecordDate) as EndDate,
		        IF(p.RecordDate IS NULL,'$EmptySymbol', count_schooldays(a.RecordDate, p.RecordDate)) as NumberOfDays,
				CASE 
				    WHEN a.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
				    WHEN a.AttendanceType = 1 THEN '$i_DayTypeAM'
				    WHEN a.AttendanceType = 2 THEN '$i_DayTypePM'
				    WHEN a.AttendanceType = 3 THEN '$i_DayTypeSession'
				END as AttendanceType,			    
				IF(a.AttendanceType = 3, a.SessionFrom, '$EmptySymbol') as SessionFrom,
				IF(a.AttendanceType = 3, a.SessionTo, '$EmptySymbol') as SessionTo, 
				IF(a.AttendanceType = 3, a.SessionTo - a.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
				lt.TypeName as LeaveTypeName, 
				 a.Reason,
				 a.Remark,
				 a.DateModified
				$more_fields 
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				LEFT JOIN CARD_STUDENT_PRESET_LEAVE p ON p.RecordID = (SELECT RecordID FROM CARD_STUDENT_PRESET_LEAVE WHERE RelatedRecordID=a.RecordID AND a.AttendanceType=0 ORDER BY RecordDate DESC LIMIT 1)
				LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID
				WHERE
				(a.StudentID='" . $lb->Get_Safe_Sql_Query($studentID) . "' $cond_filter
				$cond_keyword
				AND a.RelatedRecordID IS NULL)
				$conds_recordId
			    $order_str
				";

	} else {
	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}

		$leavetype_fields = '';
		$leavetype_join = '';
		if ($sys_custom['StudentAttendance']['LeaveType']) {
			$leavetype_fields = " lt.TypeName as LeaveTypeName, ";
			$leavetype_join = "LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID";
		}

	$sql=" SELECT a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 $session_fields
				 $leavetype_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				 IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified 
				 $more_fields 
				FROM CARD_STUDENT_PRESET_LEAVE AS a $more_joins $leavetype_join
				WHERE a.StudentID='$studentID'  $cond_filter AND 
				(a.RecordDate LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str 
		";
	}

	$temp = $lb->returnArray($sql);
	
	$display= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
	$display.="<tr>";
	$display.= "<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>\n";
	if(get_client_region() == 'zh_TW') {
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_general_startdate."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_general_enddate."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_StaffAttendance_Report_Number_Of_Date."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Type."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionFrom']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionTo']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['TotalSession']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['LeaveType']."</td>\n";
	} else {
	$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Date."</td>\n";
	$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_DayType."</td>\n";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionFrom']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionTo']."</td>\n";
	}
		if ($sys_custom['StudentAttendance']['LeaveType']) {
			$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['LeaveType']."</td>\n";
		}
	}

	$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Reason."</td>\n";
	if(get_client_region() != 'zh_TW') {
	$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['Waived']."</td>\n";
	$display.= "<td width=\"2%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ProveDocument']."</td>\n";
	}
	$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['OfficeRemark']."</td>\n";
	$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['General']['LastUpdatedTime']."</td>\n";
	if($has_apply_leave){
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ApprovalTime']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ApprovalUser']."</td>\n";
	}
	for($i=0;$i<sizeof($temp);$i++){
		if(get_client_region() == 'zh_TW') {
			list($recorddate, $enddate, $no_of_days, $attendancetype, $session_from, $session_to, $session_diff, $leave_type_name, $reason, $remark, $date_modified) = $temp[$i];
		} else {
		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
				if ($sys_custom['StudentAttendance']['LeaveType']) {
					list($recorddate, $daytype, $session_from, $session_to, $leave_type_name, $reason, $Waive, $Handin_Prove, $remark, $date_modified) = $temp[$i];
				} else {
			list($recorddate, $daytype, $session_from, $session_to, $reason, $Waive, $Handin_Prove, $remark, $date_modified) = $temp[$i];
				}
			} else {
				if ($sys_custom['StudentAttendance']['LeaveType']) {
					list($recorddate, $daytype, $leave_type_name, $reason, $Waive, $Handin_Prove, $remark, $date_modified) = $temp[$i];
		} else {
			list($recorddate, $daytype, $reason, $Waive, $Handin_Prove, $remark, $date_modified) = $temp[$i];
		}
			}
		}
		$row = "<tr>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";		
		if(get_client_region() == 'zh_TW') {
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$recorddate</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$enddate</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$no_of_days</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$attendancetype</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_from</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_to</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_diff</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$leave_type_name</td>";
		} else {
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$recorddate</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$daytype</td>";
		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_from</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_to</td>";
		}
			if ($sys_custom['StudentAttendance']['LeaveType']) {
				$row .= "<td class=\"eSporttdborder eSportprinttext\">$leave_type_name</td>";
			}
		}
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$reason</td>";		
		if(get_client_region() != 'zh_TW') {
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Waive</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Handin_Prove</td>";	
		}
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$date_modified</td>";
		if($has_apply_leave){
			$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['ApprovedAt']."&nbsp;</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['ApprovedBy']."&nbsp;</td>";
		}
		$row.="</tr>";
		$display.=$row;
	}
	if(sizeof($temp)<=0){
		$row = "<tr><td class=\"eSportprinttext\" height=\"40\" colspan=\"".count($field_array)."\" align=\"center\">$i_no_record_exists_msg</td></tr>";
		$display.=$row;
	}
	$display.="</table>";
}

?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><b><?=$i_SmartCard_DailyOperation_Preset_Absence?> - <?=$student_name?></b></td>
	</tr>
</table>
<?=$display?>
<?php
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>