<?php
// Editing by 
##################################### Change Log #####################################################
# 2020-06-03 Ray: Add TW
# 2019-10-15 Ray: Added session From/To
# 2019-08-06 Ray: Added date range
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$filter = $filter==""?2:$filter;
$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$StartDate = $StartDate==""?$academic_year_startdate:$StartDate;
$EndDate = $EndDate==""?$academic_year_enddate:$EndDate;
$date_cond = " ((DATE_FORMAT(a.RecordDate,'%Y-%m-%d')) BETWEEN '$StartDate' AND '$EndDate')";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$has_apply_leave = $plugin['eClassApp']? true : false;

$namefield = getNameFieldByLang("b.");

if(get_client_region() == 'zh_TW') {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "AttendanceType", "a.RecordDate", "EndDate","NumberOfDays","SessionFrom","SessionTo","SessionDiff","LeaveTypeName","a.Reason","a.Remark", "a.DateModified");
} else {
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.SessionFrom", "a.SessionTo", "a.Reason", "Waive", "a.Remark", "a.DateModified");
} else {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.Reason", "Waive", "a.Remark", "a.DateModified");
}
}
if($has_apply_leave){
	$field_array[] = "a.ApprovedAt";
	$field_array[] = "ApprovedBy";
}
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($StartDate!="" && $EndDate!=""){
	$lb = new libdb();
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " ,a.ApprovedAt,$approved_by_namefield as ApprovedBy ";
	}

	if(get_client_region() == 'zh_TW') {
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$cond = '';
		if($attendanceType != '') {
			$cond .= " AND a.AttendanceType='".$lc->Get_Safe_Sql_Query($attendanceType)."'";
		}

		$sql = "SELECT RelatedRecordID FROM CARD_STUDENT_PRESET_LEAVE a
                LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
                WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				AND a.RelatedRecordID IS NOT NULL
				$cond GROUP BY RelatedRecordID";
		$rs = $lb->returnArray($sql);
		$relatedRecordIdAry = Get_Array_By_Key($rs, 'RelatedRecordID');
		$conds_recordId = '';
		if(!empty($relatedRecordIdAry)) {
			$conds_recordId = " OR a.RecordID IN (" . implode(',', $relatedRecordIdAry) . ")  ";
		}

		$lc->createCountSchoolDayFunction();
		$sql = " SELECT $namefield,
				b.ClassName,
				b.ClassNumber,
				CASE 
				    WHEN a.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
				    WHEN a.AttendanceType = 1 THEN '$i_DayTypeAM'
				    WHEN a.AttendanceType = 2 THEN '$i_DayTypePM'
				    WHEN a.AttendanceType = 3 THEN '$i_DayTypeSession'
				END as AttendanceType,
			    a.RecordDate,
			    IF(p.RecordDate IS NULL, '$EmptySymbol', p.RecordDate) as EndDate,
		        IF(p.RecordDate IS NULL,'$EmptySymbol', count_schooldays(a.RecordDate, p.RecordDate)) as NumberOfDays,
				IF(a.AttendanceType = 3, a.SessionFrom, '$EmptySymbol') as SessionFrom,
				IF(a.AttendanceType = 3, a.SessionTo, '$EmptySymbol') as SessionTo, 
				IF(a.AttendanceType = 3, a.SessionTo - a.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
				lt.TypeName as LeaveTypeName, 
				 a.Reason,
				 a.Remark,
				 a.DateModified
				$more_fields 
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				LEFT JOIN CARD_STUDENT_PRESET_LEAVE p ON p.RecordID = (SELECT RecordID FROM CARD_STUDENT_PRESET_LEAVE WHERE RelatedRecordID=a.RecordID AND a.AttendanceType=0 ORDER BY RecordDate DESC LIMIT 1)
				LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID
				WHERE ($date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				AND a.RelatedRecordID IS NULL
				$cond)
				$conds_recordId
				$order_str
				";
	} else {
	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}


		$leavetype_fields = '';
		$leavetype_join = '';
		if($sys_custom['StudentAttendance']['LeaveType']) {
			$leavetype_fields = " lt.TypeName as LeaveTypeName, ";
			$leavetype_join = "LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID";
		}


	$sql=" SELECT $namefield,
				b.ClassName,b.ClassNumber,
				a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM') as daytype,
				 $session_fields
				 $leavetype_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
 				 IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified 
				 $more_fields 
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
				$more_joins 
				$leavetype_join
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";
	}

	$temp = $lb->returnArray($sql);
	
	$lexport = new libexporttext();

	if(get_client_region() == 'zh_TW') {
		$exportColumn = array("$i_UserStudentName", "$i_ClassName", "$i_ClassNumber", $i_Attendance_Type, $i_general_startdate, $i_general_enddate, $i_StaffAttendance_Report_Number_Of_Date,$Lang['StudentAttendance']['SessionFrom'],$Lang['StudentAttendance']['SessionTo'],$Lang['StudentAttendance']['TotalSession'],$Lang['StudentAttendance']['LeaveType'],$i_Attendance_Reason,$Lang['StudentAttendance']['OfficeRemark'],$Lang['General']['LastUpdatedTime']);
	} else {
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$exportColumn = array("$i_UserStudentName", "$i_ClassName", "$i_ClassNumber", $i_StudentAttendance_Field_Date,
				"$i_Attendance_DayType", $Lang['StudentAttendance']['SessionFrom'], $Lang['StudentAttendance']['SessionTo']);
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$exportColumn[] = 'LeaveTypeName';
			}
			$exportColumn = array_merge($exportColumn, array("$i_Attendance_Reason", $Lang['StudentAttendance']['Waived'], $Lang['StudentAttendance']['ProveDocument'], $Lang['StudentAttendance']['OfficeRemark'], $Lang['General']['LastUpdatedTime']));
	} else {
		$exportColumn = array("$i_UserStudentName", "$i_ClassName", "$i_ClassNumber", $i_StudentAttendance_Field_Date,
				"$i_Attendance_DayType");
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$exportColumn[] = 'LeaveTypeName';
			}
			$exportColumn = array_merge($exportColumn, array("$i_Attendance_Reason", $Lang['StudentAttendance']['Waived'], $Lang['StudentAttendance']['ProveDocument'], $Lang['StudentAttendance']['OfficeRemark'], $Lang['General']['LastUpdatedTime']));
		}
	}

	if($has_apply_leave){
		$exportColumn[] = $Lang['StudentAttendance']['ApprovalTime'];
		$exportColumn[] = $Lang['StudentAttendance']['ApprovalUser'];
	}
	$export_content = $lexport->GET_EXPORT_TXT($temp, $exportColumn);
	
	
	$export_content_final = "$i_SmartCard_DailyOperation_Preset_Absence ($StartDate - $EndDate)\n\n";
	if(sizeof($temp)<=0){
		$export_content_final .= "$i_no_record_exists_msg\n";
	} else {
		$export_content_final .= $export_content;
	}
	
}

intranet_closedb();

$filename = "preset_absence_".$StartDate."_".$EndDate.".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>