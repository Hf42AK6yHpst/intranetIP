<?php
// Editing by 
/*
 * 2014-03-17 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance_WaiveAbsent']) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

switch($task)
{
	case "getWaiveAbsenceTable":
		$x = $StudentAttendUI->Get_Waive_Absence_Table($_REQUEST);
		echo $x;
		
	break;	
	
	case "updateWaiveAbsenceRecords":
		$result = $lc->updateWaiveAbsenceRecords($RecordID, $WaiveAbsent, $AbsentSession);
		echo $result ? "1":"0";
		
	break;
}

intranet_closedb();
?>