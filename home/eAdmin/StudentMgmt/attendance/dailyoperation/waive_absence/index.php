<?php
// Editing by 
/*
 * 2014-03-17 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance_WaiveAbsent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_WaiveAbsence";

$linterface = new interface_html();

$lc->retrieveSettings();

$TAGS_OBJ[] = array($Lang['StudentAttendance']['WaiveAbsence'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

#$PAGE_NAVIGATION[] = array($display_period);

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 

# session
$select_session ='<select id="DayType" name="DayType">';
$select_session.= '<option value="" selected>'.$i_status_all.'</option>';
if(in_array($lc->attendance_mode,array('0','2','3'))){
	$select_session.='<option value="'.PROFILE_DAY_TYPE_AM.'">'.$i_StudentAttendance_Slot_AM.'</option>';
}
if(in_array($lc->attendance_mode,array('1','2','3'))){
	$select_session.='<option value="'.PROFILE_DAY_TYPE_PM.'">'.$i_StudentAttendance_Slot_PM.'</option>';
}
$select_session.='</select>';

# date range
$current_month=date('n');
$current_year =date('Y');
if(!isset($StartDate) || $StartDate==""){
	if($current_month>=9){
		$StartDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
	}else{
		$StartDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
	}
}
if(!isset($EndDate) || $EndDate==""){
	$EndDate=date('Y-m-d');
}

# select type 
$select_type = '<select id="RecordType" name="RecordType">';
$select_type .= '<option value="">'.$Lang['General']['All'].'</option>';
$select_type .= '<option value="1">'.$Lang['StudentAttendance']['AbsenceWaived'].'</option>';
$select_type .= '<option value="2">'.$Lang['StudentAttendance']['AbsenceNotWaived'].'</option>';
$select_type .= '</select>';

?>
<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';

function getWaiveAbsenceTable()
{
	$('#TableDiv').html(loadingImg);
	var data = $('#form1').serialize();
	data += '&task=getWaiveAbsenceTable';
	$.post(
		'ajax_task.php',
		data,
		function(ReturnData){
			$('#TableDiv').html(ReturnData);
		}
	);
}

function checkForm()
{
	var rank_target = $('#rankTarget').val();
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');
	var valid = true;
	
	if(rank_target == 'form' || rank_target == 'class'){
		if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}else if(rank_target == 'student'){
		if($('select#studentID\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}
	
	if(!check_date_without_return_msg(startdateObj)){
		valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		valid = false;
		alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
	}
	
	return valid;
}

function submitForm()
{
	var valid = checkForm();
	if(valid){
		getWaiveAbsenceTable();
	}
}

function submitUpdate()
{
	if(confirm('<?=$i_SmartCard_Confirm_Update_Attend?>')){
		Block_Document();
		var recordIdObj = document.getElementsByName('RecordID[]');
		var recordIdAry = [];
		var waiveAbsentObj = document.getElementsByName('WaiveAbsent[]');
		var waiveAbsentAry = [];
		var absentSessionObj = document.getElementsByName('AbsentSession[]');
		var absentSessionAry = [];
		
		for(var i=0;i<recordIdObj.length;i++){
			recordIdAry.push(recordIdObj[i].value);
			waiveAbsentAry.push(waiveAbsentObj[i].checked?waiveAbsentObj[i].value:'0');
			absentSessionAry.push(absentSessionObj[i].options[absentSessionObj[i].selectedIndex].value);
		}
		
		$.post(
			'ajax_task.php',
			{
				'task':'updateWaiveAbsenceRecords',
				'RecordID[]': recordIdAry,
				'WaiveAbsent[]': waiveAbsentAry,
				'AbsentSession[]': absentSessionAry 
			},
			function(ReturnMsg){
				if(ReturnMsg == '1'){
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				}else{
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
				}
				UnBlock_Document();
				submitForm();
			}
		);
	}
}


function showResult(str, choice)
{
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
	
	var yearClassId = '';
	if(str == 'student2ndLayer'){
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		var delim = '';
		for(var i=0;i<options.length;i++){
			yearClassId += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/disciplinev12/reports/ajaxGetLive.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	if(str == 'form'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	}else if(str == 'class'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	}else if(str == 'student'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&onchange="+encodeURIComponent("showRankTargetResult('student2ndLayer','');");
	}else if(str == 'student2ndLayer'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&studentFieldId=studentID[]&studentFieldName=studentID[]&YearClassID="+yearClassId;
	}
	url += "&academicYearId=<?=$AcademicYearID?>";
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			if(str == 'student2ndLayer'){
				showIn = "spanStudent";	
			}
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
			
			if(str == 'student'){
				showRankTargetResult('student2ndLayer','');
			}
		}
	);
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student2ndLayer') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//document.getElementById('selectAllBtn01').disabled = true;
		//document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		$('#selectAllBtn01').hide();
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		//document.getElementById('selectAllBtn01').disabled = false;
		//document.getElementById('selectAllBtn01').style.visibility = 'visible';
		$('#selectAllBtn01').show();
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function printReport(path)
{
	var url = '';
	var winType = '10';
	var win_name = 'intranet_popup'+winType;
	
	var options = $('#generated_options').val();
	var options_ary = options.split('&');
	
	var tmp_form_html = '<form id="tmp_form" name="tmp_form" method="post" action="'+path+'" target="'+win_name+'">';

	for(var i=0;i<options_ary.length;i++){
		var option_parts = options_ary[i].split('=');
		tmp_form_html += '<input type="hidden" name="'+option_parts[0]+'" value="'+option_parts[1]+'" />';
	}
	
	tmp_form_html += '<input type="hidden" name="GroupByDay" value="1" />';
	tmp_form_html += '</form>';
	
	if($('#tmp_form').length == 0){
		$('form[name=form1]').after(tmp_form_html);
	}
	newWindow(url, winType);
	$('#tmp_form').submit();
	
	if($('#tmp_form').length > 0){
		$('#tmp_form').remove();
	}
}

$(document).ready(function(){
	showRankTargetResult($("#rankTarget").val(), '');
});
</script>
<br />
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">
<table class="form_table_v30" border="0" cellspacing="0" cellpadding="5" align="center">
	<!-- Target -->
	<tr valign="top">
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_general_target?><span class="tabletextrequire">*</span></td>
		<td>
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
							<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
							<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
							<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
						</select>
					</td>
					<td valign="top" nowrap>
						<!-- Form / Class //-->
						<span id='rankTargetDetail'>
						<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
						</span>
						<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
							
						<!-- Student //-->
						<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
							<select name="studentID[]" multiple size="5" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
						</span>
						
					</td>
				</tr>
				<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
			</table>
			<span id='div_Target_err_msg'></span>
			<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_general_startdate?><span class="tabletextrequire">*</span></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_general_enddate?><span class="tabletextrequire">*</span></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", $EndDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_Attendance_DayType?><span class="tabletextrequire">*</span></td>
		<td width="70%"class="tabletext">
			<?=$select_session?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_general_Type?><span class="tabletextrequire">*</span></td>
		<td width="70%"class="tabletext">
			<?=$select_type?>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm();") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<form name="MainForm" id="MainForm" action="" method="post">
<div id="TableDiv"></div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>