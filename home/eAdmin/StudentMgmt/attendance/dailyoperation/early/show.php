<?php
//using by 
/************************************************* Changes ***********************************************
 * 2019-06-27 Ray: Add teacher remark preset select box
 * 2016-06-29 Carlos: Added Submitted Prove Document Status field.
 * 2016-01-18 Carlos: Fixed fail to get AM early leave records bug while PM is early leave in the same day. 
 * 					  But it has one limitation - it can only get the latest leave time and leave location for early leave. 
 * 2015-06-18 Carlos: Do not display the Send SMS(no template) tool button if flag is off.
 * 2015-06-05 Omas: Add Export Mobile Button
 * 2015-04-23 (Carlos): Fix office remark array index shift problem after disable the input. 
 * 2015-02-11 (Carlos): Confirm time added confirm user name. Can edit teachers' remark. Added office remark. Added column [Last Modify].
 * 2014-10-09 (Roy)  : Added send push message option
 * 2014-09-05 (Bill)  : Added apply all to attendance status, fix short cut problem
 * 2014-07-24 (Carlos): Change Period selection to radio checkboxes
 * 2011-12-21 (Carlos): Added data column Gender
 *********************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = isset($_POST['TargetDate'])? $_POST['TargetDate'] : $_GET['TargetDate'];
$DayType = isset($_POST['DayType'])? $_POST['DayType'] : $_GET['DayType'];

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewEarlyLeaveStatus";

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();

### Short-cut for Daily Late / Daily Absent/ Daily Early Leave ###
/*
$arr_page = array(
					array(1,$i_SmartCard_DailyOperation_ViewClassStatus),
					array(2,$i_SmartCard_DailyOperation_ViewLateStatus),
					array(3,$i_SmartCard_DailyOperation_ViewAbsenceStatus)
				);
$pageShortCut_Selection = getSelectByArray($arr_page," name=\"pageShortCut\" onChange=\"changePage(this.value)\" ");
*/

### class used
$lword = new libwordtemplates();
$luser = new libuser();

$lc->retrieveSettings();
if ($TargetDate == "")
{
    $TargetDate = date('Y-m-d');
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
  	$display_period = $i_DayTypeAM;
  	$LeaveType = CARD_LEAVE_AM;
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
  	$LeaveType = CARD_LEAVE_PM;
    break;
  default : 
  	if ($lc->attendance_mode == 1) {
  		$display_period = $i_DayTypePM;
	  	$LeaveType = CARD_LEAVE_PM;
	  	$DayType = PROFILE_DAY_TYPE_PM;
  	}
  	else {
    	$display_period = $i_DayTypeAM;
	  	$LeaveType = CARD_LEAVE_AM;
	  	$DayType = PROFILE_DAY_TYPE_AM;
	  }
    break;
}

# order information
$default_order_by = " a.ClassName, a.ClassNumber+0, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	$order_by = "b.LeaveSchoolTime $order_str";
	
}

# get student array with parent using parent app
$studentWithParentUsingParentAppAry = $luser->getStudentWithParentUsingParentApp();

$confirmNameField = getNameFieldByLang2("u.");
# Get Confirmation status
$sql = "SELECT c.RecordID, c.EarlyConfirmed, c.EarlyConfirmTime, $confirmNameField as ConfirmUser FROM CARD_STUDENT_DAILY_DATA_CONFIRM as c 
			LEFT JOIN INTRANET_USER as u ON u.UserID=c.EarlyConfirmUserID 
               WHERE c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND c.RecordType = '".$lc->Get_Safe_Sql_Query($DayType)."'";
$temp = $lc->returnArray($sql,4);
list ($recordID, $confirmed , $confirmTime, $confirmUser) = $temp[0];

/*
$col_width = 150;
$table_confirm = "";
$x  = "<form name=\"form2\" id=\"form2\" method=\"post\" action=\"\" style=\"margin: 0px; padding: 0px;\">";
$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$i_StudentAttendance_Field_Date:</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$linterface->GET_DATE_PICKER("TargetDate", $TargetDate,"","yy-mm-dd","Mask_Confirmed_Date")."</td></tr>";
$x .= '<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
				<td class="tabletext" width="70%">
					<table border=0>
						<tr>
							<td class="dynCalendar_card_not_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$i_StudentAttendance_CalendarLegend_NotConfirmed.'
							</td>
							<td class="dynCalendar_card_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$i_StudentAttendance_CalendarLegend_Confirmed.'
							</td>
						</tr>
					</table>
				</td>
			</tr>';
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Slot:</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">$selection_period</td></tr>\n";

if($confirmed==1)
{
    $x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_LastConfirmedTime:</td>";
    $x .= "<td class=\"tabletext\" width=\"70%\">$confirmTime</td></tr>";    
}
else
{
	$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"></td>";
    $x .= "<td class=\"tabletext\" width=\"70%\">$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
$x .= "</table>\n";
$x .= '<input type="hidden" name="Msg" id="Msg" value="">';
$x .= "</form>";
*/
$table_confirm = $StudentAttendUI->Get_Confirm_Selection_Form("form2","",PROFILE_TYPE_EARLY,$TargetDate,$DayType,1);
$ExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_EARLY,'c.');

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$LastModifyUserNameField = getNameFieldByLang2("u.");
/*
$sql  = "SELECT 
					b.RecordID, 
					a.UserID,
	        ".getNameFieldWithClassNumberByLang("a.")."as name,
			a.Gender,
          b.LeaveSchoolTime,
          IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
          c.Reason,
          c.RecordStatus, 
          ".$ExpectedReasonField." ExpectedReason,
		  d.Remark,
		  c.OfficeRemark,
		  c.DateModified, 
		  $LastModifyUserNameField as LastModifyUser 
		FROM
			$card_log_table_name as b 
			LEFT JOIN INTRANET_USER as a 
			ON (b.UserID = a.UserID) 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON c.StudentID = a.UserID 
				AND c.RecordDate = '$TargetDate' 
				AND c.DayType = $DayType 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
			LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d 
			ON (a.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType') 
			LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifyBy
         WHERE 
         		b.DayNumber = '$txt_day' 
         		AND b.LeaveStatus = '".$LeaveType."' 
         		AND a.RecordType = 2 
         		AND a.RecordStatus IN (0,1,2)
         ORDER BY 
         		$order_by
		";
*/
$sql  = "SELECT 
					b.RecordID, 
					a.UserID,
	        ".getNameFieldWithClassNumberByLang("a.")."as name,
			a.Gender,
          b.LeaveSchoolTime,
          IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
          c.Reason,
          c.RecordStatus, 
		  c.DocumentStatus, 
          ".$ExpectedReasonField." ExpectedReason,
		  d.Remark,
		  c.OfficeRemark,
		  c.DateModified, 
		  $LastModifyUserNameField as LastModifyUser 
		FROM
			$card_log_table_name as b 
			LEFT JOIN INTRANET_USER as a 
			ON (b.UserID = a.UserID) 
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON c.StudentID = a.UserID 
				AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
				AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
			LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d 
			ON (a.UserID = d.StudentID AND d.RecordDate ='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND d.DayType='".$lc->Get_Safe_Sql_Query($DayType)."') 
			LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifyBy
         WHERE 
         		b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' 
         		AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
				AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
         		AND a.RecordType = 2 
         		AND a.RecordStatus IN (0,1,2)
         ORDER BY 
         		$order_by
		";
//debug_r($sql);
$result = $lc->returnArray($sql,13);
$StudentIDArr = Get_Array_By_Key($result,'UserID');
//debug_pr($result);


$table_attend = "";

$words = $lword->getWordListAttendance(3);
$hasWord = sizeof($words)!=0;
$reason_js_array = "";

$colspan = 13;

$x = $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1, 1);

# Teacher Remark Preset
$teacher_remark_js = "";
$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
    $teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
    foreach($teacher_remark_words as $key=>$word)
        $teacher_remark_words_temp[$key]= htmlspecialchars($word);
    $x .= $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "jArrayWordsTeacherRemark", 1);
}


$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td width=\"1\" class=\"tabletoplink\">#</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['Gender']."</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_StudentAttendance_LeaveSchoolTime</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Status
					<br>
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
		        		<option value=\"0\">".$Lang['StudentAttendance']['Present']."</option>
       					<option value=\"3\" SELECTED>".$Lang['StudentAttendance']['EarlyLeave']."</option>
	        		</select>
					<br>
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();")."
				</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\" align=\"center\">$i_SmartCard_Frontend_Take_Attendance_Waived<br /><input type=\"checkbox\" name=\"all_wavied\" onClick=\"(this.checked)?setAllWaived(document.form1,1):setAllWaived(document.form1,0)\"></td>";
$x .= "<td class=\"tabletoplink\" width=\"5%\" align=\"center\">".$Lang['StudentAttendance']['ProveDocument']."<br /><input type=\"checkbox\" name=\"master_document_status\" value=\"1\" onclick=\"SetAllDocumentStatus(this.checked);\"></td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\" nowrap>
				$i_Attendance_Reason
				<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllReason\" id=\"SetAllReason\" maxlength=\"255\" value=\"\">
				".$linterface->GET_PRESET_LIST("jArrayWords", "SetAllReasonIcon", "SetAllReason")."
				<br>
				".$linterface->Get_Apply_All_Icon("javascript:SetAllReason();")."
			</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['OfficeRemark']."</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_StudentGuardian[MenuInfo]</td>";
if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2)
{
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	$lsms	= new libsmsv2();
	
	//check sms system template status
	$template_status = $lsms->returnTemplateStatus("","STUDENT_ATTEND_EARLYLEAVE");
	$template_content = $lsms->returnSystemMsg("STUDENT_ATTEND_EARLYLEAVE");
	$template_status = $template_status and trim($template_content);
	if($template_status)
		$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_SMS_Send <input type='checkbox' name='all_sms' onClick=\"(this.checked)?setAllSend(document.form1,1):setAllSend(document.form1,0)\"></td>";
	$colspan+=1;
}	

if ($plugin['eClassApp']) {
	$x .= "<td width=\"5%\" class=\"tabletoplink\">".$Lang['AppNotifyMessage']['PushMessage']."<br /><input type=\"checkbox\" name=\"all_send_push_message\" onClick=\"$('.PushMessageCheckbox').attr('checked',this.checked);\"></td>";
	$colspan += 1;
}

$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['LastModify']."</td>";
$x .= "</tr>\n";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name, $gender, $leave_time, $leave_station, $reason, $record_status, $document_status, $ExpectedReason, $remark, $office_remark, $date_modified, $last_modify_user) = $result[$i];
        $css=($i%2==0)?"tablerow1":"tablerow2";
        $css = (trim($record_status) == "")? $css:"attendance_early";
        $select_status = "<select id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i,$i)\">\n";
        $select_status .= "<option value=\"0\">".$Lang['StudentAttendance']['Present']."</option>\n";
        $select_status .= "<option value=\"3\" SELECTED>".$Lang['StudentAttendance']['EarlyLeave']."</option>\n";
        $select_status .= "</select>\n";
        $select_status .= "<input name=\"record_id[$i]\" type=\"hidden\" value=\"$record_id\">\n";
        $select_status .= "<input name=\"user_id[$i]\" type=\"hidden\" value=\"$user_id\">\n";

        $reason_comp = "<input type=\"text\" name=\"reason$i\" id=\"reason$i\" class=\"textboxnum\" maxlength=\"255\" value=\"".htmlspecialchars($ExpectedReason,ENT_QUOTES)."\" style=\"background:$enable_color\">";

        $waived_option = "<input type=\"checkbox\" name=\"waived_{$user_id}\"".(($record_status == 1) ? " CHECKED" : " ") .">";
        $sms_option = "<input type=checkbox name=sms_{$user_id}>";
        
        $x .= "<tr class=\"$css\"><td class=\"tabletext\">".($i+1)."</td>";
        $x .= "<td class=\"tabletext\">$name</td>";
        $x .= "<td class=\"tabletext\">".$gender_word[$gender]."</td>";
        $x .= "<td class=\"tabletext\">$leave_time</td>";
        $x .= "<td class=\"tabletext\">$leave_station</td>";
        $x .= "<td class=\"tabletext\">$select_status</td>";
        $x .= "<td class=\"tabletext\" align=\"center\">$waived_option</td>";
        $x .= "<td class=\"tabletext\" align=\"center\">".$linterface->Get_Checkbox("DocumentStatus_".$user_id, "DocumentStatus_".$user_id, "1",$document_status == '1', 'ClassDocumentStatus', '')."</td>";
        $x .= "<td class=\"tabletext\" nowrap>$reason_comp";
        $x .= $linterface->GET_PRESET_LIST("jArrayWords", $i, "reason$i");
        $x .= "</td>";
        
        $remark_input = "<input type=\"text\" name=\"remark[]\" id=\"remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($remark, ENT_QUOTES)."\" />";
        if($hasTeacherRemarkWord)
        {
            $remark_input .= $linterface->GET_PRESET_LIST("getTeacherRemarkReasons($i)", '_teacher_remark_'.$i, 'remark'.$i);
        }

  		$office_remark_input = "<input type=\"text\" name=\"office_remark$i\" id=\"office_remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($office_remark, ENT_QUOTES)."\" />";
        $x .= "<td class=\"tabletext\" nowrap>";
        $x .= $remark_input;
        $x .= "</td>";
        $x .= "<td class=\"tabletext\">$office_remark_input</td>";
        
        $x .= "<td class=\"tabletext\" align=\"center\">";
        $x .= "<a onMouseMove=\"moveObject('ToolMenu2', event);\" onmouseover=\"retrieveGuardianInfo($user_id);\" onmouseout=\"closeLayer('ToolMenu2');\" href=\"#\">";
        $x .= "<img src=\"$image_path/icons_guardian_info.gif\" border=\"0\" alt=\"$button_select\"></a></td>";
        if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status) 
        	$x .= "<td>$sms_option</td>";
        	
        if ($plugin['eClassApp']) {
		  	$x .= "<td class=\"tabletext\">";
		  	if (in_array($user_id, $studentWithParentUsingParentAppAry)) {
		  		$x .= "<input type=\"checkbox\" name=\"SendPushMessage[]\" id=\"SendPushMessage_".$i."\" class=\"PushMessageCheckbox\" value=\"".$user_id."\">";
		  	} else {
		  		$x .= "---";
		  	}
			$x .= "</td>";
  		}
  		$x .= "<td class=\"tabletext\">";
		if($date_modified != '' && $last_modify_user!=''){
		  	$x .= $last_modify_user.$Lang['General']['On'].$date_modified;
		}else{
		  	$x .= Get_String_Display('');
		}
		$x .= "</td>";
        $x .= "</tr>\n";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=\"tablerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"$colspan\" align=\"center\">$i_StudentAttendance_NoEarlyStudents</td></tr>\n";
}
if($plugin['eClassApp']){
	$x .= '<tr><td class="tabletextremark" colspan = "'.$colspan.'"><span>'.$Lang['MessageCenter']['ExportMobileRemarks'].'</span></td></tr>';
}
$x .= "</table>\n";
$table_attend = $x;


# order info
$table_tool_time .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:orderByTime(1)\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" alt=\"$i_SmartCard_DailyOperation_OrderBy_InSchoolTime\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_SmartCard_DailyOperation_OrderBy_InSchoolTime
					</a>
				</td>";

$table_tool_class .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:orderByTime(0)\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" alt=\"$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber
					</a>
				</td>";


$table_tool = $order_by_time==1?$table_tool_class:$table_tool_time;

if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status)	
	$sms_button = "<a href=\"javascript:sendSMS(document.form1)\"  class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
else if($sys_custom['send_sms'] && $plugin['sms'] && $bl_sms_version >= 2)
{
	if (!$template_status)
		$sms_button = "<a href=\"javascript:alert('".$Lang['StudentAttendance']['SMSTemplateNotSet']."')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
	else
		$sms_button = "<a href=\"javascript:Prompt_No_SMS_Plugin_Warning()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
}

$table_tool .= "<td>
									$sms_button
								</td>";
								
if ($plugin['eClassApp']) {
	$push_message_button = "<a href=\"javascript:sendPushMessage()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $Lang['AppNotifyMessage']['PushMessage']  ."</a>&nbsp;";
	$table_tool .= "<td nowrap=\"nowrap\">
					$push_message_button
				</td>";
}

$toolbar = $linterface->GET_LNK_NEW("javascript:newWindow('insert_record.php?TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."',1)","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?TargetDate=".urlencode($TargetDate)."&DayType=".$lc->Get_Safe_Sql_Query($DayType)."&order_by_time=".urlencode($order_by_time),"","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
if ($plugin['eClassApp']) {
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExportMobile()",$Lang['MessageCenter']['ExportMobile'],"","","",0)."&nbsp;&nbsp;";
}

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_connection.js"></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     #ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
</style>

     <script language="JavaScript">
     isMenu = true;
     </script>
     <div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
     <div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

<script language="JavaScript" type="text/javascript">
function Prompt_No_SMS_Plugin_Warning()
{
	alert("<?=$i_SMS['jsWarning']['NoPluginWarning']?>");
}
function getTeacherRemarkReasons()
{
    return jArrayWordsTeacherRemark;
}
var reasonSelection = Array();
function temp_reasonSelection(i, remark)
{
	str = "<table width=100% border=0 cellpadding=1 cellspacing=1>";
    if(remark!="")
    {
	    remark = remark.replace("'", "&#039;");
        str += "<tr><td><font color=red><b>[<?=$i_Attendance_Others?>]</b></font></td></tr>";
    	str += "<tr><td><table border=0 cellspacing=0 cellpadding=0 width=100%><tr><td valign=top width=8><font color=red> - </font></td><td align=left><a href='javascript:putBack(document.form1.reason"+i+",\""+remark+"\");'><font color=red>"+remark+"</font></a><input type='hidden' id='rrr_remark_"+i+"' value='"+remark+"'></td></tr></table><Br></td></tr>";
   	}
   	str += "<tr><td><b>[<?=$i_Attendance_Standard?>]</b></td></tr>";

   	<?
    for ($j=0; $j<sizeof($words); $j++)
    {
		$temp = addslashes($words[$j]);
		$temp2 = addslashes($temp);
	?>
		str += "<tr><td><table border=0 cellpaddin=0 cellspacing=0 width=100%><tr><td valign=top width=8> - </td><td align=left><a href='javascript:putBack(document.form1.reason"+i+",\"rrr_"+i+"_<?=$j?>\");'><?=$temp?></a><input type='hidden' id='rrr_"+i+"_<?=$j?>' value='<?=$temp?>'></td></tr></table></td></tr>";
    <?
	}
	?>
    str += "</table>";
    
    reasonSelection[i] = "<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table width=100% border=0 cellspacing=0 cellpadding=3><Td class=tipbg><table border=0 cellspacing=0 cellpadding=0><tr><td><input type=button value=' X ' onClick=hideMenu('ToolMenu')></td></tr></table></td></tr><tr><td class=tipbg valign=top><font size=-2>"+str+"</font></td></tr></table></td></tr></table>";
	
    return reasonSelection[i];
}

function setAllSend(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("sms_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}

function sendSMS(formObj)
{
	var sms_no=0;
	for(i=0;i<formObj.elements.length;i++)
	{
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("sms_")>-1){
				if(obj.checked)	sms_no = 1;
			}
		}
	}
	
	if(sms_no)
	{
		formObj.action = "../confirm_send_sms.php";
		formObj.submit();
	}
	else
	{
		alert("<?=$i_SMS_no_student_select?>");	
	}
}

function sendPushMessage() {
	var SendPushMessage = document.getElementsByName('SendPushMessage[]');
	var SendPushMessageChecked = false;
	for (var i=0; i< SendPushMessage.length; i++) {
		if (SendPushMessage[i].checked) {
			SendPushMessageChecked = true;
			break;
		}
	}
	
	if (SendPushMessageChecked == true) {
		document.getElementById('form1').action = "../send_push_message.php";
		document.getElementById('form1').submit();
	}
	else 
		alert("<?=$i_SMS_no_student_select?>");
}	

<?//=$reason_js_array?>
<!--

function openPrintPage()
{
        newWindow("show_print.php?DayType=<?=urlencode($DayType)?>&TargetDate=<?=urlencode($TargetDate)?>&order_by_time=<?=urlencode($order_by_time)?>",4);
}
function showSelection(i, allowed, remarkObjName)
{
		remarkObj = document.getElementById(remarkObjName);
	if(remarkObj!=null)
		remark = remarkObj.value;
	else remark = "";	
         if (allowed == 1)
         {
             showMenu('ToolMenu',temp_reasonSelection(i,remark));
         }
}
/*
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
*/
function putBack(obj, valueObjName)
{
	     valueObj = document.getElementById(valueObjName);

		 if(valueObj!=null)
         	obj.value = valueObj.value;
         else obj.value="";	     
         hideMenu('ToolMenu');
}
function setReasonComp(value, txtComp, hiddenFlag, index)
{
         if (value==3)
         {
             txtComp.disabled = false;
             txtComp.style.background='<?=$enable_color?>';
             hiddenFlag.value = 1;
             
             if($('#office_remark'+index).length>0){
				$('#office_remark'+index).attr('disabled','');
			}
         }
         else
         {
             txtComp.disabled = true;
             txtComp.value="";
             txtComp.style.background='<?=$disable_color?>';
             hiddenFlag.value = 0;
             
             if($('#office_remark'+index).length>0){
				$('#office_remark'+index).attr('disabled','disabled');
			 }
         }
}
function resetForm(formObj){
	formObj.reset();
  	hideMenu('ToolMenu');
	resetFields(formObj);	
}

function resetFields(formObj){
	status_list = document.getElementsByTagName('SELECT');
	if(status_list==null) return;
	
	for(i=0;i<status_list.length;i++){
		s = status_list[i];
		if(s==null) continue;
		reasonObj = eval('formObj.reason'+i);
		if(s.selectedIndex==1){
			if(reasonObj!=null){
				reasonObj.disabled=false;
				reasonObj.style.background='<?=$enable_color?>';

			}
		}else{
			if(reasonObj!=null){
				reasonObj.value="";
				reasonObj.disabled=true;
				reasonObj.style.background='<?=$disable_color?>';
			}
		}

	}
}
function orderByTime(v){
	fObj = document.form1;
	if(fObj==null ) return;
	orderByTimeObj = fObj.order_by_time;
	if(orderByTimeObj==null) return;
	orderByTimeObj.value = v;
	fObj.action="";
	fObj.submit();
	
}
function setAllWaived(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("waived_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}
function moveToolTip2(lay, FromTop, FromLeft){

     if (tooltip_ns6) {

         var myElement = document.getElementById(lay);

          myElement.style.left = (FromLeft + 10) + "px";

          myElement.style.top = (FromTop + document.body.scrollTop) + "px";

     }

     else if(tooltip_ie4) {
          eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
     }
 	else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

     if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));
   
}
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveGuardianInfo(UserID)
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("ToolMenu2");
        
        showMenu("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        myElement.style.display = 'block';
				myElement.style.visibility = 'visible';
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getGuardianInfo.php?targetUserID=" + UserID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function changePage(val)
{
	var obj = document.form1;
	var path = "/home/eAdmin/StudentMgmt/attendance/dailyoperation";
	var period = document.form1.period.value;
	var date = document.form1.TargetDate.value;
	
	if(val == 1){
		obj.action = path+"/class/class_status.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 2){
		obj.action = path+"/late/showlate.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 3){
		obj.action = path+"/absence/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
}

function moveObject( obj, e, moveLeft ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 8;
	var objHolder = obj;
	var moveDistance = 0;
	
	obj = document.getElementById(obj);
	if (obj==null) {return;}
	
	if (document.all) {
		var ScrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
		var ScrollHeight = document.documentElement.scrollTop || document.body.scrollTop;
		tempX = event.clientX + ScrollLeft;
		tempY = event.clientY + ScrollHeight;
	}
	else {
		tempX = e.pageX
		tempY = e.pageY
	}

	if(moveLeft == undefined) {
		moveDistance = 300;		
	} else {
		moveDistance = moveLeft;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
}

function closeLayer(LayerID) {
	var obj = document.getElementById(LayerID);
	obj.style.display = 'none';
	obj.style.visibility = 'hidden';
}

function SetAllReason() {
	var StatusSelection = document.getElementsByName("drop_down_status[]");
	var ApplyReason = document.getElementById("SetAllReason").value;
	for (var i=0; i< StatusSelection.length; i++) {
		if (StatusSelection[i].selectedIndex == 1) // early leave
		{
		  document.getElementById('reason'+i).value = ApplyReason;
		} 
	}
}
function SetAllAttend() {
	var AttendValue = document.getElementById("drop_down_status_all").value;
	var AttendSelection = document.getElementsByName("drop_down_status[]").length;
	for (var i=0; i< AttendSelection; i++) {
 		document.getElementById('drop_down_status['+i+']').value = AttendValue;
 		
 		var targetReasonEle = document.getElementById('reason'+i);
 		var targetEditEle = document.getElementsByName('editAllowed'+i)[0];

 		setReasonComp(AttendValue, targetReasonEle, targetEditEle, i);
	}
}

function SetAllDocumentStatus(checked)
{
	$('input.ClassDocumentStatus').attr('checked',checked);
}

function goExportMobile(){
	var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php';
	$('form#form2').attr('action', url ).submit();
	$('form#form2').attr('action', '' );
}
-->
</script>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table_confirm?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<?
if($confirmed==1)
{
?>
        <tr>
        	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
        	<td class="tabletext" width="70%"><?=$confirmTime.($confirmUser!=''?'&nbsp;('.$confirmUser.')':'')?></td>
        </tr>
<?
}
else
{
?>
				<tr>
        	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
        	<td class="tabletext" width="70%"><?=$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record?></td>
        </tr>
<?
}
?>
			<?=$StudentAttendUI->Get_Class_Status_Shortcut(PROFILE_TYPE_EARLY)?>
			</table>	
		</td>
	</tr>
</table>
<form name="form1" id="form1" method="post" action="show_update.php" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" onMouseMove="overhere()">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr class="table-action-bar">
					<td valign="bottom" align="left">
						<table border=0>
							<tr>
								<td class="attendance_early" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Confirmed']?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<?=$table_tool?>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$table_attend?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
				    <? if (sizeof($result)!=0) { ?>
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "AlertPost(document.form1,'show_update.php','$i_SmartCard_Confirm_Update_Attend?')") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<? } ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input id="DayType" name="DayType" type="hidden" value="<?=escape_double_quotes($DayType)?>">
<input name="TargetDate" type="hidden" value="<?=escape_double_quotes($TargetDate)?>">
<input name=PageLoadTime type=hidden value="<?=time()+1?>">
<input name="order_by_time" type="hidden" value="<?=escape_double_quotes($order_by_time)?>">
<? for ($i=0; $i<sizeof($result); $i++) { ?>
<input type="hidden" name="editAllowed<?=$i?>" value="1">
<? } ?>
<input type="hidden" name="this_page_title" value="<?=$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus?>">
<input type="hidden" name="this_page_nav" value="PageDailyOperation_ViewEarlyLeaveStatus">
<input type="hidden" name="this_page" value="early/show.php?TargetDate=<?=urlencode($TargetDate)?>&DayType=<?=urlencode($DayType)?>">
<input type="hidden" name="TemplateCode" value="STUDENT_ATTEND_EARLYLEAVE">
</form>
<form id="form2" name="form2" method="post" action="" >
<input type="hidden" name="StudentIDArr" value="<?=rawurlencode(serialize($StudentIDArr))?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>