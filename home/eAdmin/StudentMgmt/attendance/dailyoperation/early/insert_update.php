<?php
// Editing by 
/*
 * 2016-06-29 (Carlos): Set submit prove document status. 
 * 2015-07-03 (Carlos): Fixed js issue. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();


$lbdb = new libdb();
$lbdb->Start_Trans();
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record) + 0;

$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
$card_student_daily_log .= $txt_year."_";
$card_student_daily_log .= $txt_month;

$leave_hour = substr($early_leave_time, 0, 2);


if ($DayType == PROFILE_DAY_TYPE_AM)	# Check AM Leave
	$leave_status = 1;
else if ($DayType == PROFILE_DAY_TYPE_PM)	# Check PM Leave
	$leave_status = 2;	
else
	$leave_status = 1;


if($leave_station == "")	# Check Leave School Station is Empty or not
{
	$sql_leave_station = "LeaveSchoolStation = NULL";
}
else
{
	$sql_leave_station = "LeaveSchoolStation = '". $leave_station ."'";
}


$sql = "
		UPDATE 
			$card_student_daily_log
		SET
			LeaveSchoolTime = '$early_leave_time',
			$sql_leave_station,				
			LeaveStatus = $leave_status,
			DateModified = NOW()
		WHERE
			UserID IN ($student_list) AND
			DayNumber = $txt_day
		";
$Result['SetLeaveStatus'] = $lbdb->db_db_query($sql);
$x = 1;

$student_list = explode(',',$student_list);
for ($i=0; $i< sizeof($student_list); $i++) {
	$my_user_id = $student_list[$i];
	$Result[$my_user_id.'SetProfile'] = $lc->Set_Profile($my_user_id,$TargetDate,$DayType,PROFILE_TYPE_EARLY,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,"|**NULL**|",true);
}

$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='$DocumentStatus' WHERE RecordDate='$TargetDate' AND StudentID IN ('".implode("','",$student_list)."') AND DayType='$DayType' AND RecordType='".PROFILE_TYPE_EARLY."' ";
$Result['SetProveDocumentStatus'] = $lc->db_db_query($sql);

//debug_r($sql); die;
if (!in_array(false,$Result)) {
	$lbdb->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['EarlyLeaveCreateSuccess'];
}
else {
	$lbdb->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['EarlyLeaveCreateFail'];
}

intranet_closedb();
?>
<script language="Javascript">
window.opener.document.getElementById('Msg').value = '<?=$Msg?>';
window.opener.document.getElementById('form2').submit();

//window.opener.location.href = window.opener.location.href + '&Msg=<?=urlencode($Msg)?>';
window.close();
</script>