<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader_admin.php");
intranet_opendb();

$lbdb = new libdb();

$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
$card_student_daily_log .= $txt_year."_";
$card_student_daily_log .= $txt_month;

$leave_hour = substr($early_leave_time, 0, 2);


if ($Period == 1)	# Check AM Leave
		$leave_status = 1;
		
else if ($Period == 2)	# Check PM Leave
			$leave_status = 2;
		
else
		$leave_status = 1;


if($leave_station == "")	# Check Leave School Station is Empty or not
{
	$sql_leave_station = "LeaveSchoolStation = NULL";
}
else
{
	$sql_leave_station = "LeaveSchoolStation = '". $leave_station ."'";
}


$sql = "
		UPDATE 
				$card_student_daily_log
		SET
				LeaveSchoolTime = '$early_leave_time',
				$sql_leave_station,				
				LeaveStatus = $leave_status,
				DateModified = NOW()
		WHERE
				UserID IN ($student_list) AND
				DayNumber = $txt_day
		";

$x = 1;


$lbdb->db_db_query($sql);
?>

<?php
intranet_closedb();
include_once("../../../../templates/filefooter.php");
?>

<SCRIPT LANGUAGE=JAVASCRIPT>
opener.location.href = opener.location.href + '&msg=<?=$x?>';
self.close();
</SCRIPT>
