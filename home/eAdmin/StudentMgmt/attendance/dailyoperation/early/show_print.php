<?php
// editing by 
/********************************************* Changes *************************************************
 * 2016-06-29 Carlos: Added Submitted Prove Document Status field.
 * 2016-01-18 Carlos: Fixed fail to get AM early leave records bug while PM is early leave in the same day. 
 * 					  But it has one limitation - it can only get the latest leave time and leave location for early leave. 
 * 2015-02-12 (Carlos): Added Teacher's remark and Office Remark fields
 * 2011-12-21 (Carlos): Added data column Gender
 *******************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
###period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM: $display_period = $i_DayTypeAM;
              break;
    case PROFILE_DAY_TYPE_PM: $display_period = $i_DayTypePM;
              break;
    default : $display_period = $i_DayTypeAM;
              $DayType = PROFILE_DAY_TYPE_AM;
              break;
}

# order information
//$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

# order information
$default_order_by = " a.ClassName, a.ClassNumber+0, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	$order_by = "b.LeaveSchoolTime $order_str";
	
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Early Leave
{
	/*
	$sql  = "SELECT b.RecordID, a.UserID,
	                ".getNameFieldByLang("a.")."as name,
	                a.ClassName,
	                a.ClassNumber,
					a.Gender,
	                b.LeaveSchoolTime,
	                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
	                c.Reason,
	                c.RecordStatus,
					d.Remark,
					c.OfficeRemark 
	                FROM
	                $card_log_table_name as b
	                   LEFT JOIN INTRANET_USER as a ON b.UserID = a.UserID
	                   LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
	                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
	                           AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
						LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
						ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType' 
	                WHERE b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_AM."'
	                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
	         ORDER BY $order_by
	";
	*/
	$sql  = "SELECT b.RecordID, a.UserID,
	                ".getNameFieldByLang("a.")."as name,
	                a.ClassName,
	                a.ClassNumber,
					a.Gender,
	                b.LeaveSchoolTime,
	                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
	                c.Reason,
	                c.RecordStatus,
					c.DocumentStatus,
					d.Remark,
					c.OfficeRemark 
	                FROM
	                $card_log_table_name as b
	                   LEFT JOIN INTRANET_USER as a ON b.UserID = a.UserID
	                   LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
	                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = '$DayType'
	                           AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
						LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
						ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType' 
	                WHERE b.DayNumber = '$txt_day' AND c.RecordDate = '$TargetDate' AND c.DayType = '$DayType'
	                      AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
	                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
	         ORDER BY $order_by
	";
}
else     # Check PM Early Leave
{
	/*
	$sql  = "SELECT b.RecordID, a.UserID,
	                ".getNameFieldByLang("a.")."as name,
	                a.ClassName,
	                a.ClassNumber,
					a.Gender,
	                b.LeaveSchoolTime,
	                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
	                c.Reason,
	                c.RecordStatus,
					d.Remark,
					c.OfficeRemark 
	                FROM
	                $card_log_table_name as b
	                   LEFT JOIN INTRANET_USER as a ON b.UserID = a.UserID
	                   LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
	                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
	                           AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
						LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
						ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType' 
	                WHERE b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_PM."'
	                AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
	         ORDER BY $order_by
	";
	*/
	$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
				a.Gender,
                b.LeaveSchoolTime,
                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
                c.Reason,
                c.RecordStatus,
				c.DocumentStatus,
				d.Remark,
				c.OfficeRemark 
                FROM
                $card_log_table_name as b
                   LEFT JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = '$DayType'
                           AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
					LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
					ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType' 
                WHERE b.DayNumber = '$txt_day' AND c.RecordDate = '$TargetDate' AND c.DayType = '$DayType' 
                  AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY $order_by
	";
}

$result = $lc->returnArray($sql,12);

$table_attend = "";

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
$x .= "<tr>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">#</td>";
$x .= "	<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_ClassName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserClassNumber</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['Gender']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_StudentAttendance_LeaveSchoolTime</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_StudentAttendance_Field_CardStation</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Status</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Waived</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['ProveDocument']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\">$i_Attendance_Reason</td>
				<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['OfficeRemark']."</td>
		</tr>";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
    list($record_id, $user_id, $name,$class_name,$class_number,$gender, $leave_time, $leave_station, $reason,$record_status, $document_status, $remark, $office_remark) = $result[$i];
    $str_status = $i_StudentAttendance_Status_EarlyLeave;
    $str_reason = $reason;
    $waived_option = $record_status == 1? $i_general_yes:$i_general_no;
    
    $x .= "<tr>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$name</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_name</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_number</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".$gender_word[$gender]."</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$leave_time</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$leave_station</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$str_status</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$waived_option</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".($document_status=='1'?$Lang['General']['Yes']:$Lang['General']['No'])."</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\">$str_reason&nbsp;</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";
    $x .= "<td class=\"eSporttdborder eSportprinttext\">$office_remark&nbsp;</td>";
    $x .= "</tr>";
}
if (sizeof($result)==0)
{
	$x .= "<tr class=\"tablerow2\">";
    $x .= "<td class=\"tabletext\" colspan=\"13\" align=\"center\">$i_StudentAttendance_NoEarlyStudents</td>";
    $x .= "</tr>";
}
$x .= "</table>\n";
$table_attend = $x;
$i_title = "$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus $TargetDate ($display_period)";
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><?=$i_title?></td>
	</tr>
</table>
<?=$table_attend?>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>