<?php
// editing by 
/********************************************* Changes *************************************************
 * 2016-06-29 Carlos: Added Submitted Prove Document Status field.
 * 2016-01-18 Carlos: Fixed fail to get AM early leave records bug while PM is early leave in the same day. 
 * 					  But it has one limitation - it can only get the latest leave time and leave location for early leave. 
 * 2015-02-12 (Carlos): Added Teacher's remark and Office Remark fields
 * 2011-12-21 (Carlos): Added data column Gender
 *******************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
        case PROFILE_DAY_TYPE_AM: $display_period = $i_DayTypeAM;
                                                break;
        case PROFILE_DAY_TYPE_PM: $display_period = $i_DayTypePM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	$order_by = "b.LeaveSchoolTime $order_str";
	
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Early Leave
{
	$LeaveStatus = CARD_LEAVE_AM;
}
else     # Check PM Early Leave
{
	$LeaveStatus = CARD_LEAVE_PM;
}
/*
$sql  = "SELECT 
					a.UserLogin,
					b.RecordID, 
					a.UserID,
          ".getNameFieldByLang("a.")."as name,
          a.ClassName,
          a.ClassNumber,
		  a.Gender,
          b.LeaveSchoolTime,
          IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
          c.Reason,
          c.RecordStatus,
		  d.Remark,
		  c.OfficeRemark 
		FROM
			$card_log_table_name as b
			LEFT JOIN INTRANET_USER as a ON b.UserID = a.UserID
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON 
				c.StudentID = a.UserID 
				AND c.RecordDate = '$TargetDate' 
				AND c.DayType = $DayType
				AND c.RecordType = '".PROFILE_TYPE_EARLY."'
		LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
			ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType' 
        WHERE 
         	b.DayNumber = '$txt_day' AND b.LeaveStatus = '".$LeaveStatus."'
          AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
        ORDER BY $order_by
";
*/
$sql  = "SELECT 
					a.UserLogin,
					b.RecordID, 
					a.UserID,
          ".getNameFieldByLang("a.")."as name,
          a.ClassName,
          a.ClassNumber,
		  a.Gender,
          b.LeaveSchoolTime,
          IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
          c.Reason,
          c.RecordStatus,
		  c.DocumentStatus,
		  d.Remark,
		  c.OfficeRemark 
		FROM
			$card_log_table_name as b
			LEFT JOIN INTRANET_USER as a ON b.UserID = a.UserID
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c 
			ON 
				c.StudentID = a.UserID 
				AND c.RecordDate = '$TargetDate' 
				AND c.DayType = '$DayType' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."'
		LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
			ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType' 
        WHERE 
         	b.DayNumber = '$txt_day' 
			AND c.RecordDate = '$TargetDate' 
			AND c.DayType = '$DayType'
			AND c.RecordType = '".PROFILE_TYPE_EARLY."'
          AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
        ORDER BY $order_by
";

$result = $lc->returnArray($sql,13);

$lexport = new libexporttext();
if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
	$exportColumn[] = $i_UserLogin;
$exportColumn[] = $i_UserName;
$exportColumn[] = $i_ClassName;
$exportColumn[] = $i_UserClassNumber;
$exportColumn[] = $Lang['StudentAttendance']['Gender'];
$exportColumn[] = $i_StudentAttendance_LeaveSchoolTime;
$exportColumn[] = $i_StudentAttendance_Field_CardStation;
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_Status;
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_Waived;
$exportColumn[] = $Lang['StudentAttendance']['ProveDocument'];
$exportColumn[] = $i_Attendance_Reason;
$exportColumn[] = $Lang['StudentAttendance']['iSmartCardRemark'];
$exportColumn[] = $Lang['StudentAttendance']['OfficeRemark'];
$exportColumn[] = $i_StudentGuardian['MenuInfo'];

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
    list($UserLogin, $record_id, $user_id, $UserName,$class_name,$class_number,$gender, $leave_time, $leave_station, $reason,$record_status,$document_status, $remark, $office_remark) = $result[$i];
    $str_status = $i_StudentAttendance_Status_EarlyLeave;
    $str_reason = $reason;
    $waived_option = $record_status == 1? $i_general_yes:$i_general_no;
  
  // get guardian info
	$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
	$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
	$sql = "SELECT
					  $main_content_field,
					  Relation,
					  Phone,
					  EmPhone,
					  IsMain
					FROM
					  $eclass_db.GUARDIAN_STUDENT
					WHERE
					  UserID = '".$user_id."'
					ORDER BY
					  IsMain DESC, Relation ASC
					";
	//debug_r($sql);
	$temp = $lc->returnArray($sql,4);
	if (sizeof($temp)==0)
		$GuardianInfo = "--";
	else
	{
		$GuardianInfo = "";
    for($j=0; $j<sizeof($temp); $j++)
    {
			list($name, $relation, $phone, $em_phone) = $temp[$j];
			$GuardianInfo .= ($j+1).". ".$name." (".$ec_guardian[$relation].") (".$i_StudentGuardian_Phone.") ".$phone." (".$i_StudentGuardian_EMPhone.") ".$em_phone." ";
	  }
	}
  
  if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
		$ExportDetail[$i][] = $UserLogin; 
	$ExportDetail[$i][] = $UserName; 
	$ExportDetail[$i][] = $class_name; 
	$ExportDetail[$i][] = $class_number; 
	$ExportDetail[$i][] = $gender_word[$gender];
	$ExportDetail[$i][] = $leave_time; 
	$ExportDetail[$i][] = $leave_station; 
	$ExportDetail[$i][] = $str_status; 
	$ExportDetail[$i][] = $waived_option; 
	$ExportDetail[$i][] = ($document_status=='1'?$Lang['General']['Yes']:$Lang['General']['No']);
	$ExportDetail[$i][] = $str_reason; 
	$ExportDetail[$i][] = $remark;
	$ExportDetail[$i][] = $office_remark;
	$ExportDetail[$i][] = $GuardianInfo; 
}

$export_content = $lexport->GET_EXPORT_TXT($ExportDetail, $exportColumn);

$export_content_final = $i_SmartCard_DailyOperation_ViewEarlyLeaveStatus;
$export_content_final .= " $TargetDate ($display_period)\r\n";

if (sizeof($result)==0)
	$export_content_final .= "$i_StudentAttendance_NoEarlyStudents\r\n";
else
	$export_content_final .= $export_content;

intranet_closedb();

$filename = "showearlyleave-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>