<?php
//using by: 

##################################
# change log:
#
# 2016-02-01 Kenneth: added Message Template and Auto-fill Item
# 2014-10-09 Roy: file created, to let user confirm sending push message to late/absent/early leave studnet's parent
##################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# classes:
$linterface = new interface_html();
$LIDB = new libdb();
$lc = new libcardstudentattend2();
$luser = new libuser();
$libAppTemplate = new libeClassApp_template();
# Init libeClassApp_template

$module='StudentAttendance';
//$section='AbsentLateEarlyLeave';

$libAppTemplate->setModule($module);
$libAppTemplate->setSection($section);

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = $_REQUEST['this_page_nav'];

$cannotSendArray = array();
$canSendArray = array();
$can_send = true;
$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();

# build data:
$TargetUsers = $_REQUEST['SendPushMessage'];
for ($i = 0; $i < sizeOf($TargetUsers); $i++) {
	$studentId = $TargetUsers[$i];
	$lu = new libuser($studentId);
	$appParentIdAry = $lu->getParentUsingParentApp($studentId);
	if (in_array($studentId, $studentWithParentUsingAppAry)) {
		$canSendArray[$i]['name'] = $lu->UserName();
		$canSendArray[$i]['classname'] = $lu->ClassName;
		$canSendArray[$i]['classnumber'] = $lu->ClassNumber;
//		$canSendArray[$i]['parentname'] = implode("<br/>", BuildMultiKeyAssoc($appParentAry, "ParentID", array("OutputName"), 1, 0));
		$parentName = "";
		foreach ($appParentIdAry as $appParentId) {
			$lu_parent = new libuser($appParentId);
			if ($parentName != "") {
				$parentName .= "<br/>";
			}
			$parentName .= $lu_parent->UserName();
		}
		$canSendArray[$i]['parentname'] = $parentName;
		
		$studentIds[] = $studentId;
	} else {
		$cannotSendArray[$i]['name'] = $lu->UserName();
		$cannotSendArray[$i]['classname'] = $lu->ClassName;
		$cannotSendArray[$i]['classnumber']	= $lu->ClassNumber;
		$cannotSendArray[$i]['reason'] = $Lang['AppNotifyMessage']['ePayment']['ParentNotUsingEClassApp'];
	}
	
	
}

# message title and content
if (strpos($this_page,'late') !== false) {
	$messageTitle = $Lang['AppNotifyMessage']['eAttendance']['Late']['Title'];
	$messageContent = $Lang['AppNotifyMessage']['eAttendance']['Late']['Content'];
} else if (strpos($this_page,'absence') !== false) {
	$messageTitle = $Lang['AppNotifyMessage']['eAttendance']['Absent']['Title'];
	$messageContent = $Lang['AppNotifyMessage']['eAttendance']['Absent']['Content'];
} else if (strpos($this_page,'early') !== false) {
	$messageTitle = $Lang['AppNotifyMessage']['eAttendance']['EarlyLeave']['Title'];
	$messageContent = $Lang['AppNotifyMessage']['eAttendance']['EarlyLeave']['Content'];
}
$messageContent = str_replace("[DateInvolved]", $TargetDate, $messageContent);
# cant sent table content:
//$invalid_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\"><tr>";
//$invalid_table.= "<td>";
//$invalid_table.= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"><font color='red'> {$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
//$invalid_table.= "</td></tr></table>";
//
//$cannot_send_table_content = "";
//$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
//$x .= "<tr class=\"tabletop\">
//<td class=\"tabletoplink\" width=\"30%\">$i_UserName</td>
//<td class=\"tabletoplink\" width=\"13%\">$i_ClassName</td>
//<td class=\"tabletoplink\" width=\"12%\">$i_ClassNumber</td>
//<td class=\"tabletoplink\" width=\"55%\">$i_Attendance_Reason</td></tr>\n";
//
//$css = "tablerow1";   
//foreach($cannotSendArray as $this_ary) {
//	if(trim($this_ary['name'])=="")	continue;
//	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
//	$x .= "<tr class=\"$css\">";
//	$x .= "<td class=\"tabletext\">". $this_ary['name'] ."</td>";
//	$x .= "<td class=\"tabletext\">". $this_ary['classname'] ."</td>";
//	$x .= "<td class=\"tabletext\">". $this_ary['classnumber'] ."</td>";
//	$x .= "<td class=\"tabletext\">". $this_ary['reason'] ."</td>";
//	$x .= "</tr>\n";
//}
//$x .= "</table>\n";
//$cannot_send_table_content = $invalid_table.$x;


# can sent table content:
$invalid_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\"><tr>";
$invalid_table.= "<td>";
$invalid_table.= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
$invalid_table.= "</td></tr></table>";

$can_send_table_content = "";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\">
<td class=\"tabletoplink\" width=\"30%\">$i_UserName</td>
<td class=\"tabletoplink\" width=\"13%\">$i_ClassName</td>
<td class=\"tabletoplink\" width=\"12%\">$i_ClassNumber</td>
<td class=\"tabletoplink\" width=\"55%\">".$Lang['General']['ParentUsingEClassApp']."</td></tr>\n";

$css = "tablerow1";   
foreach($canSendArray as $this_ary) {
	if(trim($this_ary['name'])=="")	continue;
	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
	$x .= "<tr class=\"$css\">";
	$x .= "<td class=\"tabletext\">". $this_ary['name'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classname'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classnumber'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['parentname'] ."</td>";
	$x .= "</tr>\n";
}
$x .= "</table>\n";
$can_send_table_content = $invalid_table.$x;

$templateName[0][0] = '1'; 
$templateName[1][0] = '2'; 
$templateName[2][0] = '3'; 
$templateName[0][1] = 'template 1'; 
$templateName[1][1] = 'template 2'; 
$templateName[2][1] = 'template 3'; 


# UI components:
$back_button = $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = '".$this_page."';");
$send_button = $linterface->GET_ACTION_BTN($button_send, "submit");

# layout start:
$TAGS_OBJ[] = array($this_page_title, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.TargetAjaxScript = 'ajax_send_reminder_update.php';
});
function checkForm() {
	var title = document.getElementById('PushMessageTitle');
	var content = document.getElementById('MessageContent');
	if (title.value == "") {
		alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>");
		return false;
	} else if (content.value == "") {
		alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription']?>");
		return false;
	} else {
		return true;
	}
	
}
</script>

<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<br />
<form name="form1" method="post" action="send_push_message_update.php" onsubmit="return checkForm();">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['SMS']['MessageTemplates']?>
						</td>
						<td class="tabletext" width="70%">
							<?=$libAppTemplate->getMessageTemplateSelectionBox(true);?>
							<span id="AjaxStatus"></span>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['AppNotifyMessage']['Title']?> <span class="tabletextrequire">*</span>
						</td>
						<td class="tabletext" width="70%">
							<?=$linterface->GET_TEXTAREA("PushMessageTitle", $messageTitle, $taCols=70, $taRows=2)?>
							
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['AppNotifyMessage']['Description']?> <span class="tabletextrequire">*</span>
						</td>
						<td class="tabletext" width="70%">
							<?=$linterface->GET_TEXTAREA("MessageContent", $messageContent)?>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['eClassApp']['AutoFillItem']?>
						</td>
						<td class="tabletext" width="70%">
							<?=$libAppTemplate->getMessageTagSelectionBox();?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
				  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				  </tr>
					<tr>
					  <td align="center" colspan="2">
							<?= $send_button."&nbsp;".$back_button ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td><?=$can_send_table_content?></td>
		</tr>
	</table>
	<input type="hidden" name="this_page" value="<?=$this_page?>">
	<input type="hidden" name="sms_info" value="<?=$sms_info?>">
	<input type="hidden" name="TargetDate" id="TargetDate" value="<?=$TargetDate?>">
	<input type="hidden" name="DayType" id="DayType" value="<?=$DayType?>">
	<input type="hidden" name="order_by_time" value="<?=$order_by_time?>">
	<input type="hidden" name="studentIds" value="<?=implode(",", $studentIds)?>">
	<input type="hidden" name="Module" id="Module" value="<?=$module?>">
	<input type="hidden" name="Section" id="Section" value="<?=$section?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>