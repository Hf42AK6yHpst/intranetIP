<?php
// Editing by 
/*
 * 2019-06-03 (Carlos): added parameters AccessTime and AccessCode to request url.
 */
set_time_limit(86400);
ini_set("memory_limit", "500M"); 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['ImportCardlog']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$sql = "SELECT DATE_FORMAT(RecordDatetime,'%Y-%m-%d') as RecordDate,DATE_FORMAT(RecordDatetime,'%H:%i:%s') as RecordTime, SiteName, CardID FROM TEMP_CARD_STUDENT_IMPORT_CARDLOG ORDER BY RecordDatetime";
$records = $lc->returnResultSet($sql);
$record_count = count($records);

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$url = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/cardapi/attendance/receiver_csharp.php";

for($i=0;$i<$record_count;$i++)
{
	$external_date = $records[$i]['RecordDate'];
	$external_time = $records[$i]['RecordTime'];
	$sitename = $records[$i]['SiteName'];
	$CardID = $records[$i]['CardID'];
	$access_time = time();
	$access_code = generateCardApiAccessCode($access_time);
	
	$tapcard_url = $url."?CardID=".$CardID."&sitename=".urlencode($sitename)."&external_date=".$external_date."&external_time=".$external_time."&AccessTime=".$access_time."&AccessCode=".$access_code;
	
	curl_setopt($ch, CURLOPT_URL, $tapcard_url);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	$response = curl_exec($ch);
	
	//echo $response;
	//echo "<br>";
}

curl_close($ch);

intranet_closedb();
$Msg = $Lang['StudentAttendance']['OfflineRecordImportSuccess'];
header("Location: import_cardlog.php?Msg=".urlencode($Msg));
?>