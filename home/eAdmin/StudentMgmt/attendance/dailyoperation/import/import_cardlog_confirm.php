<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext-kiosk.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['ImportCardlog']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportOfflineRecords";

$linterface = new interface_html();

$li = new libdb();

$limport = new libimporttext();

$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
	$Msg = $Lang['StudentAttendance']['OfflineRecordImportFail'];
	intranet_closedb();
  	header("Location: import_cardlog.php?Msg=".urlencode($Msg));
  	exit;
} else {
    if($limport->CHECK_FILE_EXT($filename)) {
        # read file into array
        # return 0 if fail, return csv array if success
        $data = $limport->GET_IMPORT_TXT($filepath);
        //debug_r($data);
        $csv_header = array_shift($data);                # drop the title bar
    } else {
    	intranet_closedb();
        header("Location: import_cardlog.php?msg=5");
        exit;
    }
    
    $header_format = array("Time","Site","CardID");
    
    $is_header_valid = true;
   	for($i=0;$i<count($header_format);$i++) {
   		if($header_format[$i] != $csv_header[$i]) {
   			$is_header_valid = false;
   		}
   	}
   	if(!$is_header_valid){
   		intranet_closedb();
   		header("Location: import_cardlog.php?msg=5");
   		exit;
   	}
    
    $sql = "DROP TABLE TEMP_CARD_STUDENT_IMPORT_CARDLOG";
    $li->db_db_query($sql);
    $sql = "CREATE TABLE TEMP_CARD_STUDENT_IMPORT_CARDLOG (
             RecordDatetime datetime,
             SiteName varchar(255),
			 CardID varchar(255),
			 INDEX IDateTime(RecordDatetime),
			 INDEX ICardID(CardID)  
            ) ENGINE=InnoDB CHARSET=utf8";
    $li->db_db_query($sql);
    
    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($data); $i++)
    {
    	list($time,$site,$cardid) = $data[$i];
		$cardid = FormatSmartCardID($cardid);
		 
	    $ts = strtotime($time);
	    $datetime = date('Y-m-d H:i:s',$ts);
	     
	    $values .= "$delim('$datetime','".$li->Get_Safe_Sql_Query($site)."','$cardid')";
	    $delim = ",";
    }
    
    if($values != "")
    {
    	$sql = "INSERT INTO TEMP_CARD_STUDENT_IMPORT_CARDLOG (RecordDatetime,SiteName,CardID) VALUES $values";
    	$li->db_db_query($sql);
    }
	
}

$isValid = true;
$sql = "SELECT RecordDatetime,SiteName,CardID FROM TEMP_CARD_STUDENT_IMPORT_CARDLOG ORDER BY RecordDatetime";
$records = $li->returnResultSet($sql);
$record_count = count($records);

$x = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
$x .= "<tr class=\"tablebluetop\">";
$x .= "<td class=\"tabletoplink\">#</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_TimeRecorded</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Site</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_CardID</td>";
//$x .= "<td class=\"tabletoplink\">".$Lang['General']['Warning']."</td>";
$x .= "</tr>";

if($record_count == 0) {
	$x .= "<tr class=\"tablebluerow1\"><td class=\"tabletext\" colspan=\"4\" style=\"text-align:center;\">".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	$isValid = false;
}

for($i=0;$i<$record_count;$i++) {
	
	$css=($i%2==0)?"tablebluerow1":"tablebluerow2";
    $x .= "<tr class=\"$css\">";
    $x .= "<td class=\"tabletext\">".($i+1)."</td>";
    $x .= "<td class=\"tabletext\">".$records[$i]['RecordDatetime']."</td>";
    $x .= "<td class=\"tabletext\">".intranet_htmlspecialchars($records[$i]['SiteName'])."</td>";
    $x .= "<td class=\"tabletext\">".$records[$i]['CardID']."</td>";
	$x .= "</tr>";
	
}

$x .= "</table>";


$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, "import.php", 0);
if($sys_custom['StudentAttendance']['ImportCardlog']) {
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ImportTapCardRecords'], "import_cardlog.php", 1);
}

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['StudentAttendance']['ImportTapCardRecords']);

?>
<script type="text/javascript" language="JavaScript">
function checkSubmit(formObj)
{
	formObj.submitBtn.disabled = true;
	return true;
}
</script>
<br />
<form name="form1" method="POST" action="import_cardlog_update.php" onsubmit="return checkSubmit(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<?=$x?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
				    <? if ($isValid) { ?>
						<?= $linterface->GET_ACTION_BTN($button_import, "submit", "", "submitBtn") ?>
					<? } ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location.href='import_cardlog.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>