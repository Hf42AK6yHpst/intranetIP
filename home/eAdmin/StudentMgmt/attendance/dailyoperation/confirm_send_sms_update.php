<?php
//using by 

###################### Change Log Start ########################
# 	Date:	2015-12-18 Omas
#			 replace split by explode - for php 5.4
#
#	Date:	2010-01-27	YatWoon
#			add "extra_data" array to allow the content can replaced by specific data value (in_school_time)
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$ldbsms 	= new libsmsv2();
$msg 		= $Content;
$temp = explode(";",$sms_info);
$sms_ary = array();

for($i=0;$i<sizeof($temp);$i++) {
	list($student_id, $sms_phone, $gname, $in_school_time)	= explode("###", $temp[$i]);
	
	if($student_id=="")	continue;
	
	# build extra data array
	$extra_data = array();
	$extra_data['attend_card_time'] = $in_school_time;
	
	# get msg content
	$this_msg = $ldbsms->replace_content($student_id, $msg, $extra_data);
	$msg_ok = sms_substr($this_msg);
	$recipientData[] = array($sms_phone, $student_id, $gname, $msg_ok);
}

//debug_r($recipientData);
# send sms
$sms_message = $msg;
$targetType = 3;  
$picType = 2;
$adminPIC =$PHP_AUTH_USER;
$userPIC = $UserID;
$frModule= "SMS_FR_MODULE_ATTENDANCE";
$deliveryTime = "";
$isIndividualMessage=true;

$ldbsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC, $frModule, $deliveryTime,$isIndividualMessage);

intranet_closedb();

$Msg = $Lang['StudentAttendance']['SMSSentSuccessfully'];
$return_page = $this_page."?DayType=$DayType&TargetDate=$TargetDate&order_by_time=$order_by_time&sent=1&Msg=".urlencode($Msg);
//echo $return_page; die;
header("Location: $return_page");

?>