<?php
//editing by 
/**************************************************** Change Log ********************************************************************
 * 2016-08-30 (Carlos)[ip.2.5.7.10.1]: created
 ************************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewEntryLog";

$linterface = new interface_html();
$lclass= new libclass();

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$today = date("Y-m-d");

$locations = $lc->returnArray("SELECT Location as id, Location as value FROM CARD_STUDENT_TAP_CARD_LOCATION ORDER BY Location");
$location_selection = getSelectByArray($locations, ' id="Location" name="Location" ', $__selected="", $__all=0, $__noFirst=0, $__FirstTitle="", $__ParQuoteValue=1);


$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewEntryLog, "index.php", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['EntryLogByLocation'], "", 1);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table_v30" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StartDate"><?=$Lang['General']['Date']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $academic_year_startdate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $today)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Error')?>
		</td>
	</tr>
		<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="Location"><?=$Lang['StudentAttendance']['TapCardLocation']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$location_selection?>
			<?=$linterface->Get_Form_Warning_Msg('Location_Error', $Lang['StudentAttendance']['RequestSelectLocation'], 'Error')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="TargetType"><?=$Lang['AccountMgmt']['Form']." / ".$Lang['AccountMgmt']['Class']." / ".$Lang['Identity']['Student']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
							<option value="" selected="selected">-- <?=$Lang['General']['PleaseSelect']?> --</option>
							<option value="form"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></option>
							<option value="class"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></option>
							<option value="student"><?=$Lang['Identity']['Student']?></option>
						</select>
					</td>
					<td>
						<span id='DivRankTargetDetail'></span>
						<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('TargetID')){Select_All_Options('TargetID', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
					</td>
				</tr>
			</table>
			<?=$linterface->Get_Form_Warning_Msg('TargetID_Error', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'Error', false);?>
			<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		</td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="GroupBy"><?=$Lang['StaffAttendance']['GroupBy']?></label></td>
	    <td width="70%">
			<select name="GroupBy" id="GroupBy"> 
				<option value="Date"><?=$Lang['General']['Date']?></option>
				<option value="Class"><?=$Lang['StudentAttendance']['Class']?></option>
			</select>
	    </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
	<tr><td align="left" class="tabletextremark">( <?=$Lang['StudentAttendance']['EntryLogByLocationRemark']?> )</td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "submitForm('');","submitBtn") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="Format" id="Format" value="" />
</form>
<br />
<div id="ReportLayer"></div>
<br />
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

function targetTypeChanged(obj)
{
	var selectedTargetType = obj.value;
	if(selectedTargetType == 'form' || selectedTargetType == 'class'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':selectedTargetType,
				'academicYearId':academicYearId,
				'fieldId':'TargetID',
				'fieldName':'TargetID[]'
			},
			function(data){
				$('#selectAllTargetBtn').show();
				$('#DivSelectAllRemark').show();
			}
		);
	}else if(selectedTargetType == 'student'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':'student',
				'academicYearId':academicYearId,
				'fieldId':'studentTargetID',
				'fieldName':'studentTargetID[]',
				'onchange':'getStudentSelection()',
				'divStudentSelection':'DivStudentSelection'
			},
			function(data){
				$('#selectAllTargetBtn').hide();
				$('#DivSelectAllRemark').show();
				getStudentSelection();
			}
		);
	}else{
		$('#DivRankTargetDetail').html('');
		$('#selectAllTargetBtn').hide();
		$('#DivSelectAllRemark').hide();
	}
}

function getStudentSelection()
{
	var selectedYearClassId = [];
	var yearClassObj = document.getElementById('studentTargetID');
	for(var i=0;i<yearClassObj.options.length;i++) {
		if(yearClassObj.options[i].selected) {
			selectedYearClassId.push(yearClassObj.options[i].value);
		}
	}
	
	$('#DivStudentSelection').html(loadingImg);
	$.post(
		'../../common/ajax_load_target_selection.php',
		{
			'target':'student2ndLayer',
			'academicYearId':academicYearId,
			'fieldId':'studentTargetID',
			'fieldName':'studentTargetID[]',
			'studentFieldName':'TargetID[]',
			'studentFieldId':'TargetID',
			'YearClassID[]':selectedYearClassId 
		},
		function(data){
			$('#DivStudentSelection').html(data);
		}
	);
}

function submitForm(format)
{
	$('#submitBtn').attr('disabled',true);
	var is_valid = true;
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');
	var target_obj = document.getElementById('TargetID');
	var target_values = [];

	$('.Error').hide();

	if(!check_date_without_return_msg(startdateObj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		is_valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		is_valid = false;
		$('#Date_Error').show();
	}else{
		$('#Date_Error').hide();
	}

	if($('#Location option:selected').val() == ''){
		is_valid = false;
		$('#Location_Error').show();
	}

	if(target_obj)
	{
		for(var i=0;i<target_obj.options.length;i++)
		{
			if(target_obj.options[i].selected) target_values.push(target_obj.options[i].value);
		}
	}

	if(target_values.length <= 0)
	{
	 	is_valid = false;
		$('#TargetID_Error').show();
	}else{
		$('#TargetID_Error').hide();
	}
	
	if(!is_valid) $('#submitBtn').attr('disabled',false);
	
	if(is_valid)
	{
		$('#Format').val(format);
		if(format == '' || format == 'web'){
			$('#ReportLayer').html(loadingImg);
			$.post(
				'by_location_report.php',
				$('#form1').serialize(),
				function(returnHtml)
				{
					$('#ReportLayer').html(returnHtml);
					$('#submitBtn').attr('disabled',false);
				}
			);
		}else if(format == 'print')
		{
			var url = '';
			var winType = '10';
			var win_name = 'intranet_popup'+winType;
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'by_location_report.php';
			document.form1.target = win_name;
			
			newWindow(url, winType);
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}else if(format == 'csv'){
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'by_location_report.php';
			document.form1.target = '_blank';
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}
	}
}

$(document).ready(function(){
	
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>