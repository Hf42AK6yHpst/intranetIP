<?php
// Editing by 
################# Change Log [Start] #####
#	
#	Date	:	2016-08-30  Carlos - added [Entry by location] tab.
#	
#	Date	:	2015-12-17	Omas
# 			Moved if($TargetDate <> "") after global.php for php 5.4
#
################## Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if($TargetDate <> "")
{
        $URL = "?TargetDate=$TargetDate&TargetClass=".$TargetClass;
        header("Location: show_log.php".$URL);
        exit();
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewEntryLog";

$linterface = new interface_html();

$lc->retrieveSettings();
$lclass = new libclass();
if ($lc->attendance_mode != 1)
{
    $data[] = array("1",$i_DayTypeAM);
}
if ($lc->attendance_mode != 0)
{
    $data[] = array("2",$i_DayTypePM);
}
$selection_period = getSelectByArray($data, " name=\"period\" ", $period,0,1);

// Class Selection
$sql = "SELECT ClassTitleEN 
				FROM 
					YEAR as y 
					inner join 
					YEAR_CLASS as yc
					on y.YearID = yc.YearID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
				ORDER BY 
					y.sequence, yc.sequence";
$result = $lc->returnVector($sql);
$select_class = "<select name='TargetClass'>\n";
$empty_selected = ($TargetClass == '')? "SELECTED":"";

$select_class .= "<option value='' $empty_selected> -- $i_StudentAttendance_AllStudentsWithRecords -- </option>\n";
for ($i=0; $i < sizeof($result); $i++)
{
     $name = $result[$i];
     $selected_str = ($name==$TargetClass? "SELECTED":"");
     $select_class .= "<option value='$name' $selected_str>$name</OPTION>\n";
}
$select_class .= "</select>\n";

# Status of dates
$records_with_data = array();
$ts = time();
$iteration = 0;
# Count 3 months
while($iteration < 4)
{
      $year = date('Y',$ts);
      $month = date('m',$ts);
      $table_name = "CARD_STUDENT_ENTRY_LOG_".$year."_".$month;
      $sql = "SELECT DISTINCT DayNumber FROM $table_name ORDER BY DayNumber";
      $temp = @$lc->returnVector($sql);
      for($i=0; $i<sizeof($temp); $i++)
      {
          $day = $temp[$i];
          $day = ($day < 10? "0".$day : $day);
          $entry_date = $year.$month.$day; #$year."-".$month."-".$day;
          $records_with_data[] = $entry_date;
      }
      $ts = mktime(0,0,0,$month-1,1,$year);
      $iteration++;
}

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewEntryLog, "", 1);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['EntryLogByLocation'], "by_location.php", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="post" action="">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>	
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_ClassName?>
		</td>
		<td width="70%"class="tabletext"><?=$select_class?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_StudentAttendance_Field_Date?>
		</td>
		<td width="70%"class="tabletext"><?=$linterface->GET_DATE_PICKER("TargetDate", date('Y-m-d'))?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_view, "submit", "") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>