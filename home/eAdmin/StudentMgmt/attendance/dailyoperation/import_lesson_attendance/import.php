<?php
// Editing by 
/*
 * 2016-09-02 (Carlos): Added [Reason].
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
//$CurrentPage = "PageLessonAttendance_ImportLessonAttendanceRecords";
$CurrentPage = "PageLessonAttendance_DailyOverview";

### Title ###
//$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php",0);
//$TAGS_OBJ[] = array($Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Title'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/import_lesson_attendance/import.php",1);
$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

if($xmsg != ''){
	if(isset($Lang['General']['ReturnMessage'][$xmsg])){
		$msg = $Lang['General']['ReturnMessage'][$xmsg];
	}else{
		$msg = $xmsg;
	}
}

$PAGE_NAVIGATION[] = array($Lang['LessonAttendance']['DailyLessonStatus'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);
# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

$linterface->LAYOUT_START($msg);

?>
<script type="text/javascript" language="JavaScript">
function checkForm(obj)
{
	var userfile = $('#userfile').val();
	
	if(userfile == '' || Get_File_Ext(userfile) != 'csv'){
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		return false;
	}
	
	obj.submit();
}

</script>
<br />
<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data" onsubmit="checkForm(this);return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
<table>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['Btn']['Select']?>
		</td>
		<td width="70%"class="tabletext">
			<input type="file" class="file" id="userfile" name="userfile"><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletextremark">
				
			[<a class="tablelink" href="<?=GET_CSV("lesson_attendance_import.csv")?>" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a>]
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
		<td width="70%"class="tabletextremark">
<?php
	$x = '';
	for($i=0;$i<count($Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields']);$i++) {
		$x .= '"'.$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][$i][0].'" - '.$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Fields'][$i][1];
		$x .= '<br>'."\n";
	}
	echo $x;
?>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" style="color:red;" class="tabletextremark"><?=$Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Remarks']?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?=$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/10x10.gif'?>" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>