<?php
//using by :

###################### Change Log Start ########################
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancelesson'] || get_client_region() != 'zh_TW') {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if($TargetDate == "") {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;

$li = new libcardstudentattend2();

$table_name = 'SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID;

$LessonAttendUI = new libstudentattendance_ui();
$statusMapping = $LessonAttendUI->LessonAttendanceStatusMapping();


for($i=0; $i<sizeOf($user_id); $i++) {
	$my_user_id = $user_id[$i];
	$student_id = $my_user_id;
	$my_attend_overview_id = $attend_overview_id[$i];
	$my_drop_down_status = $drop_down_status[$i];
	$txtReason = ${"reason$i"};
	$txtReason = trim(htmlspecialchars_decode(stripslashes($txtReason), ENT_QUOTES));
	$remark_value = trim($remark[$i]);
	$my_leavetype_id = $leaveType[$i];

	$sql = "SELECT * FROM $table_name WHERE StudentID='".$li->Get_Safe_Sql_Query($my_user_id)."' AND AttendOverviewID='".$li->Get_Safe_Sql_Query($my_attend_overview_id)."'";
	$rs = $li->returnArray($sql);
	if(empty($rs)) {
		continue;
	}

	$rs = $rs[0];
	if($rs['LastModifiedDate'] != '') {
		$modified_ts = strtotime($rs['LastModifiedDate']);
		if ($modified_ts >= $PageLoadTime) {
			continue;
		}
	}

	if($my_drop_down_status == $statusMapping['Late']['code']) {
		$li->updateLessonAttendance($AcademicYearID, $rs['StudentID'], $rs['AttendOverviewID'], $statusMapping['Late']['code'], $txtReason, $remark_value);
	} else if($my_drop_down_status == $statusMapping['Absent']['code']) {
		$li->updateLessonAttendance($AcademicYearID, $rs['StudentID'], $rs['AttendOverviewID'], $statusMapping['Absent']['code'], $txtReason, $remark_value, $my_leavetype_id);
	} else if($my_drop_down_status == $statusMapping['Present']['code']) {
		$li->updateLessonAttendance($AcademicYearID, $rs['StudentID'], $rs['AttendOverviewID'], $statusMapping['Present']['code']);
	} else if($my_drop_down_status == $statusMapping['Outing']['code']) {
		$li->updateLessonAttendance($AcademicYearID, $rs['StudentID'], $rs['AttendOverviewID'], $statusMapping['Outing']['code']);
	}
}

$Msg = $Lang['StudentAttendance']['LessonLateListConfirmSuccess'];
header("Location: index.php?TimeSlotID=".urlencode($TimeSlotID)."&TargetDate=".urlencode($TargetDate)."&Msg=".urlencode($Msg));

intranet_closedb();

