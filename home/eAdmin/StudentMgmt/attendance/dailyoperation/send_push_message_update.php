<?php
//using by: 

##################################
# change log:
# 2016-09-15 Villa: View Abs list push msg  add "date" for auto full-in content
# 2015-12-18 Omas: replace split by explode - for php 5.4
# 2014-10-09 Roy: file created, send push message to late/absent/early leave studnet's parent
##################################

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");

intranet_auth();
intranet_opendb();

$luser = new libuser();
$libeClassApp = new libeClassApp();
$ldbsms 	= new libsmsv2();

$studentIds = explode(",",standardizeFormPostValue($_POST['studentIds']));
$isPublic = "N";
//debug_pr($luser->getParentStudentMappingInfo($studentIds));
$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentIds), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

//debug_pr($luser->getParentStudentMappingInfo($studentIds));
//debug_pr($parentStudentAssoAry);
$messageTitle = standardizeFormPostValue($_POST['PushMessageTitle']);
$messageContent = standardizeFormPostValue($_POST['MessageContent']);
$appType = $eclassAppConfig['appType']['Parent'];
$sendTimeMode = "";
$sendTimeString = "";
$individualMessageInfoAry = array();
$i = 0;
foreach ($studentIds as $studentId) {
	$thisStudent = new libuser($studentId);
	$appParentIdAry = $luser->getParentUsingParentApp($studentId);
	
//	debug_pr($appParentIdAry);
	$_individualMessageInfoAry = array();
	foreach ($appParentIdAry as $parentId) {
		$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
	}
	$_individualMessageInfoAry['messageTitle'] = $messageTitle;
	$tempContent = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
	//$_individualMessageInfoAry['messageContent'] = $tempContent;
// 	debug_pr($$TargetMessageDataAssoAry[$studentId]);
	$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $tempContent, '');
	$_individualMessageInfoAry['messageContent'] = str_replace('($Dates)',$TargetMessageDataAssoAry[$studentId]['Dates'][$studentId]['Dates'],$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace('($Num_Of_Not_Submitted)',$TargetMessageDataAssoAry[$studentId]['countnot'],$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace('($Date_Of_Absent_Late_Early_Leave)',$TargetDate,$_individualMessageInfoAry['messageContent']);
	
	$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
//debug_pr($_individualMessageInfoAry);
	$i++;
}

// debug_pr($individualMessageInfoAry);
// die();
### send message
$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);

$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];


$return_page = $this_page."?DayType=$DayType&TargetDate=$TargetDate&order_by_time=$order_by_time&sent=1&Msg=".urlencode($msg);
//echo $return_page; die;

intranet_closedb();

header("Location: $return_page");
?>