<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

set_time_limit(0);

intranet_auth();
intranet_opendb();
/*
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
*/
$lcardattend = new libcardstudentattend2();

$Result = array();

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];

$confirmed_user_id = $_SESSION['UserID'];
$sql = 'select 
					u.UserID 
				From 
					INTRANET_USER as u 
				where 
					RecordType = 2 AND RecordStatus IN (0,1)
          AND ClassName != \'\' AND ClassNumber != \'\'';
$StudentID = $lcardattend->returnVector($sql);

$sql = 'select 
					UserID,
					ClassName,
					ClassNumber,
					RecordDate,
					Period,
					AttendStatus 
				from 
					TEMP_LESSON_ATTENDANCE_STUDENT';
$Result = $lcardattend->returnArray($sql);
for ($i=0; $i< sizeof($Result); $i++) {
	$AttendStatus[$Result[$i]['RecordDate']][$Result[$i]['UserID']][$Result[$i]['Period']] = $Result[$i]['AttendStatus'];
}

$StartTimeStamp = strtotime($StartDate);
$EndTimeStamp = strtotime($EndDate);
for ($i=$StartTimeStamp; $i<= $EndTimeStamp; $i+=86400) {
	$TargetDate = date('Y-m-d',$i);
	
	$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
	list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	
	$CycleDayInfo = $lcardattend->Get_Attendance_Cycle_Day_Info($TargetDate);
	if ($CycleDayInfo[0] == 'Cycle') {
		$DayNumber = $lcardattend->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
	}
	
	$sql = 'CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' (
				  AttendOverviewID int(8) NOT NULL auto_increment,
				  SubjectGroupID int(8) NOT NULL,
				  LessonDate date NOT NULL,
				  RoomAllocationID int(8) NOT NULL,
				  StartTime time default NULL,
				  EndTime time default NULL,
				  PlanID varchar(20) default NULL, 
				  PlanName varchar(255),
				  StudentPositionAMF TEXT, 
				  PlanLayoutAMF TEXT,
				  CreateBy int(8) default NULL,
				  CreateDate datetime default NULL,
				  LastModifiedBy int(8) default NULL,
				  LastModifiedDate datetime default NULL,
				  PRIMARY KEY  (AttendOverviewID),
				  UNIQUE KEY SubjectGroupID (SubjectGroupID,LessonDate,RoomAllocationID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$lcardattend->db_db_query($sql);
	
	$sql = 'CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' (
				  AttendOverviewID int(8) NOT NULL,
				  StudentID int(8) NOT NULL,
				  AttendStatus int(2) default NULL,
				  InTime time default NULL,
				  OutTime time default NULL,
				  LateFor int(8) default 0,
				  Remarks text,
				  CreateBy int(8) default NULL,
				  CreateDate datetime default NULL,
				  LastModifiedBy int(8) default NULL,
				  LastModifiedDate datetime default NULL,
				  LeaveTypeID int(11) default NULL,
				  PRIMARY KEY  (AttendOverviewID,StudentID),
				  KEY StudentID (StudentID,AttendOverviewID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$lcardattend->db_db_query($sql);
	
	for ($j=0; $j < sizeof($StudentID); $j++) {		
  	$sql = 'select distinct
						  itra.TimeSlotID,
						  CASE 
						  	WHEN INSTR(itt.TimeSlotName,\'1\') > 0 then \'1\' 
						  	WHEN INSTR(itt.TimeSlotName,\'2\') > 0 then \'2\' 
						  	WHEN INSTR(itt.TimeSlotName,\'3\') > 0 then \'3\' 
						  	WHEN INSTR(itt.TimeSlotName,\'4\') > 0 then \'4\' 
						  	WHEN INSTR(itt.TimeSlotName,\'5\') > 0 then \'5\' 
						  	WHEN INSTR(itt.TimeSlotName,\'6\') > 0 then \'6\' 
						  	WHEN INSTR(itt.TimeSlotName,\'7\') > 0 then \'7\' 
						  	WHEN INSTR(itt.TimeSlotName,\'8\') > 0 then \'8\' 
						  	ELSE \'1\' 
						  END as Period,
						  itt.StartTime,
						  itt.EndTime,
						  stc.SubjectGroupID,
						  itra.RoomAllocationID
						from
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
							inner join
							INTRANET_PERIOD_TIMETABLE_RELATION as iptr
							on
							  \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
								and icgp.PeriodID = iptr.PeriodID
						  inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on
						    itra.TimeTableID = iptr.TimeTableID
						    and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1)
						  inner join 
						  INTRANET_TIMETABLE_TIMESLOT as itt 
						  on 
						  	itra.TimeSlotID = itt.TimeSlotID 
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on stc.SubjectGroupID = itra.SubjectGroupID
						  inner join 
						  SUBJECT_TERM as st
						  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = \''.$YearTermID.'\' 
						  inner join 
						  SUBJECT_TERM_CLASS_USER as stcu
						  on 
						  	stcu.SubjectGroupID = stc.SubjectGroupID
						  	and 
						  	stcu.UserID = \''.$StudentID[$j].'\' 
							';
		$LessonDetail = $lcardattend->returnArray($sql);
		
		for ($k=0; $k< sizeof($LessonDetail); $k++) {
			list($TimeSlotID,$Period,$StartTime,$EndTime,$SubjectGroupID,$RoomAllocationID) = $LessonDetail[$k];
			$FinalStatus = ($AttendStatus[$TargetDate][$StudentID[$j]][$Period])? $AttendStatus[$TargetDate][$StudentID[$j]][$Period]:"0";
			$sql = 'insert into SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' ( 
								SubjectGroupID,
							  LessonDate,
							  RoomAllocationID,
							  StartTime,
							  EndTime,
							  CreateBy,
							  CreateDate,
							  LastModifiedBy,
							  LastModifiedDate
							  )
							values (
								\''.$SubjectGroupID.'\',
								\''.$TargetDate.'\',
								\''.$RoomAllocationID.'\',
								\''.$StartTime.'\',
								\''.$EndTime.'\',
								\''.$_SESSION['UserID'].'\',
								NOW(),
								\''.$_SESSION['UserID'].'\',
								NOW()
								)
							on duplicate key update 
								AttendOverviewID = LAST_INSERT_ID(AttendOverviewID), 
								LastModifiedBy = \''.$_SESSION['UserID'].'\',
								LastModifiedDate = NOW()';
			$Result['InsertOverviewRecord:'.$TargetDate.','.$SubjectGroupID.','.$RoomAllocationID.','.$StudentID[$j]] = $lcardattend->db_db_query($sql);
			$AttendOverviewID = $lcardattend->db_insert_id();
			
			$sql = 'insert into SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' (
								AttendOverviewID,
							  StudentID,
							  AttendStatus,
							  CreateBy,
							  CreateDate,
							  LastModifiedBy,
							  LastModifiedDate
								)
							values (
								\''.$AttendOverviewID.'\',
								\''.$StudentID[$j].'\',
								\''.$FinalStatus.'\',
								\''.$_SESSION['UserID'].'\',
								NOW(),
								\''.$_SESSION['UserID'].'\',
								NOW()
								)
							on duplicate key update 
								LastModifiedBy = \''.$_SESSION['UserID'].'\',
								LastModifiedDate = NOW()';
			$Result['InsertStatus:'.$AttendOverviewID.','.$StudentID[$j].','.$FinalStatus] = $lcardattend->db_db_query($sql);
		}
	}
}

intranet_closedb();
$Msg = $Lang['StudentAttendance']['OfflineRecordImportSuccess'];
header("Location: import.php?Msg=".urlencode($Msg));
?>