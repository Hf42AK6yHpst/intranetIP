<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	?>
	<script>
	window.top.location = '/';
	</script>
	<?
	intranet_closedb();
	exit();
}

$lc = new libstudentattendance_ui();

$DayType = $_REQUEST['DayType'];
$TargetType = $_REQUEST['TargetType'];
$TargetDate = $_REQUEST['TargetDate'];

echo $lc->Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType,$TargetType,true,$TargetDate,$radioTarget);

$ConfirmColor = str_replace(array("\r","\n"),'',$lc->Get_Confirm_Color_Table());
?>
<script>
window.parent.document.getElementById('ConfirmColorCell').innerHTML = '<?=$ConfirmColor?>';	
</script>
<?


intranet_closedb();
?>