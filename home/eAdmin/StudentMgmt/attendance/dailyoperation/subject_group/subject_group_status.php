<?php
// Editing by 
/*
 * 2019-07-26 (Ray):    Add all subject group
 * 2014-09-23 (Carlos): Modified do not follow time table setting to take subject group attendance.
 * 						Use $sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'] can fall back to original design.
 * Created on 2013-05-14
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewSubjectGroupStatus";

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

#class used
$LIDB = new libdb();
$lc->retrieveSettings();
$attendanceMode=$lc->attendance_mode;
### Set Date from previous page
if ($TargetDate == "")
{
	$TargetDate = date('Y-m-d');
}
$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
$lc->createTable_LogAndConfirm($txt_year,$txt_month);

###period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM:
			$display_period = $i_DayTypeAM;    
      $link_page = "AM";
      $confirm_page="AM";
      $DayType = PROFILE_DAY_TYPE_AM;
      break;
    case PROFILE_DAY_TYPE_PM: 
    	$display_period = $i_DayTypePM;
      $confirm_page="PM";
     	$link_page="PM";
     	$DayType = PROFILE_DAY_TYPE_PM;
      break;
    default : 
    	if ($lc->attendance_mode == 1) {
    		$display_period = $i_DayTypePM;      
    		$DayType = PROFILE_DAY_TYPE_PM;
	      $confirm_page="PM";
	      $link_page="PM";
    	}
    	else {
	    	$display_period = $i_DayTypeAM;
		    $DayType = PROFILE_DAY_TYPE_AM;
		    $link_page = "AM";
		    $confirm_page="AM";
		  }
      break;
}

if(!$sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
	$scm_ui = new subject_class_mapping_ui();
	$fcm = new form_class_manage();
	$YearTermInfo = $fcm->Get_Academic_Year_And_Year_Term_By_Date($TargetDate);
	$YearTermID = $YearTermInfo[0]['YearTermID'];
	$SubjectSelect = $scm_ui->Get_Subject_Selection("SubjectID", $SubejctID, 'subjectChanged(this.options[this.selectedIndex].value, this);', $noFirst_='0', $firstTitle_=$button_select, $YearTermID, $OnFocus_='', $FilterSubjectWithoutSG_=1, $IsMultiple_=0, $IncludeSubjectIDArr_='', $ExtrudeSubjectIDArr=array(), $showAllItem=true);
}else{
	$result = $lc->Get_Subject_Group_Attendance_List($TargetDate,$DayType);

	$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
	$x .= "<tr class=\"tabletop\">
					<td class=\"tabletoplink\">".$Lang['SysMgr']['SubjectClassMapping']['Group']."</td>
					<td class=\"tabletoplink\">$i_StudentAttendance_Field_ConfirmedBy</td>
					<td class=\"tabletoplink\">$i_StudentAttendance_Field_LastConfirmedTime</td>
				</tr>\n";
				
	for($i=0; $i<sizeOf($result); $i++)
	{
		list($subject_group_id, $subject_group_name, $confirmed_username, $confirmed_date) = $result[$i];
		
		$css=($i%2==0)?"tablerow1":"tablerow2";
		//if($lc->groupIsRequiredToTakeAttendanceByDate($group_id,$TargetDate)){
		  $class_link = "<a class=\"tablelink\" href=\"view_student.php?SubjectGroupID=".urlencode($subject_group_id)."&TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."\">$subject_group_name</a>";
		  $x .= "<tr class=\"$css\">
		  				<td class=\"tabletext\">$class_link</td>
		  				<td class=\"tabletext\">$confirmed_username</td>
		  				<td class=\"tabletext\">$confirmed_date</td>
		  			</tr>\n";
		/*
		}else{
		  $class_link = "<a class=functionlink_new>$class_name</a>";
		  $x .="<tr class=\"$css\">
		  				<td class=\"tabletext\">$class_link</td>
		  				<td class=\"tabletext\" colspan=\"2\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>
		  			</tr>\n";
		}
		*/
	}
	$x .= "</table>\n";
}

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ViewIndividualSubjectGroupList'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($display_period);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<?
// Display radio button
echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","subject_group_status.php","Subject_Group",$TargetDate,$DayType, 1);
?>
<form name="form2" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<!--
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	-->
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StudentAttendance_Field_Date ?></td>
					<td class="tabletext" width="70%"><?=$TargetDate?> (<?=$display_period?>)</td>
				</tr>
	<?php if(!$sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'])
		  { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></td>
					<td class="tabletext" width="70%"><?=$SubjectSelect?></td>
				</tr>	
	<?php } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?=$absent_count?></td>
					<td align="right"><?=$SysMsg?></td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td id="SubjectGroupLayer">
			<?=$x?>
		</td>
	</tr>
</table>
<input type="hidden" id="DayType" name="DayType" value="<?=escape_double_quotes($DayType)?>" />
</form>
<br />
<?php 
if(!$sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable'])
{ ?>
<script type="text/javascript" language="JavaScript">
function subjectChanged(subjectId, obj)
{
	if(subjectId == ''){
	    if($(obj).find(":selected").attr("data-showall") == '1') {
	        var temp_array = [];
            $(obj).find("option").each(function() {
                if($(this).val() != '') {
                    temp_array.push($(this).val());
                }
            });
            subjectId = temp_array;
        } else {
            $('#SubjectGroupLayer').html('');
            return;
        }
	}

	$('#SubjectGroupLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('#SubjectGroupLayer').load(
		'ajax_task.php',
		{
			'task':'get_subject_group_table',
			'TargetDate': $('#TargetDate').val(),
			'DayType': $('input[name="DayType"]:checked').length>0? $('input[name="DayType"]:checked').val(): $('input#DayType').val(),
			'SubjectID[]': subjectId,
		}
	);
}
</script>
<?php 
} ?>
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.print_option");
intranet_closedb();
?>
