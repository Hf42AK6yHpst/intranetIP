<?php
// Editing by 
/*
 * 2016-8-23 (Carlos): Created for $sys_custom['StudentAttendance']['PrintAttendanceRecords'].
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['PrintAttendanceRecords']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

//$DayType;
$ts = strtotime($TargetDate);
$year = date("Y", $ts);
$month = date("m", $ts);
$day = date("d", $ts);

$school_name = GET_SCHOOL_NAME();
if(strrpos($school_name,' ')>0){
	$school_name_ch = substr($school_name,strrpos($school_name,' '));
	$school_name_en = substr($school_name,0,strrpos($school_name,' '));
	$school_name = Get_Lang_Selection($school_name_ch, $school_name_en);
}

$font_size = Get_Lang_Selection(10,8);
$margin_top = Get_Lang_Selection(2,2);
$padding = Get_Lang_Selection(2,1);

$status_map = array(
				CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['Present'],
				CARD_STATUS_ABSENT=>$Lang['StudentAttendance']['Absent'],
				CARD_STATUS_LATE=>$Lang['StudentAttendance']['Late'],
				CARD_STATUS_OUTING=>$Lang['StudentAttendance']['Outing']
			);

$class_ary = $lc->getClassListToTakeAttendanceByDate($TargetDate);
$class_size = count($class_ary);

$x .= '<style type="text/css">'."\n";
$x .= '@media print { .page_break {display:block; page-break-after: always !important; } ';
//$x .= '.print_footer {display:block;position:fixed;bottom:0;} ';
$x .= '}'."\n";
$x .= '.bold {font-weight: bold;}'."\n";
$x .= '.center {text-align: center;}'."\n";
$x .= '.center {text-align: center;}'."\n";
$x .= 'h3 {margin:4px;}'."\n";
$x .= '.common_table_list_v30 tr th, .common_table_list_v30 tr td { padding: '.$padding.'px; }'."\n";
$x .= '</style>'."\n";

$x .= '<table width="100%" align="center" class="print_hide" border="0">
		<tr>
			<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
		</tr>
		</table>'."\n";

if($DayType == PROFILE_DAY_TYPE_PM){
	$status_field = 'PMStatus';
	$time_field = 'LunchBackTime';
}else{
	$status_field = 'AMStatus';
	$time_field = 'InSchoolTime';
}

for($i=0;$i<$class_size;$i++)
{
	
	$x .= '<h3 class="center">'.$school_name.'</h3>'."\n";
	$x .= '<h3 class="center">'.$Lang['StudentAttendance']['AttendanceRecords'].'</h3>'."\n";
	
	$x .= '<div style="font-size:'.$font_size.'px;">';
		$x .= '<span class="bold">'.$Lang['General']['Class'].'</span>'.str_repeat('&nbsp;',2).$class_ary[$i][1];
		$x .= str_repeat('&nbsp;',8).'<span class="bold">'.$Lang['General']['Date'].'</span>'.str_repeat('&nbsp;',2).$TargetDate;
	$x .= '</div>'."\n";
	
	
	$student_ary = $lc->retrieveClassDayData($class_ary[$i][3], $year, $month, $day);
	$student_size = count($student_ary);
	//debug_pr($student_ary);
	
	$x .= '<table class="common_table_list_v30" style="font-size:'.$font_size.'px;">'."\n";
		$x .= '<thead>';
		$x .= '<tr class="bold">';
			$x .= '<th style="width:10%;text-align:center;">'.$Lang['General']['Status2'].'</th>
					<th style="width:10%;text-align:center;">'.$Lang['General']['ClassNumber'].'</th>
					<th style="width:19%;text-align:center;">'.$Lang['General']['Name'].'</th>
					<th style="width:10%;text-align:center;">'.$Lang['StudentAttendance']['Arrived'].'</th>
					<th style="width:11%;text-align:center;">'.$Lang['StudentAttendance']['LateReason'].'</th>
					<th style="width:5%;text-align:center;">'.$Lang['StudentAttendance']['AM2'].'</th>
					<th style="width:5%;text-align:center;">'.$Lang['StudentAttendance']['PM2'].'</th>
					<th style="width:20%;text-align:center;">'.$Lang['StudentAttendance']['DateOfHandInParentLetter'].'</th>
					<th style="width:10%;text-align:center;">'.$Lang['General']['Remark'].'</th>';
		$x .= '</tr>
			</thead>'."\n";
		
		$x.= '<tbody>';
		
	for($j=0;$j<$student_size;$j++){
		
		$x .= '<tr>';
			$x .= '<td class="center">'.$status_map[$student_ary[$j][$status_field]].'</td>';
			$x .= '<td class="center">'.$student_ary[$j]['ClassNumber'].'</td>';
			$x .= '<td class="center">'.$student_ary[$j][1].'</td>';
			$x .= '<td class="center">'.($student_ary[$j][$time_field]!=''? date("G:i", strtotime($student_ary[$j][$time_field])) : '&nbsp;').'</td>';
			$x .= '<td>&nbsp;</td>';
			$x .= '<td>&nbsp;</td>';;
			$x .= '<td>&nbsp;</td>';
			$x .= '<td>&nbsp;</td>';
			$x .= '<td>&nbsp;</td>';
		$x .= '</tr>'."\n";
		
	}	
		
	$x .= '</tbody>';
	
	$x .= '</table>'."\n";
	
	$x .= '<table class="print_footer" width="100%" style="width:100%;font-size:'.$font_size.'px;margin-top:'.$margin_top.'em;">
			<tfoot>
			<tr>	
				<td align="left">'.str_repeat('_',25).'('.str_repeat('&nbsp;',15).')</td>
				<td align="right">'.str_repeat('_',25).'('.str_repeat('&nbsp;',15).')</td>
			</tr>
			<tr>
				<td align="left">'.$Lang['StudentAttendance']['TeacherSignature'].' ('.$Lang['StudentAttendance']['AM2'].')</td>
				<td align="right">'.$Lang['StudentAttendance']['TeacherSignature'].' ('.$Lang['StudentAttendance']['PM2'].')</td>
			</tr>
			</tfoot>
			</table>';
	

	
	$x .= '<div class="page_break"></div>'."\n";
}


include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
echo $x;
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>