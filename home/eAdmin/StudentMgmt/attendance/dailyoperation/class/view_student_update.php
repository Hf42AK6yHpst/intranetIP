<?php
# using:

### note for eSchoolBus: it won't take care of attendance_mode of the site to update from school / to school taking bus record status, i.e. it'll update both for the date

############################################## Change Log ############################################
/*
 * 2019-09-27 Cameron
 * - fix eSchoolBus logic: call syncAbsentRecordFromAttendance only when not created application when update status is absent or outgoing
 * 2019-09-20 Cameron
 * - as change RecordStatus, AddedFrom, AttendanceRecordID, AttendanceApplyLeaveID to timeslot table,
 * should apply updateSchoolBusTimeSlotAttendanceRecordID() and updateSchoolBusTimeSlotApplyLeaveToDelete() to related logic for eSchoolBus
 *
 * 2019-08-09 Ray
 * - add SubmittedAbsentLateRecord
 *
 * 2019-05-21 Philips
 *  - add checkbox for Serious Late of $sys_custom['StudentAttendance']['SeriousLate']
 * 
 * 2019-02-18 Cameron
 *  - add field AttendanceApplyLeaveID for eSchoolBus to distinguish from AttendanceRecordID
 * 2019-02-14 Cameron
 *  - should apply updateSchoolBusApplyLeaveToDelete if parent has applied leave on a date and teacher update attendance status to present/late
 * 2019-02-01 Cameron
 *  - don't sync apply leave record to eSchoolBus if the student is not taking school bus student.
 * 2018-12-17 (Cameron) [ip.2.5.10.2.1] also update reason and teacher's remark in eSchoolBus corresponding records for absent status 
 *  - treat Outgoing as absent in eSchoolBus 
 * 2018-11-06 (Cameron) apply updateSchoolBusApplyLeaveToDelete for eSchoolBus
 * 2018-10-30 (Cameron) sync absent record to eSchoolBus
 * 2018-08-03 (Carlos): [ip.2.5.9.10.1] Mark attendance records confirmed by admin.
 * 2017-05-31 (Carlos): [ip.2.5.8.7.1] Check and skip outdated record if attendance record last modified time is later than page load time. 
 * 2017-02-24 (Carlos): [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
 * 2016-07-05 (Carlos): [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - update in time if time is being edited.
 * 2015-02-13 (Carlos): [ip2.5.6.3.1] Added [Waived], [Reason], [Office Remark].
 * 2014-10-17 (Carlos): [ip2.5.5.10.1] Update/insert remark records
 * 2014-03-26 (Carlos): Use 'NULL' as Waive value for Set_Profile()
 * 2014-03-14 (Carlos): $sys_custom['SmartCardAttendance_StudentAbsentSession2'] - added Absent Session
 * 2013-02-05 (Carlos): To prevent data problem, it will skip processing profile if fail to insert cardlog record
 * 2012-09-24 (Carlos): Clear_Profile() do not need to follow setting [Only synchronize eDiscipline late record when confirm late profile], should always clean profile
 * 2012-05-04 (Carlos): Modify to add no card entrance log if and only if no both InSchoolTime and LunchBackTime 
 * 2012-03-22 (Carlos): check setting OnlySynceDisLateRecordWhenConfirm to update eDiscipline late record at Set_Profile()
 * 2011-12-16 (Carlos): Modified to separate AM and PM Modify fields
* 2010-10-07 (Kenneth Chung): Unified view_student_AM.php/ view_student_PM.php to view_student.php
*/
//
//	Date 	: 20100105 (By Ronald)
//	Details : if it is a late record, will detect is it a serious late record or not.
//
############################################ End of Change Log ########################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
/*
# check page load time 
if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
	$error = 1;  // data outdated
	if($DayType==PROFILE_DAY_TYPE_AM)
	  $return_page="view_student.php";
	else if($DayType==PROFILE_DAY_TYPE_PM)
	  $return_page = "view_student.php";
	
	$return_page = $return_page."?class_name=$class_name&class_id=$class_id&DayType=$DayType&TargetDate=$TargetDate&Msg=".urlencode($Lang['StudentAttendance']['DataOutOfDate']);
	header("Location: $return_page");
	exit();	
}
*/
//$has_magic_quotes = get_magic_quotes_gpc();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

# LOCK TABLES
//$lcardattend->lockTablesAdmin();
$upadteeDisLateRecord = $lcardattend->OnlySynceDisLateRecordWhenConfirm != '1';

################################################
$directProfileInput = false;
################################################

$lcardattend->Start_Trans();
$Result = array();
if ($plugin['Disciplinev12']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
  	$ldisciplinev12 = new libdisciplinev12();
  	$LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
  
  	## Only For IP25 eDis1.2 Only ##
  	if($sys_custom['Discipline_SeriousLate']){	
	    $sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType = -1 AND LateMinutes IS NOT NULL";
	    $targetCatID = $ldisciplinev12->returnVector($sql);
	    if(sizeof($targetCatID)>0){
	    	for($i=0; $i<sizeof($targetCatID); $i++){
	    		$result = $ldisciplinev12->CategoryInUsed($targetCatID[$i]);
	    		
	    		if($result){
	    			$cnt++;
	    		}
	    	}
	    }
	    if($cnt > 0){
	    	$SeriousLateUsed = 1;
	    }
	}
}

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
// $year = getCurrentAcademicYear();
// $semester = getCurrentSemester();

//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
//debug_pr($school_year);

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

### for student confirm
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;


### Bug tracing
if($bug_tracing['smartcard_student_attend_status']){
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	if(!is_dir($file_path."/file/log_student_attend_status")){
		$lf = new libfilesystem();
		$lf->folder_new($file_path."/file/log_student_attend_status");
	}
	$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($txt_year.$txt_month.$txt_day).".txt";
}

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

$csv_student_list = "'".implode("','",$user_id)."'";
$sql = "SELECT RecordID,StudentID FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='$TargetDate' AND StudentID IN ($csv_student_list) AND DayType='$DayType'";
$remark_records = $lcardattend->returnResultSet($sql);
$RemarkStudentRecords = array();
$remark_record_count = count($remark_records);
for($i=0;$i<$remark_record_count;$i++){
	$RemarkStudentRecords[$remark_records[$i]['StudentID']] = $remark_records[$i]['RecordID'];
}

$studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$user_id,'StudentIDToRecord'=>1));

$appliedLeaveStudentAry = $lcardattend->checkStudentApplyLeave($user_id,$TargetDate,$DayType);
$appliedLeaveStudentIDAry = array_keys($appliedLeaveStudentAry);

for($i=0; $i<sizeOf($user_id); $i++)
{
	$my_user_id = $user_id[$i];
	
	$my_day = $txt_day;
	$my_drop_down_status = $drop_down_status[$i];
	
	$my_record_id = $record_id[$i];
	
	if($my_drop_down_status == -1) continue;
	
	// check and skip outdated record if attendance record last modified time is later than page load time
	if(isset($studentIdToDailyLog[$my_user_id])){
		if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
		{
			$am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
			if($am_modified_ts >= $PageLoadTime) continue;
		}
		if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
		{
			$pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
			if($pm_modified_ts >= $PageLoadTime) continue;
		}
	}
	
	// Kenneth Code Start
	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$InSchoolTimeField = "InSchoolTime";
		$AttendanceArrayTime = "MorningTime";
		$StatusField = "AMStatus";
		$ConfirmField = "IsConfirmed";
		$ConfirmByField = "ConfirmedUserID";
		
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
		$dayTypeStr = 'AM';
	}
	else {
		$lcardattend->retrieveSettings();
		if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
		{
			$InSchoolTimeField = "InSchoolTime";
			$AttendanceArrayTime = 'MorningTime';
		}
		else # retrieve LunchBackTime
		{
			$InSchoolTimeField = "LunchBackTime";
			$AttendanceArrayTime = 'LunchEnd';
		}
		$StatusField = "PMStatus";
		$ConfirmField = "PMIsConfirmed";
		$ConfirmByField = "PMConfirmedUserID";
		
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
		$dayTypeStr = 'PM';
	}
	
	$inTimesql = "select 
	            		".$InSchoolTimeField."
	            	from 
	            		$card_log_table_name
								WHERE 
									DayNumber = '$txt_day'
									AND 
									UserID = '$my_user_id'";
  $inTimeResult = $lcardattend->returnVector($inTimesql);
  $InSchoolTime = $inTimeResult[0];
      
    $waived = $_REQUEST['Waived'.$i] == ''? 0 : 1;
    $input_reason = trim($_REQUEST['input_reason'.$i]); 
    if($has_magic_quotes){
    	$input_reason = stripslashes($input_reason); 
    }
	# insert if not exist
	if($my_record_id=="")
	{
	    
    $sql = "INSERT INTO $card_log_table_name (
    					UserID,
    					DayNumber,
    					".$StatusField.",
    					".$ConfirmField.",
    					".$ConfirmByField.",
    					DateInput,
    					InputBy,
    					".$DateModifiedField.",
    					".$ModifyByField." 
    				) VALUES (
    					'$my_user_id',
    					'$my_day',
    					'$my_drop_down_status',
    					'1',
    					'".$_SESSION['UserID']."',
    					NOW(),
    					'".$_SESSION['UserID']."',
    					NOW(),
    					'".$_SESSION['UserID']."'
    				)
            ";
    $Result[$i.'_InsertCardlog'] = $lcardattend->db_db_query($sql);
    $logRecordID = $lcardattend->db_insert_id();
    
    if ($plugin['eSchoolBus']) {
        $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
        if ($isTakingBusStudent) {
            if ($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_OUTING) {
                $teacherRemark = trim($TeacherRemark[$i]);
                if($has_magic_quotes){
                    $teacherRemark = stripslashes($teacherRemark);
                }        

                if (in_array($my_user_id,(array)$appliedLeaveStudentIDAry)) {
                    $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
                    $conditionAry = array();
                    $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                    $conditionAry['StudentID'] = $my_user_id;
                    $conditionAry['StartDate'] = $TargetDate;
                    $Result[$i.'_updateSchoolBusAttendanceRecordID'] = $leSchoolBus->updateSchoolBusAttendanceRecordID($logRecordID, $conditionAry);

                    unset($conditionAry);
                    $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                    $conditionAry['t.LeaveDate'] = $TargetDate;
                    $Result[$i.'_updateSchoolBusTimeSlotAttendanceRecordID'] = $leSchoolBus->updateSchoolBusTimeSlotAttendanceRecordID($my_user_id, $logRecordID, $conditionAry);
                }
                else {
                    $Result[$i.'_SyncAbsentRecordToeSchoolBus'] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $input_reason, $teacherRemark, $dayTypeStr, $logRecordID);
                }
            }
            else if (($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE) && (in_array($my_user_id,(array)$appliedLeaveStudentIDAry))){
                $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
                $conditionAry = array();
                $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                $conditionAry['StudentID'] = $my_user_id;
                $conditionAry['StartDate'] = $TargetDate;
                $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                $Result['ChangeFromAbsentToOthers_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                unset($conditionAry);
                $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                $conditionAry['t.LeaveDate'] = $TargetDate;
                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                $Result['ChangeFromAbsentToOthers_Timeslot_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
            }
        }
    }
    
    ## Bug Tracing
    if($bug_tracing['smartcard_student_attend_status']){
      $log_date = date('Y-m-d H:i:s');
      $log_target_date = $TargetDate;
      $log_student_id = $my_user_id;
      $log_old_status = "";
      $log_new_status = $my_drop_down_status;
      $log_sql = $sql;
      $log_admin_user = $PHP_AUTH_USER;
                        
			$log_page = 'view_student_update.php';
      $log_content = get_file_content($log_filepath);
      $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
      $log_content .= $log_entry;
      write_file_content($log_content, $log_filepath);	                        
    }
    
    $log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' NULL '.$my_drop_down_status.']';
    
    // skip processing profile if fail to insert cardlog, otherwise may cause data problem
    if(!$Result[$i.'_InsertCardlog']) {
    	continue;
    }
                      
    # Check Bad actions
    if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
    {
      $lcardattend->removeBadActionFakedCard($my_user_id,$TargetDate,$DayType);
      # Forgot to bring card
      $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
      
      $Result[$my_user_id.'ClearProfile'] = $lcardattend->Clear_Profile($my_user_id,$TargetDate,$DayType,true);
    }
		else if ($my_drop_down_status==CARD_STATUS_LATE)
		{
	    $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
	    # Forgot to bring card
	    $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
			
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,'NULL',false,$upadteeDisLateRecord);
	    if(!$sys_custom['StudentAttendance']['SeriousLate']){
	       $Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$waived,false,$upadteeDisLateRecord);
	    }else {
	    	$serious_late_value = $_POST["seriousLate".$i]=='1'?'1':'0';
	        $Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$waived,false,$upadteeDisLateRecord,$serious_late_value);
	    }

			if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
				$Result[$my_user_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType,'','', PROFILE_TYPE_LATE , '','');
			}

		}else if ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING){ # No Daily Record and Absent
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",false,'NULL',false,$upadteeDisLateRecord);
			$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",false,$waived,false,$upadteeDisLateRecord);
		}
  }
  else  # update if exist
  {
		# Grab original status
		$sql = "SELECT ".$StatusField.", ".$InSchoolTimeField.", RecordID, InSchoolTime, LunchBackTime FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
		$temp = $lcardattend->returnArray($sql,3);
		list($old_status, $old_inTime, $old_record_id, $old_inSchoolTime, $old_lunchBackTime) = $temp[0];

		if ($old_status != $my_drop_down_status)
		{
			# Check bad actions
			# Late / Present -> Absent
			if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
			{
				$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
				if($old_inTime!=""){
					# Has card record but not present in classroom
					$lcardattend->addBadActionFakedCard($my_user_id,$TargetDate,$old_inTime,$DayType);
				}
			}

			# Absent -> Late / Present
			if (($old_status==CARD_STATUS_ABSENT || trim($old_status) == "") && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
			{
				$lcardattend->removeBadActionFakedCard($my_user_id,$TargetDate,$DayType);
				# forgot to bring card
				if(trim($old_inSchoolTime)=='' && trim($old_lunchBackTime)==''){ // add no card entrance log if and only if no both AM in time and PM in time
					$lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,$old_inTime);
				}
			}

			if(trim($old_status) == "" && $my_drop_down_status==CARD_STATUS_LATE) {
				if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
					$Result[$my_user_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType,'','', PROFILE_TYPE_LATE , '','');
				}
			}

			if ($plugin['eSchoolBus']) {
			    
			    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
			    if ($isTakingBusStudent) {
    			    # Late / Present -> Absent / Outgoing
    			    if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING)) { 
        			    $teacherRemark = trim($TeacherRemark[$i]);
        			    if($has_magic_quotes){
        			        $teacherRemark = stripslashes($teacherRemark);
        			    }
        			    
        			    $attendanceApplyLeaveID = $appliedLeaveStudentAry[$my_user_id];
        			    $isSchoolBusApplyLeaveRecordExist = $leSchoolBus->isSchoolBusApplyLeaveRecordExist($attendanceApplyLeaveID, $my_user_id, $TargetDate, $dayTypeStr);
        			    
        			    if (in_array($my_user_id,(array)$appliedLeaveStudentIDAry) && $isSchoolBusApplyLeaveRecordExist) {
        			        $conditionAry = array();
        			        $conditionAry['AttendanceRecordID'] = $my_record_id;
        			        $conditionAry['StudentID'] = $my_user_id;
        			        $conditionAry['StartDate'] = $TargetDate;        			        
        			        $Result[$i.'_updateSchoolBusAttendanceRecordID'] = $leSchoolBus->updateSchoolBusAttendanceRecordID($my_record_id, $conditionAry);

        			        unset($conditionAry);
                            $conditionAry['t.AttendanceRecordID'] = $my_record_id;
                            $conditionAry['t.LeaveDate'] = $TargetDate;
                            $Result[$i.'_updateSchoolBusTimeSlotAttendanceRecordID'] = $leSchoolBus->updateSchoolBusTimeSlotAttendanceRecordID($my_user_id, $my_record_id, $conditionAry);
        			    }
        			    else {
        			        $Result[$i.'_SyncAbsentRecordToeSchoolBus'] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $input_reason, $teacherRemark, $dayTypeStr, $my_record_id);
        			    }
    			    }

                    $deleteApplyLeave = false;
    			    # Absent / Outgoing -> Late / Present 
    			    if (($old_status==CARD_STATUS_ABSENT || $old_status==CARD_STATUS_OUTING) && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)) {
    			        $conditionAry = array();
    			        $conditionAry['AttendanceRecordID'] = $my_record_id;
    			        $conditionAry['StudentID'] = $my_user_id;
    			        $conditionAry['StartDate'] = $TargetDate;
    			        $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
    			        $Result['ChangeFromAbsentToOthers_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                        unset($conditionAry);
                        $conditionAry['t.AttendanceRecordID'] = $my_record_id;
                        $conditionAry['t.LeaveDate'] = $TargetDate;
                        $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                        $Result['ChangeFromAbsentToOthers_TimeSlot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);

                        $deleteApplyLeave = true;       // Use AttendanceApplyLeaveID to filter rather than AttendanceRecordID in case AttendanceRecordID is empty
                        $attendanceApplyLeaveID= $appliedLeaveStudentAry[$my_user_id];
    			    }
    			    
    			    # Present -> Late && have applied leave
    			    if ($old_status==CARD_STATUS_PRESENT && $my_drop_down_status==CARD_STATUS_LATE && in_array($my_user_id,(array)$appliedLeaveStudentIDAry)) {
    			        $deleteApplyLeave = true;
    			        $attendanceApplyLeaveID= $appliedLeaveStudentAry[$my_user_id];
    			    }
    			    
    			    # Late -> Present && have applied leave
    			    if ($old_status==CARD_STATUS_LATE  && $my_drop_down_status==CARD_STATUS_PRESENT && in_array($my_user_id,(array)$appliedLeaveStudentIDAry)) {
    			        $deleteApplyLeave = true;
    			        $attendanceApplyLeaveID = $appliedLeaveStudentAry[$my_user_id];
    			    }

    			    if ($deleteApplyLeave) {
                        $conditionAry = array();
                        $conditionAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
    			        $conditionAry['StudentID'] = $my_user_id;
    			        $conditionAry['StartDate'] = $TargetDate;
    			        $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
    			        $Result['ChangeFromAbsentToOthers_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                        unset($conditionAry);
                        $conditionAry['t.AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
                        $conditionAry['t.LeaveDate'] = $TargetDate;
                        $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                        $Result['ChangeFromAbsentToOthers_TimeSlot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
    			    }
			    }
			}
			
			$LastModifyUpdate = ", ".$DateModifiedField."=now(),
													 ".$ModifyByField."='".$_SESSION['UserID']."' ";
		}
		else {
			$LastModifyUpdate = "";
			if ($plugin['eSchoolBus']) {
			    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
			    if ($isTakingBusStudent) {

			        if (($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING)) {
        			    $teacherRemark = trim($TeacherRemark[$i]);
        			    if($has_magic_quotes){
        			        $teacherRemark = stripslashes($teacherRemark);
        			    }
        			    
        			    // update reason and teacher's remark only
        			    $Result['UpdateReasonAndRemark_'.$my_record_id] = $leSchoolBus->updateReasonAndRemark($my_user_id, $TargetDate, $my_record_id, $input_reason, $teacherRemark);
			        }
    			    else if (($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE) && (in_array($my_user_id,(array)$appliedLeaveStudentIDAry))){
    			        $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
    			        $conditionAry = array();
    			        $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
    			        $conditionAry['StudentID'] = $my_user_id;
    			        $conditionAry['StartDate'] = $TargetDate;
    			        $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
    			        $Result['ChangeFromAbsentToOthers_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                        unset($conditionAry);
                        $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                        $conditionAry['t.LeaveDate'] = $TargetDate;
                        $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                        $Result['ChangeFromAbsentToOthers_TimeSlot_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
    			    }
			    }
			}
		}
			
		# Update Daily table
		$sql = "UPDATE $card_log_table_name SET 
							".$StatusField."='".$my_drop_down_status."',  
							".$ConfirmField." = '1',
  						".$ConfirmByField." = '".$_SESSION['UserID']."'
  						".$LastModifyUpdate." 
						WHERE RecordID='".$my_record_id."'";
		$Result[$i.'4'] = $lcardattend->db_db_query($sql);
		 ## Bug Tracing
		if($bug_tracing['smartcard_student_attend_status']){
		  $log_date = date('Y-m-d H:i:s');
		  $log_target_date = $TargetDate;
		  $log_student_id = $my_user_id;
		  $log_old_status = $old_status;
		  $log_new_status = $my_drop_down_status;
		  $log_sql = $sql;
		  $log_admin_user = $PHP_AUTH_USER;
		    
			$log_page = 'view_student_update.php';
			$log_content = get_file_content($log_filepath);
			$log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
			$log_content .= $log_entry;
			write_file_content($log_content, $log_filepath);	                        
		}
		
		$log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' '.$old_status.' '.$my_drop_down_status.']';
		
		if ($my_drop_down_status==CARD_STATUS_LATE) {
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,'NULL',false,$upadteeDisLateRecord);
		    if(!$sys_custom['StudentAttendance']['SeriousLate']){
		    	$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$waived,false,$upadteeDisLateRecord);
		    } else {
		    	$serious_late_value = $_POST["seriousLate".$i]=='1'?'1':'0';
		        $Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$waived,false,$upadteeDisLateRecord, $serious_late_value);
		    }
		}
		else if ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING) {
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'NULL',false,true);
			$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|","",false,$waived,false,true);
		}
		else if ($my_drop_down_status==CARD_STATUS_PRESENT) {
			$Result[$my_user_id.'ClearProfile'] = $lcardattend->Clear_Profile($my_user_id,$TargetDate,$DayType,true);
		}
  }
	// end Kenneth Code Start
	
	$remark_value = trim($TeacherRemark[$i]);
	if(!$has_magic_quotes){
		$remark_value = addslashes($remark_value);
	}
	$Result[$my_user_id.'UpdateRemark'] = $lcardattend->updateTeacherRemark($my_user_id,$TargetDate,$DayType,$remark_value);
	
	// handle Office remark
	if($my_drop_down_status != CARD_STATUS_PRESENT){
		$office_remark_value = trim($OfficeRemark[$i]);
		if(!$use_magic_quotes){
			$office_remark_value = addslashes($office_remark_value);
		}
		$RealProfileType = $my_drop_down_status;
		if($my_drop_down_status == CARD_STATUS_OUTING){
			$RealProfileType = CARD_STATUS_ABSENT;
		}
		$lcardattend->updateOfficeRemark($my_user_id,$TargetDate,$DayType,$RealProfileType,$office_remark_value);
	}
	
	if($sys_custom['StudentAttendance']['AllowEditTime'] && isset($_POST['InSchoolTime'.$my_user_id]))
	{
		$lcardattend->updateAttendanceInTime($my_user_id,$TargetDate,$DayType,$_POST['InSchoolTime'.$my_user_id]);
	}
	
	/*
	// Handle teacher's remark records
	if(isset($RemarkStudentRecords[$my_user_id]) && $RemarkStudentRecords[$my_user_id]!='' && $RemarkStudentRecords[$my_user_id]>0){
		// Update remark
		$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$remark_value' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType'";
		$Result[$my_user_id.'UpdateRemark'] = $lcardattend->db_db_query($sql);
	}else{
		// Insert remark
		$sql = "INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark) VALUES ('$my_user_id','$TargetDate','$DayType','$remark_value')";
		$Result[$my_user_id.'InsertRemark'] = $lcardattend->db_db_query($sql);
	}
	*/
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) {
		$sql = "select 
							LateSession,
							RequestLeaveSession,
							PlayTruantSession,
							OfficalLeaveSession 
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?",AbsentSession ":"")." 
						from 
							CARD_STUDENT_STATUS_SESSION_COUNT 
						where 
							RecordDate = '".$TargetDate."' 
							and 
							StudentID = '".$my_user_id."' 
							and 
							DayType = '".$DayType."'";
		$OrgRecord = $lcardattend->returnArray($sql);
		
		if ($OrgRecord[0]['LateSession'] != $late_session[$i] 
				|| $OrgRecord[0]['RequestLeaveSession'] != $request_leave_session[$i] 
				|| $OrgRecord[0]['PlayTruantSession'] != $play_truant_session[$i] 
				|| $OrgRecord[0]['OfficalLeaveSession'] != $offical_leave_session[$i]
				|| $OrgRecord[0]['AbsentSession'] != $absent_session[$i]) {
			# Update Daily table
			$sql = "UPDATE ".$card_log_table_name." SET 
  							".$DateModifiedField."=now(),
								".$ModifyByField."='".$_SESSION['UserID']."' 
						WHERE 
							DayNumber = '".$my_day."' 
    					and 
							UserID='".$my_user_id."'";
			$Result['UpdateLastModify:'.$my_user_id] = $lcardattend->db_db_query($sql);
		}
		
		// insert/update the number of sessions to db
		$sql = "insert into CARD_STUDENT_STATUS_SESSION_COUNT (
							RecordDate,
							StudentID,
							DayType,
							LateSession,
							RequestLeaveSession,
							PlayTruantSession,
							OfficalLeaveSession,
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"AbsentSession,":"")."
							DateInput,
							InputBy,
							DateModify,
							ModifyBy
							)
						values (
							'".$TargetDate."',
							'".$my_user_id."',
							'".$DayType."',
							'".$late_session[$i]."',
							'".$request_leave_session[$i]."',
							'".$play_truant_session[$i]."',
							'".$offical_leave_session[$i]."',
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"'".$absent_session[$i]."',":"")." 
							NOW(),
						  '".$_SESSION['UserID']."',
						  NOW(),
						  '".$_SESSION['UserID']."'
						 ) 
						on duplicate key update 
							LateSession = '".$late_session[$i]."',
							RequestLeaveSession = '".$request_leave_session[$i]."',
							PlayTruantSession = '".$play_truant_session[$i]."',
							OfficalLeaveSession = '".$offical_leave_session[$i]."',
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']? "AbsentSession = '".$absent_session[$i]."',":"")."
							DateModify = NOW(),
							ModifyBy = '".$_SESSION['UserID']."'";
		$Result['UpdateSessions'] = $lcardattend->db_db_query($sql);
	}
	/*
	if( $period == "1")            # AM
	{
		$inTimesql = "select 
	            		InSchoolTime
	            		from 
	            		$card_log_table_name
						WHERE DayNumber = '$txt_day'
						AND UserID = '$my_user_id'";
        $inTimeResult = $lcardattend->returnVector($inTimesql);
        $InSchoolTime = $inTimeResult[0];
        
		# insert if not exist
		if($my_record_id=="")
		{
	    $sql = "INSERT INTO $card_log_table_name
	            (UserID,DayNumber,AMStatus,DateInput,DateModified) VALUES
	            ('$my_user_id','$my_day','$my_drop_down_status',NOW(),NOW())
	                            ";
	    $Result[$i.'1'] = $lcardattend->db_db_query($sql);
	    
	    ## Bug Tracing
	    if($bug_tracing['smartcard_student_attend_status']){
	      $log_date = date('Y-m-d H:i:s');
	      $log_target_date = $TargetDate;
	      $log_student_id = $my_user_id;
	      $log_old_status = "";
	      $log_new_status = $my_drop_down_status;
	      $log_sql = $sql;
	      $log_admin_user = $PHP_AUTH_USER;
	                        
 				$log_page = 'view_student_update.php';
        $log_content = get_file_content($log_filepath);
        $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
        $log_content .= $log_entry;
        write_file_content($log_content, $log_filepath);	                        
	    }
                        
      # Check Bad actions
      if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
      {
        $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
        
        # Forgot to bring card
        $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
      }
			else if ($my_drop_down_status==CARD_STATUS_LATE)
			{
				
			    $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
			    
			    # Forgot to bring card
			    $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
			
			    #########################################################
			    # Add late to student profile
			    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
			    $temp = $lcardattend->returnArray($sql,2);
			    list($targetClass, $targetClassNumber) = $temp[0];
			    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,AcademicYearID,YearTermID";
			    $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber','$school_year_id','$semester_id'";
			    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
			    				on duplicate key update 
			    					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
			    					DateModified = NOW() ";
			    $Result[$i.'2'] = $lcardattend->db_db_query($sql);
			    $insert_id = $lcardattend->db_insert_id();
			    
					# Calculate upgrade items
					if ($plugin['Disciplinev12'] && $LateCtgInUsed)
					{
						$dataAry = array();
						$dataAry['StudentID'] = $my_user_id;
						$dataAry['RecordDate'] = $TargetDate;
						//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
						$dataAry['Year'] = $school_year;
						$dataAry['YearID'] = $school_year_id;
						//$semester = retrieveSemester($TargetDate);
						$dataAry['Semester'] = $semester;
						$dataAry['SemesterID'] = $semester_id;
						$dataAry['StudentAttendanceID'] = $insert_id;
						$dataAry['Remark'] = $InSchoolTime;
						
						if($SeriousLateUsed == 1) {
							if($InSchoolTime != "")
							{
								$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
								$school_start_time = $arr_time_table['MorningTime'];
								
								$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
								$late_minutes = ceil($str_late_minutes/60);
								
								$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
								$min_late_minutes = $lcardattend->returnVector($sql);		
								
								if(sizeof($min_late_minutes)>0)
								{
									$dataAry['SeriousLate'] = 1;
									$dataAry['LateMinutes'] = $late_minutes;
								}
								else
								{
									$dataAry['SeriousLate'] = 0;
								}
							}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}
						}
						$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
					}
			
			    if (!$insert_id)
			    {
						$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
						          WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
						          AND RecordType = '".PROFILE_TYPE_LATE."'
						          AND DayType = '".PROFILE_DAY_TYPE_AM."'";
						$temp = $lcardattend->returnVector($sql);
						$insert_id = $temp[0];
			    }
			
			    # Update to reason table
			    $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
			    $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
			    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
			                   VALUES ($fieldsvalues) 
			                   on duplicate key update 
			                   	RecordID = LAST_INSERT_ID(RecordID), 
			                   	DateModified = NOW() ";
			    $Result[$i.'3'] = $lcardattend->db_db_query($sql);
			    #########################################################
			}else if ($my_drop_down_status==CARD_STATUS_ABSENT){ # No Daily Record and Absent
			    //$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate); 
			    //$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
			}else if ($my_drop_down_status==CARD_STATUS_OUTING){
			# 
			}

    }
    else  # update if exist
    {
			# Grab original status
			$sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
			$temp = $lcardattend->returnArray($sql,3);
			list($old_status, $old_inTime, $old_record_id) = $temp[0];
					 
			if ($old_status != $my_drop_down_status)
			{
				
			   # Check bad actions
			   # Late / Present -> Absent
			   if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
			   {
			     $lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
			     if($old_inTime!=""){
	           # Has card record but not present in classroom
	           $lcardattend->addBadActionFakedCardAM($my_user_id,$TargetDate,$old_inTime);
		       }
			   }
			   
			   # Absent -> Late / Present
			   if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
			   {
					$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
					# forgot to bring card
					$lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,$old_inTime);
			   }

			   
			
			   # Absent -> Outing
			   if($old_status == CARD_STATUS_ABSENT && $my_drop_down_status==CARD_STATUS_OUTING){
			   
			 		}
			   
			   # Late / Present -> Outing
			   if( ($old_status == CARD_STATUS_PRESENT || $old_status == CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_OUTING){
			 		}
			   
			   # Outing -> Absent
			   if($old_status == CARD_STATUS_OUTING && $my_drop_down_status==CARD_STATUS_ABSENT){
			         //$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
			         //$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
			 		}
			 		
			   # Outing -> Late/Present
			   if($old_status == CARD_STATUS_OUTING && ($my_drop_down_status==CARD_STATUS_PRESENT ||$my_drop_down_status==CARD_STATUS_LATE)){
			         //$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
			         //$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
			 		}
				
			   # Update Daily table
			   $sql = "UPDATE $card_log_table_name SET AMStatus=$my_drop_down_status,  DateModified = NOW() WHERE RecordID=$my_record_id";
			   $Result[$i.'4'] = $lcardattend->db_db_query($sql);
			   
			   ## Bug Tracing
				if($bug_tracing['smartcard_student_attend_status']){
				  $log_date = date('Y-m-d H:i:s');
				  $log_target_date = $TargetDate;
				  $log_student_id = $my_user_id;
				  $log_old_status = $old_status;
				  $log_new_status = $my_drop_down_status;
				  $log_sql = $sql;
				  $log_admin_user = $PHP_AUTH_USER;
				    
					$log_page = 'view_student_update.php';
					$log_content = get_file_content($log_filepath);
					$log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
					$log_content .= $log_entry;
					write_file_content($log_content, $log_filepath);	                        
			 }
			 
			 
		   if ($old_status == CARD_STATUS_ABSENT)
		   {
		       # Remove Previous Absent Record
		       $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		                      WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
		                            AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
		       $Result[$i.'5'] = $lcardattend->db_db_query($sql);
		       $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                      WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
		                            AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
		       $Result[$i.'6'] = $lcardattend->db_db_query($sql);
		   }

		   if ($old_status!=CARD_STATUS_LATE && $my_drop_down_status==CARD_STATUS_LATE)
		   {
			   # Add late to student profile
		       $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
		       $temp = $lcardattend->returnArray($sql,2);
		       list($targetClass, $targetClassNumber) = $temp[0];
		        
		       $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,AcademicYearID,YearTermID";
		       $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber','$school_year_id','$semester_id'";
		       $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) 
		       				VALUES ($fieldvalue) 
		       				on duplicate key update 
		       					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
		       					DateModified = NOW()";
		       $Result[$i.'7'] = $lcardattend->db_db_query($sql);
		       $insert_id = $lcardattend->db_insert_id();
		       
		       # Calculate upgrade items
		       if ($plugin['Disciplinev12'] && $LateCtgInUsed)
				{
					$dataAry = array();
					$dataAry['StudentID'] = $my_user_id;
					$dataAry['RecordDate'] = $TargetDate;
					//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
					$dataAry['Year'] = $school_year;
					$dataAry['YearID'] = $school_year_id;
					//$semester = retrieveSemester($TargetDate);
					$dataAry['Semester'] = $semester;
					$dataAry['SemesterID'] = $semester_id;
					$dataAry['StudentAttendanceID'] = $insert_id;
					$dataAry['Remark'] = $InSchoolTime;
					
					if($SeriousLateUsed == 1) {
							if($InSchoolTime != "")
							{
								$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
								$school_start_time = $arr_time_table['MorningTime'];
								
								$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
								$late_minutes = ceil($str_late_minutes/60);
										
								$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
								$min_late_minutes = $lcardattend->returnVector($sql);		
								
								if(sizeof($min_late_minutes)>0)
								{
									$dataAry['SeriousLate'] = 1;
									$dataAry['LateMinutes'] = $late_minutes;
								}
								else
								{
									$dataAry['SeriousLate'] = 0;
								}
							}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}
						}
						$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
				}
				
		       if (!$insert_id)
		       {
		            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
		                           WHERE UserID = '$my_user_id' AND AttendanceDate = '$TargetDate'
		                           AND RecordType = '".PROFILE_TYPE_LATE."'
		                           AND DayType = '".PROFILE_DAY_TYPE_AM."'";
		            $temp = $lcardattend->returnVector($sql);
		            $insert_id = $temp[0];
		       }
		       
		       # Update to reason table
		       $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
		       $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
		       $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
		                      VALUES ($fieldsvalues) 
		                      on duplicate key update 
		                      	RecordID = LAST_INSERT_ID(RecordID), 
		                      	DateModified = NOW()";
		       $Result[$i.'8'] = $lcardattend->db_db_query($sql);
		   }
		   
		   if ($old_status==CARD_STATUS_LATE && $my_drop_down_status!=CARD_STATUS_LATE)
		   {
		       //# Remove Profile Record
		       $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                      WHERE StudentID = '$my_user_id' AND RecordDate = '$TargetDate'
		                            AND DayType = '".PROFILE_DAY_TYPE_AM."' AND RecordType = '".PROFILE_TYPE_LATE."'";
		       $temp = $lcardattend->returnArray($sql,2);
		       list($tmp_record_id , $tmp_profile_id) = $temp[0];
		       
		       # $tmp_record_id <== RecordID [CARD_STUDENT_PROFILE_RECORD_REASON]
		       # $tmp_profile_id <== StudentAttendanceID [PROFILE_STUDENT_ATTENDANCE]
		       if ($tmp_record_id != '')
		       {
		         # remove late reason [CARD_STUDENT_PROFILE_RECORD_REASON]
		           $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$tmp_record_id'";
		           $Result[$i.'9'] = $lcardattend->db_db_query($sql);
	           }
	           
	           if ($tmp_profile_id != '')
	           {
	               # Reset upgrade items
	               if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	               {
					$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($tmp_profile_id);
	               }	               
	
	               # Calculate upgrade items
	               if ($plugin['Disciplinev12'] && $LateCtgInUsed)
					{
					# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
					    	}

	               # delete attendance record
	               $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$tmp_profile_id'";
	               $Result[$i.'10'] = $lcardattend->db_db_query($sql);
	           }
		       
		   }
			
			   
			}
		
		
			# Try to remove profile records if applicable
			if ($my_drop_down_status != CARD_STATUS_ABSENT)
			{
			   # Remove Previous Absent Record
			   $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
			                  WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
			                        AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
			   $Result[$i.'11'] = $lcardattend->db_db_query($sql);
			   $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
			                  WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
			                        AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
			   $Result[$i.'12'] = $lcardattend->db_db_query($sql);
			}

    }
	}
  else if( $period == "2")	############################# PM
  {
    
    	if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
		{
			$field = "InSchoolTime";
		}
		else # retrieve LunchBackTime
		{
			$field = "LunchBackTime";
		}
		$inTimesql = "select $field from $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";
        $inTimeResult = $lcardattend->returnVector($inTimesql);
        $InSchoolTime = $inTimeResult[0];
	            
          # insert if not exist
          if($my_record_id=="")
          {
                  $sql = "INSERT INTO $card_log_table_name
                                                  (UserID,DayNumber,PMStatus,DateInput,DateModified) VALUES
                                                  ('$my_user_id','$my_day','$my_drop_down_status',NOW(),NOW())
                                          ";
                  $Result[$i.'13'] = $lcardattend->db_db_query($sql);
                  
                  
                  ## Bug Tracing
                  if($bug_tracing['smartcard_student_attend_status']){
                    $log_date = date('Y-m-d H:i:s');
                    $log_target_date = $TargetDate;
                    $log_student_id = $my_user_id;
                    $log_old_status ="";
                    $log_new_status = $my_drop_down_status;
                    $log_sql = $sql;
                    $log_admin_user = $PHP_AUTH_USER;
                    
       				$log_page = 'view_student_update.php';
	            $log_content = get_file_content($log_filepath);
	            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
	            $log_content .= $log_entry;
	            write_file_content($log_content, $log_filepath);	                        
                    
                    
                    
                }                        
                  
                  
                  # Check Bad actions
                  if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
                  {
                    $lcardattend->removeBadActionFakedCardPM($my_user_id,$TargetDate);

                      # Forgot to bring card
                      $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
                  }
                  else if ($my_drop_down_status==CARD_STATUS_LATE)
                  {
                      $lcardattend->removeBadActionFakedCardPM($my_user_id,$TargetDate);

                      # Forgot to bring card
                      $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");

                      ##########################################################
                      # Add late to student profile
                      $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                      $temp = $lcardattend->returnArray($sql,2);
                      list($targetClass, $targetClassNumber) = $temp[0];
                      $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,AcademicYearID,YearTermID";
                      $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber','$school_year_id','$semester_id'";
                      $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue) 
                      				on duplicate key update 
                      					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
                      					DateModified = NOW() 
                      					";
                      $Result[$i.'14'] = $lcardattend->db_db_query($sql);
                      $insert_id = $lcardattend->db_insert_id();
                      # Calculate upgrade items
                      if ($plugin['Disciplinev12'] && $LateCtgInUsed)
				{
					$dataAry = array();
					$dataAry['StudentID'] = $my_user_id;
					$dataAry['RecordDate'] = $TargetDate;
					//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
					$dataAry['Year'] = $school_year;
					$dataAry['YearID'] = $school_year_id;
					//$semester = retrieveSemester($TargetDate);
					$dataAry['Semester'] = $semester;
					$dataAry['SemesterID'] = $semester_id;
					$dataAry['Semester'] = $semester;
					$dataAry['StudentAttendanceID'] = $insert_id;
					$dataAry['Remark'] = $InSchoolTime;
					
					if($SeriousLateUsed == 1) {
							if($InSchoolTime != "")
							{
								$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
								if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
								{
									##$field = "InSchoolTime";
									$school_start_time = $arr_time_table['MorningTime'];
								}
								else # retrieve LunchBackTime
								{
									##$field = "LunchBackTime";
									$school_start_time = $arr_time_table['LunchEnd'];
								}
								
								$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
								$late_minutes = ceil($str_late_minutes/60);
										
								$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
								$min_late_minutes = $lcardattend->returnVector($sql);		
								
								if(sizeof($min_late_minutes)>0)
								{
									$dataAry['SeriousLate'] = 1;
									$dataAry['LateMinutes'] = $late_minutes;
								}
								else
								{
									$dataAry['SeriousLate'] = 0;
								}
							}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}
						}
						$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
                	}

                      if (!$insert_id)
                      {
                           $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                          WHERE UserID = '$my_user_id' AND AttendanceDate = '$TargetDate'
                                          AND RecordType = '".PROFILE_TYPE_LATE."'
                                          AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                           $temp = $lcardattend->returnVector($sql);
                           $insert_id = $temp[0];
                      }

                      # Update to reason table
                      $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                      $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
                      $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                     VALUES ($fieldsvalues) 
                                     on duplicate key update 
                                     	RecordID = LAST_INSERT_ID(RecordID), 
                                     	DateModified = NOW() ";
                      $Result[$i.'15'] = $lcardattend->db_db_query($sql);
                      ############################################################################
                          
                      
                  }



          }
          # update if exist
          else
          {
            
               # Grab original status
               $sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
               $temp = $lcardattend->returnArray($sql,5);
               list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
               
               ## get old time
        if ($lcardattend->attendance_mode==1){ # PM only
			$bad_action_time_field = $old_inTime;
		}else{
			$bad_action_time_field = $old_lunchBackTime;
		}
               if ($old_status != $my_drop_down_status)
               {
                   
                   if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                   {
                     
                         $lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
					if($bad_action_time_field!=""){ # has time but absent
                           # Has card record but not present in classroom
                           $lcardattend->addBadActionFakedCardPM($my_user_id,$TargetDate,$bad_action_time_field);
                         }
                   }
                   
                   # Absent -> Late / Present
                   if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
                   {
                         $lcardattend->removeBadActionFakedCardPM($my_user_id,$TargetDate);
                         # forgot to bring card
                         $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,$bad_action_time_field);
                   }
                   
                   
			
                   # Update Daily table
                   $sql = "UPDATE $card_log_table_name SET PMStatus='$my_drop_down_status', DateModified = now() WHERE RecordID='$my_record_id'";
                   $Result[$i.'16'] = $lcardattend->db_db_query($sql);
                   
                  ## Bug Tracing
                  if($bug_tracing['smartcard_student_attend_status']){
                    $log_date = date('Y-m-d H:i:s');
                    $log_target_date = $TargetDate;
                    $log_student_id = $my_user_id;
                    $log_old_status = $old_status;
                    $log_new_status = $my_drop_down_status;
                    $log_sql = $sql;
                    $log_admin_user = $PHP_AUTH_USER;
                    
       				$log_page = 'view_student_update.php';
	            $log_content = get_file_content($log_filepath);
	            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
	            $log_content .= $log_entry;
	            write_file_content($log_content, $log_filepath);	                        
                    
                    
                    
                }                         
                   

                   // Remove old late/absent records
                   if ($old_status==CARD_STATUS_LATE)
                   {
                   }
                   else if ($old_status == CARD_STATUS_ABSENT)
                   {
                       # Remove Previous Absent Record
                       $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                      WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                            AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                       $Result[$i.'17'] = $lcardattend->db_db_query($sql);
                       $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                      WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                            AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                       $Result[$i.'18'] = $lcardattend->db_db_query($sql);
                   }


               }

               if ($my_drop_down_status != CARD_STATUS_ABSENT)
               {
                   # Remove Previous Absent Record
                   $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                  WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                        AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                   $Result[$i.'19'] = $lcardattend->db_db_query($sql);
                   $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                  WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                        AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                   $Result[$i.'20'] = $lcardattend->db_db_query($sql);
               }


                   if ($old_status!=CARD_STATUS_LATE && $my_drop_down_status==CARD_STATUS_LATE)
                   {
                       # Add late to student profile
                       $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                       $temp = $lcardattend->returnArray($sql,2);
                       list($targetClass, $targetClassNumber) = $temp[0];
                       $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,AcademicYearID,YearTermID";
                       $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber','$school_year_id','$semester_id'";
                       $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)
                       				on duplicate key update 
                       					StudentAttendanceID = LAST_INSERT_ID(StudentAttendanceID), 
                       					DateModified = Now()";
                       $Result[$i.'21'] = $lcardattend->db_db_query($sql);
                       $insert_id = $lcardattend->db_insert_id();

                       # Calculate upgrade items
                       if ($plugin['Disciplinev12'] && $LateCtgInUsed)
				{
					$dataAry = array();
					$dataAry['StudentID'] = $my_user_id;
					$dataAry['RecordDate'] = $TargetDate;
					//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
					$dataAry['Year'] = $school_year;
					$dataAry['YearID'] = $school_year_id;
					//$semester = retrieveSemester($TargetDate);
					$dataAry['Semester'] = $semester;
					$dataAry['SemesterID'] = $semester_id;
					$dataAry['Semester'] = $semester;
					$dataAry['StudentAttendanceID'] = $insert_id;
					$dataAry['Remark'] = $InSchoolTime;
					
					if($SeriousLateUsed == 1) {
							if($InSchoolTime != "")
							{
								$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
								if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
								{
									##$field = "InSchoolTime";
									$school_start_time = $arr_time_table['MorningTime'];
								}
								else # retrieve LunchBackTime
								{
									##$field = "LunchBackTime";
									$school_start_time = $arr_time_table['LunchEnd'];
								}
								
								$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
								$late_minutes = ceil($str_late_minutes/60);
										
								$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
								$min_late_minutes = $lcardattend->returnVector($sql);		
								
								if(sizeof($min_late_minutes)>0)
								{
									$dataAry['SeriousLate'] = 1;
									$dataAry['LateMinutes'] = $late_minutes;
								}
								else
								{
									$dataAry['SeriousLate'] = 0;
								}
							}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}
						}
						$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
                	}
                       if (!$insert_id)
                       {
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                           AND RecordType = '".PROFILE_TYPE_LATE."'
                                           AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                            $temp = $lcardattend->returnVector($sql);
                            $insert_id = $temp[0];
                       }
                       # Update to reason table
                       $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                       $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
                       $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                      VALUES ($fieldsvalues) 
                                      on duplicate key update 
                                      	RecordID = LAST_INSERT_ID(RecordID), 
                                      	DateModified = NOW()";
                       $Result[$i.'22'] = $lcardattend->db_db_query($sql);
                   }
                   
                   if ($old_status==CARD_STATUS_LATE && $my_drop_down_status!=CARD_STATUS_LATE)
                   {
                       # Remove Profile Record
                       $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                      WHERE StudentID = '$my_user_id' AND RecordDate = '$TargetDate'
                                            AND DayType = '".PROFILE_DAY_TYPE_PM."' AND RecordType = '".PROFILE_TYPE_LATE."'";
                       $temp = $lcardattend->returnArray($sql,2);
                       list($tmp_record_id , $tmp_profile_id) = $temp[0];
                       if ($tmp_record_id != '')
                       {
                           $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$tmp_record_id'";
                           $Result[$i.'23'] = $lcardattend->db_db_query($sql);
                           
                           # update attendance record as "delete"
                           $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE set RecordStatus=-1 WHERE StudentAttendanceID = '$tmp_profile_id'";
                           $Result[$i.'24'] = $lcardattend->db_db_query($sql);
                       }
                       
                       if ($tmp_profile_id != '')
                           {
                            # Reset upgrade items
                            if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                               {
                                  $ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($tmp_profile_id);
                               }

                               # Calculate upgrade items
                               if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
							# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
                    	}
                               
                               $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$tmp_profile_id'";
                                $Result[$i.'25'] = $lcardattend->db_db_query($sql);
                                
                           }
                   }


          }
  }
	*/
}

## mark as confirmed by admin user
if(count($user_id)>0){
	$confirmed_by_admin_field = $DayType == PROFILE_DAY_TYPE_PM? "PMConfirmedByAdmin" : "AMConfirmedByAdmin";
	$sql = "UPDATE $card_log_table_name SET $confirmed_by_admin_field='1' WHERE DayNumber='$txt_day' AND UserID IN (".implode(",",$user_id).")";
	$lcardattend->db_db_query($sql);
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

if( $confirmed_id <> "" )
{
        # update if record exist
        $sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID=".$_SESSION['UserID'].", DateModified = NOW() WHERE RecordID = '$confirmed_id'";
        $Result[$i.'26'] = $lcardattend->db_db_query($sql);
        $msg = 2;
}
else
{
        # insert if record not exist
        $class_id = $lcardattend->getClassID($class_name);

        $sql = "INSERT INTO $card_student_daily_class_confirm
                                        (
                                                ClassID,
                                                ConfirmedUserID,
                                                DayNumber,
                                                DayType,
                                                DateInput,
                                                DateModified
                                        ) VALUES
                                        (
                                                '$class_id',
                                                '".$_SESSION['UserID']."',
                                                '$txt_day',
                                                '$DayType',
                                                NOW(),
                                                NOW()
                                        )
                ON DUPLICATE KEY UPDATE 
                	ConfirmedUserID='".$_SESSION['UserID']."', DateModified = NOW()
                                        ";
        $Result[$i.'27'] = $lcardattend->db_db_query($sql);
        $msg = 1;
}

// remove the confirm cache status
$sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$TargetDate."' and DayType = '".$DayType."' and Type = 'Class'";
$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);

## UNLOCK TABLES
//$lcardattend->unlockTables();

### Lunch box cancel status
if ($DayType == PROFILE_DAY_TYPE_AM && $sys_custom['SmartCardAttendance_LunchBoxOption'])
{
	$lunch_tablename = $lcardattend->createTable_Card_Student_Lunch_Box_Option($txt_year, $txt_month);
	$dayNumber = $txt_day;
	for ($i=0; $i<sizeof($user_id); $i++)
	{
		$t_student_id = $user_id[$i];
		$option = ${"LunchBoxOption_$t_student_id"};
		$option += 0;
		$sql = "INSERT IGNORE INTO $lunch_tablename (StudentID, DayNumber, CancelOption, DateInput, DateModified)
		              VALUES ('$t_student_id', '$dayNumber', '".$option."', now(), now())";
		$Result[$i.'28'] = $lcardattend->db_db_query($sql);
	if ($lcardattend->db_affected_rows()!=1)
	{
		$sql = "UPDATE $lunch_tablename SET CancelOption = '$option' WHERE StudentID = '$t_student_id' AND DayNumber = '$dayNumber'";
		$Result[$i.'29'] = $lcardattend->db_db_query($sql);
	}
	
	
	}
}

if (!in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['DailyStatusConfirmSuccess'];
	$lcardattend->Commit_Trans($sql);
	$log_row .= " COMMITTED";
}
else {
	$Msg = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
	$lcardattend->RollBack_Trans($sql);
	$log_row .= " ROLLBACKED";
}

$lcardattend->log($log_row);
intranet_closedb();
$return_url = "view_student.php?class_name=$class_name&class_id=$class_id&DayType=$DayType&TargetDate=$TargetDate&Msg=".urlencode($Msg);
header("Location: $return_url");
?>