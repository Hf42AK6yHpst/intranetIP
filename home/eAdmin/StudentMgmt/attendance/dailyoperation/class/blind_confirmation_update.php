<?php
// editing by 
############################################# Change Log #############################################
# 2020-05-19 (Ray): late & absent add InsertSubmittedAbsentLateRecord
# 2018-08-03 (Carlos): [ip.2.5.9.10.1] Mark attendance records confirmed by admin.
# 2012-03-22 (Carlos): check setting OnlySynceDisLateRecordWhenConfirm to update eDiscipline late record at Set_Profile()
# 2011-12-16 (Carlos): Modified to separate AM and PM Modify fields
# 2010-02-02 (Carlos): Add checking on confirm for all class to skip class which has been confirmed already
######################################################################################################
set_time_limit(0);
ini_set('memory_limit','500M');
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $ts_record == FALSE)
{
    $ts_record = strtotime(date('Y-m-d'));
    $TargetDate = date('Y-m-d');
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lcardattend->attendance_mode;
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();
$LIDB = new libdb();

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];
$upadteeDisLateRecord = $lc->OnlySynceDisLateRecordWhenConfirm != '1';
###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM:
		$display_period = $i_DayTypeAM;
    $link_page = "AM";
    $expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
    $IsConfirmField = "IsConfirmed";
		$ConfirmUserField = "ConfirmedUserID";
		$StatusField = "AMStatus";
		
	$DateModifiedField = "DateModified";
	$ModifyByField = "ModifyBy";
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    $link_page = "PM";
    $expect_field = $lc->Get_PM_Expected_Field("b.","c.","f.");
    $IsConfirmField = "PMIsConfirmed";
		$ConfirmUserField = "PMConfirmedUserID";
		$StatusField = "PMStatus";
	
	$DateModifiedField = "PMDateModified";
	$ModifyByField = "PMModifyBy";
    break;
  default :
  	$display_period = $i_DayTypeAM;
    $DayType = PROFILE_DAY_TYPE_AM;
    $expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
    $link_page = "AM";
    $IsConfirmField = "IsConfirmed";
		$ConfirmUserField = "ConfirmedUserID";
		$StatusField = "AMStatus";
	
	$DateModifiedField = "DateModified";
	$ModifyByField = "ModifyBy";
    break;
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# select confirmed classes to skip
$sqlClassConfirmed = "SELECT ClassID, RecordID FROM $card_student_daily_class_confirm WHERE DayNumber = '$txt_day' AND DayType = '$DayType' ";
$temp = $lcardattend->returnArray($sqlClassConfirmed);
for($i=0;$i<sizeof($temp);$i++)
{
	$resultClassConfirmed[$temp[$i]['ClassID']] = $temp[$i]['RecordID'];
}	
	
### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# select confirmed classes to skip
$sqlClassConfirmed = "SELECT ClassID, RecordID FROM $card_student_daily_class_confirm WHERE DayNumber = '$txt_day' AND DayType = '$DayType' ";
$temp = $lc->returnArray($sqlClassConfirmed);
for($i=0;$i<sizeof($temp);$i++)
{
	$resultClassConfirmed[$temp[$i]['ClassID']] = $temp[$i]['RecordID'];
}
	
# select the timetable mode for each class
$resultClassMode = $lc->getClassListMode();
$j = 0;
for($i=0;$i<sizeof($resultClassMode);$i++){
	$classID = $resultClassMode[$i][0];
	$className = $resultClassMode[$i][1];
	$classMode = $resultClassMode[$i][3];
	$specialClassID= $classMode==0?0:$classID;
	
	if (!$lc->isRequiredToTakeAttendanceByDate($className,$TargetDate) || $resultClassConfirmed[$classID]) {
		$SkipClass[] = $classID;
	}
	else {
		$confirmClasses[$j]['ClassID'] = $classID;
		$confirmClasses[$j]['ClassName'] = $className;
		$j++;
	}
}

if ($lc->EnableEntryLeavePeriod) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$TargetDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}
### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$sql = "SELECT 
					b.RecordID, 
					a.UserID,
        	".$expect_field." as Status 
      FROM
        INTRANET_USER AS a 
        ".$FilterInActiveStudent ." 
        INNER JOIN
				YEAR_CLASS AS e
				ON
          a.RecordType=2
          AND a.RecordStatus IN (0,1,2)
          AND a.ClassName != '' 
          and a.ClassName=e.ClassTitleEN
          and e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' ";
if (sizeof($SkipClass) > 0) {          
	$sql .= "
	        and e.YearClassID not in ('".implode("','",$SkipClass)."') ";
}
$sql .= "
        LEFT JOIN 
        ".$card_log_table_name." AS b
        ON a.UserID=b.UserID 
        	AND b.DayNumber = ".$txt_day." 
        LEFT JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$TargetDate."' AND c.DayType = '".$DayType."')
        LEFT JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod='".$DayType."' AND f.RecordDate='$txt_year-$txt_month-$txt_day') 
      ";
//debug_r($sql); die;
$StudentStatus = $lc->returnArray($sql);

$student_id_ary = array();
	
### for student confirm
for($i=0; $i<sizeOf($StudentStatus); $i++)
{
	list($my_record_id,$my_user_id,$my_status) = $StudentStatus[$i];
  	$my_day = $txt_day;
	$student_id_ary[] = $my_user_id;
	
  # insert if not exist
  if($my_record_id=="")
  {                    
    $sql = "INSERT INTO $daily_log_table_name
            (UserID,DayNumber,".$StatusField.",".$IsConfirmField.",".$ConfirmUserField.",DateInput,InputBy,".$DateModifiedField.",".$ModifyByField.") VALUES
            ($my_user_id,$my_day,$my_status,'1','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')
            ";
    //echo "<p>$sql</p>";
    $LIDB->db_db_query($sql);
  }
  else  # update if exist
  {
		$sql ="UPDATE $daily_log_table_name SET 
						".$StatusField." = '".$my_status."',
						".$IsConfirmField." = '1',
						".$ConfirmUserField." = '".$_SESSION['UserID']."',
						".$ModifyByField." = '".$_SESSION['UserID']."',
						".$DateModifiedField." = NOW() 
					WHERE RecordID = $my_record_id";
		$LIDB->db_db_query($sql);
		//echo "<p>$sql</p>";
  }

	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		if ($my_status==CARD_STATUS_LATE) {
			$Result[$my_user_id . 'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType, '', '', PROFILE_TYPE_LATE, '', '');
		}
		if ($my_status==CARD_STATUS_ABSENT) {
			$Result[$my_user_id . 'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType, '', '', CARD_LEAVE_NORMAL, '', '');
		}
	}

  if ($my_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING || $my_status==CARD_STATUS_LATE) {
		$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'|**NULL**|',false,$upadteeDisLateRecord);
	}
	else if ($my_status==CARD_STATUS_PRESENT) {
		$Result[$my_user_id.'ClearProfile'] = $lcardattend->Clear_Profile($my_user_id,$TargetDate,$DayType,$upadteeDisLateRecord);
	}
}
//die;

## mark as confirmed by admin user
$confirmed_by_admin_field = $DayType == PROFILE_DAY_TYPE_PM? "PMConfirmedByAdmin" : "AMConfirmedByAdmin";
$sql = "UPDATE $card_log_table_name SET $confirmed_by_admin_field='1' WHERE DayNumber='$txt_day'";
$lcardattend->db_db_query($sql);


### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

for($i=0;$i<sizeof($confirmClasses);$i++){
		$class_id = $confirmClasses[$i]['ClassID'];
		$class_name = $confirmClasses[$i]['ClassName'];	
		
    # insert if record not exist
    $sql = "INSERT INTO $card_student_daily_class_confirm
            (
              ClassID,
              ConfirmedUserID,
              DayNumber,
              DayType,
              DateInput,
              DateModified
            ) VALUES
            (
              '$class_id',
              '".$_SESSION['UserID']."',
              '$txt_day',
              '$DayType',
              NOW(),
              NOW()
						)
            ON DUPLICATE KEY UPDATE 
            	ConfirmedUserID='".$_SESSION['UserID']."', 
            	DateModified = NOW()
					";
    $LIDB->db_db_query($sql);
    //echo "<p>$sql</p>";
    $msg = 1;
}

// remove the confirm cache status
$sql = "replace into CARD_STUDENT_CACHED_CONFIRM_STATUS (
					TargetDate,
					DayType,
					Type,
					ConfirmStatus) 
				value (
				 '".$TargetDate."',
				 '".$DayType."',
				 'Class',
				 '".ALLCONFIRM."'
				)";
$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);

$return_url = "class_status.php?TargetDate=$TargetDate&DayType=$DayType&msg=$msg";

header( "Location: $return_url");
?>