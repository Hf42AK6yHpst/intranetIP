<?php
// Editing by 

/*
 * 2018-05-02 (Carlos): $sys_custom['StudentAttendance']['TakeAttendanceSummaryStatistics'] - added summary statistics of status counting.
 * 2016-08-22 (Carlos): $sys_custom['StudentAttendance']['PrintAttendanceRecords'] - added print [Attendance Records] page.
 * 2014-07-16 (Bill): Display radio button
 * 2010-10-07 (Kenneth Chung): Unified view_student_AM.php/ view_student_PM.php to view_student.php
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewClassStatus";

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

#class used
$LIDB = new libdb();
$lc->retrieveSettings();
$attendanceMode=$lc->attendance_mode;
### Set Date from previous page
if ($TargetDate == "")
{
	$TargetDate = date('Y-m-d');
}
$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
$lc->createTable_LogAndConfirm($txt_year,$txt_month);

###period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM:
			$display_period = $i_DayTypeAM;
      $link_page = "AM";
      $confirm_page="AM";
      break;
    case PROFILE_DAY_TYPE_PM: 
    	$display_period = $i_DayTypePM;
      //if($attendanceMode==2||$attendanceMode==3){
      //        $confirm_page="PM_S";
      //}else{
      $confirm_page="PM";
      //}
      $link_page="PM";
      break;
    default : 
    	if ($lc->attendance_mode == 1) {
    		$display_period = $i_DayTypePM;      
    		$DayType = PROFILE_DAY_TYPE_PM;
	      $confirm_page="PM";
	      $link_page="PM";
    	}
    	else {
	    	$display_period = $i_DayTypeAM;
		    $DayType = PROFILE_DAY_TYPE_AM;
		    $link_page = "AM";
		    $confirm_page="AM";
		  }
	    break;
}

### build student table
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$sql = "SELECT DISTINCT
            yc.YearClassID,
            yc.ClassTitleEN,
            yc.ClassTitleB5,
            IF(b.ConfirmedUserID IS NULL,
              '-',
              IF(b.ConfirmedUserID <> -1, ".getNameFieldByLang("c.").", CONCAT('$i_general_sysadmin'))
             ),
            IF(b.DateModified IS NULL, '-', b.DateModified)
        FROM 
        	YEAR as y 
        	inner join 
        	YEAR_CLASS yc 
        	on y.YearID = yc.YearID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
	        LEFT OUTER JOIN 
	        $card_student_daily_class_confirm as b
          ON (yc.YearClassID=b.ClassID AND
             (b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' OR b.DayNumber IS NULL) AND
             (b.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' || b.DayType IS NULL))
	        LEFT OUTER JOIN 
	        INTRANET_USER as c 
	        ON (b.ConfirmedUserID=c.UserID OR b.ConfirmedUserID=-1) 
	      ORDER BY y.sequence, yc.sequence
				";
//echo $sql; die;
$result = $LIDB->returnArray($sql, 4);

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_ClassName</td><td class=\"tabletoplink\">$i_StudentAttendance_Field_ConfirmedBy</td><td class=\"tabletoplink\">$i_StudentAttendance_Field_LastConfirmedTime</td></tr>\n";
for($i=0; $i<sizeOf($result); $i++)
{		
        list($class_id, $class_name_en, $class_name_b5, $confirmed_username, $confirmed_date) = $result[$i];
        $css=($i%2==0)?"tablerow1":"tablerow2";
        $class_name = Get_Lang_Selection($class_name_b5,$class_name_en);
        if($lc->isRequiredToTakeAttendanceByDate($class_name_en,$TargetDate)){
	        $class_link = "<a class=\"tablelink\" href=\"view_student.php?class_name=".urlencode($class_name_en)."&class_id=".urlencode($class_id)."&TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."\">".intranet_htmlspecialchars($class_name)."</a>";
	        $x .= "<tr class=\"$css\"><td class=\"tabletext\">$class_link</td><td class=\"tabletext\">$confirmed_username</td><td class=\"tabletext\">$confirmed_date</td></tr>\n";
        }else{
	        $class_link = "<a class=functionlink_new>$class_name</a>";
	        $x .="<tr class=\"$css\"><td class=\"tabletext\">$class_link</td><td class=\"tabletext\" colspan=\"2\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>";
	    }
}
$x .= "</table>\n";

if($sys_custom['StudentAttendance']['TakeAttendanceSummaryStatistics']){
	// summary statistics
	$card_student_daily_log_table = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
	$sql = "SELECT 
				u.UserID,d.RecordID".($DayType == PROFILE_DAY_TYPE_AM? ",d.AMStatus as Status":",d.PMStatus as Status").",d.LeaveStatus".($DayType == PROFILE_DAY_TYPE_AM?",d.IsConfirmed":",d.PMIsConfirmed as IsConfirmed")." 
			FROM INTRANET_USER as u ";
	if ($lc->EnableEntryLeavePeriod) {
		$sql .= "INNER JOIN CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp ON u.UserID = selp.UserID AND '".$lc->Get_Safe_Sql_Query($TargetDate)."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
	}
	$sql.=" LEFT JOIN $card_student_daily_log_table as d ON d.UserID=u.UserID AND d.DayNumber='".$lc->Get_Safe_Sql_Query($txt_day)."'
			WHERE u.RecordType='2' AND u.RecordStatus IN (0,1,2) AND u.ClassName IS NOT NULL";
	$records = $lc->returnResultSet($sql);
	$number_of_student = count($records);
	$number_of_ontime_student = 0;
	$number_of_absent_student = 0;
	$number_of_late_student = 0;
	$number_of_outing_student = 0;
	$number_of_nonconfirmed_student = 0;
	for($i=0;$i<$number_of_student;$i++){
		$status = $records[$i]['Status'];
		$is_confirmed = $records[$i]['IsConfirmed'];
		if($is_confirmed != '1'){
			$number_of_nonconfirmed_student += 1;
		}
		if($status == CARD_STATUS_PRESENT){
			$number_of_ontime_student += 1;
		}else if($status == CARD_STATUS_ABSENT){
			$number_of_absent_student += 1;
		}else if($status == CARD_STATUS_LATE){
			$number_of_late_student += 1;
		}else if($status == CARD_STATUS_OUTING){
			$number_of_outing_student += 1;
		}
	}
	$stat='<tr>
			<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" style="margin-bottom:16px;">
				<tbody>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['TotalNumberOfStudent'].'</td>
					<td class="tabletext" width="70%">'.$number_of_student.'</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['TotalNumberOfOnTimeStudent'].'</td>
					<td class="tabletext" width="70%">'.$number_of_ontime_student.'</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['TotalNumberOfAbsentStudent'].'</td>
					<td class="tabletext" width="70%">'.$number_of_absent_student.'</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['TotalNumberOfLateStudent'].'</td>
					<td class="tabletext" width="70%">'.$number_of_late_student.'</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['TotalNumberOfOutingStudent'].'</td>
					<td class="tabletext" width="70%">'.$number_of_outing_student.'</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['TotalNumberOfNonConfirmedStudent'].'</td>
					<td class="tabletext" width="70%">'.$number_of_nonconfirmed_student.'</td>
				</tr>
				</tbody>
			</table>
			</td>
		</tr>';
}


$data = Array(
	        Array("1",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll),
	        Array("2",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs),
	        Array("3",$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate),
	        Array("4",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate)
        );
if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
{
	$data[] = array(5,$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel);
}

if($sys_custom['StudentAttendance']['PrintAttendanceRecords']){
	$data[] = array(6, $Lang['StudentAttendance']['AttendanceRecords']);
}

//$PrintFunction = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$PrintFunction = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">";
$PrintFunction .= "$i_SmartCard_DailyOperation_viewClassStatus_PrintOption</td>";
$PrintFunction .= "<td>".getSelectByArray($data, " name=print_option", $print_option,0,1)."</td></tr>";
$PrintFunction .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
$PrintFunction .= "<tr><td colspan=\"2\" align=\"center\">".$linterface->GET_ACTION_BTN($button_print, "button", "", "print2", "onclick='openPrintPage()'")."</td></tr>";
//$PrintFunction .= "</table>";

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewClassStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($display_period);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
	var option = document.form2.print_option.value;
	if(option=="")
	{
	      alert("<?=$i_alert_pleaseselect?>");
	}
	else
	{
<?php if($sys_custom['StudentAttendance']['PrintAttendanceRecords']){ ?>
		if(option==6){
			newWindow("print_attendance_records.php?TargetDate=<?=urlencode($TargetDate)?>&DayType=<?=urlencode($DayType)?>",24);
		}else 
<?php } ?>
	  if (option==5)
		{
		  newWindow("view_student_lunch_cancel_print.php?TargetDate=<?=urlencode($TargetDate)?>",4);
		}
		else
		{
			newWindow("view_student_print.php?DayType=<?=urlencode($DayType)?>&TargetDate=<?=urlencode($TargetDate)?>&option="+option,4);
		}
	}
}
-->
</script>
<br />
<?
// Display radio button
echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","class_status.php","Class",$TargetDate,$DayType, 1);
?>
<form name="form2" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<!--
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	-->
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StudentAttendance_Field_Date ?></td>
					<td class="tabletext" width="70%"><?=intranet_htmlspecialchars($TargetDate)?> (<?=$display_period?>)</td>
				</tr>
				<?=$PrintFunction?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?=$absent_count?></td>
					<td align="right"><?=$SysMsg?></td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<?=$stat?>
	<tr>
		<td>
			<?=$x?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			    </tr>
			    <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($i_SmartCard_DailyOperation_BlindConfirmation, "button", "window.location.href='blind_confirmation.php?TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.print_option");
intranet_closedb();
?>
