<?php
// editing by 
##################################### Change Log ###############################################
# 2019-03-01 by Carlos: Skip not require to take attedance classes.
# 2018-11-22 by Carlos: Modified to make Office Remark to follow the take attendance page fetch logic.
# 2015-02-13 by Carlos: [ip2.5.6.3.1] Added [Waived], [Reason], [Office Remark]. 
# 2014-07-16 by Bill: 	Add student count
# 2013-09-03 by Carlos: inner join YEAR_CLASS_USER, YEAR_CLASS to strictly get current year students
# 2013-04-16 by Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2011-11-04 by Carlos: Display signature field at beginning of each class list
# 2010-02-09 by Carlos: Add setting Default PM status not to follow AM status
################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$SettingList[] = "'PMStatusNotFollowAMStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];

#$header_filepath = "/templates/". $LAYOUT_SKIN ."/layout/print_header.php"";
$header_filepath = "$intranet_root/templates/reportheader/viewstudentattendance.php";
$header_inclusion = is_file($header_filepath);

### internal function
function printHeader($period_slot,$target_date,$ClassName, $HasPresetRecord){
        global $intranet_root, $i_StudentAttendance_Menu_DailyOperation;
        global $i_StudentAttendance_System, $i_StudentAttendance_ViewTodayRecord;
        global $i_StudentAttendance_Slot, $i_StudentAttendance_Slot_AM, $i_StudentAttendance_Slot_PM, $i_StudentAttendance_Slot_AfterSchool;
        global $i_StudentAttendance_View_Date, $i_StudentAttendance_ToSchoolTime;
        global $i_ClassName, $i_UserStudentName, $i_SmartCard_Frontend_Take_Attendance_In_School_Time, $i_StudentAttendance_Status;
        global $header_inclusion,$header_filepath;
        global $i_Attendance_Remark, $i_Attendance_Reason, $i_SmartCard_Frontend_Take_Attendance_Waived;
        global $i_Payment_Receipt_Payment_StaffInCharge_Signature;
		global $Lang;

        if ($header_inclusion)
        {
            //include_once($header_filepath);
            include($header_filepath);
        }
        $pageheader = "";
        $pageheader .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center'>";
        $pageheader .= "<tr><td class='eSportprinttitle' colspan='2'><u>$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)</u></td></tr>";

        $pageheader .= "<tr><td class='eSportprinttitle'>$i_ClassName</td>";
        $pageheader .= "<td width='70%' class='eSportprinttitle'>$ClassName</td></tr>";
        $pageheader .= "<tr><td class='eSportprinttitle'>$i_StudentAttendance_Slot</td>";
        $pageheader .= "<td width='70%' class='eSportprinttitle'>";
        switch ($period_slot)
        {
                        case PROFILE_DAY_TYPE_AM: $pageheader .= "$i_StudentAttendance_Slot_AM"; break;
                        case PROFILE_DAY_TYPE_PM: $pageheader .= "$i_StudentAttendance_Slot_PM"; break;
                        default : $pageheader .= ""; break;
        }
        $pageheader .= "</tr>";
        $pageheader .= "<tr><td class='eSportprinttitle'>$i_StudentAttendance_View_Date</td>";
        $pageheader .= "<td width='70%' class='eSportprinttitle'>$target_date</td></tr>";
        $pageheader .= "</table>";

        $pageheader .= "<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
        $pageheader .= "<tr>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>#</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>$i_UserStudentName</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>$i_StudentAttendance_Status</td>";
        if($HasPresetRecord==1) $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['SmartCard']['StudentAttendance']['PresetAbsenceInfo']."</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>$i_SmartCard_Frontend_Take_Attendance_Waived</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Attendance_Reason</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";
        $pageheader .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['StudentAttendance']['OfficeRemark']."</td>";
		$pageheader .= "</tr>";
        echo $pageheader;
}

### class used
$li = new libdb();

$page_breaker = "<p class='breakhere'>";
$pagefooter = "</table>";
$prevClass = "";

$i_title = "$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)";

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
#include_once("../../../../templates/fileheader.php");
?>
<style type="text/css">
     p.breakhere {page-break-before: always}
</style>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### build table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($lc->EnableEntryLeavePeriod) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$TargetDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}

$conds = "";

if($class_name<>"")
{
        $conds = " AND a.ClassName = \"$class_name\" ";
}

$db_status_field = ($DayType==PROFILE_DAY_TYPE_PM?$lc->Get_PM_Expected_Field("b.","c.","f."):$lc->Get_AM_Expected_Field("b.","c.","f."));

if ($option == 1)
{
}
else if ($option == 2)
{
     $conds .= " AND $db_status_field = ".CARD_STATUS_ABSENT;
}
else if ($option == 3)
{
     $conds .= " AND $db_status_field = ".CARD_STATUS_LATE;
}
else if ($option == 4)
{
     $conds .= " AND $db_status_field in ('".CARD_STATUS_LATE."','".CARD_STATUS_ABSENT."')";
}

if($DayType==PROFILE_DAY_TYPE_AM)        // AM
{
	$expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
	$AbsentExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
	$LateExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lc->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
	
	$status_field = "AMStatus";
	
	$office_remark_field = "IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,c.Detail)) as OfficeRemark ";
	
	$sql  = "SELECT        
						".getNameFieldByLang("a.")."as name,
						a.ClassName,
						a.ClassNumber,
						IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
						IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
						".$lc->Get_AM_Expected_Field("b.","c.","f.").", 
						 IF(d.Remark IS NULL, '-',d.Remark),
						 a.UserID,
						 IF(f.Reason IS NULL,'',f.Reason) as PresetReason,
						 j.RecordStatus as Waived,
						 if (".$expect_field." = '".CARD_STATUS_ABSENT."',
							".$AbsentExpectedReasonField.",
							IF(".$expect_field." = '".CARD_STATUS_LATE."',
								".$LateExpectedReasonField.",
								IF(".$expect_field." = '".CARD_STATUS_OUTING."',
									".$OutingExpectedReasonField.",
									j.Reason))) as Reason,
						 IF(f.Remark IS NULL,'',f.Remark),
						 $office_remark_field 
					FROM
						INTRANET_USER AS a 
						".$FilterInActiveStudent ." 
						INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID 
			  			INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$lc->CurAcademicYearID."' 
						LEFT JOIN $card_log_table_name AS b
						        ON        (
						                                a.UserID=b.UserID AND
						                                (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
						                        )
						LEFT JOIN CARD_STUDENT_OUTING AS c
						        ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = '".$DayType."')
						LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d ON(a.UserID=d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType')
						LEFT JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod=2 AND f.RecordDate='$txt_year-$txt_month-$txt_day') 
						LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j 
							ON j.StudentID = a.UserID 
								AND j.RecordDate = '".$TargetDate."' 
								AND j.DayType = '".$DayType."' 
								AND 
								(
									j.RecordType = b.".$status_field." 
									OR  
									(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
									AND b.".$status_field." = '".CARD_STATUS_OUTING."' 
									)
								)
					WHERE
						(b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
						a.RecordType=2 AND
						a.RecordStatus IN (0,1,2) AND
						a.ClassName IS NOT NULL AND a.ClassName !=\"\"
						$conds
					ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName
	";
		$sqlCountPresetAbsence = "SELECT A.CLASSNAME, COUNT(*) AS 'NUM' FROM INTRANET_USER AS A INNER JOIN CARD_STUDENT_PRESET_LEAVE AS B 
								  ON (A.USERID = B.STUDENTID) WHERE A.RECORDTYPE=2 AND A.RECORDSTATUS IN (0,1,2) AND B.RECORDDATE='$txt_year-$txt_month-$txt_day' AND B.DayPeriod=2 GROUP BY A.CLASSNAME";

}
else if($DayType==PROFILE_DAY_TYPE_PM)         //PM
{
	$expect_field = "IF(b.PMStatus IS NOT NULL,
                          b.PMStatus,
                          ".$lc->Get_PM_Expected_Field("b.","c.","f.").")";
    $AbsentExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
	$LateExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lc->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
    
    $status_field = "PMStatus";
	
	$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k 
										ON 
											k.StudentID = a.UserID 
											AND k.RecordDate = '".$TargetDate."' 
											AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
											AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
	
	$office_remark_field = "IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,IF(c.Detail IS NOT NULL,c.Detail,k.OfficeRemark))) as OfficeRemark ";
	
        $sql  = "SELECT        
        					".getNameFieldByLang("a.")."as name,
									a.ClassName,
									a.ClassNumber,
									IF(b.LunchBackTime IS NULL,
									  '-',
									  b.LunchBackTime),
									IF(b.LunchBackStation IS NULL,
									  '-',
									  b.LunchBackStation),
									".$lc->Get_PM_Expected_Field("b.","c.","f.").",
									IF(d.Remark IS NULL,'-',d.Remark),
									a.UserID,
									IF(f.Reason IS NULL,'',f.Reason) as PresetReason,
									j.RecordStatus as Waived,
									if (".$expect_field." = '".CARD_STATUS_ABSENT."',
										".$AbsentExpectedReasonField.",
										IF(".$expect_field." = '".CARD_STATUS_LATE."',
											".$LateExpectedReasonField.",
											IF(".$expect_field." = '".CARD_STATUS_OUTING."',
												".$OutingExpectedReasonField.",
												j.Reason))) as Reason,
									IF(f.Remark IS NULL,'',f.Remark),
									$office_remark_field 
								FROM
								  INTRANET_USER AS a 
								  ".$FilterInActiveStudent ." 
								  INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID 
		  						  INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$lc->CurAcademicYearID."' 
									LEFT JOIN $card_log_table_name AS b
									ON        (
									  a.UserID=b.UserID AND
									  (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
									)
									LEFT JOIN CARD_STUDENT_OUTING AS c
									ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = '".$DayType."')
									LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType')
									LEFT JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod=3 AND f.RecordDate='$txt_year-$txt_month-$txt_day') 
									LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j 
										ON j.StudentID = a.UserID 
											AND j.RecordDate = '".$TargetDate."' 
											AND j.DayType = '".$DayType."' 
											AND 
											(
												j.RecordType = b.".$status_field." 
												OR  
												(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
												AND b.".$status_field." = '".CARD_STATUS_OUTING."' 
												)
											)
									".$AMJoinTable." 
								WHERE
								  (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
								  a.RecordType=2 AND
								  a.RecordStatus IN(0,1,2) AND
								  a.ClassName IS NOT NULL AND a.ClassName !=\"\"
								  $conds
								ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName
";
	$sqlCountPresetAbsence = "SELECT A.CLASSNAME, COUNT(*) AS 'NUM' FROM INTRANET_USER AS A INNER JOIN CARD_STUDENT_PRESET_LEAVE AS B 
							  ON (A.USERID = B.STUDENTID) WHERE A.RECORDTYPE=2 AND A.RECORDSTATUS IN (0,1,2) AND B.RECORDDATE='$txt_year-$txt_month-$txt_day' AND B.DayPeriod=3 GROUP BY A.CLASSNAME";
}


$result = $li->returnArray($sql);

$CountPresetAbsenceResult = $li->returnArray($sqlCountPresetAbsence, 2);
$PresetAbsenceArr = array();
for($j=0;$j<sizeof($CountPresetAbsenceResult);$j++)
{
	//e.g. $PresetAbsenceArr['1A'] = NUM; where value NUM is Number of Preset Absence Records for this Class
	$PresetAbsenceArr[$CountPresetAbsenceResult[$j]['CLASSNAME']]=$CountPresetAbsenceResult[$j]['NUM'];
}

$classNameToRequireToTakeAttendanceAry = array();
$n = -1;
for ($i=0; $i<sizeof($result); $i++)
{
        list ($name,$ClassName,$ClassNumber,$InSchoolTime,$InSchoolStation,$attend_status,$remark,$t_studentID,$PresetReason,$Waived,$Reason,$PresetRemark,$OfficeRemark) = $result[$i];
		
		if(!isset($classNameToRequireToTakeAttendanceAry[$ClassName])){
			$classNameToRequireToTakeAttendanceAry[$ClassName] = $lc->isRequiredToTakeAttendanceByDate($ClassName,$TargetDate);
		}
		if(!$classNameToRequireToTakeAttendanceAry[$ClassName]){
			continue;
		}
		
		$n++;
		
		if($PresetAbsenceArr[$ClassName] > 0)
		{
			$HasPresetRecord = 1;
		}else
		{
			$HasPresetRecord = 0;
		}
		
        if ($n==0)
        {
                $noOfStudent = 0;
                printHeader($DayType,$TargetDate ,$ClassName, $HasPresetRecord);
                echo "\n";
        }
		
		if ($n!=0 && $prevClass != $ClassName)
        {
				$noOfStudent = 0;
                echo "$pagefooter\n$page_breaker\n";
                printHeader($DayType,$TargetDate,$ClassName, $HasPresetRecord);
                echo "\n";
                $prevClass = $ClassName;
        }
        $prevClass = $ClassName;

		// Display student count
		$noOfStudent++;
		$studentCount = "<td class=\"eSporttdborder eSportprinttext\">$noOfStudent</td>";

        switch ($attend_status)
        {
            case CARD_STATUS_PRESENT : $note = $i_StudentAttendance_Status_Present;
                              break;
            case CARD_STATUS_ABSENT : $note = $i_StudentAttendance_Status_PreAbsent;
                              break;
            case CARD_STATUS_LATE : $note = $i_StudentAttendance_Status_Late;
                              break;
            case CARD_STATUS_OUTING : $note = $i_StudentAttendance_Status_Outing;
                              break;
            default: $note = "";
                             break;
        }

     $class_str = ($ClassName != "" && $ClassNumber != "")? "($ClassName - $ClassNumber)":"";
     echo "<tr>";
     echo $studentCount;
     echo "<td class=\"eSporttdborder eSportprinttext\">$name $class_str</td>";
     echo "<td class=\"eSporttdborder eSportprinttext\">$InSchoolTime</td>";
     echo "<td class=\"eSporttdborder eSportprinttext\">$note</td>";
     if($PresetReason != "" || $PresetRemark != "")
     	$preset_info = "<td class=\"eSporttdborder eSportprinttext\">$i_Attendance_Reason: $PresetReason<br>$i_Attendance_Remark: $PresetRemark</td>";
     else
     	$preset_info = "<td class=\"eSporttdborder eSportprinttext\">-</td>";
     if($HasPresetRecord==1) echo $preset_info;
     echo "<td class=\"eSporttdborder eSportprinttext\">".(!in_array($attend_status, array(CARD_STATUS_PRESENT,CARD_STATUS_OUTING)) && $Waived==1?$Lang['General']['Yes']:'&nbsp;')."</td>\n";
     echo "<td class=\"eSporttdborder eSportprinttext\">$Reason&nbsp;</td>\n";
     echo "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>\n";
     echo "<td class=\"eSporttdborder eSportprinttext\">$OfficeRemark&nbsp;</td></tr>\n";
}
echo "</table><br />";

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>