<?php
//
################# Change Log [Start] #####
#
#	Date	:	2015-12-17	Omas
# 			Moved if($targetClass == "") after global.php for php 5.4
#
################## Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
if($targetClass == "")
{
	header("location: summary.php?type=3");
	exit();
}
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewLeftStudents";

$linterface = new interface_html();

#class used
$LIDB = new libdb();

$lc->retrieveSettings();
$lclass = new libclass();
$attendanceMode=$lc->attendance_mode;

# Get class list
$select_class = $lclass->getSelectClass("name=\"targetClass\"",$targetClass,1);

$sql="SELECT UserID FROM INTRANET_USER WHERE ClassName IN ('".$lc->Get_Safe_Sql_Query($targetClass)."') AND RecordType=2 AND RecordStatus IN (0,1,2)";
$temp = $LIDB->returnVector($sql);
if(sizeof($temp)>0)
	$student_list = implode(",",$temp);

### Get Student Name List
$temp_name_list = $lclass -> getStudentNameListByClassName($targetClass);

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
    case "1": $display_period = $i_DayTypeAM;
            $DayType = 2;
            $link_page = "AM";
            $confirm_page="AM";
            break;
    case "2": $display_period = $i_DayTypePM;
            $DayType = 3;
            //if($attendanceMode==2||$attendanceMode==3){
            //        $confirm_page="PM_S";
            //}else{
                    $confirm_page="PM";
            //}
                   $link_page="PM";
            break;
    default : $display_period = $i_DayTypeAM;
            $DayType = 2;
            $link_page = "AM";
            $confirm_page="AM";
            break;
}

### build student table
//$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;


# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($OrderBy != '')
{
	if($OrderBy == '1')
	{
		$order_by = $default_order_by ;
	}
	else
	{
		$order_str= " ASC ";

		$order_by = "ISNULL(b.LeaveSchoolTime), b.LeaveSchoolTime $order_str, a.ClassName, a.ClassNumber, a.EnglishName";
	}
}
else
{
	$order_by = $default_order_by ;
}


#$select_order_by = "$i_LinkSortBy</td>";
$select_order_by = "<select name=\"OrderBy\">";
if($OrderBy != '')
{
	if($OrderBy == '1')
	{
		$select_order_by .= "<option value=\"1\" SELECTED>".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
		$select_order_by .= "<option value=\"2\">".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
	}
	if($OrderBy == '2')
	{
		$select_order_by .= "<option value=\"1\">".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
		$select_order_by .= "<option value=\"2\" SELECTED>".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
	}
}
else
{
	$select_order_by .= "<option value=\"1\">".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
	$select_order_by .= "<option value=\"2\">".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
}
$select_order_by .= "</select>";

	
$sql = "SELECT
				".getNameFieldByLang("a.").",
				a.ClassName,
				a.ClassNumber,
				b.LeaveSchoolTime,
				IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
				b.LeaveStatus
		FROM
				INTRANET_USER AS a ";
if ($lc->EnableEntryLeavePeriod) {
 	$sql .= "
 	        INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.UserID = selp.UserID 
          	AND 
          	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
          ";
}
$sql .="
				LEFT OUTER JOIN 
				$card_student_daily_log AS b ON (a.UserID = b.UserID)
		WHERE
				b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' AND
				a.ClassName = '".$lc->Get_Safe_Sql_Query($targetClass)."' AND
				a.RecordType = 2 AND
				a.RecordStatus IN (0,1,2)
		ORDER BY
				$order_by
		";
		
$result = $LIDB->returnArray($sql, 6);

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tablebluetop\">";
$x .= "<td class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td class=\"tabletoplink\">$i_ClassName</td>";
$x .= "<td class=\"tabletoplink\">$i_UserClassNumber</td>";
$x .= "<td class=\"tabletoplink\">$i_StudentAttendance_Time_Departure</td>";
$x .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>";
$x .= "<td class=\"tabletoplink\">$i_StudentAttendance_Status</td></tr>\n";

for($i=0; $i<sizeof($result); $i++)
{
	list($student_name, $class_name, $class_number, $leave_school_time, $leave_school_station, $leave_status) = $result[$i];
	$css=($i%2==0)?"tablebluerow1":"tablebluerow2";
	$x .= "<tr class=\"$css\">";
	$x .= "<td class=\"tabletext\">$student_name</td>";
	$x .= "<td class=\"tabletext\">$class_name</td>";
    $x .= "<td class=\"tabletext\">$class_number</td>";
    
    if($leave_school_time != "")
    {
	    $x .= "<td class=\"tabletext\">$leave_school_time</td>";
    }
    else
    {
	    $x .= "<td class=\"tabletext\">-</td>";
    }
    
    if($leave_school_station != "")
    {
	    $x .= "<td class=\"tabletext\">$leave_school_station</td>";
    }
    else
    {
	    $x .= "<td class=\"tabletext\">-</td>";
    }
    
    if($leave_status != "")
    {
	    switch($leave_status)
	    {
		    case 0:
		    		$x .= "<td class=\"tabletext\">$i_StudentAttendance_Leave_Status_Normal</td>";
		    		break;
		    case 1:
		    		$x .= "<td class=\"tabletext\">$i_StudentAttendance_Leave_Status_Early_Leave_AM</td>";
		    		break;
		    case 2:
		    		$x .= "<td class=\"tabletext\">$i_StudentAttendance_Leave_Status_Early_Leave_PM</td>";
		    		break;
		    default:
		    		break;
    	}
    }
    else
    {
	    $x .= "<td class=\"tabletext\">-</td>";
    }
}

if(sizeof($result)<=0)
{
	$x .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" colspan=\"6\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

$x .= "</table>\n";

/*
$data = Array(
				Array("1",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll),
                Array("2",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs),
                Array("3",$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate),
                Array("4",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate)
			);
$toolbar = "$i_SmartCard_DailyOperation_viewClassStatus_PrintOption:&nbsp;".getSelectByArray($data, " name=print_option", $print_option,0,1);
$toolbar .= "&nbsp;<a class=iconLink href=javascript:openPrintPage()><img src='/images/admin/button/s_btn_print_".$intranet_session_language.".gif' border=0 align=absmiddle></a>";

$absent_count="<a class=iconLink href=\"blind_confirmation_".$confirm_page.".php?TargetDate=$TargetDate&period=$period\"><img src=\"$image_path/admin/icon_revise.gif\" border=0 align=absmiddle> $i_SmartCard_DailyOperation_BlindConfirmation</a>";
*/

$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_InSchool, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=0", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_Lunch, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=1", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_AfterSchool, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=2", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_LeavingTime, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=3", 1);

$PAGE_NAVIGATION[] = array("$targetClass");

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script type="text/javascript">
<!--

	function openPrintPage()
	{
		var option = document.form1.print_option.value;
		if(option=="")
		{
			alert("<?=$i_alert_pleaseselect?>");
		}
		else
		{
			newWindow("view_student_print.php?period=<?=urlencode($period)?>&TargetDate=<?=urlencode($TargetDate)?>&option="+option,4);
		}
	}
	function orderByTime(v)
	{
		fObj = document.form1;
		if(fObj==null ) return;
		orderByTimeObj = fObj.order_by_time;
		if(orderByTimeObj==null) return;
		orderByTimeObj.value = v;
		fObj.action="";
		fObj.submit();
	}
// -->
</script>
<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Field_Date ?>
					</td>
					<td class="tabletext" width="70%">
						<?=$linterface->GET_DATE_FIELD("TargetDate", "form1", "TargetDate", $TargetDate)?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_ClassName ?></td>
					<td class="tabletext" width="70%"><?= $select_class ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_LinkSortBy ?></td>
					<td class="tabletext" width="70%"><?= $select_order_by ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<?=$x?>
		</td>
	</tr>
</table>
<input name="order" type="hidden" value="<?=escape_double_quotes($order)?>">
<input name="order_by_time" type="hidden" value="<?=escape_double_quotes($order_by_time)?>">
</form>
<br />
<?																																																																																																	
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
intranet_closedb();
?>

