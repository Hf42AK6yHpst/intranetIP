<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcard = new libcardstudentattend2();

#class used
$LIDB = new libdb();
$lc = new libcardstudentattend2();

$type += 0;
$summary = $lc->getSummaryDetail($type);

if ($type==0)
{
	$MODULE_OBJ['title'] = $i_StudentAttendance_LeftStatus_Type_InSchool." - ";

    list($back, $not_yet) = $summary;
    if ($subtype==0)
    {
	    $MODULE_OBJ['title'] .= $i_StudentAttendance_InSchool_BackAlready;
	    $id_list = implode(",",$back);
    }
    else # subtype = 1
    {
	    $MODULE_OBJ['title'] .= $i_StudentAttendance_InSchool_NotBackYet;
	    $id_list = implode(",",$not_yet);
    }
}
else if ($type==1)
{
     $MODULE_OBJ['title'] = $i_StudentAttendance_LeftStatus_Type_Lunch." - ";

     list($gone_out, $back, $not_out) = $summary;
     if ($subtype==0)
     {
        $MODULE_OBJ['title'] .= $i_StudentAttendance_Lunch_GoneOut;
        $id_list = implode(",",$gone_out);
     }
     else if ($subtype==1)
     {
        $MODULE_OBJ['title'] .= $i_StudentAttendance_Lunch_Back;
        $id_list = implode(",",$back);
     }
     else # subtype = 2
     {
        $MODULE_OBJ['title'] .= $i_StudentAttendance_Lunch_NotOutYet;
        $id_list = implode(",",$not_out);
     }
}
else # type = 2
{
    $MODULE_OBJ['title'] = $i_StudentAttendance_LeftStatus_Type_AfterSchool . " - ";

    list($left, $stay) = $summary;
    if ($subtype==0)
    {
        $MODULE_OBJ['title'] .= $i_StudentAttendance_AfterSchool_Left;
        $id_list = implode(",",$left);
    }
    else
    {
        $MODULE_OBJ['title'] .= $i_StudentAttendance_AfterSchool_Stay;
        $id_list = implode(",",$stay);
    }
}

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$sql = "SELECT 
					u.UserID,
					u.ClassName,
					u.ClassNumber, 
					".getNameFieldByLang("u.").", 
					u.UserLogin 
				FROM 
					INTRANET_USER u ";
if ($lc->EnableEntryLeavePeriod) {
 	$sql .= "
 	        INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on u.UserID = selp.UserID 
          	AND 
          	NOW() between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
          ";
}
$sql .= "				
				WHERE u.UserID in ($id_list)
        ORDER BY 
        	u.ClassName, u.ClassNumber, u.EnglishName";
$result = $LIDB->returnArray($sql,5);
$studentList = $result;

$display .= "<tr class=\"tablebluetop\">
			<td width=\"10\" class=\"tabletoplink\">#</td>
			<td class=\"tabletoplink\">$i_UserClassName</td>
			<td class=\"tabletoplink\">$i_UserClassNumber </td>
			<td class=\"tabletoplink\">$i_UserStudentName </td>
			</tr>\n";

for ($i=0; $i<sizeof($studentList); $i++)
{
     $count = $i+1;
     $css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
     list($id,$class,$classnumber,$name,$login) = $studentList[$i];
     $display .= "<tr class=\"$css\">";
     $display .= "<td class=\"tabletext\">$count</td>";
     $display .= "<td class=\"tabletext\">$class</td>";
     $display .= "<td class=\"tabletext\">$classnumber</td>";
     $display .= "<td class=\"tabletext\">$name</td></tr>\n";
}

?>
<br />
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$display?>
</table>
<br />
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>