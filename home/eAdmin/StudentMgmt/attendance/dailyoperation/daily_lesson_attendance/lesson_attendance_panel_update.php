<?php
// Modifying by 
/*
 * 2020-06-04 (Ray): add TW
 * 2016-06-01 (Carlos): [ip2.5.7.7.1] Added reason.
 * 2015-09-21 (Carlos): [ip2.5.6.10.1] Added checking to check is page load time outdated if someone else updated the records after current page loaded.  
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
//include_once($PATH_WRT_ROOT."includes/flashservices_ver_1_2/services/liblessonattendance.php");

intranet_auth();
intranet_opendb();

if ($_REQUEST['flag'] != 2 && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
//if(Check_Record_Exist_Subject_Group_Attend_Overview($SubjectGroupID,$LessonDate,$RoomAllocationID))

//$RoomAllocationID!!!!!!!!

//function Check_Record_Exist_Subject_Group_Attend_Overview($SubjectGroupID,$LessonDate,$RoomAllocationID)
//function Save_Lesson_Plan($SubjectGroupID, $LessonDate, $RoomAllocationID, $StartTime, $EndTime, $PlanDetailPlanID="", $PlanDetailPlanName="", $StudentPositionAMF="", $PlanDetailAMF="")
//function Save_Lesson_Attendance_Data($AttendOverviewID, $StudentAttendArray)
$LessonAttendUI = new libstudentattendance_ui();
//debug_pr($drop_down_status);
//debug_pr("2. ".$SubjectGroupID);
//debug_pr("3. ".$LessonDate);
//debug_pr("4. ".$RoomAllocationID);
//debug_pr("5. ".$StartTime1);
//debug_pr("6. ".$EndTime1);
//debug_pr("7. ".$AttendOverviewID);

$LastLoadTime = intval($_REQUEST['LastLoadTime']);
if($LessonAttendUI->Is_Lesson_Plan_Outdated($SubjectGroupID, $LessonDate, $RoomAllocationID, $LastLoadTime)){
	$Msg = $Lang['StudentAttendance']['DataOutOfDate'];
	if($_REQUEST['flag'] == 2)
		header("Location: ../../../../../../home/smartcard/attendance/take_attendance.php?".$_SERVER['QUERY_STRING']."&Msg=".urlencode($Msg));
	else
		header("Location: lesson_attendance_panel.php?".$_SERVER['QUERY_STRING']."&Msg=".urlencode($Msg));
	intranet_closedb();
	exit();
}

$StudentAttendArray = array();
$AttendOverviewID1 = '';

$drop_down_status = $_REQUEST['drop_down_status'];

for($i=0;$i<count($drop_down_status);$i++) {
	$StudentAttendArray[$i][0] = $_REQUEST['StudentID'.$i];
	$StudentAttendArray[$i][1] = $drop_down_status[$i];
	$StudentAttendArray[$i][2] = $_REQUEST['remarks'.$i];
	$StudentAttendArray[$i][3] = 0;
	$StudentAttendArray[$i][4] = $drop_down_status[$i]=='0'? '': $_REQUEST["reason$i"];
	if(get_client_region() == 'zh_TW') {
		$StudentAttendArray[$i][5] = $_REQUEST['leaveType'.$i];
	}
}

if($StudentAttendArray[0][0] != ""){
	//if($LessonAttendUI->Check_Record_Exist_Subject_Group_Attend_Overview($SubjectGroupID,$LessonDate,$RoomAllocationID) == -1){
		$AttendOverviewID = $LessonAttendUI->Save_Lesson_Plan($SubjectGroupID, $LessonDate, $RoomAllocationID, $StartTime1, $EndTime1,"","","","");
		$AttendOverviewID1 = "&AttendOverviewID=".$AttendOverviewID;
	//}
	if($LessonAttendUI->Save_Lesson_Attendance_Data($AttendOverviewID, $StudentAttendArray)){
		$LessonAttendUI->Clone_Class_Lesson_Attendance_Overview_And_Students($LessonDate, $AttendOverviewID);
		$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	}else
		$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
else
	$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
intranet_closedb();
if($_REQUEST['flag'] == 2)
	header("Location: ../../../../../../home/smartcard/attendance/take_attendance.php?".$_SERVER['QUERY_STRING'].$AttendOverviewID1."&Msg=".urlencode($Msg));
else
	header("Location: lesson_attendance_panel.php?".$_SERVER['QUERY_STRING'].$AttendOverviewID1."&Msg=".urlencode($Msg));

?>