<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

//if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
if(!isset($_REQUEST['SubjectGroupID']) || !isset($_REQUEST['LessonDate'])){
	intranet_closedb();
	header ("Location: /");
	exit();
}


$FromDate = $_REQUEST['FromDate'];
$ToDate = $_REQUEST['ToDate'];
$ClassID = $_REQUEST['ClassID'];

$LessonAttendUI = new libstudentattendance_ui();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

echo $LessonAttendUI->Get_Lesson_Attendance_Panel_Form($SubjectGroupID, "SubjectGroup", $LessonDate, $AttendOverviewID, "print");

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>