<?php
// Modifying 

/*
 * 2013-08-19 (Henry Chan): The impelmentation is done
 * 2013-08-09 (Henry Chan): File Created. This is the new view of the daily lesson attendance
*/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = $_REQUEST['TargetDate'];

$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_DailyOverview";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 

echo $LessonAttendUI->Get_Lesson_Daily_Status_Form2($TargetDate);

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
var TargetDate = '<?
if(!$_REQUEST['TargetDate'])
	echo date("Y-m-d");
else
	echo $_REQUEST['TargetDate'];
?>';
function View_Lesson_Daily_Overview() {
	var TempTargetDate = document.getElementById('TargetDate').value;
	var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
	var unConfirmed = document.getElementById('chk_UnConfirmed').checked;
	var confirmed = document.getElementById('chk_Confirmed').checked;
	var filter = 0;
	if(unConfirmed && confirmed){
		filter = 0;
	}
	else if(unConfirmed){
		filter = 1;
	}
	else if(confirmed){
		filter = 2;
	}
	else{
		alert("<?=$Lang['StudentAttendance']['PleaseSelectAtLeastOneStatus']?>");
		return;
	}
	if (re.test(TempTargetDate)) {
		$("span#TargetDateWarningLayer").hide();
		TargetDate = TempTargetDate;
		Block_Element('DetailTableLayer'); 
		$("div#DetailTableLayer").load('ajax_get_lesson_attendance_detail.php',{"TargetDate":TargetDate, "Flag":2, "Filter":filter},function() {UnBlock_Element('DetailTableLayer');});
	}
	else {
		$("span#TargetDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$("span#TargetDateWarningLayer").show();
	}
}
//added new parameter AttendOverviewID
function Call_Lesson_Attendance_Flash(SubjectGroupID,SubjectGroupName,RoomAllocationID,StartTime,EndTime, AttendOverviewID) {
	var QueryString = 'SubjectGroupID='+SubjectGroupID;
	QueryString += '&SubjectGroupName='+encodeURIComponent(SubjectGroupName);
	QueryString += '&RoomAllocationID='+RoomAllocationID;
	QueryString += '&StartTime='+StartTime;
	QueryString += '&EndTime='+EndTime;
	QueryString += '&LessonDate='+TargetDate;
	QueryString += '&AttendOverviewID='+AttendOverviewID;
	
	window.location.href ='lesson_attendance_panel.php?'+QueryString;
	//window.open('pop_lesson_attendance_flash_panel.php?'+QueryString,'LessonAttendanceFlashPopUp');
}

function Show_Time_Slot_Layer(LayerID,e) {
	var tempX = 0;
	var tempY = 0;
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	$('span.member_list_group_layer').slideUp(); 
	$('span#'+LayerID).css('left',(tempX-200)+'px');
	//$('span#'+LayerID).css('top',tempY+'px');
	$('span#'+LayerID).slideDown();
}
function PrintPage(TargetDate)
{
		var unConfirmed = document.getElementById('chk_UnConfirmed').checked;
		var confirmed = document.getElementById('chk_Confirmed').checked;
		var filter = 0;
		if(unConfirmed && confirmed){
			filter = 0;
		}
		else if(unConfirmed){
			filter = 1;
		}
		else if(confirmed){
			filter = 2;
		}
		
        newWindow("index2_print.php?TargetDate="+TargetDate+"&Filter="+filter, 12);
}
</script>