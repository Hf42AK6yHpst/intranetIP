<?php
// using kenneth chung
/*
 * Change Log:
 * 2013-08-12 (Henry):added flag checking for the new Lesson Daily Status page.
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = $_REQUEST['TargetDate'];
$Flag = $_REQUEST['Flag'];
$Filter = $_REQUEST['Filter'];
$LessonAttendUI = new libstudentattendance_ui();

if($Flag == 2)
	echo $LessonAttendUI->Get_Lesson_Daily_Status_Overview2($TargetDate, $Filter);
else
	echo $LessonAttendUI->Get_Lesson_Daily_Status_Overview($TargetDate);

intranet_closedb();
?>