<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_CheckTime";

$linterface = new interface_html();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
//$displayColSpanValue = $hasAM + $hasLunch + $hasPM +1;
//echo "$hasAM,$hasLunch,$hasPM";

$db_engine = new libdb();    
$select_class=$lc->getSelectClass("name='class' onChange='changeClass()'",$class,0);

if($class!=""){
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' ORDER BY ClassName,ClassNumber";
	$temp = $lc->returnArray($sql,2);
	$select_student=getSelectByArray($temp,"name='studentID'",$studentID,0,0);
}

if($studentID!="")
{	
	$classSchoolSlotTimeTable = $lc->Get_Class_Attend_Time_Setting($target_date,$studentID);
	
	if ($classSchoolSlotTimeTable !== "NonSchoolDay") {
		$s_MorningTime = $classSchoolSlotTimeTable['MorningTime'];
		$s_LunchStart = $classSchoolSlotTimeTable['LunchStart'];
		$s_LunchEnd = $classSchoolSlotTimeTable['LunchEnd'];
		$s_LeaveSchoolTime = $classSchoolSlotTimeTable['LeaveSchoolTime'];
	}

	$classDisplay = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
    $classDisplay .= "<tr class=\"tablerow1\"><th style='padding-bottom: 15px'>$i_SmartCard_DailyOperation_Check_Time_School_Time_Table</th></tr>";
	$classDisplay .= "<tr class=\"tabletop\">";		
	if($hasAM == 1)		{	$classDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_AMStart</td>";}
	if($hasLunch == 1 )	{	$classDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_LunchStart</td>";}
	if($hasPM == 1)		{	$classDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_PMStart</td>";}
	$classDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
	$classDisplay .= "</tr>\n";
	$classDisplay .= "<tr class=\"tablerow1\">";
	if($classSchoolSlotTimeTable === "NonSchoolDay")
	{	
		$classDisplay .= "<td colspan=\"4\" class=\"tabletext\" align=\"center\">".$i_StudentAttendance_NonSchoolDay."</td>";
	}
	else
	{
		if($hasAM == 1)		{	$classDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_MorningTime."</td>";}
		if($hasLunch == 1 )	{	$classDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LunchStart."</td>";}
		if($hasPM == 1)		{	$classDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LunchEnd."</td>";}			
		$classDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LeaveSchoolTime."</td>";
	}
	$classDisplay .= "</tr>";
	$classDisplay .= "</table>";
	
	$GroupTimeTable = $lc->Get_Group_Attend_Time_Setting($target_date,$studentID,true);
	$groupDisplay = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
    $groupDisplay .= "<tr class=\"tablerow2\"><th style='padding-bottom: 15px'>$i_SmartCard_DailyOperation_Check_Time_Group_Time_Table</th></tr>";
	$groupDisplay .= "<tr class=\"tabletop\">";
	$groupDisplay .= "<td class=\"tabletoplink\" align=\"center\">#</td>";
	$groupDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_GroupTitle</td>";
	$groupDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_GroupDescription</td>";
	if($hasAM == 1)		{	$groupDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_AMStart</td>";}
	if($hasLunch == 1 )	{	$groupDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_LunchStart</td>";}
	if($hasPM == 1)		{	$groupDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_PMStart</td>";}
	$groupDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
	$groupDisplay .= "</tr>\n";
	$i=0;
	if (sizeof($GroupTimeTable) > 0) {
		foreach ($GroupTimeTable as $Key => $Val)
		{
			$css=($i%2==0)?"tablerow1":"tablerow2";
			list($s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay,$title, $desc,$mode) = $Val;
			//debug_r($Val);
	//			echo "====> [$s_groupid,  $title, $desc,$mode,$s_RecordID, $s_MorningTime, $s_LunchStart,$s_LunchEnd, $s_LeaveSchoolTime,$s_NonSchoolDay]<br>";
			$groupDisplay .= "<tr class=\"$css\">";	
			$groupDisplay .= "<td class=\"tabletext\">".($i+1)."&nbsp</td>";
			$groupDisplay .= "<td class=\"tabletext\">".$title."&nbsp</td>";
			$groupDisplay .= "<td class=\"tabletext\">".$desc."&nbsp</td>";
			switch($mode){
				case 1:
					if($s_NonSchoolDay == 1)
					{	
						$groupDisplay .= "<td class=\"tabletext\" colspan=\"4\" align = \"center\">".$i_StudentAttendance_NonSchoolDay."</td>";
					}
					else
					{
						if($hasAM == 1)		{$groupDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_MorningTime."</td>";}
						if($hasLunch == 1 )	{$groupDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LunchStart."</td>";}
						if($hasPM == 1)		{$groupDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LunchEnd."</td>";}
						$groupDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LeaveSchoolTime."</td>";
					}
					break;
				case 2:
					$groupDisplay .= "<td class=\"tabletext\" colspan=\"4\" align = \"center\">$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance</td>";
					break;
				default:
					$groupDisplay .= "<td class=\"tabletext\" colspan=\"4\" align = \"center\">$i_StudentAttendance_ClassMode_UseSchoolTimetable</td>";
					break;
			}		
			$groupDisplay .= "</tr>";
			$i++;
		}
	}
	$groupDisplay .= "</table>\n";
	
	$FinalSetting = $lc->Get_Student_Attend_Time_Setting($target_date,$studentID);
	$finalDisplay = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
    $finalDisplay .= "<tr class=\"tablerow1\"><th style='padding-bottom: 15px'>$i_SmartCard_DailyOperation_Check_Time_Final_Time_Table</th></tr>";
	$finalDisplay .= "<tr class=\"tabletop\">";
	if($hasAM == 1)		{	$finalDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_AMStart</td>";}
	if($hasLunch == 1 )	{	$finalDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_LunchStart</td>";}
	if($hasPM == 1)		{	$finalDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_PMStart</td>";}			

	$finalDisplay .= "<td class=\"tabletoplink\" align = \"center\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
	$finalDisplay .= "</tr>\n";
	$finalDisplay .= "<tr class=\"tablerow1\">";
	if ($FinalSetting !== "NonSchoolDay") {
		$s_MorningTime = $FinalSetting['MorningTime'];
		$s_LunchStart = $FinalSetting['LunchStart'];
		$s_LunchEnd = $FinalSetting['LunchEnd'];
		$s_LeaveSchoolTime = $FinalSetting['LeaveSchoolTime'];
	}
	if($FinalSetting === "NonSchoolDay")
	{	
		$finalDisplay .=  "<td class=\"tabletext\" colspan=\"4\">".$i_StudentAttendance_NonSchoolDay."</td>";
	}
	else
	{
		if($hasAM == 1)		{	$finalDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_MorningTime."</td>";}
		if($hasLunch == 1 )	{	$finalDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LunchStart."</td>";}
		if($hasPM == 1)		{	$finalDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LunchEnd."</td>";}			
		
		$finalDisplay .= "<td class=\"tabletext\" align = \"center\">".$s_LeaveSchoolTime."</td>";
	}
	$finalDisplay .=  "</tr>";		
	$finalDisplay .=  "</table>";		

////end new
         //}//close if (($time_table_mode == 0)||($time_table_mode == ''))
	 //** end

}

$CycleDayInfo = $lc->Get_Attendance_Cycle_Day_Info($target_date);

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Check_Time, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<script language='javascript'>
<!--
function changeClass(){
	submitForm();
}
function submitForm(){
	obj = document.form1;
	if(obj!=null) obj.submit();
}
function openPrintPage()
{
        newWindow("browse_by_student_print.php?class=<?=$class?>&studentID=<?=$studentID?>&filter=<?=$filter?>&keyword=<?=$keyword?>&order=<?=$order?>&field=<?=$field?>",4);
}
function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
-->
</script>
<form name="form1" method="GET">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
		<?php echo $i_UserClassName; ?> 
	</td>
	<td class="tabletext" width="70%">
		<?=$select_class?>
	</td>
</tr>
<?php if($select_student!=""){?>
<Tr>
	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
		<?=$i_UserStudentName?>
	</td>
	<td class="tabletext" width="70%">
		<?=$select_student?>
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
		<?=$i_StudentAttendance_Field_Date?>
	</td>
	<td class="tabletext" width="70%">
<?

	$sql="SELECT DISTINCT DATE_FORMAT(RecordDate,'%Y%m%d') FROM CARD_STUDENT_PRESET_LEAVE";
	$dates = $db_engine->returnVector($sql);

	$target_date = ($target_date =="")?date('Y-m-d'):$target_date;
?>
		<?=$linterface->GET_DATE_PICKER("target_date",$target_date)?>
	</td>
</tr>
<tr>
    <td align="center" colspan="2">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		</td>	
</tr>
<tr>
	<td colspan=2>
		<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
		</table>	
	</td>
</tr>
<?php } ?>
</table>
<?php if($studentID!=""){
	echo '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						'.$Lang['StudentAttendance']['PeriodType'].'
					</td>
					<td class="tabletext" width="70%">
						'.(($CycleDayInfo[0] == "Week")? $Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle']:$Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle']).'
					</td>
				</tr>';
	echo '<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						'.$Lang['StudentAttendance']['CurrentPeriodDay'].'
					</td>
					<td class="tabletext" width="70%">
						'.$Lang['SysMgr']['Homework']['WeekDay'][$CycleDayInfo[1]].'
					</td>
				</tr>
				</table>';
    echo "<br><Br>";

	//echo $i_SmartCard_DailyOperation_Check_Time_School_Time_Table."";
	echo $classDisplay;
	echo "<br><Br>";
	
	//echo $i_SmartCard_DailyOperation_Check_Time_Group_Time_Table."";
	echo $groupDisplay;
	echo "<br><Br>";
	
	//echo $i_SmartCard_DailyOperation_Check_Time_Final_Time_Table."";
	echo $finalDisplay;
} ?>
<br><br>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
<?
function getUserGroupIDArray($userID,$TargetDate)
{
	global $db_engine;
	
	$AcademicYearInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);
	#GET USER BELONGS TO WHICH GROUP, GROUP IS CLASS TYPE (3)
	$groupIDString = "";
	$sql = "select c.GroupID, c.Title,c.Description, e.mode
		from 
			INTRANET_USER a
			inner join 
			INTRANET_USERGROUP b
			on a.userid = b.userid and a.userid = '$userID'
			inner join 
			INTRANET_GROUP c 
			on b.groupid = c.groupid and c.recordtype = 3 and c.AcademicYearID = '".$AcademicYearInfo[0]."' 
			left outer join 
			YEAR_CLASS d 
			on c.groupid = d.groupid 
			left outer join 
			CARD_STUDENT_ATTENDANCE_GROUP e 
			on e.groupid = c.groupid 
		where
			d.groupid is null
		order by c.Title
		";
		$result = $db_engine->returnArray($sql,4);
//echo "sql [".$sql."] size of result [".sizeof($result)."]<br>";
	return $result;
}

function getUserGroupID($userID)
{
	global $db_engine;
	#GET USER BELONGS TO WHICH GROUP, GROUP IS CLASS TYPE (3)
	$groupIDString = "";
	$sql = "select a.UserID, a.CardID, a.EnglishName, a.ChineseName , c.GroupID, c.Title,c.Description, c.recordtype 
		from 
			INTRANET_USER a 
			inner join 
			INTRANET_USERGROUP b 
			on a.userid = b.userid and a.userid = '$userID' 
			inner join 
			INTRANET_GROUP c 
			on b.groupid = c.groupid and c.recordtype = 3
			left outer join YEAR_CLASS d on d.groupid = c.groupid 
		where
			d.groupid is null";
		$result = $db_engine->returnArray($sql,8);

	for($i=0; $i<sizeOf($result); $i++)
	{
		list($aa, $bb, $cc, $dd, $groupid, $ff,$gg,$hh) = $result[$i];
		$groupIDString.= $groupid.",";
	}
	$groupIDString = substr($groupIDString, 0, -1);
	return $groupIDString;
}
function checkTimeResult($ts_recordID,$ts_morningTime,$ts_lunchStart,$ts_lunchEnd,$ts_leaveSchool,$ts_nonSchoolDay)
{
	global $db_engine;

	$sql = "select sec_to_time(".$ts_morningTime."), sec_to_time(".$ts_lunchStart."), sec_to_time(".$ts_lunchEnd."), sec_to_time(".$ts_leaveSchool.")";
    $aaaa = $db_engine->returnArray($sql,4);
	list($bb, $cc, $dd, $ee) = $aaaa[0];
	echo "<br>ts_recordID---><b><font color = \"red\">".$ts_recordID."</font></b>";
	echo "<br>ts_morningTime--->".$ts_morningTime." <b><font color = \"red\">[".$bb."]</font></b>";
	echo "<br>ts_lunchStart--->".$ts_lunchStart."<b><font color = \"red\"> [".$cc."]</font></b>";
	echo "<br>ts_lunchEnd--->".$ts_lunchEnd." <b><font color = \"red\">[".$dd."]</font></b>";
	echo "<br>ts_leaveSchool--->".$ts_leaveSchool." <b><font color = \"red\">[".$ee."]</font></b>";
	echo "<br>ts_nonSchoolDay---><b><font color = \"red\">".$ts_nonSchoolDay."</font></b><br><br><br>";

}


#SHOULD RECEIVE CLASS/SCHOOL TIME TABLE FIRST, THEN IS GROUP TIME TABLE
function mergeTimeTable($classTimeTableArray, $groupTimeTableArray)
{
/*
	for($i=0; $i<sizeof($classTimeTableArray); $i++)
    {
		list($aa, $bb, $cc, $dd, $ee, $ff,$gg) = $classTimeTableArray[$i];
		echo "array 1[".$aa."] [".$bb."] [".$cc."][".$dd."][".$ee."][".$ff."][".$gg."]<br>";               
    }	
	for($i=0; $i<sizeof($groupTimeTableArray); $i++)
    {
		list($aa, $bb, $cc, $dd, $ee, $ff,$gg) = $groupTimeTableArray[$i];
		echo "array 2[".$aa."] [".$bb."] [".$gg."]<br>";               
    }
*/
	$tempArray = array_merge ($classTimeTableArray, $groupTimeTableArray);


	if(sizeof($tempArray) != 0)
	{
		#$tempArray[0] MUST BE THE INFORMATION ABOUT CLASS/ SCHOOL TIME SETTING
		list($prev_RecordID, $prev_MorningTime, $prev_LunchStart, $prev_LunchEnd, $prev_LeaveSchoolTime, $prev_NonSchoolDay,$prev_DayType) = $tempArray[0];
	}

	for($i=0; $i<sizeof($tempArray); $i++) {
		list($_RecordID, $_MorningTime, $_LunchStart, $_LunchEnd, $_LeaveSchoolTime, $_NonSchoolDay,$_DayType) = $tempArray[$i];
		if($_DayType > $prev_DayType )
		{
			$prev_RecordID			= $_RecordID;			
			$prev_MorningTime		= $_MorningTime;
			$prev_LunchStart		= $_LunchStart;			
			$prev_LunchEnd			= $_LunchEnd;
			$prev_LeaveSchoolTime	= $_LeaveSchoolTime;	
			$prev_DayType			= $_DayType;
			$prev_NonSchoolDay		= $_NonSchoolDay;
		}
		else if ($_DayType == $prev_DayType)
		{
			#IF EQUAL DAY TYPE, NON SCHOOL DAY RECORD WILL NOT COUNT
			if($_NonSchoolDay != 1)
			{
				#EQUAL DAY TYPE , IF PREVIOUS RECORD IS NONSCHOOL DAY, SHOULD RESET ALL THE RECORD TO THE CURRENT RECORD
				if($prev_NonSchoolDay == 1)
				{
					$prev_RecordID			= $_RecordID;			$prev_MorningTime		= $_MorningTime;
					$prev_LunchStart		= $_LunchStart;			$prev_LunchEnd			= $_LunchEnd;
					$prev_LeaveSchoolTime	= $_LeaveSchoolTime;	$prev_DayType			= $_DayType;
					$prev_NonSchoolDay		= $_NonSchoolDay;
				}
				else
				{
					#EQUAL DAY TYPE, PREVIOUS IS SCHOOL DAY (NOT NON SCHOOL DAY), COMPARE THE TIME
					#_RecordID			TAKE ANY MIN/MAX		#_MorningTime		TAKE MIN
					#_LunchStart		TAKE MAX				#_LunchEnd			TAKE MIN
					#_LeaveSchoolTime	TAKE MAX				#_NonSchoolDay		TAKE MIN (0,1) ==> 1 IS NON SCHOOL DAY

					$prev_RecordID			= ($_RecordID		 > $prev_RecordID)		 ? $_RecordID			:$prev_RecordID;
					$prev_MorningTime		= ($_MorningTime	 > $prev_MorningTime)	 ? $prev_MorningTime	:$_MorningTime;
					$prev_LunchStart		= ($_LunchStart		 > $prev_LunchStart)	 ? $_LunchStart			:$prev_LunchStart;
					$prev_LunchEnd			= ($_LunchEnd		 > $prev_LunchEnd)		 ? $prev_LunchEnd		:$_LunchEnd;
					$prev_LeaveSchoolTime	= ($_LeaveSchoolTime > $prev_LeaveSchoolTime)? $_LeaveSchoolTime    :$prev_LeaveSchoolTime;
			//		$prev_NonSchoolDay	    = ($_NonSchoolDay    > $prev_NonSchoolDay)   ? $_NonSchoolDay	:$prev_NonSchoolDay;
			//		$prev_DayType			= $_DayType;
				}
			}
		}
		//else ($_DayType < $prev_DayType){ //skip}
	}

	#PRESERVE THE DATA STURTURE IN ARRAY OF ARRAY
	$arrayElement = array($prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay); 

//	echo "return $prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay<br>";
//checkTimeResult($prev_RecordID,$prev_MorningTime,$prev_LunchStart,$prev_LunchEnd,$prev_LeaveSchoolTime,$prev_NonSchoolDay);

	$returnArray = array($arrayElement);

	return $returnArray;
}
function getGroupSlotSpecialSql ($groupID_str)
{
	global $today;
	$sql =	"SELECT 
				groupid, RecordID, MorningTime, LunchStart,
				LunchEnd, LeaveSchoolTime,NonSchoolDay
			 FROM 
				CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP
			 WHERE 
				RecordDate = '$today' AND 
				GroupId in ($groupID_str)
			 ORDER BY groupid
			";
	return $sql;
}

function getGroupSessionSpecialSql ($groupID_str)
{
	global $today;

	$sql = "SELECT 
						groupid, 
						RecordID, 
						MorningTime, 
						LunchStart,
						LunchEnd, 
						LeaveSchoolTime,
						NonSchoolDay
					FROM
						CARD_STUDENT_TIME_SESSION AS a 
						INNER JOIN
						CARD_STUDENT_TIME_SESSION_DATE_GROUP AS b 
						ON (a.SessionID = b.SessionID)
					WHERE 
						RecordDate = '$today'
						AND GroupId in ($groupID_str) 
					ORDER BY groupid
					";

	return $sql;
}

function getGroupSlotCycleWeekNormalSql($groupID_str)
{
	global $conds;
	global $weekDay;

	$sql =  "SELECT 
						a.groupid, 
						a.RecordID, 
						a.MorningTime, 
						a.LunchStart,
						a.LunchEnd, 
						a.LeaveSchoolTime,
						a.NonSchoolDay,
						a.DayType
					 FROM 
						CARD_STUDENT_GROUP_PERIOD_TIME as a
					 WHERE 
							a.GroupId in ($groupID_str) AND
							(
								(a.DayType = 1 AND a.DayValue = '$weekDay') $conds
								OR
								(a.DayType = 0 AND a.DayValue = 0)
							)
					 ORDER BY a.groupid, a.DayType DESC
					";
	return $sql;
}

function getGroupSessionCycleWeekNormalSql($groupID_str)
{
	global $conds;
	global $weekDay;

	$sql = "SELECT 
				a.groupid, a.RecordID, b.MorningTime, b.LunchStart,
				b.LunchEnd, b.LeaveSchoolTime, b.NonSchoolDay,a.DayType
			FROM 
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS a LEFT OUTER JOIN
				CARD_STUDENT_TIME_SESSION as b ON (a.SessionID = b.SessionID)					
			WHERE a.GroupId in ($groupID_str) AND
				(
					(a.DayType = 1 AND a.DayValue = '$weekDay') $conds
					OR
					(a.DayType = 0 AND a.DayValue = 0)
				)
			ORDER BY 
					a.groupid, a.DayType DESC, a.recordId desc
			";
	return $sql;
}


?>