<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
$target_date = $target_date==""?$today:$target_date;
$date_cond = " a.RecordDate = '$target_date' ";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;


if($target_date!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	$namefield = getNameFieldByLang("b.");
	
	$sql=" SELECT CONCAT('<a href=\'edit.php?RecordID=',a.RecordID,'\'>',$namefield,'</a>'),
				b.ClassName,b.ClassNumber,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 a.Remark,
				 CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				";
	$li = new libdbtable($field, $order, $pageNo);
	$li->field_array = array("$namefield","b.ClassName","b.ClassNumber","a.DayPeriod","a.Reason","a.Remark");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0,0,0,0);
	$li->wrap_array = array(0,0,0,0,0,0);
	$li->IsColOff = 2;

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassName)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassNumber)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Attendance_DayType)."</td>\n";
	$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_Attendance_Reason)."</td>\n";
	$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_Attendance_Remark)."</td>\n";
	$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

}

$sql="SELECT DISTINCT DATE_FORMAT(RecordDate,'%Y%m%d') FROM CARD_STUDENT_PRESET_LEAVE";
$dates = $li->returnVector($sql);

$legend = "<table width=100% border=0><tr><td align=center class=dynCalendar_card_not_confirmed>$i_SmartCard_DailyOperation_Preset_Absence_Has_Leave_Record</td></tr>";

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_Preset_Absence,'index.php',$i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_card_no_data";
         css_array[1] = "dynCalendar_card_not_confirmed";
         css_array[2] = "dynCalendar_card_confirmed";
         var date_array = new Array;
         <?
         for ($i=0; $i<sizeof($dates); $i++)
         {
	         $date_string = $dates[$i];
	         ?>
              date_array[<?=$date_string?>] = 1;
             <?
         }
         ?>
 </script>
 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].target_date.value = dateValue;
          }
 // -->
 </script>

<script language='javascript'>
<!--
function changeClass(){
	submitForm();
}
function submitForm(){
	obj = document.form1;
	if(obj==null) return;
	objTargetDate = document.form1.target_date;
	if(!check_date(objTargetDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
			objTargetDate.focus();
			return;
	}
	obj.submit();
}
function openPrintPage()
{
        newWindow("browse_by_date_print.php?class=<?=$class?>&target_date=<?=$target_date?>&filter=<?=$filter?>&keyword=<?=$keyword?>&order=<?=$order?>&field=<?=$field?>",4);
}
function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
-->
</script>
<form name="form1" method="GET">
<table width=560 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right width=20% style='vertical-align:middle'><?php echo $i_StudentAttendance_Field_Date; ?>: </td><td style='vertical-align:middle'>
<input type=text name=target_date value='<?=$target_date?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
         startCal.setLegend('<?=$legend?>');
         startCal.differentDisplay = true;
    //-->
    </script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>&nbsp;<a href='javascript:submitForm()'><img src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"  border=0></a>
    </td></tr>
</table>
<?php if($target_date!=""){
	$toolbar  = "<a class=iconLink href=javascript:checkGet(document.form1,'new.php')>".newIcon()."$button_new</a>\n".toolBarSpacer();
	$toolbar .= "<a class=iconLink href=javascript:exportPage('browse_by_date_export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();
	$toolbar2 = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";

	
	$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
/*
	$filterbar ="<SELECT name=filter onChange=submitForm()>";
	$filterbar.="<OPTION value='1'".($filter==1?" SELECTED ":"").">$i_StudentAttendance_Reminder_Status_Past</OPTION>";
	$filterbar.="<OPTION value='2'".($filter==2?" SELECTED ":"").">$i_StudentAttendance_Reminder_Status_Coming</OPTION>";
	$filterbar.="<OPTION value='3'".($filter==3?" SELECTED ":"").">$i_status_all</OPTION>";
	$filterbar.="</SELECT>";
*/
	$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
	
	$searchbar = $filterbar."&nbsp;".$searchbar;

?>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?></td></tr>
	<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
	</table>
	<?php echo $li->display(); ?>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>
<?php } ?>
<br><br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>