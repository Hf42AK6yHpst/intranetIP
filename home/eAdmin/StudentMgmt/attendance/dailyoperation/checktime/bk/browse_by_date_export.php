<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_opendb();

$lc = new libcardstudentattend2();


$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
$target_date = $target_date==""?$today:$target_date;
$date_cond = " a.RecordDate = '$target_date' ";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$namefield = getNameFieldByLang("b.");

$field_array = array("$namefield","b.ClassName","b.ClassNumber","a.DayPeriod","a.Reason","a.Remark");
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($target_date!=""){
	$lb = new libdb();
	$sql=" SELECT $namefield,
				b.ClassName,b.ClassNumber,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 a.Remark
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";
	$temp = $lb->returnArray($sql,6);
	
	$lexport = new libexporttext();
	
	$exportColumn = array("$i_UserStudentName", "$i_ClassName", "$i_ClassNumber", 
						"$i_Attendance_DayType", "$i_Attendance_Reason", "$i_Attendance_Remark");
	$export_content = $lexport->GET_EXPORT_TXT($temp, $exportColumn);
	
	
	$export_content_final = "$i_SmartCard_DailyOperation_Preset_Absence ($target_date)\n\n";
	if(sizeof($temp)<=0){
		$export_content_final .= "$i_no_record_exists_msg\n";
	} else {
		$export_content_final .= $export_content;
	}
	
}

intranet_closedb();

$filename = "preset_absence_".$target_date.".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>