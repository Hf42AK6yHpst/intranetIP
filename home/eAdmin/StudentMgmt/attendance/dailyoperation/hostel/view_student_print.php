<?php
// Editing by 
/*
 * 2018-05-02 (Carlos): Added UserStatus=1 to getHostelAttendanceRecords() params to only fetch active students.
 * 2017-08-09 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) ) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];

if(($GroupID=='' && !$can_take_all_groups) || $TargetDate=='' || $lc->HostelAttendanceGroupCategory == ''){
	intranet_closedb();
	header ("Location: ../../");
	exit();
}

$linterface = new interface_html();

$ts = strtotime($TargetDate);
$year = date("Y",$ts);
$month = date("m",$ts);
$YesterdayDate = date("Y-m-d", $ts - 86400);
$yesterday_year = date("Y", $ts - 86400);
$yesterday_month = date("m", $ts - 86400);

$in_status_ary = array(
	CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['HostelAttend'],
	CARD_STATUS_ABSENT=>$Lang['StudentAttendance']['HostelAbsent']
);

$out_status_ary = array(
	''=>'N.A.',
	(string)CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['HostelNormalLeave'],
	(string)PROFILE_TYPE_EARLY=>$Lang['StudentAttendance']['HostelEarlyLeave']
);

$params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$TargetDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
if(!$can_take_all_groups){
	$params['GroupID'] = $GroupID;
	$params['IsGroupStaff'] = true;
	$params['GroupUserID'] = $_SESSION['UserID'];
}else if($GroupID!=''){
	$params['GroupID'] = $GroupID;
}
$confirmed_records = $lc->getHostelAttendanceGroupsConfirmedRecords($params);

$record_params = array('RecordDate'=>$TargetDate,'AcademicYearID'=>Get_Current_Academic_Year_ID(),'CategoryID'=>$lc->HostelAttendanceGroupCategory,'UserStatus'=>1);
if(!$can_take_all_groups){
	$record_params['GroupID'] = $GroupID;
}else if($GroupID!=''){
	$record_params['GroupID'] = $GroupID;
}
$records = $lc->getHostelAttendanceRecords($year, $month, $record_params);
$record_size = count($records);
$record_params['RecordDate'] = $YesterdayDate;
$record_params['UserIDToRecord'] = 1;
$yesterday_records = $lc->getHostelAttendanceRecords($yesterday_year, $yesterday_month, $record_params);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
?>
<style type="text/css">
     p.breakhere {page-break-before: always}
</style>
<table width="98%" align="center" class="print_hide" border=0>
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit_print")?></td>
	</tr>
</table>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="eSportprinttitle" colspan="2"><u><?=$Lang['StudentAttendance']['ViewHostelGroupStatus']?></u></td></tr>
	<tr><td class="eSportprinttitle"><?=$Lang['StudentAttendance']['HostelGroup']?></td><td width="70%" class="eSportprinttitle"><?=count($confirmed_records)>1?$Lang['General']['All']:$confirmed_records[0]['Title']?></td></tr>
	<tr><td class="eSportprinttitle"><?=$i_StudentAttendance_View_Date?></td><td width="70%" class="eSportprinttitle"><?=$TargetDate?></td></tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="5" align="center" class="eSporttableborder">
	<tr>
		<td class="eSporttdborder eSportprinttabletitle">#</td>
		<td class="eSporttdborder eSportprinttabletitle" width="11%"><?=$i_UserName?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="10%"><?=$Lang['StudentAttendance']['YesterdayStatus']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="10%"><?=$Lang['StudentAttendance']['InStatus']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="10%"><?=$Lang['StudentAttendance']['InTime']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="10%"><?=$Lang['StudentAttendance']['OutStatus']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="10%"><?=$Lang['StudentAttendance']['OutTime']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="15%"><?=$Lang['General']['Remark']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="12%"><?=$Lang['General']['LastUpdatedTime']?></td>
		<td class="eSporttdborder eSportprinttabletitle" width="12%"><?=$Lang['StudentAttendance']['LastConfirmPerson']?></td>
	</tr>
<?php
$show_group_name = $GroupID=='';
$x = '';
for($i=0;$i<$record_size;$i++){
	$student_id = $records[$i]['UserID'];
	$x .= '<tr>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($i+1).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['StudentName']).($show_group_name?' ('.$records[$i]['GroupName'].')':'').'</td>';
		$yesterday_status = $Lang['General']['EmptySymbol'];
		if(isset($yesterday_records[$student_id])){
			$yesterday_remark = $yesterday_records[$student_id]['Remark'];
			if($yesterday_records[$student_id]['OutStatus'] == PROFILE_TYPE_EARLY){
				$yesterday_status = $Lang['StudentAttendance']['HostelEarlyLeave'];
			}else if($yesterday_records[$student_id]['InStatus'] == CARD_STATUS_ABSENT){
				$yesterday_status = $Lang['StudentAttendance']['HostelAbsent'];
			}else if($yesterday_records[$student_id]['InStatus']!='' && $yesterday_records[$student_id]['InStatus'] == CARD_STATUS_PRESENT){
				$yesterday_status= $Lang['StudentAttendance']['HostelAttend'];
			}
		}
		$x .= '<td class="eSporttdborder eSportprinttext">'.$yesterday_status.'</td>';
		$in_status = $records[$i]['InStatus']!=''?$records[$i]['InStatus']:$lc->DefaultAttendanceStatus;
		$x .= '<td class="eSporttdborder eSportprinttext">'.$in_status_ary[$in_status].'</td>';
		$in_time = $records[$i]['InTime']!=''? date("H:i",strtotime($records[$i]['InTime'])) : '';
		$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($in_time).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.$out_status_ary[$records[$i]['OutStatus']].'</td>';
		$out_time = $records[$i]['OutTime']!=''? date("H:i",strtotime($records[$i]['OutTime'])) : '';
		$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($out_time).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['Remark']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['DateModified']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($records[$i]['ConfirmedUser']).'</td>';
	$x .= '</tr>';
}
echo $x;
?>
</table>
<br />
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>