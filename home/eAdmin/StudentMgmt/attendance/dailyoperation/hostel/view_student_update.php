<?php
// Editing by 
/*
 * 2019-11-19 (Ray):	added $sys_custom['StudentAttendance']['HostelAttendance_Reason']
 * 2017-08-09 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");


intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();

$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) 
		|| ($GroupID=='' && !$can_take_all_groups) || $TargetDate=='' || count($StudentID)==0) {
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'] = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
	if($GroupID != ''){
		header("Location: view_student.php?GroupID=".$GroupID."&TargetDate=".$TargetDate);
	}else{
		header ("Location: index.php");
	}
	exit();
}

//$lc = new libcardstudentattend2();

$use_magic_quotes = $lc->is_magic_quotes_active;

$today = date("Y-m-d");
$ts = strtotime($TargetDate);
$tomorrow = date("Y-m-d",$ts+86400);
$year = date("Y",$ts);
$month = date("m",$ts);
$day = date("d",$ts);

$student_id_ary = $_POST['StudentID'];
$student_size = count($student_id_ary);

$params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$TargetDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
if(!$can_take_all_groups || $GroupID!=''){
	$params['GroupID'] = $GroupID;
	//$params['IsGroupStaff'] = true;
	//$params['GroupUserID'] = $_SESSION['UserID'];
}
$confirmed_records = $lc->getHostelAttendanceGroupsConfirmedRecords($params);
//$confirm_record_id = '';
//if(count($confirmed_records)>0 && $confirmed_records[0]['RecordID']!='') $confirm_record_id = $confirmed_records[0]['RecordID'];

$lc->Start_Trans();

$result_ary = array();
for($i=0;$i<$student_size;$i++){
	$data = array();
	$data['UserID'] = $student_id_ary[$i];
	$data['DayNumber'] = $day;
	$data['RecordID'] = $_POST['RecordID'][$student_id_ary[$i]];
	$data['InStatus'] = $_POST['InStatus'][$student_id_ary[$i]];
	if($sys_custom['StudentAttendance']['HostelAttendance_Reason'] != true) {
		$data['OutStatus'] = $_POST['OutStatus'][$student_id_ary[$i]];
	}
	if(isset($_POST['InTime'][$student_id_ary[$i]])){
		$data['InTime'] = $_POST['InTime'][$student_id_ary[$i]]!=''? ($TargetDate.' '.$_POST['InTime'][$student_id_ary[$i]].':00'):'';
	}
	if(isset($_POST['OutTime'][$student_id_ary[$i]])){
		$data['OutTime'] = $_POST['OutTime'][$student_id_ary[$i]]!=''? (($today > $TargetDate? $tomorrow:$TargetDate).' '.$_POST['OutTime'][$student_id_ary[$i]].':00'):''; // assume it is next day out
	}
	$data['Remark'] = trim( $use_magic_quotes? stripslashes($_POST['Remark'][$student_id_ary[$i]]) : $_POST['Remark'][$student_id_ary[$i]] );

	if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
		if($data['InStatus'] == CARD_STATUS_ABSENT) {
			$data['Waived'] = $_POST['Waived'][$student_id_ary[$i]];
			$data['Reason'] = $_POST['ReasonText'][$student_id_ary[$i]];
		} else {
			$data['Waived'] = '';
			$data['Reason'] = '';
		}
	}

	$result_ary['upsert_'.$student_id_ary[$i]] = $lc->upsertHostelAttendanceRecord($year,$month,$data);
}
if(!in_array(false,$result_ary)){
	$_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'] = $Lang['StudentAttendance']['DailyStatusConfirmSuccess'];
	for($i=0;$i<count($confirmed_records);$i++){
		$confirmed_params = array('GroupID'=>$confirmed_records[$i]['GroupID'],'RecordDate'=>$TargetDate);
		if($confirmed_records[$i]['RecordID']!=''){
			$confirmed_params['RecordID'] = $confirmed_records[$i]['RecordID'];
		}
		$lc->upsertHostelAttendanceGroupConfirmRecord($confirmed_params);
	}
	$lc->Commit_Trans();
}else{
	$_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'] = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
	$lc->RollBack_Trans();
}
intranet_closedb();
header("Location: view_student.php?GroupID=".$GroupID."&TargetDate=".$TargetDate);
?>