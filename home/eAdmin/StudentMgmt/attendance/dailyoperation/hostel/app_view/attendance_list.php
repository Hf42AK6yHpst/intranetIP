<?php

$PATH_WRT_ROOT = "../../../../../../../";

switch (strtolower($_GET['parLang'])) {
    case 'en':
        $intranet_hardcode_lang = 'en';
        break;
    default:
        $intranet_hardcode_lang = 'b5';
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");



$UserID = $uid;
$_SESSION['UserID'] = $uid;
$_SESSION['CurrentSchoolYearID'] ='';
$_SESSION['intranet_session_language'] = $intranet_hardcode_lang;



$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
 



if(!isset($date) || $date=="")
{
    $date=date('m/d/Y');
}

$ts_record = strtotime($date);

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$ToDate = date('Y-m-d',strtotime($date));
$today = date('m/d/Y');
//intranet_auth();
intranet_opendb();
$libdb = new libdb();
$lu = new libuser($UserID);
$lc = new libcardstudentattend2();
$linterface = new interface_html();
$lc->retrieveSettings();
$lc->createTable_LogAndConfirm();
$attendance_mode = $lc->attendance_mode;
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);

session_register_intranet('UserType', $lu->RecordType);

if(!isset($_SESSION['SSV_USER_ACCESS'])){
$UserRightTarget = new user_right_target();
$_SESSION['SSV_USER_ACCESS'] = $UserRightTarget->Load_User_Right($UserID);

}

// debug_pr($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]);
// debug_pr($sys_custom['StudentAttendance']['HostelAttendance']);
// debug_pr($_SESSION['UserType'] == USERTYPE_STAFF);
// die();
// #handling for the $show_main_table
if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $sys_custom['StudentAttendance']['HostelAttendance']) ) {
    intranet_closedb();
    echo $i_general_no_access_right	;
    exit;
}



// if($lc->HostelAttendanceGroupCategory == ''){
//     $lc->HostelAttendanceGroupCategory = -1;
//     //intranet_closedb();
//     //header ("Location: ../../");
//     //exit();
// }


// if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
//     $CurrentPageArr['eServiceHostelAttendance'] = 1;
// }else{
//     $CurrentPageArr['StudentAttendance'] = 1;
// }
// $CurrentPage = "PageDailyOperation_ViewHostelGroupStatus";

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

if ($ToDate == "")
{
    $ToDate = date('Y-m-d');
}



$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];

$create_table_success = $lc->createTableHostelAttendanceDailyLog($txt_year,$txt_month);
$params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$ToDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && !in_array($_SESSION['UserID'],$hostel_admins)){
    $params['IsGroupStaff'] = true;
    $params['GroupUserID'] = $_SESSION['UserID'];
}



#end handling for the $show_main_table


if(!isset($charactor) || $charactor=="")
{
    $charactor = "hostelGroup";
}

if(!isset($timeslots) || $timeslots=="")
{
    if($attendance_mode==0){
        $timeslots = "in";
    }
    else if($attendance_mode==1){
        $timeslots = "out";
    }else{
        
        if(date("a")=="pm")
        {
            $timeslots = "in";
        }
        else
        {
            $timeslots = "out";
        }
    }
}

if(!isset($msg) || $msg=="")
{
    $msg=0;
}


if($timeslots == "in") 	$DayType = PROFILE_DAY_TYPE_AM;

else 	$DayType = PROFILE_DAY_TYPE_PM;

//     if ($Settings['EnableEntryLeavePeriod']==1) {
//         $FilterInActiveStudent .= "
//         INNER JOIN
//         CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp
//         on u.UserID = selp.UserID
//         	AND
//         	'".$ToDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59')
//         ";
//     }

$show_main_table = "";
if($charactor=="hostelGroup"){
    
    $groups = $lc->getHostelAttendanceGroupsConfirmedRecords($params);
    
    if(count($groups)==0){
        $show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
    }
    
    
    for($i=0;$i<count($groups);$i++){
        
        
        if(!isset($groups[$i]['ConfirmedUser']) || !isset($groups[$i]['ConfirmedDate'])){
            $taken = 0;
        } else {
            $taken = 1;
        }
        $show_main_table .= '<tr><td width="25%" class="line"><a class="tablelink" href="javascript:handle_attendance('.$groups[$i]['GroupID'].','.$taken.')">'.$groups[$i]['Title'].'</a></td>';
        $show_main_table .= '<td width="25%"  class="line">'.$groups[$i]['ConfirmedUser'].'</td>';
        $show_main_table .= '<td width="25%" class="line">'.($groups[$i]['ConfirmedDate']!=''? $groups[$i]['ConfirmedDate']:'-').'</td>';
        $show_main_table .="<td width='25%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$groups[$i]['GroupID'].",".$taken.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
    }
    
    $hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
    $can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];
    if ($can_take_all_groups){
        $footer .=  '<div id="footer" data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">';
        
        $footer .=	'<a href="javascript:handle_attendance(\'\',\'\');" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
	                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" >'.$Lang["StudentAttendance"]["TakeAttendanceForAllHostelGroups"].'</a>';
    }
}









?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="Javascript" src='<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/lesson_attendance.js'></script>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>

<script>

	jQuery(document).ready( function() {
		 var msg = <?=$msg?>;
		 if(msg=="1") {
			 alert("<?=$Lang['StudentAttendance']['TeacherApp']['RecordHasUpdated']?>");
			 window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&timeslots=<?=$timeslots?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&parLang=<?=$parLang?>";			 			 
	     }
		//for charactor	
		 <?php if($plugin['attendancestudent']){?>	
		 $('select').selectmenu('refresh', true);
		 <?php } ?>
		 var charactor = '<?=$charactor?>';
		 if(charactor=='lesson'){
		 	timeslots_div
		 	$('#timeslots_div').hide();
		 }else{		 	
		 	if(!checkTargetDateChanged()){
		 		var today = '<?=$today?>';
		 		$('#date').val(today);
		 		refresh();	
		 	}
		 }
		//for timeslots
		 $( "input[id='<?=$timeslots?>']" ).prop( "checked", true ).checkboxradio( "refresh" );
		 
// 		 $("input[name='timeslots']").change(function(){
// 			 refresh();
// 		 });
		 
		 
		 
	});
	
	//handle the datepicker onchange function
	function onSelectedDateOfJqueryMobileDatePicker() {
		   refresh();	
	}
	
	
	function refresh(){	   		
		charactor = '<?=$charactor?>';
		
		if(!checkTargetDateChanged()){
			 location.reload();
		} else{
	    	   document.form1.action="attendance_list.php";
	    	   document.form1.submit();
	       }
	      
    }

    function handle_attendance(GroupID,taken){
       var div = document.getElementById("div1");
       var input = document.createElement("input");
       input.type = "hidden";
       input.name = "id";
       input.value = GroupID;
       div.appendChild(input);

       var input1 = document.createElement("input");
       input1.type = "hidden";
       input1.name = "taken";
       input1.value = taken;
       div.appendChild(input1);

       document.form1.action = "take_attendance.php";
       document.form1.submit();
    }
    

    

    function back(){
       alert("back");
    }

    function checkTargetDateChanged()
    {
    	var target_date = $('#date').val();
    	
    	var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
        var today = mm+'/'+dd+'/'+yyyy;
    	<?php// if($lc->DisableISmartCardPastDate=='1'){ ?>
//     	if(target_date != '' && target_date < today){
    		//alert('<?//=$Lang['StudentAttendance']['ISmartCardDisableTakePastRecord']?>');
//     		return false;
//     	}
    	<?php// } ?>
    	<?php// if($lc->CannotTakeFutureDateRecord=='1'){ ?>
//     	if(target_date != '' && target_date > today){
    		//alert('<?//=$Lang['StudentAttendance']['WarningCannotTakeFutureDateRecord']?>');
//     		return false;
//     	}
    	<?php// } ?>
    	return true;
    } 
</script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<style type="text/css">
    #body{
            
         background:#ffffff;
    }
      
    .form_table_attendence{ width:100%; margin:0;}
 	.form_table_attendence tr td.line{vertical-align:center;	text-align:center; line-height:18px; padding-top:5px; padding-bottom:5px;	border-bottom:2px solid #EFEFEF; padding-left:2px; padding-right:2px}
    .form_table_attendence tr td {vertical-align:center;	text-align:center; line-height:18px; padding-top:5px; padding-bottom:5px;padding-left:2px; padding-right:2px} 
		
	.form_table_attendence tr td.field_title{width:25%;  padding-left:2px; padding-right:2px;border-width: 5px;}
	.form_table_attendence tr td.field_title_short{	border-bottom:2px solid #FFFFFF ; width:14%; padding-left:2px; padding-right:2px}
	.form_table_attendence col.field_title_short{width:14%; }
	.form_table_attendence col.field_content_short{width:20%; }
	.form_table_attendence tr td a{ color: #9966CC;	}
	.form_table_attendence tr th a{ color: #FFFFFF;	}
    .form_table_attendence tr td a.tablelink{color: #38c; text-decoration: none;}
    .form_table_attendence tr td a:hover{ color: #FF0000;	}
	.form_table_attendence col.field_c {width:10px;}
	.form_table_attendence tr td .textbox{	width:98%}
	
	.header a:link { text-decoration: none; color: white;}
    .header a:active {text-decoration: none; color: white !important;} 
    .header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;} 
    label {padding: 10px!important;}
   
</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />	

</head>
	<body>
	
		<div data-role="page" id="body">			
   	        <div data-role="content">
				
				<form id="form1" name="form1" method="get" >
				    <?php if(!isset($msg)||$msg=="") { ?>
				      <table class="form_table_attendence" width="100%" >
				        <tr>
				          <td class="field_title">
				            <label class="label" for="date" ><?=$Lang['StudentAttendance']['RecordDate']?></label>
				          </td>
				          <td>
				            <input type="text" data-role="date" id="date" name="date" value="<?=$date?>" onchange="javascript:refresh();">
				          </td>
				        </tr>
				        <tr>				        
				         <input type='hidden' id='charactor' name='charactor' value='hostelGroup'>     				         
					        <tr id="timeslots_div">
					          <td class="field_title">
					            <label class="label" for="timeslots" ><?=$Lang['StudentAttendance']['TimeSlot']?></label>
					          </td>
					          <td style="text-align:left;">
						     	<fieldset data-role="controlgroup" data-type="horizontal" width="100%" >

							        <input type="radio" name="timeslots" id="in" value="in" >
							        <label for="in"><?=$Lang['StudentAttendance']['InStatus']?></label>	

                                    <?php if($sys_custom['StudentAttendance']['HostelAttendance_Reason'] != true) { ?>
							        <input type="radio" name="timeslots" id="out" value="out" >
							        <label for="out"><?=$Lang['StudentAttendance']['OutStatus']?></label>
									<?php } ?>
							    </fieldset>
					          </td>
					        </tr>
				      </table>				   
                     <table class="form_table_attendence" width="100%" >
                         <tr style="background-color:#E6E6E6;">
                            <td width="25%">
                            <?=$Lang['StudentAttendance']['HostelGroup']?>
                            </td>
                            <td width="25%">
                               <?=$i_StudentAttendance_Field_ConfirmedBy?>
                            </td>
                            <td width="25%">
                               <?=$i_StudentAttendance_Field_LastConfirmedTime?>
                            </td>
                            <td width="25%">
                            </td>
                         </tr> 
                         <?=$show_main_table?>                                              
                      </table>                                             
                      <div id="div1">
                      </div>
                                          
                      <input type="hidden" value=<?=$token?> name="token">
                      <input type="hidden" value=<?=$uid?> name="uid">
                      <input type="hidden" value=<?=$ul?> name="ul">                
                      <input type="hidden" value=<?=$parLang?> name="parLang">  
                    <?php } ?> 
                    <?= $footer?>
				</form>
				 
			</div>
	    </div>
	   
	</body>
</html>

<?php

intranet_closedb();

?>