<?php
// Editing by 
/*
 * 2020-02-03 (Ray):    Added HostelAttendanceDisablePastDate & HostelAttendanceDisableFutureDate
 * 2019-11-18 (Ray):    Added $sys_custom['StudentAttendance']['HostelAttendance_Reason']
 * 2018-05-02 (Carlos): Added UserStatus=1 to getHostelAttendanceRecords() params to only fetch active students.
 * 2017-08-09 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");



intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) ) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];

if( ($GroupID=='' && !$can_take_all_groups) || $TargetDate=='' || $lc->HostelAttendanceGroupCategory == ''){
	intranet_closedb();
	header ("Location: ../../");
	exit();
}

if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
	$GeneralSetting = $lc->GetLibGeneralSettings();
	$SettingList = array();
	$SettingList[] = "'HostelAttendanceDisablePastDate'";
	$SettingList[] = "'HostelAttendanceDisableFutureDate'";
	$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
}

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
	$CurrentPageArr['eServiceHostelAttendance'] = 1;
}else{
	$CurrentPageArr['StudentAttendance'] = 1;
}
$CurrentPage = "PageDailyOperation_ViewHostelGroupStatus";

$linterface = new interface_html();
$libgrouping = new libgrouping();

$ts = strtotime($TargetDate);
$year = date("Y",$ts);
$month = date("m",$ts);
$YesterdayDate = date("Y-m-d", $ts - 86400);
$yesterday_year = date("Y", $ts - 86400);
$yesterday_month = date("m", $ts - 86400);

$params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$TargetDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
if(!$can_take_all_groups){
	$params['GroupID'] = $GroupID;
	$params['IsGroupStaff'] = true;
	$params['GroupUserID'] = $_SESSION['UserID'];
}else if($GroupID!=''){
	$params['GroupID'] = $GroupID;
}
$confirmed_records = $lc->getHostelAttendanceGroupsConfirmedRecords($params);

$groups = $libgrouping->returnCategoryGroups($lc->HostelAttendanceGroupCategory);
if(!$can_take_all_groups){
	$can_take_groups = $lc->getHostelAttendanceGroupsConfirmedRecords(array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$TargetDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory,'IsGroupStaff'=>true,'GroupUserID'=>$_SESSION['UserID']));
	$responsible_group_ids = Get_Array_By_Key($can_take_groups,'GroupID');
	$responsible_groups = array();
	for($i=0;$i<count($groups);$i++){
		if(in_array($groups[$i]['GroupID'],$responsible_group_ids)){
			$responsible_groups[] = $groups[$i];
		}
	}
	$groups = $responsible_groups;
}
if($can_take_all_groups){
	array_unshift($groups,array('',$Lang['General']['All']));
}
$group_select = $linterface->GET_SELECTION_BOX($groups, ' id="GroupID" name="GroupID" onchange="document.form1.submit();" ', '', $GroupID, false);


$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_Date</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$linterface->GET_DATE_PICKER("TargetDate", $TargetDate)."</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['StudentAttendance']['HostelGroup']."</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$group_select."</td></tr>\n";

$confirmed_records_size = count($confirmed_records);
if($confirmed_records_size>0){
	for($i=0;$i<$confirmed_records_size;$i++){
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".($i==0?$i_StudentAttendance_Field_LastConfirmedTime:"&nbsp;")."</td>";
		$x .= "<td class=\"tabletext\" width=\"70%\">".($confirmed_records_size>1?$confirmed_records[$i]['Title'].": ":'').($confirmed_records[$i]['ConfirmedDate']!=''? $confirmed_records[$i]['ConfirmedDate']." (".$confirmed_records[$i]['ConfirmedUser'].")":$Lang['General']['EmptySymbol'])."</td></tr>";
	}
}else{
	$x .= "<tr><td>&nbsp;</td><td class=\"tabletext\" width=\"70%\">$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
$x .= "<tr>
		<td class=\"dotline\" colspan=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\" /></td>
		</tr>
		<tr><td align=\"center\" colspan=\"2\">".$linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "document.form1.submit();")."</td></tr>";
$x .= "</table>\n";
$table_confirm = $x;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
/*
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:AbsToPre()\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" alt=\"button_import\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_StudentAttendance_Action_SetAbsentToOntime
					</a>
				</td>";
*/
$in_status_ary = array(
	array(CARD_STATUS_PRESENT,$Lang['StudentAttendance']['HostelAttend']),
	array(CARD_STATUS_ABSENT,$Lang['StudentAttendance']['HostelAbsent'])
);

$out_status_ary = array(
	array('','N.A.'),
	array((string)CARD_STATUS_PRESENT,$Lang['StudentAttendance']['HostelNormalLeave']),
	array((string)PROFILE_TYPE_EARLY,$Lang['StudentAttendance']['HostelEarlyLeave'])
);

$setAllInStatus = "<br />".$linterface->GET_SELECTION_BOX($in_status_ary, ' id="in_status_all" name="in_status_all" ', '', $lc->DefaultAttendanceStatus, false)."<br />
					".$linterface->Get_Apply_All_Icon("javascript:setAllStatus('in_status',document.getElementById('in_status_all').value);");

$setAllOutStatus = "<br />".$linterface->GET_SELECTION_BOX($out_status_ary, ' id="out_status_all" name="out_status_all" ', '', '', true)."<br />
					".$linterface->Get_Apply_All_Icon("javascript:setAllStatus('out_status',document.getElementById('out_status_all').value);");


$table_attend = "";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
	$x .= "<td class=\"tabletop\">#</td>";
	$x .= "<td class=\"tabletop\" width=\"11%\">$i_UserName</td>";
	$x .= "<td class=\"tabletop\" width=\"10%\">".$Lang['StudentAttendance']['YesterdayStatus']."</td>";
	$x .= "<td class=\"tabletop\" width=\"10%\">".$Lang['StudentAttendance']['InStatus'].$setAllInStatus."</td>";
	$x .= "<td class=\"tabletop\" width=\"10%\">".$Lang['StudentAttendance']['InTime']." (hh:mm) <span class=\"red\">#</span></td>";
	if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
		$x .= "<td class=\"tabletop\" width=\"10%\">".$i_SmartCard_Frontend_Take_Attendance_Waived." <span class=\"red\">#</span></td>";
		$x .= "<td class=\"tabletop\" width=\"10%\">".$i_Attendance_Reason." <span class=\"red\">#</span></td>";
	}
    if($sys_custom['StudentAttendance']['HostelAttendance_Reason'] != true) {
        $x .= "<td class=\"tabletop\" width=\"10%\">" . $Lang['StudentAttendance']['OutStatus'] . $setAllOutStatus . "</td>";
    }
	$x .= "<td class=\"tabletop\" width=\"10%\">".$Lang['StudentAttendance']['OutTime']." (hh:mm) <span class=\"red\">#</span></td>";
	$x .= "<td class=\"tabletop\" width=\"15%\">".$Lang['General']['Remark']." <span class=\"red\">#</span></td>";
	$x .= "<td class=\"tabletop\" width=\"12%\">".$Lang['General']['LastUpdatedTime']."</td>";
	$x .= "<td class=\"tabletop\" width=\"12%\">".$Lang['StudentAttendance']['LastConfirmPerson']."</td>";
$x .= "</tr>";

$record_params = array('RecordDate'=>$TargetDate,'AcademicYearID'=>Get_Current_Academic_Year_ID(),'CategoryID'=>$lc->HostelAttendanceGroupCategory,'UserStatus'=>1);
if(!$can_take_all_groups){
	$record_params['GroupID'] = $GroupID;
}else if($GroupID!=''){
	$record_params['GroupID'] = $GroupID;
}
$records = $lc->getHostelAttendanceRecords($year, $month, $record_params);
$record_size = count($records);
$record_params['RecordDate'] = $YesterdayDate;
$record_params['UserIDToRecord'] = 1;
$yesterday_records = $lc->getHostelAttendanceRecords($yesterday_year, $yesterday_month, $record_params);


$show_group_name = $GroupID=='';

for($i=0;$i<$record_size;$i++){
	$student_id = $records[$i]['UserID'];
	$row_css = 'tablebottom';
	if($records[$i]['OutStatus'] == PROFILE_TYPE_EARLY){
		$row_css = 'attendance_early';
	}else if($records[$i]['InStatus'] == CARD_STATUS_ABSENT){
		$row_css = 'attendance_norecord';
	}else if($records[$i]['RecordID'] != '' && $records[$i]['ModifiedBy']!=''){
		$row_css = 'row_approved';
	}

	$last_records = false;
	if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
		if ($row_css == 'tablebottom') {
			if(isset($yesterday_records[$student_id]) && $yesterday_records[$student_id]['InStatus'] != '') {
				$last_records = $yesterday_records[$student_id];
			}

			if($last_records == false) {
				$params = array('StartDate' => $yesterday_year . '-' . $yesterday_month . '-01', 'EndDate' => $YesterdayDate, 'UserID' => $student_id);
				$last_hostel_records = $lc->getStudentLastHostelAttendanceRecords($yesterday_year, $yesterday_month, $params);
				$last_hostel_records = BuildMultiKeyAssoc($last_hostel_records, 'UserID');

				if (isset($last_hostel_records[$student_id]) && $last_hostel_records[$student_id]['InStatus'] != '') {
					$last_records = $last_hostel_records[$student_id];
				}
			}

			if($last_records == false) {
				$last_year = $yesterday_year;
				$last_month = $yesterday_month - 1;
				if($last_month == 0) {
					$last_month = 12;
					$last_year = $yesterday_year - 1;
				}
				$EndDate = date("Y-m-t", strtotime($last_year.'-'.$last_month.'-01'));
				$params = array('StartDate'=>date("Y-m-d", strtotime($last_year.'-'.$last_month.'-01')),'EndDate'=>$EndDate,'UserID'=>$student_id);
				$last_hostel_records = $lc->getStudentLastHostelAttendanceRecords($last_year,$last_month,$params);
				$last_hostel_records = BuildMultiKeyAssoc($last_hostel_records, 'UserID');
				if (isset($last_hostel_records[$student_id]) && $last_hostel_records[$student_id]['InStatus'] != '') {
					$last_records = $last_hostel_records[$student_id];
				}
			}
		}
	}

	$x .= '<tr class="'.$row_css.'">';
		$x .= '<td>'.($i+1).'</td>';
		$x .= '<td>'.$records[$i]['StudentName'].($show_group_name?' ('.$records[$i]['GroupName'].')':'').'<input type="hidden" name="StudentID[]" value="'.$student_id.'" /><input type="hidden" name="RecordID['.$student_id.']" value="'.$records[$i]['RecordID'].'" /></td>';
		$yesterday_status = $Lang['General']['EmptySymbol'];
		if(isset($yesterday_records[$student_id])){
			$yesterday_remark = $yesterday_records[$student_id]['Remark'];
			if($yesterday_records[$student_id]['OutStatus'] == PROFILE_TYPE_EARLY){
				$yesterday_status = $Lang['StudentAttendance']['HostelEarlyLeave'];
			}else if($yesterday_records[$student_id]['InStatus'] == CARD_STATUS_ABSENT){
				$yesterday_status = $Lang['StudentAttendance']['HostelAbsent'];
			}else if($yesterday_records[$student_id]['InStatus']!='' && $yesterday_records[$student_id]['InStatus'] == CARD_STATUS_PRESENT){
				$yesterday_status= $Lang['StudentAttendance']['HostelAttend'];
			}
		}

		$x .= '<td><span title="'.intranet_htmlspecialchars($yesterday_remark).'">'.$yesterday_status.'</span></td>';

	$in_status = $records[$i]['InStatus']!=''?$records[$i]['InStatus']:$lc->DefaultAttendanceStatus;
        if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
			if($row_css == 'tablebottom') {
			    if($last_records != false) {
					$in_status = $last_records['InStatus'];
				}
			}
        }

		$x .= '<td>'.$linterface->GET_SELECTION_BOX($in_status_ary, ' class="in_status" id="InStatus'.$student_id.'" name="InStatus['.$student_id.']" onchange="onStatusChanged(\'In\','.$student_id.')" ', '', $in_status, false).'</td>';
		$in_time = $records[$i]['InTime']!=''? date("H:i",strtotime($records[$i]['InTime'])) : '';
		$in_time_hidden = $in_status == CARD_STATUS_ABSENT? ' style="display:none" ' : '';
		$in_time_html = '<span id="InTime_Display'.$student_id.'" '.$in_time_hidden.'>'.$in_time.'&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "onRevealEditTime($student_id,'".$in_time."','In')", "InEditTimeBtn".$student_id, "", "", '').'</span>';
		$in_time_html.= '<span id="InTime_Edit'.$student_id.'" style="display:none;">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "onCancelEditTime($student_id,'In')", "InCancelTimeBtn".$student_id, "", "", '').'</span>';
		$x .= '<td>'.$in_time_html.'</td>';
	    if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
			$waived = $records[$i]['Waived'];
			$reason = $records[$i]['Reason'];
			if($row_css == 'tablebottom') {
				$in_status = $last_records['InStatus'];
				$waived = $last_records['Waived'];
				$reason = $last_records['Reason'];
			}

			$waive_display = $in_status==CARD_STATUS_PRESENT ? 'style="display:none"' : '';
			$style_display = $in_status==CARD_STATUS_PRESENT ? 'display:none;':'';


			$x .= '<td><input type="checkbox" class="WaiveBtn" name="Waived['.$student_id.']" id="Waived'.$student_id.'" value="1" '.(($waived == 1)? 'checked':'').' '.$waive_display.'/></td>';
			$x .= "<td nowrap>";
		    $x .= '<div id="ReasonDiv'.$student_id.'" style="'.$style_display.'">';
			$x .= "<select class=\"reason_select\" id=\"Reason$student_id\" name=\"Reason[$student_id]\" onchange=\"onReasonChanged('$student_id')\" style=\"\">";
			$x .= "<option value=\"\">N.A</option>";
			$lastVaue = end($Lang['StudentAttendance']['HostelGroupAbsenceReason']);
			$have_selected = false;
			$style_display = '';
			foreach($Lang['StudentAttendance']['HostelGroupAbsenceReason'] as $temp) {
			    $selected_str = '';
			    if($temp == $lastVaue) {
					$style_display = 'display:none;';
                    if($reason != '' && $have_selected == false) {
						$selected_str = 'SELECTED';
						$style_display = '';
					}
				} elseif($temp == $reason) {
					$selected_str = 'SELECTED';
					$have_selected = true;
					$style_display = 'display:none;';
				}
			    $x .= "<option value=\"$temp\" $selected_str>".$temp."</option>";
			}
			$x .= "</select>";
            $x .= "<br/>";
			$x .= $linterface->GET_TEXTBOX_NAME("ReasonText".$student_id, "ReasonText[$student_id]", $reason, 'ReasonText', array('style'=>'width: 100px;'.$style_display));

			$x .= '</div>';
			$x .= "</td>";
	    }

        if($sys_custom['StudentAttendance']['HostelAttendance_Reason'] != true) {
            $x .= '<td>' . $linterface->GET_SELECTION_BOX($out_status_ary, ' class="out_status" id="OutStatus' . $student_id . '" name="OutStatus[' . $student_id . ']" onchange="onStatusChanged(\'Out\',' . $student_id . ')" ', '', $records[$i]['OutStatus'], true) . '</td>';
        }

		$out_time = $records[$i]['OutTime']!=''? date("H:i",strtotime($records[$i]['OutTime'])) : '';
		$out_time_hidden = $records[$i]['OutStatus']==''? ' style="display:none" ':'';
		$out_time_html = '<span id="OutTime_Display'.$student_id.'" '.$out_time_hidden.'>'.$out_time.'&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "onRevealEditTime($student_id,'".$out_time."','Out')", "OutEditTimeBtn".$student_id, "", "", '').'</span>';
		$out_time_html.= '<span id="OutTime_Edit'.$student_id.'" style="display:none;">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "onCancelEditTime($student_id,'Out')", "OutCancelTimeBtn".$student_id, "", "", '').'</span>';
		$x .= '<td>'.$out_time_html.'</td>';
		$x .= '<td>'.$linterface->GET_TEXTBOX('Remark_'.$student_id, 'Remark['.$student_id.']', $records[$i]['Remark']).'</td>';
		$x .= '<td>'.Get_String_Display($records[$i]['DateModified']).'</td>';
		$x .= '<td>'.Get_String_Display($records[$i]['ConfirmedUser']).'</td>';
	$x .= '</tr>';
}

$x .= "</table>";

$table_attend = $x;


$PAGE_NAVIGATION[] = array($display_period,'index.php?TargetDate='.urlencode($TargetDate));
$PAGE_NAVIGATION[] = array($group_name);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ViewHostelGroupStatus'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
if(isset($_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<form name="form1" id="form1" method="post" action="view_student.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table_confirm?>
		</td>
	</tr>
		<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	    	<tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
	    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td valign="bottom" align="left">
						<table border=0>
							<tr>
								<td class="attendance_norecord" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['HostelAbsent']?>
								</td>
								<td class="attendance_early" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['HostelEarlyLeave']?>
								</td>
								<td class="tablebottom" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['UnConfirmed']?>
								</td>
							</tr>
						</table>
					</td>
					<!--
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<?=$table_tool?>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
					-->
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?= $table_attend ?>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" style="margin-top:8px">
				<tr>
					<td align="left"><?=str_replace('<!--SYMBOL-->','<span class="red">#</span>',$Lang['StudentAttendance']['OptionalDataRemark'])?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
                        <?php
                        $show_save_button = true;
						if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
							if ($Settings['HostelAttendanceDisablePastDate'] == 1) {
								if (strtotime(date("Y-m-d")) > $ts) {
									$show_save_button = false;
								}
							}
							if ($Settings['HostelAttendanceDisableFutureDate'] == 1) {
								if ($ts > strtotime(date("Y-m-d"))) {
									$show_save_button = false;
								}
							}
						}
                        ?>
                        <?php if($show_save_button == true) { ?>
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "Check_Form(document.form1,'view_student_update.php','$i_SmartCard_Confirm_Update_Attend?')") ?>
						<?php } ?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='index.php?TargetDate=".urlencode($TargetDate)."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br>
<script type="text/javascript" language="javascript">

$(document).ready(function() {
    <?php if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) { ?>
    $(".reason_select").each(function() {
        //$(this).trigger("change");
    });

    <?php } ?>
});

function openPrintPage()
{
	newWindow("view_student_print.php?GroupID=<?=urlencode($GroupID)?>&TargetDate=<?=urlencode($TargetDate)?>",24);
}

function setAllStatus(select_class, select_value)
{
	var objs = $('.'+select_class);
	for(var i=0;i<objs.length;i++){
		$(objs[i]).val(select_value);
		<?php if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) { ?>
        if(select_class == 'in_status') {
            $(objs[i]).trigger("change");
        }
        <?php } ?>
	}
}

function onRevealEditTime(uid, time, inout)
{
	var text_field = '<input type="text" id="'+inout+'Time'+uid+'" name="'+inout+'Time['+uid+']" value="'+time+'" size="12" onchange="onEditTimeChanged('+uid+',\''+inout+'\')" />';
	
	var text_obj = $('#'+inout+'Time'+uid);
	if(text_obj && text_obj.length > 0)
	{
		text_obj.val(time);
	}else{
		$('#'+inout+'CancelTimeBtn'+uid).before(text_field);
	}
	
	$('#'+inout+'Time_Display'+uid).hide();
	$('#'+inout+'Time_Edit'+uid).show();
}

function onCancelEditTime(uid,inout)
{
	$('#'+inout+'Time_Display'+uid).show();
	$('#'+inout+'Time_Edit'+uid).hide();
	$('#'+inout+'Time'+uid).remove();
}

function onEditTimeChanged(uid,inout)
{
	var obj = $('#'+inout+'Time'+uid);
	var val = $.trim(obj.val());
	if(val != ''){
		if(!val.match(/^\d\d:\d\d$/g)){
			obj.val('');
			if($('#'+inout+'Time_Warn'+uid).length == 0){
				$('#'+inout+'Time_Edit'+uid).append('<div id="'+inout+'Time_Warn'+uid+'" class="red"><?=$Lang['StudentAttendance']['SlotTimeFormatWarning']?></div>');
			}
			$('#'+inout+'Time_Warn'+uid).show();
			setTimeout(function(){
				$('#'+inout+'Time_Warn'+uid).hide();
			},3000);
			return;
		}
		var parts = val.split(':');
		var hr = Math.max(0, Math.min(24, parseInt(parts[0])));
		var min = Math.max(0, Math.min(59, parseInt(parts[1])));
		//var sec = Math.max(0, Math.min(59, parseInt(parts[2])));
		
		var val_str = hr < 10? '0' + hr : '' + hr;
		val_str += ':';
		val_str += min < 10? '0' + min : '' + min;
		//val_str += ':';
		//val_str += sec < 10? '0' + sec : '' + sec;
		obj.val(val_str);
	}
}

function onReasonChanged(uid) {
    var val = $("#Reason"+uid).val();
    var lastValue = $('#Reason'+uid+' option:last-child').val();
    if(val == lastValue) {
        $("#ReasonText"+uid).val('');
        $("#ReasonText" + uid).show();
    } else {
        $("#ReasonText" + uid).hide();
        $("#ReasonText" + uid).val(val);
    }
}

function onStatusChanged(inout,uid)
{
	if(inout == 'In'){
		if($('#InStatus'+uid).val()=='<?=CARD_STATUS_ABSENT?>'){
			$('#InTime_Display'+uid).hide();
			$('#InTime_Edit'+uid).hide();
			$('#Waived'+uid).show();
			$('#ReasonDiv'+uid).show();
		}else{
			$('#InTime_Display'+uid).show();
			$('#InTime_Edit'+uid).hide();
            $('#Waived'+uid).hide();
            $('#ReasonDiv'+uid).hide();
		}
	}else if(inout == 'Out'){
		if($('#OutStatus'+uid).val()==''){
			$('#OutTime_Display'+uid).hide();
			$('#OutTime_Edit'+uid).hide();
		}else{
			$('#OutTime_Display'+uid).show();
			$('#OutTime_Edit'+uid).hide();
		}
	}
}

function Check_Form(obj, url, msg)
{
	<?php if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) { ?>
    var input_reason_check = true;
    $(".reason_select").each(function() {
        var val = $(this).val();
        var lastValue = $(this).find('option:last-child').val();
        if(val == lastValue) {
            var in_status = $(this).closest('tr').find('.in_status').val();
            if(in_status == '<?=CARD_STATUS_ABSENT?>') {
                if($(this).closest('tr').find('.ReasonText').val() == '') {
                    input_reason_check = false;
                    return;
                }
            }
        }
    });

    if(input_reason_check == false) {
        alert('<?=$i_studentAttendance_HostelAttendance_InputReason?>');
        return;
    }
    <?php } ?>

	if(confirm(msg)){
		obj.action = "view_student_update.php";
		obj.submit();
	}
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>