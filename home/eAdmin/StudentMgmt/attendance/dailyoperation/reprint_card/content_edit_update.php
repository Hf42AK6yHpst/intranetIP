<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$lcardstudentattend2 = new libcardstudentattend2();
$lfs = new libfilesystem ();

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}

$recordID = $_POST['recordID'];
$NewPhoto = ${"NewPhoto"};
$StudentSTRN = $_POST['StudentSTRN'];
$StudentBarcode = $_POST['StudentBarcode'];
$StudentSociety = $_POST['StudentSociety'];
$StudentPPSAccountNo = $_POST['StudentPPSAccountNo'];
$IssueDate = $_POST['IssueDate'];
$ExpiryDate = $_POST['ExpiryDate'];
$YearName1 = $_POST['YearName1'];
$YearName2 = $_POST['YearName2'];
$YearName3 = $_POST['YearName3'];
$YearName4 = $_POST['YearName4'];
$YearName5 = $_POST['YearName5'];
$YearName6 = $_POST['YearName6'];
$YearName7 = $_POST['YearName7'];
$YearName8 = $_POST['YearName8'];
$YearName9 = $_POST['YearName9'];
$YearName10 = $_POST['YearName10'];
$YearName11 = $_POST['YearName11'];
$YearName12 = $_POST['YearName12'];
$ClassName1 = $_POST['ClassName1'];
$ClassName2 = $_POST['ClassName2'];
$ClassName3 = $_POST['ClassName3'];
$ClassName4 = $_POST['ClassName4'];
$ClassName5 = $_POST['ClassName5'];
$ClassName6 = $_POST['ClassName6'];
$ClassName7 = $_POST['ClassName7'];
$ClassName8 = $_POST['ClassName8'];
$ClassName9 = $_POST['ClassName9'];
$ClassName10 = $_POST['ClassName10'];
$ClassName11 = $_POST['ClassName11'];
$ClassName12 = $_POST['ClassName12'];
$TeacherRemark = $_POST['TeacherRemark'];

$successAry = array();

if ($NewPhoto!="")
{
    $filePathBase = $intranet_root;

    $_folderDivision = ceil($recordID / 1500);

    $_filePath = $filePathBase . '/file/eClassApp/ReprintCard/' . $_folderDivision . '/' . $recordID . '_' . date ( 'Ymd_His' ). '.jpg';

    $_folderPath = $filePathBase . '/file/eClassApp/ReprintCard/' . $_folderDivision . '/';
    if (! file_exists ( $_folderPath )) {
        $successAry ['createRootFolder'] = $lfs->folder_new ( $_folderPath );
    }
    $successAry ['uploadFile'] = $lfs->lfs_copy($NewPhoto, $_filePath);
    if ($successAry ['uploadFile']) {
        $_dbFilePath = str_replace ( $filePathBase, '', $_filePath );
        $sql = "Update 
                        REPRINT_CARD_RECORD 
                Set 
                        PhotoPath = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $_dbFilePath ) . "',
                        ModifiedDate = now(),
                        ModifiedBy = '" . $ParentUserID . "'
                Where
                        RecordID = '" . $recordID . "'
                ";
        $successAry ['updateAttachmentRecord'] = $lcardstudentattend2->db_db_query ( $sql );
    }
}

$sql = "Update 
                REPRINT_CARD_RECORD
        Set
                StudentSTRN = '" . $StudentSTRN . "',
                StudentBarcode = '" . $StudentBarcode . "',
                StudentSociety = '" . $StudentSociety . "',
                StudentPPSAccountNo = '" . $StudentPPSAccountNo . "',
                TeacherRemark = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $TeacherRemark ) . "',
                YearName1 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName1 ) . "',
                YearName2 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName2 ) . "',
                YearName3 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName3 ) . "',
                YearName4 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName4 ) . "',
                YearName5 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName5 ) . "',
                YearName6 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName6 ) . "',
                YearName7 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName7 ) . "',
                YearName8 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName8 ) . "',
                YearName9 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName9 ) . "',
                YearName10 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName10 ) . "',
                YearName11 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName11 ) . "',
                YearName12 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $YearName12 ) . "',
                ClassName1 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName1 ) . "',
                ClassName2 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName2 ) . "',
                ClassName3 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName3 ) . "',
                ClassName4 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName4 ) . "',
                ClassName5 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName5 ) . "',
                ClassName6 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName6 ) . "',
                ClassName7 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName7 ) . "',
                ClassName8 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName8 ) . "',
                ClassName9 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName9 ) . "',
                ClassName10 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName10 ) . "',
                ClassName11 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName11 ) . "',
                ClassName12 = '" . $lcardstudentattend2->Get_Safe_Sql_Query ( $ClassName12 ) . "',
                IssueDate = '" . $IssueDate . "',
                ExpiryDate = '" . $ExpiryDate . "',
                ModifiedDate = now(),
                ModifiedBy = '" . $_SESSION['UserID'] . "'
        Where
                RecordID = '" . $recordID . "'
        ";
$successAry['UpdateContent'] = $lcardstudentattend2->db_db_query($sql);

if (in_array(false, $successAry)) {
    $returnMsgKey = 'UpdateUnsuccess';
}
else {
    $returnMsgKey = 'UpdateSuccess';
}
intranet_closedb();

header('Location: content_list.php?recordID='.$recordID.'&returnMsgKey='.$returnMsgKey);
?>