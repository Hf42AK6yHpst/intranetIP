<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$lcardstudentattend2 = new libcardstudentattend2();

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}


$recordID = $_POST['recordID'];
$rejectReason = $_POST['rejectReason'];

$successAry = array();

$sql = "Update 
                REPRINT_CARD_RECORD
        Set
                CardStatus = '5',
                RejectReason = '" .$lcardstudentattend2->Get_Safe_Sql_Query ( $rejectReason ) . "',
                ModifiedDate = now(),
                ModifiedBy = '" . $_SESSION['UserID'] . "'
        Where
                RecordID = '" . $recordID . "'
        ";
$successAry['Reject'] = $lcardstudentattend2->db_db_query($sql);

if (in_array(false, $successAry)) {
    $returnMsgKey = 'RecordRejectUnSuccess';
}
else {
    $returnMsgKey = 'RecordRejectSuccess';
}
intranet_closedb();

header('Location: list.php?returnMsgKey='.$returnMsgKey);
?>