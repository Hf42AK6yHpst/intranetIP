<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

### get parameters
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();

$recordID = $_GET['recordID']?$_GET['recordID']:$_POST['recordID'];

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}


### show return message
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];


### tab display
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ReprintCard";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

//SQL part
$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
$CardStatus ="CASE r.CardStatus
				   WHEN 0 THEN '".$Lang['StudentAttendance']['ReprintCard']['PendingConfirm']."'
				   WHEN 1 THEN '".$Lang['StudentAttendance']['ReprintCard']['PendingPayment']."'
				   WHEN 2 THEN '".$Lang['StudentAttendance']['ReprintCard']['AwaitingCardReprint']."'
				   WHEN 3 THEN '".$Lang['StudentAttendance']['ReprintCard']['Sent']."'
			       WHEN 4 THEN '".$Lang['StudentAttendance']['ReprintCard']['ApplicationCompleted']."'
				   WHEN 5 THEN '".$Lang['StudentAttendance']['ReprintCard']['Rejected']."'
				   WHEN 6 THEN '".$Lang['StudentAttendance']['ReprintCard']['Cancelled']."'
			   END";

$ShippingMethod = "CASE r.DeliveryMethod
                        WHEN 1 THEN '".$Lang['StudentAttendance']['ReprintCard']['PickUp']."'
                        WHEN 2 THEN '".$Lang['StudentAttendance']['ReprintCard']['CourierToApplicant']."'
                   END";

$PaymentMethod = "CASE r.PaymentMethod
                        WHEN 'tng' THEN '".$Lang['StudentAttendance']['ReprintCard']['TNG']."'
                        WHEN 'alipay' THEN '".$Lang['StudentAttendance']['ReprintCard']['ALIPAY']."'
                   END";

$PaymentItem = "CASE r.PaymentItem
                        WHEN 'REPRINTCARD' THEN '".$Lang['StudentAttendance']['ReprintCard']['ReprintCardFee']."'
                   END";

$sql = "Select
                r.PhotoPath,
                iu.ChineseName as StudentNameChi,
                iu.EnglishName as StudentNameEng,
                $classNameField as ClassName, 
                ycu.ClassNumber, 
                r.ReprintCardReason,
                $ShippingMethod as ShippingMethod,
                r.ApplyDate,
                $CardStatus as CardStatus,
                r.StudentSTRN,
                r.StudentBarcode,
                r.StudentSociety,
                r.StudentPPSAccountNo,
                r.IssueDate,
                r.ExpiryDate,
                $PaymentMethod as PaymentMethod,
                $PaymentItem as PaymentItem,
                r.PaymentAmountFee,
                r.PaymentDate,
                r.ReferenceNumber,
                r.CardStatus as CardStatusType,
                r.YearName1,
                r.YearName2,
                r.YearName3,
                r.YearName4,
                r.YearName5,
                r.YearName6,
                r.YearName7,
                r.YearName8,
                r.YearName9,
                r.YearName10,
                r.YearName11,
                r.YearName12,
                r.ClassName1,
                r.ClassName2,
                r.ClassName3,
                r.ClassName4,
                r.ClassName5,
                r.ClassName6,
                r.ClassName7,
                r.ClassName8,
                r.ClassName9,
                r.ClassName10,
                r.ClassName11,
                r.ClassName12,
                r.TeacherRemark,
                r.StudentID
        From 
                REPRINT_CARD_RECORD as r
                Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
                Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
                Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
		Where
			    r.RecordID = '" . $recordID . "'
		        And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		";
$result = $lcardstudentattend2->returnArray ( $sql );

### settings table
$x .= '<table class="form_table_v30">'."\r\n";
//student photo
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['StudentPhoto'].'</td>'."\r\n";
$x .= '<td><img src="'.$result[0]['PhotoPath'].'" border="0" align="absmiddle" width="100px" /><input type="hidden" name="imagePath" id="imagePath" value="'.$result[0]['PhotoPath'].'"></td>'."\r\n";
$x .= '</tr>'."\r\n";

//student name chi
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['General']['StudentNameChi'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentNameChi'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student name eng
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['General']['StudentNameEng'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentNameEng'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student class
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['General']['Class'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ClassName'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student class number
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ClassNumber'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ClassNumber'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student id
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['StudentID'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentSTRN'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student barcode
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['AccountMgmt']['Barcode'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentBarcode'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student Society
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Society'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentSociety'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student PPS
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['PPS'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentPPSAccountNo'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

// YEAR 12

$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Year(1-12)'].'</td>'."\r\n";
$x .= '<td><table>
       <tr><td style="border:0px; width:50px">1</td>
       <td style="border:0px; width:50px">2</td>
       <td style="border:0px; width:50px">3</td>
       <td style="border:0px; width:50px">4</td>
       <td style="border:0px; width:50px">5</td>
       <td style="border:0px; width:50px">6</td>
       <td style="border:0px; width:50px">7</td>
       <td style="border:0px; width:50px">8</td>
       <td style="border:0px; width:50px">9</td>
       <td style="border:0px; width:50px">10</td>
       <td style="border:0px; width:50px">11</td>
       <td style="border:0px; width:50px">12</td></tr>
       <tr><td style="border:0px">'.$result[0]['YearName1'].'</td>
       <td style="border:0px">'.$result[0]['YearName2'].'</td>
       <td style="border:0px">'.$result[0]['YearName3'].'</td>
       <td style="border:0px">'.$result[0]['YearName4'].'</td>
       <td style="border:0px">'.$result[0]['YearName5'].'</td>
       <td style="border:0px">'.$result[0]['YearName6'].'</td>
       <td style="border:0px">'.$result[0]['YearName7'].'</td>
       <td style="border:0px">'.$result[0]['YearName8'].'</td>
       <td style="border:0px">'.$result[0]['YearName9'].'</td>
       <td style="border:0px">'.$result[0]['YearName10'].'</td>
       <td style="border:0px">'.$result[0]['YearName11'].'</td>
       <td style="border:0px">'.$result[0]['YearName12'].'</td></tr></table></td>'."\r\n";
$x .= '</tr>'."\r\n";

// CLASS 12

$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Class(1-12)'].'</td>'."\r\n";
$x .= '<td><table>
       <tr><td style="border:0px; width:50px">1</td>
       <td style="border:0px;width:50px">2</td>
       <td style="border:0px;width:50px">3</td>
       <td style="border:0px;width:50px">4</td>
       <td style="border:0px;width:50px">5</td>
       <td style="border:0px;width:50px">6</td>
       <td style="border:0px;width:50px">7</td>
       <td style="border:0px;width:50px">8</td>
       <td style="border:0px;width:50px">9</td>
       <td style="border:0px;width:50px">10</td>
       <td style="border:0px;width:50px">11</td>
       <td style="border:0px;width:50px">12</td></tr>
       <tr><td style="border:0px">'.$result[0]['ClassName1'].'</td>
       <td style="border:0px">'.$result[0]['ClassName2'].'</td>
       <td style="border:0px">'.$result[0]['ClassName3'].'</td>
       <td style="border:0px">'.$result[0]['ClassName4'].'</td>
       <td style="border:0px">'.$result[0]['ClassName5'].'</td>
       <td style="border:0px">'.$result[0]['ClassName6'].'</td>
       <td style="border:0px">'.$result[0]['ClassName7'].'</td>
       <td style="border:0px">'.$result[0]['ClassName8'].'</td>
       <td style="border:0px">'.$result[0]['ClassName9'].'</td>
       <td style="border:0px">'.$result[0]['ClassName10'].'</td>
       <td style="border:0px">'.$result[0]['ClassName11'].'</td>
       <td style="border:0px">'.$result[0]['ClassName12'].'</td></tr></table></td>'."\r\n";
$x .= '</tr>'."\r\n";$x .= '</tr>'."\r\n";

//Issue Date
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['IssueDate'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['IssueDate'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Expiry Date
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ExpiryDate'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ExpiryDate'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Replacement Reason
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ReplacementReason'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ReprintCardReason'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Delivery Method
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['DeliveryMethod'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ShippingMethod'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Apply Date
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ApplyDate'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ApplyDate'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

##show payment relate
if($result[0]['CardStatusType']==2||$result[0]['CardStatusType']==3||$result[0]['CardStatusType']==4){

    // Payment method
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['PaymentMethod'].'</td>'."\r\n";
    $x .= '<td>'.$result[0]['PaymentMethod'].'</td>'."\r\n";
    $x .= '</tr>'."\r\n";

    // Payment Item
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['PaymentItem'].'</td>'."\r\n";
    $x .= '<td>'.$result[0]['PaymentItem'].'</td>'."\r\n";
    $x .= '</tr>'."\r\n";

    // Payment Amount
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['PaymentAmount'].'</td>'."\r\n";
    $x .= '<td>$'.$result[0]['PaymentAmountFee'].'</td>'."\r\n";
    $x .= '</tr>'."\r\n";

    // Payment Date
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['PaymentDate'].'</td>'."\r\n";
    $x .= '<td>'.$result[0]['PaymentDate'].'</td>'."\r\n";
    $x .= '</tr>'."\r\n";

    // Reference Number
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ReferenceNumber'].'</td>'."\r\n";
    $x .= '<td>'.$result[0]['ReferenceNumber'].'</td>'."\r\n";
    $x .= '</tr>'."\r\n";
}

//Status
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Status'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['CardStatus'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Teacher Remark
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['TeacherRemark'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['TeacherRemark'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .= '</table>'."\r\n";
$htmlAry['contentTable'] = $x;

### button
$htmlAry['editButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'goEdit();');
$htmlAry['approveButton'] = $linterface->GET_ACTION_BTN($Lang['StudentAttendance']['ReprintCard']['ApproveAndNext'], 'button', 'goApprove();');
$htmlAry['rejectButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Reject'], 'button', 'goReject();');
$htmlAry['cancelButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'goCancel();');
$htmlAry['backButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', 'goCancel();');
$htmlAry['insertButton'] = $linterface->GET_ACTION_BTN($Lang['StudentAttendance']['ReprintCard']['InsertSmartCardID'], 'button', 'goInsert();');
?>
    <script type="text/javascript">
        $(document).ready( function() {

        });

        function goEdit() {
            $('form#form1').attr('action', 'content_edit.php').submit();
        }

        function goInsert() {
            $('form#form1').attr('action', 'smartcardID_insert.php').submit();
        }

        function goReject() {
            var rejectReason = document.getElementById('rejectReason');
            rejectReason.value = prompt("<?=$Lang['StudentAttendance']['ReprintCard']['PleaseInsertRejectReason']?>", "");
            $('form#form1').attr('action', 'reject.php').submit();
        }

        function goApprove() {

            var imagePath = document.getElementById('imagePath').value;

            if(imagePath==""){
                alert("<?=$Lang['StudentAttendance']['ReprintCard']['PleaseUploadStudentPhoto']?>");
            }else{
                $('form#form1').attr('action', 'approve.php').submit();
            }
        }

        function goCancel() {
            $('form#form1').attr('action', 'list.php').submit();
        }

    </script>
    <form name="form1" id="form1" method="POST">
        <div class="table_board">
            <?=$htmlAry['contentTable']?>
        </div>
        <br />

        <div class="edit_bottom">
            <p class="spacer"></p>
             <?php if($result[0]['CardStatusType']==0){?>
                <?=$htmlAry['editButton']?>
                <?=$htmlAry['approveButton']?>
                <?=$htmlAry['rejectButton']?>
                <?=$htmlAry['cancelButton']?>
            <?php }else if($result[0]['CardStatusType']==3){?>
                 <?=$htmlAry['insertButton']?>
                 <?=$htmlAry['cancelButton']?>
            <?php }else{?>
                <?=$htmlAry['backButton']?>
            <?php }?>
            <p class="spacer"></p>
        </div>
        <input type="hidden" name="recordID" id="recordID" value="<?=$recordID?>">
        <input type="hidden" name="studentID" id="studentID" value="<?=$result[0]['StudentID']?>">
        <input type="hidden" name="rejectReason" id="rejectReason" value="">
    </form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>