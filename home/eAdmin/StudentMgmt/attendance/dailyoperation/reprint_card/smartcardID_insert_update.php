<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_reprintCard.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/eClassAppConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libAES.php");
intranet_auth();
intranet_opendb();

$lcardstudentattend2 = new libcardstudentattend2();
$lfs = new libfilesystem ();
$libreprintcard = new libeClassApp_reprintCard ();

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}

$recordID = $_POST['recordID'];
$studentID = $_POST['studentID'];
$SmartCardID = $_POST['SmartCardID'];

$successAry = array();

$sql = "Update 
                INTRANET_USER
        Set
                CardID = '" . $SmartCardID . "',
                DateModified = now(),
                ModifyBy = '" . $_SESSION['UserID'] . "'
        Where
                UserID = '" . $studentID . "'
        ";

$successAry['UpdateSmartCardID'] = $lcardstudentattend2->db_db_query($sql);


$sql1 = "Update 
                REPRINT_CARD_RECORD
        Set
                CardStatus = '4',
                ModifiedDate = now(),
                ModifiedBy = '" . $_SESSION['UserID'] . "'
        Where
                RecordID = '" . $recordID . "'
        ";

$successAry['UpdateCardStatus'] = $lcardstudentattend2->db_db_query($sql1);

if($successAry['UpdateCardStatus']){
    sendCompleteStatusToMIS($recordID);
}

if (in_array(false, $successAry)) {
    $returnMsgKey = 'UpdateUnsuccess';
}
else {
    $returnMsgKey = 'UpdateSuccess';
}

function sendCompleteStatusToMIS($RecordID)
{
    global $eclassAppConfig;

    $ldb = new libdb();
    $lauth = new libeclassapiauth();
    $laes = new libAES($eclassAppConfig['aesKey_reprintCard']);
    $jsonObj = new JSON_obj();

    // get the access key of eClass Store
    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module = 'eClassStore' AND SettingName = 'AccessKey'";
    $settingAry = $ldb->returnResultSet($sql);
    $accessKey = $settingAry[0]['SettingValue'];

    // get IP/EJ platform API key
    $apiKey = $lauth->GetAPIKeyByProject('IP/EJ platform');

    // prepare API data
    $orderRecordId = $RecordID;

    $dataAry['eClassRequest']['RequestMethod'] = 'CompleteReprintCardOrder';
    $dataAry['eClassRequest']['APIKey'] = $apiKey;
    $dataAry['eClassRequest']['Request']['AccessKey'] = $accessKey;
    $dataAry['eClassRequest']['Request']['IntranetOrderRecordID'] = $orderRecordId;

    $jsonPlainText = $jsonObj->encode($dataAry);
    $jsonPostAry['eClassRequestEncrypted'] = $laes->encrypt($jsonPlainText);
    $jsonPostText = $jsonObj->encode($jsonPostAry);


    $headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
    $centralServerUrl = $eclassAppConfig['reprintCard']['apiPath'];

    session_write_close();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonPostText);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    $responseJson = curl_exec($ch);
    curl_close($ch);

    $responseJson = standardizeFormPostValue($responseJson);
    $responseJsonAry = $jsonObj->decode($responseJson);

    $reponseJsonEncryptedText = $responseJsonAry['eClassResponseEncrypted'];
    $responseJsonDecryptedText = $laes->decrypt($reponseJsonEncryptedText);
    if ($responseJsonDecryptedText != '') {
        $responseJsonDecryptedAry = $jsonObj->decode($responseJsonDecryptedText);
    }

    $returnAry ['MISresponseArray'] = $responseJsonDecryptedAry;
}

intranet_closedb();

header('Location: content_list.php?recordID='.$recordID.'&returnMsgKey='.$returnMsgKey);
?>