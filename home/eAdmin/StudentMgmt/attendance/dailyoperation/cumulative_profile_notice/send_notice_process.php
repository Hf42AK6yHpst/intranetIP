<?php
// Modifying by: kenneth chung

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetUsers = $_REQUEST['StudentID'];
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$TotalCount = $_REQUEST['Count'];
$attendance_type = $_REQUEST['attendance_type'];
$TemplateID = $_REQUEST['TemplateID'];
$AdditionalInfo = $_REQUEST['AdditionalInfo'];

$StudentAttend = new libcardstudentattend2();
$lnotice = new libnotice();
$lucc = new libucc();

$TemplateDetail = $StudentAttend->Get_Template_Detail($TemplateID);

$template_category = $TemplateDetail[0]['CategoryID'];
$SpecialData = array();
$IssuerInfo = $StudentAttend->Get_UserInfo_By_ID(array($_SESSION['UserID']));
$IssueUserName = $IssuerInfo[0]['Name'];
$IssueUserID = $_SESSION['UserID'];
$NoticeNumber = time();
$TargetRecipientType = "U";
$RecordType = NOTICE_TO_SOME_STUDENTS;
$UserInfo = $StudentAttend->Get_UserInfo_By_ID($TargetUsers);

for ($i=0; $i< sizeof($UserInfo); $i++) {
	$RecipientID = array($UserInfo[$i]['UserID']);
	$data['StudentName'] = $UserInfo[$i]['Name'];
	$data['ClassName'] = $UserInfo[$i]['ClassName'];
	$data['ClassNumber'] = $UserInfo[$i]['ClassNumber'];
	$data['AdditionalInfo'] = $AdditionalInfo;
	$data['FromDate'] = $StartDate;
	$data['ToDate'] = $EndDate;
	$data['TotalCount'] = $TotalCount[$UserInfo[$i]['UserID']];
	$data['AttendanceType'] = ($attendance_type == PROFILE_TYPE_ABSENT)? $Lang['StudentAttendance']['Absent']:$Lang['StudentAttendance']['Late'];
	if (!$lnotice->disabled)
	{				
		$ReturnArr = $lucc->setNoticeParameter('StudentAttendance',$NoticeNumber,$TemplateID,
																						$data,$IssueUserID,$IssueUserName,$TargetRecipientType,
																						$RecipientID,$RecordType);
		$NoticeID = $lucc->sendNotice();
	}
}

header("Location: index.php?Msg=".urlencode($Lang['StudentAttendance']['eNotice']['NoticeSentSuccess']));
intranet_closedb();
?>