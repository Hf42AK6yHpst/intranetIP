<?php
//editing by 
/******************* Changes ***********************
 * 2019-07-02 (Ray): Add print, csv format
 * 2017-01-12 (Carlos): Fix to join CARD_STUDENT_DAILY_LOG_YYYY_MM get absent/late records only, no outing records.
 * 2011-07-25 (Carlos): Fix join condition on CARD_STUDENT_PROFILE_RECORD_REASON RecordStatus to be 0 or NULL which means not waived
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo "die";
	intranet_closedb();
	exit();
}

$CmpField='TotalCount';
$CmpField2='ClassName';
$CmpField3='ClassNumber';

function ___usort3($a,$b)
{
	global $CmpField, $CmpField2, $CmpField3;
	
	if(!is_numeric($a[$CmpField])||!is_numeric($b[$CmpField])) {
		if ($CmpField2 != '' && $a[$CmpField] == $b[$CmpField]) {
			if ($a[$CmpField2] == $b[$CmpField2]) {
				if($a[$CmpField3] == $b[$CmpField3]){
					return 0;
				}else{
					return round($a[$CmpField3]-$b[$CmpField3]);
				}
			}else{
				return strcasecmp($a[$CmpField2],$b[$CmpField2]);
			}
		}
		else {
			return -round($a[$CmpField] - $b[$CmpField]);
		}
	}
	
	if ($a[$CmpField] == $b[$CmpField]) {
		if ($CmpField2 != '') {
			if ($a[$CmpField2] == $b[$CmpField2]) {
				if($a[$CmpField3] == $b[$CmpField3]){
					$result = 0;
				}else{
					return ($a[$CmpField3]-$b[$CmpField3]);
				}
			}
			else {
				$result = strcasecmp($a[$CmpField2], $b[$CmpField2]);
			}
		}
		else {
			$result =  0;
		}
	}
	else
	{
		$result = -round($a[$CmpField] - $b[$CmpField]);
	}
	return $result;
}

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$attendance_type = $_REQUEST['attendance_type'];
$CountMoreThan = $_REQUEST['CountMoreThan'];
$Format = $_REQUEST['Format'];

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();
//$StudentAttendUI->retrieveSettings();

if($StartDate=="" || $EndDate==""){
	$ts = strtotime(date('Y-m-d'));
	$StartDate = getStartOfAcademicYear($ts);
	$EndDate = getEndOfAcademicYear($ts);
}

$absent_count_value = 1;
# Get Profile Count for Late/Absent/Earlyleave
if ($attendance_type == PROFILE_TYPE_ABSENT) {
	$absent_count_value = $StudentAttendUI->ProfileAttendCount==1?0.5:1;
	//$CountMoreThan = $CountMoreThan/$absent_count_value;
}

$username_field = getNameFieldByLang('a.');

$ts_start = strtotime($StartDate);
$ts_enddate = strtotime($EndDate);
$ts_start = strtotime(substr($StartDate,0,7)."-01");
$ts_end = strtotime(date("Y-m-t",$ts_enddate));

$final_result = array();
$temp_final_result = array();
$userIdToRowIndex = array();
for($ts_cur=$ts_start;$ts_cur<$ts_end;$ts_cur=strtotime("+1 month",$ts_cur))
{
	$year = date("Y",$ts_cur);
	$month = date("m",$ts_cur);
	$CARD_STUDENT_DAILY_LOG_TABLE = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
	
	$sql="
		SELECT 
			a.UserID,
			a.UserLogin,
			$username_field as StudentName,
			a.ClassName,
			a.ClassNumber, 
			sum(1) as TotalCount 
		FROM 
			INTRANET_USER AS a 
			inner join $CARD_STUDENT_DAILY_LOG_TABLE as t ON t.UserID=a.UserID 
			inner join CARD_STUDENT_PROFILE_RECORD_REASON AS c 
			on 
				t.UserID = c.StudentID AND c.RecordDate=DATE_FORMAT(CONCAT('$year-$month-',t.DayNumber),'%Y-%m-%d') 
				AND ((t.AMStatus=c.RecordType AND c.DayType=2) OR (t.PMStatus=c.RecordType AND c.DayType=3))  
				AND a.RecordType=2 
				AND a.RecordStatus IN(0,1,2) 
				AND (c.RecordStatus = 0 OR c.RecordStatus IS NULL)
				AND c.RecordType = '".$attendance_type."' 
				AND c.RecordDate between '".$StartDate." 00:00:00' and '".$EndDate." 23:59:59' ";
	if ($StudentAttendUI->EnableEntryLeavePeriod) {
		$sql .= "
			INNER JOIN 
			CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
			on 
				c.StudentID = selp.UserID 
				and 
				c.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
			";
	}
	$sql .= "
		group by 
			c.RecordID  
		ORDER BY TotalCount,a.ClassName,a.ClassNumber
	";
	//debug_r($sql);die;
	$temp_result = $StudentAttendUI->returnArray($sql);
	$temp_size = count($temp_result);
	//$final_result = array_merge($final_result, $temp_result);
	for($i=0;$i<$temp_size;$i++)
	{
		if(!isset($userIdToRowIndex[$temp_result[$i]['UserID']])){
			$temp_final_result[] = $temp_result[$i];
			$userIdToRowIndex[$temp_result[$i]['UserID']] = count($temp_final_result)-1;
		}else{
			$row_index = $userIdToRowIndex[$temp_result[$i]['UserID']];
			$temp_final_result[$row_index]['TotalCount'] += $temp_result[$i]['TotalCount'];
			$temp_final_result[$row_index][5] += $temp_result[$i][5];
		}
	}
}
for($i=count($temp_final_result)-1;$i>=0;$i--)
{
	$temp_final_result[$i]['TotalCount'] = $temp_final_result[$i]['TotalCount'] * $absent_count_value;
	$temp_final_result[$i][5] = $temp_final_result[$i][5] * $absent_count_value;
	if($temp_final_result[$i]['TotalCount'] > $CountMoreThan){
		$final_result[] = $temp_final_result[$i];
	}
}
usort($final_result,'___usort3');
##################### output ######################

switch($attendance_type){
    case CARD_STATUS_ABSENT : $attendance_type_name = $i_StudentAttendance_Status_Absent; break;
    case CARD_STATUS_LATE : $attendance_type_name = $i_StudentAttendance_Status_Late; break;
    default : $attendance_type_name = ''; break;
}
if($Format == 'csv') {
    $header = array();
    $header[] = array($i_general_startdate, $StartDate);
    $header[] = array($i_general_enddate, $EndDate);
    $header[] = array($i_Attendance_attendance_type, $attendance_type_name);
    $header[] = array($Lang['StudentAttendance']['ShowIfCountMoreThan'], $CountMoreThan);

    $rows = array();
    $rows[] = array($Lang['StudentAttendance']['ClassName'], $Lang['StudentAttendance']['ClassNumber'], $i_UserStudentName, $Lang['StudentAttendance']['TotalCount']);
    if (sizeof($final_result) > 0) {
        foreach ($final_result as $row) {
            List($StudentID, $UserLogin, $StudentName, $ClassName, $ClassNumber, $Total) = $row;
            $rows[] = array(
                $ClassName,
                $ClassNumber,
				$StudentName,
                $Total
            );
        }
    }

    $filename = "eAttendance_SendeNoticeToCumulativeAbsentLateReport.csv";
    $lexport = new libexporttext();
    $export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
    $lexport->EXPORT_FILE($filename, $export_content);
} else if($Format == 'print') {

    $table_attr = ' align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="96%" ';
    $th_attr = ' class="eSporttdborder eSportprinttabletitle" ';
    $td_attr = ' class="eSporttdborder eSportprinttext" ';

    $x = "";

    $x .= '<table width="96%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","print_button").'</td>
				</tr>
			</table>'."\n";
    $x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
             <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['Header']['Menu']['eAttednance'].' - '.$Lang['StudentAttendance']['CumulativeProfileNotice'].'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$i_general_startdate.' : '.$StartDate.'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$i_general_enddate.' : '.$EndDate.'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$i_Attendance_attendance_type.' : '.$attendance_type_name.'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['StudentAttendance']['ShowIfCountMoreThan'].' : '.$CountMoreThan.'</span></td>
			 </tr>

		   </table>
		   <p>&nbsp;</p>'."\n";



    $x .= '<table '.$table_attr.'>
                <tbody>
				  	<tr class="tabletop">
                        <th width="13%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassName'].'</th>
                        <th width="13%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassNumber'].'</th>
                        <th width="13%" nowrap="nowrap" '.$th_attr.'>'.$i_UserStudentName.'</th>
                        <th width="13%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['TotalCount'].'</th>
				    </tr>'."\n";

    if (sizeof($final_result) > 0) {
        foreach ($final_result as $row) {
            List($StudentID, $UserLogin, $StudentName, $ClassName, $ClassNumber, $Total) = $row;
            $x .= '<tr valign=\'top\'>';
            $x .= '<td nowrap="nowrap" ' . $td_attr . '>' . $ClassName . '</td>';
            $x .= '<td nowrap="nowrap" ' . $td_attr . '>' . $ClassNumber . '</td>';
			$x .= '<td nowrap="nowrap" ' . $td_attr . '>' . $StudentName . '</td>';
            $x .= '<td nowrap="nowrap" ' . $td_attr . '>' . $Total . '</td>';
            $x .= '</tr>' . "\n";
        }
    }

    $x .= '</tbody>';
    $x .= '</table><br />';

    include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

    echo $x;

    include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
} else {
// Get Template Selection
    $TemplateList = $StudentAttendUI->Get_eNotice_Template_By_Category("CumulativeProfile");
    $TemplateSelect = '<select name="TemplateID" id="TemplateID">';
    for ($i = 0; $i < sizeof($TemplateList); $i++) {
        List($TemplateID, $Title) = $TemplateList[$i];
        $TemplateSelect .= '<option value="' . $TemplateID . '">' . $Title . '</option>';
    }
    $TemplateSelect .= '</select>';
    $TemplateSelect .= '&nbsp;<a href="#" class="tablelink" target="_blank" onclick="newWindow(\'' . $PATH_WRT_ROOT . 'home/eAdmin/StudentMgmt/attendance/dailyoperation/absence/preview_notice.php?TemplateID=\'+document.getElementById(\'TemplateID\').options[document.getElementById(\'TemplateID\').selectedIndex].value,10); return false;">' . $Lang['StudentAttendance']['eNoticeTemplatePreview'] . '</a>';

// Get Additional Info
    include_once($PATH_WRT_ROOT . "templates/html_editor/fckeditor.php");
    $oFCKeditor = new FCKeditor('AdditionalInfo', "100%", "200", "", "");
    $Editor = $oFCKeditor->Create2();

# DISPLAY
    $display .= '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['eNoticeTemplateName'] . '</td>
							<td width="70%"class="tabletext">
								' . $TemplateSelect . '
							</td>
						</tr>
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['AdditionInfo'] . '</td>
							<td width="70%"class="tabletext">
								' . $Editor . '
							</td>
						</tr>
						</table>';

    $display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
				<tr class=\"tablebluetop\">";
    $display .= "<td class=\"tabletoplink\" width=\"10%\">" . $Lang['StudentAttendance']['ClassName'] . "</td>";
    $display .= "<td class=\"tabletoplink\" width=\"10%\">" . $Lang['StudentAttendance']['ClassNumber'] . "</td>";
	$display .= "<td class=\"tabletoplink\" width=\"25%\">" . $i_UserStudentName . "</td>";
    $display .= "<td class=\"tabletoplink\" width=\"54%\">" . $Lang['StudentAttendance']['TotalCount'] . "</td>";
    $display .= "<td class=\"tabletoplink\" width=\"1%\">
							<input type=\"checkbox\" name=\"SelectAll\" id=\"SelectAll\" onclick=\"$('.StudentCheckBox').attr('checked',this.checked);\">
						</td>";
    $display .= "</tr>";
    if (sizeof($final_result) > 0) {
        $css = "tablebluerow2";
        $j = 0;
        for ($i = 0; $i < sizeof($final_result); $i++) {
            List($StudentID, $UserLogin, $StudentName, $ClassName, $ClassNumber, $Total) = $final_result[$i];

            $css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
            $display .= "<tr class=\"$css\">";
            $display .= "<td class=\"tabletext\">" . $ClassName . "</td>";
            $display .= "<td class=\"tabletext\">" . $ClassNumber . "</td>";
			$display .= "<td class=\"tabletext\">" . $StudentName . "</td>";
            $display .= "<td class=\"tabletext\">" . $Total . "</td>";
            $display .= "<td class=\"tabletext\">
									<input type=\"checkbox\" name=\"StudentID[]\" id=\"StudentID[]\" class=\"StudentCheckBox\" value=\"" . $StudentID . "\">
									<input type=\"hidden\" name=\"Count[" . $StudentID . "]\" id=\"StudentID[" . $StudentID . "]\" class=\"StudentCheckBox\" value=\"" . $Total . "\">
								</td>";
            $display .= "</tr>";
        }
    } else {
        $display .= "<tr class=\"tablebluerow2\">";
        $display .= "<td class=\"tabletext\" colspan=\"5\" align=\"center\">" . $i_no_record_exists_msg . "</td>";
        $display .= "</tr>";
    }
    $display .= "</table>";

    $display .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
					  	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
					  </tr>
					</table>
					<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
						  <td align="center">
						     <!--   ' . $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "PrintPage(document.PrintExportForm); return false;", "print") . '
                                ' . $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "ExportPage(document.PrintExportForm); return false;", "export") . ' -->
								' . $linterface->GET_ACTION_BTN($Lang['StudentAttendance']['SendNotice'], "button", " Send_Notice();") . '
								' . $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", "", "reset2") . '
							</td>
						</tr>
					</table>
					<input type="hidden" name="StartDate" id="hStartDate" value="' . $StartDate . '">
					<input type="hidden" name="EndDate" id="hEndDate" value="' . $EndDate . '">
					<input type="hidden" name="attendance_type" id="hattendance_type" value="' . $attendance_type . '">
					';
    echo $display;
}

intranet_closedb();
?>