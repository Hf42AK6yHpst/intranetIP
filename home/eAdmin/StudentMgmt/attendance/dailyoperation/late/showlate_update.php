<?php
# using:

############################################## Change Log ############################################
# 2020-05-19 Ray
# - late & absent add InsertSubmittedAbsentLateRecord
# 2019-10-02 Cameron
# - should update eSchoolBus apply leave status to delete if original status is absent/outgoing
# 2019-02-01 Cameron
#  - don't sync apply leave record to eSchoolBus if the student is not taking school bus student.
# 2018-12-17 Cameron: sync absent & outgoing record to eSchoolBus
# 2018-11-15 Cameron: add hidden field routeID to return for eSchoolBus
# 2018-08-03 Carlos: [ip.2.5.9.10.1] Mark attendance records confirmed by admin.
# 2017-05-31 Carlos: [ip.2.5.8.7.1] Check and skip outdated record if attendance record last modified time is later than page load time. 
# 2017-02-24 Carlos: [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
# 2015-04-23 Carlos: Fix office remark array index shift problem after disable the input. 
# 2015-04-22 Carlos: Cater AM/PM modify by and date modified fields.
# 2015-02-11: Carlos - Update teacher's remark. Update office remark.
# 2012-06-13: Carlos - add/remove no card entrance record when status cahnge to absent / outing
# 2011-05-17: Henry Chow - add checking on status of eDis LATE category
# 2010-04-22: Carlos - Allow input late session if student is late
# 20100329: Kenneth Chung: Simplify the coding flow and remove duplicate flow
#	Date 	: 20100105 (By Ronald)
#	Details : if it is a late record, will detect is it a serious late record or not.
############################################ End of Change Log ########################################

### also need to update /cardapi/attendance/receiver.php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# class used
$LIDB = new libdb();

$lcardattend = new libcardstudentattend2();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

if($plugin['Disciplinev12']) {	# check whether eDis "Late" category is set to "Not In Use" , and with any GM Late record in Target Date

	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	
	$CanPassGmLateChecking = $ldiscipline->Check_Can_Pass_Gm_Late_Checking($TargetDate);
	
	if(!$CanPassGmLateChecking) {
		header("Location: showlate.php?DayType=".urlencode($DayType)."&TargetDate=".urlencode($TargetDate)."&order_by_time=".urlencode($order_by_time)."&Msg=".$Lang['General']['ReturnMessage']['UpdateUnsuccess']);
		exit;
	}
}
/*
if(sizeof($user_id) >0)
{
	# check page load time 
	if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
		$error = 1;  // data outdated
		
		$return_page = "showlate.php";
		
		$return_page = $return_page."?DayType=$DayType&TargetDate=$TargetDate&error=$error&Msg=".urlencode($Lang['StudentAttendance']['DataOutdatedWarning']);
		header("Location: $return_page");
		exit();	
	}

}
*/
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || ($DayType!=PROFILE_DAY_TYPE_AM && $DayType!=PROFILE_DAY_TYPE_PM) )
{
    header("Location: index.php");
    exit();
}

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

//$use_magic_quotes = get_magic_quotes_gpc();
$use_magic_quotes = $lcardattend->is_magic_quotes_active;

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
/*$year = getCurrentAcademicYear();
$semester = getCurrentSemester();*/

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$warning_student_ids = array();

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

$studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$user_id,'StudentIDToRecord'=>1));

for($i=0; $i<sizeOf($user_id); $i++)
{
  $my_user_id = $user_id[$i];
  $student_id = $my_user_id;
  $my_day = $txt_day;
  $my_drop_down_status = $drop_down_status[$i];
  $my_record_id = $record_id[$i];
  $LateTimeInput = $LateTime[$my_user_id];
  
	// check and skip outdated record if attendance record last modified time is later than page load time
	if(isset($studentIdToDailyLog[$my_user_id])){
		if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
		{
			$am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
			if($am_modified_ts >= $PageLoadTime) continue;
		}
		if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
		{
			$pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
			if($pm_modified_ts >= $PageLoadTime) continue;
		}
	}
  
  // Retrieve Waived only if late
  ($my_drop_down_status == CARD_STATUS_LATE || $my_drop_down_status == CARD_STATUS_ABSENT) ? ($my_record_status = (${"waived_".$my_user_id} == '') ? 0 : 1) : "";
	
	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$InSchoolTimeField = "InSchoolTime";
		$AttendanceArrayTime = "MorningTime";
		$StatusField = "AMStatus";
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
		$dayTypeStr = 'AM';
	}
	else {
		$lcardattend->retrieveSettings();
		if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
		{
			$InSchoolTimeField = "InSchoolTime";
			$AttendanceArrayTime = 'MorningTime';
		}
		else # retrieve LunchBackTime
		{
			$InSchoolTimeField = "LunchBackTime";
			$AttendanceArrayTime = 'LunchEnd';
		}
		$StatusField = "PMStatus";
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
		$dayTypeStr = 'PM';
	}
	
	if ($my_drop_down_status == CARD_STATUS_LATE)       # Late
  {
  	if ($LateTimeInput != "") {
  		$sql = 'Update '.$card_log_table_name.' set 
  							'.$InSchoolTimeField.' = \''.$LateTimeInput.'\', 
  							'.$DateModifiedField.' = NOW(), 
		            '.$ModifyByField.' = \''.$_SESSION['UserID'].'\' 
  						Where 
  							DayNumber = \''.$txt_day.'\'
								AND UserID = \''.$my_user_id.'\'
  					 ';
  		$Result['-1'] = $lcardattend->db_db_query($sql);
  		$InSchoolTime = $LateTimeInput;
  	}
  	else {
	    $inTimesql = "select 
						    			".$InSchoolTimeField." 
						    		from 
						    			$card_log_table_name
										WHERE 
											DayNumber = '$txt_day'
											AND UserID = '$my_user_id'";
	    $inTimeResult = $lcardattend->returnVector($inTimesql);
	    $InSchoolTime = $inTimeResult[0];
	  }

    # Reason input
    $txtReason = ${"reason$i"};
    $txtReason = trim(htmlspecialchars_decode(stripslashes($txtReason), ENT_QUOTES));
		
		// code added by kenneth
    $Result[$student_id."SetProfile"] = $lcardattend->Set_Profile($student_id,$TargetDate,$DayType,$my_drop_down_status,$txtReason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,($LateTimeInput != ""),$my_record_status);
    // end code added by kenneth


    if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
        $Result[$student_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($student_id, $TargetDate, $DayType,'','', PROFILE_TYPE_LATE , '','');
    }

    /*
    # Get ProfileRecordID
    $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                   WHERE RecordDate = '$TargetDate'
                         AND StudentID = '$my_user_id'
                         AND DayType = '".$DayType."'
                         AND RecordType = '".PROFILE_TYPE_LATE."'";
    $temp = $lcardattend->returnArray($sql,2);
    
    list($reason_record_id, $reason_profile_id) = $temp[0];

    if ($reason_record_id == "") {          # Reason record not exists
        # Search whether attendance exists by date, student and type
        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                       WHERE AttendanceDate = '$TargetDate'
                             AND UserID = '$my_user_id'
                             AND DayType = '".$DayType."'
                             AND RecordType = '".PROFILE_TYPE_LATE."'";
        $temp = $lcardattend->returnVector($sql);
        $attendance_id = $temp[0];
        
        if ($attendance_id == "")          # Record not exists
        {
          if ($my_record_status != 1) {
            # Insert profile record
            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
            $temp = $lcardattend->returnArray($sql,2);
            list ($user_classname, $user_classnum) = $temp[0];

            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
            $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".$DayType."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
            $Result['0'] = $lcardattend->db_db_query($sql);
            $attendance_id = $lcardattend->db_insert_id();
        	}
        	
          # Calculate upgrade items
          if ($plugin['Disciplinev12'] && $LateCtgInUsed){
            if($attendance_id) {
              $semInfo = getAcademicYearAndYearTermByDate($TargetDate);
              $acy = new academic_year_term($semInfo[0]);
              
              $dataAry = array();
							$dataAry['StudentID'] = $my_user_id;
							$dataAry['RecordDate'] = $TargetDate;
							//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
							$dataAry['Year'] = $acy->YearNameEN;
							$dataAry['Semester'] = $acy->YearTermNameEN;
							$dataAry['StudentAttendanceID'] = $attendance_id;
							$dataAry['AcademicYearID'] = $acy->AcademicYearID;
							$dataAry['YearTermID'] = $acy->YearTermID;
							$dataAry['Remark'] = $InSchoolTime;
	
							if($SeriousLateUsed == 1) 
							{
								if($InSchoolTime != "")
								{
									$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
									$school_start_time = $arr_time_table[$AttendanceArrayTime];
									
									$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
									$late_minutes = ceil($str_late_minutes/60);
											
									$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
									$min_late_minutes = $lcardattend->returnVector($sql);
									
									if(sizeof($min_late_minutes)>0)
									{
										$dataAry['SeriousLate'] = 1;
										$dataAry['LateMinutes'] = $late_minutes;
									}
									else
									{
										$dataAry['SeriousLate'] = 0;
									}
								}
								else
								{
									$dataAry['SeriousLate'] = 0;
								}
							}
							//debug("A : ");
							//debug_r($dataAry);
							$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);								
						}
          } 
          elseif ($plugin['Discipline'] && $LateCtgInUsed) {
						$t_date = $TargetDate;
						$s_id = $my_user_id;
                       
						if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
							$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
							$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
							if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
							{
							    $warning_student_ids[] = $student_id;
							}
							$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
						}
						else{
							$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
							$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
							if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
							{
							    $warning_student_ids[] = $student_id;
							}
							$ldiscipline->calculateUpgradeLateToDetention($student_id);
						}
          }
        }
        else
        {
          # [PROFILE_STUDENT_ATTENDANCE] exists
          if ($my_record_status == 1) {
						# Added by peter 2006/10/11
						# Reset Upgrade items
						if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
								$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
						}
						else if ($plugin['Discipline'] && $LateCtgInUsed)
						{
						   $t_date = $TargetDate;
						   $s_id = $my_user_id;
						   if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
						   $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
						   $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
						
						}
						
						   else{
						       $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
						       $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
						   }
						}
                   
            // Delete Record if waived
            $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
            $Result['1'] = $lcardattend->db_db_query($sql);
            $attendance_id = "";
            # added by peter 2006/10/11
            # Calculate upgrade items
						if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
							# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
						}
						else if ($plugin['Discipline'] && $LateCtgInUsed)
						{
							$t_date = $TargetDate;
							if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
								{
									$warning_student_ids[] = $s_id;
								}
								$ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
							}
							else{
								if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
								{
									$warning_student_ids[] = $s_id;
								}
								$ldiscipline->calculateUpgradeLateToDetention($s_id);
							}
						}
          }
          else {
          	if ($LateTimeInput != "") {
	          	// remove previous normal late missconduct record
							# Reset Upgrade items
							if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
									$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
							}
							else if ($plugin['Discipline'] && $LateCtgInUsed)
							{
							   $t_date = $TargetDate;
							   $s_id = $my_user_id;
							   if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								   $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
								   $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
								
								 }
							   else{
							       $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
							       $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
							   }
							}
							
							// recalculate the late category for the input time
							# Calculate upgrade items
		          if ($plugin['Disciplinev12'] && $LateCtgInUsed){
		            if($attendance_id) {
		              $semInfo = getAcademicYearAndYearTermByDate($TargetDate);
		              $acy = new academic_year_term($semInfo[0]);
		              
		              $dataAry = array();
									$dataAry['StudentID'] = $my_user_id;
									$dataAry['RecordDate'] = $TargetDate;
									//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
									$dataAry['Year'] = $acy->YearNameEN;
									$dataAry['Semester'] = $acy->YearTermNameEN;
									$dataAry['StudentAttendanceID'] = $attendance_id;
									$dataAry['AcademicYearID'] = $acy->AcademicYearID;
									$dataAry['YearTermID'] = $acy->YearTermID;
									$dataAry['Remark'] = $InSchoolTime;
			
									if($SeriousLateUsed == 1) 
									{
										if($InSchoolTime != "")
										{
											$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
											$school_start_time = $arr_time_table[$AttendanceArrayTime];
											
											$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
											$late_minutes = ceil($str_late_minutes/60);
													
											$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
											$min_late_minutes = $lcardattend->returnVector($sql);
											
											if(sizeof($min_late_minutes)>0)
											{
												$dataAry['SeriousLate'] = 1;
												$dataAry['LateMinutes'] = $late_minutes;
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									//debug("A : ");
									//debug_r($dataAry);
									$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);								
								}
		          } 
		          elseif ($plugin['Discipline'] && $LateCtgInUsed) {
								$t_date = $TargetDate;
								$s_id = $my_user_id;
		                       
								if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
									$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
									if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
									{
									    $warning_student_ids[] = $student_id;
									}
									$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
								}
								else{
									$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
									if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
									{
									    $warning_student_ids[] = $student_id;
									}
									$ldiscipline->calculateUpgradeLateToDetention($student_id);
								}
		          }
						}
						# Update Reason in profile record By AttendanceID
						$sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason' WHERE StudentAttendanceID = '$attendance_id'";
						$Result['2'] = $lcardattend->db_db_query($sql);
          }
        }

        # added on 2007-04-30
        # remove previous absent record from Reason table ( if exists ) 
        $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".$DayType."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
        $Result['3'] = $lcardattend->db_db_query($sql);
        
        # added on 2007-07-10
        # remove previous absent record from PROFILE table ( if exists ) 
        $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".$DayType."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
        $Result['4'] = $lcardattend->db_db_query($sql);

        
        # Insert to Reason table
        $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
        $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_LATE."', '".$DayType."', '$my_record_status', now(), now() ";
        if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
        {
        	$no_late_session = $late_session[$i];
        	$fieldname .= ",AbsentSession ";
        	$fieldsvalues .= ",'$no_late_session' ";
        }
        $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                       VALUES ($fieldsvalues)";
        $Result['5'] = $lcardattend->db_db_query($sql);
    }
    else  # Reason record exists
    {
	    if ($reason_profile_id == "")    # Profile ID not exists
	    {
        # Search whether attendance exists by date, student and type
        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                       WHERE AttendanceDate = '$TargetDate'
                             AND UserID = '$my_user_id'
                             AND DayType = '".$DayType."'
                             AND RecordType = '".PROFILE_TYPE_LATE."'";
        $temp = $lcardattend->returnVector($sql);
        $attendance_id = $temp[0];

        if ($attendance_id == "")          # Record not exists
        {
					if ($my_record_status != 1)	# not waive
					{
				    # Insert profile record
				    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
				    $temp = $lcardattend->returnArray($sql,2);
				    list ($user_classname, $user_classnum) = $temp[0];
				    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
				    $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".$DayType."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
				    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
				    $Result['6'] = $lcardattend->db_db_query($sql);
				    $attendance_id = $lcardattend->db_insert_id();
					}
					
					# Calculate upgrade items
					if ($plugin['Disciplinev12'] && $LateCtgInUsed)
					{
						if($attendance_id)
						{
						$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
					  $acy = new academic_year_term($semInfo[0]);
					  
					  $dataAry = array();
						$dataAry['StudentID'] = $my_user_id;
						$dataAry['RecordDate'] = $TargetDate;
						$dataAry['Year'] = $acy->YearNameEN;
						$dataAry['Semester'] = $acy->YearTermNameEN;
						$dataAry['StudentAttendanceID'] = $attendance_id;
						$dataAry['AcademicYearID'] = $acy->AcademicYearID;
						$dataAry['YearTermID'] = $acy->YearTermID;
						$dataAry['Remark'] = $InSchoolTime;
						
						if($SeriousLateUsed == 1) {
							if($InSchoolTime != "")
							{
								$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
								$school_start_time = $arr_time_table[$AttendanceArrayTime];
								
								$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
								$late_minutes = ceil($str_late_minutes/60);
								
								$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
								$min_late_minutes = $lcardattend->returnVector($sql);		
								
								if(sizeof($min_late_minutes)>0)
								{
									$dataAry['SeriousLate'] = 1;
									$dataAry['LateMinutes'] = $late_minutes;
								}
								else
								{
									$dataAry['SeriousLate'] = 0;
								}
							}
							else
							{
								$dataAry['SeriousLate'] = 0;
							}
						}
						//debug("B : ");
						//debug_r($dataAry);
						$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
						}
					} 
					else if ($plugin['Discipline'] && $LateCtgInUsed)
					{
				    $t_date = $TargetDate;
				    $s_id = $my_user_id;
						if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
							$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
							$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
							if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
							{
								$warning_student_ids[] = $student_id;
							}
							$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
						}                                    
						else{                                                             
							$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
							$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
				      if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
				      {
				          $warning_student_ids[] = $student_id;
				      }
						  $ldiscipline->calculateUpgradeLateToDetention($student_id);
						}
					}
        }
        else
        {
          if ($my_record_status == 1)	# waive
          {
						# Added by peter 2006/10/11
						# Reset Upgrade items
						if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
							$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
						}
						else if ($plugin['Discipline'] && $LateCtgInUsed)
						{
							$t_date = $TargetDate;
							$s_id = $my_user_id;
							if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
								$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
							}else{
								$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
								$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
							}
						}
                                           
            // Delete Record if waived
            $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
            $Result['7'] = $lcardattend->db_db_query($sql);
            $attendance_id = "";
            # added by peter 2006/10/11
            # Calculate upgrade items
            if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
							# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
						}
        		else if ($plugin['Discipline'] && $LateCtgInUsed)
						{
						  $t_date = $TargetDate;
							if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
								{
									$warning_student_ids[] = $s_id;
								}
								$ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
							}else{
								if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
								{
									$warning_student_ids[] = $s_id;
								}
							  $ldiscipline->calculateUpgradeLateToDetention($s_id);
							}
						}
          }
          else
          {
          	if ($LateTimeInput != "") {
	          	// remove previous normal late missconduct record
							# Reset Upgrade items
							if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
									$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
							}
							else if ($plugin['Discipline'] && $LateCtgInUsed)
							{
							   $t_date = $TargetDate;
							   $s_id = $my_user_id;
							   if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								   $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
								   $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
								
								 }
							   else{
							       $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
							       $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
							   }
							}
							
							// recalculate the late category for the input time
							# Calculate upgrade items
		          if ($plugin['Disciplinev12'] && $LateCtgInUsed){
		            if($attendance_id) {
		              $semInfo = getAcademicYearAndYearTermByDate($TargetDate);
		              $acy = new academic_year_term($semInfo[0]);
		              
		              $dataAry = array();
									$dataAry['StudentID'] = $my_user_id;
									$dataAry['RecordDate'] = $TargetDate;
									//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
									$dataAry['Year'] = $acy->YearNameEN;
									$dataAry['Semester'] = $acy->YearTermNameEN;
									$dataAry['StudentAttendanceID'] = $attendance_id;
									$dataAry['AcademicYearID'] = $acy->AcademicYearID;
									$dataAry['YearTermID'] = $acy->YearTermID;
									$dataAry['Remark'] = $InSchoolTime;
			
									if($SeriousLateUsed == 1) 
									{
										if($InSchoolTime != "")
										{
											$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
											$school_start_time = $arr_time_table[$AttendanceArrayTime];
											
											$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
											$late_minutes = ceil($str_late_minutes/60);
													
											$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
											$min_late_minutes = $lcardattend->returnVector($sql);
											
											if(sizeof($min_late_minutes)>0)
											{
												$dataAry['SeriousLate'] = 1;
												$dataAry['LateMinutes'] = $late_minutes;
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									//debug("A : ");
									//debug_r($dataAry);
									$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);								
								}
		          } 
		          elseif ($plugin['Discipline'] && $LateCtgInUsed) {
								$t_date = $TargetDate;
								$s_id = $my_user_id;
		                       
								if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
									$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
									if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
									{
									    $warning_student_ids[] = $student_id;
									}
									$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
								}
								else{
									$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
									if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
									{
									    $warning_student_ids[] = $student_id;
									}
									$ldiscipline->calculateUpgradeLateToDetention($student_id);
								}
		          }
						}
						# Update Reason in profile record By AttendanceID
						$sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason' WHERE StudentAttendanceID = '$attendance_id'";
						$Result['8'] = $lcardattend->db_db_query($sql);
          }
        }
	    }
	    else  # Has Profile ID
	    {
	      # Search Attendance By ID
	      $sql = "SELECT StudentAttendanceID, RecordType FROM PROFILE_STUDENT_ATTENDANCE
	                     WHERE StudentAttendanceID = '$reason_profile_id'";
	      $temp = $lcardattend->returnArray($sql);
	      list($attendance_id, $ori_record_type) = $temp[0];
	      //$attendance_id = $temp[0];
	      
	      if ($attendance_id == "")          # Record not exists
	      {
          # Search attendance by date, student and type
          $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                         WHERE AttendanceDate = '$TargetDate'
                               AND UserID = '$my_user_id'
                               AND DayType = '".$DayType."'
                               AND RecordType = '".PROFILE_TYPE_LATE."'";
          $temp = $lcardattend->returnVector($sql);
          $attendance_id = $temp[0];
	
          if ($attendance_id == "")          # Record not exists
          {
            if ($my_record_status != 1)	# not waive
            {
              # insert reason in profile record
              $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
              $temp = $lcardattend->returnArray($sql,2);
              list ($user_classname, $user_classnum) = $temp[0];
              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
              $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_LATE."','".$DayType."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
              $Result['9'] = $lcardattend->db_db_query($sql);
              $attendance_id = $lcardattend->db_insert_id();
        		}

	          # Calculate upgrade items
	          if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	          {
							if($attendance_id)
							{
								$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
								$acy = new academic_year_term($semInfo[0]);
								
								$dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $acy->YearNameEN;
								$dataAry['Semester'] = $acy->YearTermNameEN;
								$dataAry['StudentAttendanceID'] = $attendance_id;
								$dataAry['AcademicYearID'] = $acy->AcademicYearID;
								$dataAry['YearTermID'] = $acy->YearTermID;
								$dataAry['Remark'] = $InSchoolTime;
							
								if($SeriousLateUsed == 1) {
									if($InSchoolTime != "")
									{
										$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
										$school_start_time = $arr_time_table[$AttendanceArrayTime];
										
										$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
										$late_minutes = ceil($str_late_minutes/60);
										
										$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
										$min_late_minutes = $lcardattend->returnVector($sql);		
										
										if(sizeof($min_late_minutes)>0)
										{
											$dataAry['SeriousLate'] = 1;
											$dataAry['LateMinutes'] = $late_minutes;
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									else
									{
										$dataAry['SeriousLate'] = 0;
									}
								}
								//debug("C : ");
								//debug_r($dataAry);
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
							}
	          } 
	          else if ($plugin['Discipline'] && $LateCtgInUsed)
	          {
							$t_date = $TargetDate;
							$s_id = $my_user_id;
	               
							if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
								$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
								if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
								{
								  $warning_student_ids[] = $s_id;
								}
								$ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
							}
							else{
								$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
								$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
							  if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
							  {
							  	$warning_student_ids[] = $student_id;
							  }
							  $ldiscipline->calculateUpgradeLateToDetention($student_id);
							}
	          }
          }
          else
          {
            if ($my_record_status == 1)	# waive
            {
							# Added by peter 2006/10/11
							# Reset Upgrade items
							if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
								$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
							}
							else if ($plugin['Discipline'] && $LateCtgInUsed)
							{
								$t_date = $TargetDate;
								$s_id = $my_user_id;
								if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
									$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
								}else{
									$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
								}
							}
							// Delete Record if waived
              $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
              $Result['10'] = $lcardattend->db_db_query($sql);
              $attendance_id = NULL;
              # added by peter 2006/10/11
							# Calculate upgrade items
							if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
								# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
							}
							else if ($plugin['Discipline'] && $LateCtgInUsed)
							{
							  $t_date = $TargetDate;
								if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
									if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
									{
										$warning_student_ids[] = $s_id;
									}
									$ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
								}else{
									if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
									{
										$warning_student_ids[] = $s_id;
									}
									$ldiscipline->calculateUpgradeLateToDetention($s_id);
								}
							}
            }
            else
            {
	            if ($LateTimeInput != "") {
		          	// remove previous normal late missconduct record
								# Reset Upgrade items
								if ($plugin['Disciplinev12'] && $LateCtgInUsed)
								{
										$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
								}
								else if ($plugin['Discipline'] && $LateCtgInUsed)
								{
								   $t_date = $TargetDate;
								   $s_id = $my_user_id;
								   if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
									   $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
									   $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
									
									 }
								   else{
								       $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
								       $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
								   }
								}
								
								// recalculate the late category for the input time
								# Calculate upgrade items
			          if ($plugin['Disciplinev12'] && $LateCtgInUsed){
			            if($attendance_id) {
			              $semInfo = getAcademicYearAndYearTermByDate($TargetDate);
			              $acy = new academic_year_term($semInfo[0]);
			              
			              $dataAry = array();
										$dataAry['StudentID'] = $my_user_id;
										$dataAry['RecordDate'] = $TargetDate;
										//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
										$dataAry['Year'] = $acy->YearNameEN;
										$dataAry['Semester'] = $acy->YearTermNameEN;
										$dataAry['StudentAttendanceID'] = $attendance_id;
										$dataAry['AcademicYearID'] = $acy->AcademicYearID;
										$dataAry['YearTermID'] = $acy->YearTermID;
										$dataAry['Remark'] = $InSchoolTime;
				
										if($SeriousLateUsed == 1) 
										{
											if($InSchoolTime != "")
											{
												$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
												$school_start_time = $arr_time_table[$AttendanceArrayTime];
												
												$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
												$late_minutes = ceil($str_late_minutes/60);
														
												$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
												$min_late_minutes = $lcardattend->returnVector($sql);
												
												if(sizeof($min_late_minutes)>0)
												{
													$dataAry['SeriousLate'] = 1;
													$dataAry['LateMinutes'] = $late_minutes;
												}
												else
												{
													$dataAry['SeriousLate'] = 0;
												}
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										//debug("A : ");
										//debug_r($dataAry);
										$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);								
									}
			          } 
			          elseif ($plugin['Discipline'] && $LateCtgInUsed) {
									$t_date = $TargetDate;
									$s_id = $my_user_id;
			                       
									if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
										$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
										$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
										if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
										{
										    $warning_student_ids[] = $student_id;
										}
										$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
									}
									else{
										$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
										$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
										if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
										{
										    $warning_student_ids[] = $student_id;
										}
										$ldiscipline->calculateUpgradeLateToDetention($student_id);
									}
			          }
							}
              # Update Reason in profile record By AttendanceID
              $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                     WHERE StudentAttendanceID = '$attendance_id'";
              $Result['11'] = $lcardattend->db_db_query($sql);
            }
          }
	      }
	      else # profile record exists and valid
	      {
          if ($my_record_status == 1)		# waive late record
          {
						# Added by peter 2006/10/11
						# Reset Upgrade items
						if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
						 # delete late record and misconduct record
						 $ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
						}
						else if ($plugin['Discipline'] && $LateCtgInUsed)
						{
						   $t_date = $TargetDate;
						   $s_id = $my_user_id;
						  if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
						   $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
						   $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
						
						}
						   else{
						           $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
						           $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
						   }
						}
						
            // Delete Record if waived
            $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
            $Result['12'] = $lcardattend->db_db_query($sql);
            $attendance_id = NULL;
						# added by peter 2006/10/11
						# Calculate upgrade items
						if ($plugin['Disciplinev12'] && $LateCtgInUsed)
						{
						 # calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
						}
						else if ($plugin['Discipline'] && $LateCtgInUsed)
						{
						   $t_date = $TargetDate;
						     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
						if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
						{
						  $warning_student_ids[] = $s_id;
						}
						$ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
						
						}
						   else{
						           if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
						           {
						               $warning_student_ids[] = $s_id;
						           }
						           $ldiscipline->calculateUpgradeLateToDetention($s_id);
						   }
						}
          }
          else
          {
          	if ($LateTimeInput != "") {
	          	// remove previous normal late missconduct record
							# Reset Upgrade items
							if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
									$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
							}
							else if ($plugin['Discipline'] && $LateCtgInUsed)
							{
							   $t_date = $TargetDate;
							   $s_id = $my_user_id;
							   if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
								   $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
								   $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
								
								 }
							   else{
							       $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
							       $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
							   }
							}
							
							// recalculate the late category for the input time
							# Calculate upgrade items
		          if ($plugin['Disciplinev12'] && $LateCtgInUsed){
		            if($attendance_id) {
		              $semInfo = getAcademicYearAndYearTermByDate($TargetDate);
		              $acy = new academic_year_term($semInfo[0]);
		              
		              $dataAry = array();
									$dataAry['StudentID'] = $my_user_id;
									$dataAry['RecordDate'] = $TargetDate;
									//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
									$dataAry['Year'] = $acy->YearNameEN;
									$dataAry['Semester'] = $acy->YearTermNameEN;
									$dataAry['StudentAttendanceID'] = $attendance_id;
									$dataAry['AcademicYearID'] = $acy->AcademicYearID;
									$dataAry['YearTermID'] = $acy->YearTermID;
									$dataAry['Remark'] = $InSchoolTime;
			
									if($SeriousLateUsed == 1) 
									{
										if($InSchoolTime != "")
										{
											$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
											$school_start_time = $arr_time_table[$AttendanceArrayTime];
											
											$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
											$late_minutes = ceil($str_late_minutes/60);
													
											$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
											$min_late_minutes = $lcardattend->returnVector($sql);
											
											if(sizeof($min_late_minutes)>0)
											{
												$dataAry['SeriousLate'] = 1;
												$dataAry['LateMinutes'] = $late_minutes;
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									//debug("A : ");
									//debug_r($dataAry);
									$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);								
								}
		          } 
		          elseif ($plugin['Discipline'] && $LateCtgInUsed) {
								$t_date = $TargetDate;
								$s_id = $my_user_id;
		                       
								if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
									$ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
									if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
									{
									    $warning_student_ids[] = $student_id;
									}
									$ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
								}
								else{
									$ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
									$ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
									if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
									{
									    $warning_student_ids[] = $student_id;
									}
									$ldiscipline->calculateUpgradeLateToDetention($student_id);
								}
		          }
						}
            # Update reason in profile record
            $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                   WHERE StudentAttendanceID = '$attendance_id'";
            $Result['13'] = $lcardattend->db_db_query($sql);
          }
	      }
	    }
	    
	    # added on 2007-04-30
	    # remove previous absent record from Reason table ( if exists ) 
	    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".$DayType."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
	    $Result['14'] = $lcardattend->db_db_query($sql);
	    
	    # added on 2007-07-10
	    # remove previous absent record from PROFILE table ( if exists ) 
	    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".$DayType."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
	    $Result['15'] = $lcardattend->db_db_query($sql);
	
	    
	    # Update reason table record
	    $attendance_id = ($my_record_status == 1) ? NULL : $attendance_id;
	    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
	                   ProfileRecordID = '$attendance_id',
	                   RecordStatus = '$my_record_status' ";
	    if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	    {
	    	$no_late_session = $late_session[$i];
	    	$sql .= ",AbsentSession = '$no_late_session' ";
	    }
		$sql .=" WHERE RecordID= '$reason_record_id'";
	    $Result['16'] = $lcardattend->db_db_query($sql);
    }
    */
  }
  else if ($my_drop_down_status == CARD_STATUS_PRESENT)    # On Time
  {
  	$sql = "UPDATE $card_log_table_name
		              SET ".$StatusField." = '".CARD_STATUS_PRESENT."',
		               ".$DateModifiedField." = NOW(), 
		               ".$ModifyByField." = '".$_SESSION['UserID']."' 
		              WHERE DayNumber = '$txt_day'
		                    AND UserID = '$my_user_id'";
		$Result[$student_id."ToNormal"] = $lcardattend->db_db_query($sql);

  	$Result[$student_id."ClearProfile"] = $lcardattend->Clear_Profile($student_id,$TargetDate,$DayType);
  	/*
    # Search whether attendance exists by date, student and type
    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                   WHERE AttendanceDate = '$TargetDate'
                         AND UserID = '$my_user_id'
                         AND DayType = '".$DayType."'
                         AND RecordType = '".PROFILE_TYPE_LATE."'";
    $temp = $lcardattend->returnVector($sql);
    $attendance_id = $temp[0];
    
		# Remove Reason record
		$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		             WHERE RecordDate = '$TargetDate'
		                   AND StudentID = '$my_user_id'
		                   AND DayType = '".$DayType."'
		                   AND RecordType = '".PROFILE_TYPE_LATE."'";
		$Result['17'] = $lcardattend->db_db_query($sql);
		
		# Reset Upgrade items
		if ($plugin['Disciplinev12'] && $LateCtgInUsed)
		{
			$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
		}
		else if ($plugin['Discipline'] && $LateCtgInUsed)
		{
			$target_date = $TargetDate;
			$student_id = $my_user_id;
			if($ldiscipline->isUseAccumulativeLateSetting($target_date)){
				$ldiscipline->resetAccumulativeUpgradeLateToDemerit($student_id,$target_date);
				$ldiscipline->resetAccumulativeUpgradeLateToDetention($student_id,$target_date);
        if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$target_date))
        {
        	$warning_student_ids[] = $student_id;
        }
        $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$target_date);
			}                     
			else{
				$ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
				$ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
			}
		}

		# Remove Profile record
		$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		             WHERE AttendanceDate = '$TargetDate'
		                   AND UserID = '$my_user_id'
		                   AND DayType = '".$DayType."'
		                   AND RecordType = '".PROFILE_TYPE_LATE."'";
		$Result['18'] = $lcardattend->db_db_query($sql);
		# Calculate upgrade items
		if ($plugin['Disciplinev12'] && $LateCtgInUsed)
		{
			# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
  	}
  	else if ($plugin['Discipline'] && $LateCtgInUsed)
		{  
			$t_date = $TargetDate;
			if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
        if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
        {
            $warning_student_ids[] = $student_id;
        }
        $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
			}	                 
			else{
				if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
				{
				   $warning_student_ids[] = $student_id;
				}
				$ldiscipline->calculateUpgradeLateToDetention($student_id);
			}
		}
		*/
  }
  else if ($my_drop_down_status == CARD_STATUS_ABSENT)       # Absent
  {
  	$sql = "UPDATE $card_log_table_name
		              SET ".$StatusField." = '".CARD_STATUS_ABSENT."',
		               ".$DateModifiedField." = NOW(),
		               ".$ModifyByField." = '".$_SESSION['UserID']."' 
		              WHERE DayNumber = '$txt_day'
		                    AND UserID = '$my_user_id'";
		$Result[$my_user_id."ToAbsent"] = $lcardattend->db_db_query($sql);
		
    # Reason input
    $txtReason = trim(htmlspecialchars_decode(stripslashes(${"reason$i"}), ENT_QUOTES));
    
    $Result[$student_id."SetProfile"] = $lcardattend->Set_Profile($student_id,$TargetDate,$DayType,$my_drop_down_status,$txtReason,"|**NULL**|","|**NULL**|","|**NULL**|","",false,$my_record_status);
  
  	$sql = "SELECT ".$InSchoolTimeField." as InTime, InSchoolTime, LunchBackTime, AMStatus, PMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";	
	$temp = $lcardattend->returnArray($sql);
	$tmp_InTime = trim($temp[0]['InTime']);
	$tmp_InSchoolTime = trim($temp[0]['InSchoolTime']);
	$tmp_LunchBackTime = trim($temp[0]['LunchBackTime']);
	$tmp_AMStatus = trim($temp[0]['AMStatus']);
	$tmp_PMStatus = trim($temp[0]['PMStatus']);
	
	if($StatusField == 'AMStatus') {
		if( ($tmp_PMStatus=='' || $tmp_PMStatus==CARD_STATUS_ABSENT || $tmp_PMStatus==CARD_STATUS_OUTING)
			 || !(($tmp_PMStatus == CARD_STATUS_PRESENT || $tmp_PMStatus == CARD_STATUS_LATE) && $tmp_LunchBackTime == '') ) {
			$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
		}
	}else if($StatusField == 'PMStatus'){ // dealing with PM
		if( ($tmp_AMStatus=='' || $tmp_AMStatus==CARD_STATUS_ABSENT || $tmp_AMStatus==CARD_STATUS_OUTING)
			 || !(($tmp_AMStatus == CARD_STATUS_PRESENT || $tmp_AMStatus == CARD_STATUS_LATE) && $tmp_InSchoolTime == '') ) {
			$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
		}
	}

  if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
      $Result[$student_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($student_id, $TargetDate, $DayType,'','', CARD_LEAVE_NORMAL , '','');
  }

	if ($plugin['eSchoolBus']) {
	    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
	    if ($isTakingBusStudent) {
    	    # Late -> Absent
            $teacherRemark = trim($remark[$i]);
            if($has_magic_quotes){
                $teacherRemark = stripslashes($teacherRemark);
            }
            $input_reason = stripslashes(trim(${"reason$i"}));
            $Result[$i.'_SyncAbsentRecordToeSchoolBus'] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $input_reason, $teacherRemark, $dayTypeStr, $my_record_id);
	    }
    }
  }
  else if ($my_drop_down_status == CARD_STATUS_OUTING) {
  	$sql = "UPDATE $card_log_table_name
            SET ".$StatusField." = '".CARD_STATUS_OUTING."',
             	".$DateModifiedField." = NOW(),
             	".$ModifyByField." = '".$_SESSION['UserID']."' 
            WHERE DayNumber = '$txt_day'
                  AND UserID = $my_user_id";
		$Result[$my_user_id."ToAbsent"] = $lcardattend->db_db_query($sql);

  	# Reason input
    $txtReason = trim(htmlspecialchars_decode(stripslashes(${"reason$i"}), ENT_QUOTES));
    
    $Result[$student_id."SetProfile"] = $lcardattend->Set_Profile($student_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,$my_record_status);
  
  	$sql = "SELECT ".$InSchoolTimeField." as InTime, InSchoolTime, LunchBackTime, AMStatus, PMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";	
	$temp = $lcardattend->returnArray($sql);
	$tmp_InTime = trim($temp[0]['InTime']);
	$tmp_InSchoolTime = trim($temp[0]['InSchoolTime']);
	$tmp_LunchBackTime = trim($temp[0]['LunchBackTime']);
	$tmp_AMStatus = trim($temp[0]['AMStatus']);
	$tmp_PMStatus = trim($temp[0]['PMStatus']);
	
	if($StatusField == 'AMStatus') {
		if( ($tmp_PMStatus=='' || $tmp_PMStatus==CARD_STATUS_ABSENT || $tmp_PMStatus==CARD_STATUS_OUTING)
			 || !(($tmp_PMStatus == CARD_STATUS_PRESENT || $tmp_PMStatus == CARD_STATUS_LATE) && $tmp_LunchBackTime == '') ) {
			$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
		}
	}else if($StatusField == 'PMStatus'){ // dealing with PM
		if( ($tmp_AMStatus=='' || $tmp_AMStatus==CARD_STATUS_ABSENT || $tmp_AMStatus==CARD_STATUS_OUTING)
			 || !(($tmp_AMStatus == CARD_STATUS_PRESENT || $tmp_AMStatus == CARD_STATUS_LATE) && $tmp_InSchoolTime == '') ) {
			$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
		}
	}
	
	if ($plugin['eSchoolBus']) {
	    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
	    if ($isTakingBusStudent) {
    	    # Late -> Outgoing (eSchoolBus will be treated as Absent)
    	    $teacherRemark = trim($remark[$i]);
    	    if($has_magic_quotes){
    	        $teacherRemark = stripslashes($teacherRemark);
    	    }
    	    $input_reason = stripslashes(trim(${"reason$i"}));
    	    $Result[$i.'_SyncAbsentRecordToeSchoolBus'] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $input_reason, $teacherRemark, $dayTypeStr, $my_record_id);
	    }
	}
	
  }
  else # Unknown action
  {
      // do nothing
  }

    if (($my_drop_down_status == CARD_STATUS_PRESENT) ||  ($my_drop_down_status == CARD_STATUS_LATE)) {
        # Grab original status
        $sql = "SELECT RecordID FROM $card_log_table_name WHERE UserID='$my_user_id' AND DayNumber = '$txt_day'";
        $dailyLogRecordAry = $lcardattend->returnResultSet($sql);
        if (count($dailyLogRecordAry)) {
            $_recordID = $dailyLogRecordAry[0]['RecordID'];
        }

        if ($plugin['eSchoolBus']) {

            $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
            if ($isTakingBusStudent) {
                $appliedLeaveStudentAry = $lcardattend->checkStudentApplyLeave($my_user_id, $TargetDate, $DayType);
                $appliedLeaveStudentIDAry = array_keys($appliedLeaveStudentAry);

                if (in_array($my_user_id, (array)$appliedLeaveStudentIDAry)) {       // applied leave
                    $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
                    $conditionAry = array();
                    $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                    $conditionAry['StudentID'] = $my_user_id;
                    $conditionAry['StartDate'] = $TargetDate;
                    $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                    $Result['ChangeFromAbsentToOthers_' . $appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                    unset($conditionAry);
                    $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                    $conditionAry['t.LeaveDate'] = $TargetDate;
                    $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                    $Result['ChangeFromAbsentToOthers_Timeslot_' . $appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
                } else {      // not apply leave, use AttendanceRecordID to find
                    $conditionAry = array();
                    $conditionAry['AttendanceRecordID'] = $_recordID;
                    $conditionAry['StudentID'] = $my_user_id;
                    $conditionAry['StartDate'] = $TargetDate;
                    $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                    $Result['ChangeFromAbsentToOthers_' . $_recordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                    unset($conditionAry);
                    $conditionAry['t.AttendanceRecordID'] = $_recordID;
                    $conditionAry['t.LeaveDate'] = $TargetDate;
                    $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                    $Result['ChangeFromAbsentToOthers_TimeSlot_' . $_recordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
                }
            }
        }
    }

  	// handle Teacher's remark
	$remark_value = trim($remark[$i]);
	if(!$use_magic_quotes){
		$remark_value = addslashes($remark_value);
	}
	$lcardattend->updateTeacherRemark($my_user_id,$TargetDate,$DayType,$remark_value);
	
	// handle Office remark
	if($my_drop_down_status != CARD_STATUS_PRESENT){
		$office_remark_value = trim($_REQUEST['office_remark'.$i]);
		if(!$use_magic_quotes){
			$office_remark_value = addslashes($office_remark_value);
		}
		$RealProfileType = $my_drop_down_status;
		if($my_drop_down_status == CARD_STATUS_OUTING){
			$RealProfileType = CARD_STATUS_ABSENT;
		}
		$lcardattend->updateOfficeRemark($my_user_id,$TargetDate,$DayType,$RealProfileType,$office_remark_value);
	}
	
	$log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' 2 '.$my_drop_down_status.']';
} # End of For-Loop

## mark as confirmed by admin user
if(count($user_id)>0){
	$confirmed_by_admin_field = $DayType == PROFILE_DAY_TYPE_PM? "PMConfirmedByAdmin" : "AMConfirmedByAdmin";
	$sql = "UPDATE $card_log_table_name SET $confirmed_by_admin_field='1' WHERE DayNumber='$txt_day' AND UserID IN (".implode(",",$user_id).")";
	$lcardattend->db_db_query($sql);
}


# Update Confirm Record
$sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
               SET LateConfirmed = 1, LateConfirmTime = now(), LateConfirmUserID='$UserID', DateModified = now()
               WHERE RecordDate = '$TargetDate' AND RecordType = '".$DayType."'";
$Result['20'] = $lcardattend->db_db_query($sql);
if ($lcardattend->db_affected_rows()!=1)         # Not Exists
{
  # Not exists
  $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, LateConfirmed,RecordType, LateConfirmTime, LateConfirmUserID, DateInput, DateModified)
                 VALUES ('$TargetDate',1,'".$DayType."',now(),'$UserID',now(),now())";
  $Result['21'] = $lcardattend->db_db_query($sql);
}

$lcardattend->log($log_row);

intranet_closedb();

if (sizeof($warning_student_ids)!=0)
{
        $body_tags = "onLoad=document.form1.submit()";
        include_once($PATH_WRT_ROOT."templates/fileheader.php");
        ?>
        <form name=form1 action=showlate_msg_prompt.php method=POST>
        <?
        for ($i=0; $i<sizeof($warning_student_ids); $i++)
        {
             ?>
             <input type=hidden name=WarningStudentID[] value="<?=$warning_student_ids[$i]?>">
             <?
        }
        ?>
        <input type=hidden name=DayType value="<?=escape_double_quotes($DayType)?>">
        <input type=hidden name=TargetDate value="<?=escape_double_quotes($TargetDate)?>">
        <input type=hidden name=routeID value="<?=escape_double_quotes($routeID)?>">
        </form>
        <?
        include_once($PATH_WRT_ROOT."templates/filefooter.php");

}
else
{
	$Msg = $Lang['StudentAttendance']['LateListConfirmSuccess'];
  header("Location: showlate.php?DayType=".urlencode($DayType)."&TargetDate=".urlencode($TargetDate)."&routeID=".urlencode($routeID)."&order_by_time=".urlencode($order_by_time)."&Msg=".urlencode($Msg));
}

?>