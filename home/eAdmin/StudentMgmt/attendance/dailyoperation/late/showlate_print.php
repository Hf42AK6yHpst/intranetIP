<?php
// using by 
##################################### Change Log ######################################
# 2018-11-15 Cameron: add filter $routeID for eSchoolBus
# 2015-02-11 Carlos: Added column [Office Remark]
# 2014-01-28 Carlos: Added table column [Remark]
# 2011-12-21: Carlos - added data column Gender
# 2010-04-22: Carlos - Show late session if flag is switched on
#######################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

### class used
$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM: 
    	$display_period = $i_DayTypeAM;
        break;
    case PROFILE_DAY_TYPE_PM: 
    	$display_period = $i_DayTypePM;
        break;
    default : 
    	$display_period = $i_DayTypeAM;
        $DayType = PROFILE_DAY_TYPE_AM;
        break;
}


# order information
$default_order_by = " a.ClassName, a.ClassNumber+0, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	$order_str= $order== 1?" DESC ":" ASC ";

	if($DayType==PROFILE_DAY_TYPE_AM){
		$order_by = "b.InSchoolTime $order_str";
	}
	else {
		$order_by = "b.LunchBackTime $order_str";
	}
}


$busRouteJoin = '';     // default value
$busRouteFilter = '';

if ($plugin['eSchoolBus']) {
    $academicYearTermAry = getAcademicYearAndYearTermByDate($TargetDate);
    $academicYearID = $academicYearTermAry['AcademicYearID'];
    if ($DayType == PROFILE_DAY_TYPE_AM) {
        $amPm = 'AM';
    }
    else if ($DayType == PROFILE_DAY_TYPE_PM) {
        $amPm = 'PM';
    }
    if ($routeID) {
        $busRouteJoin = " LEFT JOIN INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=a.UserID AND ur.AcademicYearID='".$lc->Get_Safe_Sql_Query($academicYearID)."'
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION rc ON rc.RouteCollectionID=ur.RouteCollectionID ";
        $busRouteFilter = " AND ur.DateStart<='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND ur.DateEnd>='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND rc.RouteID='".$lc->Get_Safe_Sql_Query($routeID)."' AND rc.AmPm='".$lc->Get_Safe_Sql_Query($amPm)."' AND rc.IsDeleted='0' ";
    }
}


### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
				a.Gender,
                b.InSchoolTime,
                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                c.Reason,
                c.RecordStatus,
				d.Remark,
				c.OfficeRemark   
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."'
                           AND c.RecordType = '".PROFILE_TYPE_LATE."' 
					LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
					ON b.UserID = d.StudentID AND d.RecordDate ='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND d.DayType='".$lc->Get_Safe_Sql_Query($DayType)."'
                    {$busRouteJoin} 
                WHERE b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' AND b.AMStatus = '".CARD_STATUS_LATE."'
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                {$busRouteFilter}
         ORDER BY $order_by
";
}
else     # Check PM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
				a.Gender,
                b.LunchBackTime,
                IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
                c.Reason,
                c.RecordStatus,
				d.Remark,
				c.OfficeRemark   
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."'
                           AND c.RecordType = '".PROFILE_TYPE_LATE."' 
					LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
					ON b.UserID = d.StudentID AND d.RecordDate ='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND d.DayType='".$lc->Get_Safe_Sql_Query($DayType)."'
                    {$busRouteJoin} 
                WHERE b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' AND b.PMStatus = '".CARD_STATUS_LATE."'
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                {$busRouteFilter}
         ORDER BY $order_by
";

}

$result = $lc->returnArray($sql,10);

$table_attend = "";
$col_span = 13;
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
$x .= "<tr>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">#</td>";
$x .= "	<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_ClassName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserClassNumber</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['Gender']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Status</td>";
$x .=  "<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Waived</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">$i_Attendance_Reason</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['OfficeRemark']."</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$i_StudentGuardian['MenuInfo']."</td>
		</tr>";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name,$class_name,$class_number,$gender, $in_school_time, $in_school_station, $reason,$record_status, $remark, $office_remark) = $result[$i];
        $str_status = $i_StudentAttendance_Status_Late;
        $str_reason = $reason;
        $waived_option = $record_status == 1? $i_general_yes:$i_general_no;
        $x .= "<tr>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$name</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_name</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_number</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".$gender_word[$gender]."</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$in_school_time&nbsp;</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$in_school_station</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$str_status</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$waived_option</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">$str_reason&nbsp;</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">$office_remark&nbsp;</td>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">";
        
        // Get guardian information
		    $main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
		    $main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
		
		    $sql = "SELECT
                  $main_content_field,
                  Relation,
                  Phone,
                  EmPhone,
                  IsMain
								FROM
								  $eclass_db.GUARDIAN_STUDENT
								WHERE
								  UserID = '".$lc->Get_Safe_Sql_Query($user_id)."'
								ORDER BY
								  IsMain DESC, Relation ASC
		                ";
		    $temp = $lc->returnArray($sql,4);
				
				unset($layer_content);
		    if (sizeof($temp)==0)
		    {
		        $layer_content = "--";
		    }
		    else
		    {
		        for($j=0; $j<sizeof($temp); $j++)
		        {
              list($name, $relation, $phone, $em_phone) = $temp[$j];
              $no = $j+1;
              $layer_content .= "$name ($ec_guardian[$relation]) ($i_StudentGuardian_Phone) $phone ($i_StudentGuardian_EMPhone) $em_phone <br>\n";
		        }
		    }
		
		    $x .= $layer_content;
		    $x .= "</td>";
        $x .= "</tr>";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=\"tablerow2\">";
    $x .= "<td class=\"tabletext\" colspan=\"$col_span\" align=\"center\">$i_StudentAttendance_NoLateStudents</td>";
    $x .= "</tr>";
}
$x .= "</table>\n";
$table_attend = $x;

$i_title = "$i_SmartCard_DailyOperation_ViewLateStatus $TargetDate ($display_period)";
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><?=$i_title?></td>
	</tr>
</table>
<?=$table_attend?>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>