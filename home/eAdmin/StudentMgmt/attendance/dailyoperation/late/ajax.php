<?php
/*
 *      copy from eSchoolBus
 *      
 *      2018-11-15 Cameron
 *          - create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eSchoolBus_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_ui.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");

intranet_auth();
intranet_opendb();
$indexVar['linterface'] = new libSchoolBus_ui();
$indexVar['libSchoolBus'] = new libSchoolBus();
$indexVar['db'] = new libSchoolBus_db();

$linterface = $indexVar['linterface'];
$action = $_POST['action'];


switch ($action){
	
	case 'routeSelection_dateSearch':
		$date = $_POST['date'];
		$routeID = $_POST['routeID'];
		$amPm = $_POST['amPm'];
		$weekday_num = date("N", strtotime($date));
		
		switch($weekday_num){
			case 1:
				$weekday = 'MON';
			break;
			case 2:
				$weekday = 'TUE';
			break;
			case 3:
				$weekday = 'WED';
			break;
			case 4:
				$weekday = 'THU';
			break;
			case 5:
				$weekday = 'FRI';
			break;
			default:
				echo $Lang['eSchoolBus']['Managment']['Attendance']['Warning']['WeekendNotAvailable'];
				return;
			break;
		}	
		echo $linterface->getRouteSelection('id="routeID" name="routeID"',$routeID,$type='N',$amPm,$weekday,$date);        // normal route only
	break;
	default:
		echo 'error';
	break;
}

intranet_closedb();

?>