<?php
// using by 
/*
 * 2014-07-24 Carlos: Change Period selection to radio checkboxes
 * 2010-04-19 By Carlos: Added confirm colors(#1. all confirmed, #2. all not confirmed, 3#. partially confirmed) in calendar date picker
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewGroupStatus";

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

$lc->retrieveSettings();
$attendanceMode=$lc->attendance_mode;

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewGroupStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<?
echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","group_status.php","Group","","",1);
?>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
intranet_closedb();
?>