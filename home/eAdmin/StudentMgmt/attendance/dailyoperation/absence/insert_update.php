<?php
// Editing by 
/*
 * 2017-04-11 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || $TargetDate=='' || $DayType=='' || !in_array($DayType,array(PROFILE_DAY_TYPE_AM,PROFILE_DAY_TYPE_PM)) || count($student)==0) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();


$lbdb = new libdb();
$lbdb->Start_Trans();
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record) + 0;

$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
$card_student_daily_log .= $txt_year."_";
$card_student_daily_log .= $txt_month;



if ($DayType == PROFILE_DAY_TYPE_AM)
	$am_pm_prefix = "AM";
else if ($DayType == PROFILE_DAY_TYPE_PM)
	$am_pm_prefix = "PM";	

$Result = array();

//$student_list = explode(',',$student_list);
$student_list = $student;
for ($i=0; $i< sizeof($student_list); $i++) {
	$my_user_id = $student_list[$i];
	
	$sql = "
		UPDATE 
			$card_student_daily_log
		SET 
			".$am_pm_prefix."Status = '".CARD_STATUS_ABSENT."',
			DateModified = NOW(),
			ModifyBy='".$_SESSION['UserID']."' 
		WHERE 
			UserID='$my_user_id' AND 
			DayNumber = '$txt_day' 
		";
	$Result['SetStatus_'.$my_user_id] = $lbdb->db_db_query($sql);
	$affected_rows = $lbdb->db_affected_rows();
	if($affected_rows == 0){
		$sql = "INSERT INTO $card_student_daily_log (UserID,DayNumber,".$am_pm_prefix."Status,DateInput,InputBy,DateModified,ModifyBy) VALUES ('$my_user_id','$txt_day','".CARD_STATUS_ABSENT."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')";
		$Result['InsertDailylog_'.$my_user_id] = $lbdb->db_db_query($sql);
	}
	
	$Result['SetProfile_'.$my_user_id] = $lc->Set_Profile($my_user_id,$TargetDate,$DayType,PROFILE_TYPE_ABSENT,"|**NULL**|", "|**NULL**|", "|**NULL**|", "|**NULL**|", "",false,"|**NULL**|",false);
}

$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='$DocumentStatus' WHERE RecordDate='$TargetDate' AND StudentID IN ('".implode("','",$student_list)."') AND DayType='$DayType' AND RecordType='".PROFILE_TYPE_ABSENT."' ";
$Result['SetProveDocumentStatus'] = $lc->db_db_query($sql);

if (!in_array(false,$Result)) {
	$lbdb->Commit_Trans();
	$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else {
	$lbdb->RollBack_Trans();
	$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

intranet_closedb();
?>
<script type="text/javascript" language="Javascript">
window.opener.document.getElementById('Msg').value = '<?=$Msg?>';
window.opener.document.getElementById('form2').submit();

//window.opener.location.href = window.opener.location.href + '&Msg=<?=urlencode($Msg)?>';
window.close();
</script>