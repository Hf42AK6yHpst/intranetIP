<?php
// Editing by

##################################### Change Log #####################################################
# 2020-06-04 Ray: update CARD_STUDENT_PRESET_LEAVE when waive tick
# 2020-05-19 Ray: late & absent add InsertSubmittedAbsentLateRecord
# 2019-12-10 Ray: update CARD_STUDENT_PRESET_LEAVE when DocumentStatus tick
# 2019-09-23 Cameron: - as change RecordStatus, AddedFrom, AttendanceRecordID, AttendanceApplyLeaveID to timeslot table,
# should apply updateSchoolBusTimeSlotApplyLeaveToDelete() to related logic for eSchoolBus
# 2019-05-23 Carlos: For outing status, always set record status (i.e. waived) to 0 to make the record confirmed.
# 2019-02-01 Cameron: [ip.2.5.10.2.1] 
#   - sync record to eSchoolBus for absent and outgoing status: check if apply leave record already exist, add record if not exit, else update reason and teacher's remark
#   - don't sync apply leave record to eSchoolBus if the student is not taking school bus student.
# 2018-12-17 Cameron: [ip.2.5.10.2.1] also update reason and teacher's remark in eSchoolBus corresponding records for absent and outgoing status,
#   sync record to eSchoolBus for other status
# 2018-08-03 Carlos: [ip.2.5.9.10.1] Mark attendance records confirmed by admin.
# 2017-05-31 Carlos: [ip.2.5.8.7.1] Check and skip outdated record if attendance record last modified time is later than page load time. 
# 2017-02-24 Carlos: [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
# 2016-08-26 Bill: Update and auto sync Processed by School value
# 2016-01-28 Carlos: When change from absent to late, follow setting OnlySynceDisLateRecordWhenConfirm to sync late record to eDis.
# 2015-10-14 Omas : Add Document status extended logic.. automatic sync AM & PM Document status
# 2015-04-23 Carlos: Fix office remark array index shift problem after disable the input. 
# 2015-04-22 Carlos: Cater AM/PM modify by and date modified fields.
# 2015-02-11 Carlos: Update teacher's remark. Update office remark.
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
# 2014-03-17 Carlos: Update [Waive Absence] for $sys_custom['StudentAttendance_WaiveAbsent']
# 2012-06-13 Carlos: add no card entrance record if change status to Present / Late and no in time
# 2010-03-25 Carlos: Customization - Added textbox Remark and checkbox Has handin parent letter
######################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


# class used
$LIDB = new libdb();
$lcardattend = new libcardstudentattend2();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

/*
# check page load time 
$error = "";
if(sizeof($user_id) > 0)
{
	if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
		$error = 1;  // data outdated
		//echo "Error<BR>";
		//echo $PageLoadTime;
		$return_page = "show.php";
		
		$return_page = $return_page."?DayType=$DayType&TargetDate=$TargetDate&error=$error&Msg=".urlencode($Lang['StudentAttendance']['DataOutdatedWarning']);
		header("Location: $return_page");
		exit();	
	}
}
*/

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || ($DayType!=PROFILE_DAY_TYPE_PM && $DayType!=PROFILE_DAY_TYPE_AM) )
{
    header("Location: index.php");
    exit();
}

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

//$use_magic_quotes = get_magic_quotes_gpc();
$use_magic_quotes = $lcardattend->is_magic_quotes_active;

$upadteeDisLateRecord = $lcardattend->OnlySynceDisLateRecordWhenConfirm != '1';

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
/*$year = getCurrentAcademicYear();
$semester = getCurrentSemester();*/

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

$my_record_date = date('Y-m-d',$ts_record);

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$user_id,'StudentIDToRecord'=>1));

for($i=0; $i<sizeOf($user_id); $i++)
{
  $my_user_id = $user_id[$i];
  $my_day = $txt_day;
  $my_drop_down_status = $drop_down_status[$i];
  $my_record_id = $record_id[$i];
	
	// check and skip outdated record if attendance record last modified time is later than page load time
	if(isset($studentIdToDailyLog[$my_user_id])){
		if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
		{
			$am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
			if($am_modified_ts >= $PageLoadTime) continue;
		}
		if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
		{
			$pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
			if($pm_modified_ts >= $PageLoadTime) continue;
		}
	}
	
  // Retrieve Waived only if absent
  ($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE) ? ($my_record_status = (${"waived_".$my_user_id} == '') ? 0 : 1) : "";
	
	if($my_drop_down_status == CARD_STATUS_OUTING){
		$my_record_status = 0;
	}
	
	// KENNETH CHUNG CODE
	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$InSchoolTimeField = "InSchoolTime";
		$StatusField = "AMStatus";
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
		$dayTypeStr = 'AM';
	}
	else {
		$lcardattend->retrieveSettings();
		if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
		{
			$InSchoolTimeField = "InSchoolTime";
		}
		else # retrieve LunchBackTime
		{
			$InSchoolTimeField = "LunchBackTime";
		}
		$StatusField = "PMStatus";
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
		$dayTypeStr = 'PM';
	}
	
	$sql = "UPDATE $card_log_table_name
          SET 
          	".$StatusField." = '".$my_drop_down_status."',
          	".$ModifyByField." = '".$_SESSION['UserID']."', 
          	".$DateModifiedField." = NOW()
          WHERE DayNumber = '$txt_day'
                AND UserID = '$my_user_id' ";
  $lcardattend->db_db_query($sql);
	
	if ($my_drop_down_status == CARD_STATUS_ABSENT)       # Absent
  {
    ## hand in proved-document status
    $documentStatus = $_POST['HandIn_prove_'.$my_user_id]=='1'?"1":"0";
    
//    $HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='".$documentStatus."' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='$my_drop_down_status'";
//	$result = $lcardattend->db_db_query($HandIn_prove_sql);
    ## 2015-10-14 document status extended logic.. automatic sync AM & PM
	if($my_record_status == 1) {
		$sql = "SELECT 
			RecordID,
			StudentID,
			Waive
		FROM 
			CARD_STUDENT_PRESET_LEAVE 
		WHERE 
			StudentID='".$my_user_id."' AND
			DayPeriod = '".$lcardattend->Get_Safe_Sql_Query($DayType)."' AND
			( '".$lcardattend->Get_Safe_Sql_Query($TargetDate)."' = RecordDate )";
		$PresetAbsentStudentRecordAry =  $lcardattend->returnResultSet($sql);
		$PresetAbsentStudentAssoAry = BuildMultiKeyAssoc($PresetAbsentStudentRecordAry,'StudentID');
		$PresetAbsentStudentAry = Get_Array_By_Key($PresetAbsentStudentRecordAry,'StudentID');
		if (in_array($my_user_id, $PresetAbsentStudentAry)) {
			if($PresetAbsentStudentAssoAry[$my_user_id]['Waive'] == '0'){
				$sql = "UPDATE CARD_STUDENT_PRESET_LEAVE SET Waive='1' WHERE RecordID='".$PresetAbsentStudentAssoAry[$my_user_id]['RecordID']."'";
				$lcardattend->db_db_query($sql);
			}
		}
	}

    if($documentStatus==1){
    	$HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='".$documentStatus."' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' /*AND DayType='$DayType'*/ AND RecordType='$my_drop_down_status'";
    	$result = $lcardattend->db_db_query($HandIn_prove_sql);
//    	$sql = "select * from CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='$my_drop_down_status'";

		$sql = "SELECT 
			RecordID,
			StudentID,
			DocumentStatus
		FROM 
			CARD_STUDENT_PRESET_LEAVE 
		WHERE 
			StudentID='".$my_user_id."' AND
			DayPeriod = '".$lcardattend->Get_Safe_Sql_Query($DayType)."' AND
			( '".$lcardattend->Get_Safe_Sql_Query($TargetDate)."' = RecordDate )";
		$PresetAbsentStudentRecordAry =  $lcardattend->returnResultSet($sql);
		$PresetAbsentStudentAssoAry = BuildMultiKeyAssoc($PresetAbsentStudentRecordAry,'StudentID');
		$PresetAbsentStudentAry = Get_Array_By_Key($PresetAbsentStudentRecordAry,'StudentID');
		if (in_array($my_user_id, $PresetAbsentStudentAry)) {
			if($PresetAbsentStudentAssoAry[$my_user_id]['DocumentStatus'] == '0'){
				$sql = "UPDATE CARD_STUDENT_PRESET_LEAVE SET DocumentStatus='1' WHERE RecordID='".$PresetAbsentStudentAssoAry[$my_user_id]['RecordID']."'";
				$lcardattend->db_db_query($sql);
			}
		}
    }
    else{
    	$HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='".$documentStatus."' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='$my_drop_down_status'";
    	$result = $lcardattend->db_db_query($HandIn_prove_sql);
    }
    
    ## process by school
    $processed = $_POST['processed_by_'.$my_user_id]=='1'? "1" : "0";
    if($processed==1){
    	$processed_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Processed='".$processed."' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' /*AND DayType='$DayType'*/ AND RecordType='$my_drop_down_status'";
    	$lcardattend->db_db_query($processed_sql);
    }
    else{
    	$processed_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Processed='".$processed."' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='$my_drop_down_status'";
    	$lcardattend->db_db_query($processed_sql);
    }
    
    # Reason input
    $txtReason = trim(htmlspecialchars_decode(stripslashes(${"reason$i"}), ENT_QUOTES));
		$txtRemark = ($sys_custom['StudentAttendance_AbsenceRemark'])? trim(htmlspecialchars_decode(stripslashes($absence_remark[$i]), ENT_QUOTES)):"|**NULL**|";
    $handin = ($sys_custom['StudentAttendance_AbsenceHandinLetter'])? ${"handin_letter_$my_user_id"}:"|**NULL**|";
    
    $Result[$my_user_id."SetProfile"] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$txtReason,$txtRemark,"|**NULL**|",$handin,"",false,$my_record_status);
		
	if($sys_custom['StudentAttendance_WaiveAbsent']){
		$waive_absent_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET WaiveAbsent='".($_REQUEST['waive_absent'.$my_user_id]==1?"1":"0")."' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='$my_drop_down_status'";
		$lcardattend->db_db_query($waive_absent_sql);
	}

	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$Result[$my_user_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType,'','', CARD_LEAVE_NORMAL , '','');
	}

    # Add medical reason
    if ($sys_custom['hku_medical_research'])
    {
        # Try insert
        $sql = "INSERT IGNORE INTO SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                       (StudentID, RecordDate, DayType, MedicalReasonType, DateInput , DateModified)
                       VALUES
                       ('$my_user_id', '$TargetDate', '".$DayType."',
                       '".${"medical_reason_type_".$my_user_id}."', now(),now())";
        $lcardattend->db_db_query($sql);
        if ($lcardattend->db_affected_rows()!=1)
        {
            # Try update
            $sql = "UPDATE SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                           SET MedicalReasonType = '".${"medical_reason_type_".$my_user_id}."'
                           WHERE RecordDate = '$TargetDate' AND DayType = '".$DayType."'
                           AND StudentID = '$my_user_id'
                           ";
            $lcardattend->db_db_query($sql);
        }
    }
    
  }
  else if ($my_drop_down_status == CARD_STATUS_OUTING) {
  	# Table CARD_STUDENT_PROFILE_RECORD_REASON no status on CARD_STATUS_OUTING therefore if CARD_STATUS_OUTING set DocumentStatus = 1
  	//$Outing_HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus= 1 WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='$my_drop_down_status'";//RecordType =3 ?
  	$Outing_HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus= 1, Processed=1 WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType' AND RecordType='1'";
  	$lcardattend->db_db_query($Outing_HandIn_prove_sql);
  	
  	# Reason input
  	$txtReason = trim(htmlspecialchars_decode(stripslashes(${"reason$i"}), ENT_QUOTES));
		$txtRemark = ($sys_custom['StudentAttendance_AbsenceRemark'])? trim(htmlspecialchars_decode(stripslashes($absence_remark[$i]), ENT_QUOTES)):"|**NULL**|";
    $handin = ($sys_custom['StudentAttendance_AbsenceHandinLetter'])? ${"handin_letter_$my_user_id"}:"|**NULL**|";
    
    $Result[$my_user_id."SetProfile"] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$txtReason,$txtRemark,"|**NULL**|",$handin,"",false,$my_record_status);
    
    # remove medical reason
		if ($sys_custom['hku_medical_research'])
		{
		  # Try remove
		  $sql = "DELETE FROM SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
             WHERE RecordDate = '$TargetDate' AND DayType = '".$DayType."'
             AND StudentID = '$my_user_id'
             ";
		  $lcardattend->db_db_query($sql);
		}
  }
  else if ($my_drop_down_status == CARD_STATUS_PRESENT)    # On Time
  {
  	$Result[$my_user_id."ClearProfile"] = $lcardattend->Clear_Profile($my_user_id,$TargetDate,$DayType);
	/*
		$sql = "SELECT ".$InSchoolTimeField." FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";
		$Temp = $lcardattend->returnVector($sql);
		$tmp_InSchoolTime = $Temp[0];
		if($tmp_InSchoolTime != ""){		## if no in school time add no card entrance
		 	$lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
			# forgot to bring card
			$lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
		}
	*/
		$sql = "SELECT ".$InSchoolTimeField." as InTime, InSchoolTime, LunchBackTime FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";	
		$temp = $lcardattend->returnArray($sql);
		$tmp_InTime = trim($temp[0]['InTime']);
		$tmp_InSchoolTime = trim($temp[0]['InSchoolTime']);
		$tmp_LunchBackTIme = trim($temp[0]['LunchBackTime']);
		
		if($tmp_InTime != ""){		## if no in school time add no card entrance
		 	$lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
		}
		
		if($tmp_InSchoolTime == '' && $tmp_LunchBackTime == ''){
			# forgot to bring card
			$lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,'');
		}
		
		# remove medical reason
		if ($sys_custom['hku_medical_research'])
		{
		  # Try remove
		  $sql = "DELETE FROM SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
             WHERE RecordDate = '$TargetDate' AND DayType = '".$DayType."'
             AND StudentID = '$my_user_id'
             ";
		  $lcardattend->db_db_query($sql);
		}

	  if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) {
		  $sql = "update CARD_STUDENT_STATUS_SESSION_COUNT set
									LateSession = '0',
									RequestLeaveSession = '0',
									PlayTruantSession = '0',
									OfficalLeaveSession = '0',
									AbsentSession = '0',
									DateModify = NOW(),
									ModifyBy = '".$_SESSION['UserID']."'
								where
									RecordDate = '".$TargetDate."'
									AND
									StudentID = '".$my_user_id."'
									AND
									DayType = '".$DayType."'";
		  $lcardattend->db_db_query($sql);
	  }
  }
  else if ($my_drop_down_status == CARD_STATUS_LATE)    # Late
  {
  	/*
	  $inTimesql = "select 
					    			".$InSchoolTimeField." 
					    		from 
					    			$card_log_table_name
									WHERE 
										DayNumber = '$txt_day'
										AND UserID = '$my_user_id'";
    $inTimeResult = $lcardattend->returnVector($inTimesql);
    $InSchoolTime = $inTimeResult[0];
	*/
	$sql = "SELECT ".$InSchoolTimeField." as InTime, InSchoolTime, LunchBackTime FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";	
	$temp = $lcardattend->returnArray($sql);
	$InSchoolTime = trim($temp[0]['InTime']);
	$tmp_InSchoolTime = trim($temp[0]['InSchoolTime']);
	$tmp_LunchBackTIme = trim($temp[0]['LunchBackTime']);
	
    # Reason input
    $txtReason = intranet_htmlspecialchars(trim(${"reason$i"}));
	
	if($InSchoolTime != ""){		## if no in school time add no card entrance
	 	$lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
	}
	
	if($tmp_InSchoolTime == '' && $tmp_LunchBackTime == ''){
		# forgot to bring card
		$lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,'');
	}
	
		// code added by kenneth
    $Result[$student_id."SetProfile"] = 
    $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$txtReason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$my_record_status,false,$upadteeDisLateRecord);
    // end code added by kenneth

	  if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		  $Result[$my_user_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType,'','', PROFILE_TYPE_LATE , '','');
	  }

  }
  else # Unknown action
  {
       # Do nthg
  }

  if ($plugin['eSchoolBus']) {
      $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
      if ($isTakingBusStudent) {
          if ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING) {               # Absent / Outgoing
              $thisReason = trim(${"reason$i"});
              $teacherRemark = trim($remark[$i]);
              if($has_magic_quotes){
                  $teacherRemark = stripslashes($teacherRemark);
                  $thisReason = stripslashes($thisReason);
              }
      
              // check if apply leave record already exist
              $isAppliedLeave = $leSchoolBus->isTimeSlotAppliedLeave($my_user_id, $TargetDate, $TargetDate, $dayTypeStr, $dayTypeStr);
              if ($isAppliedLeave) {
                  // update reason and teacher's remark only
                  $Result['UpdateReasonAndRemark_'.$my_record_id] = $leSchoolBus->updateReasonAndRemark($my_user_id, $TargetDate, $my_record_id, $thisReason, $teacherRemark);
              }
              else {    // add record
                  $Result['SyncAbsentRecordToeSchoolBus_'.$logRecordID] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $thisReason, $teacherRemark, $dayTypeStr, $my_record_id);
              }
          }
          else if ($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE) {       # Absent -> Late / Present
              $conditionAry['AttendanceRecordID'] = $my_record_id;
              $conditionAry['StudentID'] = $my_user_id;
              $conditionAry['StartDate'] = $TargetDate;
              $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
              $Result['ChangeFromAbsentToOthers_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

              unset($conditionAry);
              $conditionAry['t.AttendanceRecordID'] = $my_record_id;
              $conditionAry['t.LeaveDate'] = $TargetDate;
              $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
              $Result['ChangeFromAbsentToOthers_Timeslot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
          }
      }
  }
  
  
	// END OF KENNETH CHUNG CODE

	// handle Teacher's remark
	$remark_value = trim($remark[$i]);
	if(!$use_magic_quotes){
		$remark_value = addslashes($remark_value);
	}
	$lcardattend->updateTeacherRemark($my_user_id,$TargetDate,$DayType,$remark_value);
	//$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$remark_value',DateModified=NOW(), ModifyBy='$UserID' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType'";
	//$lcardattend->db_db_query($sql);
	//if($lcardattend->db_affected_rows()==0){
	//	$sql = "INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark,DateModified,ModifyBy) VALUES ('$my_user_id','$TargetDate','$DayType','$remark_value',NOW(),'$UserID')";
	//	$lcardattend->db_db_query($sql);
	//}
	
	// handle Office remark
	if($my_drop_down_status != CARD_STATUS_PRESENT){
		$office_remark_value = trim($_REQUEST['office_remark'.$i]);
		if(!$use_magic_quotes){
			$office_remark_value = addslashes($office_remark_value);
		}
		$RealProfileType = $my_drop_down_status;
		if($my_drop_down_status == CARD_STATUS_OUTING){
			$RealProfileType = CARD_STATUS_ABSENT;
		}
		$lcardattend->updateOfficeRemark($my_user_id,$TargetDate,$DayType,$RealProfileType,$office_remark_value);
		//$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET OfficeRemark='$office_remark_value',DateModified=NOW() WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='$DayType' AND RecordType='$RealProfileType'";
		//$lcardattend->db_db_query($sql);
	}
	
	//HKUSPH
	if($sys_custom['eClassApp']['HKUSPH']){
		include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
		include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
		
		$db_hkuFlu = new libHKUFlu_db();
		$hkuFlu = new HKUFlu();
		
		$not_a_sick_leave = $_POST['not_a_sick_leave_'.$my_user_id];
		$have_record = $_POST['hkusph_have_record_'.$my_user_id];
		$preset_leave_id = $_POST['hkusph_preset_leave_id_'.$my_user_id];
		$sick_leave_id = $_POST['hkusph_sick_leave_id_'.$my_user_id];
		$leave_date = $_POST['hkusph_leave_date_'.$my_user_id];
		$original_not_sick_leave = $_POST['hkusph_not_sick_leave_'.$my_user_id];
		$preset_leave_without_sick_leave = $_POST['hkusph_preset_leave_without_sick_leave_'.$my_user_id];
		
		if($original_not_sick_leave == 'F' && $not_a_sick_leave == '1'){
			if($have_record == 'T'){
				if($preset_leave_id != '' && $preset_leave_without_sick_leave == 'F'){
					$db_hkuFlu->editSickLeaveByPresetLeave($preset_leave_id, $sick_date = '', $array_symptom_code = array(), $other_symptom = '', $array_sick_code = array(), $other_sick = '', $is_sick_leave = false, $from = 'T');
				}else{
					$db_hkuFlu->editSickLeaveBySickLeaveID($sick_leave_id, $eClass_leave_id = null, $sick_date = '', $array_symptom_code = array(), $other_symptom = '', $array_sick_code = array(), $other_sick = '', $is_sick_leave = false, $from = 'T');
				}
			}else{
				$db_hkuFlu->addNonSickLeave($eClass_leave_id = null, $eClass_preset_leave_id = null, $parent_id = '', $student_id = $my_user_id, $leave_date, $from = 'T');
			}
		}else{
			
		}
	}
	//END-HKUSPH
	
	$log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' 1 '.$my_drop_down_status.']';
	
/*	
  if( $period == "1")        # AM
  {
      if ($my_drop_down_status == CARD_STATUS_ABSENT)       # Absent
      {
          # Reason input
          $txtReason = ${"reason$i"};
          $txtReason = intranet_htmlspecialchars($txtReason);

          # Get ProfileRecordID
          $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                         WHERE RecordDate = '$TargetDate'
                               AND StudentID = '$my_user_id'
                               AND DayType = '".PROFILE_DAY_TYPE_AM."'
                               AND RecordType = '".PROFILE_TYPE_ABSENT."'";
          
          $temp = $lcardattend->returnArray($sql,2);
          list($reason_record_id, $reason_profile_id) = $temp[0];

          if ($reason_record_id == "")           # Reason record not exists
          {
              # Search whether attendance exists by date, student and type
              $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                             WHERE AttendanceDate = '$TargetDate'
                                   AND UserID = '$my_user_id'
                                   AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                   AND RecordType = '".PROFILE_TYPE_ABSENT."'";
              $temp = $lcardattend->returnVector($sql);
              $attendance_id = $temp[0];
              if ($attendance_id == "")          # Record not exists
              {
                      if ($my_record_status != 1)
                      {
                          # Insert profile record
                          $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                          $temp = $lcardattend->returnArray($sql,2);
                          list ($user_classname, $user_classnum) = $temp[0];
                          $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                          $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                          $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                          $lcardattend->db_db_query($sql);
                          $attendance_id = $lcardattend->db_insert_id();
                      }
              }
              
              else
              {
                      if ($my_record_status == 1)
                      {
                              // Delete Record if waived
                                                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                  $lcardattend->db_db_query($sql);
                                                  $attendance_id = "";
                      }
                      else
                      {
                          # Update Reason in profile record By AttendanceID
                          $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                 WHERE StudentAttendanceID = '$attendance_id'";
                      	$lcardattend->db_db_query($sql);
                  	}
              }

              # remove previous Late Record from Reason Table ( if exists)
              $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);

              # remove previous Late Record from PROFILE Table ( if exists)
              $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);

		# Record number of absent sessions for Status Absent
          	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
          	{
          		$no_absent_session = $absent_session[$i];
          		# Insert to Reason table with number of Absent Sessions
                $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, AbsentSession, DateInput, DateModified";
                $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_AM."', '$my_record_status', '$no_absent_session', now(), now() ";
          	}else
          	{
                # Insert to Reason table
                $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_AM."', '$my_record_status', now(), now() ";
          	}
          	
          	if($sys_custom['StudentAttendance_AbsenceRemark'])
            {
            	$txtRemark = trim(htmlspecialchars_decode($absence_remark[$i], ENT_QUOTES));
            	$fieldname .= ",Remark";
            	if($txtRemark == '')
            		$fieldsvalues .= ",NULL ";
            	else
            		$fieldsvalues .= ",'".$lcardattend->Get_Safe_Sql_Query($txtRemark)."' ";
            }
            if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
            {
            	$handin = ${"handin_letter_$my_user_id"}==1?1:0;
            	$fieldname .= ",HandinLetter";
            	$fieldsvalues .= ",'$handin'";
            }
            
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $lcardattend->db_db_query($sql);
          }
          else  # Reason record exists
          {
              if ($reason_profile_id == "")    # Profile ID not exists
              {
                  # Search whether attendance exists by date, student and type
                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                 WHERE AttendanceDate = '$TargetDate'
                                       AND UserID = '$my_user_id'
                                       AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                       AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                  $temp = $lcardattend->returnVector($sql);
                  $attendance_id = $temp[0];

                  if ($attendance_id == "")          # Record not exists
                  {
                          if ($my_record_status != 1)
                          {
                              # Insert profile record
                              $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                              $temp = $lcardattend->returnArray($sql,2);
                              list ($user_classname, $user_classnum) = $temp[0];
                              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                              $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                              $lcardattend->db_db_query($sql);
                              $attendance_id = $lcardattend->db_insert_id();
                          }
                  }
                  else
                  {
                          if ($my_record_status == 1)
                          {
                                  // Delete Record if waived
                                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                          $lcardattend->db_db_query($sql);
                                                          $attendance_id = "";
                          }
                          else
                          {
                              # Update Reason in profile record By AttendanceID
                              $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                     WHERE StudentAttendanceID = '$attendance_id'";
                              $lcardattend->db_db_query($sql);
                          }
                  }
              }
              else  # Has Profile ID
              {
                  # Search Attendance By ID
                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                 WHERE StudentAttendanceID = '$reason_profile_id'";
                  $temp = $lcardattend->returnVector($sql);
                  $attendance_id = $temp[0];

                  if ($attendance_id == "")          # Record not exists
                  {
                      # Search attendance by date, student and type
                      $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                     WHERE AttendanceDate = '$TargetDate'
                                           AND UserID = '$my_user_id'
                                           AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                           AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                      $temp = $lcardattend->returnVector($sql);
                      $attendance_id = $temp[0];
                      if ($attendance_id == "")          # Record not exists
                      {
                              if ($my_record_status != 1)
                              {
                                  # insert reason in profile record
                                  $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                  $temp = $lcardattend->returnArray($sql,2);
                                  list ($user_classname, $user_classnum) = $temp[0];
                                  $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                  $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                  $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                  $lcardattend->db_db_query($sql);
                                  $attendance_id = $lcardattend->db_insert_id();
                              }
                      }
                      else
                      {
                              if ($my_record_status == 1)
                              {
                                      // Delete Record if waived
                                                                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                  $lcardattend->db_db_query($sql);
                                                                  $attendance_id = NULL;
                              }
                              else
                              {
                                  # Update Reason in profile record By AttendanceID
                                  $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                         WHERE StudentAttendanceID = '$attendance_id'";
                                  $lcardattend->db_db_query($sql);
                              }
                      }
                  }
                  else # profile record exists and valid
                  {
                          if ($my_record_status == 1)
                          {
                                  // Delete Record if waived
                                                          $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                          $lcardattend->db_db_query($sql);
                                                          $attendance_id = NULL;
                          }
                          else
                          {
                              # Update reason in profile record
                              $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                     WHERE StudentAttendanceID = '$attendance_id'";
                              $lcardattend->db_db_query($sql);
                          }
                  }
              }

              # remove previous Late Record from Reason Table ( if exists)
              $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);

              # remove previous Late Record from PROFILE Table ( if exists)
              $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);
		
		$extra_fields = "";
		if($sys_custom['StudentAttendance_AbsenceRemark'])
            {
            	$txtRemark = trim(htmlspecialchars_decode($absence_remark[$i],ENT_QUOTES));
            	if($txtRemark == '')
            		$extra_fields .= ",Remark=NULL ";
            	else
            		$extra_fields .= ",Remark = '".$lcardattend->Get_Safe_Sql_Query($txtRemark)."' ";
            }
            if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
            {
            	$handin = ${"handin_letter_$my_user_id"}==1?1:0;
            	$extra_fields .= ",HandinLetter = '$handin' ";
            }
		
		# Record number of absent sessions for Status Absent
          	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
          	{
          		$no_absent_session = $absent_session[$i];
          		# Update reason table record with number of Absent Sessions
                $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                               ProfileRecordID = '$attendance_id',
                               RecordStatus = '$my_record_status',
                               RecordType = '".PROFILE_TYPE_ABSENT."',
                               AbsentSession = '$no_absent_session'
						   $extra_fields 
                               WHERE RecordID= '$reason_record_id'";
          	}else
          	{
                # Update reason table record
                $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                               ProfileRecordID = '$attendance_id',
                               RecordStatus = '$my_record_status'
						   $extra_fields
                               WHERE RecordID= '$reason_record_id'";
          	}
              $lcardattend->db_db_query($sql);
          }
          
	# Update AMStatus if status is changed
	$sql = "SELECT AMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = $my_user_id";
          $arr_AMStatus = $lcardattend->returnArray($sql);
          $tmp_AMStatus = $arr_AMStatus[0][0];
          if($tmp_AMStatus != $my_drop_down_status){		## if different, then update
             $sql = "UPDATE $card_log_table_name
                            SET AMStatus = '".$my_drop_down_status."',
                            DateModified = NOW()
                            WHERE DayNumber = '$txt_day'
                                  AND UserID = '$my_user_id'";
             $lcardattend->db_db_query($sql);
          }
	
          # Add medical reason
          if ($sys_custom['hku_medical_research'])
          {
              # Try insert
              $sql = "INSERT IGNORE INTO SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                             (StudentID, RecordDate, DayType, MedicalReasonType, DateInput , DateModified)
                             VALUES
                             ('$my_user_id', '$TargetDate', '".PROFILE_DAY_TYPE_AM."',
                             '".${"medical_reason_type_".$my_user_id}."', now(),now())";
              $lcardattend->db_db_query($sql);
              if ($lcardattend->db_affected_rows()!=1)
              {
                  # Try update
                  $sql = "UPDATE SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                 SET MedicalReasonType = '".${"medical_reason_type_".$my_user_id}."'
                                 WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                 AND StudentID = '$my_user_id'
                                 ";
                  $lcardattend->db_db_query($sql);
              }
          }

      }
      else if ($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_OUTING)    # On Time or Outing
      {
           # Remove Reason record
           $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                         WHERE RecordDate = '$TargetDate'
                               AND StudentID = '$my_user_id'
                               AND DayType = '".PROFILE_DAY_TYPE_AM."'
                               AND RecordType = '".PROFILE_TYPE_ABSENT."'";
           $lcardattend->db_db_query($sql);
           # Remove Profile record
           $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                         WHERE AttendanceDate = '$TargetDate'
                               AND UserID = '$my_user_id'
                               AND DayType = '".PROFILE_DAY_TYPE_AM."'
                               AND RecordType = '".PROFILE_TYPE_ABSENT."'";
           $lcardattend->db_db_query($sql);
           
           # Set to Daily Record Table
           # Set AMStatus and InSchoolTime
           # Keep InSchool Time
	 ## First - Check the selected status is different from the old one or not
           $sql = "SELECT AMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";
           $arr_AMStatus = $lcardattend->returnArray($sql);
           $tmp_AMStatus = $arr_AMStatus[0][0];
           if($tmp_AMStatus != $my_drop_down_status){		## if different, then update
             $sql = "UPDATE $card_log_table_name
                            SET AMStatus = '".$my_drop_down_status."',
                            DateModified = NOW()
                            WHERE DayNumber = '$txt_day'
                                  AND UserID = '$my_user_id'";
             $lcardattend->db_db_query($sql);
           }

           if($my_drop_down_status== CARD_STATUS_PRESENT){ ## Bad Action

                     $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
                     # forgot to bring card
                     $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
               }

           # Add medical reason
           if ($sys_custom['hku_medical_research'])
           {
              # Try remove
              $sql = "DELETE FROM SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                 WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                 AND StudentID = '$my_user_id'
                                 ";
              $lcardattend->db_db_query($sql);

          }


      }
      else # Unknown action
      {
           # Do nthg
      }
  }
  else if ($period == "2") # PM
  {
      if ($my_drop_down_status == CARD_STATUS_ABSENT)       # Late
      {
          # Reason input
          $txtReason = ${"reason$i"};
          $txtReason = intranet_htmlspecialchars($txtReason);

          # Get ProfileRecordID
          $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                         WHERE RecordDate = '$TargetDate'
                               AND StudentID = '$my_user_id'
                               AND DayType = '".PROFILE_DAY_TYPE_PM."'
                               AND RecordType = '".PROFILE_TYPE_ABSENT."'";
          $temp = $lcardattend->returnArray($sql,2);
          list($reason_record_id, $reason_profile_id) = $temp[0];

          if ($reason_record_id == "")           # Reason record not exists
          {
              # Search whether attendance exists by date, student and type
              $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                             WHERE AttendanceDate = '$TargetDate'
                                   AND UserID = '$my_user_id'
                                   AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                   AND RecordType = '".PROFILE_TYPE_ABSENT."'";
              $temp = $lcardattend->returnVector($sql);
              $attendance_id = $temp[0];
              if ($attendance_id == "")          # Record not exists
              {
                      if ($my_record_status != 1)
                      {
                          # Insert profile record
                          $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                          $temp = $lcardattend->returnArray($sql,2);
                          list ($user_classname, $user_classnum) = $temp[0];
                          $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                          $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                          $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                          $lcardattend->db_db_query($sql);
                          $attendance_id = $lcardattend->db_insert_id();
                      }
              }
              else
              {
                      if ($my_record_status == 1)
                      {
                              // Delete Record if waived
                                                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                  $lcardattend->db_db_query($sql);
                                                  $attendance_id = NULL;
                      }
                      else
                      {
                          # Update Reason in profile record By AttendanceID
                          $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                 WHERE StudentAttendanceID = '$attendance_id'";
                          $lcardattend->db_db_query($sql);
                      }
              }

              # remove previous Late Record from Reason Table ( if exists)
              $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);

              # remove previous Late Record from PROFILE Table ( if exists)
              $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);

		# Record number of absent sessions for Status Absent
          	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
          	{
          		$no_absent_session = $absent_session[$i];
          		# Insert to Reason table with number of Absent Sessions
                $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, AbsentSession, DateInput, DateModified";
                $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_PM."', '$my_record_status', '$no_absent_session', now(), now() ";
          	}else
          	{
                # Insert to Reason table
                $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_ABSENT."', '".PROFILE_DAY_TYPE_PM."', '$my_record_status', now(), now() ";
          	}
          	
          	if($sys_custom['StudentAttendance_AbsenceRemark'])
            {
            	$txtRemark = trim(htmlspecialchars_decode($absence_remark[$i], ENT_QUOTES));
            	$fieldname .= ",Remark";
            	if($txtRemark=='')
            		$fieldsvalues .= ",NULL ";
            	else
            		$fieldsvalues .= ",'".$lcardattend->Get_Safe_Sql_Query($txtRemark)."'";
            }
            if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
            {
            	$handin = ${"handin_letter_$my_user_id"}==1?1:0;
            	$fieldname .= ",HandinLetter";
            	$fieldsvalues .= ",'$handin'";
            }
          	
              $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                             VALUES ($fieldsvalues)";
              $lcardattend->db_db_query($sql);
          }
          else  # Reason record exists
          {
              if ($reason_profile_id == "")    # Profile ID not exists
              {
                  # Search whether attendance exists by date, student and type
                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                 WHERE AttendanceDate = '$TargetDate'
                                       AND UserID = '$my_user_id'
                                       AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                       AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                  $temp = $lcardattend->returnVector($sql);
                  $attendance_id = $temp[0];
                  if ($attendance_id == "")          # Record not exists
                  {
                          if ($my_record_status != 1)
                          {
                              # Insert profile record
                              $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                              $temp = $lcardattend->returnArray($sql,2);
                              list ($user_classname, $user_classnum) = $temp[0];
                              $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                              $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                              $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                              $lcardattend->db_db_query($sql);
                              $attendance_id = $lcardattend->db_insert_id();
                          }
                  }
                  else
                  {
                          if ($my_record_status == 1)
                                                  {
                                                          // Delete Record if waived
                                                          $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                          $lcardattend->db_db_query($sql);
                                                          $attendance_id = NULL;
                                                  }
                                                  else
                                                  {
                              # Update Reason in profile record By AttendanceID
                              $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                     WHERE StudentAttendanceID = '$attendance_id'";
                              $lcardattend->db_db_query($sql);
                          }
                  }
              }
              else  # Has Profile ID
              {
                  # Search Attendance By ID
                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                 WHERE StudentAttendanceID = '$reason_profile_id'";
                  $temp = $lcardattend->returnVector($sql);
                  $attendance_id = $temp[0];
                  if ($attendance_id == "")          # Record not exists
                  {
                      # Search attendance by date, student and type
                      $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                     WHERE AttendanceDate = '$TargetDate'
                                           AND UserID = '$my_user_id'
                                           AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                           AND RecordType = '".PROFILE_TYPE_ABSENT."'";
                      $temp = $lcardattend->returnVector($sql);
                      $attendance_id = $temp[0];
                      if ($attendance_id == "")          # Record not exists
                      {
                              if ($my_record_status != 1)
                              {
                                  # insert reason in profile record
                                  $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                  $temp = $lcardattend->returnArray($sql,2);
                                  list ($user_classname, $user_classnum) = $temp[0];
                                  $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                  $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_ABSENT."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                  $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                  $lcardattend->db_db_query($sql);
                                  $attendance_id = $lcardattend->db_insert_id();
                          }
                      }
                      else
                      {
                              if ($my_record_status == 1)
                              {
                                      // Delete Record if waived
                                                                  $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                  $lcardattend->db_db_query($sql);
                                                                  $attendance_id = "";
                              }
                              else
                              {
                                  # Update Reason in profile record By AttendanceID
                                  $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                         WHERE StudentAttendanceID = '$attendance_id'";
                                  $lcardattend->db_db_query($sql);
                              }
                      }
                  }
                  else # profile record exists and valid
                  {
                          if ($my_record_status == 1)
                          {
                                  // Delete Record if waived
                                                          $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                          $lcardattend->db_db_query($sql);
                                                          $attendance_id = "";
                          }
                          else
                          {
                              # Update reason in profile record
                              $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                     WHERE StudentAttendanceID = '$attendance_id'";
                              $lcardattend->db_db_query($sql);
                          }
                  }
              }

              # remove previous Late Record from Reason Table ( if exists)
              $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);

              # remove previous Late Record from PROFILE Table ( if exists)
              $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_LATE."'";
              $lcardattend->db_db_query($sql);
		
		$extra_fields = "";
		if($sys_custom['StudentAttendance_AbsenceRemark'])
            {
            	$txtRemark = trim(htmlspecialchars_decode($absence_remark[$i],ENT_QUOTES));
            	if($txtRemark=='')
            		$extra_fields .= ",Remark=NULL ";
            	else
            		$extra_fields .= ",Remark = '".$lcardattend->Get_Safe_Sql_Query($txtRemark)."' ";
            }
            if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
            {
            	$handin = ${"handin_letter_$my_user_id"}==1?1:0;
            	$extra_fields .= ",HandinLetter = '$handin' ";
            }
		# Record number of absent sessions for Status Absent
          	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
          	{
          		$no_absent_session = $absent_session[$i];
          		# Update reason table record with number of Absent Sessions
                $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                               ProfileRecordID = '$attendance_id',
                               RecordStatus = '$my_record_status',
                               AbsentSession = '$no_absent_session'
						   $extra_fields 
                               WHERE RecordID= '$reason_record_id'";
          	}else
          	{
                # Update reason table record
                $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                               ProfileRecordID = '$attendance_id',
                               RecordStatus = '$my_record_status'
						   $extra_fields 
                               WHERE RecordID= '$reason_record_id'";
          	}
              $lcardattend->db_db_query($sql);
          }
	
	# Update PMStatus if status is changed
	$sql = "SELECT PMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = $my_user_id";
          $arr_PMStatus = $lcardattend->returnArray($sql);
          $tmp_PMStatus = $arr_PMStatus[0][0];
          if($tmp_PMStatus != $my_drop_down_status){		## if different, then update
             $sql = "UPDATE $card_log_table_name
                            SET PMStatus = '".$my_drop_down_status."',
                            DateModified = NOW()
                            WHERE DayNumber = '$txt_day'
                                  AND UserID = '$my_user_id'";
             $lcardattend->db_db_query($sql);
          }
	
          # Add medical reason
          if ($sys_custom['hku_medical_research'])
          {
              # Try insert
              $sql = "INSERT IGNORE INTO SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                             (StudentID, RecordDate, DayType, MedicalReasonType, DateInput , DateModified)
                             VALUES
                             ('$my_user_id', '$TargetDate', '".PROFILE_DAY_TYPE_PM."',
                             '".${"medical_reason_type_".$my_user_id}."', now(),now())";
              $lcardattend->db_db_query($sql);
              if ($lcardattend->db_affected_rows()!=1)
              {
                  # Try update
                  $sql = "UPDATE SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                 SET MedicalReasonType = '".${"medical_reason_type_".$my_user_id}."'
                                 WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                 AND StudentID = '$my_user_id'
                                 ";
                  $lcardattend->db_db_query($sql);
              }
          }


      }
      else if ($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_OUTING)    # On Time or Outing
      {
           # Remove Reason record
           $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                         WHERE RecordDate = '$TargetDate'
                               AND StudentID = '$my_user_id'
                               AND DayType = '".PROFILE_DAY_TYPE_PM."'
                               AND RecordType = '".PROFILE_TYPE_ABSENT."'";
           $lcardattend->db_db_query($sql);
           # Remove Profile record
           $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                         WHERE AttendanceDate = '$TargetDate'
                               AND UserID = '$my_user_id'
                               AND DayType = '".PROFILE_DAY_TYPE_PM."'
                               AND RecordType = '".PROFILE_TYPE_ABSENT."'";
           $lcardattend->db_db_query($sql);
           # Set to Daily Record Table
           # Set PMStatus
           ## First - Check the selected status is different from the old one or not
           $sql = "SELECT PMStatus FROM $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = '$my_user_id'";
           $arr_PMStatus = $lcardattend->returnVector($sql);
           $tmp_PMStatus = $arr_PMStatus[0];
           if($tmp_PMStatus != $my_drop_down_status){		## if different, then update
             $sql = "UPDATE $card_log_table_name
                            SET PMStatus = '".$my_drop_down_status."',
                            DateModified = NOW()
                            WHERE DayNumber = '$txt_day'
                                  AND UserID = '$my_user_id'";
             $lcardattend->db_db_query($sql);
           }

          if($my_drop_down_status== CARD_STATUS_PRESENT){ ## Bad Action

                     $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
                     # forgot to bring card
                     $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
               }

           # Add medical reason
           if ($sys_custom['hku_medical_research'])
           {
              # Try remove
              $sql = "DELETE FROM SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON
                                 WHERE RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                 AND StudentID = '$my_user_id'
                                 ";
              $lcardattend->db_db_query($sql);

           }

      }
      else # Unknown action
      {
           # Do nthg
      }

  }
  else # Unknown action
  {
         # Do nthg
  }
*/
} # End of For-Loop

## mark as confirmed by admin user
if(count($user_id)>0){
	$confirmed_by_admin_field = $DayType == PROFILE_DAY_TYPE_PM? "PMConfirmedByAdmin" : "AMConfirmedByAdmin";
	$sql = "UPDATE $card_log_table_name SET $confirmed_by_admin_field='1' WHERE DayNumber='$txt_day' AND UserID IN (".implode(",",$user_id).")";
	$lcardattend->db_db_query($sql);
}


if ($lcardattend->attendance_mode=="NO")
{
	$lcardattend->retrieveSettings();
}

$need2mail = false;
if ($DayType==PROFILE_DAY_TYPE_AM)
{
	$period_type = PROFILE_DAY_TYPE_AM;
	# Send email
	if ($sys_custom['hku_medical_research'])
	{
    if ($sys_custom['hku_medical_research_force_send'] || $lcardattend->attendance_mode==0)
    {
      $need2mail = true;
      $mail_success = $lcardattend->sendMedicalReport($TargetDate);
    }
	}
}
else if ($DayType==PROFILE_DAY_TYPE_PM)
{
	$period_type = PROFILE_DAY_TYPE_PM;
	# Send email
	if ($sys_custom['hku_medical_research'])
	{
    $need2mail = true;
    $mail_success = $lcardattend->sendMedicalReport($TargetDate);
	}
}
else
{
	$period_type = "";
}
if ($period_type != "")
{
  # Update Confirm Record
  $sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
         SET AbsenceConfirmed = 1, AbsenceConfirmTime = now(), AbsenceConfirmUserID='$UserID', DateModified = now()
         WHERE RecordDate = '$TargetDate' AND RecordType = '$period_type'";
  $lcardattend->db_db_query($sql);
  if ($lcardattend->db_affected_rows()!=1)         # Not Exists
  {
    # Not exists
    $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, AbsenceConfirmed, AbsenceConfirmUserID, RecordType, AbsenceConfirmTime, DateInput, DateModified)
                   VALUES ('$TargetDate',1,'$UserID','$period_type',now(),now(),now())";
    $lcardattend->db_db_query($sql);
  }
}

if ($need2mail)
{
  if ($mail_success)
  {
  	$mail_msg= "&mail_msg=1";
  }
  else
  {
    $mail_msg = "&mail_msg=2";
  }
}

$lcardattend->log($log_row);

intranet_closedb();

$Msg = $Lang['StudentAttendance']['AbsenceListConfirmSuccess'];
header("Location: show.php?DayType=".urlencode($DayType)."&TargetDate=".urlencode($TargetDate)."&Msg=".urlencode($Msg).$mail_msg);
?>