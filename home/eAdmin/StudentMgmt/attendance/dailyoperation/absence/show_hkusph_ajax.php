<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");

intranet_auth();
intranet_opendb();

$db = new libHKUFlu_db();

$hkuFlu = new HKUFlu();
$sick_code = $hkuFlu->getSickCode();
$symptom_code = $hkuFlu->getSymptomCode();

$preset_leave_id = $_POST['preset_leave_id'];
$sick_leave_id = $_POST['sick_leave_id'];
$have_record = $_POST['have_record'];
$sick_date = $_POST['sick_date'];
$array_sick_index = explode(';', $_POST['sick_index_list']);
$array_symptom_index = explode(';', $_POST['symptom_index_list']);
$other_sick = $_POST['sick_other'];
$other_symptom = $_POST['symptom_other'];
$student_id = $_POST['student_id'];
$leave_date = $_POST['leave_date'];

$array_sick_code = array();
foreach((array)$array_sick_index as $index){
	array_push($array_sick_code,$sick_code[$index]);
}

$array_symptom_code = array();
foreach((array)$array_symptom_index as $index){
	array_push($array_symptom_code,$symptom_code[$index]);
}

if($_POST['preset_leave_id'] != ''){
	if($have_record == 'T'){
		$sick_leave_id = $db->editSickLeaveByPresetLeave($preset_leave_id, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave = true, $from = 'T');
	}else{
		$sick_leave_id = $db->addSickLeaveByPresetLeave($preset_leave_id, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave = true, $from = 'T');
	}
	
	echo $preset_leave_id;
	
}elseif($_POST['sick_leave_id'] != ''){
	$sick_leave_id = $db->editSickLeaveBySickLeaveID($sick_leave_id, $eClass_leave_id = null, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave = true, $from = 'T');
	
	echo 's'.$sick_leave_id;
}else{
	$sick_leave_id = $db->addSickLeave('', '', '', $student_id, $sick_date, $leave_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $from = 'T');
	echo 's'.$sick_leave_id;
}

?>