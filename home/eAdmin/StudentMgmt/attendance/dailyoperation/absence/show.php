<?php
// Using by:

##################################### Change Log #####################################################
# 2019-09-23 by Ray: Add preset absent tooltip mouse over not close
# 2019-06-27 Ray: Add teacher remark preset select box
# 2018-03-26 Bill: Added checking for valid notice templates > show / hide Send Notice function ($haveInUseAbsentTemplates)    [2017-1201-0905-54066]
# 2017-07-20 Paul: Added new flag $sys_custom['attendance_absence_hide_notice'] to hide send notice function
# 2017-04-11 Carlos: Added New absent student function.
# 2016-08-26 Bill: Add new column Processed by School & Add preset selection for column Teacher's remarks
# 2016-08-15 Henry HM: Add column for HKUSPH
# 2015-06-18 Carlos: Do not display the Send SMS(no template) tool button if flag is off.
# 2015-06-05 Omas: Add Export Mobile Button
# 2015-04-23 Carlos: Fix office remark array index shift problem after disable the input. 
# 2015-02-11 Carlos: Confirm time added confirm user name. Can edit teachers' remark. Added office remark. Added column [Last Modify].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
# 2014-10-09 Roy:	Added send push message option
# 2014-09-05 Bill:	 Add apply all to attendance status, fix short cut problem
# 2014-07-24 Carlos: Change Period selection to radio checkboxes
# 2014-03-17 Carlos: Added [Waive Absence] checkbox for $sys_custom['StudentAttendance_WaiveAbsent']
# 2013-04-16 Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2012-12-13 YatWoon: Add remark for school absenteeism monitoring ($sys_custom['hku_medical_research'])
# 2011-12-21 Carlos: Added data column Gender
# 2011-11-04 Carlos: Hide waive checkbox if toggle to On Time
#
#	Date:	2010-04-14	YatWoon
#			update send sms icon coding (problem: display no sms template alert even template is set in admin console
#
# 2010-03-25 Carlos: Customization - Added textbox Remark and checkbox Has handin parent letter
#
######################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if($sys_custom['eClassApp']['HKUSPH'])
{
	include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
	include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
	
	$db_hkuFlu = new libHKUFlu_db();
	$hkuFlu = new HKUFlu();
}

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

### Short-cut for Daily Late / Daily Absent/ Daily Early Leave ###
/*
$arr_page = array(
					array(1,$i_SmartCard_DailyOperation_ViewClassStatus),
					array(2,$i_SmartCard_DailyOperation_ViewLateStatus),
					array(3,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus)
				);
$pageShortCut_Selection = getSelectByArray($arr_page," name=\"pageShortCut\" onChange=\"changePage(this.value)\" ");
*/

$TargetDate = isset($_POST['TargetDate'])? $_POST['TargetDate'] : $_GET['TargetDate'];
$DayType = isset($_POST['DayType'])? $_POST['DayType'] : $_GET['DayType'];

$lc = new libcardstudentattend2();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewAbsenceStatus";

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();
$luser = new libuser();

### Class used
$lword = new libwordtemplates();

$lc->retrieveSettings();
if ($TargetDate == "")
{
	$TargetDate = date('Y-m-d');
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### Period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM:
        $display_period = $i_DayTypeAM;
        break;
    case PROFILE_DAY_TYPE_PM:
      	$display_period = $i_DayTypePM;
        break;
    default :
      	if ($lc->attendance_mode == 1) {
      		$display_period = $i_DayTypePM;
      		$DayType = PROFILE_DAY_TYPE_PM;
      	}
      	else {
        	$display_period = $i_DayTypeAM;
    	    $DayType = PROFILE_DAY_TYPE_AM;
        }
    break;
}

# Get student array with parent using parent app
$studentWithParentUsingParentAppAry = $luser->getStudentWithParentUsingParentApp();

# [2017-1201-0905-54066] Check any In Use Notice Template
$haveInUseAbsentTemplates = false;
if($plugin['notice'] && !$sys_custom['attendance_absence_hide_notice'])
{
    $TemplateList = $StudentAttendUI->Get_eNotice_Template_By_Category("Absent");
    $haveInUseAbsentTemplates = !empty($TemplateList);
}

# Get Confirmation status
$confirmNameField = getNameFieldByLang2("u.");
$sql = "select c.RecordID, c.AbsenceConfirmed, c.AbsenceConfirmTime, $confirmNameField as ConfirmUser FROM CARD_STUDENT_DAILY_DATA_CONFIRM as c 
		left join INTRANET_USER as u ON u.UserID=c.AbsenceConfirmUserID 
               WHERE c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND c.RecordType = '".$lc->Get_Safe_Sql_Query($DayType)."'";
$temp = $lc->returnArray($sql,4);
list ($recordID, $confirmed , $confirmTime, $confirmUser) = $temp[0];

########################## Up to here

$col_width = 150;
$table_confirm = "";
$col_span = 10;
/*
$x  = "<form name=\"form3\" id=\"form3\" method=\"post\" action=\"\" style=\"margin: 0px; padding: 0px;\">";
$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$i_StudentAttendance_Field_Date:</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$linterface->GET_DATE_PICKER("TargetDate", $TargetDate,"","yy-mm-dd","Mask_Confirmed_Date")."</td></tr>";
$x .= '<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
				<td class="tabletext" width="70%">
					<table border=0>
						<tr>
							<td class="dynCalendar_card_not_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$i_StudentAttendance_CalendarLegend_NotConfirmed.'
							</td>
							<td class="dynCalendar_card_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$i_StudentAttendance_CalendarLegend_Confirmed.'
							</td>
						</tr>
					</table>
				</td>
			</tr>';
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Slot:</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">$selection_period</td></tr>\n";

if($confirmed==1)
{
        $x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_LastConfirmedTime:</td>";
        $x .= "<td class=\"tabletext\" width=\"70%\">$confirmTime</td></tr>";
}
else
{
        $x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"></td>";
        $x .= "<td class=\"tabletext\" width=\"70%\">$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
$x .= "</table>";
$x .= "<input type=\"hidden\" name=\"flag\" value=\"1\">";
$x .= "</form>";
*/
$table_confirm = $StudentAttendUI->Get_Confirm_Selection_Form("form3","",PROFILE_TYPE_ABSENT,$TargetDate,$DayType,1);

### Build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM absent
{
	$FieldList = "b.InSchoolTime,
          			IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
          			b.AMStatus,";
  $StatusField = 'b.AMStatus';
}
else     # Check PM Late
{
	$FieldList = "b.LunchBackTime,
			          IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
			          b.PMStatus,";
	$StatusField = 'b.PMStatus'; 
	$AMReasonJoinSql = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e 
											ON 
												e.StudentID = a.UserID 
												AND e.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
												AND e.DayType = '".PROFILE_DAY_TYPE_AM."'
												AND e.RecordType = '".PROFILE_TYPE_ABSENT."' ";
}
$AbsExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'c.','e.','d.','f.');
$OutExpectedReasonField = $lc->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'c.','','d.','g.',true);

$LastModifyUserNameField = getNameFieldByLang2("u.");

$sql  = "SELECT 
				  f.RecordID as CardStudentPresetLeaveID,
				  b.RecordID, a.UserID,
		          ".getNameFieldWithClassNumberByLang("a.")." as name,
				  a.Gender,
		          ".$FieldList."
		          c.Reason,
		          c.RecordStatus,
		          d.Remark,
				  c.Remark as AbsenceRemark,
				  IF(c.OfficeRemark IS NULL AND TRIM(f.Remark)!='',f.Remark,c.OfficeRemark) as OfficeRemark,
				  c.HandinLetter, 
				  IF (".$StatusField." = '".CARD_STATUS_ABSENT."',".$AbsExpectedReasonField.",".$OutExpectedReasonField.") as ExpectedReason, 
				  f.Waive as PresetWaive, 
				  c.eNoticeID,
				  c.DocumentStatus as DocumentStatus,
				  c.DateModified, 
				  $LastModifyUserNameField as LastModifyUser ";
if($sys_custom['StudentAttendance_WaiveAbsent']) {
	$sql .= "     ,c.WaiveAbsent ";
}
$sql.= "		  ,c.Processed as processedby
		FROM
	                $card_log_table_name as b
					LEFT JOIN INTRANET_USER as a 
					ON b.UserID = a.UserID
					LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
					ON c.StudentID = a.UserID 
						AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
						AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' 
						AND c.RecordType = '".PROFILE_TYPE_ABSENT."'
					LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
					ON (b.UserID = d.StudentID AND d.RecordDate='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND d.DayType='".$lc->Get_Safe_Sql_Query($DayType)."') 
					".$AMReasonJoinSql." 
					LEFT JOIN CARD_STUDENT_PRESET_LEAVE f 
					ON 
						f.StudentID = a.UserID 
						AND f.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
						AND f.DayPeriod = '".$lc->Get_Safe_Sql_Query($DayType)."' 
					LEFT JOIN CARD_STUDENT_OUTING g 
					ON 
						g.UserID = a.UserID 
						AND g.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
						AND g.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' 
					LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifyBy 
        WHERE 
        	b.DayNumber = '$txt_day'
            AND (".$StatusField." = '".CARD_STATUS_ABSENT."' OR ".$StatusField." = '".CARD_STATUS_OUTING."')
            AND a.RecordType = 2 
            AND a.RecordStatus IN (0,1,2)
        ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName ";
$result = $lc->returnArray($sql,12);
$StudentIDArr = Get_Array_By_Key($result,'UserID');

# Get Preset absence info
$AbsentStudentAry = array();
foreach ($result as $resultAry){
	$AbsentStudentAry[] = $resultAry['UserID'];
}
$sql = "SELECT 
			StudentID,
			DayPeriod,
			Reason,
			DocumentStatus,
			Remark,
			Waive
		FROM 
			CARD_STUDENT_PRESET_LEAVE 
		WHERE 
			StudentID IN (".implode(',', IntegerSafe($AbsentStudentAry)).") AND
			DayPeriod = '".$lc->Get_Safe_Sql_Query($DayType)."' AND
			( '".$lc->Get_Safe_Sql_Query($TargetDate)."' = RecordDate )";
$PresetAbsentStudentRecordAry =  $lc->returnResultSet($sql);
$PresetAbsentStudentAssoAry = BuildMultiKeyAssoc($PresetAbsentStudentRecordAry,'StudentID');
$PresetAbsentStudentAry = Get_Array_By_Key($PresetAbsentStudentRecordAry,'StudentID');

//debug_pr($sql);
//debug_pr($AbsentStudentAry);

$table_attend = "";
$words_absence = $lword->getWordListAttendance(1);
$AbsentHasWord = sizeof($words_absence)!=0;
$words_late = $lword->getWordListAttendance(2);
$LateHasWord = sizeof($words_late)!=0;
foreach($words_absence as $key=>$word)
	$words_absence[$key]= htmlspecialchars($word);
foreach($words_late as $key=>$word)
	$words_late[$key]= htmlspecialchars($word);
$reason_js_array = "";

// Get Outing Reason
$words_outing = $lc->Get_Outing_Reason();
foreach($words_outing as $key=>$word) 
	$words_outing[$key] = htmlspecialchars($word);

// Get preset Teacher's Remark from txt file
/*$fs = new libfilesystem();
$words_remarks = array();
$file_path = $intranet_root."/file/student_attendance/preset_absent_reason2.txt";
if (file_exists($file_path))
{
	// Get data
	$data = trim($fs->file_read($file_path));
	if ($data != "")
	{
		$data = explode("\n", $data);
		for ($i=0; $i<sizeof($data); $i++)
		{
			if(trim($data[$i])!="")
				$words_remarks[] = trim($data[$i]);
		}
	}
}*/
# Teacher Remark Preset
$words_remarks = array();
$teacher_remark_js = "";
$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
    $teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
    foreach($teacher_remark_words as $key=>$word)
        $words_remarks[$key]= htmlspecialchars($word);
}


$x = $linterface->CONVERT_TO_JS_ARRAY($words_absence, "AbsentArrayWords", 1, 1);
$x .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "LateArrayWords", 1, 1);
$x .= $linterface->CONVERT_TO_JS_ARRAY($words_outing, "OutingArrayWords", 1, 1);
$x .= $linterface->CONVERT_TO_JS_ARRAY($words_remarks, "RemarksArrayWords", 1, 1);

$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td class=\"tabletoplink\" width=\"1\">#</td>";
$x .= "<td class=\"tabletoplink\" width=\"10%\">$i_UserName</td>";
$x .= "<td class=\"tabletoplink\" width=\"5%\">".$Lang['StudentAttendance']['Gender']."</td> ";
$x .= "<td class=\"tabletoplink\" width=\"5%\">$i_SmartCard_Frontend_Take_Attendance_Status
					<br>
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
		        		<option value=\"".CARD_STATUS_PRESENT."\">".$Lang['StudentAttendance']['Present']."</option>
		        		<option value=\"".CARD_STATUS_ABSENT."\" SELECTED>".$Lang['StudentAttendance']['Absent']."</option>
		        		<option value=\"".CARD_STATUS_OUTING."\">".$Lang['StudentAttendance']['Outing']."</option>
		        		<option value=\"".CARD_STATUS_LATE."\">".$Lang['StudentAttendance']['Late']."</option>
	        		</select>
					<br>
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();")."
				</td>";
$x .= "<td class=\"tabletoplink\" width=\"3%\" align=\"center\">$i_SmartCard_Frontend_Take_Attendance_Waived<br /><input type=\"checkbox\" name=\"all_wavied\" onClick=\"(this.checked)?setAllWaived(document.form1,1):setAllWaived(document.form1,0)\"></td>";

## Hand in prove document status
	$x .= "<td class=\"tabletoplink\" width=\"3%\" align=\"center\">".$Lang['StudentAttendance']['ProveDocument']."<br /><input type=\"checkbox\" name=\"master_Handin_prove\" value=\"1\" onClick=\"(this.checked)?setAllHandin(document.form1,1):setAllHandin(document.form1,0);\"></td>";
	$col_span+=1;

## Processed by School
	$x .= "<td class=\"tabletoplink\" width=\"3%\" align=\"center\">".$Lang['StudentAttendance']['ProcessedBySchool']."<br /><input type=\"checkbox\" name=\"processed_by_school\" value=\"1\" onClick=\"(this.checked)?setAllProcessed(document.form1,1):setAllProcessed(document.form1,0);\"></td>";
	$col_span+=1;

if($sys_custom['StudentAttendance_WaiveAbsent']){
	$x .= "<td class=\"tabletoplink\" width=\"3%\" align=\"center\">".$Lang['StudentAttendance']['WaiveAbsence']."<br /><input type=\"checkbox\" name=\"master_waive_absent\" value=\"1\" onClick=\"$('input[name*=waive_absent]').attr('checked',this.checked);\"></td>";
	$col_span+=1;
}

$x .= "<td class=\"tabletoplink\" width=\"10%\">
				$i_Attendance_Reason
				<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllReason\" id=\"SetAllReason\" maxlength=\"255\" value=\"\">
				".$linterface->GET_PRESET_LIST("AbsentArrayWords", "SetAllReasonIcon", "SetAllReason")."
				<br>
				".$linterface->Get_Apply_All_Icon("javascript:SetAllReason();")."
			</td>";

if($sys_custom['eClassApp']['HKUSPH']){
	$x .= '<td class="tabletoplink" width="10%">'.$Lang['HKUSPH']['NotASickLeave'].'</td>';
	$x .= '<td class="tabletoplink" width="10%">'.$Lang['HKUSPH']['Title'].'</td>';
}

// Added preset remark
$x .= "<td class=\"tabletoplink\" width=\"10%\">
				".$Lang['StudentAttendance']['iSmartCardRemark']."
				<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllRemark\" id=\"SetAllRemark\" maxlength=\"255\" value=\"\">
				".$linterface->GET_PRESET_LIST("RemarksArrayWords", "SetAllRemarkIcon", "SetAllRemark")."
				<br>
				".$linterface->Get_Apply_All_Icon("javascript:SetAllRemarks();")."
			</td>";

$x .= "<td class=\"tabletoplink\" width=\"10%\">".$Lang['StudentAttendance']['OfficeRemark']."</td>";
if($sys_custom['StudentAttendance_AbsenceRemark'])
{
	$x .= "<td class=\"tabletoplink\" width=\"10%\">".$Lang['StudentAttendance']['Notes']."</td>";
	$col_span+=1;
}
$x .= "<td class=\"tabletoplink\" width=\"3%\">$i_StudentGuardian[MenuInfo]</td>";

if($sys_custom['hku_medical_research'])
{
	$x .= "<td class=\"tabletoplink\" width=\"5%\">$i_MedicalReasonTitle</td>";
	
	# Retrieve Stored Reason
	# $DayType
	$sql  = "SELECT a.UserID, b.MedicalReasonType ";
	$sql .= "FROM INTRANET_USER as a ";
	$sql .= "LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as b ";
	$sql .= "ON b.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND b.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."' AND a.UserID = b.StudentID";
	
	$temp = $lc->returnArray($sql,2);
	$data_medical = build_assoc_array($temp);
	$col_span+=1;
}

if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2)
{
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	$lsms = new libsmsv2();
	
	//check sms system template status
	$template_status = $lsms->returnTemplateStatus("","STUDENT_ATTEND_ABSENCE");
	$template_content = $lsms->returnSystemMsg("STUDENT_ATTEND_ABSENCE");
	$template_status = $template_status and trim($template_content);
    
	if($template_status)
	{
		$x .= "<td class=\"tabletoplink\" align=\"center\" width=\"3%\">$i_SMS_Send<br /><input type='checkbox' name='all_sms' onClick=\"(this.checked)?setAllSend(document.form1,1):setAllSend(document.form1,0)\"></td>";
		$col_span+=1;
	}
}

if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
{
	$x .= "<td class=\"tabletoplink\" width=\"3%\">".$Lang['StudentAttendance']['HasHandinParentLetter']."<br /><input type=\"checkbox\" name=\"all_handin_letter\" onClick=\"(this.checked)?setAllHandinLetter(document.form1,1):setAllHandinLetter(document.form1,0)\"></td>";
	$col_span+=1;
}

if($plugin['notice'] && !$sys_custom['attendance_absence_hide_notice'] && $haveInUseAbsentTemplates) {
	$x .= "<td class=\"tabletoplink\" width=\"3%\">".$Lang['StudentAttendance']['SendNotice']."<br /><input type=\"checkbox\" name=\"all_send_notice\" onClick=\"$('.NoticeCheckbox').attr('checked',this.checked);\"></td>";
	$col_span+=1;
}

if ($plugin['eClassApp']) {
	$x .= "<td class=\"tabletoplink\" width=\"3%\">".$Lang['AppNotifyMessage']['PushMessage']."<br /><input type=\"checkbox\" name=\"all_send_push_message\" onClick=\"$('.PushMessageCheckbox').attr('checked',this.checked);\"></td>";
	$col_span+=1;
}

$x .= "<td class=\"tabletoplink\" width=\"10%\">".$Lang['StudentAttendance']['LastModify']."</td>";
$x .= "</tr>";

$select_word = "";
$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$benchmark['StartOfLoop_1'] = time();

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeof($result); $i++)
{
  list($preset_leave_id, $record_id, $user_id, $name, $gender, $in_school_time, $in_school_station, $old_status, $reason, $record_status, $remark, $absence_remark, $office_remark, $handin_letter, $ExpectedReason, $PresetWaive, $eNoticeID, $DocumentStatus, $date_modified, $last_modify_user) = $result[$i];
  
  if($sys_custom['StudentAttendance_WaiveAbsent']){
  	$waive_absent = $result[$i]['WaiveAbsent'];
  }
  
  $processBySchool = $result[$i]['processedby'];
  
  $css = ($i%2==0)?"tablerow1":"tablerow2";
  $css = (trim($record_status) == "")? $css:"attendance_norecord";
  $select_status = "<select id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"displayCheckbox(this.value,'$user_id'); setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i,$i)\">\n";
  $select_status .= "<option value=\"".CARD_STATUS_PRESENT."\">".$Lang['StudentAttendance']['Present']."</option>\n";
  $select_status .= "<option value=\"".CARD_STATUS_ABSENT."\"".($old_status==CARD_STATUS_ABSENT?" selected":"").">".$Lang['StudentAttendance']['Absent']."</option>\n";
  $select_status .= "<option value=\"".CARD_STATUS_OUTING."\"".($old_status==CARD_STATUS_OUTING?" selected":"").">".$Lang['StudentAttendance']['Outing']."</option>\n";
  $select_status .= "<option value=\"".CARD_STATUS_LATE."\">".$Lang['StudentAttendance']['Late']."</option>\n";
  $select_status .= "</select>\n";
  $select_status .= "<input name=\"record_id[$i]\" type=\"hidden\" value=\"$record_id\">\n";
  $select_status .= "<input name=\"user_id[$i]\" type=\"hidden\" value=\"$user_id\">\n";
	
	//$tmp_reason = ($reason=="" && $record_status=="")? $remark:$reason;
	$tmp_reason = $ExpectedReason;
  $reason_comp = "<input class=\"textboxnum\" type=\"text\" name=\"reason$i\" id=\"reason$i\" maxlength=\"255\" value=\"".htmlspecialchars($tmp_reason,ENT_QUOTES)."\" ".(($old_status!=CARD_STATUS_ABSENT && $old_status!=CARD_STATUS_OUTING)?"style=\"background:$disable_color\" DISABLED=true":"style=\"background:$enable_color\"").">";
  
  if ($AbsentHasWord)
  {
    if($remark!="")
    	$select_word = "<a onMouseMove=\"overhere()\" href=javascript:showSelection($i,document.form1.editAllowed$i.value,'$remark')><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a>";
    else
    	$select_word = "<a onMouseMove=\"overhere()\" href=javascript:showSelection($i,document.form1.editAllowed$i.value,'')><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a>";
    	
  }
 	
//   $disabled = $old_status==CARD_STATUS_OUTING ? "disabled" : "";
  	$waived_option = $old_status!=CARD_STATUS_OUTING ? "<input type=\"checkbox\" name=\"waived_{$user_id}\"".(($record_status == 1 || ($record_status == "" && $PresetWaive == 1)) ? " CHECKED" : " ") .">" : "";

	if( in_array($user_id, $PresetAbsentStudentAry) && ($PresetAbsentStudentAssoAry[$user_id]['Waive'] != $record_status)){
	    $displayWaivedOption = "<td class=\"tabletext\" align=\"center\"><span class=\"tabletextrequire\" style=\"height:100%; vertical-align:top;\">*</span><span id=\"cb_{$user_id}\">$waived_option</span></td>";
	} else {
		$displayWaivedOption = "<td class=\"tabletext\" align=\"center\"><span id=\"cb_{$user_id}\">$waived_option</span></td>";
	}

##hand in prove document status(start)
  	if($DocumentStatus == 1){
  		$dbValueDefaultCheck = "checked";
  	}
  	else{
  		$dbValueDefaultCheck = "";
  	}
  	if (in_array($user_id, $PresetAbsentStudentAry)){
  		$displayDocumentStatus = ($PresetAbsentStudentAssoAry[$user_id]['DocumentStatus'] == '1')? $Lang['General']['Yes'] : $Lang['General']['No'] ;
  		
  		# Preset Absence
  		$preset_table = "<table width=\"200\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\"><tr>";
		$preset_table .= "<tr class=\"tablebluetop\">";
		$preset_table .= "<td class=\"tabletoplink\">$i_SmartCard_DailyOperation_Preset_Absence</td></tr>";
		$preset_table .= "<tr class=\"tablebluerow1\"><td class=\"tabletext\" valign=\"top\">$i_UserName: $name </td></tr>";
		$preset_table .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" valign=\"top\">$i_Attendance_DayType: ".$display_period."</td></tr>";
		$preset_table .= "<tr class=\"tablebluerow1\"><td class=\"tabletext\" valign=\"top\">$i_Attendance_Reason: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$PresetAbsentStudentAssoAry[$user_id]['Reason'])."</td></tr>";
		$preset_table .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" valign=\"top\">".$Lang['StudentAttendance']['ProveDocument'].": ".$displayDocumentStatus."</td></tr>";
		$preset_table .= "<tr class=\"tablebluerow1\"><td class=\"tabletext\" valign=\"top\">$i_Attendance_Remark: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$PresetAbsentStudentAssoAry[$user_id]['Remark'])."</td></tr>";
		$preset_table .= "</table>";
		$preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
		$preset_abs_info = "<img onMouseOut=\"hidePresetAbs()\" onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" >";
  	}
  	else{
  		$preset_abs_info ="";
  	}
  	
  	// Update Pocessed by School when checked or unchecked
  	$HandIn_prove = $old_status!=CARD_STATUS_OUTING ? "<input type=\"checkbox\" name=\"HandIn_prove_{$user_id}\" value=\"1\" onClick=\"(this.checked)?document.form1.processed_by_{$user_id}.checked=true:document.form1.processed_by_{$user_id}.checked=false\" $dbValueDefaultCheck>".$preset_abs_info : "";
  	
  	if( in_array($user_id, $PresetAbsentStudentAry) && ($PresetAbsentStudentAssoAry[$user_id]['DocumentStatus'] != $DocumentStatus)){
  		$displayHandinProve = "<td class=\"tabletext\" align=\"center\"><span class=\"tabletextrequire\" style=\"height:100%; vertical-align:top;\">*</span><span id=\"cc_{$user_id}\">$HandIn_prove</span></td>";
  	}
  	else{
  		$displayHandinProve = "<td class=\"tabletext\" align=\"center\"><span id=\"cc_{$user_id}\">$HandIn_prove</span></td>";
  	}
##hand in prove document status(end)  	
  	
	// Processed by School
  	if($processBySchool == 1){
  		$processBySchoolChecked = "checked";
  	}
  	else{
  		$processBySchoolChecked = "";
  	}
  	$ProcessBy_School = $old_status!=CARD_STATUS_OUTING ? "<input type=\"checkbox\" name=\"processed_by_{$user_id}\" value=\"1\" $processBySchoolChecked>" : "";
  	$displayProcessBySchool = "<td class=\"tabletext\" align=\"center\"><span id=\"cd_{$user_id}\">$ProcessBy_School</span></td>";
  	
  $sms_option = "<input type=\"checkbox\" name=\"sms_{$user_id}\">";
  
  if($sys_custom['StudentAttendance_AbsenceRemark']) $txt_absence_remark = "<input type=\"text\" name=\"absence_remark[]\" id=\"absence_remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($absence_remark, ENT_QUOTES)."\" >";
  if($sys_custom['StudentAttendance_AbsenceHandinLetter']) $cb_handin_letter = "<input type=\"checkbox\" name=\"handin_letter_{$user_id}\" id=\"handin_letter$i\" value=\"1\" ".($handin_letter==1?" CHECKED ":"").">";
  
  $x .= "<tr class=\"$css\">";
  $x .= "<td class=\"tabletext\">".($i+1)."</td>";
  $x .= "<td class=\"tabletext\">$name</td>";
  $x .= "<td class=\"tabletext\">".$gender_word[$gender]."</td>";
  $x .= "<td class=\"tabletext\">$select_status</td>";
  //$x .= "<td class=\"tabletext\" align=\"center\"><span id=\"cb_{$user_id}\">$waived_option</span></td>";
  $x .= $displayWaivedOption;
  $x .= $displayHandinProve; // prove document status
  $x .= $displayProcessBySchool;
  
  if($sys_custom['StudentAttendance_WaiveAbsent']){
  	$x .= "<td class=\"tabletext\" align=\"center\"><input type=\"checkbox\" name=\"waive_absent".$user_id."\" id=\"waive_absent".$user_id."\" value=\"1\" ".($waive_absent==1?"checked":"")." ".($old_status!=CARD_STATUS_ABSENT?'style="display:none;"':'')." /></td>";
  }
  
  $x .= "<td class=\"tabletext\" nowrap>$reason_comp";
  $x .= $linterface->GET_PRESET_LIST("getReasons($i)", $i, "reason$i");
  $x .= "</td>";
			
	if($sys_custom['eClassApp']['HKUSPH']){
		$preset_leave_without_sick_leave = false;
		if($preset_leave_id != ''){
			$rows_full_sick_leave = $db_hkuFlu->getSickLeaveByPresentLeaveID($preset_leave_id, $include_not_sick_leave = true);
			if(count($rows_full_sick_leave)<=0){
				$rows_full_sick_leave = $db_hkuFlu->getStandAloneSickLeaveByStudentIDAndDate(array($user_id), $TargetDate);
				if(count($rows_full_sick_leave)>0){
					$preset_leave_without_sick_leave = true;
				}
			}
		}else{
			$rows_full_sick_leave = $db_hkuFlu->getStandAloneSickLeaveByStudentIDAndDate(array($user_id), $TargetDate);
		}
		
		$link_innerHTML = '<img src="/images/icon_web_hkusph.png">';
		
		$have_record = false;
		$html_cell = '';
		$is_sick_leave = true;
		
		foreach((array)$rows_full_sick_leave as $sick_leave_id => $row_full_sick_leave){
		
			$sick_string = '';
			$symptom_string = '';
			$have_record = false;
			
			foreach((array)$row_full_sick_leave['sick_leave_sick'] as $row_sick){
				$sick_string .= ($sick_string==''?'':', ') . $Lang['HKUSPH']['array_sick'][$hkuFlu->getIndexFromSickCode($row_sick['SickCode'])] . $row_sick['Other'];
			}
			
			foreach((array)$row_full_sick_leave['sick_leave_symptom'] as $row_symptom){
				$symptom_string .= ($symptom_string==''?'':', ') . $Lang['HKUSPH']['array_symptom'][$hkuFlu->getIndexFromSymptomCode($row_symptom['SymptomCode'])] . $row_symptom['Other'];
			}
			
			$have_record = true;
			
			if($preset_leave_id != '' && !$preset_leave_without_sick_leave){
				$btn = '<a href="#" class="tablelink hkuspl-add-sick '.$user_id.'" onclick="g_data={\'student_info\':\''.$name.'\',\'preset_leave_id\':\''.$preset_leave_id.'\',\'have_record\':\''.($have_record?'T':'F').'\',\'leave_date\':\''.$TargetDate.'\'};document.getElementById(\'dynSizeThickboxLink\').click();">'.$link_innerHTML.'</a>';
				$html_cell .= ($html_cell == ''?'':'<br/><br/>').'<a id="HKUFLU_'.$preset_leave_id.'"></a>' . $btn . '<b>' . ($sick_string == ''?'':('<br/>' . $Lang['HKUSPH']['Sick'] . ':')) . '</b>' . $sick_string . '<b>' . ($symptom_string == ''?'':('<br/>' . $Lang['HKUSPH']['Symptom'] . ':')) . '</b>' . $symptom_string . '';
			}else{
				$btn = '<a href="#" class="tablelink hkuspl-add-sick '.$user_id.'" onclick="g_data={\'student_info\':\''.$name.'\',\'sick_leave_id\':\''.$sick_leave_id.'\',\'have_record\':\''.($have_record?'T':'F').'\',\'leave_date\':\''.$TargetDate.'\'};document.getElementById(\'dynSizeThickboxLink\').click();">'.$link_innerHTML.'</a>';
				$html_cell .= ($html_cell == ''?'':'<br/><br/>').'<a id="HKUFLU_s'.$sick_leave_id.'"></a>' . $btn . '<b>' . ($sick_string == ''?'':('<br/>' . $Lang['HKUSPH']['Sick'] . ':')) . '</b>' . $sick_string . '<b>' . ($symptom_string == ''?'':('<br/>' . $Lang['HKUSPH']['Symptom'] . ':')) . '</b>' . $symptom_string . '';
			}
			
			$is_sick_leave = $row_full_sick_leave['sick_leave']['IsSickLeave'] == '1';
			
			break;
		}
		
		if(count($rows_full_sick_leave) <= 0){
			$btn = '<a href="#" class="tablelink hkuspl-add-sick '.$user_id.'" onclick="g_data={\'student_info\':\''.$name.'\',\'student_id\':\''.$user_id.'\',\'leave_date\':\''.$TargetDate.'\',\'have_record\':\''.($have_record?'T':'F').'\',\'preset_leave_id\':\''.$preset_leave_id.'\'};document.getElementById(\'dynSizeThickboxLink\').click();">'.$link_innerHTML.'</a>';
			$html_cell = $btn;
		}
		
		$html_hidden = '';
		$html_hidden .= '<input type="hidden" name="hkusph_have_record_'.$user_id.'" value="'.($have_record?'T':'F').'">';
		$html_hidden .= '<input type="hidden" name="hkusph_preset_leave_id_'.$user_id.'" value="'.$preset_leave_id.'">';
		$html_hidden .= '<input type="hidden" name="hkusph_sick_leave_id_'.$user_id.'" value="'.$sick_leave_id.'">';
		$html_hidden .= '<input type="hidden" name="hkusph_leave_date_'.$user_id.'" value="'.$TargetDate.'">';
		$html_hidden .= '<input type="hidden" name="hkusph_not_sick_leave_'.$user_id.'" value="'.($is_sick_leave?'F':'T').'">';
		$html_hidden .= '<input type="hidden" name="hkusph_preset_leave_without_sick_leave_'.$user_id.'" value="'.($preset_leave_without_sick_leave?'T':'F').'">';
				
		$x .= '<td class="tabletext"><input type="checkbox" class="hkuspl-not-sick" data-id="'.$user_id.'" data-have-record="'.($have_record?'T':'F').'" data-is-sick-leave="'.($is_sick_leave?'T':'F').'" name="not_a_sick_leave_'.$user_id.'" '.($is_sick_leave?'':' checked="checked" ').' value="1" />'.$html_hidden.'</td>';
		$x .= '<td class="tabletext">'.$html_cell.'</td>';
	}
  
  $remark_input = "<input type=\"text\" name=\"remark[]\" id=\"remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($remark, ENT_QUOTES)."\" />";
  $office_remark_input = "<input type=\"text\" name=\"office_remark$i\" id=\"office_remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($office_remark, ENT_QUOTES)."\" />";
  
  // Add preset remark to Teacher's remarks 
  $x .= '<td class="tabletext" nowrap>'.$remark_input;
  $x .= $linterface->GET_PRESET_LIST("getRemarks($i)", $i, "remark$i");
  $x .= '</td>';
  
  $x .= '<td class="tabletext">'.$office_remark_input.'</td>';
  if($sys_custom['StudentAttendance_AbsenceRemark']) $x .="<td class=\"tabletext\">$txt_absence_remark</td>";
  $x .= "<td class=\"tabletext\" align=\"center\">";
  $x .= "<a onMouseMove=\"moveObject('ToolMenu2', event);\" onmouseover=\"retrieveGuardianInfo($user_id);\" onmouseout=\"closeLayer('ToolMenu2');\" href=\"#\">";
  $x .= "<img src=\"$image_path/icons_guardian_info.gif\" border=\"0\" alt=\"$button_select\">";
  $x .= "</a></td>";
  
  if ($sys_custom['hku_medical_research']) {
		$x .= "<td class=\"tabletext\"><select name=\"medical_reason_type_".$user_id."\">";
		for ($j=0; $j<sizeof($i_MedicalReasonName); $j++) {
			list($t_reasonType, $t_reasonName) = $i_MedicalReasonName[$j];
			$t_selected_value = $data_medical[$user_id];
			$x .= "<option value='$t_reasonType' ".($t_selected_value==$t_reasonType?"SELECTED":"").">$t_reasonName</option>\n";
		}
		$x .= "</select></td>";
  }
  if ($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status)
  	$x .= "<td class=\"tabletext\" align=\"center\">$sms_option</td>";
  
  if($sys_custom['StudentAttendance_AbsenceHandinLetter']) $x .= "<td class=\"tabletext\">$cb_handin_letter</td>";
  if($plugin['notice'] && !$sys_custom['attendance_absence_hide_notice'] && $haveInUseAbsentTemplates) {
  	$x .= "<td class=\"tabletext\">";
  	if ($record_status != "") {
	  	if ($eNoticeID != "" && $eNoticeID != 0) {
	  		$x .= "<a title=\" ".$Lang['ePayment']['ViewNotice']."\" href=\"#\" onclick=\"newWindow('/home/eService/notice/sign.php?NoticeID=".$eNoticeID."',10); return false;\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0></a>";
	  	}
	  	else
	  		$x .= "<input type=\"checkbox\" name=\"SendNotice[]\" id=\"SendNotice_".$i."\" class=\"NoticeCheckbox\" value=\"".$user_id."\">";
  	}
  	else 
  		$x .= "&nbsp;";
  	$x .= "</td>";
  }
  
  if ($plugin['eClassApp']) {
  	$x .= "<td class=\"tabletext\">";
  	if (in_array($user_id, $studentWithParentUsingParentAppAry)) {
  		$x .= "<input type=\"checkbox\" name=\"SendPushMessage[]\" id=\"SendPushMessage_".$i."\" class=\"PushMessageCheckbox\" value=\"".$user_id."\">";
  	} else {
  		$x .= "---";
  	}
  	$x .= "</td>";
  }
  
  $x .= "<td class=\"tabletext\">";
  	if($date_modified != '' && $last_modify_user!=''){
  		$x .= $last_modify_user.$Lang['General']['On'].$date_modified;
  	}else{
  		$x .= Get_String_Display('');
  	}
  $x .= "</td>";
  $x .= "</tr>";
}

$benchmark['EndOfLoop_1'] = time();

if (sizeof($result)==0)
{
	$x .= "<tr class=\"tablerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"$col_span\" align=\"center\">$i_StudentAttendance_NoAbsentStudents</td></tr>\n";
}
$x .= "<tr><td class='tabletextremark' colspan = '5'><span>".$Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistency']."</span></td><tr>";
if($plugin['eClassApp']){
	$x .= '<tr><td class="tabletextremark" colspan = "'.$col_span.'"><span>'.$Lang['MessageCenter']['ExportMobileRemarks'].'</span></td></tr>';
}
$x .= "</table>\n";

$table_attend = $x;

// $template_status = false;

if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status) {
	$table_tool_sms .= "<a href=\"javascript:sendSMS(document.form1)\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" alt=\"$i_SMS_Send\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_SMS_Send
						</a>";
}
else if($sys_custom['send_sms'] && $plugin['sms'] && $bl_sms_version >= 2)
{
	if (!$template_status)
		$table_tool_sms .= "<a href=\"javascript:alert('".$Lang['StudentAttendance']['SMSTemplateNotSet']."')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\" />". $i_SMS_Send  ."</a>&nbsp;";
	else
		$table_tool_sms .= "<a href=\"javascript:Prompt_No_SMS_Plugin_Warning()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
}

if ($plugin['notice'] && !$sys_custom['attendance_absence_hide_notice'] && $haveInUseAbsentTemplates) 
{
	$table_tool_notice .= "<a href=\"#\" onclick=\"javascript:sendNotice(); return false;\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" alt=\"".$Lang['StudentAttendance']['SendNotice']."\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".$Lang['StudentAttendance']['SendNotice']."
						</a>";
}

if ($plugin['eClassApp']) {
	$table_tool_push_message .= "<a href=\"javascript:sendPushMessage()\" class=\"tabletool\">
					<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" alt=\"$i_SMS_Send\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
					".$Lang['AppNotifyMessage']['PushMessage']."
					</a>";
}

$table_tool = $table_tool_sms.$table_tool_notice.$table_tool_push_message;

$toolbar  = $linterface->GET_LNK_NEW("javascript:newWindow('insert_record.php?TargetDate=$TargetDate&DayType=$DayType',12)","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?TargetDate=$TargetDate&DayType=$DayType","","","","",0);
if($sys_custom['hku_medical_research']) {
	/*
	$exportMed = "<a class=\"contenttool\" href=\"export_medical_report.php?TargetDate=$TargetDate\">";
	$exportMed .= "<img src=\"".$PATH_WRT_ROOT."/images/2009a/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
	$exportMed .= $i_MedicalReason_Export;
	$exportMed .= "</a>";
	*/
	$toolbar .= $linterface->GET_LNK_EXPORT("export_medical_report.php?TargetDate=$TargetDate",$i_MedicalReason_Export,"","","",0);
}
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
if ($plugin['eClassApp']) {
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExportMobile()",$Lang['MessageCenter']['ExportMobile'],"","","",0)."&nbsp;&nbsp;";
}

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(intranet_htmlspecialchars(urldecode($Msg)));

$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

/*if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");

if ($mail_msg == 1) {
	$SysMsg .= $linterface->GET_SYS_MSG("", $i_MedicalDataTransferSuccess);
} else if ($mail_msg == 2) {
	$SysMsg .= $linterface->GET_SYS_MSG("", $i_MedicalDataTransferFailed);
}
if($medical_msg==1){
	$SysMsg = $linterface->GET_SYS_MSG("",$i_MedicalExportError);
}*/
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_connection.js"></script>
<style type="text/css">
#ToolMenu {
	position:absolute;
	top: 0px;
	left: 0px;
	z-index:4;
}

#ToolMenu2 {
	position:absolute;
	top: 0px;
	left: 0px;
	z-index:10;
}
</style>
<div id="ToolMenu" style="width:200; height:100; visibility:hidden"></div>
<div id="ToolMenu2" style="width:0px; height:0px; visibility:hidden"></div>
<div id="tooltip3" style="position:absolute; left:100px; top:30px; z-index:1; visibility: hidden"></div>
<script language="JavaScript" type="text/javascript">
<!--

// for Preset Absence
function showPresetAbs(obj,reason){
	var pos_left = getPostion(obj,"offsetLeft");
	var pos_top  = getPostion(obj,"offsetTop");
	
	offsetX = (obj==null)?0:obj.width;
	//offsetY = (obj==null)?0:obj.height;
	offsetY =0;
	objDiv = document.getElementById('tooltip3');
	if(objDiv!=null){
		objDiv.innerHTML = reason;
		objDiv.style.visibility='visible';
		objDiv.style.top = pos_top+offsetY+"px";
		objDiv.style.left = pos_left+offsetX+"px";
        $("#tooltip3").stop(true, true);
		setDivVisible(true, "tooltip3", "lyrShim");
	}
}
$(document).ready(function() {
    $("#tooltip3").hover(function() {
        $(this).stop(true).fadeTo(400, 1);
    }, function() {
        $("#lyrShim").hide();
        $("#tooltip3").fadeOut(400);
    });
});
// for preset absence
function hidePresetAbs(){
	obj = document.getElementById('tooltip3');
	//if(obj!=null)
	//	obj.style.visibility='hidden';
	//setDivVisible(false, "tooltip3", "lyrShim");
    $("#lyrShim").hide();
    $("#tooltip3").fadeOut(400);
}

function getReasons(pos)
{
	var jArrayTemp = new Array();
	var obj2 = document.getElementById("drop_down_status["+pos+"]");
	if (obj2.selectedIndex == 1)
	{
	  return AbsentArrayWords;
	} 
	else if (obj2.selectedIndex == 2) 
	{
		return OutingArrayWords;
	}
	else if (obj2.selectedIndex == 3)
	{
		return LateArrayWords;
	}
	else 
		return jArrayTemp;
}

// return preset Teacher's remarks 
function getRemarks(pos)
{
	return RemarksArrayWords;
}

function setAllWaived(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("waived_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}

function setAllHandin(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("HandIn_prove_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
	setAllProcessed(formObj,val)
}

function setAllSend(formObj,val) {
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
	  obj = formObj.elements[i];
	  if(typeof(obj)!="undefined"){
	    if(obj.name.indexOf("sms_")>-1){
	    	obj.checked = val==1?true:false;
	    }
	  }
	}
}

function setAllProcessed(formObj,val){
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("processed_by_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}

function sendSMS(formObj) {
	var sms_no=0;
	for(i=0;i<formObj.elements.length;i++) {
	  obj = formObj.elements[i];
	  if(typeof(obj)!="undefined") {
	    if(obj.name.indexOf("sms_")>-1) {
	    	if(obj.checked) sms_no = 1;
	    }
	  }
	}
	
	if(sms_no) {
	  formObj.action = "../confirm_send_sms.php";
	  formObj.submit();
	} else {
		alert("<?=$i_SMS_no_student_select?>");
	}
}

function sendNotice() {
	var SendNotice = document.getElementsByName('SendNotice[]');
	var SendNoticeChecked = false;
	for (var i=0; i< SendNotice.length; i++) {
		if (SendNotice[i].checked) {
			SendNoticeChecked = true;
			break;
		}
	}
	
	if (SendNoticeChecked == true) {
		document.getElementById('form1').action = "send_notice.php";
		document.getElementById('form1').submit();
	}
	else 
		alert("<?=$Lang['StudentAttendance']['CheckSendNotice']?>");
}

function sendPushMessage() {
	var SendPushMessage = document.getElementsByName('SendPushMessage[]');
	var SendPushMessageChecked = false;
	for (var i=0; i< SendPushMessage.length; i++) {
		if (SendPushMessage[i].checked) {
			SendPushMessageChecked = true;
			break;
		}
	}
	
	if (SendPushMessageChecked == true) {
		document.getElementById('form1').action = "../send_push_message.php";
		document.getElementById('form1').submit();
	}
	else 
		alert("<?=$i_SMS_no_student_select?>");
}

function openPrintPage()
{
    newWindow("show_print.php?DayType=<?=urlencode($DayType)?>&TargetDate=<?=urlencode($TargetDate)?>",4);
}

function showSelection(i, allowed, remark)
{
	if (allowed == 1)
	{
		writeToLayer('ToolMenu',temp_reasonSelection(i,remark));
		
		halfLayerHeight = parseInt(eval(doc + "ToolMenu" +".offsetHeight"))/2;
		if(mouse.y-halfLayerHeight >0)
			t = mouse.y-halfLayerHeight;
		else
			t = mouse.y-halfLayerHeight;
		
		l = mouse.x;
		moveToolTip2('ToolMenu', t, l);
		showLayer('ToolMenu');
	}
}

function getWindowWidth() {
  var myWidth = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
  } else if( document.documentElement && document.documentElement.clientWidth) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
  } else if( document.body && document.body.clientWidth) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
  }
  return myWidth;
}

function moveToolTip2(lay, FromTop, FromLeft){

   if (tooltip_ns6) {
     var myElement = document.getElementById(lay);
     myElement.style.left = (FromLeft + 10) + "px";
     myElement.style.top = (FromTop + document.body.scrollTop) + "px";
   } else if(tooltip_ie4) {
     eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
   } else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

   if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));
}

function putBack(obj, value)
{
   obj.value = value;
   hideMenu('ToolMenu');
}

function displayCheckbox(status, user_id)
{
	if(status==<?=CARD_STATUS_OUTING?> || status==<?=CARD_STATUS_PRESENT?> || status==<?=CARD_STATUS_LATE?>)	// hidden the checkbox
	{
		if(status!=<?=CARD_STATUS_LATE?>){
			eval("document.getElementById('cb_"+ user_id +"').innerHTML = '';");
		}
		eval("document.getElementById('cc_"+ user_id +"').innerHTML = '';");
		eval("document.getElementById('cd_"+ user_id +"').innerHTML = '';");
		<?php if($sys_custom['StudentAttendance_WaiveAbsent']){ ?>
			$('input#waive_absent'+user_id).hide();
		<?php } ?>
	}
	else
	{
		eval("document.getElementById('cb_"+ user_id +"').innerHTML = '<input type=\"checkbox\" name=\"waived_"+ user_id +"\">';");
		eval("document.getElementById('cc_"+ user_id +"').innerHTML = '<input type=\"checkbox\" name=\"HandIn_prove_"+ user_id +"\">';");
		eval("document.getElementById('cd_"+ user_id +"').innerHTML = '<input type=\"checkbox\" name=\"processed_by_"+ user_id +"\">';");
		<?php if($sys_custom['StudentAttendance_WaiveAbsent']){ ?>
			$('input#waive_absent'+user_id).show();
		<?php } ?>
	}
	
}

function setReasonComp(value, txtComp, hiddenFlag,i)
{
	linkObj = document.getElementById('poslink'+i);
	
	if (value==<?=CARD_STATUS_ABSENT?> || value==<?=CARD_STATUS_LATE?> || value==<?=CARD_STATUS_OUTING?> )
	{
		txtComp.disabled = false;
		txtComp.style.background='<?=$enable_color?>';
		hiddenFlag.value = 1;
		if(linkObj!=null){
			linkObj.style.visibility='visible';
		}
		if($('#office_remark'+i).length>0){
			$('#office_remark'+i).attr('disabled','');
		}
	<?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
   		absentSessionObj = document.getElementById('absent_session'+i);
   		if(absentSessionObj != null)
   		{
   			absentSessionObj.style.display = "inline";
   		}
    <?}?>
	}
	else
	{
		txtComp.disabled = true;
		txtComp.value="";
		txtComp.style.background='<?=$disable_color?>';
		hiddenFlag.value = 0;
		if(linkObj!=null){
			linkObj.style.visibility='hidden';
		}
		
		hideMenu('ToolMenu');
		
		if($('#office_remark'+i).length>0){
			$('#office_remark'+i).attr('disabled','disabled');
		}
  <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
   		absentSessionObj = document.getElementById('absent_session'+i);
   		if(absentSessionObj != null)
   		{
   			absentSessionObj.style.display = "none";
   		}
  <?}?>
	}
}

function resetForm(formObj){
	formObj.reset();
  	hideMenu('ToolMenu');
	resetFields(formObj);	
}

function resetFields(formObj){
        status_list = document.getElementsByTagName('SELECT');
        if(status_list==null) return;
		j =0;
        for(i=0;i<status_list.length;i++){
                s = status_list[i];
                if(s==null) continue;
                if(s.name.indexOf("drop_down_status")==-1) continue;
                 
               	reasonObj = eval('formObj.reason'+j);
               	linkObj = document.getElementById('poslink'+j);
               	j++;
                if(s.selectedIndex==1){
                        if(reasonObj!=null){
                                reasonObj.disabled=false;
                                reasonObj.style.background='<?=$enable_color?>';
                        }
                        if(linkObj!=null){
								linkObj.style.visibility='visible';
						}
						
					  <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
			           		absentSessionObj = document.getElementById('absent_session'+i);
			           		if(absentSessionObj != null)
			           		{
			           			absentSessionObj.style.display = "inline";
			           		}
		           	  <?}?>
                }else{
                        if(reasonObj!=null){
                                reasonObj.value="";
                                reasonObj.disabled=true;
                                reasonObj.style.background='<?=$disable_color?>';

                        }
                        if(linkObj!=null){
								linkObj.style.visibility='hidden';
						}
                        
                      <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
			           		absentSessionObj = document.getElementById('absent_session'+i);
			           		if(absentSessionObj != null)
			           		{
			           			absentSessionObj.style.display = "none";
			           		}
		           	  <?}?>
                }
                

        }
}
// AJAX follow-up
var callback = {
    success: function ( o )
    {
    	jChangeContent( "ToolMenu2", o.responseText );
    }
}
// start AJAX
function retrieveGuardianInfo(UserID)
{
    //FormObject.testing.value = 1;
    
    obj = document.form2;
    var myElement = document.getElementById("ToolMenu2");
    
    showMenu("ToolMenu2","<table border='0' width='300' cellpadding='3' cellspacing='0'><tr class='tablebluetop'><td class='tabletoplink'>Loading</td></tr></table>");
		myElement.style.display = 'block';
		myElement.style.visibility = 'visible';
		// fix the problem of the layer outside the screen
		document.getElementById("ToolMenu2").style.left = getWindowWidth()-(myElement.offsetWidth*2)+50;
    YAHOO.util.Connect.setForm(obj);
    
    var path = "getGuardianInfo.php?targetUserID=" + UserID;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

initialize = 1;
isToolTip = false;
isMenu = true;

function changePage(val)
{
	var obj = document.form1;
	var path = "/home/eAdmin/StudentMgmt/attendance/dailyoperation";
	var period = document.form1.period.value;
	var date = document.form1.TargetDate.value;
	
	if(val == 1){
		obj.action = path+"/class/class_status.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 2){
		obj.action = path+"/late/showlate.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 3){
		obj.action = path+"/early/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
}

function moveObject( obj, e, moveLeft ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 8;
	var objHolder = obj;
	var moveDistance = 0;
	
	obj = document.getElementById(obj);
	if (obj==null) {return;}
	
	if (document.all) {
		var ScrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
		var ScrollHeight = document.documentElement.scrollTop || document.body.scrollTop;
		tempX = event.clientX + ScrollLeft;
		tempY = event.clientY + ScrollHeight;
	}
	else {
		tempX = e.pageX
		tempY = e.pageY
	}

	if(moveLeft == undefined) {
		moveDistance = 300;		
	} else {
		moveDistance = moveLeft;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
}

function closeLayer(LayerID) {
	var obj = document.getElementById(LayerID);
	obj.style.display = 'none';
	obj.style.visibility = 'hidden';
}

function Check_Form() {
	if ($('#DPWL-TargetDate').html() == "") {
		document.getElementById('form3').submit();
	}
}

function SetAllReason() {
	var StatusSelection = document.getElementsByName("drop_down_status[]");
	var ApplyReason = document.getElementById("SetAllReason").value;
	for (var i=0; i< StatusSelection.length; i++) {
		if (StatusSelection[i].selectedIndex == 1) // absence
		{
		  document.getElementById('reason'+i).value = ApplyReason;
		} 
	}
}

function SetAllAttend() {
	var AttendValue = document.getElementById("drop_down_status_all").value;
	var AttendSelection = document.getElementsByName('drop_down_status[]').length;
	for (var i=0; i< AttendSelection; i++) {
 		document.getElementById('drop_down_status['+i+']').value = AttendValue;
 		
 		var targetUserID = document.getElementsByName('user_id['+i+']')[0].value;
 		var targetReasonEle = document.getElementById('reason'+i);
 		var targetEditEle = document.getElementsByName('editAllowed'+i)[0];
 		
 		displayCheckbox(AttendValue, targetUserID);
 		setReasonComp(AttendValue, targetReasonEle, targetEditEle, i);
	}
}
<?if($sys_custom['StudentAttendance_AbsenceHandinLetter']){?>
function setAllHandinLetter(formObj,val) {
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
	  obj = formObj.elements[i];
	  if(typeof(obj)!="undefined"){
	    if(obj.name.indexOf("handin_letter_")>-1){
	    	obj.checked = val==1?true:false;
	    }
	  }
	}
}
<?}?>

function SetAllRemarks() {
	var StatusSelection = document.getElementsByName("drop_down_status[]");
	var ApplyRemark = document.getElementById("SetAllRemark").value;
	for (var i=0; i< StatusSelection.length; i++) {
		document.getElementById('remark'+i).value = ApplyRemark;
	}
}

function goExportMobile(){
	var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php';
	$('form#form2').attr('action', url ).submit();
	$('form#form2').attr('action', '' );
}

<?php if($sys_custom['eClassApp']['HKUSPH']){ ?>
var g_page = 'show_hkusph.php';
var g_data = null;

function onloadThickBox(page, data){
	$('div#TB_ajaxContent').load(
		page, 
		data,
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

$(function(){
	$('.hkuspl-not-sick').bind('click',function(event){
		var id = $(this).attr('data-id');
		var have_record = $(this).attr('data-have-record');
		var is_sick_leave = $(this).attr('data-is-sick-leave');
		if($(this).is(':checked')){ //after click, T/F is inverted
		}else{
			if(have_record == 'T' && is_sick_leave == 'F'){
				event.preventDefault();
				$('.hkuspl-add-sick.'+id).trigger('click');
			}
		}
	});
});
<?php } ?>
-->
</script>
<?php echo $linterface->Include_Thickbox_JS_CSS(); ?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table_confirm?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<?
if($confirmed==1)
{
?>
        <tr>
        	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
        	<td class="tabletext" width="70%"><?=$confirmTime.($confirmUser!=''?'&nbsp;('.$confirmUser.')':'')?></td>
        </tr>
<?
}
else
{
?>
				<tr>
        	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
        	<td class="tabletext" width="70%"><?=$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record?></td>
        </tr>
<?
}
?>
			<?=$StudentAttendUI->Get_Class_Status_Shortcut(PROFILE_TYPE_ABSENT)?>
			</table>	
		</td>
	</tr>
</table>
<br />
<form name="form1" id="form1" method="post" action="show_update.php" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr class="table-action-bar">
					<td valign="bottom" align="left">
						<table border=0>
							<tr>
								<td class="attendance_norecord" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Confirmed']?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<td>
											<?=$table_tool?>
											</td>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$table_attend?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
				    <? if (sizeof($result)!=0) { ?>
				    	<? if($sys_custom['hku_medical_research']){?>
				    	<?="<br>".$Lang['StudentAttendance']['FluSurvRemark'] . "<br><br>"?>
				    	<? } ?>
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "AlertPost(document.form1,'show_update.php','$i_SmartCard_Confirm_Update_Attend?')", "btn_submit") ?>
					<?}?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm(document.form1)","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input id="DayType" name="DayType" type="hidden" value="<?=escape_double_quotes($DayType)?>">
<input name="TargetDate" type="hidden" value="<?=escape_double_quotes($TargetDate)?>">
<? for ($i=0; $i<sizeof($result); $i++) {
	$old_status = $result[$i][5];
?>
<input type="hidden" name="editAllowed<?=$i?>" value="<?=($old_status==CARD_STATUS_ABSENT?1:0)?>">
<? } ?>
<input type="hidden" name="this_page_title" value="<?=$i_SmartCard_DailyOperation_ViewAbsenceStatus?>">
<input type="hidden" name="this_page_nav" value="PageDailyOperation_ViewAbsenceStatus">
<input type="hidden" name="this_page" value="absence/show.php?TargetDate=<?=urlencode($TargetDate)?>&DayType=<?=urlencode($DayType)?>">
<input type="hidden" name="TemplateCode" value="STUDENT_ATTEND_ABSENCE">
<input name=PageLoadTime type=hidden value="<?=time()+1?>">
</form>
<form id="form2" name="form2" method="post" action="" >
<input name="TargetDate" type="hidden" value="<?=escape_double_quotes($TargetDate)?>">
<input type="hidden" name="StudentIDArr" value="<?=rawurlencode(serialize($StudentIDArr))?>">
</form>
<br />
<?php if($sys_custom['eClassApp']['HKUSPH']){ ?>
<a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);" onClick="load_dyn_size_thickbox_ip('<?php echo $Lang['HKUSPH']['Title']; ?>', 'onloadThickBox(g_page,g_data);',inlineID='', defaultHeight=500, defaultWidth=800)" style="display:none;">Dynamic size</a>
<?php } ?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>