<?php
// using by 
##################################### Change Log #####################################################
# 2015-02-11 Carlos: Added column [Office Remark]
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
# 2014-03-17 Carlos: Display [Waive Absence] for $sys_custom['StudentAttendance_WaiveAbsent']
# 2013-04-16 Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2013-03-11 Carlos: Add back teacher's remark field
# 2011-12-21 Carlos: Added data column Gender
# 2010-10-21 Kenneth chung: Customization - add user login on csv output - $sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin']
# 2010-03-25 Carlos: Customization - Added textbox Remark and checkbox Has handin parent letter
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: $display_period = $i_DayTypeAM;
            break;
  case PROFILE_DAY_TYPE_PM: $display_period = $i_DayTypePM;
            break;
  default : $display_period = $i_DayTypeAM;
            $DayType = PROFILE_DAY_TYPE_AM;
            break;
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
	$InSchoolTime = "InSchoolTime";
	$InSchoolStation = "InSchoolStation";
	$StatusField = "AMStatus";
}
else     # Check PM Late
{
	$InSchoolTime = "LunchBackTime";
	$InSchoolStation = "LunchBackStation";
	$StatusField = "PMStatus";
}

$sql  = "SELECT 
					a.UserLogin,
					b.RecordID, 
					a.UserID,
					".getNameFieldByLang("a.")."as name,
					a.ClassName,
					a.ClassNumber,
					a.Gender,
					b.".$InSchoolTime.",
					IF(b.".$InSchoolStation." IS NULL, '-', b.".$InSchoolStation."),
					b.".$StatusField.",
					c.Reason,
					c.RecordStatus,
					IF(b.".$StatusField." = '".CARD_STATUS_ABSENT."',IF(c.AbsentSession IS NULL, '-', c.AbsentSession),'-') as AbsentSession,
					d.Remark,
					IF(c.OfficeRemark IS NULL AND TRIM(f.Remark)!='',f.Remark,c.OfficeRemark) as OfficeRemark,
					c.Remark as AbsenceRemark,
					c.HandinLetter,
					IF(c.DocumentStatus =  1 AND c.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus 
					";
if($sys_custom['StudentAttendance_WaiveAbsent']){
	$sql .= ",c.WaiveAbsent ";
}
$sql .= "	FROM
					$card_log_table_name as b
					LEFT OUTER JOIN 
					INTRANET_USER as a 
					ON b.UserID = a.UserID
					LEFT OUTER JOIN 
					CARD_STUDENT_PROFILE_RECORD_REASON as c 
			    		ON c.StudentID = a.UserID 
			    			AND c.RecordDate = '$TargetDate' 
			    			AND c.DayType = $DayType
			     			 AND c.RecordType = '".PROFILE_TYPE_ABSENT."' 
					LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
						ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType') 
					LEFT JOIN CARD_STUDENT_PRESET_LEAVE f 
					ON 
						f.StudentID = a.UserID 
						AND f.RecordDate = '$TargetDate' 
						AND f.DayPeriod = '".$DayType."' 
				WHERE 
					b.DayNumber = '$txt_day' 
				  AND (b.".$StatusField." = '".CARD_STATUS_ABSENT."' OR b.".$StatusField." = '".CARD_STATUS_OUTING."') 
					AND a.RecordType = 2 AND a.RecordStatus IN (0,1) 
				ORDER BY 
					a.ClassName, a.ClassNumber, a.EnglishName";
$result = $lc->returnArray($sql,17);

$lexport = new libexporttext();
// prepare export header
if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
	$exportColumn[] = $i_UserLogin;
$exportColumn[] = $i_UserName;
$exportColumn[] = $i_ClassName;
$exportColumn[] = $i_UserClassNumber;
$exportColumn[] = $Lang['StudentAttendance']['Gender'];
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_Status;
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_Waived;
$exportColumn[] = $Lang['StudentAttendance']['ProveDocument'];
if($sys_custom['StudentAttendance_WaiveAbsent']){
	$exportColumn[] = $Lang['StudentAttendance']['WaiveAbsence'];
}
$exportColumn[] = $i_Attendance_Reason;
$exportColumn[] = $Lang['StudentAttendance']['iSmartCardRemark'];
$exportColumn[] = $Lang['StudentAttendance']['OfficeRemark'];
if($sys_custom['StudentAttendance_AbsenceRemark']) array_push($exportColumn, "$i_Attendance_Remark");
if ($sys_custom['hku_medical_research']) {
	$exportColumn[] = "$i_MedicalReasonTitle";
	# Retrieve Stored Reason
	# $DayType
	$sql1  = "SELECT a.UserID, b.MedicalReasonType ";
	$sql1 .= "FROM INTRANET_USER as a ";
	$sql1 .= "LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as b ";
	$sql1 .= "ON b.RecordDate = '$TargetDate' AND b.DayType = '$DayType' AND a.UserID = b.StudentID";
	
	$temp = $lc->returnArray($sql1,2);
	$data_medical = build_assoc_array($temp);
}
if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
	$exportColumn[] = $Lang['StudentAttendance']['HasHandinParentLetter'];
$exportColumn[] = $i_StudentGuardian['MenuInfo'];

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];	

// prepare export body
for($i=0; $i<sizeOf($result); $i++)
{
	list($UserLogin,$record_id, $user_id, $UserName,$class_name,$class_number, $gender, $in_school_time, 
			$in_school_station, $old_status, $reason,$record_status, $absent_session, $remark, $office_remark, 
			$absence_remark, $handin_letter, $DocumentStatus) = $result[$i];
	
	// get guardian info
	$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
	$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
	$sql = "SELECT
					  $main_content_field,
					  Relation,
					  Phone,
					  EmPhone,
					  IsMain
					FROM
					  $eclass_db.GUARDIAN_STUDENT
					WHERE
					  UserID = '".$user_id."'
					ORDER BY
					  IsMain DESC, Relation ASC
					";
	//debug_r($sql);
	$temp = $lc->returnArray($sql,4);
	if (sizeof($temp)==0)
		$GuardianInfo = "--";
	else
	{
		$GuardianInfo = "";
    for($j=0; $j<sizeof($temp); $j++)
    {
			list($name, $relation, $phone, $em_phone) = $temp[$j];
			$GuardianInfo .= ($j+1).". ".$name." (".$ec_guardian[$relation].") (".$i_StudentGuardian_Phone.") ".$phone." (".$i_StudentGuardian_EMPhone.") ".$em_phone." ";
	  }
	}
	
	$str_status = ($old_status==CARD_STATUS_ABSENT?$i_StudentAttendance_Status_Absent:$i_StudentAttendance_Status_Outing);
	$str_reason = $reason;
	$waived_option = $record_status == 1? $i_general_yes:$i_general_no;
	if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
		$ExportDetail[$i][] = $UserLogin;
	$ExportDetail[$i][] = $UserName;
	$ExportDetail[$i][] = $class_name;
	$ExportDetail[$i][] = $class_number;
	$ExportDetail[$i][] = $gender_word[$gender];
	$ExportDetail[$i][] = $str_status;
	$ExportDetail[$i][] = $waived_option;
	$ExportDetail[$i][] = $DocumentStatus;
	if($sys_custom['StudentAttendance_WaiveAbsent']){
		$ExportDetail[$i][] = $result[$i]['WaiveAbsent'] == 1? $Lang['General']['Yes'] : $Lang['General']['No'];
	}
	$ExportDetail[$i][] = $str_reason;
	$ExportDetail[$i][] = $remark;
	$ExportDetail[$i][] = $office_remark;
	if($sys_custom['StudentAttendance_AbsenceRemark']) 
		$ExportDetail[$i][] = $absence_remark;
	if ($sys_custom['hku_medical_research']) {
		for ($j=0; $j<sizeof($i_MedicalReasonName); $j++) {
			list($t_reasonType, $t_reasonName) = $i_MedicalReasonName[$j];
			$t_selected_value = $data_medical[$user_id];
			if ($t_selected_value==$t_reasonType) {
				$medical_reason = $t_reasonName;
				break;
			}
		}
		$ExportDetail[$i][] = $medical_reason;
	}
	if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
	{
		if($handin_letter == 1)		
			$ExportDetail[$i][] = $i_general_yes;
		else
			$ExportDetail[$i][] = $i_general_no;
	}
	$ExportDetail[$i][] = $GuardianInfo;
}

$export_content = $lexport->GET_EXPORT_TXT($ExportDetail, $exportColumn);

$export_content_final = $i_SmartCard_DailyOperation_ViewAbsenceStatus;
$export_content_final .= " $TargetDate ($display_period)\r\n";

if (sizeof($result)==0)
	$export_content_final .= "$i_StudentAttendance_NoAbsentStudents\r\n";
else
	$export_content_final .= $export_content;

intranet_closedb();

$filename = "showabsence-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>