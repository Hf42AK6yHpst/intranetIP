<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();
$limport = new libimporttext();
$lf = new libfilesystem();


$format_array = array("Class Name","Class Number","Date","Leave Type","Leave Sessions","Reason");
$filepath = $_FILES['userfile']['tmp_name'];
$filename = $_FILES['userfile']['name'];

if($filepath=="none" || $filepath == "")
{
	# import failed
    intranet_closedb();
    $_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
    header("Location:import.php");
    exit();
}

$ext = strtoupper($lf->file_ext($filename));
if($limport->CHECK_FILE_EXT($filename))
{
  # read file into array
  # return 0 if fail, return csv array if success
  //$data = $lf->file_read_csv($filepath);
  $data = $limport->GET_IMPORT_TXT($filepath);
  if(sizeof($data)>0)
  {
  		$toprow = array_shift($data);                   # drop the title bar
  }else
  {
  		$_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	    intranet_closedb();
	    header("Location: import.php");
	    exit();
  }
}
for ($i=0; $i<sizeof($format_array); $i++)
{
	 if ($toprow[$i] != $format_array[$i])
	 {
	 	 $_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	     intranet_closedb();
	     header("Location: import.php");
	     exit();
	 }
}

$data_size = count($data);
$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");

$sql = "CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_ATTENDANCE_IMPORT_LEAVE_RECORDS (
			RecordID int(11) NOT NULL auto_increment,
			SessionID varchar(300) NOT NULL,
			ImportTime datetime NOT NULL,
			RecordDate date NOT NULL,
			ClassName varchar(255) NOT NULL,
			ClassNumber varchar(255) NOT NULL,
			StudentID int(11) NOT NULL,
			LeaveType tinyint NOT NULL COMMENT 'Absent:3,Outing:1',
			LeaveSessions text DEFAULT NULL,
			Reason text NOT NULL,
			PRIMARY KEY(RecordID),
			INDEX IdxSessionID(SessionID),
			INDEX IdxImportTime(ImportTime) 
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";

$LessonAttendUI->db_db_query($sql);

$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
$sql = "DELETE FROM SUBJECT_GROUP_ATTENDANCE_IMPORT_LEAVE_RECORDS WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
$LessonAttendUI->db_db_query($sql);

$name_field = getNameFieldByLang2();
$sql = "SELECT UserID,ClassName,ClassNumber,$name_field as StudentName FROM INTRANET_USER 
		WHERE RecordType='".USERTYPE_STUDENT."' AND ClassName IS NOT NULL AND ClassNumber IS NOT NULL AND ClassName<>'' AND ClassNumber<>''";
$students = $LessonAttendUI->returnResultSet($sql);
$students_count = count($students);
$classToStudentMap = array();
$studentIdToStudentMap = array();
for($i=0;$i<$students_count;$i++){
	$classToStudentMap[$students[$i]['ClassName']][$students[$i]['ClassNumber']] = $students[$i];
	$studentIdToStudentMap[$students[$i]['UserID']] = $students[$i];
}

if($sys_custom['LessonAttendance_LaSalleCollege']){
	$settings = $LessonAttendUI->Get_General_Settings('LessonAttendance', $LessonAttendUI->GetLessonAttendanceSettingsList());
	$last_lesson = $settings['EndSessionToCountAsPM'] !='' ? $settings['EndSessionToCountAsPM'] : 9;
}

$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();

$valid_leave_type_ary = array($statusMap['Absent']['code'],$statusMap['Outing']['code']);
$leave_type_text_ary = array($statusMap['Absent']['code']=>$statusMap['Absent']['text'],$statusMap['Outing']['code']=>$statusMap['Outing']['text']);

$date_studentid_to_record = array();
$valuesAry = array();
$errorsAry = array();
for($i=0;$i<$data_size;$i++){
	
	$col = 0;
	$class_name = trim($data[$i][$col++]);
	$class_number = trim($data[$i][$col++]);
	$date = trim($data[$i][$col++]);
	$leave_type = trim($data[$i][$col++]);
	if($sys_custom['LessonAttendance_LaSalleCollege']){
		$leave_sessions = trim($data[$i][$col++]);
	}
	$reason = trim($data[$i][$col++]);
	
	$error_row = array();
	
	$student_id = '';
	if(!isset($classToStudentMap[$class_name][$class_number])){
		$error_row['Student'] = $Lang['LessonAttendance']['StudentCannotBeFound'];
	}else{
		$student_id = $classToStudentMap[$class_name][$class_number]['UserID'];
	}
	
	$date = convertDateTimeToStandardFormat($date,false);
	if($date == ''){
		$error_row['Date'] = $Lang['LessonAttendance']['InvalidDateFormat'];
	}
	
	if($date != '' && $student_id !=''){
		if(!isset($date_studentid_to_record[$date][$student_id])){
			$existing_records = $LessonAttendUI->GetLessonAttendanceLeaveRecords(array('RecordDate'=>$date,'StudentID'=>$student_id));
			if(count($existing_records)>0){
				$error_row['Duplicate'] = $Lang['LessonAttendance']['StudentAlreadyHasLeaveRecordsOnThatDay'];
			}
		}else{
			$error_row['Duplicate'] = $Lang['LessonAttendance']['StudentAlreadyHasLeaveRecordsOnThatDay'];
		}
		$date_studentid_to_record[$date][$student_id] = $data[$i];
	}
	
	if(!in_array($leave_type, $valid_leave_type_ary)){
		$error_row['LeaveType'] = $Lang['LessonAttendance']['InvalidLeaveType'];
	}
	
	if($sys_custom['LessonAttendance_LaSalleCollege']){
		$sessions_ary = explode(',',$leave_sessions);
		if(count($sessions_ary)==0){
			$error_row['LeaveSessions'] = $Lang['LessonAttendance']['InvalidLessonSessions'];
		}else{
			foreach($sessions_ary as $s){
				if(!is_numeric($s) || $s > $last_lesson){
					$error_row['LeaveSessions'] = $Lang['LessonAttendance']['InvalidLessonSessions'];
					break;
				}
			}
		}
	}
	
	if(count($error_row)>0){
		$errorsAry[$i] = $error_row;
	}else{
		$values = array($cur_sessionid, $cur_time,$date, $class_name, $class_number, $student_id, $leave_type);
		if($sys_custom['LessonAttendance_LaSalleCollege']){
			$values = array_merge($values, array($leave_sessions));
		}
		$values = array_merge($values, array($reason));
		$valuesAry[] = $values;
	}
}

if(count($errorsAry)==0 && count($valuesAry)>0){
	
	$sql = "INSERT INTO SUBJECT_GROUP_ATTENDANCE_IMPORT_LEAVE_RECORDS (SessionID,ImportTime,RecordDate,ClassName,ClassNumber,StudentID,LeaveType,LeaveSessions,Reason) VALUES ";
	$chunks = array_chunk($valuesAry,200);
	for($i=0;$i<count($chunks);$i++){
		$rows = $chunks[$i];
		$values = "";
		$delim = "";
		for($j=0;$j<count($rows);$j++){
			$values .= $delim."('".implode("','", $rows[$j])."')";
			$delim = ",";
		}
		$query = $sql . $values;
		//debug_pr($query);
		$LessonAttendUI->db_db_query($query);
		usleep(10);
	}
}

$x = '<table class="common_table_list_v30" width="96%">';
$x .= '<thead>
			<tr>
				<th class="num_check">'.$Lang['General']['ImportArr']['Row'].'</th>';
		$x .= '<th style="width:15%">'.$Lang['General']['RecordDate'].'</th>';
		$x .= '<th style="width:20%">'.$Lang['Identity']['Student'].'</th>';
		$x .= '<th style="width:10%">'.$Lang['General']['Type'].'</th>';
	if($sys_custom['LessonAttendance_LaSalleCollege']){	
		$x .= '<th style="width:15%">'.$Lang['LessonAttendance']['Lesson'].'</th>';
	}
		$x .= '<th style="width:15%">'.$Lang['LessonAttendance']['Reason'].'</th>';
		$x .= '<th style="width:25%">'.$Lang['General']['Error'].'</th>';
	$x .= '</tr>
		</thead>';
$x .= '<tbody>';

for($i=0;$i<$data_size;$i++){
	$row_num = $i;
	
	$col = 0;
	$class_name = trim($data[$i][$col++]);
	$class_number = trim($data[$i][$col++]);
	$date = trim($data[$i][$col++]);
	$leave_type =  trim($data[$i][$col++]);
	if($sys_custom['LessonAttendance_LaSalleCollege']){
		$leave_sessions = trim($data[$i][$col++]);
	}
	$reason = trim($data[$i][$col++]);
	$display_errors = isset($errorsAry[$row_num]) && count($errorsAry[$row_num])>0 ? '<span class="red">'. implode('<br />',$errorsAry[$row_num]).'</span>' : '&nbsp;';
	$x .= '<tr>';
		$x .= '<td>'.($row_num+2).'</td>';
		$error_css = isset($errorsAry[$row_num]) && isset($errorsAry[$row_num]['Date'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.$date.'</td>';
		$error_css = isset($errorsAry[$row_num]) && isset($errorsAry[$row_num]['Student'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.$classToStudentMap[$class_name][$class_number]['StudentName'].'</td>';
		$error_css = isset($errorsAry[$row_num]) && isset($errorsAry[$row_num]['LeaveType'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.($leave_type_text_ary[$leave_type]).'</td>';
	if($sys_custom['LessonAttendance_LaSalleCollege']){		
		$error_css = isset($errorsAry[$row_num]) && isset($errorsAry[$row_num]['LeaveSessions'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.$leave_sessions.'</td>';
	}
		$x .= '<td>'.Get_String_Display($reason).'</td>';
		$x .= '<td>'.$display_errors.'</td>';
	$x .= '</tr>';
}
$x .= '</tbody>';
$x .= '</table>';

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_LessonAttendanceLeaveRecords";

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array (
			$Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],'index.php'
		);
$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import'],''
		);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);


### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG']))
{
	$Msg = $_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'];
	unset($_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG']);
}
$linterface->LAYOUT_START($Msg); 
?>
<br/>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		</tr>
		<tr>
			<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<div align="center">
	<table width="90%" border="0" cellpadding="3" cellspacing="0">
		<tr>
			<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['TotalRecord']?></td>
			<td class="tabletext"><?=$data_size?></td>
		</tr>
		<tr>
			<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['SuccessfulRecord']?></td>
			<td class="tabletext"><?=($data_size - sizeof($errorsAry))?></td>
		</tr>
		<tr>
			<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['FailureRecord']?></td>
			<td class="tabletext"><?=count($errorsAry)?></td>
		</tr>
	</table>
	<?=$x?>
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
<?php
	if (sizeof($errorsAry) == 0)
		echo $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_update.php';").'&nbsp;';
	echo $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
?>
			</td>
		</tr>
	</table>
	</div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>