<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lexport = new libexporttext();

$exportColumn = array("Class Name","Class Number","Date","Leave Type","Leave Sessions","Reason");
$rows = array();
$rows[] = array("1A","1","2016-09-01","3","1,2,3","SL");
$rows[] = array("2B","10","2016-09-01","1","7,8,9","Competition");


intranet_closedb();

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$filename = 'import_lesson_leave.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>