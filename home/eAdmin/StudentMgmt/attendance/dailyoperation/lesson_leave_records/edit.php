<?php
// Editing by 
/*
 * 2016-08-11 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

if(isset($RecordID)){
	if(is_array($RecordID)) $RecordID = $RecordID[0];
	$is_edit = true;
	$records = $lc->GetLessonAttendanceLeaveRecords(array('RecordID'=>$RecordID));
	$hidden_fields = '<input type="hidden" id="RecordID" name="RecordID" value="'.$RecordID.'" />';
	$LeaveType = $records[0]['LeaveType'];
	$Reason = $records[0]['Reason'];
}

if(!isset($StudentID)){
	$StudentID = array();
}

if($sys_custom['LessonAttendance_LaSalleCollege']){
	$settings = $lc->Get_General_Settings('LessonAttendance', $lc->GetLessonAttendanceSettingsList());
	$last_lesson = $settings['EndSessionToCountAsPM'] !='' ? $settings['EndSessionToCountAsPM'] : 9;
}

# submitType 1 : add by class name ( class number)
# submitType 2 : add by user login 
if($submitType==1 && $user_id!=""){
	if(!in_array($user_id,$StudentID))
		array_push($StudentID,$user_id);
}
else if($submitType==2 && $user_login !=""){
	$sql ="SELECT UserID FROM INTRANET_USER WHERE UserLogin='$user_login' AND RecordType=2 AND RecordStatus IN (0,1,2)";
	
	$temp = $lc->returnVector($sql);
	if($temp[0]!="" && !in_array($temp,$StudentID))
		array_push($StudentID,$temp[0]);
}

if(sizeof($StudentID)>0){
	$selected_student_list = implode(",",IntegerSafe($StudentID));
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND UserID IN($selected_student_list) ORDER BY ClassName,ClassNumber";
	
	$temp = $lc->returnArray($sql,2);
}else{
	 $temp = array();
	 $selected_student_list="''";
}

$select_student = getSelectByArray($temp," id=\"StudentID[]\" name=\"StudentID[]\" size=\"10\" multiple","",0,1);

$select_class = $lc->getSelectClass("name=\"class\" onChange=\"changeClass()\"",$class,0);

if($class!=""){
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' AND UserID NOT IN ($selected_student_list) ORDER BY ClassName,ClassNumber";
	$temp = $lc->returnArray($sql,2);
	$select_classnum=getSelectByArray($temp,"name='user_id'",$user_id,0,0);
	$btn = $linterface->GET_BTN($button_add, "button", "addStudent(1)");
	$select_classnum.="&nbsp$btn";
}


$reason_js = '';
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
foreach($statusMap as $key => $value)
{
	$reason_ary = $LessonAttendUI->GetLessonAttendanceReasons('', $value['code']);
	$reason_text_ary = Get_Array_By_Key($reason_ary, 'ReasonText');
	$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($reason_text_ary, "jArrayLessonReasons".$value['code'], 1);
}


$select_input_reason = $linterface->GET_PRESET_LIST("getLessonAttendanceReasons()", 'la_', "Reason");	

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_LessonAttendanceLeaveRecords";

$pages_arr = array(
	array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],'index.php'),
	array($RecordID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(); 
?>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/lesson_attendance.css" rel="stylesheet" type="text/css" />
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>
<?=$reason_js?>
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form name="form1" id="form1" action="<?=$is_edit?"edit_update.php":""?>" method="post">
<?=$hidden_fields?>
<div id="WarningLayer"></div>
<table width="100%" cellpadding="2" class="form_table_v30">
	<col class="field_title">
	<col class="field_c">
<?php if($is_edit){ ?>	
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['Identity']['Student']?> <span class="tabletextrequire">*</span>
		</td>
		<td width="70%">
			<?=$records[0]['StudentName']?>
			<input type="hidden" name="StudentID[]" value="<?=$records[0]['StudentID']?>" />
		</td>
	</tr>
	<tr class="data-row">
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Date']?> <span class="tabletextrequire">*</span></td>
		<td width="70%" >
			<?=$records[0]['RecordDate']?>
			<input type="hidden" name="RecordDate[]" value="<?=$records[0]['RecordDate']?>" />
	<?php 
	if($sys_custom['LessonAttendance_LaSalleCollege']){
		$LeaveSessions = explode(',',$records[0]['LeaveSessions']);
		$x = '&nbsp;&nbsp;'.$Lang['LessonAttendance']['Lesson'].':';
		for($j=1;$j<=$last_lesson;$j++){
			$x .= '&nbsp;<input type="checkbox" name="LeaveSessions[]" value="'.$records[0]['RecordDate'].'_'.$j.'" '.(in_array($j,(array)$LeaveSessions)?'checked="checked"':'').' />'.$j;
		}
		echo $x;
	}
	echo $linterface->Get_Thickbox_Warning_Msg_Div("RecordDate_Error",$Lang['LessonAttendance']['RequestSelectLessons'], "WarnMsg");
	?>
		</td>
	</tr>
<?php }else{ ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title">
			<?=$i_general_students_selected?> <span class="tabletextrequire">*</span>
		</td>
		<td width="70%">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="tabletext">
						<?=$select_student?>
						<?=$linterface->Get_Thickbox_Warning_Msg_Div("StudentID_Error",$Lang['General']['JS_warning']['SelectAtLeastOneStudent'], "WarnMsg")?>
					</td>
					<td class="tabletext">&nbsp;</td>
					<td class="tabletext" valign="bottom">
						<?= $linterface->GET_BTN($button_select, "button", "newWindow('/home/common_choose/index.php?fieldname=StudentID[]&permitted_type=2&excluded_type=1,3,4', 9)") ?><br />
						<?= $linterface->GET_BTN($button_remove, "button", "checkOptionRemove(document.form1.elements['studentID[]']);submitForm('');") ?>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<span class="tabletextremark">(<?=$i_general_alternative?>)</span>
						<table cellpadding="1" cellspacing="0" border="0">
							<tr><td class="tabletext" nowrap valign="bottom"><?=$i_UserLogin; ?></td></tr>
							<tr><td class="tabletext" nowrap valign="bottom">
								<input type="text" class="textboxnum" name="user_login" maxlength="100">&nbsp;
								<?= $linterface->GET_BTN($button_add, "button", "addStudent(2)") ?><br />
							</td></tr>
							<tr><td class="tabletext" nowrap valign="bottom"><?=$i_ClassNameNumber; ?></td></tr>
							<tr><td class="tabletext" nowrap valign="bottom">
								<?=$select_class?><?=$select_classnum?>
							</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Date']?> <span class="tabletextrequire">*</span></td>
		<td width="70%" >
<?php	
$default_date = date('Y-m-d');
$date_picker = $linterface->GET_DATE_PICKER("DatePicker", $default_date, ' style="visibility:hidden;width:0px;" ', "yy-mm-dd", "", "", "", "onDatePickerSelected(dateText);", "DatePickerText");

$x .= '<table border="0" cellpadding="4" cellspacing="0" width="40%">
			<tbody>
				<tr>
					<td style="border:1px solid #CCCCCC" valign="top">
						<table id="TableDate" border="0" cellpadding="4" cellspacing="0" width="100%">
							<tbody>
								<tr class="tabletop">
									<td>#</td>
									<td nowrap="nowrap">'.$Lang['StudentAttendance']['DateSelected'].'</td>
									<td>'.$date_picker.'</td>
								</tr>';
				if(isset($RecordDate) && is_array($RecordDate))
				{
					for($i=0;$i<count($RecordDate);$i++){
						$x .= '<tr class="tablerow1 data-row">
									<td>'.($i+1).'</td>
									<td class="data-col">'.$RecordDate[$i].'<input type="hidden" name="RecordDate[]" value="'.$RecordDate[$i].'" />';
							if($sys_custom['LessonAttendance_LaSalleCollege']){
								$x .= '&nbsp;&nbsp;'.$Lang['LessonAttendance']['Lesson'].':';
								for($j=1;$j<=$last_lesson;$j++){
									//$id = uniqid();
									$x .= '&nbsp;<input type="checkbox" name="LeaveSessions[]" value="'.$RecordDate[$i].'_'.$j.'" '.(in_array($RecordDate[$i].'_'.$j,(array)$LeaveSessions)?'checked="checked"':'').' />'.$j;
								}
							}
							$x .= '</td>
									<td>'.$linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "onDateDeleted(this);", "", 1).'</td>
								</tr>';
					}
				}else{
						$x .= '<tr class="tablerow1 data-row">
									<td>1</td>
									<td class="data-col">'.$default_date.'<input type="hidden" name="RecordDate[]" value="'.$default_date.'" />';
						if($sys_custom['LessonAttendance_LaSalleCollege']){
							$x .= '&nbsp;&nbsp;'.$Lang['LessonAttendance']['Lesson'].':';
							for($i=1;$i<=$last_lesson;$i++){
								//$id = uniqid();
								$x .= '&nbsp;<input type="checkbox" name="LeaveSessions[]" value="'.$default_date.'_'.$i.'" />'.$i;
							}
						}
							$x .= '</td>
									<td>'.$linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "onDateDeleted(this);", "", 1).'</td>
								</tr>';
				}
					$x .= '</tbody>
						</table>
					</td>';
		$x .= '</tr>';
	$x .= '</tbody>';
$x .= '</table>';

echo $x;
?>
		<?=$linterface->Get_Thickbox_Warning_Msg_Div("RecordDate_Error",$Lang['StudentAttendance']['Warning']['PleaseSelectAtLeastOneDay'], "WarnMsg")?>
		</td>
	</tr>
<?php } ?>
	<tr>
		<td class="field_title"><?=$Lang['General']['Type']?> <span class="tabletextrequire">*</span></td>
		<td>
			<?=$linterface->Get_Radio_Button("LeaveTypeAbsent", "LeaveType", $statusMap['Absent']['code'], $LeaveType==$statusMap['Absent']['code'] || $LeaveType=='', $__Class="", $__Display=$statusMap['Absent']['text'], $__Onclick="",$__isDisabled=0).'&nbsp;'?>
			<?=$linterface->Get_Radio_Button("LeaveTypeOuting", "LeaveType", $statusMap['Outing']['code'], $LeaveType==$statusMap['Outing']['code'], $__Class="", $__Display=$statusMap['Outing']['text'], $__Onclick="",$__isDisabled=0)?>
			<?=$linterface->Get_Thickbox_Warning_Msg_Div("LeaveType_Error",'', "WarnMsg")?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['LessonAttendance']['Reason']?></td>
		<td>
			<?=$linterface->GET_TEXTBOX_NAME("Reason", "Reason", $Reason, '', array()).'&nbsp;'.$select_input_reason?>
		</td>
	</tr>
</table>

<?=$linterface->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button",$is_edit? "checkEditForm(document.form1);" : "checkSubmitForm(document.form1);",'submitBtn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php'",'cancelBtn')?>
</div>
<input type="hidden" name="submitType" value="">
</form>
<script type="text/javascript" language="javascript">

var table_row_template = '<tr class="#TABLEROW_CLASS# data-row">';
table_row_template += '<td>#ROW_NUMBER#</td>';
table_row_template += '<td class="data-col">#DATE#<input type="hidden" name="RecordDate[]" value="#DATE#" />';
<?php 
if($sys_custom['LessonAttendance_LaSalleCollege']){ 
	$html = '&nbsp;&nbsp;'.$Lang['LessonAttendance']['Lesson'].':';
	for($i=1;$i<=$last_lesson;$i++){
		//$id = uniqid();
		$html .= '&nbsp;<input type="checkbox" name="LeaveSessions[]" value="#DATE#_'.$i.'" />'.$i;
	}
	echo 'table_row_template += \''.$html.'\';'."\n";
}
?>
table_row_template += '</td>';
table_row_template += '<td><?=$linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "onDateDeleted(this);", "", 1)?></td>';
table_row_template += '</tr>';

function onDateDeleted(objA)
{
	var row = $(objA).closest('tr');
	$(row).remove();
	onDatePickerSelected('');
}

function onDatePickerSelected(dateText)
{
	var dates = getValuesByName('RecordDate[]');
	if(dateText !='' && dates.indexOf(dateText)==-1){
		dates.push(dateText);
	}
	dates.sort();
	var row_css = 1;
	var rows_html = '';
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
	var checked_sessions = getCheckedValuesByName('LeaveSessions[]');
<?php } ?>
	for(var i=0;i<dates.length;i++){
		row_css = i % 2 == 0 ? 1:2;
		var row_html = table_row_template.replace(/#TABLEROW_CLASS#/gi, 'tablerow'+row_css);
		row_html = row_html.replace(/#ROW_NUMBER#/gi, i+1);
		row_html = row_html.replace(/#DATE#/gi, dates[i]);
		rows_html += row_html;
	}
	$('#TableDate tr.data-row').remove();
	$('#TableDate tr.tabletop').after(rows_html);
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
	var session_objs = document.getElementsByName('LeaveSessions[]');
	for(var i=0;i<session_objs.length;i++){
		if($.inArray(session_objs[i].value,checked_sessions)>=0){
			session_objs[i].checked = true;
		}
	}
<?php } ?>
}

function getValuesByName(objName)
{
	var objAry = document.getElementsByName(objName);
	var ary = [];
	for(var i=0;i<objAry.length;i++){
		ary.push(objAry[i].value);
	}
	return ary;
}

function getCheckedValuesByName(objName)
{
	var objAry = document.getElementsByName(objName);
	var ary = [];
	for(var i=0;i<objAry.length;i++){
		if(objAry[i].checked) ary.push(objAry[i].value);
	}
	return ary;
}

function changeClass(){
	submitForm('');
}

function submitForm(newurl){
	obj = document.form1;
	studentObjs = document.getElementsByName('StudentID[]');
	studentObj = studentObjs[0];
	if(obj==null || studentObj==null) return;
	for(i=0;i<studentObj.options.length;i++){
		studentObj.options[i].selected = true;
	}
	obj.action=newurl;
	obj.submit();
}
function addStudent(stype){
	obj = document.form1.submitType;
	if(obj==null) return;
	obj.value = stype;
	submitForm('');	
}

function checkEditForm(formObj)
{
	var valid = true;
	$('.WarnMsg').hide();
	$('#submitBtn').attr('disabled',true);
	
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
	var rows = $('.data-row');
	var checked_sessions_count = [];
	for(var i=0;i<rows.length;i++){
		var row_checked_sessions = $(rows[i]).find('input[type="checkbox"]:checked');
		if(row_checked_sessions.length == 0){
			checked_sessions_count.push(i);
			valid = false;
		}
	}
	if(!valid && checked_sessions_count.length>0){
		$('#RecordDate_Error').html('<?=$Lang['LessonAttendance']['RequestSelectLessons']?>').show();
	}
<?php } ?>
	
	if(valid){
		formObj.submit();
	}else{
		$('#submitBtn').attr('disabled',false);
	}
}

function checkSubmitForm(formObj)
{
	var valid = true;
	$('.WarnMsg').hide();
	$('#submitBtn').attr('disabled',true);
	selectionObj = document.getElementById("StudentID[]");
	Remove_All_Selection_Option(selectionObj,"");
	Select_All_Options("StudentID[]",true);
	var TargetUserID = [];
	
	for(var i=0;i<selectionObj.options.length;i++){
		TargetUserID.push(selectionObj.options[i].value.replace('U',''));
	}
	if(TargetUserID.length == 0)
	{
		$('#StudentID_Error').html('<?=$Lang['General']['JS_warning']['SelectAtLeastOneStudent']?>').show();
		valid = false;
	}
	
	var dateAry = getValuesByName('RecordDate[]');
	if(dateAry.length == 0){
		$('#RecordDate_Error').html('<?=$Lang['StudentAttendance']['Warning']['PleaseSelectAtLeastOneDay']?>').show();
		valid = false;
	}
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
	var rows = $('.data-row');
	var checked_sessions_count = [];
	for(var i=0;i<rows.length;i++){
		var row_checked_sessions = $(rows[i]).find('input[type="checkbox"]:checked');
		if(row_checked_sessions.length == 0){
			checked_sessions_count.push(i);
			valid = false;
		}
	}
	if(!valid && checked_sessions_count.length>0){
		$('#RecordDate_Error').html('<?=$Lang['LessonAttendance']['RequestSelectLessons']?>').show();
	}
<?php } ?>
	if(valid){
		Check_Duplicate_Records(formObj);
	}else{
		$('#submitBtn').attr('disabled',false);
	}
}

function Check_Duplicate_Records(formObj)
{
	Block_Element('form1','<?=$Lang['StudentAttendance']['Checking']?>');
	
	var RecordDates = getValuesByName('RecordDate[]');
	var UserSelection = document.getElementById('StudentID[]');
	var TargetUserID = [];
	
	for(var i=0;i<UserSelection.options.length;i++){
		TargetUserID.push(UserSelection.options[i].value.replace('U',''));
	}
	
	$.post(
		'ajax.php',
		{
			'task': 'checkDuplicateRecords',
			'RecordDate[]':RecordDates,
			'StudentID[]':TargetUserID 
		},
		function(data){
			UnBlock_Element('form1');
			if(data != ''){
				$('#WarningLayer').html(data);
				jAlert('<?=$Lang['LessonAttendance']['OverlapRecordsWarning']?>', '<?=$Lang['Btn']['Confirm']?>', function(r) {
				    $('#submitBtn').attr('disabled',false);
				});
			}else{
				$('#WarningLayer').html('');
				formObj.action = 'edit_update.php';
				formObj.submit();
			}
		}
	);
}

function getLessonAttendanceReasons()
{
	var returnArray = new Array();
	var leave_type = $('input[name="LeaveType"]:checked').val();
	returnArray = eval('jArrayLessonReasons'+leave_type);
	return returnArray;
}
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>