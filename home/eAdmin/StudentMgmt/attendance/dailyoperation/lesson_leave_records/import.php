<?php
// Editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_LessonAttendanceLeaveRecords";

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array (
			$Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],'index.php'
		);
$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import'],''
		);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);


### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG']))
{
	$Msg = $_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'];
	unset($_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG']);
}
$linterface->LAYOUT_START($Msg); 
?>
<script type="text/javascript" language="JavaScript">
function checkForm(obj)
{
	var userfile = $('#userfile').val();
	
	if(userfile == '' || Get_File_Ext(userfile) != 'csv'){
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		return false;
	}
	
	obj.submit();
}
</script>
<br />
<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data" onsubmit="return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
<table>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['Btn']['Select']?>
		</td>
		<td width="70%"class="tabletext">
			<input type="file" class="file" id="userfile" name="userfile"><br />
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
		<td width="70%">
			<?php
				$x = '';
				for($i=0;$i<count($Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields']);$i++) {
					$x .= '"'.$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][$i][0].'" - '.$Lang['LessonAttendance']['ImportLessonAttendanceLeaveRecords']['Fields'][$i][1];
					$x .= '<br>'."\n";
				}
				echo $x;
			?>
			<br />
			<a class="tablelink" href="get_csv_sample.php" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm(document.form1);", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").'&nbsp;'?>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location = 'index.php';", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>