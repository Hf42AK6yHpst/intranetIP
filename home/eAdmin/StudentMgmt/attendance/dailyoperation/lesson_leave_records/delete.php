<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

if($_SERVER['REQUEST_METHOD'] != 'POST' || count($_POST['RecordID'])==0)
{
	intranet_closedb();
	$_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	header ("Location: index.php");
	exit();
}

$success = $lc->DeleteLessonAttendanceLeaveRecords($_POST['RecordID']);
$_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
header ("Location:index.php");
exit();
?>