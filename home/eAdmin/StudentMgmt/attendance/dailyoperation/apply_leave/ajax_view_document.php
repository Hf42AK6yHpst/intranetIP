<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

intranet_auth();
intranet_opendb();

### get parameters
$recordId = $_POST['recordId'];


$lapplyleave = new libapplyleave();
$linterface = new interface_html();

$recordId = $_POST['recordId'];
$imageAry = $lapplyleave->getDocumentImageHtml($recordId);
$numOfImage = count($imageAry);

$x = '';
$x .= '<table style="width:100%;">';
for ($i=0; $i<$numOfImage; $i++) {
	$_imageHtml = $imageAry[$i]['imageHtml'];
	
	$x .= '<tr><td style="text-align:center;">'.$_imageHtml.'</td></tr>';
}
$x .= '</table>';
$htmlAry['imageTable'] = $x;




### action buttons
$htmlAry['approveBtn'] = $linterface->Get_Action_Btn($Lang['StudentAttendance']['ApplyLeaveAry']['ApproveDocument'], "button", "javascript:changeDocumentStatus('".$recordId."', 2);", 'approveBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['rejectBtn'] = $linterface->Get_Action_Btn($Lang['StudentAttendance']['ApplyLeaveAry']['RejectDocument'], "button", "javascript:changeDocumentStatus('".$recordId."', 3);", 'rejectBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<?=$htmlAry['imageTable']?>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<span></span>
			<p class="spacer"></p>
			<?=$htmlAry['approveBtn']?>
			<?=$htmlAry['rejectBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</form>
</div>