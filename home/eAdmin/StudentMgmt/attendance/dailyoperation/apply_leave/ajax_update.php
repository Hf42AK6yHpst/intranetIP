<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

### get parameters
$actionType = $_POST['actionType'];

$lapplyleave = new libapplyleave();

if ($actionType == 'changeDocumentStatus') {
	$leaveRecordId = $_POST['leaveRecordId'];
	$documentStatus = $_POST['documentStatus'];
	
	$success = $lapplyleave->updateApplyLeaveStatus($leaveRecordId, 'DocumentStatus', $documentStatus);
	
	if ($documentStatus == 3) {
		// send push notification if reject the document
		$libeClassApp = new libeClassApp();
		
		
		// get apply leave info
		$leaveInfoAry = $lapplyleave->getApplyLeaveData($leaveRecordId);
		$studentId = $leaveInfoAry[0]['StudentID'];
		$inputDate = $leaveInfoAry[0]['InputDate'];
		
		// get student's parents
		$objUser = new libuser($studentId);
		$studentParentMappingAry = BuildMultiKeyAssoc($objUser->getParentStudentMappingInfo($studentId), 'StudentID', array('ParentID'), $SingleValue=0, $BuildNumericArray=1);
		$relatedParentIdAry = Get_Array_By_Key($studentParentMappingAry[$studentId], 'ParentID');
		$numOfParent = count($relatedParentIdAry);
		
		// build message
		$messageInfoAry = array();
		for ($i=0; $i<$numOfParent; $i++) {
			$_parentId = $relatedParentIdAry[$i];
	
			$messageInfoAry[0]['relatedUserIdAssoAry'][$_parentId] = array($studentId);
		}
		
		$messageTitle = $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['ResubmitDocumentTitle'];
		$messageContent = $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['ResubmitDocumentContent'];
		$messageContent = str_replace('<!--studentNameEnWithClass-->', $objUser->EnglishName.' ('.$objUser->ClassName.'-'.$objUser->ClassNumber.')', $messageContent);
		$messageContent = str_replace('<!--studentNameChWithClass-->', $objUser->ChineseName.' ('.$objUser->ClassName.'-'.$objUser->ClassNumber.')', $messageContent);
		$messageContent = str_replace('<!--datetime-->', date('Y-m-d H:i', strtotime($inputDate)), $messageContent);
		
		$successAry['sendPushMessage'] = $libeClassApp->sendPushMessage($messageInfoAry, $messageTitle, $messageContent, $isPublic='', $recordStatus=0);
	}
	
	$returnVal = ($success)? '1' : '0';
}

echo $returnVal;
?>