<?php
## using:  

##################################### Change Log #####################################################
#
# 2019-09-23 Cameron: should also update RecordStatus in INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT by updateSchoolBusTimeSlotApplyLeaveStatus()
#
# 2019-02-18 Cameron: change AttendanceRecordID to AttendanceApplyLeaveID for condition in updateSchoolBusApplyLeaveStatus
#
# 2018-10-30 Cameron: also delete (mark status as delete) eSchoolBus apply leave record  
#
# 2017-09-18 Bill:	Support Class Teacher to manage apply leave list 	[2017-0908-1248-12235]
#
######################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

intranet_auth();
intranet_opendb();

$lapplyleave = new libapplyleave();

// [2017-0908-1248-12235] Handling for Apply Leave (App) (Class Teacher)
if($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $_GET["cteach"]==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$TeachingClasses = $lteaching->returnTeacherClass($UserID);
	$isClassTeacher = count((array)$TeachingClasses) > 0;
}


### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
// [2017-0908-1248-12235]
if ($plugin['eClassApp'] && $_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $isClassTeacher) {
	$canAccess = true;
	$classTeacherAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}


$recordIdAry = $_POST['recordIdAry'];



$successAry = array();
$lapplyleave->Start_Trans();

$successAry['deleteRecord'] = $lapplyleave->deleteApplyLeaveRecord($recordIdAry);

if ($plugin['eSchoolBus'] && count($recordIdAry)) {
    include_once($PATH_WRT_ROOT."/includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
    $status = $schoolBusConfig['ApplyLeaveStatus']['Deleted'];
    foreach((array)$recordIdAry as $_recordID) {
        $conditionAry['AttendanceApplyLeaveID'] = $_recordID;
        $successAry['deleteeSchoolBusRecord_'.$_recordID] = $leSchoolBus->updateSchoolBusApplyLeaveStatus($status, $conditionAry);

        $successAry['deleteeSchoolBusTimeSlotRecord_'.$_recordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveStatus($status, $conditionAry);
    }
}

if (in_array(false, $successAry)) {
	$lapplyleave->RollBack_Trans();
	$returnMsgKey = 'DeleteUnsuccess';
}
else {
	$lapplyleave->Commit_Trans();
	$returnMsgKey = 'DeleteSuccess';
}
intranet_closedb();


//header('Location: list.php?returnMsgKey='.$returnMsgKey);
$return_url = "list.php?returnMsgKey=".$returnMsgKey;
if($classTeacherAccess)
	$return_url = "../../class_teacher/apply_leave_list.php?returnMsgKey=".$returnMsgKey;
header("Location: $return_url");
?>