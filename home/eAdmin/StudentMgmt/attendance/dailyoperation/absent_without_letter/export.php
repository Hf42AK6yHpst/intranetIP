<?php
//Modifying by: 

############ Change Log Start ###############
#
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();


# SQL statement

$studentAry = $ldiscipline->storeStudent('0', $form, Get_Current_Academic_Year_ID());
if(sizeof($studentAry)>0)
	$stdConds = " AND RR.StudentID IN (".implode(',',$studentAry).")";

$today = date('Y-m-d');

$currentAcademicYear = Get_Current_Academic_Year_ID();
$yearStartDate = getStartDateOfAcademicYear($currentAcademicYear);
$yearEndDate = getEndDateOfAcademicYear($currentAcademicYear);
$name_field = getNameFieldByLang('USR.');
$clsName = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
$dateConds = ($on==1) ? " AND RR.RecordDate<='$pastDate'" : " AND RR.RecordDate='$pastDate'";

$sql = "SELECT
			$clsName as clsName,
			ycu.ClassNumber as clsNo,
			$name_field as name,
			RR.RecordDate, 
			RR.RecordDate as DayPass,
			IF(RR.Last_Discipline_RecordDate IS NULL, '---', RR.Last_Discipline_RecordDate) as lastDate,
			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', RR.RecordID ,'>') as checkbox
		FROM
			CARD_STUDENT_PROFILE_RECORD_REASON RR INNER JOIN
			INTRANET_USER USR ON (USR.UserID=RR.StudentID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=RR.StudentID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$currentAcademicYear) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) 
		WHERE 
			yc.AcademicYearID=$currentAcademicYear AND
			RR.RecordType = 1 AND
			DayType IN (2,3) AND
			(HandinLetter = 0 OR HandinLetter IS NULL) AND
			(RR.RecordDate BETWEEN '$yearStartDate' AND '$yearEndDate') 
			$dateConds $stdConds
		ORDER BY RR.RecordDate DESC, y.Sequence, yc.Sequence, ycu.ClassNumber
		";
$result = $lc->returnArray($sql);

for($i=0; $i<sizeof($result); $i++)
{
	 	
 	$ExportArr[$i][0] = $result[$i]['clsName'];				//class Name
	$ExportArr[$i][1] = $result[$i]['clsNo'];				//Class Number
	$ExportArr[$i][2] = $result[$i]['name'];				//Student name
	$ExportArr[$i][3] = $result[$i]['RecordDate'];			//absent date
	$ExportArr[$i][4] = $lc->countSchoolDayDiff($result[$i]['DayPass'], date("Y-m-d")).$i_general_Days;			//date diff 
	$ExportArr[$i][5] = $result[$i]['lastDate'];			//last update of discipline record
}

intranet_closedb();
//debug_pr($ExportArr);
$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, $Lang['StudentAttendance']['AbsentDate'], $i_general_Days, $Lang['StudentAttendance']['LastDisciplineRecord']);
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

$filename = 'Absent_without_parent_letter.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>