<?php
//using by : henry
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if ($page_size_change == 1)	# change "num per page"
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
} else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if($field=="") $field = 2;
if ($ck_approval_page_field!=$field && $field!="")
{
	//echo "#";
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
} else if (!isset($field) || $ck_approval_page_field == "")
{
	//echo "!";
	$field = $sortEventDate;
	//$field = 2;
}


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$lc = new libcardstudentattend2();
$ldiscipline = new libdisciplinev12();

# level selection menu
$levels = $lclass->getLevelArray();
$levelSelection = getSelectByArray($levels, ' name="form" id="form"', $form, 1, 0, $i_Discipline_All_Forms, 2);

# amount selection menu
$amountSelection = "<select name='amount' id='amount'>";
for($i=0; $i<=60; $i++) {
	$selected_str = ($i==$amount) ? " selected" : "";
	$amountSelection .= "<option name='$i' $selected_str>$i</option>";
}
$amountSelection .= "</select>";

# form content
$table = "<table width='95%' cellpadding='2' cellspacing='0' border='0' align='center'>";
$table .= "	<tr>";
$table .= "		<td class='formfieldtitle tabletext' width='30%'>".$i_Discipline_Form."</td>";
$table .= "		<td width='70%'>$levelSelection</td>";
$table .= "	</tr><tr>";
$table .= "		<td class='formfieldtitle tabletext'>".$Lang['StudentAttendance']['NoOfDaySinceAbsentDay']."</td>";
$table .= "		<td>$amountSelection $i_general_Days <input type='checkbox' name='on' id='on' value='1' ".(($on==1) ? "checked" : "")."><label for='on'>".$Lang['eDiscipline']['OnOrAbove']."</label></td>";
$table .= "	</tr>";
$table .= "	</tr><tr>";
$table .= "		<td class='formfieldtitle tabletext'>".$Lang['eAttendance']['DisplayPastPunishmentRecord']."</td>";
$table .= "		<td><input type='checkbox' name='displayPunishment' id='displayPunishment' value='1'".(($displayPunishment) ? " checked" : "")."><label for='displayPunishment'>$i_general_yes</label></td>";
$table .= "	</tr>";
$table .= "</table>";


if($submit_flag==1) {
	
	# check form
	if($form=="")
		$form = 0;

	# check display of past record
	if($displayPunishment)	
		$specConds = " OR Last_Discipline_RecordDate IS NOT NULL"; 
		
	# get student list
	$studentAry = $ldiscipline->storeStudent('0', $form, Get_Current_Academic_Year_ID());
	if(sizeof($studentAry)>0)
		$stdConds = " AND RR.StudentID IN (".implode(',',$studentAry).")";
		
	$today = date('Y-m-d');
	$pastDate = $lc->retrievePastDateByDateCount($amount, $today);
	//echo $pastDate;
	
	$currentAcademicYear = Get_Current_Academic_Year_ID();
	$yearStartDate = getStartDateOfAcademicYear($currentAcademicYear);
	$yearEndDate = getEndDateOfAcademicYear($currentAcademicYear);
	$name_field = getNameFieldByLang('USR.');
	$clsName = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
	$dateConds = ($on==1) ? " AND RR.RecordDate<='$pastDate'" : " AND RR.RecordDate='$pastDate'";
	
	$sql = "SELECT
				CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
				$name_field as name,
				RR.RecordDate, 
				RR.RecordDate as DayPass,
				IF(RR.Last_Discipline_RecordDate IS NULL, '---', RR.Last_Discipline_RecordDate) as lastDate,
				CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', RR.RecordID ,'>') as checkbox
			FROM
				CARD_STUDENT_PROFILE_RECORD_REASON RR INNER JOIN
				INTRANET_USER USR ON (USR.UserID=RR.StudentID) LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.UserID=RR.StudentID) LEFT OUTER JOIN
				YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$currentAcademicYear) LEFT OUTER JOIN
				YEAR y ON (y.YearID=yc.YearID) 
			WHERE 
				yc.AcademicYearID=$currentAcademicYear AND
				RR.RecordType = 1 AND
				DayType IN (2,3) AND
				(HandinLetter = 0 OR HandinLetter IS NULL $specConds) AND
				(RR.RecordDate BETWEEN '$yearStartDate' AND '$yearEndDate') 
				$dateConds $stdConds
			";
		
	$li = new libdbtable2007($field, $order, $pageNo);
	$li->field_array = array("ClassNameNum", "name", "RR.RecordDate", "DayPass", "lastDate");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = $eDiscipline["Record"];
	$li->column_array = array(0,0,0,0,0);
	$li->wrap_array = array(0,0,0,0,0);
	$li->sortby = "y.Sequence, yc.Sequence, ycu.ClassNumber";
	$li->IsColOff = "eAttendance_ParentLetter";
	$pos = 0;
	
	$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_class)."</td>\n";
	$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_general_name)."</td>\n";
	$li->column_list .= "<td width='25%'>".$li->column($pos++, $Lang['StudentAttendance']['AbsentDate'])."</td>\n";
	$li->column_list .= "<td width='25%'>".$i_general_Days."</td>\n";$pos++;
	$li->column_list .= "<td width='20%'>".$li->column($pos++, $Lang['StudentAttendance']['LastDisciplineRecord'])."</td>\n";
	$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";	
	
}

# will check the $plugin['Disciplinev12'] in table
$table_tool = "<td><a href='javascript:confirmSubmit()' class='tabletool'>".$Lang['eDiscipline']['AddToDiscipline']."</a></td>";

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_MissParentLetter";

$linterface = new interface_html();

//$toolbar = $linterface->GET_LNK_EXPORT("export.php?form=$form&amount=$amount&on=$on","","","","",0);
$toolbar = $linterface->GET_LNK_EXPORT("javascript:goExport()","","","","",0);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['DailyOperation_MissParentLetter'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function checkform() {
	var obj = document.form1;
	/*
	if(obj.form.value=="")	 {
		alert("<?=$i_alert_pleaseselect." ".$ec_iPortfolio['form']?>");
		obj.form.focus();
		return false;	
	} else { */
		obj.action = "index.php";
		obj.submit();
	//}
}

function confirmSubmit() {
	if(countChecked(document.form1,'RecordID[]')==0) {
		alert(globalAlertMsg2);
	} else {
		document.form1.action = "addConfirm.php";
		document.form1.submit();
	}
}


function goExport() {
	document.form1.action = "export.php";
	document.form1.submit();
}

-->
</script>

<br />
<form name="form1" method="post" action="" onSubmit="return checkform()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	    	<tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
		  </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_view, "submit") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<? if($submit_flag==1) { ?>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="4" cellspacing="0" align="center">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
				</tr>
			</table>
		</td>
	</tr>
	<? if($plugin['Disciplinev12']) { ?>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr class="table-action-bar">
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<?=$table_tool?>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<? } ?>
	<tr>
		<td>
			<?=$li->display()?>
		</td>
	</tr>
<!--
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
-->
</table>
<? } ?>
<input type="hidden" name="pastDate" id="pastDate" value="<?=$pastDate?>">
<input type="hidden" name="submit_flag" id="submit_flag" value="1">
<input type="hidden" name="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
