<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ((!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) &&
		(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancelesson'])
	 ) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


if ($_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
	intranet_closedb();
	header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/class/class_status.php");
	exit();
}
else {
	intranet_closedb();
	header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index.php");
	exit();
}

/*
$lcardstudentattend2 = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;

$linterface = new interface_html();
$TAGS_OBJ[] = array($i_StudentAttendance_System, "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$linterface->LAYOUT_STOP();
*/
intranet_closedb();
?>