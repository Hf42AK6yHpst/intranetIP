<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();

$list = implode(",",$RecordID);
$sql = "DELETE FROM CARD_STUDENT_HELPER_STUDENT WHERE RecordID IN ($list)";

if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['HelperListUpdateSuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['HelperListUpdateFail'];
}

intranet_closedb();
header("Location: index.php?select_class=$select_class&Msg=".urlencode($Msg));
?>