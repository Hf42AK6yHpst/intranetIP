<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();
$li->Start_Trans();
//class_name
if (sizeof($student_id)==0)
{
    $delete_cond = "";
}
else
{
    $list = implode(",", $student_id);
    $delete_cond = "AND a.StudentID NOT IN ($list)";
}
# for insert
for($i=0; $i<sizeOf($student_id); $i++){
	$fieldname = "StudentID, DateInput, DateModified";
	$fieldvalue = "'".$student_id[$i]."',NOW(),NOW()";
	$sql = "INSERT INTO CARD_STUDENT_HELPER_STUDENT 
						($fieldname) 
					VALUES 
						($fieldvalue) 
					ON DUPLICATE KEY UPDATE 
						DateModified = NOW()";
	$Result['InsertHelper'.$i] = $li->db_db_query($sql);
}

# for delete
$sql = "SELECT a.StudentID
        FROM
					CARD_STUDENT_HELPER_STUDENT as a
					LEFT OUTER JOIN 
					INTRANET_USER as b ON (a.StudentID=b.UserID)
        WHERE  b.ClassName=\"$class_name\" $delete_cond";
$delete_id = $li->returnVector($sql, 1);

if (sizeof($delete_id) > 0) {
	$list = implode(",", $delete_id);
	$sql = "DELETE FROM CARD_STUDENT_HELPER_STUDENT WHERE StudentID in ($list)";
	$Result['RemoveHelper'] = $li->db_db_query($sql);
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['HelperListUpdateFail'];
	$li->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['HelperListUpdateSuccess'];
	$li->Commit_Trans();
}

//debug_r($Result); die;

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>