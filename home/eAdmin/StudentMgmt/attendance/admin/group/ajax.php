<?php
// Editing by 
/*
 * 2017-10-26 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

switch($task)
{
	case "getDataTable":
		
		$groups = $lc->getAttendanceGroupRecords(array());
		$group_ids = Get_Array_By_Key($groups,'GroupID');
		$groupIdToUsers = $lc->getAttendanceGroupAdminUsers(array('GroupID'=>$group_ids,'GroupIDToUsers'=>1));
		
		$title_field = Get_Lang_Selection('TitleChinese','Title');
		$x = '';
		$x.= '<table class="common_table_list_v30 ">
				<thead>
					<tr class="tabletop">
						<th width="1" class="num_check" style="font-weight:bold">#</th><th width="30%" style="font-weight:bold">'.$i_GroupName.'</th><th width="68%" style="font-weight:bold">'.$Lang['StudentAttendance']['ResponsibleUsersForTakingAttendance'].'</th><th width="34px">&nbsp;</th>
					</tr>
				</thead>
				<tbody>';
		for($i=0;$i<count($groups);$i++){
			$x .= '<tr>';
				$x .= '<td class="tableContent">'.($i+1).'</td>';
				$x .= '<td class="tabletext tablerow">'.$groups[$i][$title_field].'</td>';
				$x .= '<td class="tabletext tablerow">';
					$users = isset($groupIdToUsers[$groups[$i]['GroupID']])? $groupIdToUsers[$groups[$i]['GroupID']] : array();
					$sep = '';
					for($j=0;$j<count($users);$j++){
						$x .= $sep.$users[$j]['UserName'];
						$sep = ', ';
					}
				$x .= '</td>';
				$x .= '<td>'.$linterface->Get_Thickbox_Div($___Height=450, $___Width=750, $___ExtraClass='edit_dim', $___Title=$Lang['StudentAttendance']['AddRemoveUsers'], $___OnClick='loadUIForAddRemoveUsers('.$groups[$i]['GroupID'].');', $InlineID="FakeLayer", $___Content="", $___DivID='', $___LinkID='EditBtn'.$groups[$i]['GroupID'], $___ExtraTags='').'</td>';
			$x .= '</tr>'."\n";
		}
		if(count($groups)==0){
			$x .= '<tr><td colspan="4" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
		}			
		$x.= '</tbody>
			</table>';
		
		echo $x;
	break;
	
	case "getAddRemoveUsersUI":
		
		$title_field = Get_Lang_Selection('TitleChinese','Title');
		$groups = $lc->getAttendanceGroupRecords(array('GroupID'=>$GroupID));
		$users = $lc->getAttendanceGroupAdminUsers(array('GroupID'=>$GroupID));
		$user_ary = array();
		for($i=0;$i<count($users);$i++){
			$user_ary[] = array($users[$i]['UserID'],$users[$i]['UserName']);
		}
		$user_selection = $linterface->GET_SELECTION_BOX($user_ary, ' id="GroupUserID[]" name="GroupUserID[]" multiple="multiple" size="12" style="min-width:12em;" ', $ParDefault, $ParSelected="", $CheckType=false);
		$add_user_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], 'button', "newWindow('/home/common_choose/index.php?OpenerFormName=modalForm&fieldname=GroupUserID[]&permitted_type=1,2&excluded_type=3,4',15)", $ParName="select_btn", $ParOtherAttribute="", $OtherClass="", $ParTitle=$Lang['Btn']['Select']);
		$remove_user_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], 'button', "remove_selection_option('GroupUserID[]')", $ParName="remove_btn", $ParOtherAttribute="", $OtherClass="", $ParTitle=$Lang['Btn']['Remove']);
		$select_all_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], 'button', $ParOnClick="Select_All_Options('GroupUserID[]',true);", $ParName="SelectAllBtn", $ParOtherAttribute="", $OtherClass="", $ParTitle='');
		
		$x .= '<form name="modalForm" id="modalForm" action="" method="post" onsubmit="return false;">'."\n";
		$x .= '<input type="hidden" name="GroupID" id="GroupID" value="'.$GroupID.'" />';
		$x .= '<input type="hidden" name="task" id="task" value="addRemoveUsers" />';
		$x .= '<div id="modalContent" style="min-height:370px;overflow-y:auto;">'."\n";
		$x .= '<br />'."\n";
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">'."\n";
			$x .= '<col class="field_c">'."\n";
			
			$x .= '<tr>';
				$x .= '<td class="field_title">'.$i_GroupName.'</td>';
				$x .= '<td>'.$groups[0][$title_field].'</td>';
			$x .= '</tr>';
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ResponsibleUsersForTakingAttendance'].'</td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($user_selection.'&nbsp;'.$add_user_btn.'&nbsp;'.$remove_user_btn.'&nbsp;', $select_all_btn, $SpanID='UserSelectionDiv');
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("GroupUserID_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
		$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		
		//$x .= $linterface->MandatoryField();		
		
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","submitModalForm(document.forms['modalForm']);").'&nbsp;';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","tb_remove();")."\n";
		$x .= '</div>'."\n";
		
		$x .= '</form>'."\n";
		
		echo $x;
		
	break;
	
	case "addRemoveUsers":
	
		$lc->Start_Trans();
		$result = array();
		$GroupID = IntegerSafe($GroupID);
		$user_ids = array();
		for($i=0;$i<count($GroupUserID);$i++){
			if($GroupUserID[$i] != '') $user_ids[] = str_replace('U','', $GroupUserID[$i]);
		}
		$user_ids = array_values(array_unique($user_ids));
		$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_GROUP_ADMIN WHERE GroupID='$GroupID'";
		$result['delete'] = $lc->db_db_query($sql);
		if(count($user_ids)>0)
		{
			$sql = "INSERT INTO CARD_STUDENT_ATTENDANCE_GROUP_ADMIN (GroupID,UserID,DateInput,InputBy) VALUES ";
			$sep = "";
			for($i=0;$i<count($user_ids);$i++){
				$sql .= $sep."('$GroupID','".$user_ids[$i]."',NOW(),'".$_SESSION['UserID']."')";
				$sep = ",";
			}
			$result['insert'] = $lc->db_db_query($sql);
		} 
		if(!in_array(false,$result)){
			$return_msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
			$lc->Commit_Trans();
		}else{
			$return_msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
			$lc->RollBack_Trans();
		}
		echo $return_msg;
	break;
}

intranet_closedb();
?>