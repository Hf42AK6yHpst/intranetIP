<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) ) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$CurrentPage = "PageCustomFeatures_HostelGroupStudentList";


$lc = new libcardstudentattend2();
$linterface = new interface_html('popup.html');

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HostelGroupStudentList'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $Lang['StudentAttendance']['HostelGroupStudentList'];
$linterface->LAYOUT_START();

$libgrouping = new libgrouping();

$groups = $libgrouping->returnCategoryGroups($lc->HostelAttendanceGroupCategory);
$groups = Get_Array_By_Key($groups, 'GroupID');

$TargetDate = date("Y-m-d");
$ts = strtotime($TargetDate);
$year = date("Y", $ts);
$month = date("m", $ts);
$params = array('StartDate'=>$year.'-'.$month.'-01','EndDate'=>$TargetDate,'GroupID'=>$groups);
$attendance_records = $lc->getStudentLastHostelAttendanceRecords($year,$month,$params);


$month = $month - 1;
if($month == 0) {
	$month = 12;
	$year = $year - 1;
}
$EndDate = date("Y-m-t", strtotime($year.'-'.$month.'-01'));
$params = array('StartDate'=>date("Y-m-d", strtotime($year.'-'.$month.'-01')),'EndDate'=>$EndDate,'GroupID'=>$groups);
$previous_records = $lc->getStudentLastHostelAttendanceRecords($year,$month,$params);
$previous_records = BuildMultiKeyAssoc($previous_records, 'UserID');


$present_records = array();
$absent_records = array();
foreach($attendance_records as $temp) {
	if($temp['InStatus'] == '') {
        if(isset($previous_records[$temp['UserID']])) {
			$temp = $previous_records[$temp['UserID']];
		}
	}

    if($temp['InStatus'] == CARD_STATUS_PRESENT) {
		$present_records[] = $temp;
	} else {
		$absent_records[] = $temp;
	}
}

?>
<style>
    .main_content { height: auto; }
</style>
<h1><?=$Lang['StudentAttendance']['HostelGroupStudentList']?></h1>
<h2><?=date("Y-m-d H:i:s")?></h2>

<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
        <td width="45%" valign="top">
            <h3><?=$Lang['StudentAttendance']['HostelGroupStudentPresentNumber']?>: <?=count($present_records)?></h3>
            <table width="100%" border="1" cellpadding="5" cellspacing="0" align="left">
                <tr>
                    <td>
						<?php foreach($present_records as $temp) {
							$student_name = substr($temp['StudentName'],0, strpos($temp['StudentName'], '('));
							echo $temp['GroupName'].' - '.$student_name;
							echo '<br/>';
						} ?>
                    </td>
                </tr>
            </table>
        </td>
        <td width="10%"></td>
        <td width="45%" valign="top">
            <h3><?=$Lang['StudentAttendance']['HostelGroupStudentAbsentNumber']?>: <?=count($absent_records)?></h3>
            <table width="100%" border="1" cellpadding="5" cellspacing="0" align="left">
                <tr>
                    <td>
						<?php foreach($absent_records as $temp) {
						    $student_name = substr($temp['StudentName'],0, strpos($temp['StudentName'], '('));
						    $out_time = '';
						    if($temp['OutTime'] != '') {
								$out_time = ' (' . $temp['OutTime'] . ')';
							}
					        echo $temp['GroupName'].' - '.$student_name;
						    echo $out_time;
					        echo '<br/>';
						 } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script>
$(document).ready(function() {
    setTimeout(function() {
        location.reload();
    }, (60*1000));
});

</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>