<?php
# using by: 

#########################################
# 									
#	Date:	2014-12-10 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Added SmartCardID4
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added SmartCardID2 and SmartCardID3
#
#	Date:	2012-11-30	YatWoon
#			Add flag $special_feature['eAttendance']['RFID'] to display RFID if client request [Case#2012-1011-1045-50071]
#
#########################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$li = new libdb();

$CardID = trim($_REQUEST['CardID']);
if($sys_custom['SupplementarySmartCard']){
	$CardID2 = trim($_REQUEST['CardID2']);
	$CardID3 = trim($_REQUEST['CardID3']);
	$CardID4 = trim($_REQUEST['CardID4']);
}
$RFID = trim($_REQUEST['RFID']);

if(trim($CardID)!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE UserID!='$StudentID' AND (CardID='$CardID'".($sys_custom['SupplementarySmartCard']?" OR CardID2='$CardID' OR CardID3='$CardID' OR CardID4='$CardID'":"").")";
	//debug_r($sql);
	$temp = $li->returnVector($sql);
}
if($sys_custom['SupplementarySmartCard'] && trim($CardID2)!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE UserID!='$StudentID' AND (CardID='$CardID2' OR CardID2='$CardID2' OR CardID3='$CardID2' OR CardID4='$CardID2')";
	//debug_r($sql);
	$temp3 = $li->returnVector($sql);
}
if($sys_custom['SupplementarySmartCard'] && trim($CardID3)!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE UserID!='$StudentID' AND (CardID='$CardID3' OR CardID2='$CardID3' OR CardID3='$CardID3' OR CardID4='$CardID3')";
	//debug_r($sql);
	$temp4 = $li->returnVector($sql);
}
if($sys_custom['SupplementarySmartCard'] && trim($CardID4)!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE UserID!='$StudentID' AND (CardID='$CardID4' OR CardID2='$CardID4' OR CardID3='$CardID4' OR CardID4='$CardID4')";
	//debug_r($sql);
	$temp5 = $li->returnVector($sql);
}

if(trim($RFID)!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE UserID!='$StudentID' AND RFID='$RFID'";
	//debug_r($sql);
	$temp2 = $li->returnVector($sql);
}

if(sizeof($temp)>0 || sizeof($temp2)>0 || sizeof($temp3)>0 || sizeof($temp4)>0 || sizeof($temp5)>0){
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    $linterface = new interface_html();
    $PAGE_NAVIGATION[] = array($button_import);
		$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
		$linterface->LAYOUT_START();
		
		echo "<br />";
		echo "<table id=\"html_body_frame\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
					<td>".$linterface->GET_NAVIGATION($PAGE_NAVIGATION)."</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			<table>";
        
		echo "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
         		<tr><td class=\"tabletext\">Smart Card ID has been used by other user.</td></tr>
         		</table>";
        echo "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
					<tr>
				    	<td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>
				    </tr>
				</table>
	            <table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
					<tr>
					    <td align=\"center\" colspan=\"2\">".
							$linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.go(-1)").
						"</td>
					</tr>
				</table>";
        $linterface->LAYOUT_STOP();
        exit();
}
else {
	$sql = "UPDATE INTRANET_USER SET CardID = '$CardID' ";
	if($sys_custom['SupplementarySmartCard']){
		$sql.= ",CardID2='$CardID2',CardID3='$CardID3',CardID4='$CardID4' ";
	}
	if($special_feature['eAttendance']['RFID'])
	{
		$sql .= ", RFID='$RFID' ";
	}
	$sql .= " WHERE UserID = '$StudentID'";
	if ($li->db_db_query($sql)) {
		$Msg = $Lang['StudentAttendance']['CardIDUpdateSuccess'];
	}
	else {
		$Msg = $Lang['StudentAttendance']['CardIDUpdateFail'];
	}
}

header("Location: studentcard.php?targetClass=$targetClass&pageNo=$pageNo&order=$order&field=$field&Msg=".urlencode($Msg));
intranet_closedb();
?>