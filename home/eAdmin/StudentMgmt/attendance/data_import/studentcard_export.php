<?php
# using by: 

#########################################
# 									
#	Date:	2014-12-11 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Added SmartCardID4
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added SmartCardID2 and SmartCardID3
#
#	Date:	2012-11-30	YatWoon
#			Add flag $special_feature['eAttendance']['RFID'] to display RFID if client request [Case#2012-1011-1045-50071]
#
#########################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcard = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataImport_CardID";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_StudentAttendance_ImportCardID, "", 0);

$PAGE_NAVIGATION[] = array($button_export);

$MODULE_OBJ = $lcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="GET" action="studentcard_export_csv.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<input type="radio" name="format" id="format1" value="1" CHECKED>
			<label for="format1"><?="$i_UserLogin, $i_SmartCard_CardID".($sys_custom['SupplementarySmartCard']?", ".$Lang['AccountMgmt']['SmartCardID2'].", ".$Lang['AccountMgmt']['SmartCardID3'].", ".$Lang['AccountMgmt']['SmartCardID4']:"")?><?=$special_feature['eAttendance']['RFID']?", ".$Lang['StudentAttendance']['RFID']:""?></label><br />
			<input type="radio" name="format" id="format2" value="2" CHECKED>
			<label for="format2"><?="$i_UserClassName, $i_UserClassNumber, $i_SmartCard_CardID".($sys_custom['SupplementarySmartCard']?", ".$Lang['AccountMgmt']['SmartCardID2'].", ".$Lang['AccountMgmt']['SmartCardID3'].", ".$Lang['AccountMgmt']['SmartCardID4']:"")?><?=$special_feature['eAttendance']['RFID']?", ".$Lang['StudentAttendance']['RFID']:""?></label><br />
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_export, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $order; ?>">
<input type="hidden" name="field" value="<?php echo $field; ?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>