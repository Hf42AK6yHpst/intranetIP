<?php
# using by: 

#########################################
# 									
#	Date:	2014-12-10 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Added SmartCardID4
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added SmartCardID2 and SmartCardID3
#	
#	Date:	2012-11-30	YatWoon
#			Add flag $special_feature['eAttendance']['RFID'] to display RFID if client request [Case#2012-1011-1045-50071]
#
#########################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcard = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataImport_CardID";

$linterface = new interface_html();

$StudentID = (is_array($StudentID)? $StudentID[0]: $StudentID);
$lu = new libuser($StudentID);
if($sys_custom['SupplementarySmartCard']){
	$sql = "SELECT CardID,CardID2,CardID3,CardID4,RFID FROM INTRANET_USER WHERE UserID='$StudentID'";
	$CardIDRecord = $lu->returnResultSet($sql);
}

$TAGS_OBJ[] = array($i_StudentAttendance_ImportCardID, "", 0);

$PAGE_NAVIGATION[] = array($button_edit);

$MODULE_OBJ = $lcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script type="text/javascript" language="javascript">
function checkSubmit(formObj)
{
<?php if($sys_custom['SupplementarySmartCard']){ ?>	
	var cardid = formObj.CardID.value.Trim();
	var cardid2 = formObj.CardID2.value.Trim();
	var cardid3 = formObj.CardID3.value.Trim();
	var cardid4 = formObj.CardID4.value.Trim();
	
	var smartcardid_ary = [];
	var smartcardid_word = [];
	smartcardid_ary.push(cardid);
	smartcardid_ary.push(cardid2);
	smartcardid_ary.push(cardid3);
	smartcardid_ary.push(cardid4);
	smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID']?>');
	smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID2']?>');
	smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID3']?>');
	smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID4']?>');
	
	for(var i=0;i<smartcardid_ary.length;i++){
		for(var j=0;j<smartcardid_ary.length;j++){
			if(i != j){
				if(smartcardid_ary[i]!='' && smartcardid_ary[j]!='' && smartcardid_ary[i] == smartcardid_ary[j]){
					alert(smartcardid_word[j] + "<?=$Lang['AccountMgmt']['IsDuplicatedInput']?>");
					return false;
				}
			}
		}
	}
<?php } ?>	
	return true;
}
</script>
<br />
<form name="form1" method="POST" action="studentcard_edit_update.php" onsubmit="return checkSubmit(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
		<td width="70%"class="tabletext">
			<?=$lu->UserNameClassNumber()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_CardID?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="CardID" value="<?=$lu->CardID?>">
		</td>
	</tr>
	<?php if($sys_custom['SupplementarySmartCard']){ ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['AccountMgmt']['SmartCardID2']?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="CardID2" value="<?=$CardIDRecord[0]['CardID2']?>">
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['AccountMgmt']['SmartCardID3']?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="CardID3" value="<?=$CardIDRecord[0]['CardID3']?>">
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['AccountMgmt']['SmartCardID4']?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="CardID4" value="<?=$CardIDRecord[0]['CardID4']?>">
		</td>
	</tr>
	<?php } ?>
	<? if($special_feature['eAttendance']['RFID']) {?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['RFID']?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="RFID" value="<?=$lu->RFID?>">
		</td>
	</tr>
	<? } ?>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'studentcard.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $order; ?>">
<input type="hidden" name="field" value="<?php echo $field; ?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>