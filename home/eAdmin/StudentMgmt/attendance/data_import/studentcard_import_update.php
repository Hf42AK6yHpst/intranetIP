<?php
// using by: 
/******************************************** Changes **************************************************
 * 2016-08-29 (Bill): Get UserLogin when import SmartCardID using format2 for checking
 * 2014-12-11 (Bill): $sys_custom['SupplementarySmartCard'] - Added SmartCardID4 
 * 2014-12-01 (Carlos): Remove UTF-8 non-breaking space C2A0 from userlogin, class and class number
 * 2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added SmartCardID2 and SmartCardID3 
 * 2012-11-30 (YatWoon): Add flag control for RFID column [Case#2012-1011-1045-50071]
 * 2011-11-03 (Carlos): force format CardID to 10 digitss
 ********************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

function Get_Userlogin_By_ClassNameNumber($name, $num)
{
	global $lc;
	
	// Get current year_id
	$ay = Get_Current_Academic_Year_ID();
		
	$sql = "Select
				c.UserLogin
			From
				YEAR_CLASS_USER as a
				left join YEAR_CLASS as b on (b.YearClassID=a.YearClassID)
				inner join INTRANET_USER as c on (a.UserID=c.UserID)
			Where 
				b.AcademicYearID = '$ay' and
				b.ClassTitleEN = '$name' and
				a.ClassNumber = '$num'";
	$result = $lc->returnVector($sql);
	return $result[0];
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataImport_CardID";

$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;

$limport = new libimporttext();

if ($format == 1){
	$format_array = array("UserLogin","CardID");
	$flagAry = array(1,1);
	if($sys_custom['SupplementarySmartCard']){
		$format_array[] = "CardID2";
		$format_array[] = "CardID3";
		$format_array[] = "CardID4";
		$flagAry[] = 1;
		$flagAry[] = 1;
		$flagAry[] = 1;
	}
	$flagAry[] = 0;
}else{
	$format_array = array("ClassName","ClassNumber","CardID");
	$flagAry = array(1,1,1);
	if($sys_custom['SupplementarySmartCard']){
		$format_array[] = "CardID2";
		$format_array[] = "CardID3";
		$format_array[] = "CardID4";
		$flagAry[] = 1;
		$flagAry[] = 1;
		$flagAry[] = 1;
	}
	$flagAry[] = 0;
}
if($special_feature['eAttendance']['RFID']){
		$format_array[] = "RFID";
		$flagAry[] = 1;
		
}		

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: studentcard_import.php?msg=5");
} else {
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
				#20130902 Siuwan Get data from csv with reference
                /* $data = $limport->GET_IMPORT_TXT($filepath);
                $header_row = array_shift($data);                   # drop the title bar */
				$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath, "", "", $format_array, $flagAry);
				$header_row = array_shift($data); 
        }

        # Check Format
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($header_row[$i] != $format_array[$i])
             {
                 $format_wrong = true;
             }
        }

        if ($format_wrong)
        {
	        # debugging
	        #echo "<pre>";
	        #print_r($data);
	        #echo "</pre>";
	        
	        $display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
                 		<tr><td class=\"tabletext\">".$i_import_invalid_format."</td></tr>
                 		</table>";
            $display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
            for ($i=0; $i<sizeof($format_array); $i++)
            {
                 $display .= "<tr><td class=\"tabletext\">".$format_array[$i]."</td></tr>\n";
            }
			$display .= "<tr><td class=\"tabletext\"><font color='red'>^</font>Student Name</td></tr>\n";
			$display .= "<tr><td class=\"tabletextremark\">".$Lang['formClassMapping']['Reference']."</td></tr>\n";
            $display .= "</table>";
        }
        else
        {
            for ($i=0; $i<sizeof($data); $i++)
            {
	            
                 if ($format==1)
                 {
                     list($login, $cardid, $cardid2, $cardid3, $cardid4, $RFID) = $data[$i];
                     $login = str_replace("\xc2\xa0", '', $login);
                     $cardid = FormatSmartCardID($cardid);
                     
                     if($sys_custom['SupplementarySmartCard']){
	                     $cardid2 = FormatSmartCardID($cardid2);
	                     $cardid3 = FormatSmartCardID($cardid3);
	                     $cardid4 = FormatSmartCardID($cardid4);
	                     
	                     if($cardid4 != "" && ($cardid4==$cardid || $cardid4==$cardid2 || $cardid4==$cardid3)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID4'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
	                     
	                     if($cardid3 != "" && ($cardid3==$cardid || $cardid3==$cardid2 || $cardid3==$cardid4)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID3'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
	                     
	                     if($cardid2 != "" && ($cardid2==$cardid || $cardid2==$cardid3 || $cardid2==$cardid4)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID2'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
	                     
	                     if($cardid != "" && ($cardid==$cardid2 || $cardid==$cardid3 || $cardid==$cardid4)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
                     }
                     if($cardid!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid'".($sys_custom['SupplementarySmartCard']?" OR CardID2='$cardid' OR CardID3='$cardid' OR CardID4='$cardid'":"").") AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }
		             
		             if($sys_custom['SupplementarySmartCard'] && $cardid2!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid2' OR CardID2='$cardid2' OR CardID3='$cardid2' OR CardID4='$cardid2') AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }
		             
		             if($sys_custom['SupplementarySmartCard'] && $cardid3!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid3' OR CardID2='$cardid3' OR CardID3='$cardid3' OR CardID4='$cardid3') AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }		   
		                       
		             if($sys_custom['SupplementarySmartCard'] && $cardid4!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid4' OR CardID2='$cardid4' OR CardID3='$cardid4' OR CardID4='$cardid4') AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }
		                 
		                 if ($RFID!="") {
		                 	## check if RFID exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE RFID ='$RFID' AND UserLogin!='$login'";
	                     $temp1 = $li->returnVector($sql);
	                     
	                    if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['StudentAttendance']['RFIDDuplicateWarning'];
		                     continue;
			                 }
		                }
                     
                     $sql = "UPDATE INTRANET_USER SET CardID = '$cardid' ";
                     if($sys_custom['SupplementarySmartCard']){
					 	$sql.= ", CardID2='$cardid2', CardID3='$cardid3', CardID4='$cardid4' ";
                     }
                     if($special_feature['eAttendance']['RFID'])
                     {
                     	$sql .= ",RFID='$RFID' ";
                 	}
                     $sql .= " WHERE UserLogin = '$login' AND RecordType = 2 AND RecordStatus IN (0,1,2)";
                     $li->db_db_query($sql);
                     $affected = $li->db_affected_rows();
                     if ($affected == 0)
                     {
                         # Check whether this record exists
                         $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$login' AND RecordType = 2";
                         // $temp = $li->db_db_query($sql);
                         $temp = $li->returnVector($sql,1);
                         if ($temp[0]=="")
                         {
                             $recordValid[$i] = false;
                             $reason[$i] = $Lang['StudentAttendance']['StudentNotFound'];
                         }
                         else
                         {
                             $recordValid[$i] = true;
                         }
                     }
                     else
                     {
                         $recordValid[$i] = true;
                     }
                 }
                 else
                 {
                     list($class,$classnum,$cardid,$cardid2,$cardid3,$cardid4,$RFID) = $data[$i];
                     $class = str_replace("\xc2\xa0", '', $class);
                     $classnum = str_replace("\xc2\xa0", '', $classnum);
                     
                 	 // Get UserLogin by Class Name and Class Number
                     $login = Get_Userlogin_By_ClassNameNumber(trim($class), trim($classnum));
                     $login = str_replace("\xc2\xa0", '', $login);
                     
                     $cardid = FormatSmartCardID($cardid);
                     if($sys_custom['SupplementarySmartCard']){
	                     $cardid2 = FormatSmartCardID($cardid2);
	                     $cardid3 = FormatSmartCardID($cardid3);
	                     $cardid4 = FormatSmartCardID($cardid4);
	                     
	                     if($cardid4!= "" && ($cardid4==$cardid || $cardid4==$cardid2 || $cardid4==$cardid3)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID4'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
	                     
	                     if($cardid3 != "" && ($cardid3==$cardid || $cardid3==$cardid2 || $cardid3==$cardid4)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID3'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
	                     
	                     if($cardid2 != "" && ($cardid2==$cardid || $cardid2==$cardid3 || $cardid2==$cardid4)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID2'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
	                     
	                     if($cardid != "" && ($cardid==$cardid2 || $cardid==$cardid3 || $cardid==$cardid4)){
	                     	$recordValid[$i]=false;
			                $reason[$i]= $Lang['AccountMgmt']['SmartCardID'].$Lang['AccountMgmt']['IsDuplicatedInput'];
	                     	continue;
	                     }
                     }
                     if($cardid!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid'".($sys_custom['SupplementarySmartCard']?" OR CardID2='$cardid' OR CardID3='$cardid' OR CardID4='$cardid'":"").") AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }
		             if($sys_custom['SupplementarySmartCard'] && $cardid2!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid2' OR CardID2='$cardid2' OR CardID3='$cardid2' OR CardID4='$cardid2') AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }
		             if($sys_custom['SupplementarySmartCard'] && $cardid3!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid3' OR CardID2='$cardid3' OR CardID3='$cardid3' OR CardID4='$cardid3') AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }    
		             if($sys_custom['SupplementarySmartCard'] && $cardid4!=""){
	                     ## check if cardid exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE (CardID ='$cardid4' OR CardID2='$cardid4' OR CardID3='$cardid4' OR CardID4='$cardid4') AND UserLogin!='$login'";
	                     $temp = $li->returnVector($sql);
	                     
	                     if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'];
		                     continue;
			             }
		             }   
		                 
		                 if ($RFID!="") {
		                 	## check if RFID exists
	                     $sql="SELECT UserID FROM INTRANET_USER WHERE RFID ='$RFID' AND UserLogin!='$login'";
	                     $temp1 = $li->returnVector($sql);
	                     
	                    if(sizeof($temp)!=0){
		                     $recordValid[$i]=false;
		                     $reason[$i]=$Lang['StudentAttendance']['RFIDDuplicateWarning'];
		                     continue;
			                 }
		                }
					 
		                $sql = "UPDATE INTRANET_USER SET CardID = '$cardid' ";
		                if($sys_custom['SupplementarySmartCard']){
							$sql.= ",CardID2='$cardid2',CardID3='$cardid3',CardID4='$cardid4' ";
		                }
		                if($special_feature['eAttendance']['RFID'])
                     {
                     	$sql .= ",RFID='$RFID' ";
                 	}
                 	$sql .= " WHERE ClassName = '$class' AND ClassNumber = '$classnum' AND RecordType = 2 AND RecordStatus IN (0,1,2)";
                     $li->db_db_query($sql);
                     $affected = $li->db_affected_rows();
                     if ($affected == 0)
                     {
                         # Check whether this record exists
                         $sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$class' AND ClassNumber = '$classnum' AND RecordType = 2";
                         // $temp = $li->db_db_query($sql);
                         $temp = $li->returnVector($sql,1);

                         if ($temp[0]=="")
                         {
                             $recordValid[$i] = false;
                             $reason[$i] = $Lang['StudentAttendance']['StudentNotFound'];
                         }
                         else
                         {
                             $recordValid[$i] = true;
                         }
                     }
                     else
                     {
                         $recordValid[$i] = true;
                     }
                 }
            }
            
            $css = "$tablebluerow2";

            # Mark Reason to display
            for ($i=0; $i<sizeof($recordValid); $i++)
            {
                 if (!$recordValid[$i] && $reason[$i]!="")
                 {
	                  $css = ($css=="tablebluerow1")?"tablebluerow2":"tablebluerow1";
	                  $display .= "<tr class=\"$css\">";
	                  $display .= "<td class=\"tabletext\">".($i+3)."</td>"; // Row 1 is English header, Row 2 is Chinese header, and display index offset by one, starting at row one.
	                  $display .= "<td class=\"tabletext\">".$reason[$i]."</td>";
	                  $display .= "</tr>";
                 }
            }
            if ($display != "")
            {
                $display_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
                $display_table .= "<tr class=\"tablebluetop\">";
                $display_table .= "<td class=\"tabletoplink\">Line</td><td class=\"tabletoplink\">Reason</td>";
                $display_table .= "</tr>";
                $display_table .= $display;
                $display_table .= "</table>";
                
                $display = $display_table;
            }
        }

        if ($display != '')
        {
            include_once($PATH_WRT_ROOT."includes/libinterface.php");
			
			$linterface = new interface_html();
			
			$TAGS_OBJ[] = array($i_StudentAttendance_ImportCardID, "", 0);

			$PAGE_NAVIGATION[] = array($button_import);
			
			$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
			$linterface->LAYOUT_START();
			
			echo "<br />";
			echo "<table id=\"html_body_frame\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td>".$linterface->GET_NAVIGATION($PAGE_NAVIGATION)."</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
				<table>";
			
			if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
			if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
			if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
			
			
            if (!$format_wrong)
            {
                 $display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
									<tr>
								    	<td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>
								    </tr>
								</table>
                                <table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
									<tr>
									    <td align=\"center\" colspan=\"2\">".
											$linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.go(-2)").
										"</td>
									</tr>
								</table>";
                 echo "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
                 		<tr><td class=\"tabletext\">
                 		$i_general_ImportFailed
                 		</td></tr>
                 		</table>";
            }
            else
            {
                $display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
									<tr>
								    	<td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>
								    </tr>
								</table>
                                <table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
									<tr>
									    <td align=\"center\" colspan=\"2\">".
											$linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.go(-1)").
										"</td>
									</tr>
								</table>";
            }
            echo $display;
            $linterface->LAYOUT_STOP();
        }
        else
        {
            header("Location: studentcard.php?msg=2");
        }

}

intranet_closedb();

?>