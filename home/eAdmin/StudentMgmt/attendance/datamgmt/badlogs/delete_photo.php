<?php
// Editing by 
/*
 * 2017-05-17 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['NoCardPhoto']) {
	intranet_closedb();
	//header ("Location: /");
	$_SESSION['STUDENT_ATTENDANCE_NO_CARD_PHOTO_RETURN_MSG'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	exit();
}

if(!isset($_POST['RecordID']) || empty($_POST['RecordID']) || (is_array($_POST['RecordID']) && count($_POST['RecordID'])==0))
{
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_NO_CARD_PHOTO_RETURN_MSG'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	exit();
}

$lf = new libfilesystem();
$lc = new libcardstudentattend2();

$base_dir = $file_path.'/file/student_attendance/no_card_photo/';

$record_id_ary = IntegerSafe( $_POST['RecordID'] );
$records = $lc->getNoCardPhotoRecords(array('RecordID'=>$record_id_ary));
$record_size = count($records);

$log = date('Y-m-d H:i:s').' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';
$result_ary = array();
for($i=0;$i<$record_size;$i++)
{
	$log .= '['.http_build_query($records[$i],'',',').']';
	
	$record_id = $records[$i]['RecordID'];
	$photo_path = $records[$i]['PhotoPath'];
	if(preg_match('/^\d+\/.+$/',$photo_path)){
		$full_photo_path = $base_dir.$photo_path;
		// delete the image file
		$result_ary['DeleteFile_'.$i] = $lf->file_remove($full_photo_path);
		if($result_ary['DeleteFile_'.$i]){
			$sql = "DELETE FROM CARD_STUDENT_NO_CARD_PHOTO WHERE RecordID='$record_id'";
			$result_ary['DeleteRecord_'.$i] = $lc->db_db_query($sql);
		}
	}
}

$lc->log($log);

intranet_closedb();
$_SESSION['STUDENT_ATTENDANCE_NO_CARD_PHOTO_RETURN_MSG'] = !in_array(false,$result_ary)? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
header("Location: photo.php");
exit;
?>