<?php
// Editing by 
/*
 * 2019-07-03 Ray:    Add delete option
 * 2017-05-17 Carlos: $sys_custom['StudentAttendance']['NoCardPhoto'] Added [No Bring Card Photo] tab.
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_datamgmt_badlogs_browse_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_datamgmt_badlogs_browse_page_number!="")
{
	$pageNo = $ck_attend_datamgmt_badlogs_browse_page_number;
}

if ($ck_attend_datamgmt_badlogs_browse_page_order!=$order && $order!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_page_order", $order, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_page_order = $order;
} else if (!isset($order) && $ck_attend_datamgmt_badlogs_browse_page_order!="")
{
	$order = $ck_attend_datamgmt_badlogs_browse_page_order;
}

if ($ck_attend_datamgmt_badlogs_browse_page_field!=$field && $field!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_page_field", $field, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_page_field = $field;
} else if (!isset($field) && $ck_attend_datamgmt_badlogs_browse_page_field!="")
{
	$field = $ck_attend_datamgmt_badlogs_browse_page_field;
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataManagement_BadLogs";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($type > 5 || $type < 1) $type = 1;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$today_ts = strtotime(date('Y-m-d'));
$StartDate = ($_REQUEST['StartDate'])? $_REQUEST['StartDate']:date('Y-m-d',getStartOfAcademicYear($today_ts));
$EndDate = ($_REQUEST['EndDate'])? $_REQUEST['EndDate']:date('Y-m-d',getEndOfAcademicYear($today_ts));

$user_field = getNameFieldByLang("b.");

$sql  = "SELECT
        	a.RecordDate, 
        	b.ClassName, 
        	b.ClassNumber, 
        	$user_field,";
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM) {
	$sql .= "
					a.RecordTime,";
}
$sql .= "
          CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>') as idcheckbox
     		FROM
        	CARD_STUDENT_BAD_ACTION as a ";
if ($lc->EnableEntryLeavePeriod) {
	$sql .= "
					INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.StudentID = selp.UserID 
          	AND 
          	a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
}
$sql .= "       	
					LEFT OUTER JOIN 
        	INTRANET_USER as b 
        	ON a.StudentID = b.UserID AND b.RecordType = 2
     		WHERE
          a.RecordType = '$type' 
          AND 
          a.RecordDate between '".$StartDate."' and '".$EndDate." 23:59:59' 
          AND 
          (b.ChineseName like '%$keyword%'
           OR b.EnglishName like '%$keyword%'
           OR b.ClassName like '$keyword'
           OR b.ClassNumber like '%$keyword%'
           OR a.RecordDate = '$keyword'
           )
            ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.RecordDate","b.ClassName","b.ClassNumber","b.EnglishName");
$name_width = 40;
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM)
{
    $li->field_array[] = "a.RecordTime";
    $name_width -= 20;
}

$can_delete_log = false;
if($sys_custom['StudentAttendance']['CanDeletebadRecordLog'] == true) {
	if ($type == CARD_BADACTION_LUNCH_NOTINLIST || $type == CARD_BADACTION_LUNCH_BACKALREADY || $type == CARD_BADACTION_FAKED_CARD_AM || $type == CARD_BADACTION_FAKED_CARD_PM || $type == CARD_BADACTION_NO_CARD_ENTRANCE) {
		$can_delete_log = true;
	}
}

if($can_delete_log) {
	$li->field_array[] = "idcheckbox";
}

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

$delete_bar = '';
if($can_delete_log) {
	$li->column_array[] = 0;
	$li->wrap_array[] = 0;
	$delete_bar = '<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td valign="bottom">
                                <div class="common_table_tool">
                                    <a href="javascript:removeChecked('.$type.');" class="tool_delete">'.$button_delete.'
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>';
}

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_general_record_date)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='$name_width%'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM)
{
    $li->column_list .= "<td width='20%'>".$li->column($pos++, $i_general_record_time)."</td>\n";
}

if($can_delete_log) {
	$li->column_list .= "<td width='20%'><input type=\"checkbox\" name=\"deleteAll\" onclick=\"checkAllDelete(this.checked)\"></td>\n";
}
#$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";

$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkGet(document.form1, 'export.php')","","","","",0);

$searchbar .= '<table>
							<tr>
								<td nowrap>'.$Lang['StaffAttendance']['StartDate'].':</td>
								<td>'.$linterface->GET_DATE_PICKER("StartDate",$StartDate).'</td>
							</tr>
							<tr>
								<td nowrap>'.$Lang['StaffAttendance']['EndDate'].':</td>
								<td>'.$linterface->GET_DATE_PICKER("EndDate",$EndDate).'</td>
							</tr>
							<tr>
							</table><br>';
$searchbar .= "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.action='';document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_NotInLunchList, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=1", ($type==CARD_BADACTION_LUNCH_NOTINLIST)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_GoLunchAgain, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=2", ($type==CARD_BADACTION_LUNCH_BACKALREADY)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_FakedCardAM, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=3", ($type==CARD_BADACTION_FAKED_CARD_AM)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_FakedCardPM, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=4", ($type==CARD_BADACTION_FAKED_CARD_PM)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_NoCardRecord, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=5", ($type==CARD_BADACTION_NO_CARD_ENTRANCE)?1:0);
if($sys_custom['StudentAttendance']['NoCardPhoto']){
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['NoBringCardPhoto'],'photo.php', 0);
}

#$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");

?>
<script Language="JavaScript">
function checkAllDelete(checked){
    if(checked){
        $('input[name="RecordID[]"]').attr('checked','checked');
    } else {
        $('input[name="RecordID[]"]').removeAttr('checked');
    }
}
function removeChecked(Type){
    var count = $('input[name="RecordID[]"]:checked').length;
    if(count < 1){
        alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
    } else if(confirm('<?=$i_Usage_RemoveConfirm?>')){
        var TargetIds = $('input[name="RecordID[]"]:checked').map(function(){
            return $(this).val();
        }).get();
        location.href = "remove.php?type="+Type+"&TargetIds="+TargetIds;
    }
}
</script>
<br />
<form name="form1" method="get">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="15" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$searchbar?></td>
    <tr>
</table>

<?=$delete_bar?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="padding-left: 15px">
    <tr>
        <td align="left"><?php echo $li->display("95%"); ?></td>
    <tr>
</table>


<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="type" value="<?=$type?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
