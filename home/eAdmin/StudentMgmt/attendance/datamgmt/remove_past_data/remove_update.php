<?php
// editing by 
/*
 * 2019-12-09 (YatWoon): Add class and daytype selection
 * 2012-09-21 (Carlos): added delete log
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']
	/* || !$sys_custom['StudentAttendance']['RemovePastData']*/) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->Start_Trans();

$Result = array();
for($i=0;$i<count($dates);$i++){
	$Result['RemoveData_'.$dates[$i]] = $lc->Remove_Whole_Day_Data($dates[$i], $ClassID, $TargetDayType);
}

if(!in_array(false,$Result)){
	$sql = "SELECT COUNT(*) FROM CARD_STUDENT_DAILY_LOG_".$TargetYear."_".$TargetMonth." ";
	$dailylog_count = $lc->returnVector($sql);
	if($dailylog_count[0] == 0){
		$sql = "DELETE FROM CARD_STUDENT_RECORD_DATE_STORAGE WHERE Year = '$TargetYear' AND Month = '$TargetMonth'";
        $Result['DeleteStudentRecordDateStorage'] = $lc->db_db_query($sql);
	}
}

// Delete Log
if(empty($ClassID))
{
	$SectionStr = "Remove Past Data";
}
else{ // Delete Log (selected Class)
	$SectionStr = "Remove Past Data - Class ID: " . $ClassID;
}
if($TargetDayType)
{
	$SectionStr .= "(DayType:". $TargetDayType .")";
}
$sql_log = "INSERT INTO MODULE_RECORD_DELETE_LOG (Module,Section,RecordDetail,LogDate,LogBy) 
			VALUES ('Student Attendance','". $SectionStr ."','".(count($dates)>0?implode(",",$dates):"")."',NOW(),'".$_SESSION['UserID']."')";
$lc->db_db_query($sql_log);

if (in_array(false,$Result)) {
	$lc->RollBack_Trans();
	$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}
else {
	$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
	$lc->Commit_Trans();
}

header("Location: index.php?ClassID=$ClassID&TargetDayType=$TargetDayType&Msg=".urlencode($Msg));
?>