<?php
// editing by 
##########################
# 2019-12-09 YatWoon: Add Class and DayType selection
##########################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']
	/* || !$sys_custom['StudentAttendance']['RemovePastData'] */) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataManagement_RemovePastData";

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

$lc->retrieveSettings();

$TAGS_OBJ[] = array($Lang['StudentAttendance']['RemovePastData'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script type="text/javascript" language="JavaScript">
function Get_Remove_Past_Data_Calendar()
{
	var target_date = $("input#TargetDate").val().Trim();
	var class_id = $("#ClassID").val();
	var day_type = $("#TargetDayType").val();
	if($("#DPWL-TargetDate").html().Trim()!=''){
		alert('<?=$Lang['General']['InvalidDateFormat']?>');
		return;
	}
	
	Block_Element("CalendarDiv");
	$("#CalendarDiv").load(
		"ajax_task.php?task=Get_Remove_Past_Data_Calendar&TargetDate=" + target_date + "&ClassID=" + class_id + "&TargetDayType=" + day_type,
		function(data){
			UnBlock_Element("CalendarDiv");
		}
	);
}

function CheckAllDates(self,class_name)
{
	var checked = self.checked? 'checked':'';
	$("input."+class_name).each(function(i){
		$(this).attr('checked',checked);
	});
}

function CheckRowDates(self,rowid)
{
	var checked = self.checked? 'checked':'';
	$("tr#"+rowid+" input.class_checkable").each(function(i){
		$(this).attr('checked',checked);
	});
}

function CheckSubmit(self)
{
	var dates = $('input[name="dates\\[\\]"]:checked');
	if(dates.length == 0){
		alert("<?=$Lang['StudentAttendance']['Warning']['PleaseSelectAtLeastOneDay']?>");
		return;
	}
	if(confirm("<?=$Lang['StudentAttendance']['Warning']['RemoveWholeDayDataWarning']?>")){
		self.submit();
	}
}
</script>
<?php
echo '<br />';
echo $StudentAttendUI->Get_Remove_Past_Data_Form('', $ClassID, $TargetDayType);
echo '<br />';

$linterface->LAYOUT_STOP();
intranet_closedb();
?>