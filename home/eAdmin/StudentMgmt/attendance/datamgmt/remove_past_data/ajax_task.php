<?php
// editing by 
##############
# 2019-12-10 YatWoon: Add TargetClass and TargetDayType
##############
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']
	/* || !$sys_custom['StudentAttendance']['RemovePastData']*/) {
	intranet_closedb();
	exit();
}

$lc = new libstudentattendance_ui();

$task = $_REQUEST['task'];

switch($task)
{
	case "Get_Remove_Past_Data_Calendar":
		$TargetDate = $_REQUEST['TargetDate'];
		$TargetClass = $_REQUEST['ClassID'];
		$TargetDayType = $_REQUEST['TargetDayType'];
		echo $lc->Get_Remove_Past_Data_Calendar($TargetDate, $TargetClass, $TargetDayType);
	break;
}

intranet_closedb();
?>

