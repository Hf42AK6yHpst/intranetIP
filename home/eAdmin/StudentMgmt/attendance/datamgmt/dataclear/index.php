<?php
// Editing by 
/*
 * 2015-01-27 (Carlos): Move the remark to the top and display in red.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataManagement_DataClear";

$linterface = new interface_html();

$records = $lc->retrieveYearMonthList();

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'Record[]','remove_month.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_DataMgmt_DataClear, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="post" action="remove_month.php">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark" style="color:red"><?=$i_StudentAttendance_DataRemoval_Warning?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
		<td class="tabletoplink"><?="$i_general_Year-$i_general_Month"?></td>
		<td class="tabletoplink" width="10%"><?="$button_remove?"?></td>
	</tr>
	<?
	for ($i=0; $i<sizeof($records); $i++)
	{
	     list ($year, $month) = $records[$i];
	     $css = ($i%2==0)?"tablerow1":"tablerow2";
	     if ($month <= 9) $month = "0".$month;
	?>
	<tr class="<?=$css?>">
		<td class="tabletext"><?="$year-$month"?></td>
		<td class="tabletext"><input type="checkbox" name="Record[]" value="<?="$year-$month"?>"></td>
	</tr>
	<?
	}
	?>
</table>
<br />
<!--
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark"><?=$i_StudentAttendance_DataRemoval_Warning?></td>
	</tr>
</table>
-->
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>