<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->Start_Trans();
for ($i=0; $i<sizeof($Record); $i++)
{
     $array = explode("-", $Record[$i]);
     list($year, $month) = $array;
     $Result[] = $lc->removeMonthData($year, $month);
}

if (in_array(false,$Result)) {
	$lc->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['MonthlyDataDeleteFail'];
}
else {
	$Msg = $Lang['StudentAttendance']['MonthlyDataDeleteSuccess'];
	$lc->Commit_Trans();
}

header("Location: index.php?Msg=".urlencode($Msg));
?>