<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

#$data = Array(Array("1",$i_DayTypeAM),Array("2",$i_DayTypePM));
#$selection_period = getSelectByArray($data, " name=period ", $period,0,1);

$lc = new libclass();
$classes = $lc->getClassList();
$select_class = "";
$select_class .= "<input type=checkbox name=allClass value=1 onClick=(this.checked)?setClassBox(1,this.form):setClassBox(0,this.form)> $i_status_all $i_SmartCard_ClassName<br>\n";
#
$curr_lvl = "";
for ($i=0; $i<sizeof($classes); $i++)
{
     list($classID, $classname, $classlevel) = $classes[$i];
     if ($classlevel!=$curr_lvl && $curr_lvl!="")
     {
         $select_class .= "<br>\n";
     }
     $curr_lvl = $classlevel;

     $select_class .= "<input type=checkbox name=ClassID[] value='$classID'>$classname &nbsp;\n";
}

/*


*/
?>
<SCRIPT LANGUAGE=Javascript>
function setClassBox(allchecked, obj)
{
        len=obj.elements.length;

        var i=0;
        var disable;
        for( i=0 ; i<len ; i++)
        {
             if (obj.elements[i].name=="ClassID[]")
             {
                 obj.elements[i].disabled=allchecked;
                 if (allchecked)
                 {
                     obj.elements[i].checked=allchecked;
                 }
             }
        }
}
</SCRIPT>
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].TargetDate.value = dateValue;
          }
 // -->
 </script>

<form name="form1" method="post" action="show.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DataManagement,'../',$i_StudentAttendance_Menu_DataMgmt_UndoPastProfileRecord, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_StudentAttendance_Field_Date; ?>:</td><td>
<input type=text name=TargetDate value='<?=date('Y-m-d')?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td align=right><?php echo $i_SmartCard_ClassName; ?>:</td><td><?=$select_class?></td></tr>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>
<?
include_once("../../../../templates/adminfooter.php");
?>