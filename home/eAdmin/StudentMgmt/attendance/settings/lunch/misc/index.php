<?php
// Editing by 
/*
 * 2018-01-08 Carlos: Always allow to set MinsToTreatAsPMIn even do not need to record lunch out, allow school to control when to count as PM in time.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_LunchSettings";

$linterface = new interface_html();

if($update=="1"){
	$Setting['NoRecordLunchOut'] = ($no_record=="1")?$no_record:"0";
	$Setting['LunchOutOnce'] = ($out_once=="1")?$out_once:"0";
	$Setting['AllAllowGoOut'] = ($all_allow=="1")?$all_allow:"0";
	$Setting['MinsToTreatAsPMIn'] = ($MinsToTreatAsPMIn == "")? "0":$MinsToTreatAsPMIn;
	
	$GeneralSetting->Start_Trans();
	if ($GeneralSetting->Save_General_Setting('StudentAttendance',$Setting)) {
		$GeneralSetting->Commit_Trans();
		$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	}
	else {
		$GeneralSetting->RollBack_Trans();
		$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	}
}

$SettingList[] = "'NoRecordLunchOut'";
$SettingList[] = "'LunchOutOnce'";
$SettingList[] = "'AllAllowGoOut'";
$SettingList[] = "'MinsToTreatAsPMIn'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);

$checked_no_record = ($Settings['NoRecordLunchOut']=="1")?"checked":"";
$checked_out_once = ($Settings['LunchOutOnce']=="1")?"checked":"";
$checked_all_allow = ($Settings['AllAllowGoOut']=="1")?"checked":"";
$MinsToTreatAsPMIn = ($Settings['MinsToTreatAsPMIn']=="")?"0":$Settings['MinsToTreatAsPMIn'];

$select_no_record = "<input name=\"no_record\" type=\"checkbox\" $checked_no_record value=\"1\">&nbsp;$i_SmartCard_Settings_Lunch_Misc_No_Record";
$select_out_once = "<input name=\"out_once\" type=\"checkbox\" $checked_out_once value=\"1\">&nbsp;$i_SmartCard_Settings_Lunch_Misc_Out_Once";
$select_all_allow = "<input name=\"all_allow\" type=\"checkbox\" $checked_all_allow value=\"1\">&nbsp;$i_SmartCard_Settings_Lunch_Misc_All_Allow";

$TAGS_OBJ[] = array($i_SmartCard_Settings_Lunch_List_Title, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/lunch/list/", 0);
$TAGS_OBJ[] = array($i_SmartCard_Settings_Lunch_Misc_Title, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/lunch/misc/", 1);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get" action="">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="no_record"><?= $i_SmartCard_Settings_Lunch_Misc_No_Record ?></label></td>
		<td class="tabletext" width="70%"><input name="no_record" id="no_record" type="checkbox" <?=$checked_no_record?> value="1" /></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="out_once"><?= $i_SmartCard_Settings_Lunch_Misc_Out_Once ?></label></td>
		<td class="tabletext" width="70%"><input name="out_once" id="out_once" type="checkbox" <?=$checked_out_once?> value="1"></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="all_allow"><?= $i_SmartCard_Settings_Lunch_Misc_All_Allow ?></label></td>
		<td class="tabletext" width="70%"><input name="all_allow" id="all_allow" type="checkbox" <?=$checked_all_allow?> value="1"></td>
	</tr>
<?
	if ($lc->attendance_mode == "2") {
?>
	<tr id="TreatAsPMInRow">
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="all_allow"><?= $Lang['StudentAttendance']['MinsToTreatAsPMIn'] ?></label></td>
		<td class="tabletext" width="70%">
			<input name="MinsToTreatAsPMIn" id="MinsToTreatAsPMIn" type="text" maxlength="3" size="3" class="tabletext" value="<?=$MinsToTreatAsPMIn?>">
			<span class="tabletextremark">
				<?=$Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecordNote']?>
			</span>
		</td>
	</tr>
<?
	}
?>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="update" value="1">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>