<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();

$li->Start_Trans();
if(sizeOf($student_id)>0){
    for($i=0; $i<sizeOf($student_id); $i++){
        $fieldname = "StudentID, DateInput, DateModified";
        $fieldvalue = "'".$student_id[$i]."',NOW(),NOW()";
        $sql = "INSERT IGNORE INTO CARD_STUDENT_LUNCH_ALLOW_LIST ($fieldname) VALUES ($fieldvalue)";
        $Result[] = $li->db_db_query($sql);
    }
} else if(sizeOf($class_name)>0) {
    $list = "'" . implode("','",$class_name) . "'";
    $fieldname = "StudentID, DateInput, DateModified";
    $sub_sql = "SELECT UserID, now(), now() FROM INTRANET_USER WHERE ClassName IN ($list) AND RecordType=2 AND RecordStatus IN (0,1,2)";
    $sql = "INSERT IGNORE INTO CARD_STUDENT_LUNCH_ALLOW_LIST ($fieldname)
                   $sub_sql
            ";
    $Result[] = $li->db_db_query($sql);
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$li->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$li->Commit_Trans();
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>