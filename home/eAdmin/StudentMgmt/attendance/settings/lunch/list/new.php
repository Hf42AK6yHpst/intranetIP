<?
// editing by 
/*
 * 2012-09-10 (Carlos): fixed [Whole Level] checkbox for select_type=1
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_LunchSettings";

$TempUserID = $_SESSION['UserID'];

$linterface = new interface_html();

#class used
$LICLASS = new libclass();

# build select individual add or class
## select current/past
$firstname_type_table = "-- $button_select --";
$array_type_table_name = array($i_SmartCard_Settings_Lunch_New_Type_Class, $i_SmartCard_Settings_Lunch_New_Type_Student);
$array_type_table_data = array("1", "2");
$select_type_table = getSelectByValueDiffName($array_type_table_data,$array_type_table_name,"name='select_type' onChange=checkGet(this.form,'new.php')",$select_type,1,0, $firstname_type_table);


# build selection of class - for class
if($select_type=="1"){
        $select_all_class_table = "";
        $class_list = $LICLASS->getClassList();
        //$current_level = $class_list[0][2];
        $lvlArray = array();
        $x = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\">";
        $x .= "<tr><td class=\"tabletext\">";
        $x .= "<input type=\"checkbox\" name=\"checkall\" onclick=\"checkUncheckAll(this)\">$i_general_WholeSchool</td></tr>";
        $x .= "<tr><td class=\"tabletext\">";
        for($i=0; $i<sizeOf($class_list); $i++){
                list($ClassID, $ClassName, $ClassLevelID) = $class_list[$i];
                
                $x .= "<input name=\"class_name[]\" id=\"class_name$ClassName\" type=\"checkbox\" value=\"$ClassName\"><label for=\"class_name".$ClassName."\">$ClassName</label>".toolBarSpacer();
                $lvlArray[] = "class_name$ClassName";
                
                //if ($current_level != $ClassLevelID) {
                if( $ClassLevelID != $class_list[$i+1][2]) {	
	                if (sizeof($lvlArray) > 1) {
				     	$x .= "<input type=\"checkbox\" name=\"checkall$ClassLevelID$i\" id=\"checkall$ClassLevelID$i\" onclick=\"checkUncheckPart('checkall$ClassLevelID$i','";
				     	$x .= implode(",", $lvlArray);
				     	$x .= "')\"> $i_Payment_Item_Select_Class_Level";
				     	$x .= " ";
			    	}
			    	$lvlArray = array();
			    	//$current_level = $ClassLevelID;
        			$x .= "</td></tr><tr><td class=\"tabletext\">";
		    	}
        }
        $x .="</td></tr>";
        $x .= "</table>\n";

        $select_all_class_table = $x;
        $str_instruction = "$i_SmartCard_Settings_Lunch_Instruction_Class<br /><br />";
}
else if($select_type=="2"){     # build selection of class - for student

        $select_class_table = "";
        $class_list = $LICLASS->getClassList();
        $x = "<select name=\"select_class\" onChange=\"checkGet(this.form,'new.php')\">\n";
        $x .= "<option value=\"\"> -- $button_select -- </option>\n";
        for($i=0; $i<sizeOf($class_list); $i++){
                list($ClassID, $ClassName, $ClassLevelID) = $class_list[$i];
                $selected = ($ClassName==$select_class)?"SELECTED":"";
                $x .= "<option value=\"$ClassName\" $selected>$ClassName</option>\n";
        }
        $x .= "</select>\n";
        $select_class_table = $x;
}

#build student
if($select_class<>"" AND $select_type=="2"){
        $select_student_table = "";

        $name_field = getNameFieldByLang("a.");
        $sql = "SELECT a.UserID, $name_field, a.ClassNumber FROM INTRANET_USER as a LEFT OUTER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST as b ON (a.UserID=b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = '$select_class' AND b.StudentID IS NULL ORDER BY a.ClassNumber, a.EnglishName";
        $student_list = $LICLASS->returnArray($sql,3);

    	$checkall = "<input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'student_id[]'):setChecked(0,this.form,'student_id[]')\">";

        $x  = "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
        $x .= "<tr class=\"tabletop\">";
        $x .= "<td class=\"tabletoplink\">$checkall</td>";
        $x .= "<td class=\"tabletoplink\">$i_UserClassNumber</td>";
        $x .= "<td class=\"tabletoplink\">$i_UserName</td>";
        $x .= "</tr>";
        for($i=0; $i<sizeOf($student_list); $i++){
        	list($UserID, $name_field, $ClassNumber) = $student_list[$i];
            $css = ($i%2==0)?"tablerow1":"tablerow2";
        	$x .= "<tr class=\"$css\">";
        	$x .= "<td class=\"tabletext\"><input name=\"student_id[]\" type=\"checkbox\" value=\"$UserID\"></td>";
        	$x .= "<td class=\"tabletext\">$ClassNumber</td>";
        	$x .= "<td class=\"tabletext\">$name_field</td>";
        	$x .= "</tr>";
        }
        if (sizeof($student_list)==0)
        {
            $x .= "<tr class=\"$css\">";
            $x .= "<td class=\"tabletext\" align=\"center\" colspan=\"3\">$i_SmartCard_Settings_NoStudentsAvailableForSelection</td>";
            $x .= "</tr>";
        }
        $x .= "</table>\n";

        $select_student_table = $x;
        $str_instruction = "$i_SmartCard_Settings_Lunch_Instruction_Student<br /><br />";
}

$TAGS_OBJ[] = array($i_SmartCard_Settings_Lunch_List_Title, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/lunch/list/", 1);
$TAGS_OBJ[] = array($i_SmartCard_Settings_Lunch_Misc_Title, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/lunch/misc/", 0);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="javascript">
function checkUncheckAll(theElement) {
	var theForm = theElement.form, z = 0;
	for(z=0; z<theForm.length;z++){
		if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkall'){
			theForm[z].checked = theElement.checked;
		}
	}
}

function checkUncheckPart(controller,theElements) {
	var formElements = theElements.split(',');
	var theController = document.getElementById(controller);
	for(var z=0; z<formElements.length;z++){
		theItem = document.getElementById(formElements[z]);
		if(theItem){
			if(theItem.type){
				if(theItem.type == 'checkbox' && theItem.id != theController.id){
					theItem.checked = theController.checked;
				}
			} else {
			
				var nextArray = '';
				for(var x=0;x <theItem.childNodes.length;x++){
					if(theItem.childNodes[x]){
						if (theItem.childNodes[x].id){
							nextArray += theItem.childNodes[x].id+',';
						}
					}
				}
				checkUncheckPart(controller,nextArray);
			}
		}
	}
}

function checkform(obj){
        <?
            if ($select_type=="1"){
                    echo "var element = 'class_name[]'";
            }
            else if ($select_type=="2"){
                    echo "var element = 'student_id[]'";
            }
        ?>

        if(countChecked(obj,element)==0) {
            alert(globalAlertMsg2);
            return false;
        }

    return true;
}
</script>
<br />
<form name="form1" method="post" action="new_update.php" method="post" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_SmartCard_Settings_Lunch_New_Type ?></td>
					<td class="tabletext" width="70%"><?=$select_type_table?></td>
				</tr>
				<? if($select_type==1) { ?>
				<tr>
					<td valign="top" class="tabletext" colspan="2"><?= $select_class_table ?></td>
				</tr>
				<? } else if ($select_type==2) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=($select_type==2)?$i_ClassName:""?></td>
					<td class="tabletext" width="70%"><?=$select_class_table?></td>
				</tr>
				<? } else if ($select_type<>"") { ?>
				<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <? } ?>
                <tr>
					<td valign="top" class="tabletext" colspan="2"><br /><?= $str_instruction ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<? if($select_student_table<>"") { ?>
				<tr>
					<td class="tabletext" align="left"><?=$select_student_table?></td>
				</tr>
				<? } 
				   if($select_all_class_table<>"") {
				?>
				<tr>
					<td class="tabletext" align="left"><?=$select_all_class_table?></td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
	    <td align="center">
	    	<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	    		<tr><td align="center">
			    <?
		        if (($select_class<>""&&sizeof($student_list)!=0) || $select_type=="1") {
				?>
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<? } ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='index.php?select_class=$select_class&keyword=$keyword'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				</td></tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$_SESSION['UserID'] = $TempUserID;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>