<?
### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_lunch_setting_view_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_lunch_setting_view_page_number", $pageNo, 0, "", "", 0);
	$ck_lunch_setting_view_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_lunch_setting_view_page_number!="")
{
	$pageNo = $ck_lunch_setting_view_page_number;
}

if ($ck_lunch_setting_view_page_order!=$order && $order!="")
{
	setcookie("ck_lunch_setting_view_page_order", $order, 0, "", "", 0);
	$ck_lunch_setting_view_page_order = $order;
} else if (!isset($order) && $ck_lunch_setting_view_page_order!="")
{
	$order = $ck_lunch_setting_view_page_order;
}

if ($ck_lunch_setting_view_page_field!=$field && $field!="")
{
	setcookie("ck_lunch_setting_view_page_field", $field, 0, "", "", 0);
	$ck_lunch_setting_view_page_field = $field;
} else if (!isset($field) && $ck_lunch_setting_view_page_field!="")
{
	$field = $ck_lunch_setting_view_page_field;
}

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_LunchSettings";

$linterface = new interface_html();

# TABLE SQL
$keyword = trim($keyword);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 3;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        default: $field = 3; break;
}

$conds = "";

if($select_class<>""){
        $conds = "AND b.ClassName = \"$select_class\" ";
}

$sql  = "SELECT
                b.UserLogin,
                b.EnglishName,
                b.ChineseName,
                b.ClassName,
                b.ClassNumber,
                CONCAT('<input type=''checkbox'' name=''RecordID[]'' value=''', a.RecordID ,'''>')
        FROM
                CARD_STUDENT_LUNCH_ALLOW_LIST AS a
                                                LEFT OUTER JOIN INTRANET_USER  AS b ON (a.StudentID=b.UserID)
        WHERE
                (b.UserLogin like '%$keyword%' OR
                b.UserEmail like '%$keyword%' OR
                b.EnglishName like '%$keyword%' OR
                b.ChineseName like '%$keyword%' OR
                b.ClassName like '%$keyword%'
                )
                $conds
        ";
//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.UserLogin","b.EnglishName","b.ChineseName","b.ClassName","b.ClassNumber");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_user;
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,1,0,0,0);
$li->IsColOff = 2;

# TABLE COLUMN
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='18%'>".$li->column(0, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width='30%'>".$li->column(1, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(2, $i_UserChineseName)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(3, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(4, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')","","","","",0);

# filter
$LIDB = new libdb();

$sql = "SELECT DISTINCT b.ClassName
        FROM
                CARD_STUDENT_LUNCH_ALLOW_LIST as a
                LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID=b.UserID)
        WHERE TRIM(b.ClassName) <> '' 
		ORDER BY b.ClassName";
$class_list = $LIDB->returnVector($sql);

$x = "<select name=\"select_class\" onChange=\"submit()\">\n";
$x .= "<option value=\"\" ".(!isset($select_class) || $select_class==""? "SELECTED":"")."> -- $i_general_all_classes -- </option>\n";
for($i=0; $i<sizeOf($class_list); $i++){
	if ($class_list[$i] != "" && $select_class != "")
    	$selected = ($class_list[$i]==$select_class)?"SELECTED":"";
    else
    	$selected = "";
    $x .= "<option value=\"".$class_list[$i]."\" $selected>".$class_list[$i]."</option>\n";
}
$x .= "</select>\n";

$filterbar .= $x;

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" alt=\"button_remove\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";

$TAGS_OBJ[] = array($i_SmartCard_Settings_Lunch_List_Title, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/lunch/list/", 1);
$TAGS_OBJ[] = array($i_SmartCard_Settings_Lunch_Misc_Title, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/lunch/misc/", 0);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>