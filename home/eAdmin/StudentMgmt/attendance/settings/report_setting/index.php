<?
//editing by 
/*
 * 2014-09-30 (Carlos): Changed add button and delete button style.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$lword = new libwordtemplates();
$AbsentWords = $lword->getWordListAttendance(PROFILE_TYPE_ABSENT);
$LateWords = $lword->getWordListAttendance(PROFILE_TYPE_LATE);
$EarlyLeaveWords = $lword->getWordListAttendance(PROFILE_TYPE_EARLY);

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_ReportSettings";

$linterface = new interface_html();

$sql = "SELECT StatusSymbol FROM CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL";
$ResultArr = $lc->returnArray($sql);

$sqlAbsent = "SELECT Reason, StatusSymbol FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType='".PROFILE_TYPE_ABSENT."' ORDER BY LastModified";
$AbsentArr = $lc->returnArray($sqlAbsent);

$sqlLate = "SELECT Reason, StatusSymbol FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType='".PROFILE_TYPE_LATE."' ORDER BY LastModified";
$LateArr = $lc->returnArray($sqlLate);

$sqlEarlyLeave = "SELECT Reason, StatusSymbol FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType='".PROFILE_TYPE_EARLY."' ORDER BY LastModified";
$EarlyLeaveArr = $lc->returnArray($sqlEarlyLeave);

$AbsentRowID = 0;
$LateRowID = 0;
$EarlyLeaveRowID = 0;

$AbsentRows = "";
for($i=0;$i<sizeof($AbsentArr);$i++)
{
	$AbsentRows.="<tr id=\"AbsRow".$AbsentRowID."\">";
	$AbsentRows.="<td class=\"tablerow1\"><input type='text' name='AbsentReasons[]' id='AbsReason".$AbsentRowID."' width='100%' maxlength='255' value='".htmlspecialchars($AbsentArr[$i]['Reason'],ENT_QUOTES)."' />&nbsp;";
	$AbsentRows.="<a href=\"javascript:void(0);\" onmousedown=\"ShowSelection(event,'AbsentMenu','AbsReason".$AbsentRowID."');\"><img onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('absposimg$AbsentRowID','','"."{$image_path}/{$LAYOUT_SKIN}"."/icon_pre-set_on.gif',1)\" name=\"absposimg$AbsentRowID\" id=\"absposimg$AbsentRowID\" src=\""."{$image_path}/{$LAYOUT_SKIN}"."/icon_pre-set_off.gif\" border=\"0\" alt=\"".$i_Profile_SelectReason."\" title=\"".$i_Profile_SelectReason."\"></a></td>";
	$AbsentRows.="<td class=\"tablerow1\"><input type='text' name='AbsentSymbols[]' size='5' maxlength='3' value='".htmlspecialchars($AbsentArr[$i]['StatusSymbol'],ENT_QUOTES)."' /></td>";
	$AbsentRows.="<td class=\"tablerow1\" align=\"right\"><span class=\"table_row_tool\" style=\"float:right;\"><a class=\"delete\" href=\"javascript:deleteRow('AbsentReasonTable','AbsRow".$AbsentRowID."')\" alt=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" title=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" ></a></span></td>";
	$AbsentRows.="</tr>\n";
	$AbsentRowID += 1;
}

$LateRows = "";
for($i=0;$i<sizeof($LateArr);$i++)
{
	$LateRows.="<tr id=\"LatRow".$LateRowID."\">";
	$LateRows.="<td class=\"tablerow1\"><input type='text' name='LateReasons[]' id='LatReason".$LateRowID."' width='100%' maxlength='255' value='".htmlspecialchars($LateArr[$i]['Reason'],ENT_QUOTES)."' />&nbsp;";
	$LateRows.="<a href=\"javascript:void(0);\" onmousedown=\"ShowSelection(event,'LateMenu','LatReason".$LateRowID."');\"><img onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('latposimg$LateRowID','','"."{$image_path}/{$LAYOUT_SKIN}"."/icon_pre-set_on.gif',1)\" name=\"latposimg$LateRowID\" id=\"latposimg$LateRowID\"  src=\""."{$image_path}/{$LAYOUT_SKIN}"."/icon_pre-set_off.gif\" border=\"0\" alt=\"".$i_Profile_SelectReason."\" title=\"".$i_Profile_SelectReason."\"></a></td>";
	$LateRows.="<td class=\"tablerow1\"><input type='text' name='LateSymbols[]' size='5' maxlength='3' value='".htmlspecialchars($LateArr[$i]['StatusSymbol'],ENT_QUOTES)."' /></td>";
	$LateRows.="<td class=\"tablerow1\" align=\"right\"><span class=\"table_row_tool\" style=\"float:right;\"><a class=\"delete\" href=\"javascript:deleteRow('LateReasonTable','LatRow".$LateRowID."')\" alt=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" title=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" ></a></span></td>";
	$LateRows.="</tr>\n";
	$LateRowID += 1;
}

$EarlyLeaveRows = "";
for($i=0;$i<sizeof($EarlyLeaveArr);$i++)
{
	$EarlyLeaveRows.="<tr id=\"EalRow".$EarlyLeaveRowID."\">";
	$EarlyLeaveRows.="<td class=\"tablerow1\"><input type='text' name='EarlyLeaveReasons[]' id='EalReason".$EarlyLeaveRowID."' width='100%' maxlength='255' value='".htmlspecialchars($EarlyLeaveArr[$i]['Reason'],ENT_QUOTES)."' />&nbsp;";
	$EarlyLeaveRows.="<a href=\"javascript:void(0);\" onmousedown=\"ShowSelection(event,'EarlyLeaveMenu','EalReason".$EarlyLeaveRowID."');\"><img onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('ealposimg$EarlyLeaveRowID','','"."{$image_path}/{$LAYOUT_SKIN}"."/icon_pre-set_on.gif',1)\" name=\"ealposimg$EarlyLeaveRowID\" id=\"ealposimg$EarlyLeaveRowID\"  src=\""."{$image_path}/{$LAYOUT_SKIN}"."/icon_pre-set_off.gif\" border=\"0\" alt=\"".$i_Profile_SelectReason."\" title=\"".$i_Profile_SelectReason."\"></a></td>";
	$EarlyLeaveRows.="<td class=\"tablerow1\"><input type='text' name='EarlyLeaveSymbols[]' size='5' maxlength='3' value='".htmlspecialchars($EarlyLeaveArr[$i]['StatusSymbol'],ENT_QUOTES)."' /></td>";
	$EarlyLeaveRows.="<td class=\"tablerow1\" align=\"right\"><span class=\"table_row_tool\" style=\"float:right;\"><a class=\"delete\" href=\"javascript:deleteRow('EarlyLeaveReasonTable','EalRow".$EarlyLeaveRowID."')\" alt=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" title=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\"></a></span></td>";
	$EarlyLeaveRows.="</tr>\n";
	$EarlyLeaveRowID += 1;
}



$TAGS_OBJ[] = array($Lang['SmartCard']['StudentAttendence']['ReportSettings'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);
//if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="javascript" type="text/javascript">
var TargetWord="";//The Textbox to be filled from selection
function ShowSelection(event, layerid, fillWhichWord)
{
	var e;
	if (!event) e = window.event;
	else e=event;
	var posx,posy;
	
	if(document.all)
	{
		posy=e.clientY+document.documentElement.scrollTop;
		posx=e.clientX+document.documentElement.scrollLeft;
	}else
	{
		posy=e.pageY;
		posx=e.pageX;
	}
	//if(e.pageY) posy=e.pageY;
	//else posy=e.clientY+document.body.scrollTop;
	//if(e.pageX) posx=e.pageX;
	//else posx=e.clientX+document.body.scrollLeft;
	
	var layer=document.getElementById(layerid);
	var offsety=posy-5;
	layer.style.top=offsety+"px";
	var offsetx=posx+5;
	layer.style.left=offsetx+"px";
	
	HideSelection();
	layer.style.visibility="visible";
	
	TargetWord = fillWhichWord;
}

function HideSelection()
{
	document.getElementById("AbsentMenu").style.visibility="hidden";
	document.getElementById("LateMenu").style.visibility="hidden";
	document.getElementById("EarlyLeaveMenu").style.visibility="hidden";
}

function FillWord(wordid)
{
	var word = document.getElementById(wordid).value;
	var target = document.getElementById(TargetWord);
	if(target != null) target.value = word;
	HideSelection();
}

var AbsentRowID = <?=$AbsentRowID?>;
var LateRowID = <?=$LateRowID?>;
var EarlyLeaveRowID = <?=$EarlyLeaveRowID?>;

function deleteRow(tableID, rowID)
{
	var tableBody = document.getElementById(tableID).getElementsByTagName("tbody")[0];
	tableBody.removeChild(document.getElementById(rowID));
}

function addRow(tableID, ReasonType)
{
    var tableBody = document.getElementById(tableID).getElementsByTagName("tbody")[0];
    var rowObj = document.createElement("tr");
    if(ReasonType == "Absent")
    	rowObj.setAttribute("id","AbsRow"+AbsentRowID);
    else if(ReasonType == "Late")
    	rowObj.setAttribute("id","LatRow"+LateRowID);
    else
    	rowObj.setAttribute("id","EalRow"+EarlyLeaveRowID);
    
    var txtReason="";
    if(ReasonType == "Absent")
    {
    	txtReason += "<input type='text' name='"+ReasonType+"Reasons[]' id='AbsReason"+AbsentRowID+"' width='100%' maxlength='255' value='' />&nbsp;";
    	txtReason += "<a href=\"javascript:void(0);\" onmousedown=\"ShowSelection(event,'AbsentMenu','AbsReason"+AbsentRowID+"');\"><img onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('absposimg"+AbsentRowID+"','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_on.gif',1)\" name=\"absposimg"+AbsentRowID+"\" id=\"absposimg"+AbsentRowID+"\" src=\"<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_off.gif\" border=\"0\" alt=\"<?=$i_Profile_SelectReason?>\" title=\"<?=$i_Profile_SelectReason?>\"></a>";
    }
    else if(ReasonType == "Late")
    {
    	txtReason += "<input type='text' name='"+ReasonType+"Reasons[]' id='LatReason"+LateRowID+"' width='100%' maxlength='255' value='' />&nbsp;";
    	txtReason += "<a href=\"javascript:void(0);\" onmousedown=\"ShowSelection(event,'LateMenu','LatReason"+LateRowID+"');\"><img onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('latposimg"+LateRowID+"','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_on.gif',1)\" name=\"latposimg"+LateRowID+"\" id=\"latposimg"+LateRowID+"\" src=\"<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_off.gif\" border=\"0\" alt=\"<?=$i_Profile_SelectReason?>\" title=\"<?=$i_Profile_SelectReason?>\"></a>";
    }
    else
    {
    	txtReason += "<input type='text' name='"+ReasonType+"Reasons[]' id='EalReason"+EarlyLeaveRowID+"' width='100%' maxlength='255' value='' />&nbsp;";
    	txtReason += "<a href=\"javascript:void(0);\" onmousedown=\"ShowSelection(event,'EarlyLeaveMenu','EalReason"+EarlyLeaveRowID+"');\"><img onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('ealposimg"+EarlyLeaveRowID+"','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_on.gif',1)\" name=\"ealposimg"+EarlyLeaveRowID+"\" id=\"ealposimg"+EarlyLeaveRowID+"\" src=\"<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_off.gif\" border=\"0\" alt=\"<?=$i_Profile_SelectReason?>\" title=\"<?=$i_Profile_SelectReason?>\"></a>";
    }
    
    var td1 = document.createElement("td");
    td1.setAttribute("class","tablerow1");
    //td1.setAttribute("align","center");
    td1.innerHTML = txtReason;
    
    var txtSymbol = "<input type='text' name='"+ReasonType+"Symbols[]' size='5' maxlength='3' value='' />";
    
    var td2 = document.createElement("td");
    td2.innerHTML = txtSymbol;
    td2.setAttribute("class","tablerow1");
    //td2.setAttribute("align","center");
    
    //var deleteButton = document.createElement("a");
    var delete_js = '';
    if(ReasonType == "Absent"){
    	//deleteButton.setAttribute("href", "javascript:deleteRow('"+tableID+"','AbsRow"+AbsentRowID+"');");
    	delete_js = "javascript:deleteRow('"+tableID+"','AbsRow"+AbsentRowID+"');";
    }else if(ReasonType == "Late"){
    	//deleteButton.setAttribute("href", "javascript:deleteRow('"+tableID+"','LatRow"+LateRowID+"');");
    	delete_js = "javascript:deleteRow('"+tableID+"','LatRow"+LateRowID+"');";
    }else{
    	//deleteButton.setAttribute("href", "javascript:deleteRow('"+tableID+"','EalRow"+EarlyLeaveRowID+"');");
    	delete_js = "javascript:deleteRow('"+tableID+"','EalRow"+EarlyLeaveRowID+"');";
    }
    //deleteButton.setAttribute("alt","<?=$Lang['SmartCard']['StudentAttendance']['DeleteRow']?>");
    //deleteButton.setAttribute("title","<?=$Lang['SmartCard']['StudentAttendance']['DeleteRow']?>");
    //deleteButton.appendChild(document.createTextNode("X"));
    
    var deleteCell = '<span class="table_row_tool" style="float:right;"><a class="delete" href="'+delete_js+'" alt="<?=$Lang['SmartCard']['StudentAttendance']['DeleteRow']?>" title="<?=$Lang['SmartCard']['StudentAttendance']['DeleteRow']?>"></a></span>';
    
    var td3 = document.createElement("td");
    //td3.appendChild (deleteButton);
    td3.innerHTML = deleteCell;
   	td3.setAttribute("class","tablerow1");
    td3.setAttribute("align","right");
    
    rowObj.appendChild(td1);
    rowObj.appendChild(td2);
    rowObj.appendChild(td3);
    
    tableBody.appendChild(rowObj);
    
    if(ReasonType == "Absent")
    	AbsentRowID += 1;
    else if(ReasonType == "Late")
    	LateRowID += 1;
    else
    	EarlyLeaveRowID += 1;
}

function CheckDuplicate()
{
	var defPresent = document.getElementById("HiddenPresent").value;
	var defAbsent = document.getElementById("HiddenAbsent").value;
	var defLate = document.getElementById("HiddenLate").value;
	var defEarlyLeave = document.getElementById("HiddenEarlyLeave").value;
	var defWaived = document.getElementById("HiddenWaived").value;
	
	var StatPresent = Trim(document.getElementById("StatusNormal").value);
	var StatAbsent = Trim(document.getElementById("StatusAbsent").value);
	var StatLate = Trim(document.getElementById("StatusLate").value);
	var StatEarlyLeave = Trim(document.getElementById("StatusEarlyLeave").value);
	var StatWaived = Trim(document.getElementById("StatusWaived").value);
	
	var PresentSymbol="";
	if(StatPresent == "")
		PresentSymbol = defPresent;
	else
		PresentSymbol = StatPresent;
	
	var AbsentSymbol="";
	if(StatAbsent == "")
		AbsentSymbol = defAbsent;
	else
		AbsentSymbol = StatAbsent;
		
	var LateSymbol="";
	if(StatLate == "")
		LateSymbol = defLate;
	else
		LateSymbol = StatLate;
		
	var EarlyLeaveSymbol="";
	if(StatEarlyLeave == "")
		EarlyLeaveSymbol = defEarlyLeave;
	else
		EarlyLeaveSymbol = StatEarlyLeave;
		
	var WaivedSymbol="";
	if(StatWaived == "")
		WaivedSymbol = defWaived;
	else
		WaivedSymbol = StatWaived;
	
	var AbsReasonsArr=new Array();
	AbsReasonsArr=document.getElementsByName("AbsentReasons[]");
	var AbsSymbolsArr=new Array();
	AbsSymbolsArr=document.getElementsByName("AbsentSymbols[]");
	var LatReasonsArr=new Array();
	LatReasonsArr=document.getElementsByName("LateReasons[]");
	var LatSymbolsArr=new Array();
	LatSymbolsArr=document.getElementsByName("LateSymbols[]");
	var EalReasonsArr=new Array();
	EalReasonsArr=document.getElementsByName("EarlyLeaveReasons[]");
	var EalSymbolsArr=new Array();
	EalSymbolsArr=document.getElementsByName("EarlyLeaveSymbols[]");
	
	var AbsReasons=new Array();
	for(var i=0;i<AbsReasonsArr.length;i++)
	{
		AbsReasons.push(Trim(AbsReasonsArr[i].value));
	}
	var Symbols=new Array();
	Symbols.push(PresentSymbol);
	Symbols.push(AbsentSymbol);
	Symbols.push(LateSymbol);
	Symbols.push(EarlyLeaveSymbol);
	Symbols.push(WaivedSymbol);
	
	for(var i=0;i<AbsSymbolsArr.length;i++)
	{
		Symbols.push(Trim(AbsSymbolsArr[i].value));
	}
	
	var LatReasons=new Array();
	for(var i=0;i<LatReasonsArr.length;i++)
	{
		LatReasons.push(Trim(LatReasonsArr[i].value));
	}
	
	for(var i=0;i<LatSymbolsArr.length;i++)
	{
		Symbols.push(Trim(LatSymbolsArr[i].value));
	}
	
	var EalReasons=new Array();
	for(var i=0;i<EalReasonsArr.length;i++)
	{
		EalReasons.push(Trim(EalReasonsArr[i].value));
	}
	
	for(var i=0;i<EalSymbolsArr.length;i++)
	{
		Symbols.push(Trim(EalSymbolsArr[i].value));
	}
	
	var error_output = "";
	var no_dup_reasons = 0;
	var no_dup_symbols = 0;
	
	// Check duplicated reasons for Absent
	for(var i=0;i<AbsReasons.length;i++)
	{
		for(var j=i+1;j<AbsReasons.length;j++)
		{
			if(AbsReasons[i].valueOf() != "" && AbsReasons[j].valueOf() != "")
			{
				if(AbsReasons[i].valueOf() == AbsReasons[j].valueOf() )
				{
					no_dup_reasons += 1;
				}
			}
		}
	}
	
	// Check duplicated reasons for Late
	for(var i=0;i<LatReasons.length;i++)
	{
		for(var j=i+1;j<LatReasons.length;j++)
		{
			if(LatReasons[i].valueOf() != "" && LatReasons[j].valueOf() != "")
			{
				if(LatReasons[i].valueOf() == LatReasons[j].valueOf() )
				{
					no_dup_reasons += 1;
				}
			}
		}
	}
	
	// Check duplicated reasons for early leave
	for(var i=0;i<EalReasons.length;i++)
	{
		for(var j=i+1;j<EalReasons.length;j++)
		{
			if(EalReasons[i].valueOf() != "" && EalReasons[j].valueOf() != "")
			{
				if(EalReasons[i].valueOf() == EalReasons[j].valueOf() )
				{
					no_dup_reasons += 1;
				}
			}
		}
	}
	
	for(var i=0;i<Symbols.length;i++)
	{
		for(var j=i+1;j<Symbols.length;j++)
		{
			if(Symbols[i].valueOf() != "" && Symbols[j].valueOf() != "")
			{
				if(Symbols[i].valueOf() == Symbols[j].valueOf() )
				{
					no_dup_symbols += 1;
				}
			}
		}
	}
	
	if(no_dup_reasons > 0) error_output = error_output + no_dup_reasons+" <?=$Lang['SmartCard']['StudentAttendance']['ReasonsAreDuplicated']?>\n";
	if(no_dup_symbols > 0) error_output = error_output + no_dup_symbols+" <?=$Lang['SmartCard']['StudentAttendance']['SymbolsAreDuplicated']?>\n";
	
	if(error_output != "")
	{
		alert(error_output);
		return false;
	}else
	{
		return true;
	}
}

function SubmitForm()
{
	if(CheckDuplicate())
	{
		document.getElementById("form1").submit();
	}else
	{
		return false;
	}
}
</script>


<br />
<form name="form1" id="form1" method="post" action="update.php">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td align="left"><?=$Lang['SmartCard']['StudentAttendance']['CustomizeStatusSymbols']?></td></tr>
</table>
<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
		<td class="tabletoplink"><?=$i_StudentAttendance_Status?></td>
		<td class="tabletoplink"><?=$Lang['SmartCard']['StudentAttendence']['CustomSymbol']?></td>
		<td class="tabletoplink"><?=$Lang['SmartCard']['StudentAttendence']['DefaultSymbol']?></td>
	</tr>
  	<tr>
  		<td class="tablerow1"><?=$i_StudentAttendance_Status_Present?>&nbsp;</td>
  		<td class="tablerow1"><input type=text name=StatusNormal id="StatusNormal" size=5 maxlength=3 value="<?=($ResultArr[0]['StatusSymbol']==NULL)?"":htmlspecialchars($ResultArr[0]['StatusSymbol'],ENT_QUOTES);?>"></td>
  		<td class="tablerow1"><?=$i_StudentAttendance_Symbol_Present?></td>
  	</tr>
  	<tr>
  		<td class="tablerow2"><?=$i_StudentAttendance_Status_Absent?>&nbsp;</td>
  		<td class="tablerow2"><input type=text name=StatusAbsent id="StatusAbsent" size=5 maxlength=3 value="<?=($ResultArr[1]['StatusSymbol']==NULL)?"":htmlspecialchars($ResultArr[1]['StatusSymbol'],ENT_QUOTES);?>"></td>
  		<td class="tablerow2"><?=$i_StudentAttendance_Symbol_Absent?></td>
  	</tr>
  	<tr>
  		<td class="tablerow1"><?=$i_StudentAttendance_Status_Late?>&nbsp;</td>
  		<td class="tablerow1"><input type=text name=StatusLate id="StatusLate" size=5 maxlength=3 value="<?=($ResultArr[2]['StatusSymbol']==NULL)?"":htmlspecialchars($ResultArr[2]['StatusSymbol'],ENT_QUOTES);?>"></td>
  		<td class="tablerow1"><?=$lc->DefaultAttendanceSymbol[CARD_STATUS_LATE]?></td>
  	</tr>
  	<tr>
  		<td class="tablerow2"><?=$i_StudentAttendance_Status_EarlyLeave?>&nbsp;</td>
  		<td class="tablerow2"><input type=text name=StatusEarlyLeave id="StatusEarlyLeave" size=5 maxlength=3 value="<?=($ResultArr[3]['StatusSymbol']==NULL)?"":htmlspecialchars($ResultArr[3]['StatusSymbol'],ENT_QUOTES);?>"></td>
  		<td class="tablerow2"><?=$i_StudentAttendance_Symbol_EarlyLeave?></td>
  	</tr>
  	<tr>
  		<td class="tablerow1"><?=$i_StudentAttendance_Status_Waived?>&nbsp;</td>
  		<td class="tablerow1"><input type=text name=StatusWaived id="StatusWaived" size=5 maxlength=3 value="<?=($ResultArr[4]['StatusSymbol']==NULL)?"":htmlspecialchars($ResultArr[4]['StatusSymbol'],ENT_QUOTES);?>"></td>
  		<td class="tablerow1"><?=$i_StudentAttendance_Symbol_Waived?></td>
  	</tr>
</table>
<input id="HiddenPresent" type="hidden" value="<?=strip_tags($i_StudentAttendance_Symbol_Present)?>" />
<input id="HiddenAbsent" type="hidden" value="<?=strip_tags($i_StudentAttendance_Symbol_Absent)?>" />
<input id="HiddenLate" type="hidden" value="<?=strip_tags($i_StudentAttendance_Symbol_Late)?>" />
<input id="HiddenEarlyLeave" type="hidden" value="<?=strip_tags($i_StudentAttendance_Symbol_EarlyLeave)?>" />
<input id="HiddenWaived" type="hidden" value="<?=strip_tags($i_StudentAttendance_Symbol_Waived)?>" />
<br />
<br><br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td align="left"><?=$Lang['SmartCard']['StudentAttendance']['CustomizeReasonSymbols']?></td></tr>
</table>
<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
<td align="left"><?=$i_Profile_Absent?></td>
<td align="right"><span class="table_row_tool" style="float:right;"><a class="add" href='javascript:addRow("AbsentReasonTable","Absent")' alt="<?=$Lang['SmartCard']['StudentAttendance']['AddRow']?>" title="<?=$Lang['SmartCard']['StudentAttendance']['AddRow']?>"></a></span></td>
</tr>
</table>
<table id="AbsentReasonTable" name="AbsentReasonTable" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
  <tbody>	
	<tr class="tabletop">
		<td class="tabletoplink" style="vertical-align:middle"><?=$i_Attendance_Reason?></td>
		<td class="tabletoplink"><?=$Lang['SmartCard']['StudentAttendence']['CustomSymbol']?></td>
		<td class="tabletoplink">&nbsp;</td>
	</tr>
	<? echo $AbsentRows;?>
  </tbody>
</table>
<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
<td align="left"><?=$i_Profile_Late?></td>
<td align="right"><span class="table_row_tool" style="float:right;"><a class="add" href='javascript:addRow("LateReasonTable","Late")' alt="<?=$Lang['SmartCard']['StudentAttendance']['AddRow']?>" title="<?=$Lang['SmartCard']['StudentAttendance']['AddRow']?>"></a></span></td>
</tr>
</table>
<table id="LateReasonTable" name="LateReasonTable" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
  <tbody>	
	<tr class="tabletop">
		<td class="tabletoplink" style="vertical-align:middle"><?=$i_Attendance_Reason?></td>
		<td class="tabletoplink"><?=$Lang['SmartCard']['StudentAttendence']['CustomSymbol']?></td>
		<td class="tabletoplink">&nbsp;</td>
	</tr>
	<? echo $LateRows;?>
  </tbody>
</table>
<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
<td align="left"><?=$i_Profile_EarlyLeave?></td>
<td align="right"><span class="table_row_tool" style="float:right;"><a class="add" href='javascript:addRow("EarlyLeaveReasonTable","EarlyLeave")' alt="<?=$Lang['SmartCard']['StudentAttendance']['AddRow']?>" title="<?=$Lang['SmartCard']['StudentAttendance']['AddRow']?>"></a></span></td>
</tr>
</table>
<table id="EarlyLeaveReasonTable" name="EarlyLeaveReasonTable" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
  <tbody>	
	<tr class="tabletop">
		<td class="tabletoplink" style="vertical-align:middle"><?=$i_Attendance_Reason?></td>
		<td class="tabletoplink"><?=$Lang['SmartCard']['StudentAttendence']['CustomSymbol']?></td>
		<td class="tabletoplink">&nbsp;</td>
	</tr>
	<? echo $EarlyLeaveRows;?>
  </tbody>
</table>

<div id="AbsentMenu" style="position:absolute;width=0px;height=0px;visibility:hidden;">
<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>
<tr>
	<td height='19'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td width='5' height='19'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_01.gif' width='5' height='19'></td>
				<td height='19' valign='middle' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_02.gif'></td>
				<td width='19' height='19'><a href='javascript:void(0)' onClick="HideSelection();"><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_off.gif' name='abs_pre_close' width='19' height='19' border='0' id='abs_pre_close' onMouseOver="MM_swapImage('abs_pre_close','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_on.gif',1)" onMouseOut='MM_swapImgRestore()' /></a></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td width='5' height='50' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif' width='5' height='19'></td>
				<td bgcolor='#FFFFF7'>
					<div style="overflow: auto; height: 100px; width: 160px;">
						<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>
							<?
							$abswords="";
							for ($j=0; $j<sizeof($AbsentWords); $j++)
							{
								$temp = $AbsentWords[$j];
							    $abswords.="<tr><td style=\"border-bottom:1px #EEEEEE solid;background-color:#FFFFFF;\"><a class=\"presetlist\" href=\"javascript:void(0);\" onclick=\"FillWord('AbsWord$j');\" title=\"$temp\">".$temp."</a><input type='hidden' id='AbsWord$j' value='$temp'></td></tr>";
							}
							echo $abswords;
							?>
						</table>
					</div>
				</td>
				<td width='6' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif' width='6' height='6' border='0' /></td>
			</tr>
			<tr>
				<td width='5' height='6'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_07.gif' width='5' height='6'></td>
				<td height='6' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif' width='100%' height='6' border='0' /></td>
				<td width='6' height='6'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_09.gif' width='6' height='6'></td>
			</tr>
		</table>
	</td>
</tr>
</table>
</div>
<div id="LateMenu" style="position:absolute;width=0px;height=0px;visibility:hidden;">
<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>
<tr>
	<td height='19'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td width='5' height='19'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_01.gif' width='5' height='19'></td>
				<td height='19' valign='middle' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_02.gif'></td>
				<td width='19' height='19'><a href='javascript:void(0)' onClick="HideSelection();"><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_off.gif' name='lat_pre_close' width='19' height='19' border='0' id='lat_pre_close' onMouseOver="MM_swapImage('lat_pre_close','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_on.gif',1)" onMouseOut='MM_swapImgRestore()' /></a></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td width='5' height='50' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif' width='5' height='19'></td>
				<td bgcolor='#FFFFF7'>
					<div style="overflow: auto; height: 100px; width: 160px;">
						<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>
							<?
							$latewords="";
							for ($j=0; $j<sizeof($LateWords); $j++)
							{
								$temp = $LateWords[$j];
							    $latewords.="<tr><td style=\"border-bottom:1px #EEEEEE solid;background-color:#FFFFFF;\"><a class=\"presetlist\" href=\"javascript:void(0);\" onclick=\"FillWord('LatWord$j');\" title=\"$temp\">".$temp."</a><input type='hidden' id='LatWord$j' value='$temp'></td></tr>";
							}
							echo $latewords;
							?>
						</table>
					</div>
				</td>
				<td width='6' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif' width='6' height='6' border='0' /></td>
			</tr>
			<tr>
				<td width='5' height='6'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_07.gif' width='5' height='6'></td>
				<td height='6' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif' width='100%' height='6' border='0' /></td>
				<td width='6' height='6'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_09.gif' width='6' height='6'></td>
			</tr>
		</table>
	</td>
</tr>
</table>
</div>
<div id="EarlyLeaveMenu" style="position:absolute;width=0px;height=0px;visibility:hidden;">
<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>
<tr>
	<td height='19'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td width='5' height='19'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_01.gif' width='5' height='19'></td>
				<td height='19' valign='middle' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_02.gif'></td>
				<td width='19' height='19'><a href='javascript:void(0)' onClick="HideSelection();"><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_off.gif' name='eal_pre_close' width='19' height='19' border='0' id='eal_pre_close' onMouseOver="MM_swapImage('eal_pre_close','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_on.gif',1)" onMouseOut='MM_swapImgRestore()' /></a></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td width='5' height='50' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif' width='5' height='19'></td>
				<td bgcolor='#FFFFF7'>
					<div style="overflow: auto; height: 100px; width: 160px;">
						<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>
							<?
							$ealwords="";
							for ($j=0; $j<sizeof($EarlyLeaveWords); $j++)
							{
								$temp = $EarlyLeaveWords[$j];
							    $ealwords.="<tr><td style=\"border-bottom:1px #EEEEEE solid;background-color:#FFFFFF;\"><a class=\"presetlist\" href=\"javascript:void(0);\" onclick=\"FillWord('EalWord$j');\" title=\"$temp\">".$temp."</a><input type='hidden' id='EalWord$j' value='$temp'></td></tr>";
							}
							echo $ealwords;
							?>
						</table>
					</div>
				</td>
				<td width='6' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif' width='6' height='6' border='0' /></td>
			</tr>
			<tr>
				<td width='5' height='6'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_07.gif' width='5' height='6'></td>
				<td height='6' background='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif' width='100%' height='6' border='0' /></td>
				<td width='6' height='6'><img src='<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_09.gif' width='6' height='6'></td>
			</tr>
		</table>
	</td>
</tr>
</table>
</div>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "button", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onclick=\"return SubmitForm();\" ") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>