<?php 
# using: 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$sql  = "SELECT
					CONCAT('<a class=\'tablelink\' href=\'edit.php?TemplateID=', TemplateID, '\'>', Title, '</a>') as Title,
					IF(CategoryID='0',CONCAT('--'),
					CASE (CategoryID) ";
foreach ($StudentAttendUI->eNoticeTemplateCategorySetting as $Key => $Value) {
	$sql .= "WHEN '".$Key."' THEN '".$Value."' ";
}
$sql .= "
					ELSE '' END) as CategoryID,
          IF(RecordStatus=1,CONCAT('$i_Discipline_System_Discipline_Template_Published'),CONCAT('$i_Discipline_System_Discipline_Template_Draft')) as Status,
          CONCAT('<input type=\'checkbox\' name=\'TemplateID[]\' value=\'', TemplateID,'\'>')
        FROM INTRANET_NOTICE_MODULE_TEMPLATE
        WHERE 
        	Module = 'StudentAttendance' ";
if ($_REQUEST['RecordStatus'] != "") {
	$sql .= "	AND RecordStatus = '".$_REQUEST['RecordStatus']."' ";
}
if ($_REQUEST['CategoryID'] != "" && $_REQUEST['CategoryID'] != '0') {
	$sql .= " AND CategoryID = '".$_REQUEST['CategoryID']."' ";
}


$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Title", "CategoryID", "Status");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='40%' >".$li->column($pos++, $Lang['StudentAttendance']['eNoticeTemplateName'])."</td>\n";
$li->column_list .= "<td width='30%'>".$li->column($pos++, $Lang['StudentAttendance']['eNoticeTemplateCategory'])."</td>\n";
$li->column_list .= "<td width='30%'>".$li->column($pos++, $Lang['StudentAttendance']['eNoticeTemplateStatus'])."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("TemplateID[]")."</td>\n";

if(is_array($StudentAttendUI->eNoticeTemplateCategorySetting))
{
	$catArr[0] = array('0',$Lang['General']['All']);
	
	foreach($StudentAttendUI->eNoticeTemplateCategorySetting as $Key=> $Value)
	{
		$catArr[] = array($Key,$Value);
	} 
}

$statusArr[0] = array("",$Lang['General']['All']);
$statusArr[1] = array(1,$Lang['StudentAttendance']['eNoticeTemplateActive']);
$statusArr[2] = array(2,$Lang['StudentAttendance']['eNoticeTemplateInActive']);

$statusSelection = $linterface->GET_SELECTION_BOX($statusArr, 'id="RecordStatus", name="RecordStatus" onChange="document.form1.submit()"', "", $RecordStatus);
$catSelection = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID", name="CategoryID" onChange="document.form1.submit()"', "", $CategoryID);
$toolbar = $linterface->GET_LNK_NEW("new.php",$button_new,"","","",0);

# Top menu highlight setting
$CurrentPage['eNoticTemplateSetting'] = 1;

$TAGS_OBJ[] = array($Lang['StudentAttendance']['eNoticeTemplate']);

$CurrentPageArr['StudentAttendance'] = 1;

# Left menu 
$MODULE_OBJ = $StudentAttendUI->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START(urldecode($Msg));

?>
<script language="javascript">
<!--
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
</script>
<br />
<form name="form1" method="post" action="">
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
										</tr>
									</table>
								</td>
								<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
							</tr>
							<tr class="table-action-bar"><td><?=$catSelection?><?=$statusSelection?></td>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'TemplateID[]','edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'TemplateID[]','remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>