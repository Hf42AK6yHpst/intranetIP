<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
intranet_opendb();

$li = new libcardstudentattend2();
$li->Start_Trans();
$nonSchoolDay=($non_school_day==""||$non_school_day==0)?0:1;

$ApplyAlsoList = $_REQUEST['ApplyAlsoList'];
$ApplyAlsoList[] = $ClassID;

for ($j=0; $j< sizeof($ApplyAlsoList); $j++) {
	if($DayType==1 || $DayType==2||$DayType==0){  # WeekDay or # Cycle Day
		$sql = "INSERT IGNORE INTO CARD_STUDENT_GROUP_PERIOD_TIME
						(GroupId,DayType, DayValue, MorningTime, LunchStart, LunchEnd, LeaveSchoolTime, NonSchoolDay,DateInput, DateModified)
						VALUES 
						('".$ApplyAlsoList[$j]."','$DayType','$DayValue','$normal_am','$normal_lunch','$normal_pm','$normal_leave','$nonSchoolDay',now(),now()) 
						on duplicate key update 
							MorningTime = '$normal_am', LunchStart = '$normal_lunch',
		          LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',
		          NonSchoolDay='$nonSchoolDay',DateModified= now()";
		$Result['InsertTimeSessionRegular-GroupID:'.$ApplyAlsoList[$j]] = $li->db_db_query($sql);
	}
	else { # Special Day
		$TargetDate = $_REQUEST['target_date'];
		for ($i=0; $i< sizeof($TargetDate); $i++) {
			$sql = "INSERT IGNORE INTO CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP (
								GroupID,
								RecordDate,
								MorningTime,
								LunchStart,
								LunchEnd,
								LeaveSchoolTime,
								NonSchoolDay,
								DateInput,
								DateModified) 
							VALUES (
								'".$ApplyAlsoList[$j]."',
								'".$TargetDate[$i]."',
								'".$normal_am."',
								'".$normal_lunch."',
								'".$normal_pm."',
								'".$normal_leave."',
								'".$nonSchoolDay."',
								now(),
								now()) 
							on duplicate key update 
								MorningTime = '".$normal_am."', 
								LunchStart = '".$normal_lunch."',
				        LunchEnd = '".$normal_pm."', 
				        LeaveSchoolTime = '".$normal_leave."',
				        NonSchoolDay='".$nonSchoolDay."',
				        DateModified= now()";
			$Result['InsertGroupSpecificDateTime-GroupID:'.$ApplyAlsoList[$j].'-TargetDate:'.$TargetDate[$i]] = $li->db_db_query($sql);
		}
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$li->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$li->Commit_Trans();
}
//echo "sql [".$sql."]<br>";
intranet_closedb();
header("Location: group_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
?>