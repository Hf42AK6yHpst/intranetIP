<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";
$linterface = new interface_html();

$lc = new libcardstudentattend2();
$SynchronizeHolidays=$Lang['StudentAttendance']['Button']['SynchronizeHolidays'];

//$objSpecialGroup = new libcardstudentattendgroup();
//$class_mode = $objSpecialGroup->getSpecialGroupAttendanceMode($ClassID);

$class_mode = $lc->getSpecialGroupAttendanceMode($ClassID);
								
if ($class_mode != 1)
{
    header("Location: index.php");
    intranet_closedb();
    exit();
}

$lc->retrieveSettings();
#$normal_time_array = $lc->getTimeArray(0,0);
#list($normal_am, $normal_lunch, $normal_pm, $normal_leave) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

//$classname = $objSpecialGroup->getSpecialGroupName($ClassID);
$classname = $lc->getSpecialGroupName($ClassID);

//$class_time_array = $objSpecialGroup->getGroupTimeArray($ClassID,0,0);
$class_time_array = $lc->getGroupTimeArray($ClassID,0,0);
if (sizeof($class_time_array)<4)
{
    $hasNormal = false;
}
else
{
    $hasNormal = true;
    list($class_am,$class_lunch, $class_pm, $class_leave) = $class_time_array;
}
#$class_am = ($class_am==""? $normal_am : $class_am);
#$class_lunch = ($class_lunch==""? $normal_lunch : $class_lunch);
#$class_pm = ($class_pm==""? $normal_pm : $class_pm);
#$class_leave = ($class_leave==""? $normal_leave : $class_leave);


//$week_time_array = $objSpecialGroup->getGroupTimeArrayList($ClassID,1);
//$cycle_time_array = $objSpecialGroup->getGroupTimeArrayList($ClassID,2);

$week_time_array = $lc->getGroupTimeArrayList($ClassID,1);
$cycle_time_array = $lc->getGroupTimeArrayList($ClassID,2);


$today = date('Y-m-d',time());
$sql="SELECT RecordDate,TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
   FROM CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP WHERE RecordDate>='$today' AND GroupID='$ClassID' ORDER BY RecordDate";

$special_time_array= $lc->returnArray($sql,6);

// start of weekday table
$week_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$week_table .= "<tr class=\"tabletop\">";
$week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_WeekDay</td>";
$no_of_col = 2;
if($hasAM) {
	$week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
	$no_of_col++;
}
if($hasLunch) {
	$week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
	$no_of_col++;
}
if($hasPM) {
	$week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
	$no_of_col++;
}
$week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
$week_table .= "</tr>\n";
if (sizeof($week_time_array) == 0) {
    $week_table .= "<tr class=\"tablerow2 tabletext\">";
    $week_table .= "<td class=\"tabletext\" colspan=\"$no_of_col\" align=\"center\">$i_no_record_exists_msg</td>";
    $week_table .= "</tr>";
} else {
	for ($i=0; $i<sizeof($week_time_array); $i++)
	{
		list($weekday, $week_am, $week_lunch, $week_pm, $week_leave,$week_non_school_day) = $week_time_array[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$day = $i_DayType0[$weekday];
		$editlink = "<a class=functionlink href=group_time_edit.php?ClassID=$ClassID&type=1&value=$weekday><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(1,'$weekday')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		$week_table .= "<tr class=\"$css\">";
		$week_table .= "<td class=\"tabletext\">$day $editlink</td>";
		if($week_non_school_day==1){
			$week_table.="<td class=\"tabletext\" colspan=4>$i_StudentAttendance_NonSchoolDay</td>";
		}else{
		 if($hasAM)
		   $week_table .= "<td class=\"tabletext\">$week_am</td>";
		if($hasLunch)
		   $week_table .= "<td class=\"tabletext\">$week_lunch</td>";
		if($hasPM)
		   $week_table .= "<td class=\"tabletext\">$week_pm</td>";
		   $week_table .= "<td class=\"tabletext\">$week_leave</td>";
		}
		$week_table .= "</tr>\n";
	}
}
$week_table .= "</table>\n";
// end of weekday table

// start of cycle table
$cycle_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$cycle_table .= "<tr class=\"tabletop\">";
$cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_CycleDay</td>";
if($hasAM)
	$cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
if($hasLunch)
	$cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
if($hasPM)
	$cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
$cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
$cycle_table .= "</tr>\n";
if (sizeof($cycle_time_array) == 0) {
    $cycle_table .= "<tr class=\"tablerow2 tabletext\">";
    $cycle_table .= "<td class=\"tabletext\" colspan=\"$no_of_col\" align=\"center\">$i_no_record_exists_msg</td>";
    $cycle_table .= "</tr>";
} else {
	for ($i=0; $i<sizeof($cycle_time_array); $i++)
	{
		list($cycleday, $cycle_am, $cycle_lunch, $cycle_pm, $cycle_leave,$cycle_non_school_day) = $cycle_time_array[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$editlink = "<a class=functionlink href=group_time_edit.php?ClassID=$ClassID&type=2&value=$cycleday><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(2,'$cycleday')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		$cycle_table .= "<tr class=\"$css\">";
		$cycle_table .= "<td class=\"tabletext\">$cycleday $editlink</td>";
		if($cycle_non_school_day==1){
			$cycle_table.="<td class=\"tabletext\" colspan=4>$i_StudentAttendance_NonSchoolDay</td>";
		}else{
		 if($hasAM)
		   $cycle_table .= "<td class=\"tabletext\">$cycle_am</td>";
		if($hasLunch)
		   $cycle_table .= "<td class=\"tabletext\">$cycle_lunch</td>";
		if($hasPM)
		   $cycle_table .= "<td class=\"tabletext\">$cycle_pm</td>";
		   $cycle_table .= "<td class=\"tabletext\">$cycle_leave</td>";
		}
		$cycle_table .= "</tr>\n";
	}
}
$cycle_table .= "</table>\n";
// end of cycle table

// start of special table
$special_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$special_table .= "<tr class=\"tabletop\">";
$special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SpecialDay</td>";
if($hasAM)
	$special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
if($hasLunch)
	$special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
if($hasPM)
	$special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
$special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
$special_table .= "</tr>\n";
if (sizeof($special_time_array) == 0) {
    $special_table .= "<tr class=\"tablerow2 tabletext\">";
    $special_table .= "<td class=\"tabletext\" colspan=\"$no_of_col\" align=\"center\">$i_no_record_exists_msg</td>";
    $special_table .= "</tr>";
} else {
	for ($i=0; $i<sizeof($special_time_array); $i++)
	{
		list($specialday, $special_am, $special_lunch, $special_pm, $special_leave,$special_non_school_day) = $special_time_array[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";		
		$editlink = "<a class=functionlink href=\"group_time_edit.php?ClassID=$ClassID&value=$specialday&type=3\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(3,'$specialday')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		$special_table .= "<tr class=\"$css\">";
		$special_table .= "<td class=\"tabletext\">$specialday $editlink</td>";
		if($special_non_school_day==1){
			$special_table .="<td class=\"tabletext\" colspan=4>$i_StudentAttendance_NonSchoolDay</td>";
		}else{
		 if($hasAM)
		 	$special_table .= "<td class=\"tabletext\">$special_am</td>";
		 if($hasLunch)
		 	$special_table .= "<td class=\"tabletext\">$special_lunch</td>";
		 if($hasPM)
		 	$special_table .= "<td class=\"tabletext\">$special_pm</td>";
		 $special_table .= "<td class=\"tabletext\">$special_leave</td>";
		}
		$special_table .= "</tr>\n";
	}
}
$special_table .= "</table>\n";
// end of special table

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 1);
$PAGE_NAVIGATION[] = array($classname);

$PAGE_NAVIGATION1 = $i_StudentAttendance_NormalDays;
$PAGE_NAVIGATION2 = $i_StudentAttendance_Weekday_Specific;
$PAGE_NAVIGATION3 = $i_StudentAttendance_Cycleday_Specific;
$PAGE_NAVIGATION4 = $i_StudentAttendance_SpecialDay;

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<SCRIPT LANGUAGE=Javascript>
	<!--
function removeDaySetting(DayType, DayValue)
{
         if (confirm('<?=$i_Usage_RemoveConfirm?>'))
         {
             location.href = "group_time_remove.php?ClassID=<?=$ClassID?>&type="+DayType+"&value="+DayValue;
         }
}

function checkForm(formObj){

	amObj = formObj.normal_am;
	lunchObj=formObj.normal_lunch;
	pmObj = formObj.normal_pm;
	leaveObj = formObj.normal_leave;

	if(amObj!=null){
		am = amObj.value;
		if(!isValidTimeFormat(am)){
			amObj.focus();
			return false;
		}
	}
	if(lunchObj!=null){
		lunch = lunchObj.value;
		if(!isValidTimeFormat(lunch)){
			lunchObj.focus();
			return false;
		}
	}
	if(pmObj!=null){
		pm = pmObj.value;
		if(!isValidTimeFormat(pm)){
			pmObj.focus();
			return false;
		}
	}
	if(leaveObj!=null){
		leave = leaveObj.value;
		if(!isValidTimeFormat(leave)){
			leaveObj.focus();
			return false;
		}
	}
	if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
		return true;
	return false;
}
function isValidTimeFormat(timeVal){
  	var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
		if (!timeVal.match(re)) {
				alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
				return false;
		}
		return true;
}
function isValidValues(amObj,lunchObj,pmObj,leaveObj){
			if(amObj!=null){
					if(lunchObj!=null && amObj.value>=lunchObj.value){ 
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
						amObj.focus();
						return false;
					}
					else if(pmObj!=null && amObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
						amObj.focus();
						return false;
					}
					else if(leaveObj!=null && amObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
						amObj.focus();
						return false;
					}
			}
			if(lunchObj!=null){
					if(pmObj!=null && lunchObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
						lunchObj.focus();
						return false;
					}
					else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
						lunchObj.focus();
						return false;
					}
			}
			if(pmObj!=null){
					if(leaveObj!=null && pmObj.value>=leaveObj.value){
							alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
							pmObj.focus();
							return false;
					}
			}
			return true;
}
// -->
</SCRIPT>
<form name="form1" method="POST" ACTION="group_edit_update.php" onSubmit="return checkForm(this)">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION1) ?></td>
	</tr>
<? if (!$hasNormal) { ?>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_EDIT("group_add.php?ClassID=$ClassID&type=0","","","","",0) ?></td>
	</tr>
</table>
<? } else { ?>
	<tr>
		<td align="left">
		<?
			$rx = "<a class=\"contenttool\" href=\"javascript:removeDaySetting(0,0)\">";
			$rx .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_delete.gif\" border=\"0\" align=\"absmiddle\"> ";
			$rx .= $button_clear;
			$rx .= "</a>";
			echo $rx;
		?>
		</td>
	</tr>
</table>
<? } ?>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
	<?
	### Mode 1,3,4
	if ($hasAM) {
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_AMStart?></td>
	<? } ?>
	<?
	### Mode 3,4
	if ($hasLunch) {
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_LunchStart?></td>
	<? } ?>
	<?
	### Mode 2,3,4
	if ($hasPM) {
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_PMStart?></td>
	<? } ?>
	<?
	### Mode 1,2,3,4
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
	</tr>
<? if (!$hasNormal) { ?>
	<tr>
		<td class="tablerow2 tabletext" align="center" colspan="4"><?=$i_StudentAttendance_NoSpecialSettings?></td>
	</tr>
<? } else { ?>
	<tr>
	<?
	### Mode 1,3,4
	if ($hasAM) {
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_am" value="<?=$class_am?>">
		</td>
	<? } ?>
	<?
	### Mode 3,4
	if ($hasLunch) {
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_lunch" value="<?=$class_lunch?>">
		</td>
	<? } ?>
	<?
	### Mode 2,3,4
	if ($hasPM) {
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_pm" value="<?=$class_pm?>">
		</td>
	<? } ?>
	<?
	### Mode 1,2,3,4
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_leave" value="<?=$class_leave?>">
		</td>
	</tr>
<? } ?>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<? if ($hasNormal) { ?>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
	<tr>
	    <td class="tabletextremark"><?=$i_StudentAttendance_Slot_SettingsDescription?></td>
	</tr>
</table>
<? } ?>
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
</form>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></td>
	</tr>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_NEW("group_add.php?ClassID=$ClassID&type=1","","","","",0) ?></td>
	</tr>
</table>
<?=$week_table?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION3) ?></td>
	</tr>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_NEW("group_add.php?ClassID=$ClassID&type=2","","","","",0) ?></td>
	</tr>
</table>
<?=$cycle_table?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION4) ?></td>
	</tr>
	<tr>
		<td align="left">
			<?= $linterface->GET_LNK_NEW("group_add.php?ClassID=$ClassID&type=3","","","","",0) ?>
			<?
				$rx = "<a class=\"contenttool\" href=\"details.php?ClassID=$ClassID\">";
				$rx .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
				$rx .= $i_StudentAttendance_ViewPastRecords;
				$rx .= "</a>";
				echo $rx;
			?>
			<?=$linterface->GET_LNK_GENERATE("group_synchronize_holidays.php?ClassID=$ClassID","$SynchronizeHolidays","","","",0)?>						
		</td>
	</tr>
</table>
<?=$special_table?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>