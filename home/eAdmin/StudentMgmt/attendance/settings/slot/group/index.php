<?php
## Using By : Max
##########################################################
## Modification Log
## 2010-02-04: Max (201001221711)
## - Move some js functions to a common js file -> settings_common_script.js
## - get the bundleChangeSelection from getSelectionTableGroupMode() in library libcardstudentattend2.php

## 2009-12-04: Max (200912041605)
## - support multiple classes/groups change
##########################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libspecialgroup.php");
//include_once($PATH_WRT_ROOT."includes/libcardstudentattendgroup.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";

$linterface = new interface_html();

//$lc = new libcardstudentattendgroup();
$lc = new libcardstudentattend2();
$_recordTypeID = 3;  #3 ==> class type
$classList = $lc->getGroupListMode($_recordTypeID);

# selection table
if ($_POST["showAll"] == "" && $_GET["showAll"] == "") {
	$showAll = -1;
} else if ($_POST["showAll"] == "") {
	$showAll = $_GET["showAll"];
} else {
	$showAll = $_POST["showAll"];
}
if($showAll == "1")
{
	$_SESSION['groupShowAll'] = "1";   
}
else if($showAll == "0")
{
	unset($_SESSION['groupShowAll']);
}
$selectionTable = $lc->getSelectionTableGroupMode($showAll);

# class attendence list
$class_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$class_table .= "<tr class=\"tabletop\">";
$class_table .= "<td class=\"tabletoplink\">$i_GroupName</td>";
$class_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_GroupMode</td>";
$class_table .= "<td><input type='checkbox' name='ClassIDsMaster' onClick=\"(this.checked)?setChecked(1,this.form,'ClassIDs[]'):setChecked(0,this.form,'ClassIDs[]')\" /></td>";
$class_table .= "</tr>\n";
for ($i=0; $i<sizeof($classList); $i++)
{
     list($ClassID, $ClassName, $mode) = $classList[$i];
     if ($mode == 2)
     {
         $word_mode = $i_StudentAttendance_GroupMode_NoNeedToTakeAttendance;
     }
     else if ($mode == 1)
     {
          $word_mode = $i_StudentAttendance_GroupMode_UseGroupTimetable;
     }
     else
     {
         $word_mode = $i_StudentAttendance_GroupMode_UseSchoolTimetable;
     }
     $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
     $editlink = "<a class=functionlink href=group.php?ClassID=$ClassID><img src=\"$image_path/icon_edit.gif\" border=0></a>";
     $class_table .= "<tr class=$css>\n";
     $class_table .= "<td class=\"tabletext\">$ClassName $editlink</td>
     									<td class=\"tabletext\">$word_mode</td>";
	 $class_table .= "<td><input type='checkbox' name='ClassIDs[]' value='{$ClassID}' /></td>";
     $class_table .= "</tr>\n";
}
$class_table .= "</table>\n";

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 1);

$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(urldecode($Msg));

?>
<script type="text/javascript" src="../../settings_common_script.js"></script>
<form name="form1" action="index.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
  	<td>
		<?=$selectionTable?>
	</td>
	</tr>
	</table>
	<br />
<?=$class_table?>
</form>
<br />
<br><br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>