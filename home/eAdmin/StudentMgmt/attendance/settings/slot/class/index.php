<?php
## Using By : 
##########################################################
## Modification Log
## 2010-02-04: Max (201001221711)
## - Move some js functions to a common js file -> settings_common_script.js
## - get the bundleChangeSelection from getBundleChangeTableBoxClassMode() in library libcardstudentattend2.php

## 2009-12-04: Max (200912041605)
## - support multiple classes/groups change
##########################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";
$linterface = new interface_html();

$lc = new libcardstudentattend2();
$classList = $lc->getClassListMode();

# selection table
if ($_POST["showAll"] == "" && $_GET["showAll"] == "") {
	$showAll = -1;
} else if ($_POST["showAll"] == "") {
	$showAll = $_GET["showAll"];
} else {
	$showAll = $_POST["showAll"];
}
$selectionTable = $lc->getSelectionTableClassMode($showAll);

# class attendence list
$class_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$class_table .= "<tr class=\"tabletop\">";
$class_table .= "<td class=\"tabletoplink\">$i_ClassName</td>";
$class_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_ClassMode</td>";
$class_table .= "<td><input type='checkbox' name='ClassIDsMaster' onClick=\"(this.checked)?setChecked(1,this.form,'ClassIDs[]'):setChecked(0,this.form,'ClassIDs[]')\" /></td>";
$class_table .= "</tr>\n";
for ($i=0; $i<sizeof($classList); $i++)
{
     list($ClassID, $ClassName, $ClassNameB5, $mode) = $classList[$i];
     $DisplayClassName = Get_Lang_Selection($ClassNameB5,$ClassName);
     if ($mode == 2)
     {
         $word_mode = $i_StudentAttendance_ClassMode_NoNeedToTakeAttendance;
         if ($showAll != -1 && $showAll != 2) {
         	continue;
         } else {
         	// do nothing
         }
     }
     else if ($mode == 1)
     {
          $word_mode = $i_StudentAttendance_ClassMode_UseClassTimetable;
         if ($showAll != -1 && $showAll != 1) {
         	continue;
         } else {
         	// do nothing
         }
     }
     else if ($mode == 0)
     {
         $word_mode = $i_StudentAttendance_ClassMode_UseSchoolTimetable;
         if ($showAll != -1 && $showAll != 0) {
         	continue;
         } else {
         	// do nothing
         }
     } else {
     	// do nothing
     }
     $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
     $editlink = "<a href=\"class.php?ClassID=$ClassID\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" alt=\"$button_edit\" border=\"0\"></a>";
     $class_table .= "<tr class=\"$css\">\n";
     $class_table .= "<td class=\"tabletext\">$DisplayClassName $editlink</td>";
     $class_table .= "<td class=\"tabletext\">$word_mode</td>";
	 $class_table .= "<td><input type='checkbox' name='ClassIDs[]' value='{$ClassID}' /></td>";
     $class_table .= "</tr>\n";
}
$class_table .= "</table>\n";

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(urldecode($Msg));

?>
<script type="text/javascript" src="../../settings_common_script.js"></script>
<br />
<form name="form1" action="index.php" method="POST">
<?=$selectionTable?>
<?=$class_table?>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>