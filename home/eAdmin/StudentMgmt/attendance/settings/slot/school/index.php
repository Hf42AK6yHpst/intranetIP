<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";

$SynchronizeHolidays=$Lang['StudentAttendance']['Button']['SynchronizeHolidays'];
$linterface = new interface_html();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$lb= new libdb();
$normal_time_array = $lc->getTimeArray(0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave,$non_school_day) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

$week_time_array = $lc->getTimeArrayList(1);
$cycle_time_array = $lc->getTimeArrayList(2);
$today = date('Y-m-d',time());
$sql="SELECT RecordDate,IF(ClassID IS NULL,0,ClassID),TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
   FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate>='$today' AND (ClassID='0' OR ClassID='')ORDER BY RecordDate";
$special_time_array= $lc->returnArray($sql,7);
if (sizeof($week_time_array)!= 0)
{
    $week_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
    $week_table .= "<tr class=\"tabletop\">";
    $week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_WeekDay</td>";
    if($hasAM)
            $week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
    if($hasLunch)
            $week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
    if($hasPM)
            $week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
    $week_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
    $week_table .= "</tr>";
    for ($i=0; $i<sizeof($week_time_array); $i++)
    {
         list($weekday, $week_am, $week_lunch, $week_pm, $week_leave,$non_school_day) = $week_time_array[$i];
         $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
         $day = $i_DayType0[$weekday];
         $editlink = "<a href=\"edit.php?type=1&value=$weekday\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" alt=\"$button_edit\" border=\"0\"></a>";
		 $editlink .= "<a href=\"javascript:removeDaySetting(1,'$weekday')\"><img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" alt=\"$button_remove\" border=\"0\"></a>";
         $week_table .= "<tr class=\"$css\">";
         $week_table .= "<td class=\"tabletext\">$day $editlink</td>";
         if($non_school_day==1){
                 $week_table .="<td colspan=\"4\" class=\"tabletext\">$i_StudentAttendance_NonSchoolDay</td>";
         }
         else{
                 if($hasAM)
                         $week_table .= "<td class=\"tabletext\">$week_am</td>";
                 if($hasLunch)
                         $week_table .= "<td class=\"tabletext\">$week_lunch</td>";
                 if($hasPM)
                         $week_table .= "<td class=\"tabletext\">$week_pm</td>";
                 $week_table .= "<td class=\"tabletext\">$week_leave</td>";
               }
                        $week_table .= "</tr>\n";
    }
    $week_table .= "</table>\n";
}
if (sizeof($cycle_time_array)!= 0)
{
    $cycle_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
    $cycle_table .= "<tr class=\"tabletop\">";
    $cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_CycleDay</td>";
    if($hasAM)
            $cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
    if($hasLunch)
            $cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
    if($hasPM)
            $cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
    $cycle_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
    $cycle_table .= "</tr>\n";
    for ($i=0; $i<sizeof($cycle_time_array); $i++)
    {
         list($cycleday, $cycle_am, $cycle_lunch, $cycle_pm, $cycle_leave,$non_school_day) = $cycle_time_array[$i];
		 $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
         $editlink = "<a href=\"edit.php?type=2&value=$cycleday\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" alt=\"$button_edit\" border=\"0\"></a>";
		 $editlink .= "<a href=\"javascript:removeDaySetting(2,'$cycleday')\"><img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" alt=\"$button_remove\" border=\"0\"></a>";
         $cycle_table .= "<tr class=\"$css\">";
         $cycle_table .= "<td class=\"tabletext\">$cycleday $editlink</td>";
         if($non_school_day==1){
                         $cycle_table .="<td class=\"tabletext\" colspan=\"4\">$i_StudentAttendance_NonSchoolDay</td>";
         }
         else{
                 if($hasAM)
                         $cycle_table .= "<td class=\"tabletext\">$cycle_am</td>";
                 if($hasLunch)
                         $cycle_table .= "<td class=\"tabletext\">$cycle_lunch</td>";
                 if($hasPM)
                         $cycle_table .= "<td class=\"tabletext\">$cycle_pm</td>";
                  $cycle_table .= "<td class=\"tabletext\">$cycle_leave</td>";
         }
         $cycle_table .= "</tr>\n";

    }
    $cycle_table .= "</table>\n";
}
if (sizeof($special_time_array)!= 0){
   	$special_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
    $special_table .= "<tr class=\"tabletop\">";
    $special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_Special</td>";
           if($hasAM)
            $special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
    if($hasLunch)
            $special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
    if($hasPM)
            $special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
    $special_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td>";
    $special_table .= "<td class=\"tabletoplink\"><input type=\"checkbox\" name=\"deleteAll\" onclick=\"checkAllDelete(this.checked)\"></td>";
    $special_table .= "</tr>\n";
    for ($i=0; $i<sizeof($special_time_array); $i++)
    {
         list($specialday, $classid,$special_am, $special_lunch, $special_pm, $special_leave,$non_school_day) = $special_time_array[$i];
		 $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
         $editlink = "<a href=\"edit.php?type=3&TargetDate=$specialday&ClassID=0\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" alt=\"$button_edit\" border=\"0\"></a>";
		 $editlink .= "<a href=\"javascript:removeDaySetting(3,'$specialday')\"><img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" alt=\"$button_remove\" border=\"0\"></a>";
         $special_table .= "<tr class=\"$css\">";
         $special_table .= "<td class=\"tabletext\">$specialday $editlink</td>";
         if($non_school_day==1){
         	$special_table .="<td class=\"tabletext\" colspan=\"4\">$i_StudentAttendance_NonSchoolDay</td>";
         }
         else{
                 if($hasAM)
                         $special_table .= "<td class=\"tabletext\">$special_am</td>";
                 if($hasLunch)
                         $special_table .= "<td class=\"tabletext\">$special_lunch</td>";
                 if($hasPM)
                         $special_table .= "<td class=\"tabletext\">$special_pm</td>";
                  $special_table .= "<td class=\"tabletext\">$special_leave</td>";
         }
         $special_table .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"delete[]\" value=\"$specialday\"></td>";
         $special_table .= "</tr>\n";

    }
   
    $special_table .= "</table>\n";
}



$PAGE_NAVIGATION1 = $i_StudentAttendance_NormalDays;
$PAGE_NAVIGATION2 = $i_StudentAttendance_Weekday_Specific;
$PAGE_NAVIGATION3 = $i_StudentAttendance_Cycleday_Specific;
$PAGE_NAVIGATION4 = $i_StudentAttendance_SpecialDay;

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);
?>
<SCRIPT LANGUAGE=Javascript>
        <!--
function removeDaySetting(DayType, DayValue)
{

         if (confirm('<?=$i_Usage_RemoveConfirm?>'))
         {
                     location.href = "remove.php?type="+DayType+"&value="+DayValue;
         }
}
function removeSpecialDaySetting(targetDate){
         if (confirm('<?=$i_Usage_RemoveConfirm?>'))
         {
                     location.href = "special_date_remove.php?TargetDate="+targetDate;
         }

}
function checkAllDelete(checked){
	if(checked){
		$('input[name="delete[]"]').attr('checked','checked');
	} else {
		$('input[name="delete[]"]').removeAttr('checked');
	}
}
function removeChecked(DayType){
	var count = $('input[name="delete[]"]:checked').length;
	if(count < 1){
   	 	alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
   	} else if(confirm('<?=$i_Usage_RemoveConfirm?>')){
		var targetVal = $('input[name="delete[]"]:checked').map(function(){
      		return $(this).val();
   	 	}).get();
   		location.href = "remove.php?type="+DayType+"&value="+targetVal;
	}
}
function checkForm(formObj){

        amObj = formObj.normal_am;
        lunchObj=formObj.normal_lunch;
        pmObj = formObj.normal_pm;
        leaveObj = formObj.normal_leave;

        if(amObj!=null){
                am = amObj.value;
                if(!isValidTimeFormat(am)){
                        amObj.focus();
                        return false;
                }
        }
        if(lunchObj!=null){
                lunch = lunchObj.value;
                if(!isValidTimeFormat(lunch)){
                        lunchObj.focus();
                        return false;
                }
        }
        if(pmObj!=null){
                pm = pmObj.value;
                if(!isValidTimeFormat(pm)){
                        pmObj.focus();
                        return false;
                }
        }
        if(leaveObj!=null){
                leave = leaveObj.value;
                if(!isValidTimeFormat(leave)){
                        leaveObj.focus();
                        return false;
                }
        }
        if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
                return true;
        return false;
}
function isValidTimeFormat(timeVal){
                // check if the timeVal is in the form of hh:mm
          var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
                if (!timeVal.match(re)) {
                                alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
                                return false;
                }
                return true;
}
function isValidValues(amObj,lunchObj,pmObj,leaveObj){
//                        amObj = formObj.normal_am;
//                        lunchObj=formObj.normal_lunch;
//                        pmObj = formObj.normal_pm;
//                        leaveObj = formObj.normal_leave;
                        if(amObj!=null){
                                        if(lunchObj!=null && amObj.value>=lunchObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
                                                amObj.focus();
                                                return false;
                                        }
                                        else if(pmObj!=null && amObj.value>=pmObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
                                                amObj.focus();
                                                return false;
                                        }
                                        else if(leaveObj!=null && amObj.value>=leaveObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
                                                amObj.focus();
                                                return false;
                                        }
                        }
                        if(lunchObj!=null){
                                        if(pmObj!=null && lunchObj.value>=pmObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
                                                lunchObj.focus();
                                                return false;
                                        }
                                        else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
                                                lunchObj.focus();
                                                return false;
                                        }
                        }
                        if(pmObj!=null){
                                        if(leaveObj!=null && pmObj.value>=leaveObj.value){
                                                        alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
                                                        pmObj.focus();
                                                        return false;
                                        }
                        }
                        return true;
}
// -->
function synchronizeHolidays(){
alert("a");
}
</SCRIPT>
<form name="form1" method="POST" ACTION="update.php" onSubmit="return checkForm(this)">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION1) ?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
	<?
	### Mode 1,3,4
	if ($hasAM) {
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_AMStart?></td>
	<? } ?>
	<?
	### Mode 3,4
	if ($hasLunch) {
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_LunchStart?></td>
	<? } ?>
	<?
	### Mode 2,3,4
	if ($hasPM) {
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_PMStart?></td>
	<? } ?>
	<?
	### Mode 1,2,3,4
	?>
		<td class="tabletoplink"><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
	</tr>
	<tr>
	<?
	### Mode 1,3,4
	if ($hasAM) {
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_am" value="<?=$normal_am?>">
		</td>
	<? } ?>
	<?
	### Mode 3,4
	if ($hasLunch) {
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_lunch" value="<?=$normal_lunch?>">
		</td>
	<? } ?>
	<?
	### Mode 2,3,4
	if ($hasPM) {
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_pm" value="<?=$normal_pm?>">
		</td>
	<? } ?>
	<?
	### Mode 1,2,3,4
	?>
		<td class="tablerow2 tabletext">
			<input type="text" class="textboxnum" maxlength="5" name="normal_leave" value="<?=$normal_leave?>">
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
	<tr>
	    <td class="tabletextremark"><?=$i_StudentAttendance_Slot_SettingsDescription?></td>
	</tr>
</table>
</form>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><a name="1"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></a></td>
	</tr>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_NEW("add.php?type=1","","","","",0) ?></td>
	</tr>
</table>
<?=$week_table?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><a name="2"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION3) ?></a></td>
	</tr>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_NEW("add.php?type=2","","","","",0) ?></td>
	</tr>
</table>
<?=$cycle_table?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left"><a name="3"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION4) ?></a></td>
	</tr>
	<tr>
		<td align="left">
			<?= $linterface->GET_LNK_NEW("add.php?type=3","","","","",0) ?>
			<?
				$rx = "<a class=\"contenttool\" href=\"details.php\">";
				$rx .= "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
				$rx .= $i_StudentAttendance_ViewPastRecords;
				$rx .= "</a>";
				echo $rx;
			?>
			<?=$linterface->GET_LNK_GENERATE("synchronize_holidays.php","$SynchronizeHolidays","","","",0)?>
		</td>
	</tr>
</table>
<!--		Delete special day settings		-->
<?php if (sizeof($special_time_array)!= 0){ ?>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="bottom">
			<div class="common_table_tool">
				<a href="javascript:removeChecked(7)" class="tool_delete"><?=$button_delete?></a>
			</div>
		</td>
	</tr>
</table>
<?php }　?>
<?=$special_table?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />

</form>
<?
$linterface->LAYOUT_STOP();
if ($hasAM)
	print $linterface->FOCUS_ON_LOAD("form1.normal_am");
else if ($hasLunch)
	print $linterface->FOCUS_ON_LOAD("form1.normal_lunch");
else if ($hasPM)
	print $linterface->FOCUS_ON_LOAD("form1.normal_pm");
intranet_closedb();
?>