<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libcardstudentattend2();
$li->Start_Trans();
$sql = "INSERT INTO CARD_STUDENT_PERIOD_TIME
					(DayType, DayValue, MorningTime, LunchStart, LunchEnd, LeaveSchoolTime, DateInput, DateModified)
				VALUES 
					(0,0,'$normal_am','$normal_lunch','$normal_pm','$normal_leave',now(),now())";
$Result['InsertToPeriodTime'] = $li->db_db_query($sql);

if ($li->db_affected_rows()!=1)
{
  $sql = "UPDATE CARD_STUDENT_PERIOD_TIME
                 SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
                     LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',
                     DateModified= now()
                 WHERE DayType=0 AND DayValue=0";
  $Result['InsertToPeriodTime'] = $li->db_db_query($sql);
}

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>