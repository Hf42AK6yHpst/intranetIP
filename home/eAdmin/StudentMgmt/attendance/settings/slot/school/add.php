<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";
$linterface = new interface_html();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$normal_time_array = $lc->getTimeArray(0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getDayValueWithoutSpecific($type);

if ($type==2)  # Cycle
{
    $select_title = $i_StudentAttendance_CycleDay;
    $select_day = getSelectByValue($days,"name=DayValue");
}
else if($type==1)   # Week
{
    $select_title = $i_StudentAttendance_WeekDay;
    $select_day = "<SELECT name=DayValue>\n";
    $select_day .= "<OPTION value=''>-- $button_select --</OPTION>";
    for ($i=0; $i<sizeof($days); $i++)
    {
         $word = $i_DayType0[$days[$i]];
         $select_day .= "<OPTION value='".$days[$i]."'>".$word."</OPTION>\n";
    }
    $select_day .= "</SELECT>\n";
}else if($type==3){  # Special
	$select_title = $i_StudentAttendance_TimeSlot_SpecialDay;
	$select_day = '<span id="dates">';
	$select_day .= $linterface->GET_DATE_PICKER("target_date[]", date('Y-m-d'),"","yy-mm-dd","","","","","target_date0");
	$select_day .= "<br></span>\n";
	$select_day .= $linterface->GET_SMALL_BTN(" + ", "button", "addDate(document.getElementById('dateFieldCount').value)")."<br />";
}
else{  # Normal 
			$select_title = $i_StudentAttendance_NormalDays;
}

$TAGS_OBJ[] = array($i_general_BasicSettings, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_new);

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);
?>

<script language="javascript">
	          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].TargetDate.value = dateValue;
          }



function checkForm(formObj){
		if(checkForm1(formObj))
			return checkForm2(formObj);
		return false;
}
function checkForm1(formObj){
	targetDayObj = document.getElementsByName('target_date[]');
	if(targetDayObj!=null && targetDayObj.length > 0){
		ReturnVal = true;
		for (var i=0; i< targetDayObj.length; i++) {
			obj=targetDayObj[i];
			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) {
				ReturnVal = false;
				break;
			}
		}
		
		return ReturnVal;
	}
	else {
		dayValueObj =formObj.DayValue;
		if(dayValueObj!=null){
			i= formObj.DayValue.selectedIndex;
			if(i==0){
				alert("<?if($type==2) echo $i_StudentAttendance_Warn_Please_Select_CycleDay;else if($type==1) echo $i_StudentAttendance_Warn_Please_Select_WeekDay;?>");
				return false;
			}else{
				return true;
			}
		}
	}
	
	return false;	
}
function checkForm2(formObj){
	if(formObj.non_school_day!=null && formObj.non_school_day.checked==true)
			return true;

	amObj = formObj.normal_am;
	lunchObj=formObj.normal_lunch;
	pmObj = formObj.normal_pm;
	leaveObj = formObj.normal_leave;

	if(amObj!=null){
		am = amObj.value;
		if(!isValidTimeFormat(am)){
			amObj.focus();
			return false;
		}
	}
	if(lunchObj!=null){
		lunch = lunchObj.value;
		if(!isValidTimeFormat(lunch)){
			lunchObj.focus();
			return false;
		}
	}
	if(pmObj!=null){
		pm = pmObj.value;
		if(!isValidTimeFormat(pm)){
			pmObj.focus();
			return false;
		}
	}
	if(leaveObj!=null){
		leave = leaveObj.value;
		if(!isValidTimeFormat(leave)){
			leaveObj.focus();
			return false;
		}
	}

	if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
		return true;
	return false;
}



	function isValidTimeFormat(timeVal){
  	var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
		if (!timeVal.match(re)) {
				alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
				return false;
		}
		return true;
}
function isValidValues(amObj,lunchObj,pmObj,leaveObj){
			if(amObj!=null){
					if(lunchObj!=null && amObj.value>=lunchObj.value){ 
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
						amObj.focus();
						return false;
					}
					else if(pmObj!=null && amObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
						amObj.focus();
						return false;
					}
					else if(leaveObj!=null && amObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
						amObj.focus();
						return false;
					}
			}
			if(lunchObj!=null){
					if(pmObj!=null && lunchObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
						lunchObj.focus();
						return false;
					}
					else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
						lunchObj.focus();
						return false;
					}
			}
			if(pmObj!=null){
					if(leaveObj!=null && pmObj.value>=leaveObj.value){
							alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
							pmObj.focus();
							return false;
					}
			}
			return true;
}
function setForm(formObj){
	nonSchoolDayObj = formObj.non_school_day;
	amObj = formObj.normal_am;
	lunchObj=formObj.normal_lunch;
	pmObj = formObj.normal_pm;
	leaveObj = formObj.normal_leave;
	if(nonSchoolDayObj!=null){
		if(nonSchoolDayObj.checked){
			if(amObj!=null) amObj.disabled=true;
			if(lunchObj!=null) lunchObj.disabled=true;
			if(pmObj!=null) pmObj.disabled=true;
			if(leaveObj!=null) leaveObj.disabled=true;
		}else{
			if(amObj!=null) amObj.disabled=false;
			if(lunchObj!=null) lunchObj.disabled=false;
			if(pmObj!=null) pmObj.disabled=false;
			if(leaveObj!=null) leaveObj.disabled=false;
		}
	}
}
function resetForm(formObj){
		formObj.reset();
		setForm(formObj);
}

function addDate(count){
	document.form1.dateFieldCount.value = count+1;
	obj = document.getElementById('dates');
	if(obj==null) return;
	$.post('../../../dailyoperation/preset_absence/ajax_get_new_date_picker.php',{"DateCount":count},function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('span#dates').append(data);
			}
		});
}
</script>

<form name="form1" method="POST" ACTION="add_update.php" onSubmit="return checkForm(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$select_title?></td>
					<td class="tabletext" width="70%"><?=$select_day?></td>
				</tr>
				<?
				### Mode 1,3,4
				if ($hasAM) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_AMStart?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_am" value="<?=$normal_am?>"></td>
				</tr>
				<? } 
				### Mode 3,4
				if ($hasLunch) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_LunchStart?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_lunch" value="<?=$normal_lunch?>"></td>
				</tr>
				<? } 
				### Mode 2,3,4
				if ($hasPM) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_PMStart?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_pm" value="<?=$normal_pm?>"></td>
				</tr>
				<? } 
				### Mode 1,2,3,4
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_leave" value="<?=$normal_leave?>"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_NonSchoolDay?></td>
					<td class="tabletext" width="70%"><input type="checkbox" onClick="setForm(this.form)" name="non_school_day" value="1"></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><br /><?=$i_StudentAttendance_Slot_SettingsDescription?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="DayType" value="<?=$type?>">
<input type="hidden" name="dateFieldCount" id="dateFieldCount" value="1">
</form>

<?
$linterface->LAYOUT_STOP();
if ($type == 3)
	print $linterface->FOCUS_ON_LOAD("form1.normal_am");
else
	print $linterface->FOCUS_ON_LOAD("form1.DayValue");
	
intranet_closedb();
?>