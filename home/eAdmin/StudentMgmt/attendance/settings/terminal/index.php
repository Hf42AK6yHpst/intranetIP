<?
// Editing by 
/*
 * 2016-08-10 (Carlos): $sys_custom['StudentAttendance']['SyncAttendance'] - added crontab job to synchronize attendance data from MyIT School attendance data source.
 * 2015-05-14 (Carlos): $sys_custom['StudentAttendance']['MonitorTerminal'] - added terminal monitoring settings. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TerminalSettings";
$linterface = new interface_html();

$SettingList[] = "'TerminalIP'";
$SettingList[] = "'IgnorePeriod'";
if($sys_custom['StudentAttendance']['MonitorTerminal']){
	$SettingList[] = "'TerminalMonitor'"; // 1 to enable
	$SettingList[] = "'TerminalPingPeriod'"; // in minutes
	$SettingList[] = "'TerminalNotifyUser'"; // UserID csv
	//$SettingList[] = "'TerminalPingTimeSlot'"; // hh:mm,hh:mm|hh:mm,hh:mm|...
}
if($sys_custom['StudentAttendance']['SyncAttendance']){
	$SettingList[] = "'SyncAttendance'";
}
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$ips = $Settings['TerminalIP'];
$ignore_period = $Settings['IgnorePeriod'] + 0;

if($sys_custom['StudentAttendance']['MonitorTerminal']){
	$TerminalMonitor = $Settings['TerminalMonitor'];
	$TerminalPingPeriod = trim($Settings['TerminalPingPeriod']) == ''? 1 : $Settings['TerminalPingPeriod'];
	
	$notifyUserIdAry = explode(",",trim($Settings['TerminalNotifyUser']));
	$sql = "SELECT CONCAT('U',UserID) as UserID,".getNameFieldByLang()." as UserName FROM INTRANET_USER WHERE UserID IN (".$Settings['TerminalNotifyUser'].")";
	$userAry = $GeneralSetting->returnResultSet($sql);
	$notifyUIDAry = array();
	for($i=0;$i<sizeof($userAry);$i++){
		$notifyUIDAry[] = array($userAry[$i]['UserID'],$userAry[$i]['UserName']);
	}
	$notifyUserSelection = $linterface->GET_SELECTION_BOX($notifyUIDAry,"id='TerminalNotifyUser' name='TerminalNotifyUser[]' size='6' multiple","");
}

$TAGS_OBJ[] = array($i_SmartCard_TerminalSettings, "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<?=$linterface->Include_JS_CSS()?>
<br />
<form name="form1" method="post" action="update.php">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right" class="tabletext">
			<?=$SysMsg?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_SmartCard_Terminal_IPList?>
		</td>
		<td class="tabletext" width="70%">
			<span class="tabletextremark"><?=$i_SmartCard_Terminal_IPInput?><br />
			<?=$i_SmartCard_Terminal_YourAddress?>:<?=getRemoteIpAddress()?></span><br />
			<?=$linterface->GET_TEXTAREA("IPList", $ips)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_SmartCard_Terminal_IgnorePeriod?>
		</td>
		<td class="tabletext" width="70%">
			<input type="text" name="ignore_period" class="textboxnum" value="<?=$ignore_period?>"> <?=$i_SmartCard_Terminal_IgnorePeriod_unit?>
		</td>
	</tr>
	<?php
	if($sys_custom['StudentAttendance']['MonitorTerminal']){
		$x = '<tr>';
			$x.= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['MonitorTerminal'].'</td>';
			$x.= '<td class="tabletext" width="70%">
					<input type="checkbox" id="TerminalMonitor" name="TerminalMonitor" value="1" '.($TerminalMonitor==1?'checked="checked"':'').' onchange="toggleTerminalMonitor(this.checked);" />
				  </td>';
		$x .= '</tr>';
		$display_monitor_css = $TerminalMonitor==1?'':' style="display:none;"';
		$x .= '<tr class="terminal-monitor"'.$display_monitor_css.'>';
			$x.= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['NotifyUser'].'</td>';
			$x.= '<td class="tabletext" width="70%">
					<span>'.$notifyUserSelection.'</span>
					<span>'.$linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=TerminalNotifyUser[]&ppl_type=pic&permitted_type=1&excluded_type=4&DisplayGroupCategory=0&DisplayInternalRecipientGroup=0&Disable_AddGroup_Button=1', 9)").'
					'.$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:checkOptionRemove(document.getElementById('TerminalNotifyUser'));").'</span>
				  </td>';
		$x .= '</tr>';
		
		$x .= '<tr class="terminal-monitor"'.$display_monitor_css.'>';
			$x .= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['MonitorPeriod'].'</td>';
			$x.= '<td class="tabletext" width="70%">
					<input type="text" class="textboxnum" id="TerminalPingPeriod" name="TerminalPingPeriod" value="'.$TerminalPingPeriod.'" onchange="terminalPeriodValueChanged(this);" /> '.$i_SmartCard_Terminal_IgnorePeriod_unit .'
				  </td>';	
		$x .= '</tr>';
		$x .= '<tr class="terminal-monitor"'.$display_monitor_css.'>';
			$x .= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['MonitorTimeSlot'].'</td>';
			$x.= '<td class="tabletext" width="70%">
					<div id="MonitorTimeSlotDiv"></div>
				  </td>';
		$x .= '</tr>';
		echo $x;
	}
	?>
	<?php
	if($sys_custom['StudentAttendance']['SyncAttendance']){
		$x = '<tr>';
			$x.= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['SyncAttendanceData'].'</td>';
			$x.= '<td class="tabletext" width="70%">';
				$x.= $linterface->Get_Radio_Button("SyncAttendanceY", "SyncAttendance", "1", $Settings['SyncAttendance']==1,"",$Lang['General']['Yes']).'&nbsp;';
				$x.= $linterface->Get_Radio_Button("SyncAttendanceN", "SyncAttendance", "0", $Settings['SyncAttendance']!=1,"",$Lang['General']['No']);
			$x .='</td>';
		$x .= '</tr>';
		echo $x;
	}
	?>
	<tr>
		<td colspan="2" class="tabletext">
			<br /><?=$i_SmartCard_Description_Terminal_IP_Settings?>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", $sys_custom['StudentAttendance']['MonitorTerminal']? "Select_All_Options('TerminalNotifyUser',true);" : "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?php if($sys_custom['StudentAttendance']['MonitorTerminal']){ ?>
<script type="text/javascript" language="JavaScript">
var loading_img = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var row_template = '<tr>';
	row_template += '<td></td>';
	row_template += '<td></td>';
	row_template += '<td>';
		row_template += '<input type="hidden" name="StartTime[]" value="" />';
		row_template += '<input type="hidden" name="EndTime[]" value="" />';
		row_template += '<span class="table_row_tool">';
		row_template += '<a class="edit" href="javascript:void(0);" onclick="getTerminalTimeSlotEditForm($(this).closest(\'tr\'));" title="<?=$Lang['Btn']['Edit']?>"></a>';
		row_template += '<a class="delete" href="javascript:void(0);" onclick="removeTerminalTimeSlot($(this).closest(\'tr\'));" title="<?=$Lang['Btn']['Delete']?>"></a>';
		row_template += '</span>';
	row_template += '</td>';
	row_template += '</tr>';
var edit_row = null;

function getTerminalTimeSlotTable(reload)
{
	edit_row = null;
	var postData = {"task":"getTerminalTimeSlotTable"};
	if(reload){
		var startTimeObjAry = document.getElementsByName('StartTime[]');
		var endTimeObjAry = document.getElementsByName('EndTime[]');
		var time_slot = '';
		var delim = '';
		for(var i=0;i<startTimeObjAry.length;i++){
			time_slot += delim + startTimeObjAry[i].value + ',' + endTimeObjAry[i].value;
			delim = '|';
		}
		postData['reload'] = 1;
		postData['TimeSlot'] = time_slot;
	}
	
	$('#MonitorTimeSlotDiv').html(loading_img);
	$.post(
		'ajax_task.php',
		postData,
		function(returnHtml)
		{
			$('#MonitorTimeSlotDiv').html(returnHtml);
		}
	);
}

function getTerminalTimeSlotEditForm(obj)
{
	var word_edit = '<?=$Lang['Btn']['Edit']?>';
	var word_new = '<?=$Lang['Btn']['New']?>';
	edit_row = obj? $(obj) : null;
	var start_time = edit_row? $(edit_row.find('input[name="StartTime[]"]')[0]).val():'';
	var end_time = edit_row? $(edit_row.find('input[name="EndTime[]"]')[0]).val():'';
	js_Show_ThickBox(obj?word_edit:word_new);
	$.get(
		'ajax_task.php',
		{
			"task":"getTerminalTimeSlotEditForm",
			"StartTime":start_time,
			"EndTime":end_time 
		},
		function(returnHtml){
			$('div#TB_ajaxContent').html(returnHtml);
		}
	);
}

function removeTerminalTimeSlot(obj)
{
	if(confirm('<?=$Lang['StudentAttendance']['ConfirmRemoveMonitorTimeSlot']?>'))
	{
		edit_row = null;
		$(obj).remove();
	}
}

function updateTerminalTimeSlot()
{
	Block_Thickbox();
	var is_valid = true;
	var start_hour = $('#StartTimeEdit_hour').val();
	var start_min = $('#StartTimeEdit_min').val();
	//var start_sec = $('#StartTimeEdit_sec').val();
	var end_hour = $('#EndTimeEdit_hour').val();
	var end_min = $('#EndTimeEdit_min').val();
	//var end_sec = $('#EndTimeEdit_sec').val();
	var start_time = (start_hour.length<=1?'0'+start_hour:start_hour) + ':' + (start_min.length<=1?'0'+start_min:start_min) /* + ':' + (start_sec.length<=1?'0'+start_sec:start_sec) */;
	var end_time = (end_hour.length<=1?'0'+end_hour:end_hour) + ':' + (end_min.length<=1?'0'+end_min:end_min) /* + ':' + (end_sec.length<=1?'0'+end_sec:end_sec) */;
	
	if(start_time > end_time){
		is_valid = false;
		$('#StartTimeEditWarnLayer').html('<?=$Lang['General']['InvalidData']?>' + ': ' + start_time + ' > ' + end_time);
		$('#StartTimeEditWarnLayer').show();
	}else{
		$('#StartTimeEditWarnLayer').hide();
	}
	
	if(!is_valid){
		UnBlock_Thickbox();
		return;
	}
	
	// check time period overlapping
	var rows = $('#TerminalTimeSlotTable tbody tr');
	for(var i=0;i<rows.length;i++){
		if(edit_row == null || rows[i] != edit_row.get(0))
		{
			var row_start_time = $(rows[i]).find('input[name="StartTime[]"]')[0].value;
			var row_end_time = $(rows[i]).find('input[name="EndTime[]"]')[0].value;
			console.log(row_start_time, row_end_time);
			if((start_time >= row_start_time && start_time <= row_end_time) || (end_time >= row_start_time && end_time <= row_end_time)
				|| (row_start_time >= start_time && row_start_time <= end_time) || (row_end_time >= start_time && row_end_time <= end_time))
			{
				is_valid = false;
				$('#EndTimeEditWarnLayer').html('<?=$Lang['StudentAttendance']['OverlappingTimeSlot']?>' + ' [' + row_start_time + ' - ' + row_end_time + ']');
				$('#EndTimeEditWarnLayer').show();
				break;
			}
		}
	}
	
	if(!is_valid){
		UnBlock_Thickbox();
		return;
	}else{
		$('#EndTimeEditWarnLayer').hide();
	}
	
	if(!edit_row){
		$('#TerminalTimeSlotTable tbody').append(row_template);
		edit_row = $($('#TerminalTimeSlotTable tbody tr:last'));
	}
	
	if(edit_row)
	{
		$(edit_row.find('td')[0]).html(start_time);
		$(edit_row.find('td')[1]).html(end_time);
		$(edit_row.find('input[name="StartTime[]"]')[0]).val(start_time);
		$(edit_row.find('input[name="EndTime[]"]')[0]).val(end_time);
	}
	
	window.top.tb_remove();
	getTerminalTimeSlotTable(1);
}

function toggleTerminalMonitor(isOn)
{
	if(isOn){
		$('.terminal-monitor').show();
	}else{
		$('.terminal-monitor').hide();
	}
}

function terminalPeriodValueChanged(obj)
{
	var text_val = $.trim(obj.value);
	var num_val = parseInt(text_val);
	if(text_val=='' || (isNaN(num_val) || num_val < 1)){
		num_val = 1;
	}
	obj.value = num_val;
}

$(document).ready(function(){
	getTerminalTimeSlotTable(0);
});
</script>
<?php } ?>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.IPList");
intranet_closedb();
?>