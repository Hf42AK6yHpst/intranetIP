<?
// Editing by 
/*
 * 2016-08-10 (Carlos): $sys_custom['StudentAttendance']['SyncAttendance'] - added crontab job to synchronize attendance data from MyIT School attendance data source.
 * 2015-05-14 (Carlos): $sys_custom['StudentAttendance']['MonitorTerminal'] - added terminal monitoring settings. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$SettingList['TerminalIP'] = $_REQUEST['IPList'];
$SettingList['IgnorePeriod'] = $_REQUEST['ignore_period'];

if($sys_custom['StudentAttendance']['MonitorTerminal']){
	include_once($PATH_WRT_ROOT."includes/libcrontab.php");
	
	$lcrontab = new libcrontab();
	$site = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	
	// remove all exiting jobs
	$currentSettingList = array("'TerminalPingTimeSlot'");
	$currentSettings = $GeneralSetting->Get_General_Setting('StudentAttendance',$currentSettingList);
	$cur_time_slot = trim($currentSettings['TerminalPingTimeSlot']);
	if($cur_time_slot != '')
	{
		$time_slot_ary = explode("|", $cur_time_slot);
		for($i=0;$i<count($time_slot_ary);$i++){
			$script = $site."/cardapi/attendance/monitor_schedule.php?job_num=".$i;
			$lcrontab->removeJob($script);
		}
	}
	
	$SettingList['TerminalMonitor'] = $_REQUEST['TerminalMonitor'];
	$SettingList['TerminalPingPeriod'] = $_REQUEST['TerminalPingPeriod'];
	$time_slot = '';
	$TerminalPingTimeSlotAry = array();
	//if($SettingList['TerminalMonitor'] == 1)
	//{
	$delim = '';
	$StartTime = $_REQUEST['StartTime'];
	$EndTime = $_REQUEST['EndTime'];
	for($i=0;$i<count($StartTime);$i++){
		$TerminalPingTimeSlotAry[] = array($StartTime[$i],$EndTime[$i]);
		$time_slot .= $delim.$StartTime[$i].','.$EndTime[$i];
		$delim = '|';
	}
	//}
	$SettingList['TerminalPingTimeSlot'] = $time_slot;
	
	$TerminalNotifyUser = $_REQUEST['TerminalNotifyUser'];
	$notify_user_value = '';
	$delim = '';
	for($i=0;$i<count($TerminalNotifyUser);$i++){
		$uid_value = trim(str_replace('U','',$TerminalNotifyUser[$i]));
		if($uid_value != ''){
			$notify_user_value .= $delim.$uid_value;
			$delim = ',';
		}
	}
	$SettingList['TerminalNotifyUser'] = $notify_user_value;
}

if($sys_custom['StudentAttendance']['SyncAttendance']){
	$SettingList['SyncAttendance'] = $_REQUEST['SyncAttendance'];
}

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	
	if($sys_custom['StudentAttendance']['MonitorTerminal'] && $_REQUEST['TerminalMonitor'] == 1){
		$job_result = array();
		for($i=0;$i<count($TerminalPingTimeSlotAry);$i++)
		{
			$hour_min = explode(":",$TerminalPingTimeSlotAry[$i][0]);
			$script = $site."/cardapi/attendance/monitor_schedule.php?job_num=".$i;
			$job_result[] = $lcrontab->setJob($hour_min[1], $hour_min[0], '*', '*', '*', $script);
		}
	}
	
	if($sys_custom['StudentAttendance']['SyncAttendance']){
		include_once($PATH_WRT_ROOT."includes/libcrontab.php");
	
		$crontab = new libcrontab();
		$site = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
		$script = $site."/cardapi/attendance/sync_attendance.php";
		$crontab->removeJob($script);
		
		if($_REQUEST['SyncAttendance'] == 1){
			$crontab->setJob('*', '*', '*', '*', '*', $script);
		}
	}
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

header("Location: index.php?Msg=".urlencode($Msg));
?>