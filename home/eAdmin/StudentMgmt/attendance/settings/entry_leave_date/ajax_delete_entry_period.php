<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$RecordID = stripslashes(trim(urldecode($_REQUEST['RecordID'])));

$StudAttend = new libcardstudentattend2();

if ($StudAttend->Delete_Entry_Period($RecordID)) {
	echo $Lang['StudentAttendance']['EntryPeriodRemoveSuccess'];
}
else {
	echo $Lang['StudentAttendance']['EntryPeriodRemoveFail'];
}

intranet_closedb();
?>