<?php
// editing by 
/*********************************** Change log *******************************************
 * 2012-09-10 (Carlos): added js Remove_All_EntryLeaveDates()
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttendUI = new libstudentattendance_ui();
$GeneralSetting = new libgeneralsettings();

$linterface = new interface_html();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage['StudentEntryLeaveSetting'] = 1;
$TAGS_OBJ[] = array($Lang['StudentAttendance']['EntryLeaveDate'], "", 0);
$MODULE_OBJ = $StudentAttendUI->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

echo $StudentAttendUI->Get_Entry_Leave_Date_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// dom function 
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Working_Period_List();
	else
		return false;
}
}

// ajax function
{	
function Delete_Entry_Period(RecordID) {
	var PostVar = {
		"RecordID": RecordID
		};
	
	Block_Element("EntryLeaveTableLayer");
	$.post('ajax_delete_entry_period.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Get_Return_Message(data);
				View_Entry_Leave_Date();
			}
		});
}

function Check_Entry_Leave_Period() {
	var PeriodArray = new Array();
	
	var PeriodStart = Trim($('Input#PeriodStart').val());
	PeriodArray = PeriodStart.split('-');
	var DateStart = new Date();
	DateStart.setFullYear(PeriodArray[0],(PeriodArray[1]-1),PeriodArray[2]);
	
	var PeriodEnd = Trim($('Input#PeriodEnd').val());
	PeriodArray = PeriodEnd.split('-');
	var DateEnd = new Date();
	DateEnd.setFullYear(PeriodArray[0],(PeriodArray[1]-1),PeriodArray[2]);
	
	var WarningRow = document.getElementById('PeriodWarningRow');
	var WarningLayer = $('div#PeriodWarningLayer');
	
	if (PeriodStart == "" && PeriodEnd == "") {
		WarningLayer.html('<?=$Lang['StudentAttendance']['EntryLeavePeriodNullWarning']?>');
		WarningRow.style.display = "";
	}
	else if (DateStart >= DateEnd) {
		WarningLayer.html('<?=$Lang['StudentAttendance']['EntryLeavePeriodEndDateLesserThenStartDateWarning']?>');
		WarningRow.style.display = "";
	}
	else {
		var PostVar = {
				StudentID: $('Input#StudentID').val(),
				RecordID: $('Input#RecordID').val(),
				"PeriodStart": encodeURIComponent(PeriodStart),
				"PeriodEnd": encodeURIComponent(PeriodEnd)
				};
				
		$.post('ajax_check_entry_leave.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else {
					if (data == "1") {
						WarningLayer.html('');
						WarningRow.style.display = "none";
						
						Save_Entry_Leave_Period();
					}
					else {
						WarningLayer.html('<?=$Lang['StudentAttendance']['EntryLeavePeriodOverlapWarning']?>');
						WarningRow.style.display = "";
					}
				}
			});
	}
}

function Save_Entry_Leave_Period() {
	var PostVar = {
			StudentID: $('Input#StudentID').val(),
			RecordID: $('Input#RecordID').val(),
			PeriodStart: encodeURIComponent($('Input#PeriodStart').val()),
			PeriodEnd: encodeURIComponent($('Input#PeriodEnd').val())
			};
	
	Block_Thickbox();
	$.post('ajax_save_entry_leave_period.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				View_Entry_Leave_Date();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		});
}
	
function Get_Entry_Leave_Form(StudentID,RecordID) {
	var RecordID = RecordID || "";
	var PostVar = {
		"StudentID": StudentID,
		"RecordID":RecordID
	};
	
	$.post('ajax_get_entry_leave_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}

function View_Entry_Leave_Date() {
	var PostVar = {
			ClassID: Get_Selection_Value("ClassID","String")
			};

	Block_Element("EntryLeaveTableLayer");
	$('div#EntryLeaveTableLayer').load('ajax_get_entry_leave_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							Thick_Box_Init();
							UnBlock_Element("EntryLeaveTableLayer");
						}
					});
}

function Remove_All_EntryLeaveDates()
{
	if(confirm('<?=$Lang['StudentAttendance']['Warning']['ConfirmRemoveAllEntryLeaveDates']?>'))
	{
		Block_Element("EntryLeaveDateDiv");
		$.post('ajax_delete_entry_period.php',
			{
				'RecordID':''
			},
			function (data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					UnBlock_Element('EntryLeaveDateDiv');
					Get_Return_Message(data);
					View_Entry_Leave_Date();
				}
			});
	}
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>