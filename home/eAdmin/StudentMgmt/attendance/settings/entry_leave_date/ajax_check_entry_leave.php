<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StudAttend = new libcardstudentattend2();

$PeriodStart = trim(urldecode(stripslashes($_REQUEST['PeriodStart'])));
$PeriodEnd = trim(urldecode(stripslashes($_REQUEST['PeriodEnd'])));
$StudentID = $_REQUEST['StudentID'];
$RecordID = $_REQUEST['RecordID'];

if ($StudAttend->Check_Entry_Leave_Period($StudentID,$RecordID,$PeriodStart,$PeriodEnd)) {
	echo "1"; // ok
}
else {
	echo "0"; // not ok
}

intranet_closedb();
?>