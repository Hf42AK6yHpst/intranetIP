<?php
/*
 * 2016-12-07 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['CustomizedTimeSlotSettings']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

$task = $_POST['task'];

switch($task)
{
	case 'getModalForm':
		
		$RecordID = IntegerSafe($_POST['RecordID']);
		$StartTime = '00:00:00';
		$EndTime = '00:00:00';
		$Status = CARD_STATUS_LATE;
		$RecordStatus = '1';
		$record = array();
		if($RecordID > 0){
			$record = $lc->GetCustomizedTimeSlotRecords(array('RecordID'=>$RecordID));
			$StartTime = $record[0]['StartTime'];
			$EndTime = $record[0]['EndTime'];
			$Status = $record[0]['Status'];
			$RecordStatus = $record[0]['RecordStatus'];
		}
		
		$status_ary = array();
		$status_ary[] = array(CARD_STATUS_PRESENT,$Lang['StudentAttendance']['Present']);
		$status_ary[] = array(CARD_STATUS_ABSENT,$Lang['StudentAttendance']['Absent']);
		$status_ary[] = array(CARD_STATUS_LATE,$Lang['StudentAttendance']['Late']);
		$status_selection = getSelectByArray($status_ary, ' id="Status" name="Status" ', $Status, $__all=0, $__noFirst=1, $__FirstTitle="", $__ParQuoteValue=1);
	
		$x .= '<form name="modalForm" id="modalForm" action="" method="post" onsubmit="return false;">'."\n";
		$x .= '<div id="modalContent" style="overflow-y:auto;">'."\n";
		$x .= '<br />'."\n";
		$x .= $linterface->Get_Thickbox_Warning_Msg_Div("Overall_Error","", "WarnMsgDiv")."\n";
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">'."\n";
			$x .= '<col class="field_c">'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['StudentAttendance']['StartTime'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_Time_Selection('StartTime', $StartTime, $others_tab='',$hideSecondSeletion='', $intervalArr=array(1,1,1), $parObjId=false);
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("StartTime_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['StudentAttendance']['EndTime'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_Time_Selection('EndTime', $EndTime, $others_tab='',$hideSecondSeletion='', $intervalArr=array(1,1,1), $parObjId=false);
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("EndTime_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['StudentAttendance']['AttendanceStatus'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $status_selection;
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("AttendanceStatus_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['General']['Status'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_Radio_Button("RecordStatus_Enabled", "RecordStatus", 1, $RecordStatus=='1', $Class="", $Lang['General']['Enabled'], $Onclick="",$isDisabled=0).'&nbsp;';
					$x .= $linterface->Get_Radio_Button("RecordStatus_Disabled", "RecordStatus", 0, $RecordStatus=='0', $Class="", $Lang['General']['Disabled'], $Onclick="",$isDisabled=0);
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("RecordStatus_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
		$x .= '</table>'."\n";
		if($record[0]['RecordID'] !='' && $record[0]['RecordID'] > 0){
			$x .= '<input type="hidden" name="RecordID" id="RecordID" value="'.$RecordID.'" />'."\n";
		}
		$x .= '<input type="hidden" name="task" id="task" value="" />';
		$x .= '</div>'."\n";
		
		$x .= $linterface->MandatoryField();		
		
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkEditForm();").'&nbsp;';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","tb_remove();")."\n";
		$x .= '</div>'."\n";
		
		$x .= '</form>'."\n";
		
		echo $x;
	break;
	
	case 'upsertRecord':
		
		$values = $_POST;
		$values['StartTime'] = sprintf("%02d:%02d:%02d", $_POST['StartTime_hour'], $_POST['StartTime_min'], $_POST['StartTime_sec']);
		$values['EndTime'] = sprintf("%02d:%02d:%02d", $_POST['EndTime_hour'], $_POST['EndTime_min'], $_POST['EndTime_sec']);
		$success = $lc->UpsertCustomizedTimeSlotRecords($values);
		$msg = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		$_SESSION['STUDENT_ATTENDANCE_CUSTOMIZED_TIME_SLOT_SETTINGS_RESULT'] = $msg;
		echo $success ? '1':'0';
		
	break;
	
	case 'checkTimeOverlap':
		
		include_once($PATH_WRT_ROOT."includes/json.php");
		
		$libjson = new JSON_obj();
		
		$values = array();
		$values['CheckTimeOverlap'] = 1;
		$values['StartTime'] = sprintf("%02d:%02d:%02d", $_POST['StartTime_hour'], $_POST['StartTime_min'], $_POST['StartTime_sec']);
		$values['EndTime'] = sprintf("%02d:%02d:%02d", $_POST['EndTime_hour'], $_POST['EndTime_min'], $_POST['EndTime_sec']);
		if(isset($_POST['RecordID'])){
			$values['ExcludeRecordID'] = $_POST['RecordID'];
		}
		$records = $lc->GetCustomizedTimeSlotRecords($values);
		
		echo $libjson->encode($records);
		
	break;
	
	case 'enableRecord':
		
		$RecordStatus = $_POST['RecordStatus'];
		$RecordIDAry = $_POST['RecordID'];
		if(in_array($RecordStatus,array('0','1')) && count($RecordIDAry)>0){
			for($i=0;$i<count($RecordIDAry);$i++){
				$values = array('RecordID'=>$RecordIDAry[$i],'RecordStatus'=>$RecordStatus);
				$lc->UpsertCustomizedTimeSlotRecords($values);
			}
			echo '1';
		}else{
			echo '0';
		}
	break;
	
	case 'deleteRecord':
		
		$success = $lc->DeleteCustomizedTimeSlotRecords($RecordID);
		$msg = $success ? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		$_SESSION['STUDENT_ATTENDANCE_CUSTOMIZED_TIME_SLOT_SETTINGS_RESULT'] = $msg;
		echo $success?'1':'0';
		
	break;
}

intranet_closedb();
?>