<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HostelAttendance']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = $lcardstudentattend2->GetLibGeneralSettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_HostelAttendanceSettings";
$linterface = new interface_html();
$lgroupcat = new libgroupcategory();

//$SettingList = array();
//$SettingList[] = "'HostelAttendanceGroupCategory'";
//$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);

if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
	$SettingList = array();
    $SettingList[] = "'HostelAttendanceLocationIn'";
    $SettingList[] = "'HostelAttendanceLocationOut'";
	$SettingList[] = "'HostelAttendanceDisablePastDate'";
	$SettingList[] = "'HostelAttendanceDisableFutureDate'";
    $Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
}

$category_selection = $lgroupcat->returnSelectCategory(' id="HostelAttendanceGroupCategory" name="HostelAttendanceGroupCategory" ',$noID=false, $all=0, $lcardstudentattend2->HostelAttendanceGroupCategory, $except = array());

$admin_users = $lcardstudentattend2->getHostelAttendanceAdminUsers();
$user_selection = $linterface->GET_SELECTION_BOX($admin_users, ' id="HostelAttendanceAdmin[]" name="HostelAttendanceAdmin[]" multiple="multiple" size="10" style="min-width:12em;" ', $ParDefault, $ParSelected="", $CheckType=false);
$display_admin = '';
for($i=0;$i<count($admin_users);$i++){
	$display_admin .= $admin_users[$i]['UserName']."<br />\n";
}

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HostelAttendanceSettings'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_SETTINGS_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_SETTINGS_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_SETTINGS_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<script type="text/javascript" language="javascript">
function editForm(edit)
{
	if(edit){	
		$('.edit').show();
		$('.display').hide();
	}else{
		$('.edit').hide();
		$('.display').show();
	}
}

function submitForm()
{
	Select_All_Options("HostelAttendanceAdmin[]",true);
	document.form1.submit();
}

$(document).ready(function(){
	$('.display_category').html($('#HostelAttendanceGroupCategory option:selected').text());
});
</script>
<br />
<form name="form1" id="form1" method="post" action="update.php" onsubmit="return false;">
	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['HostelGroupCategory']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$category_selection?>
					</span>
					<span class="display display_category"></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['HostelAttendanceAdmin']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$user_selection?>
						<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Select'], 'button', "newWindow('/home/common_choose/index.php?fieldname=HostelAttendanceAdmin[]&permitted_type=1&excluded_type=2,3,4',15)", $ParName="select_btn", $ParOtherAttribute="", $OtherClass="", $ParTitle=$Lang['Btn']['Select']);?>
						<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], 'button', "remove_selection_option('HostelAttendanceAdmin[]')", $ParName="remove_btn", $ParOtherAttribute="", $OtherClass="", $ParTitle=$Lang['Btn']['Remove']);?>
					</span>
					<span class="display display_admin"><?=$display_admin?></span>
				</td>
			</tr>
            <?php if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) { ?>
            <tr>
                <td class="field_title"><?=$Lang['StudentAttendance']['HostelAttendanceLocationIn']?></td>
                <td>
                <span class="edit" style="display:none">
                    <?=$linterface->GET_TEXTBOX_NAME('LocationIn', 'LocationIn', $Settings['HostelAttendanceLocationIn'])?>
                </span>
                    <span class="display display_admin"><?=$Settings['HostelAttendanceLocationIn']?></span>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['StudentAttendance']['HostelAttendanceLocationOut']?></td>
                <td>
            <span class="edit" style="display:none">
                <?=$linterface->GET_TEXTBOX_NAME('LocationOut', 'LocationOut', $Settings['HostelAttendanceLocationOut'])?>
            </span>
                    <span class="display display_admin"><?=$Settings['HostelAttendanceLocationOut']?></span>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['StudentAttendance']['HostelAttendanceDisablePastDate']?></td>
                <td>
            <span class="edit" style="display:none">
                <input type="checkbox" name="DisablePastDate" value="1" <?=($Settings['HostelAttendanceDisablePastDate']==1?" CHECKED":"")?>>
            </span>
                    <span class="display display_admin"><?=(($Settings['HostelAttendanceDisablePastDate'] == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'])?></span>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['StudentAttendance']['HostelAttendanceDisableFutureDate']?></td>
                <td>
            <span class="edit" style="display:none">
                <input type="checkbox" name="DisableFutureDate" value="1" <?=($Settings['HostelAttendanceDisableFutureDate']==1?" CHECKED":"")?>>
            </span>
                    <span class="display display_admin"><?=(($Settings['HostelAttendanceDisableFutureDate'] == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'])?></span>
                </td>
            </tr>
            <?php } ?>
		</table>
	</div>
	<div class="edit_bottom">
		<?=$linterface->Spacer()?>
		<div class="edit" style="display:none">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'submitForm();','submit_btn','');?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'editForm(0);','cancel_btn','');?>
		</div>
		<div class="display">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'editForm(1);','edit_btn','');?>
		</div>
		<?=$linterface->Spacer()?>
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>