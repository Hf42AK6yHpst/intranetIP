<?php
// Editing by 
/*
 * 2016-08-31 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_HKUSPHDataSubmissionSettings";
$linterface = new interface_html();

$SettingList[] = "'HKUSPH_JobStatus'";
$SettingList[] = "'HKUSPH_JobExecutionTime'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);

$time = $Settings['HKUSPH_JobExecutionTime'];
if($time == ''){
	$time = '00:00';
}
$time_parts = explode(':',$time);
$hour = $time_parts[0];
$minute = $time_parts[1];

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HKUSPHDataSubmissionSettings'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENT_ATTENDANCE_HKUSPH_JOB_SETTINGS_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_HKUSPH_JOB_SETTINGS_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_HKUSPH_JOB_SETTINGS_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<script type="text/javascript" language="javascript">
function editForm(edit)
{
	if(edit){	
		$('.edit').show();
		$('.display').hide();
	}else{
		$('.edit').hide();
		$('.display').show();
	}
}

function submitForm()
{
	document.form1.submit();
}
</script>
<br />
<form name="form1" id="form1" method="post" action="update.php" onsubmit="return false;">
	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['SubmissionJob']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$linterface->Get_Radio_Button('HKUSPH_JobStatus_Enabled', 'HKUSPH_JobStatus', 1, $Settings['HKUSPH_JobStatus']==1, $__Class="", $__Display=$Lang['General']['Enabled'], $__Onclick="",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('HKUSPH_JobStatus_Disabled', 'HKUSPH_JobStatus', 0, $Settings['HKUSPH_JobStatus']!=1, $__Class="", $__Display=$Lang['General']['Disabled'], $__Onclick="",$__isDisabled=0)?>
					</span>
					<span class="display"><?=$Settings['HKUSPH_JobStatus']==1?$Lang['General']['Enabled']:$Lang['General']['Disabled']?></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['SubmissionTime']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$Lang['StudentAttendance']['DailyOn'].$linterface->Get_Time_Selection_Box('hour', 'hour', $hour, $__others_tab='', $__interval=1, 'hour').':'.$linterface->Get_Time_Selection_Box('minute', 'min', $minute, $__others_tab='', $__interval=1, 'minute')?>
					</span>
					<span class="display"><?=$Settings['HKUSPH_JobExecutionTime']==''?Get_String_Display($Settings['HKUSPH_JobExecutionTime']):($Lang['StudentAttendance']['DailyOn'].$Settings['HKUSPH_JobExecutionTime'])?></span>
				</td>
			</tr>
		</table>
	</div>
	<div class="edit_bottom">
		<?=$linterface->Spacer()?>
		<div class="edit" style="display:none">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'submitForm();','submit_btn','');?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'editForm(0);','cancel_btn','');?>
		</div>
		<div class="display">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'editForm(1);','edit_btn','');?>
		</div>
		<?=$linterface->Spacer()?>
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>