<?php
// Editing by 
/*
 * 2019-07-05  (Ray):   Added send to admin, send to teacher
 * 2018-05-07 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_ContinuousAbsenceAlert";
$linterface = new interface_html();

$Settings = $lcardstudentattend2->Settings;

$absent_days = $Settings['ContinuousAbsenceAlertAbsentDays'];
if($absent_days == ''){
	$absent_days = 7;
}

$job_status = $Settings['ContinuousAbsenceAlertJobStatus'];
$job_execution_time = $Settings['ContinuousAbsenceAlertJobExecutionTime'];
if($job_execution_time == ''){
	$job_execution_time = '00:00';
}
$job_execution_time_parts = explode(':',$job_execution_time);
$job_execution_time_hour = $job_execution_time_parts[0];
$job_execution_time_minute = $job_execution_time_parts[1];


$job_adminids = $Settings['ContinuousAbsenceAlertJobSendToAdminIds'];
$name_field = getNameFieldByLang2("u.");
$select_admin = '';
$selected_admin = '';
$admin_user_ids_filtered = array();
$admin_user_ids_selected = array();

$selected_admin_array = array();
if (strlen($job_adminids) > 0) {
    $selected_admin_array = explode(',', $job_adminids);
}
$sql = "SELECT 
                    DISTINCT u.UserID, $name_field as UserName
                FROM ROLE as r 
                INNER JOIN ROLE_RIGHT as rr ON rr.RoleID=r.RoleID 
                INNER JOIN ROLE_MEMBER as rm ON rm.RoleID=r.RoleID 
                INNER JOIN INTRANET_USER as u ON u.UserID=rm.UserID 
                WHERE u.RecordStatus='1' AND u.RecordType=1 AND rr.FunctionName='eAdmin-StudentAttendance' ";

$admin_user_ids = $lcardstudentattend2->returnArray($sql);
foreach ($admin_user_ids as $temp) {
    if (in_array($temp['UserID'], $selected_admin_array) == false) {
        $admin_user_ids_filtered[] = $temp;
    } else {
        $admin_user_ids_selected[] = $temp;
    }
}
$adminStringForShow = '';
if ($Settings['ContinuousAbsenceAlertJobSendToAdmin'] == 1 && $Settings['ContinuousAbsenceAlertJobSendToAdminAll'] != 1) {
    foreach ($admin_user_ids_selected as $i => $adminInfo) {
        if ($i != 0) {
			$adminStringForShow .= '<br>';
        }
		$adminStringForShow .= $adminInfo['UserName'];
    }
}
$select_admin = getSelectByArray($admin_user_ids_filtered, "id=\"adminID\" size=\"10\" multiple=\"multiple\" name=\"adminID[]\" style=\"width: 99%\"","",0,1);
$selected_admin = getSelectByArray($admin_user_ids_selected, "id=\"adminIDSelected\" size=\"10\" multiple=\"multiple\" name=\"adminIDSelected[]\" style=\"width: 99%\"","",0,1);


$job_teacherids = $Settings['ContinuousAbsenceAlertJobSendToTeacherIds'];
$selected_teacher_array = array();
if(strlen($job_teacherids) > 0) {
	$selected_teacher_array = explode(',', $job_teacherids);
}
$sql = "SELECT u.UserID, $name_field as UserName
    FROM INTRANET_USER as u
    WHERE u.RecordStatus='1' AND u.RecordType='".USERTYPE_STAFF."' AND u.Teaching='1' ORDER BY UserName";

$teacher_user_ids = $lcardstudentattend2->returnArray($sql);
$teacher_ids_filtered = array();
$teacher_ids_selected = array();
foreach($teacher_user_ids as $temp) {
	if(in_array($temp['UserID'], $selected_teacher_array) == false) {
		$teacher_ids_filtered[] = $temp;
	} else {
		$teacher_ids_selected[] = $temp;
	}
}
$classTeacherStringForShow = '';
if($Settings['ContinuousAbsenceAlertJobSendToTeacher'] == 1 && $Settings['ContinuousAbsenceAlertJobSendToTeacherAll'] != 1) {
	foreach ($teacher_ids_selected as $i => $teacherInfo) {
		if ($i != 0) {
			$classTeacherStringForShow .= '<br>';
		}
		$classTeacherStringForShow .= $teacherInfo['UserName'];
	}
}

$select_teacher = getSelectByArray($teacher_ids_filtered, "id=\"teacherID\" size=\"10\" multiple=\"multiple\" name=\"teacherID[]\" style=\"width: 99%\"", "", 0, 1);
$selected_teacher = getSelectByArray($teacher_ids_selected, "id=\"teacherIDSelected\" size=\"10\" multiple=\"multiple\" name=\"teacherIDSelected[]\" style=\"width: 99%\"", "", 0, 1);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ContinuousAbsenceAlert'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENT_ATTENDANCE_CONTINUOUS_ABSENCE_ALERT_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_CONTINUOUS_ABSENCE_ALERT_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_CONTINUOUS_ABSENCE_ALERT_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<script type="text/javascript" language="javascript">
function absenceDaysChanged(obj)
{
	var text_val = $.trim(obj.value);
	var num_val = parseInt(text_val);
	if(isNaN(num_val) || text_val == '' || num_val <= 0){
		obj.value = '7';
		return;
	}
	obj.value = num_val;
}

function editForm(edit)
{
	if(edit){	
		$('.edit').show();
		$('.display').hide();
	}else{
		$('.edit').hide();
		$('.display').show();
	}
}


function submitForm()
{
    var error_no = 0;
    $("#div_ToAdmin_err_msg").html('');
    if (compareDate($('#DateStart').val(),$('#DateEnd').val()) > 0){
        $('#div_DateEnd_err_msg').html('<?=$i_Homework_new_duedate_wrong?>');
        error_no ++;
    }

    var to_admin = $("input=[name='ContinuousAbsenceAlertJobSendToAdmin']:checked").val();
    var to_teacher = $("input=[name='ContinuousAbsenceAlertJobSendToTeacher']:checked").val();
    if(to_admin != 1 && to_teacher != 1) {
        $("#div_ToAdmin_err_msg").html('<?=$Lang['StudentAttendance']['SelectSendToAdminClassTeacherErrorMessage']?>');
        error_no++;
    }

    if (error_no != 0) {
        return;
    }

    Select_All_Options('adminIDSelected',true);
    Select_All_Options('teacherIDSelected',true);
    document.form1.submit();
}
function showUserIdSelect(target, show) {
    if(show) {
        $("#" + target).show();
    } else {
        $("#" + target).hide();
    }
}
function Add_All_User(selectid, selectedid) {
    var User = document.getElementById(selectid);
    var UserSelected = document.getElementById(selectedid);

    for (var i = (User.length -1); i >= 0 ; i--) {
        UserObj = User.options[i];
        UserSelected.options[UserSelected.length] = new Option(UserObj.text,UserObj.value);
        User.options[i] = null;
    }
}
function Add_Selected_User(selectid, selectedid) {
    var User = document.getElementById(selectid);
    var UserSelected = document.getElementById(selectedid);

    for (var i = (User.length - 1); i >= 0; i--) {
        if (User.options[i].selected) {
            UserObj = User.options[i];
            UserSelected.options[UserSelected.length] = new Option(UserObj.text, UserObj.value);
            User.options[i] = null;
        }
    }
}


</script>
<br />
<form name="form1" id="form1" method="post" action="update.php" onsubmit="return false;">
	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['ContinuousAbsentDays']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$linterface->GET_TEXTBOX_NUMBER("AbsentDays", "AbsentDays", $absent_days, '', array('onchange'=>'absenceDaysChanged(this);'))?> <?=$Lang['StudentAttendance']['Days']?>
					</span>
					<span class="display"><?=$absent_days?> <?=$Lang['StudentAttendance']['Days']?></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['EmailNotificationJobStatus']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobStatus_Enabled', 'ContinuousAbsenceAlertJobStatus', 1, $Settings['ContinuousAbsenceAlertJobStatus']==1, $__Class="", $__Display=$Lang['General']['Enabled'], $__Onclick="",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobStatus_Disabled', 'ContinuousAbsenceAlertJobStatus', 0, $Settings['ContinuousAbsenceAlertJobStatus']!=1, $__Class="", $__Display=$Lang['General']['Disabled'], $__Onclick="",$__isDisabled=0)?>
					</span>
					<span class="display"><?=$Settings['ContinuousAbsenceAlertJobStatus']==1?$Lang['General']['Enabled']:$Lang['General']['Disabled']?></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['EmailNotificationJobExecutionTime']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$Lang['StudentAttendance']['DailyOn'].$linterface->Get_Time_Selection_Box('ContinuousAbsenceAlertJobExecutionTime_hour', 'hour', $job_execution_time_hour, $__others_tab='', $__interval=1, 'ContinuousAbsenceAlertJobExecutionTime_hour').':'.$linterface->Get_Time_Selection_Box('ContinuousAbsenceAlertJobExecutionTime_minute', 'min', $job_execution_time_minute, $__others_tab='', $__interval=1, 'ContinuousAbsenceAlertJobExecutionTime_minute')?>
					</span>
					<span class="display"><?=$Settings['ContinuousAbsenceAlertJobExecutionTime']==''?Get_String_Display($Settings['ContinuousAbsenceAlertJobExecutionTime']):($Lang['StudentAttendance']['DailyOn'].$Settings['ContinuousAbsenceAlertJobExecutionTime'])?></span>
				</td>
			</tr>
            <tr>
                <td class="field_title"><?=$Lang['StaffAttendance']['DateRange']?></td>
                <td>
					<span class="edit" style="display:none">
						<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobDateRangeAcademicYear', 'ContinuousAbsenceAlertJobDateRangeAcademicYear', 1, $Settings['ContinuousAbsenceAlertJobDateRangeAcademicYear']==1, $__Class="", $__Display=$Lang['StudentAttendance']['EmailNotificationJobCurrentAcademicYear'], $__Onclick="showUserIdSelect('alert_job_date_range', false)",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobDateRangeSpecificDateRange', 'ContinuousAbsenceAlertJobDateRangeAcademicYear', 0, $Settings['ContinuousAbsenceAlertJobDateRangeAcademicYear']!=1, $__Class="", $__Display=$Lang['StudentAttendance']['EmailNotificationJobSpecificDateRange'], $__Onclick="showUserIdSelect('alert_job_date_range', true)",$__isDisabled=0)?>
					</span>
                    <span class="display">
                        <?php
						if($Settings['ContinuousAbsenceAlertJobDateRangeAcademicYear'] == 1) {
							echo $Lang['StudentAttendance']['EmailNotificationJobCurrentAcademicYear'];
						} else {
							echo $iDiscipline['Period_Start'].' '.$Settings['ContinuousAbsenceAlertJobDateRangeStartDate'].' '.$iDiscipline['Period_End'].' '.$Settings['ContinuousAbsenceAlertJobDateRangeEndDate'];
						}
						?>
                    </span>
                    <div class="edit" style="display:none">
                        <div id="alert_job_date_range" style="<?=(($Settings['ContinuousAbsenceAlertJobDateRangeAcademicYear']==1) ? 'display:none;' : '')?>">
                            <br>
							<?=$iDiscipline['Period_Start']?> <?=$linterface->GET_DATE_PICKER("DateStart",$Settings['ContinuousAbsenceAlertJobDateRangeStartDate'])?>
							<?=$iDiscipline['Period_End']?> <?=$linterface->GET_DATE_PICKER("DateEnd",$Settings['ContinuousAbsenceAlertJobDateRangeEndDate'])?>
                            <br><span id='div_DateEnd_err_msg'></span>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="field_title"><?=$Lang['StudentAttendance']['EmailNotificationJobSendToAdmin']?></td>
                <td>
					<span class="edit" style="display:none">
		                <?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToAdmin_Yes', 'ContinuousAbsenceAlertJobSendToAdmin', 1, $Settings['ContinuousAbsenceAlertJobSendToAdmin']==1, $__Class="", $__Display=$Lang['General']['Yes'], $__Onclick="showUserIdSelect('alert_job_send_to_admin', true)",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToAdmin_No', 'ContinuousAbsenceAlertJobSendToAdmin', 0, $Settings['ContinuousAbsenceAlertJobSendToAdmin']!=1, $__Class="", $__Display=$Lang['General']['No'], $__Onclick="showUserIdSelect('alert_job_send_to_admin', false)",$__isDisabled=0)?>
					    <br><span id='div_ToAdmin_err_msg'></span>
                    </span>
                    <span class="display"><?=$Settings['ContinuousAbsenceAlertJobSendToAdmin']==1?$Lang['General']['Yes']:$Lang['General']['No']?>
					<?php if($Settings['ContinuousAbsenceAlertJobSendToAdmin'] == 1 && $Settings['ContinuousAbsenceAlertJobSendToAdminAll'] != 1) { ?>
                    <div>
						<table class="common_table_list_v30">
							<tr>
								<td class="field_title" style="width:30%">
									<?=$Lang['StudentAttendance']['OnlySelectedAdminUsers']?>
								</td>
								<td>
									<?=$adminStringForShow ?>
								</td>
							</tr>
						</table>
					</div>
						<?php } ?>
                    </span>

                    <div class="edit" style="display:none">
                        <div id="alert_job_send_to_admin" style="<?=(($Settings['ContinuousAbsenceAlertJobSendToAdmin']!=1) ? 'display:none;' : '')?>">
							<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToAdminIds_All', 'ContinuousAbsenceAlertJobSendToAdminAll', 1, $Settings['ContinuousAbsenceAlertJobSendToAdminAll']==1, $__Class="", $__Display=$Lang['StudentAttendance']['AllAdminUsers'], $__Onclick="showUserIdSelect('alert_job_select_admin', false)",$__isDisabled=0)?>
							<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToAdminIds_Select', 'ContinuousAbsenceAlertJobSendToAdminAll', 0, $Settings['ContinuousAbsenceAlertJobSendToAdminAll']!=1, $__Class="", $__Display=$Lang['StudentAttendance']['OnlySelectedAdminUsers'], $__Onclick="showUserIdSelect('alert_job_select_admin', true)",$__isDisabled=0)?>

                            <div id="alert_job_select_admin" style="<?=(($Settings['ContinuousAbsenceAlertJobSendToAdminAll']==1) ? 'display:none;' : '')?>">
                                <table width="100%" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td style="width: 45%">
											<?=$select_admin?>
                                        </td>
                                        <td align="center">
                                            <input name="AddAll" onclick="Add_All_User('adminID','adminIDSelected');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value=">>" style="width:40px;" title="Add All">
                                            <br>
                                            <input name="Add" onclick="Add_Selected_User('adminID','adminIDSelected');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value=">" style="width:40px;" title="Add Selected">
                                            <br><br>
                                            <input name="Remove" onclick="Add_Selected_User('adminIDSelected','adminID');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<" style="width:40px;" title="Remove Selected">
                                            <br>
                                            <input name="RemoveAll" onclick="Add_All_User('adminIDSelected','adminID');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<<" style="width:40px;" title="Remove All">
                                        </td>
                                        <td style="width: 45%">
											<?=$selected_admin?>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['StudentAttendance']['EmailNotificationJobSendToTeacher']?></td>
                <td>
					<span class="edit" style="display:none">
		                <?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToTeacher_Yes', 'ContinuousAbsenceAlertJobSendToTeacher', 1, $Settings['ContinuousAbsenceAlertJobSendToTeacher']==1, $__Class="", $__Display=$Lang['General']['Yes'], $__Onclick="showUserIdSelect('alert_job_send_to_teacher', true)",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToTeacher_No', 'ContinuousAbsenceAlertJobSendToTeacher', 0, $Settings['ContinuousAbsenceAlertJobSendToTeacher']!=1, $__Class="", $__Display=$Lang['General']['No'], $__Onclick="showUserIdSelect('alert_job_send_to_teacher', false)",$__isDisabled=0)?>
					</span>
                    <span class="display"><?=$Settings['ContinuousAbsenceAlertJobSendToTeacher']==1?$Lang['General']['Yes']:$Lang['General']['No']?>
                    <?php if($Settings['ContinuousAbsenceAlertJobSendToTeacher'] == 1 && $Settings['ContinuousAbsenceAlertJobSendToTeacherAll'] != 1) { ?>
                    <div>
						<table class="common_table_list_v30">
							<tr>
								<td class="field_title" style="width:30%">
									<?=$Lang['StudentAttendance']['OnlySelectedTeacherUsers']?>
								</td>
								<td>
									<?=$classTeacherStringForShow ?>
								</td>
							</tr>
						</table>
					</div>
                    <?php } ?>
					</span>

                    <div class="edit" style="display:none">
                        <div id="alert_job_send_to_teacher" style="<?=(($Settings['ContinuousAbsenceAlertJobSendToTeacher']!=1) ? 'display:none;' : '')?>">
							<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToTeacherIds_All', 'ContinuousAbsenceAlertJobSendToTeacherAll', 1, $Settings['ContinuousAbsenceAlertJobSendToTeacherAll']==1, $__Class="", $__Display=$Lang['StudentAttendance']['AllTeacherUsers'], $__Onclick="showUserIdSelect('alert_job_select_teacher', false)",$__isDisabled=0)?>
							<?=$linterface->Get_Radio_Button('ContinuousAbsenceAlertJobSendToTeacherIds_Select', 'ContinuousAbsenceAlertJobSendToTeacherAll', 0, $Settings['ContinuousAbsenceAlertJobSendToTeacherAll']!=1, $__Class="", $__Display=$Lang['StudentAttendance']['OnlySelectedTeacherUsers'], $__Onclick="showUserIdSelect('alert_job_select_teacher', true)",$__isDisabled=0)?>

                            <div id="alert_job_select_teacher" style="<?=(($Settings['ContinuousAbsenceAlertJobSendToTeacherAll']==1) ? 'display:none;' : '')?>">
                                <table width="100%" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td style="width: 45%">
											<?=$select_teacher?>
                                        </td>
                                        <td align="center">
                                            <input name="AddAll" onclick="Add_All_User('teacherID','teacherIDSelected');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value=">>" style="width:40px;" title="Add All">
                                            <br>
                                            <input name="Add" onclick="Add_Selected_User('teacherID','teacherIDSelected');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value=">" style="width:40px;" title="Add Selected">
                                            <br><br>
                                            <input name="Remove" onclick="Add_Selected_User('teacherIDSelected','teacherID');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<" style="width:40px;" title="Remove Selected">
                                            <br>
                                            <input name="RemoveAll" onclick="Add_All_User('teacherIDSelected','teacherID');" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<<" style="width:40px;" title="Remove All">
                                        </td>
                                        <td style="width: 45%">
											<?=$selected_teacher?>
                                        </td>
                                    </tr>
                                </table>


                            </div>
                        </div>
                    </div>
                </td>
            </tr>
			<tr>
				<td colspan="2"><div class="tabletextremark"><?=$Lang['StudentAttendance']['ContinuousAbsenceAlertRemark']?></div></td>
			</tr>
		</table>
	</div>
	
	<div class="edit_bottom">
		<?=$linterface->Spacer()?>
		<div class="edit" style="display:none">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'submitForm();','submit_btn','');?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'editForm(0);','cancel_btn','');?>
		</div>
		<div class="display">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'editForm(1);','edit_btn','');?>
		</div>
		<?=$linterface->Spacer()?>
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>