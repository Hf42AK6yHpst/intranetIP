<?
// Editing by 
/*
 * 2014-06-20 (Carlos): add $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] for the session step value
 * 2014-03-13 (Carlos): $sys_custom['SmartCardAttendance_StudentAbsentSession2'] - Added default number of [Absent] session 
 * 2013-11-05 (Henry): File Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) && ($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_SessionSettings";

$linterface = new interface_html();
/*
$SettingList[] = "'DefaultNumOfSessionForAbsent'";
$SettingList[] = "'DefaultNumOfSessionForLate'";
$SettingList[] = "'DefaultNumOfSessionForLeave'";
$SettingList[] = "'DefaultNumOfSessionForAbsenteesism'";
$SettingList[] = "'DefaultNumOfSessionForHoliday'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
*/
$Settings = $lcardstudentattend2->getDefaultNumOfSessionSettings();

//creation of the selectionBox
if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	$AbsentSession = -1;
	$selAbsentSession = "<select name=\"absent_session\" id=\"absent_session\"/>";
	$AbsentSelected = (!isset($Settings['DefaultNumOfSessionForAbsent']) || $Settings['DefaultNumOfSessionForAbsent'] == -1)? "selected":"";
	$selAbsentSession .= "<option value=\"-1\" ".$AbsentSelected.">".$Lang['General']['NotApplicable']."</option>";
	
	$k=0.0;
	while($k<=CARD_STUDENT_MAX_SESSION){
		$AbsentSelected = (isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] == $k)? "selected":"";
		$selAbsentSession .= "<option value=\"$k\" ".$AbsentSelected." >".$k."</option>";
		$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep']>0? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
	}
	$selAbsentSession .= "</select>\n";
}
if($sys_custom['SmartCardAttendance_StudentAbsentSession']){
	$LateSession = -1;
	$OfficalLeaveSession = -1;
	$RequestLeaveSession = -1;
	$PlayTruantSession = -1;
	
	$selLateSession = "<select name=\"late_session\" id=\"late_session\"/>";
	$selRequestLeaveSession = "<select name=\"request_leave_session\" id=\"request_leave_session\"/>";
	$selPlayTruantSession = "<select name=\"play_truant_session\" id=\"play_truant_session\"/>";
	$selOfficalLeaveSession = "<select name=\"offical_leave_session\" id=\"offical_leave_session\"/>";
	
	$LateSelected = (!isset($Settings['DefaultNumOfSessionForLate']) || $Settings['DefaultNumOfSessionForLate'] == -1)? "selected":"";
	$OfficalLeaveSelected = (!isset($Settings['DefaultNumOfSessionForHoliday']) || $Settings['DefaultNumOfSessionForHoliday'] == -1)? "selected":"";
	$RequestLeaveSelected = (!isset($Settings['DefaultNumOfSessionForLeave']) || $Settings['DefaultNumOfSessionForLeave'] == -1)? "selected":"";
	$PlayTruantSelected = (!isset($Settings['DefaultNumOfSessionForAbsenteesism']) || $Settings['DefaultNumOfSessionForAbsenteesism'] == -1)? "selected":"";
	
	$selOfficalLeaveSession .= "<option value=\"-1\" ".$OfficalLeaveSelected.">".$Lang['General']['NotApplicable']."</option>";
	$selLateSession .= "<option value=\"-1\" ".$LateSelected.">".$Lang['General']['NotApplicable']."</option>";
	$selRequestLeaveSession .= "<option value=\"-1\" ".$RequestLeaveSelected.">".$Lang['General']['NotApplicable']."</option>";
	$selPlayTruantSession .= "<option value=\"-1\" ".$PlayTruantSelected.">".$Lang['General']['NotApplicable']."</option>";
	
	$k=0.0;
	while($k<=CARD_STUDENT_MAX_SESSION){
		$LateSelected = (isset($Settings['DefaultNumOfSessionForLate']) && $Settings['DefaultNumOfSessionForLate'] == $k)? "selected":"";
		$OfficalLeaveSelected = (isset($Settings['DefaultNumOfSessionForHoliday']) && $Settings['DefaultNumOfSessionForHoliday'] == $k)? "selected":"";
		$RequestLeaveSelected = (isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] == $k)? "selected":"";
		$PlayTruantSelected = (isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] == $k)? "selected":"";
		
		$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
		$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
		$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
		$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
		$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep']>0? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
	}
	$selOfficalLeaveSession .= "</select>\n";
	$selLateSession .= "</select>\n";
	$selRequestLeaveSession .= "</select>\n";
	$selPlayTruantSession .= "</select>\n";
}

$TAGS_OBJ[] = array($Lang['StudentAttendance']['SessionSettings'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="post" action="update.php">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right" class="tabletext">
			<?=$SysMsg?>
		</td>
	</tr>
	<?php if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){ ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultNumAbsent']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$selAbsentSession?>
		</td>
	</tr>
	<?php } ?>
	<?php if($sys_custom['SmartCardAttendance_StudentAbsentSession']){ ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultNumLate']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$selLateSession?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultNumRequestedLeave']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$selRequestLeaveSession?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultNumAbsenteesism']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$selPlayTruantSession?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultNumPublicOfficialHoliday']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$selOfficalLeaveSession?>
		</td>
	</tr>
	<?php } ?>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "button", "goSubmit()") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<script>
function goSubmit() {
	var obj = document.form1;
	obj.submit();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>