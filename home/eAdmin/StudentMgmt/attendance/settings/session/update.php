<?
// Editing by 
/*
 * 2014-03-13 (Carlos): $sys_custom['SmartCardAttendance_StudentAbsentSession2'] - Added default number of [Absent] session 
 * 2013-11-06 (Henry): File Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) && ($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	$SettingList['DefaultNumOfSessionForAbsent'] = $_REQUEST['absent_session'];
}
if($sys_custom['SmartCardAttendance_StudentAbsentSession']){
	$SettingList['DefaultNumOfSessionForLate'] = $_REQUEST['late_session'];
	$SettingList['DefaultNumOfSessionForLeave'] = $_REQUEST['request_leave_session'];
	$SettingList['DefaultNumOfSessionForAbsenteesism'] = $_REQUEST['play_truant_session'];
	$SettingList['DefaultNumOfSessionForHoliday'] = $_REQUEST['offical_leave_session'];
}

$lc = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

header("Location: index.php?Msg=".urlencode($Msg));
?>