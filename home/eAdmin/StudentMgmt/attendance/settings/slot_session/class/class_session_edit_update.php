<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->Start_Trans();
if($type == 1)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $weekday_session, DateModified = 'NOW()' WHERE ClassID = $ClassID AND DayType = $type AND DayValue = '$value'";
	$Result['UpdateSessionRegular'] = $lc->db_db_query($sql);
}

if($type == 2)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $cycleday_session, DateModified = 'NOW()' WHERE ClassID = $ClassID AND DayType = $type AND DayValue = '$value'";
	$Result['UpdateSessionRegular'] = $lc->db_db_query($sql);
}

if($type == 3)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_DATE SET SessionID = $specialday_session, DateModified = 'NOW()' WHERE ClassID = $ClassID AND RecordDate = '$value'";
	$Result['UpdateTimeSessionDate'] = $lc->db_db_query($sql);
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lc->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lc->Commit_Trans();
}

Header("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
intranet_closedb();
?>