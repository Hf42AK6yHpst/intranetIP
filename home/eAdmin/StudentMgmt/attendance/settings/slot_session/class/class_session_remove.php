<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libcardstudentattend2();
if($type==1||$type==2) {
	$sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID='$ClassID' AND DayType='$type' AND DayValue ='$value'";
} else if($type == 7) {
	$value = str_replace(",","','",$value);
	$sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID='$ClassID' AND RecordDate IN ('$value')";
} else {
	$sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID='$ClassID' AND RecordDate='$TargetDate'";
}

if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

intranet_closedb();
header("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
?>