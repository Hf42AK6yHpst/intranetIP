<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

//echo $SessionID;
/*
if($SetOtherClasses != '')
{
	if($SetOtherClasses == 0)
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
	else
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
}
else
{
	$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
}

if($SetOtherClasses == 1)
{
*/
	$sql = "SELECT NonSchoolDay FROM CARD_STUDENT_TIME_SESSION WHERE SessionID = '".$lc->Get_Safe_Sql_Query($SessionID)."'";
	$check_non_school_day = $lc->returnVector($sql);

	$temp_arr = array(array(0,"$i_StudentAttendance_NormalDays"),array(1,"$i_StudentAttendance_Weekday_Specific"),array(2,"$i_StudentAttendance_Cycleday_Specific"),array(3,"$i_StudentAttendance_SpecialDay"));
	$result = $lc->getClassList();
	$select_type = getSelectByArray($temp_arr, "name=selected_type onChange=\"document.form1.flag.value=1; this.form.submit();\"", $selected_type,0,1);

	$table_content .= "<tr>
											<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_TimeSession_TypeSelection</td>";
	$table_content .= "<td class=\"tabletext\" width=\"70%\">$select_type</td></tr>";
	if(($selected_type == "") || ($selected_type == 0))
	{
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ClassName</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td class=\"tabletext\" width=\"70%\">";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
	if($selected_type == 1)
	{
		$select_day = "<SELECT name=DayValue>\n";
	    $select_day .= "<OPTION value=''>-- $button_select --</OPTION>";
	    for ($i=0; $i<7; $i++)
	    {
	         $word = $i_DayType0[$i];
	         $select_day .= "<OPTION value='".$i."'>".$word."</OPTION>\n";
	    }
	    $select_day .= "</SELECT>\n";
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_WeekDay</td><td class=\"tabletext\" width=\"70%\">$select_day</td></tr>";
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ClassName</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td class=\"tabletext\" width=\"70%\">";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
	if($selected_type == 2)
	{
		$sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS";	
		$temp = $lc->returnVector($sql);
		$select_day = getSelectByValue($temp,"name=DayValue");
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_CycleDay</td><td class=\"tabletext\" width=\"70%\">$select_day</td></tr>";
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_TimeSession_ClassSelection</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td class=\"tabletext\" width=\"70%\">";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
	if($selected_type == 3)
	{
		$select_day = $linterface->GET_DATE_PICKER("TargetDate",date('Y-m-d'));
		$select_day .= "&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>\n";
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_TimeSlot_SpecialDay</td><td class=\"tabletext\" width=\"70%\">$select_day</td></tr>";
		$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_TimeSession_ClassSelection</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td class=\"tabletext\" width=\"70%\">";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
//}

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 0);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>

<SCRIPT Language="JavaScript">
function checkForm()
{
	var obj = document.form1;
	var temp = document.form1['class[]'];
	var checking = 0;
	
	if(obj.selected_type.value != "")
	{		
		if(obj.selected_type.value == 0)
		{
			if(<?=$check_non_school_day[0]?> == 0)
			{
				for(i=0; i<temp.length; i++)
				{
					if(temp[i].checked)
						checking = 1;
				}
			}
			else
			{
				alert("<?=$i_StudentAttendance_NonSchoolDaySetting_Warning?>");
				return false;
			}
		}	
		
		if((obj.selected_type.value == 1)||(obj.selected_type.value == 2))
		{
			if(obj.DayValue.value != "")
			{
				for(i=0; i<temp.length; i++)
				{
					if(temp[i].checked)
						checking = 1;
				}
			}
			else
			{
				if(obj.selected_type.value == 1)
				{
					alert("<?=$i_StudentAttendance_Warn_Please_Select_Weekday?>");
					return false;
				}
				if(obj.selected_type.value == 2)
				{
					alert("<?=$i_StudentAttendance_Warn_Please_Select_CycleDay?>");
					return false;
				}
			}
		}
		if(obj.selected_type.value == 3)
		{
			if(obj.TargetDate != "")
			{
				for(i=0; i<temp.length; i++)
				{
					if(temp[i].checked)
						checking = 1;
				}
			}
			else
			{
				alert("<?=$i_StudentAttendance_Warn_Please_Select_A_Day?>");
				return false;
			}
		}
		if(checking == 1)
		{
			obj.action = "multi_class_update.php?SessionID=<?=urlencode($SessionID)?>";
			obj.submit();
		}
		else
		{
			alert("<?=$i_StudentAttendance_Class_Select_Instruction?>");
			return false;
		}
	}
	else
	{
		alert("<?=$i_StudentAttendance_TimeSession_TypeSelectionWarning?>");
		return false;
	}
}
</SCRIPT>

<form name=form1 action="" method=POST onSubmit="return checkForm()";>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<?=$table_content?>
		</table>
	</td>
</tr>
</table>

<table width=560 align=center>
<tr><td height=10></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr>
	<td align="center" colspan="2">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'edit.php?s_id=".urlencode($SessionID)."';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	</td>
</tr>
</table>
<input type=hidden name=flag value=0>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>