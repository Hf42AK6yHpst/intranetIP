<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libcardstudentattend2();


$lclass->Start_Trans();
$sql = "UPDATE CARD_STUDENT_TIME_SESSION SET RecordStatus = 0";
$Result['ResetTimeSessionStatus'] = $lclass->db_db_query($sql);

if(sizeof($record_status)>0)
{
	$update_list = implode(",",$record_status);
		
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION SET RecordStatus = 1 WHERE SessionID IN ($update_list)";
	$Result['UpdateTimeSessionStatus'] = $lclass->db_db_query($sql);
}

if($i==0)
{
	$sql = "SELECT RecordID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID=0 AND DayType=0 AND DayValue=0";
	$result = $lclass->returnArray($sql,1);
	if(sizeof($result)==0)
	{
		//echo "Hi";
		$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION WHERE NonSchoolDay = 0 AND RecordStatus = 1 ORDER BY SessionID ASC LIMIT 0, 1";
		$result = $lclass->returnArray($sql,1);
		//print_r($result);
		if(sizeof($result)>0)
		{
			for($j=0;$j<sizeof($result);$j++)
			{
				list($s_id) = $result[$j];
			}
			$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', 0, 0, 0, $s_id, '', '', NOW(), NOW())";
			//echo $sql;
			$Result['InsertRegularTimeSession'] = $lclass->db_db_query($sql);
		}
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lclass->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lclass->Commit_Trans();
}
		
Header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>