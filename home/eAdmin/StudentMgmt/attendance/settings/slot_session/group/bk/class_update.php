<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();

if ($mode != 1 && $mode != 2)
{
    $mode = 0;
}
$li = new libdb();
$sql = "INSERT INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified)
               VALUES ('$ClassID','$mode',NOW(),NOW())";
$li->db_db_query($sql);

if ($li->db_affected_rows()!=1)
{
    $sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE
                   SET Mode = '$mode', DateModified = NOW() WHERE ClassID = '$ClassID'";
    $li->db_db_query($sql);
}

if ($mode != 1)
{
    # Clear detail timetable
    $sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_REGULAR
                   WHERE ClassID = '$ClassID'";
    # Clear special timetable
    $sql2= "DELETE FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID='$ClassID' ";
    $li->db_db_query($sql);
    $li->db_db_query($sql2);
    $url = "class.php";
}
else
{
    $url = "class_edit.php";
}

intranet_closedb();
header("Location: $url?ClassID=$ClassID&msg=2");
?>