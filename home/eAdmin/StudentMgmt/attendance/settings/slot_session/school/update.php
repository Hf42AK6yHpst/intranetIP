<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->Start_Trans();
if($normal_session_id!="")
{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', 0, 0, 0, $normal_session_id, '', '', NOW(), NOW())";
	$Result['InsertTimeSessionRegular'] = $lc->db_db_query($sql);
	$x=1;
	if ($lc->db_affected_rows()==0)
	{
		$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $normal_session_id, DateModified = NOW() WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
		$Result['UpdateTimeSessionRegular'] = $lc->db_db_query($sql);
		$x=2;
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lc->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lc->Commit_Trans();
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>