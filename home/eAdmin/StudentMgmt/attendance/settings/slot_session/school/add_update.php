<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getSessionDayValueWithoutSpecific($type);

$lc->Start_Trans();
if($type==1)
{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', 0, 1, '$DayValue', $weekday_session, '', '', NOW(), NOW())";
	$Result['InsertTimeSessionRegular'] = $lc->db_db_query($sql);
	$x=1;
	if ($lc->db_affected_rows()==0)
	{
		$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $weekday_session, DateModified = NOW() WHERE DayType = 1 AND DayValue = '$DayValue'";
		$Result['UpdateTimeSessionRegular'] = $lc->db_db_query($sql);
		$x=2;
	}
}
if($type==2)
{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', 0, 2, '$DayValue', $cycle_session, '', '', NOW(), NOW())";
	$Result['InsertTimeSessionRegular'] = $lc->db_db_query($sql);
	$x=1;
	if ($lc->db_affected_rows()==0)
	{
		$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $cycle_session, DateModified = NOW() WHERE DayType = 2 AND DayValue = '$DayValue'";
		$Result['UpdateTimeSessionRegular'] = $lc->db_db_query($sql);
		$x=2;
	}	
}
if($type==3)
{
	$TargetDate = $_REQUEST['target_date'];
	for ($i=0; $i< sizeof($TargetDate); $i++) {
		$sql = "INSERT INTO CARD_STUDENT_TIME_SESSION_DATE VALUES 
						('', 0, '".$TargetDate[$i]."', '".$special_session."', '', '', NOW(), NOW()) 
						on duplicate key update 
							SessionID = '".$special_session."', 
							DateModified = NOW()
						";
		$Result['InsertTimeSessionDate'.$i] = $lc->db_db_query($sql);
	
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lc->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lc->Commit_Trans();
}

header("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>