<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

# Retreive data
(isset($_GET['SlotID']) && $SlotID != "") ? $SlotID = $_GET['SlotID'] : die();
(isset($_GET['DayType']) && $DayType != "") ? $DayType = $_GET['DayType'] : die();

$li = new libdb();
$sql = "SELECT SlotStart, SlotEnd FROM CARD_STUDENT_ATTENDANCE_CUSTOMIZED_1_TIMESLOT WHERE SlotID = '$SlotID' AND DayType = '$DayType'";
$data = $li->returnArray($sql, 2);
$SlotStart = $data[0][0];
$SlotEnd = $data[0][1];

switch($SlotID)
{
	case 1 : 
		$status = $i_QualiEd_StudentAttendance_OnTime;
		break;
	case 2 : 
		$status = $i_QualiEd_StudentAttendance_LateType1;
		break;
	case 3 :
		$status = $i_QualiEd_StudentAttendance_LateType2;
		break;
	case 4 :
		$status = $i_QualiEd_StudentAttendance_Absent;
		break;	
}

$type = ($DayType == 2) ? $i_DayTypeAM : $i_DayTypePM;

# Get Select Hour
$temp = explode(':', $SlotStart);
$SlotStartHour = $temp[0];
$SlotStartMinute = $temp[1];
$SlotStart_select_hour = "<select name='slotstart_hour'>";
for($i=0; $i<24; $i++)
{
	$curr_hour = ($i<10) ? "0".$i : $i;
	$selected = ($SlotStartHour == $curr_hour) ? "SELECTED" : ""; 
	$SlotStart_select_hour .= "<option value=\"$curr_hour\" $selected>$curr_hour</option>";
}
$SlotStart_select_hour .= "</select>";

# Get Select Min
$SlotStart_select_minute = "<select name='slotstart_minute'>";
for($i=0; $i<60; $i++)
{
	$curr_minute = ($i<10) ? "0".$i : $i;
	$selected = ($SlotStartMinute == $curr_minute) ? "SELECTED" : ""; 
	$SlotStart_select_minute .= "<option value=\"$curr_minute\" $selected>$curr_minute</option>";
}
$SlotStart_select_minute .= "</select>";

# Get Select Hour
$temp = explode(':', $SlotEnd);
$SlotEndHour = $temp[0];
$SlotEndMinute = $temp[1];
$SlotEnd_select_hour = "<select name='slotend_hour'>";
for($i=0; $i<24; $i++)
{
	$curr_hour = ($i<10) ? "0".$i : $i;
	$selected = ($SlotEndHour == $curr_hour) ? "SELECTED" : ""; 
	$SlotEnd_select_hour .= "<option value=\"$curr_hour\" $selected>$curr_hour</option>";
}
$SlotEnd_select_hour .= "</select>";

# Get Select Min
$SlotEnd_select_minute = "<select name='slotend_minute'>";
for($i=0; $i<60; $i++)
{
	$curr_minute = ($i<10) ? "0".$i : $i;
	$selected = ($SlotEndMinute == $curr_minute) ? "SELECTED" : ""; 
	$SlotEnd_select_minute .= "<option value=\"$curr_minute\" $selected>$curr_minute</option>";
}
$SlotEnd_select_minute .= "</select>";

?>

<form name="form1" method="post" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_SmartCard_SystemSettings,'../',$i_StudentAttendance_TimeSlotSettings, 'index.php', $button_edit, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=right><?=$i_StudentAttendance_Slot?>:&nbsp</td><td><?=$type."-".$SlotID?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Slot_Start?>:&nbsp</td><td><?=$SlotStart_select_hour." : ".$SlotStart_select_minute." : 00"?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Slot_End?>:&nbsp</td><td><?=$SlotEnd_select_hour." : ".$SlotEnd_select_minute." : 59"?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Status?>:&nbsp</td><td><?=$status?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?php                
	echo btnSubmit() ." ". btnReset() . toolBarSpacer();
?>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=SlotID value="<?=$SlotID?>">
<input type=hidden name=DayType value="<?=$DayType?>">
</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>

