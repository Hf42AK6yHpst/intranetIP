<?php
/*
 * 2016-06-06 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();


$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_lesson_attendance_reasons_page_size", "numPerPage");
$arrCookies[] = array("ck_lesson_attendance_reasons_page_number", "pageNo");
$arrCookies[] = array("ck_lesson_attendance_reasons_page_order", "order");
$arrCookies[] = array("ck_lesson_attendance_reasons_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6,7))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_lesson_attendance_reasons_page_size) && $ck_lesson_attendance_reasons_page_size != "") $page_size = $ck_lesson_attendance_reasons_page_size;
$li = new libdbtable2007($field,$order,$pageNo);


$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
$reason_type_field = "CASE r.ReasonType ";
foreach($statusMap as $value){
	$reason_type_field .= "WHEN '".$value['code']."' THEN '".$value['text']."' ";
}
$reason_type_field .= "END ";

$ReasonType = trim($_POST['ReasonType']);

$modifier_name_field = getNameFieldByLang2("u.");

$sql = "SELECT 
			$reason_type_field as ReasonType,
			CONCAT('<a href=\"javascript:showModalForm(',r.ReasonID,')\">',r.ReasonText,'</a>') as ReasonText,
			r.ReasonSymbol,
			r.ModifiedDate,
			$modifier_name_field as ModifiedName,
			CONCAT('<input type=\"checkbox\" name=\"ReasonID[]\" value=\"',r.ReasonID,'\">') as CheckBox 
		FROM SUBJECT_GROUP_ATTENDANCE_REASON as r 
		LEFT JOIN INTRANET_USER as u ON r.ModifiedBy=u.UserID 
		WHERE 1 ";
if($ReasonType != ''){
	$sql .= " AND r.ReasonType='$ReasonType' ";
}

$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("ReasonType","r.ReasonText","r.ReasonSymbol","r.ModifiedDate","ModifiedName");
$li->column_array = array(22,18,22,22,22);
$li->wrap_array = array(0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['LessonAttendance']['ReasonType'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['LessonAttendance']['Reason'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['LessonAttendance']['Symbol'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("ReasonID[]")."</th>\n";
$li->no_col = $pos+2;


$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
$reason_types = array();
foreach($statusMap as $value)
{
	if($value['code'] == 0) continue;
	$reason_types[] = array(''.$value['code'],$value['text']);
}
$reason_type_selection = $linterface->GET_SELECTION_BOX($reason_types, ' id="ReasonType" name="ReasonType" onchange="document.form1.submit();" ', $Lang['General']['All'], $ReasonType, true);

$tool_buttons = array();
$tool_buttons[] = array('edit',"javascript:checkEdit2(document.form1,'ReasonID[]','editReason();')");
$tool_buttons[] = array('delete',"javascript:checkRemove2(document.form1,'ReasonID[]','deleteReason();')");

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_LessonAttendanceReasons";

### Title ###
//if(get_client_region() == 'zh_TW') {
	$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessionAttendanceSetting'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/attendance/settings/lesson", 0);
	$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonAttendanceReasons'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/attendance/settings/lesson_attendance_reasons", 1);
//} else {
//	$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonAttendanceReasons'], "", 0);
//}
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($_SESSION['LESSON_ATTENDANCE_REASON_RETURN_MSG']))
{
	$Msg = $_SESSION['LESSON_ATTENDANCE_REASON_RETURN_MSG'];
	unset($_SESSION['LESSON_ATTENDANCE_REASON_RETURN_MSG']);
}
$linterface->LAYOUT_START($Msg); 
?>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$linterface->Get_Thickbox_Link(480, 750,  'new', $Lang['Btn']['New'], 'showModalForm(0)', $InlineID="FakeLayer", $Lang['Btn']['New']);?>
		</div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		<?=$Lang['LessonAttendance']['Status'].': '.$reason_type_selection?>
	</div>
	<br style="clear:both">
    <div style="height: 15px"></div>
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<?=$li->display();?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<script type="text/javascript">
function showModalForm(id)
{
	var is_edit = id? true: false;
	
	tb_show(is_edit?'<?=$Lang['Btn']['Edit']?>':'<?=$Lang['Btn']['New']?>',"#TB_inline?height=480&width=750&inlineId=FakeLayer");
	$.post(
		'ajax.php',
		{
			'task':'getModalForm',
			'ReasonID': id 
		},
		function(returnHtml){
			$("div#TB_ajaxContent").html(returnHtml);
		}
	);
}

function editReason()
{
	var objs = document.getElementsByName('ReasonID[]');
	var selected_id = 0;
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked){
			selected_id = objs[i].value;
		}
	}
	
	showModalForm(selected_id);
}

function checkEditForm()
{
	var valid = true;
	var reason_text = $.trim($('#ReasonText').val());
	var reason_symbol = $.trim($('#ReasonSymbol').val());
	
	if(reason_text == ''){
		$('#ReasonText_Error').html('<?=$Lang['LessonAttendance']['RequestInputThisData']?>').show();
		valid = false;
	}else{
		$('#ReasonText_Error').html('').hide();
	}
	
	if(reason_symbol == ''){
		$('#ReasonSymbol_Error').html('<?=$Lang['LessonAttendance']['RequestInputThisData']?>').show();
		valid = false;
	}else{
		$('#ReasonSymbol_Error').html('').hide();
	}
	
	var upsertForm = function(){
		$('#task').val('upsertReason');
		var formData = $('#modalForm').serialize();
		$.post(
			'ajax.php',
			formData,
			function(returnCode)
			{
				tb_remove();
				window.location.reload();
			}
		);
	};
	
	var checkReasonSymbol = function(){
		Block_Thickbox();
		$('#task').val('checkReasonSymbol');
		var formData = $('#modalForm').serialize();
		$.post(
			'ajax.php',
			formData,
			function(returnData)
			{
				var count = parseInt(returnData);
				if(count <= 0){
					$('#ReasonSymbol_Error').html('').hide();
					upsertForm();
				}else{
					$('#ReasonSymbol_Error').html('<?=$Lang['LessonAttendance']['SameReasonSymbolExists']?>').show();
					UnBlock_Thickbox();
				}
			}
		);
	};
	
	var checkReasonText = function(){
		Block_Thickbox();
		$('#task').val('checkReasonText');
		var formData = $('#modalForm').serialize();
		$.post(
			'ajax.php',
			formData,
			function(returnData)
			{
				var count = parseInt(returnData);
				if(count <= 0){
					$('#ReasonText_Error').html('').hide();
					checkReasonSymbol();
				}else{
					$('#ReasonText_Error').html('<?=$Lang['LessonAttendance']['SameReasonExists']?>').show();
					UnBlock_Thickbox();
				}
			}
		);
	}
	
	if(valid){
		checkReasonText();
	}
}

function deleteReason()
{
	Block_Thickbox();
	
	var reasonIdAry = [];
	var objs = document.getElementsByName('ReasonID[]');
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked){
			reasonIdAry.push(objs[i].value);
		}
	}
	
	$.post(
		'ajax.php',
		{
			'task':'deleteReason',
			'ReasonID[]': reasonIdAry 
		},
		function(returnData)
		{
			window.location.reload();
		}
	);
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>