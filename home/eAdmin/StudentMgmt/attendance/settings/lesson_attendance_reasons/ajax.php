<?php
/*
 * 2016-06-06 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

$task = $_POST['task'];

switch($task)
{
	case 'getModalForm':
		
		$ReasonID = IntegerSafe($_POST['ReasonID']);
		$record = array();
		if($ReasonID > 0){
			$record = $LessonAttendUI->GetLessonAttendanceReasons($ReasonID);
		}
		
		$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
		$reason_types = array();
		foreach($statusMap as $value)
		{
			if($value['code'] == 0) continue;
			$reason_types[] = array($value['code'],$value['text']);
		}
		$reason_type_selection = getSelectByArray($reason_types, ' id="ReasonType" name="ReasonType"  ', $record[0]['ReasonType'], $__all=0, $__noFirst=1, $__FirstTitle="", $__ParQuoteValue=1);
		
		$x .= '<form name="modalForm" id="modalForm" action="" method="post" onsubmit="return false;">'."\n";
		$x .= '<div id="modalContent" style="overflow-y:auto;">'."\n";
		$x .= '<br />'."\n";
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">'."\n";
			$x .= '<col class="field_c">'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['LessonAttendance']['ReasonType'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $reason_type_selection;
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("ReasonType_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['LessonAttendance']['Reason'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->GET_TEXTBOX_NAME("ReasonText", "ReasonText", $record[0]['ReasonText'], $__OtherClass='', $__OtherPar=array());
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("ReasonText_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td class="field_title">'.$Lang['LessonAttendance']['Symbol'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->GET_TEXTBOX_NUMBER("ReasonSymbol", "ReasonSymbol", $record[0]['ReasonSymbol'], '', array('maxlength'=>'5'))."\n";
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("ReasonSymbol_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
		$x .= '</table>'."\n";
		if($record[0]['ReasonID'] !='' && $record[0]['ReasonID'] > 0){
			$x .= '<input type="hidden" name="ReasonID" id="ReasonID" value="'.$ReasonID.'" />'."\n";
		}
		$x .= '<input type="hidden" name="task" id="task" value="" />';
		$x .= '</div>'."\n";
		
		$x .= $linterface->MandatoryField();		
		
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkEditForm();").'&nbsp;';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","tb_remove();")."\n";
		$x .= '</div>'."\n";
		
		$x .= '</form>'."\n";
		
		$x .= '<script type="text/javascript">'."\n";
			$x .= '$(document).ready(function(){'."\n";
				//$x .= '$("#modalContent").height($("#TB_ajaxContent").height() * 0.8 + "px");'."\n";
				$x .= '$("#ReasonType").focus();'."\n";
			$x .= '});'."\n";
		$x .= '</script>'."\n";
		
		echo $x;
		
	break;
	
	case 'upsertReason':
		
		$success = $LessonAttendUI->UpsertLessonAttendanceReason($_POST);
		$msg = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		$_SESSION['LESSON_ATTENDANCE_REASON_RETURN_MSG'] = $msg;
		echo $success ? '1':'0';
		
	break;
	
	case 'checkReasonText':
	
		$ReasonType = trim($_POST['ReasonType']);
		$CheckText = trim($_POST['ReasonText']);
		$ReasonID = trim($_POST['ReasonID']);
		$exist = $LessonAttendUI->IsLessonAttendanceReasonExist($ReasonType, $CheckText, $ReasonID, 'ReasonText');
		echo $exist? '1':'0';
	
	break;
	
	case 'checkReasonSymbol':
	
		$ReasonType = trim($_POST['ReasonType']);
		$CheckText = trim($_POST['ReasonSymbol']);
		$ReasonID = trim($_POST['ReasonID']);
		$exist = $LessonAttendUI->IsLessonAttendanceReasonExist($ReasonType, $CheckText, $ReasonID, 'ReasonSymbol');
		echo $exist? '1':'0';
	
	break;
	
	case 'deleteReason':
		
		$success = $LessonAttendUI->DeleteLessonAttendanceReason($ReasonID);
		$msg = $success ? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		$_SESSION['LESSON_ATTENDANCE_REASON_RETURN_MSG'] = $msg;
		echo $success?'1':'0';
		
	break;
}



intranet_closedb();
?>