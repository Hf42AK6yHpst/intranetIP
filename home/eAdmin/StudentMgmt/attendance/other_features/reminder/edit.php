<?php

###################################################
#
#	Date:	2012-12-11	YatWoon
#			improved: allow select non-teaching staff [Case#2012-1128-1829-53071]
#
###################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Reminder";

$linterface = new interface_html();

$ReminderID = (is_array($ReminderID)? $ReminderID[0]:$ReminderID);

$li = new libdb();
$user_field = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT a.StudentID, $user_field, a.DateOfReminder, a.TeacherID, a.Reason FROM CARD_STUDENT_REMINDER as a
        LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus = 1 WHERE ReminderID = $ReminderID";
$result = $li->returnArray($sql,5);
list($studentID, $name, $remindDate, $TeacherID, $Reason) = $result[0];

$lclass = new libclass();

$namefield = getNameFieldByLang();
$sql = "SELECT UserID,$namefield,Teaching FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
$staff = $lclass->returnArray($sql);

// $select_teacher = getSelectByArray($staff,"name=\"TeacherID\"","$TeacherID",1,1);
foreach($staff as $k=>$d)
{
	$t_type = $d['Teaching'] ==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
	
	$t[$t_type][$d[0]] = $d[1];
}
$select_teacher = getSelectByAssoArray($t, "name=\"TeacherID\"", $TeacherID);

$TAGS_OBJ[] = array($i_StudentAttendance_Reminder, "", 0);

$PAGE_NAVIGATION[] = array($button_edit);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language="javascript">
<!--
function checkForm(obj){
	

		if(dateValidation(obj.RemindDate))
			return true;
		return false;
}
function dateValidation(dateObj){
	v = dateObj.value;
  	var re = new RegExp("^[1-9][0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|1[1-9]|2[1-9]|3[0-1])$");
	if (!v.match(re)) {
			alert("<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>");
			dateObj.focus();
			return false;
	}
	return true;
 }
//-->
</script>
<br />
<form name="form1" action="edit_update.php" method="POST" onsubmit="return checkForm(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
		<td width="70%" class="tabletext"><?=$name?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_Date?></td>
		<td width="70%" class="tabletext"><?=$linterface->GET_DATE_PICKER("RemindDate", $remindDate)?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_Teacher?></td>
		<td width="70%" class="tabletext"><?=$select_teacher?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_Reason?></td>
		<td width="70%" class="tabletext"><?=$linterface->GET_TEXTAREA("Reason", $Reason)?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="ReminderID" value="<?=$ReminderID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
