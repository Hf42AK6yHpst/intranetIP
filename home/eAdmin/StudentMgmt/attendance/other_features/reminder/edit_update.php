<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();
$Reason = intranet_htmlspecialchars($Reason);

$sql = "UPDATE CARD_STUDENT_REMINDER SET DateOfReminder = '$RemindDate', TeacherID = '$TeacherID'
        , Reason = '$Reason', DateModified = now()
        WHERE ReminderID = $ReminderID";

if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['ReminderRecordUpdateSuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['ReminderRecordUpdateFail'];
}
header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
