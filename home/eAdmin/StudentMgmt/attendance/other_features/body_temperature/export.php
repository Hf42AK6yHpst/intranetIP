<?php

/*
 * 2018-01-29 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['RecordBodyTemperature']) {
	intranet_closedb();
	//header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$lexport = new libexporttext();

$field_array = array("r.RecordDate","StudentClassName","StudentClassNumber","StudentName","r.TemperatureValue","r.DateInput");

$export_column = array($Lang['General']['RecordDate'], $Lang['StudentAttendance']['ClassName'], $Lang['StudentAttendance']['ClassNumber'], $Lang['StudentAttendance']['StudentName'], $Lang['StudentAttendance']['BodyTemperature'], $Lang['General']['DateInput']);

$name_field = Get_Lang_Selection("u.ChineseName","u.EnglishName");
$archived_name_field = Get_Lang_Selection("au.ChineseName","au.EnglishName");
$cond = "";

if(isset($StartDate) && $StartDate!=''){
	$cond .= " AND r.RecordDate >= '".$StartDate."' ";
}
if(isset($EndDate) && $EndDate!=''){
	$cond .= " AND r.RecordDate <= '".$EndDate."' ";
}

if(isset($Keyword) && trim($Keyword) != ''){
	$keyword = trim( $lc->is_magic_quotes_active ? stripslashes($Keyword): $Keyword );
	$keyword = $lc->Get_Safe_Sql_Like_Query($keyword);
	$cond .= " AND ($name_field LIKE '%$keyword%' OR $archived_name_field LIKE '%$keyword%') ";
}

if($field == '' || !in_array($field,array(0,1,2,3,4,5))){
	$field = 0;
}
if($order == '' || !in_array($order,array(0,1))){
	$order = 0;
}

$sql = "SELECT 
			r.RecordDate,
			IF(u.UserID IS NOT NULL,u.ClassName,au.ClassName) as StudentClassName,
			IF(u.UserID IS NOT NULL,u.ClassNumber,au.ClassNumber) as StudentClassNumber,
			IF(u.UserID IS NOT NULL,$name_field,CONCAT('*',$archived_name_field)) as StudentName,
			CONCAT(r.TemperatureValue,' ℃ ') as TemperatureValue,
			r.DateInput 
		FROM 
		CARD_STUDENT_BODY_TEMPERATURE_RECORD as r 
		LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID AND u.RecordType='".USERTYPE_STUDENT."' 
		LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID AND au.RecordType='".USERTYPE_STUDENT."' 
		WHERE 1 $cond ";
$sql .= " ORDER BY ".$field_array[$field]." ".($order == 1?" ASC ":" DESC ");
$rows = $lc->returnArray($sql);

$export_content = $lexport->GET_EXPORT_TXT($rows, $export_column);

// Output the file to user browser
$filename = "body_temperature_records.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
exit;
?>