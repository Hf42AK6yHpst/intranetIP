<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$ClassName = trim(urldecode(stripslashes($_REQUEST['ClassName'])));

echo $lclass->getStudentSelectByClass($ClassName,"name=StudentID");

intranet_closedb();
?>