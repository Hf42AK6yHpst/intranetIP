<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();
$lc = new libcardstudentattend2();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Outing";
$linterface = new interface_html();

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StudentAttendUI->Get_Preset_Outing_Import_Finish_Page();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>