<?
// Editing by 
/*
 * !!!! Deprecated, use update.php instead !!!!
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();
$StudentID = Get_User_Array_From_Common_Choose($_REQUEST['SelectedUserIDArr'],array(2));
$Location = intranet_htmlspecialchars($Location);
$Remark = intranet_htmlspecialchars($Remark);
$Objective = intranet_htmlspecialchars($Objective);
$PIC = intranet_htmlspecialchars($PIC);
$OutingDate = (array)$OutingDate;

$outTimeStr = ($OutTime ==""? "NULL":"'$OutTime'");
$backTimeStr = ($BackTime ==""? "NULL":"'$BackTime'");

$li->Start_Trans();
for ($i=0; $i< sizeof($StudentID); $i++) {
	for($j=0;$j<sizeof($OutingDate);$j++){
		$sql = "INSERT INTO CARD_STUDENT_OUTING (
							UserID, 
							RecordDate,
							OutTime,
							BackTime,
							Location,
							FromWhere,
							Objective,
							Detail,
							PIC,
							DateInput,
							DateModified)
		        VALUES (
		        	'".$StudentID[$i]."',
		        	'".$OutingDate[$j]."',
		        	$outTimeStr,
		        	$backTimeStr,
		        	'".$li->Get_Safe_Sql_Query($Location)."',
		        	'".$li->Get_Safe_Sql_Query($FromWhere)."',
		        	'".$li->Get_Safe_Sql_Query($Objective)."',
		        	'".$li->Get_Safe_Sql_Query($Remark)."',
		        	'".$li->Get_Safe_Sql_Query($PIC)."',
		        	now(),
		        	now())";
		$Result['insertOutingRecordFor:'.$StudentID[$i].':'.$OutingDate[$j]] = $li->db_db_query($sql);
	}
}

if(!in_array(false,$Result)) {
	$li->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['OutingRecordCreateSuccess'];
}
else {
	$li->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['OutingRecordCreateFail'];
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
