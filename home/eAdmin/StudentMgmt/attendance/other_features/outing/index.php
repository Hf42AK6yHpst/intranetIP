<?php
## Using By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_datamgmt_features_outing_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_datamgmt_features_outing_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_datamgmt_features_outing_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_datamgmt_features_outing_page_number!="")
{
	$pageNo = $ck_attend_datamgmt_features_outing_page_number;
}

if ($ck_attend_datamgmt_features_outing_page_order!=$order && $order!="")
{
	setcookie("ck_attend_datamgmt_features_outing_page_order", $order, 0, "", "", 0);
	$ck_attend_datamgmt_features_outing_page_order = $order;
} else if (!isset($order) && $ck_attend_datamgmt_features_outing_page_order!="")
{
	$order = $ck_attend_datamgmt_features_outing_page_order;
}

if ($ck_attend_datamgmt_features_outing_page_field!=$field && $field!="")
{
	setcookie("ck_attend_datamgmt_features_outing_page_field", $field, 0, "", "", 0);
	$ck_attend_datamgmt_features_outing_page_field = $field;
} else if (!isset($field) && $ck_attend_datamgmt_features_outing_page_field!="")
{
	$field = $ck_attend_datamgmt_features_outing_page_field;
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Outing";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

if (isset($targetID) && $targetID != "")
{
    $conds = "AND a.UserID = $targetID";
}
//---Henry Added 20131105 [start]
if($FromDate != "" && $ToDate !="" && $chk_date){
	$conds .="AND a.RecordDate >= '$FromDate' AND a.RecordDate <= '$ToDate'";
}
//---Henry Added 20131105 [end]
$user_field = getNameFieldWithClassNumberByLang("b.");

$sql  = "SELECT
           a.RecordDate, 
           $user_field,
           CASE 
           	WHEN a.DayType = '".PROFILE_DAY_TYPE_AM."' THEN '".$Lang['StudentAttendance']['DayTypeAM']."' 
           	WHEN a.DayType = '".PROFILE_DAY_TYPE_PM."' THEN '".$Lang['StudentAttendance']['DayTypePM']."' 
           	ELSE '".$Lang['StudentAttendance']['DayTypeWD']."' 
           END as Session,
           IF(a.OutTime IS NULL OR a.OutTime = '00:00:00' OR a.OutTime = '','$i_SmartCard_StudentOutingNoOut',a.OutTime),
           IF(a.BackTime IS NULL OR a.BackTime = '00:00:00' OR a.BackTime = '','$i_SmartCard_StudentOutingNoBack',a.BackTime),
           a.PIC,
           a.Objective,
           CONCAT('<input type=checkbox name=OutingID[] value=', a.OutingID ,'>')
         FROM
           CARD_STUDENT_OUTING as a ";
if ($lc->EnableEntryLeavePeriod) {
	$sql .= "
					INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.UserID = selp.UserID 
          	AND 
          	a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
}
$sql .= "
					LEFT OUTER JOIN 
          INTRANET_USER as b ON a.UserID = b.UserID
        WHERE
          (b.ChineseName like '%$keyword%'
           OR b.EnglishName like '%$keyword%'
           OR b.ClassName like '%$keyword%'
           OR a.PIC LIKE '%$keyword%'
           OR a.Objective LIKE '%$keyword%'
           OR a.RecordDate = '$keyword')
           $conds
            ";
//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.RecordDate",$user_field,"Session","a.OutTime","a.BackTime","a.PIC","a.Objective");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_SmartCard_StudentOutingDate)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $Lang['StudentAttendance']['DayType'])."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_SmartCard_StudentOutingOutTime)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_SmartCard_StudentOutingBackTime)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_SmartCard_StudentOutingPIC)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_SmartCard_StudentOutingReason)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("OutingID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'OutingID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'OutingID[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");
//---Henry Added 2013105 [start]
$searchbar .= " ";
$searchbar .= $linterface->Get_Checkbox("chk_date", "chk_date", 1, $chk_date,'','','chkDateClick(this)');
$searchbar .= " ";
$searchbar .= $Lang['General']['From'];
$searchbar .= " ";
$searchbar .= $linterface->GET_DATE_PICKER("FromDate", $FromDate,'',"yy-mm-dd","","","","");
$searchbar .= " ";
$searchbar .= $Lang['General']['To'];
$searchbar .= " ";
$searchbar .= $linterface->GET_DATE_PICKER("ToDate", $ToDate,'',"yy-mm-dd","","","","");
$searchbar .= " ";
$searchbar .= $linterface->GET_SMALL_BTN($button_apply, "button", "checkDate()","submitDate",($chk_date?'':'disabled'));
//---Henry Added 2013105 [end]
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("95%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<SCRIPT LANGUAGE="Javascript">
function chkDateClick(obj){
	if(obj.checked)
		document.getElementById('submitDate').disabled = false;
	else
		document.getElementById('submitDate').disabled = true;
}
function checkDate(){
	if(document.getElementById('chk_date').checked){
		if(document.getElementById('FromDate').value > document.getElementById('ToDate').value){
			alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
			return;
		}
	}
	document.form1.submit();
}
</SCRIPT>
