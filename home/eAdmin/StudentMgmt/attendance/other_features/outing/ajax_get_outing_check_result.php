<?php
// Editing by 
/*
 * 2015-01-28 (Carlos): Changed $OutingDates as array type, check multiple dates duplications. 
 * 2013-12-17 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$TargetUserID = IntegerSafe($_REQUEST['TargetUserID']); // array
$RecordDate = (array)$_REQUEST['RecordDate']; // array 
$DayType = $_REQUEST['DayType']; // array

$day_types = "'".implode("','",(array)$DayType)."'";

$NameField = getNameFieldWithClassNumberByLang("u.");
$sql = "SELECT a.UserID,$NameField as 'StudentName',a.RecordDate,a.DayType,a.OutTime,a.BackTime,a.Location,a.FromWhere,a.Objective,a.PIC,a.Detail 
		FROM CARD_STUDENT_OUTING as a 
		INNER JOIN INTRANET_USER as u ON u.UserID=a.UserID 
		WHERE a.RecordDate IN ('".implode("','",$RecordDate)."') AND a.DayType IN ($day_types) AND a.UserID IN ('".implode("','",(array)$TargetUserID)."')
		ORDER BY a.RecordDate,StudentName,a.DayType";
$records = $lc->returnResultSet($sql);
$record_count = count($records);

if($record_count > 0){
	$linterface = new interface_html();
	$x = '<table class="common_table_list_v30">';
		$x.= '<thead>';
			$x.= '<tr><th class="num_check">#</th>';
			$x.= '<th style="width:20%">'.$Lang['General']['RecordDate'].'</th>';
			$x.= '<th style="width:20%">'.$i_UserStudentName.'</th>';
			$x.= '<th style="width:10%">'.$Lang['StudentAttendance']['DayType'].'</th>';
			$x.= '<th style="width:10%">'.$i_SmartCard_StudentOutingOutTime.'</th>';
			$x.= '<th style="width:10%">'.$i_SmartCard_StudentOutingBackTime.'</th>';
			$x.= '<th style="width:10%">'.$i_SmartCard_StudentOutingPIC.'</th>';
			$x.= '<th style="width:20%">'.$i_SmartCard_StudentOutingReason.'</th>';
			$x.= '</tr>';
		$x.= '</thead>';
		$x.= '<tbody>';
		for($i=0;$i<$record_count;$i++) {
			$x .= '<tr>';
				$x .= '<td>'.($i+1).'</td>';
				$x .= '<td>'.$records[$i]['RecordDate'].'</td>';
				$x .= '<td>'.$records[$i]['StudentName'].'</td>';
				$x .= '<td>'.($records[$i]['DayType']==PROFILE_DAY_TYPE_AM? $Lang['StudentAttendance']['DayTypeAM']:$Lang['StudentAttendance']['DayTypePM']).'</td>';
				$x .= '<td>'.$records[$i]['OutTime'].'&nbsp;</td>';
				$x .= '<td>'.$records[$i]['BackTime'].'&nbsp;</td>';
				$x .= '<td>'.$records[$i]['PIC'].'&nbsp;</td>';
				$x .= '<td>'.$records[$i]['Objective'].'&nbsp;</td>';
			$x .= '</tr>';
		}
		$x.= '</tbody>';
	$x.= '</table>';
	
	$warning_box = $linterface->Get_Warning_Message_Box('',$x,'');
	
	echo $warning_box;
}

intranet_closedb();
?>