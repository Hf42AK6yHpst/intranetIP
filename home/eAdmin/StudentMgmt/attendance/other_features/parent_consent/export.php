<?php
// Editing by 
/*
 * 2017-07-07 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lexport = new libexporttext();
$lc = new libcardstudentattend2();

$header_map = $lc->getHKUSPHParentConsentCsvHeaderMap();
$header = array_keys($header_map);
$rows = array();

$filterMap = array();
$AcademicYearID = Get_Current_Academic_Year_ID();
$filterMap['AcademicYearID'] = $AcademicYearID;
if(isset($TargetClass) && $TargetClass != '' && $TargetClass != '0'){
	if(strpos($TargetClass,'::')!==false){
		$filterMap['YearClassID'] = str_replace('::','', $TargetClass);
	}else{
		$filterMap['YearID'] = $TargetClass;
	}
}
if(isset($ConsentStatus) && $ConsentStatus != ''){
	$filterMap['ConsentStatus'] = $ConsentStatus;
}
if(isset($Keyword) && trim($Keyword)!=''){
	$filterMap['Keyword'] = $Keyword;
}
$filterMap['CustomSort'] = 1;
$filterMap['SortOrder'] = $order;
$filterMap['SortField'] = $field;
$records = $lc->getHKUSPHParentConsentRecords($filterMap);
$record_count = count($records);
for($i=0;$i<$record_count;$i++){
	$row = array();
	foreach($header_map as $key => $field){
		$row[] = $records[$i][$field];
	}
	$rows[] = $row;
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $header);	

intranet_closedb();
$filename = "HKUSPH_parent_consent.csv";
$lexport->EXPORT_FILE($filename, $export_content);
exit;
?>