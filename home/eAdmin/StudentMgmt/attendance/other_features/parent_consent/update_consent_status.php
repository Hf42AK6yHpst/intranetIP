<?php
// Editing by 
/*
 * 2017-07-07 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH'] || !in_array($_REQUEST['agree'],array('1','0','-1')) || !isset($_POST['RecordID'])) {
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	header("Location:index.php");
	exit();
}

$lc = new libcardstudentattend2();

$agree = $_REQUEST['agree'];
$recordIdAry = $_POST['RecordID'];
$record_count = count($recordIdAry);

$resultAry = array();
for($i=0;$i<$record_count;$i++){
	$record = $recordIdAry[$i];
	$ids = explode(",",$record);
	$parent_id = $ids[0];
	$student_id = $ids[1];
	if($parent_id == '' || $student_id == ''){
		continue;
	}
	if($agree == '-1'){
		$sql = "DELETE FROM HKUSPH_AGREEMENT WHERE StudentID='$student_id' AND ParentID='$parent_id'";
		$resultAry[] = $lc->db_db_query($sql);
		continue;
	}
	$sql = "UPDATE HKUSPH_AGREEMENT SET IsAgreed='$agree',SignDate=NOW() WHERE StudentID='$student_id' AND ParentID='$parent_id'";
	$update_success = $lc->db_db_query($sql);
	$resultAry[] = $update_success;
	$affected_rows = $lc->db_affected_rows();
	if($affected_rows == 0){
		$sql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID='$parent_id'";
		$name_vector = $lc->returnVector($sql);
		$sql = "INSERT INTO HKUSPH_AGREEMENT (StudentID,ParentID,IsAgreed,ParentName,SignDate,Status) VALUES ('$student_id','$parent_id','$agree','".$lc->Get_Safe_Sql_Query($name_vector[0])."',NOW(),'a')";
		$insert_success = $lc->db_db_query($sql);
		$resultAry[] = $insert_success;
	}
}

$_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'] = !in_array(false,$resultAry)? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
intranet_closedb();
header("Location:index.php");
exit();
?>