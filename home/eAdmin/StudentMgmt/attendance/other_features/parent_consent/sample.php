<?php
// Editing by 
/*
 * 2017-07-07 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lexport = new libexporttext();
$lc = new libcardstudentattend2();

$header_map = $lc->getHKUSPHParentConsentCsvHeaderMap($type);
$header = array_keys($header_map);
$rows = array();
if($type == 2){
	$rows[] = array('6A','1','Chan Siu Man','同意','陳大文','陳大文','2016-09-02 09:25:00');
	$rows[] = array('5A','2','Lee Ho Ho','不同意','John Lee','John Lee','2016-10-03 18:30:00');
	$rows[] = array('4A','3','Wong Kai','同意','Amy Wong','Amy Wong','2017-03-15 14:00:00');
}else{
	$rows[] = array('p00001','Chan Tai Man','s00001','6A','1','Chan Siu Man','1','2016-09-02 09:25:00');
	$rows[] = array('p00002','John Lee','s00002','5A','2','Lee Ho Ho','0','2016-10-03 18:30:00');
	$rows[] = array('p00003','Amy Wong','s00003','4A','3','Wong Kai','1','2017-03-15 14:00:00');
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $header);	

intranet_closedb();
$filename = "HKUSPH_parent_consent_sample.csv";
$lexport->EXPORT_FILE($filename, $export_content);
exit;
?>