<div style="padding:8px;border:2px solid #000000;font-size:1em;font-family:arial,serif;">
	<div style="margin-bottom:15px;">Please be informed that <!--STUDENT_NAME--> (<!--CLASS_NAME-->-<!--CLASS_NUMBER-->) has been recorded (late/absent) in eAttendance, details are as below: </div>
	<table cellpadding="6px;" style="width:100%;border-collapse:collapse;border:1px solid #000000;">
		<tbody>
			<tr>
				<td style="width:50%;border:1px solid #000000;">Event Date</td><td style="width:50%;border:1px solid #000000;"><!--EVENT_DATE--></td>
			</tr>
			<tr>
				<td style="width:50%;border:1px solid #000000;">Type</td><td style="width:50%;border:1px solid #000000;">Negative</td>
			</tr>
			<tr>
				<td style="width:50%;border:1px solid #000000;">Record</td><td style="width:50%;border:1px solid #000000;"><!--RECORD--></td>
			</tr>
			<tr>
				<td style="width:50%;border:1px solid #000000;">Student</td><td style="width:50%;border:1px solid #000000;"><!--STUDENT_NAME--> (<!--CLASS_NAME-->-<!--CLASS_NUMBER-->)</td>
			</tr>
			<tr>
				<td style="width:50%;border:1px solid #000000;">Statistics</td><td style="width:50%;border:1px solid #000000;">Lateness: <!--LATE_COUNT--> &nbsp;&nbsp;&nbsp;&nbsp;Absence: <!--ABSENT_COUNT--></td>
			</tr>
		</tbody>
	</table>
</div>