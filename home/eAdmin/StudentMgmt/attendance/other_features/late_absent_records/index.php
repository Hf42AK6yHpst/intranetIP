<?php
// Editing by 
/*
 * 2017-11-14 (Carlos): $sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_LateAbsentRecords";
$linterface = new interface_html();

if(!isset($AcademicYearID) || $AcademicYearID==''){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}

$fcm = new form_class_manage();
$academic_years = $fcm->Get_Academic_Year_List();
$academic_year_selection_data = array();
$academic_year_namefield = Get_Lang_Selection("YearNameB5","YearNameEN");
for($i=0;$i<count($academic_years);$i++){
	$academic_year_selection_data[] = array($academic_years[$i]['AcademicYearID'],$academic_years[$i][$academic_year_namefield]);
}
$academic_year_selection = $linterface->GET_SELECTION_BOX($academic_year_selection_data, ' name="AcademicYearID" id="AcademicYearID" onchange="document.form1.submit();" ', '', $AcademicYearID, $CheckType=false);

$year_terms = $fcm->Get_Academic_Year_Term_List($AcademicYearID);
$year_term_selection_data = array();
$year_term_selection_data[] = array('', $Lang['SysMgr']['Homework']['AllYearTerms']);
$year_term_namefield = Get_Lang_Selection("YearTermNameB5","YearTermNameEN");
for($i=0;$i<count($year_terms);$i++){
	$year_term_selection_data[] = array($year_terms[$i]['YearTermID'],$year_terms[$i][$year_term_namefield]);
}
$year_term_selection = $linterface->GET_SELECTION_BOX($year_term_selection_data, ' name="YearTermID" id="YearTermID" onchange="document.form1.submit();" ', '', $YearTermID, $CheckType=false);

$year_classes = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $___YearID='', $___TeachingOnly=0, $___YearClassIDArr='');
$year_class_selection_data = array();
$year_class_namefield = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
for($i=0;$i<count($year_classes);$i++){
	$year_class_selection_data[] = array($year_classes[$i]['YearClassID'],$year_classes[$i][$year_class_namefield],$year_classes[$i]['YearName']);
}
$year_class_selection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($year_class_selection_data, ' name="YearClassID" id="YearClassID" onchange="document.form1.submit();" ', $Lang['SysMgr']['FormClassMapping']['AllClass'], $YearClassID);

if(!isset($LateCount)){
	$LateCount = 0;
}
if(!isset($AbsentCount)){
	$AbsentCount = 0;
}
$late_count_filter = str_replace('<!--NUMBER-->',$linterface->GET_TEXTBOX_NUMBER("LateCount", "LateCount", $LateCount, $___OtherClass='', $___OtherPar=array('onkeyup'=>'GoSearch(event);')),$Lang['StudentAttendance']['MoreThanNumberOfLate']);
$absent_count_filter = str_replace('<!--NUMBER-->',$linterface->GET_TEXTBOX_NUMBER("AbsentCount", "AbsentCount", $AbsentCount, $___OtherClass='', $___OtherPar=array('onkeyup'=>'GoSearch(event);')),$Lang['StudentAttendance']['MoreThanNumberOfAbsence']);

if(!isset($AndOr) || !in_array($AndOr,array('OR','AND'))){
	$AndOr = 'OR';
}
$and_or_selection_data = array();
$and_or_selection_data[] = array('OR',$Lang['StudentAttendance']['Or']);
$and_or_selection_data[] = array('AND',$Lang['StudentAttendance']['And']);
$and_or_selection = $linterface->GET_SELECTION_BOX($and_or_selection_data,' name="AndOr" id="AndOr" onchange="document.form1.submit();" ','',$AndOr);


$date_filter = $linterface->Get_Checkbox("ByDateRange", "ByDateRange", "1", $ByDateRange==1, $___Class='', $Lang['StudentAttendance']['ByDateRange'].'?', 'this.checked?$(\'#DateRangeContainer\').show():$(\'#DateRangeContainer\').hide();', $___Disabled='');
$date_filter.= '&nbsp;<span id="DateRangeContainer"'.($ByDateRange?'':' style="display:none;"').'>';
$date_filter.= $Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate",$StartDate,$___OtherMember="",$___DateFormat="yy-mm-dd",$___MaskFunction="",$___ExtWarningLayerID="",$___ExtWarningLayerContainer="",$___OnDatePickSelectedFunction="");
$date_filter.= $Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate",$EndDate,$___OtherMember="",$___DateFormat="yy-mm-dd",$___MaskFunction="",$___ExtWarningLayerID="",$___ExtWarningLayerContainer="",$___OnDatePickSelectedFunction="");
$date_filter.= $linterface->GET_SMALL_BTN($Lang['Btn']['Apply'], "button", $___ParOnClick="document.form1.submit();", "ApplyBtn", $___ParOtherAttribute="", $___OtherClass="", $___ParTitle='');
$date_filter.= '</span>';


$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_student_attendance_late_absent_records_page_size", "numPerPage");
$arrCookies[] = array("ck_student_attendance_late_absent_records_page_number", "pageNo");
$arrCookies[] = array("ck_student_attendance_late_absent_records_page_order", "order");
$arrCookies[] = array("ck_student_attendance_late_absent_records_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 1;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_student_attendance_late_absent_records_page_size) && $ck_student_attendance_late_absent_records_page_size != "") $page_size = $ck_student_attendance_late_absent_records_page_size;
$li = new libdbtable2007($field,$order,$pageNo);


$params = array('GetDBQuery'=>1,'libdbtable'=>$li,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
if($ByDateRange && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate) && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$EndDate)){
	$params['StartDate'] = $StartDate;
	$params['EndDate'] = $EndDate;
}
$db_query_info = $lcardstudentattend2->getStudentLateAbsentCountingRecords($params);

$li->field_array = $db_query_info[0];
$li->sql = $db_query_info[1];
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = $db_query_info[3];
$li->wrap_array = $db_query_info[4];
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 0;
}

foreach($db_query_info[2] as $column_def)
{
	$li->column_list .= $column_def;
}


$tool_buttons = array();
$tool_buttons[] = array('email',"javascript:checkEditMultiple2(document.form1,'StudentID[]','EmailToClassTeachers();')",$Lang['StudentAttendance']['EmailToClassTeachers']);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['LateAbsentRecords'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENT_ATTENDANCE_LATE_ABSENT_RECORDS_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_LATE_ABSENT_RECORDS_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_LATE_ABSENT_RECORDS_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<br />
<form id="form1" name="form1" method="post" action="">
<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("Keyword", trim($lc->is_magic_quotes_active? stripslashes($Keyword): $Keyword), ' onkeyup="GoSearch(event);" ')?>
	<br style="clear:both" />
</div>
<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<?=$academic_year_selection.$year_term_selection.$year_class_selection.'&nbsp;|&nbsp;'.$late_count_filter.'&nbsp;'.$and_or_selection.'&nbsp;'.$absent_count_filter.'&nbsp;|&nbsp;'.$date_filter?>
	</td>
</tr>
<tr>
    <td valign="bottom">
        <?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
    </td>
</tr>
</table>
<?=$li->display()?>
<br style="clear:both" />
<p class="tabletextremark">(<?=str_replace('<!--LATE_TIME-->',$lcardstudentattend2->HKUGAC_LateTime,$Lang['StudentAttendance']['HKUGACLateAbsentRecordsNote'])?>)</p>
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</div>
<div id="InfoLayer" class="selectbox_layer selectbox_layer_show_info" style="visibility:visible;display:none;">
	<div style="float:right">
		<a href="javascript:void(0);" onclick="$('#InfoLayer').hide('fast');">
			<img border="0" src="/images/<?=$LAYOUT_SKIN?>/ecomm/btn_mini_off.gif">
		</a>
	</div>
	<p class="spacer"></p>
	<div class="content" style="min-width:200px;max-height:512px;overflow-y:auto;"></div>
</div>
</form>
<script type="text/javascript" language="javascript">
var cancel_sending_email = false;
var last_clicked_btn = null;
function displayInfoLayer(btnElement,layerId,selectedStudentId,recordType)
{
	var loading_img = '<?=$linterface->Get_Ajax_Loading_Image()?>';
	var btnObj = $(btnElement);
	var layerObj = $('#'+layerId);
	var posX = btnObj.offset().left;
	var posY = btnObj.offset().top + btnObj.height();
	
	layerObj.hide('fast');
	if(layerObj.is(':visible') && btnElement == last_clicked_btn){
		last_clicked_btn = btnElement;
		return;
	}
	last_clicked_btn = btnElement;
	$('#'+layerId+' .content').html(loading_img);
	layerObj.css({'position':'absolute','top':posY+'px','left':posX+'px'});
	layerObj.show('fast');
	var formData = $('#form1').serialize();
	formData += '&task=getDetailRecords&SelectedStudentID='+selectedStudentId+'&RecordType='+recordType
	$.post(
		'ajax.php',
		formData,
		function(returnHTML){
			$('#'+layerId+' .content').html(returnHTML);
		}
	);
}

function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function EmailToClassTeachers()
{
	cancel_sending_email = false;
	var loading_img = '<?=$linterface->Get_Ajax_Loading_Image()?>';
	tb_show('<?=$Lang['StudentAttendance']['EmailToClassTeachers']?>',"#TB_inline?height=600&width=720&inlineId=FakeLayer");
	
	var student_id_objs = document.getElementsByName('StudentID[]');
	var selected_student_ids = [];
	for(var i=0;i<student_id_objs.length;i++){
		if(student_id_objs[i].checked){
			selected_student_ids.push(student_id_objs[i].value);
		}
	}
	
	$("div#TB_ajaxContent").html(loading_img);
	
	var send_email = function(){
		if(selected_student_ids.length == 0 || cancel_sending_email){
			$('#Spinner').hide();
			$('#CancelBtn').hide();
			//setTimeout(function(){tb_remove();},3000);
			return;
		}
		var student_id = selected_student_ids.shift();
		var formData = $('#form1').serialize();
		formData += '&task=sendEmail&TargetStudentID=' + student_id;
		$.post(
			'ajax.php',
			formData,
			function(result){
				var sent_count = parseInt( $('#SentCount').text() );
				sent_count += 1;
				$('#SentCount').html(sent_count);
				if(result == '1'){
					$('#SendStatus_'+student_id).html('<span style="color:green"><?=$Lang['StudentAttendance']['Success']?></span>');
				}else{
					$('#SendStatus_'+student_id).html('<span style="color:red"><?=$Lang['StudentAttendance']['Failed']?></span>');
				}
				send_email();
			}
		);
	}
	
	var formData = $('#form1').serialize();
	formData += '&task=getStudentEmailTable';
	$.post(
		'ajax.php',
		formData,
		function(returnHTML){
			$("div#TB_ajaxContent").html(returnHTML);
			send_email();
		}
	);
}

function CancelSending()
{
	cancel_sending_email = true;
	$('#CancelBtn').hide();
	$('#FeedbackContainer').html('<?=$Lang['StudentAttendance']['SendingWasCancelled']?>').show();
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>