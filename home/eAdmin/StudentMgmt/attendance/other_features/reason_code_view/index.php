<?php
// Using : 

################################################
#	Date:	2019-08-15  Carlos
#			Hide [Absent Teacher's remarks] tag as it has been migrated to [Preset Teacher's Remarks].
#
#	Date:	2016-08-26	Bill
#			Added Absent Teacher's remarks Management tag
#
#	Date:	2011-05-27	YatWoon
#			Fixed: Add $plugin['WebSAMS'] checking, if no WebSAMS plugin, then will redirect to "Outing Reason Management"
#
################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (!isset($plugin['WebSAMS']) || !$plugin['WebSAMS'])
{
	header("Location: ../outing_reason_manage/");
	exit;
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_ViewWebSAMSReasonCode";

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field == "") $field = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
# TABLE INFO

$sql = "SELECT 
					IF(RecordType=1, CONCAT(CodeID,'<font color=red>*</font>'),CodeID), 
					ReasonText,
					CASE 
						WHEN ReasonType = 1 THEN '".$i_Profile_Absent."' 
						WHEN ReasonType = 2 THEN '".$i_Profile_Late."' 
						WHEN ReasonType = 3 THEN '".$i_Profile_EarlyLeave."' 
						ELSE '&nbsp' 
					END 
 			 	FROM 
 			 		INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE 
 			 	WHERE 
 			 		(ReasonText LIKE '%$keyword%' OR CodeID LIKE '%$keyword%')
 	   ";
$sql .= ($record_type =="")?"":" AND ReasonType='$record_type' ";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CodeID","ReasonText","ReasonType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=5 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_WebSAMS_Attendance_Reason_ReasonCode)."</td>\n";
$li->column_list .= "<td width=55% class=tableTitle>".$li->column(1, $i_WebSAMS_Attendance_Reason_ReasonText)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $i_WebSAMS_Attendance_Reason_ReasonType)."</td>\n";

// TABLE FUNCTION BAR
$searchbar  = "<select name=record_type onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value='' ".(($record_type=='')?"selected":"").">$i_status_all</option>\n";
$searchbar .= "<option value=1 ".(($record_type==1)?"selected":"").">$i_Profile_Absent</option>\n";
$searchbar .= "<option value=2 ".(($record_type==2)?"selected":"").">$i_Profile_Late</option>\n";
$searchbar .= "<option value=3 ".(($record_type==3)?"selected":"").">$i_Profile_EarlyLeave</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=textboxnum type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");
//$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ViewWebsamsReasonCode'],"", 1);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['OutingReasonManage'],"../outing_reason_manage/", 0);
//$TAGS_OBJ[] = array($Lang['StudentAttendance']['AbsentReasonManage'],"../absent_reason/", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));
?>
 

<form name="form1" method="get">
<br>
<table width="96%" border="0" cellpadding="15" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">&nbsp;</td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="padding-left: 15px">
    <tr>
        <td><?=$li->display("96%"); ?></td>
    </tr>
</table>

<table width="96%" cellspacing="0" cellpadding="0" border="0" align="center">
	<tbody><tr>
		<td align="left">
			<span class="tabletextremark">
				<?=$Lang['StudentAttendance']['ViewWEBSAMSReasonRemark']?><br>
				<font color=red>*</font> - <?=$i_WebSAMS_Attendance_Reason_OtherReason_Notice?>
			</span>
		</td>
	</tr>
</tbody></table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>