<?php
// Using : 

################################################
#	Date:	2019-08-15  Carlos
#			Hide [Absent Teacher's remarks] tag as it has been migrated to [Preset Teacher's Remarks].
#
#	Date:	2016-08-26	Bill
#			Added Absent Teacher's remarks Management tag
#
#	Date:	2011-05-27	YatWoon
#			Add $plugin['WebSAMS'] checking for TAGS_OBJ
#
################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_ViewWebSAMSReasonCode";

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field == "") $field = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
# TABLE INFO

$sql = "SELECT 
					ReasonText, 
					CONCAT('<div class=\"table_row_tool\"> 
						<a title=\"".$Lang['Btn']['Edit']."\" href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" onclick=\"Get_Reason_Form(\\'',RecordID,'\\');\" class=\"edit_dim thickbox\" ></a>
						<a title=\"".$Lang['Btn']['Delete']."\" class=\"delete_dim\" href=\"#\" onclick=\"Delete_Reason(\\'',RecordID,'\\');\"></a>
					</div>') as ContentTools
 			 	FROM 
 			 		CARD_STUDENT_OUTING_REASON  
 			 	WHERE 
 			 		ReasonText LIKE '%$keyword%' 
 	   ";
//debug_r($sql);
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("ReasonText");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0);
$li->wrap_array = array(0);	
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=5 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=75% class=tableTitle>".$li->column(0, $i_WebSAMS_Attendance_Reason_ReasonText)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>
											<div class=\"table_row_tool\"> 
												<a title=\"".$Lang['Btn']['Add']."\" href=\"#TB_inline?height=450&width=750&inlineId=FakeLayer\" onclick=\"Get_Reason_Form();\" class=\"add thickbox\" ></a>
											</div>
										 </td>\n";

// TABLE FUNCTION BAR
$searchbar .= "<input class=textboxnum type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");
//$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

if (isset($plugin['WebSAMS']) && $plugin['WebSAMS'])
{
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ViewWebsamsReasonCode'],"../reason_code_view/", 0);
}
$TAGS_OBJ[] = array($Lang['StudentAttendance']['OutingReasonManage'],"", 1);
//$TAGS_OBJ[] = array($Lang['StudentAttendance']['AbsentReasonManage'],"../absent_reason/", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

echo $StudentAttendUI->Include_JS_CSS();
?>
 

<form name="form1" id="form1" method="get">
<br>
<table width="96%" border="0" cellpadding="15" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">&nbsp;</td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="padding-left: 15px">
    <tr>
        <td><?=$li->display("96%"); ?></td>
    </tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="Msg" id="Msg" value="">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// Ajax function 
{
function Get_Reason_Form(RecordID) {
	var RecordID = RecordID || "";
	var PostVar = {
		"RecordID":RecordID
	};
	$.post("ajax_get_reason_form.php",PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				$('div#TB_ajaxContent').html(data);
			}
		}
	);
}

function Check_Reason(GoSubmit) {
	var GoSubmit = GoSubmit	|| false;
	var PostVar = {
		"ReasonText":$('#ReasonText').val(),
		"RecordID":$('#RecordID').val()
	};
	
	$.post("ajax_check_reason.php",PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = "/";
			else if (data == "0") {
				$("#ReasonTextWarningRow").css('display','none');
				$("#ReasonTextWarningLayer").html('');
				if (GoSubmit) Save_Reason();
			}
			else {
				$("#ReasonTextWarningRow").css('display','');
				$("#ReasonTextWarningLayer").html('<?=$Lang['StudentAttendance']['OutingReasonDuplicateOrBlank']?>');
			}
		}
	);
}

function Save_Reason() {
	var PostVar = {
		"ReasonText":$('#ReasonText').val(),
		"RecordID":$('#RecordID').val()
	};
	
	$.post("ajax_save_reason.php",PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				$("#Msg").val(data);
				document.getElementById('form1').submit();
			}
		}
	);
}

function Delete_Reason(RecordID) {
	if (confirm('<?=$Lang['StudentAttendance']['OutingReasonDeleteConfirm']?>')) {
		var PostVar = {
			"RecordID":RecordID
		};
		
		$.post("ajax_delete_reason.php",PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					$("#Msg").val(data);
					document.getElementById('form1').submit();
				}
			}
		);
	}
}
}
	
// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

$(document).ready(function() {
	Thick_Box_Init();
});
</script>