<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_LeaveSchoolOption";

$linterface = new interface_html();

$lc = new libcardstudentattend2();

$TAGS_OBJ[] = array($button_edit, "edit.php", 0);
$TAGS_OBJ[] = array($button_import, "import.php", 1);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));
?>
<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data"">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_select_file?></td>
		<td width="70%"class="tabletext">
			<input type="file" class="file" name="userfile"><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
		<td width="70%"class="tabletextremark">
			<?=$i_StudentAttendance_LeaveOption_ImportFileDescription?><br /><br />
			<a class="tablelink" href="<?=GET_CSV("leave_option_sample.csv")?>" target="_blank"><?=$i_general_clickheredownloadsample?></a>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>

</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>