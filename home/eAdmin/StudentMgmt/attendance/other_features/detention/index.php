<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_datamgmt_features_detention_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_datamgmt_features_detention_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_datamgmt_features_detention_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_datamgmt_features_detention_page_number!="")
{
	$pageNo = $ck_attend_datamgmt_features_detention_page_number;
}

if ($ck_attend_datamgmt_features_detention_page_order!=$order && $order!="")
{
	setcookie("ck_attend_datamgmt_features_detention_page_order", $order, 0, "", "", 0);
	$ck_attend_datamgmt_features_detention_page_order = $order;
} else if (!isset($order) && $ck_attend_datamgmt_features_detention_page_order!="")
{
	$order = $ck_attend_datamgmt_features_detention_page_order;
}

if ($ck_attend_datamgmt_features_detention_page_field!=$field && $field!="")
{
	setcookie("ck_attend_datamgmt_features_detention_page_field", $field, 0, "", "", 0);
	$ck_attend_datamgmt_features_detention_page_field = $field;
} else if (!isset($field) && $ck_attend_datamgmt_features_detention_page_field!="")
{
	$field = $ck_attend_datamgmt_features_detention_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Detention";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

if (isset($targetID) && $targetID != "")
{
    $conds = "AND a.UserID = $targetID";
}
$user_field = getNameFieldWithClassNumberByLang("b.");

$sql  = "SELECT
           a.RecordDate,
           $user_field,
           IF(a.ArrivalTime IS NULL,'$i_SmartCard_DetentionNotArrived',a.ArrivalTime),
           IF(a.DepartureTime IS NULL,'$i_SmartCard_DetentionNotLeft',a.DepartureTime),
           a.Reason,
           a.Location, a.Remark,
           CONCAT('<input type=checkbox name=DetentionID[] value=', a.DetentionID ,'>')
         FROM
           CARD_STUDENT_DETENTION as a ";
if ($lc->EnableEntryLeavePeriod) {
	$sql .= "
					INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.StudentID = selp.UserID 
          	AND 
          	a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
}
$sql .= "
           LEFT OUTER JOIN 
           INTRANET_USER as b 
           ON a.StudentID = b.UserID
         WHERE
            (a.RecordDate = '$keyword' OR b.ChineseName like '%$keyword%'
             OR b.EnglishName like '%$keyword%'
             OR a.Location LIKE '%$keyword%'
             OR a.Remark LIKE '%$keyword%')
             $conds
              ";
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.RecordDate",$user_field,"a.ArrivalTime","a.DepartureTime","a.Reason","a.Location","a.Remark");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(0, $i_SmartCard_DetentionDate)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(1, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(2, $i_SmartCard_DetentionArrivalTime)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(3, $i_SmartCard_DetentionDepartureTime)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(4, $i_SmartCard_DetentionReason)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(5, $i_SmartCard_DetentionLocation)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(6, $i_SmartCard_Remark)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("DetentionID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'DetentionID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'DetentionID[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("95%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
