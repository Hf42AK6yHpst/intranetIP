<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();

$Location = intranet_htmlspecialchars($Location);
$Remark = intranet_htmlspecialchars($Remark);
$Reason = intranet_htmlspecialchars($Reason);

$arrivalTimeStr = ($ArrivalTime ==""? "NULL":"'$ArrivalTime'");
$departureTimeStr = ($DepartureTime ==""? "NULL":"'$DepartureTime'");

$sql = "UPDATE CARD_STUDENT_DETENTION SET
               RecordDate = '$RecordDate',
               ArrivalTime = $arrivalTimeStr,
               DepartureTime = $departureTimeStr,
               Location = '$Location',
               Reason = '$Reason',
               Remark = '$Remark',
               DateModified = now()
        WHERE DetentionID = $DetentionID
               ";
if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['DetentionRecordUpdateSuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['DetentionRecordUpdateFail'];
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
