<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ldb = new libdb();
$lclass = new libclass();
$scm = new subject_class_mapping();

/*
$TargetType = $_REQUEST['TargetType'];
$TargetID = $_REQUEST['TargetID'];
$StudentIdAry = array();
if($TargetType=='student'){
	$StudentIdAry = $TargetID;
}else if($TargetType == 'class'){
	$ClassIdAry = $TargetID;
	$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
}else if($TargetType == 'form'){
	$FormIdAry = $TargetID;
	$ClassIdAry = array();
	
	for($i=0;$i<sizeof($FormIdAry);$i++)
	{
		$ClassList = $lclass->returnClassListByLevel($FormIdAry[$i]);
		for($j=0;$j<sizeof($ClassList);$j++) {
			$ClassIdAry[] = $ClassList[$j][0];
		}
	}
	$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
}
*/

$target = $_REQUEST['target']; // form | class | student | student2ndLayer | subject
$fieldId = $_REQUEST['fieldId']; // <select> id
$fieldName = $_REQUEST['fieldName']; // <select> name
$academicYearId = $_REQUEST['academicYearId'];
$studentFieldId = $_REQUEST['studentFieldId']; // student <select> id
$studentFieldName = $_REQUEST['studentFieldName']; // student <select> name
$singleSelection = $_REQUEST['singleSelection']==1;
$displaySize = $_REQUEST['displaySize']? max(5, intval($_REQUEST['displaySize'])) : 8; 
//$yearClassIdAry = (array)$_REQUEST['YearClassID']; // use to select students if target=class  

if($target=='form') {
	
	//$temp = $lclass->getSelectLevel("name=\"$fieldName\" id=\"$fieldId\" class=\"formtextbox\" multiple size=\"5\" ", $TargetClassLevel, false);
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'"').'>';
	$ClassLvlArr = $lclass->getLevelArray();
	
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		$temp .= '<option value="'.$ClassLvlArr[$i][0].'"';
		if(!$singleSelection) $temp .= ' selected';
		$temp .= '>'.$ClassLvlArr[$i][1].'</option>'."\n";
	}
	$temp .= '</select>'."\n";
	
}

if($target=="class") {
	
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'"').'>';
	
	$classResult = $lclass->getClassList($academicYearId);
	
	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][0].'" ';
		if(!$singleSelection) $temp .= ' selected';
		$temp .= '>'.$classResult[$k][1].'</option>';
	}
	$temp .= '</select>'."\n";
}

if($target=="student") {
	
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'" ').' onchange="'.$onchange.'">';
	
	$classResult = $lclass->getClassList($academicYearId);
			
	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][0].'" ';
		if(!$singleSelection) $temp .= ' selected';
		$temp .= '>'.$classResult[$k][1].'</option>';
	}
	$temp .= '</select>'."\n<br>";
	if($singleSelection){
		$temp .= '<span id="'.$divStudentSelection.'"></span>'."\n";
	}else{
		$temp .= '<div id="'.$divStudentSelection.'"></div>'."\n";
	}
}


if($target=="student2ndLayer") {
	$name_field = getNameFieldByLang("u.");
	$yearClassIdAry = (array)$_REQUEST['YearClassID'];
	if(!$singleSelection) $temp .= '<br />';
	$temp .= '<select name="'.$studentFieldName.'" id="'.$studentFieldId.'" class="formtextbox" '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'"').'>';
	
	$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
	$sql = "SELECT $clsName, ycu.ClassNumber, $name_field, u.UserID 
			FROM INTRANET_USER as u
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=u.UserID) 
			INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
			INNER JOIN YEAR as y ON y.YearID=yc.YearID 
			WHERE yc.YearClassID IN ('".implode("','",$yearClassIdAry)."') AND u.RecordType=2 
			ORDER BY y.Sequence,yc.Sequence,ycu.ClassNumber 
			";
	
	$studentResult = $ldb->returnArray($sql, 3);

	for($k=0;$k<sizeof($studentResult);$k++) {
		$temp .= '<option value="'.$studentResult[$k][3].'" ';
		if(!$singleSelection) $temp .= ' selected';
		$temp .= '>'.$studentResult[$k][0].'-'.$studentResult[$k][1].' '.$studentResult[$k][2].'</option>';
	}
	
	$temp .= '</select>'."\n";
	//$temp .= '<br />';
	if(!$singleSelection) $temp .= $linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('$studentFieldId', true);return false;");
}	

if($target == "subject")
{
	$subject_list = $scm->Get_Subject_List();
	$subject_ary = array();
	$subject_name_field = Get_Lang_Selection("SubjectDescB5","SubjectDescEN");
	$subject_shortname_field = Get_Lang_Selection("SubjectShortNameB5","SubjectShortNameEN");
	for($i=0;$i<count($subject_list);$i++)
	{
		$subject_ary[] = array($subject_list[$i]['SubjectID'],$subject_list[$i][$subject_name_field]);
	}
	$temp = getSelectByArray($subject_ary, ' id="'.$fieldId.'" name="'.$fieldName.'"  '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'" '), $__selected="", $__all=0, $__noFirst=1, $__FirstTitle="", $__ParQuoteValue=1);
}

echo $temp;

intranet_closedb();
?>