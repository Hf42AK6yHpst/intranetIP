<?php
######################################################
# Generate Statistic Report for Non-Exam Subjects
######################################################

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}
// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
#include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();
		
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		$MS_Type = strtolower(substr($ClassLevelName, 0, 1));
		$MS_Level = substr($ClassLevelName, 1, 1);
		
		$summary = "";
		if($MS_Type == "p") {
			$school = "Primary";
		} else if($MS_Type == "s") {
			$school = "Secondary";
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array_keys($ColumnTitleAry);
		
		// Check if ReportColumnID have value
		// 	if yes, only term mark is used
		// 	if no, overall marks of all terms are used
		if (isset($ReportColumnID) && $ReportColumnID != "") {
			// Hardcoded Column Name since names in $ColumnTitleAry are not exactly "SA1" & "SA2"
			if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "SA1";
			if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
			
			$StatReportTitle = "Analysis Report of ".$DisplayColumnName." for non-exam subjects (".$school.")";
		} else {
			$StatReportTitle = "Analysis Report for non-exam subjects (".$school.")";
		}
		
		$display = "";
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "G")
				continue;
				
			# check if the subject got grade A*
			$thisSchemeID = $FormSubjectGradingSchemeArr[$SubjectID]["schemeID"];
			$table = $lreportcard->DBName.".RC_GRADING_SCHEME_RANGE";
			$sql  = "SELECT COUNT(GradingSchemeRangeID) ";
			$sql .= "FROM $table ";
			$sql .= "WHERE SchemeID = '$thisSchemeID' AND Grade = 'A*'";
			$schemeGradeInfo = $lreportcard->returnVector($sql);
			if ($schemeGradeInfo[0] == 0)
			{
				$has_AStar = 0;
			}
			else
			{
				$has_AStar = 1;
			}
			
			$display .= "<tr><td class='SubjectName'>". convert2unicode(str_replace(" ()", "", $SubjectName[0]), 1, 2) ."</td><td colspan='13'></td></tr>";
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				$thisClassID = $ClassArr[$i]["ClassID"];
				
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$thisClassID] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$thisClassID] = 0;
				$ExNumber[$thisClassID] = 0;
				$BandNumber["A*"][$thisClassID] = 0;
				$BandNumber["A"][$thisClassID] = 0;
				$BandNumber["B"][$thisClassID] = 0;
				$BandNumber["C"][$thisClassID] = 0;
				$BandNumber["D"][$thisClassID] = 0;
				$BandNumber["/"][$thisClassID] = 0;
				$BandPercent["A*"][$thisClassID] = 0;
				$BandPercent["A"][$thisClassID] = 0;
				$BandPercent["B"][$thisClassID] = 0;
				$BandPercent["C"][$thisClassID] = 0;
				$BandPercent["D"][$thisClassID] = 0;
				$BandPercent["/"][$thisClassID] = 0;
				//$ClassStudentTotalmark[$thisClassID] = 0;
				//$ClassStudentAverageMark[$thisClassID] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if (trim($ClassResult[$j]["Grade"]) == "-") {
						$AbsNumber[$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "/" || $ClassResult[$j]["Grade"] == "N.A.") {
						$ExNumber[$thisClassID]++;
						
						if ($ClassResult[$j]["Grade"] == "/")
						{
							$StudentBand = $ClassResult[$j]["Grade"];
							if (!is_array($StudentBand))
							{
								$BandNumber[$StudentBand][$thisClassID]++;
							}
						}
					} else if ($ClassResult[$j]["Grade"] != "") {
						//$ClassStudentTotalmark[$thisClassID] += $ClassResult[$j]["Mark"];
						$StudentBand = $ClassResult[$j]["Grade"];
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$thisClassID]++;
					} 
				}
				
				$TotalNumber = $NumberOfClassStudent[$thisClassID];
				$AbsExNumber = $AbsNumber[$thisClassID]+$ExNumber[$thisClassID];
				$TotalExamNumber = $TotalNumber - $AbsExNumber;
				
				//$BandNumber["1-3"][$thisClassID] = $BandNumber["1"][$thisClassID]+$BandNumber["2"][$thisClassID]+$BandNumber["3"][$thisClassID];
				
				// Prevent divide by Zero
				if ($TotalExamNumber != 0) {
					$BandPercent["A*"][$thisClassID] = calculatePercent($BandNumber["A*"][$thisClassID], $TotalExamNumber);
					$BandPercent["A"][$thisClassID] = calculatePercent($BandNumber["A"][$thisClassID], $TotalExamNumber);
					$BandPercent["B"][$thisClassID] = calculatePercent($BandNumber["B"][$thisClassID], $TotalExamNumber);
					$BandPercent["C"][$thisClassID] = calculatePercent($BandNumber["C"][$thisClassID], $TotalExamNumber);
					$BandPercent["D"][$thisClassID] = calculatePercent($BandNumber["D"][$thisClassID], $TotalExamNumber);
					$BandPercent["/"][$thisClassID] = calculatePercent($BandNumber["/"][$thisClassID], $TotalExamNumber);
					
					//$BandPercent["1-3"][$thisClassID] = calculatePercent($BandNumber["1-3"][$thisClassID], $NumberOfClassStudent[$thisClassID]);
					
					//$ClassStudentAverageMark[$thisClassID] = round(($ClassStudentTotalmark[$thisClassID]/($NumberOfClassStudent[$thisClassID]-$ExNumber[$thisClassID])), 1);
				}
				
				# update on 11 Dec by Ivan - show subject teacher instead of class teacher
				/*
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				*/
				
				$SubjectTeacherArr = $lreportcard->returnSubjectTeacher($thisClassID, $SubjectID,$ReportID,$ClassStudentIDList[0]);
				$SubjectTeacherName = !empty($SubjectTeacherArr) ? implode(", ", $SubjectTeacherArr) : "-";
				
				# special handling of A*
				# show nth if no A* in grading scheme
				if (!$has_AStar)
				{
					$BandNumber["A*"][$thisClassID] = "";
					$BandPercent["A*"][$thisClassID] = "";
				}
				
				$display .= "<tr>
												<td class='tablerow_separator'>".$ClassArr[$i]["ClassName"]."</td>
												<td class='tablerow_separator'>".$SubjectTeacherName."</td>
												<td align='center' class='tablerow_separator'>".$TotalNumber."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$AbsExNumber."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$TotalExamNumber."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["A*"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["A*"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["A"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["A"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["B"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["B"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["C"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["C"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["D"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["D"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["/"][$thisClassID]."&nbsp;</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["/"][$thisClassID]."&nbsp;</td>
										</tr>";
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			
			$ClassLevelTotalExamNumber = $NumberOfClassLevelStudent - $ClassLevelAbsNumber - $ClassLevelExNumber;
			
			$ClassLevelBandNumber["A*"] = array_sum($BandNumber["A*"]);
			$ClassLevelBandNumber["A"] = array_sum($BandNumber["A"]);
			$ClassLevelBandNumber["B"] = array_sum($BandNumber["B"]);
			$ClassLevelBandNumber["C"] = array_sum($BandNumber["C"]);
			$ClassLevelBandNumber["D"] = array_sum($BandNumber["D"]);
			$ClassLevelBandNumber["/"] = array_sum($BandNumber["/"]);
			
			//$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($ClassLevelTotalExamNumber != 0) {
				$ClassLevelBandPercent["A*"] = calculatePercent($ClassLevelBandNumber["A*"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["A"] = calculatePercent($ClassLevelBandNumber["A"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["B"] = calculatePercent($ClassLevelBandNumber["B"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["C"] = calculatePercent($ClassLevelBandNumber["C"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["D"] = calculatePercent($ClassLevelBandNumber["D"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["/"] = calculatePercent($ClassLevelBandNumber["/"], $ClassLevelTotalExamNumber);
				
				//$ClassLevelBandPercent["1-3"] = calculatePercent($ClassLevelBandNumber["1-3"], $NumberOfClassLevelStudent);
				
				//$ClassLevelStudentAverageMark = round(($ClassLevelStudentTotalMark/$NumberOfClassLevelStudent) ,1);
			}
			
			# special handling of A*
			# show nth if no A* in grading scheme
			if (!$has_AStar)
			{
				$ClassLevelBandNumber["A*"] = "";
				$ClassLevelBandPercent["A*"] = "";
			}
				
			$display .= "<tr>
											<td class='tablerow_separator'>&nbsp;</td>
											<td class='tablerow_separator'>Level Average</td>
											<td align='center' class='tablerow_separator'>".$NumberOfClassLevelStudent."&nbsp;</td>
											<td align='center' class='tablerow_separator'>&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelTotalExamNumber."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["A*"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["A*"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["A"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["A"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["B"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["B"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["C"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["C"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["D"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["D"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["/"]."&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["/"]."&nbsp;</td>
									</tr>
									<tr><td colspan='14'>&nbsp;</td></tr>";
			
			
		} // End Loop 1: Subjects
		
		#debug_r($FormSubjectGradingSchemeArr);
		############## Interface Start ##############
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 24px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 3px;
	border-bottom: 5px solid #DDD;
}

h2 {
	font-size: 16px;
	font-family: "Times CY", "Times New Roman", serif;
	margin: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
}

#StatReportWrapper {
	min-height: 800px;
	height: auto;
	_height: 800px;
	border-top: 5px solid #DDD;
	border-bottom: 5px solid #DDD;
}

#StatTable thead {
	font-size: 15px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
}

#StatTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.SubjectName {
	font-size: 12px;
	font-weight: bold;
	font-family: arial, "lucida console", sans-serif;
	padding-bottom: 5px;
}

.tablerow_separator {
	BORDER-BOTTOM: #DCDCDC 1px solid
}

.GMS_text14bi {
	font-family: "Times New Roman", "Arial", "Lucida Console";	
	font-weight: bold;
	font-size: 14px;
	font-style: italic;
}

.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
-->
</style>
<script type="text/JavaScript" language="JavaScript">

</script>
<div id="StatReportWrapper" style="width:100%;">
	<h1><?=$StatReportTitle?></h1>
	<h2>
		School Year&nbsp;&nbsp;<?= $SchoolYear ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Level:&nbsp;<?= $ClassLevelName ?>
	</h2>
	<table id="StatTable" width="100%" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th align='left'>Subject Teacher</th>
				<th>Total</th>
				<th>Abs/Ex</th>
				<th>Total Exam</th>
				<th colspan="2">A*</th>
				<th colspan="2">A</th>
				<th colspan="2">B</th>
				<th colspan="2">C</th>
				<th colspan="2">D</th>
				<th colspan="2">VR</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>Number</th>
				<th>Number</th>
				<th>Number</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
			</tr>
		</thead>
		<tbody>
			<?= $display ?>
			<tr><td height="1" class="tabletext tableline" colspan='17'><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	    	<tr><td class="GMS_text14bi" colspan='16'>&nbsp;<?=$LastGenerated?></td></tr>
		</tbody>
		
		
	</table>
</div>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
