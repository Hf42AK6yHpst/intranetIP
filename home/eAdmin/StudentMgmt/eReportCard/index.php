<?php
// Modifying by : Bill

/********************** Change Log ***********************/
#
#   Date:       2020-04-28 (Bill)   [2019-0924-1029-57289]
#               Redirect handling for View Group Members - with access page settings ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
#   Date:       2019-12-10 (Bill)   [2019-0924-1029-57289]
#               Redirect handling for Parent & Student ($eRCTemplateSetting['Management']['ParentStudentHideMSVerification']) & $eRCTemplateSetting['Report']['ParentStudentReportPrinting'])
#
#	Date:		2018-07-23 (Bill)	[2017-0502-1024-21096]
#				Redirect to Report Printing for View Group Member  ($eRCTemplateSetting['ViewGroupAccessReportOnly'])
#
#	Date:		2017-05-15 (Bill)	[2017-0502-1024-21096]
#				Allow View Group Member to access eAdmin > Student Mgmt > eReportCard
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_opendb();

unset($ck_ReportCard_UserType);
unset($IntranetReportCardType);

$skipAcademicYearChecking = $_GET['skipAcademicYearChecking'];

if($top_menu_mode == 1 && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"] == 1)
{
	$IntranetReportCardType = "ADMIN";
}
else if ($top_menu_mode == 1 && $sys_custom['eRC']['Settings']['ViewGroupUserType'] && $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_view_group_memeber"] == 1)
{
	$IntranetReportCardType = "VIEW_GROUP";
}
else
{
	//include_once ("$PATH_WRT_ROOT/includes/libuser.php");
	//$header_lu = new libuser($_SESSION['UserID']);
	
	if ($_SESSION['UserType'] == USERTYPE_STAFF) {
		$IntranetReportCardType = "TEACHER";
    }
	else if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
		$IntranetReportCardType = "STUDENT";
    }
	else if ($_SESSION['UserType'] == USERTYPE_PARENT) {
		$IntranetReportCardType = "PARENT";
    }
}

//$ck_ReportCard_UserType = ($ck_ReportCard_UserType == "ADMIN" && $top_menu_mode==1) ? "ADMIN" : "TEACHER";
//$ck_ReportCard_UserType = ($ck_ReportCard_UserType != "STUDENT" && $top_menu_mode==1) ? "ADMIN" : "TEACHER";
//setcookie("ck_ReportCard_UserType", $ck_ReportCard_UserType, 0, "", "", 0);

if (isset($IntranetReportCardType))
{
	# TYPES: ADMIN, TEACHER, STUDENT
	//setcookie("ck_ReportCard_UserType", $IntranetReportCardType, 0, "", "", 0);
	//$ck_ReportCard_UserType = $IntranetReportCardType;
	session_register_intranet("ck_ReportCard_UserType", $IntranetReportCardType);
}

/*
if($ck_ReportCard_UserType=="TEACHER") {
	header("Location: management/marksheet_revision/marksheet_revision.php");
}
else if($ck_ReportCard_UserType=="STUDENT" || $ck_ReportCard_UserType=="PARENT") {
	header("Location: management/marksheet_feedback/index.php");
}
else {
	# Admin
	#$top_menu_mode = 1;
	#header("Location: settings/basic_settings/highlight.php");
	
	if ($top_menu_mode == 1)
		header("Location: management/schedule/schedule.php");
	else
		header("Location: management/marksheet_revision/marksheet_revision.php");
}
*/

### Check if active year is current academic year
$libreportcard = new libreportcard();
$activeAcademicYearID = $libreportcard->GET_ACTIVE_YEAR_ID();
$curAcademicYearID = Get_Current_Academic_Year_ID();

$alertNotCurrentYear = false;
if ($skipAcademicYearChecking)
{
	// no checking
}
else
{
	if ($activeAcademicYearID != $curAcademicYearID)
	{
		# Not viewing current academic year data in eRC => show an alert to tell the user
		$alertNotCurrentYear = true;
		$activeAcademicYearName = $libreportcard->GET_ACTIVE_YEAR_NAME();
		
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$ObjAcademicYear = new academic_year($curAcademicYearID);
		$curAcademicYearName = $ObjAcademicYear->Get_Academic_Year_Name();
		
		$alertMsg = $eReportCard['jsNotCurrentAcademicYearInfo'];
		$alertMsg = str_replace('|||--eRC Active Year--|||', $activeAcademicYearName, $alertMsg);
		$alertMsg = str_replace('|||--Current Academic Year--|||', $curAcademicYearName, $alertMsg);
		$alertMsg .= ($ck_ReportCard_UserType == 'ADMIN') ? $eReportCard['jsNotCurrentAcademicYearAdmin'] : $eReportCard['jsNotCurrentAcademicYearTeacher'];
	}
}

### determine redirect user to which page
$targetPage = $PATH_WRT_ROOT.'home/index.php';
if ($ck_ReportCard_UserType == "TEACHER")
{
	$isClassTeacher = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"];
	$isSubjectTeacher = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_teacher"];
	$isSubjectPanel = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"];

	if ($isClassTeacher || $isSubjectTeacher) {
		$targetPage = 'management/marksheet_revision/marksheet_revision.php';
	}
	else if ($isSubjectPanel) {
		$targetPage = 'reports/grand_marksheet/index.php';
	}
}
else if ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT")
{
    //$targetPage = 'management/marksheet_feedback/index.php';
    if ($eRCTemplateSetting['Management']['ParentStudentHideMSVerification']) {
        if ($eRCTemplateSetting['Report']['ParentStudentReportPrinting']) {
            $targetPage = 'reports/print_student_report/index.php';
        }
    }
    else {
	    $targetPage = 'management/marksheet_feedback/index.php';
    }
}
else if ($ck_ReportCard_UserType == "VIEW_GROUP")
{
	$targetPage = 'management/marksheet_revision/marksheet_revision.php';
	if($eRCTemplateSetting['ViewGroupAccessReportOnly']) {
	    $targetPage = 'reports/class_teacher_print/index.php';
	}

	// [2019-0923-1726-31289]
    if($eRCTemplateSetting['ViewPageAssignedOnly'])
    {
	    // Get root path for redirect
        $thisModuleObj = $libreportcard->GET_MODULE_OBJ_ARR();
        if($thisModuleObj['root_path'] != '')
        {
            $hasValidTargetPage = false;
            $targetRootPagePath = explode('home/eAdmin/StudentMgmt/eReportCard/', $thisModuleObj['root_path']);
            if($targetRootPagePath[1] != '') {
                $targetPage = $targetRootPagePath[1];
                $hasValidTargetPage = true;
            }
        }
        
        if(!$hasValidTargetPage) {
            No_Access_Right_Pop_Up();
        }
    }
}
else if ($ck_ReportCard_UserType == "ADMIN")
{
	if ($top_menu_mode == 1) {
		// admin user coming from eAdmin -> admin mode
		$targetPage = 'management/schedule/schedule.php';
	}
	else {
		// admin user coming from eService -> normal teacher mode
		$targetPage = 'management/marksheet_revision/marksheet_revision.php';
	}
}
?>

<script>
var forwardedAlready = false;
<? if ($alertNotCurrentYear == true) { ?>
	alert("<?=$alertMsg?>");
	
	<? if (($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"] && $ck_ReportCard_UserType=='ADMIN') || ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_view_group_memeber"] && $ck_ReportCard_UserType=='VIEW_GROUP')){ ?>
		// do nothing
	<? } else { ?>
		window.location = '<?=$PATH_WRT_ROOT?>home/index.php';
		forwardedAlready = true;
	<? } ?>
<? } ?>

if (!forwardedAlready) {
	window.location = '<?=$targetPage?>';
}
</script>