<?php
// Using:

/*************************************************************
 *  Modification Log
 *      20190212 Bill:  [2019-0212-1333-42235]
 *          - fixed: php error due to in_array()
 *      20180420 Bill:  [2018-0418-1605-22206] 
 *          - trim $thisMarkDifference - prevent cannot check score difference correctly
 * 		20110209 Marcus:
 * 			- cater GrandSD
 * ***********************************************************/

@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
$NoLangWordings = true;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}


### Get Data
$ReportID = $_POST['ReportID'];
$FromReportID = $_POST['FromReportID'];
$FromReportColumnID = $_POST['FromReportColumnID'];
$ToReportID = $_POST['ToReportID'];
$ToReportColumnID = $_POST['ToReportColumnID'];
$DetermineBy = $_POST['DetermineBy'];

$ClassLevelID = $_POST['ClassLevelID'];
$Semester = $_POST['Semester'];

### Get Subjects
$GradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, $withGrandMark=0, $ReportID);
$SubjectIDArr = array_keys($GradingSchemeArr);


//$GrandFieldDetermineBy = $lreportcard->Get_Grand_Position_Determine_Field();
$GrandFieldDetermineBy = $DetermineBy;
$GrandFieldID = $lreportcard->Get_Grand_Field_SubjectID($GrandFieldDetermineBy);
$SubjectIDArr[] = $GrandFieldID;
$numOfSubject = count($SubjectIDArr);

### Get Form Students
$FormStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=1, $isShowStyle=0);

### Get excluded student list
//$excludeStudentArr = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS();
$excludeStudentAssoArr = array();
$fromReportBasicInfoAry = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$fromReportYearTermID = $fromReportBasicInfoAry['Semester'];
$fromReportReportType = ($fromReportYearTermID == 'F')? 'W' : 'T';
if ($fromReportReportType == 'T') {
	$excludeStudentAssoArr[$FromReportID][0] = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($FromReportID);
}
else {
	$termReportInfoAry = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($FromReportID);
	$numOfTermReport = count($termReportInfoAry);
	for ($i=0; $i<$numOfTermReport; $i++) {
		$_termReportId = $termReportInfoAry[$i]['ReportID'];
		$_reportColumnId = $termReportInfoAry[$i]['ReportColumnID'];
		
		$excludeStudentAssoArr[$FromReportID][$_reportColumnId] = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($_termReportId);
	}
	$excludeStudentAssoArr[$FromReportID][0] = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($FromReportID);
}

$toReportBasicInfoAry = $lreportcard->returnReportTemplateBasicInfo($ToReportID);
$toReportYearTermID = $toReportBasicInfoAry['Semester'];
$toReportReportType = ($toReportYearTermID == 'F')? 'W' : 'T';
if ($toReportReportType == 'T') {
	$excludeStudentAssoArr[$ToReportID][0] = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($ToReportID);
}
else {
	$termReportInfoAry = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ToReportID);
	$numOfTermReport = count($termReportInfoAry);
	for ($i=0; $i<$numOfTermReport; $i++) {
		$_termReportId = $termReportInfoAry[$i]['ReportID'];
		$_reportColumnId = $termReportInfoAry[$i]['ReportColumnID'];
		
		$excludeStudentAssoArr[$ToReportID][$_reportColumnId] = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($_termReportId);
	}
	$excludeStudentAssoArr[$ToReportID][0] = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($ToReportID);
}


$StudentMarkInfoArr = array();
$StudentMarkDifferenceArr = array();
for ($i=0; $i<$numOfSubject; $i++)
{
	$thisSubjectID = $SubjectIDArr[$i];
	
	### Get Grading Scheme Info
	//$GradingSchemeInfo[$thisSubjectID] = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, $withGrandResult=1, $returnAsso=0, $ReportID);
	//$thisScaleInput = $GradingSchemeInfo[$thisSubjectID]['ScaleInput'];
	$thisScaleInput = $GradingSchemeArr[$thisSubjectID]['scaleInput'];
	
	### Get Marks
	if ($thisSubjectID > 0)
	{
		# Normal Subject
		$SubjectMarkArr_From = $lreportcard->getMarks($FromReportID, '', '', $ParentSubjectOnly=0, $includeAdjustedMarks=1, 'Mark Desc', $thisSubjectID, $FromReportColumnID);
		$SubjectMarkArr_To = $lreportcard->getMarks($ToReportID, '', '', $ParentSubjectOnly=0, $includeAdjustedMarks=1, 'Mark Desc', $thisSubjectID, $ToReportColumnID);
	}
	else
	{
		# Grand Mark
		$MarkArrTemp_From = $lreportcard->getReportResultScore($FromReportID, $FromReportColumnID);
		$MarkArrTemp_To = $lreportcard->getReportResultScore($ToReportID, $ToReportColumnID);
		
		foreach ((array)$MarkArrTemp_From as $thisStudentID => $thisStudentMarkArr)
		{
			$SubjectMarkArr_From[$thisStudentID][$thisSubjectID][$FromReportColumnID]['Mark'] = $thisStudentMarkArr[$FromReportColumnID][$GrandFieldDetermineBy];
			$SubjectMarkArr_From[$thisStudentID][$thisSubjectID][$FromReportColumnID]['RawMark'] = $thisStudentMarkArr[$FromReportColumnID][$GrandFieldDetermineBy];
			$SubjectMarkArr_From[$thisStudentID][$thisSubjectID][$FromReportColumnID]['OrderMeritForm'] = $thisStudentMarkArr[$FromReportColumnID]['OrderMeritForm'];
			
		}
		
		foreach ((array)$MarkArrTemp_To as $thisStudentID => $thisStudentMarkArr)
		{
			$SubjectMarkArr_To[$thisStudentID][$thisSubjectID][$ToReportColumnID]['Mark'] = $thisStudentMarkArr[$ToReportColumnID][$GrandFieldDetermineBy];
			$SubjectMarkArr_To[$thisStudentID][$thisSubjectID][$ToReportColumnID]['RawMark'] = $thisStudentMarkArr[$ToReportColumnID][$GrandFieldDetermineBy];
			$SubjectMarkArr_To[$thisStudentID][$thisSubjectID][$ToReportColumnID]['OrderMeritForm'] = $thisStudentMarkArr[$ToReportColumnID]['OrderMeritForm'];
		}
	}
	
	### consolidate marks of different reports of each students
	foreach((array)$SubjectMarkArr_From as $thisStudentID => $thisStudentSubjectMarkArr_From)
	{
		### Compare marks subject only
		if ($thisSubjectID > 0 && $thisScaleInput != 'M') {
			continue;
		}
		
		//2014-0403-0919-14140
		if ($eRCTemplateSetting['Management']['AcademicProgress']['IncludeExcludedRankingStudent']) {
			// includes excluded ranking 
		}
		else {
		    if (in_array($thisStudentID, (array)$excludeStudentAssoArr[$FromReportID][$FromReportColumnID]) || in_array($thisStudentID, (array)$excludeStudentAssoArr[$ToReportID][$ToReportColumnID])) {
				continue;
			}
		}
			
		$thisYearClassID = $FormStudentInfoArr[$thisStudentID]['YearClassID'];
		$thisStudentSubjectMarkArr_To = $SubjectMarkArr_To[$thisStudentID];
				
		$thisRawMark_From = $thisStudentSubjectMarkArr_From[$thisSubjectID][$FromReportColumnID]['RawMark'];
		$thisMark_From = $thisStudentSubjectMarkArr_From[$thisSubjectID][$FromReportColumnID]['Mark'];
		$thisGrade_From = $thisStudentSubjectMarkArr_From[$thisSubjectID][$FromReportColumnID]['Grade'];
		$thisRanking_From = $thisStudentSubjectMarkArr_From[$thisSubjectID][$FromReportColumnID]['OrderMeritForm'];
		
		
		### Skip non-numeric score, special case
		//2016-0624-1210-27206
		//if (is_numeric($thisRawMark_From) == false || $lreportcard->Check_If_Grade_Is_SpecialCase($thisGrade_From))
		if (is_numeric($thisRawMark_From) == false || $lreportcard->Check_If_Grade_Is_SpecialCase($thisGrade_From) || $thisRanking_From==-1)
			continue;
		
			
		$thisRawMark_To = $thisStudentSubjectMarkArr_To[$thisSubjectID][$ToReportColumnID]['RawMark'];
		$thisMark_To = $thisStudentSubjectMarkArr_To[$thisSubjectID][$ToReportColumnID]['Mark'];
		$thisGrade_To = $thisStudentSubjectMarkArr_To[$thisSubjectID][$ToReportColumnID]['Grade'];
		$thisRanking_To = $thisStudentSubjectMarkArr_To[$thisSubjectID][$ToReportColumnID]['OrderMeritForm'];
		
		### Skip non-numeric score, special case
		//2016-0624-1210-27206
		//if (is_numeric($thisRawMark_To) == false || $lreportcard->Check_If_Grade_Is_SpecialCase($thisGrade_To))
		if (is_numeric($thisRawMark_To) == false || $lreportcard->Check_If_Grade_Is_SpecialCase($thisGrade_To) || $thisRanking_To==-1)
			continue;
		
		$thisRawMark_From_calculation = $thisRawMark_From;
		if ($thisRawMark_From == -1) {
			$thisRawMark_From_calculation = 0;
		}
		
		$thisRawMark_To_calculation = $thisRawMark_To;
		if ($thisRawMark_To == -1) {
			$thisRawMark_To_calculation = 0;
		}
		
		// [2018-0418-1605-22206] to trim $thisMarkDifference - prevent cannot check score difference
		$thisMarkDifference = $thisRawMark_To_calculation - $thisRawMark_From_calculation;
		if(!is_array($thisMarkDifference)){
		    $thisMarkDifference = trim($thisMarkDifference);
		}
		
		$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['FromScore'] = $thisRawMark_From;
		$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['ToScore'] = $thisRawMark_To;
		$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['ScoreDifference'] = $thisMarkDifference;
		
		$StudentMarkDifferenceArr[$thisSubjectID][$thisYearClassID][$thisStudentID] = $thisMarkDifference;
		$StudentMarkDifferenceArr[$thisSubjectID][0][$thisStudentID] = $thisMarkDifference;
	}
			
	
	### Calculate the Improvement position of the student
	foreach((array)$StudentMarkDifferenceArr[$thisSubjectID] as $thisYearClassID => $thisClassMarkDifferenceArr)
	{
		### Sort the Mark Difference to get the most improved student
		arsort($thisClassMarkDifferenceArr, SORT_NUMERIC);
	
		$thisRank = 0;
		$lastMark = -1;
		$sameMarkCount = 0;
		foreach((array)$thisClassMarkDifferenceArr as $thisStudentID => $thisMark)
		{
			if ($eRCTemplateSetting['RankingMethod'] == '1223')
			{
				# 1,2,2,3
				if ($thisMark != $lastMark)
					$thisRank++;
			}
			else
			{
				# 1,2,2,4
				if ($thisMark != $lastMark)
				{
					$thisRank++;
					$thisRank += $sameMarkCount;
					$sameMarkCount = 0;
				}
				else
				{
					$sameMarkCount++;
				}
			}
			
			$lastMark = $thisMark;
			
			if ($thisYearClassID == 0)
			{
				$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['OrderFormScoreDifference'] = $thisRank;
			}
			else
			{
				$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['OrderClassScoreDifference'] = $thisRank;
				
				if ($eRCTemplateSetting['Management']['AcademicProgress']['NoPrizeQuotaLimit']) {
					// get the prize if the mark is improved
					if ($eRCTemplateSetting['Management']['AcademicProgress']['MinimumImprovementMark'] > 0) {
						if (my_round($thisMark, 10) >= my_round($eRCTemplateSetting['Management']['AcademicProgress']['MinimumImprovementMark'], 10)) {
							$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['AcademicProgressPrize'] = 1;
						}
						else {
							$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['AcademicProgressPrize'] = 0;
						}
					}
					else {
						if ($thisMark > 0) {
							$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['AcademicProgressPrize'] = 1;
						}
						else {
							$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['AcademicProgressPrize'] = 0;
						}
					}
				}
				else {
					// Get the prize if the Grand Mark improved most in Class
					if ($thisRank == 1 && $thisSubjectID < 0)
						$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['AcademicProgressPrize'] = 1;
					else
						$StudentMarkInfoArr[$thisSubjectID][$thisStudentID]['AcademicProgressPrize'] = 0;
				}
			}
		}
	}	
} 


$SuccessArr['DeleteRecords'] = $lreportcard->Delete_Academic_Progress($ReportID);
$SuccessArr['InsertRecords'] = $lreportcard->Insert_Academic_Progress($ReportID, $FromReportID, $ToReportID, $StudentMarkInfoArr);


if (in_array(false, $SuccessArr))
	$ReturnMsgKey = 'GenerateFailed';
else
{
	$ReturnMsgKey = 'GenerateSuccess';
}
	
$params = ((isset($Semester)) ? "Semester=$Semester&" : "")."ReportID=$ReportID&ClassLevelID=$ClassLevelID&ReturnMsgKey=$ReturnMsgKey";

intranet_closedb();
header("Location: index.php?$params");

?>