<?php
// Using: 

/*************************************************************
 *  Modification Log
 * 		20110209 Marcus:
 * 			- cater GrandSD
 * ***********************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

### Gen Report Info		
$ReportTemplateData = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ReportTitlePieces = explode(":_:",$ReportTemplateData["ReportTitle"]);
$ReportTitle = implode("<br>",$ReportTitlePieces);
$ClassLevelID = $ReportTemplateData["ClassLevelID"];
$ReportTypeDisplay = $lreportcard_ui->Get_Report_Card_Type_Display($ReportID, 1);

$SemID = $ReportTemplateData['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";
if ($ReportType == 'T')
{
	# term report
	# set the report selection to the first term report and this report by default
	$exludeSemesterArr = array($SemID, 'F');
	$TermReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $others='', $ClassLevelID, '', 1, $exludeSemesterArr);
	$FromReportID = $TermReportInfoArr['ReportID'];
	$ToReportID = $ReportID;
}
else
{
	# consolidated report
	# set the report selection to the first term and the last term included in the consolidated report by default
	$TermReportInfoArr = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1);
	$numOfTermReport = count($TermReportInfoArr);
	
	$FromReportID = $TermReportInfoArr[0]['ReportID'];
	$ToReportID = $TermReportInfoArr[$numOfTermReport-1]['ReportID'];
}

# Navigation
$PAGE_NAVIGATION[] = array($eReportCard['Management_AcademicProgress'], "javascript:jsGoBack()"); 
$PAGE_NAVIGATION[] = array(str_replace(':_:', ' ', $ReportTemplateData["ReportTitle"]), ""); 
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);


$ly = new Year($ClassLevelID);
$FormName = $ly->YearName;

### Determine By Selection
$DetermineBySelection = $lreportcard->Get_Order_Criteria_Selection("DetermineBy",1);

### From Report Selection
$FromSelection = $lreportcard->Get_Report_Selection($ClassLevelID, $FromReportID, 'FromReportID', 'js_Changed_Report_Selection(this.value, \'From\');', $ForVerification=0, $ForSubmission=0, $HideNonGenerated=1, $OtherTags='');

### To Report Selection
$ToSelection = $lreportcard->Get_Report_Selection($ClassLevelID, $ToReportID, 'ToReportID', 'js_Changed_Report_Selection(this.value, \'To\');', $ForVerification=0, $ForSubmission=0, $HideNonGenerated=1, $OtherTags='');



### Page Layout  
$CurrentPage = "Management_AcademicProgress";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Management_AcademicProgress'], "", 0);
$linterface->LAYOUT_START();


?>
<script>
$().ready( function() {
	SelectAll();
	js_Changed_Report_Selection($('select#FromReportID').val(), 'From');
	js_Changed_Report_Selection($('select#ToReportID').val(), 'To');
});

function SelectAll()
{
	$("[name='ClassID[]']").contents().attr("selected",true);
}

function jsCheckForm()
{
	var jsFromReportID = $('select#FromReportID').val();
	var jsFromReportColumnID = $('select#FromReportColumnID').val();
	var jsToReportID = $('select#ToReportID').val();
	var jsToReportColumnID = $('select#ToReportColumnID').val();
	
	if (jsFromReportID == jsToReportID && jsFromReportColumnID == jsToReportColumnID)
	{
		alert('<?=$eReportCard['ManagementArr']['AcademicProgressArr']['jsWarningArr']['SameReportSelected']?>');
		$('select#FromReportID').focus();
		return false;
	}
	
	document.frm.submit();
}

function jsGoBack()
{
	var classLevelID = $('input#ClassLevelID').val();
	var semester = $('input#Semester').val();
	
	var params = '';
	params += 'Semester='+semester;
	params += '&ClassLevelID='+classLevelID;
	
	window.location='index.php?'+params;
}

// jsType = "From" / "To"
function js_Changed_Report_Selection(jsReportID, jsType) {
	var jsTargetSpanID = jsType + 'ReportColumnSpan';
	var jsSelectionID = jsType + 'ReportColumnID';
	
	$('span#' + jsTargetSpanID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'ReportColumn_Selection',
			ReportID: jsReportID,
			Selected: 0,
			SelectionID: jsSelectionID
		},
		function(ReturnData)
		{
			
		}
	);
}
</script>

<br>
<!-- Start Main Table //-->
<form id="frm" name="frm" method="POST" action="generate_update.php">
<table width="100%" border="0" cellpadding"0" cellspacing="0">
	<tr><td colspan="2" class="navigation"><?=$PageNavigation?></td></tr>
<table>

<br style="clear:both" />

<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                         
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table>
                  </td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td>
	            <table width="100%" border="0" cellspacing="0" cellpadding="4">
		            <tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$ReportTitle?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportType']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$ReportTypeDisplay?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Form']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$FormName?></td>
					</tr>
					
					
					<tr>
						<td colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title">- <?=$eReportCard['ManagementArr']['AcademicProgressArr']['GenerationOptions']?> -</em></td>
					</tr>

					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['ManagementArr']['AcademicProgressArr']['DetermineBy'] ?>
						</td>
						<td valign="top" class="tabletext" width="70%"><?=$DetermineBySelection?></td>
					</tr>
					
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ManagementArr']['AcademicProgressArr']['FromReportCard']?></td>
						<td valign="top" class="tabletext">
							<?=$FromSelection?>
							<span id="FromReportColumnSpan"></span>
						</td>
					</tr>
					
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ManagementArr']['AcademicProgressArr']['ToReportCard']?></td>
						<td valign="top" class="tabletext">
							<?=$ToSelection?>
							<span id="ToReportColumnSpan"></span>
						</td>
					</tr>
				</table>
			</td>
		  </tr>
		  
		  <tr><td>&nbsp;</td></tr>
		  
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
          <tr>
          	<td align="center">
          		<br><br>
          		<?=$linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "jsCheckForm();", "PrintBtn")?>
          		&nbsp;&nbsp;
          		<?=$linterface->GET_ACTION_BTN($button_back, "button", "jsGoBack();", "BackBtn")?>
          	</td>
          </tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" id="Semester" name="Semester" value="<?=$Semester?>">
<input type="hidden" id="ReportID" name="ReportID" value="<?=$ReportID?>">
</form>
<br>
<br>
<!-- End Main Table //-->
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>