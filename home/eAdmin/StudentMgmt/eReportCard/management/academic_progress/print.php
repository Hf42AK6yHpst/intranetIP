<?
// Using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
$lreportcard_ui = new libreportcard_ui();
	
if ($lreportcard->hasAccessRight()) 
{
	$sql_query = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['sql_query']));
	$DisplayDataArr = $lreportcard->returnArray($sql_query);
	
	### Report Title
	$ProgressReportTitle = $eReportCard['Management_AcademicProgress'];
	
	### Report Info
	$ReportInfo = '';
	
	### Get Report Info
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ClassLevelID = $ReportInfoArr['ClassLevelID'];
	$ReportTitlePieces = explode(":_:",$ReportInfoArr["ReportTitle"]);
	$ReportTitle = implode("<br>", $ReportTitlePieces);
	$ReportTypeDisplay = $lreportcard_ui->Get_Report_Card_Type_Display($ReportID, 1);
	
	$ly = new Year($ClassLevelID);
	$FormName = $ly->YearName;
	
	if ($YearClassID != '')
	{
		$ObjClass = new year_class($YearClassID);
		$ClassName .= $ObjClass->Get_Class_Name();
	}
	else
		$ClassName = $eReportCard['AllClasses'];
	
	### Build Table
	$table = '';
	$table .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>\n";
		# Report Title & Type
		$table .= "<tr valign='top'>\n";
			$table .= "<td width='8%'>".$eReportCard['ReportTitle']."</td>";
			$table .= "<td width='5'>: </td>";
			$table .= "<td>".$ReportTitle."</td>";
			$table .= "<td width='8%'>".$eReportCard['ReportType']."</td>";
			$table .= "<td width='5'>: </td>";
			$table .= "<td>".$ReportTypeDisplay."</td>";
		$table .= "</tr>\n";
		# Form & Class Name
		$table .= "<tr valign='top'>\n";
			$table .= "<td>".$eReportCard['Form']."</td>";
			$table .= "<td>: </td>";
			$table .= "<td>".$FormName."</td>";
			$table .= "<td>".$eReportCard['Class']."</td>";
			$table .= "<td>: </td>";
			$table .= "<td>".$ClassName."</td>";
		$table .= "</tr>\n";
		
	$table .= "</table>\n";
	$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
		$table .= "<thead>\n";
			$table .= "<tr>\n";
				$table .= "<th>#</th>\n";
				$table .= "<th>".$eReportCard["Class"]."</th>\n";
				$table .= "<th>".$eReportCard['ClassNo']."</th>\n";
				$table .= "<th>".$eReportCard['Student']."</th>\n";
				$table .= "<th>".$eReportCard['ManagementArr']['AcademicProgressArr']['FromMark']."</th>\n";
				$table .= "<th>".$eReportCard['ManagementArr']['AcademicProgressArr']['ToMark']."</th>\n";
				$table .= "<th>".$eReportCard['ManagementArr']['AcademicProgressArr']['MarkDifference']."</th>\n";
				$table .= "<th>".$eReportCard['ManagementArr']['AcademicProgressArr']['ClassPosition']."</th>\n";
				$table .= "<th>".$eReportCard['ManagementArr']['AcademicProgressArr']['FormPosition']."</th>\n";
				$table .= "<th>".$eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrize']."</th>\n";
			$table .= "</tr>\n";
		$table .= "</thead>\n";
		
		$table .= "<tbody>\n";
		$counter = 0;
			foreach ($DisplayDataArr as $row => $rowDataArr)
			{
				$counter++;
				
				$table .= "<tr>\n";
					$table .= "<td align='center'>".$counter."</td>\n";
					
					$DisplayedColumn = 0;
					foreach ($rowDataArr as $column => $cellData)
					{
						if (is_numeric($column) && $DisplayedColumn < 9)
						{
							if ($eRCTemplateSetting['Management']['AcademicProgress']['ExportUserLogin'] && $column==0) {
								continue;
							}
							
							$DisplayedColumn++;
							$table .= "<td align='center'>".$cellData."</td>\n";
						}
					}
				$table .= "</tr>\n";
			}
		$table .= "</tbody>\n";
	$table .= "</table>\n";
	
	$css = $lreportcard->Get_GrandMS_CSS();
	
	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><h1>'.$ProgressReportTitle.'</h1></td>
					</tr>
					<tr><td class="tabletext">'.$ReportInfo.'</td></tr>
					<tr><td>'.$table.'</td></tr>
				</table>';
		
	echo $lreportcard->Get_Report_Header($ProgressReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();	
}
?>