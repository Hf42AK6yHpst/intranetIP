<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();
	
if ($lreportcard->hasAccessRight()) 
{
	$lexport = new libexporttext();
	
	### If no selected class, show form name in the file name. Otherwise, show the class name
	$sql_query = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['sql_query']));
	$DisplayDataArr = $lreportcard->returnArray($sql_query);
	
	### Report Info
	$ReportInfo = '';
	
	### Get Report Info
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ClassLevelID = $ReportInfoArr['ClassLevelID'];
	$ReportTitlePieces = explode(":_:",$ReportInfoArr["ReportTitle"]);
	$ReportTitle = implode(" ", $ReportTitlePieces);
	$ReportTypeDisplay = $lreportcard_ui->Get_Report_Card_Type_Display($ReportID, 1);
	
	$ly = new Year($ClassLevelID);
	$FormName = $ly->YearName;
	
	if ($YearClassID != '')
	{
		$ObjClass = new year_class($YearClassID);
		$ClassName = $ObjClass->Get_Class_Name();
		
		### Report Title
		$ProgressReportTitle = $eReportCard['Management_AcademicProgress'].'_'.$ClassName;
	}
	else
	{
		$ClassName = $eReportCard['AllClasses'];
		
		### Report Title
		$ProgressReportTitle = $eReportCard['Management_AcademicProgress'].'_'.$FormName;
	}
		
		
	$i_counter = 0;
	$j_counter = 0;
	$ExportArr[$i_counter][$j_counter++] = "Form";
	$ExportArr[$i_counter][$j_counter++] = $FormName;
	$ExportArr[$i_counter][$j_counter++] = "";
	$ExportArr[$i_counter][$j_counter++] = "Class";
	$ExportArr[$i_counter][$j_counter++] = $ClassName;
	
	$i_counter++;
	$j_counter = 0;
	if ($eRCTemplateSetting['Management']['AcademicProgress']['ExportUserLogin']) {
		$ExportArr[$i_counter][$j_counter++] = "User Login";
	}
	$ExportArr[$i_counter][$j_counter++] = "Class";
	$ExportArr[$i_counter][$j_counter++] = "Class Number";
	if ($eRCTemplateSetting['Management']['AcademicProgress']['ExportStudentNameBothLang']) {
		$ExportArr[$i_counter][$j_counter++] = Get_Lang_Selection("Student Name (Chi)", "Student Name (Eng)");
		$ExportArr[$i_counter][$j_counter++] = Get_Lang_Selection("Student Name (Eng)", "Student Name (Chi)");
	}
	else {
		$ExportArr[$i_counter][$j_counter++] = "Student";		
	}
	$ExportArr[$i_counter][$j_counter++] = "From Mark";
	$ExportArr[$i_counter][$j_counter++] = "To Mark";
	$ExportArr[$i_counter][$j_counter++] = "Mark Difference";
	$ExportArr[$i_counter][$j_counter++] = "Class Position";
	$ExportArr[$i_counter][$j_counter++] = "Form Position";
	$ExportArr[$i_counter][$j_counter++] = "Progress Prize";
	
	$i_counter++;
	$j_counter = 0;
	if ($eRCTemplateSetting['Management']['AcademicProgress']['ExportUserLogin']) {
		$maxCol = 10;
	}
	else {
		$maxCol = 9;
	}
	foreach ($DisplayDataArr as $row => $rowDataArr)
	{
		$DisplayedColumn = 0;
		foreach ($rowDataArr as $column => $cellData)
		{
			if (is_numeric($column) && $DisplayedColumn < $maxCol)
			{
				$DisplayedColumn++;
				$ExportArr[$i_counter][$j_counter++] = $cellData;
				
				if ($eRCTemplateSetting['Management']['AcademicProgress']['ExportUserLogin'] && $column == 3) {	// show both student name lang
					$ExportArr[$i_counter][$j_counter++] = $rowDataArr['StudentName2'];
				}
			}
		}
		$i_counter++;
		$j_counter = 0;
	}
	
	### Export Content
	$exportColumn = array("Report Title", $ReportTitle, "", "Report Type", $ReportTypeDisplay);
	$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
	
	intranet_closedb();
	
	// Output the file to user browser
	$filename = "$ProgressReportTitle.csv";
	$filename = str_replace(' ', '_', $filename);
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>