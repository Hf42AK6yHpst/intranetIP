<?php
#  Editing by ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_mgmt_progress_reportId", "ReportID");
$arrCookies[] = array("ck_mgmt_progress_classLevelId", "ClassLevelID");	
$arrCookies[] = array("ck_mgmt_progress_subjectId", "SubjectID");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}


 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Management_Progress";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Submission']." ".$eReportCard['Management_Progress'], "submission.php", 1);
$linterface->LAYOUT_START();


### Get initial values if not passed
$FormArr = $lreportcard->GET_ALL_FORMS(1);
$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;

$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
$ReportIDArr = Get_Array_By_Key($ReportTypeArr, 'ReportID');
$ReportID = ($ReportID == "" || !in_array($ReportID, $ReportIDArr)) ? $ReportIDArr[0] : $ReportID;

echo $lreportcard_ui->Get_Management_Progress_Subject_Group_View_UI($ReportID, $ClassLevelID, $SubjectID);
?>
<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function(){

});

function js_Change_View_Mode() {
	$('form#form1')	.attr('action', 'submission.php')
					.submit();
}

function js_Show_Layer(jsSubjectGroupID, jsClickedObjID, jsFloatDivID, jsFloatContentDivID) {
	$('div#' + jsFloatContentDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	changeLayerPosition(jsClickedObjID, jsFloatDivID, 0, 20);
	MM_showHideLayers(jsFloatDivID, '', 'show');
	
	$('div#' + jsFloatContentDivID).load(
		"ajax_reload.php", 
		{ 
			Action: 'Progress_Subject_Group_Layer',
			ReportID: '<?=$ReportID?>',
			SubjectGroupID: jsSubjectGroupID
		},
		function(returnString)
		{
			
		}
	);
}

function js_Hide_Layer(jsFloatDivID) {
	MM_showHideLayers(jsFloatDivID, '', 'hide');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>