<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
 
intranet_auth();
intranet_opendb();

if (!isset($UploadType) || $UploadType=="")
	header("Location: index.php");

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	
	$fcm = new form_class_manage();
	$lexport = new libexporttext();
	$lreportcard = new libreportcardcustom();
	$curReportCardYearId = $lreportcard->schoolYearID;
	
	
	$uploadType = $_POST['UploadType'];
	$classId = $_POST['ClassID'];
	
	
	### get related student
	$classInfoAry = $lreportcard->GET_CLASSES_BY_FORM('', $classId);
	$formId = $classInfoAry[0]['ClassLevelID'];
	$className = $classInfoAry[0]['ClassName'];
	$studentAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL('', $formId, $classId);
	$numOfStudent = count($studentAry);
	$studentIdAry = Get_Array_By_Key($studentAry, 'UserID');	
	
	
	### get other info field
	$otherInfoItemAry = $lreportcard->getOtherInfoConfig($uploadType);
	$numOfOtherInfoItem = count($otherInfoItemAry);
	
	
	### get all past academic year
	$academicYearAry = $fcm->Get_Academic_Year_List('', $OrderBySequence=0);
	$numOfAcademicYear = count($academicYearAry);
	
	$startCountAcademicYear = false;
	$pastAcademicYearAry = array();
	$termAssoAry = array();
	$lreportcardAry = array();
	for ($i=0; $i<$numOfAcademicYear; $i++) {
		$_academicYearId = $academicYearAry[$i]['AcademicYearID'];
		
		if ($_academicYearId == $curReportCardYearId) {
			$startCountAcademicYear = true;
		}
		
		if ($startCountAcademicYear) {
			// skip invalid academic year
			$lreportcardAry[$_academicYearId] = new libreportcard($_academicYearId);
			$_schoolYear = $lreportcardAry[$_academicYearId]->schoolYear;
			if ($_schoolYear == '') {
				continue;
			}
		
			$pastAcademicYearAry[] = $academicYearAry[$i];
			$termAssoAry[$_academicYearId] = $fcm->Get_Academic_Year_Term_List($_academicYearId); 
		}
	}
	$numOfPastAcademicYear = count($pastAcademicYearAry);
	
	// convert to asc order 
	$pastAcademicYearAry = array_reverse($pastAcademicYearAry);
	
	
	### Consolidate export data
	$otherInfoAry = array();
	$withDataAcademicYearAry = array();
	for ($i=0; $i<$numOfPastAcademicYear; $i++) {
		$_academicYearId = $pastAcademicYearAry[$i]['AcademicYearID'];
		
		$otherInfoAry[$_academicYearId] = BuildMultiKeyAssoc($lreportcardAry[$_academicYearId]->Get_Student_OtherInfo_Data($uploadType, $TermID='', $ClassID='', $studentIdAry), array('StudentID', 'ItemCode', 'TermID'), array('Information'), $SingleValue=1);
		
		if (count($otherInfoAry[$_academicYearId]) > 0) {
			$withDataAcademicYearAry[] = $pastAcademicYearAry[$i];
		}
	}
	$numOfWithDataAcademicYear = count($withDataAcademicYearAry);
	
	
	### Build export header array
	$exportHeaderAry = array();
	$exportDataAry = array();
	for ($i=0; $i<$numOfOtherInfoItem; $i++) {
		$_titleEn = $otherInfoItemAry[$i]['EnglishTitle'];
		$_titleCh = $otherInfoItemAry[$i]['ChineseTitle'];
		
		if ($i <= 4) {
			// class name, class number, userlogin, websams number, student name
			$exportHeaderAry[] = $_titleEn;
			$exportDataAry[0][] = $_titleCh;
		}
		else {
			// remove "(" and ")"
			$_titleCh = substr($_titleCh, 1, -1);
			
			for ($j=0; $j<$numOfWithDataAcademicYear; $j++) {
				$__academicYearId = $withDataAcademicYearAry[$j]['AcademicYearID'];
				$__academicYearNameEn = $withDataAcademicYearAry[$j]['YearNameEN'];
				$__academicYearNameCh = $withDataAcademicYearAry[$j]['YearNameB5'];
				$__academicYearName = Get_Lang_Selection($__academicYearNameCh, $__academicYearNameEn);
				
				$__termAry = $termAssoAry[$__academicYearId];
				$__numOfTerm = count((array)$__termAry);
				for ($k=0; $k<$__numOfTerm; $k++) {
					$___termEn = $__termAry[$k]['YearTermNameEN'];
					$___termCh = $__termAry[$k]['YearTermNameB5'];
					$___termName = Get_Lang_Selection($___termCh, $___termEn);
					
					$___columnNameEn = $__academicYearName.' '.$___termName.' '.$_titleEn;
					$___columnNameCh = $__academicYearName.' '.$___termName.' '.$_titleCh;
					$exportHeaderAry[] = $___columnNameEn;
					$exportDataAry[0][] = '('.$___columnNameCh.')';
				}
			}
		}
	}
	
	
	### Build export data array
	$rowCount = 1;
	for ($i=0; $i<$numOfStudent; $i++) {
		$_studentId = $studentAry[$i]['UserID'];
		$_classNameEn = $studentAry[$i]['ClassTitleEn'];
		$_classNameCh = $studentAry[$i]['ClassTitleCh'];
		$_className = Get_Lang_Selection($_classNameCh, $_classNameEn);
		$_classNumber = $studentAry[$i]['ClassNumber'];
		$_userLogin = $studentAry[$i]['UserLogin'];
		$_websamsRegNo = $studentAry[$i]['WebSAMSRegNo'];
		$_studentName = $studentAry[$i]['StudentName'];
		
		$exportDataAry[$rowCount][] = $_className;
		$exportDataAry[$rowCount][] = $_classNumber;
		$exportDataAry[$rowCount][] = $_userLogin;
		$exportDataAry[$rowCount][] = $_websamsRegNo;
		$exportDataAry[$rowCount][] = $_studentName;
		
		
		// loop each other info item (starts from 5)
		for ($j=5; $j<$numOfOtherInfoItem; $j++) {
			$__otherInfoItemCode = $otherInfoItemAry[$j]['EnglishTitle'];
			
			for ($k=0; $k<$numOfWithDataAcademicYear; $k++) {
				$___academicYearId = $withDataAcademicYearAry[$k]['AcademicYearID'];
				
				$___termAry = $termAssoAry[$___academicYearId];
				$___numOfTerm = count((array)$___termAry);
				for ($l=0; $l<$___numOfTerm; $l++) {
					$____termId = $___termAry[$l]['YearTermID'];
					
					$____studentItemData = $otherInfoAry[$___academicYearId][$_studentId][$__otherInfoItemCode][$____termId];
					$exportDataAry[$rowCount][] = $____studentItemData;
				}
			}
		}
		$rowCount++;
	}
	
	
	### output to browser
	$exportContent = $lexport->GET_EXPORT_TXT($exportDataAry, $exportHeaderAry);
	$filename = 'past_year_data_'.str_replace(' ', '_', $className.'_'.$uploadType).'.csv';
	$lexport->EXPORT_FILE($filename, $exportContent);
		
	intranet_closedb();		
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>