<?php
# using: 

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";

	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

############################################################################################################
########################## Function Start ##############################
function GET_CSV_FILE($ParFolderPath)
{
	if (file_exists($ParFolderPath)) {
		$handle = opendir($ParFolderPath);
		
		
		
		while (($file = readdir($handle))!==false) {
			if($file!="." && $file!="..") {
				
				$SplitArr = explode(".", $file);
				
				# updated on 31 Dec by Ivan
				# the code below will not work with this file name because there are two "."
				# 2008_1_P.6 Fortitude.csv
				# $ext = substr(basename($file), (strpos(basename($file), ".")+1));
				
				# new code
				$ext = $SplitArr[count($SplitArr)-1];
				
				if($ext=="csv") {
					$FileArray[] = $file;
				}
			}
		}
		if (is_array($FileArray) && sizeof($FileArray)>0) {
			sort($FileArray);
		}
		
		closedir($handle);
	}
	return $FileArray;
}
########################### Function End ###############################

$li = new libfilesystem();

if(!isset($UploadType))
	$UploadType = $lreportcard->configFilesType[0];

$targetFolderPath = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/".$UploadType;
$targetURLPath = "/file/reportcard2008/".$lreportcard->schoolYear."/".$UploadType;

$targetFileArray = GET_CSV_FILE($targetFolderPath);

$semesterData = getSemesters($lreportcard->schoolYearID, 1);
$activeYearName = $lreportcard->GET_ACTIVE_YEAR_NAME();


# init variables
$haveWholeYear = false;
$haveAllClass = false;

$YearTermSelectionArr = array();
$YearSelectionArr = array();
$YearClassSelectionArr = array();
if(sizeof($targetFileArray) > 0) {
	$haveFiles = false;
	$ii = 0;
	for($i=0; $i<sizeof($targetFileArray); $i++) {
		$fileName = $targetFileArray[$i];
		# e.g. $fileName = $YearTermID_$YearID_$YearClassID.csv
		
		$last_modified_timestamp = filemtime($targetFolderPath."/".$fileName);
		$last_modified_date = date("Y-m-d H:i:s", $last_modified_timestamp);
		
		$fileNamePieces = explode(".", $fileName);
		$fileNamePieces = explode("_",$fileNamePieces[0]);
		
		$thisYearTermID = $fileNamePieces[0];
		$thisYearID = $fileNamePieces[1];
		$thisYearClassID = $fileNamePieces[2];
		
		### Get Term Info
		if ($thisYearTermID==0) {
			$thisYearTermName = $eReportCard['WholeYear'];
			$haveWholeYear = true;
		} else {
			$thisYearTermName = $semesterData[$thisYearTermID];
			$YearTermSelectionArr[$thisYearTermID] = $thisYearTermName;
		}
		
		### Get Form Info		
		$objYear = new Year($thisYearID);
		$thisYearName = $objYear->YearName;
		$YearSelectionArr[$thisYearID] = $thisYearName;
		
		### Get Class Info
		
		if ($thisYearClassID == 0)
		{
			$thisClassName = $eReportCard['WholeForm'];
			$haveAllClass = true;
		}
		else
		{
			$objClass = new year_class($thisYearClassID);
			$thisClassName = $objClass->Get_Class_Name();
		}
		
		$thisCheckYearClassID = ($thisYearClassID==0)? '' : $thisYearClassID;
		$checkclass = $lreportcard->returnClassTeacherClass($UserID, $thisYearID, '', $thisCheckYearClassID);	
		if($ck_ReportCard_UserType=="ADMIN" || sizeof($checkclass))
		{
			if ($thisYearClassID != 0)
				$YearClassSelectionArr[$thisYearClassID] = $thisClassName;
		}
		else
		{
			continue;	
		}
		
		
		### Check if need to skip the file display
		$thisCompareYearTermID = ($thisYearTermID==0)? '-1' : $thisYearTermID;
		if ($YearTermID != '' && $YearTermID !== $thisCompareYearTermID)
		{
			continue;
		}
			
		if ($YearID != '' && $YearID !== $thisYearID)
		{
			continue;
		}
			
		$thisCompareYearClassID = ($thisYearClassID==0)? '-1' : $thisYearClassID;
		if ($YearClassID != '' && $YearClassID !== $thisCompareYearClassID)
		{
			continue;
		}
		
		$css = ($ii%2?"2":"");
		
		$downloadFileName = str_replace('_unicode', '', $fileName);
		$thisHref = GET_CSV($downloadFileName, $targetFolderPath."/");
			
		$haveFiles = true;
		$x .= "<tr class='tablegreenrow$css'>";
			$x .= "<td class='tabletext' style='text-align:center'>".($ii+1)."</td>";
			$x .= "<td class='tabletext'>$thisYearTermName</td>";
			$x .= "<td class='tabletext'>$thisYearName</td>";
			$x .= "<td class='tabletext'>$thisClassName</td>";
			$x .= "<td class='tabletext'><a href='".$thisHref."' target='_blank' class='tablelink'><img src='".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_files/xls.gif' border='0' hspace='5' align='absmiddle' />".$eReportCard['DownloadCSV']."</a></td>";
			$x .= "<td class='tabletext'>$last_modified_date</td>";
			$x .= "<td class='tabletext' align='center' width='1'><input type='checkbox' name='CsvFileName[]' value='$fileName' />";
			#$x .= "<td class='tabletext' align='center' width='1'><a href=\"javascript:doRemove('".$UploadType."', '".$fileName."')\"><img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_delete.gif\" border=\"0\" align=\"absmiddle\" title='".$button_remove."'></a></td>";
		$x .= "</tr>";
		$ii++;
	}
	if (!$haveFiles)
		$x = "<tr><td class='tabletext' colspan='5' align='center'>".$i_no_record_exists_msg."</td></tr>";
} else {
	$x = "<tr><td class='tabletext' colspan='5' align='center'>".$i_no_record_exists_msg."</td></tr>";
}


### Term Selection
if ($haveWholeYear) {
	$TmpYearTermSelectionArr = $YearTermSelectionArr;
	
	$YearTermSelectionArr = array();
	$YearTermSelectionArr['-1'] = $eReportCard['WholeYear'];
	foreach ((array)$TmpYearTermSelectionArr as $key => $value)
		$YearTermSelectionArr[$key] = $value;
}
$thisTag = "id='YearTermID' name='YearTermID' onChange='this.form.submit()'";
$select_term = getSelectByAssoArray($YearTermSelectionArr, $thisTag, $YearTermID, $isAll=1, $noFirst=0, $eReportCard['AllTerms']);

### Form Selection
$thisTag = "id='YearID' name='YearID' onChange='this.form.submit()'";
$select_form = getSelectByAssoArray($YearSelectionArr, $thisTag, $YearID, $isAll=1, $noFirst=0, $Lang['SysMgr']['FormClassMapping']['All']['Form']);

# select Class
if ($haveAllClass) {
	$TmpYearClassSelectionArr = $YearClassSelectionArr;
	
	$YearClassSelectionArr = array();
	$YearClassSelectionArr['-1'] = $eReportCard['WholeForm'];
	foreach ((array)$TmpYearClassSelectionArr as $key => $value)
		$YearClassSelectionArr[$key] = $value;
}
$thisTag = "id='YearClassID' name='YearClassID' onChange='this.form.submit()'";
$select_class = getSelectByAssoArray($YearClassSelectionArr, $thisTag, $YearClassID, $isAll=1, $noFirst=0, $eReportCard['AllClasses']);

$filterbar = $select_term.$select_form.$select_class;

$toolbar = $linterface->GET_LNK_UPLOAD("javascript:checkNew('file_upload.php?UploadType=".$UploadType."')");

if (sizeof($targetFileArray) > 0) {
	$table_tool = "<table border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td width='21'><img src='{$image_path}/{$LAYOUT_SKIN}/tablegreen_tool_01.gif' width='21' height='23'></td>
						<td background='{$image_path}/{$LAYOUT_SKIN}/tablegreen_tool_02.gif'>
							<table border='0' cellspacing='0' cellpadding='2'>
								<tr>
									<td nowrap=\"nowrap\">
										<a href=\"javascript:checkRemove(document.form1,'CsvFileName[]','file_remove_update.php')\" class=\"tabletool\">
											<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
											".$Lang['Btn']['Delete']."
										</a>
									</td>
								</tr>
							</table>
						</td>
						<td width='6'><img src='{$image_path}/{$LAYOUT_SKIN}/tablegreen_tool_03.gif' width='6' height='23'></td>
					</tr>
				</table>";
} else {
	$table_tool = "";
}

if ($ck_ReportCard_UserType!="ADMIN") {
	$table_tool = "";
}

############################################################################################################
# tag information
$TAGS_OBJ =array();
$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);
$linterface->LAYOUT_START();


### Warning Message
$WarningMsgBox = $linterface->Get_Warning_Message_Box('', $eReportCard['ManagementArr']['OtherInfoArr']['WarningMsg'], $others="");

?>
<br/>
<script language="javascript">
<!--
function doRemove(jUploadType, jFileName){
	var jConfirmMsg = (jFileName=='ALL') ? "<?=$eReportCard['FileRemoveAllConfirm']?>" : "<?=$eReportCard['FileRemoveConfirm']?>";
	if(confirm(jConfirmMsg))
	{
		document.form1.UploadType.value = jUploadType;
		document.form1.FileName.value = jFileName;
		document.form1.action = "file_remove_update.php";
		document.form1.submit();
	}
}
//-->
</script>
<form name="form1" method="post">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?= $linterface->GET_SYS_MSG($Result); ?></td>
	</tr>
	<tr><td colspan="2"><?=$WarningMsgBox?></td></tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$filterbar?><?=$searchbar?></td>
		<td valign="bottom" align="right"><?=$table_tool?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tablegreentop">
		<td class="tabletopnolink" width="5%" align="center">#</td>
		<td class="tabletopnolink" width="15%" align="left"><?=$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term']?></td>
		<td class="tabletopnolink" width="15%" align="left"><?=$eReportCard['Form']?></td>
		<td class="tabletopnolink" width="30%" align="left"><?=$i_ClassName?></td>
		<td class="tabletopnolink" width="20%" align="left"><?=$eReportCard['File']?></td>
		<td class="tabletopnolink" width="15%" align="left"><?=$eReportCard['LastModifiedDate']?></td>
		<td class="tabletopnolink" width="1" align="center"><input type="checkbox" onClick="(this.checked)?setChecked(1,this.form,'CsvFileName[]'):setChecked(0,this.form,'CsvFileName[]')"></td>
	</tr>
	<?=$x?>
</table>
<input type="hidden" name="UploadType" value="<?=$UploadType?>" />
<input type="hidden" name="FileName" />
</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>