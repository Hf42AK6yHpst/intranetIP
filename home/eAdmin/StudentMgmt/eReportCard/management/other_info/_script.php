<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
//include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard = new libreportcard();

$RC_OTHER_INFO_CATEGORY = $lreportcard->Get_Table_Name('RC_OTHER_INFO_CATEGORY');
$RC_OTHER_INFO_ITEM = $lreportcard->Get_Table_Name('RC_OTHER_INFO_ITEM');

$fs = new libfilesystem();
$limport = new libimporttext();

global $eRC_OtherInfoCategoryNameConfig;
$keyed_config = $eRC_OtherInfoCategoryNameConfig;
	
$folderlist = $fs->return_folder(".");

$lreportcard->Start_Trans();
for($i=0 ; $i<sizeof($folderlist);$i++)
{
	$fs->rs = array();
	$subfolderlist = $fs->return_folderlist($folderlist[$i]);
	
	$SchoolName = basename($folderlist[$i]);
	for($j=0;$j<sizeof($subfolderlist);$j++)
	{
		$filepath = $subfolderlist[$j];
		
		if($fs->file_ext($filepath)!='.csv') continue;
		
		$config_name = str_replace("_config.csv","",basename($filepath));
		
		if(!$keyed_config[$config_name]) continue; // skip 
		
		list($CateogryCode, $CategoryNameEn, $CategoryNameCh) = $keyed_config[$config_name];
		
		
		$sql = "
			INSERT INTO
				$RC_OTHER_INFO_CATEGORY
				(CategoryCode, CategoryNameEn, CategoryNameCh, SchoolName)
				VALUES
				('$CateogryCode','$CategoryNameEn','$CategoryNameCh','$SchoolName')
		"; 
		
		$success["Category"][$SchoolName] = $lreportcard->db_db_query($sql);
		
		if(!$success["Category"][$SchoolName])	continue;
		
		$CategoryID = mysql_insert_id();
		
		$data = $limport->GET_IMPORT_TXT($filepath);
		$ItemSqlArr= array();
		$InsertField = array();
		
		if(!$lreportcard->Is_Other_Info_Config_CSV_Valid($data[0])) continue;
		
		for($k=6;$k<sizeof($data);$k++)
		{
			list($ItemNameEn,$ItemNameCh,$RecordType,$Length,$Sample1,$Sample2,$Sample3) = $data[$k];
			
			switch($RecordType)
			{
				case "str": $RecordTypeStr = libreportcard::$OTHER_INFO_ITEM_TYPE_STR; break;
				case "num": $RecordTypeStr = libreportcard::$OTHER_INFO_ITEM_TYPE_NUM; break;
				case "grade": $RecordTypeStr = libreportcard::$OTHER_INFO_ITEM_TYPE_GRADE; break;
			}
			
			$ItemSqlArr[] ="('".$lreportcard->Get_Safe_Sql_Query($ItemNameEn)."','".$lreportcard->Get_Safe_Sql_Query($ItemNameEn)."','".$lreportcard->Get_Safe_Sql_Query(substr($ItemNameCh, 1, -1))."','$RecordTypeStr','$Length','".$lreportcard->Get_Safe_Sql_Query($Sample1)."','".$lreportcard->Get_Safe_Sql_Query($Sample2)."','".$lreportcard->Get_Safe_Sql_Query($Sample3)."','$CategoryID')";
			
			$InsertField[] = $ItemNameEn;
		}
		$ItemSql = implode(",",$ItemSqlArr);
		
		$sql = "
			INSERT INTO
				$RC_OTHER_INFO_ITEM
				(ItemCode, ItemNameEn, ItemNameCh, RecordType, Length, Sample1, Sample2, Sample3, CategoryID)
				VALUES
				$ItemSql
		";
		
		$success["Item"][$SchoolName][$CateogryCode][implode(",",$InsertField)] =  $lreportcard->db_db_query($sql);
	}
	
}

if(in_multi_array(false, $success))
{
	$lreportcard->RollBack_Trans();
	echo "Fail";
}
else
{
	$lreportcard->Commit_Trans();
	echo "Success";
}

//debug_pr($folderlist);
	
intranet_closedb();		
?>