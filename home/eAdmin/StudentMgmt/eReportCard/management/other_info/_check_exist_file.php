<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard = new libreportcardrubrics_custom();

# update cookie
$arrCookies[] = array("ck_other_info_reportid", "ReportID");
$arrCookies[] = array("ck_other_info_classlevelid", "ClassLevelID");	
$arrCookies[] = array("ck_other_info_classid", "ClassID");
updateGetCookies($arrCookies);

# Get data
$ReportID = $_REQUEST['ReportID'];
$CategoryID = $_REQUEST['CategoryID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = $_REQUEST['ClassID'];

$Record = $lreportcard->Get_Student_OtherInfo_Record($ReportID,$CategoryID,$ClassLevelID,$ClassID);

echo empty($Record)?0:1;

intranet_closedb();
?>