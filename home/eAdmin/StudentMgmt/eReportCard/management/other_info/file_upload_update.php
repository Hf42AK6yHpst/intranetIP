<?php
// Using: 

/***************************************************************
 * Modification Log
 * 20190502 Bill:
 *      - prevent SQL Injection + Cross-site Scripting
 * 20120215 Marcus:
 * 		- modified Update_Other_Info_Data, added param $YearID, $YearClassID delete student records using YearID or YearClassID
 * *************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    ### Handle SQL Injection + XSS [START]
    $UploadType = cleanCrossSiteScriptingCode($UploadType);
    $YearTermID = IntegerSafe($YearTermID);
    $YearID = IntegerSafe($YearID);
    $YearClassID = IntegerSafe($YearClassID);
    ### Handle SQL Injection + XSS [END]
    
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	$linterface = new interface_html();
	$OtherInfoArr = unserialize(urldecode($_POST['OtherInfoArr']));
	
	$lreportcard->Start_Trans();
	
	$success = $lreportcard->Update_Other_Info_Data($YearTermID, $UploadType, $OtherInfoArr, $YearID, $YearClassID);
	
	if($success)
	{
		$SuccessMsg = $eReportCard['ImportOtherInfoSuccess'];
		$lreportcard->Commit_Trans();
	}
	else
	{
		$SuccessMsg = $eReportCard['ImportOtherInfoFail'];
		$lreportcard->RollBack_Trans();
	}
	
	# Back Btn
	$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:js_Go_Back_To_Other_Info()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
	
	### Title ###
	# Step Obj 
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
	
	# Tag information
	$TAGS_OBJ = array();
	$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);

	# Page navigation (leave the array empty if no need)
	$PAGE_NAVIGATION[] = array($eReportCard['OtherInfoArr'][$UploadType], "javascript:js_Go_Back_To_Other_Info()");
	$PAGE_NAVIGATION[] = array($eReportCard['Import'], "");
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START();
	?>
	
	<br>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
		</tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td colspan="2" align="center"><?=$BackBtn?></td>
		</tr>
	</table>

<script>
function js_Go_Back_To_Other_Info()
{
	window.location="index.php?UploadType=<?=$UploadType?>&TermID=<?=$YearTermID?>";
}
</script>

	<?
	$linterface->LAYOUT_STOP();
}
else
{
	echo "You have no priviledge to access this page.";
}
?>