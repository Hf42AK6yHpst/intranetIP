<?php
// Using: 

#########################################
#
#   Date:   2019-05-02  Bill
#           - prevent SQL Injection + Cross-site Scripting
#           - Class Selection > Change 'YearClassID' to 'YearClassSelect' to prevent IntegerSafe() 
#
#########################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!isset($UploadType) || $UploadType=="") {
	header("Location: index.php");
}

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	
	if (!in_array($UploadType, $lreportcard->getOtherInfoType())) {
		header("Location: index.php");
	}
	
	### Handle SQL Injection + XSS [START]
	$UploadType = cleanCrossSiteScriptingCode($UploadType);
	
	$TermID = IntegerSafe($TermID);
	$ClassID = IntegerSafe($ClassID);
	### Handle SQL Injection + XSS [END]
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

	# tag information
	$TAGS_OBJ =array();
	$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html();

		if($Result=="wrong_csv_header") {
			$Msg = $Lang['General']['ReturnMessage']['WrongCSVHeader'];
		}
		$linterface->LAYOUT_START($Msg);
		
		echo $lreportcard_ui->Get_Management_OtherInfo_Import_UI($UploadType, $TermID, $ClassID);
?>
<script language="javascript">
function js_Go_Back_To_Other_Info()
{
	if ('<?=$ClassID?>' == '') {
		window.location="index.php?UploadType=<?=$UploadType?>&TermID=<?=$TermID?>";
	}
	else {
		window.location="edit.php?UploadType=<?=$UploadType?>&TermID=<?=$TermID?>&ClassID=<?=$ClassID?>";
	}
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	var jsYearTermID = $('select#YearTermID').val();
	var jsYearClassSelect = $('select#YearClassSelect').val();
	
	if (jsYearClassSelect == '') {
		jsYearClassSelect = 0;
	}
		
	$.post(
		"./ajax_task.php", 
		{ 
			Task: 'Check_Other_Info_Data_Exist',
			UploadType: '<?=$UploadType?>',
			YearTermID: jsYearTermID,
			YearClassSelect: jsYearClassSelect
		},
		function(ReturnData)
		{
			if (ReturnData == 1)
			{
				if (confirm('<?=$eReportCard['AlertExistFile']?>'))
				{
					document.getElementById('form1').submit();
				}
				else
				{
					return false;
				}
			}
			else
			{
				document.getElementById('form1').submit();
			}
		}
	);
}
</script>

<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>