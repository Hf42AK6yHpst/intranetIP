<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$CSVFile = $_REQUEST['CSVFile'];

$lreportcard->Start_Trans();

foreach($CSVFile as $csv)
{
	list($ReportID, $ClassLevelID, $ClassID) = explode("_",$csv);
	
	$Success[$csv] = $lreportcard->Remove_Student_OtherInfo_Data($ReportID, $CategoryID, $ClassLevelID, $ClassID);
}

if(in_array(false,$Success))
{
	$msg = "DeleteUnsuccess";
	$lreportcard->Rollback_Trans();
}
else
{
	$msg = "DeleteSuccess";
	$lreportcard->Commit_Trans();
}

header("location: index.php?CategoryID=$CategoryID&msg=$msg");
	
intranet_closedb();		
?>