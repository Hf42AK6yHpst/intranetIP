<?php
# using: 

############ Change log ################
#
#   Date:   2019-05-02  Bill
#           prevent SQL Injection + Cross-site Scripting
#
#	Date:	2016-06-06	Bill	[2015-0416-1040-06164]
#			add * to grade level
#
########################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php"); 
	
	# Temp
//	$ReportCardCustomSchoolName = "lutheran_academy";
//	$ReportCardCustomSchoolName = "hkuga_college";

	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
    if ($lreportcard->hasAccessRight())
    {
        ### Handle SQL Injection + XSS [START]
        $UploadType = cleanCrossSiteScriptingCode($UploadType);
        
        $TermID = IntegerSafe($TermID);
        $ClassID = IntegerSafe($ClassID);
        ### Handle SQL Injection + XSS [END]
        
        $linterface = new interface_html();
        $lreportcard_ui = new libreportcard_ui();

		$TAGS_OBJ = array();
		$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);
		$linterface->LAYOUT_START();
		
		echo $linterface->Include_Cookies_JS_CSS();
		echo $linterface->Include_OverlayTextarea_JS_CSS();
		echo $lreportcard_ui->Get_Other_Info_Edit_UI($UploadType, $TermID, $ClassID);
?>

<script>
function js_Go_Back_To_Other_Info()
{
	window.location="index.php?UploadType=<?=$UploadType?>&TermID=<?=$TermID?>";
}

function js_Reload_Page() {
	var TermID = $('select#TermID').val();
	var ClassID = $('select#ClassID').val();
	
	$.cookies.set('eRC_Mgmt_OtherInfo_TermID', TermID.toString());
	window.location="edit.php?UploadType=<?=$UploadType?>&ClassID=" + ClassID + "&TermID=" + TermID;
}

function js_Check_Form()
{
	$("div.WarnMsgDiv").hide();
	var errObj = new Array();
	
	$("input.num").each(function(){
		var thisObj = $(this);
		if(thisObj.val() && !IsNumeric(thisObj.val()))
		{
			$("div#WarnDiv"+thisObj.attr("id")).show();
			errObj.push(thisObj);
		}
	});

	//var patt=/^[a-zA-Z][+|-]?$/;
	var patt=/^[a-zA-Z][+|\*|-]?$/;
	$("input.grade").each(function(){
		var thisObj = $(this);
		if(thisObj.val() && !patt.test(thisObj.val()))
		{
			$("div#WarnDiv"+thisObj.attr("id")).show();
			errObj.push(thisObj);
		}
	});

	if(errObj.length>0)
	{
		errObj[0].focus();
		return false;	
	}
	
	$('form#form1').attr('action', 'edit_update.php');
}

function js_Import()
{
	var TermID = $("#TermID").val();

	window.location = "file_upload.php?UploadType=<?=$UploadType?>&TermID=<?=$TermID?>&ClassID=<?=$ClassID?>";	
}

function goExportPastData() {
	$('form#form1').attr('action', 'export_all_year.php').submit();
}

$(function(){
	$("textarea").overlayTextarea({opacity:0.85, highlightParent:true, triggerWidthLimit:300});
}); 
function jsApplyToAll(targetValueElement, targetClass)
{
	var targetValue = $('#' + targetValueElement).val();
	
	$('.' + targetClass).each( function () {
		$(this).val(targetValue);
	});
}
</script>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>