<?php
// Using :

/**************************************************************
 *	Modification log:
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - Create File   ($eRCTemplateSetting['Management']['ConductGradeCalculation'])
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    $lreportcard = new libreportcardcustom();
}
else {
    $lreportcard = new libreportcard();
}

if (!$eRCTemplateSetting['Management']['ConductGradeCalculation'] || !$lreportcard->hasAccessRight()) {
    intranet_closedb();
    header("Location:/index.php");
    exit();
}

$TermID = '';
if (isset($_GET['TermID'])) {
    $TermID = $_GET['TermID'];
}
$reportTermID = ($TermID == '' || $TermID == 0) ? 'F' : $TermID;

$FormID = '';
if (isset($_GET['FormID'])) {
    $FormID = $_GET['FormID'];
}

$ClassID = '';
if (isset($_GET['ClassID'])) {
    $ClassID = $_GET['ClassID'];
}

// loop reports
$reportYearIDIncluded = array();
$reportList = $lreportcard->Get_Report_List($FormID, '', 1);
foreach($reportList as $thisReportInfo)
{
    if($thisReportInfo['Semester'] == $reportTermID)
    {
        if(in_array($thisReportInfo['ClassLevelID'], $reportYearIDIncluded)) {
            continue;
        }
        $reportYearIDIncluded[] = $thisReportInfo['ClassLevelID'];

        // Remove old generated conduct grades
        //$lreportcard->Delete_Student_Conduct_Grade($thisReportInfo['ReportID']);

        // loop classes
        $classArr = $lreportcard->GET_CLASSES_BY_FORM($thisReportInfo['ClassLevelID']);
        for($i=0; $i<sizeof($classArr); $i++) {
            if($ClassID != '') {
                if($ClassID != $classArr[$i]['ClassID']) {
                    continue;
                }
            }

            // loop class students
            $studentArr = $lreportcard->GET_STUDENT_BY_CLASS($classArr[$i]['ClassID']);
            for($j=0; $j<sizeof($studentArr); $j++) {
                list($conductScore, $conductGrade) = $lreportcard->Calculate_Student_Conduct_Score_Grade($thisReportInfo['ReportID'], $studentArr[$j]['UserID']);

                $studentConductRecord = $lreportcard->Get_Student_Conduct_Grade($thisReportInfo['ReportID'], array($studentArr[$j]['UserID']));
                if(empty($studentConductRecord)) {
                    $dataArr = array();
                    $dataArr['ReportID'] = $thisReportInfo['ReportID'];
                    $dataArr['StudentID'] = $studentArr[$j]['UserID'];
                    $dataArr['ConductScore'] = $conductScore;
                    $dataArr['ConductGrade'] = $conductGrade;
                    $lreportcard->Insert_Student_Conduct_Grade($dataArr);
                }
                else {
                    $lreportcard->Update_Student_Conduct_Grade($thisReportInfo['ReportID'], $studentArr[$j]['UserID'], $conductScore, $conductGrade);
                }
            }
        }
    }
}

intranet_closedb();

header("Location: index.php?FormID=$FormID&TermID=$TermID&msg=AddSuccess");
?>