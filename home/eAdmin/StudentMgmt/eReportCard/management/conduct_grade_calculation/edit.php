<?php
# using:

############ Change log ################
#
#  20200428 Bill:  [2019-0924-1029-57289]
#      - Create File   ($eRCTemplateSetting['Management']['ConductGradeCalculation'])
#
########################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
        $lreportcard = new libreportcardcustom();
    }
    else {
        $lreportcard = new libreportcard();
    }

    $CurrentPage = "Management_ConductGradeCalculation";
    $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    $TAGS_OBJ[] = array($eReportCard['Management_ConductGradeCalculation'], "", 0);

    if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['Management']['ConductGradeCalculation'])
    {
        ### Handle SQL Injection + XSS [START]
        if($TermID != '') {
            $TermID = IntegerSafe($TermID);
        }
        $ClassID = IntegerSafe($ClassID);
        ### Handle SQL Injection + XSS [END]

        $linterface = new interface_html();
        $lreportcard_ui = new libreportcard_ui();

        $linterface->LAYOUT_START();

        echo $linterface->Include_Cookies_JS_CSS();
        echo $lreportcard_ui->Get_Conduct_Grade_Modify_Edit_UI($TermID, $ClassID);
?>

<script>
    function js_Go_Back_To_Other_Info()
    {
        window.location = "index.php?TermID=<?=$TermID?>";
    }

    function js_Reload_Page() {
        var TermID = $('select#TermID').val();
        var ClassID = $('select#ClassID').val();

        $.cookies.set('eRC_Mgmt_ConductGradeCal_TermID', TermID.toString());
        window.location="edit.php?ClassID=" + ClassID + "&TermID=" + TermID;
    }

    function js_Check_Form()
    {
        $('input#submitBtn').attr('disabled', true);
        $('form#form1').attr('action', 'update.php');
    }
</script>

<?
        $linterface->LAYOUT_STOP();
        intranet_closedb();
    }
    else
    {
        ?>
        You have no priviledge to access this page.
        <?
    }
}
else
{
    ?>
    You have no priviledge to access this page.
    <?
}
?>