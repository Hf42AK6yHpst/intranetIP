<?php
// Using :

/**************************************************************
 *	Modification log:
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - Create File   ($eRCTemplateSetting['Management']['ConductGradeCalculation'])
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
        $lreportcard = new libreportcardcustom();
    }
    else {
        $lreportcard = new libreportcard();
    }

    $CurrentPage = "Management_ConductGradeCalculation";
    $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    $TAGS_OBJ[] = array($eReportCard['Management_ConductGradeCalculation'], "", 0);

    if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['Management']['ConductGradeCalculation'])
    {
        $linterface = new interface_html();
        $lreportcard_ui = new libreportcard_ui();

        $TermID = '';
        if (isset($_GET['TermID'])) {
            $TermID = $_GET['TermID'];
        }
        else if (isset($_COOKIE['eRC_Mgmt_ConductGradeCal_TermID'])) {
            $TermID = $_COOKIE['eRC_Mgmt_ConductGradeCal_TermID'];
        }
        else {
            $CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
            if ($lreportcard->schoolYearID == $CurrentYearTermArr['AcademicYearID']) {
                $TermID = $CurrentYearTermArr['YearTermID'];
            }
        }
        if ($TermID == '') {
            $TermID = 0;
        }

        $FormID = '';
        if (isset($_GET['FormID'])) {
            $FormID = $_GET['FormID'];
        }
        else if (isset($_COOKIE['eRC_Mgmt_ConductGradeCal_FormID'])) {
            $FormID = $_COOKIE['eRC_Mgmt_ConductGradeCal_FormID'];
        }

        $ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
        $linterface->LAYOUT_START($ReturnMsg);

        echo $linterface->Include_Cookies_JS_CSS();
        echo $lreportcard_ui->Get_Conduct_Grade_Calculation_Overview_UI($TermID, $FormID);
?>

<script>
    $(document).ready( function() {
        <? if($clearCoo) { ?>
            $.cookies.del('eRC_Mgmt_ConductGradeCal_TermID');
            $.cookies.del('eRC_Mgmt_ConductGradeCal_FormID');
        <? } ?>
    });

    function js_Generate() {
        var FormID = $("#FormID").val();
        var TermID = $("#TermID").val();

        if(confirm('<?=$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['GenerateConduct']?>')) {
            window.location = "generate.php?FormID=" + FormID + "&TermID=" + TermID;
        }
    }

    function js_Reload_Other_Info_Table() {
        var FormID = $("#FormID").val();
        var TermID = $("#TermID").val();

        window.location = "index.php?FormID=" + FormID + "&TermID=" + TermID;
    }
</script>

<?
        $linterface->LAYOUT_STOP();
        intranet_closedb();
    }
    else
    {
        ?>
        You have no priviledge to access this page.
        <?
    }
}
else
{
    ?>
    You have no priviledge to access this page.
    <?
}
?>