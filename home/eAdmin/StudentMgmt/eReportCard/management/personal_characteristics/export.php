<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
	
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


# initialize data
$Data['SubjectID'] = $_GET['SubjectID'];
$Data['ReportID'] = $_GET['ReportID'];
$Data['ClassLevelID'] = $_GET['ClassLevelID'];
$Data['ClassID'] = $_GET['ClassID'];
$Data['SubjectGroupID'] = $_GET['SubjectGroupID'];
$FromExport = $_GET['FromExport'];
		
		
$ExportArr = array();
$lexport = new libexporttext();


# Get all students from the class
if ($Data['SubjectGroupID']=='') {
	$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
}
else {
	$StudentInfoArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 0);
}
$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
$numOfStudent = count($StudentInfoArr);


# Get all characteristics of the subject of the class level
$CharInfoArr = $lreportcard->GET_CLASSLEVEL_CHARACTERISTICS($Data);
$numOfChar = count($CharInfoArr);


### Get Scale Info
$ScaleInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale();
$ScaleIDArr = Get_Array_By_Key($ScaleInfoArr, 'ScaleID');
$ScaleInfoAssoArr = BuildMultiKeyAssoc($ScaleInfoArr, 'ScaleID');
$ScaleDefaultInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale('', '', $IsDefault=1);


### Get Student Personal Char Info
//$StudentPersonalCharAssoArr[$ReportID][$SubjectID][$StudentID]['comment' or $scaleID] = data
$StudentPersonalCharAssoArr = $lreportcard->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $ReportID, $SubjectID, $ReturnScaleID=1, $ReturnCharID=1, $ReturnComment=1);


for ($i=0; $i<$numOfStudent; $i++) {	
	$thisStudentID = $StudentInfoArr[$i]['UserID'];
	$thisClassName = $StudentInfoArr[$i]['ClassName'];
	$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
	$thisStudentName = $StudentInfoArr[$i]['StudentName'];
	
	$thisColumeCount = 0;
	$ExportArr[$i][$thisColumeCount++] = $thisClassName;
	$ExportArr[$i][$thisColumeCount++] = $thisClassNumber;
	$ExportArr[$i][$thisColumeCount++] = $thisStudentName;
	
	for ($j=0; $j<$numOfChar; $j++) {
		$thisCharID = $CharInfoArr[$j]['CharID'];
		$thisScaleID = $StudentPersonalCharAssoArr[$ReportID][$SubjectID][$thisStudentID][$thisCharID];
		$thisScaleID = ($thisScaleID=='' || !in_array($thisScaleID, (array)$ScaleIDArr))? $ScaleDefaultInfoArr[0]['ScaleID'] : $thisScaleID;
		
		if ($FromExport) {
			$thisScaleName = Get_Lang_Selection($ScaleInfoAssoArr[$thisScaleID]['NameCh'], $ScaleInfoAssoArr[$thisScaleID]['NameEn']);
			$thisScaleName = ($thisScaleName=='')? $Lang['General']['EmptySymbol'] : $thisScaleName;
			
			$ExportArr[$i][$thisColumeCount++] = $thisScaleName;
		}
		else {
			$thisScaleCode = $ScaleInfoAssoArr[$thisScaleID]['Code'];
			$ExportArr[$i][$thisColumeCount++] = $thisScaleCode;
		}
	}
	
	if($eRCTemplateSetting['PersonalCharacteristicComment']) {
		$thisComment = $StudentPersonalCharAssoArr[$ReportID][$SubjectID][$thisStudentID]['comment'];
		$ExportArr[$i][$thisColumeCount++] = $thisComment;
	}
}

# Define column title
$exportColumnArr = $eReportCard['PersonalChar']['ExportTitleArr'];
$exportColumnPropertyArr = array(1, 1, 2);
for ($i = 0 ; $i < $numOfChar; $i++) {
	$exportColumnArr['En'][] = $CharInfoArr[$i]['CharTitleEn'];
	$exportColumnArr['Ch'][] = $CharInfoArr[$i]['CharTitleCh'];
	$exportColumnPropertyArr[] = 1;
}
if($eRCTemplateSetting['PersonalCharacteristicComment'])
{
	$exportColumnArr['En'][] = $eReportCard['PersonalChar']['ExportTitle']['Comment']['En'];
	$exportColumnArr['Ch'][] = $eReportCard['PersonalChar']['ExportTitle']['Comment']['Ch'];
}

# define column title (2 dimensions array, 1st row is english, 2nd row is chinese)
$lexport = new libexporttext();
$exportColumn = $lexport->GET_EXPORT_HEADER_COLUMN($exportColumnArr, $exportColumnPropertyArr);

$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

# Class Name
if ($SubjectGroupID != '')
{
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$Class_SG_Name = $obj_SubjectGroup->Get_Class_Title();
}
else
{
	$lclass = new libclass();
	$Class_SG_Name = $lclass->getClassName($ClassID);
}

# Subject Name
if ($SubjectID == 0)
	$subjectTitle = $eReportCard['Template']['OverallCombined'];
else
	$subjectTitle = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID, $intranet_session_language);
	
if ($FromExport)
{
	//$filename = "export_personal_characteristics_$className_$subjectTitle.csv";
	$filename = $subjectTitle."_".$Class_SG_Name."_PersonalCharacteristics.csv";
}
else
	$filename = "sample_personal_characteristics_$className_$subjectTitle.csv";

$filename = str_replace(' ', '_', $filename);

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
