<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
	
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");


if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# initialize data
$StudentIDArr = $_POST['StudentID'];
$CharIDArr = $_POST['CharID'];
$ClassLevelID = $_POST['ClassLevelID'];
$ClassID = $_POST['ClassID'];
$ReportID = $_POST['ReportID'];
$SubjectID = $_POST['SubjectID'];
$SubjectGroupID = $_POST['SubjectGroupID'];
$FromMS = $_POST['FromMS'];


$DataArr['ReportID'] = $ReportID;
$DataArr['SubjectID'] = $SubjectID;
$DataArr['SubjectGroupID'] = $SubjectGroupID;

$processList = array();

for ($i=0; $i<sizeof($StudentIDArr); $i++)
{
	$DataArr['StudentID'] = $StudentIDArr[$i];	
	$DataArr['Comment'] = trim(stripslashes($_POST['comment_'.$DataArr['StudentID']]));
	
	$data_temp = array();
	for ($j=0; $j<sizeof($CharIDArr); $j++)	{
		$CharID = $CharIDArr[$j];

		# get student performance of this characteristics
		$performance = ${"char_".$DataArr['StudentID']."_".$CharID};
		
		$data_temp[] = $CharID.":".$performance;
	}
	$DataArr['CharData'] = implode(",", $data_temp);
	
	
	if ($lreportcard->IS_STUDENT_CHARACTERISTICS_RECORD_EXIST($DataArr)) {
		# update	
		$result = $lreportcard->UPDATE_STUDENT_CHARACTERISTICS_RECORD($DataArr);
	}
	else {
		# insert
		$result = $lreportcard->INSERT_STUDENT_CHARACTERISTICS_RECORD($DataArr);
	}
	$processList[] = ($result)? true : false;
}

$ReturnMsgKey = in_array(false, $processList)? 'UpdateUnsuccess' : 'UpdateSuccess';

intranet_closedb();
header("Location: edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&ReturnMsgKey=$ReturnMsgKey&FromMS=$FromMS");
?>