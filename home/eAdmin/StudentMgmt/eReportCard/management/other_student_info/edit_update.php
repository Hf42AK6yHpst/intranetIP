<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();
 
$PageRight = "TEACHER";

//if (!isset($_POST) && (!isset($StudentID) || !isset($ciphours) || !isset($attendance) || !isset($conduct) || !isset($cca)) )
if (!isset($_POST) && (!isset($StudentID) || !isset($ciphours) || !isset($attendance) || !isset($conduct) || !isset($napfa)))
	header("Location:index.php");

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:index.php");
}

# Initialization
$success = true;
$entries = array();

// StudentID
$StudentIDArr = $StudentID;

// OtherStudentInfoID
$OtherStudentInfoIDArr = array();
if(isset($otherStudentInfoID)){
	$OtherStudentInfoIDArr = array_keys($otherStudentInfoID);
} 


if(sizeof($StudentIDArr) > 0){
	$table = $lreportcard->DBName.".RC_OTHER_STUDENT_INFO";
	
	for($i=0 ; $i<count($StudentIDArr) ; $i++){
		$StudentID = $StudentIDArr[$i];
		$std_cip = $ciphours[$StudentID];
		$std_attendance = $attendance[$StudentID];
		$std_conduct = $conduct[$StudentID];
		$std_cca = $cca[$StudentID];
		$std_napfa = $napfa[$StudentID];
		
		# Main
		if(in_array($StudentID, $OtherStudentInfoIDArr)){	# UPDATE
			$sql  = "UPDATE 
						$table 
					SET 
						CIPHours = ".(($std_cip != "") ? "'$std_cip'" : "NULL").", 
						Attendance = ".(($std_attendance != "") ? "'$std_attendance'" : " NULL").", 
						Conduct = ".(($std_conduct != "") ? "'$std_conduct'" : "NULL").", 
						CCA = '$std_cca', 
						NAPFA = ".(($std_napfa != "") ? "'$std_napfa'" : "NULL").", 
						TeacherID = '".$lreportcard->uid."', 
						DateModified = NOW() 
					WHERE 
						OtherStudentInfoID = '$otherStudentInfoID[$StudentID]'";
			$success = $lreportcard->db_db_query($sql);
		} else { 		# INSERT
			$entries[]  = "('$StudentID', '$ReportID', 
							".(($std_cip != "") ? "'".$std_cip."'" : "NULL").", 
							".(($std_attendance != "") ? "'".$std_attendance."'" : "NULL").", 
							".(($std_conduct != "") ? "'".$std_conduct."'" : "NULL").", 
							".(($std_napfa != "") ? "'".$std_napfa."'" : "NULL").", 
							'$std_cca', '".$lreportcard->uid."', NOW(), NOW() )";
		}
	}
	
	if(count($entries) > 0){
		$field = "StudentID, ReportID, CIPHours, Attendance, Conduct, NAPFA, CCA, TeacherID, DateInput, DateModified";
		$entries = implode(",", $entries);
		$sql = "INSERT INTO $table ($field) VALUES $entries";
		$success = $lreportcard->db_db_query($sql);
	}
}


intranet_closedb();

$Result = ($success) ? "update" : "update_failed";

if($IsSaveBefCancel){
	header("Location: index.php?Result=$Result");
} else {
	$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&ClassID=$ClassID";
	header("Location: edit.php?$params&Result=$Result");
}
?>

