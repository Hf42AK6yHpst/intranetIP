<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

#################################################################################################

$lreportcard = new libreportcard();

$lexport = new libexporttext();


# Get Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

# Get Student Info
$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
$StudentSize = sizeof($StudentArr);

$StudentIDArr = array();
if($StudentSize > 0){
	for($i=0 ; $i<$StudentSize ; $i++)
		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
}


# Header 
# Comma
$Content = "WebSAMSRegNumber,ClassName,ClassNumber,StudentName";
$Content .= ",CipHours,Attendance,Conduct,NAPFA,CCARemarks";
$Content .= "\r\n";
# 20140722 SIS HIDE CIP NAPFA
# Tab 
$exportColumn = array("WebSAMSRegNumber", "ClassName", "ClassNumber", "StudentName");
if(!$eRCTemplateSetting['OtherStudentInfo']['HideCIP']){
	$exportColumn[] = "CipHours";
}
$exportColumn[] = "Attendance";
$exportColumn[] = "Conduct";
if(!$eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']){
	$exportColumn[] = "NAPFA";
}
$exportColumn[] = "CCARemarks";


# content
$Value = '';
$row = array();
$rows = array();

for($j=0; $j<$StudentSize; $j++){
	list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArr[$j];
	
	# Comma Separator 
	$Content .= $WebSAMSRegNo.",".$ClassName.",".$StudentNo.",".$StudentName;
	$Content .= ",,,,";
	$Content .= "\r\n";
	
	# Tab Space Separator
	$row[] = trim($WebSAMSRegNo);
    $row[] = trim($ClassName);
	$row[] = trim($StudentNo);
	$row[] = trim($StudentName);
	$rows[] = $row;
	unset($row);
	
	
}

#################################################################################################

intranet_closedb();

// Output the file to user browser
$filename = "other_student_info_template.csv";

# Comma Separator  
//output2browser($Content, $filename);

# Tab Space Separator
$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

?>