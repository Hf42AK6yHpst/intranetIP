<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
//	include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
//	$lreportcard = new libreportcardSIS();
 if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard2008();
}
	
	$CurrentPage = "Management_OtherStudentInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
		############################################################################################################
		
		# Class
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		if (sizeof($ClassArr) > 0) {
			for($i=0 ; $i<count($ClassArr) ;$i++) {
				if($ClassArr[$i][0] == $ClassID)
					$ClassName = $ClassArr[$i][1];
			}
			
			# Period (Term or Whole Year)
			$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = $ReportInfo['SemesterTitle'];
		}
		
		
		
		# Button
	    $display_button = '';
		$display_button .= '<input name="Confirm" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_confirm.'" onclick="jSUBMIT_FORM()">';
        $display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">';
        $display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="jBACK()">';
	    
		
		############################################################################################################
        
		$PAGE_NAVIGATION[] = array($button_import);
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_OtherStudentInfo'], "", 0);
		$linterface->LAYOUT_START();
		
		$params = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
?>

<script language="javascript">
function jCHECK_FORM(){
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	
	if(!jCHECK_FORM(obj))
		return;
		
	obj.submit();
}

function jBACK(){
	var form_id = document.getElementById("ClassLevelID").value;
	var report_id = document.getElementById("ReportID").value;
	var class_id = document.getElementById("ClassID").value;
	
	location.href = "edit.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&ClassID="+class_id;
}
</script>

<br/>
<form name="FormMain" method="post" action="import_update.php" enctype="multipart/form-data">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	    <tr>
	      <td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	  	</tr>
	  	<tr>
	      <td width="30px">&nbsp;</td>
	      <td>
	      	<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr>
	            <td><?=$eReportCard['Period']?> : <strong><?=$PeriodName?></strong></td>
	          	<td><?=$eReportCard['Class']?> : <strong><?=$ClassName?></strong></td>
	          </tr>
	      	</table>
	      	<br/>
	      	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="tabletext">
	      	  <tr>
	      	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eReportCard['File']?></td>
	      	    <td><input type="file" name="userfile" size=25><br /><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td>
	      	  </tr>
	      	  <tr>
				<td colspan="2">
				<a class="tablelink" href="javascript:self.location.href='generate_csv_template.php?<?=$params?>'"><img src="<?=$PATH_WRT_ROOT?>/images/2007a/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"> <?=$eReportCard['DownloadCSVFile']?></a>
				</td>
			  </tr>
	      	</table>
	      </td>
	    </tr>
	  </table>
	  <br/>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
        	</table>
          </td>
      	</tr>
      </table>
	</td>
  </tr>
</table>

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>

</form>




<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>