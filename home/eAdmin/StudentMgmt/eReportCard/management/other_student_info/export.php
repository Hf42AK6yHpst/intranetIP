<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

#################################################################################################
 if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008();
	}
//include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
//$lreportcard = new libreportcardSIS();
//$lreportcard = new libreportcard();

$lexport = new libexporttext();

# Get Data
// Period (Term x, Whole Year, etc)
$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

// Student Info
$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
$StudentSize = sizeof($StudentArr);

$StudentIDArr = array();
if(count($StudentArr) > 0){
	for($i=0 ; $i<sizeof($StudentArr) ; $i++)
		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
}


# Initialization
$row = array();
$rows = array();
$exportColumn = array();
$otherStudentInfo = array();


# Get DB Data 
$otherStudentInfo = $lreportcard->GET_OTHER_STUDENT_INFO($StudentIDArr, $ReportID);
# 20140722 SIS HIDE CIP NAPFA
# Header 
$exportColumn[] = "WebSAMSRegNumber";
$exportColumn[] = "ClassName";
$exportColumn[] = "ClassNumber";
$exportColumn[] = "StudentName";
if(!$eRCTemplateSetting['OtherStudentInfo']['HideCIP']){
	$exportColumn[] = "CipHours";
}
$exportColumn[] = "Attendance";
$exportColumn[] = "Conduct";
if(!$eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']){
	$exportColumn[] = "NAPFA";
}
$exportColumn[] = "CCARemarks";


# content
for($j=0; $j<$StudentSize; $j++){
	list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArr[$j];
	
	$row[] = trim($WebSAMSRegNo);
    $row[] = trim($ClassName);
	$row[] = trim($StudentNo);
	$row[] = trim($StudentName);
	
	# Assign Data
	if(isset($otherStudentInfo[$StudentID]) ){
		if(!$eRCTemplateSetting['OtherStudentInfo']['HideCIP']){
			$row[] = $otherStudentInfo[$StudentID]['CIPHours'];
		}
		$row[] = $eReportCard['OtherStudentInfo_Attendance'][$otherStudentInfo[$StudentID]['Attendance']];
		$row[] = $eReportCard['OtherStudentInfo_Conduct'][$otherStudentInfo[$StudentID]['Conduct']];
		if(!$eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']){
			$row[] = $eReportCard['OtherStudentInfo_NAPFA'][$otherStudentInfo[$StudentID]['NAPFA']];
		}
		$row[] = $otherStudentInfo[$StudentID]['CCA'];
	} else {
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
	}
	
	$rows[] = $row;
	unset($row);
}





#################################################################################################

$set1 = array("&nbsp;", " ");
$set2 = array("-", ",");
$filename = str_replace($set1, "", $Prefix."Other_Student_Info_".trim(str_replace($set2, "_", $PeriodName))."_".trim($ClassName).".csv");

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($Content, $filename);

?>